﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Cardinal.EasyApps.ED.Common.ViewModel;
using Cardinal.EasyApps.ED.Model;

namespace Cardinal.EasyApps.ED.TransProc.ViewModel
{
    /// <summary>
    /// Main Windows ViewModel
    /// </summary>
    public class MainWindowsViewModel : ViewModelBase, IDataErrorInfo
    {
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;

        ObservableCollection<Lote> _lotesAProcesarYTransferir;
        ObservableCollection<LoteConError> _lotesConError;
        ObservableCollection<Lote> _historialLotes;

        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <returns>The error message for the property. The default is an empty string ("").</returns>
        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                //switch (columnName)
                //{
                //}
                return error;
            }
        }

        #region Constructors

        public MainWindowsViewModel(Cardinal.EasyApps.ED.Common.Dal.IRepository repository)
        {
            _repository = repository;
            RefrescarListas();
        }

        #endregion Constructors

        #region Propertys

        /// <summary>
        /// Obtener la Version de la Aplicacion
        /// </summary>
        /// <returns></returns>

        public static string AppVersion
        {
            get
            {
                return Cardinal.EasyApps.ED.Common.Utils.AppVersion.GetAppVersion();
            }
        }

        /// <summary>
        /// Gets the name of the aplication.
        /// </summary>
        /// <returns></returns>

        public static string ApplicationName
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "EasyDoc - {0}", AppVersion);
            }
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <returns></returns>

        public static string Title
        {
            get
            {
                return Properties.Resources.MainFormTitle;
            }
        }

        public ObservableCollection<Lote> LotesAProcesarYTransferir
        {
            get
            {
                return _lotesAProcesarYTransferir;
            }
            set
            {
                if (_lotesAProcesarYTransferir != value)
                {
                    _lotesAProcesarYTransferir = value;
                    OnNotifyPropertyChanged("LotesAProcesarYTransferir");
                    OnNotifyPropertyChanged("PendientesHeader");
                }
            }
        }

        public ObservableCollection<Lote> HistorialLotes
        {
            get
            {
                return _historialLotes;
            }
            set
            {
                if (_historialLotes != value)
                {
                    _historialLotes = value;
                    OnNotifyPropertyChanged("HistorialLotes");
                }
            }
        }

        public ObservableCollection<LoteConError> LotesConError
        {
            get
            {
                return _lotesConError;
            }
            set
            {
                if (_lotesConError != value)
                {
                    _lotesConError = value;
                    OnNotifyPropertyChanged("LotesConError");
                }
            }
        }

        public string PendientesHeader
        {
            get
            {
                if (LotesAProcesarYTransferir == null)
                {
                    return string.Format("Pendientes: SIN INFO");
                }
                else
                {
                    return string.Format("Pendientes: {0}", LotesAProcesarYTransferir.Count);
                }
            }
        }

        public int CantidadImagenesLoteProximoProcesar
        {
            get
            {
                if (LotesAProcesarYTransferir == null || LotesAProcesarYTransferir.Count == 0)
                {
                    return 0;
                }
                else
                {
                    var lote = LotesAProcesarYTransferir[0];

                    lote.Imagenes = _repository.GetImagenesLote(lote).ToList();
                    return lote.Imagenes.Count;
                }
            }
        }

        #endregion Propertys

        #region Method

        public void RefrescarListas()
        {
            LotesAProcesarYTransferir = new ObservableCollection<Lote>(_repository.GetLotesAProcesarYTransferir());
            //_lotesConError = new ObservableCollection<LoteConError>(_repository.GetLotesConErrorTransferenciaYProcesamiento());
            //_historialLotes = new ObservableCollection<Lote>(_repository.GetLotesHistorialTransferenciaYProcesamiento());
        }

        public void ProcesarTransferirLote(BackgroundWorker worker)
        {
            if (LotesAProcesarYTransferir.Count > 0)
            {
                var pathLocalProcesamiento = ConfigurationManager.AppSettings["PathLocalProcesamiento"];
                var lote = LotesAProcesarYTransferir[0];

                lote.Imagenes = _repository.GetImagenesLote(lote).ToList();

                worker.ReportProgress(0, string.Format("Procesando Lote: {0}", lote.CodigoLote));

                //Preparar Lotes Si No estan En Directorios
                // Procesar Lote
                int i = 0;
                foreach (var img in lote.Imagenes)
                {
                    worker.ReportProgress(i, string.Format("Procesando Lote: {0} Imagen: {1}", lote.CodigoLote, img.NroOrden));
                    //Estan En el Directorio del Server
                    string imgPathLocal = img.GetPathFull(pathLocalProcesamiento, true);
                    bool exists = System.IO.File.Exists(imgPathLocal);

                    if (!exists)
                    {
                        //Existe el Directorio?
                        bool existsDir = System.IO.Directory.Exists(@System.IO.Path.GetDirectoryName(imgPathLocal));
                        if (!existsDir)
                        {
                            System.IO.Directory.CreateDirectory(@System.IO.Path.GetDirectoryName(imgPathLocal));
                        }

                        //Si no hay que copiarlas
                        System.IO.File.Copy(@img.PathRedImagenFull, @imgPathLocal, true);
                    }

                    Cardinal.EasyApps.ED.Procesamiento.ProcesarImagen.ProcesarUnaImagen(imgPathLocal, lote, img.NroOrden, img.Indice);

                    //Copio al Servidor
                    //Si no hay que copiarlas
                    System.IO.File.Copy(@imgPathLocal, @img.PathRedImagenFull, true);

                    i++;
                    worker.ReportProgress(i, string.Format("Fin Proceso Lote: {0} Imagen: {1}", lote.CodigoLote, img.NroOrden));
                    System.Threading.Thread.Sleep(100);
                }
            }
            else
            {
                worker.ReportProgress(1, string.Format("No Hay Lotes a Procesar"));
            }
        }

        #endregion Method
    }
}
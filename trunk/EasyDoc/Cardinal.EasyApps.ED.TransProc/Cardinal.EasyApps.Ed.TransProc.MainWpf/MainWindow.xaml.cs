﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace Cardinal.EasyApps.ED.TransProc.MainWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadRibbonWindow
    {
        BackgroundWorker _worker = new BackgroundWorker();
        DispatcherTimer _procesoTimer = new DispatcherTimer();
        bool _detenerProceso;

        private class Argumentos
        {
            public ViewModel.MainWindowsViewModel ViewModel { get; set; }

            public Argumentos(ViewModel.MainWindowsViewModel viewModel)
            {
                ViewModel = viewModel;
            }
        }

        /// <summary>
        /// Contructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            _worker.WorkerReportsProgress = true;
            _worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _procesoTimer.Tick += new EventHandler(_procesoTimer_Tick);
            _procesoTimer.Interval = new TimeSpan(0, 0, 1);
        }

        void _procesoTimer_Tick(object sender, EventArgs e)
        {
            _procesoTimer.Stop();
            //    //_worker = new BackgroundWorker();
            nombreLoteLabel.Content = "Empezando a Procesar";
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
            radProgressBar1.Maximum = 1;
            radProgressBar1.Value = 0;
            radProgressBar1.Minimum = 0;

            if (mainWindowsViewModel.LotesAProcesarYTransferir.Count == 0)
            {
                nombreLoteLabel.Content = "Sin Lotes para procesar";
                ValidarArranqueTimer();
            }
            else
            {
                radProgressBar1.Maximum = mainWindowsViewModel.CantidadImagenesLoteProximoProcesar;
                _worker.RunWorkerAsync(new Argumentos(mainWindowsViewModel));
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            nombreLoteLabel.Content = e.UserState.ToString();
            radProgressBar1.Value = e.ProgressPercentage;
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            nombreLoteLabel.Content = "PROCESO TERMINADO";
            radProgressBar1.Value = radProgressBar1.Maximum;
            ValidarArranqueTimer();
            RefrescarListas();
        }

        private void ValidarArranqueTimer()
        {
            if (!_detenerProceso)
            {
                _procesoTimer.Start();
            }
            else
            {
                ValidarEstadoProceso();
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var argumentos = (Argumentos)e.Argument;
            argumentos.ViewModel.ProcesarTransferirLote(_worker);

            System.Threading.Thread.Sleep(1000);
        }

        private void RadRibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = ViewModelLocator.MainWindowViewModel;

            mainRibbon.ApplicationName = ViewModel.MainWindowsViewModel.ApplicationName;
            mainRibbon.Title = ViewModel.MainWindowsViewModel.Title;

            RefrescarListas();
        }

        private void RefrescarListas()
        {
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
            mainWindowsViewModel.RefrescarListas();
        }

        private void RadRibbonButton_Click(object sender, RoutedEventArgs e)
        {
            RefrescarListas();
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            _procesoTimer.Start();
            _detenerProceso = false;
            ValidarEstadoProceso();
        }

        private void ValidarEstadoProceso()
        {
            Uri src = null;
            if (_procesoTimer.IsEnabled || _worker.IsBusy)
            {
                IniciarButton.IsEnabled = false;
                DetenerButton.IsEnabled = true;

                if (_detenerProceso)
                {
                    estadoProcesoLabel.Content = "Iniciado - Marcado Para Detenerse";
                    src = new Uri(@"/TransProc.MainWpf;component/Images/Annotate_Warning.ico", UriKind.Relative);
                }
                else
                {
                    estadoProcesoLabel.Content = "Iniciado";
                    src = new Uri(@"/TransProc.MainWpf;component/Images/Annotate_Default.ico", UriKind.Relative);
                }
            }
            else
            {
                IniciarButton.IsEnabled = true;
                DetenerButton.IsEnabled = false;
                estadoProcesoLabel.Content = "Detenido";
                src = new Uri(@"/TransProc.MainWpf;component/Images/Annotate_Error.ico", UriKind.Relative);
            }
            BitmapImage img = new BitmapImage(src);
            estadoProcesoImage.Source = img;
            //estadoProcesoImage.Source = new ImageSourceConverter().ConvertFromString(packUri) as ImageSource;
        }

        private void RadRibbonButton_Click_2(object sender, RoutedEventArgs e)
        {
            _detenerProceso = true;
            ValidarEstadoProceso();
        }
    }
}
﻿using System.Configuration;
using Ninject;

namespace Cardinal.EasyApps.ED.TransProc.MainWpf
{
    public static class ViewModelLocator
    {
        static IKernel kernel;

        //static ViewModelLocator()
        //{
        //    IKernel kernel = new StandardKernel();
        //    kernel.Bind<Cardinal.EasyApps.ED.Common.Dal.IRepository>()
        //    .To<Cardinal.EasyApps.ED.Dal.Repository.Repository>()
        //    .WithConstructorArgument("connectionString",
        //                             ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString);
        //}

        public static ViewModel.MainWindowsViewModel MainWindowViewModel
        {
            get
            {
                return new ViewModel.MainWindowsViewModel(new Cardinal.EasyApps.ED.Dal.EntityFramework.Repository(ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString));
            }
        }
    }
}
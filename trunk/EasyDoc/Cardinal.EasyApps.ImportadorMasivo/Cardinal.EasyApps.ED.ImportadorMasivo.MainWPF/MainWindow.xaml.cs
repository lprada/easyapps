﻿using Cardinal.EasyApps.ED.Model;
using Cardinal.EasyApps.ED.ImportadorMasivo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Cardinal.EasyApps.ED.ImportadorMasivo.MainWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadRibbonWindow
    {
        private static Accusoft.ImagXpressSdk.ImagXpress imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                this.DataContext = ViewModelLocator.MainWindowViewModel;

                mainRibbon.ApplicationName = ViewModel.MainWindowsViewModel.ApplicationName;
                mainRibbon.Title = ViewModel.MainWindowsViewModel.Title;
            }
        }

        private void RefrescarListas()
        {
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
            mainWindowsViewModel.RefrescarListas(mainWindowsViewModel.PathSeleccionado);
        }

        private void RefrescarListas(string path)
        {
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
            mainWindowsViewModel.RefrescarListas(path);
        }

        private void RadRibbonButton_Click(object sender, RoutedEventArgs e)
        {
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
            //Selecciono Directorio
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.Description = "Por Favor seleccione el directorio donde se encuentran los lotes a importar";

            //dialog.RootFolder = System.Environment.SpecialFolder.Desktop;
            dialog.SelectedPath = mainWindowsViewModel.PathImportacionDefault;
            dialog.ShowNewFolderButton = false;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                RefrescarListas(dialog.SelectedPath);
            }
        }

        private void RadMenuItem_Click_5(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            if (lotesImportarRadGridView.Items.Count > 0)
            {
                if (MessageBox.Show("Esta Seguro propagar el dato de la Plantilla del Primer Registros a los Seleccionados?(ESTO REEMPLAZARA EL DATO EXISTENTE)", "Propagar dato de Caja",
                                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    List<string> lotes = new List<string>();
                    Cardinal.EasyApps.ED.Model.LoteImportar firstRow = (Cardinal.EasyApps.ED.Model.LoteImportar)lotesImportarRadGridView.Items[0];
                    foreach (Cardinal.EasyApps.ED.Model.LoteImportar row in lotesImportarRadGridView.SelectedItems)
                    {
                        lotes.Add(row.CodigoLote);
                    }
                    var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
                    mainWindowsViewModel.ModificarValorPlantilla(firstRow.CodigoPlantillaVolumen, new System.Collections.ObjectModel.ReadOnlyCollection<string>(lotes));
                    lotesImportarRadGridView.ItemsSource = null;
                    lotesImportarRadGridView.ItemsSource = mainWindowsViewModel.LotesAImportar;
                }
            }
        }

        private void RadMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            if (lotesImportarRadGridView.Items.Count > 0)
            {
                if (MessageBox.Show("Esta Seguro propagar el dato de la Caja del Primer Registros a los Seleccionados?(ESTO REEMPLAZARA EL DATO EXISTENTE)", "Propagar dato de Caja",
                                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    List<string> lotes = new List<string>();
                    Cardinal.EasyApps.ED.Model.LoteImportar firstRow = (Cardinal.EasyApps.ED.Model.LoteImportar)lotesImportarRadGridView.Items[0];
                    foreach (Cardinal.EasyApps.ED.Model.LoteImportar row in lotesImportarRadGridView.SelectedItems)
                    {
                        lotes.Add(row.CodigoLote);
                    }
                    var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
                    mainWindowsViewModel.ModificarValorCaja(firstRow.CodigoCaja, new System.Collections.ObjectModel.ReadOnlyCollection<string>(lotes));
                    lotesImportarRadGridView.ItemsSource = null;
                    lotesImportarRadGridView.ItemsSource = mainWindowsViewModel.LotesAImportar;
                }
            }
        }

        private void ImportarLotes(MainWindowsViewModel mainWindowsViewModel)
        {
            int i = 0;

            InicializarProcesoImportacion();
            foreach (Cardinal.EasyApps.ED.Model.LoteImportar row in lotesImportarRadGridView.SelectedItems)
            {
                i = mainWindowsViewModel.ImportarLote(i, row);
            }

            FinalizarProcesoImportacion();
        }

        private void FinalizarProcesoImportacion()
        {
            Uri src;
            BitmapImage img;
            src = new Uri(@"/Cardinal.EasyApps.ED.ImportadorMasivo.MainWPF;component/Images/Annotate_Error.ico", UriKind.Relative);
            img = new BitmapImage(src);
            estadoProcesoImage.Source = img;
            MessageBox.Show("Fin de Importacion", "Importar Lotes", MessageBoxButton.OK, MessageBoxImage.Information);
            estadoProcesoLabel.Content = "Proceso Finalizado.";
            RefrescarListas();
        }

        private void FinalizarProcesoCompresion()
        {
            Uri src;
            BitmapImage img;
            src = new Uri(@"/Cardinal.EasyApps.ED.ImportadorMasivo.MainWPF;component/Images/Annotate_Error.ico", UriKind.Relative);
            img = new BitmapImage(src);
            estadoProcesoImage.Source = img;
            MessageBox.Show("Fin del Proceso de Conversion", "Realizar Compresion de Imagenes", MessageBoxButton.OK, MessageBoxImage.Information);
            estadoProcesoLabel.Content = "Proceso Finalizado.";
            RefrescarThumbnailsLotesSeleccionados();
        }

        private void InicializarProcesoImportacion()
        {
            Uri src;
            BitmapImage img;
            src = null;
            src = new Uri(@"/Cardinal.EasyApps.ED.ImportadorMasivo.MainWPF;component/Images/Annotate_Warning.ico", UriKind.Relative);
            estadoProcesoLabel.Content = "Importando";
            img = new BitmapImage(src);
            estadoProcesoImage.Source = img;
        }

        private void InicializarProcesoCompresion()
        {
            Uri src;
            BitmapImage img;
            src = null;
            src = new Uri(@"/Cardinal.EasyApps.ED.ImportadorMasivo.MainWPF;component/Images/Annotate_Warning.ico", UriKind.Relative);
            estadoProcesoLabel.Content = "Comprimiendo Imagenes";
            img = new BitmapImage(src);
            estadoProcesoImage.Source = img;
        }

        private bool ValidacionesImportacion(MainWindowsViewModel mainWindowsViewModel)
        {
            foreach (Cardinal.EasyApps.ED.Model.LoteImportar row in lotesImportarRadGridView.SelectedItems)
            {
                if (mainWindowsViewModel.CodigoCajaObligatorio && string.IsNullOrWhiteSpace(row.CodigoCaja))
                {
                    MessageBox.Show("Debe Ingresar el Codigo de Caja para importar los lotes seleccionados.", "Importar Lotes", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
                if (string.IsNullOrWhiteSpace(row.CodigoPlantillaVolumen))
                {
                    MessageBox.Show("Debe seleccionar la Plantilla  y el Volumen para importar los lotes seleccionados.", "Importar Lotes", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }

                if (!mainWindowsViewModel.ValidarCaja(row))
                {
                    MessageBox.Show(String.Format("La Caja {0} no exite, por favor dar de Alta.", row.CodigoCaja), "Importar Lotes", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }
            return true;
        }

        private void lotesImportarRadGridView_SelectionChanged(object sender, SelectionChangeEventArgs e)
        {
            RefrescarThumbnailsLotesSeleccionados();
        }

        private void RefrescarThumbnailsLotesSeleccionados()
        {
            verImagenesThumbnailWpfUserControlUserControl1.ClearThumbnails();
            lotesImportarRadGridView.IsEnabled = false;
            foreach (Cardinal.EasyApps.ED.Model.LoteImportar row in lotesImportarRadGridView.SelectedItems)
            {
                verImagenesThumbnailWpfUserControlUserControl1.AddToThumbnails(System.IO.Directory.GetFiles(row.PathLocalLote));
            }
            lotesImportarRadGridView.IsEnabled = true;
        }

        private void radGridView_RowValidating(object sender, GridViewRowValidatingEventArgs e)
        {
            LoteImportar lote = e.Row.DataContext as LoteImportar;
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;

            //Valido si Existe la Caja
            if (!mainWindowsViewModel.ValidarCaja(lote))
            {
                GridViewCellValidationResult validationResult = new GridViewCellValidationResult();
                validationResult.PropertyName = "Caja";
                validationResult.ErrorMessage = String.Format("La Caja {0} no exite, por favor dar de Alta.", lote.CodigoCaja);
                e.ValidationResults.Add(validationResult);
                e.IsValid = false;
            }
        }

        private void RadMenuItem_Click_1(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            if (lotesImportarRadGridView.Items.Count > 0)
            {
                if (MessageBox.Show("Esta Seguro de realizar la compresion de las imagenes del Lote?(ESTO MODIFICARA LAS IMAGENES)", "Realizar Compresion de Imagenes",
                                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    ComprimirLotes();
                }
            }
        }

        private void ComprimirLotes()
        {
            InicializarProcesoCompresion();
            List<LoteImportar> lotes = new List<LoteImportar>();
            foreach (Cardinal.EasyApps.ED.Model.LoteImportar row in lotesImportarRadGridView.SelectedItems)
            {
                lotes.Add(row);
            }
            var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
            mainWindowsViewModel.RealizarCompresionImagenes(new System.Collections.ObjectModel.ReadOnlyCollection<LoteImportar>(lotes));

            FinalizarProcesoCompresion();
        }

        private void IniciarConCompresionButton_Click(object sender, RoutedEventArgs e)
        {
            if (lotesImportarRadGridView.Items.Count > 0)
            {
                if (MessageBox.Show("Esta Seguro de realizar la compresion de las imagenes del Lote y Luego Importar los mismos?(ESTO MODIFICARA LAS IMAGENES)", "Realizar Compresion de Imagenes e Importacion",
                                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
                    if (!ValidacionesImportacion(mainWindowsViewModel))
                    {
                        return;
                    }
                    ComprimirLotes();
                    ImportarLotes(mainWindowsViewModel);

                }
            }
        }

        private void RadRibbonButton_Click_Importar(object sender, RoutedEventArgs e)
        {
            if (lotesImportarRadGridView.Items.Count > 0)
            {
                if (MessageBox.Show("Esta Seguro de realizar la importacion de los Lotes?", "Importacion de Lotes",
                                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var mainWindowsViewModel = this.DataContext as ViewModel.MainWindowsViewModel;
                    if (!ValidacionesImportacion(mainWindowsViewModel))
                    {
                        return;
                    }

                    ImportarLotes(mainWindowsViewModel);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Cardinal.EasyApps.ED.ImportadorMasivo.MainWPF
{
    /// <summary>
    /// 
    /// </summary>
    public static class ViewModelLocator
    {
        //static IKernel kernel;

        ////static ViewModelLocator()
        ////{
        ////    IKernel kernel = new StandardKernel();
        ////    kernel.Bind<Cardinal.EasyApps.ED.Common.Dal.IRepository>()
        ////    .To<Cardinal.EasyApps.ED.Dal.Repository.Repository>()
        ////    .WithConstructorArgument("connectionString",
        ////                             ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString);
        ////}

        /// <summary>
        /// Gets the main window view model.
        /// </summary>
        public static ViewModel.MainWindowsViewModel MainWindowViewModel
        {
            get
            {
                return new ViewModel.MainWindowsViewModel(new Cardinal.EasyApps.ED.Dal.EntityFramework.Repository(ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString));
            }
        }
    }
}

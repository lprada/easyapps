﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cardinal.EasyApps.ED.Common.ViewModel;
using System.ComponentModel;
using System.Globalization;
using System.Collections.ObjectModel;
using Cardinal.EasyApps.ED.Model;
using System.Configuration;

namespace Cardinal.EasyApps.ED.ImportadorMasivo.ViewModel
{
    /// <summary>
    /// Main Windows ViewModel
    /// </summary>
    public class MainWindowsViewModel : ViewModelBase, IDataErrorInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;
        ObservableCollection<LoteImportar> _lotesAImportar;
        private string _nombreLoteEnProceso;
        private static Accusoft.ImagXpressSdk.ImagXpress imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress();

        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <returns>The error message for the property. The default is an empty string ("").</returns>
        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                //switch (columnName)
                //{
                //}
                return error;
            }
        }

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowsViewModel"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public MainWindowsViewModel(Cardinal.EasyApps.ED.Common.Dal.IRepository repository)
        {
            _repository = repository;
            PathSeleccionado = string.Empty;
        }

        #endregion Constructors

        #region Propertys

        /// <summary>
        /// Obtener la Version de la Aplicacion
        /// </summary>
        /// <returns></returns>

        public static string AppVersion
        {
            get
            {
                return Cardinal.EasyApps.ED.Common.Utils.AppVersion.GetAppVersion();
            }
        }

        /// <summary>
        /// Gets the name of the aplication.
        /// </summary>
        /// <returns></returns>

        public static string ApplicationName
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "EasyDoc - {0}", AppVersion);
            }
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        /// <returns></returns>

        public static string Title
        {
            get
            {
                return Properties.Resources.ImportadorMasivo;
            }
        }

        private string _pathSeleccionado;

        /// <summary>
        /// Gets or sets the path seleccionado.
        /// </summary>
        /// <value>
        /// The path seleccionado.
        /// </value>
        public string PathSeleccionado
        {
            get
            {
                return _pathSeleccionado;
            }
            set
            {
                _pathSeleccionado = value;
                OnNotifyPropertyChanged("PathSeleccionado");
            }
        }

        /// <summary>
        /// Gets or sets the lotes A importar.
        /// </summary>
        /// <value>
        /// The lotes A importar.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ObservableCollection<LoteImportar> LotesAImportar
        {
            get
            {
                return _lotesAImportar;
            }
            set
            {
                if (_lotesAImportar != value)
                {
                    _lotesAImportar = value;
                    OnNotifyPropertyChanged("LotesAImportar");
                }
            }
        }

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        public ObservableCollection<Cliente> Empresas
        {
            get
            {
                return new ObservableCollection<Cliente>(_repository.GetEmpresas());
            }
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        public ObservableCollection<PlantillaVolumen> Plantillas
        {
            get
            {
                return new ObservableCollection<PlantillaVolumen>(_repository.GetPlantillasVolumenes().OrderBy(p => p.Cliente.Nombre).ThenBy(p => p.Nombre).ThenBy(p => p.Volumen.CodigoVolumen));
            }
        }

        /// <summary>
        /// Gets a value indicating whether [codigo caja obligatorio].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [codigo caja obligatorio]; otherwise, <c>false</c>.
        /// </value>
        public bool CodigoCajaObligatorio
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return Convert.ToBoolean(appConfig["CodigoCajaObligatorio"]);
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the path importacion default.
        /// </summary>
        public string PathImportacionDefault
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return appConfig["PathImportacionDefault"];
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets the proximo codigo lote.
        /// </summary>
        /// <returns></returns>
        public string GetProximoCodigoLote(string path)
        {
            var appConfig = ConfigurationManager.AppSettings;

            return _repository.GetProximoNombreLote(path, appConfig["PrefijoLote"], CodigoEstacionEscaneoImportacion, appConfig["MascaraNumeroLote"]);
        }

        /// <summary>
        /// Gets the chrominance.
        /// </summary>
        public int Chrominance
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return Convert.ToInt32(appConfig["Chrominance"]);
                }
                catch
                {
                    return 20;
                }
            }
        }

        /// <summary>
        /// Gets the luminance.
        /// </summary>
        public int Luminance
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return Convert.ToInt32(appConfig["Luminance"]);
                }
                catch
                {
                    return 20;
                }
            }
        }

        /// <summary>
        /// Gets the codigo estacion escaneo importacion.
        /// </summary>
        public string CodigoEstacionEscaneoImportacion
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return appConfig["CodigoEstacionEscaneoImportacion"];
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets or sets the path lotes importados.
        /// </summary>
        /// <value>
        /// The path lotes importados.
        /// </value>
        public string PathLotesImportados
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return appConfig["PathLotesEstacionLocal"];
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether [usa transferencia].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [usa transferencia]; otherwise, <c>false</c>.
        /// </value>
        public bool UsaTransferencia
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return Convert.ToBoolean(appConfig["UsaTransferencia"]);
                }
                catch
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Gets the plantillas separar lote importacion.
        /// </summary>
        public string PlantillasSepararLoteImportacion
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return appConfig["PlantillasSepararLoteImportacion"];
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets the cantidad imagenes separar lote importacion.
        /// </summary>
        public int CantidadImagenesSepararLoteImportacion
        {
            get
            {
                try
                {
                    var appConfig = ConfigurationManager.AppSettings;

                    return Convert.ToInt32(appConfig["CantidadImagenesSepararLoteImportacion"]);
                }
                catch
                {
                    return -1;
                }
            }
        }

        /// <summary>
        /// Gets or sets the nombre lote en proceso.
        /// </summary>
        /// <value>
        /// The nombre lote en proceso.
        /// </value>
        public string NombreLoteEnProceso
        {
            get
            {
                return _nombreLoteEnProceso;
            }
            set
            {
                _nombreLoteEnProceso = value;
                System.Windows.Forms.Application.DoEvents();
                OnNotifyPropertyChanged("NombreLoteEnProceso");
            }
        }

        private int _valorProceso;

        /// <summary>
        /// Gets or sets the valor proceso.
        /// </summary>
        /// <value>
        /// The valor proceso.
        /// </value>
        public int ValorProceso
        {
            get
            {
                return _valorProceso;
            }
            set
            {
                _valorProceso = value;
                System.Windows.Forms.Application.DoEvents();

                OnNotifyPropertyChanged("ValorProceso");
            }
        }

        private int _valorMaximoProceso;

        /// <summary>
        /// Gets or sets the valor maximo proceso.
        /// </summary>
        /// <value>
        /// The valor maximo proceso.
        /// </value>
        public int ValorMaximoProceso
        {
            get
            {
                return _valorMaximoProceso;
            }
            set
            {
                _valorMaximoProceso = value;
                System.Windows.Forms.Application.DoEvents();

                OnNotifyPropertyChanged("ValorMaximoProceso");
            }
        }

        private string _mensajeProceso;

        /// <summary>
        /// Gets or sets the mensaje proceso.
        /// </summary>
        /// <value>
        /// The mensaje proceso.
        /// </value>
        public string MensajeProceso
        {
            get
            {
                return _mensajeProceso;
            }
            set
            {
                _mensajeProceso = value;
                System.Windows.Forms.Application.DoEvents();

                OnNotifyPropertyChanged("MensajeProceso");
            }
        }

        #endregion Propertys

        #region Method

        /// <summary>
        /// Modificars the valor caja.
        /// </summary>
        /// <param name="codigoCaja">The codigo caja.</param>
        /// <param name="codigosLotes">The codigos lotes.</param>
        public void ModificarValorCaja(string codigoCaja, ReadOnlyCollection<string> codigosLotes)
        {
            if (codigosLotes != null)
            {
                foreach (var item in LotesAImportar)
                {
                    if (codigosLotes.Contains(item.CodigoLote))
                    {
                        item.CodigoCaja = codigoCaja;
                    }
                }
                OnNotifyPropertyChanged("LotesAImportar");
            }
        }

        /// <summary>
        /// Modificars the valor plantilla.
        /// </summary>
        /// <param name="codigoPlantillaVolumen">The codigo plantilla volumen.</param>
        /// <param name="codigosLotes">The codigos lotes.</param>
        public void ModificarValorPlantilla(string codigoPlantillaVolumen, ReadOnlyCollection<string> codigosLotes)
        {
            if (codigosLotes != null)
            {
                foreach (var item in LotesAImportar)
                {
                    if (codigosLotes.Contains(item.CodigoLote))
                    {
                        item.CodigoPlantillaVolumen = codigoPlantillaVolumen;
                    }
                }
                OnNotifyPropertyChanged("LotesAImportar");
            }
        }

        /// <summary>
        /// Refrescars the listas.
        /// </summary>
        /// <param name="path">The path.</param>
        public void RefrescarListas(string path)
        {
            PathSeleccionado = path;
            LotesAImportar = new ObservableCollection<LoteImportar>(GetLotesImportar(path));
            //_lotesConError = new ObservableCollection<LoteConError>(_repository.GetLotesConErrorTransferenciaYProcesamiento());
            //_historialLotes = new ObservableCollection<Lote>(_repository.GetLotesHistorialTransferenciaYProcesamiento());
            OnNotifyPropertyChanged("Empresas");
            OnNotifyPropertyChanged("Plantillas");
        }

        private static List<LoteImportar> GetLotesImportar(string path)
        {
            List<LoteImportar> res = new List<LoteImportar>();

            //Me fijo todos los directorios del Directorio que no tengas terminacion en _YAIMPO_YYYYMMDD
            var dirs = System.IO.Directory.GetDirectories(path);

            foreach (var dir in dirs)
            {
                if (!dir.Contains("YAIMPO"))
                {
                    LoteImportar l = new LoteImportar();
                    l.CantidadImagenes = System.IO.Directory.GetFiles(dir).Count();
                    l.CodigoLote = dir;
                    l.FechaCreacion = DateTime.Now;
                    l.Plantilla = null;
                    l.Tag = null;
                    l.PathLocalLote = dir;
                    res.Add(l);
                }
            }
            return res;
        }

        /// <summary>
        /// Importars the lote.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        public int ImportarLote(int i, Cardinal.EasyApps.ED.Model.LoteImportar row)
        {
            imagXpress1.Licensing.UnlockRuntime(1908224035, 373698848, 1341601282, 16216);
            i++;
            NombreLoteEnProceso = row.CodigoLote;

            //Importo el Lote
            //Copiar Lote a Directorio
            //Obtengo el Codigo de Plantilla y el codigo de volumen
            //var info = row.CodigoPlantillaVolumen.Split(new string[] { ";***;" }, StringSplitOptions.None);
            //string codigoPlantilla = info[0];
            //string codigoVolumen = info[1];

            //Ver Si hay que separar

            string destPath = string.Empty;
            //Si la plantilla del Lote es de separar lotes los separo
            if (PlantillasSepararLoteImportacion.Contains(row.CodigoPlantilla))
            {
                //int cantImagenesEnLote = 0;
                //Genero la Cantidad necesaria de Lotes de Importacion como Imagenes haya
                ObtenerImagenesImportables(row.PathLocalLote, row);
                if (row.Imagenes.Count <= CantidadImagenesSepararLoteImportacion)
                {
                    destPath = ImportarLoteEasyDoc(row);
                }
                else
                {
                    //copio a directorios dentro del otro las imagenes para importar
                    var files = System.IO.Directory.GetFiles(row.PathLocalLote);
                    int imgSubDirectorio = 0;
                    int numeroSubLote = 1;
                    foreach (var file in files)
                    {
                        string pathSubLote = System.IO.Path.Combine(row.PathLocalLote, String.Format("SUBDIR{0}", numeroSubLote));
                        if (imgSubDirectorio == 0)
                        {
                            bool exists = System.IO.Directory.Exists(@pathSubLote);
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(pathSubLote);
                            }
                        }
                        System.IO.File.Copy(@file, System.IO.Path.Combine(pathSubLote, System.IO.Path.GetFileName(file)), true);
                        imgSubDirectorio++;

                        if (imgSubDirectorio == CantidadImagenesSepararLoteImportacion)
                        {
                            imgSubDirectorio = 0;
                            numeroSubLote++;
                        }
                    }

                    int numeroSubLoteImportado = 0;
                    var directories = System.IO.Directory.GetDirectories(row.PathLocalLote);
                    int cantDirs = 0;
                    foreach (var dir in directories)
                    {
                        if (!dir.Contains("ANTESCOMPRESION"))
                        {
                            cantDirs++;
                        }
                    }
                    foreach (var dir in directories)
                    {
                        if (!dir.Contains("ANTESCOMPRESION")) {
                            LoteImportar loteImportar = new LoteImportar();
                            loteImportar.CantidadImagenes = row.CantidadImagenes;
                            loteImportar.CodigoCaja = row.CodigoCaja;
                            loteImportar.CodigoEstacionEscaneoImportacion = row.CodigoEstacionEscaneoImportacion;
                            loteImportar.CodigoLote = row.CodigoLote;
                            loteImportar.CodigoLoteEasyDoc = row.CodigoLoteEasyDoc;
                            loteImportar.CodigoPlantillaVolumen = row.CodigoPlantillaVolumen;
                            loteImportar.FechaCreacion = row.FechaCreacion;
                            loteImportar.PathLocalLote = dir;
                            if (string.IsNullOrWhiteSpace(row.Tag))
                            {
                                loteImportar.Tag = string.Format("{0} de {1}", numeroSubLoteImportado + 1, cantDirs);
                            }
                            else
                            {
                                loteImportar.Tag = string.Format("{0} - {1} de {2}", row.Tag, numeroSubLoteImportado + 1, cantDirs);
                            }
                            loteImportar.UsaTransferencia = row.UsaTransferencia;
                            numeroSubLoteImportado++;
                            ImportarLoteEasyDoc(loteImportar);
                        }
                    }
                }
            }
            else
            {
                destPath = ImportarLoteEasyDoc(row);
            }
            //Renombrar Directorio para no volver a Importar
            System.IO.Directory.Move(row.PathLocalLote,
                                     String.Format("{0}_YAIMPO_{1}", row.PathLocalLote, DateTime.Now.ToString("yyyyMMdd")));

            ValorProceso = i;
            return i;
        }

        private string ImportarLoteEasyDoc(LoteImportar row)
        {
            MensajeProceso = "Obteniendo Numero de Lote en EasyDoc";
            string destDirPath = System.IO.Path.Combine(PathLotesImportados, row.CodigoVolumen);
            row.CodigoLoteEasyDoc = GetProximoCodigoLote(destDirPath);
            row.UsaTransferencia = UsaTransferencia;
            row.CodigoEstacionEscaneoImportacion = CodigoEstacionEscaneoImportacion;
            //// Move Batch Files to Transfer Dir
            MensajeProceso = "Generando Directorio de Importacion.";

            string destPath = System.IO.Path.Combine(destDirPath, row.CodigoLoteEasyDoc);

            bool exists = System.IO.Directory.Exists(@destPath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(@destPath);
            }

            // I create the capturando.txt file to prevent the transfer to start in this batch
            System.IO.File.CreateText(System.IO.Path.Combine(@destPath, "capturando.txt")).Close();

            //Transformo los Tif JPG a JPG
            MensajeProceso = "Realizando Procesamiento de Imangenes para importacion";

            TransformarTifAJpg(row);

            MensajeProceso = "Copiando Imagenes a Directorio de Importacion";
            row.Imagenes = new List<Imagen>();
            CopyBatchToFinalDestinationWithRename(row.PathLocalLote, destPath, row);

            //Crear Datos EN BD
            MensajeProceso = "Agregando Lote a Base de Datos";
            _repository.AgregarLoteImportacion(row);
            // I delete the capturando.txt file so the transfer can start in this batch
            string fileName = @System.IO.Path.Combine(@destPath, "capturando.txt");
            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }

            return destPath;
        }

        private void TransformarTifAJpg(LoteImportar row)
        {
            //Transformo los Tif JPG a JPG
            string[] archivos = System.IO.Directory.GetFiles(row.PathLocalLote, "*.tif");
            foreach (string archivo in archivos)
            {
                using (Accusoft.ImagXpressSdk.ImageX imgPegasus = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, archivo))
                {
                    if (imgPegasus.ImageXData.Compression == Accusoft.ImagXpressSdk.Compression.Jpeg)
                    {
                        MensajeProceso = String.Format("Realizando Procesamiento de Imangenes para importacion: {0}", archivo);

                        Accusoft.ImagXpressSdk.SaveOptions so = new Accusoft.ImagXpressSdk.SaveOptions();
                        so.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jpeg;
                        so.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jpeg;
                        so.Jpeg.Chrominance = Chrominance;
                        so.Jpeg.Luminance = Luminance;
                        string dest = System.IO.Path.Combine(row.PathLocalLote, (System.IO.Path.GetFileNameWithoutExtension(archivo)) + ".JPG");
                        imgPegasus.Save(dest, so);
                        System.IO.File.Delete(archivo);
                    }
                }
            }
        }

        private void CopyBatchToFinalDestination(string source, string destination, LoteImportar loteAImportar)
        {
            if (source == null)
            {
                throw new System.ArgumentNullException(source);
            }
            if (destination == null)
            {
                throw new System.ArgumentNullException(destination);
            }

            string[] files;
            if (destination[destination.Length - 1] != System.IO.Path.DirectorySeparatorChar)
                destination += System.IO.Path.DirectorySeparatorChar;
            if (!System.IO.Directory.Exists(destination))
                System.IO.Directory.CreateDirectory(destination);
            files = System.IO.Directory.GetFileSystemEntries(source);
            foreach (string element in files)
            {
                MensajeProceso = String.Format("Copiando Imagenes a Directorio de Importacion: {0}", element);

                if (!System.IO.Path.GetExtension(element).Contains("db"))
                {
                    Imagen img = AgregarImagen(element);
                    loteAImportar.Imagenes.Add(img);
                    System.IO.File.Copy(element, destination + System.IO.Path.GetFileName(element), true);
                }
            }
        }

        private void CopyBatchToFinalDestinationWithRename(string source, string destination, LoteImportar loteAImportar)
        {
            if (source == null)
            {
                throw new System.ArgumentNullException(source);
            }
            if (destination == null)
            {
                throw new System.ArgumentNullException(destination);
            }

            string[] files;
            int imagen = 1;

            if (destination[destination.Length - 1] != System.IO.Path.DirectorySeparatorChar)
                destination += System.IO.Path.DirectorySeparatorChar;
            if (!System.IO.Directory.Exists(destination))
                System.IO.Directory.CreateDirectory(destination);
            files = System.IO.Directory.GetFileSystemEntries(source);
            foreach (string element in files)
            {
                MensajeProceso = String.Format("Copiando Imagenes a Directorio de Importacion: {0}", element);
                if (!System.IO.Path.GetExtension(element).Contains("db"))
                {
                    if (System.IO.Directory.Exists(element))
                        CopyBatchToFinalDestination(element, destination + System.IO.Path.GetFileName(element), loteAImportar);
                    else
                    {
                        Imagen img = AgregarImagen(element);
                        loteAImportar.Imagenes.Add(img);
                        string destFilename = String.Format("{0:00000000}{1}", imagen, System.IO.Path.GetExtension(element));
                        //destFilename = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(element), destFilename);
                        System.IO.File.Copy(element, destination + destFilename, true);
                        imagen += 1;
                    }
                }
            }
        }

        private void ObtenerImagenesImportables(string source, LoteImportar loteAImportar)
        {
            if (source == null)
            {
                throw new System.ArgumentNullException(source);
            }

            string[] files;
            // int imagen = 1;

            files = System.IO.Directory.GetFileSystemEntries(source);
            foreach (string element in files)
            {
                MensajeProceso = String.Format("Obteniendo Imagenes a Directorio de Importacion: {0}", element);
                if (!System.IO.Path.GetExtension(element).Contains("db"))
                {
                    Imagen img = AgregarImagen(element);
                    loteAImportar.Imagenes.Add(img);
                }
            }
        }

        private static Imagen AgregarImagen(string element)
        {
            Imagen img = new Imagen();
            img.CodigoLote = string.Empty;
            img.Extension = System.IO.Path.GetExtension(element);
            img.Indice = 0;
            img.NroOrden = 0;
            return img;
        }

        /// <summary>
        /// Validars the caja.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <returns></returns>
        public bool ValidarCaja(LoteImportar lote)
        {
            if (!string.IsNullOrWhiteSpace(lote.CodigoCaja))
            {
                if (lote.Plantilla == null)
                {
                    if (!string.IsNullOrWhiteSpace(lote.CodigoPlantillaVolumen))
                    {
                        return _repository.ExisteCaja(lote.CodigoCaja, lote.CodigoPlantilla);
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    string cajaCompleto = string.Format("{0}_{1}", lote.Plantilla.Cliente.CodigoCliente, lote.CodigoCaja);

                    return _repository.ExisteCaja(cajaCompleto);
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Realizars the compresion imagenes.
        /// </summary>
        /// <param name="lotesAProcesar">The lotes A procesar.</param>
        public void RealizarCompresionImagenes(ReadOnlyCollection<LoteImportar> lotesAProcesar)
        {
            imagXpress1.Licensing.UnlockRuntime(1908224035, 373698848, 1341601282, 16216);
            MensajeProceso = "Comprimiendo Imagenes";
            int i = 0;
            ValorMaximoProceso = lotesAProcesar.Count;
            foreach (var lote in lotesAProcesar)
            {
                NombreLoteEnProceso = lote.CodigoLote;
                //Copio el Lote en un dirTempBK
                string pathSubLote = System.IO.Path.Combine(lote.PathLocalLote, String.Format("ANTESCOMPRESION{0}", DateTime.Now.ToString("yyyyMMddmmss")));

                bool exists = System.IO.Directory.Exists(@pathSubLote);
                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(pathSubLote);
                }
                copyDirectory(lote.PathLocalLote, pathSubLote);

                var files = System.IO.Directory.GetFiles(lote.PathLocalLote);
                foreach (var file in files)
                {
                    if (System.IO.Path.GetExtension(file).ToUpper() == ".JPG" || System.IO.Path.GetExtension(file).ToUpper() == ".TIF")
                    {
                        using (Accusoft.ImagXpressSdk.ImageX img = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, file))
                        {
                            MensajeProceso = String.Format("Comprimiendo Imagen: {0}", file);
                            int bPixel = img.BitsPerPixel;

                            if (bPixel == 1)
                            {
                                Accusoft.ImagXpressSdk.SaveOptions so = new Accusoft.ImagXpressSdk.SaveOptions();
                                so.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff;
                                so.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4;
                                img.Save(file, so);
                            }
                            else
                            {
                                Accusoft.ImagXpressSdk.SaveOptions so = new Accusoft.ImagXpressSdk.SaveOptions();
                                so.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jpeg;
                                so.Jpeg.Chrominance = Chrominance;
                                so.Jpeg.Luminance = Luminance;
                                so.Jpeg.SubSampling = Accusoft.ImagXpressSdk.SubSampling.SubSampling411;
                                img.Save(file, so);
                            }

                            if (img.ImageXData.Compression == Accusoft.ImagXpressSdk.Compression.Unknown)
                            {
                                Accusoft.ImagXpressSdk.SaveOptions so = new Accusoft.ImagXpressSdk.SaveOptions();
                                so.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jpeg;
                                so.Jpeg.Chrominance = Chrominance;
                                so.Jpeg.Luminance = Luminance;
                                so.Jpeg.SubSampling = Accusoft.ImagXpressSdk.SubSampling.SubSampling411;
                                img.Save(file, so);
                            }
                        }
                    }
                }
            }
            i++;
            ValorProceso = i;
            //Realizo para todas las imagens la compresion
        }

        /// <summary>
        /// Copies the directory.
        /// </summary>
        /// <param name="src">The SRC.</param>
        /// <param name="dst">The DST.</param>
        private static void copyDirectory(string src, string dst)
        {
            string[] files;
            if (dst[dst.Length - 1] != System.IO.Path.DirectorySeparatorChar)
                dst += System.IO.Path.DirectorySeparatorChar;
            if (!System.IO.Directory.Exists(dst))
                System.IO.Directory.CreateDirectory(dst);
            files = System.IO.Directory.GetFileSystemEntries(src);
            foreach (string element in files)
            {
                if (!element.Contains("ANTESCOMPRESION"))
                {
                    if (System.IO.Directory.Exists(element))
                        copyDirectory(element, dst + System.IO.Path.GetFileName(element));
                    else
                        System.IO.File.Copy(element, dst + System.IO.Path.GetFileName(element), true);
                }
            }
            #endregion
        }
    }
}
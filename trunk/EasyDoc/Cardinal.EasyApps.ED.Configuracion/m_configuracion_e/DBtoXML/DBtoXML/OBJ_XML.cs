﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBtoXML
{
    public class OBJ_XML
    {
        public List<CS_Operacion> lsOperaciones;

        public List<CS_Operacion> LsOperaciones
        {
            get { return lsOperaciones; }
            set { lsOperaciones = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBtoXML
{
    public enum Tipo_Operacion
    { 
        INSERT,
        DELETE,
        UPDATE,
        CREATE,
        ALTER
    }
}
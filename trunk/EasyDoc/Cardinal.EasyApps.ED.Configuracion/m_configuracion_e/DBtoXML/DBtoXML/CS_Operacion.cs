﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBtoXML
{
    public class CS_Operacion
    {
        private Tipo_Operacion tipo_opera;
        private string tabla;
        private List<string> set;
        private List<CS_Fields> campos;
        private List<string> values;
        private List<string> where;

        public List<string> Where
        {
            get { return where; }
            set { where = value; }
        }

        public List<string> Values
        {
            get { return values; }
            set { values = value; }
        }

        public List<CS_Fields> Campos
        {
            get { return campos; }
            set { campos = value; }
        }

        public List<string> Set
        {
            get { return set; }
            set { set = value; }
        }

        public string Tabla
        {
            get { return tabla; }
            set { tabla = value; }
        }

        public Tipo_Operacion Tipo_opera
        {
            get { return tipo_opera; }
            set { tipo_opera = value; }
        }
    }
}

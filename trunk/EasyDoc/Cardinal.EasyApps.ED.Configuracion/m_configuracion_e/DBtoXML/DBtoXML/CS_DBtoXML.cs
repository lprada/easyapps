﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Security.Cryptography;

namespace DBtoXML
{
    public class CS_DBtoXML
    {
        public bool SaveFile(string _filename, string texto)
        {
            bool ok = false;

            try
            {
                TextWriter tw = new StreamWriter(_filename);
                tw.WriteLine(texto);
                tw.Close();

                ok = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ok = false;
            }

            return ok;
        }

        public string CalcularMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString());
            }
            return sb.ToString();
        }

        public bool ValidarXML(string XSDFILEPATH, string XMLFILEPATH)
        {
            bool isValid = true;

            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add(null, XSDFILEPATH);
                settings.ValidationType = ValidationType.Schema;
                XmlDocument document = new XmlDocument();
                document.Load(XMLFILEPATH);
                XmlReader rdr = XmlReader.Create(new StringReader(document.InnerXml), settings);
                while (rdr.Read()) { }
            }
            catch
            {
                isValid = false;
            }

            return isValid;
        }

        public string ImportXML(string _filename)
        {
            StringBuilder return_command = new StringBuilder();

            XDocument doc = XDocument.Load(_filename);

            string encFile = System.IO.File.ReadAllText(_filename).Substring("<?xml version='1.0'?><procedimiento>".Length, System.IO.File.ReadAllText(_filename).Substring("<?xml version='1.0'?><procedimiento>".Length).Length - System.IO.File.ReadAllText(_filename).Substring(System.IO.File.ReadAllText(_filename).IndexOf("<checksum>")).Length);
            string chksum = string.Empty;

            string tipo_operacion = string.Empty;
            string tabla = string.Empty;

            List<string> lsValues = new List<string>();
            List<string> lsSet = new List<string>();
            List<string> lsWhere = new List<string>();
            List<CS_Fields> lsCampos = new List<CS_Fields>();

            var operaciones = from xml_opera in doc.Elements("procedimiento").Elements("operaciones").Descendants("operacion")
                              select xml_opera.Elements();

            var check = from xml_opera in doc.Elements("procedimiento").Elements("checksum")
                        select xml_opera.Value;
            encFile = CalcularMD5Hash(encFile);
            chksum = check.ElementAt(0);

            if (encFile == chksum)
            {
                foreach (var operacion in operaciones)
                {
                    lsValues.Clear();
                    lsSet.Clear();
                    lsWhere.Clear();
                    lsCampos.Clear();
                    tipo_operacion = string.Empty;
                    tabla = string.Empty;

                    var v_tipo_op = operacion.Where(item => item.Name == "tipo_operacion").Select(item => item.Value);
                    tipo_operacion = v_tipo_op.ElementAt(0);

                    var v_tabla = operacion.Where(item => item.Name == "tabla").Select(item => item.Value);
                    tabla = v_tabla.ElementAt(0);

                    var v_values = operacion.Where(item => item.Name == "values").Select(item => item.Value);
                    foreach (var values in v_values)
                    {
                        lsValues.Add(values);
                    }

                    var v_set = operacion.Where(item => item.Name == "set").Select(item => item.Value);
                    foreach (var set in v_set)
                    {
                        lsSet.Add(set);
                    }

                    var v_where = operacion.Where(item => item.Name == "where").Select(item => item.Value);
                    foreach (var where in v_where)
                    {
                        lsWhere.Add(where);
                    }

                    var v_field = operacion.Where(item => item.Name == "field");
                    foreach (var field in v_field)
                    {
                        CS_Fields cs = new CS_Fields();
                        cs.Nombre = field.Element("nombre").Value;
                        cs.Datatype = field.Element("datatype").Value;
                        cs.Longitud = Convert.ToInt32(field.Element("longitud").Value);
                        lsCampos.Add(cs);
                    }

                    switch (tipo_operacion)
                    {
                        case "INSERT":
                            return_command.Append("INSERT INTO " + tabla + " VALUES (");
                            for (int i = 0; i < lsValues.Count - 1; i++)
                            {
                                return_command.Append(lsValues[i] + ", ");
                            }
                            return_command.Append(lsValues.Last() + ");\n");
                            break;
                        case "UPDATE":
                            return_command.Append("UPDATE " + tabla + " SET ");
                            for (int i = 0; i < lsSet.Count - 1; i++)
                            {
                                return_command.Append(lsSet[i] + ", ");
                            }
                            return_command.Append(lsSet.Last());
                            return_command.Append(" WHERE ");
                            for (int j = 0; j < lsWhere.Count - 1; j++)
                            {
                                return_command.Append(lsWhere[j] + " AND ");
                            }
                            return_command.Append(lsWhere.Last());
                            return_command.Append(";\n");
                            break;
                        case "DELETE":
                            return_command.Append("DELETE FROM ");
                            return_command.Append(tabla + " ");
                            return_command.Append("WHERE ");
                            for (int k = 0; k < lsWhere.Count - 1; k++)
                            {
                                return_command.Append(lsWhere[k] + " AND ");
                            }
                            return_command.Append(lsWhere.Last());
                            return_command.Append(";\n");
                            break;
                        case "CREATE":
                            return_command.Append("CREATE ");
                            return_command.Append(tabla + " (");
                            for (int k = 0; k < lsCampos.Count; k++)
                            {
                                return_command.Append(lsCampos[k].Nombre + " " + lsCampos[k].Datatype + "(" + lsCampos[k].Longitud.ToString() + "), ");
                            }
                            return_command.Append(lsCampos.Last().Nombre + " " + lsCampos.Last().Datatype + "(" + lsCampos.Last().Longitud.ToString() + ")");
                            return_command.Append(");\n");
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                return_command.Append("Error de checksum: archivo incorrecto o corrupto");
            }

            return return_command.ToString();
        }

        public string ExportXML(OBJ_XML _operaciones)
        {
            StringBuilder sb_XML = new StringBuilder();
            string toEncrypt = string.Empty;

            sb_XML.Append("<?xml version='1.0'?>");
            sb_XML.Append("<procedimiento>");
            sb_XML.Append("<operaciones>");

            for (int i = 0; i < _operaciones.LsOperaciones.Count; i++)
            {
                switch (_operaciones.LsOperaciones[i].Tipo_opera)
                {
                    case Tipo_Operacion.INSERT:
                        sb_XML.Append("<operacion id='" + i + "'>");
                        sb_XML.Append("<tipo_operacion>INSERT</tipo_operacion>");
                        sb_XML.Append("<tabla>" + _operaciones.LsOperaciones[i].Tabla + "</tabla>");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Values.Count; k++)
                        {
                            sb_XML.Append("<values>" + _operaciones.LsOperaciones[i].Values[k] + "</values>");
                        }
                        sb_XML.Append("</operacion>");
                        break;
                    case Tipo_Operacion.DELETE:
                        sb_XML.Append("<operacion id='" + i + "'>");
                        sb_XML.Append("<tipo_operacion>DELETE</tipo_operacion>");
                        sb_XML.Append("<tabla>" + _operaciones.LsOperaciones[i].Tabla + "</tabla>");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Where.Count; k++)
                        {
                            sb_XML.Append("<where>" + _operaciones.LsOperaciones[i].Where[k] + "</where>");
                        }
                        sb_XML.Append("</operacion>");
                        break;
                    case Tipo_Operacion.UPDATE:
                        sb_XML.Append("<operacion id='" + i + "'>");
                        sb_XML.Append("<tipo_operacion>UPDATE</tipo_operacion>");
                        sb_XML.Append("<tabla>" + _operaciones.LsOperaciones[i].Tabla + "</tabla>");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Set.Count; k++)
                        {
                            sb_XML.Append("<set>" + _operaciones.LsOperaciones[i].Set[k] + "</set>");
                        }
                        for (int j = 0; j < _operaciones.LsOperaciones[i].Where.Count; j++)
                        {
                            sb_XML.Append("<where>" + _operaciones.LsOperaciones[i].Where[j] + "</where>");
                        }
                        sb_XML.Append("</operacion>");
                        break;
                    case Tipo_Operacion.CREATE:
                        sb_XML.Append("<operacion id='" + i.ToString() + "'>");
                        sb_XML.Append("<tipo_operacion>CREATE</tipo_operacion>");
                        sb_XML.Append("<tabla>" + _operaciones.LsOperaciones[i].Tabla + "</tabla>");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Campos.Count; k++)
                        {
                            sb_XML.Append("<field id='" + k.ToString() + "'>");
                            sb_XML.Append("<nombre>" + _operaciones.LsOperaciones[i].Campos[k].Nombre + "</nombre>");
                            sb_XML.Append("<datatype>" + _operaciones.LsOperaciones[i].Campos[k].Datatype + "</datatype>");
                            sb_XML.Append("<longitud>" + _operaciones.LsOperaciones[i].Campos[k].Longitud.ToString() + "</longitud>");
                            sb_XML.Append("</field>");
                        }
                        sb_XML.Append("</operacion>");
                        break;
                    default:
                        break;
                }
            }
            sb_XML.Append("</operaciones>");

            toEncrypt = sb_XML.ToString().Substring("<?xml version='1.0'?><procedimiento>".Length);
            sb_XML.Append("<checksum>");
            sb_XML.Append(CalcularMD5Hash(toEncrypt));
            sb_XML.Append("</checksum>");
            sb_XML.Append("</procedimiento>");

            return sb_XML.ToString();
        }

        public string ExportSQL(OBJ_XML _operaciones)
        {
            StringBuilder sb_sql = new StringBuilder();

            for (int i = 0; i < _operaciones.LsOperaciones.Count; i++)
            {
                switch (_operaciones.LsOperaciones[i].Tipo_opera)
                {
                    case Tipo_Operacion.INSERT:
                        sb_sql.Append("INSERT INTO ");
                        sb_sql.Append(_operaciones.LsOperaciones[i].Tabla + " ");
                        sb_sql.Append("VALUES (");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Values.Count -1; k++)
                        {
                            sb_sql.Append(_operaciones.LsOperaciones[i].Values[k] + ", ");
                        }
                        sb_sql.Append(_operaciones.LsOperaciones[i].Values.Last());
                        sb_sql.Append(");\n");
                        break;
                    case Tipo_Operacion.DELETE:
                        sb_sql.Append("DELETE FROM ");
                        sb_sql.Append(_operaciones.LsOperaciones[i].Tabla + " ");
                        sb_sql.Append("WHERE ");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Where.Count -1; k++)
                        {
                            sb_sql.Append(_operaciones.LsOperaciones[i].Where[k] + " AND ");
                        }
                        sb_sql.Append(_operaciones.LsOperaciones[i].Where.Last());
                        sb_sql.Append(";\n");
                        break;
                    case Tipo_Operacion.UPDATE:
                        sb_sql.Append("UPDATE ");
                        sb_sql.Append(_operaciones.LsOperaciones[i].Tabla + " ");
                        sb_sql.Append("SET ");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Set.Count -1; k++)
                        {
                            sb_sql.Append(_operaciones.LsOperaciones[i].Set[k] + ", ");
                        }
                        sb_sql.Append(_operaciones.LsOperaciones[i].Set.Last());
                        sb_sql.Append(" WHERE ");
                        for (int j = 0; j < _operaciones.LsOperaciones[i].Where.Count -1; j++)
                        {
                            sb_sql.Append(_operaciones.LsOperaciones[i].Where[j] + " AND ");
                        }
                        sb_sql.Append(_operaciones.LsOperaciones[i].Where.Last());
                        sb_sql.Append(";\n");
                        break;
                    case Tipo_Operacion.CREATE:
                        sb_sql.Append("CREATE ");
                        sb_sql.Append(_operaciones.LsOperaciones[i].Tabla + " (");
                        for (int k = 0; k < _operaciones.LsOperaciones[i].Campos.Count -1; k++)
                        {
                            sb_sql.Append(_operaciones.LsOperaciones[i].Campos[k].Nombre + " " + _operaciones.LsOperaciones[i].Campos[k].Datatype + "(" + _operaciones.LsOperaciones[i].Campos[k].Longitud.ToString() + "), ");
                        }
                        sb_sql.Append(_operaciones.LsOperaciones[i].Campos.Last().Nombre + " " + _operaciones.LsOperaciones[i].Campos.Last().Datatype + "(" + _operaciones.LsOperaciones[i].Campos.Last().Longitud.ToString() + ")");
                        sb_sql.Append(");\n");
                        break;
                    default:
                        break;
                }
            }
            
            return sb_sql.ToString();
        }
    }
}
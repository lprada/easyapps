﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBtoXML
{
    public class CS_Fields
    {
        private string nombre;
        private string datatype;
        private int longitud;

        public int Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }

        public string Datatype
        {
            get { return datatype; }
            set { datatype = value; }
        }
        
        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }       
    }
}

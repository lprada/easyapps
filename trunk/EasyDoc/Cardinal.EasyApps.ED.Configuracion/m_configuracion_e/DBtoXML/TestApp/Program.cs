﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBtoXML;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //TEST 1: INSERT
            CS_Operacion opera = new CS_Operacion();
            opera.Tipo_opera = Tipo_Operacion.INSERT;
            opera.Tabla = "TABLA_INSERT";
            List<string> values = new List<string>();
            values.Add("VALOR1");
            values.Add("VALOR2");
            values.Add("VALOR3");
            opera.Values = values;

            //TEST 2: DELETE
            CS_Operacion opera_2 = new CS_Operacion();
            opera_2.Tipo_opera = Tipo_Operacion.DELETE;
            opera_2.Tabla = "TABLA_DELETE";
            List<string> where_delete = new List<string>();
            where_delete.Add("VALOR1 = 5");
            where_delete.Add("VALOR2 = 2");
            where_delete.Add("VALOR3 = 1");
            opera_2.Where = where_delete;

            //TEST 3: UPDATE
            CS_Operacion opera_3 = new CS_Operacion();
            opera_3.Tipo_opera = Tipo_Operacion.UPDATE;
            opera_3.Tabla = "TABLA_UPDATE";
            List<string> set_update = new List<string>();
            set_update.Add("VALOR7 = 50");
            set_update.Add("VALOR8 = 20");
            set_update.Add("VALOR9 = 10");
            List<string> where_update = new List<string>();
            where_update.Add("VALOR1 = 5");
            where_update.Add("VALOR2 = 2");
            where_update.Add("VALOR3 = 1");
            opera_3.Where = where_update;
            opera_3.Set = set_update;

            //TEST 4: CREATE
            CS_Operacion opera_4 = new CS_Operacion();
            opera_4.Tipo_opera = Tipo_Operacion.CREATE;
            opera_4.Tabla = "TABLA_CREATE";
            List<CS_Fields> lsCampos = new List<CS_Fields>();
            CS_Fields c1 = new CS_Fields();
            CS_Fields c2 = new CS_Fields();
            CS_Fields c3 = new CS_Fields();
            c1.Nombre = "CAMPO1";
            c1.Datatype = SQLServer.VARCHAR.ToString();
            c1.Longitud = 255;
            c2.Nombre = "CAMPO2";
            c2.Datatype = SQLServer.VARCHAR.ToString();
            c2.Longitud = 1024;
            c3.Nombre = "CAMPO3";
            c3.Datatype = SQLServer.VARCHAR.ToString();
            c3.Longitud = 4000;
            lsCampos.Add(c1);
            lsCampos.Add(c2);
            lsCampos.Add(c3);
            opera_4.Campos = lsCampos;


            OBJ_XML obj = new OBJ_XML();
            obj.LsOperaciones = new List<CS_Operacion>();
            obj.LsOperaciones.Add(opera);
            obj.LsOperaciones.Add(opera_2);
            obj.LsOperaciones.Add(opera_3);
            obj.LsOperaciones.Add(opera_4);


            CS_DBtoXML test = new CS_DBtoXML();
            string salida = string.Empty;
            salida = test.ExportXML(obj);
            bool graba = test.SaveFile(@"d:\prb.xml", salida);

            Console.WriteLine(salida);
            Console.ReadLine();
            salida = test.ExportSQL(obj);
            Console.WriteLine(salida);
            Console.ReadLine();

            //TEST 5: IMPORT
            string retorno = test.ImportXML(@"d:\prb.xml");
            Console.WriteLine(retorno);
            Console.ReadLine();

            //TEST 6: VALIDAR XSD
            bool ok = test.ValidarXML(@"d:\prb.xsd", @"d:\prb.xml");
            if(ok)
                Console.WriteLine("XML valido");
            else
                Console.WriteLine("XML invalido");
            Console.ReadLine();
        }
    }
}

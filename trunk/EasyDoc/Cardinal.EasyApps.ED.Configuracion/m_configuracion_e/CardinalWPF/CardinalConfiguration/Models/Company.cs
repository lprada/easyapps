﻿using System;
using SimpleMvvmToolkit;

namespace CardinalConfiguration.Models
{
    public class Company : ModelBase<Company>
    {
        private int idCompany;
        public int IdCompany
        {
            get { return idCompany; }
            set
            {
                idCompany = value;
                NotifyPropertyChanged(m => m.IdCompany);
            }
        }

        private string businessName;
        public string BusinessName
        {
            get { return businessName; }
            set
            {
                businessName = value;
                NotifyPropertyChanged(m => m.BusinessName);
            }
        }

        private string state;
        public string State
        {
            get { return state; }
            set
            {
                state = value;
                NotifyPropertyChanged(m => m.State);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleMvvmToolkit;

namespace CardinalConfiguration
{
    public class Fields : ModelBase<Fields>
    {
        private string campo;
        public string Campo
        {
            get { return campo; }
            set
            {
                campo = value;
                NotifyPropertyChanged(m => m.Campo);
            }
        }

        private string descripcion;
        public string Descripcion
        {
            get { return descripcion; }
            set
            {
                descripcion = value;
                NotifyPropertyChanged(m => m.Descripcion);
            }
        }

        private string tipoDato;
        public string TipoDato
        {
            get { return tipoDato; }
            set
            {
                tipoDato = value;
                NotifyPropertyChanged(m => m.TipoDato);
            }
        }

        private int longitud;
        public int Longitud
        {
            get { return longitud; }
            set
            {
                longitud = value;
                NotifyPropertyChanged(m => m.Longitud);
            }
        }

        private string mascara;
        public string Mascara
        {
            get { return mascara; }
            set
            {
                mascara = value;
                NotifyPropertyChanged(m => m.Mascara);
            }
        }
    }
}

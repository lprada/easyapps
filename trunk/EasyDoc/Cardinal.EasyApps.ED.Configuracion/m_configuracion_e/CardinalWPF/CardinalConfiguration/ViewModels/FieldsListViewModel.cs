﻿using System;
using System.Windows;
using System.Threading;
using System.Collections.ObjectModel;

// Toolkit namespace
using SimpleMvvmToolkit;

// Toolkit extension methods
using SimpleMvvmToolkit.ModelExtensions;

namespace CardinalConfiguration
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvmprop</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// </summary>
    public class FieldsListViewModel : ViewModelBase<FieldsListViewModel>
    {
        #region Initialization and Cleanup

        // TODO: Add a member for IXxxServiceAgent
        private IFieldsServiceAgent serviceAgent;

        // Default ctor
        public FieldsListViewModel() { }

        // TODO: ctor that accepts IXxxServiceAgent
        public FieldsListViewModel(IFieldsServiceAgent serviceAgent)
        {
            this.serviceAgent = serviceAgent;
        }

        #endregion

        #region Notifications

        // TODO: Add events to notify the view or obtain data from the view
        public event EventHandler<NotificationEventArgs<Exception>> ErrorNotice;

        #endregion

        #region Properties

        // TODO: Add properties using the mvvmprop code snippet
        private ObservableCollection<Fields> listadeCampos;
        public ObservableCollection<Fields> ListadeCampos
        {
            get { return listadeCampos; }
            set
            {
                listadeCampos = value;
                NotifyPropertyChanged(vm => vm.ListadeCampos);
            }
        }
        #endregion

        #region Methods

        // TODO: Add methods that will be called by the view


        public void LoadFields()
        {
            var listaDeCampos = serviceAgent.LoadFields();
            ListadeCampos = new ObservableCollection<Fields>(listaDeCampos);
        }

        public ObservableCollection<Fields> CargarDatos()
        {
            var listaDeCampos = serviceAgent.LoadFields();
            return new ObservableCollection<Fields>(listadeCampos);
        }
        #endregion

        #region Completion Callbacks

        // TODO: Optionally add callback methods for async calls to the service agent
        
        #endregion

        #region Helpers

        // Helper method to notify View of an error
        private void NotifyError(string message, Exception error)
        {
            // Notify view of an error
            Notify(ErrorNotice, new NotificationEventArgs<Exception>(message, error));
        }

        #endregion
    }
}
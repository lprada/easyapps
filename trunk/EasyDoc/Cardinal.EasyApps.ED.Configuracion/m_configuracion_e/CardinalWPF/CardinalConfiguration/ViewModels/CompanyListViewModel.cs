﻿using System;
using System.Windows;
using System.Threading;
using System.Collections.ObjectModel;

// Toolkit namespace
using SimpleMvvmToolkit;

// Toolkit extension methods
using SimpleMvvmToolkit.ModelExtensions;


namespace CardinalConfiguration
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// Use the <strong>mvvmprop</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// </summary>
    public class CompanyListViewModel : ViewModelBase<CompanyListViewModel>
    {
        #region Initialization and Cleanup

        // TODO: Add a member for IXxxServiceAgent
        private ICompanyServiceAgent serviceAgent;

        // Default ctor
        public CompanyListViewModel() { }

        // TODO: ctor that accepts IXxxServiceAgent
        public CompanyListViewModel(ICompanyServiceAgent serviceAgent)
        {
            this.serviceAgent = serviceAgent;
        }

        #endregion

        #region Notifications

        // TODO: Add events to notify the view or obtain data from the view
        public event EventHandler<NotificationEventArgs<Exception>> ErrorNotice;

        #endregion

        #region Properties

        // TODO: Add properties using the mvvmprop code snippet

        private ObservableCollection<Models.Company> companies;
        public ObservableCollection<Models.Company> Companies
        {
            get { return companies; }
            set
            {
                companies = value;
                NotifyPropertyChanged(vm => vm.Companies);
            }
        }


        #endregion

        #region Methods

        // TODO: Add methods that will be called by the view
        public void LoadCompanies()
        {
            var companies = serviceAgent.LoadCompanies();
            Companies = new ObservableCollection<Models.Company>(companies);
        }
        #endregion

        #region Completion Callbacks

        // TODO: Optionally add callback methods for async calls to the service agent
        
        #endregion

        #region Helpers

        // Helper method to notify View of an error
        private void NotifyError(string message, Exception error)
        {
            // Notify view of an error
            Notify(ErrorNotice, new NotificationEventArgs<Exception>(message, error));
        }

        #endregion
    }
}
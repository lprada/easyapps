﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Telerik.Windows.Controls;
using DBtoXML;

namespace CardinalConfiguration
{
    /// <summary>
    /// Interaction logic for MainMenuWindow.xaml
    /// </summary>
    public partial class MainMenuWindow
    {
        public MainMenuWindow()
        {
            InitializeComponent();
            StyleManager.SetTheme(radRibbonBar1, new Windows7Theme());
        }


        private void radRibbonBar1_SelectedTabChanged(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

        }

        private void RadRibbonButton_Click(object sender, RoutedEventArgs e)
        {

            switch (((RadRibbonButton)sender).Name)
            {
                case "optPlantillas":
                    #region WindowAddPlantillas
                    AltaPlantillas windowAddPlantillas = new AltaPlantillas();
                    StyleManager.SetTheme(windowAddPlantillas, new VistaTheme());
                    windowAddPlantillas.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    //windowAddPlantillas.Width = 200;
                    //windowAddPlantillas.Height = 200;
                    //windowAddPlantillas.Owner = this;
                    //windowAddPlantillas.IsRestricted = true;
                    //windowAddPlantillas.RestrictedAreaMargin = new Thickness
                    //{
                    //    Left = 455,
                    //    Top = 620,
                    //    Right = 455,

                    //};
                    windowAddPlantillas.Show();
                    #endregion
                    break;
                case "optPlantillasVer":
                    //#region windowPlantillasView
                    //VerPlantillas windowViewPlantillas = new VerPlantillas();
                    //StyleManager.SetTheme(windowViewPlantillas, new VistaTheme());
                    //windowViewPlantillas.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    //windowViewPlantillas.Width = 200;
                    //windowViewPlantillas.Height = 200;
                    //windowViewPlantillas.Owner = this;
                    //windowViewPlantillas.IsRestricted = true;
                    //windowViewPlantillas.RestrictedAreaMargin = new Thickness
                    //{
                    //    Left = 455,
                    //    Top = 620,
                    //    Right = 455,

                    //};
                    //windowViewPlantillas.Show();
                    //#endregion
                    break;
                case "optDiccionarioAlta":
                    #region DiccionarioWindowAlta
                    AgregarCampos windowAltaDiccionario = new AgregarCampos();
                    StyleManager.SetTheme(windowAltaDiccionario, new VistaTheme());
                    windowAltaDiccionario.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    //windowAltaDiccionario.Width = 200;
                    //windowAltaDiccionario.Height = 200;
                    //windowAltaDiccionario.Owner = this;
                    //windowAltaDiccionario.IsRestricted = true;
                    //windowAltaDiccionario.RestrictedAreaMargin = new Thickness
                    //{
                    //    Left = 455,
                    //    Top = 620,
                    //    Right = 455,

                    //};
                    windowAltaDiccionario.Show();
                    #endregion
                    break;
                case "optDiccionarioVer":
                    //#region optDiccionarioVer
                    //VerDiccionarios windowViewDiccionario = new VerDiccionarios();
                    //StyleManager.SetTheme(windowViewDiccionario, new VistaTheme());
                    //windowViewDiccionario.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    //windowViewDiccionario.Width = 200;
                    //windowViewDiccionario.Height = 200;
                    //windowViewDiccionario.Owner = this;
                    //windowViewDiccionario.IsRestricted = true;
                    //windowViewDiccionario.RestrictedAreaMargin = new Thickness
                    //{
                    //    Left = 455,
                    //    Top = 620,
                    //    Right = 455,

                    //};
                    //windowViewDiccionario.Show();
                    //#endregion
                    break;
                case "optVentana":

                    #region optVentana


                    AltaEmpresas window = new AltaEmpresas();

                    StyleManager.SetTheme(window, new VistaTheme());
                    window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    //window.Width = 200;
                    //window.Height = 200;
                    //window.Owner = this;
                    //window.IsRestricted = true;
                    //window.RestrictedAreaMargin = new Thickness
                    //{
                    //    Left = 455,
                    //    Top = 620,
                    //    Right = 455,

                    //};
                    window.Show();
                    #endregion
                    break;
                case "optVentana2":
                    #region optVentana2



                    ViewCompanies window2 = new ViewCompanies();

                    StyleManager.SetTheme(window2, new VistaTheme());
                    window2.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    //window2.Width = 200;
                    //window2.Height = 200;
                    //window2.Owner = this;
                    //window2.IsRestricted = true;
                    //window2.RestrictedAreaMargin = new Thickness
                    //{
                    //    Left = 455,
                    //    Top = 620,
                    //    Right = 455,

                    //};
                    window2.Show();
                    #endregion
                    break;
                default:
                    break;
            }

            if (((Telerik.Windows.Controls.RadRibbonButton)sender).Text == "idGenerate")
            {

                image1.Visibility = Visibility.Hidden;
                //listBox1.Visibility = Visibility.Visible;
                //TEST 1: INSERT
                CS_Operacion opera = new CS_Operacion();
                opera.Tipo_opera = Tipo_Operacion.INSERT;
                opera.Tabla = "TABLA_INSERT";
                List<string> values = new List<string>();
                values.Add("VALOR1");
                values.Add("VALOR2");
                values.Add("VALOR3");
                opera.Values = values;

                //TEST 2: DELETE
                CS_Operacion opera_2 = new CS_Operacion();
                opera_2.Tipo_opera = Tipo_Operacion.DELETE;
                opera_2.Tabla = "TABLA_DELETE";
                List<string> where_delete = new List<string>();
                where_delete.Add("VALOR1 = 5");
                where_delete.Add("VALOR2 = 2");
                where_delete.Add("VALOR3 = 1");
                opera_2.Where = where_delete;

                //TEST 3: UPDATE
                CS_Operacion opera_3 = new CS_Operacion();
                opera_3.Tipo_opera = Tipo_Operacion.UPDATE;
                opera_3.Tabla = "TABLA_UPDATE";
                List<string> set_update = new List<string>();
                set_update.Add("VALOR7 = 50");
                set_update.Add("VALOR8 = 20");
                set_update.Add("VALOR9 = 10");
                List<string> where_update = new List<string>();
                where_update.Add("VALOR1 = 5");
                where_update.Add("VALOR2 = 2");
                where_update.Add("VALOR3 = 1");
                opera_3.Where = where_update;
                opera_3.Set = set_update;

                //TEST 4: CREATE
                CS_Operacion opera_4 = new CS_Operacion();
                opera_4.Tipo_opera = Tipo_Operacion.CREATE;
                opera_4.Tabla = "TABLA_CREATE";
                List<CS_Fields> lsCampos = new List<CS_Fields>();
                CS_Fields c1 = new CS_Fields();
                CS_Fields c2 = new CS_Fields();
                CS_Fields c3 = new CS_Fields();
                c1.Nombre = "CAMPO1";
                c1.Datatype = SQLServer.VARCHAR.ToString();
                c1.Longitud = 255;
                c2.Nombre = "CAMPO2";
                c2.Datatype = SQLServer.VARCHAR.ToString();
                c2.Longitud = 1024;
                c3.Nombre = "CAMPO3";
                c3.Datatype = SQLServer.VARCHAR.ToString();
                c3.Longitud = 4000;
                lsCampos.Add(c1);
                lsCampos.Add(c2);
                lsCampos.Add(c3);
                opera_4.Campos = lsCampos;


                OBJ_XML obj = new OBJ_XML();
                obj.LsOperaciones = new List<CS_Operacion>();
                obj.LsOperaciones.Add(opera);
                obj.LsOperaciones.Add(opera_2);
                obj.LsOperaciones.Add(opera_3);
                obj.LsOperaciones.Add(opera_4);


                CS_DBtoXML test = new CS_DBtoXML();
                string salida = string.Empty;
                salida = test.ExportXML(obj);
                //listBox1.Items.Add(salida);
                salida = test.ExportSQL(obj);


            }

        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            CS_DBtoXML test = new CS_DBtoXML();
            string retorno = test.ImportXML(@"e:\prueba.xml");

            //listBox1.Items.Clear();
            //listBox1.Items.Add(retorno);
        }

        private void RadWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            this.CloseAllWindows();
        }

        private void CloseAllWindows()
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                App.Current.Windows[intCounter].Close();
        }
    }


}

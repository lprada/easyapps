﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CardinalConfiguration.Models;

namespace CardinalConfiguration
{
    public interface ICompanyServiceAgent
    {
        List<Company> LoadCompanies();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardinalConfiguration
{
    public class MockFieldServiceAgent : IFieldsServiceAgent
    {
        public Fields CreateField()
        {
            return new Fields
            {
                Campo = "txt_nomrbe",
                Descripcion = "Nombre de campo",
                Longitud = 20,
                Mascara = "***",
                TipoDato = "Varchar(20)"
            };
        }


        public List<Fields> LoadFields()
        {
            return new List<Fields>
            {
                new Fields{  Campo = "txt_nomrbe",
                             Descripcion = "Nombre de campo",
                             Longitud = 20,
                             Mascara = "***",
                             TipoDato = "Varchar(20)"
                            },
                            new Fields{  Campo = "txt_Fecha",
                             Descripcion = "Nombre de fecha",
                             Longitud = 20,
                             Mascara = "***",
                             TipoDato = "DateTime"
                            }
            };
        }
    }
}

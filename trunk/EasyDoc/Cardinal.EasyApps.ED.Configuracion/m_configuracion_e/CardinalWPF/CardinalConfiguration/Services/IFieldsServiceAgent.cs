﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CardinalConfiguration
{
    public interface IFieldsServiceAgent
    {
        Fields CreateField();
        List<Fields> LoadFields();
    }
}

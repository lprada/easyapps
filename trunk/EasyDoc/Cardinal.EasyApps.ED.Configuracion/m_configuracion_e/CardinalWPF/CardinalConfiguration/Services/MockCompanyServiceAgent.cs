﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CardinalConfiguration.Models;

namespace CardinalConfiguration
{
    public class MockCompanyServiceAgent : ICompanyServiceAgent
    {
        public List<Company> LoadCompanies()
        {
            return new List<Company>
            {
               new Company { IdCompany=1, BusinessName="Jumbo", State="A"},
               new Company { IdCompany=2, BusinessName="Carrefour", State="A"}
            };
        }
    }
}

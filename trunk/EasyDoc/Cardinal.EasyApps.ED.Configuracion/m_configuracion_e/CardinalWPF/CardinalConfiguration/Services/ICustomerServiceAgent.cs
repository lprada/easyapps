﻿using System;
using System.Linq;

namespace CardinalConfiguration
{
    public interface ICustomerServiceAgent
    {
        Customer CreateCustomer();
    }
}

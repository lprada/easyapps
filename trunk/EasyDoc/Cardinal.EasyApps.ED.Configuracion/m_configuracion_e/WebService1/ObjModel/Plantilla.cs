﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Objetos
{
    public class Plantilla
    {
        private string empresa;
        private string descripcion;
        private List<Campos> lista_campos;
        private bool no_nulo;
        private bool ocr;
        private bool codigo_barras;
        private bool oculto_indexacion;

        public string Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        
        public List<Campos> Lista_campos
        {
            get { return lista_campos; }
            set { lista_campos = value; }
        }
        
        public bool No_nulo
        {
            get { return no_nulo; }
            set { no_nulo = value; }
        }
        
        public bool Ocr
        {
            get { return ocr; }
            set { ocr = value; }
        }
        
        public bool Codigo_barras
        {
            get { return codigo_barras; }
            set { codigo_barras = value; }
        }
        
        public bool Oculto_indexacion
        {
            get { return oculto_indexacion; }
            set { oculto_indexacion = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Objetos
{
    public class Empresa
    {
        private string codigo;
        private string razon_social;
        private string estado;

        public string Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public string Razon_social
        {
            get { return razon_social; }
            set { razon_social = value; }
        }

        public string Estado
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Cardinal.EasyApps.ED.Common.PDF;

namespace Cardinal.EasyApps.ED.WS.Abssa
{
    /// <summary>
    /// Summary description for AbssaWebService
    /// </summary>
    [WebService(Namespace = "http://abssaweb.cardinalsystems.com.ar/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AbssaWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public DateTime GetServerDateTime()
        {
            return DateTime.Now;
        }

        [WebMethod]
        public bool ExisteImagen(string numeroSobre)
        {
            AbssaDataClassesDataContext ctx = new AbssaDataClassesDataContext();

            var imgs = from p in ctx.ZBFNRQABs
                       where p.nrosobre == numeroSobre
                       select p;

            if (imgs.Count() == 0)
            {
                return false;
            }
            foreach (var img in imgs)
            {
                bool estaEnPapelera = ctx.papeleras.FirstOrDefault(p => p.cod_lote == img.COD_LOTE && p.nro_orden == img.NRO_ORDEN) != null;

                if (!estaEnPapelera)
                {
                    return true;
                }
            }
            return false;

        }

        [WebMethod]
        public byte[] GetImagen(string numeroSobre)
        {

            AbssaDataClassesDataContext ctx = new AbssaDataClassesDataContext();

            var imgs = from p in ctx.ZBFNRQABs
                       where p.nrosobre == numeroSobre
                       select p;

            if (imgs.Count() == 0)
            {
                byte[] bufSinImagen  = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/SinImagen.pdf"));
                return bufSinImagen;
            }

            SimplePDF sPDF = new SimplePDF();
            PDFPermissions permisos = new PDFPermissions();

            sPDF.open_file("", permisos);

            foreach (var img in imgs)
            {
                bool estaEnPapelera = ctx.papeleras.FirstOrDefault(p => p.cod_lote == img.COD_LOTE && p.nro_orden == img.NRO_ORDEN) != null;

                if (!estaEnPapelera)
                {
                    //Genero el PATH
                    string ext = ctx.tipos.First(p => p.tipo1 == img.TIPO).extension;
                    string pathRed = ctx.montajes.First(p => p.volumen == img.VOLUMEN).mountAccesoRed;

                    string path = System.IO.Path.Combine(pathRed, img.COD_LOTE);
                    path = System.IO.Path.Combine(path, string.Format("{0:00000000}.{1}", img.NRO_ORDEN, ext));
                    switch (ext.ToUpper())
                    {
                        case "PDF":
                            sPDF.pdf_en_nueva_pagina(path, 0);
                            break;
                        case "TIF":
                            sPDF.tiff_en_nueva_pagina(path, 0);
                            break;
                        case "JPG":
                            sPDF.jpg_en_nueva_pagina(path, 0);
                            break;
                        default:
                            break;
                    }
                }
            } 
            
            sPDF.close_file();

            byte[] buf = sPDF.get_file_bytes();

            if (buf.Length > 0)
            {
              
                //System.IO.File.WriteAllBytes(Server.MapPath("~/App_Data/prueba.pdf"), buf);
                return buf;
            }
            else
            {
                buf = System.IO.File.ReadAllBytes(Server.MapPath("~/App_Data/SinImagen.pdf"));
                return buf;
            }

        }

    }
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.431
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cardinal.EasyApps.ED.WS.AbssaT.AbssaService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://abssaweb.cardinalsystems.com.ar/", ConfigurationName="AbssaService.AbssaWebServiceSoap")]
    public interface AbssaWebServiceSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://abssaweb.cardinalsystems.com.ar/GetServerDateTime", ReplyAction="*")]
        System.DateTime GetServerDateTime();
        
        // CODEGEN: Generating message contract since element name numeroSobre from namespace http://abssaweb.cardinalsystems.com.ar/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://abssaweb.cardinalsystems.com.ar/ExisteImagen", ReplyAction="*")]
        Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenResponse ExisteImagen(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequest request);
        
        // CODEGEN: Generating message contract since element name numeroSobre from namespace http://abssaweb.cardinalsystems.com.ar/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://abssaweb.cardinalsystems.com.ar/GetImagen", ReplyAction="*")]
        Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenResponse GetImagen(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ExisteImagenRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ExisteImagen", Namespace="http://abssaweb.cardinalsystems.com.ar/", Order=0)]
        public Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequestBody Body;
        
        public ExisteImagenRequest() {
        }
        
        public ExisteImagenRequest(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://abssaweb.cardinalsystems.com.ar/")]
    public partial class ExisteImagenRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string numeroSobre;
        
        public ExisteImagenRequestBody() {
        }
        
        public ExisteImagenRequestBody(string numeroSobre) {
            this.numeroSobre = numeroSobre;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ExisteImagenResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="ExisteImagenResponse", Namespace="http://abssaweb.cardinalsystems.com.ar/", Order=0)]
        public Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenResponseBody Body;
        
        public ExisteImagenResponse() {
        }
        
        public ExisteImagenResponse(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://abssaweb.cardinalsystems.com.ar/")]
    public partial class ExisteImagenResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=0)]
        public bool ExisteImagenResult;
        
        public ExisteImagenResponseBody() {
        }
        
        public ExisteImagenResponseBody(bool ExisteImagenResult) {
            this.ExisteImagenResult = ExisteImagenResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetImagenRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetImagen", Namespace="http://abssaweb.cardinalsystems.com.ar/", Order=0)]
        public Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequestBody Body;
        
        public GetImagenRequest() {
        }
        
        public GetImagenRequest(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://abssaweb.cardinalsystems.com.ar/")]
    public partial class GetImagenRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string numeroSobre;
        
        public GetImagenRequestBody() {
        }
        
        public GetImagenRequestBody(string numeroSobre) {
            this.numeroSobre = numeroSobre;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class GetImagenResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="GetImagenResponse", Namespace="http://abssaweb.cardinalsystems.com.ar/", Order=0)]
        public Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenResponseBody Body;
        
        public GetImagenResponse() {
        }
        
        public GetImagenResponse(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://abssaweb.cardinalsystems.com.ar/")]
    public partial class GetImagenResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public byte[] GetImagenResult;
        
        public GetImagenResponseBody() {
        }
        
        public GetImagenResponseBody(byte[] GetImagenResult) {
            this.GetImagenResult = GetImagenResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface AbssaWebServiceSoapChannel : Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AbssaWebServiceSoapClient : System.ServiceModel.ClientBase<Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap>, Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap {
        
        public AbssaWebServiceSoapClient() {
        }
        
        public AbssaWebServiceSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AbssaWebServiceSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AbssaWebServiceSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AbssaWebServiceSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.DateTime GetServerDateTime() {
            return base.Channel.GetServerDateTime();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenResponse Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap.ExisteImagen(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequest request) {
            return base.Channel.ExisteImagen(request);
        }
        
        public bool ExisteImagen(string numeroSobre) {
            Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequest inValue = new Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequest();
            inValue.Body = new Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenRequestBody();
            inValue.Body.numeroSobre = numeroSobre;
            Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.ExisteImagenResponse retVal = ((Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap)(this)).ExisteImagen(inValue);
            return retVal.Body.ExisteImagenResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenResponse Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap.GetImagen(Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequest request) {
            return base.Channel.GetImagen(request);
        }
        
        public byte[] GetImagen(string numeroSobre) {
            Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequest inValue = new Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequest();
            inValue.Body = new Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenRequestBody();
            inValue.Body.numeroSobre = numeroSobre;
            Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.GetImagenResponse retVal = ((Cardinal.EasyApps.ED.WS.AbssaT.AbssaService.AbssaWebServiceSoap)(this)).GetImagen(inValue);
            return retVal.Body.GetImagenResult;
        }
    }
}

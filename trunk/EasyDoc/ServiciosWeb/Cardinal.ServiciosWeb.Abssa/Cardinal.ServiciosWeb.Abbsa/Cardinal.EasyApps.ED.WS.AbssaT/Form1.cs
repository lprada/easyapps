﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Cardinal.EasyApps.ED.WS.AbssaT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            AbssaService.AbssaWebServiceSoapClient client = new AbssaService.AbssaWebServiceSoapClient();
            MessageBox.Show(client.ExisteImagen(radMaskedEditBox1.Text.Trim()).ToString(), "Existe Imagen", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            AbssaService.AbssaWebServiceSoapClient client = new AbssaService.AbssaWebServiceSoapClient();
            string filename = System.IO.Path.GetTempFileName();
            var bytes = client.GetImagen(radMaskedEditBox1.Text.Trim());
            System.IO.File.WriteAllBytes(@filename,bytes);
            this.webBrowser1.Url = new Uri(String.Format("file://{0}", filename));
            this.webBrowser1.Refresh();
        }
    }
}

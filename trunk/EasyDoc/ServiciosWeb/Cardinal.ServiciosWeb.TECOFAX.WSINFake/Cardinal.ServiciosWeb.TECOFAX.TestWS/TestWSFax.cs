﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Cardinal.ServiciosWeb.TECOFAX.TestWS
{
    public partial class frmTextWSFax : Form
    {
        public frmTextWSFax()
        {
            InitializeComponent();
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            txtResultado.Text = String.Empty;

            try
            {
                int paginas;
                byte[] bytesFax;
                TECOWS.TECOWSINFAKE ws = new TECOWS.TECOWSINFAKE();

                try
                {
                    FileStream fs = new FileStream(txtArchivo.Text, FileMode.Open);
                    bytesFax = new byte[fs.Length];
                    fs.Read(bytesFax, 0, Convert.ToInt32(fs.Length));
                    fs.Close();


                    paginas = int.Parse(txtPaginas.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("(Test WS Fax / Procesar) Error :" + ex.Message);
                }

                string resultado = ws.AgregarFaxPorBytes(dtpFechaHora.Value, txtAni.Text, txtTelefono.Text, txtBOCId.Text, txtOpciones.Text, txtTramite.Text, txtNroCliente.Text, paginas, bytesFax);
                txtResultado.Text = "OK." + Environment.NewLine + Environment.NewLine + " FaxId: " + resultado;

            }
            catch (Exception ex)
            {
                txtResultado.Text = ex.Source + " : " + ex.Message;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Seleccionar la imagen de fax";
            ofd.Multiselect = false;
            DialogResult dr = ofd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtArchivo.Text = ofd.FileName;
            }
        }
    
    }


}

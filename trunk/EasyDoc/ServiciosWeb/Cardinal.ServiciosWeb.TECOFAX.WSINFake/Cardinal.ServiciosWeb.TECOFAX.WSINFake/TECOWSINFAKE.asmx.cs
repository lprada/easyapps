﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Cardinal.ServiciosWeb.TECOFAX.WSINFake
{
    /// <summary>
    /// Summary description for TECOWSINFAKE
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TECOWSINFAKE : System.Web.Services.WebService
    {
      

        [WebMethod]
        public string AgregarFaxPorBytes(DateTime fechaHora, string ani, string telefono, string bocid, string opciones,
                                         string tramite, string nrocliente, int paginas, byte[] BytesFax)
        {
            string faxId = String.Empty;

            try
            {
                DateTime dtFechaHora = GetDateTimeServer();

                ValidarDatos(fechaHora, dtFechaHora, ani, telefono, bocid, opciones, tramite, nrocliente);

                //ver el problema del campo FaxId: 
                // deberia venir como parametro o se calcula en este momento?
                faxId = dtFechaHora.ToString("yyyyMMddHHmmssfff");

                string PathDestinoImagenesFax = GetPathDestinoImagenesFax(dtFechaHora);

                GrabarArchivoDeFax(faxId, PathDestinoImagenesFax, paginas, BytesFax);

                GrabarFaxLog(faxId, fechaHora, ani, telefono, bocid, opciones, tramite, nrocliente, paginas);

            }
            catch (Exception ex)
            {
                throw new Exception("(AgregarFaxPorBytes) : " + ex.Message);
            }

            //devolver el FaxId ?
            return faxId;
        }

        private void ValidarDatos(DateTime fechaHora, DateTime fechaHoraServer, string ani, string telefono, string bocid, string opciones, string tramite, string nrocliente)
        {
            if (fechaHora > fechaHoraServer.AddHours(5))
            {
                throw new Exception("Error en el valor del parametro fechahora: es mayor a la fecha y hora actual.");
            }

            ValidarLongitudYTipo(ani, "ani");

            ValidarLongitudYTipo(telefono, "telefono");

            ValidarLongitudYTipo(tramite, "tramite");

            ValidarLongitudYTipo(nrocliente, "nrocliente");

            ValidarOpcionesXBOC(opciones, bocid);
        }
  
        private void ValidarLongitudYTipo(string valor, string nombreParametro)
        {
            if (valor.Trim().Length > 20)
            {
                throw new Exception("Error en el valor del parametro " + nombreParametro + ": no puede ser de longitud mayor a 20.");
            }

            Decimal decimalAux;
            if (valor.Trim().Length > 0 && !Decimal.TryParse(valor.Trim(), out decimalAux))
            {
                throw new Exception("Error en el valor del parametro " + nombreParametro + ": debe contener solo digitos.");
            }
        }
  
        private void ValidarOpcionesXBOC(string opciones, string bocid)
        {
            DataFaxServerDataContext ctx = new DataFaxServerDataContext();

            var opcionesxboc = from p in ctx.OpcionesxBOCs
                               where p.Opcion == opciones.Trim()
                               select p;

            if (opcionesxboc.Count() == 0)
            {
                throw new Exception("Error en el valor del parametro opciones: no se encontro el registro en la tabla OpcionesxBOC con opciones = " + opciones.Trim());
            }

            if (opcionesxboc.First().BOCId != bocid.Trim())
            {
                throw new Exception("Error en el valor del parametro bocid: el registro en la tabla OpcionesxBOC con opciones = " + opciones.Trim() + " NO tiene BOCId = " + bocid);
            }
        }

        private void GrabarFaxLog(string faxId, DateTime fechaHora, string ani, string telefono, string bocid, string opciones, string tramite, string nrocliente, int paginas)
        {
            DataFaxServerDataContext ctx = new DataFaxServerDataContext();

            FaxLog faxLog = new FaxLog();

            faxLog.FaxID = faxId;
            faxLog.FechaHora = fechaHora;
            faxLog.ANI = ani;
            faxLog.Telefono = telefono;
            faxLog.Opciones = opciones;
            faxLog.BOCID = bocid;
            faxLog.Tramite = tramite;
            faxLog.Estado = "P";
            faxLog.BOCID_ORI = bocid;
            faxLog.Opciones_ORI = opciones;
            faxLog.Paginas = paginas;
            faxLog.TipoDocumento = "X";
            faxLog.Origen = "WS";
            faxLog.NroCliente = nrocliente;

            try
            {
                ctx.FaxLogs.InsertOnSubmit(faxLog);

                ctx.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al insertar el registro en la tabla FaxLog: " + ex.Message);
            }
        }

        private DateTime GetDateTimeServer()
        {
            //obtener la fecha y hora del servidor de base de datos
            DataFaxServerDataContext ctx = new DataFaxServerDataContext();

            var FechasHoras = ctx.ExecuteQuery<DateTime>("Select getdate()");

            DateTime dt = FechasHoras.FirstOrDefault();
            if (dt == null)
                dt = DateTime.Now;

            return dt;
        }

        private void GrabarArchivoDeFax(string NombreArchivo, string PathDestino, int paginas, byte[] bytesFax)
        {
            try
            {
                //grabar el array de bytes a un archivo
                //string fullPath = Path.Combine(PathArchivosTemp,NombreArchivo + ".tif");

                //System.IO.FileStream fs = new System.IO.FileStream(fullPath, FileMode.CreateNew);
            
                //fs.Write(bytesFax,0,bytesFax.Count());
            
                //fs.Close();

                Image image;

                MemoryStream ms = new MemoryStream(bytesFax);
                image = Image.FromStream(ms);

                try
                {
                    ConvertToSinglePageTiffs(image, NombreArchivo, PathDestino, paginas);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    image.Dispose();
                }

                //try
                //{
                //    File.Delete(fullPath);
                //} catch {}
            }
            catch (Exception ex)
            {
                throw new Exception("Error al grabar el fax: " + ex.Message);
            }
        }

        private string GetPathDestinoImagenesFax(DateTime fechaHoraActual)
        {
            //ver el path de grabacion (tabla UbicacionesFaxes)
            DataFaxServerDataContext ctx = new DataFaxServerDataContext();
            
            string dtActual = fechaHoraActual.ToString("yyyyMMddHHmmss");
            string PathDestinoImagenesFax = String.Empty;

            foreach (UbicacionesFax ubifax in ctx.UbicacionesFaxes)
            {
                if (String.Compare(ubifax.FaxID_Desde, dtActual) < 0 && String.Compare(dtActual,ubifax.FaxID_Hasta) < 0)
                {
                    PathDestinoImagenesFax = ubifax.FaxDir;
                    break;
                }
            }

            if (PathDestinoImagenesFax == String.Empty)
                throw new Exception("No se encontro la ubicacion donde deben ir los archivos de faxes.");

            return PathDestinoImagenesFax;
        }

        private void ConvertToSinglePageTiffs(Image image, string fileName, string destFolder, int paginas)
        {
            const string TIFF_CODEC = "image/tiff";
            const string TIFF_FILE_EXTENSION = ".tif";
            const long ENCODING_SCHEME = (long)EncoderValue.CompressionCCITT4;

            ImageCodecInfo codecInfo = GetCodecInfo(TIFF_CODEC);

            FrameDimension frameDim = new FrameDimension(image.FrameDimensionsList[0]);
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, ENCODING_SCHEME);

            if (paginas != image.GetFrameCount(frameDim))
            {
                throw new Exception("Error en el valor del parametro paginas: es distinto a la cantidad de paginas de la imagen.");
            }

            for (int i = 0; i < image.GetFrameCount(frameDim); i++)
            {
                image.SelectActiveFrame(frameDim, i);

                string fileNameWOExt = Path.GetFileNameWithoutExtension(fileName);
                string newFileName = string.Concat(fileNameWOExt, (i + 1).ToString("000"), TIFF_FILE_EXTENSION);

                //string folder = Path.Combine(Path.GetDirectoryName(fileName), destFolder);
                if (!Directory.Exists(destFolder))
                {
                    Directory.CreateDirectory(destFolder);
                }

                image.Save(Path.Combine(destFolder, newFileName), codecInfo, encoderParams);
            }

        }

        private ImageCodecInfo GetCodecInfo(string codec)
        {
            ImageCodecInfo codecInfo = null;

            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if (info.MimeType == codec)
                {
                    codecInfo = info;
                    break;
                }
            }

            return codecInfo;
        }

    }
}

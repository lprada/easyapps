﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accusoft.ThumbnailXpressSdk;

namespace Cardinal.EasyApps.ED.WindowsControls.Imagenes
{
    class AccusoftUnlocks
    {
        private AccusoftUnlocks()
        {
        }

        public static void UnlockRuntimes(ThumbnailXpress tx1)
        {
            if (tx1 != null)
            {
                tx1.Licensing.UnlockRuntime(1810251547, 249286662, 1216513881, 28695);
                tx1.Licensing.UnlockIXRuntime(1810251547, 249286662, 1216513881, 28695);
            }

            //if (tx2 != null)
            //{
            //    //tx2.Licensing.UnlockRuntime(xxxx, xxxx, xxxx, xxxx);
            //    //tx2.Licensing.UnlockIXRuntime(xxxx, xxxx, xxxx, xxxx);
            //}

            //if (ix != null)
            //{
            //    //ix.Licensing.UnlockRuntime(xxxx,xxxx,xxxx,xxxx);
            //}
        }
    }
}

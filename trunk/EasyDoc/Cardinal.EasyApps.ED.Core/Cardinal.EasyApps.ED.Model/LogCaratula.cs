﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class LogCaratula
    {
        /// <summary>
        /// Gets or sets the C d_ MOVIMIENTO.
        /// </summary>
        /// <value>
        /// The C d_ MOVIMIENTO.
        /// </value>
        public int CD_MOVIMIENTO { get; set; }

        /// <summary>
        /// Gets or sets the C d_ USUARI o_ MOVIMIENTO.
        /// </summary>
        /// <value>
        /// The C d_ USUARI o_ MOVIMIENTO.
        /// </value>
        public string CD_USUARIO_MOVIMIENTO { get; set; }

        /// <summary>
        /// Gets or sets the F c_ MOVIMIENTO.
        /// </summary>
        /// <value>
        /// The F c_ MOVIMIENTO.
        /// </value>
        public DateTime FC_MOVIMIENTO { get; set; }

        /// <summary>
        /// Gets or sets the T x_ OBSERVACION.
        /// </summary>
        /// <value>
        /// The T x_ OBSERVACION.
        /// </value>
        public string TX_OBSERVACION { get; set; }
    }
}

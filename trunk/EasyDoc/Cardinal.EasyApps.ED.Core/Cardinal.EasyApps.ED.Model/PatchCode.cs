﻿namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    ///
    /// </summary>
    public enum PatchCodeValues
    {
        /// <summary>
        /// Patch de Nivel 1
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "KIPATCH")]
        KIPATCH1 = 1,
        /// <summary>
        /// Patch de Nivel 2
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "KIPATCH")]
        KIPATCH2 = 2,
        /// <summary>
        /// Patch Invalido
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "INVALIDO")]
        INVALIDO = 0
    }
}
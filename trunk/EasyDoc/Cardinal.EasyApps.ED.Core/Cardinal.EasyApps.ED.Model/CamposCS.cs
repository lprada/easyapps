﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Campo
    /// </summary>
    public class CamposCS
    {
        /// <summary>
        /// Gets or sets the documento.
        /// </summary>
        /// <value>
        /// The documento.
        /// </value>
        public decimal documento { get; set; }
        /// <summary>
        /// Gets or sets the campo.
        /// </summary>
        /// <value>
        /// The campo.
        /// </value>
        public string campo { get; set; }
        /// <summary>
        /// Gets or sets the valor.
        /// </summary>
        /// <value>
        /// The valor.
        /// </value>
        public string valor { get; set; }
        /// <summary>
        /// Gets or sets the tx campo.
        /// </summary>
        /// <value>
        /// The tx campo.
        /// </value>
        public string txCampo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [es detalle].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [es detalle]; otherwise, <c>false</c>.
        /// </value>
        public bool EsDetalle { get; set; }

        /// <summary>
        /// Gets or sets the orden detalle.
        /// </summary>
        /// <value>
        /// The orden detalle.
        /// </value>
        public short OrdenDetalle { get; set; }


    }
}

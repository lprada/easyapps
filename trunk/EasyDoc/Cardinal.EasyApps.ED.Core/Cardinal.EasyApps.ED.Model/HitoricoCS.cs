﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class HitoricoCS
    {
        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public decimal codigo { get; set; }
        /// <summary>
        /// Gets or sets the fecha.
        /// </summary>
        /// <value>
        /// The fecha.
        /// </value>
        public DateTime fecha { get; set; }
        /// <summary>
        /// Gets or sets the empresa.
        /// </summary>
        /// <value>
        /// The empresa.
        /// </value>
        public string empresa { get; set; }
        /// <summary>
        /// Gets or sets the plantilla.
        /// </summary>
        /// <value>
        /// The plantilla.
        /// </value>
        public string plantilla { get; set; }
        /// <summary>
        /// Gets or sets the nombre_empresa.
        /// </summary>
        /// <value>
        /// The nombre_empresa.
        /// </value>
        public string nombre_empresa { get; set; }

        /// <summary>
        /// Gets or sets the usuario.
        /// </summary>
        /// <value>
        /// The usuario.
        /// </value>
        public string usuario { get; set; }
        /// <summary>
        /// Gets or sets the campos_valores.
        /// </summary>
        /// <value>
        /// The campos_valores.
        /// </value>
        public IList<CamposCS> campos_valores { get; set; }
    }
}

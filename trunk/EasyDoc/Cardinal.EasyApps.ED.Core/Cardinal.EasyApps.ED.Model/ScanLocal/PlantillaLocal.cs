﻿using System.Collections.Generic;
using System.Linq;
using System;
namespace Cardinal.EasyApps.ED.Model.ScanLocal
{
    /// <summary>
    /// Plantilla o Template
    /// </summary>
     [Serializable]
    public class PlantillaLocal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlantillaLocal"/> class.
        /// </summary>
        public PlantillaLocal()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlantillaLocal"/> class.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <param name="nombre">The nombre.</param>
        /// <param name="campos">The campos.</param>
        public PlantillaLocal(string codigoPlantilla, string nombre, IList<PlantillaCampoLocal> campos)
        {
            CodigoPlantilla = codigoPlantilla;
            Nombre = nombre;
            Campos = campos;
           
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlantillaLocal"/> class.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <param name="nombre">The nombre.</param>
        public PlantillaLocal(string codigoPlantilla, string nombre)
        {
            CodigoPlantilla = codigoPlantilla;
            Nombre = nombre;
            Campos = new List<PlantillaCampoLocal>();
        }

        /// <summary>
        /// Gets or sets the codigo plantilla.
        /// </summary>
        /// <value>
        /// The codigo plantilla.
        /// </value>
        public string CodigoPlantilla { get; set; }
      

        /// <summary>
        /// Gets or sets the nombre.
        /// </summary>
        /// <value>
        /// The nombre.
        /// </value>
        public string Nombre { get; set; }

        /// <summary>
        /// Gets or sets the campos.
        /// </summary>
        /// <value>
        /// The campos.
        /// </value>
        public IList<PlantillaCampoLocal> Campos { get; set; }
      
    }
}
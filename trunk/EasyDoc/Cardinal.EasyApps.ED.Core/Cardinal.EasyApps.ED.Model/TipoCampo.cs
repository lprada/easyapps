﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// Tipo de Campo
    /// </summary>
    [Serializable]
    public enum TipoCampo
    {
        /// <summary>
        /// Tipo Texto
        /// </summary>
        Texto = 12,
        /// <summary>
        /// Tipo Numero
        /// </summary>
        Numero = 3,
        /// <summary>
        /// Tipo Fecha
        /// </summary>
        Fecha = 9,
        /// <summary>
        /// Tipo Boleano de Verdadedo o Falso
        /// </summary>
        Boolean = 13,
        /// <summary>
        /// Tipo Fecha interna
        /// </summary>
        FechaInterna =  11,
        /// <summary>
        /// Tipo Fecha interna
        /// </summary>
        SmallInt = 2
       

    }

    /// <summary>
    /// Tipo Campo Info
    /// </summary>
    public class TipoCampoInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoCampoInfo"/> class.
        /// </summary>
        public TipoCampoInfo()
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="TipoCampoInfo"/> class.
        /// </summary>
        /// <param name="tipoCampo">The tipo campo.</param>
        public TipoCampoInfo(TipoCampo tipoCampo)
        {
            Codigo = (int)tipoCampo;
            Descripcion = tipoCampo.ToString();
          
        	
        }

        /// <summary>
        /// Gets or sets the codigo.
        /// </summary>
        /// <value>
        /// The codigo.
        /// </value>
        public int Codigo { get; set; }
        /// <summary>
        /// Gets or sets the descripcion.
        /// </summary>
        /// <value>
        /// The descripcion.
        /// </value>
        public string Descripcion { get; set; }
        
    }
}

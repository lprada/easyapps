﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.Model
{
    /// <summary>
    /// 
    /// </summary>
    public enum EstadosCajas
    {
        /// <summary>
        /// Estados Caja
        /// </summary>
        None = 0,
        /// <summary>
        /// Esperando
        /// </summary>
        Esperando = 1,
        /// <summary>
        /// En Proceso
        /// </summary>
        En_Proceso = 2,
        /// <summary>
        /// Terminada
        /// </summary>
        Terminada = 3,
        /// <summary>
        /// Devuelta
        /// </summary>
        Devuelta = 4,


        /// <summary>
        /// En Preparacion de Papel
        /// </summary>
        Preparando_Papel = 5,
        /// <summary>
        /// Finilizado la Preparacion de Papale
        /// </summary>
        Terminado_Preparacion_Papel = 6,
        /// <summary>
        /// En Escaneo
        /// </summary>
        Digitalizando = 7,
        /// <summary>
        /// En Indexacion
        /// </summary>
        Indexando = 8,
        /// <summary>
        /// En CC o Rescaning
        /// </summary>
        CC_Rescaning = 9,
        /// <summary>
        /// Lista para el Rearmado
        /// </summary>
        Lista_Para_Rearmado = 10,
        /// <summary>
        /// En Rearmado
        /// </summary>
        Rearmado = 11,



        /// <summary>
        /// Con Faltante de Datos
        /// </summary>
        Faltante_Datos = 100,
        /// <summary>
        /// Con Error en el Control Documental del Contenido
        /// </summary>
        Error_Control_Documental = 101,
        /// <summary>
        /// Retirada antes de Finalizar su Procesamiento
        /// </summary>
        Retirada = 102,
        /// <summary>
        /// Eliminada
        /// </summary>
        Eliminada = 103
    }
}

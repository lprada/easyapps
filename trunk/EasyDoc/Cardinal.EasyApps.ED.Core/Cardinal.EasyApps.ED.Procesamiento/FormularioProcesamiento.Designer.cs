﻿namespace Cardinal.EasyApps.ED.Procesamiento
{
    partial class FormularioProcesamiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (imageXView != null && imageXView.Image != null)
                {
                    imageXView.Image.Dispose();
                    imageXView.Image = null;
                }
                if (imageXView != null)
                {
                    imageXView.Dispose();
                    imageXView = null;
                }
                if (imageXViewTemp != null && imageXViewTemp.Image != null)
                {
                    imageXViewTemp.Image.Dispose();
                    imageXViewTemp.Image = null;
                }
                if (imageXViewTemp != null)
                {
                    imageXViewTemp.Dispose();
                    imageXViewTemp = null;
                }

                if (imagXpress != null)
                {
                    imagXpress.Dispose();
                    imagXpress = null;
                }
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.imageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.imageXViewTemp = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.tareaTextBox = new System.Windows.Forms.TextBox();
            this.imagXpress = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.processor = new Accusoft.ImagXpressSdk.Processor(this.components);
            this.barcodeXpress1 = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress();
            this.scanFix1 = new PegasusImaging.WinForms.ScanFix5.ScanFix(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.imageXView.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.Controls.Add(this.imageXView, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tareaTextBox, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1128, 545);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // imageXView
            // 
            this.imageXView.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;
            this.imageXView.AutoScroll = true;
            this.tableLayoutPanel1.SetColumnSpan(this.imageXView, 2);
            this.imageXView.Controls.Add(this.imageXViewTemp);
            this.imageXView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageXView.Location = new System.Drawing.Point(8, 54);
            this.imageXView.Name = "imageXView";
            this.imageXView.Size = new System.Drawing.Size(1112, 483);
            this.imageXView.TabIndex = 1;
            // 
            // imageXViewTemp
            // 
            this.imageXViewTemp.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;
            this.imageXViewTemp.AutoScroll = true;
            this.imageXViewTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageXViewTemp.Location = new System.Drawing.Point(0, 0);
            this.imageXViewTemp.Name = "imageXViewTemp";
            this.imageXViewTemp.Size = new System.Drawing.Size(1108, 479);
            this.imageXViewTemp.TabIndex = 2;
            this.imageXViewTemp.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(8, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 46);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tarea Actual";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tareaTextBox
            // 
            this.tareaTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tareaTextBox.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tareaTextBox.Location = new System.Drawing.Point(177, 8);
            this.tareaTextBox.Name = "tareaTextBox";
            this.tareaTextBox.ReadOnly = true;
            this.tareaTextBox.Size = new System.Drawing.Size(943, 40);
            this.tareaTextBox.TabIndex = 3;
            // 
            // barcodeXpress1
            // 
            this.barcodeXpress1.ErrorLevel = PegasusImaging.WinForms.BarcodeXpress5.ErrorLevelInfo.Production;
            // 
            // scanFix1
            // 
            this.scanFix1.Debug = false;
            this.scanFix1.DebugLogFile = "C:\\Users\\lprada\\AppData\\Local\\Temp\\ScanFix5.log";
            this.scanFix1.ErrorLevel = PegasusImaging.WinForms.ScanFix5.ErrorLevel.Production;
            this.scanFix1.ResolutionUnits = System.Drawing.GraphicsUnit.Inch;
            // 
            // FormularioProcesamiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1128, 545);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormularioProcesamiento";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulario de Procesamiento";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormularioProcesamiento_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.imageXView.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion Windows Form Designer generated code

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView;
        private Accusoft.ImagXpressSdk.ImageXView imageXViewTemp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tareaTextBox;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress;
        private Accusoft.ImagXpressSdk.Processor processor;
        private PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress barcodeXpress1;
        private PegasusImaging.WinForms.ScanFix5.ScanFix scanFix1;
    }
}
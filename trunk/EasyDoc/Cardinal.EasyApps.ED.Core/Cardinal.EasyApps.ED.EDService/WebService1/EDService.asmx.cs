﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Cardinal.EasyApps.ED.Model;
using System.Configuration;
using Cardinal.EasyApps.ED.Common.Dal;

namespace WebService1
{
    /// <summary>
    /// Summary description for EdService
    /// </summary>
    [WebService(Namespace = "http://cardinalsystems.com.ar/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class EdService : System.Web.Services.WebService
    {
        //****************************************************//
        //********************EMPRESAS************************//
        //****************************************************//
        [WebMethod]
        public List<Cliente> TraerEmpresas()
        {
            IRepository _repository = GetRepository();

            return _repository.GetEmpresas().ToList();
        }

        [WebMethod]
        public bool AltaEmpresa(Cliente cliente)
        {
            IRepository _repository = GetRepository();

            return _repository.AddCliente(cliente);
        }

        [WebMethod]
        public bool BajaEmpresa(Cliente cliente)
        {
            IRepository _repository = GetRepository();

            return _repository.DelCliente(cliente);
        }

        //****************************************************//
        //*******************DICCIONARIO**********************//
        //****************************************************//

        [WebMethod]
        public List<CampoDiccionario> TraerDiccionario()
        {
            IRepository _repository = GetRepository();

            var res = _repository.GetDiccionarioDatos();
            return res;
        }

        [WebMethod]
        public bool AltaDiccionario(CampoDiccionario campo)
        {
            IRepository _repository = GetRepository();

            return _repository.AddCampo(campo);
        }


        [WebMethod]
        public bool AltaVolumnTamplate(string tmplate, string volume)
        {
            IRepository _repository = GetRepository();
            
            return _repository.AddVolumeTamplate(tmplate, volume);
        }


        [WebMethod]
        public List<string> GetTamplateVolumens(string codigoPlantilla)
        {
            IRepository _repository = GetRepository();

            return _repository.GetTamplateVolumens(codigoPlantilla);
        }


        [WebMethod]
        public bool  DelDiccionario(CampoDiccionario campo)
        {
            IRepository _repository = GetRepository();

            return _repository.DelCampo(campo);
        }

        ////****************************************************//
        ////*******************PLANTILLAS***********************//
        ////****************************************************//

        [WebMethod]
        public List<PlantillaServicio> TraerPlantillas()
        {
            IRepository _repository = GetRepository();

            return _repository.GetPlantillasServicios().ToList();
        }


        [WebMethod]
        public PlantillaServicio AltaPlantilla(PlantillaServicio plantilla)
        {
            IRepository _repository = GetRepository();

            return _repository.AddPlantilla(plantilla);
        }


        [WebMethod]
        public bool BajaPlantilla(PlantillaServicio plantilla)
        {
            IRepository _repository = GetRepository();

            return _repository.DelPlantilla(plantilla);
        }

        private static IRepository GetRepository()
        {
            IRepository _repository =
            new Cardinal.EasyApps.ED.Dal.EntityFramework.Repository(ConfigurationManager.ConnectionStrings["EDEntities"].ConnectionString);
            return _repository;
        }


        [WebMethod]
        public bool BajaFisicaPlantilla(PlantillaServicio plantilla, string password)
        {

            IRepository _repository = GetRepository();

            return _repository.DelFisicoPlantilla(plantilla,password);
        }

        [WebMethod]
        public List<TipoCampoInfo> TraerTiposDatos()
        {
            List<TipoCampoInfo> lista = new List<TipoCampoInfo>();
            // Se carga la lista con los tipo de datos que maneja Cardinal

            lista.Add(new TipoCampoInfo(TipoCampo.Boolean));
            lista.Add(new TipoCampoInfo(TipoCampo.Fecha));
            lista.Add(new TipoCampoInfo(TipoCampo.FechaInterna));
            lista.Add(new TipoCampoInfo(TipoCampo.Numero));
            lista.Add(new TipoCampoInfo(TipoCampo.Texto));
            lista.Add(new TipoCampoInfo(TipoCampo.SmallInt));



            return lista;
        }
        [WebMethod]
        public string GetNuevoCodigoPlantila()
        {
            IRepository _repository = GetRepository();

            return _repository.GetNuevoCodigoPlantila();
        }

        [WebMethod]
        public bool ExisteCodigoPlantilla(string codigo)
        {
            IRepository _repository = GetRepository();

            return _repository.ExisteCodigoPlantilla(codigo);
        }


        [WebMethod]
        public bool DelCampoPlantilla(PlantillaServicio plantilla,string codigoCampo)
        {
            IRepository _repository = GetRepository();

            return _repository.DelCampoPlantilla(plantilla,codigoCampo);

        }



        [WebMethod]
        public List<CampoCaseConflict> GetCampoCaseConflicts()
        {
            IRepository _repository = GetRepository();

            return _repository.GetCampoCaseConflicts().ToList();

        }

        [WebMethod]
        public List<Mascara> GetMascaras()
        {
            IRepository _repository = GetRepository();

            return _repository.GetMascaras();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Objetos
{
    public class Campos
    {
        private string campo;
        private string descripcion;
        private string tipo_dato;
        private int longitud;
        private string mascara;

        public string Campo
        {
            get { return campo; }
            set { campo = value; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public string Tipo_dato
        {
            get { return tipo_dato; }
            set { tipo_dato = value; }
        }

        public int Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }

        public string Mascara
        {
            get { return mascara; }
            set { mascara = value; }
        }
    }
}

﻿/*
Deployment script for Cardinal.EasyApps.ED.Database
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, NUMERIC_ROUNDABORT, QUOTED_IDENTIFIER OFF;


GO
:setvar DatabaseName "Cardinal.EasyApps.ED.Database"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
USE [master]
GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL
    AND DATABASEPROPERTYEX(N'$(DatabaseName)','Status') <> N'ONLINE')
BEGIN
    RAISERROR(N'The state of the target database, %s, is not set to ONLINE. To deploy to this database, its state must be set to ONLINE.', 16, 127,N'$(DatabaseName)') WITH NOWAIT
    RETURN
END

GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL)
	BEGIN
		DECLARE @rc      int,                       -- return code
				@fn      nvarchar(4000),            -- file name to back up to
				@dir     nvarchar(4000)             -- backup directory

		EXEC @rc = [master].[dbo].[xp_instance_regread] N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'BackupDirectory', @dir output, 'no_output'

		IF (@dir IS NULL)
		BEGIN 
			EXEC @rc = [master].[dbo].[xp_instance_regread] N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'DefaultData', @dir output, 'no_output'
		END

		IF (@dir IS NULL)
		BEGIN
			EXEC @rc = [master].[dbo].[xp_instance_regread] N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\Setup', N'SQLDataRoot', @dir output, 'no_output'
			SELECT @dir = @dir + N'\Backup'
		END

		SELECT  @fn = @dir + N'\' + N'$(DatabaseName)' + N'-' + 
				CONVERT(nchar(8), GETDATE(), 112) + N'-' + 
				RIGHT(N'0' + RTRIM(CONVERT(nchar(2), DATEPART(hh, GETDATE()))), 2) + 
				RIGHT(N'0' + RTRIM(CONVERT(nchar(2), DATEPART(mi, getdate()))), 2) + 
				RIGHT(N'0' + RTRIM(CONVERT(nchar(2), DATEPART(ss, getdate()))), 2) + 
				N'.bak' 
				BACKUP DATABASE [$(DatabaseName)] TO DISK = @fn
	END

GO
IF (DB_ID(N'$(DatabaseName)') IS NOT NULL) 
BEGIN
    ALTER DATABASE [$(DatabaseName)]
    SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE [$(DatabaseName)];
END

GO
PRINT N'Creating $(DatabaseName)...'
GO
CREATE DATABASE [$(DatabaseName)]
    ON 
    PRIMARY(NAME = [EasyDoc.Database], FILENAME = 'E:\DATA\ProyectosTFS\EasyDoc\EasyDocNet2\trunk\EasyDocNet2\Database\EasyDoc.Database.mdf', SIZE = 5120 KB, FILEGROWTH = 1024 KB)
    LOG ON (NAME = [EasyDoc.Database_log], FILENAME = 'E:\DATA\ProyectosTFS\EasyDoc\EasyDocNet2\trunk\EasyDocNet2\Database\EasyDoc.Database_log.ldf', SIZE = 3456 KB, MAXSIZE = 2097152 MB, FILEGROWTH = 10 %) COLLATE SQL_Latin1_General_CP850_CI_AI
GO
EXECUTE sp_dbcmptlevel [$(DatabaseName)], 80;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS OFF,
                ANSI_PADDING OFF,
                ANSI_WARNINGS OFF,
                ARITHABORT OFF,
                CONCAT_NULL_YIELDS_NULL OFF,
                NUMERIC_ROUNDABORT OFF,
                QUOTED_IDENTIFIER OFF,
                ANSI_NULL_DEFAULT OFF,
                CURSOR_DEFAULT GLOBAL,
                RECOVERY SIMPLE,
                CURSOR_CLOSE_ON_COMMIT OFF,
                AUTO_CREATE_STATISTICS ON,
                AUTO_SHRINK OFF,
                AUTO_UPDATE_STATISTICS ON,
                RECURSIVE_TRIGGERS OFF 
            WITH ROLLBACK IMMEDIATE;
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_CLOSE OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ALLOW_SNAPSHOT_ISOLATION OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET READ_COMMITTED_SNAPSHOT OFF;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET AUTO_UPDATE_STATISTICS_ASYNC OFF,
                PAGE_VERIFY NONE,
                DATE_CORRELATION_OPTIMIZATION OFF,
                DISABLE_BROKER,
                PARAMETERIZATION SIMPLE,
                SUPPLEMENTAL_LOGGING OFF 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET TRUSTWORTHY OFF,
        DB_CHAINING OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'The database settings cannot be modified. You must be a SysAdmin to apply these settings.';
    END


GO
IF IS_SRVROLEMEMBER(N'sysadmin') = 1
    BEGIN
        IF EXISTS (SELECT 1
                   FROM   [master].[dbo].[sysdatabases]
                   WHERE  [name] = N'$(DatabaseName)')
            BEGIN
                EXECUTE sp_executesql N'ALTER DATABASE [$(DatabaseName)]
    SET HONOR_BROKER_PRIORITY OFF 
    WITH ROLLBACK IMMEDIATE';
            END
    END
ELSE
    BEGIN
        PRINT N'The database settings cannot be modified. You must be a SysAdmin to apply these settings.';
    END


GO
USE [$(DatabaseName)]
GO
IF fulltextserviceproperty(N'IsFulltextInstalled') = 1
    EXECUTE sp_fulltext_database 'enable';


GO
/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO
PRINT N'Creating [dbo].[anotacion]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[anotacion] (
    [volumen]   VARCHAR (8)    NULL,
    [codlote]   VARCHAR (50)   NOT NULL,
    [nroorden]  SMALLINT       NOT NULL,
    [tipo]      VARCHAR (1)    NOT NULL,
    [txt]       VARCHAR (1024) NULL,
    [tp]        INT            NOT NULL,
    [lft]       INT            NOT NULL,
    [hght]      INT            NOT NULL,
    [wdth]      INT            NOT NULL,
    [backcolor] VARCHAR (12)   NULL,
    [tag]       INT            NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_anotacion...';


GO
ALTER TABLE [dbo].[anotacion]
    ADD CONSTRAINT [pk_anotacion] PRIMARY KEY CLUSTERED ([codlote] ASC, [nroorden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[aud_arch]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_arch] (
    [pt]       SMALLINT      NOT NULL,
    [vol_ini]  VARCHAR (8)   NOT NULL,
    [vol_fin]  VARCHAR (8)   NOT NULL,
    [cod_lote] VARCHAR (50)  NOT NULL,
    [cod_us]   INT           NOT NULL,
    [fecha]    SMALLDATETIME NULL,
    [inicio]   SMALLDATETIME NOT NULL,
    [tiempo]   SMALLDATETIME NOT NULL,
    [total]    SMALLINT      NOT NULL,
    [totalOK]  SMALLINT      NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[aud_capt]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_capt] (
    [cod_lote] NVARCHAR (50) NOT NULL,
    [cod_us]   NVARCHAR (50) NOT NULL,
    [fecha]    SMALLDATETIME NOT NULL,
    [inicio]   SMALLDATETIME NOT NULL,
    [tiempo]   SMALLDATETIME NOT NULL,
    [total_im] SMALLINT      NOT NULL,
    [tipocap]  NVARCHAR (50) NOT NULL,
    [scanner]  NVARCHAR (20) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[aud_clas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_clas] (
    [cod_lote]       VARCHAR (50)  NOT NULL,
    [cod_us]         INT           NOT NULL,
    [fecha]          SMALLDATETIME NULL,
    [inicio]         SMALLDATETIME NOT NULL,
    [tiempo]         SMALLDATETIME NOT NULL,
    [totalim]        SMALLINT      NOT NULL,
    [clasif]         SMALLINT      NOT NULL,
    [deleted]        SMALLINT      NOT NULL,
    [depurar]        SMALLINT      NOT NULL,
    [caracter]       INT           NOT NULL,
    [documentos]     SMALLINT      NOT NULL,
    [TipoIndexacion] INT           NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[aud_cons]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_cons] (
    [cod_lote] VARCHAR (50)  NOT NULL,
    [cod_us]   VARCHAR (50)  NOT NULL,
    [fecha]    SMALLDATETIME NOT NULL,
    [hora]     VARCHAR (200) NOT NULL,
    [orden]    INT           NOT NULL,
    [tmplate]  VARCHAR (8)   NOT NULL,
    [identDoc] VARCHAR (255) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[aud_logis]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_logis] (
    [cod_us]     VARCHAR (50)  NOT NULL,
    [fecha]      SMALLDATETIME NOT NULL,
    [hora]       SMALLDATETIME NOT NULL,
    [tipo_obj]   VARCHAR (1)   NOT NULL,
    [cod_obj]    VARCHAR (30)  NOT NULL,
    [operacion]  INT           NOT NULL,
    [comentario] VARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[aud_preppapel]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_preppapel] (
    [user_id]   VARCHAR (50) NOT NULL,
    [fecha]     DATETIME     NOT NULL,
    [cantCajas] INT          NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_aud_preppapel...';


GO
ALTER TABLE [dbo].[aud_preppapel]
    ADD CONSTRAINT [PK_aud_preppapel] PRIMARY KEY CLUSTERED ([user_id] ASC, [fecha] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[aud_seg]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[aud_seg] (
    [cod_us]     INT           NOT NULL,
    [fecha]      SMALLDATETIME NOT NULL,
    [hora]       SMALLDATETIME NOT NULL,
    [tipo_obj]   VARCHAR (1)   NOT NULL,
    [cod_obj]    VARCHAR (30)  NOT NULL,
    [operacion]  INT           NOT NULL,
    [comentario] VARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[audit_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[audit_T] (
    [fecha_dt]       DATETIME      NOT NULL,
    [user_id_fk]     CHAR (50)     NOT NULL,
    [task_id_fk]     CHAR (50)     NOT NULL,
    [task_number_bi] BIGINT        NOT NULL,
    [observ_vc]      VARCHAR (250) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_audit_T...';


GO
ALTER TABLE [dbo].[audit_T]
    ADD CONSTRAINT [pk_audit_T] PRIMARY KEY CLUSTERED ([fecha_dt] ASC, [user_id_fk] ASC, [task_id_fk] ASC, [task_number_bi] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[AuditoriaGraboVerificacion]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[AuditoriaGraboVerificacion] (
    [cod_us]    INT           NOT NULL,
    [fecha]     DATETIME      NOT NULL,
    [tmplate]   VARCHAR (8)   NOT NULL,
    [cod_lote]  VARCHAR (50)  NOT NULL,
    [nro_orden] SMALLINT      NOT NULL,
    [campo]     VARCHAR (8)   NOT NULL,
    [valor1]    VARCHAR (200) NOT NULL,
    [valor2]    VARCHAR (200) NOT NULL,
    [eleccion]  INT           NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[busqueda_body_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[busqueda_body_T] (
    [id]                INT           NOT NULL,
    [abro_parentesis]   CHAR (5)      NOT NULL,
    [campo]             CHAR (8)      NOT NULL,
    [valor]             VARCHAR (250) NOT NULL,
    [operador]          CHAR (10)     NOT NULL,
    [cierro_parentesis] CHAR (5)      NOT NULL,
    [operador_row]      CHAR (10)     NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_busqueda_body_T...';


GO
ALTER TABLE [dbo].[busqueda_body_T]
    ADD CONSTRAINT [pk_busqueda_body_T] PRIMARY KEY CLUSTERED ([id] ASC, [abro_parentesis] ASC, [campo] ASC, [valor] ASC, [operador] ASC, [cierro_parentesis] ASC, [operador_row] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[busqueda_header_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[busqueda_header_T] (
    [id]             INT          NOT NULL,
    [nombre]         VARCHAR (50) NULL,
    [fecha_creacion] DATETIME     NOT NULL,
    [tmplate]        VARCHAR (8)  NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_busqueda_header_T...';


GO
ALTER TABLE [dbo].[busqueda_header_T]
    ADD CONSTRAINT [pk_busqueda_header_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Cajas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Cajas] (
    [cod_caja]     VARCHAR (30)  NOT NULL,
    [LoginEmpresa] VARCHAR (20)  NULL,
    [Fecha]        DATETIME      NULL,
    [Estado]       INT           NULL,
    [FRecepcion]   DATETIME      NULL,
    [FProceso]     DATETIME      NULL,
    [FTerminacion] DATETIME      NULL,
    [FEntrega]     DATETIME      NULL,
    [FRecepDatos]  DATETIME      NULL,
    [FCaptura]     DATETIME      NULL,
    [estadoLogico] INT           NULL,
    [tmplate]      VARCHAR (255) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_Cajas...';


GO
ALTER TABLE [dbo].[Cajas]
    ADD CONSTRAINT [PK_Cajas] PRIMARY KEY CLUSTERED ([cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[CajasDefinitivas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[CajasDefinitivas] (
    [cod_caja]           VARCHAR (30)  NOT NULL,
    [LoginEmpresa]       VARCHAR (20)  NULL,
    [Fecha]              DATETIME      NULL,
    [Estado]             INT           NULL,
    [FechaDatosMerge]    DATETIME      NULL,
    [CantSobrantes]      INT           NULL,
    [CantFaltantes]      INT           NULL,
    [FechaInicioProceso] DATETIME      NULL,
    [FechaDevolucion]    DATETIME      NULL,
    [descr]              VARCHAR (200) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_CajasDefinitivas...';


GO
ALTER TABLE [dbo].[CajasDefinitivas]
    ADD CONSTRAINT [PK_CajasDefinitivas] PRIMARY KEY CLUSTERED ([cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[CajasEntregas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[CajasEntregas] (
    [cod_caja]    VARCHAR (30) NULL,
    [cod_entrega] VARCHAR (30) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[datasrc]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[datasrc] (
    [codigo]     INT           NOT NULL,
    [datasrc]    VARCHAR (254) NOT NULL,
    [usuario]    VARCHAR (30)  NULL,
    [password]   VARCHAR (50)  NULL,
    [usetxret]   SMALLINT      NOT NULL,
    [pgrant]     SMALLINT      NOT NULL,
    [porderby]   SMALLINT      NOT NULL,
    [pindex]     SMALLINT      NOT NULL,
    [typedep]    VARCHAR (60)  NULL,
    [dstype]     VARCHAR (20)  NULL,
    [fmtFechas]  VARCHAR (40)  NULL,
    [dstypename] VARCHAR (30)  NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_datasrc...';


GO
ALTER TABLE [dbo].[datasrc]
    ADD CONSTRAINT [pk_datasrc] PRIMARY KEY CLUSTERED ([codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[device]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[device] (
    [coddev]  SMALLINT     NOT NULL,
    [descrip] VARCHAR (10) NOT NULL,
    [tipo]    SMALLINT     NOT NULL,
    [libre]   INT          NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_device...';


GO
ALTER TABLE [dbo].[device]
    ADD CONSTRAINT [pk_device] PRIMARY KEY CLUSTERED ([coddev] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[dicdatos]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[dicdatos] (
    [campo]      VARCHAR (8)  NOT NULL,
    [descripcio] VARCHAR (50) NOT NULL,
    [tipo]       SMALLINT     NOT NULL,
    [longitud]   SMALLINT     NULL,
    [decimales]  SMALLINT     NULL,
    [rango_min]  VARCHAR (20) NULL,
    [rango_max]  VARCHAR (20) NULL,
    [mascara]    VARCHAR (20) NULL,
    [interno]    SMALLINT     NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_dicdatos...';


GO
ALTER TABLE [dbo].[dicdatos]
    ADD CONSTRAINT [pk_dicdatos] PRIMARY KEY CLUSTERED ([campo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Documento]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Documento] (
    [DocumentoId]   UNIQUEIDENTIFIER NOT NULL,
    [Nombre]        NVARCHAR (100)   NOT NULL,
    [FechaCreacion] DATETIME         NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_Documento...';


GO
ALTER TABLE [dbo].[Documento]
    ADD CONSTRAINT [PK_Documento] PRIMARY KEY CLUSTERED ([DocumentoId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[DocumentoImagen]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[DocumentoImagen] (
    [DocumentoId] UNIQUEIDENTIFIER NOT NULL,
    [cod_lote]    VARCHAR (50)     NOT NULL,
    [nro_orden]   INT              NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_DocumentoImagen...';


GO
ALTER TABLE [dbo].[DocumentoImagen]
    ADD CONSTRAINT [PK_DocumentoImagen] PRIMARY KEY CLUSTERED ([DocumentoId] ASC, [cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[DocumentoSuscripcion]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[DocumentoSuscripcion] (
    [DocumentoSuscripcionId] UNIQUEIDENTIFIER NOT NULL,
    [FechaAlta]              DATETIME         NOT NULL,
    [Template]               VARCHAR (8)      NOT NULL,
    [UsuarioId]              CHAR (50)        NULL,
    [GrupoId]                CHAR (50)        NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_DocumentoSuscripcion...';


GO
ALTER TABLE [dbo].[DocumentoSuscripcion]
    ADD CONSTRAINT [PK_DocumentoSuscripcion] PRIMARY KEY CLUSTERED ([DocumentoSuscripcionId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[DocumentoSuscripcionTag]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[DocumentoSuscripcionTag] (
    [DocumentoSuscripcionTagId] UNIQUEIDENTIFIER NOT NULL,
    [FechaAlta]                 DATETIME         NOT NULL,
    [Template]                  VARCHAR (8)      NOT NULL,
    [UsuarioId]                 CHAR (50)        NULL,
    [GrupoId]                   CHAR (50)        NULL,
    [TipoTagId]                 INT              NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_DocumentoSuscripcionTag...';


GO
ALTER TABLE [dbo].[DocumentoSuscripcionTag]
    ADD CONSTRAINT [PK_DocumentoSuscripcionTag] PRIMARY KEY CLUSTERED ([DocumentoSuscripcionTagId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[DocumentoTag]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[DocumentoTag] (
    [DocumentoTagId] UNIQUEIDENTIFIER NOT NULL,
    [DocumentoId]    UNIQUEIDENTIFIER NOT NULL,
    [TipoTagId]      INT              NOT NULL,
    [Fecha]          DATETIME         NOT NULL,
    [UsuarioId]      CHAR (50)        NOT NULL,
    [Eliminada]      BIT              NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_DocumentoTag...';


GO
ALTER TABLE [dbo].[DocumentoTag]
    ADD CONSTRAINT [PK_DocumentoTag] PRIMARY KEY CLUSTERED ([DocumentoTagId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Empresas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Empresas] (
    [LoginEmpresa] VARCHAR (20)  NOT NULL,
    [DescEmpresa]  VARCHAR (50)  NULL,
    [NroCli]       VARCHAR (10)  NULL,
    [CUIT]         VARCHAR (20)  NULL,
    [PerInicio]    DATETIME      NULL,
    [PerDuracion]  INT           NULL,
    [DomCalle]     VARCHAR (30)  NULL,
    [DomNumero]    VARCHAR (20)  NULL,
    [DomPiso]      VARCHAR (10)  NULL,
    [DomLocalidad] VARCHAR (30)  NULL,
    [DomProvincia] VARCHAR (30)  NULL,
    [DomCP]        VARCHAR (10)  NULL,
    [ConApellido]  VARCHAR (30)  NULL,
    [ConNombre]    VARCHAR (30)  NULL,
    [ConCargo]     VARCHAR (30)  NULL,
    [ConTelefono]  VARCHAR (30)  NULL,
    [ConInterno]   VARCHAR (10)  NULL,
    [ConFax]       VARCHAR (30)  NULL,
    [ConEmail]     VARCHAR (100) NULL,
    [Estado]       VARCHAR (1)   NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_Empresas...';


GO
ALTER TABLE [dbo].[Empresas]
    ADD CONSTRAINT [pk_Empresas] PRIMARY KEY CLUSTERED ([LoginEmpresa] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[EmpresaSector]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[EmpresaSector] (
    [LoginEmpresa] VARCHAR (20)     NOT NULL,
    [SectorId]     UNIQUEIDENTIFIER NOT NULL,
    [Nombre]       VARCHAR (100)    NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_EmpresaSector...';


GO
ALTER TABLE [dbo].[EmpresaSector]
    ADD CONSTRAINT [PK_EmpresaSector] PRIMARY KEY CLUSTERED ([SectorId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Entregas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Entregas] (
    [cod_entrega]    VARCHAR (30) NOT NULL,
    [LoginEmpresa]   VARCHAR (20) NULL,
    [nro_entrega]    INT          NULL,
    [Fecha]          DATETIME     NULL,
    [Estado]         INT          NULL,
    [CantImagenes]   INT          NULL,
    [CantDocumentos] INT          NULL,
    [CantCDs]        INT          NULL,
    [FechaBajada]    DATETIME     NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_Entregas...';


GO
ALTER TABLE [dbo].[Entregas]
    ADD CONSTRAINT [pk_Entregas] PRIMARY KEY CLUSTERED ([cod_entrega] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[EntregasTmplates]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[EntregasTmplates] (
    [cod_entrega]    VARCHAR (30) NOT NULL,
    [tmplate]        VARCHAR (8)  NOT NULL,
    [CantImagenes]   INT          NULL,
    [CantDocumentos] INT          NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_EntregasTmplates...';


GO
ALTER TABLE [dbo].[EntregasTmplates]
    ADD CONSTRAINT [PK_EntregasTmplates] PRIMARY KEY CLUSTERED ([cod_entrega] ASC, [tmplate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[ErroresControl]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ErroresControl] (
    [tmplate]   VARCHAR (8)  NOT NULL,
    [cod_lote]  VARCHAR (50) NOT NULL,
    [nro_orden] SMALLINT     NOT NULL,
    [cod_caja]  VARCHAR (30) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[estadosCajas_t]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[estadosCajas_t] (
    [id]    INT           NOT NULL,
    [descr] VARCHAR (100) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_estadosCajas_t...';


GO
ALTER TABLE [dbo].[estadosCajas_t]
    ADD CONSTRAINT [pk_estadosCajas_t] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[estadosEntregas_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[estadosEntregas_T] (
    [id]    INT           NOT NULL,
    [descr] VARCHAR (100) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_estadosEntregas_T...';


GO
ALTER TABLE [dbo].[estadosEntregas_T]
    ADD CONSTRAINT [pk_estadosEntregas_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[filtros]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[filtros] (
    [filtro]     INT          NOT NULL,
    [fstring]    VARCHAR (4)  NOT NULL,
    [descripcio] VARCHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_filtros...';


GO
ALTER TABLE [dbo].[filtros]
    ADD CONSTRAINT [pk_filtros] PRIMARY KEY CLUSTERED ([filtro] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[group_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[group_T] (
    [group_id] CHAR (50)     NOT NULL,
    [descr]    VARCHAR (200) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_group_T...';


GO
ALTER TABLE [dbo].[group_T]
    ADD CONSTRAINT [pk_group_T] PRIMARY KEY CLUSTERED ([group_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[grpyus]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[grpyus] (
    [codigo]       VARCHAR (200) NOT NULL,
    [tipo]         VARCHAR (1)   NULL,
    [descripcio]   VARCHAR (50)  NOT NULL,
    [aplicacion]   VARCHAR (40)  NULL,
    [login]        VARCHAR (40)  NOT NULL,
    [clave]        VARCHAR (50)  NULL,
    [LoginEmpresa] VARCHAR (20)  NULL,
    [Apellido]     VARCHAR (30)  NULL,
    [Nombre]       VARCHAR (30)  NULL,
    [Cargo]        VARCHAR (30)  NULL,
    [Telefono]     VARCHAR (30)  NULL,
    [Interno]      VARCHAR (10)  NULL,
    [Fax]          VARCHAR (30)  NULL,
    [Email]        VARCHAR (100) NULL,
    [CodSucursal]  INT           NULL,
    [TipoConexion] INT           NULL,
    [Estado]       VARCHAR (1)   NULL,
    [CambiarClave] VARCHAR (1)   NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_grpyus...';


GO
ALTER TABLE [dbo].[grpyus]
    ADD CONSTRAINT [pk_grpyus] PRIMARY KEY CLUSTERED ([codigo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[hipervinculos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[hipervinculos_T] (
    [templateOriginal] VARCHAR (20) NOT NULL,
    [campoOriginal]    VARCHAR (20) NOT NULL,
    [templateDestino]  VARCHAR (20) NOT NULL,
    [campoDestino]     VARCHAR (20) NOT NULL,
    [orden]            INT          NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_hipervinculos_T...';


GO
ALTER TABLE [dbo].[hipervinculos_T]
    ADD CONSTRAINT [pk_hipervinculos_T] PRIMARY KEY CLUSTERED ([templateOriginal] ASC, [campoOriginal] ASC, [templateDestino] ASC, [campoDestino] ASC, [orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[hosts]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[hosts] (
    [host]     VARCHAR (20) NOT NULL,
    [usuario]  VARCHAR (50) NOT NULL,
    [password] VARCHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_hosts...';


GO
ALTER TABLE [dbo].[hosts]
    ADD CONSTRAINT [pk_hosts] PRIMARY KEY CLUSTERED ([host] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[idlotes]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[idlotes] (
    [cod_lote]           VARCHAR (50)  NOT NULL,
    [tmplate]            VARCHAR (8)   NOT NULL,
    [act_nro]            SMALLINT      NULL,
    [ult_nro]            SMALLINT      NULL,
    [indexando]          SMALLINT      NULL,
    [caja]               INT           NULL,
    [feccreacion]        DATETIME      NULL,
    [scanner]            VARCHAR (20)  NULL,
    [controlrealizado]   SMALLINT      NULL,
    [tag]                VARCHAR (255) NULL,
    [usrindexando]       VARCHAR (255) NULL,
    [ipindexando]        VARCHAR (255) NULL,
    [enIndexacion]       VARCHAR (50)  NULL,
    [fechaUltAcceso]     DATETIME      NULL,
    [fechaProcDocumento] DATETIME      NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK__idlotes__6E01572D...';


GO
ALTER TABLE [dbo].[idlotes]
    ADD CONSTRAINT [PK__idlotes__6E01572D] PRIMARY KEY CLUSTERED ([cod_lote] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[indicadores_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[indicadores_T] (
    [indicador]   VARCHAR (50)  NOT NULL,
    [etiqueta]    VARCHAR (100) NOT NULL,
    [polaridad]   CHAR (2)      NULL,
    [verde]       BIGINT        NULL,
    [amarillo]    BIGINT        NULL,
    [rojo]        BIGINT        NULL,
    [leyverde]    VARCHAR (250) NULL,
    [leyamarillo] VARCHAR (250) NULL,
    [leyrojo]     VARCHAR (250) NULL,
    [spCant]      VARCHAR (100) NULL,
    [spPorc]      VARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_indicadores_T...';


GO
ALTER TABLE [dbo].[indicadores_T]
    ADD CONSTRAINT [pk_indicadores_T] PRIMARY KEY CLUSTERED ([indicador] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[invalidos]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[invalidos] (
    [tmplate]   VARCHAR (8)  NOT NULL,
    [cod_lote]  VARCHAR (50) NOT NULL,
    [nro_orden] SMALLINT     NOT NULL,
    [FechaHora] DATETIME     NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_invalidos...';


GO
ALTER TABLE [dbo].[invalidos]
    ADD CONSTRAINT [PK_invalidos] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_auditoriaDocumentos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_auditoriaDocumentos_T] (
    [fecha]          DATETIME      NULL,
    [caja]           VARCHAR (50)  NULL,
    [doc]            VARCHAR (50)  NULL,
    [lote]           VARCHAR (50)  NULL,
    [usuario]        VARCHAR (50)  NULL,
    [estOriCaja]     INT           NULL,
    [estDestCaja]    INT           NULL,
    [estOriDoc]      INT           NULL,
    [estDestDoc]     INT           NULL,
    [oper]           VARCHAR (50)  NULL,
    [numoper]        BIGINT        NULL,
    [comen]          VARCHAR (400) NULL,
    [estOriCajaDef]  INT           NULL,
    [estDestCajaDef] INT           NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[LD_cajas_definitivas_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_cajas_definitivas_T] (
    [cod_caja]   VARCHAR (15) NOT NULL,
    [fecha_alta] DATETIME     NOT NULL,
    [estado]     TINYINT      NULL,
    [numRem]     VARCHAR (20) NULL,
    [grupo]      CHAR (3)     NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_cajas_definitivas_T...';


GO
ALTER TABLE [dbo].[LD_cajas_definitivas_T]
    ADD CONSTRAINT [pk_LD_cajas_definitivas_T] PRIMARY KEY CLUSTERED ([cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_cajas_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_cajas_T] (
    [cod_caja]   VARCHAR (15) NOT NULL,
    [fecha_alta] DATETIME     NOT NULL,
    [estado]     TINYINT      NULL,
    [numRem]     VARCHAR (20) NULL,
    [grupo]      CHAR (3)     NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_cajas_T...';


GO
ALTER TABLE [dbo].[LD_cajas_T]
    ADD CONSTRAINT [pk_LD_cajas_T] PRIMARY KEY CLUSTERED ([cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_controlDocumentos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_controlDocumentos_T] (
    [id]    VARCHAR (50)  NOT NULL,
    [descr] VARCHAR (200) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_controlDocumentos_T...';


GO
ALTER TABLE [dbo].[LD_controlDocumentos_T]
    ADD CONSTRAINT [pk_LD_controlDocumentos_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_Documentos_HistoricoCB]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_Documentos_HistoricoCB] (
    [legajo]      VARCHAR (255) NOT NULL,
    [legajoAnter] VARCHAR (255) NOT NULL,
    [nombres]     VARCHAR (255) NOT NULL,
    [apellido]    VARCHAR (255) NOT NULL,
    [apenom]      VARCHAR (500) NOT NULL,
    [separar]     CHAR (1)      NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[LD_documentos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_documentos_T] (
    [id_documento]         VARCHAR (50) NOT NULL,
    [fecha_alta]           DATETIME     NULL,
    [fecha_ultimo_ingreso] DATETIME     NULL,
    [estado]               TINYINT      NULL,
    [fecha_reconocimiento] DATETIME     NULL,
    [fecha_control_qa]     DATETIME     NULL,
    [fecha_devolucion]     DATETIME     NULL,
    [caja_proceso]         VARCHAR (15) NOT NULL,
    [caja_devolucion]      VARCHAR (15) NULL,
    [remitoExtraccion]     VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_LD_documentos_T...';


GO
ALTER TABLE [dbo].[LD_documentos_T]
    ADD CONSTRAINT [PK_LD_documentos_T] PRIMARY KEY CLUSTERED ([id_documento] ASC, [caja_proceso] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_estado_documentos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_estado_documentos_T] (
    [id]    INT          NOT NULL,
    [descr] VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_estado_documentos_T...';


GO
ALTER TABLE [dbo].[LD_estado_documentos_T]
    ADD CONSTRAINT [pk_LD_estado_documentos_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_estados_cajas_definitivas_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_estados_cajas_definitivas_T] (
    [id]    INT          NOT NULL,
    [descr] VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_estados_cajas_definitivas_T...';


GO
ALTER TABLE [dbo].[LD_estados_cajas_definitivas_T]
    ADD CONSTRAINT [pk_LD_estados_cajas_definitivas_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_estados_cajas_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_estados_cajas_T] (
    [id]    INT          NOT NULL,
    [descr] VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_estados_cajas_T...';


GO
ALTER TABLE [dbo].[LD_estados_cajas_T]
    ADD CONSTRAINT [pk_LD_estados_cajas_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[LD_operaciones_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LD_operaciones_T] (
    [id]    VARCHAR (50)  NOT NULL,
    [descr] VARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_LD_operaciones_T...';


GO
ALTER TABLE [dbo].[LD_operaciones_T]
    ADD CONSTRAINT [pk_LD_operaciones_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[licencias]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[licencias] (
    [texto] VARCHAR (1000) NOT NULL,
    [modif] TIMESTAMP      NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[lookup]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[lookup] (
    [reftab]   VARCHAR (20) NULL,
    [datasrc]  INT          NULL,
    [campoext] VARCHAR (20) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[lotes]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[lotes] (
    [cod_lote]  VARCHAR (50)  NOT NULL,
    [nro_orden] SMALLINT      NOT NULL,
    [volumen]   VARCHAR (8)   NOT NULL,
    [rotada]    SMALLINT      NULL,
    [reparada]  SMALLINT      NULL,
    [tipo]      INT           NOT NULL,
    [fecscan]   SMALLDATETIME NOT NULL,
    [ocr]       VARCHAR (1)   NOT NULL,
    [indice]    INT           NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_lotes...';


GO
ALTER TABLE [dbo].[lotes]
    ADD CONSTRAINT [PK_lotes] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[lotes].[IX_lotes_codlote_nroorden]...';


GO
CREATE NONCLUSTERED INDEX [IX_lotes_codlote_nroorden]
    ON [dbo].[lotes]([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [dbo].[LotesCajas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LotesCajas] (
    [cod_lote] VARCHAR (50) NOT NULL,
    [cod_caja] VARCHAR (30) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[LotesCajas].[IX_LotesCajas]...';


GO
CREATE CLUSTERED INDEX [IX_LotesCajas]
    ON [dbo].[LotesCajas]([cod_lote] ASC, [cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0);


GO
PRINT N'Creating [dbo].[LotesCajasDefinitivas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[LotesCajasDefinitivas] (
    [cod_lote] VARCHAR (50) NOT NULL,
    [cod_caja] VARCHAR (30) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_LotesCajasDefinitivas...';


GO
ALTER TABLE [dbo].[LotesCajasDefinitivas]
    ADD CONSTRAINT [PK_LotesCajasDefinitivas] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [cod_caja] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[marcasrescanning_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[marcasrescanning_T] (
    [id]    TINYINT       NOT NULL,
    [descr] VARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_marcasrescanning_T...';


GO
ALTER TABLE [dbo].[marcasrescanning_T]
    ADD CONSTRAINT [pk_marcasrescanning_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[membership_group_role_t]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[membership_group_role_t] (
    [role_id_fk]  CHAR (50)     NOT NULL,
    [group_id_fk] CHAR (50)     NOT NULL,
    [obj]         VARCHAR (200) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_membership_group_role_t...';


GO
ALTER TABLE [dbo].[membership_group_role_t]
    ADD CONSTRAINT [pk_membership_group_role_t] PRIMARY KEY CLUSTERED ([role_id_fk] ASC, [group_id_fk] ASC, [obj] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[membership_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[membership_T] (
    [user_id_fk]  CHAR (50) NOT NULL,
    [group_id_fk] CHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_membership_T...';


GO
ALTER TABLE [dbo].[membership_T]
    ADD CONSTRAINT [pk_membership_T] PRIMARY KEY CLUSTERED ([user_id_fk] ASC, [group_id_fk] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[membership_user_role_t]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[membership_user_role_t] (
    [user_id_fk] CHAR (50)     NOT NULL,
    [role_id_fk] CHAR (50)     NOT NULL,
    [obj]        VARCHAR (200) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_membership_user_role_t...';


GO
ALTER TABLE [dbo].[membership_user_role_t]
    ADD CONSTRAINT [pk_membership_user_role_t] PRIMARY KEY CLUSTERED ([role_id_fk] ASC, [user_id_fk] ASC, [obj] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[MLLanguages_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[MLLanguages_T] (
    [name]    VARCHAR (200) NOT NULL,
    [id]      CHAR (10)     NOT NULL,
    [culture] VARCHAR (50)  NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_MLLanguages_T...';


GO
ALTER TABLE [dbo].[MLLanguages_T]
    ADD CONSTRAINT [pk_MLLanguages_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[MLMessages_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[MLMessages_T] (
    [id]            INT           NOT NULL,
    [MLLanguage_ID] CHAR (10)     NOT NULL,
    [text]          VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_MLMessages_T...';


GO
ALTER TABLE [dbo].[MLMessages_T]
    ADD CONSTRAINT [pk_MLMessages_T] PRIMARY KEY CLUSTERED ([id] ASC, [MLLanguage_ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[MLObjects_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[MLObjects_T] (
    [id]           VARCHAR (250) NOT NULL,
    [MLMessage_ID] INT           NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_MLObjects_T...';


GO
ALTER TABLE [dbo].[MLObjects_T]
    ADD CONSTRAINT [pk_MLObjects_T] PRIMARY KEY CLUSTERED ([id] ASC, [MLMessage_ID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[montaje]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[montaje] (
    [pt]             SMALLINT      NOT NULL,
    [volumen]        VARCHAR (30)  NOT NULL,
    [mount]          VARCHAR (255) NOT NULL,
    [acceso]         SMALLINT      NOT NULL,
    [host]           VARCHAR (20)  NULL,
    [mountAccesoRed] VARCHAR (500) COLLATE Modern_Spanish_CI_AS NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_montaje...';


GO
ALTER TABLE [dbo].[montaje]
    ADD CONSTRAINT [pk_montaje] PRIMARY KEY CLUSTERED ([volumen] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[notas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[notas] (
    [cod_lote]  VARCHAR (20)  NOT NULL,
    [nro_orden] SMALLINT      NOT NULL,
    [tipo]      VARCHAR (20)  NULL,
    [txt]       VARCHAR (200) NULL,
    [tp]        VARCHAR (20)  NULL,
    [lft]       VARCHAR (20)  NULL,
    [hght]      VARCHAR (20)  NULL,
    [wdth]      VARCHAR (20)  NULL,
    [backcolor] VARCHAR (20)  NULL,
    [tag]       VARCHAR (20)  NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_notas...';


GO
ALTER TABLE [dbo].[notas]
    ADD CONSTRAINT [pk_notas] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Notificacion]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Notificacion] (
    [NotificacionId] UNIQUEIDENTIFIER NOT NULL,
    [FechaAlta]      DATETIME         NOT NULL,
    [Identificador]  CHAR (50)        NOT NULL,
    [UsuarioId]      CHAR (50)        NOT NULL,
    [Mensaje]        NVARCHAR (MAX)   NOT NULL,
    [Leida]          BIT              NOT NULL,
    [Archivada]      BIT              NOT NULL,
    [FechaLectura]   DATETIME         NULL,
    [FechaArchivo]   DATETIME         NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_Notificacion...';


GO
ALTER TABLE [dbo].[Notificacion]
    ADD CONSTRAINT [PK_Notificacion] PRIMARY KEY CLUSTERED ([NotificacionId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[NotificacionDetalle]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[NotificacionDetalle] (
    [NotificacionDetalleId] UNIQUEIDENTIFIER NOT NULL,
    [NotificacionId]        UNIQUEIDENTIFIER NOT NULL,
    [DocumentoId]           UNIQUEIDENTIFIER NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_NotificacionDetalle...';


GO
ALTER TABLE [dbo].[NotificacionDetalle]
    ADD CONSTRAINT [PK_NotificacionDetalle] PRIMARY KEY CLUSTERED ([NotificacionDetalleId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[OperacionEntregaDetalle]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[OperacionEntregaDetalle] (
    [OperacionEntregaDetalleId] INT           IDENTITY (1, 1) NOT NULL,
    [OperacionEntregaId]        INT           NOT NULL,
    [Orden]                     SMALLINT      NOT NULL,
    [Proceso]                   VARCHAR (500) NOT NULL,
    [Parametros]                VARCHAR (500) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_OperacionEntregaDetalle...';


GO
ALTER TABLE [dbo].[OperacionEntregaDetalle]
    ADD CONSTRAINT [PK_OperacionEntregaDetalle] PRIMARY KEY CLUSTERED ([OperacionEntregaDetalleId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[OperacionEntregas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[OperacionEntregas] (
    [OperacionEntregaId] INT          IDENTITY (1, 1) NOT NULL,
    [LoginEmpresa]       VARCHAR (20) NOT NULL,
    [OperacionId]        VARCHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_OperacionEntregas...';


GO
ALTER TABLE [dbo].[OperacionEntregas]
    ADD CONSTRAINT [PK_OperacionEntregas] PRIMARY KEY CLUSTERED ([OperacionEntregaId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[papelera]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[papelera] (
    [cod_lote]  VARCHAR (50) NOT NULL,
    [nro_orden] INT          NOT NULL,
    [tmplate]   VARCHAR (8)  NOT NULL,
    [FechaHora] DATETIME     NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[parameters]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[parameters] (
    [guid]               VARCHAR (255) NOT NULL,
    [description]        VARCHAR (255) NOT NULL,
    [type]               TINYINT       NOT NULL,
    [value]              VARCHAR (255) NOT NULL,
    [validateexpression] VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_parameters...';


GO
ALTER TABLE [dbo].[parameters]
    ADD CONSTRAINT [PK_parameters] PRIMARY KEY CLUSTERED ([guid] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[permisos]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[permisos] (
    [codigo]   VARCHAR (200) NOT NULL,
    [tmplate]  VARCHAR (8)   NOT NULL,
    [permisos] VARCHAR (30)  NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[permissions_groups_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[permissions_groups_T] (
    [task_id_fk]  CHAR (50) NOT NULL,
    [group_id_fk] CHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_permissions_groups_T...';


GO
ALTER TABLE [dbo].[permissions_groups_T]
    ADD CONSTRAINT [pk_permissions_groups_T] PRIMARY KEY CLUSTERED ([task_id_fk] ASC, [group_id_fk] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[permissions_roles_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[permissions_roles_T] (
    [role_task_id_fk] CHAR (50) NOT NULL,
    [role_id_fk]      CHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_permissions_roles_T...';


GO
ALTER TABLE [dbo].[permissions_roles_T]
    ADD CONSTRAINT [pk_permissions_roles_T] PRIMARY KEY CLUSTERED ([role_task_id_fk] ASC, [role_id_fk] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[permissions_users_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[permissions_users_T] (
    [task_id_fk] CHAR (50) NOT NULL,
    [user_id_fk] CHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_permissions_users_T...';


GO
ALTER TABLE [dbo].[permissions_users_T]
    ADD CONSTRAINT [pk_permissions_users_T] PRIMARY KEY CLUSTERED ([task_id_fk] ASC, [user_id_fk] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Procesamientos]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Procesamientos] (
    [cod_lote]             VARCHAR (50)  NOT NULL,
    [nro_orden]            SMALLINT      NOT NULL,
    [accion_procesamiento] VARCHAR (50)  NOT NULL,
    [cantidad]             INT           NOT NULL,
    [parametros]           VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_Procesamientos...';


GO
ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [PK_Procesamientos] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC, [accion_procesamiento] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[ProcesoEntrega_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ProcesoEntrega_T] (
    [cod_entrega]         VARCHAR (30)  NOT NULL,
    [lotesProcesados]     INT           NULL,
    [registrosProcesados] INT           NULL,
    [cajaActual]          VARCHAR (30)  NULL,
    [loteActual]          VARCHAR (16)  NULL,
    [operacion]           VARCHAR (100) NULL,
    [estado]              INT           NULL,
    [cdActual]            INT           NULL,
    [tamano]              BIGINT        NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_ProcesoEntrega_T...';


GO
ALTER TABLE [dbo].[ProcesoEntrega_T]
    ADD CONSTRAINT [pk_ProcesoEntrega_T] PRIMARY KEY CLUSTERED ([cod_entrega] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[ProcesoProcImg_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ProcesoProcImg_T] (
    [cod_lote]            VARCHAR (30)  NOT NULL,
    [registrosProcesados] INT           NULL,
    [operacion]           VARCHAR (100) NULL,
    [estado]              INT           NULL,
    [tmplate]             VARCHAR (8)   NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_ProcesoProcImg_T...';


GO
ALTER TABLE [dbo].[ProcesoProcImg_T]
    ADD CONSTRAINT [pk_ProcesoProcImg_T] PRIMARY KEY CLUSTERED ([cod_lote] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[ProcImg_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ProcImg_T] (
    [id]    INT          NOT NULL,
    [Descr] VARCHAR (50) NOT NULL,
    [code]  VARCHAR (20) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_ProcImg_T...';


GO
ALTER TABLE [dbo].[ProcImg_T]
    ADD CONSTRAINT [pk_ProcImg_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[ProcImg_Tmplate_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ProcImg_Tmplate_T] (
    [procimg_id] INT          NOT NULL,
    [tmplate]    VARCHAR (8)  NOT NULL,
    [orden]      INT          NOT NULL,
    [cant]       INT          NULL,
    [param1]     VARCHAR (50) NULL,
    [param2]     VARCHAR (50) NULL,
    [param3]     VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_ProcImg_Tmplate_T...';


GO
ALTER TABLE [dbo].[ProcImg_Tmplate_T]
    ADD CONSTRAINT [pk_ProcImg_Tmplate_T] PRIMARY KEY CLUSTERED ([procimg_id] ASC, [tmplate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[recepcionCajas]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[recepcionCajas] (
    [id]           VARCHAR (50) NOT NULL,
    [usuario]      VARCHAR (50) NOT NULL,
    [loginempresa] VARCHAR (50) NOT NULL,
    [estado]       VARCHAR (50) NOT NULL,
    [motivo]       VARCHAR (50) NOT NULL,
    [fechaCaptura] DATETIME     NOT NULL,
    [cod_recp]     INT          IDENTITY (1, 1) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[recepcionCajasDetalle]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[recepcionCajasDetalle] (
    [caja]         VARCHAR (50) NOT NULL,
    [idEntrega]    VARCHAR (50) NOT NULL,
    [fechaCaptura] DATETIME     NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[RecoleccionDatosCentros]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[RecoleccionDatosCentros] (
    [fecha]        DATETIME       NOT NULL,
    [tabla]        VARCHAR (255)  NULL,
    [sentenciaSQL] VARCHAR (4000) NULL,
    [estado]       INT            NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[RegistrosCopiados]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[RegistrosCopiados] (
    [tmplate]        VARCHAR (8)  NOT NULL,
    [cod_lote]       VARCHAR (50) NOT NULL,
    [nro_orden]      SMALLINT     NOT NULL,
    [nro_orden_dest] SMALLINT     NOT NULL,
    [fechahora]      DATETIME     NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[rescanning_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[rescanning_T] (
    [cod_lote]      VARCHAR (50)  NOT NULL,
    [nro_orden]     SMALLINT      NOT NULL,
    [marca]         TINYINT       NULL,
    [fechamarca]    DATETIME      NULL,
    [user_id]       CHAR (50)     NULL,
    [comentario]    VARCHAR (255) NULL,
    [clear]         CHAR (1)      NULL,
    [user_id_clear] CHAR (50)     NULL,
    [resultado]     VARCHAR (50)  NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_rescanning_T...';


GO
ALTER TABLE [dbo].[rescanning_T]
    ADD CONSTRAINT [pk_rescanning_T] PRIMARY KEY CLUSTERED ([cod_lote] ASC, [nro_orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[role_t]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[role_t] (
    [role_id] CHAR (50)     NOT NULL,
    [descr]   VARCHAR (200) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_role_t...';


GO
ALTER TABLE [dbo].[role_t]
    ADD CONSTRAINT [pk_role_t] PRIMARY KEY CLUSTERED ([role_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[role_task_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[role_task_T] (
    [role_task_id]  CHAR (50)     NOT NULL,
    [descr]         VARCHAR (200) NOT NULL,
    [lastNumber_bi] BIGINT        NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_role_task_T...';


GO
ALTER TABLE [dbo].[role_task_T]
    ADD CONSTRAINT [pk_role_task_T] PRIMARY KEY CLUSTERED ([role_task_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[RZonaUsuario]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[RZonaUsuario] (
    [tmplate] VARCHAR (8)   NOT NULL,
    [orden]   SMALLINT      NOT NULL,
    [usuario] VARCHAR (200) NOT NULL,
    [tp]      INT           NOT NULL,
    [lft]     INT           NOT NULL,
    [bttm]    INT           NOT NULL,
    [rght]    INT           NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_RZonaUsuario...';


GO
ALTER TABLE [dbo].[RZonaUsuario]
    ADD CONSTRAINT [PK_RZonaUsuario] PRIMARY KEY CLUSTERED ([tmplate] ASC, [orden] ASC, [usuario] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Scanners]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Scanners] (
    [codigo]      VARCHAR (10)  NOT NULL,
    [descripcion] VARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[SecuenciaLotes]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[SecuenciaLotes] (
    [SecuenciaLotesId]    UNIQUEIDENTIFIER NOT NULL,
    [LoteInicioSecuencia] VARCHAR (50)     NOT NULL,
    [FechaInicio]         DATETIME         NOT NULL,
    [FechaFin]            DATETIME         NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_SecuenciaLotes...';


GO
ALTER TABLE [dbo].[SecuenciaLotes]
    ADD CONSTRAINT [PK_SecuenciaLotes] PRIMARY KEY CLUSTERED ([SecuenciaLotesId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[SecuenciaLotes].[IX_SecuenciaLotes]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SecuenciaLotes]
    ON [dbo].[SecuenciaLotes]([LoteInicioSecuencia] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [dbo].[SecuenciaLotesDetalle]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[SecuenciaLotesDetalle] (
    [SecuenciaLotesDetalleId] UNIQUEIDENTIFIER NOT NULL,
    [SecuenciaLotesId]        UNIQUEIDENTIFIER NOT NULL,
    [Lote]                    VARCHAR (50)     NOT NULL,
    [Orden]                   SMALLINT         NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_SecuenciaLotesDetalle...';


GO
ALTER TABLE [dbo].[SecuenciaLotesDetalle]
    ADD CONSTRAINT [PK_SecuenciaLotesDetalle] PRIMARY KEY CLUSTERED ([SecuenciaLotesDetalleId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[SecuenciaLotesDetalle].[IX_SecuenciaLotesDetalle]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SecuenciaLotesDetalle]
    ON [dbo].[SecuenciaLotesDetalle]([Lote] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [dbo].[SecuenciaLotesDetalle].[IX_SecuenciaLotesDetalle_1]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SecuenciaLotesDetalle_1]
    ON [dbo].[SecuenciaLotesDetalle]([Lote] ASC, [Orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [dbo].[SeguridadDatos]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[SeguridadDatos] (
    [tmplate]     VARCHAR (8)  NOT NULL,
    [login]       VARCHAR (20) NOT NULL,
    [acceso_full] INT          NOT NULL,
    [campo]       VARCHAR (8)  NOT NULL,
    [valor]       VARCHAR (10) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[SeguridadDatosGrupos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[SeguridadDatosGrupos_T] (
    [tmplate]     VARCHAR (8)   NOT NULL,
    [group_id]    VARCHAR (50)  NOT NULL,
    [acceso_full] CHAR (1)      NOT NULL,
    [campo]       VARCHAR (8)   NOT NULL,
    [valor]       VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[SeguridadDatosUsuarios_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[SeguridadDatosUsuarios_T] (
    [tmplate]     VARCHAR (8)   NOT NULL,
    [user_id]     VARCHAR (50)  NOT NULL,
    [acceso_full] CHAR (1)      NOT NULL,
    [campo]       VARCHAR (8)   NOT NULL,
    [valor]       VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[sello]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[sello] (
    [valor] VARCHAR (1024) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[seteosCapturaTemplate]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[seteosCapturaTemplate] (
    [tmplate]  VARCHAR (50)  NOT NULL,
    [scanner]  VARCHAR (50)  NOT NULL,
    [filename] VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[SubLotes]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[SubLotes] (
    [cod_lote] VARCHAR (50) NOT NULL,
    [indice]   INT          NOT NULL,
    [nivel]    INT          NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[SubLotes].[IX_sublotes_cod_lote_indice]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_sublotes_cod_lote_indice]
    ON [dbo].[SubLotes]([cod_lote] ASC, [indice] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [dbo].[talonarios_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[talonarios_T] (
    [id]                VARCHAR (50)  NOT NULL,
    [descr]             VARCHAR (100) NULL,
    [ultNumero]         BIGINT        NULL,
    [centroFacturacion] CHAR (4)      NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_talonarios_T...';


GO
ALTER TABLE [dbo].[talonarios_T]
    ADD CONSTRAINT [pk_talonarios_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[Tarea]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Tarea] (
    [TareaId]        UNIQUEIDENTIFIER NOT NULL,
    [FechaAgregado]  DATETIME         NOT NULL,
    [Nombre]         NVARCHAR (100)   NOT NULL,
    [FechaInicio]    DATETIME         NULL,
    [Datos]          NVARCHAR (MAX)   NOT NULL,
    [Mensaje]        NVARCHAR (MAX)   NULL,
    [FechaEjecucion] DATETIME         NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_Tarea...';


GO
ALTER TABLE [dbo].[Tarea]
    ADD CONSTRAINT [PK_Tarea] PRIMARY KEY CLUSTERED ([TareaId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[task_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[task_T] (
    [task_id]       CHAR (50)     NOT NULL,
    [descr]         VARCHAR (200) NOT NULL,
    [lastNumber_bi] BIGINT        NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_task_T...';


GO
ALTER TABLE [dbo].[task_T]
    ADD CONSTRAINT [pk_task_T] PRIMARY KEY CLUSTERED ([task_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[tipos]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[tipos] (
    [tipo]       INT          NOT NULL,
    [descripcio] VARCHAR (50) NOT NULL,
    [extension]  VARCHAR (5)  NOT NULL,
    [visor]      INT          NOT NULL,
    [filtro]     INT          NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_tipos...';


GO
ALTER TABLE [dbo].[tipos]
    ADD CONSTRAINT [pk_tipos] PRIMARY KEY CLUSTERED ([tipo] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[tiposCampos_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[tiposCampos_T] (
    [id]    INT           NOT NULL,
    [descr] VARCHAR (100) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_tiposCampos_T...';


GO
ALTER TABLE [dbo].[tiposCampos_T]
    ADD CONSTRAINT [pk_tiposCampos_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[TipoTag]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[TipoTag] (
    [TipoTagId]   INT            IDENTITY (1, 1) NOT NULL,
    [Descripcion] NVARCHAR (100) NOT NULL,
    [Icono]       NVARCHAR (100) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_TipoTag...';


GO
ALTER TABLE [dbo].[TipoTag]
    ADD CONSTRAINT [PK_TipoTag] PRIMARY KEY CLUSTERED ([TipoTagId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[tmplates]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[tmplates] (
    [tmplate]             VARCHAR (8)   NOT NULL,
    [descripcio]          VARCHAR (50)  NOT NULL,
    [tipoindex]           SMALLINT      NOT NULL,
    [periodica]           SMALLINT      NOT NULL,
    [nro_zonas]           SMALLINT      NOT NULL,
    [tipo]                VARCHAR (2)   NOT NULL,
    [parent]              VARCHAR (8)   NOT NULL,
    [datasrc]             INT           NOT NULL,
    [ocr]                 SMALLINT      NOT NULL,
    [produccion]          VARCHAR (1)   NULL,
    [LoginEmpresa]        VARCHAR (20)  NULL,
    [BillingType]         VARCHAR (50)  NULL,
    [usaMergeDatos]       INT           NULL,
    [diasprocesoWarning]  INT           NULL,
    [diasprocesoError]    INT           NULL,
    [firmadigi]           CHAR (1)      NULL,
    [SeteosCaptura]       VARCHAR (255) NULL,
    [FormatoCapturaBN]    VARCHAR (255) NULL,
    [FormatoCapturaGrey]  VARCHAR (255) NULL,
    [FormatoCapturaColor] VARCHAR (255) NULL,
    [orderImagenes]       CHAR (1)      NULL,
    [cajasMensuales]      INT           NULL,
    [SeteosIndexacion]    VARCHAR (50)  NULL,
    [seteoDocumental]     TINYINT       NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_tmplates...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [pk_tmplates] PRIMARY KEY CLUSTERED ([tmplate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[TmplatesDatosComerciales]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[TmplatesDatosComerciales] (
    [Tmplate]                VARCHAR (8)      NOT NULL,
    [BillingType]            NVARCHAR (50)    NOT NULL,
    [DiasProcesoWarning]     INT              NOT NULL,
    [DiasProcesoError]       INT              NOT NULL,
    [CajasMensuales]         INT              NOT NULL,
    [SectorId]               UNIQUEIDENTIFIER NOT NULL,
    [ResponsableId]          UNIQUEIDENTIFIER NOT NULL,
    [PeriodicidadDias]       TINYINT          NOT NULL,
    [VolumenEsperadoDiario]  BIGINT           NOT NULL,
    [VolumenEsperadoSemanal] BIGINT           NOT NULL,
    [VolumenEsperadoMensual] BIGINT           NOT NULL,
    [Instructivo]            NVARCHAR (MAX)   NULL,
    [Cliente]                NVARCHAR (50)    NOT NULL,
    [MedioEntrega]           NVARCHAR (50)    NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_TmplatesDatosComerciales...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [PK_TmplatesDatosComerciales] PRIMARY KEY CLUSTERED ([Tmplate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[TmplatesResponsable]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[TmplatesResponsable] (
    [ResponsableId] UNIQUEIDENTIFIER NOT NULL,
    [Nombre]        NVARCHAR (100)   NOT NULL,
    [Telefono]      NVARCHAR (100)   NOT NULL,
    [Email]         NVARCHAR (100)   NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_TmplatesResponsable...';


GO
ALTER TABLE [dbo].[TmplatesResponsable]
    ADD CONSTRAINT [PK_TmplatesResponsable] PRIMARY KEY CLUSTERED ([ResponsableId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[typedep]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[typedep] (
    [codigo] INT      NOT NULL,
    [basico] SMALLINT NOT NULL,
    [depend] SMALLINT NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[user_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[user_T] (
    [user_id]                CHAR (50)     NOT NULL,
    [nameComplete]           VARCHAR (200) NOT NULL,
    [descr]                  VARCHAR (200) NULL,
    [changePassword]         CHAR (1)      NULL,
    [notAllowChangePassword] CHAR (1)      NULL,
    [bloq]                   CHAR (1)      NULL,
    [del]                    CHAR (1)      NULL,
    [password]               CHAR (100)    NOT NULL,
    [networkAlias]           VARCHAR (200) NULL,
    [fechalogin]             DATETIME      NULL,
    [fechapassword]          DATETIME      NULL,
    [reintetoslogin]         INT           NULL,
    [email]                  VARCHAR (200) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_user_T...';


GO
ALTER TABLE [dbo].[user_T]
    ADD CONSTRAINT [pk_user_T] PRIMARY KEY CLUSTERED ([user_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[usxgr]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[usxgr] (
    [cod_grupo] VARCHAR (200) NOT NULL,
    [cod_us]    VARCHAR (200) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[Version]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[Version] (
    [Product]   NVARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DBVersion] CHAR (6)      COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_Version...';


GO
ALTER TABLE [dbo].[Version]
    ADD CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED ([Product] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[VersionDetalles]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[VersionDetalles] (
    [VersionDetallesId] UNIQUEIDENTIFIER NOT NULL,
    [Product]           NVARCHAR (50)    COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DBVersion]         CHAR (6)         COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Fecha]             DATETIME         NOT NULL,
    [Usuario]           NVARCHAR (255)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [ScriptName]        NVARCHAR (255)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Script]            IMAGE            NULL,
    [Modificacion]      TIMESTAMP        NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating PK_VersionDetalles...';


GO
ALTER TABLE [dbo].[VersionDetalles]
    ADD CONSTRAINT [PK_VersionDetalles] PRIMARY KEY CLUSTERED ([VersionDetallesId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[visores]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[visores] (
    [visor]      INT           NOT NULL,
    [programa]   VARCHAR (254) NOT NULL,
    [descripcio] VARCHAR (20)  NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_visores...';


GO
ALTER TABLE [dbo].[visores]
    ADD CONSTRAINT [pk_visores] PRIMARY KEY CLUSTERED ([visor] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[vols]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[vols] (
    [volumen]        VARCHAR (30) NOT NULL,
    [coddev]         SMALLINT     NOT NULL,
    [finiciovalidez] DATETIME     NULL,
    [ffinvalidez]    DATETIME     NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_vols...';


GO
ALTER TABLE [dbo].[vols]
    ADD CONSTRAINT [pk_vols] PRIMARY KEY CLUSTERED ([volumen] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[volumestemplates]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[volumestemplates] (
    [volume]  VARCHAR (255) NOT NULL,
    [tmplate] VARCHAR (255) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_volumestemplates...';


GO
ALTER TABLE [dbo].[volumestemplates]
    ADD CONSTRAINT [pk_volumestemplates] PRIMARY KEY CLUSTERED ([volume] ASC, [tmplate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[WSProcImg_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[WSProcImg_T] (
    [id]    INT          NOT NULL,
    [descr] VARCHAR (50) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_WSProcImg_T...';


GO
ALTER TABLE [dbo].[WSProcImg_T]
    ADD CONSTRAINT [pk_WSProcImg_T] PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[WSProcImg_Tmplate_T]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[WSProcImg_Tmplate_T] (
    [wsProcImg_id] INT         NOT NULL,
    [tmplate]      VARCHAR (8) NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating pk_WSProcImg_Tmplate_T...';


GO
ALTER TABLE [dbo].[WSProcImg_Tmplate_T]
    ADD CONSTRAINT [pk_WSProcImg_Tmplate_T] PRIMARY KEY CLUSTERED ([wsProcImg_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


GO
PRINT N'Creating [dbo].[ZAAAAAAA]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ZAAAAAAA] (
    [COD_LOTE]  VARCHAR (20) NOT NULL,
    [NRO_ORDEN] INT          NOT NULL,
    [VOLUMEN]   VARCHAR (8)  NULL,
    [ROTACION]  INT          NULL,
    [TIPO]      INT          NULL,
    [TAMANO]    INT          NULL,
    [INDICE]    INT          NULL,
    [PRUEBA]    VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[ZAAAAAAB]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ZAAAAAAB] (
    [COD_LOTE]  VARCHAR (20) NOT NULL,
    [NRO_ORDEN] INT          NOT NULL,
    [VOLUMEN]   VARCHAR (8)  NULL,
    [ROTACION]  INT          NULL,
    [TIPO]      INT          NULL,
    [TAMANO]    INT          NULL,
    [INDICE]    INT          NULL,
    [PRUEBA]    VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[ZAAAAAAC]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[ZAAAAAAC] (
    [COD_LOTE]  VARCHAR (20) NOT NULL,
    [NRO_ORDEN] INT          NOT NULL,
    [VOLUMEN]   VARCHAR (8)  NULL,
    [ROTACION]  INT          NULL,
    [TIPO]      INT          NULL,
    [TAMANO]    INT          NULL,
    [INDICE]    INT          NULL,
    [PRUEBA]    VARCHAR (50) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[zona]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[zona] (
    [tmplate]      VARCHAR (8)   NOT NULL,
    [campo]        VARCHAR (8)   NOT NULL,
    [orden]        SMALLINT      NOT NULL,
    [tp]           INT           NULL,
    [lft]          INT           NULL,
    [bttm]         INT           NULL,
    [rght]         INT           NULL,
    [mndtory]      SMALLINT      NULL,
    [ocr1]         CHAR (1)      NULL,
    [ocr2]         CHAR (1)      NULL,
    [ocr3]         CHAR (1)      NULL,
    [ocr4]         SMALLINT      NULL,
    [ocr5]         SMALLINT      NOT NULL,
    [reftab]       VARCHAR (20)  NULL,
    [campoext]     VARCHAR (50)  NULL,
    [datasrc]      SMALLINT      NULL,
    [visual]       VARCHAR (5)   NULL,
    [filtrotabext] VARCHAR (255) NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[zonascan]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE TABLE [dbo].[zonascan] (
    [descrip]   VARCHAR (20) NOT NULL,
    [derecha]   INT          NOT NULL,
    [izquierda] INT          NOT NULL,
    [arriba]    INT          NOT NULL,
    [abajo]     INT          NOT NULL
);


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating DF_aud_clas_TipoIndexacion...';


GO
ALTER TABLE [dbo].[aud_clas]
    ADD CONSTRAINT [DF_aud_clas_TipoIndexacion] DEFAULT ((0)) FOR [TipoIndexacion];


GO
PRINT N'Creating DF_DocumentoTag_Eliminada...';


GO
ALTER TABLE [dbo].[DocumentoTag]
    ADD CONSTRAINT [DF_DocumentoTag_Eliminada] DEFAULT ('False') FOR [Eliminada];


GO
PRINT N'Creating DF_idlotes_controlrealizado...';


GO
ALTER TABLE [dbo].[idlotes]
    ADD CONSTRAINT [DF_idlotes_controlrealizado] DEFAULT ((0)) FOR [controlrealizado];


GO
PRINT N'Creating idlotesfeccracion...';


GO
ALTER TABLE [dbo].[idlotes]
    ADD CONSTRAINT [idlotesfeccracion] DEFAULT (getdate()) FOR [feccreacion];


GO
PRINT N'Creating DF__LD_Docume__apeno__10566F31...';


GO
ALTER TABLE [dbo].[LD_Documentos_HistoricoCB]
    ADD CONSTRAINT [DF__LD_Docume__apeno__10566F31] DEFAULT ('') FOR [apenom];


GO
PRINT N'Creating DF__LD_Docume__separ__114A936A...';


GO
ALTER TABLE [dbo].[LD_Documentos_HistoricoCB]
    ADD CONSTRAINT [DF__LD_Docume__separ__114A936A] DEFAULT ('N') FOR [separar];


GO
PRINT N'Creating DF_Notificacion_Archivada...';


GO
ALTER TABLE [dbo].[Notificacion]
    ADD CONSTRAINT [DF_Notificacion_Archivada] DEFAULT ('False') FOR [Archivada];


GO
PRINT N'Creating DF_Notificacion_Leida...';


GO
ALTER TABLE [dbo].[Notificacion]
    ADD CONSTRAINT [DF_Notificacion_Leida] DEFAULT ('False') FOR [Leida];


GO
PRINT N'Creating DF_OperacionEntregaDetalle_Orden...';


GO
ALTER TABLE [dbo].[OperacionEntregaDetalle]
    ADD CONSTRAINT [DF_OperacionEntregaDetalle_Orden] DEFAULT ((0)) FOR [Orden];


GO
PRINT N'Creating DF_OperacionEntregaDetalle_Parametros...';


GO
ALTER TABLE [dbo].[OperacionEntregaDetalle]
    ADD CONSTRAINT [DF_OperacionEntregaDetalle_Parametros] DEFAULT ('') FOR [Parametros];


GO
PRINT N'Creating DF_Procesamientos_cantidad...';


GO
ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [DF_Procesamientos_cantidad] DEFAULT ((0)) FOR [cantidad];


GO
PRINT N'Creating DF_Procesamientos_parametros...';


GO
ALTER TABLE [dbo].[Procesamientos]
    ADD CONSTRAINT [DF_Procesamientos_parametros] DEFAULT ('') FOR [parametros];


GO
PRINT N'Creating DF__Recolecci__estad__17F790F9...';


GO
ALTER TABLE [dbo].[RecoleccionDatosCentros]
    ADD CONSTRAINT [DF__Recolecci__estad__17F790F9] DEFAULT (0) FOR [estado];


GO
PRINT N'Creating DF_SecuenciaLotes_FechaInicio...';


GO
ALTER TABLE [dbo].[SecuenciaLotes]
    ADD CONSTRAINT [DF_SecuenciaLotes_FechaInicio] DEFAULT (getdate()) FOR [FechaInicio];


GO
PRINT N'Creating DF__tmplates__cajasM__047AA831...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF__tmplates__cajasM__047AA831] DEFAULT (0) FOR [cajasMensuales];


GO
PRINT N'Creating DF__tmplates__diaspr__5D60DB10...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF__tmplates__diaspr__5D60DB10] DEFAULT (0) FOR [diasprocesoWarning];


GO
PRINT N'Creating DF__tmplates__diaspr__5E54FF49...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF__tmplates__diaspr__5E54FF49] DEFAULT (0) FOR [diasprocesoError];


GO
PRINT N'Creating DF_tmplates_BillingType...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF_tmplates_BillingType] DEFAULT ('IMG') FOR [BillingType];


GO
PRINT N'Creating DF_tmplates_firmadigi...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF_tmplates_firmadigi] DEFAULT ('N') FOR [firmadigi];


GO
PRINT N'Creating DF_tmplates_ocr...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF_tmplates_ocr] DEFAULT ((1)) FOR [ocr];


GO
PRINT N'Creating DF_tmplates_orderImagenes...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF_tmplates_orderImagenes] DEFAULT ('A') FOR [orderImagenes];


GO
PRINT N'Creating DF_tmplates_seteoDocumental...';


GO
ALTER TABLE [dbo].[tmplates]
    ADD CONSTRAINT [DF_tmplates_seteoDocumental] DEFAULT ((0)) FOR [seteoDocumental];


GO
PRINT N'Creating DF_TmplatesDatosComerciales_BillingType...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [DF_TmplatesDatosComerciales_BillingType] DEFAULT ('IMG') FOR [BillingType];


GO
PRINT N'Creating DF_TmplatesDatosComerciales_CajasMensuales...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [DF_TmplatesDatosComerciales_CajasMensuales] DEFAULT ((0)) FOR [CajasMensuales];


GO
PRINT N'Creating DF_TmplatesDatosComerciales_DiasProcesoError...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [DF_TmplatesDatosComerciales_DiasProcesoError] DEFAULT ((0)) FOR [DiasProcesoError];


GO
PRINT N'Creating DF_TmplatesDatosComerciales_DiasProcesoWarning...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [DF_TmplatesDatosComerciales_DiasProcesoWarning] DEFAULT ((0)) FOR [DiasProcesoWarning];


GO
PRINT N'Creating DF_TmplatesDatosComerciales_PeriodicidadDias...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [DF_TmplatesDatosComerciales_PeriodicidadDias] DEFAULT ((0)) FOR [PeriodicidadDias];


GO
PRINT N'Creating DF_TmplatesDatosComerciales_VolumenEsperadoMensual...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales]
    ADD CONSTRAINT [DF_TmplatesDatosComerciales_VolumenEsperadoMensual] DEFAULT ((0)) FOR [VolumenEsperadoMensual];


GO
PRINT N'Creating DF_VersionDetalles_Fecha...';


GO
ALTER TABLE [dbo].[VersionDetalles]
    ADD CONSTRAINT [DF_VersionDetalles_Fecha] DEFAULT (getdate()) FOR [Fecha];


GO
PRINT N'Creating fk_Anotacion_Idlotes...';


GO
ALTER TABLE [dbo].[anotacion] WITH NOCHECK
    ADD CONSTRAINT [fk_Anotacion_Idlotes] FOREIGN KEY ([codlote]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_audit_T_task_T...';


GO
ALTER TABLE [dbo].[audit_T] WITH NOCHECK
    ADD CONSTRAINT [fk_audit_T_task_T] FOREIGN KEY ([task_id_fk]) REFERENCES [dbo].[task_T] ([task_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_audit_T_user_T...';


GO
ALTER TABLE [dbo].[audit_T] WITH NOCHECK
    ADD CONSTRAINT [fk_audit_T_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_busqueda_body_T_busqueda_header_T...';


GO
ALTER TABLE [dbo].[busqueda_body_T] WITH NOCHECK
    ADD CONSTRAINT [fk_busqueda_body_T_busqueda_header_T] FOREIGN KEY ([id]) REFERENCES [dbo].[busqueda_header_T] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_Cajas_Empresas...';


GO
ALTER TABLE [dbo].[Cajas] WITH NOCHECK
    ADD CONSTRAINT [fk_Cajas_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_CajasEntregas_Cajas...';


GO
ALTER TABLE [dbo].[CajasEntregas] WITH NOCHECK
    ADD CONSTRAINT [fk_CajasEntregas_Cajas] FOREIGN KEY ([cod_caja]) REFERENCES [dbo].[Cajas] ([cod_caja]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_CajasEntregas_Entregas...';


GO
ALTER TABLE [dbo].[CajasEntregas] WITH NOCHECK
    ADD CONSTRAINT [fk_CajasEntregas_Entregas] FOREIGN KEY ([cod_entrega]) REFERENCES [dbo].[Entregas] ([cod_entrega]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_DocumentoImagen_Documento...';


GO
ALTER TABLE [dbo].[DocumentoImagen] WITH NOCHECK
    ADD CONSTRAINT [FK_DocumentoImagen_Documento] FOREIGN KEY ([DocumentoId]) REFERENCES [dbo].[Documento] ([DocumentoId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_DocumentoTag_Documento...';


GO
ALTER TABLE [dbo].[DocumentoTag] WITH NOCHECK
    ADD CONSTRAINT [FK_DocumentoTag_Documento] FOREIGN KEY ([DocumentoId]) REFERENCES [dbo].[Documento] ([DocumentoId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_EmpresaSector_Empresas...';


GO
ALTER TABLE [dbo].[EmpresaSector] WITH NOCHECK
    ADD CONSTRAINT [FK_EmpresaSector_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_EntregasTmplates_Entregas...';


GO
ALTER TABLE [dbo].[EntregasTmplates] WITH NOCHECK
    ADD CONSTRAINT [FK_EntregasTmplates_Entregas] FOREIGN KEY ([cod_entrega]) REFERENCES [dbo].[Entregas] ([cod_entrega]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_EntregasTmplates_tmplates...';


GO
ALTER TABLE [dbo].[EntregasTmplates] WITH NOCHECK
    ADD CONSTRAINT [FK_EntregasTmplates_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_grpyus_Empresas...';


GO
ALTER TABLE [dbo].[grpyus] WITH NOCHECK
    ADD CONSTRAINT [fk_grpyus_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_idlotes_tmplates...';


GO
ALTER TABLE [dbo].[idlotes] WITH NOCHECK
    ADD CONSTRAINT [fk_idlotes_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_invalidos_idlotes...';


GO
ALTER TABLE [dbo].[invalidos] WITH NOCHECK
    ADD CONSTRAINT [fk_invalidos_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_invalidos_tmplates...';


GO
ALTER TABLE [dbo].[invalidos] WITH NOCHECK
    ADD CONSTRAINT [fk_invalidos_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_LD_auditoriaDocumentos_T_idlotes...';


GO
ALTER TABLE [dbo].[LD_auditoriaDocumentos_T] WITH NOCHECK
    ADD CONSTRAINT [fk_LD_auditoriaDocumentos_T_idlotes] FOREIGN KEY ([lote]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_lotes_idlotes...';


GO
ALTER TABLE [dbo].[lotes] WITH NOCHECK
    ADD CONSTRAINT [fk_lotes_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_membership_group_role_t_group_T...';


GO
ALTER TABLE [dbo].[membership_group_role_t] WITH NOCHECK
    ADD CONSTRAINT [fk_membership_group_role_t_group_T] FOREIGN KEY ([group_id_fk]) REFERENCES [dbo].[group_T] ([group_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_membership_group_role_t_role_T...';


GO
ALTER TABLE [dbo].[membership_group_role_t] WITH NOCHECK
    ADD CONSTRAINT [fk_membership_group_role_t_role_T] FOREIGN KEY ([role_id_fk]) REFERENCES [dbo].[role_t] ([role_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_membership_T_group_T...';


GO
ALTER TABLE [dbo].[membership_T] WITH NOCHECK
    ADD CONSTRAINT [fk_membership_T_group_T] FOREIGN KEY ([group_id_fk]) REFERENCES [dbo].[group_T] ([group_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_membership_T_user_T...';


GO
ALTER TABLE [dbo].[membership_T] WITH NOCHECK
    ADD CONSTRAINT [fk_membership_T_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_membership_user_role_t_role_T...';


GO
ALTER TABLE [dbo].[membership_user_role_t] WITH NOCHECK
    ADD CONSTRAINT [fk_membership_user_role_t_role_T] FOREIGN KEY ([role_id_fk]) REFERENCES [dbo].[role_t] ([role_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_membership_user_role_t_user_T...';


GO
ALTER TABLE [dbo].[membership_user_role_t] WITH NOCHECK
    ADD CONSTRAINT [fk_membership_user_role_t_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_MLMessages_T_MLLanguages_T...';


GO
ALTER TABLE [dbo].[MLMessages_T] WITH NOCHECK
    ADD CONSTRAINT [fk_MLMessages_T_MLLanguages_T] FOREIGN KEY ([MLLanguage_ID]) REFERENCES [dbo].[MLLanguages_T] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_montaje_vols...';


GO
ALTER TABLE [dbo].[montaje] WITH NOCHECK
    ADD CONSTRAINT [fk_montaje_vols] FOREIGN KEY ([volumen]) REFERENCES [dbo].[vols] ([volumen]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_OperacionEntregas_Empresas...';


GO
ALTER TABLE [dbo].[OperacionEntregas] WITH NOCHECK
    ADD CONSTRAINT [FK_OperacionEntregas_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permisos_grpyus...';


GO
ALTER TABLE [dbo].[permisos] WITH NOCHECK
    ADD CONSTRAINT [fk_permisos_grpyus] FOREIGN KEY ([codigo]) REFERENCES [dbo].[grpyus] ([codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permisos_tmplates...';


GO
ALTER TABLE [dbo].[permisos] WITH NOCHECK
    ADD CONSTRAINT [fk_permisos_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permissions_groups_T_group_T...';


GO
ALTER TABLE [dbo].[permissions_groups_T] WITH NOCHECK
    ADD CONSTRAINT [fk_permissions_groups_T_group_T] FOREIGN KEY ([group_id_fk]) REFERENCES [dbo].[group_T] ([group_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permissions_groups_T_task_T...';


GO
ALTER TABLE [dbo].[permissions_groups_T] WITH NOCHECK
    ADD CONSTRAINT [fk_permissions_groups_T_task_T] FOREIGN KEY ([task_id_fk]) REFERENCES [dbo].[task_T] ([task_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permissions_roles_T_role_T...';


GO
ALTER TABLE [dbo].[permissions_roles_T] WITH NOCHECK
    ADD CONSTRAINT [fk_permissions_roles_T_role_T] FOREIGN KEY ([role_id_fk]) REFERENCES [dbo].[role_t] ([role_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permissions_roles_T_role_task_T...';


GO
ALTER TABLE [dbo].[permissions_roles_T] WITH NOCHECK
    ADD CONSTRAINT [fk_permissions_roles_T_role_task_T] FOREIGN KEY ([role_task_id_fk]) REFERENCES [dbo].[role_task_T] ([role_task_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permissions_users_T_task_T...';


GO
ALTER TABLE [dbo].[permissions_users_T] WITH NOCHECK
    ADD CONSTRAINT [fk_permissions_users_T_task_T] FOREIGN KEY ([task_id_fk]) REFERENCES [dbo].[task_T] ([task_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_permissions_users_T_user_T...';


GO
ALTER TABLE [dbo].[permissions_users_T] WITH NOCHECK
    ADD CONSTRAINT [fk_permissions_users_T_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_ProcImg_Tmplate_T_ProcImg_T...';


GO
ALTER TABLE [dbo].[ProcImg_Tmplate_T] WITH NOCHECK
    ADD CONSTRAINT [fk_ProcImg_Tmplate_T_ProcImg_T] FOREIGN KEY ([procimg_id]) REFERENCES [dbo].[ProcImg_T] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_SecuenciaLotes_idlotes...';


GO
ALTER TABLE [dbo].[SecuenciaLotes] WITH NOCHECK
    ADD CONSTRAINT [FK_SecuenciaLotes_idlotes] FOREIGN KEY ([LoteInicioSecuencia]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_SecuenciaLotesDetalle_idlotes...';


GO
ALTER TABLE [dbo].[SecuenciaLotesDetalle] WITH NOCHECK
    ADD CONSTRAINT [FK_SecuenciaLotesDetalle_idlotes] FOREIGN KEY ([Lote]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_SecuenciaLotesDetalle_SecuenciaLotes...';


GO
ALTER TABLE [dbo].[SecuenciaLotesDetalle] WITH NOCHECK
    ADD CONSTRAINT [FK_SecuenciaLotesDetalle_SecuenciaLotes] FOREIGN KEY ([SecuenciaLotesId]) REFERENCES [dbo].[SecuenciaLotes] ([SecuenciaLotesId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_SubLotes_idlotes...';


GO
ALTER TABLE [dbo].[SubLotes] WITH NOCHECK
    ADD CONSTRAINT [fk_SubLotes_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_tmplates_Empresas...';


GO
ALTER TABLE [dbo].[tmplates] WITH NOCHECK
    ADD CONSTRAINT [fk_tmplates_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_TmplatesDatosComerciales_EmpresaSector...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales] WITH NOCHECK
    ADD CONSTRAINT [FK_TmplatesDatosComerciales_EmpresaSector] FOREIGN KEY ([SectorId]) REFERENCES [dbo].[EmpresaSector] ([SectorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_TmplatesDatosComerciales_tmplates...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales] WITH NOCHECK
    ADD CONSTRAINT [FK_TmplatesDatosComerciales_tmplates] FOREIGN KEY ([Tmplate]) REFERENCES [dbo].[tmplates] ([tmplate]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_TmplatesDatosComerciales_TmplatesResponsable...';


GO
ALTER TABLE [dbo].[TmplatesDatosComerciales] WITH NOCHECK
    ADD CONSTRAINT [FK_TmplatesDatosComerciales_TmplatesResponsable] FOREIGN KEY ([ResponsableId]) REFERENCES [dbo].[TmplatesResponsable] ([ResponsableId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_usxgr_grpyus...';


GO
ALTER TABLE [dbo].[usxgr] WITH NOCHECK
    ADD CONSTRAINT [fk_usxgr_grpyus] FOREIGN KEY ([cod_us]) REFERENCES [dbo].[grpyus] ([codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_usxgr_grpyus2...';


GO
ALTER TABLE [dbo].[usxgr] WITH NOCHECK
    ADD CONSTRAINT [fk_usxgr_grpyus2] FOREIGN KEY ([cod_grupo]) REFERENCES [dbo].[grpyus] ([codigo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_vols_device...';


GO
ALTER TABLE [dbo].[vols] WITH NOCHECK
    ADD CONSTRAINT [fk_vols_device] FOREIGN KEY ([coddev]) REFERENCES [dbo].[device] ([coddev]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_zona_dicdatos...';


GO
ALTER TABLE [dbo].[zona] WITH NOCHECK
    ADD CONSTRAINT [fk_zona_dicdatos] FOREIGN KEY ([campo]) REFERENCES [dbo].[dicdatos] ([campo]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating fk_zona_tmplates...';


GO
ALTER TABLE [dbo].[zona] WITH NOCHECK
    ADD CONSTRAINT [fk_zona_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating [dbo].[LD_Documentos_CBApeNomTrigger]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
-- =============================================
-- Create trigger basic template(After trigger)
-- =============================================
CREATE TRIGGER [LD_Documentos_CBApeNomTrigger]
ON [dbo].[LD_Documentos_HistoricoCB]
FOR DELETE, INSERT, UPDATE 
AS 
BEGIN
	update LD_Documentos_HistoricoCB set apenom = apellido + ' ' + nombres where apenom <> apellido + ' ' + nombres
END
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[listTableRowCounts]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
CREATE PROCEDURE [dbo].[listTableRowCounts] 
AS 
BEGIN 
    SET NOCOUNT ON 
 
    DECLARE @SQL VARCHAR(255) 
    SET @SQL = 'DBCC UPDATEUSAGE (' + DB_NAME() + ')' 
    EXEC(@SQL) 
 
    CREATE TABLE #foo 
    ( 
        tablename VARCHAR(255), 
        rc INT 
    ) 
     
    INSERT #foo 
        EXEC sp_msForEachTable 
            'SELECT PARSENAME(''?'', 1), 
            COUNT(*) FROM ?' 
 
    SELECT tablename, rc 
        FROM #foo 
        ORDER BY rc DESC 
 
    DROP TABLE #foo 
END
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[GetIdCentroDigitalizacion]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
-- =============================================
-- Create scalar function
-- =============================================

CREATE FUNCTION [dbo].GetIdCentroDigitalizacion	()
RETURNS char(10)
AS
BEGIN
	RETURN 'LUG'
END
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
PRINT N'Creating [dbo].[valorNumerico]...';


GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON;


GO
create function valorNumerico(@cadena varchar(50))
returns int
as
begin
  declare @cadaux varchar(50)
  declare @i int
  set @i = 1
  set @cadaux = ''
  while (@i <= len(@cadena))
  begin
    if (isnumeric(substring(@cadena,@i,1)) = 1) and (substring(@cadena,@i,1) <> '-') and (substring(@cadena,@i,1) <> '+')
    begin
      set @cadaux = @cadaux + substring(@cadena,@i,1)
    end
  set @i = @i + 1
  end
  return cast(@cadaux as int)
end
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER OFF;


GO
-- Refactoring step to update target server with deployed transaction logs
CREATE TABLE  [dbo].[__RefactorLog] (OperationKey UNIQUEIDENTIFIER NOT NULL PRIMARY KEY)
GO
sp_addextendedproperty N'microsoft_database_tools_support', N'refactoring log', N'schema', N'dbo', N'table', N'__RefactorLog'
GO

GO
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO
PRINT N'Checking existing data against newly created constraints';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[anotacion] WITH CHECK CHECK CONSTRAINT [fk_Anotacion_Idlotes];

ALTER TABLE [dbo].[audit_T] WITH CHECK CHECK CONSTRAINT [fk_audit_T_task_T];

ALTER TABLE [dbo].[audit_T] WITH CHECK CHECK CONSTRAINT [fk_audit_T_user_T];

ALTER TABLE [dbo].[busqueda_body_T] WITH CHECK CHECK CONSTRAINT [fk_busqueda_body_T_busqueda_header_T];

ALTER TABLE [dbo].[Cajas] WITH CHECK CHECK CONSTRAINT [fk_Cajas_Empresas];

ALTER TABLE [dbo].[CajasEntregas] WITH CHECK CHECK CONSTRAINT [fk_CajasEntregas_Cajas];

ALTER TABLE [dbo].[CajasEntregas] WITH CHECK CHECK CONSTRAINT [fk_CajasEntregas_Entregas];

ALTER TABLE [dbo].[DocumentoImagen] WITH CHECK CHECK CONSTRAINT [FK_DocumentoImagen_Documento];

ALTER TABLE [dbo].[DocumentoTag] WITH CHECK CHECK CONSTRAINT [FK_DocumentoTag_Documento];

ALTER TABLE [dbo].[EmpresaSector] WITH CHECK CHECK CONSTRAINT [FK_EmpresaSector_Empresas];

ALTER TABLE [dbo].[EntregasTmplates] WITH CHECK CHECK CONSTRAINT [FK_EntregasTmplates_Entregas];

ALTER TABLE [dbo].[EntregasTmplates] WITH CHECK CHECK CONSTRAINT [FK_EntregasTmplates_tmplates];

ALTER TABLE [dbo].[grpyus] WITH CHECK CHECK CONSTRAINT [fk_grpyus_Empresas];

ALTER TABLE [dbo].[idlotes] WITH CHECK CHECK CONSTRAINT [fk_idlotes_tmplates];

ALTER TABLE [dbo].[invalidos] WITH CHECK CHECK CONSTRAINT [fk_invalidos_idlotes];

ALTER TABLE [dbo].[invalidos] WITH CHECK CHECK CONSTRAINT [fk_invalidos_tmplates];

ALTER TABLE [dbo].[LD_auditoriaDocumentos_T] WITH CHECK CHECK CONSTRAINT [fk_LD_auditoriaDocumentos_T_idlotes];

ALTER TABLE [dbo].[lotes] WITH CHECK CHECK CONSTRAINT [fk_lotes_idlotes];

ALTER TABLE [dbo].[membership_group_role_t] WITH CHECK CHECK CONSTRAINT [fk_membership_group_role_t_group_T];

ALTER TABLE [dbo].[membership_group_role_t] WITH CHECK CHECK CONSTRAINT [fk_membership_group_role_t_role_T];

ALTER TABLE [dbo].[membership_T] WITH CHECK CHECK CONSTRAINT [fk_membership_T_group_T];

ALTER TABLE [dbo].[membership_T] WITH CHECK CHECK CONSTRAINT [fk_membership_T_user_T];

ALTER TABLE [dbo].[membership_user_role_t] WITH CHECK CHECK CONSTRAINT [fk_membership_user_role_t_role_T];

ALTER TABLE [dbo].[membership_user_role_t] WITH CHECK CHECK CONSTRAINT [fk_membership_user_role_t_user_T];

ALTER TABLE [dbo].[MLMessages_T] WITH CHECK CHECK CONSTRAINT [fk_MLMessages_T_MLLanguages_T];

ALTER TABLE [dbo].[montaje] WITH CHECK CHECK CONSTRAINT [fk_montaje_vols];

ALTER TABLE [dbo].[OperacionEntregas] WITH CHECK CHECK CONSTRAINT [FK_OperacionEntregas_Empresas];

ALTER TABLE [dbo].[permisos] WITH CHECK CHECK CONSTRAINT [fk_permisos_grpyus];

ALTER TABLE [dbo].[permisos] WITH CHECK CHECK CONSTRAINT [fk_permisos_tmplates];

ALTER TABLE [dbo].[permissions_groups_T] WITH CHECK CHECK CONSTRAINT [fk_permissions_groups_T_group_T];

ALTER TABLE [dbo].[permissions_groups_T] WITH CHECK CHECK CONSTRAINT [fk_permissions_groups_T_task_T];

ALTER TABLE [dbo].[permissions_roles_T] WITH CHECK CHECK CONSTRAINT [fk_permissions_roles_T_role_T];

ALTER TABLE [dbo].[permissions_roles_T] WITH CHECK CHECK CONSTRAINT [fk_permissions_roles_T_role_task_T];

ALTER TABLE [dbo].[permissions_users_T] WITH CHECK CHECK CONSTRAINT [fk_permissions_users_T_task_T];

ALTER TABLE [dbo].[permissions_users_T] WITH CHECK CHECK CONSTRAINT [fk_permissions_users_T_user_T];

ALTER TABLE [dbo].[ProcImg_Tmplate_T] WITH CHECK CHECK CONSTRAINT [fk_ProcImg_Tmplate_T_ProcImg_T];

ALTER TABLE [dbo].[SecuenciaLotes] WITH CHECK CHECK CONSTRAINT [FK_SecuenciaLotes_idlotes];

ALTER TABLE [dbo].[SecuenciaLotesDetalle] WITH CHECK CHECK CONSTRAINT [FK_SecuenciaLotesDetalle_idlotes];

ALTER TABLE [dbo].[SecuenciaLotesDetalle] WITH CHECK CHECK CONSTRAINT [FK_SecuenciaLotesDetalle_SecuenciaLotes];

ALTER TABLE [dbo].[SubLotes] WITH CHECK CHECK CONSTRAINT [fk_SubLotes_idlotes];

ALTER TABLE [dbo].[tmplates] WITH CHECK CHECK CONSTRAINT [fk_tmplates_Empresas];

ALTER TABLE [dbo].[TmplatesDatosComerciales] WITH CHECK CHECK CONSTRAINT [FK_TmplatesDatosComerciales_EmpresaSector];

ALTER TABLE [dbo].[TmplatesDatosComerciales] WITH CHECK CHECK CONSTRAINT [FK_TmplatesDatosComerciales_tmplates];

ALTER TABLE [dbo].[TmplatesDatosComerciales] WITH CHECK CHECK CONSTRAINT [FK_TmplatesDatosComerciales_TmplatesResponsable];

ALTER TABLE [dbo].[usxgr] WITH CHECK CHECK CONSTRAINT [fk_usxgr_grpyus];

ALTER TABLE [dbo].[usxgr] WITH CHECK CHECK CONSTRAINT [fk_usxgr_grpyus2];

ALTER TABLE [dbo].[vols] WITH CHECK CHECK CONSTRAINT [fk_vols_device];

ALTER TABLE [dbo].[zona] WITH CHECK CHECK CONSTRAINT [fk_zona_dicdatos];

ALTER TABLE [dbo].[zona] WITH CHECK CHECK CONSTRAINT [fk_zona_tmplates];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        DECLARE @VarDecimalSupported AS BIT;
        SELECT @VarDecimalSupported = 0;
        IF ((ServerProperty(N'EngineEdition') = 3)
            AND (((@@microsoftversion / power(2, 24) = 9)
                  AND (@@microsoftversion & 0xffff >= 3024))
                 OR ((@@microsoftversion / power(2, 24) = 10)
                     AND (@@microsoftversion & 0xffff >= 1600))))
            SELECT @VarDecimalSupported = 1;
        IF (@VarDecimalSupported > 0)
            BEGIN
                EXECUTE sp_db_vardecimal_storage_format N'$(DatabaseName)', 'ON';
            END
    END


GO

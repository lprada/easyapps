﻿CREATE TABLE [dbo].[Tarea] (
    [TareaId]        UNIQUEIDENTIFIER NOT NULL,
    [FechaAgregado]  DATETIME         NOT NULL,
    [Nombre]         NVARCHAR (100)   NOT NULL,
    [FechaInicio]    DATETIME         NULL,
    [Datos]          NVARCHAR (MAX)   NOT NULL,
    [Mensaje]        NVARCHAR (MAX)   NULL,
    [FechaEjecucion] DATETIME         NULL
);


﻿ALTER TABLE [dbo].[hipervinculos_T]
    ADD CONSTRAINT [pk_hipervinculos_T] PRIMARY KEY CLUSTERED ([templateOriginal] ASC, [campoOriginal] ASC, [templateDestino] ASC, [campoDestino] ASC, [orden] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


﻿/*
CREATE TRIGGER [dbo].[SubloteNivel2]
   ON  [dbo].[lotes]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  --solo el tmplate ICF
  if exists(select * from inserted i, idlotes il
            where i.cod_lote = il.cod_lote and (il.tmplate = 'ZATCHU' or il.tmplate = 'ZHAJAU'))
  begin
    delete sublotes 
    from sublotes sl, inserted i, idlotes il
    where sl.cod_lote = i.cod_lote
    and sl.indice = i.indice
    and sl.nivel = 2
    and i.indice % 2 = 1
    and il.cod_lote = i.cod_lote and (il.tmplate = 'ZATCHU' or il.tmplate = 'ZHAJAU')

	INSERT INTO sublotes
	select i.cod_lote,i.indice,2 from inserted i, idlotes il
    where i.indice % 2 = 1
    and il.cod_lote = i.cod_lote and (il.tmplate = 'ZATCHU' or il.tmplate = 'ZHAJAU')
  end

END*/



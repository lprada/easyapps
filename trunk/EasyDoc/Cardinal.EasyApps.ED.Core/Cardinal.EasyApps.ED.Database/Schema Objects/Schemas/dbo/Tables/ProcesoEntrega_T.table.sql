﻿CREATE TABLE [dbo].[ProcesoEntrega_T] (
    [cod_entrega]         VARCHAR (30)  NOT NULL,
    [lotesProcesados]     INT           NULL,
    [registrosProcesados] INT           NULL,
    [cajaActual]          VARCHAR (30)  NULL,
    [loteActual]          VARCHAR (16)  NULL,
    [operacion]           VARCHAR (100) NULL,
    [estado]              INT           NULL,
    [cdActual]            INT           NULL,
    [tamano]              BIGINT        NULL
);




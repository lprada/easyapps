﻿CREATE TABLE [dbo].[lotes] (
    [cod_lote]  VARCHAR (50)  NOT NULL,
    [nro_orden] SMALLINT      NOT NULL,
    [volumen]   VARCHAR (8)   NOT NULL,
    [rotada]    SMALLINT      NULL,
    [reparada]  SMALLINT      NULL,
    [tipo]      INT           NOT NULL,
    [fecscan]   SMALLDATETIME NOT NULL,
    [ocr]       VARCHAR (1)   NOT NULL,
    [indice]    INT           NULL
);




﻿CREATE TABLE [dbo].[aud_logis] (
    [cod_us]     VARCHAR (50)  NOT NULL,
    [fecha]      SMALLDATETIME NOT NULL,
    [hora]       SMALLDATETIME NOT NULL,
    [tipo_obj]   VARCHAR (1)   NOT NULL,
    [cod_obj]    VARCHAR (30)  NOT NULL,
    [operacion]  INT           NOT NULL,
    [comentario] VARCHAR (100) NULL
);




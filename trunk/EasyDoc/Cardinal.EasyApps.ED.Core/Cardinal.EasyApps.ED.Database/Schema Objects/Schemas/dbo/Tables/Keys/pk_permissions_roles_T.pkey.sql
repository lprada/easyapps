﻿ALTER TABLE [dbo].[permissions_roles_T]
    ADD CONSTRAINT [pk_permissions_roles_T] PRIMARY KEY CLUSTERED ([role_task_id_fk] ASC, [role_id_fk] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


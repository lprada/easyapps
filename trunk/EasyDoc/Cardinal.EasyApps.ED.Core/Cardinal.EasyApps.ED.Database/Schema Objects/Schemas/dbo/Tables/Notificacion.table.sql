﻿CREATE TABLE [dbo].[Notificacion] (
    [NotificacionId] UNIQUEIDENTIFIER NOT NULL,
    [FechaAlta]      DATETIME         NOT NULL,
    [Identificador]  CHAR (50)        NOT NULL,
    [UsuarioId]      CHAR (50)        NOT NULL,
    [Mensaje]        NVARCHAR (MAX)   NOT NULL,
    [Leida]          BIT              NOT NULL,
    [Archivada]      BIT              NOT NULL,
    [FechaLectura]   DATETIME         NULL,
    [FechaArchivo]   DATETIME         NULL
);


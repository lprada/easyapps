﻿CREATE TABLE [dbo].[recepcionCajas] (
    [id]           VARCHAR (50) NOT NULL,
    [usuario]      VARCHAR (50) NOT NULL,
    [loginempresa] VARCHAR (50) NOT NULL,
    [estado]       VARCHAR (50) NOT NULL,
    [motivo]       VARCHAR (50) NOT NULL,
    [fechaCaptura] DATETIME     NOT NULL,
    [cod_recp]     INT          IDENTITY (1, 1) NOT NULL
);




﻿ALTER TABLE [dbo].[Tarea]
    ADD CONSTRAINT [PK_Tarea] PRIMARY KEY CLUSTERED ([TareaId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


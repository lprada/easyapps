﻿ALTER TABLE [dbo].[role_t]
    ADD CONSTRAINT [pk_role_t] PRIMARY KEY CLUSTERED ([role_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


﻿CREATE TABLE [dbo].[datasrc] (
    [codigo]     INT           NOT NULL,
    [datasrc]    VARCHAR (254) NOT NULL,
    [usuario]    VARCHAR (30)  NULL,
    [password]   VARCHAR (50)  NULL,
    [usetxret]   SMALLINT      NOT NULL,
    [pgrant]     SMALLINT      NOT NULL,
    [porderby]   SMALLINT      NOT NULL,
    [pindex]     SMALLINT      NOT NULL,
    [typedep]    VARCHAR (60)  NULL,
    [dstype]     VARCHAR (20)  NULL,
    [fmtFechas]  VARCHAR (40)  NULL,
    [dstypename] VARCHAR (30)  NULL
);




﻿ALTER TABLE [dbo].[membership_user_role_t]
    ADD CONSTRAINT [pk_membership_user_role_t] PRIMARY KEY CLUSTERED ([role_id_fk] ASC, [user_id_fk] ASC, [obj] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);


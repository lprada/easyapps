﻿CREATE TABLE [dbo].[Cajas] (
    [cod_caja]     VARCHAR (30)  NOT NULL,
    [LoginEmpresa] VARCHAR (20)  NULL,
    [Fecha]        DATETIME      NULL,
    [Estado]       INT           NULL,
    [FRecepcion]   DATETIME      NULL,
    [FProceso]     DATETIME      NULL,
    [FTerminacion] DATETIME      NULL,
    [FEntrega]     DATETIME      NULL,
    [FRecepDatos]  DATETIME      NULL,
    [FCaptura]     DATETIME      NULL,
    [estadoLogico] INT           NULL,
    [tmplate]      VARCHAR (255) NULL
);




﻿CREATE TABLE [dbo].[TipoTag] (
    [TipoTagId]   INT            IDENTITY (1, 1) NOT NULL,
    [Descripcion] NVARCHAR (100) NOT NULL,
    [Icono]       NVARCHAR (100) NULL
);


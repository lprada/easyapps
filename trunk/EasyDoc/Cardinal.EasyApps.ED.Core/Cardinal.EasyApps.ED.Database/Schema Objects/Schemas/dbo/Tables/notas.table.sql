﻿CREATE TABLE [dbo].[notas] (
    [cod_lote]  VARCHAR (20)  NOT NULL,
    [nro_orden] SMALLINT      NOT NULL,
    [tipo]      VARCHAR (20)  NULL,
    [txt]       VARCHAR (200) NULL,
    [tp]        VARCHAR (20)  NULL,
    [lft]       VARCHAR (20)  NULL,
    [hght]      VARCHAR (20)  NULL,
    [wdth]      VARCHAR (20)  NULL,
    [backcolor] VARCHAR (20)  NULL,
    [tag]       VARCHAR (20)  NULL
);




﻿using Cardinal.EasyApps.ED.Common.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;

namespace Cardinal.EasyApps.ED.Tests
{
    
    
    /// <summary>
    ///This is a test class for AppVersionTest and is intended
    ///to contain all AppVersionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AppVersionTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetAppVersion
        ///</summary>
        [TestMethod()]
        public void GetAppVersionTest()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

            var version = assembly.GetName().Version;

            string expected = string.Format(CultureInfo.InvariantCulture,"{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
            string actual;
            actual = AppVersion.GetAppVersion();
            Assert.AreEqual(expected, actual);
        }
    }
}

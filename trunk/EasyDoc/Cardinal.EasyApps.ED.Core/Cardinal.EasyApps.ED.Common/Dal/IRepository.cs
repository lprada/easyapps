﻿using System.Linq;
using Cardinal.EasyApps.ED.Model;
using System.Collections.Generic;

namespace Cardinal.EasyApps.ED.Common.Dal
{
    /// <summary>
    /// Interface para el Repositorio de ED.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets the DB version.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        string GetDBVersion();

        /// <summary>
        /// Gets the lotes por estado.
        /// </summary>
        /// <param name="estado">The estado.</param>
        /// <returns></returns>
        IQueryable<Lote> GetLotesPorEstado(EstadoLote estado);

        /// <summary>
        /// Gets the lotes A procesar Y transferir.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<Lote> GetLotesAProcesarYTransferir();

        /// <summary>
        /// Gets the lotes con error transferencia Y procesamiento.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<LoteConError> GetLotesConErrorTransferenciaYProcesamiento();

        /// <summary>
        /// Gets the lotes historial transferencia Y procesamiento.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<Lote> GetLotesHistorialTransferenciaYProcesamiento();

        /// <summary>
        /// Gets the imagenes lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <returns></returns>
        IQueryable<Imagen> GetImagenesLote(Lote lote);

        /// <summary>
        /// Gets the path red volumen.
        /// </summary>
        /// <param name="volumen">The volumen.</param>
        /// <returns></returns>
        string GetPathRedVolumen(string volumen);

        /// <summary>
        /// Gets the extension tipo.
        /// </summary>
        /// <param name="tipo">The tipo.</param>
        /// <returns></returns>
        string GetExtensionTipo(int tipo);

        /// <summary>
        /// Poners the en papelera.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="nroOrden">The nro orden.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "En")]
        void PonerEnPapelera(Lote lote, int nroOrden);

        /// <summary>
        /// Marcars the como sub lote.
        /// </summary>
        /// <param name="lote">The lote.</param>
        /// <param name="indice">The indice.</param>
        /// <param name="patchCode">The patch code.</param>
        /// <param name="eliminarImagenAnterior">if set to <c>true</c> [eliminar imagen anterior].</param>
        void MarcarComoSubLote(Lote lote, int indice, Model.PatchCodeValues patchCode, bool eliminarImagenAnterior);

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<Cliente> GetEmpresas();

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        IQueryable<Cliente> GetEmpresas(string loginToken);

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IQueryable<PlantillaVolumen> GetPlantillasVolumenes();


        ///// <summary>
        ///// Gets the volumenes validos.
        ///// </summary>
        ///// <param name="CodigoPlantilla">The codigo plantilla.</param>
        ///// <returns></returns>
        //IQueryable<Volumen> GetVolumenesValidos(string CodigoPlantilla);

        /// <summary>
        /// Gets the proximo nombre lote.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="prefijoLote">The prefijo lote.</param>
        /// <param name="codigoEstacionEscaneoImportacion">The codigo estacion escaneo importacion.</param>
        /// <param name="mascaraNumeroLote">The mascara numero lote.</param>
        /// <returns></returns>
        string GetProximoNombreLote(string path, string prefijoLote, string codigoEstacionEscaneoImportacion, string mascaraNumeroLote);

        /// <summary>
        /// Agregars the lote importacion.
        /// </summary>
        /// <param name="loteAImportar">The lote A importar.</param>
        void AgregarLoteImportacion(LoteImportar loteAImportar);

      /// <summary>
        /// Existes the lote.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <returns></returns>
        bool ExisteLote(string codLote);

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <returns></returns>
        bool ExisteCaja(string codCaja);

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        bool ExisteCaja(string codCaja, string codigoPlantilla);

        /// <summary>
        /// Determines whether [is login token valid] [the specified login token].
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns>
        ///   <c>true</c> if [is login token valid] [the specified login token]; otherwise, <c>false</c>.
        /// </returns>
        bool IsLoginTokenValid(string loginToken);

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillas();
        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillas(string codigoCliente);
        /// <summary>
        /// Get list of volumen names for the plantilla
        /// </summary>
        /// <param name="codigoPlantilla">the plantilla name</param>
        /// <returns>a list of the plantilla volumns</returns>
        List<string> GetTamplateVolumens(string codigoPlantilla);
        /// <summary>
        /// Gets the plantillas servicios.
        /// </summary>
        /// <returns></returns>
        IQueryable<PlantillaServicio> GetPlantillasServicios();
        /// <summary>
        /// Gets the plantilla campos.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        IQueryable<PlantillaCampo> GetPlantillaCampos(string codigoPlantilla);
        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillasWithLoginToken(string loginToken);
        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        IQueryable<Plantilla> GetPlantillas(string loginToken,string codigoCliente);


        


        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <returns></returns>
        IQueryable<TablaExterna> GetTablasExternas();

        /// <summary>
        /// Gets the historico by usuario.
        /// </summary>
        /// <param name="nroUsuario">The nro usuario.</param>
        /// <returns></returns>
        IQueryable<HitoricoCS> GetHistoricoByUsuario(string nroUsuario);

        /// <summary>
        /// Anulars the caratula cliente.
        /// </summary>
        /// <param name="id_caratula">The id_caratula.</param>
        /// <returns></returns>
        bool AnularCaratulaCliente(int id_caratula);

        /// <summary>
        /// Gets the campos by documento.
        /// </summary>
        /// <param name="nroDocumento">The nro documento.</param>
        /// <returns></returns>
        IQueryable<CamposCS> GetCamposByDocumento(decimal nroDocumento);


        /// <summary>
        /// Logs the operation caratulas.
        /// </summary>
        /// <param name="logDTO">The log DTO.</param>
        void LogOperationCaratulas(LogCaratula logDTO);


        /// <summary>
        /// Gets the caratula vista por id.
        /// </summary>
        /// <param name="nroCaratula">The nro caratula.</param>
        /// <returns></returns>
        CaratulaCS GetCaratulaVistaPorId(int nroCaratula);


        ///// <summary>
        ///// Gets the caratula completa.
        ///// </summary>
        ///// <returns></returns>
        //IQueryable<CaratulaCS> GetCaratulaCompleta();


        /// <summary>
        /// Gets the max document ID.
        /// </summary>
        /// <returns></returns>
        int GetMaxDocumentID();

        /// <summary>
        /// Saves the documento.
        /// </summary>
        /// <param name="documento">The documento.</param>
        /// <returns></returns>
        long saveDocumento(DocumentoEmpresa documento);


        /// <summary>
        /// Existes the key valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        bool ExisteKeyValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key);

        /// <summary>
        /// Agregars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        bool AgregarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string value);

        /// <summary>
        /// Editars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        bool EditarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string newValue);

        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulas();
        /// <summary>
        /// Gets the caratulas.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        IQueryable<CaratulaCS> GetCaratulas(string username);


        /// <summary>
        /// Realizars the inbound.
        /// </summary>
        /// <param name="numeroCaratula">The numero caratula.</param>
        void RealizarInbound(long numeroCaratula);

        /// <summary>
        /// Adds the cliente.
        /// </summary>
        /// <param name="cliente">The cliente.</param>
        /// <returns></returns>
        bool AddCliente(Cliente cliente);

        /// <summary>
        /// Dels the cliente.
        /// </summary>
        /// <param name="cliente">The cliente.</param>
        /// <returns></returns>
        bool DelCliente(Cliente cliente);

        /// <summary>
        /// Gets the diccionario datos.
        /// </summary>
        /// <returns></returns>
        List<CampoDiccionario> GetDiccionarioDatos();

        /// <summary>
        /// Adds the campo.
        /// </summary>
        /// <param name="campo">The campo.</param>
        /// <returns></returns>
        bool AddCampo(CampoDiccionario campo);


        /// <summary>
        /// Del the campo.
        /// </summary>
        /// <param name="campo">The campo.</param>
        /// <returns></returns>
        bool DelCampo(CampoDiccionario campo);

      

        /// <summary>
        /// Adds the plantilla.
        /// </summary>
        /// <param name="plantilla">The plantilla.</param>
        /// <returns></returns>
        PlantillaServicio AddPlantilla(PlantillaServicio plantilla);

        /// <summary>
        /// Dels the plantilla.
        /// </summary>
        /// <param name="plantilla">The plantilla.</param>
        /// <returns></returns>
        bool DelPlantilla(PlantillaServicio plantilla);

        /// <summary>
        /// Dels the fisico plantilla.
        /// </summary>
        /// <param name="plantilla">The plantilla.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        bool DelFisicoPlantilla(PlantillaServicio plantilla, string password);

        /// <summary>
        /// Existes the codigo plantilla.
        /// </summary>
        /// <param name="codigo">The codigo.</param>
        /// <returns></returns>
        bool ExisteCodigoPlantilla(string codigo);

        /// <summary>
        /// Gets the nuevo codigo plantila.
        /// </summary>
        /// <returns></returns>
        string GetNuevoCodigoPlantila();

        /// <summary>
        /// Dels the campo plantilla.
        /// </summary>
        /// <param name="plantilla">The plantilla.</param>
        /// <param name="codigoCampo">The codigo campo.</param>
        /// <returns></returns>
        bool DelCampoPlantilla(PlantillaServicio plantilla, string codigoCampo);


        /// <summary>
        /// add new record to volumestemplates
        /// </summary>
        /// <param name="tmplate">the new record tamplate name</param>
        /// <param name="volume">the new record volume</param>
        /// <returns></returns>
        bool AddVolumeTamplate(string tmplate, string volume);


       /// <summary>
        /// List of case mismach between the campo column in zonaand dicdatos tables
       /// </summary>
       /// <returns></returns>
        List<CampoCaseConflict> GetCampoCaseConflicts();
    }
}
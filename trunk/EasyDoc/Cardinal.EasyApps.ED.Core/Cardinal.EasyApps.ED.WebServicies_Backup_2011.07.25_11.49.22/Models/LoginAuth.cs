﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cardinal.EasyApps.ED.WebServicies.Models
{
    public class LoginAuth
    {
        public string Result { get; set; }
        public string Token { get; set; }
    }
}
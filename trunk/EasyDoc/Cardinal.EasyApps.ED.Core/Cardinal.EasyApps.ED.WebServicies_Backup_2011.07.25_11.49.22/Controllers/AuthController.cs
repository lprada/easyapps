﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cardinal.EasyApps.ED.WebServicies.Controllers
{
    public class AuthController : Controller
    {
        //
        // GET: /Auth/

        [HttpPost]
        public ActionResult GetLoginToken(string username, string password)
        {
            string token = LCP.ODO.BusinessObjects.Usuario.GetLoginToken(username,password,"ED");
            Cardinal.EasyApps.ED.WebServicies.Models.LoginAuth loginAuth = new Models.LoginAuth();
            if (!String.IsNullOrEmpty(token))
            {
                loginAuth.Result = "OK";
                loginAuth.Token = token;
            }
            else
            {
                loginAuth.Result = "FAIL";
                loginAuth.Token = string.Empty;
            }
            return new JsonResult
            {
                Data = loginAuth
            };
        }

    }
}

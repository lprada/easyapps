INSERT INTO TipoTag
           (Descripcion
           ,Icono)
     VALUES
           ('Revisado'
           ,'112_Tick_Green.ico')
GO

INSERT INTO TipoTag
           (Descripcion
           ,Icono)
     VALUES
           ('Bandera Roja'
           ,'1532_Flag_Red.ico')
GO

INSERT INTO TipoTag
           (Descripcion
           ,Icono)
     VALUES
           ('Bandera Azul'
           ,'1532_Flag_Blue.ico')
GO

INSERT INTO TipoTag
           (Descripcion
           ,Icono)
     VALUES
           ('Bandera Verde'
           ,'1532_Flag_Green.ico')
GO

INSERT INTO TipoTag
           (Descripcion
           ,Icono)
     VALUES
           ('Bandera Violeta'
           ,'1532_Flag_Purple.ico')
GO

INSERT INTO TipoTag
           (Descripcion
           ,Icono)
     VALUES
           ('Bandera Amarilla'
           ,'1532_Flag_Yellow.ico')
GO

UPDATE Version
   SET DBVersion = '100056'
 WHERE Product ='EasyDoc'
GO




update Version
set DBVersion = '100056'
where Product = 'EasyDoc'
GO

select * from Version
where Product = 'EasyDoc'
GO
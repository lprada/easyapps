/*
   Tuesday, July 22, 200811:41:29 AM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Entregas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Entregas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Entregas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.EntregasTmplates ADD CONSTRAINT
	FK_EntregasTmplates_Entregas FOREIGN KEY
	(
	cod_entrega
	) REFERENCES dbo.Entregas
	(
	cod_entrega
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.EntregasTmplates ADD CONSTRAINT
	FK_EntregasTmplates_tmplates FOREIGN KEY
	(
	tmplate
	) REFERENCES dbo.tmplates
	(
	tmplate
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EntregasTmplates', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EntregasTmplates', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EntregasTmplates', 'Object', 'CONTROL') as Contr_Per 
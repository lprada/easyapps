/*
   Thursday, October 29, 20095:20:50 PM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Documento SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Documento', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Documento', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Documento', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.DocumentoTag ADD CONSTRAINT
	FK_DocumentoTag_Documento FOREIGN KEY
	(
	DocumentoId
	) REFERENCES dbo.Documento
	(
	DocumentoId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DocumentoTag SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.DocumentoTag', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.DocumentoTag', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.DocumentoTag', 'Object', 'CONTROL') as Contr_Per 
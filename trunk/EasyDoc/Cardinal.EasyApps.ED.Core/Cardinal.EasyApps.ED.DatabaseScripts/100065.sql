/*
   Friday, November 20, 20091:47:18 PM
   User: sa
   Server: THEFORCE\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Tarea
	(
	TareaId uniqueidentifier NOT NULL,
	FechaAgregado datetime NOT NULL,
	Nombre nvarchar(100) NOT NULL,
	FechaInicio datetime NULL,
	Datos nvarchar(MAX) NOT NULL,
	Mensaje nvarchar(MAX) NULL,
	FechaEjecucion datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Tarea SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Tarea)
	 EXEC('INSERT INTO dbo.Tmp_Tarea (TareaId, FechaAgregado, Nombre, FechaInicio, Datos, Mensaje, FechaEjecucion)
		SELECT TareaId, CONVERT(datetime, FechaAgregado), Nombre, FechaInicio, Datos, Mensaje, FechaEjecucion FROM dbo.Tarea WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Tarea
GO
EXECUTE sp_rename N'dbo.Tmp_Tarea', N'Tarea', 'OBJECT' 
GO
ALTER TABLE dbo.Tarea ADD CONSTRAINT
	PK_Tarea PRIMARY KEY CLUSTERED 
	(
	TareaId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Tarea', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Tarea', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Tarea', 'Object', 'CONTROL') as Contr_Per 
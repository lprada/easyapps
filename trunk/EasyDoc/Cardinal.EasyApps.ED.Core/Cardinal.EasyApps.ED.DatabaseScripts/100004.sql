/*
   Friday, July 11, 20085:33:29 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.SecuenciaLotes
	(
	SecuenciaLotesId uniqueidentifier NOT NULL,
	LoteInicioSecuencia varchar(50) NOT NULL,
	LoteFinSecuencia varchar(50) NULL,
	FechaInicio datetime NOT NULL,
	FechaFin datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.SecuenciaLotes ADD CONSTRAINT
	DF_SecuenciaLotes_FechaInicio DEFAULT getdate() FOR FechaInicio
GO
ALTER TABLE dbo.SecuenciaLotes ADD CONSTRAINT
	PK_SecuenciaLotes PRIMARY KEY CLUSTERED 
	(
	SecuenciaLotesId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'CONTROL') as Contr_Per 
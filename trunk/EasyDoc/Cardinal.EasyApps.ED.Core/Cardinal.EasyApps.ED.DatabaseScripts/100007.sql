/*
   Friday, July 11, 20085:41:43 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.SecuenciaLotesDetalle ADD CONSTRAINT
	FK_SecuenciaLotesDetalle_SecuenciaLotes FOREIGN KEY
	(
	SecuenciaLotesId
	) REFERENCES dbo.SecuenciaLotes
	(
	SecuenciaLotesId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.SecuenciaLotesDetalle', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotesDetalle', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.SecuenciaLotesDetalle', 'Object', 'CONTROL') as Contr_Per 
/*
   Tuesday, September 15, 200911:36:42 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MLObjects_T
	(
	id varchar(250) NOT NULL,
	MLMessage_ID int NOT NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.MLObjects_T)
	 EXEC('INSERT INTO dbo.Tmp_MLObjects_T (id, MLMessage_ID)
		SELECT id, CONVERT(int, MLMessage_ID) FROM dbo.MLObjects_T WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.MLObjects_T
GO
EXECUTE sp_rename N'dbo.Tmp_MLObjects_T', N'MLObjects_T', 'OBJECT' 
GO
ALTER TABLE dbo.MLObjects_T ADD CONSTRAINT
	pk_MLObjects_T PRIMARY KEY CLUSTERED 
	(
	id,
	MLMessage_ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.MLObjects_T', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.MLObjects_T', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.MLObjects_T', 'Object', 'CONTROL') as Contr_Per 
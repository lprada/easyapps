/*
This script was created by Visual Studio on 7/11/2008 at 12:04 PM.
Run this script on anakin\sqlexpress.EasyDoc.Database.dbo to make it the same as anakin\sqlexpress.EasyDocNetDatabaseEmpty.dbo.
Please back up your target database before running this script.
*/
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[dicdatos]'
GO
CREATE TABLE [dbo].[dicdatos]
(
[campo] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descripcio] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tipo] [smallint] NOT NULL,
[longitud] [smallint] NULL,
[decimales] [smallint] NULL,
[rango_min] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[rango_max] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[mascara] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[interno] [smallint] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_dicdatos] on [dbo].[dicdatos]'
GO
ALTER TABLE [dbo].[dicdatos] ADD CONSTRAINT [pk_dicdatos] PRIMARY KEY CLUSTERED  ([campo]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[zona]'
GO
CREATE TABLE [dbo].[zona]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[campo] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[orden] [smallint] NOT NULL,
[tp] [int] NULL,
[lft] [int] NULL,
[bttm] [int] NULL,
[rght] [int] NULL,
[mndtory] [smallint] NULL,
[ocr1] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ocr2] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ocr3] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ocr4] [smallint] NULL,
[ocr5] [smallint] NOT NULL,
[reftab] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[campoext] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[datasrc] [smallint] NULL,
[visual] [varchar] (5) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[filtrotabext] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Empresas]'
GO
CREATE TABLE [dbo].[Empresas]
(
[LoginEmpresa] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[DescEmpresa] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[NroCli] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[CUIT] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[PerInicio] [datetime] NULL,
[PerDuracion] [int] NULL,
[DomCalle] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[DomNumero] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[DomPiso] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[DomLocalidad] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[DomProvincia] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[DomCP] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConApellido] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConNombre] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConCargo] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConTelefono] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConInterno] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConFax] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ConEmail] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Estado] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_Empresas] on [dbo].[Empresas]'
GO
ALTER TABLE [dbo].[Empresas] ADD CONSTRAINT [pk_Empresas] PRIMARY KEY CLUSTERED  ([LoginEmpresa]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[grpyus]'
GO
CREATE TABLE [dbo].[grpyus]
(
[codigo] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tipo] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[descripcio] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[aplicacion] [varchar] (40) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[login] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[clave] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[LoginEmpresa] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Apellido] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Nombre] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Cargo] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Telefono] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Interno] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Fax] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[CodSucursal] [int] NULL,
[TipoConexion] [int] NULL,
[Estado] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[CambiarClave] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_grpyus] on [dbo].[grpyus]'
GO
ALTER TABLE [dbo].[grpyus] ADD CONSTRAINT [pk_grpyus] PRIMARY KEY CLUSTERED  ([codigo]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[user_T]'
GO
CREATE TABLE [dbo].[user_T]
(
[user_id] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nameComplete] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[changePassword] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[notAllowChangePassword] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[bloq] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[del] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[password] [char] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[networkAlias] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[fechalogin] [datetime] NULL,
[fechapassword] [datetime] NULL,
[reintetoslogin] [int] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_user_T] on [dbo].[user_T]'
GO
ALTER TABLE [dbo].[user_T] ADD CONSTRAINT [pk_user_T] PRIMARY KEY CLUSTERED  ([user_id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[membership_user_role_t]'
GO
CREATE TABLE [dbo].[membership_user_role_t]
(
[user_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[role_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[obj] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_membership_user_role_t] on [dbo].[membership_user_role_t]'
GO
ALTER TABLE [dbo].[membership_user_role_t] ADD CONSTRAINT [pk_membership_user_role_t] PRIMARY KEY CLUSTERED  ([role_id_fk], [user_id_fk], [obj]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[task_T]'
GO
CREATE TABLE [dbo].[task_T]
(
[task_id] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[lastNumber_bi] [bigint] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_task_T] on [dbo].[task_T]'
GO
ALTER TABLE [dbo].[task_T] ADD CONSTRAINT [pk_task_T] PRIMARY KEY CLUSTERED  ([task_id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[audit_T]'
GO
CREATE TABLE [dbo].[audit_T]
(
[fecha_dt] [datetime] NOT NULL,
[user_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[task_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[task_number_bi] [bigint] NOT NULL,
[observ_vc] [varchar] (250) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_audit_T] on [dbo].[audit_T]'
GO
ALTER TABLE [dbo].[audit_T] ADD CONSTRAINT [pk_audit_T] PRIMARY KEY CLUSTERED  ([fecha_dt], [user_id_fk], [task_id_fk], [task_number_bi]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[role_task_T]'
GO
CREATE TABLE [dbo].[role_task_T]
(
[role_task_id] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[lastNumber_bi] [bigint] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_role_task_T] on [dbo].[role_task_T]'
GO
ALTER TABLE [dbo].[role_task_T] ADD CONSTRAINT [pk_role_task_T] PRIMARY KEY CLUSTERED  ([role_task_id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[permissions_roles_T]'
GO
CREATE TABLE [dbo].[permissions_roles_T]
(
[role_task_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[role_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_permissions_roles_T] on [dbo].[permissions_roles_T]'
GO
ALTER TABLE [dbo].[permissions_roles_T] ADD CONSTRAINT [pk_permissions_roles_T] PRIMARY KEY CLUSTERED  ([role_task_id_fk], [role_id_fk]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[role_t]'
GO
CREATE TABLE [dbo].[role_t]
(
[role_id] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_role_t] on [dbo].[role_t]'
GO
ALTER TABLE [dbo].[role_t] ADD CONSTRAINT [pk_role_t] PRIMARY KEY CLUSTERED  ([role_id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[group_T]'
GO
CREATE TABLE [dbo].[group_T]
(
[group_id] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_group_T] on [dbo].[group_T]'
GO
ALTER TABLE [dbo].[group_T] ADD CONSTRAINT [pk_group_T] PRIMARY KEY CLUSTERED  ([group_id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[membership_T]'
GO
CREATE TABLE [dbo].[membership_T]
(
[user_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[group_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_membership_T] on [dbo].[membership_T]'
GO
ALTER TABLE [dbo].[membership_T] ADD CONSTRAINT [pk_membership_T] PRIMARY KEY CLUSTERED  ([user_id_fk], [group_id_fk]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[membership_group_role_t]'
GO
CREATE TABLE [dbo].[membership_group_role_t]
(
[role_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[group_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[obj] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_membership_group_role_t] on [dbo].[membership_group_role_t]'
GO
ALTER TABLE [dbo].[membership_group_role_t] ADD CONSTRAINT [pk_membership_group_role_t] PRIMARY KEY CLUSTERED  ([role_id_fk], [group_id_fk], [obj]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LotesCajas]'
GO
CREATE TABLE [dbo].[LotesCajas]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[cod_caja] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MLLanguages_T]'
GO
CREATE TABLE [dbo].[MLLanguages_T]
(
[name] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[id] [char] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[culture] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_MLLanguages_T] on [dbo].[MLLanguages_T]'
GO
ALTER TABLE [dbo].[MLLanguages_T] ADD CONSTRAINT [pk_MLLanguages_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MLMessages_T]'
GO
CREATE TABLE [dbo].[MLMessages_T]
(
[id] [char] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[MLLanguage_ID] [char] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[text] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_MLMessages_T] on [dbo].[MLMessages_T]'
GO
ALTER TABLE [dbo].[MLMessages_T] ADD CONSTRAINT [pk_MLMessages_T] PRIMARY KEY CLUSTERED  ([id], [MLLanguage_ID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[permisos]'
GO
CREATE TABLE [dbo].[permisos]
(
[codigo] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[permisos] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[vols]'
GO
CREATE TABLE [dbo].[vols]
(
[volumen] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[coddev] [smallint] NOT NULL,
[finiciovalidez] [datetime] NULL,
[ffinvalidez] [datetime] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_vols] on [dbo].[vols]'
GO
ALTER TABLE [dbo].[vols] ADD CONSTRAINT [pk_vols] PRIMARY KEY CLUSTERED  ([volumen]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[montaje]'
GO
CREATE TABLE [dbo].[montaje]
(
[pt] [smallint] NOT NULL,
[volumen] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[mount] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[acceso] [smallint] NOT NULL,
[host] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_montaje] on [dbo].[montaje]'
GO
ALTER TABLE [dbo].[montaje] ADD CONSTRAINT [pk_montaje] PRIMARY KEY CLUSTERED  ([volumen]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Entregas]'
GO
CREATE TABLE [dbo].[Entregas]
(
[cod_entrega] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[LoginEmpresa] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[nro_entrega] [int] NULL,
[Fecha] [datetime] NULL,
[Estado] [int] NULL,
[CantImagenes] [int] NULL,
[CantDocumentos] [int] NULL,
[CantCDs] [int] NULL,
[FechaBajada] [datetime] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_Entregas] on [dbo].[Entregas]'
GO
ALTER TABLE [dbo].[Entregas] ADD CONSTRAINT [pk_Entregas] PRIMARY KEY CLUSTERED  ([cod_entrega]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CajasEntregas]'
GO
CREATE TABLE [dbo].[CajasEntregas]
(
[cod_caja] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[cod_entrega] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_auditoriaDocumentos_T]'
GO
CREATE TABLE [dbo].[LD_auditoriaDocumentos_T]
(
[fecha] [datetime] NULL,
[caja] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[doc] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[estOriCaja] [int] NULL,
[estDestCaja] [int] NULL,
[estOriDoc] [int] NULL,
[estDestDoc] [int] NULL,
[oper] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[numoper] [bigint] NULL,
[comen] [varchar] (400) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[estOriCajaDef] [int] NULL,
[estDestCajaDef] [int] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[RecoleccionDatosCentros]'
GO
CREATE TABLE [dbo].[RecoleccionDatosCentros]
(
[fecha] [datetime] NOT NULL,
[tabla] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[sentenciaSQL] [varchar] (4000) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[estado] [int] NOT NULL CONSTRAINT [DF__Recolecci__estad__17F790F9] DEFAULT (0)
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Cajas]'
GO
CREATE TABLE [dbo].[Cajas]
(
[cod_caja] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[LoginEmpresa] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Fecha] [datetime] NULL,
[Estado] [int] NULL,
[FRecepcion] [datetime] NULL,
[FProceso] [datetime] NULL,
[FTerminacion] [datetime] NULL,
[FEntrega] [datetime] NULL,
[FRecepDatos] [datetime] NULL,
[FCaptura] [datetime] NULL,
[estadoLogico] [int] NULL,
[tmplate] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Cajas] on [dbo].[Cajas]'
GO
ALTER TABLE [dbo].[Cajas] ADD CONSTRAINT [PK_Cajas] PRIMARY KEY CLUSTERED  ([cod_caja]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[permissions_groups_T]'
GO
CREATE TABLE [dbo].[permissions_groups_T]
(
[task_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[group_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_permissions_groups_T] on [dbo].[permissions_groups_T]'
GO
ALTER TABLE [dbo].[permissions_groups_T] ADD CONSTRAINT [pk_permissions_groups_T] PRIMARY KEY CLUSTERED  ([task_id_fk], [group_id_fk]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[busqueda_header_T]'
GO
CREATE TABLE [dbo].[busqueda_header_T]
(
[id] [int] NOT NULL,
[nombre] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[fecha_creacion] [datetime] NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_busqueda_header_T] on [dbo].[busqueda_header_T]'
GO
ALTER TABLE [dbo].[busqueda_header_T] ADD CONSTRAINT [pk_busqueda_header_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[busqueda_body_T]'
GO
CREATE TABLE [dbo].[busqueda_body_T]
(
[id] [int] NOT NULL,
[abro_parentesis] [char] (5) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[campo] [char] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[valor] [varchar] (250) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[operador] [char] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cierro_parentesis] [char] (5) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[operador_row] [char] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_busqueda_body_T] on [dbo].[busqueda_body_T]'
GO
ALTER TABLE [dbo].[busqueda_body_T] ADD CONSTRAINT [pk_busqueda_body_T] PRIMARY KEY CLUSTERED  ([id], [abro_parentesis], [campo], [valor], [operador], [cierro_parentesis], [operador_row]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VersionDetalles]'
GO
CREATE TABLE [dbo].[VersionDetalles]
(
[VersionDetallesId] [uniqueidentifier] NOT NULL,
[Product] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DBVersion] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Fecha] [datetime] NOT NULL CONSTRAINT [DF_VersionDetalles_Fecha] DEFAULT (getdate()),
[Usuario] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScriptName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Script] [image] NULL,
[Modificacion] [timestamp] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VersionDetalles] on [dbo].[VersionDetalles]'
GO
ALTER TABLE [dbo].[VersionDetalles] ADD CONSTRAINT [PK_VersionDetalles] PRIMARY KEY CLUSTERED  ([VersionDetallesId]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[permissions_users_T]'
GO
CREATE TABLE [dbo].[permissions_users_T]
(
[task_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[user_id_fk] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_permissions_users_T] on [dbo].[permissions_users_T]'
GO
ALTER TABLE [dbo].[permissions_users_T] ADD CONSTRAINT [pk_permissions_users_T] PRIMARY KEY CLUSTERED  ([task_id_fk], [user_id_fk]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[anotacion]'
GO
CREATE TABLE [dbo].[anotacion]
(
[volumen] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[codlote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nroorden] [smallint] NOT NULL,
[tipo] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[txt] [varchar] (1024) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[tp] [int] NOT NULL,
[lft] [int] NOT NULL,
[hght] [int] NOT NULL,
[wdth] [int] NOT NULL,
[backcolor] [varchar] (12) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[tag] [int] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_anotacion] on [dbo].[anotacion]'
GO
ALTER TABLE [dbo].[anotacion] ADD CONSTRAINT [pk_anotacion] PRIMARY KEY CLUSTERED  ([codlote], [nroorden]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ProcImg_T]'
GO
CREATE TABLE [dbo].[ProcImg_T]
(
[id] [int] NOT NULL,
[Descr] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[code] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_ProcImg_T] on [dbo].[ProcImg_T]'
GO
ALTER TABLE [dbo].[ProcImg_T] ADD CONSTRAINT [pk_ProcImg_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ProcImg_Tmplate_T]'
GO
CREATE TABLE [dbo].[ProcImg_Tmplate_T]
(
[procimg_id] [int] NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[orden] [int] NOT NULL,
[cant] [int] NULL,
[param1] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[param2] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[param3] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_ProcImg_Tmplate_T] on [dbo].[ProcImg_Tmplate_T]'
GO
ALTER TABLE [dbo].[ProcImg_Tmplate_T] ADD CONSTRAINT [pk_ProcImg_Tmplate_T] PRIMARY KEY CLUSTERED  ([procimg_id], [tmplate]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[papelera]'
GO
CREATE TABLE [dbo].[papelera]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[FechaHora] [datetime] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lotes]'
GO
CREATE TABLE [dbo].[lotes]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[volumen] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[rotada] [smallint] NULL,
[reparada] [smallint] NULL,
[tipo] [int] NOT NULL,
[fecscan] [smalldatetime] NOT NULL,
[ocr] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[indice] [int] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[device]'
GO
CREATE TABLE [dbo].[device]
(
[coddev] [smallint] NOT NULL,
[descrip] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tipo] [smallint] NOT NULL,
[libre] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_device] on [dbo].[device]'
GO
ALTER TABLE [dbo].[device] ADD CONSTRAINT [pk_device] PRIMARY KEY CLUSTERED  ([coddev]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubLotes]'
GO
CREATE TABLE [dbo].[SubLotes]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[indice] [int] NOT NULL,
[nivel] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[usxgr]'
GO
CREATE TABLE [dbo].[usxgr]
(
[cod_grupo] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_us] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_Documentos_HistoricoCB]'
GO
CREATE TABLE [dbo].[LD_Documentos_HistoricoCB]
(
[legajo] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[legajoAnter] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nombres] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[apellido] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[apenom] [varchar] (500) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL CONSTRAINT [DF__LD_Docume__apeno__10566F31] DEFAULT (''),
[separar] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL CONSTRAINT [DF__LD_Docume__separ__114A936A] DEFAULT ('N')
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tmplates]'
GO
CREATE TABLE [dbo].[tmplates]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descripcio] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tipoindex] [smallint] NOT NULL,
[periodica] [smallint] NOT NULL,
[nro_zonas] [smallint] NOT NULL,
[tipo] [varchar] (2) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[parent] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[datasrc] [int] NOT NULL,
[ocr] [smallint] NOT NULL,
[produccion] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[LoginEmpresa] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[BillingType] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[usaMergeDatos] [int] NULL,
[diasprocesoWarning] [int] NULL CONSTRAINT [DF__tmplates__diaspr__5D60DB10] DEFAULT (0),
[diasprocesoError] [int] NULL CONSTRAINT [DF__tmplates__diaspr__5E54FF49] DEFAULT (0),
[firmadigi] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL CONSTRAINT [DF_tmplates_firmadigi] DEFAULT ('N'),
[SeteosCaptura] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[FormatoCapturaBN] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[FormatoCapturaGrey] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[FormatoCapturaColor] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[orderImagenes] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL CONSTRAINT [DF_tmplates_orderImagenes] DEFAULT ('A'),
[cajasMensuales] [int] NULL CONSTRAINT [DF__tmplates__cajasM__047AA831] DEFAULT (0)
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_tmplates] on [dbo].[tmplates]'
GO
ALTER TABLE [dbo].[tmplates] ADD CONSTRAINT [pk_tmplates] PRIMARY KEY CLUSTERED  ([tmplate]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[idlotes]'
GO
CREATE TABLE [dbo].[idlotes]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[act_nro] [smallint] NULL,
[ult_nro] [smallint] NULL,
[indexando] [smallint] NULL,
[caja] [int] NULL,
[feccreacion] [datetime] NULL CONSTRAINT [idlotesfeccracion] DEFAULT (getdate()),
[scanner] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[controlrealizado] [smallint] NULL CONSTRAINT [DF_idlotes_controlrealizado] DEFAULT (0)
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__idlotes__6E01572D] on [dbo].[idlotes]'
GO
ALTER TABLE [dbo].[idlotes] ADD CONSTRAINT [PK__idlotes__6E01572D] PRIMARY KEY CLUSTERED  ([cod_lote]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetIdCentroDigitalizacion]'
GO
-- =============================================
-- Create scalar function
-- =============================================

CREATE FUNCTION [dbo].GetIdCentroDigitalizacion	()
RETURNS char(10)
AS
BEGIN
	RETURN 'LUG'
END









GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[valorNumerico]'
GO
create function valorNumerico(@cadena varchar(50))
returns int
as
begin
  declare @cadaux varchar(50)
  declare @i int
  set @i = 1
  set @cadaux = ''
  while (@i <= len(@cadena))
  begin
    if (isnumeric(substring(@cadena,@i,1)) = 1) and (substring(@cadena,@i,1) <> '-') and (substring(@cadena,@i,1) <> '+')
    begin
      set @cadaux = @cadaux + substring(@cadena,@i,1)
    end
  set @i = @i + 1
  end
  return cast(@cadaux as int)
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_arch]'
GO
CREATE TABLE [dbo].[aud_arch]
(
[pt] [smallint] NOT NULL,
[vol_ini] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[vol_fin] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_us] [int] NOT NULL,
[fecha] [smalldatetime] NULL,
[inicio] [smalldatetime] NOT NULL,
[tiempo] [smalldatetime] NOT NULL,
[total] [smallint] NOT NULL,
[totalOK] [smallint] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_capt]'
GO
CREATE TABLE [dbo].[aud_capt]
(
[cod_lote] [nvarchar] (16) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_us] [nvarchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha] [smalldatetime] NOT NULL,
[inicio] [smalldatetime] NOT NULL,
[tiempo] [smalldatetime] NOT NULL,
[total_im] [smallint] NOT NULL,
[tipocap] [nvarchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[scanner] [nvarchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_clas]'
GO
CREATE TABLE [dbo].[aud_clas]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_us] [int] NOT NULL,
[fecha] [smalldatetime] NULL,
[inicio] [smalldatetime] NOT NULL,
[tiempo] [smalldatetime] NOT NULL,
[totalim] [smallint] NOT NULL,
[clasif] [smallint] NOT NULL,
[deleted] [smallint] NOT NULL,
[depurar] [smallint] NOT NULL,
[caracter] [int] NOT NULL,
[documentos] [smallint] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_cons]'
GO
CREATE TABLE [dbo].[aud_cons]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_us] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha] [smalldatetime] NOT NULL,
[hora] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[orden] [smallint] NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[identDoc] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_logis]'
GO
CREATE TABLE [dbo].[aud_logis]
(
[cod_us] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha] [smalldatetime] NOT NULL,
[hora] [smalldatetime] NOT NULL,
[tipo_obj] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_obj] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[operacion] [int] NOT NULL,
[comentario] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_preppapel]'
GO
CREATE TABLE [dbo].[aud_preppapel]
(
[user_id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha] [datetime] NOT NULL,
[cantCajas] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_aud_preppapel] on [dbo].[aud_preppapel]'
GO
ALTER TABLE [dbo].[aud_preppapel] ADD CONSTRAINT [PK_aud_preppapel] PRIMARY KEY CLUSTERED  ([user_id], [fecha]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[aud_seg]'
GO
CREATE TABLE [dbo].[aud_seg]
(
[cod_us] [int] NOT NULL,
[fecha] [smalldatetime] NOT NULL,
[hora] [smalldatetime] NOT NULL,
[tipo_obj] [varchar] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_obj] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[operacion] [int] NOT NULL,
[comentario] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AuditoriaGraboVerificacion]'
GO
CREATE TABLE [dbo].[AuditoriaGraboVerificacion]
(
[cod_us] [int] NOT NULL,
[fecha] [datetime] NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[campo] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[valor1] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[valor2] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[eleccion] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CajasDefinitivas]'
GO
CREATE TABLE [dbo].[CajasDefinitivas]
(
[cod_caja] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[LoginEmpresa] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[Fecha] [datetime] NULL,
[Estado] [int] NULL,
[FechaDatosMerge] [datetime] NULL,
[CantSobrantes] [int] NULL,
[CantFaltantes] [int] NULL,
[FechaInicioProceso] [datetime] NULL,
[FechaDevolucion] [datetime] NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[datasrc]'
GO
CREATE TABLE [dbo].[datasrc]
(
[codigo] [int] NOT NULL,
[datasrc] [varchar] (254) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[usuario] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[password] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[usetxret] [smallint] NOT NULL,
[pgrant] [smallint] NOT NULL,
[porderby] [smallint] NOT NULL,
[pindex] [smallint] NOT NULL,
[typedep] [varchar] (60) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[dstype] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[fmtFechas] [varchar] (40) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[dstypename] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_datasrc] on [dbo].[datasrc]'
GO
ALTER TABLE [dbo].[datasrc] ADD CONSTRAINT [pk_datasrc] PRIMARY KEY CLUSTERED  ([codigo]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[EntregasTmplates]'
GO
CREATE TABLE [dbo].[EntregasTmplates]
(
[cod_entrega] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[CantImagenes] [int] NULL,
[CantDocumentos] [int] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ErroresControl]'
GO
CREATE TABLE [dbo].[ErroresControl]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[cod_caja] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[estadosCajas_t]'
GO
CREATE TABLE [dbo].[estadosCajas_t]
(
[id] [int] NOT NULL,
[descr] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_estadosCajas_t] on [dbo].[estadosCajas_t]'
GO
ALTER TABLE [dbo].[estadosCajas_t] ADD CONSTRAINT [pk_estadosCajas_t] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[estadosEntregas_T]'
GO
CREATE TABLE [dbo].[estadosEntregas_T]
(
[id] [int] NOT NULL,
[descr] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_estadosEntregas_T] on [dbo].[estadosEntregas_T]'
GO
ALTER TABLE [dbo].[estadosEntregas_T] ADD CONSTRAINT [pk_estadosEntregas_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[filtros]'
GO
CREATE TABLE [dbo].[filtros]
(
[filtro] [int] NOT NULL,
[fstring] [varchar] (4) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descripcio] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_filtros] on [dbo].[filtros]'
GO
ALTER TABLE [dbo].[filtros] ADD CONSTRAINT [pk_filtros] PRIMARY KEY CLUSTERED  ([filtro]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[hipervinculos_T]'
GO
CREATE TABLE [dbo].[hipervinculos_T]
(
[templateOriginal] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[campoOriginal] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[templateDestino] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[campoDestino] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[orden] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_hipervinculos_T] on [dbo].[hipervinculos_T]'
GO
ALTER TABLE [dbo].[hipervinculos_T] ADD CONSTRAINT [pk_hipervinculos_T] PRIMARY KEY CLUSTERED  ([templateOriginal], [campoOriginal], [templateDestino], [campoDestino], [orden]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[hosts]'
GO
CREATE TABLE [dbo].[hosts]
(
[host] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[password] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_hosts] on [dbo].[hosts]'
GO
ALTER TABLE [dbo].[hosts] ADD CONSTRAINT [pk_hosts] PRIMARY KEY CLUSTERED  ([host]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[indicadores_T]'
GO
CREATE TABLE [dbo].[indicadores_T]
(
[indicador] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[etiqueta] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[polaridad] [char] (2) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[verde] [bigint] NULL,
[amarillo] [bigint] NULL,
[rojo] [bigint] NULL,
[leyverde] [varchar] (250) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[leyamarillo] [varchar] (250) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[leyrojo] [varchar] (250) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[spCant] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[spPorc] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_indicadores_T] on [dbo].[indicadores_T]'
GO
ALTER TABLE [dbo].[indicadores_T] ADD CONSTRAINT [pk_indicadores_T] PRIMARY KEY CLUSTERED  ([indicador]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_cajas_definitivas_T]'
GO
CREATE TABLE [dbo].[LD_cajas_definitivas_T]
(
[cod_caja] [varchar] (15) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha_alta] [datetime] NOT NULL,
[estado] [tinyint] NULL,
[numRem] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[grupo] [char] (3) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_cajas_definitivas_T] on [dbo].[LD_cajas_definitivas_T]'
GO
ALTER TABLE [dbo].[LD_cajas_definitivas_T] ADD CONSTRAINT [pk_LD_cajas_definitivas_T] PRIMARY KEY CLUSTERED  ([cod_caja]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_cajas_T]'
GO
CREATE TABLE [dbo].[LD_cajas_T]
(
[cod_caja] [varchar] (15) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha_alta] [datetime] NOT NULL,
[estado] [tinyint] NULL,
[numRem] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[grupo] [char] (3) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_cajas_T] on [dbo].[LD_cajas_T]'
GO
ALTER TABLE [dbo].[LD_cajas_T] ADD CONSTRAINT [pk_LD_cajas_T] PRIMARY KEY CLUSTERED  ([cod_caja]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_controlDocumentos_T]'
GO
CREATE TABLE [dbo].[LD_controlDocumentos_T]
(
[id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_controlDocumentos_T] on [dbo].[LD_controlDocumentos_T]'
GO
ALTER TABLE [dbo].[LD_controlDocumentos_T] ADD CONSTRAINT [pk_LD_controlDocumentos_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_documentos_T]'
GO
CREATE TABLE [dbo].[LD_documentos_T]
(
[id_documento] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fecha_alta] [datetime] NULL,
[fecha_ultimo_ingreso] [datetime] NULL,
[estado] [tinyint] NULL,
[fecha_reconocimiento] [datetime] NULL,
[fecha_control_qa] [datetime] NULL,
[fecha_devolucion] [datetime] NULL,
[caja_proceso] [varchar] (15) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[caja_devolucion] [varchar] (15) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[remitoExtraccion] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LD_documentos_T] on [dbo].[LD_documentos_T]'
GO
ALTER TABLE [dbo].[LD_documentos_T] ADD CONSTRAINT [PK_LD_documentos_T] PRIMARY KEY CLUSTERED  ([id_documento], [caja_proceso]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_estado_documentos_T]'
GO
CREATE TABLE [dbo].[LD_estado_documentos_T]
(
[id] [int] NOT NULL,
[descr] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_estado_documentos_T] on [dbo].[LD_estado_documentos_T]'
GO
ALTER TABLE [dbo].[LD_estado_documentos_T] ADD CONSTRAINT [pk_LD_estado_documentos_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_estados_cajas_definitivas_T]'
GO
CREATE TABLE [dbo].[LD_estados_cajas_definitivas_T]
(
[id] [int] NOT NULL,
[descr] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_estados_cajas_definitivas_T] on [dbo].[LD_estados_cajas_definitivas_T]'
GO
ALTER TABLE [dbo].[LD_estados_cajas_definitivas_T] ADD CONSTRAINT [pk_LD_estados_cajas_definitivas_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_estados_cajas_T]'
GO
CREATE TABLE [dbo].[LD_estados_cajas_T]
(
[id] [int] NOT NULL,
[descr] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_estados_cajas_T] on [dbo].[LD_estados_cajas_T]'
GO
ALTER TABLE [dbo].[LD_estados_cajas_T] ADD CONSTRAINT [pk_LD_estados_cajas_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LD_operaciones_T]'
GO
CREATE TABLE [dbo].[LD_operaciones_T]
(
[id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_LD_operaciones_T] on [dbo].[LD_operaciones_T]'
GO
ALTER TABLE [dbo].[LD_operaciones_T] ADD CONSTRAINT [pk_LD_operaciones_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[licencias]'
GO
CREATE TABLE [dbo].[licencias]
(
[texto] [varchar] (1000) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[modif] [timestamp] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lookup]'
GO
CREATE TABLE [dbo].[lookup]
(
[reftab] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[datasrc] [int] NULL,
[campoext] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LotesCajasDefinitivas]'
GO
CREATE TABLE [dbo].[LotesCajasDefinitivas]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[cod_caja] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[marcasrescanning_T]'
GO
CREATE TABLE [dbo].[marcasrescanning_T]
(
[id] [tinyint] NOT NULL,
[descr] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_marcasrescanning_T] on [dbo].[marcasrescanning_T]'
GO
ALTER TABLE [dbo].[marcasrescanning_T] ADD CONSTRAINT [pk_marcasrescanning_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MLObjects_T]'
GO
CREATE TABLE [dbo].[MLObjects_T]
(
[id] [varchar] (250) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[MLMessage_ID] [char] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_MLObjects_T] on [dbo].[MLObjects_T]'
GO
ALTER TABLE [dbo].[MLObjects_T] ADD CONSTRAINT [pk_MLObjects_T] PRIMARY KEY CLUSTERED  ([id], [MLMessage_ID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[notas]'
GO
CREATE TABLE [dbo].[notas]
(
[cod_lote] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[tipo] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[txt] [varchar] (200) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[tp] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[lft] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[hght] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[wdth] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[backcolor] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[tag] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_notas] on [dbo].[notas]'
GO
ALTER TABLE [dbo].[notas] ADD CONSTRAINT [pk_notas] PRIMARY KEY CLUSTERED  ([cod_lote], [nro_orden]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[parameters]'
GO
CREATE TABLE [dbo].[parameters]
(
[guid] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[description] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[type] [tinyint] NOT NULL,
[value] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[validateexpression] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_parameters] on [dbo].[parameters]'
GO
ALTER TABLE [dbo].[parameters] ADD CONSTRAINT [PK_parameters] PRIMARY KEY CLUSTERED  ([guid]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ProcesoEntrega_T]'
GO
CREATE TABLE [dbo].[ProcesoEntrega_T]
(
[cod_entrega] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[lotesProcesados] [int] NULL,
[registrosProcesados] [int] NULL,
[cajaActual] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[loteActual] [varchar] (16) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[operacion] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[estado] [int] NULL,
[cdActual] [int] NULL,
[tamano] [bigint] NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_ProcesoEntrega_T] on [dbo].[ProcesoEntrega_T]'
GO
ALTER TABLE [dbo].[ProcesoEntrega_T] ADD CONSTRAINT [pk_ProcesoEntrega_T] PRIMARY KEY CLUSTERED  ([cod_entrega]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ProcesoProcImg_T]'
GO
CREATE TABLE [dbo].[ProcesoProcImg_T]
(
[cod_lote] [varchar] (30) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[registrosProcesados] [int] NULL,
[operacion] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[estado] [int] NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_ProcesoProcImg_T] on [dbo].[ProcesoProcImg_T]'
GO
ALTER TABLE [dbo].[ProcesoProcImg_T] ADD CONSTRAINT [pk_ProcesoProcImg_T] PRIMARY KEY CLUSTERED  ([cod_lote]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[recepcionCajas]'
GO
CREATE TABLE [dbo].[recepcionCajas]
(
[id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[usuario] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[loginempresa] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[estado] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[motivo] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fechaCaptura] [datetime] NOT NULL,
[cod_recp] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[recepcionCajasDetalle]'
GO
CREATE TABLE [dbo].[recepcionCajasDetalle]
(
[caja] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[idEntrega] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[fechaCaptura] [datetime] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[RegistrosCopiados]'
GO
CREATE TABLE [dbo].[RegistrosCopiados]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[nro_orden_dest] [smallint] NOT NULL,
[fechahora] [datetime] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[rescanning_T]'
GO
CREATE TABLE [dbo].[rescanning_T]
(
[cod_lote] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[nro_orden] [smallint] NOT NULL,
[marca] [tinyint] NULL,
[fechamarca] [datetime] NULL,
[user_id] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[comentario] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[clear] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[user_id_clear] [char] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[resultado] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_rescanning_T] on [dbo].[rescanning_T]'
GO
ALTER TABLE [dbo].[rescanning_T] ADD CONSTRAINT [pk_rescanning_T] PRIMARY KEY CLUSTERED  ([cod_lote], [nro_orden]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Scanners]'
GO
CREATE TABLE [dbo].[Scanners]
(
[codigo] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descripcion] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SeguridadDatos]'
GO
CREATE TABLE [dbo].[SeguridadDatos]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[login] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[acceso_full] [int] NOT NULL,
[campo] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[valor] [varchar] (10) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SeguridadDatosGrupos_T]'
GO
CREATE TABLE [dbo].[SeguridadDatosGrupos_T]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[group_id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[acceso_full] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[campo] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[valor] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SeguridadDatosUsuarios_T]'
GO
CREATE TABLE [dbo].[SeguridadDatosUsuarios_T]
(
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[user_id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[acceso_full] [char] (1) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[campo] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[valor] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[sello]'
GO
CREATE TABLE [dbo].[sello]
(
[valor] [varchar] (1024) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[seteosCapturaTemplate]'
GO
CREATE TABLE [dbo].[seteosCapturaTemplate]
(
[tmplate] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[scanner] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[filename] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[talonarios_T]'
GO
CREATE TABLE [dbo].[talonarios_T]
(
[id] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descr] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NULL,
[ultNumero] [bigint] NULL,
[centroFacturacion] [char] (4) COLLATE SQL_Latin1_General_CP850_CI_AI NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_talonarios_T] on [dbo].[talonarios_T]'
GO
ALTER TABLE [dbo].[talonarios_T] ADD CONSTRAINT [pk_talonarios_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tipos]'
GO
CREATE TABLE [dbo].[tipos]
(
[tipo] [int] NOT NULL,
[descripcio] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[extension] [varchar] (5) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[visor] [int] NOT NULL,
[filtro] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_tipos] on [dbo].[tipos]'
GO
ALTER TABLE [dbo].[tipos] ADD CONSTRAINT [pk_tipos] PRIMARY KEY CLUSTERED  ([tipo]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tiposCampos_T]'
GO
CREATE TABLE [dbo].[tiposCampos_T]
(
[id] [int] NOT NULL,
[descr] [varchar] (100) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_tiposCampos_T] on [dbo].[tiposCampos_T]'
GO
ALTER TABLE [dbo].[tiposCampos_T] ADD CONSTRAINT [pk_tiposCampos_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[typedep]'
GO
CREATE TABLE [dbo].[typedep]
(
[codigo] [int] NOT NULL,
[basico] [smallint] NOT NULL,
[depend] [smallint] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Version]'
GO
CREATE TABLE [dbo].[Version]
(
[Product] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DBVersion] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Version] on [dbo].[Version]'
GO
ALTER TABLE [dbo].[Version] ADD CONSTRAINT [PK_Version] PRIMARY KEY CLUSTERED  ([Product]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[visores]'
GO
CREATE TABLE [dbo].[visores]
(
[visor] [int] NOT NULL,
[programa] [varchar] (254) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[descripcio] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_visores] on [dbo].[visores]'
GO
ALTER TABLE [dbo].[visores] ADD CONSTRAINT [pk_visores] PRIMARY KEY CLUSTERED  ([visor]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[volumestemplates]'
GO
CREATE TABLE [dbo].[volumestemplates]
(
[volume] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[tmplate] [varchar] (255) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_volumestemplates] on [dbo].[volumestemplates]'
GO
ALTER TABLE [dbo].[volumestemplates] ADD CONSTRAINT [pk_volumestemplates] PRIMARY KEY CLUSTERED  ([volume], [tmplate]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WSProcImg_T]'
GO
CREATE TABLE [dbo].[WSProcImg_T]
(
[id] [int] NOT NULL,
[descr] [varchar] (50) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_WSProcImg_T] on [dbo].[WSProcImg_T]'
GO
ALTER TABLE [dbo].[WSProcImg_T] ADD CONSTRAINT [pk_WSProcImg_T] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WSProcImg_Tmplate_T]'
GO
CREATE TABLE [dbo].[WSProcImg_Tmplate_T]
(
[wsProcImg_id] [int] NOT NULL,
[tmplate] [varchar] (8) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [pk_WSProcImg_Tmplate_T] on [dbo].[WSProcImg_Tmplate_T]'
GO
ALTER TABLE [dbo].[WSProcImg_Tmplate_T] ADD CONSTRAINT [pk_WSProcImg_Tmplate_T] PRIMARY KEY CLUSTERED  ([wsProcImg_id]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[zonascan]'
GO
CREATE TABLE [dbo].[zonascan]
(
[descrip] [varchar] (20) COLLATE SQL_Latin1_General_CP850_CI_AI NOT NULL,
[derecha] [int] NOT NULL,
[izquierda] [int] NOT NULL,
[arriba] [int] NOT NULL,
[abajo] [int] NOT NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[grpyus]'
GO
ALTER TABLE [dbo].[grpyus] WITH NOCHECK ADD
CONSTRAINT [fk_grpyus_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[permisos]'
GO
ALTER TABLE [dbo].[permisos] WITH NOCHECK ADD
CONSTRAINT [fk_permisos_grpyus] FOREIGN KEY ([codigo]) REFERENCES [dbo].[grpyus] ([codigo]),
CONSTRAINT [fk_permisos_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[usxgr]'
GO
ALTER TABLE [dbo].[usxgr] WITH NOCHECK ADD
CONSTRAINT [fk_usxgr_grpyus] FOREIGN KEY ([cod_us]) REFERENCES [dbo].[grpyus] ([codigo]),
CONSTRAINT [fk_usxgr_grpyus2] FOREIGN KEY ([cod_grupo]) REFERENCES [dbo].[grpyus] ([codigo])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[papelera]'
GO
ALTER TABLE [dbo].[papelera] ADD
CONSTRAINT [fk_papelera_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote]),
CONSTRAINT [fk_papelera_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[anotacion]'
GO
ALTER TABLE [dbo].[anotacion] ADD
CONSTRAINT [fk_Anotacion_Idlotes] FOREIGN KEY ([codlote]) REFERENCES [dbo].[idlotes] ([cod_lote])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CajasEntregas]'
GO
ALTER TABLE [dbo].[CajasEntregas] ADD
CONSTRAINT [fk_CajasEntregas_Cajas] FOREIGN KEY ([cod_caja]) REFERENCES [dbo].[Cajas] ([cod_caja]),
CONSTRAINT [fk_CajasEntregas_Entregas] FOREIGN KEY ([cod_entrega]) REFERENCES [dbo].[Entregas] ([cod_entrega])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[busqueda_body_T]'
GO
ALTER TABLE [dbo].[busqueda_body_T] ADD
CONSTRAINT [fk_busqueda_body_T_busqueda_header_T] FOREIGN KEY ([id]) REFERENCES [dbo].[busqueda_header_T] ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Cajas]'
GO
ALTER TABLE [dbo].[Cajas] ADD
CONSTRAINT [fk_Cajas_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[LD_auditoriaDocumentos_T]'
GO
ALTER TABLE [dbo].[LD_auditoriaDocumentos_T] ADD
CONSTRAINT [fk_LD_auditoriaDocumentos_T_idlotes] FOREIGN KEY ([lote]) REFERENCES [dbo].[idlotes] ([cod_lote])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[permissions_users_T]'
GO
ALTER TABLE [dbo].[permissions_users_T] ADD
CONSTRAINT [fk_permissions_users_T_task_T] FOREIGN KEY ([task_id_fk]) REFERENCES [dbo].[task_T] ([task_id]),
CONSTRAINT [fk_permissions_users_T_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[audit_T]'
GO
ALTER TABLE [dbo].[audit_T] ADD
CONSTRAINT [fk_audit_T_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id]),
CONSTRAINT [fk_audit_T_task_T] FOREIGN KEY ([task_id_fk]) REFERENCES [dbo].[task_T] ([task_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[membership_T]'
GO
ALTER TABLE [dbo].[membership_T] ADD
CONSTRAINT [fk_membership_T_group_T] FOREIGN KEY ([group_id_fk]) REFERENCES [dbo].[group_T] ([group_id]),
CONSTRAINT [fk_membership_T_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SubLotes]'
GO
ALTER TABLE [dbo].[SubLotes] ADD
CONSTRAINT [fk_SubLotes_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[permissions_groups_T]'
GO
ALTER TABLE [dbo].[permissions_groups_T] ADD
CONSTRAINT [fk_permissions_groups_T_group_T] FOREIGN KEY ([group_id_fk]) REFERENCES [dbo].[group_T] ([group_id]),
CONSTRAINT [fk_permissions_groups_T_task_T] FOREIGN KEY ([task_id_fk]) REFERENCES [dbo].[task_T] ([task_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[lotes]'
GO
ALTER TABLE [dbo].[lotes] ADD
CONSTRAINT [fk_lotes_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[permissions_roles_T]'
GO
ALTER TABLE [dbo].[permissions_roles_T] ADD
CONSTRAINT [fk_permissions_roles_T_role_task_T] FOREIGN KEY ([role_task_id_fk]) REFERENCES [dbo].[role_task_T] ([role_task_id]),
CONSTRAINT [fk_permissions_roles_T_role_T] FOREIGN KEY ([role_id_fk]) REFERENCES [dbo].[role_t] ([role_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tmplates]'
GO
ALTER TABLE [dbo].[tmplates] ADD
CONSTRAINT [fk_tmplates_Empresas] FOREIGN KEY ([LoginEmpresa]) REFERENCES [dbo].[Empresas] ([LoginEmpresa])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[LotesCajas]'
GO
ALTER TABLE [dbo].[LotesCajas] ADD
CONSTRAINT [fk_LotesCajas_Cajas] FOREIGN KEY ([cod_caja]) REFERENCES [dbo].[Cajas] ([cod_caja]),
CONSTRAINT [fk_LotesCajas_idlotes] FOREIGN KEY ([cod_lote]) REFERENCES [dbo].[idlotes] ([cod_lote])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ProcImg_Tmplate_T]'
GO
ALTER TABLE [dbo].[ProcImg_Tmplate_T] ADD
CONSTRAINT [fk_ProcImg_Tmplate_T_ProcImg_T] FOREIGN KEY ([procimg_id]) REFERENCES [dbo].[ProcImg_T] ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[vols]'
GO
ALTER TABLE [dbo].[vols] ADD
CONSTRAINT [fk_vols_device] FOREIGN KEY ([coddev]) REFERENCES [dbo].[device] ([coddev])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[zona]'
GO
ALTER TABLE [dbo].[zona] ADD
CONSTRAINT [fk_zona_dicdatos] FOREIGN KEY ([campo]) REFERENCES [dbo].[dicdatos] ([campo]),
CONSTRAINT [fk_zona_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[membership_user_role_t]'
GO
ALTER TABLE [dbo].[membership_user_role_t] ADD
CONSTRAINT [fk_membership_user_role_t_user_T] FOREIGN KEY ([user_id_fk]) REFERENCES [dbo].[user_T] ([user_id]),
CONSTRAINT [fk_membership_user_role_t_role_T] FOREIGN KEY ([role_id_fk]) REFERENCES [dbo].[role_t] ([role_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[montaje]'
GO
ALTER TABLE [dbo].[montaje] ADD
CONSTRAINT [fk_montaje_vols] FOREIGN KEY ([volumen]) REFERENCES [dbo].[vols] ([volumen])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[membership_group_role_t]'
GO
ALTER TABLE [dbo].[membership_group_role_t] ADD
CONSTRAINT [fk_membership_group_role_t_group_T] FOREIGN KEY ([group_id_fk]) REFERENCES [dbo].[group_T] ([group_id]),
CONSTRAINT [fk_membership_group_role_t_role_T] FOREIGN KEY ([role_id_fk]) REFERENCES [dbo].[role_t] ([role_id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MLMessages_T]'
GO
ALTER TABLE [dbo].[MLMessages_T] ADD
CONSTRAINT [fk_MLMessages_T_MLLanguages_T] FOREIGN KEY ([MLLanguage_ID]) REFERENCES [dbo].[MLLanguages_T] ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[idlotes]'
GO
ALTER TABLE [dbo].[idlotes] ADD
CONSTRAINT [fk_idlotes_tmplates] FOREIGN KEY ([tmplate]) REFERENCES [dbo].[tmplates] ([tmplate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[LD_Documentos_CBApeNomTrigger] on [dbo].[LD_Documentos_HistoricoCB]'
GO
-- =============================================
-- Create trigger basic template(After trigger)
-- =============================================
CREATE TRIGGER [LD_Documentos_CBApeNomTrigger]
ON [dbo].[LD_Documentos_HistoricoCB]
FOR DELETE, INSERT, UPDATE 
AS 
BEGIN
	update LD_Documentos_HistoricoCB set apenom = apellido + ' ' + nombres where apenom <> apellido + ' ' + nombres
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded.'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed.'
GO
DROP TABLE #tmpErrors
GO

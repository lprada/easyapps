/*
   Tuesday, June 02, 20099:46:14 AM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OperacionEntregaDetalle ADD
	Parametros varchar(500) NOT NULL CONSTRAINT DF_OperacionEntregaDetalle_Parametros DEFAULT ''
GO
COMMIT
select Has_Perms_By_Name(N'dbo.OperacionEntregaDetalle', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.OperacionEntregaDetalle', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.OperacionEntregaDetalle', 'Object', 'CONTROL') as Contr_Per 
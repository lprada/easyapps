/*
   Tuesday, May 31, 20112:17:01 PM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ActividadUsuario
	(
	user_id char(50) NOT NULL,
	TareaUsuarioId bigint NOT NULL,
	FechaInicio datetime NOT NULL,
	FechaFin datetime NULL,
	cod_caja varchar(30) NULL,
	cod_lote varchar(50) NULL,
	cod_entrega varchar(30) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	PK_ActividadUsuario PRIMARY KEY CLUSTERED 
	(
	user_id,
	TareaUsuarioId,
	FechaInicio
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ActividadUsuario SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'CONTROL') as Contr_Per 
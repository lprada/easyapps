/*
   Thursday, July 31, 20084:20:49 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.TmplatesResponsable
	(
	ResponsableId uniqueidentifier NOT NULL,
	Nombre nvarchar(100) NOT NULL,
	Telefono nvarchar(100) NOT NULL,
	Email nvarchar(100) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.TmplatesResponsable ADD CONSTRAINT
	PK_TmplatesResponsable PRIMARY KEY CLUSTERED 
	(
	ResponsableId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TmplatesResponsable', 'Object', 'CONTROL') as Contr_Per 
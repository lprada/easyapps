/*
   Wednesday, July 01, 20093:52:21 PM
   User: sa
   Server: 192.168.16.2
   Database: EasyDocNet
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_aud_cons
	(
	cod_lote varchar(50) NOT NULL,
	cod_us varchar(50) NOT NULL,
	fecha smalldatetime NOT NULL,
	hora varchar(200) NOT NULL,
	orden int NOT NULL,
	tmplate varchar(8) NOT NULL,
	identDoc varchar(255) NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.aud_cons)
	 EXEC('INSERT INTO dbo.Tmp_aud_cons (cod_lote, cod_us, fecha, hora, orden, tmplate, identDoc)
		SELECT cod_lote, cod_us, fecha, hora, CONVERT(int, orden), tmplate, identDoc FROM dbo.aud_cons WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.aud_cons
GO
EXECUTE sp_rename N'dbo.Tmp_aud_cons', N'aud_cons', 'OBJECT' 
GO
COMMIT
select Has_Perms_By_Name(N'dbo.aud_cons', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.aud_cons', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.aud_cons', 'Object', 'CONTROL') as Contr_Per 
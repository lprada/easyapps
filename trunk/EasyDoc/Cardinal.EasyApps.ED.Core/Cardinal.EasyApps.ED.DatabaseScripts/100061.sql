INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (264
           ,'es_AR'     
           ,'Busqueda por Marcas')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (264
           ,'en_us'     
           ,'Tag Cloud')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (265
           ,'es_AR'     
           ,'Busqueda por Logistica')
GO



INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (265
           ,'en_us'     
           ,'Logistics Search')
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (266
           ,'en_us'     
           ,'Without Tags')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (266
           ,'es_AR'     
           ,'Sin Marcas')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (267
           ,'en_us'     
           ,'Tag')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (267
           ,'es_AR'     
           ,'Marca')
GO



INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.btnBuscarLogistica'
           ,23)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.lblBusquedaMarcas'
           ,264)
GO
INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.lblBusquedaLogistica'
           ,265)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.lblSinMarcas'
           ,266)
GO
INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmBusqueda.lblMarcasTitulo'
           ,267)
GO
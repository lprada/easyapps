CREATE TABLE invalidos(
	tmplate varchar(8) NOT NULL,
	cod_lote varchar(50)  NOT NULL,
	nro_orden smallint NOT NULL,
	FechaHora datetime NULL
) 
ALTER TABLE dbo.invalidos  WITH CHECK ADD  CONSTRAINT fk_invalidos_idlotes FOREIGN KEY(cod_lote)
REFERENCES dbo.idlotes (cod_lote)
ALTER TABLE dbo.invalidos CHECK CONSTRAINT fk_invalidos_idlotes
ALTER TABLE dbo.invalidos  WITH CHECK ADD  CONSTRAINT fk_invalidos_tmplates FOREIGN KEY(tmplate)
REFERENCES dbo.tmplates (tmplate)
ALTER TABLE dbo.invalidos CHECK CONSTRAINT fk_invalidos_tmplates
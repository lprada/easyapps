/*
   Thursday, July 31, 20084:18:45 PM
   User: 
   Server: ANAKIN\SQLEXPRESS
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Empresas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.EmpresaSector ADD CONSTRAINT
	FK_EmpresaSector_Empresas FOREIGN KEY
	(
	LoginEmpresa
	) REFERENCES dbo.Empresas
	(
	LoginEmpresa
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.EmpresaSector', 'Object', 'CONTROL') as Contr_Per 
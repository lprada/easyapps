/*
   Thursday, March 31, 20119:33:13 AM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.idlotes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.tmplates SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.tmplates', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.papelera
	DROP CONSTRAINT FK_papelera_papelera
GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	FK_papelera_papelera FOREIGN KEY
	(
	tmplate
	) REFERENCES dbo.tmplates
	(
	tmplate
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.papelera ADD CONSTRAINT
	FK_papelera_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.papelera SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.papelera', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.papelera', 'Object', 'CONTROL') as Contr_Per 
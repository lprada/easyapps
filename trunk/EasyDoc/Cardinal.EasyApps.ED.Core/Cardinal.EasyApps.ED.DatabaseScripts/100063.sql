INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (268
           ,'en_us'     
           ,'Tag')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (268
           ,'es_AR'     
           ,'Marcar')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (269
           ,'en_us'     
           ,'Save')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (269
           ,'es_AR'     
           ,'Grabar')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (270
           ,'en_us'     
           ,'Tasks')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (270
           ,'es_AR'     
           ,'Tareas')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmPanelControl.Tareas'
           ,270)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lblTitulo'
           ,270)
GO


INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.lnkRefrescar'
           ,130)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (271
           ,'en_us'     
           ,'Task Id')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (271
           ,'es_AR'     
           ,'Tarea Id')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.uwgResultado.TareaId'
           ,271)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (272
           ,'en_us'     
           ,'Date Added')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (272
           ,'es_AR'     
           ,'Fecha Agregada')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.uwgResultado.FechaAgregado'
           ,272)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (272
           ,'en_us'     
           ,'Date Added')
GO


INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.uwgResultado.Nombre'
           ,34)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (273
           ,'en_us'     
           ,'Date Started')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (273
           ,'es_AR'     
           ,'Fecha Inicio')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.uwgResultado.FechaInicio'
           ,273)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (274
           ,'en_us'     
           ,'Data')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (274
           ,'es_AR'     
           ,'Datos')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.uwgResultado.Datos'
           ,274)
GO



INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmVerTareas.uwgResultado.Mensaje'
           ,125)
GO


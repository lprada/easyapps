/*
   Tuesday, June 14, 20116:04:54 PM
   User: sa
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Caratulas.TB_DOCUMENTO
	DROP CONSTRAINT PK_TB_DOCUMENTO
GO
ALTER TABLE Caratulas.TB_DOCUMENTO ADD CONSTRAINT
	PK_TB_DOCUMENTO PRIMARY KEY CLUSTERED 
	(
	CD_CODIGO_DOCUMENTO,
	CD_CAMPO,
	CD_PLANTILLA
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE Caratulas.TB_DOCUMENTO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'Caratulas.TB_DOCUMENTO', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'Caratulas.TB_DOCUMENTO', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'Caratulas.TB_DOCUMENTO', 'Object', 'CONTROL') as Contr_Per 
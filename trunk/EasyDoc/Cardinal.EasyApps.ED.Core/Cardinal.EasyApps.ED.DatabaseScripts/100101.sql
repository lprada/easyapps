/*
   Tuesday, May 31, 20112:19:02 PM
   User: 
   Server: (local)
   Database: EasyDoc.Database
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Entregas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Entregas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Entregas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Entregas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.idlotes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.idlotes', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Cajas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Cajas', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.TareaUsuario SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.TareaUsuario', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.TareaUsuario', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.TareaUsuario', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.user_T SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.user_T', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.user_T', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.user_T', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	FK_ActividadUsuario_user_T FOREIGN KEY
	(
	user_id
	) REFERENCES dbo.user_T
	(
	user_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	FK_ActividadUsuario_TareaUsuario FOREIGN KEY
	(
	TareaUsuarioId
	) REFERENCES dbo.TareaUsuario
	(
	TareaUsuarioId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	FK_ActividadUsuario_Cajas FOREIGN KEY
	(
	cod_caja
	) REFERENCES dbo.Cajas
	(
	cod_caja
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	FK_ActividadUsuario_idlotes FOREIGN KEY
	(
	cod_lote
	) REFERENCES dbo.idlotes
	(
	cod_lote
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActividadUsuario ADD CONSTRAINT
	FK_ActividadUsuario_Entregas FOREIGN KEY
	(
	cod_entrega
	) REFERENCES dbo.Entregas
	(
	cod_entrega
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ActividadUsuario SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.ActividadUsuario', 'Object', 'CONTROL') as Contr_Per 
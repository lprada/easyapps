INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (283
           ,'en_us'     
           ,'Notification')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (283
           ,'es_AR'     
           ,'Notificaciones')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificaciones.lblTitulo '
           ,283)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificaciones.lnkRefrescar'
           ,130)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (284
           ,'en_us'     
           ,'Notification')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (284
           ,'es_AR'     
           ,'Notificacion')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lblTitulo  '
           ,284)
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lnkNotificaciones'
           ,283)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (285
           ,'en_us'     
           ,'Identification')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (285
           ,'es_AR'     
           ,'Identificacion')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lblIdentificador '
           ,285)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (286
           ,'en_us'     
           ,'Date Added')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (286
           ,'es_AR'     
           ,'Fecha Alta')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lblFechaAlta '
           ,286)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (287
           ,'en_us'     
           ,'Date Readed')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (287
           ,'es_AR'     
           ,'Fecha Lectura')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lblFechaLeida '
           ,287)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (288
           ,'en_us'     
           ,'Date Archive')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (288
           ,'es_AR'     
           ,'Fecha Archivo')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lblFechaArchivo '
           ,288)
GO



INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (289
           ,'en_us'     
           ,'Documents')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (289
           ,'es_AR'     
           ,'Documento')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lbldocumentos '
           ,289)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (290
           ,'en_us'     
           ,'See All')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (290
           ,'es_AR'     
           ,'Ver Todas')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificaciones.lnkVerArchivadas'
           ,290)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (291
           ,'en_us'     
           ,'Mark as Read')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (291
           ,'es_AR'     
           ,'Marcar como Leida')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lnkMarcarLeida'
           ,291)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (292
           ,'en_us'     
           ,'Mark as UnRead')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (292
           ,'es_AR'     
           ,'Marcar como No Leida')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lnkMarcarNoLeida '
           ,292)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (293
           ,'en_us'     
           ,'Save')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (293
           ,'es_AR'     
           ,'Archivar')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lnkMarcarArchivar '
           ,293)
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (294
           ,'en_us'     
           ,'UnSave')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (294
           ,'es_AR'     
           ,'No Archivar')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmNotificacion.lnkMarcarNoArchivar  '
           ,294)
GO


INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (295
           ,'en_us'     
           ,'Email')
GO

INSERT INTO MLMessages_T
           ([id]
           ,[MLLanguage_ID]
           ,[text])
     VALUES
           (295
           ,'es_AR'     
           ,'Email')
GO

INSERT INTO MLObjects_T
           (id
           ,MLMessage_ID)
     VALUES
           ('wfrmUserData.lblEmail'
           ,295)
GO

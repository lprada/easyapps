﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cardinal.EasyApps.ED.WebServicies.Controllers
{
    public class AuthController : Controller
    {
        private readonly Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;

        public AuthController(Cardinal.EasyApps.ED.Common.Dal.IRepository repository)
        {
            _repository = repository;
        }
        [HttpPost]
        public ActionResult GetLoginToken(string username, string password)
        {
            string token = LCP.ODO.BusinessObjects.Usuario.GetLoginToken(username,password,"ED");
            Cardinal.EasyApps.ED.Model.LoginAuth loginAuth = new Cardinal.EasyApps.ED.Model.LoginAuth();
            if (!String.IsNullOrEmpty(token))
            {
                loginAuth.Result = "OK";
                loginAuth.Token = token;
            }
            else
            {
                loginAuth.Result = "FAIL";
                loginAuth.Token = string.Empty;
            }
            return new JsonResult
            {
                Data = loginAuth
            };
        }

        [HttpPost]
        public ActionResult IsLoginTokenValid(string loginToken)
        {
            if (!_repository.IsLoginTokenValid(loginToken))
            {
                return new JsonResult
                {
                    Data = "TOKEN INVALID"
                };
            }
            else {
                return new JsonResult
                {
                    Data = "TOKEN VALID"
                };
            }
        }

    }
}

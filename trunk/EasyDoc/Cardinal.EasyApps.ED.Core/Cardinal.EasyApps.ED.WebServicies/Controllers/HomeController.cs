﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cardinal.EasyApps.ED.WebServicies.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Bienvenido a los Servicios Web de EasyDoc.";

            return View();
        }

        public ActionResult GetLoginToken()
        {
            return View();
        }
        public ActionResult Configuracion()
        {
            return View();
        }

        public ActionResult ValidarLoginToken()
        {
            return View();
        }
        

    }
}

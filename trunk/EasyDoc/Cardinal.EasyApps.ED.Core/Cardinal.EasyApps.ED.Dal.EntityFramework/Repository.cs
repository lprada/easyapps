﻿using Cardinal.EasyApps.ED.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Cardinal.EasyApps.ED.Common.Dal;
using System.Globalization;
using System.Text;

namespace Cardinal.EasyApps.ED.Dal.EntityFramework
{
    public class Repository : IRepository
    {
        private string _connectionString = string.Empty;
        private EDEntities _ctx;

        public Repository(string connectionString)
        {
            _connectionString = connectionString;
            _ctx = new EDEntities(connectionString);
            _ctx.ContextOptions.LazyLoadingEnabled = true;
        }

        public string GetDBVersion()
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Model.Lote> GetLotesPorEstado(Model.EstadoLote estado)
        {
            throw new System.NotImplementedException();
        }

        public string GetPathRedVolumen(string volumen)
        {
            var vol = _ctx.montajes.SingleOrDefault(p => p.volumen == volumen);

            if (vol != null)
            {
                if (!String.IsNullOrWhiteSpace(vol.mountAccesoRed))
                {
                    return vol.mountAccesoRed;
                }
                else
                {
                    return vol.mount;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetExtensionTipo(int tipo)
        {
            var tipoBD = _ctx.tipos.SingleOrDefault(p => p.tipo == tipo);

            return tipoBD.extension;
        }

        public IQueryable<Model.Imagen> GetImagenesLote(Model.Lote lote)
        {
            List<Model.Imagen> res = new List<Model.Imagen>();

            //string sql = String.Format("SELECT nro_orden,indice,volumen from {0}AB where cod_lote ='{1}'", lote.Plantilla.CodigoPlantilla, lote.CodigoLote);

            //ObtenerImagenesLote(res, sql);

            //sql = String.Format("SELECT nro_orden,indice,volumen from {0}AA where cod_lote ='{1}'", lote.Plantilla.CodigoPlantilla, lote.CodigoLote);

            ObtenerImagenesLote(res, lote.Plantilla.CodigoPlantilla + "AB", lote.CodigoLote);
            ObtenerImagenesLote(res, lote.Plantilla.CodigoPlantilla + "AA", lote.CodigoLote);

            var imgLotes = from p in _ctx.lotes
                           where p.cod_lote == lote.CodigoLote
                           select p;

            foreach (var item in imgLotes)
            {
                Model.Imagen img = LoadImagenModel(lote.CodigoLote, item.volumen, item.nro_orden, item.indice.Value, item.tipo);
                res.Add(img);
            }

            return res.AsQueryable();
        }

        private void ObtenerImagenesLote(List<Model.Imagen> res, string tmplate, string lote)
        {
            var query = _ctx.GetDatosGeneralesImagenesLote(tmplate, lote);

            foreach (var item in query)
            {
                Model.Imagen img = LoadImagenModel(lote, item.volumen, item.nro_orden, item.indice.Value, item.tipo.Value);
                res.Add(img);
            }
        }

        private Model.Imagen LoadImagenModel(string lote, string volumen, int nroOrden, int indice, int tipo)
        {
            Model.Imagen img = new Model.Imagen();
            img.Volumen = volumen;
            img.CodigoLote = lote;
            img.Indice = indice;
            img.NroOrden = nroOrden;
            img.Tipo = tipo;
            img.Extension = GetExtensionTipo(tipo);
            img.PathRedImagen = GetPathRedVolumen(volumen);
            return img;
        }

        public IQueryable<Model.Lote> GetLotesAProcesarYTransferir()
        {
            short estadoPendienteProcesar = Convert.ToInt16(Model.EstadoLote.PendienteProcesar);
            short estadoPendienteTransferirEscanner = Convert.ToInt16(Model.EstadoLote.PendienteTransferirEscaner);
            short estadoPendienteTransferirProcesamiento = Convert.ToInt16(Model.EstadoLote.PendienteTransferirProcesamiento);

            var lotes = from p in _ctx.idlotes
                        where p.indexando == estadoPendienteProcesar || p.indexando == estadoPendienteTransferirEscanner || p.indexando == estadoPendienteTransferirProcesamiento
                        select new Model.Lote
                        {
                            CodigoLote = p.cod_lote,
                            Tag = p.tag,
                            FechaCreacion = p.feccreacion,
                            CantidadImagenes = p.ult_nro,
                            Plantilla = new Model.Plantilla
                                        {
                                            CodigoPlantilla = p.tmplate,
                                            Nombre = p.tmplate1.descripcio,
                                            Cliente = new Model.Cliente
                                                      {
                                                          CodigoCliente = p.tmplate1.LoginEmpresa,
                                                          Nombre = p.tmplate1.Empresa.DescEmpresa
                                                      }
                                        }
                        };

            return lotes;
        }

        public IQueryable<Model.LoteConError> GetLotesConErrorTransferenciaYProcesamiento()
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<Model.Lote> GetLotesHistorialTransferenciaYProcesamiento()
        {
            throw new System.NotImplementedException();
        }

        public void PonerEnPapelera(Model.Lote lote, int nroOrden)
        {
            var papelera = _ctx.papeleras.SingleOrDefault(p => p.cod_lote == lote.CodigoLote && p.nro_orden == nroOrden);
            if (papelera == null)
            {
                papelera = new papelera();
                papelera.cod_lote = lote.CodigoLote;
                papelera.FechaHora = DateTime.Now;
                papelera.nro_orden = nroOrden;
                papelera.tmplate = lote.Plantilla.CodigoPlantilla;
                _ctx.papeleras.AddObject(papelera);
                _ctx.SaveChanges();
            }
        }

        public void MarcarComoSubLote(Model.Lote lote, int indice, Model.PatchCodeValues patchCode, bool eliminarImagenAnterior)
        {
            if (eliminarImagenAnterior)
            {
                var subloteAnterior = _ctx.SubLotes.SingleOrDefault(p => p.cod_lote == lote.CodigoLote && p.indice == indice - 1);
                if (subloteAnterior != null)
                {
                    _ctx.SubLotes.DeleteObject(subloteAnterior);
                }
            }

            var sublote = _ctx.SubLotes.SingleOrDefault(p => p.cod_lote == lote.CodigoLote && p.indice == indice);
            if (sublote == null)
            {
                sublote = new SubLote();
                sublote.indice = indice;
                sublote.cod_lote = lote.CodigoLote;
                _ctx.SubLotes.AddObject(sublote);
            }
            sublote.nivel = Convert.ToInt32(patchCode);
            _ctx.SaveChanges();
        }

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        public IQueryable<Model.Cliente> GetEmpresas(string loginToken)
        {
            List<Model.Cliente> res = new List<Cliente>();
            var list = GetEmpresas();

            if (IsLoginTokenValid(loginToken))
            {
                var usr = GetUsuario(loginToken);

                if (usr != null)
                {
                    foreach (var item in list)
                    {
                        //Tiene Permisos para ver esta Empresa
                        if (TienePermisoObjeto(usr, TareaConsultaEmpresaID, item.CodigoCliente))
                        {
                            res.Add(item);
                        }
                    }
                }
            }
            return res.AsQueryable();
        }

        private Guid GrupoAdminID = new Guid("cfbe8828-249c-4ef2-8103-8c48aed2003d");
        private Guid TareaConsultaEmpresaID = new Guid("016163d4-65cd-43f2-8c75-99961e261104");
        private Guid TareaConsultaEPlantillaID = new Guid("09e0cfc1-2430-4b83-b741-9ec992f56327");

        private bool TienePermisoObjeto(Usuario usuario, Guid tareaId, string objeto)
        {
            //Me fijo los grupos
            foreach (var item in usuario.Grupoes)
            {
                //Grupo ADMIN ED
                if (item.GrupoId == GrupoAdminID)
                {
                    return true;
                }

                var perm = item.PermisosGrupoes.FirstOrDefault(p => p.ObjetoExternoId == objeto && p.TareaId == tareaId);
                if (perm != null)
                {
                    return true;
                }
            }

            //Me dijo el Usuario
            var permUsuario = usuario.PermisosUsuarios.FirstOrDefault(p => p.ObjetoExternoId == objeto && p.TareaId == tareaId);
            if (permUsuario != null)
            {
                return true;
            }
            return false;
        }

        private Usuario GetUsuario(string loginToken)
        {
            return GetLoginToken(loginToken).Usuario;
        }

        /// <summary>
        /// Gets the empresas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Model.Cliente> GetEmpresas()
        {
            return from p in _ctx.Empresas
                   where p.Estado == "A"
                   select new Model.Cliente
                   {
                       CodigoCliente = p.LoginEmpresa,
                       Nombre = p.DescEmpresa
                   };
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Model.PlantillaVolumen> GetPlantillasVolumenes()
        {
            var plantillas = from p in _ctx.tmplates
                             select p;

            List<Model.PlantillaVolumen> res = new List<Model.PlantillaVolumen>();
            foreach (var tmp in plantillas)
            {
                var hoy = DateTime.Now;
                //Busco los Volumnes validos para la Plantilla
                var volsTmplate = from p in _ctx.volumestemplates
                                  where p.tmplate == tmp.tmplate1
                                  select p;

                if (volsTmplate.Count() == 0)
                {
                    //No tiene seteados volumnes para el tmplate 
                    //agregar todos
                    var vols = from p in _ctx.vols
                               where (p.finiciovalidez == null || (p.ffinvalidez >= hoy && p.finiciovalidez <= hoy))
                               select p;
                    foreach (var vol in vols)
                    {
                        AddPlantillaTmplate(tmp, vol, res);
                    }
                }
                else
                {
                    //tiene seteados volumnes para el tmplate 
                    //agregar los configurados
                    var vols = from p in _ctx.vols
                               where (p.finiciovalidez == null || (p.ffinvalidez >= hoy && p.finiciovalidez <= hoy))
                               select p;

                    foreach (var vol in vols)
                    {
                        foreach (var item in volsTmplate)
                        {
                            if (item.volume == vol.volumen)
                            {
                                AddPlantillaTmplate(tmp, vol, res);
                            }
                        }
                    }
                }
            }

            return res.AsQueryable();
        }

        private void AddPlantillaTmplate(tmplate tmp, vol vol, List<PlantillaVolumen> res)
        {
            Model.PlantillaVolumen tmpVolumen = new Model.PlantillaVolumen();
            tmpVolumen.CodigoPlantilla = tmp.tmplate1;
            tmpVolumen.Nombre = tmp.descripcio;
            tmpVolumen.Cliente = new Model.Cliente
            {
                CodigoCliente = tmp.Empresa.LoginEmpresa,
                Nombre = tmp.Empresa.DescEmpresa
            };
            tmpVolumen.Volumen = new Model.Volumen
            {
                CodigoVolumen = vol.volumen
            };
            tmpVolumen.CodigoPlantillaVolumen = String.Format("{0};***;{1}", tmpVolumen.CodigoPlantilla, vol.volumen);
            res.Add(tmpVolumen);
        }

        public string GetProximoNombreLote(string path, string prefijoLote, string codigoEstacionEscaneoImportacion, string mascaraNumeroLote)
        {
            string batchPrefix = String.Format("{0}", DateTime.Now.ToString(prefijoLote));
            string batchScanningPosittion = codigoEstacionEscaneoImportacion;

            int nextNumber = GetProximoNumeroLote(String.Format("{0}_{1}", batchPrefix, batchScanningPosittion));
            string batchNumber = String.Format("{0}", nextNumber.ToString(mascaraNumeroLote));
            string batchName = batchPrefix + "_" + batchScanningPosittion + "_" + batchNumber;
            bool exists = System.IO.Directory.Exists(System.IO.Path.Combine(path, batchName));

            while (exists)
            {
                nextNumber += 1;
                batchNumber = String.Format("{0}", nextNumber.ToString(mascaraNumeroLote));
                batchName = batchPrefix + "_" + batchScanningPosittion + "_" + batchNumber;
                exists = System.IO.Directory.Exists(System.IO.Path.Combine(path, batchName));
            }
            return batchName;
        }

        /// <summary>
        /// Gets the next batch number.
        /// </summary>
        /// <param name="batchPrefix">The batch prefix.</param>
        /// <returns></returns>
        private int GetProximoNumeroLote(string inicioCodigoLote)
        {
            var lotes = from p in _ctx.idlotes
                        where p.cod_lote.StartsWith(inicioCodigoLote)
                        select p;

            int maximo = 0;
            foreach (var lote in lotes)
            {
                string cod_lote = lote.cod_lote;
                Int32 num;
                if (Int32.TryParse(cod_lote.Substring(inicioCodigoLote.Length + 1), NumberStyles.Integer, null, out num))
                {
                    if (num > maximo)
                    {
                        maximo = num;
                    }
                }
            }
            return maximo + 1;
        }

        /// <summary>
        /// Existes the lote.
        /// </summary>
        /// <param name="codLote">The cod lote.</param>
        /// <returns></returns>
        public bool ExisteLote(string codLote)
        {
            return _ctx.idlotes.SingleOrDefault(p => p.cod_lote == codLote) != null;
        }

        /// <summary>
        /// Agregars the lote importacion.
        /// </summary>
        /// <param name="loteAImportar">The lote A importar.</param>
        public void AgregarLoteImportacion(LoteImportar loteAImportar)
        {
            if (ExisteLote(loteAImportar.CodigoLoteEasyDoc))
            {
                throw new InvalidOperationException("Imposible Agregar Lote, ya existe.");
            }

            //INSERT INTO IDLOTES

            idlote loteNuevo = new idlote();
            loteNuevo.cod_lote = loteAImportar.CodigoLoteEasyDoc;
            loteNuevo.tmplate = loteAImportar.CodigoPlantilla;
            bool hayProcesamiento = _ctx.tmplates.Single(p => p.tmplate1 == loteAImportar.CodigoPlantilla).ocr == 1;
            if (hayProcesamiento)
            {
                if (loteAImportar.UsaTransferencia)
                {
                    loteNuevo.indexando = (int)EstadoLote.PendienteTransferirEscaner;
                }
                else
                {
                    loteNuevo.indexando = (int)EstadoLote.PendienteProcesar;
                }
            }
            else
            {
                loteNuevo.indexando = (int)EstadoLote.Disponible;
            }
            loteNuevo.caja = 0;
            loteNuevo.tag = loteAImportar.Tag;
            loteNuevo.scanner = loteAImportar.CodigoEstacionEscaneoImportacion;
            loteNuevo.feccreacion = _ctx.GetServerDateTime().FirstOrDefault().Value;
            loteNuevo.act_nro = Convert.ToInt16(loteAImportar.Imagenes.Count);
            loteNuevo.ult_nro = Convert.ToInt16(loteAImportar.Imagenes.Count);

            _ctx.idlotes.AddObject(loteNuevo);

            //Lotes
            var listaImagenes = loteAImportar.Imagenes.ToList();
            for (int i = 0; i < listaImagenes.Count; i++)
            {
                lote regLote = new lote();
                regLote.cod_lote = loteNuevo.cod_lote;
                regLote.nro_orden = Convert.ToInt16(i + 1);
                regLote.volumen = loteAImportar.CodigoVolumen;
                regLote.rotada = 0;
                regLote.reparada = 0;
                regLote.tipo = GetTipo(listaImagenes[i].Extension);
                regLote.ocr = "0";
                regLote.fecscan = loteNuevo.feccreacion.Value;
                regLote.indice = regLote.nro_orden;

                _ctx.lotes.AddObject(regLote);
            }

            //Insert into lotesCajas
            if (!String.IsNullOrWhiteSpace(loteAImportar.CodigoCaja))
            {
                //string cajaCompleto = string.Format("{0}_{1}", loteAImportar.Plantilla.Cliente.CodigoCliente, loteAImportar.CodigoCaja);
                //Caja caja = _ctx.Cajas.FirstOrDefault(p => p.cod_caja == cajaCompleto);
                var caja = GetCaja(loteAImportar.CodigoPlantilla, loteAImportar.CodigoCaja);
                if (caja != null)
                {
                    loteNuevo.Cajas.Add(caja);
                    caja.Estado = (int)EstadosCajas.En_Proceso;
                }
            }

            _ctx.SaveChanges();
        }

        public int GetTipo(string extension)
        {
            var tipo = _ctx.tipos.FirstOrDefault(p => p.extension == extension.Replace(".", ""));
            if (tipo != null)
            {
                return tipo.tipo;
            }
            else
            {
                return 4;
            }
        }

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <returns></returns>
        public bool ExisteCaja(string codCaja)
        {
            return _ctx.Cajas.SingleOrDefault(p => p.cod_caja == codCaja) != null;
        }

        /// <summary>
        /// Existes the caja.
        /// </summary>
        /// <param name="codCaja">The cod caja.</param>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public bool ExisteCaja(string codCaja, string codigoPlantilla)
        {
            var caja = GetCaja(codigoPlantilla, codCaja);

            return caja != null;
        }

        private Caja GetCaja(string codigoPlantilla, string codCaja)
        {
            var loginEmpresa = _ctx.tmplates.Single(p => p.tmplate1 == codigoPlantilla).LoginEmpresa;

            string cajaCompleto = string.Format("{0}_{1}", loginEmpresa, codCaja);

            var caja = _ctx.Cajas.SingleOrDefault(p => p.cod_caja == cajaCompleto);
            return caja;
        }

        /// <summary>
        /// Determines whether [is login token valid] [the specified login token].
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns>
        ///   <c>true</c> if [is login token valid] [the specified login token]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsLoginTokenValid(string loginToken)
        {
            return GetLoginToken(loginToken) != null;
        }

        private LoginToken GetLoginToken(string loginToken)
        {
            return _ctx.LoginTokens.SingleOrDefault(p => p.Token == loginToken);
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas()
        {
            return from p in _ctx.tmplates
                   select new Model.Plantilla
                   {
                       CodigoPlantilla = p.tmplate1,
                       Nombre = p.descripcio,
                       Cliente = new Model.Cliente
                                 {
                                     CodigoCliente = p.LoginEmpresa,
                                     Nombre = p.Empresa.DescEmpresa
                                 }
                   };
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas(string codigoCliente)
        {
            return from p in _ctx.tmplates
                   where p.LoginEmpresa == codigoCliente
                   select new Model.Plantilla
                   {
                       CodigoPlantilla = p.tmplate1,
                       Nombre = p.descripcio,
                       Cliente = new Model.Cliente
                                 {
                                     CodigoCliente = p.LoginEmpresa,
                                     Nombre = p.Empresa.DescEmpresa
                                 }
                   };
        }



        public List<string> GetTamplateVolumens(string codigoTamplate)
        {
            return (from p in _ctx.volumestemplates
                   where p.tmplate == codigoTamplate
                   select p.volume).ToList();
                   
        }

        /// <summary>
        /// Gets the plantilla campos.
        /// </summary>
        /// <param name="codigoPlantilla">The codigo plantilla.</param>
        /// <returns></returns>
        public IQueryable<PlantillaCampo> GetPlantillaCampos(string codigoPlantilla)
        {
            List<PlantillaCampo> res = new List<PlantillaCampo>();

            var campos = from p in _ctx.zonas
                         where p.tmplate == codigoPlantilla
                                && p.dicdato != null
                         select p;

            foreach (var campo in campos)
            {
                Model.PlantillaCampo item = new PlantillaCampo();
                item.CampoId = campo.campo;
                item.Nombre = campo.dicdato.campo;
                item.Orden = campo.orden;
                item.EsObligatorio = (campo.mndtory == null) ? false : (campo.mndtory == 0 ? false : true);
                item.MaxValor = campo.dicdato.rango_max;
                item.MinValor = campo.dicdato.rango_min;
                item.CantidadCaracteresMaximos = (campo.dicdato.longitud.HasValue) ? Convert.ToInt16(campo.dicdato.longitud) : -1;
                
                item.TipoCampo = (TipoCampo)campo.dicdato.tipo;
               
               
                item.ValoresPosibles = new List<KeyValuePair<string, string>>();
                item.EsCampoDetalle = campo.ocr2 == "D";

                if (item.TipoCampo == TipoCampo.Texto && campo.reftab != null && campo.visual.Substring(1, 1) == "2")
                {
                    string sql = string.Format("select {0} as Id,{1} as Value from {2}", item.CampoId, campo.campoext, campo.reftab);

                    var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                    foreach (var valor in query)
                    {
                        KeyValuePair<string, string> keyValue = new KeyValuePair<string, string>(valor.Id.ToUpper(), valor.Value.ToUpper());
                        item.ValoresPosibles.Add(keyValue);
                    }
                }

                res.Add(item);
            }

            return res.AsQueryable();
        }

        /// <summary>
        /// Gets the tablas externas.
        /// </summary>
        /// <returns></returns>
        public IQueryable<TablaExterna> GetTablasExternas()
        {
            List<TablaExterna> res = new List<TablaExterna>();

            var tablas = from p in _ctx.zonas
                         where p.reftab != null || p.reftab != string.Empty
                         group p by new { p.reftab, p.campoext, p.campo } into g
                         select new Model.TablaExterna
                         {
                             Nombre = g.Key.reftab,
                             CampoId = g.Key.campo,
                             CampoExterno = g.Key.campoext
                         };

            foreach (var item in tablas)
            {
                item.ValoresPosibles = new List<KeyValuePair<string, string>>();
                string sql = string.Format("select {0} as Id,{1} as Value from {2}", item.CampoId, item.CampoExterno, item.Nombre);

                var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                foreach (var valor in query)
                {
                    KeyValuePair<string, string> keyValue = new KeyValuePair<string, string>(valor.Id.ToUpper(), valor.Value.ToUpper());
                    item.ValoresPosibles.Add(keyValue);
                }
                res.Add(item);
            }

            return res.AsQueryable();
        }

        public IQueryable<HitoricoCS> GetHistoricoByUsuario(string nroUsuario)
        {
            List<Model.HitoricoCS> query = new List<Model.HitoricoCS>();

            query = (from empresa in _ctx.TB_EMPRESA_DOCUMENTOS
                     join documentos in _ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO
                     where empresa.CD_USUARIO_CARGA == nroUsuario
                     where empresa.FL_ESTADO_DOCUMENTO == "1"
                     select new HitoricoCS
                         {
                             codigo = documentos.CD_CODIGO_DOCUMENTO,
                             fecha = empresa.FC_CREACION,
                             empresa = empresa.CD_EMPRESA,
                             plantilla = documentos.TX_PLANTILLA,
                             nombre_empresa = empresa.TX_EMPRESA,
                             usuario = empresa.CD_USUARIO_CARGA,
                         }).Distinct().OrderByDescending(empresa => empresa.fecha).ToList();

            for (int i = 0; i < query.Count; i++)
            {
                query[i].campos_valores = GetCamposByDocumento(query[i].codigo).ToList();
            }

            return query.AsQueryable();
        }

        public bool AnularCaratulaCliente(int id_caratula)
        {
            var query = from doc in _ctx.TB_EMPRESA_DOCUMENTOS
                        where doc.CD_CODIGO_DOCUMENTO == id_caratula
                        select doc;

            foreach (TB_EMPRESA_DOCUMENTOS doc in query)
            {
                doc.FL_ESTADO_DOCUMENTO = "0";
            }

            try
            {
                _ctx.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IQueryable<CamposCS> GetCamposByDocumento(decimal nroDocumento)
        {
            var query = (from documentos in _ctx.TB_DOCUMENTO
                         where documentos.CD_CODIGO_DOCUMENTO == nroDocumento
                         orderby documentos.OrdenDetalle
                         select new CamposCS
                         {
                             documento = documentos.CD_CODIGO_DOCUMENTO,
                             campo = documentos.TX_CAMPO,
                             valor = documentos.VL_CAMPO,
                         });
            return query;
        }

        public void LogOperationCaratulas(LogCaratula logDTO)
        {
            TB_LOGS item = new TB_LOGS();
            item.CD_MOVIMIENTO = logDTO.CD_MOVIMIENTO;
            item.CD_USUARIO_MOVIMIENTO = logDTO.CD_USUARIO_MOVIMIENTO;
            item.FC_MOVIMIENTO = logDTO.FC_MOVIMIENTO;
            item.TX_OBSERVACION = logDTO.TX_OBSERVACION;

            _ctx.AddObject("TB_LOGS", item);

            _ctx.SaveChanges();
        }

        public CaratulaCS GetCaratulaVistaPorId(int nroCaratula)
        {
            var caratula = _ctx.TB_EMPRESA_DOCUMENTOS.FirstOrDefault(p => p.CD_CODIGO_DOCUMENTO == nroCaratula);

            if (caratula != null)
            {
                return LoadCaratula(caratula);
            }

            return new CaratulaCS();
        }

        private CaratulaCS LoadCaratula(TB_EMPRESA_DOCUMENTOS caratula)
        {
            CaratulaCS res = new CaratulaCS();
            //res.empresa = caratula.CD_EMPRESA;
            //res.nombre_empresa = caratula.TX_EMPRESA;
            //res.usuario = caratula.CD_USUARIO_CARGA;
            //res.fecha = caratula.FC_CREACION;
            //res.plantilla = string.Empty;
            //res.codigo_3d = caratula.CD_CODIGO_3D;
            //res.codigo = caratula.CD_CODIGO_DOCUMENTO;
            //res.FechaInbound = caratula.FechaInbound;
            //res.FechaOutbound = caratula.FechaOutBound;

            //foreach (var item in caratula.TB_DOCUMENTO.OrderBy(p => p.OrdenDetalle).ThenBy(p => p.TX_CAMPO))
            //{
            //    res.plantilla = item.TX_PLANTILLA;
            //    CamposCS campo = new CamposCS();

            //    campo.campo = item.CD_CAMPO;
            //    campo.EsDetalle = item.ESDETALLE.HasValue && item.ESDETALLE.Value;
            //    campo.txCampo = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.TX_CAMPO.ToLower());
            //    campo.valor = item.VL_CAMPO;
            //    campo.OrdenDetalle = item.OrdenDetalle;

            //    res.DetalleCampos.Add(campo);
            //}

            return res;
        }

        //public IQueryable<CaratulaCS> GetCaratulaCompleta()
        //{
        //    var query = (from empresa in _ctx.TB_EMPRESA_DOCUMENTOS
        //                 join documentos in _ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO
        //                 select new CaratulaCS
        //                 {
        //                     empresa = empresa.CD_EMPRESA,
        //                     usuario = empresa.CD_USUARIO_CARGA,
        //                     fecha = empresa.FC_CREACION,
        //                     codigo = documentos.CD_CODIGO_DOCUMENTO,
        //                     valor = documentos.VL_CAMPO,
        //                     campo = documentos.TX_CAMPO,
        //                     plantilla = documentos.TX_PLANTILLA,
        //                     nombre_empresa = empresa.TX_EMPRESA,
        //                     codigo_3d = empresa.CD_CODIGO_3D,
        //                 });
        //    return query;
        //}

        public int GetMaxDocumentID()
        {
            try
            {
                return Convert.ToInt32(_ctx.TB_EMPRESA_DOCUMENTOS.Max(d => d.CD_CODIGO_DOCUMENTO));
            }
            catch
            {
                return 0;
            }
        }

        public long saveDocumento(DocumentoEmpresa documento)
        {
            List<TB_DOCUMENTO> listDocs = new List<TB_DOCUMENTO>();

            TB_EMPRESA_DOCUMENTOS empresaDoc = new TB_EMPRESA_DOCUMENTOS
            {
                CD_CODIGO_3D = documento.codigo3d,
                CD_USUARIO_CARGA = documento.usuarioCarga,
                CD_EMPRESA = documento.empresa.CodigoCliente,
                CD_CODIGO_DOCUMENTO = GetMaxDocumentID() + 1,
                TX_EMPRESA = documento.empresa.Nombre,
                FC_CREACION = documento.fechaCreacion,
                FL_ESTADO_DOCUMENTO = documento.estadoDocumento
            };

            _ctx.TB_EMPRESA_DOCUMENTOS.AddObject(empresaDoc);

            foreach (CamposCS item in documento.listaCampos)
            {
                TB_DOCUMENTO documentoEntity = new TB_DOCUMENTO
                {
                    CD_CODIGO_DOCUMENTO = empresaDoc.CD_CODIGO_DOCUMENTO,
                    CD_CAMPO = item.campo,
                    TX_CAMPO = item.txCampo,
                    CD_PLANTILLA = documento.plantilla,
                    TX_PLANTILLA = documento.txPlantilla,
                    VL_CAMPO = item.valor,
                    ESDETALLE = item.EsDetalle,
                    OrdenDetalle = item.OrdenDetalle
                };

                _ctx.TB_DOCUMENTO.AddObject(documentoEntity);
            }

            _ctx.SaveChanges();

            return empresaDoc.CD_CODIGO_DOCUMENTO;
        }

        /// <summary>
        /// Existes the key valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public bool ExisteKeyValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key)
        {
            try
            {
                string sql = string.Format("select {0} as Id,{1} as Value from {2} where {0}='{3}'", campoId, campoExterno, tablaExterna, key);

                var query = _ctx.ExecuteStoreQuery<ValorPosibleCombo>(sql);
                return query.Count() > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Agregars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public bool AgregarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string value)
        {
            try
            {
                string sql = string.Format("INSERT INTO {2}({0},{1}) VALUES ('{3}','{4}')", campoId, campoExterno, tablaExterna, key.ToUpper(), value.ToUpper());

                int num = _ctx.ExecuteStoreCommand(sql);
                return num > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Editars the valor tabla externa.
        /// </summary>
        /// <param name="tablaExterna">The tabla externa.</param>
        /// <param name="campoId">The campo id.</param>
        /// <param name="campoExterno">The campo externo.</param>
        /// <param name="key">The key.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        public bool EditarValorTablaExterna(string tablaExterna, string campoId, string campoExterno, string key, string newValue)
        {
            try
            {
                string sql = string.Format("UPDATE {2} set {1} ='{4}' where {0}='{3}'", campoId, campoExterno, tablaExterna, key.ToUpper(), newValue.ToUpper());

                int num = _ctx.ExecuteStoreCommand(sql);
                return num > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillasWithLoginToken(string loginToken)
        {
            List<Model.Plantilla> res = new List<Plantilla>();
            var list = GetPlantillas();

            if (IsLoginTokenValid(loginToken))
            {
                var usr = GetUsuario(loginToken);

                if (usr != null)
                {
                    foreach (var item in list)
                    {
                        //Tiene Permisos para ver esta Empresa
                        if (TienePermisoObjeto(usr, TareaConsultaEPlantillaID, item.CodigoPlantilla))
                        {
                            res.Add(item);
                        }
                    }
                }
            }
            return res.AsQueryable();
        }

        /// <summary>
        /// Gets the plantillas.
        /// </summary>
        /// <param name="loginToken">The login token.</param>
        /// <param name="codigoCliente">The codigo cliente.</param>
        /// <returns></returns>
        public IQueryable<Plantilla> GetPlantillas(string loginToken, string codigoCliente)
        {
            List<Model.Plantilla> res = new List<Plantilla>();
            var list = GetPlantillas(codigoCliente);

            if (IsLoginTokenValid(loginToken))
            {
                var usr = GetUsuario(loginToken);

                if (usr != null)
                {
                    foreach (var item in list)
                    {
                        //Tiene Permisos para ver esta Empresa
                        if (TienePermisoObjeto(usr, TareaConsultaEPlantillaID, item.CodigoPlantilla))
                        {
                            res.Add(item);
                        }
                    }
                }
            }
            return res.AsQueryable();
        }

        public IQueryable<CaratulaCS> GetCaratulas()
        {
            List<CaratulaCS> res = new List<CaratulaCS>();

            var caratulas = from p in _ctx.TB_EMPRESA_DOCUMENTOS
                            select p;

            foreach (var caratula in caratulas)
            {
                res.Add(LoadCaratula(caratula));
            }

            return res.AsQueryable();
        }

        public IQueryable<CaratulaCS> GetCaratulas(string username)
        {
            List<CaratulaCS> res = new List<CaratulaCS>();

            var caratulas = from p in _ctx.TB_EMPRESA_DOCUMENTOS
                            where p.CD_USUARIO_CARGA == username
                            select p;

            foreach (var caratula in caratulas)
            {
                res.Add(LoadCaratula(caratula));
            }

            return res.AsQueryable();
        }

        public void RealizarInbound(long numeroCaratula)
        {
            var caratula = _ctx.TB_EMPRESA_DOCUMENTOS.FirstOrDefault(p => p.CD_CODIGO_DOCUMENTO == numeroCaratula);
            if (caratula != null && !caratula.FechaInbound.HasValue)
            {
                caratula.FechaInbound = DateTime.Now;
                _ctx.SaveChanges();
            }
        }

        public bool AddCliente(Cliente cliente)
        {
            Empresa emp = new Empresa();
            emp.LoginEmpresa = cliente.CodigoCliente;
            emp.DescEmpresa = cliente.Nombre;
            emp.Estado = "A";
            _ctx.Empresas.AddObject(emp);
            _ctx.SaveChanges();
            return true;
        }

        public bool DelCliente(Cliente cliente)
        {
            var cli = _ctx.Empresas.SingleOrDefault(p => p.LoginEmpresa == cliente.CodigoCliente);
            if (cli != null)
            {
                cli.Estado = "D";
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public List<CampoDiccionario> GetDiccionarioDatos()
        {
            return (from p in _ctx.dicdatos
                    select new Model.CampoDiccionario
                    {
                        CampoId = p.campo,
                        CantidadCaracteresMaximos = 0,
                        Nombre = p.descripcio,
                        TipoCampo = (Cardinal.EasyApps.ED.Model.TipoCampo)p.tipo,
                        MaxValor = p.rango_max,
                        MinValor = p.rango_min
                    }).ToList();
        }

        public bool AddCampo(CampoDiccionario campo)
        {
            dicdato dato = new dicdato();
            dato.campo = campo.CampoId;
            dato.descripcio = campo.Nombre;
            dato.tipo = (byte)campo.TipoCampo;
            dato.rango_max = campo.MaxValor;
            dato.rango_min = campo.MinValor;
            _ctx.dicdatos.AddObject(dato);
            _ctx.SaveChanges();
            return true;
        }

        public bool DelCampo(CampoDiccionario campo)
        {
            dicdato dato = new dicdato();
            dato.campo = campo.CampoId;
            dato.descripcio = campo.Nombre;
            dato.tipo = (byte)campo.TipoCampo;
            dato.rango_max = campo.MaxValor;
            dato.rango_min = campo.MinValor;
            _ctx.dicdatos.DeleteObject(dato);
            _ctx.SaveChanges();
            return true;
        }

        public bool AddVolumeTamplate(string tmplate, string volume)
        {
            volumestemplate volumesTemplate = new volumestemplate();

            volumesTemplate.tmplate = tmplate;
            volumesTemplate.volume = volume;
            _ctx.volumestemplates.AddObject(volumesTemplate);
            _ctx.SaveChanges();
            return true;
        }

        private static char GetRandomUpperCaseCharacter(Random rnd)
        {
            return ((char)((short)'A' + rnd.Next(26)));
        }

        public PlantillaServicio AddPlantilla(PlantillaServicio plantilla)
        {
            //Genero Codigo
            bool existe = true;
            do
            {
                Random rnd = new Random();
                plantilla.CodigoPlantilla = "Z" + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd);
                existe = _ctx.tmplates.Where(p => p.tmplate1 == plantilla.CodigoPlantilla).Count() != 0;
            }
            while (existe);

            //Tmplate
            var tmp = new tmplate();
            tmp.tmplate1 = plantilla.CodigoPlantilla;
            tmp.descripcio = plantilla.Nombre;
            tmp.tipoindex = 0;
            tmp.periodica = 0;
            tmp.nro_zonas = 0;
            tmp.tipo = "T";
            tmp.parent = "1";
            tmp.datasrc = 1;
            tmp.ocr = 1;
            tmp.produccion = "A";
            tmp.LoginEmpresa = plantilla.Cliente.CodigoCliente;
            tmp.BillingType = "IMG"; tmp.usaMergeDatos = 0;
            tmp.diasprocesoError = 0;
            tmp.diasprocesoWarning = 0;
            tmp.firmadigi = "N";
            tmp.SeteosCaptura = null;
            tmp.FormatoCapturaBN = null;
            tmp.FormatoCapturaColor = null;
            tmp.FormatoCapturaGrey = null;
            tmp.orderImagenes = "A";
            tmp.cajasMensuales = 0;
            tmp.SeteosIndexacion = null;
            tmp.seteoDocumental = 0;

            _ctx.tmplates.AddObject(tmp);


            //Dicdatos
            short i = 1;
            foreach (var item in plantilla.Campos)
            {
                //existe = _ctx.dicdatos.Where(p => p.campo == item.CampoId).Count() != 0;
                existe = _ctx.dicdatos.Any(p => p.campo == item.CampoId);
                if (!existe)
                {
                    dicdato dicdato = new dicdato();
                    dicdato.campo = item.CampoId;
                    dicdato.descripcio = item.Nombre;
                    dicdato.interno = 0;
                    dicdato.mascara = string.Empty;
                    dicdato.rango_max = item.MaxValor;
                    dicdato.rango_min = item.MinValor;
                    dicdato.tipo = (short)item.TipoCampo;
                    dicdato.decimales = null;
                    dicdato.longitud = null;
                    switch (item.TipoCampo)
                    {
                        case TipoCampo.Texto:
                        case TipoCampo.Numero:
                        case TipoCampo.SmallInt:
                            dicdato.longitud = (short)item.CantidadCaracteresMaximos;
                            dicdato.decimales = null;
                            break;
                        case TipoCampo.Fecha:
                        case TipoCampo.Boolean:
                        case TipoCampo.FechaInterna:
                        default:
                            break;
                    }
                    _ctx.dicdatos.AddObject(dicdato);

                }
                ////Zona
                zona zona = new EntityFramework.zona();
                zona.campo = item.CampoId;
                zona.tmplate = plantilla.CodigoPlantilla;
                zona.orden = i;
                zona.tp = 0;
                zona.lft = 0;
                zona.bttm = 0;
                zona.rght = 0;
                zona.mndtory = item.EsObligatorio ? (short)1 : (short)0;
                zona.ocr1 = null;
                zona.ocr2 = null;
                zona.ocr3 = null;
                zona.ocr4 = null;
                zona.ocr5 = 0;
                zona.reftab = null;
                zona.campoext = null;
                zona.datasrc = 1;
                zona.visual = "00";
                zona.filtrotabext = null;
                _ctx.zonas.AddObject(zona);
                i++;
            }


            //TODO: Crear Tabla

            _ctx.SaveChanges();
            return plantilla;
        }

        public bool DelPlantilla(PlantillaServicio plantilla)
        {
            var tmp = _ctx.tmplates.SingleOrDefault(p => p.tmplate1 == plantilla.CodigoPlantilla);
            if (tmp != null)
            {
                tmp.produccion = "D";
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DelFisicoPlantilla(PlantillaServicio plantilla, string password)
        {
            if (password == "Ab123456")
            {
                return DelPlantilla(plantilla);

            }
            else
            {
                return true;
            }

        }

        public IQueryable<PlantillaServicio> GetPlantillasServicios()
        {
            var res = (from p in _ctx.tmplates
                       where p.produccion == "0" || p.produccion == "A"
                       select new Model.PlantillaServicio
                       {
                           CodigoPlantilla = p.tmplate1,
                           Nombre = p.descripcio,
                           Cliente = new Model.Cliente
                                     {
                                         CodigoCliente = p.LoginEmpresa,
                                         Nombre = p.Empresa.DescEmpresa
                                     }
                       }).ToList();

            foreach (var item in res)
            {
                item.Campos = GetPlantillaCampos(item.CodigoPlantilla).ToList();
            }

            return res.AsQueryable();
        }





        public string GetNuevoCodigoPlantila()
        {
            bool existe = true;
            string codigo = string.Empty;
            do
            {
                Random rnd = new Random();
                codigo = "Z" + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd) + GetRandomUpperCaseCharacter(rnd);
                existe = _ctx.tmplates.Where(p => p.tmplate1 == codigo).Count() != 0;
            }
            while (existe);

            return codigo;
        }

        public bool ExisteCodigoPlantilla(string codigo)
        {
            return _ctx.tmplates.Where(p => p.tmplate1 == codigo).Count() != 0;
        }


        public bool DelCampoPlantilla(PlantillaServicio plantilla, string codigoCampo)
        {
            try
            {
                var zona = _ctx.zonas.FirstOrDefault(p => p.tmplate == plantilla.CodigoPlantilla && p.campo == codigoCampo);

                if (zona != null)
                {
                    _ctx.zonas.DeleteObject(zona);
                    _ctx.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {

                return false;
            }
        }


        public List<CampoCaseConflict> GetCampoCaseConflicts()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT * FROM ");
            sql.Append(" (SELECT zona.tmplate AS zonaTmplate, ");
            sql.Append(" zona.campo AS zonaCampo,");
            sql.Append(" dicdatos.campo  AS DicdatosCampo");
            sql.Append(" FROM  zona INNER JOIN " );
            sql.Append(" dicdatos ON zona.campo = dicdatos.campo) AS t  ");
            sql.Append(" WHERE t.DicdatosCampo <> t.zonaCampo  ");
            sql.Append(" COLLATE sql_latin1_General_CP1_cs_as  ");

            var res = from p in _ctx.ExecuteStoreQuery<CampoCaseConflict>(sql.ToString())
                      select new CampoCaseConflict 
                        {
                            DicdatosCampo = p.DicdatosCampo, 
                            ZonaCampo = p.ZonaCampo, 
                            ZonaTmplate = p.ZonaTmplate 
                        };

            return res.ToList();
        }

    }
}
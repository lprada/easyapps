﻿namespace Depuracion_EasyDoc
{
    partial class Depuracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Comenzar = new System.Windows.Forms.Button();
            this.Info = new System.Windows.Forms.Label();
            this.lvwItems = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // Comenzar
            // 
            this.Comenzar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Comenzar.Location = new System.Drawing.Point(175, 12);
            this.Comenzar.Name = "Comenzar";
            this.Comenzar.Size = new System.Drawing.Size(140, 50);
            this.Comenzar.TabIndex = 0;
            this.Comenzar.Text = "Comenzar";
            this.Comenzar.UseVisualStyleBackColor = true;
            this.Comenzar.Click += new System.EventHandler(this.Comenzar_Click);
            // 
            // Info
            // 
            this.Info.Location = new System.Drawing.Point(12, 340);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(273, 26);
            this.Info.TabIndex = 1;
            this.Info.Text = "Informacion...";
            // 
            // lvwItems
            // 
            this.lvwItems.Location = new System.Drawing.Point(38, 81);
            this.lvwItems.Name = "lvwItems";
            this.lvwItems.Size = new System.Drawing.Size(403, 232);
            this.lvwItems.TabIndex = 2;
            this.lvwItems.UseCompatibleStateImageBehavior = false;
            this.lvwItems.View = System.Windows.Forms.View.List;
            // 
            // Depuracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 375);
            this.Controls.Add(this.lvwItems);
            this.Controls.Add(this.Info);
            this.Controls.Add(this.Comenzar);
            this.Name = "Depuracion";
            this.Text = "Depuracion";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Comenzar;
        private System.Windows.Forms.Label Info;
        private System.Windows.Forms.ListView lvwItems;
    }
}
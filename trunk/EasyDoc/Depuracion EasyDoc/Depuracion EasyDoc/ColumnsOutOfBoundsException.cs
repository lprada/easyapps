﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Depuracion_EasyDoc
{
    class ColumnsOutOfBoundsException : Exception
    {
        public ColumnsOutOfBoundsException(String tab)
        {
            Console.WriteLine("Las tablas "+tab+" tienen distinta cantidad de campos");

        }
    }
}

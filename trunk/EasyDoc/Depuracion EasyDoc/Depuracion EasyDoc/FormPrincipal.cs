﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using NLog;

namespace Depuracion_EasyDoc
{
    public partial class Depuracion : Form
    {
        private  SqlConnection connProd;
        private  SqlConnection connHist;
        private  string base_origen = ConfigurationManager.AppSettings["base_origen"];
        private  string base_destino = ConfigurationManager.AppSettings["base_destino"];
        private  string diasdepuracion = ConfigurationManager.AppSettings["depuracion_dias"];
        private  string depurar = ConfigurationManager.AppSettings["depuracion"];
        private  Logger logger = NLog.LogManager.GetCurrentClassLogger();


        public Depuracion()
        {
            InitializeComponent();
        }

        private void Comenzar_Click(object sender, EventArgs e)
        {
            start();
        }

        public void refreshInfo(string info)
        {
            Info.Text = info;
            Info.Refresh();
        }


        public void start()
        {
            string base_origen = ConfigurationManager.AppSettings["base_origen"];
            string base_destino = ConfigurationManager.AppSettings["base_destino"];

            string prod = ConfigurationManager.AppSettings["Produccion"];


            connProd = new SqlConnection(@"Data Source=darthcaedus;Initial Catalog=" + base_origen + ";User Id=sa;Password=Yo123456;");
            connHist = new SqlConnection(@"Data Source=darthcaedus;Initial Catalog=" + base_destino + ";User Id=sa;Password=Yo123456;");

            insertarTabla("Empresas");
            insertarTabla("tmplates");
            DepurarAuditorias();
            DepurarErroresControl();
            DepurarTemplates();
            DepurarIdLotes();
            DepurarLotesCajas();
            DepurarPapelera();
            DepurarRecepcionCajas(); //(NO SE CON QUE CRITERIO FILTRAR!!!)
            DepurarProcesamientos();
            DepurarCajas();
            refreshInfo("Depuración terminada");
            
        }


        private  DataSet CrearDataSet(string sql, SqlConnection con)
        {
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds, "Produccion");
            adapter.Dispose();
            return ds;
        }

        private  void Execute(string sql, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
        }

        private string scalarExecute(string sql, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(sql, conn);
            return cmd.ExecuteScalar().ToString();
        }

        private void DepurarAuditorias()
        {
            refreshInfo("Depurando tablas de auditoria...");
            
            //hay registros huerfanos como L091112_K160_0001 que no está vinculado a ninguna caja.
            //algunos otros ni siquiera aparecen en idlotes.
            //en un futuro no muy lejano tendria que hacer una pasada distinta que revise la integridad y borre estos datos "basura"
            connProd.Open();
            string sql = "select lc.cod_lote, e.cod_entrega from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja order by lc.cod_lote asc";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;

            connHist.Open();

            verificarTablas(base_origen, base_destino, "aud_capt");
            verificarTablas(base_origen, base_destino, "aud_clas");
            verificarTablas(base_origen, base_destino, "aud_logis");
            verificarTablas(base_origen, base_destino, "aud_cons");
            verificarTablas(base_origen, base_destino, "AuditoriaGraboVerificacion");
            verificarTablas(base_origen, base_destino, "AuditoriaRecorte");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_lote = dr[0].ToString();
                string cod_entrega = dr[1].ToString();

                //Reviso si no es un registro huerfano, si lo es se elimina.
                string huerfanocheck = String.Empty;
                string sqldephuerfano = String.Empty;
                string sqlhuerfanocheck = "select * from " + base_origen + "..idlotes id, " + base_origen + "..LotesCajas lc where id.cod_lote='" + cod_lote + "' and lc.cod_lote='" + cod_lote + "'";

                try { huerfanocheck = scalarExecute(sqlhuerfanocheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (huerfanocheck == String.Empty)
                {
                    //acá deberia poner el código de borrado
                    logger.Info("El lote: " + cod_lote + " es huerfano");
                    lvwItems.Items.Add("El lote: " + cod_lote + " es huerfano");
                    lvwItems.Refresh();

                    string sqlborrar = "delete from " + base_origen + "..idlotes where cod_lote='" + cod_lote + "'; delete from " + base_origen + "..lotescajas where cod_lote='" + cod_lote + "'";
                    try
                    {
                        Execute(sqlborrar, connHist);
                    }
                    catch (Exception e) {
                        e.ToString();
                    }
                }

                else
                {
                    //limpio aud_capt

                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();
                    string entregascheck = String.Empty;
                    string sqldepentregas = String.Empty;
                    string sqlentregascheck = "select * from " + base_destino + "..aud_capt where cod_lote='" + cod_lote + "'";

                    try { entregascheck = scalarExecute(sqlentregascheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }

                    if (entregascheck == String.Empty)
                    {
                        sqldepentregas = "insert into " + base_destino + "..aud_capt select * from " + base_origen + "..aud_capt where cod_lote='" + cod_lote + "'";

                        try {
                            Execute(sqldepentregas, connHist);
                            if (depurar == "1")
                            {
                               sqldepentregas = "delete from " + base_origen + "..aud_capt where cod_lote='" + cod_lote + "'";
                               Execute(sqldepentregas, connHist);
                            }
                        }catch (Exception e){
                            logger.Info(e.ToString());
                        }
                    }

                    //limpio aud_clas

                    string aud_clascheck = String.Empty;
                    string sqldepaud_clas = String.Empty;
                    string sqlaud_clascheck = "select * from " + base_destino + "..aud_clas where cod_lote='" + cod_lote + "'";

                    try { aud_clascheck = scalarExecute(sqlaud_clascheck, connHist); }
                    catch (Exception e) {
                        logger.Info( e.ToString());
                    }
                    if (aud_clascheck == String.Empty)
                    {
                        sqldepaud_clas = "insert into " + base_destino + "..aud_clas select * from " + base_origen + "..aud_clas where cod_lote='" + cod_lote + "'";

                        try {
                            Execute(sqldepaud_clas, connHist);
                            if (depurar == "1")
                            {
                                sqldepaud_clas = "delete from " + base_origen + "..aud_clas where cod_lote='" + cod_lote + "'";
                                Execute(sqldepaud_clas, connHist);
                            }
                        }catch (Exception e){
                            logger.Info(e.ToString());
                        }
                    }

                    //limpio aud_cons

                    string aud_conscheck = String.Empty;
                    string sqldepaud_cons = String.Empty;
                    string sqlaud_conscheck = "select * from " + base_destino + "..aud_cons where cod_lote='" + cod_lote + "'";

                    try { aud_conscheck = scalarExecute(sqlaud_conscheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }
                    if (aud_conscheck == String.Empty)
                    {
                        sqldepaud_cons = "insert into " + base_destino + "..aud_cons select * from " + base_origen + "..aud_cons where cod_lote='" + cod_lote + "'";

                        try {
                            Execute(sqldepaud_cons, connHist);
                            if (depurar == "1")
                            {
                                sqldepaud_cons = "delete from " + base_origen + "..aud_cons where cod_lote='" + cod_lote + "'";
                                Execute(sqldepaud_cons, connHist);
                            }
                        }catch (Exception e){
                            logger.Info(e.ToString());
                        }
                        
                    }

                    //limpio aud_logis

                    string aud_logischeck = String.Empty;
                    string sqldepaud_logis = String.Empty;
                    string sqlaud_logischeck = "select * from " + base_destino + "..aud_logis where cod_obj='" + cod_entrega + "'";

                    try { aud_logischeck = scalarExecute(sqlaud_logischeck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }
                    if (aud_logischeck == String.Empty)
                    {
                        sqldepaud_logis = "insert into " + base_destino + "..aud_logis select * from " + base_origen + "..aud_logis where cod_obj='" + cod_entrega + "'";

                        try {
                            Execute(sqldepaud_logis, connHist);
                            if (depurar == "1")
                            {
                                sqldepaud_logis = "delete from " + base_origen + "..aud_logis where cod_obj='" + cod_entrega + "'";
                                Execute(sqldepaud_logis, connHist);
                            }
                        }catch (Exception e){
                            logger.Info(e.ToString());
                        } 
                    }

                    //limpio AuditoriaGraboVerificacion

                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();
                    string AuditGraboVercheck = String.Empty;
                    string sqldepAuditGraboVer = String.Empty;
                    string sqlAuditGraboVercheck = "select * from " + base_destino + "..aud_capt where cod_lote='" + cod_lote + "'";

                    try { AuditGraboVercheck = scalarExecute(sqlAuditGraboVercheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }

                    if (AuditGraboVercheck == String.Empty)
                    {
                        sqldepAuditGraboVer = "insert into " + base_destino + "..AuditoriaGraboVerificacion select * from " + base_origen + "..AuditoriaGraboVerificacion where cod_lote='" + cod_lote + "'";

                        try {
                            Execute(sqldepAuditGraboVer, connHist);
                            if (depurar == "1")
                            {
                                sqldepAuditGraboVer = "delete from " + base_origen + "..AuditoriaGraboVerificacion where cod_lote='" + cod_lote + "'";
                                Execute(sqldepAuditGraboVer, connHist);
                            }
                        }catch (Exception e){
                            logger.Info(e.ToString());
                        }
                    }

                    //limpio AuditoriaRecorte

                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();
                    string AuditRecortecheck = String.Empty;
                    string sqldepAuditRecorte = String.Empty;
                    string sqlAuditRecortecheck = "select * from " + base_destino + "..aud_capt where cod_lote='" + cod_lote + "'";

                    try { AuditRecortecheck = scalarExecute(sqlAuditRecortecheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }

                    if (AuditRecortecheck == String.Empty)
                    {
                        sqldepAuditRecorte = "insert into " + base_destino + "..AuditoriaRecorte select * from " + base_origen + "..AuditoriaRecorte where cod_lote='" + cod_lote + "'";

                        try
                        {
                            Execute(sqldepAuditRecorte, connHist);
                            if (depurar == "1")
                            {
                                sqldepAuditRecorte = "delete from " + base_origen + "..AuditoriaRecorte where cod_lote='" + cod_lote + "'";
                                Execute(sqldepAuditRecorte, connHist);
                            }
                        }
                        catch (Exception e)
                        {
                            logger.Info(e.ToString());
                        }
                    }
                }
            }

            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion Audirotias terminada");
            lvwItems.Clear();
        }


        private  void DepurarProcesamientos()
        {
            refreshInfo("Depurando tablas de Procesamientos...");
            //terminar
            connProd.Open();
            string sql = "select p.cod_lote,feccreacion from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..idlotes p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = p.cod_lote group by p.cod_lote,feccreacion";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;

            connHist.Open();

            verificarTablas(base_origen, base_destino, "Procesamientos");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                //{
                string cod_lote = dr[0].ToString();
                string procesamientoscheck = String.Empty;
                string sqldepprocesamientos = String.Empty;
                string sqlprocesamientoscheck = "select * from " + base_destino + "..Procesamientos where cod_lote='" + cod_lote + "'";
                
                //BORRAR
                logger.Info("Procesamientos: " + cod_lote + " fue depurado");
                lvwItems.Items.Add(cod_lote);
                lvwItems.Refresh();


                try { procesamientoscheck = scalarExecute(sqlprocesamientoscheck, connHist); }
                catch (Exception e) {
                    logger.Info( e.ToString());
                }

                if (procesamientoscheck == String.Empty)
                {
                    sqldepprocesamientos = "insert into " + base_destino + "..Procesamientos select * from " + base_origen + "..Procesamientos where cod_lote='" + cod_lote + "'";

                    try
                    {
                        Execute(sqldepprocesamientos, connHist);
                        if (depurar == "1")
                        {
                            sqldepprocesamientos = "delete from " + base_origen + "..Procesamientos where cod_lote='" + cod_lote + "'";
                            Execute(sqldepprocesamientos, connHist);
                        }
                    }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    } 
                    
                }
                //}
            }
            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion Procesamientos terminada");
            lvwItems.Clear();
        }

        private  void DepurarIdLotes()
        {
            refreshInfo("Depurando tablas de idlotes...");

            connProd.Open();
            string sql = "select p.cod_lote,feccreacion from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..idlotes p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = p.cod_lote group by p.cod_lote,feccreacion";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;
            //string sql = "select * from idlotes";
            //int fechaDepurar = Convert.ToInt32(DateTime.Now.ToString("yyMMdd")) - Convert.ToInt32(ConfigurationManager.AppSettings["idlotes_meses"]+"00");

            connHist.Open();

            verificarTablas(base_origen, base_destino, "idlotes");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                //{
                string cod_lote = dr[0].ToString();
                string idlotescheck = String.Empty;
                string sqldepidlotes = String.Empty;
                string sqlidlotescheck = "select * from " + base_destino + "..idlotes where cod_lote='" + cod_lote + "'";


                try { idlotescheck = scalarExecute(sqlidlotescheck, connHist); }
                catch (Exception e)
                {
                    logger.Info(e.ToString());
                }

                if (idlotescheck == String.Empty)
                {
                    sqldepidlotes = "insert into " + base_destino + "..idlotes select * from " + base_origen + "..idlotes where cod_lote='" + cod_lote + "'";

                    if (depurar == "1")
                    {
                        sqldepidlotes += "insert into " + base_destino + "..invalidos select * from " + base_origen + "..invalidos where cod_lote='" + cod_lote + "';";

                    }
                    try
                    {
                        Execute(sqldepidlotes, connHist);
                        logger.Info("Idlotes: " + cod_lote + " fue insertado");
                        lvwItems.Items.Add(cod_lote);
                        lvwItems.Refresh();
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    } 
                }
            }
            //}
            DepurarSecuenciaLotes(ds);
            DepurarSubLotes(ds);
            DepurarLotes(ds);

            refreshInfo("Depurando tablas de idlotes...");

            if (depurar == "1")
            {
                foreach (DataRow dr in ds.Tables["Produccion"].Rows)
                {
                    //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                    //{
                    string cod_lote = dr[0].ToString();
                    string idlotescheck = String.Empty;
                    string sqldepidlotes = String.Empty;
                    string sqlidlotescheck = "select * from " + base_destino + "..idlotes where cod_lote='" + cod_lote + "'";

                    try { idlotescheck = scalarExecute(sqlidlotescheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }

                    if (idlotescheck != String.Empty)
                    {
                        //BORRAR
                        logger.Info("Idlotes: " + cod_lote + " fue depurado");

                        sqldepidlotes = "delete from " + base_origen + "..invalidos where cod_lote = '" + cod_lote + "'";
                        sqldepidlotes += "delete from " + base_origen + "..idlotes where cod_lote='" + cod_lote + "'";
                        try
                        {
                            Execute(sqldepidlotes, connHist);
                        }
                        catch (Exception e) {
                            logger.Info(e.ToString());
                        }
                    }
                }
            }

            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion Idlotes terminada");
            lvwItems.Clear();
        }

        private  void DepurarSecuenciaLotesDetalles(DataSet ds)
        {
            refreshInfo("Depurando tablas de SecuenciaLotesDetalles...");

            verificarTablas(base_origen, base_destino, "SecuenciaLotesDetalle");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                //{
                string cod_lote = dr[0].ToString();
                string idlotescheck = String.Empty;
                string sqldepidlotes = String.Empty;
                string sqlidlotescheck = "select * from " + base_destino + "..SecuenciaLotesDetalle where lote='" + cod_lote + "'";

                try { idlotescheck = scalarExecute(sqlidlotescheck, connHist); }
                catch (Exception e)
                {
                    logger.Info(e.ToString());
                }

                if (idlotescheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();

                    sqldepidlotes = sqldepidlotes = "insert into " + base_destino + "..secuenciaLotesDetalle select * from " + base_origen + "..secuenciaLotesDetalle where lote='" + cod_lote + "';";

                    try
                    {
                        Execute(sqldepidlotes, connHist);
                        logger.Info(cod_lote + " fue insertado - SecLDetalles");
                        if (depurar == "1")
                        {
                            //BORRAR
                            sqldepidlotes = "delete from " + base_origen + "..secuenciaLotesDetalle where lote = '" + cod_lote + "'";
                            Execute(sqldepidlotes, connHist);
                            logger.Info(cod_lote + " fue depurado - SecLDetalles");
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    }
                }
            }

            logger.Info("Depuracion SecuenciaLotesDetalles terminada");

        }


        private  void DepurarSecuenciaLotes(DataSet ds)
        {
            refreshInfo("Depurando tablas de SecuenciaLotes...");

            verificarTablas(base_origen, base_destino, "SecuenciaLotes");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                //{
                string cod_lote = dr[0].ToString();
                string idlotescheck = String.Empty;
                string sqldepidlotes = String.Empty;
                string sqlidlotescheck = "select * from " + base_destino + "..SecuenciaLotes where LoteInicioSecuencia = '" + cod_lote + "'";

                try { idlotescheck = scalarExecute(sqlidlotescheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (idlotescheck == String.Empty)
                {
                    sqldepidlotes = "insert into " + base_destino + "..secuenciaLotes select * from " + base_origen + "..secuenciaLotes where LoteInicioSecuencia = '" + cod_lote + "'";
                    try
                    {
                        Execute(sqldepidlotes, connHist);
                        logger.Info(cod_lote + " fue insertado - SecLotes");
                        lvwItems.Items.Add(cod_lote);
                        lvwItems.Refresh();
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    } 
                }
            }
            DepurarSecuenciaLotesDetalles(ds);
            refreshInfo("Depurando tablas de SecuenciaLotes...");

            if (depurar == "1")
            {
                foreach (DataRow dr in ds.Tables["Produccion"].Rows)
                {
                    //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                    //{
                    string cod_lote = dr[0].ToString();
                    string idlotescheck = String.Empty;
                    string sqldepidlotes = String.Empty;
                    string sqlidlotescheck = "select sl.* from " + base_destino + "..SecuenciaLotes sl, " + base_destino + "..SecuenciaLotesDetalle sld  where sld.lote='" + cod_lote + "' and sld.SecuenciaLotesId = sl.SecuenciaLotesId";

                    try { idlotescheck = scalarExecute(sqlidlotescheck, connHist); }
                    catch (Exception e) {
                        logger.Info( e.ToString());
                    }

                    if (idlotescheck != String.Empty)
                    {
                        //BORRAR
                        logger.Info(cod_lote + " fue depurado - SecLotes");

                        sqldepidlotes = "delete sl from " + base_origen + "..secuenciaLotes sl, " + base_destino + "..secuenciaLotesDetalle sld where sld.Lote = '" + cod_lote + "' and sld.SecuenciaLotesId = sl.SecuenciaLotesId";
                        try {
                            Execute(sqldepidlotes, connHist);
                        } catch (Exception e){
                            logger.Info(e.ToString());
                        }
                    }
                }
            }

            logger.Info("Depuracion SecuenciaLotes terminada");

        }



        private  void DepurarSubLotes(DataSet ds)
        {
            refreshInfo("Depurando tablas de SubLotes...");

            verificarTablas(base_origen, base_destino, "Sublotes");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                //if (Convert.ToInt32(String.Format("{0:yyMMdd}", Convert.ToDateTime(dr[1].ToString()))) <= fechaDepurar)
                //{
                string cod_lote = dr[0].ToString();
                string Sublotescheck = String.Empty;
                string sqldepSublotes = String.Empty;
                string sqlSublotescheck = "select * from " + base_destino + "..SubLotes where cod_lote='" + cod_lote + "'";


                try { Sublotescheck = scalarExecute(sqlSublotescheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString()); 
                }

                if (Sublotescheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();

                    sqldepSublotes = "insert into " + base_destino + "..Sublotes select * from " + base_origen + "..Sublotes where cod_lote='" + cod_lote + "'";

                    try {
                        Execute(sqldepSublotes, connHist);
                        logger.Info(cod_lote + " fue insertado - sublote");
                        if (depurar == "1")
                        {
                            //BORRAR
                            sqldepSublotes = "delete from " + base_origen + "..Sublotes where cod_lote='" + cod_lote + "'";
                            Execute(sqldepSublotes, connHist);
                            logger.Info(cod_lote + " fue depurado - sublote");
                        }
                    }catch (Exception e){
                        logger.Info(e.ToString());
                    }
                    
                }
                //}
            }
            logger.Info("Depuracion sublotes terminada");

        }


        private  void DepurarLotes(DataSet ds)
        {
            refreshInfo("Depurando tablas de Lotes...");
            //string diasdepuracion = ConfigurationManager.AppSettings["depuracion_dias"];

            verificarTablas(base_origen, base_destino, "Lotes");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_lote = dr[0].ToString();
                string Lotescheck = String.Empty;
                string sqldepLotes = String.Empty;
                string sqlLotescheck = "select * from " + base_destino + "..Lotes where cod_lote='" + cod_lote + "'";

                try { Lotescheck = scalarExecute(sqlLotescheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (Lotescheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();

                    try
                    {
                        sqldepLotes = "insert into " + base_destino + "..Lotes select * from " + base_origen + "..Lotes where cod_lote='" + cod_lote + "'";
                        Execute(sqldepLotes, connHist);
                        logger.Info(cod_lote + " fue insertado - Lotes");
                        if (depurar == "1")
                        {
                            //BORRAR
                            sqldepLotes = "delete from " + base_origen + "..Lotes where cod_lote='" + cod_lote + "'";
                            Execute(sqldepLotes, connHist);
                            logger.Info(cod_lote + " fue depurado - Lotes");
                        }
                    }
                    catch (Exception e){
                        logger.Info(e.ToString());
                    }
                }
            }
            logger.Info("Depuracion Lotes terminada");
        }



        private  void DepurarErroresControl()
        {
            refreshInfo("Depurando tablas de ErroresControl...");

            connProd.Open();
            string sql = "select p.cod_lote from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..ErroresControl p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = p.cod_lote group by p.cod_lote";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;

            connHist.Open();

            verificarTablas(base_origen, base_destino, "ErroresControl");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_lote = dr[0].ToString();
                string ErroresControlcheck = String.Empty;
                string sqldepErroresControl = String.Empty;
                string sqlErroresControlcheck = "select * from " + base_destino + "..ErroresControl where cod_lote='" + cod_lote + "'";


                try { ErroresControlcheck = scalarExecute(sqlErroresControlcheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (ErroresControlcheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();

                    try {
                        sqldepErroresControl = "insert into " + base_destino + "..ErroresControl select * from " + base_origen + "..ErroresControl where cod_lote='" + cod_lote + "'";
                        Execute(sqldepErroresControl, connHist);
                        logger.Info(cod_lote + " fue insertado");

                        if (depurar == "1")
                        {
                            sqldepErroresControl = "delete from " + base_origen + "..ErroresControl where cod_lote='" + cod_lote + "'";
                            Execute(sqldepErroresControl, connHist);
                            logger.Info(cod_lote + " fue depurado");
                        }
                    }catch (Exception e){
                        logger.Info(e.ToString());
                    }                    
                }
            }
            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion ErroresControl terminada");
            lvwItems.Clear();
        }

        private  void DepurarLotesCajas()
        {
            refreshInfo("Depurando tablas de LotesCajas...");

            connProd.Open();
            string sql = "select lc.cod_caja from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja group by lc.cod_caja";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false; 

            connHist.Open();

            verificarTablas(base_origen, base_destino, "LotesCajas");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_caja = dr[0].ToString();
                string LotesCajascheck = String.Empty;
                string sqldepLotesCajas = String.Empty;
                string sqlLotesCajascheck = "select * from " + base_destino + "..LotesCajas where cod_caja='" + cod_caja + "'";

                try { LotesCajascheck = scalarExecute(sqlLotesCajascheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (LotesCajascheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_caja);
                    lvwItems.Refresh();

                    try {
                        sqldepLotesCajas = "insert into " + base_destino + "..LotesCajas select * from " + base_origen + "..LotesCajas where cod_caja='" + cod_caja + "'";
                        Execute(sqldepLotesCajas, connHist);
                        logger.Info(cod_caja + " fue insertado - LotesCajas");

                        if (depurar == "1")
                         {
                            sqldepLotesCajas = "delete from " + base_origen + "..LotesCajas where cod_caja='" + cod_caja + "'";
                            Execute(sqldepLotesCajas, connHist);
                            logger.Info(cod_caja + " fue depurado - LotesCajas");
                        }
                    }catch (Exception e){
                        logger.Info(e.ToString());
                    }                    
                }
            }
            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion LotesCajas terminada");
            lvwItems.Clear();
        }

        private  void DepurarPapelera()
        {
            refreshInfo("Depurando tablas de Papeleras...");
            //EL CRITERIO DE DEPURACION ESTÁ MAL, HABLARLO CON LUIS

            connProd.Open();
            string sql = "select p.cod_lote from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..papelera p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = p.cod_lote group by p.cod_lote";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;
            //PROBAR ESTE:
            //select pa.cod_lote from LotesCajas lc, papelera pa, Entregas e, CajasEntregas ce where not e.FechaBajada is null and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = pa.cod_lote and datediff(d,e.FechaBajada,GetDate()) >= 10 group by pa.cod_lote 

            connHist.Open();

            verificarTablas(base_origen, base_destino, "Papelera");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_lote = dr[0].ToString();
                string Papeleracheck = String.Empty;
                string sqldepPapelera = String.Empty;
                string sqlPapeleracheck = "select * from " + base_destino + "..Papelera where cod_lote='" + cod_lote + "'";

                try { Papeleracheck = scalarExecute(sqlPapeleracheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (Papeleracheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_lote);
                    lvwItems.Refresh();

                    try {
                        sqldepPapelera = "insert into " + base_destino + "..Papelera select * from " + base_origen + "..Papelera where cod_lote='" + cod_lote + "'";
                        Execute(sqldepPapelera, connHist);
                        logger.Info(cod_lote + " fue insertado - Papelera");

                        if (depurar == "1")
                         {
                            sqldepPapelera = "delete from " + base_origen + "..Papelera where cod_lote='" + cod_lote + "'";
                            Execute(sqldepPapelera, connHist); 
                            logger.Info(cod_lote + " fue depurado - Papelera");
                        }
                    }catch (Exception e){
                        logger.Info(e.ToString());
                    }
                }
            }
            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion papelera terminada");
            lvwItems.Clear();
        }

        private  void DepurarRecepcionCajas()
        {
            refreshInfo("Depurando tablas de RecepcionCajas...");

            connProd.Open();
            string sql = "select rcd.caja, rcd.idEntrega from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, recepcionCajasDetalle rcd where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_caja = rcd.caja group by rcd.caja, rcd.idEntrega";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;
            
            connHist.Open();

            verificarTablas(base_origen, base_destino, "recepcionCajasDetalle");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_caja = dr[0].ToString();
                string idEntrega = dr[1].ToString();
                string LotesCajascheck = String.Empty;
                string sqldepLotesCajas = String.Empty;
                string sqlLotesCajascheck = "select * from " + base_destino + "..recepcionCajasDetalle where caja='" + cod_caja + "'";

                try { LotesCajascheck = scalarExecute(sqlLotesCajascheck, connHist); }
                catch (Exception e) {
                    logger.Info( e.ToString());
                }

                if (LotesCajascheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_caja);
                    lvwItems.Refresh();

                    try {
                        sqldepLotesCajas = "insert into " + base_destino + "..recepcionCajasDetalle select * from " + base_origen + "..recepcionCajasDetalle where caja='" + cod_caja + "'; insert into " + base_destino + "..recepcionCajas select * from " + base_origen + "..recepcionCajas where idEntrega='" + idEntrega + "'";
                        Execute(sqldepLotesCajas, connHist);
                        logger.Info(cod_caja + " fue insertado - recepcionCajasDetalle");

                        if (depurar == "1")
                        {
                            sqldepLotesCajas = "delete from " + base_origen + "..recepcionCajasDetalle where caja='" + cod_caja + "'";
                            Execute(sqldepLotesCajas, connHist);
                            logger.Info(cod_caja + " fue depurado - recepcionCajasDetalle");
                        }
                    }catch (Exception e){
                        logger.Info(e.ToString());
                    }
                }
            }
            connHist.Close();
            connProd.Close();

            logger.Info("Depuracion RecepcionCajas terminada");
            lvwItems.Clear();
        }

        private  void DepurarCajas()
        {
            refreshInfo("Depurando tablas de Cajas...");

            connProd.Open();
            string sql = "select p.cod_caja from " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..Cajas p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = p.cod_caja group by p.cod_caja";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;

            connHist.Open();

            verificarTablas(base_origen, base_destino, "Cajas");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_caja = dr[0].ToString();
                string Cajascheck = String.Empty;
                string sqldepCajas = String.Empty;
                string sqlCajascheck = "select * from " + base_destino + "..Cajas where cod_caja='" + cod_caja + "'";

                try { Cajascheck = scalarExecute(sqlCajascheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (Cajascheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_caja);
                    lvwItems.Refresh();

                    //BORRAR
                    
                    try
                    {
                        sqldepCajas = "insert into " + base_destino + "..Cajas select * from " + base_origen + "..Cajas where cod_caja='" + cod_caja + "';";
                        Execute(sqldepCajas, connHist);
                        logger.Info(cod_caja + " fue insertado - CAJAS");
                    }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }
                }
            }

            DepurarEntregas(ds);
            refreshInfo("Depurando tablas de Cajas...");

            if (depurar == "1")
            {
                foreach (DataRow dr in ds.Tables["Produccion"].Rows)
                {
                    string cod_caja = dr[0].ToString();
                    string Cajascheck = String.Empty;
                    string sqldepCajas = String.Empty;
                    string sqlCajascheck = "select * from " + base_destino + "..Cajas where cod_caja='" + cod_caja + "'";
                    
                    //BORRAR
                    logger.Info(cod_caja + " fue depurado - CAJAS");

                    try { Cajascheck = scalarExecute(sqlCajascheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }

                    if (Cajascheck != String.Empty)
                    {
                        //BORRAR
                        logger.Info(cod_caja + " fue depurado - CAJAS");

                        sqldepCajas += "delete from " + base_origen + "..Cajas where cod_caja='" + cod_caja + "'";
                        try{
                            Execute(sqldepCajas, connHist);
                        }catch (Exception e){
                            logger.Info(e.ToString());
                        }
                    }
                }

                connHist.Close();
                connProd.Close();
            }
            logger.Info("Depuracion Cajas terminada");
            lvwItems.Clear();
        }


        private  void DepurarEntregas(DataSet dt)
        {
            refreshInfo("Depurando tablas de Entregas...");

            string sql = "select e.cod_entrega from " + base_origen + "..Entregas e where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion;
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;

            verificarTablas(base_origen, base_destino, "Entregas");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_entrega = dr[0].ToString();
                string entregascheck = String.Empty;
                string sqldepentregas = String.Empty;
                string sqlentregascheck = "select * from " + base_destino + "..Entregas where cod_entrega='" + cod_entrega + "'";

                try { entregascheck = scalarExecute(sqlentregascheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (entregascheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_entrega);
                    lvwItems.Refresh();

                    sqldepentregas = "insert into " + base_destino + "..Entregas select * from " + base_origen + "..Entregas where cod_entrega='" + cod_entrega + "'; insert into " + base_destino + "..EntregasTmplates select * from " + base_origen + "..EntregasTmplates where cod_entrega='" + cod_entrega + "'";
                    try
                    {
                        Execute(sqldepentregas, connHist);
                        logger.Info(cod_entrega + " insertada");
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    } 
                }
            }

            DepurarCajasEntregas(dt);
            refreshInfo("Depurando tablas de Entregas...");

            if (depurar == "1")
            {
                foreach (DataRow dr in ds.Tables["Produccion"].Rows)
                {
                    string cod_entrega = dr[0].ToString();
                    string entregascheck = String.Empty;
                    string sqldepentregas = String.Empty;
                    string sqlentregascheck = "select * from " + base_destino + "..Entregas where cod_entrega='" + cod_entrega + "'";
                    
                    //BORRAR
                    logger.Info(cod_entrega + " depurada");

                    try { entregascheck = scalarExecute(sqlentregascheck, connHist); }
                    catch (Exception e) {
                        logger.Info(e.ToString());
                    }

                    if (entregascheck != String.Empty)
                    {
                        //BORRAR
                        logger.Info(cod_entrega + " depurada");

                        sqldepentregas = "delete from " + base_origen + "..EntregasTmplates where cod_entrega='" + cod_entrega + "'";
                        sqldepentregas += "delete from " + base_origen + "..Entregas where cod_entrega='" + cod_entrega + "';";
                        try
                        {
                            Execute(sqldepentregas, connHist);
                        }
                        catch (Exception e)
                        {
                            logger.Info(e.ToString());
                        } 
                    }
                }
            }
            logger.Info("Depuracion Entregas terminada");
            lvwItems.Clear();

        }

        private  void DepurarCajasEntregas(DataSet ds)
        {
            refreshInfo("Depurando tablas de CajasEntregas...");

            verificarTablas(base_origen, base_destino, "CajasEntregas");

            foreach (DataRow dr in ds.Tables["Produccion"].Rows)
            {
                string cod_caja = dr[0].ToString();
                string Cajascheck = String.Empty;
                string sqldepCajas = String.Empty;
                string sqlCajascheck = "select * from " + base_destino + "..CajasEntregas where cod_caja='" + cod_caja + "'";


                try { Cajascheck = scalarExecute(sqlCajascheck, connHist); }
                catch (Exception e) {
                    logger.Info(e.ToString());
                }

                if (Cajascheck == String.Empty)
                {
                    lvwItems.Items.Add(cod_caja);
                    lvwItems.Refresh();

                    try
                    {
                        // borramos primero de CajasEntregas x la fk
                        sqldepCajas = "insert into " + base_destino + "..CajasEntregas select * from " + base_origen + "..CajasEntregas where cod_caja='" + cod_caja + "'";
                        Execute(sqldepCajas, connHist);
                        logger.Info(cod_caja + " fue insertado - CAJASENTREGAS");

                        if (depurar == "1")
                        {
                            sqldepCajas = "delete from " + base_origen + "..CajasEntregas where cod_caja='" + cod_caja + "';";
                            Execute(sqldepCajas, connHist);
                            logger.Info(cod_caja + " fue depurado - CAJASENTREGAS");
                        }
                    }
                    catch (Exception e){
                        logger.Info(e.ToString());
                    }
                    
                }
            }
            logger.Info("Depuracion CajasEntregas terminada");
        }

        private  void insertarTabla(string tabla)
        {
            refreshInfo("Insertando tablas de Tmplates...");
            connProd.Open();
            string sql = "select * from " + base_origen + ".." + tabla;
            connHist.Open();
            DataSet hist = CrearDataSet(sql, connProd);

            if (tabla == "tmplates")
            {
                verificarTablas(base_origen, base_destino, "tmplates");
                insertarTmplates(hist);
            }
            else
            {
                verificarTablas(base_origen, base_destino, "Empresas");
                insertarEmpresas(hist);
            }

            connHist.Close();
            connProd.Close();
        }

        private  void insertarTmplates(DataSet tmplates)
        {
            string base_origen = ConfigurationManager.AppSettings["base_origen"];
            string base_destino = ConfigurationManager.AppSettings["base_destino"];

            foreach (DataRow dr in tmplates.Tables["Produccion"].Rows)
            {
                string tmpl = dr[0].ToString();
                string sql = "if exists (select td.* from " + base_destino + "..tmplates td where td.tmplate = '" + tmpl + "') ";
                sql += "update " + base_destino + "..tmplates set ";
                sql += "descripcio = '" + dr[1].ToString() + "', tipoindex = '" + dr[2].ToString() + "', periodica = '" + dr[3].ToString() + "', nro_zonas = '" + dr[4].ToString() + "', tipo = '" + dr[5].ToString() + "', parent = '" + dr[6].ToString() + "', datasrc = '" + dr[7].ToString() + "', ocr = '" + dr[8].ToString() + "', produccion = '" + dr[9].ToString() + "', LoginEmpresa = '" + dr[10].ToString() + "', BillingType = '" + dr[11].ToString() + "', usaMergeDatos = '" + dr[12].ToString() + "', diasprocesoWarning = '" + dr[13].ToString() + "', diasprocesoError = '" + dr[14].ToString() + "', firmadigi = '" + dr[15].ToString() + "', SeteosCaptura = '" + dr[16].ToString() + "', FormatoCapturaBN = '" + dr[17].ToString() + "', FormatoCapturaGrey = '" + dr[18].ToString() + "', FormatoCapturaColor = '" + dr[19].ToString() + "', orderImagenes = '" + dr[20].ToString() + "', cajasMensuales = '" + dr[21].ToString() + "', seteosIndexacion = '" + dr[22].ToString() + "'";
                sql += " where tmplate = '" + tmpl + "'";
                sql += "else insert into " + base_destino + "..tmplates select * from " + base_origen + "..tmplates where tmplate = '" + tmpl + "'";
                try
                {
                    Execute(sql, connProd);
                }
                catch (Exception e) {
                    logger.Info(e.ToString());
                } 
            }
        }

        private  void insertarEmpresas(DataSet empresas)
        {
            foreach (DataRow dr in empresas.Tables["Produccion"].Rows)
            {
                string logEmp = dr[0].ToString();
                Object obj = (dr[4].ToString() == "01/01/1900 12:00:00 a.m.") ? null : dr[4].ToString();
                string sql = "if exists (select e.* from " + base_destino + "..Empresas e where e.LoginEmpresa = '" + logEmp + "') ";
                sql += "update " + base_destino + "..Empresas set ";
                sql += "DescEmpresa = '" + dr[1].ToString() + "', NroCli = '" + dr[2].ToString() + "', CUIT = '" + dr[3].ToString() + "', perInicio = '" + obj + "', perDuracion = '" + dr[5].ToString() + "', DomCalle = '" + dr[6].ToString() + "', DomNumero = '" + dr[7].ToString() + "', DomPiso = '" + dr[8].ToString() + "', DomLocalidad = '" + dr[9].ToString() + "', DomProvincia = '" + dr[10].ToString() + "', DomCP = '" + dr[11].ToString() + "', ConApellido = '" + dr[12].ToString() + "', ConNombre = '" + dr[13].ToString() + "', ConCargo = '" + dr[14].ToString() + "', ConTelefono = '" + dr[15].ToString() + "', ConInterno = '" + dr[16].ToString() + "', ConFax = '" + dr[17].ToString() + "', ConEmail = '" + dr[18].ToString() + "', Estado = '" + dr[19].ToString() + "'";
                sql += " where LoginEmpresa = '" + logEmp + "'";
                sql += "else insert into " + base_destino + "..Empresas select * from " + base_origen + "..Empresas where LoginEmpresa = '" + logEmp + "'";
                try
                {
                    Execute(sql, connProd);
                }
                catch (Exception e)
                {
                    logger.Info(e.ToString());
                } 
            }
        }

        private  void DepurarTemplates()
        {
            refreshInfo("Depurando tablas de Tmplates...");
            DataSet tmplates = new DataSet();
            tmplates = GetTmplates();

            foreach (DataRow dr in tmplates.Tables["Produccion"].Rows)
            {
                string Row = dr[0].ToString();
                connProd.Open();
                connHist.Open();

                string sqlcheck = "SELECT count(*) FROM " + base_destino + "..sysobjects WHERE xtype='u' AND name='" + Row + "'";
                //string sql = "IF NOT EXISTS (SELECT 1 FROM EasyDocNet_Historico..sysobjects WHERE xtype='u' AND name='"+ Row +"')  select *  into EasyDocNet_Historico.." + Row + " from EasyDocNet.." + Row + "ELSE insert into EasyDocNet_Historico.." + Row + " select * from EasyDocNet.." + Row;                
                SqlCommand cmd = new SqlCommand(sqlcheck, connProd);

                verificarTablas(base_origen, base_destino, Row);

                int i = Convert.ToInt16(cmd.ExecuteScalar());

                if (i == 0)
                {
                    string sql = "select z.* into " + base_destino + ".." + Row + " from " + base_origen + ".." + Row + " z, " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..idlotes p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = p.cod_lote and z.COD_LOTE = p.cod_lote";
                    //string sql = "select *  into EasyDocNet_Historico.." + Row + " from EasyDocNet.." + Row;
                    try
                    {
                        Execute(sql, connProd);
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    } 

                }
                else
                {
                    string sql = "insert into " + base_destino + ".." + Row + " select z.* from " + base_origen + ".." + Row + " z, " + base_origen + "..Entregas e, " + base_origen + "..CajasEntregas ce, " + base_origen + "..LotesCajas lc, " + base_origen + "..idlotes p where not e.FechaBajada is null and datediff(d,e.FechaBajada,GetDate()) >= " + diasdepuracion + " and e.cod_entrega = ce.cod_entrega and ce.cod_caja = lc.cod_caja and lc.cod_lote = p.cod_lote and z.COD_LOTE = p.cod_lote and not exists (select * from " + base_destino + ".." + Row + " where cod_lote = z.cod_lote)";
                    //string sql = "select *  into EasyDocNet_Historico.." + Row + " from EasyDocNet.." + Row;
                    try
                    {
                        Execute(sql, connProd);
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    }
                }

                if (depurar == "1")
                {
                    string sql = "delete z from " + base_origen + ".." + Row + " z where exists (select d.* from " + base_destino + ".." + Row + " d where z.cod_lote = d.cod_lote)";
                    try
                    {
                        Execute(sql, connProd);
                    }
                    catch (Exception e)
                    {
                        logger.Info(e.ToString());
                    }
                }
                

                connProd.Close();
                connHist.Close();
            }
        }

        private  DataSet GetTmplates()
        {
            connProd.Open();
            string sql = "select * FROM sysobjects WHERE xtype='u' AND name like 'Z%AB' OR name like 'Z%AA'";
            DataSet ds = CrearDataSet(sql, connProd);
            ds.EnforceConstraints = false;
            //Se depuran las AA y las AB
            connProd.Close();

            SqlCommand cmdhist = new SqlCommand(sql, connHist);
            SqlDataAdapter TmplatesHistAdapter = new SqlDataAdapter(cmdhist);

            connProd.Open();
            TmplatesHistAdapter.Fill(ds, "Historico");
            connProd.Close();

            return ds;
        }

        private  void verificarTablas(string bOrigen, string bDestino, string checkQuery)
        {
            //no se puede hacer insert into de una base a otra xq en una las mismas tablas tienen mas campos q en la otra

            //cuento la cantidad de campos de la base origen
            string query = "select COUNT(*) from information_schema.COLUMNS where table_name = '" + checkQuery + "'";
            SqlCommand qcmd = new SqlCommand(query, connProd);
            int cantActu = Convert.ToInt16(qcmd.ExecuteScalar());

            //cuento la cantidad de campos q hay en la base destino
            query = "select COUNT(*) from information_schema.COLUMNS where table_name = '" + checkQuery + "'";
            qcmd = new SqlCommand(query, connHist);
            int cantVal = Convert.ToInt16(qcmd.ExecuteScalar());

            if (cantVal != 0 && cantActu != cantVal)
            {
                logger.Error("Las tablas " + checkQuery + " tienen distinta cantidad de campos");
                this.Close();
                Application.Exit();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Cardinal.EasyApps.ED.ScanIndex.VModel
{
    public class ProcesoWindowsViewModel : Cardinal.EasyApps.ED.Common.ViewModel.ViewModelBase
    {

        public ProcesoWindowsViewModel()
        {
            RefreshInfo();
        }

        private void RefreshInfo()
        {


            Procesos = new ObservableCollection<string>();

            Procesos.Add("Proceso 1");
            Procesos.Add("Proceso 2");
            Procesos.Add("Proceso 3");

            ProcesoSeleccionado = "Proceso 1";
           
        }

        public string ProcesoSeleccionado
        {
            get
            {
                return _procesoSeleccionado;
            }
            set
            {
                if (_procesoSeleccionado != value)
                {
                    _procesoSeleccionado = value;


                    OnNotifyPropertyChanged("ProcesoSeleccionado");
                }
            }
        }
        ObservableCollection<string> _procesos;
        string _procesoSeleccionado;
        public ObservableCollection<string> Procesos
        {
            get
            {
                return _procesos;
            }
            set
            {
                if (_procesos != value)
                {
                    _procesos = value;
                    OnNotifyPropertyChanged("Procesos");
                }
            }
        }
    }
}

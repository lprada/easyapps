﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Cardinal.EasyApps.ED.Model.ScanLocal;
using Cardinal.EasyApps.ED.ScanIndex.Dal;
using System.Windows;
using System.Windows.Controls;


namespace Cardinal.EasyApps.ED.ScanIndex.VModel
{
    public class DiccionarioDatosWindowViewModel : Cardinal.EasyApps.ED.Common.ViewModel.ViewModelBase
    {
        private ScanIndexContext ctx = DalHelper.GetScanIndexContext();

        public DiccionarioDatosWindowViewModel()
        {
            if (!IsDesignTime)
            {
                RefreshInfo();
            }
        }

        private void RefreshInfo()
        {
            Campos = new ObservableCollection<CampoLocal>();




       
            var diccionario = ctx.DiccionarioDatos.ToList();

            foreach (var item in diccionario)
            {

                CampoLocal campo = new CampoLocal();
                campo.CampoId = item.CampoId;
                campo.CantidadCaracteresMaximos = item.CantidadCaracteresMaximos;
                campo.EsObligatorio = item.EsObligatorio;
                campo.MaxValor = item.MaxValor;
                campo.MinValor = item.MinValor;
                campo.Nombre = item.Nombre;
                campo.Tipo = item.Tipo;
                campo.Codigo = item.Codigo;
                campo.Mascara = item.Mascara;

                Campos.Add(campo);
                
            }


            if (Campos.Count > 0)
            {
                CampoSeleccionado = Campos[0];
            }
        }

        public CampoLocal CampoSeleccionado
        {
            get
            {
                return _campoSeleccionado;
            }
            set
            {
                if (_campoSeleccionado != value)
                {
                    _campoSeleccionado = value;

                    OnNotifyPropertyChanged("PlantillaCampoSeleccionado");
                }
            }
        }

        ObservableCollection<CampoLocal> _campos;
        CampoLocal _campoSeleccionado;

        public ObservableCollection<CampoLocal> Campos
        {
            get
            {
                return _campos;
            }
            set
            {
                if (_campos != value)
                {
                    _campos = value;
                    OnNotifyPropertyChanged("Campos");
                }
            }
        }
    }
}


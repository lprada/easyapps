﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.ScanIndex.VModel
{
    public class MainWindoViewModel : Cardinal.EasyApps.ED.Common.ViewModel.ViewModelBase
    {

        public string TituloAplicacion
        {
            get
            {
                System.Reflection.Assembly assem = System.Reflection.Assembly.GetAssembly(typeof(MainWindoViewModel));
                System.Reflection.AssemblyName assemName = assem.GetName();
                Version ver = assemName.Version;
                string version = "(Version {0}.{1}.{2}.{3})";
                version = System.String.Format(version,
                                                 ver.Major,
                                                 ver.Minor,
                                                 ver.Build,
                                                 ver.Revision
                                                );

                return string.Format("ScanIndex {0}", version);

            }
        }
    }
}

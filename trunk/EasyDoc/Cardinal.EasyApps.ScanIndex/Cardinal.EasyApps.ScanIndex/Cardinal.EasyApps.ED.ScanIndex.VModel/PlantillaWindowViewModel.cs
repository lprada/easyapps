﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Cardinal.EasyApps.ED.Model.ScanLocal;

namespace Cardinal.EasyApps.ED.ScanIndex.VModel
{
    public class PlantillaWindowViewModel : Cardinal.EasyApps.ED.Common.ViewModel.ViewModelBase
    {
        public PlantillaWindowViewModel()
        {
            RefreshInfo();
        }

        private void RefreshInfo()
        {
            Plantillas = new ObservableCollection<PlantillaLocal>();

            Plantillas.Add(new PlantillaLocal("1", "Plantillas 1"));
            Plantillas.Add(new PlantillaLocal("1", "Plantillas 1"));
            Plantillas.Add(new PlantillaLocal("1", "Plantillas 1"));

            PlantillaSeleccionada = Plantillas[0];
        }

        public PlantillaLocal PlantillaSeleccionada
        {
            get
            {
                return _plantillaSeleccionada;
            }
            set
            {
                if (_plantillaSeleccionada != value)
                {
                    _plantillaSeleccionada = value;

                    OnNotifyPropertyChanged("PlantillaSeleccionada");
                }
            }
        }

        ObservableCollection<PlantillaLocal> _plantillas;
        PlantillaLocal _plantillaSeleccionada;

        public ObservableCollection<PlantillaLocal> Plantillas
        {
            get
            {
                return _plantillas;
            }
            set
            {
                if (_plantillas != value)
                {
                    _plantillas = value;
                    OnNotifyPropertyChanged("Plantillas");
                }
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigSharp;
using System.Data;

namespace Cardinal.EasyApp.sED.ScanIndex.DBUpg
{
    [MigrationExport]
    public class Migration2 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("SeteosScanner")
            .WithPrimaryKeyColumn("SeteosScannerId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("Nombre", DbType.String).OfSize(255)
            .WithNotNullableColumn("MostrarUIEscaneo", DbType.Boolean)
            .WithNotNullableColumn("RotacionSoftware", DbType.Int16)
            .WithNotNullableColumn("ModoSegundaHoja", DbType.Int16)
            .WithNotNullableColumn("SeteosEscanner", DbType.Binary);
        }
    }
}

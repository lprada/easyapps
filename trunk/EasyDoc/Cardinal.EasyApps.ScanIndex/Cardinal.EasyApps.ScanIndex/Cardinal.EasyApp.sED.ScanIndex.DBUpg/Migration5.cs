﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigSharp;
using System.Data;

namespace Cardinal.EasyApp.sED.ScanIndex.DBUpg
{
    [MigrationExport]
    public class Migration5 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("Lote")
            .WithPrimaryKeyColumn("LoteId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("Nombre", DbType.String).OfSize(255)
            .WithNotNullableColumn("Estado", DbType.Int16)
            .WithNotNullableColumn("ProcesoId", DbType.Int32)
            .WithNotNullableColumn("FechaCreacion", DbType.DateTime).HavingCurrentDateTimeAsDefault();


            //FK
            db.Tables["Lote"].AddForeignKeyTo("Proceso", "ProcesoFK").Through("ProcesoId", "ProcesoId");


         
        }
    }
}
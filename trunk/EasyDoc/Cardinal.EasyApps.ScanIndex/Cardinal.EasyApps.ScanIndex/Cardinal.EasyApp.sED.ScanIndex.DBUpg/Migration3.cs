﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigSharp;
using System.Data;

namespace Cardinal.EasyApp.sED.ScanIndex.DBUpg
{
    [MigrationExport]
    public class Migration3 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("DiccionarioDatos")
            .WithPrimaryKeyColumn("CampoId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("Nombre", DbType.String).OfSize(255)
            .WithNotNullableColumn("EsObligatorio", DbType.Boolean)
            .WithNotNullableColumn("Tipo", DbType.Int16)
            .WithNullableColumn("CantidadCaracteresMaximos", DbType.Int32)
            .WithNullableColumn("Mascara", DbType.String).OfSize(255)
            .WithNullableColumn("MinValor", DbType.String).OfSize(255)
            .WithNullableColumn("MaxValor", DbType.String).OfSize(255)
            .WithNullableColumn("Codigo", DbType.String).OfSize(255);

            db.CreateTable("DatosCampoCombo")
            .WithPrimaryKeyColumn("CampoId", DbType.Int32)
            .WithPrimaryKeyColumn("Numero", DbType.Int32)
            .WithNotNullableColumn("Descripcion", DbType.String).OfSize(255)
            .WithNotNullableColumn("CodigoPublicacion", DbType.String).OfSize(255);

            //Diccionario de Datos
            db.Execute("INSERT INTO DiccionarioDatos(Nombre,Tipo,Mascara,MinValor,MaxValor,Codigo,EsObligatorio) values ('Campo EasyDoc',1,'Aaaaa',null,null,'PRUEBA','False')");
            db.Execute("INSERT INTO DiccionarioDatos(Nombre,Tipo,Mascara,MinValor,MaxValor,Codigo,EsObligatorio) values ('Campo Texto',1,'Aaaaa',null,null,'PRUEBA','False')");
            db.Execute("INSERT INTO DiccionarioDatos(Nombre,Tipo,Mascara,MinValor,MaxValor,Codigo,EsObligatorio) values ('Campo Fecha',2,null,null,null,'PRUEBA','False')");
            db.Execute("INSERT INTO DiccionarioDatos(Nombre,Tipo,Mascara,MinValor,MaxValor,Codigo,EsObligatorio) values ('Campo Numerico',4,'#9999',null,null,'PRUEBA','False')");
            db.Execute("INSERT INTO DiccionarioDatos(Nombre,Tipo,Mascara,MinValor,MaxValor,Codigo,EsObligatorio) values ('Campo Combo',3,null,null,null,'PRUEBA','False')");

            //Valores Combo
            db.Execute("INSERT INTO DatosCampoCombo(CampoId,Numero,Descripcion,CodigoPublicacion) values (5,1,'Valor Combo 1','1')");
            db.Execute("INSERT INTO DatosCampoCombo(CampoId,Numero,Descripcion,CodigoPublicacion) values (5,2,'Valor Combo 2','2')");
            db.Execute("INSERT INTO DatosCampoCombo(CampoId,Numero,Descripcion,CodigoPublicacion) values (5,3,'Valor Combo 3','3')");
            db.Execute("INSERT INTO DatosCampoCombo(CampoId,Numero,Descripcion,CodigoPublicacion) values (5,4,'Valor Combo 4','4')");
        }
    }
}
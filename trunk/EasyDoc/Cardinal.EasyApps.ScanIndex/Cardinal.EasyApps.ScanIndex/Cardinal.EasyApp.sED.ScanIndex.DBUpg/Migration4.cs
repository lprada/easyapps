﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigSharp;
using System.Data;

namespace Cardinal.EasyApp.sED.ScanIndex.DBUpg
{
    [MigrationExport]
    public class Migration4 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("Plantilla")
            .WithPrimaryKeyColumn("PlantillaId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("Descripcion", DbType.String).OfSize(255)
            .WithNotNullableColumn("Codigo", DbType.String).OfSize(255);

            db.CreateTable("PlantillaCampo")
            .WithPrimaryKeyColumn("PlantillaId", DbType.Int32)
            .WithPrimaryKeyColumn("CampoId", DbType.Int32)
            .WithNotNullableColumn("Orden", DbType.Int16);

            db.CreateTable("PlantillaProceso")
            .WithPrimaryKeyColumn("ProcesoId", DbType.Int32)
            .WithPrimaryKeyColumn("PlantillaId", DbType.Int32);

            //FK
            db.Tables["PlantillaCampo"].AddForeignKeyTo("Plantilla", "PlantillaFK").Through("PlantillaId", "PlantillaId");
            db.Tables["PlantillaCampo"].AddForeignKeyTo("DiccionarioDatos", "DiccionarioDatosFK").Through("CampoId", "CampoId");

            db.Tables["PlantillaProceso"].AddForeignKeyTo("Proceso", "ProcesoFK").Through("ProcesoId", "ProcesoId");
            db.Tables["PlantillaProceso"].AddForeignKeyTo("Plantilla", "PlantillaFK").Through("PlantillaId", "PlantillaId");


            //Plantillas
            db.Execute("INSERT INTO Plantilla(Descripcion,Codigo) values ('Prueba A','ZPRUEB')");
            db.Execute("INSERT INTO Plantilla(Descripcion,Codigo) values ('Prueba B','ZPRUEB')");
            db.Execute("INSERT INTO Plantilla(Descripcion,Codigo) values ('Prueba EasyDoc','ZAAAAA')");

            //Campos Plnatillas
            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (1,2,1)");
            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (1,3,1)");
            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (1,4,1)");
            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (1,5,1)");

            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (3,1,1)");

            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (2,2,1)");
            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (2,3,1)");
            db.Execute("INSERT INTO PlantillaCampo(PlantillaId,CampoId,Orden) values (2,4,1)");
        }
    }
}
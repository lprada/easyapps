﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigSharp;
using System.Data;

namespace Cardinal.EasyApp.sED.ScanIndex.DBUpg
{
    [MigrationExport]
    public class Migration1 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("Proceso")
            .WithPrimaryKeyColumn("ProcesoId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("Descripcion", DbType.String).OfSize(255)
            .WithNotNullableColumn("Nombre", DbType.String).OfSize(255)
            .WithNotNullableColumn("NombreLoteId", DbType.Guid)
            .WithNotNullableColumn("ProcesamientoImagenesId", DbType.Guid)
            .WithNotNullableColumn("PublicacionId", DbType.Guid);
        }
    }
}

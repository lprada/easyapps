﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MigSharp;
using System.Data;

namespace Cardinal.EasyApp.sED.ScanIndex.DBUpg
{
    [MigrationExport]
    public class Migration6 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("Imagen")
            .WithPrimaryKeyColumn("ImagenId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("LoteId", DbType.Int32)
            .WithNotNullableColumn("Numero", DbType.Int32)
            .WithNullableColumn("Dato", DbType.Binary)
            .WithNotNullableColumn("Extension", DbType.String).OfSize(10).HavingDefault("TIF")
            .WithNotNullableColumn("Borrada", DbType.Boolean)
            .WithNotNullableColumn("Nivel", DbType.Int16).HavingDefault<Int16>(0)
            .WithNotNullableColumn("DatoOriginal", DbType.Binary)
            .WithNotNullableColumn("PlantillaId", DbType.Int32);

            db.CreateTable("ImagenDatos")
            .WithPrimaryKeyColumn("ImagenId", DbType.Int32)
            .WithPrimaryKeyColumn("CampoId", DbType.Int32)
            .WithNotNullableColumn("Valor", DbType.String).OfSize(500);

            db.CreateTable("ImagenDatosCB")
            .WithPrimaryKeyColumn("ImagenDatosCBId", DbType.Int32).AsIdentity()
            .WithNotNullableColumn("ImagenId", DbType.Int32)
            .WithNotNullableColumn("Key", DbType.String).OfSize(500)
            .WithNotNullableColumn("Value", DbType.String).OfSize(500);

            //FK
            db.Tables["Imagen"].AddForeignKeyTo("Lote", "LoteFK").Through("LoteId", "LoteId");

            db.Tables["ImagenDatos"].AddForeignKeyTo("Imagen", "ImagenFK").Through("ImagenId", "ImagenId");
            db.Tables["ImagenDatos"].AddForeignKeyTo("DiccionarioDatos", "DiccionarioDatosFK").Through("CampoId", "CampoId");

            db.Tables["ImagenDatosCB"].AddForeignKeyTo("Imagen", "ImagenFK").Through("ImagenId", "ImagenId");
        }
    }
}
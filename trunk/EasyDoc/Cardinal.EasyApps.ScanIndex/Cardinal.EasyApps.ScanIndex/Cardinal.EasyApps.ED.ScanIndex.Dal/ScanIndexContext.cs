﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Cardinal.EasyApps.ED.Model.ScanLocal;

namespace Cardinal.EasyApps.ED.ScanIndex.Dal
{
    /// <summary>
    /// Context para ScanIndex
    /// </summary>
   public class ScanIndexContext : DbContext
    {
        /// <summary>
        /// Gets or sets the diccionario datos.
        /// </summary>
        /// <value>
        /// The diccionario datos.
        /// </value>
        public DbSet<CampoLocal> DiccionarioDatos { get; set; }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CampoLocal>().ToTable("DiccionarioDatos");
            modelBuilder.Entity<CampoLocal>().HasKey(p => p.CampoId);
            modelBuilder.Entity<CampoLocal>().Ignore(p => p.TipoCampo);

        }

    }
}

﻿#pragma checksum "..\..\..\ProcesosWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "0B819E8B660DDA93265E88326B1FAC5C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.431
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Cardinal.EasyApps.ED.ScanIndex.VModel;
using Cardinal.EasyApps.ED.ScanIndex.Windows;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.RibbonBar;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Data;
using Telerik.Windows.Shapes;


namespace Cardinal.EasyApps.ED.ScanIndex.Windows {
    
    
    /// <summary>
    /// ProcesosWindow
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class ProcesosWindow : Telerik.Windows.Controls.RadWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grilla;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox ProcesoCombo;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NombreProceso;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CambiarNombre;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditarProceso;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EliminarProceso;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AgregarNuevo;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabControl tabsProcesos;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem TabCaptura;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem TabProcesamiento;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem TabPublicacion;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel AccionesPanel;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GrabarSeteo;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\ProcesosWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelarSeteo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Cardinal.EasyApps.ED.ScanIndex.Windows;component/procesoswindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\ProcesosWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.grilla = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.ProcesoCombo = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 4:
            this.NombreProceso = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.CambiarNombre = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.EditarProceso = ((System.Windows.Controls.Button)(target));
            return;
            case 7:
            this.EliminarProceso = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.AgregarNuevo = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.tabsProcesos = ((Telerik.Windows.Controls.RadTabControl)(target));
            return;
            case 10:
            this.TabCaptura = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            case 11:
            this.TabProcesamiento = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            case 12:
            this.TabPublicacion = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            case 13:
            this.AccionesPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 14:
            this.GrabarSeteo = ((System.Windows.Controls.Button)(target));
            return;
            case 15:
            this.CancelarSeteo = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}


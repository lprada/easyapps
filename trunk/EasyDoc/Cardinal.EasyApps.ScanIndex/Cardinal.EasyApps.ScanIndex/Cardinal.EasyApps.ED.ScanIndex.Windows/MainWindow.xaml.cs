﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cardinal.EasyApps.ED.ScanIndex.VModel;
using MigSharp;
using System.Configuration;
using Telerik.Windows.Controls;
using System.Diagnostics;

namespace Cardinal.EasyApps.ED.ScanIndex.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Telerik.Windows.Controls.RadRibbonWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-AR");
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-AR");

           
        }

        private void salirButton_Click(object sender, RoutedEventArgs e)
        {
            CerrarAplicacion();
        }
  
        private void CerrarAplicacion()
        {
            Application.Current.Shutdown();
        }

        private void verProcesosButton_Click(object sender, RoutedEventArgs e)
        {
            ProcesosWindow win = new ProcesosWindow();
            

            win.ShowDialog();
        }

        private Assembly MigratorDBAssembly = typeof(Cardinal.EasyApp.sED.ScanIndex.DBUpg.Migration1).Assembly;

        private void RadRibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var migrator = GetDBMigrator();
            if (!migrator.IsUpToDate(MigratorDBAssembly))
            {
               var migrations = migrator.FetchMigrations(MigratorDBAssembly);
                DialogParameters parameters = new DialogParameters();
                parameters.Content = String.Format("Es Necesario hacer {0} Modificacion/es en la Base de Datos, quiere realizarlo?",migrations.Count);
                parameters.DialogStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                parameters.Closed = this.OnClosed;
                RadWindow.Confirm(parameters);
            }
        }

        private static Migrator GetDBMigrator()
        {
            var connString = ConfigurationManager.ConnectionStrings["ScanIndexContext"].ConnectionString;
            var migrator = new Migrator(connString, ProviderNames.SqlServerCe4);
            return migrator;
        }

        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            if (e.DialogResult.HasValue && e.DialogResult.Value)
            {
                try
                {
                    var migrator = GetDBMigrator();
                    migrator.MigrateAll(MigratorDBAssembly);
                }
                catch(Exception ex)
                {
                    RadWindow.Alert(String.Format("Error al Actualizar Base de Datos {0}. Cerrando Aplicacion.", ex.Message));
                    CerrarAplicacion();
                }
            }
            else
            {
                RadWindow.Alert("Cerrando Aplicacion. Base de Datos Desactualizada.");
                CerrarAplicacion();
            }
        }

        private void verPlantillasButton_Click(object sender, RoutedEventArgs e)
        {
            PlantillasWindow win = new PlantillasWindow();


            win.ShowDialog();
        }

        private void verDiccionarioDAtosButton_Click(object sender, RoutedEventArgs e)
        {
            DiccionarioDatosWindow win = new DiccionarioDatosWindow();


            win.ShowDialog();
        }
    }
}

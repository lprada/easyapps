﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.ScanIndex.Windows.Proceso.PlugIns
{
    public interface IPublicacion
    {
        string Descripcion
        {
            get;
        }
        Guid GetId();
        //void PublicarLote(Common.Dto.LoteDto lote, Common.ILCPScanLiteDalRepository repository);
    }
}

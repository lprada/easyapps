﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Cardinal.EasyApps.ED.ScanIndex.Windows.Proceso.PlugIns
{
    public static class PlugInHelper
    {
        public static PlugInsList GetPlugInsList(string pathDefault, string path)
        {
            PlugInsList res = new PlugInsList();

            //Cargo las Default
            string dllPublicName = System.IO.Path.Combine(pathDefault, "LCP.ScanLite.Publicacion.EasyDocWS.dll");
            string dllPName = System.IO.Path.Combine(pathDefault, "LCP.ScanLite.ProcesamientoImagenes.WF4.dll");
            string dllNName = System.IO.Path.Combine(pathDefault, "LCP.ScanLite.Dal.EntityFramework.dll");
            ProcesarFile(res, dllPublicName);
            ProcesarFile(res, dllPName);
            ProcesarFile(res, dllNName);

            List<string> files = new List<string>();
            bool exists = System.IO.Directory.Exists(@path);
            if (exists)
            {
                FileSearch(path, files);
                DirSearch(path, files);
                foreach (var file in files)
                {
                    ProcesarFile(res, file);
                }
            }
            return res;

        }

        private static void ProcesarFile(PlugInsList res, string file)
        {
            Assembly asm = Assembly.LoadFrom(file);
            foreach (Type type in asm.GetTypes())
            {
                if (type != null)
                {
                    TestProcesamientoImagen(res, type);
                    TestNombreLote(res, type);
                    TestPublicacion(res, type);

                }
            }
        }

        private static void TestPublicacion(PlugInsList res, Type type)
        {
            try
            {
                IPublicacion asmPublicacion = Activator.CreateInstance(type) as IPublicacion;
                if (asmPublicacion != null)
                {
                    res.PublicacionPlugIn.Add(asmPublicacion);
                }
            }
            //catch (AccessViolationException ex)
            //{

            //}
            //catch (Exception e)
            //{

            //}
            catch
            {
            }
        }
        private static void TestNombreLote(PlugInsList res, Type type)
        {
            try
            {
                INombreLote asmNombreLote = Activator.CreateInstance(type) as INombreLote;
                if (asmNombreLote != null)
                {
                    res.NombreLotesPlugIn.Add(asmNombreLote);
                }
            }
            catch
            {

            }
            //catch (AccessViolationException ex)
            //{
            //    string a = "";
            //}
            //catch (Exception e)
            //{
            //    string a = "";
            //}
        }

        private static void TestProcesamientoImagen(PlugInsList res, Type type)
        {
            try
            {
                IProcesamientoImagen asmProcesamientoImagen = Activator.CreateInstance(type) as IProcesamientoImagen;
                if (asmProcesamientoImagen != null)
                {
                    res.ProcesamientoImagenesPlugIn.Add(asmProcesamientoImagen);
                }
            }
            catch
            {
            }
            //catch (AccessViolationException ex)
            //{
            //    string a = "";
            //}
            //catch (Exception e)
            //{
            //    string a = "";
            //}
        }


        private static void DirSearch(string sDir, List<string> files)
        {

            foreach (string d in System.IO.Directory.GetDirectories(sDir))
            {
                FileSearch(d, files);
                DirSearch(d, files);
            }

        }

        private static void FileSearch(string d, List<string> files)
        {
            foreach (string f in System.IO.Directory.GetFiles(d, "*.dll"))
            {

                if (!f.Contains("Csla.dll"))
                {

                    files.Add(f);
                }
            }
        }


        public static IProcesamientoImagen GetProcesamientoImagen(Guid nombreLoteId, PlugInsList listaPlugIns)
        {
            return GetProcesamientoImagen(nombreLoteId, listaPlugIns.ProcesamientoImagenesPlugIn);
        }

        public static IProcesamientoImagen GetProcesamientoImagen(Guid nombreLoteId, List<IProcesamientoImagen> lista)
        {
            foreach (var item in lista)
            {
                if (item.GetId().Equals(nombreLoteId))
                {
                    return item;
                }
            }
            return null;
        }


        public static INombreLote GetNombreLote(Guid nombreLoteId, PlugInsList listaPlugIns)
        {
            return GetNombreLote(nombreLoteId, listaPlugIns.NombreLotesPlugIn);
        }

        public static INombreLote GetNombreLote(Guid nombreLoteId, List<INombreLote> lista)
        {
            foreach (var item in lista)
            {
                if (item.GetId().Equals(nombreLoteId))
                {
                    return item;
                }
            }
            return null;
        }
    }
}

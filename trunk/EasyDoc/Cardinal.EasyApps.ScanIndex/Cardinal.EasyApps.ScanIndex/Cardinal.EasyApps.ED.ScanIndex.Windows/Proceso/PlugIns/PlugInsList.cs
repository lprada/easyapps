﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.ScanIndex.Windows.Proceso.PlugIns
{
    public class PlugInsList
    {
        public List<IPublicacion> PublicacionPlugIn { get; set; }
        public List<IProcesamientoImagen> ProcesamientoImagenesPlugIn { get; set; }
        public List<INombreLote> NombreLotesPlugIn { get; set; }

        public PlugInsList()
        {
            ProcesamientoImagenesPlugIn = new List<IProcesamientoImagen>();
            NombreLotesPlugIn = new List<INombreLote>();
            PublicacionPlugIn = new List<IPublicacion>();
        }
    }
}

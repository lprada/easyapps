﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.ScanIndex.Windows.Proceso.PlugIns
{
    public interface IProcesamientoImagen
    {

        string Descripcion
        {
            get;
        }
        Guid GetId();
        IDictionary<string, object> ProcesarImagen(string file);

        System.Windows.Controls.UserControl GetUserControlMonitoreo();
        void ClearUserControl();

       // void ProcesarLote(Common.Dto.LoteDto lote, Common.ILCPScanLiteDalRepository repository);

    }
}

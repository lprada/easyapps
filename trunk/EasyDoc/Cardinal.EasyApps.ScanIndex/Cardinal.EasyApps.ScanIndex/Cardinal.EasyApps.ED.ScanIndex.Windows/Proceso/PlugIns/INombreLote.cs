﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.EasyApps.ED.ScanIndex.Windows.Proceso.PlugIns
{
    public interface INombreLote
    {
        string GetProximoNombreLote(string puestoId);
        string Descripcion
        {
            get;
        }
        Guid GetId();
    }
}

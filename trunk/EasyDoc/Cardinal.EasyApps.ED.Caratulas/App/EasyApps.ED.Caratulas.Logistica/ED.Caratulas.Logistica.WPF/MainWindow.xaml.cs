﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using System.ComponentModel;
using System.Configuration;

namespace ED.Caratulas.Logistica.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadRibbonWindow
    {
        private Cardinal.EasyApps.ED.Model.LoginAuth _loginAuth;

        private string apiUrl = string.Empty;
        private string apiVersion = string.Empty;

        public MainWindow()
        {
            InitializeComponent();
            System.Reflection.Assembly assem = System.Reflection.Assembly.GetAssembly(typeof(MainWindow));
            System.Reflection.AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;
            string version = "(Version {0}.{1}.{2}.{3})";
            version = System.String.Format(version,
                                           ver.Major,
                                           ver.Minor,
                                           ver.Build,
                                           ver.Revision);

            ribbon.ApplicationName = String.Format("EasyDoc ({0})", version);
        }

        private void CerrarAplicacion()
        {
            Application.Current.Shutdown();
        }

        private void RadRibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                var appConfig = ConfigurationManager.AppSettings;

                apiUrl = appConfig["ApiUrl"];
                apiVersion = appConfig["ApiVersion"];

                Cardinal.EasyApps.ED.Api.Caratulas.Access.Auth authApi =
                new Cardinal.EasyApps.ED.Api.Caratulas.Access.Auth(apiUrl, apiVersion);

                string token = string.Empty;
                int cantProcesos = 0;

                do
                {
                    Cardinal.EasyApps.ED.WindowsControls.Auth.LoginForm form = new Cardinal.EasyApps.ED.WindowsControls.Auth.LoginForm();

                    bool resDialog = form.ShowDialog() == System.Windows.Forms.DialogResult.OK;
                    if (resDialog)
                    {
                        token = authApi.GetLoginToken(form.UserName, form.Password);
                    }
                    else
                    {
                        token = string.Empty;
                        cantProcesos = 100;
                    }
                    cantProcesos++;
                    if (String.IsNullOrWhiteSpace(token) && resDialog)
                    {
                        MessageBox.Show("Ingreso denegado. Vuelva a intentar.");
                    }
                }
                while (String.IsNullOrWhiteSpace(token) && cantProcesos < 3);
                if (String.IsNullOrWhiteSpace(token))
                {
                    MessageBox.Show("Ingreso denegado. Cerrando Aplicacion");
                    CerrarAplicacion();
                }
                _loginAuth = authApi.LoginToken;
                RefreshDatos();
            }
        }

        private void ribbon_SelectedTabChanged(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            RefreshDatos();
        }

        private void RefreshDatos()
        {
            RadRibbonBar ribbonBar = this.ribbon;
            if (ribbonBar.SelectedTab == null)
            {
                ribbonBar.SelectedTab = this.inboundTab;
            }

            RadRibbonTab currentlySelectedTab = ribbonBar.SelectedTab;
            detalleGrid.Visibility = System.Windows.Visibility.Collapsed;
            inboundGrid.Visibility = System.Windows.Visibility.Collapsed;
            switch (currentlySelectedTab.Name)
            {
                case "inboundTab":
                    LoadInbound();
                    break;
                case "outboundTab":

                    break;

                case "detalleTab":
                    LoadCaratulas();
                    break;
                default:

                    break;
            }
        }

        private void LoadCaratulas()
        {
            Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
            new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);

            var res = caratulaApi.GetCaratulas();
            detalleRadGridView.ItemsSource = res;
            detalleGrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void LoadInbound()
        {
            Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
            new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);

            var res = caratulaApi.GetInbound(30);
            detalleRadGridView.ItemsSource = res;
            detalleGrid.Visibility = System.Windows.Visibility.Visible;
        }

        private void RadRibbonButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshDatos();
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula caratulaApi =
            new Cardinal.EasyApps.ED.Api.Caratulas.Access.Caratula(apiUrl, apiVersion, _loginAuth);

            RealizarInboundRadForm form = new RealizarInboundRadForm();
            while (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //Realizo Inbound
                caratulaApi.RealizarInbound(form.NumeroCaratula);
                form = new RealizarInboundRadForm();
            }

            RefreshDatos();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;

namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LogService" in code, svc and config file together.
    public class LogService : ILogService
    {

        public void LogOperation(TB_LOGS logDTO)
        {
            using (CardinalEntities ctx = new CardinalEntities())
            {
                ctx.AddObject("TB_LOGS", logDTO);
                ctx.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;



namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICardinaServices" in both code and config file together.
    [ServiceContract]
    public interface ICardinaServices
    {
        //[OperationContract]
        //List<TB_EMPRESAS> getEmpresas();

        [OperationContract]
        List<EmpresaJSON> getEmpresasJSON(Token _token);

        //[OperationContract]
        //List<Plantilla> getPlantillasByEmpresa(int codEmpresa);

        [OperationContract]
        List<PlantillaJSON> getPlantillasByEmpresaJSON(Token _token, string codEmpresa);

        [OperationContract]
        List<CamposJSON> getCamposByPlantillaJSON(Token _token, string codPlantilla);

        [OperationContract]
        RessultMessage saveDocumento(DocumentoEmpresa documento);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cardinal.Model;


namespace Cardinal.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "HitoricoService" in code, svc and config file together.
    public class HitoricoService : IHitoricoService
    {

        public List<HitoricoCS> GetHistoricoByUsuario(string nroUsuario)
        {
            List<Model.HitoricoCS> query = new List<Model.HitoricoCS>();

            using (CardinalEntities ctx = new CardinalEntities())
            {
                query = (from empresa in ctx.TB_EMPRESA_DOCUMENTOS
                             join documentos in ctx.TB_DOCUMENTO on empresa.CD_CODIGO_DOCUMENTO equals documentos.CD_CODIGO_DOCUMENTO
                             where empresa.CD_USUARIO_CARGA == nroUsuario
                             where empresa.FL_ESTADO_DOCUMENTO == "1"
                             select new HitoricoCS
                             {
                                 codigo = documentos.CD_CODIGO_DOCUMENTO,
                                 fecha = empresa.FC_CREACION,
                                 empresa = empresa.CD_EMPRESA,
                                 plantilla = documentos.TX_PLANTILLA,
                                 nombre_empresa = empresa.TX_EMPRESA,
                                 usuario = empresa.CD_USUARIO_CARGA,
                             }).Distinct().OrderByDescending(empresa => empresa.fecha).ToList();
                

                for (int i = 0; i < query.Count; i++)
                {
                    query[i].campos_valores = GetCamposByDocumento(query[i].codigo);
                }
            }
            return query;           
        }

        public List<CamposCS> GetCamposByDocumento(decimal nroDocumento)
        {
            using (CardinalEntities ctx = new CardinalEntities())
            {
                var query = (from documentos in ctx.TB_DOCUMENTO
                             where documentos.CD_CODIGO_DOCUMENTO == nroDocumento
                             select new CamposCS
                             {
                                 documento = documentos.CD_CODIGO_DOCUMENTO,
                                 campo = documentos.TX_CAMPO,
                                 valor = documentos.VL_CAMPO,
                             }).ToList();
                return query;
            }
        }

        public bool AnularCaratulaCliente(int id_caratula)
        {
            using (CardinalEntities ctx = new CardinalEntities())
            {
                var query = from doc in ctx.TB_EMPRESA_DOCUMENTOS
                            where doc.CD_CODIGO_DOCUMENTO == id_caratula
                            select doc;

                foreach (TB_EMPRESA_DOCUMENTOS doc in query)
                {
                    doc.FL_ESTADO_DOCUMENTO = "0";
                }

                try
                {
                    ctx.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
        }
    }
}
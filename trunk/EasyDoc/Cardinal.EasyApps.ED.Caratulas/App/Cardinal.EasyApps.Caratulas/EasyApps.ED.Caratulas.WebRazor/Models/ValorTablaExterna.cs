﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cardinal.Caratulas.Models
{
    public class ValorTablaExterna
    {
        public static List<ValorTablaExterna> GetListaValoresTablaExterna(List<KeyValuePair<string, string>> valoresPosibles)
        {
            List<ValorTablaExterna> res = new List<ValorTablaExterna>();
            foreach (var item in valoresPosibles)
            {
                res.Add(new ValorTablaExterna(item.Key, item.Value));
                
            }

            return res;
        }

        public ValorTablaExterna()
        {
          
        }
        public ValorTablaExterna(string key, string value)
        {
            Key = key;
            Value = value;
        }
         
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
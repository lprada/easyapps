﻿
using System.Web.Mvc;

using System.Web.Security;
using Cardinal.Caratulas.Models;


using Cardinal.EasyApps.ED.Model;
using MvcMiniProfiler;

namespace Cardinal.Caratulas.Controllers
{
    public class AccountController : Controller
    {

     
       
        public ActionResult LogOn()
        {
            MiniProfiler profiler = MiniProfiler.Current;
            using (profiler.Step("LogOn Get"))
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            MiniProfiler profiler = MiniProfiler.Current;
            using (profiler.Step("LogOn Post"))
            {
                if (ModelState.IsValid)
                {
                    using (profiler.Step("Autenticate JSON"))
                    {
                         LoginAuth loginAuth = ConexionServiciosED.AuthenticateJSON(model.UserName, model.Password);

                         if (loginAuth.Result == "OK")
                        {
                            using (profiler.Step("Autenticate OK"))
                            {
                                #region SaveTokenSession
                                Session["TokenTicket"] = loginAuth;
                                Session["UserName"] = model.UserName;
                                #endregion
                                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                                {
                                    return Redirect(returnUrl);
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Home");
                                }
                            }
                        }
                        else
                        {
                            using (profiler.Step("Autenticate FAIL"))
                            {
                                ModelState.AddModelError("", "El nombre de usuaro o contraseña es incorrecta.");
                            }
                        }
                    }


                }

                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("LogOn", "Account");
        }


     
    }
}

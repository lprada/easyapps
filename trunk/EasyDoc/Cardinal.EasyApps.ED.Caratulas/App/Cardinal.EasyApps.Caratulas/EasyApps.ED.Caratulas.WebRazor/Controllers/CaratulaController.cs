﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Collections;
using Telerik.Web.Mvc;
using System.Text;
using Telerik.Reporting.Processing;
using Cardinal.EasyApps.ED.Model;
using Cardinal.EasyApps.ED.Common.Dal;
using System.Configuration;

namespace Cardinal.Caratulas.Controllers
{
    [Authorize]
    public class CaratulaController : Controller
    {
        Cardinal.EasyApps.ED.Common.Dal.IRepository _repository;

        public CaratulaController(IRepository repository)
        {
            _repository = repository;
        }

        public JsonResult _GetPlantillas(string codEmpresa)
        {
            var plantillas = ConexionServiciosED.GetPlantillasByEmpresaJSON((LoginAuth)Session["TokenTicket"], codEmpresa);

            return Json(new SelectList(plantillas, "CodigoPlantilla", "Nombre"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.Controles = "";

            // SelectList lista = new SelectList(, "CodigoCliente", "Nombre");
            ViewBag.Empresas = ConexionServiciosED.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
            Session["EmpresasJSON"] = ViewBag.Empresas;
         
            _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se consulto" });
     
            // ViewBag.PlantillaPorEmpresaServiceJSONURL = ConfigurationManager.AppSettings["PlantillaPorEmpresaServiceJSON"].ToString();
            return View();
        }

        public ActionResult GetCamposPlantilla(string plantillaId)
        {
            var camposPlantillas = ConexionServiciosED.GetCamposByPlantillaJSON((LoginAuth)Session["TokenTicket"], plantillaId);
            return PartialView("_CamposPlantilla", camposPlantillas);
        }

        [HttpPost]
        public ActionResult CreateCaratula()
        {
            if (string.IsNullOrWhiteSpace(Request.Form["Plantilla"]))
            {
                ModelState.AddModelError("Plantilla", "Por favor Seleccione una Plantilla.");
            }

            //Valido Campos

            var plantillaId = Request.Form["Plantilla"];
            var empresaId = Request.Form["Empresa"];

            var plantilla =
            ConexionServiciosED.GetPlantillasByEmpresaJSON((LoginAuth)Session["TokenTicket"], empresaId)
                               .Where(p => p.CodigoPlantilla == plantillaId)
                               .FirstOrDefault();
            var camposPlantillas = ConexionServiciosED.GetCamposByPlantillaJSON((LoginAuth)Session["TokenTicket"], plantillaId);

            foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false))
            {
                if (item.EsObligatorio)
                {
                    var valorCampo = Request.Form[item.CampoId];
                    if (string.IsNullOrWhiteSpace(valorCampo))
                    {
                        ModelState.AddModelError(item.CampoId, String.Format("Por favor ingrese un valor para el campo {0}.", item.Nombre));
                    }
                }
            }

            if (ModelState.IsValid)
            {
                int IDDocumentoMax = 0;

                List<CamposCS> listaCampos = new List<CamposCS>();
                DocumentoEmpresa documento = new DocumentoEmpresa
                {
                    empresa = ConexionServiciosED.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]).Where(p => p.CodigoCliente == empresaId).FirstOrDefault(),
                    estadoDocumento = "1",
                    fechaCreacion = DateTime.Now,
                    usuarioCarga = User.Identity.Name,
                    codigoDocumento = IDDocumentoMax
                };
                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == false))
                {
                    CamposCS campo = new CamposCS
                    {
                        campo = item.CampoId, valor = Request.Form[item.CampoId], txCampo = item.Nombre,
                        EsDetalle = false, OrdenDetalle = 0
                    };
                    listaCampos.Add(campo);
                }

                documento.listaCampos = listaCampos;
                documento.plantilla = plantillaId;
                documento.txPlantilla = plantilla.Nombre;
                documento.codigo3d = generarValores3d(IDDocumentoMax, listaCampos);

                //Cargo los Detalles 
                foreach (var item in camposPlantillas.Where(p => p.EsCampoDetalle == true))
                {
                    CamposCS campo = new CamposCS
                    {
                        campo = item.CampoId,
                        valor = Request.Form[item.CampoId],
                        txCampo = item.Nombre,
                        EsDetalle = true
                    };
                    listaCampos.Add(campo);

                    short i = 1;
                    bool seguir = !String.IsNullOrWhiteSpace(Request.Form[String.Format("{0}{1}", item.CampoId, i)]);
                    while (seguir)
                    {
                        CamposCS campoDetalle = new CamposCS
                        {
                            campo = String.Format("{0}_{1}", item.CampoId, i),
                            valor = Request.Form[String.Format("{0}{1}", item.CampoId, i)],
                            txCampo = item.Nombre,
                            EsDetalle = true,
                            OrdenDetalle = i
                        };
                        listaCampos.Add(campoDetalle);
                        i++;
                        seguir = !String.IsNullOrWhiteSpace(Request.Form[String.Format("{0}{1}", item.CampoId, i)]);
                    }
                }

                var numDoc = _repository.saveDocumento(documento);

                //Session["Documento"] = documento.codigoDocumento;
                _repository.LogOperationCaratulas(new LogCaratula { CD_MOVIMIENTO = 1, CD_USUARIO_MOVIMIENTO = Session["UserName"].ToString(), FC_MOVIMIENTO = DateTime.Now, TX_OBSERVACION = "Se dió de alta el siguiente documento " + documento.codigo3d });
                //return RedirectToAction("Index", "Message", new { message = "El documento fue creado con éxito" });
                return RedirectToAction("VerReporte", "Historico", new { id = numDoc });
            }
            else
            {
                return ErroCargaCaratula();
            }
        }

        private ActionResult ErroCargaCaratula()
        {
            ViewBag.Empresas = ConexionServiciosED.GetEmpresasJSON((LoginAuth)Session["TokenTicket"]);
            Session["EmpresasJSON"] = ViewBag.Empresas;

            //Debo Guardar Empresa, Plantilla

            return View("Create");
        }

        private string generarValores3d(int IDDocumentoMax, List<CamposCS> listaCampos)
        {
            StringBuilder sb3d = new StringBuilder();
            sb3d.Append(IDDocumentoMax);
            sb3d.Append(",");
            foreach (CamposCS item in listaCampos)
            {
                sb3d.Append(item.valor);
                sb3d.Append(",");
            }
            return sb3d.ToString();
        }

        public ActionResult GetPdfCaratula(int caratulaId)
        {
            Cardinal.EasyApps.ED.Caratulas.WebRazor.Reportes.CaratulaReport reporte = new Cardinal.EasyApps.ED.Caratulas.WebRazor.Reportes.CaratulaReport();

           var data = _repository.GetCaratulaVistaPorId(caratulaId);

           reporte.ReportParameters["Empresa"].Value = data.nombre_empresa;
            reporte.ReportParameters["Plantilla"].Value = data.plantilla;
            reporte.ReportParameters["Usuario"].Value = data.usuario;
            reporte.ReportParameters["Fecha"].Value = data.fecha;
         


            reporte.DataSource = data.DetalleCampos;
            MessagingToolkit.QRCode.Codec.QRCodeEncoder qr = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            string str = data.codigo_3d;//(reporte.Items.Find("codigo_3d_not_visible", true)[0] as Telerik.Reporting.TextBox).Value;
            System.Drawing.Bitmap bmp = qr.Encode(str);
            (reporte.Items.Find("picBoxQR", true)[0] as Telerik.Reporting.PictureBox).Value = bmp;

            //BarCode Caratula
            (reporte.Items.Find("barcode2", true)[0] as Telerik.Reporting.Barcode).Value = data.codigo.ToString();

            ReportProcessor reportProcessor = new ReportProcessor();
            RenderingResult result = reportProcessor.RenderReport("PDF", reporte, null);
            return File(result.DocumentBytes, System.Net.Mime.MediaTypeNames.Application.Pdf, String.Format("Caratula {0}.pdf", caratulaId));
        }

       
        public ActionResult GetCaratulaJSON(int caratulaId)
        {
           
            return new JsonResult
            {
                Data = _repository.GetCaratulaVistaPorId(caratulaId),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet

            };
        }



    }
}
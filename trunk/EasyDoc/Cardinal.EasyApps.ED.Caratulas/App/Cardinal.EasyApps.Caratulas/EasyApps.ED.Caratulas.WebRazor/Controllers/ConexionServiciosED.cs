﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cardinal.EasyApps.ED.Model;
using System.Configuration;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Cardinal.Caratulas.Controllers
{
    public static class ConexionServiciosED
    {
        public static  LoginAuth AuthenticateJSON(string username, string password)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["LoginServiceJSON"].ToString());

            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("username={0}&password={1}", username, password);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                JObject o = JObject.Parse(jsonResponse);
                token.Result = (string)o["Result"];
                token.Token = (string)o["Token"];
                reader.Close();
            }
            #endregion

            return token;
        }

        public static List<Cliente> GetEmpresasJSON(LoginAuth _token)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["EmpresaServiceJSON"].ToString());
            List<Cliente> listaEmpresasJSON = new List<Cliente>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}", _token.Token);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listaEmpresasJSON = JsonConvert.DeserializeObject<List<Cliente>>(jsonResponse);
                
                reader.Close();
            }
            #endregion

            return listaEmpresasJSON;
        }

        public static List<Plantilla> GetPlantillasByEmpresaJSON(LoginAuth _token, string codEmpresa)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["PlantillaPorEmpresaServiceJSON"].ToString());
            List<Plantilla> listPlantillasJSON = new List<Plantilla>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}&codigoEmpresa={1}", _token.Token, codEmpresa);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listPlantillasJSON = JsonConvert.DeserializeObject<List<Plantilla>>(jsonResponse);

                reader.Close();
            }
            #endregion

            return listPlantillasJSON;
        }

        public static List<PlantillaCampo> GetCamposByPlantillaJSON(LoginAuth _token, string codPlantilla)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["CamposPlantillaPorEmpresaServiceJSON"].ToString());
            List<PlantillaCampo> listCamposJSON = new List<PlantillaCampo>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}&codigoPlantilla={1}", _token.Token, codPlantilla);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listCamposJSON = JsonConvert.DeserializeObject<List<PlantillaCampo>>(jsonResponse);

                reader.Close();
            }
            #endregion

            return listCamposJSON.OrderBy(x => x.Orden).ToList();
        }


        public static List<TablaExterna> GetTablasExternasJSON(LoginAuth _token)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["TablasExternasJSON"].ToString());
            List<TablaExterna> listaTablasExternasJSON = new List<TablaExterna>();
            #region WebRequest
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}", _token.Token);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
            #endregion

            #region WebResponse [JSON SERIALIZATION]
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                listaTablasExternasJSON = JsonConvert.DeserializeObject<List<TablaExterna>>(jsonResponse);

                reader.Close();
            }
            #endregion

            return listaTablasExternasJSON.ToList();
        }

        public static bool AddValorTablaExterna(LoginAuth _token, string tablaExterna, string campoId, string campoExterno, string key, string value)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["AddValorTablaExterna"].ToString());
           
           
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}&tablaExterna={1}&campoId={2}&campoExterno={3}&key={4}&value={5}", _token.Token, tablaExterna, campoId, campoExterno, key, value);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
           

           
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();
            ResultadoOperacion res = new ResultadoOperacion();
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();
                
                
                res = JsonConvert.DeserializeObject<ResultadoOperacion>(jsonResponse);

                reader.Close();
            }

            return res.Result == "TRUE";
        }

        public static bool EditValorTablaExterna(LoginAuth _token, string tablaExterna, string campoId, string campoExterno, string key, string newValue)
        {
            Uri address = new Uri(ConfigurationManager.AppSettings["EditValorTablaExterna"].ToString());

         
            HttpWebRequest request = WebRequest.Create(address) as HttpWebRequest;
            request.Method = "POST";
            request.UseDefaultCredentials = false;

            request.PreAuthenticate = true;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "application/json";

            string data = string.Format("logintoken={0}&tablaExterna={1}&campoId={2}&campoExterno={3}&key={4}&newValue={5}", _token.Token, tablaExterna, campoId, campoExterno, key, newValue);

            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

            request.ContentLength = byteData.Length;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }
         

          
            string jsonResponse = "";

            // Get response 
            LoginAuth token = new LoginAuth();
            ResultadoOperacion res = new ResultadoOperacion();
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                jsonResponse = reader.ReadToEnd();


                res = JsonConvert.DeserializeObject<ResultadoOperacion>(jsonResponse);

                reader.Close();
            }

            return res.Result == "TRUE";
        }
    }
}
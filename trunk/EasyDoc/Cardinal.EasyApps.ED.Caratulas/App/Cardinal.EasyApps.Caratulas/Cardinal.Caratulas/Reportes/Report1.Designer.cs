namespace Cardinal.Caratulas.Reportes
{
    partial class Report1
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report1));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            this.vL_CAMPOGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.vL_CAMPODataTextBox = new Telerik.Reporting.TextBox();
            this.campoDataTextBox = new Telerik.Reporting.TextBox();
            this.vL_CAMPOGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.vL_CAMPOGroup = new Telerik.Reporting.Group();
            this.campoGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.campoGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.campoGroup = new Telerik.Reporting.Group();
            this.labelsGroupHeader = new Telerik.Reporting.GroupHeaderSection();
            this.labelsGroupFooter = new Telerik.Reporting.GroupFooterSection();
            this.labelsGroup = new Telerik.Reporting.Group();
            this.pageHeader = new Telerik.Reporting.PageHeaderSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.pageFooter = new Telerik.Reporting.PageFooterSection();
            this.currentTimeTextBox = new Telerik.Reporting.TextBox();
            this.pageInfoTextBox = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.reportHeader = new Telerik.Reporting.ReportHeaderSection();
            this.cD_EMPRESACaptionTextBox = new Telerik.Reporting.TextBox();
            this.cD_EMPRESADataTextBox = new Telerik.Reporting.TextBox();
            this.cD_USUARIO_CARGACaptionTextBox = new Telerik.Reporting.TextBox();
            this.cD_USUARIO_CARGADataTextBox = new Telerik.Reporting.TextBox();
            this.fC_CREACIONCaptionTextBox = new Telerik.Reporting.TextBox();
            this.fC_CREACIONDataTextBox = new Telerik.Reporting.TextBox();
            this.desc_plantillaCaptionTextBox = new Telerik.Reporting.TextBox();
            this.desc_plantillaDataTextBox = new Telerik.Reporting.TextBox();
            this.barcode1 = new Telerik.Reporting.Barcode();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.reportFooter = new Telerik.Reporting.ReportFooterSection();
            this.detail = new Telerik.Reporting.DetailSection();
            this.entityDataSource1 = new Telerik.Reporting.EntityDataSource();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // vL_CAMPOGroupHeader
            // 
            this.vL_CAMPOGroupHeader.Height = new Telerik.Reporting.Drawing.Unit(0.40007877349853516D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.vL_CAMPOGroupHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.vL_CAMPODataTextBox,
            this.campoDataTextBox});
            this.vL_CAMPOGroupHeader.Name = "vL_CAMPOGroupHeader";
            this.vL_CAMPOGroupHeader.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.vL_CAMPOGroupHeader.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.vL_CAMPOGroupHeader.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // vL_CAMPODataTextBox
            // 
            this.vL_CAMPODataTextBox.CanGrow = true;
            this.vL_CAMPODataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.7000000476837158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.093331336975097656D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.vL_CAMPODataTextBox.Name = "vL_CAMPODataTextBox";
            this.vL_CAMPODataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.0999996662139893D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.vL_CAMPODataTextBox.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.vL_CAMPODataTextBox.Style.Font.Bold = true;
            this.vL_CAMPODataTextBox.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            this.vL_CAMPODataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.vL_CAMPODataTextBox.StyleName = "Data";
            this.vL_CAMPODataTextBox.Value = "=Fields.VL_CAMPO";
            // 
            // campoDataTextBox
            // 
            this.campoDataTextBox.CanGrow = true;
            this.campoDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.campoDataTextBox.Name = "campoDataTextBox";
            this.campoDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1000000238418579D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.campoDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.campoDataTextBox.StyleName = "Data";
            this.campoDataTextBox.Value = "=Fields.campo";
            // 
            // vL_CAMPOGroupFooter
            // 
            this.vL_CAMPOGroupFooter.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.vL_CAMPOGroupFooter.Name = "vL_CAMPOGroupFooter";
            // 
            // vL_CAMPOGroup
            // 
            this.vL_CAMPOGroup.GroupFooter = this.vL_CAMPOGroupFooter;
            this.vL_CAMPOGroup.GroupHeader = this.vL_CAMPOGroupHeader;
            this.vL_CAMPOGroup.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.VL_CAMPO")});
            this.vL_CAMPOGroup.Name = "vL_CAMPOGroup";
            // 
            // campoGroupHeader
            // 
            this.campoGroupHeader.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.campoGroupHeader.Name = "campoGroupHeader";
            this.campoGroupHeader.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // campoGroupFooter
            // 
            this.campoGroupFooter.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.campoGroupFooter.Name = "campoGroupFooter";
            this.campoGroupFooter.Style.Visible = true;
            // 
            // campoGroup
            // 
            this.campoGroup.GroupFooter = this.campoGroupFooter;
            this.campoGroup.GroupHeader = this.campoGroupHeader;
            this.campoGroup.Groupings.AddRange(new Telerik.Reporting.Data.Grouping[] {
            new Telerik.Reporting.Data.Grouping("=Fields.campo")});
            this.campoGroup.Name = "campoGroup";
            // 
            // labelsGroupHeader
            // 
            this.labelsGroupHeader.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.labelsGroupHeader.Name = "labelsGroupHeader";
            this.labelsGroupHeader.PrintOnEveryPage = true;
            // 
            // labelsGroupFooter
            // 
            this.labelsGroupFooter.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.labelsGroupFooter.Name = "labelsGroupFooter";
            this.labelsGroupFooter.Style.Visible = false;
            // 
            // labelsGroup
            // 
            this.labelsGroup.GroupFooter = this.labelsGroupFooter;
            this.labelsGroup.GroupHeader = this.labelsGroupHeader;
            this.labelsGroup.Name = "labelsGroup";
            // 
            // pageHeader
            // 
            this.pageHeader.Height = new Telerik.Reporting.Drawing.Unit(0.90000009536743164D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.pageHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1});
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Style.BackgroundImage.MimeType = "";
            this.pageHeader.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.pageHeader.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // pageFooter
            // 
            this.pageFooter.Height = new Telerik.Reporting.Drawing.Unit(1.3999220132827759D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.pageFooter.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.currentTimeTextBox,
            this.pageInfoTextBox,
            this.pictureBox2,
            this.pictureBox3});
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // currentTimeTextBox
            // 
            this.currentTimeTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.62083339691162109D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.59992188215255737D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.currentTimeTextBox.Name = "currentTimeTextBox";
            this.currentTimeTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5791667699813843D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.currentTimeTextBox.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.currentTimeTextBox.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.currentTimeTextBox.StyleName = "PageInfo";
            this.currentTimeTextBox.Value = "=NOW()";
            // 
            // pageInfoTextBox
            // 
            this.pageInfoTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.320833683013916D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.59992188215255737D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pageInfoTextBox.Name = "pageInfoTextBox";
            this.pageInfoTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5791667699813843D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pageInfoTextBox.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.pageInfoTextBox.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.pageInfoTextBox.StyleName = "PageInfo";
            this.pageInfoTextBox.Value = "=PageNumber";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.0416666679084301D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3019653856754303D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(5.9791665077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19795608520507813D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.899921715259552D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(6D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3354949951171875D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // reportHeader
            // 
            this.reportHeader.Height = new Telerik.Reporting.Drawing.Unit(2.6000001430511475D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.reportHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.cD_EMPRESACaptionTextBox,
            this.cD_EMPRESADataTextBox,
            this.cD_USUARIO_CARGACaptionTextBox,
            this.cD_USUARIO_CARGADataTextBox,
            this.fC_CREACIONCaptionTextBox,
            this.fC_CREACIONDataTextBox,
            this.desc_plantillaCaptionTextBox,
            this.desc_plantillaDataTextBox,
            this.barcode1,
            this.pictureBox4});
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // cD_EMPRESACaptionTextBox
            // 
            this.cD_EMPRESACaptionTextBox.CanGrow = true;
            this.cD_EMPRESACaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.0187500715255737D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_EMPRESACaptionTextBox.Name = "cD_EMPRESACaptionTextBox";
            this.cD_EMPRESACaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.2791669368743896D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_EMPRESACaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.cD_EMPRESACaptionTextBox.StyleName = "Caption";
            this.cD_EMPRESACaptionTextBox.Value = "Empresa:";
            // 
            // cD_EMPRESADataTextBox
            // 
            this.cD_EMPRESADataTextBox.CanGrow = true;
            this.cD_EMPRESADataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.0187500715255737D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_EMPRESADataTextBox.Name = "cD_EMPRESADataTextBox";
            this.cD_EMPRESADataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.4000003337860107D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_EMPRESADataTextBox.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.cD_EMPRESADataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.cD_EMPRESADataTextBox.StyleName = "Data";
            this.cD_EMPRESADataTextBox.Value = "=Fields.CD_EMPRESA";
            // 
            // cD_USUARIO_CARGACaptionTextBox
            // 
            this.cD_USUARIO_CARGACaptionTextBox.CanGrow = true;
            this.cD_USUARIO_CARGACaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.3001575469970703D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_USUARIO_CARGACaptionTextBox.Name = "cD_USUARIO_CARGACaptionTextBox";
            this.cD_USUARIO_CARGACaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.7791666984558105D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_USUARIO_CARGACaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.cD_USUARIO_CARGACaptionTextBox.StyleName = "Caption";
            this.cD_USUARIO_CARGACaptionTextBox.Value = "Usuario:";
            // 
            // cD_USUARIO_CARGADataTextBox
            // 
            this.cD_USUARIO_CARGADataTextBox.CanGrow = true;
            this.cD_USUARIO_CARGADataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.3001575469970703D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_USUARIO_CARGADataTextBox.Name = "cD_USUARIO_CARGADataTextBox";
            this.cD_USUARIO_CARGADataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.9000003337860107D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.cD_USUARIO_CARGADataTextBox.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.cD_USUARIO_CARGADataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.cD_USUARIO_CARGADataTextBox.StyleName = "Data";
            this.cD_USUARIO_CARGADataTextBox.Value = "=Fields.CD_USUARIO_CARGA";
            // 
            // fC_CREACIONCaptionTextBox
            // 
            this.fC_CREACIONCaptionTextBox.CanGrow = true;
            this.fC_CREACIONCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.6001577377319336D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.fC_CREACIONCaptionTextBox.Name = "fC_CREACIONCaptionTextBox";
            this.fC_CREACIONCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.1979167461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.fC_CREACIONCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fC_CREACIONCaptionTextBox.StyleName = "Caption";
            this.fC_CREACIONCaptionTextBox.Value = "Fecha de creaci�n:";
            // 
            // fC_CREACIONDataTextBox
            // 
            this.fC_CREACIONDataTextBox.CanGrow = true;
            this.fC_CREACIONDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.4000003337860107D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.6001577377319336D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.fC_CREACIONDataTextBox.Name = "fC_CREACIONDataTextBox";
            this.fC_CREACIONDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.fC_CREACIONDataTextBox.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.fC_CREACIONDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.fC_CREACIONDataTextBox.StyleName = "Data";
            this.fC_CREACIONDataTextBox.Value = "=Fields.FC_CREACION";
            // 
            // desc_plantillaCaptionTextBox
            // 
            this.desc_plantillaCaptionTextBox.CanGrow = true;
            this.desc_plantillaCaptionTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.59999996423721313D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30015754699707031D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.desc_plantillaCaptionTextBox.Name = "desc_plantillaCaptionTextBox";
            this.desc_plantillaCaptionTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.79992121458053589D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.desc_plantillaCaptionTextBox.Style.BackgroundColor = System.Drawing.Color.White;
            this.desc_plantillaCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.desc_plantillaCaptionTextBox.StyleName = "Caption";
            this.desc_plantillaCaptionTextBox.Value = "Plantilla tipo:";
            // 
            // desc_plantillaDataTextBox
            // 
            this.desc_plantillaDataTextBox.CanGrow = true;
            this.desc_plantillaDataTextBox.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30015739798545837D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.desc_plantillaDataTextBox.Name = "desc_plantillaDataTextBox";
            this.desc_plantillaDataTextBox.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3000003099441528D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.desc_plantillaDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.desc_plantillaDataTextBox.StyleName = "Data";
            this.desc_plantillaDataTextBox.Value = "=Fields.desc_plantilla";
            // 
            // barcode1
            // 
            this.barcode1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.3000006675720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.barcode1.Name = "barcode1";
            this.barcode1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5999997854232788D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.barcode1.Value = "= Fields.CD_CODIGO_DOCUMENTO";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(2.3000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch));
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(5.8791670799255371D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19996070861816406D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // reportFooter
            // 
            this.reportFooter.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.reportFooter.Name = "reportFooter";
            // 
            // detail
            // 
            this.detail.Height = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.detail.Name = "detail";
            // 
            // entityDataSource1
            // 
            this.entityDataSource1.ConnectionString = "DB_CardinalEntities2";
            this.entityDataSource1.Name = "entityDataSource1";
            this.entityDataSource1.ObjectContext = typeof(Cardinal.Caratulas.Models.DB_CardinalEntities);
            this.entityDataSource1.ObjectContextMember = "VistaCaratulas";
            // 
            // Report1
            // 
            this.DataSource = this.entityDataSource1;
            this.Groups.AddRange(new Telerik.Reporting.Group[] {
            this.vL_CAMPOGroup,
            this.campoGroup,
            this.labelsGroup});
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.vL_CAMPOGroupHeader,
            this.vL_CAMPOGroupFooter,
            this.campoGroupHeader,
            this.campoGroupFooter,
            this.labelsGroupHeader,
            this.labelsGroupFooter,
            this.pageHeader,
            this.pageFooter,
            this.reportHeader,
            this.reportFooter,
            this.detail});
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Title")});
            styleRule1.Style.Color = System.Drawing.Color.Black;
            styleRule1.Style.Font.Bold = true;
            styleRule1.Style.Font.Italic = false;
            styleRule1.Style.Font.Name = "Cambria";
            styleRule1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(20D, Telerik.Reporting.Drawing.UnitType.Point);
            styleRule1.Style.Font.Strikeout = false;
            styleRule1.Style.Font.Underline = false;
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Caption")});
            styleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(167)))), ((int)(((byte)(227)))));
            styleRule2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(34)))), ((int)(((byte)(77)))));
            styleRule2.Style.Font.Bold = true;
            styleRule2.Style.Font.Italic = false;
            styleRule2.Style.Font.Name = "Calibri";
            styleRule2.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(9D, Telerik.Reporting.Drawing.UnitType.Point);
            styleRule2.Style.Font.Strikeout = false;
            styleRule2.Style.Font.Underline = false;
            styleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Data")});
            styleRule3.Style.Font.Name = "Calibri";
            styleRule3.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(9D, Telerik.Reporting.Drawing.UnitType.Point);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("PageInfo")});
            styleRule4.Style.Font.Name = "Calibri";
            styleRule4.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            styleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.Width = new Telerik.Reporting.Drawing.Unit(6.0208334922790527D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.EntityDataSource entityDataSource1;
        private Telerik.Reporting.GroupHeaderSection vL_CAMPOGroupHeader;
        private Telerik.Reporting.TextBox vL_CAMPODataTextBox;
        private Telerik.Reporting.GroupFooterSection vL_CAMPOGroupFooter;
        private Telerik.Reporting.Group vL_CAMPOGroup;
        private Telerik.Reporting.GroupHeaderSection campoGroupHeader;
        private Telerik.Reporting.TextBox campoDataTextBox;
        private Telerik.Reporting.GroupFooterSection campoGroupFooter;
        private Telerik.Reporting.Group campoGroup;
        private Telerik.Reporting.GroupHeaderSection labelsGroupHeader;
        private Telerik.Reporting.GroupFooterSection labelsGroupFooter;
        private Telerik.Reporting.Group labelsGroup;
        private Telerik.Reporting.PageHeaderSection pageHeader;
        private Telerik.Reporting.PageFooterSection pageFooter;
        private Telerik.Reporting.TextBox currentTimeTextBox;
        private Telerik.Reporting.TextBox pageInfoTextBox;
        private Telerik.Reporting.ReportHeaderSection reportHeader;
        private Telerik.Reporting.TextBox cD_EMPRESACaptionTextBox;
        private Telerik.Reporting.TextBox cD_EMPRESADataTextBox;
        private Telerik.Reporting.TextBox cD_USUARIO_CARGACaptionTextBox;
        private Telerik.Reporting.TextBox cD_USUARIO_CARGADataTextBox;
        private Telerik.Reporting.TextBox fC_CREACIONCaptionTextBox;
        private Telerik.Reporting.TextBox fC_CREACIONDataTextBox;
        private Telerik.Reporting.TextBox desc_plantillaCaptionTextBox;
        private Telerik.Reporting.TextBox desc_plantillaDataTextBox;
        private Telerik.Reporting.ReportFooterSection reportFooter;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.Barcode barcode1;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.PictureBox pictureBox1;

    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Bienvenido al EasyDoc Caratulas</h2>
    <p>
        Para conocer mas acerca de la empresa <a href="http://www.cardinalsystems.com.ar" title="Cardinal Systems">http://www.cardinalsystems.com.ar</a>.
    </p>
</asp:Content>
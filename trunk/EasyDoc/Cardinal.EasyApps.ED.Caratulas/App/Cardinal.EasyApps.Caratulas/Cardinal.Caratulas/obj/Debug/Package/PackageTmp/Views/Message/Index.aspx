﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<style>
.message
{
    background:#D0F0C7;
    border:1px solid black;
    color: Black;
    font-family: Calibri;
    padding:10px;
}
</style>
<h2>Mensajes</h2><br />
<div class="message">
<script type="text/javascript">
    window.open('../Historico/VerReporte/' + <%=ViewBag.Documento %>, "Reporte", "width=1024,height=768,scrollbars=yes");
</script>
<%=ViewBag.Message %>
<p> <%= Html.ActionLink("Ver reporte", "../Historico/VerReporte", new { id = ViewBag.Documento })%> </p>


</div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<List<Cardinal.EasyApps.ED.Model.TablaExterna>>" UICulture="es-ES" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
  VerTablasExternas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <h2>
    Ver Tablas Externas
  </h2>

  <br />
 

  <% 
      Html.Telerik().Grid(Model)
       .Name("Grid")
       .DataKeys(keys => { keys.Add(o => o.Nombre); keys.Add(o => o.CampoId); keys.Add(o => o.CampoExterno); })
       .Columns(columns =>
       {
           columns.Bound(o => o.Nombre).Title("Nombre");
           columns.Bound(o => o.CampoId).Title("Campo Id");
           columns.Bound(o => o.CampoExterno).Title("Campo Externo");
           columns.Template(o =>
 {
    %>
    <%= Html.ActionLink("Ver Datos", "VerDatosTablaExterna", "TablasExternas", new { tablaExterna = o.Nombre, campoId = o.CampoId, campoExterno = o.CampoExterno }, null)%>
    <%
 }).Title("Acciones");
       })
       .Pageable(paging => paging.PageSize(25))
       .Sortable()
       .Render(); 
%>
</asp:Content>

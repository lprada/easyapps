﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Cardinal.Caratulas.Models;

using Newtonsoft.Json.Linq;
using Cardinal.EasyApps.ED.Model;
using MvcMiniProfiler;

namespace Cardinal.Caratulas.Controllers
{
    public class AccountController : Controller
    {

     
       
        public ActionResult LogOn()
        {
            MiniProfiler profiler = MiniProfiler.Current;
            using (profiler.Step("LogOn Get"))
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            MiniProfiler profiler = MiniProfiler.Current;
            using (profiler.Step("LogOn Post"))
            {
                if (ModelState.IsValid)
                {
                    using (profiler.Step("Autenticate JSON"))
                    {
                        Token token = new Token();
                        token = ConexionServiciosED.AuthenticateJSON(model.UserName, model.Password);

                        if (token.Result == "OK")
                        {
                            using (profiler.Step("Autenticate OK"))
                            {
                                #region SaveTokenSession
                                Session["TokenTicket"] = token;
                                Session["UserName"] = model.UserName;
                                #endregion
                                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                                {
                                    return Redirect(returnUrl);
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Home");
                                }
                            }
                        }
                        else
                        {
                            using (profiler.Step("Autenticate FAIL"))
                            {
                                ModelState.AddModelError("", "El nombre de usuaro o contraseña es incorrecta.");
                            }
                        }
                    }


                }

                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("LogOn", "Account");
        }


     
    }
}

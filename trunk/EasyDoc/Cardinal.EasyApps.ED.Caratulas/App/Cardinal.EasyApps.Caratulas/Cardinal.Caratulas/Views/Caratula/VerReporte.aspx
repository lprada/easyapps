﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register assembly="Telerik.ReportViewer.WebForms, Version=5.0.11.316, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" namespace="Telerik.ReportViewer.WebForms" tagprefix="telerik" %>
<%@ Import  Namespace="Cardinal.Caratulas.CaratulaWCF" %>

<form clientidmode="Static" id="frep" runat="server">

<telerik:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="800px">
</telerik:ReportViewer>
</form>

<script runat="server">

	public override void VerifyRenderingInServerForm(Control control)
	{

	}

	protected override void OnLoad(EventArgs e)
	{
        if (!Page.IsPostBack)
        {
            int parametro = Convert.ToInt32(ViewContext.RouteData.Values["id"].ToString());

            CaratulaServiceClient srvCaratulasWCF = new CaratulaServiceClient();
            Cardinal.Caratulas.Reportes.Report4 reporte = new Cardinal.Caratulas.Reportes.Report4();
            reporte.DataSource = srvCaratulasWCF.GetCaratulaVistaPorId(parametro);
            ReportViewer1.Report = reporte;
        }
        
		base.OnLoad(e);
	}
    
</script>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
    public  class CaratulaCS
    {
        public string empresa { get; set; }
        public string nombre_empresa { get; set; }
        public string usuario { get; set; }
        public DateTime fecha { get; set; }
        public decimal codigo { get; set; }
        public string campo { get; set; }
        public string valor { get; set; }
        public string plantilla { get; set; }
        public string codigo_3d { get; set; }
    }
}

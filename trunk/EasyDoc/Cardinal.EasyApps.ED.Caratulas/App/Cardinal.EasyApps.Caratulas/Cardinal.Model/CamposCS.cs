﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
    public class CamposCS
    {
        public decimal documento { get; set; }
        public string campo { get; set; }
        public string valor { get; set; }
        public string txCampo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
    [Serializable]
    public class Token
    {
        public string Result { get; set; }
        public string TokenTicket { get; set; }
    }
}

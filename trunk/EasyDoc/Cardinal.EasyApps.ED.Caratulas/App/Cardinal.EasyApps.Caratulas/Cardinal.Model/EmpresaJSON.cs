﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Cardinal.Model
{
    [Serializable]
    public class EmpresaJSON
    {
        [Required(ErrorMessage="Seleccione una empresa")]
        public string CodigoCliente { get; set; }

        public string Nombre { get; set; }
       
    }
}

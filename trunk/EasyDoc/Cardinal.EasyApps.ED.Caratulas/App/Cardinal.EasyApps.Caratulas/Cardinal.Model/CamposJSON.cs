﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cardinal.Model
{
    public class CamposJSON
    {
        public string CampoId { get; set; }
        public string Nombre { get; set; }
        public int Orden { get; set; }
        public bool EsObligatorio { get; set; }
        public int CantidadCaracteresMaximos { get; set; }
        public int? MaxValor { get; set; }
        public int? MinValor { get; set; }
        List<String> ValoresPosibles { get; set; }
        public int TipoCampo { get; set; }
    }
}

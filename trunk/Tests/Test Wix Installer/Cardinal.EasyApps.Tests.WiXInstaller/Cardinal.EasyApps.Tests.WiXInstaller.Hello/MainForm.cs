﻿using System.Windows.Forms;

namespace Cardinal.EasyApps.Tests.WiXInstaller.Hello
{
    /// <summary>
    /// Main Form
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            Mensaje msg = new Mensaje();
            this.label1.Text = msg.GetMensajeBienvenida();
        }
    }
}
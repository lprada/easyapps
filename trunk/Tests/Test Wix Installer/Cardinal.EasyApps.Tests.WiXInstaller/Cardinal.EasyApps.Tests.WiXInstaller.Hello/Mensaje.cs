﻿using System;

namespace Cardinal.EasyApps.Tests.WiXInstaller.Hello
{
    /// <summary>
    /// Clase de Mensajes
    /// </summary>
    public class Mensaje
    {
        /// <summary>
        /// Get Public Mensajes
        /// </summary>
        /// <returns></returns>
        public string GetMensajeBienvenida()
        {
            return String.Format("Hello Word!!! at {0}", DateTime.Now.ToString("MM/dd/yyyy hh:mm:sss"));
        }
    }
}
﻿using System;
using System.Windows.Data;

namespace Cardinal.EasyApps.SolicitudCambio.MainWpf
{
    /// <summary>
    /// Convert para Bool Invirtiendo el Valor
    /// </summary>
    public class BoolInverterConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Convert
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool) && targetType != typeof(bool?))
            {
                throw new InvalidOperationException("The target must be a boolean");
            }
            if (null == value)
            {
                return null;
            }
            return !(bool)value;
        }

        /// <summary>
        /// convert Back
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(bool) && targetType != typeof(bool?))
            {
                throw new InvalidOperationException("The target must be a boolean");
            }
            if (null == value)
            {
                return null;
            }
            return !(bool)value;
        }

        #endregion IValueConverter Members
    }
}
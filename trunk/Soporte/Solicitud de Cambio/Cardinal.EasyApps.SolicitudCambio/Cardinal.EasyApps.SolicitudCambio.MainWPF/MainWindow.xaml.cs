﻿using System.Windows;
using Telerik.Windows.Controls;

namespace Cardinal.EasyApps.SolicitudCambio.MainWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RadRibbonWindow
    {
        /// <summary>
        /// Constructor Default
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RadRibbonButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var mainWindowsViewModel = grilla.DataContext as ViewModel.MainWindowsViewModel;
            if (SolicitudCambio.ViewModel.ValidationHelper.GetValidationErrors(mainWindowsViewModel).Count > 0)
            {
                MessageBox.Show("Imposible Crear La Solicitud, Por Favor revise los errores", "Error Al Generar Solicitud", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            VerReporteWindow win = new VerReporteWindow();
            win.MainWindowsViewModel = mainWindowsViewModel;
            win.ShowDialog();
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            var mainWindowsViewModel = grilla.DataContext as ViewModel.MainWindowsViewModel;
            mainWindowsViewModel.NuevaSolicitud();
        }
    }
}
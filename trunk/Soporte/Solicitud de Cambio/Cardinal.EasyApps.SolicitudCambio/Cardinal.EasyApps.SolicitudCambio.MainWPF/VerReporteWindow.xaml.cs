﻿using System.Windows;

namespace Cardinal.EasyApps.SolicitudCambio.MainWpf
{
    /// <summary>
    /// Interaction logic for VerReporteWindow.xaml
    /// </summary>
    public partial class VerReporteWindow : Window
    {
        /// <summary>
        /// Cronstructor
        /// </summary>
        public VerReporteWindow()
        {
            InitializeComponent();
        }

        internal ViewModel.MainWindowsViewModel MainWindowsViewModel { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            this.reportViewer.Report = MainWindowsViewModel.GetSolicitudCambioReporte();
            this.reportViewer.RefreshReport();
        }
    }
}
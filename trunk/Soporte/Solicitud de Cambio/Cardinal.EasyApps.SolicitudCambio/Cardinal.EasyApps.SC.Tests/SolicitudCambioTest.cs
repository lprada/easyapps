﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cardinal.EasyApps.SolicitudCambio.Tests
{
    /// <summary>
    ///This is a test class for SolicitudCambioTest and is intended
    ///to contain all SolicitudCambioTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SolicitudCambioTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes

        /// <summary>
        ///A test for SolicitudCambio Constructor
        ///</summary>
        [TestMethod()]
        public void SolicitudCambioConstructorTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            Assert.AreEqual(target.FechaSolicitud, DateTime.Now);
            Assert.AreEqual(target.FechaManejadoPorFecha, target.FechaSolicitud.Value.AddDays(30));
            Assert.AreEqual(target.TipoSolicitud, "Error");
            Assert.AreEqual(target.Ambiente, "Desarrollo");
            Assert.AreEqual(target.Sistema, "General");
            Assert.AreEqual(target.Version, "Sin Version");
            Assert.AreEqual(target.PrioridadPropuesta, "Normal");
        }

        /// <summary>
        ///A test for Ambiente
        ///</summary>
        [TestMethod()]
        public void AmbienteDesarrolloTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Desarrollo";
            string actual;
            target.Ambiente = expected;
            actual = target.Ambiente;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Comentarios
        ///</summary>
        [TestMethod()]
        public void ComentariosTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Comentarios";
            string actual;
            target.Comentarios = expected;
            actual = target.Comentarios;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ComportamientoActual
        ///</summary>
        [TestMethod()]
        public void ComportamientoActualTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Comportamiento Actual";
            string actual;
            target.ComportamientoActual = expected;
            actual = target.ComportamientoActual;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ComportamientoPropuesto
        ///</summary>
        [TestMethod()]
        public void ComportamientoPropuestoTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Comportamiento Propuesto";
            string actual;
            target.ComportamientoPropuesto = expected;
            actual = target.ComportamientoPropuesto;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Descripcion
        ///</summary>
        [TestMethod()]
        public void DescripcionTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Descripcion";
            string actual;
            target.Descripcion = expected;
            actual = target.Descripcion;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EmpresaDuenaProceso
        ///</summary>
        [TestMethod()]
        public void EmpresaDuenaProcesoTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Empresa Dueña Proceso";
            string actual;
            target.EmpresaDuenaProceso = expected;
            actual = target.EmpresaDuenaProceso;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EmpresaSolicitante
        ///</summary>
        [TestMethod()]
        public void EmpresaSolicitanteTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Empresa Solicitante";
            string actual;
            target.EmpresaSolicitante = expected;
            actual = target.EmpresaSolicitante;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FechaManejadoPorFecha
        ///</summary>
        [TestMethod()]
        public void FechaManejadoPorFechaTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            Nullable<DateTime> expected = DateTime.Now.AddDays(5);
            Nullable<DateTime> actual;
            target.FechaManejadoPorFecha = expected;
            actual = target.FechaManejadoPorFecha;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FechaSolicitud
        ///</summary>
        [TestMethod()]
        public void FechaSolicitudTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            Nullable<DateTime> expected = DateTime.Now.AddDays(15);
            Nullable<DateTime> actual;
            target.FechaSolicitud = expected;
            actual = target.FechaSolicitud;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ImplementarSinSolucion
        ///</summary>
        [TestMethod()]
        public void ImplementarSinSolucionTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            bool expected = true;
            bool actual;
            target.ImplementarSinSolucion = expected;
            actual = target.ImplementarSinSolucion;
            Assert.AreEqual(expected, actual);
            expected = false;
            target.ImplementarSinSolucion = expected;
            actual = target.ImplementarSinSolucion;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for InfoAdicional
        ///</summary>
        [TestMethod()]
        public void InfoAdicionalTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            bool expected = true;
            bool actual;
            target.InfoAdicional = expected;
            actual = target.InfoAdicional;
            Assert.AreEqual(expected, actual);
            expected = false;
            target.InfoAdicional = expected;
            actual = target.InfoAdicional;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ManejadoPorFecha
        ///</summary>
        [TestMethod()]
        public void ManejadoPorFechaTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            bool expected = true;
            bool actual;
            target.ManejadoPorFecha = expected;
            actual = target.ManejadoPorFecha;
            Assert.AreEqual(expected, actual);
            expected = true;
            target.ManejadoPorFecha = expected;
            actual = target.ManejadoPorFecha;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for NombreOriginador
        ///</summary>
        [TestMethod()]
        public void NombreOriginadorTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Nombre Originador";
            string actual;
            target.NombreOriginador = expected;
            actual = target.NombreOriginador;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonaSolicitante
        ///</summary>
        [TestMethod()]
        public void PersonaSolicitanteTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Persona Solicitante";
            string actual;
            target.PersonaSolicitante = expected;
            actual = target.PersonaSolicitante;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PrioridadPropuesta
        ///</summary>
        [TestMethod()]
        public void PrioridadPropuestaTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Emergencia";
            string actual;
            target.PrioridadPropuesta = expected;
            actual = target.PrioridadPropuesta;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Sistema
        ///</summary>
        [TestMethod()]
        public void SistemaTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "EasyDoc";
            string actual;
            target.Sistema = expected;
            actual = target.Sistema;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for TipoSolicitud
        ///</summary>
        [TestMethod()]
        public void TipoSolicitudTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Cambio";
            string actual;
            target.TipoSolicitud = expected;
            actual = target.TipoSolicitud;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Titulo
        ///</summary>
        [TestMethod()]
        public void TituloTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "Titulo";
            string actual;
            target.Titulo = expected;
            actual = target.Titulo;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Version
        ///</summary>
        [TestMethod()]
        public void VersionTest()
        {
            Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio target = new Cardinal.EasyApps.SolicitudCambio.Model.SolicitudCambio();
            string expected = "2011.2.1.1051";
            string actual;
            target.Version = expected;
            actual = target.Version;
            Assert.AreEqual(expected, actual);
        }
    }
}
﻿using System;
using Cardinal.EasyApps.SolicitudCambio.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cardinal.EasyApps.SolicitudCambio.Tests
{
    /// <summary>
    ///This is a test class for MainWindowsViewModelTest and is intended
    ///to contain all MainWindowsViewModelTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MainWindowsViewModelTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes

        /// <summary>
        ///A test for AmbienteSeleccionado
        ///</summary>
        [TestMethod()]
        public void AmbienteSeleccionadoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            MainWindowsViewModel.Ambiente expected = MainWindowsViewModel.Ambiente.Produccion;
            MainWindowsViewModel.Ambiente actual;
            target.AmbienteSeleccionado = expected;
            actual = target.AmbienteSeleccionado;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Comentarios
        ///</summary>
        [TestMethod()]
        public void ComentariosTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Comentarios";
            string actual;
            target.Comentarios = expected;
            actual = target.Comentarios;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ComportamientoActual
        ///</summary>
        [TestMethod()]
        public void ComportamientoActualTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Comportamiento Actual";
            string actual;
            target.ComportamientoActual = expected;
            actual = target.ComportamientoActual;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ComportamientoPropuesto
        ///</summary>
        [TestMethod()]
        public void ComportamientoPropuestoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Comportamiento Propuesto";
            string actual;
            target.ComportamientoPropuesto = expected;
            actual = target.ComportamientoPropuesto;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Descripcion
        ///</summary>
        [TestMethod()]
        public void DescripcionTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Descripcion";
            string actual;
            target.Descripcion = expected;
            actual = target.Descripcion;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EmpresaDuenaProceso
        ///</summary>
        [TestMethod()]
        public void EmpresaDuenaProcesoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Empresa Dueña Proceso";
            string actual;
            target.EmpresaDuenaProceso = expected;
            actual = target.EmpresaDuenaProceso;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for EmpresaSolicitante
        ///</summary>
        [TestMethod()]
        public void EmpresaSolicitanteTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Empresa Solicitante";
            string actual;
            target.EmpresaSolicitante = expected;
            actual = target.EmpresaSolicitante;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FechaManejadoPorFecha
        ///</summary>
        [TestMethod()]
        public void FechaManejadoPorFechaTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            Nullable<DateTime> expected = DateTime.Now.AddDays(16);
            Nullable<DateTime> actual;
            target.FechaManejadoPorFecha = expected;
            actual = target.FechaManejadoPorFecha;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for FechaSolicitud
        ///</summary>
        [TestMethod()]
        public void FechaSolicitudTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            Nullable<DateTime> expected = DateTime.Now.AddDays(16);
            Nullable<DateTime> actual;
            target.FechaSolicitud = expected;
            actual = target.FechaSolicitud;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ImplementarSinSolucion
        ///</summary>
        [TestMethod()]
        public void ImplementarSinSolucionTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            bool expected = true;
            bool actual;
            target.ImplementarSinSolucion = expected;
            actual = target.ImplementarSinSolucion;
            Assert.AreEqual(expected, actual);
            expected = true;
            target.ImplementarSinSolucion = expected;
            actual = target.ImplementarSinSolucion;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for InfoAdicional
        ///</summary>
        [TestMethod()]
        public void InfoAdicionalTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            bool expected = true;
            bool actual;
            target.InfoAdicional = expected;
            actual = target.InfoAdicional;
            Assert.AreEqual(expected, actual);
            expected = true;
            target.InfoAdicional = expected;
            actual = target.InfoAdicional;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ManejadoPorFecha
        ///</summary>
        [TestMethod()]
        public void ManejadoPorFechaTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            bool expected = true;
            bool actual;
            target.ManejadoPorFecha = expected;
            actual = target.ManejadoPorFecha;
            Assert.AreEqual(expected, actual);
            expected = true;
            target.ManejadoPorFecha = expected;
            actual = target.ManejadoPorFecha;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for NombreOriginador
        ///</summary>
        [TestMethod()]
        public void NombreOriginadorTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Nombre Originador";
            string actual;
            target.NombreOriginador = expected;
            actual = target.NombreOriginador;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PersonaSolicitante
        ///</summary>
        [TestMethod()]
        public void PersonaSolicitanteTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Empresa Solicitante";
            string actual;
            target.PersonaSolicitante = expected;
            actual = target.PersonaSolicitante;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for PrioridadPropuestaSeleccionada
        ///</summary>
        [TestMethod()]
        public void PrioridadPropuestaSeleccionadaTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            MainWindowsViewModel.PrioridadPropuesta expected = MainWindowsViewModel.PrioridadPropuesta.Intermedia;
            MainWindowsViewModel.PrioridadPropuesta actual;
            target.PrioridadPropuestaSeleccionada = expected;
            actual = target.PrioridadPropuestaSeleccionada;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SistemaSeleccionado
        ///</summary>
        [TestMethod()]
        public void SistemaSeleccionadoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            MainWindowsViewModel.SistemasHabilitados expected = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            MainWindowsViewModel.SistemasHabilitados actual;
            target.SistemaSeleccionado = expected;
            actual = target.SistemaSeleccionado;

            expected = MainWindowsViewModel.SistemasHabilitados.EasyArchive;

            target.SistemaSeleccionado = expected;
            actual = target.SistemaSeleccionado;
            Assert.AreEqual(expected, actual);

            expected = MainWindowsViewModel.SistemasHabilitados.General;

            target.SistemaSeleccionado = expected;
            actual = target.SistemaSeleccionado;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for TipoSolicitudSeleccionada
        ///</summary>
        [TestMethod()]
        public void TipoSolicitudSeleccionadaTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            MainWindowsViewModel.TipoSolicitud expected = MainWindowsViewModel.TipoSolicitud.Mejora;
            MainWindowsViewModel.TipoSolicitud actual;
            target.TipoSolicitudSeleccionada = expected;
            actual = target.TipoSolicitudSeleccionada;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Titulo
        ///</summary>
        [TestMethod()]
        public void TituloTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "Titulo";
            string actual;
            target.Titulo = expected;
            actual = target.Titulo;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for VersionSeleccionado
        ///</summary>
        [TestMethod()]
        public void VersionSeleccionadoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            string expected = "2011.1.8.1514";
            string actual;
            target.VersionSeleccionado = expected;
            actual = target.VersionSeleccionado;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionTituloTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = string.Empty;
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionComentariosTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = string.Empty;
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionComportamientoActualTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionComportamientoPropuestoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionDescripcionTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionEmpresaDuenaProcesoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.EmpresaDuenaProceso = string.Empty;
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionEmpresaSolicitanteTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";

            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionFechaManejadoPorFechaTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = null;
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionFechaSolicitudTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = null;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionNombreOriginadorTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.PersonaSolicitante = "Persona Solicitante";
            target.NombreOriginador = string.Empty;
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionPersonaSolicitanteTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetSolicitudCambioReporteExceptionVersionSeleccionadoTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = string.Empty;
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }

        /// <summary>
        ///A test for GetSolicitudCambioReporte
        ///</summary>
        [TestMethod()]
        public void GetSolicitudCambioReporteTest()
        {
            MainWindowsViewModel target = new MainWindowsViewModel();
            target.Titulo = "Titulo";
            target.Comentarios = "Comentario";
            target.ComportamientoActual = "Comportamiento Actual";
            target.ComportamientoPropuesto = "Comportamiento Propuesto";
            target.Descripcion = "Descripcion";
            target.EmpresaDuenaProceso = "Empresa Dueña del Proceso";
            target.EmpresaSolicitante = "Empresa Solicitante";
            target.FechaManejadoPorFecha = DateTime.Now.AddDays(5);
            target.FechaSolicitud = DateTime.Now;
            target.ImplementarSinSolucion = true;
            target.InfoAdicional = true;
            target.ManejadoPorFecha = true;
            target.NombreOriginador = "Nombre Originador";
            target.PersonaSolicitante = "Persona Solicitante";
            target.SistemaSeleccionado = MainWindowsViewModel.SistemasHabilitados.EasyDoc;
            target.TipoSolicitudSeleccionada = MainWindowsViewModel.TipoSolicitud.Error;
            target.VersionSeleccionado = "Sin Version";
            SolicitudDeCambioReport actual = target.GetSolicitudCambioReporte();
            Assert.IsNotNull(actual);
        }
    }
}
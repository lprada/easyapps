﻿using System;
using System.Globalization;

namespace Cardinal.EasyApps.SolicitudCambio.Model
{
    /// <summary>
    /// Modelo de Solicitud de Cambio
    /// </summary>
    public class SolicitudCambio
    {
        /// <summary>
        /// Constructor Default
        /// </summary>
        /// <remarks>Pone la Fecha de Solicitud en DateTime.Now</remarks>
        public SolicitudCambio()
        {
            FechaSolicitud = DateTime.Now;
            FechaManejadoPorFecha = FechaSolicitud.Value.AddDays(30);
            TipoSolicitud = "Error";
            Ambiente = "Desarrollo";
            Sistema = "General";
            Version = "Sin Version";
            PrioridadPropuesta = "Normal";
            NumeroSolicitud = String.Format(CultureInfo.InvariantCulture, "{0}-001", DateTime.Now.ToString("yyyyMMdd",CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Un pequeño titulo que sumariza el proposito del Cambio
        /// </summary>

        public string Titulo { get; set; }

        /// <summary>
        /// Gets or sets the numero solicitud.
        /// </summary>
        /// <value>
        /// The numero solicitud.
        /// </value>
        public string NumeroSolicitud { get; set; }


        /// <summary>
        /// Fecha de cuando el Cambio fue Propuesto
        /// </summary>
        public DateTime? FechaSolicitud { get; set; }

        /// <summary>
        /// Nombre del sistema (Por ejemplo EasyDoc)
        /// </summary>
        public string Sistema { get; set; }

        /// <summary>
        /// Numero de version a la que aplica el cambio (Por ejemplo 7.1.3548)
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// El ambiente donde aplica este cambio.
        /// </summary>
        public string Ambiente { get; set; }

        /// <summary>
        /// Propiedad Propuesta para la Solicitud
        /// </summary>
        public string PrioridadPropuesta { get; set; }

        /// <summary>
        /// Tipo de Solicitud
        /// </summary>
        public string TipoSolicitud { get; set; }

        /// <summary>
        /// Empresa que solicita el cambio
        /// </summary>
        public string EmpresaSolicitante { get; set; }

        /// <summary>
        /// (Si aplica y es distinto de Persona Solicitante). Nombre de la persona que descubrió el error, determino que era necesario el cambio o sugirió la mejora.
        /// </summary>
        public string NombreOriginador { get; set; }

        /// <summary>
        /// Empresa que es "dueña" del proceso que se ve afectado por este cambio. Esta empresa será la responsable de testear el cambio. Solo requerido si es distinto de Empresa Solicitante.
        /// </summary>
        public string EmpresaDuenaProceso { get; set; }

        /// <summary>
        /// Nombre de la persona que completa este formulario
        /// </summary>
        public string PersonaSolicitante { get; set; }

        /// <summary>
        /// Selección "SI" si el cambio, corrección de error o mejora debe ser resuelto en producción antes de cierta fecha.
        /// </summary>
        public bool ManejadoPorFecha { get; set; }

        /// <summary>
        /// Si este es un nuevo cambio no documentado descubierto durante el testeo en QA que no requiere solución antes de pasar a producción.
        /// </summary>
        public bool ImplementarSinSolucion { get; set; }

        /// <summary>
        /// Si pantallas o algún otro documento es adjuntado a esta solicitud. Su envió deberá incluir todas las pantallas en el email que incluye este documento.
        /// </summary>
        public bool InfoAdicional { get; set; }

        /// <summary>
        /// La fecha para cuando es requerido el cambio (Si Aplica).
        /// </summary>
        public DateTime? FechaManejadoPorFecha { get; set; }

        /// <summary>
        /// Descripción detallada del propósito de esta solicitud de cambio. En caso de ser una mejora o un cambio se deberá escribir las respuestas a las siguientes preguntas: el Qué?, el Quién? y el Cómo?, también incluir el porqué y el para qué. De ser necesario por favor incluya como documentación adicional el documento de Solicitud de Requerimiento de Software.
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Comportamiento actual de la aplicación con respecto a cambio solicitado.( Si está reportando un "error", describa los pasos para recrear el problema). Cuando describa como recrear un error, por favor sea especifico e incluya todos los pasos necesarios para reproducir el mismo desde la perspectiva de alguien que no está familiarizado con la aplicación.
        /// </summary>
        public string ComportamientoActual { get; set; }

        /// <summary>
        /// Lo que la aplicación debería hacer luego de que este cambio se haya realizado. Debe ser lo suficiente detallado para que se pueda hacer un plan de testing para certificar la implementación.
        /// </summary>
        public string ComportamientoPropuesto { get; set; }

        /// <summary>
        /// comentario que mejore el entendimiento de la solicitud.
        /// </summary>
        public string Comentarios { get; set; }
    }
}
﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;

namespace Cardinal.EasyApps.SolicitudCambio.ViewModel
{
    /// <summary>
    /// Main Windows ViewModel
    /// </summary>
    public class MainWindowsViewModel : ViewModelBase, IDataErrorInfo
    {
        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations")]
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <returns>The error message for the property. The default is an empty string ("").</returns>
        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                switch (columnName)
                {
                    case "NumeroSolicitud":
                        error = ValidacionTexto(error, NumeroSolicitud);
                        break;
                    case "Titulo":
                        error = ValidacionTexto(error, Titulo);
                        break;
                    case "PrioridadPropuestaSeleccionada":
                        error = ValidacionNull(error, PrioridadPropuestaSeleccionada);
                        break;
                    case "TipoSolicitud":
                        error = ValidacionNull(error, TipoSolicitudSeleccionada);
                        break;
                    case "FechaSolicitud":
                        error = ValidacionNull(error, FechaSolicitud);
                        break;
                    case "SistemaSeleccionado":
                        error = ValidacionNull(error, SistemaSeleccionado);
                        break;
                    case "VersionSeleccionado":
                        error = ValidacionTexto(error, VersionSeleccionado);
                        break;
                    case "AmbienteSeleccionado":
                        error = ValidacionNull(error, AmbienteSeleccionado);
                        break;
                    case "EmpresaSolicitante":
                        error = ValidacionTexto(error, EmpresaSolicitante);
                        break;
                    case "PersonaSolicitante":
                        error = ValidacionTexto(error, PersonaSolicitante);
                        break;
                    case "EmpresaDuenaProceso":
                        error = ValidacionTexto(error, EmpresaDuenaProceso);
                        break;
                    case "NombreOriginador":
                        error = ValidacionTexto(error, NombreOriginador);
                        break;
                    case "ManejadoPorFecha":
                        error = ValidacionNull(error, ManejadoPorFecha);
                        break;
                    case "FechaManejadoPorFecha":
                        if (ManejadoPorFecha)
                        {
                            error = ValidacionNull(error, FechaManejadoPorFecha);
                        }
                        break;
                    case "ImplementarSinSolucion":
                        error = ValidacionNull(error, ImplementarSinSolucion);
                        break;
                    case "InfoAdicional":
                        error = ValidacionNull(error, InfoAdicional);
                        break;
                    case "Descripcion":
                        error = ValidacionTexto(error, Descripcion);
                        break;
                    case "ComportamientoActual":
                        error = ValidacionTexto(error, ComportamientoActual);
                        break;
                    case "ComportamientoPropuesto":
                        error = ValidacionTexto(error, ComportamientoPropuesto);
                        break;
                    case "Comentarios":
                        error = ValidacionTexto(error, Comentarios);
                        break;
                }
                return error;
            }
        }

        private static string ValidacionNull(string error, object obj)
        {
            if (obj == null)
            {
                error = string.Format(CultureInfo.InvariantCulture, "No puede estar Vacio.");
            }
            return error;
        }

        private static string ValidacionTexto(string error, string texto)
        {
            if (String.IsNullOrWhiteSpace(texto))
            {
                error = string.Format(CultureInfo.InvariantCulture, "No puede estar Vacio.");
            }
            return error;
        }

        /// <summary>
        /// Tipo de Solicitud
        /// </summary>
        public enum TipoSolicitud
        {
            /// <summary>
            /// Error
            /// </summary>
            Error,
            /// <summary>
            /// Cambio
            /// </summary>
            Cambio,
            /// <summary>
            /// Mejora
            /// </summary>
            Mejora
        }
        /// <summary>
        /// Sistemas Habilitados donde ser puede realizar una solicitud.
        /// </summary>
        public enum SistemasHabilitados
        {
            /// <summary>
            /// Es una Solicitud General, sin referencia a un Sistema Especifico
            /// </summary>
            General,
            /// <summary>
            /// Sistema EasyArchive
            /// </summary>
            EasyArchive,
            /// <summary>
            /// Sistema EasyDoc
            /// </summary>
            EasyDoc
        }

        /// <summary>
        /// Ambientes Habilitados  donde ser puede realizar una solicitud.
        /// </summary>
        public enum Ambiente
        {
            /// <summary>
            /// Desarrollo
            /// </summary>
            Desarrollo,
            /// <summary>
            /// Testing
            /// </summary>
            Testing,
            /// <summary>
            /// Control de Calidad
            /// </summary>
            QA,
            /// <summary>
            /// Produccion
            /// </summary>
            Produccion
        }

        /// <summary>
        /// Propiedad Propuesta para la Solicitud
        /// </summary>
        public enum PrioridadPropuesta
        {
            /// <summary>
            /// Emergencia
            /// </summary>
            Emergencia,
            /// <summary>
            /// Intermedia
            /// </summary>
            Intermedia,
            /// <summary>
            /// Normal
            /// </summary>
            Normal
        }

        private Model.SolicitudCambio _solicitud;

        ///// <summary>
        ///// Solcitud de Cambio
        ///// </summary>
        //public Model.SolicitudCambio Solicitud
        //{
        //    get { return _solicitud; }
        //    set { _solicitud = value; }
        //}

        ObservableCollection<SistemasHabilitados> _sistemas;
        SistemasHabilitados _sistemaSeleccionado;

        ObservableCollection<string> _versiones;

        Ambiente _ambienteSeleccionado;

        PrioridadPropuesta _prioridadPropuesta;

        TipoSolicitud _tipoSolicitud;

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindowsViewModel()
        {
            GenerarNuevaSolicitud();
        }

        private void GenerarNuevaSolicitud()
        {
            _solicitud = new Model.SolicitudCambio();

            FechaSolicitud = DateTime.Now;
            FechaManejadoPorFecha = FechaSolicitud.Value.AddDays(30);
            TipoSolicitudSeleccionada = TipoSolicitud.Mejora;
            AmbienteSeleccionado = Ambiente.Desarrollo;
            SistemaSeleccionado = SistemasHabilitados.General;
            VersionSeleccionado = "Sin Version";
            PrioridadPropuestaSeleccionada = PrioridadPropuesta.Normal;

            Sistemas = new ObservableCollection<SistemasHabilitados>();

            Sistemas.Add(SistemasHabilitados.General);
            Sistemas.Add(SistemasHabilitados.EasyArchive);
            Sistemas.Add(SistemasHabilitados.EasyDoc);

            SistemaSeleccionado = SistemasHabilitados.General;
            GetVersionesDeSistema();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Propiedad Propuesta para la Solicitud
        /// </summary>
        public PrioridadPropuesta PrioridadPropuestaSeleccionada
        {
            get
            {
                return _prioridadPropuesta;
            }
            set
            {
                if (_prioridadPropuesta != value)
                {
                    _prioridadPropuesta = value;
                    _solicitud.PrioridadPropuesta = value.ToString();
                    OnNotifyPropertyChanged("PrioridadPropuestaSeleccionado");
                }
            }
        }

        /// <summary>
        /// Tipo de Solicitud Seleccionada
        /// </summary>
        public TipoSolicitud TipoSolicitudSeleccionada
        {
            get
            {
                return _tipoSolicitud;
            }
            set
            {
                if (_tipoSolicitud != value)
                {
                    _tipoSolicitud = value;
                    _solicitud.TipoSolicitud = value.ToString();
                    OnNotifyPropertyChanged("TipoSolicitudSeleccionada");
                }
            }
        }

        /// <summary>
        /// Fecha de Solicitud
        /// </summary>
        public DateTime? FechaSolicitud
        {
            get
            {
                return _solicitud.FechaSolicitud;
            }
            set
            {
                if (_solicitud.FechaSolicitud != value)
                {
                    _solicitud.FechaSolicitud = value;
                    OnNotifyPropertyChanged("FechaSolicitud");
                }
            }
        }

        /// <summary>
        /// La fecha para cuando es requerido el cambio (Si Aplica).
        /// </summary>
        public DateTime? FechaManejadoPorFecha
        {
            get
            {
                return _solicitud.FechaManejadoPorFecha;
            }
            set
            {
                if (_solicitud.FechaManejadoPorFecha != value)
                {
                    _solicitud.FechaManejadoPorFecha = value;
                    OnNotifyPropertyChanged("FechaSolicitud");
                }
            }
        }

        /// <summary>
        /// Titulo de Solicitud
        /// </summary>
        public string Titulo
        {
            get
            {
                return _solicitud.Titulo;
            }
            set
            {
                if (_solicitud.Titulo != value)
                {
                    _solicitud.Titulo = value;
                    OnNotifyPropertyChanged("Titulo");
                }
            }
        }


        /// <summary>
        /// Gets or sets the numero solicitud.
        /// </summary>
        /// <value>
        /// The numero solicitud.
        /// </value>
        public string NumeroSolicitud
        {
            get
            {
                return _solicitud.NumeroSolicitud;
            }
            set
            {
                if (_solicitud.NumeroSolicitud != value)
                {
                    _solicitud.NumeroSolicitud = value;
                    OnNotifyPropertyChanged("NumeroSolicitud");
                }
            }
        }

        /// <summary>
        /// Sistema Seleccionado
        /// </summary>
        public SistemasHabilitados SistemaSeleccionado
        {
            get
            {
                return _sistemaSeleccionado;
            }
            set
            {
                if (_sistemaSeleccionado != value)
                {
                    _sistemaSeleccionado = value;
                    GetVersionesDeSistema();
                    _solicitud.Sistema = value.ToString();
                    OnNotifyPropertyChanged("SistemaSeleccionado");
                }
            }
        }

        /// <summary>
        /// Ambiente Seleccionado
        /// </summary>
        public Ambiente AmbienteSeleccionado
        {
            get
            {
                return _ambienteSeleccionado;
            }
            set
            {
                if (_ambienteSeleccionado != value)
                {
                    _ambienteSeleccionado = value;
                    _solicitud.Ambiente = value.ToString();
                    OnNotifyPropertyChanged("AmbienteSeleccionado");
                }
            }
        }

        /// <summary>
        /// Version Seleccionada
        /// </summary>
        public string VersionSeleccionado
        {
            get
            {
                return _solicitud.Version;
            }
            set
            {
                if (_solicitud.Version != value)
                {
                    _solicitud.Version = value;
                    OnNotifyPropertyChanged("VersionSeleccionado");
                }
            }
        }

        /// <summary>
        /// Empresa que solicita el cambio.
        /// </summary>
        public string EmpresaSolicitante
        {
            get
            {
                return _solicitud.EmpresaSolicitante;
            }
            set
            {
                if (_solicitud.EmpresaSolicitante != value)
                {
                    _solicitud.EmpresaSolicitante = value;

                    if (String.IsNullOrWhiteSpace(EmpresaDuenaProceso))
                    {
                        EmpresaDuenaProceso = value;
                    }
                    OnNotifyPropertyChanged("EmpresaSolicitante");
                }
            }
        }

        /// <summary>
        /// (Si aplica y es distinto de Persona Solicitante). Nombre de la persona que descubrió el error, determino que era necesario el cambio o sugirió la mejora.
        /// </summary>
        public string NombreOriginador
        {
            get
            {
                return _solicitud.NombreOriginador;
            }
            set
            {
                if (_solicitud.NombreOriginador != value)
                {
                    _solicitud.NombreOriginador = value;

                    OnNotifyPropertyChanged("NombreOriginador");
                }
            }
        }

        /// <summary>
        /// Empresa que es "dueña" del proceso que se ve afectado por este cambio. Esta empresa será la responsable de testear el cambio. Solo requerido si es distinto de Empresa Solicitante.
        /// </summary>
        public string EmpresaDuenaProceso
        {
            get
            {
                return _solicitud.EmpresaDuenaProceso;
            }
            set
            {
                if (_solicitud.EmpresaDuenaProceso != value)
                {
                    _solicitud.EmpresaDuenaProceso = value;
                    OnNotifyPropertyChanged("EmpresaDuenaProceso");
                }
            }
        }

        /// <summary>
        /// Nombre de la persona que completa este formulario
        /// </summary>
        public string PersonaSolicitante
        {
            get
            {
                return _solicitud.PersonaSolicitante;
            }
            set
            {
                if (_solicitud.PersonaSolicitante != value)
                {
                    _solicitud.PersonaSolicitante = value;
                    if (String.IsNullOrWhiteSpace(NombreOriginador))
                    {
                        NombreOriginador = value;
                    }

                    OnNotifyPropertyChanged("PersonaSolicitante");
                }
            }
        }

        /// <summary>
        /// Selección "SI" si el cambio, corrección de error o mejora debe ser resuelto en producción antes de cierta fecha.
        /// </summary>
        public bool ManejadoPorFecha
        {
            get
            {
                return _solicitud.ManejadoPorFecha;
            }
            set
            {
                if (_solicitud.ManejadoPorFecha != value)
                {
                    _solicitud.ManejadoPorFecha = value;

                    OnNotifyPropertyChanged("ManejadoPorFecha");
                }
            }
        }

        /// <summary>
        /// Si este es un nuevo cambio no documentado descubierto durante el testeo en QA que no requiere solución antes de pasar a producción.
        /// </summary>
        public bool ImplementarSinSolucion
        {
            get
            {
                return _solicitud.ImplementarSinSolucion;
            }
            set
            {
                if (_solicitud.ImplementarSinSolucion != value)
                {
                    _solicitud.ImplementarSinSolucion = value;

                    OnNotifyPropertyChanged("ImplementarSinSolucion");
                }
            }
        }

        /// <summary>
        /// Si pantallas o algún otro documento es adjuntado a esta solicitud. Su envió deberá incluir todas las pantallas en el email que incluye este documento.
        /// </summary>
        public bool InfoAdicional
        {
            get
            {
                return _solicitud.InfoAdicional;
            }
            set
            {
                if (_solicitud.InfoAdicional != value)
                {
                    _solicitud.InfoAdicional = value;

                    OnNotifyPropertyChanged("InfoAdicional");
                }
            }
        }

        /// <summary>
        /// Descripción detallada del propósito de esta solicitud de cambio. En caso de ser una mejora o un cambio se deberá escribir las respuestas a las siguientes preguntas: el Qué?, el Quién? y el Cómo?, también incluir el porqué y el para qué. De ser necesario por favor incluya como documentación adicional el documento de Solicitud de Requerimiento de Software.
        /// </summary>
        public string Descripcion
        {
            get
            {
                return _solicitud.Descripcion;
            }
            set
            {
                if (_solicitud.Descripcion != value)
                {
                    _solicitud.Descripcion = value;

                    OnNotifyPropertyChanged("Descripcion");
                }
            }
        }

        /// <summary>
        /// Comportamiento actual de la aplicación con respecto a cambio solicitado.( Si está reportando un "error", describa los pasos para recrear el problema). Cuando describa como recrear un error, por favor sea especifico e incluya todos los pasos necesarios para reproducir el mismo desde la perspectiva de alguien que no está familiarizado con la aplicación.
        /// </summary>
        public string ComportamientoActual
        {
            get
            {
                return _solicitud.ComportamientoActual;
            }
            set
            {
                if (_solicitud.ComportamientoActual != value)
                {
                    _solicitud.ComportamientoActual = value;

                    OnNotifyPropertyChanged("ComportamientoActual");
                }
            }
        }

        /// <summary>
        /// Lo que la aplicación debería hacer luego de que este cambio se haya realizado. Debe ser lo suficiente detallado para que se pueda hacer un plan de testing para certificar la implementación.
        /// </summary>
        public string ComportamientoPropuesto
        {
            get
            {
                return _solicitud.ComportamientoPropuesto;
            }
            set
            {
                if (_solicitud.ComportamientoPropuesto != value)
                {
                    _solicitud.ComportamientoPropuesto = value;

                    OnNotifyPropertyChanged("ComportamientoPropuesto");
                }
            }
        }

        /// <summary>
        /// comentario que mejore el entendimiento de la solicitud.
        /// </summary>
        public string Comentarios
        {
            get
            {
                return _solicitud.Comentarios;
            }
            set
            {
                if (_solicitud.Comentarios != value)
                {
                    _solicitud.Comentarios = value;

                    OnNotifyPropertyChanged("Comentarios");
                }
            }
        }

        /// <summary>
        /// Lista de sistemas Habilitados
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ObservableCollection<SistemasHabilitados> Sistemas
        {
            get
            {
                return _sistemas;
            }
            set
            {
                if (_sistemas != value)
                {
                    _sistemas = value;
                    OnNotifyPropertyChanged("Sistemas");
                }
            }
        }

        /// <summary>
        /// Lista de Versiones
        /// </summary>
        public ObservableCollection<string> Versiones
        {
            get
            {
                return _versiones;
            }
            set
            {
                if (_versiones != value)
                {
                    _versiones = value;
                    OnNotifyPropertyChanged("Versiones");
                }
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Obteniene la Lista de Versiones segun Sistema Seleccionado
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "De")]
        public void GetVersionesDeSistema()
        {
            Versiones = new ObservableCollection<string>();
            Versiones.Add("Sin Version");

            switch (SistemaSeleccionado)
            {
                case SistemasHabilitados.EasyArchive:
                    CargoVersionesEasyArchive();
                    break;
                case SistemasHabilitados.EasyDoc:
                    CargoVersionesEasyDoc();
                    break;
                default:
                    break;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private void CargoVersionesEasyArchive()
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private void CargoVersionesEasyDoc()
        {
        }

        /// <summary>
        /// Get Reporte de Solicitud de Cambio
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public SolicitudDeCambioReport GetSolicitudCambioReporte()
        {
            if (ValidationHelper.GetValidationErrors(this).Count > 0)
            {
                throw new InvalidOperationException("Imposible Crear La Solicitud, Por Favor revise los errores");
            }

            SolicitudDeCambioReport rpt = new SolicitudDeCambioReport();
            rpt.DataSource = _solicitud;
            rpt.DocumentName = string.Format("{0} - {1}",_solicitud.NumeroSolicitud, _solicitud.Titulo);
            return rpt;
        }
        /// <summary>
        /// Nuevas the solicitud.
        /// </summary>
        public void NuevaSolicitud()
        {
            GenerarNuevaSolicitud();
        }
        #endregion Methods

    
    }
}
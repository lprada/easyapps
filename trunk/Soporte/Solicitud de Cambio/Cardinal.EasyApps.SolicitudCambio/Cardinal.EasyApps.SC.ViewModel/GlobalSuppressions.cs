﻿// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1824:MarkAssembliesWithNeutralResourcesLanguage")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Scope = "member", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.MainWindowsViewModel.#GetSolicitudCambioReporte()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.MainWindowsViewModel.#Versiones")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.MainWindowsViewModel+Ambiente")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.MainWindowsViewModel+PrioridadPropuesta")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.MainWindowsViewModel+SistemasHabilitados")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.MainWindowsViewModel+TipoSolicitud")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.SolicitudDeCambioReport")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "De", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.SolicitudDeCambioReport")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "Telerik.Reporting.CheckBox.set_Text(System.String)", Scope = "member", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.SolicitudDeCambioReport.#InitializeComponent()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "Telerik.Reporting.TextBox.set_Value(System.String)", Scope = "member", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.SolicitudDeCambioReport.#InitializeComponent()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Scope = "type", Target = "Cardinal.EasyApps.SolicitudCambio.ViewModel.ValidationHelper+DataError")]

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Cardinal.EasyApps.SolicitudCambio.ViewModel
{
    /// <summary>
    /// Validation Helper
    /// </summary>
    public static class ValidationHelper
    {
        /// <summary>
        /// Checks for errors in <c>validatable</c> and returns all the errors found.
        /// </summary>
        public static ICollection<DataError> GetValidationErrors(IDataErrorInfo validatable)
        {
            if (validatable == null)
            {
                throw new ArgumentNullException("validatable");
            }
            List<DataError> errors = new List<DataError>();

            // Iterate through every property in the class
            foreach (var prop in validatable.GetType().GetProperties())
            {
                // If the property has an error, then add it to the list
                string errorMessage = validatable[prop.Name];
                if (!String.IsNullOrWhiteSpace(errorMessage))
                {
                    errors.Add(new DataError(prop.Name, errorMessage.Trim()));
                }
            }

            return errors;
        }

        /// <summary>
        /// Represents a single error.  It's a propertyName, errorMessage pair
        /// </summary>
        public class DataError
        {
            private string _propertyName;
            private string _errorMessage;

            /// <summary>
            /// Initializes a new instance of the <see cref="DataError"/> class.
            /// </summary>
            /// <param name="propertyName">Name of the property.</param>
            /// <param name="errorMessage">The error message.</param>
            public DataError(string propertyName, string errorMessage)
            {
                _propertyName = propertyName;
                _errorMessage = errorMessage;
            }

            /// <summary>
            /// The name of the property that has the error.
            /// </summary>
            public string PropertyName
            {
                get { return _propertyName; }
            }

            /// <summary>
            /// A description of the property's error.
            /// </summary>
            public string ErrorMessage
            {
                get { return _errorMessage; }
            }
        }
    }
}
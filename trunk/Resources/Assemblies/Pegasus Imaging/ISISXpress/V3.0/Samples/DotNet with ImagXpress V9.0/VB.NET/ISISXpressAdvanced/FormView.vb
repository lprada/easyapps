Public Class FormView
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents lbldesc As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormView))
        Me.lbldesc = New System.Windows.Forms.Label
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(ImagXpress1)
        Me.SuspendLayout()
        '
        'lbldesc
        '
        Me.lbldesc.BackColor = System.Drawing.Color.Aqua
        Me.lbldesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldesc.Location = New System.Drawing.Point(16, 8)
        Me.lbldesc.Name = "lbldesc"
        Me.lbldesc.Size = New System.Drawing.Size(472, 64)
        Me.lbldesc.TabIndex = 2
        Me.lbldesc.Text = resources.GetString("lbldesc.Text")
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(16, 80)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(467, 293)
        Me.ImageXView1.TabIndex = 3
        '
        'FormView
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(512, 397)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.lbldesc)
        Me.Name = "FormView"
        Me.Text = "FormView"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " Custom function code "
    Public Sub AddImage(ByVal DibHandle As System.IntPtr)
        'get the scanned DIB from ISIS 
        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(ImagXpress1, DibHandle)
        If (Me.Visible = False) Then
            Me.ShowDialog()
        End If
    End Sub
#End Region
#Region " Form Event code "
    Private Sub FormView_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        FormView.ActiveForm.Close()
    End Sub
#End Region

End Class

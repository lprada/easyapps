using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ISISXprCSharp
{
	/// <summary>
	/// Summary description for FormView.
	/// </summary>
	public class FormView : System.Windows.Forms.Form
	{
      
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private System.Windows.Forms.Label lbldesc;
	

		public FormView()
		{
			
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
				
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormView));
            this.lbldesc = new System.Windows.Forms.Label();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
            this.SuspendLayout();
            // 
            // lbldesc
            // 
            this.lbldesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lbldesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldesc.Location = new System.Drawing.Point(16, 8);
            this.lbldesc.Name = "lbldesc";
            this.lbldesc.Size = new System.Drawing.Size(492, 62);
            this.lbldesc.TabIndex = 1;
            this.lbldesc.Text = resources.GetString("lbldesc.Text");
            // 
            // imageXView1
            // 
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(19, 83);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(489, 344);
            this.imageXView1.TabIndex = 2;
            // 
            // FormView
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(552, 452);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.lbldesc);
            this.Name = "FormView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ISIS Xpress Advanced C# Demo";
            this.ResumeLayout(false);

		}
		#endregion

		public void AddImage (System.IntPtr DibHandle)
		{
			//get the scanned DIB from ISIS 
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(imagXpress1,DibHandle);
			if (this.Visible == false)
			{
		 	   this.ShowDialog ();	
		 	}
		}


	
		
	}
}

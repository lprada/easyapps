using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ISISXprCSharp
{
	/// <summary>
	/// Summary description for FormView.
	/// </summary>
	public class FormView : System.Windows.Forms.Form
	{
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private Label label1;
	
	
		//private PegasusImaging.WinForms.IsisXpress3.IsisXpress isis2;

		public FormView()
		{
			
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}

                if (imageXView1 != null)
                {
                    imageXView1.Dispose();
                }

                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                }
				
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormView));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(29, 110);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(559, 270);
            this.imageXView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Cyan;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(536, 64);
            this.label1.TabIndex = 1;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // FormView
            // 
            this.ClientSize = new System.Drawing.Size(592, 426);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imageXView1);
            this.Name = "FormView";
            this.Load += new System.EventHandler(this.FormView_Load);
            this.ResumeLayout(false);

		}
		#endregion



		public void AddImage (System.IntPtr DibHandle)
		{
			//get the scanned DIB from ISIS 
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(imagXpress1,DibHandle);
			if (this.Visible == false)
			{
		 	this.ShowDialog ();	
		 	}
		}

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FormView_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

		
	}
}

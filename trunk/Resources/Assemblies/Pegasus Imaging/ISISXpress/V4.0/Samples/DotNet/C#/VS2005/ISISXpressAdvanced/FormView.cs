using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ISISXprCSharp
{
	/// <summary>
	/// Summary description for FormView.
	/// </summary>
	public class FormView : System.Windows.Forms.Form
    {
        private IContainer components;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Label label1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
	

		public FormView()
		{
			
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
				
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormView));
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(19, 83);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(726, 452);
            this.imageXView1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(83)))), ((int)(((byte)(2)))));
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(724, 64);
            this.label1.TabIndex = 3;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // FormView
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(784, 564);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imageXView1);
            this.Name = "FormView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ISIS Xpress Advanced C# Demo";
            this.ResumeLayout(false);

		}
		#endregion

		public void AddImage (System.IntPtr DibHandle)
		{
			//get the scanned DIB from ISIS 
            imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress1, DibHandle);
			if (this.Visible == false)
			{
		 	   this.ShowDialog ();	
		 	}
		}


	
		
	}
}

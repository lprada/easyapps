Imports Accusoft.ISISXpressSdk


Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' You must unlock the control runtime with numbers supplied by Pegasus Imaging Corp.
        IsisXpress1.Licensing.UnlockRuntime(111, 222, 333, 444)

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            ' Call dispose to clean up ISISXpress instance
            IsisXpress1.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents progressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents statusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents groupBoxFileName As System.Windows.Forms.GroupBox
    Friend WithEvents buttonScanSingle As System.Windows.Forms.Button
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents buttonFiles As System.Windows.Forms.Button
    Friend WithEvents textBoxFilename As System.Windows.Forms.TextBox
    Friend WithEvents textBoxdir As System.Windows.Forms.TextBox
    Friend WithEvents textBoxroot As System.Windows.Forms.TextBox
    Friend WithEvents comboBoxcompress As System.Windows.Forms.ComboBox
    Friend WithEvents comboBoxschema As System.Windows.Forms.ComboBox
    Friend WithEvents checkBoxMulti As System.Windows.Forms.CheckBox
    Friend WithEvents checkBoxView As System.Windows.Forms.CheckBox
    Friend WithEvents checkBoxsave As System.Windows.Forms.CheckBox
    Friend WithEvents checkBoxschema As System.Windows.Forms.CheckBox
    Friend WithEvents buttonCancel As System.Windows.Forms.Button
    Friend WithEvents buttonScanBatch As System.Windows.Forms.Button
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents labelMax As System.Windows.Forms.Label
    Friend WithEvents labelMin As System.Windows.Forms.Label
    Friend WithEvents hScrollBarSetting As System.Windows.Forms.HScrollBar
    Friend WithEvents comboBoxSetting As System.Windows.Forms.ComboBox
    Friend WithEvents comboBoxTypes As System.Windows.Forms.ComboBox
    Friend WithEvents labelSetting As System.Windows.Forms.Label
    Friend WithEvents buttonScanSet As System.Windows.Forms.Button
    Friend WithEvents buttonSelectScanner As System.Windows.Forms.Button
    Friend WithEvents labelScanner As System.Windows.Forms.Label
    Friend WithEvents textBoxScannerName As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents IsisXpress1 As Accusoft.ISISXpressSdk.IsisXpress
    Friend WithEvents lstScanners As System.Windows.Forms.ListBox
    Friend WithEvents cmdGetDrivers As System.Windows.Forms.Button

    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.progressBar1 = New System.Windows.Forms.ProgressBar
        Me.statusBar1 = New System.Windows.Forms.StatusBar
        Me.groupBoxFileName = New System.Windows.Forms.GroupBox
        Me.buttonScanSingle = New System.Windows.Forms.Button
        Me.label5 = New System.Windows.Forms.Label
        Me.label4 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.buttonFiles = New System.Windows.Forms.Button
        Me.textBoxFilename = New System.Windows.Forms.TextBox
        Me.textBoxdir = New System.Windows.Forms.TextBox
        Me.textBoxroot = New System.Windows.Forms.TextBox
        Me.comboBoxcompress = New System.Windows.Forms.ComboBox
        Me.comboBoxschema = New System.Windows.Forms.ComboBox
        Me.checkBoxMulti = New System.Windows.Forms.CheckBox
        Me.checkBoxView = New System.Windows.Forms.CheckBox
        Me.checkBoxsave = New System.Windows.Forms.CheckBox
        Me.checkBoxschema = New System.Windows.Forms.CheckBox
        Me.buttonCancel = New System.Windows.Forms.Button
        Me.buttonScanBatch = New System.Windows.Forms.Button
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.lstScanners = New System.Windows.Forms.ListBox
        Me.cmdGetDrivers = New System.Windows.Forms.Button
        Me.label6 = New System.Windows.Forms.Label
        Me.labelMax = New System.Windows.Forms.Label
        Me.labelMin = New System.Windows.Forms.Label
        Me.hScrollBarSetting = New System.Windows.Forms.HScrollBar
        Me.comboBoxSetting = New System.Windows.Forms.ComboBox
        Me.comboBoxTypes = New System.Windows.Forms.ComboBox
        Me.labelSetting = New System.Windows.Forms.Label
        Me.buttonScanSet = New System.Windows.Forms.Button
        Me.buttonSelectScanner = New System.Windows.Forms.Button
        Me.labelScanner = New System.Windows.Forms.Label
        Me.textBoxScannerName = New System.Windows.Forms.TextBox
        Me.IsisXpress1 = New Accusoft.ISISXpressSdk.IsisXpress
        Me.groupBoxFileName.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.FileName = "doc1"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "&File"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "E&xit"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "About..."
        '
        'progressBar1
        '
        Me.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.progressBar1.Location = New System.Drawing.Point(0, 469)
        Me.progressBar1.Name = "progressBar1"
        Me.progressBar1.Size = New System.Drawing.Size(559, 16)
        Me.progressBar1.TabIndex = 10
        '
        'statusBar1
        '
        Me.statusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.statusBar1.Location = New System.Drawing.Point(0, 485)
        Me.statusBar1.Name = "statusBar1"
        Me.statusBar1.Size = New System.Drawing.Size(559, 32)
        Me.statusBar1.TabIndex = 9
        '
        'groupBoxFileName
        '
        Me.groupBoxFileName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBoxFileName.Controls.Add(Me.buttonScanSingle)
        Me.groupBoxFileName.Controls.Add(Me.label5)
        Me.groupBoxFileName.Controls.Add(Me.label4)
        Me.groupBoxFileName.Controls.Add(Me.label3)
        Me.groupBoxFileName.Controls.Add(Me.label2)
        Me.groupBoxFileName.Controls.Add(Me.label1)
        Me.groupBoxFileName.Controls.Add(Me.buttonFiles)
        Me.groupBoxFileName.Controls.Add(Me.textBoxFilename)
        Me.groupBoxFileName.Controls.Add(Me.textBoxdir)
        Me.groupBoxFileName.Controls.Add(Me.textBoxroot)
        Me.groupBoxFileName.Controls.Add(Me.comboBoxcompress)
        Me.groupBoxFileName.Controls.Add(Me.comboBoxschema)
        Me.groupBoxFileName.Controls.Add(Me.checkBoxMulti)
        Me.groupBoxFileName.Controls.Add(Me.checkBoxView)
        Me.groupBoxFileName.Controls.Add(Me.checkBoxsave)
        Me.groupBoxFileName.Controls.Add(Me.checkBoxschema)
        Me.groupBoxFileName.Controls.Add(Me.buttonCancel)
        Me.groupBoxFileName.Controls.Add(Me.buttonScanBatch)
        Me.groupBoxFileName.Location = New System.Drawing.Point(8, 214)
        Me.groupBoxFileName.Name = "groupBoxFileName"
        Me.groupBoxFileName.Size = New System.Drawing.Size(543, 249)
        Me.groupBoxFileName.TabIndex = 8
        Me.groupBoxFileName.TabStop = False
        '
        'buttonScanSingle
        '
        Me.buttonScanSingle.Location = New System.Drawing.Point(32, 208)
        Me.buttonScanSingle.Name = "buttonScanSingle"
        Me.buttonScanSingle.Size = New System.Drawing.Size(80, 30)
        Me.buttonScanSingle.TabIndex = 15
        Me.buttonScanSingle.Text = "Scan Single"
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(16, 160)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(72, 16)
        Me.label5.TabIndex = 14
        Me.label5.Text = "FileName"
        Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(136, 120)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(72, 16)
        Me.label4.TabIndex = 13
        Me.label4.Text = "Compression"
        Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(160, 88)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(48, 16)
        Me.label3.TabIndex = 12
        Me.label3.Text = "File Dir"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(152, 56)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(56, 16)
        Me.label2.TabIndex = 11
        Me.label2.Text = "File Root"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(160, 24)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(48, 16)
        Me.label1.TabIndex = 10
        Me.label1.Text = "Schema"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'buttonFiles
        '
        Me.buttonFiles.Location = New System.Drawing.Point(352, 160)
        Me.buttonFiles.Name = "buttonFiles"
        Me.buttonFiles.Size = New System.Drawing.Size(24, 24)
        Me.buttonFiles.TabIndex = 9
        Me.buttonFiles.Text = "..."
        '
        'textBoxFilename
        '
        Me.textBoxFilename.Location = New System.Drawing.Point(112, 160)
        Me.textBoxFilename.Name = "textBoxFilename"
        Me.textBoxFilename.Size = New System.Drawing.Size(224, 20)
        Me.textBoxFilename.TabIndex = 8
        '
        'textBoxdir
        '
        Me.textBoxdir.Location = New System.Drawing.Point(224, 88)
        Me.textBoxdir.Name = "textBoxdir"
        Me.textBoxdir.Size = New System.Drawing.Size(152, 20)
        Me.textBoxdir.TabIndex = 7
        '
        'textBoxroot
        '
        Me.textBoxroot.Location = New System.Drawing.Point(224, 56)
        Me.textBoxroot.Name = "textBoxroot"
        Me.textBoxroot.Size = New System.Drawing.Size(152, 20)
        Me.textBoxroot.TabIndex = 6
        '
        'comboBoxcompress
        '
        Me.comboBoxcompress.Items.AddRange(New Object() {"TIFF Raw", "TIFF PackBits", "TIFF G3", "TIFF G3M", "TIFF G4", "JPEG Color", "JPEG Grey", "DCX"})
        Me.comboBoxcompress.Location = New System.Drawing.Point(224, 120)
        Me.comboBoxcompress.Name = "comboBoxcompress"
        Me.comboBoxcompress.Size = New System.Drawing.Size(152, 21)
        Me.comboBoxcompress.TabIndex = 5
        '
        'comboBoxschema
        '
        Me.comboBoxschema.Items.AddRange(New Object() {"$####", "$###", "$##"})
        Me.comboBoxschema.Location = New System.Drawing.Point(224, 24)
        Me.comboBoxschema.Name = "comboBoxschema"
        Me.comboBoxschema.Size = New System.Drawing.Size(152, 21)
        Me.comboBoxschema.TabIndex = 4
        '
        'checkBoxMulti
        '
        Me.checkBoxMulti.Location = New System.Drawing.Point(16, 120)
        Me.checkBoxMulti.Name = "checkBoxMulti"
        Me.checkBoxMulti.Size = New System.Drawing.Size(104, 24)
        Me.checkBoxMulti.TabIndex = 3
        Me.checkBoxMulti.Text = "Multi Page"
        '
        'checkBoxView
        '
        Me.checkBoxView.Checked = True
        Me.checkBoxView.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkBoxView.Location = New System.Drawing.Point(16, 88)
        Me.checkBoxView.Name = "checkBoxView"
        Me.checkBoxView.Size = New System.Drawing.Size(104, 24)
        Me.checkBoxView.TabIndex = 2
        Me.checkBoxView.Text = "View Image"
        '
        'checkBoxsave
        '
        Me.checkBoxsave.Location = New System.Drawing.Point(16, 56)
        Me.checkBoxsave.Name = "checkBoxsave"
        Me.checkBoxsave.Size = New System.Drawing.Size(104, 24)
        Me.checkBoxsave.TabIndex = 1
        Me.checkBoxsave.Text = "Save File"
        '
        'checkBoxschema
        '
        Me.checkBoxschema.Location = New System.Drawing.Point(16, 24)
        Me.checkBoxschema.Name = "checkBoxschema"
        Me.checkBoxschema.Size = New System.Drawing.Size(104, 24)
        Me.checkBoxschema.TabIndex = 0
        Me.checkBoxschema.Text = "Use Schema"
        '
        'buttonCancel
        '
        Me.buttonCancel.Location = New System.Drawing.Point(312, 208)
        Me.buttonCancel.Name = "buttonCancel"
        Me.buttonCancel.Size = New System.Drawing.Size(80, 30)
        Me.buttonCancel.TabIndex = 6
        Me.buttonCancel.Text = "Cancel"
        '
        'buttonScanBatch
        '
        Me.buttonScanBatch.Location = New System.Drawing.Point(168, 208)
        Me.buttonScanBatch.Name = "buttonScanBatch"
        Me.buttonScanBatch.Size = New System.Drawing.Size(80, 30)
        Me.buttonScanBatch.TabIndex = 5
        Me.buttonScanBatch.Text = "Scan Batch"
        '
        'groupBox1
        '
        Me.groupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox1.Controls.Add(Me.lstScanners)
        Me.groupBox1.Controls.Add(Me.cmdGetDrivers)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.labelMax)
        Me.groupBox1.Controls.Add(Me.labelMin)
        Me.groupBox1.Controls.Add(Me.hScrollBarSetting)
        Me.groupBox1.Controls.Add(Me.comboBoxSetting)
        Me.groupBox1.Controls.Add(Me.comboBoxTypes)
        Me.groupBox1.Controls.Add(Me.labelSetting)
        Me.groupBox1.Controls.Add(Me.buttonScanSet)
        Me.groupBox1.Controls.Add(Me.buttonSelectScanner)
        Me.groupBox1.Controls.Add(Me.labelScanner)
        Me.groupBox1.Controls.Add(Me.textBoxScannerName)
        Me.groupBox1.Location = New System.Drawing.Point(9, 4)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(542, 204)
        Me.groupBox1.TabIndex = 7
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Scanner Settings"
        '
        'lstScanners
        '
        Me.lstScanners.FormattingEnabled = True
        Me.lstScanners.Location = New System.Drawing.Point(382, 69)
        Me.lstScanners.Name = "lstScanners"
        Me.lstScanners.Size = New System.Drawing.Size(154, 121)
        Me.lstScanners.TabIndex = 12
        '
        'cmdGetDrivers
        '
        Me.cmdGetDrivers.Location = New System.Drawing.Point(393, 19)
        Me.cmdGetDrivers.Name = "cmdGetDrivers"
        Me.cmdGetDrivers.Size = New System.Drawing.Size(125, 39)
        Me.cmdGetDrivers.TabIndex = 11
        Me.cmdGetDrivers.Text = "Get Scanner and Driver Names"
        Me.cmdGetDrivers.UseVisualStyleBackColor = True
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(192, 152)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(48, 32)
        Me.label6.TabIndex = 10
        Me.label6.Text = "label6"
        '
        'labelMax
        '
        Me.labelMax.Location = New System.Drawing.Point(328, 128)
        Me.labelMax.Name = "labelMax"
        Me.labelMax.Size = New System.Drawing.Size(48, 24)
        Me.labelMax.TabIndex = 9
        Me.labelMax.Text = "labelMax"
        Me.labelMax.Visible = False
        '
        'labelMin
        '
        Me.labelMin.Location = New System.Drawing.Point(56, 128)
        Me.labelMin.Name = "labelMin"
        Me.labelMin.Size = New System.Drawing.Size(48, 24)
        Me.labelMin.TabIndex = 8
        Me.labelMin.Text = "labelMin"
        Me.labelMin.Visible = False
        '
        'hScrollBarSetting
        '
        Me.hScrollBarSetting.Location = New System.Drawing.Point(136, 128)
        Me.hScrollBarSetting.Name = "hScrollBarSetting"
        Me.hScrollBarSetting.Size = New System.Drawing.Size(160, 16)
        Me.hScrollBarSetting.TabIndex = 7
        Me.hScrollBarSetting.Visible = False
        '
        'comboBoxSetting
        '
        Me.comboBoxSetting.Location = New System.Drawing.Point(80, 128)
        Me.comboBoxSetting.Name = "comboBoxSetting"
        Me.comboBoxSetting.Size = New System.Drawing.Size(280, 21)
        Me.comboBoxSetting.TabIndex = 6
        Me.comboBoxSetting.Visible = False
        '
        'comboBoxTypes
        '
        Me.comboBoxTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxTypes.Items.AddRange(New Object() {"0 - XResolution", "1 - YResolution", "2 - PageSize", "3 - Brightness", "4 - Contrast", "5 - XPosition", "6 - YPosition", "7 - ImageWidth", "8 - ImageLength", "9 - ResolutionUnits", "10 - ScanType", "11 - ScanAhead", "12 - ScanAheadMaxPages", "13 - ScanAheadPages", "14 - SamplesPerPixel", "15 - BitsPerPixel"})
        Me.comboBoxTypes.Location = New System.Drawing.Point(80, 100)
        Me.comboBoxTypes.Name = "comboBoxTypes"
        Me.comboBoxTypes.Size = New System.Drawing.Size(152, 21)
        Me.comboBoxTypes.TabIndex = 5
        '
        'labelSetting
        '
        Me.labelSetting.Location = New System.Drawing.Point(16, 104)
        Me.labelSetting.Name = "labelSetting"
        Me.labelSetting.Size = New System.Drawing.Size(48, 16)
        Me.labelSetting.TabIndex = 4
        Me.labelSetting.Text = "Setting"
        '
        'buttonScanSet
        '
        Me.buttonScanSet.Location = New System.Drawing.Point(248, 64)
        Me.buttonScanSet.Name = "buttonScanSet"
        Me.buttonScanSet.Size = New System.Drawing.Size(104, 23)
        Me.buttonScanSet.TabIndex = 3
        Me.buttonScanSet.Text = "Scanner Settings"
        '
        'buttonSelectScanner
        '
        Me.buttonSelectScanner.Location = New System.Drawing.Point(248, 32)
        Me.buttonSelectScanner.Name = "buttonSelectScanner"
        Me.buttonSelectScanner.Size = New System.Drawing.Size(104, 23)
        Me.buttonSelectScanner.TabIndex = 2
        Me.buttonSelectScanner.Text = "Select Scanner"
        '
        'labelScanner
        '
        Me.labelScanner.Location = New System.Drawing.Point(16, 32)
        Me.labelScanner.Name = "labelScanner"
        Me.labelScanner.Size = New System.Drawing.Size(56, 16)
        Me.labelScanner.TabIndex = 1
        Me.labelScanner.Text = "Scanner:"
        '
        'textBoxScannerName
        '
        Me.textBoxScannerName.Location = New System.Drawing.Point(80, 32)
        Me.textBoxScannerName.Name = "textBoxScannerName"
        Me.textBoxScannerName.Size = New System.Drawing.Size(120, 20)
        Me.textBoxScannerName.TabIndex = 0
        '
        'IsisXpress1
        '
        Me.IsisXpress1.ErrorLevel = Accusoft.ISISXpressSdk.ErrorLevelInfo.Production
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(559, 517)
        Me.Controls.Add(Me.progressBar1)
        Me.Controls.Add(Me.statusBar1)
        Me.Controls.Add(Me.groupBoxFileName)
        Me.Controls.Add(Me.groupBox1)
        Me.Menu = Me.MainMenu1
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ISISXpress Basic VB.NET Demo"
        Me.groupBoxFileName.ResumeLayout(False)
        Me.groupBoxFileName.PerformLayout()
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Global variable declarations
    Private gNumPages As Int32

#Region " Form event code "
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '****Must call the UnlockRuntime to distribute the runtime.
        'ISISXpress1.Licensing.UnlockRuntime(1234,1234,1234,1234);
        '//set some default dialog settings
        SaveFileDialog1.Filter = "TIFF(*.tif)|*.tif|DCX(*.dcx)|*.dcx|JPEG(*.jpg)|*.jpg|RAW(*.raw)|*.raw"
        SaveFileDialog1.DefaultExt = ".tif"

        textBoxScannerName.Text = IsisXpress1.Scanner.Name
        gNumPages = 0
        comboBoxTypes.SelectedIndex = 0
        comboBoxschema.SelectedIndex = 0
        comboBoxcompress.SelectedIndex = 4
        TypeSelect()
        checkBoxschema.Checked = False
        checkBoxsave.Checked = False
        SchemaSelect()
    End Sub
#End Region
#Region " MainMenu event code "
    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Application.Exit()
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        IsisXpress1.AboutBox()
    End Sub
#End Region
#Region " Buttons click event code "

    Private Sub buttonFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonFiles.Click
        If (SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then
            textBoxFilename.Text = SaveFileDialog1.FileName
        End If
    End Sub

    Private Sub buttonSelectScanner_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSelectScanner.Click
        Try
            statusBar1.Text = ""
            IsisXpress1.Scanner.Select()
            textBoxScannerName.Text = IsisXpress1.Scanner.Name
            comboBoxTypes.SelectedIndex = 0
        Catch ex As Exception
            statusBar1.Text = "The Error Reported Is: " + ex.Message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub buttonScanSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonScanSet.Click
        Try

            IsisXpress1.Scanner.Setup(frmMain.ActiveForm.Handle.ToInt32(), Accusoft.ISISXpressSdk.SetupOptions.Normal)
            TypeSelect()

        Catch ex As Exception

            statusBar1.Text = "The Error Reported Is: " + ex.Message
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub buttonScanSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonScanSingle.Click
        Try
            progressBar1.Value = 0
            If (ScanUpdate() = False) Then Exit Sub
            buttonScanBatch.Enabled = False
            buttonScanSingle.Enabled = False
            buttonCancel.Enabled = True

            IsisXpress1.Scanner.ScanSingle()

            buttonScanBatch.Enabled = True
            buttonScanSingle.Enabled = True
            buttonCancel.Enabled = False
        Catch ex As Exception
            buttonScanBatch.Enabled = True
            buttonScanSingle.Enabled = True
            buttonCancel.Enabled = False
            statusBar1.Text = "The Error Reported Is:" + ex.Message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub buttonScanBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonScanBatch.Click
        'FormView LaunchForm;
        Try
            progressBar1.Value = 0
            If (ScanUpdate() = False) Then Exit Sub
            buttonScanBatch.Enabled = False
            buttonScanSingle.Enabled = False
            buttonCancel.Enabled = True

            'scan in batch mode
            IsisXpress1.Scanner.ScanBatch()

            'LaunchForm = new FormView();

            'LaunchForm.Show();
            buttonScanBatch.Enabled = True
            buttonScanSingle.Enabled = True
            buttonCancel.Enabled = False
        Catch ex As Exception
            buttonScanBatch.Enabled = True
            buttonScanSingle.Enabled = True
            buttonCancel.Enabled = False
            statusBar1.Text = "The Error Reported Is:" + ex.Message
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub buttonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCancel.Click
        buttonCancel.Enabled = False
        IsisXpress1.Scanner.Cancel(False)
    End Sub

#End Region
#Region " Misc. control event code "
    Private Sub comboBoxSetting_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles comboBoxSetting.SelectedIndexChanged
        If (IsisXpress1.Scanner.Setting.DataType = Accusoft.ISISXpressSdk.DataTypeInfo.ListString) Then
            IsisXpress1.Scanner.Setting.StringValue = comboBoxSetting.Text
        Else
            IsisXpress1.Scanner.Setting.LongValue = Int32.Parse(comboBoxSetting.Text)
        End If
    End Sub

    Private Sub comboBoxTypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles comboBoxTypes.SelectedIndexChanged
        TypeSelect()
    End Sub

    Private Sub checkBoxschema_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkBoxschema.CheckedChanged
        SchemaSelect()
    End Sub

    Private Sub hScrollBarSetting_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarSetting.ValueChanged
        If (CType(hScrollBarSetting.Tag, Boolean) = True) Then
            IsisXpress1.Scanner.Setting.LongValue = hScrollBarSetting.Value
        End If
        label6.Text = hScrollBarSetting.Value.ToString()

    End Sub
#End Region
#Region " ISISXpress event code "




#End Region
#Region " Custom function code "

    Private Function ScanUpdate() As Boolean

        Dim result As Boolean

        'set the default settings for the scanners
        IsisXpress1.Output.Directory = textBoxdir.Text
        IsisXpress1.Output.Filename = textBoxFilename.Text
        IsisXpress1.Output.Root = textBoxroot.Text
        IsisXpress1.Output.FileSchema = comboBoxschema.Text
        IsisXpress1.Output.FileType = CType(comboBoxcompress.SelectedIndex, Accusoft.ISISXpressSdk.FileTypeInfo)
        IsisXpress1.Output.CreateFiles = checkBoxsave.Checked
        IsisXpress1.Output.CreateDib = checkBoxView.Checked
        IsisXpress1.Output.MultiplePages = checkBoxMulti.Checked

        result = True
        If ((IsisXpress1.Output.CreateFiles = True) And (IsisXpress1.Output.UseSchema = False) And (textBoxFilename.Text.Length = 0)) Then
            MsgBox("Please enter a filename.")
            result = False
        End If

        If ((IsisXpress1.Output.CreateFiles = True) And (IsisXpress1.Output.UseSchema = True) And (textBoxdir.Text.Length = 0)) Then
            MsgBox("Please enter a file directory.")
            result = False
        End If

        If ((IsisXpress1.Output.CreateFiles = True) And (IsisXpress1.Output.UseSchema = True) And (textBoxroot.Text.Length = 0)) Then
            MsgBox("Please enter a file root.")
            result = False
        End If

        Return result
    End Function

    Private Sub TypeSelect()
        Dim ScanSet As String
        Try
            statusBar1.Text = ""

            ScanSet = comboBoxTypes.Items.Item(comboBoxTypes.SelectedIndex).ToString()
            ScanSet = ScanSet.Remove(2, (ScanSet.Length - 2))

            IsisXpress1.Scanner.Setting.ScanSetting = CType(Int32.Parse(ScanSet), Accusoft.ISISXpressSdk.ScanSettingInfo)

            If (IsisXpress1.Scanner.Setting.Supported = True) Then

                If (IsisXpress1.Scanner.Setting.DataType = Accusoft.ISISXpressSdk.DataTypeInfo.RangeLong) Then
                    ' RangeLong
                    labelMin.Text = IsisXpress1.Scanner.Setting.RangeMinimumLong.ToString()
                    labelMax.Text = IsisXpress1.Scanner.Setting.RangeMaximumLong.ToString()
                    label6.Text = IsisXpress1.Scanner.Setting.LongValue.ToString()
                    hScrollBarSetting.Tag = False
                    hScrollBarSetting.Minimum = IsisXpress1.Scanner.Setting.RangeMinimumLong
                    hScrollBarSetting.Maximum = IsisXpress1.Scanner.Setting.RangeMaximumLong
                    hScrollBarSetting.Value = IsisXpress1.Scanner.Setting.LongValue
                    hScrollBarSetting.Tag = True
                    If (hScrollBarSetting.Minimum = hScrollBarSetting.Maximum) Then
                        hScrollBarSetting.Enabled = False
                    Else
                        hScrollBarSetting.Enabled = True
                    End If
                    comboBoxSetting.Visible = False
                    labelMin.Visible = True
                    labelMax.Visible = True
                    label6.Visible = True
                    hScrollBarSetting.Visible = True
                    ' if range
                ElseIf ((IsisXpress1.Scanner.Setting.DataType = Accusoft.ISISXpressSdk.DataTypeInfo.ListString) Or _
                     (IsisXpress1.Scanner.Setting.DataType = Accusoft.ISISXpressSdk.DataTypeInfo.ListLong)) Then

                    Dim Index As Integer

                    comboBoxSetting.Items.Clear()

                    For Index = 0 To IsisXpress1.Scanner.Setting.ListCount - 1

                        If (IsisXpress1.Scanner.Setting.DataType = Accusoft.ISISXpressSdk.DataTypeInfo.ListString) Then

                            comboBoxSetting.Items.Add(IsisXpress1.Scanner.Setting.ListString(Index))
                            '//							
                            If (IsisXpress1.Scanner.Setting.ListString(Index) = IsisXpress1.Scanner.Setting.StringValue) Then
                                comboBoxSetting.SelectedIndex = Index
                            End If

                            '// not ascii list long
                        ElseIf (IsisXpress1.Scanner.Setting.DataType = Accusoft.ISISXpressSdk.DataTypeInfo.ListLong) Then
                            comboBoxSetting.Items.Add(IsisXpress1.Scanner.Setting.ListLong(Index))
                            If (IsisXpress1.Scanner.Setting.ListLong(Index) = IsisXpress1.Scanner.Setting.LongValue) Then
                                comboBoxSetting.SelectedIndex = Index
                            End If
                        Else

                            MsgBox("Uncoded Data Type")
                            statusBar1.Text = "Uncoded Data Type"
                        End If
                    Next

                    labelMax.Visible = False
                    labelMin.Visible = False
                    label6.Visible = False
                    hScrollBarSetting.Visible = False
                    comboBoxSetting.Visible = True

                Else
                    statusBar1.Text = "Uncoded Data Type"
                End If

            Else
                labelMax.Visible = False
                labelMin.Visible = False
                label6.Visible = False
                hScrollBarSetting.Visible = False
                comboBoxSetting.Visible = False
                statusBar1.Text = "Unsupported Data Type"
            End If

        Catch ex As Exception
            statusBar1.Text = "The Error Reported Is:" + ex.Message
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SchemaSelect()
        If (checkBoxschema.Checked = True) Then
            label5.Enabled = False
            label2.Enabled = True
            label3.Enabled = True
            label1.Enabled = True
            'set some basic properties
            IsisXpress1.Output.UseSchema = True
            IsisXpress1.Output.CreateFiles = True
            textBoxFilename.Enabled = False
            comboBoxschema.Enabled = True
            textBoxroot.Enabled = True
            textBoxdir.Enabled = True
            checkBoxsave.Enabled = False
            buttonFiles.Enabled = False
        Else
            label5.Enabled = True
            label2.Enabled = False
            label3.Enabled = False
            label1.Enabled = False
            IsisXpress1.Output.UseSchema = False
            textBoxFilename.Enabled = True
            comboBoxschema.Enabled = False
            textBoxroot.Enabled = False
            textBoxdir.Enabled = False
            checkBoxsave.Enabled = True
            buttonFiles.Enabled = True
        End If
    End Sub

#End Region

    Private Sub IsisXpress1_FeederEmpty(ByVal sender As System.Object, ByVal e As Accusoft.ISISXpressSdk.FeederEmptyEventArgs) Handles IsisXpress1.FeederEmpty
        MessageBox.Show("Click OK when scanner is ready")
    End Sub

    Private Sub IsisXpress1_Progress(ByVal sender As System.Object, ByVal e As Accusoft.ISISXpressSdk.ProgressEventArgs) Handles IsisXpress1.Progress

        progressBar1.Maximum = 100
        progressBar1.Minimum = 0
        progressBar1.Value = e.Percent
    End Sub

    Private Sub IsisXpress1_Scanned(ByVal sender As System.Object, ByVal e As Accusoft.ISISXpressSdk.ScannedEventArgs) Handles IsisXpress1.Scanned

        If (IsisXpress1.Output.CreateDib = True And checkBoxView.Checked = True) Then
            Dim viewer As New FormView()
            'retrieve the scanned DIB via the ToHdib method _
            'Please note: You could use the ToBitMap method to transfer the image data from the ISISXpress control.
            viewer.AddImage(IsisXpress1.Output.ToHdib())
            Application.DoEvents()
        End If
    End Sub

    Private Sub IsisXpress1_Scanning(ByVal sender As System.Object, ByVal e As Accusoft.ISISXpressSdk.ScanningEventArgs) Handles IsisXpress1.Scanning
        textBoxFilename.Text = IsisXpress1.Output.Filename
    End Sub

    Private Sub cmdGetDrivers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGetDrivers.Click
        Dim drlist As New Accusoft.ISISXpressSdk.DriverAndScannerList()

        IsisXpress1.Scanner.GetDriversAndScannerModels(drlist)
        Dim scanners As String
        Dim counter As Int32

        For counter = 0 To drlist.Count - 1
            scanners = drlist(counter).DriverName + drlist(counter).ScannerModel
            lstScanners.Items.Add(scanners)
        Next

    End Sub

    Private Sub checkBoxsave_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkBoxsave.CheckedChanged
        If checkBoxsave.Checked Then
            checkBoxschema.Enabled = False
        Else
            checkBoxschema.Enabled = True
        End If
    End Sub
End Class

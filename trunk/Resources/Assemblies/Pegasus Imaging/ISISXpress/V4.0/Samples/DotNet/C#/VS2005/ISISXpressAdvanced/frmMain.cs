using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Accusoft.ISISXpressSdk;

namespace ISISXprCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBoxScannerName;
		private System.Windows.Forms.Label labelScanner;
		private System.Windows.Forms.Button buttonSelectScanner;
		private System.Windows.Forms.Label labelSetting;
		private System.Windows.Forms.ComboBox comboBoxTypes;
		private System.Windows.Forms.ComboBox comboBoxSetting;
		private System.Windows.Forms.HScrollBar hScrollBarSetting;
		private System.Windows.Forms.Button buttonScanSet;
		private System.Windows.Forms.Label labelMin;
		private System.Windows.Forms.Label labelMax;
		private System.Windows.Forms.GroupBox groupBoxFileName;
		private System.Windows.Forms.CheckBox checkBoxschema;
		private System.Windows.Forms.CheckBox checkBoxsave;
		private System.Windows.Forms.CheckBox checkBoxView;
		private System.Windows.Forms.CheckBox checkBoxMulti;
		private System.Windows.Forms.ComboBox comboBoxschema;
		private System.Windows.Forms.ComboBox comboBoxcompress;
		private System.Windows.Forms.TextBox textBoxroot;
		private System.Windows.Forms.TextBox textBoxdir;
		private System.Windows.Forms.Button buttonFiles;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxFilename;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.Button buttonScanBatch;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.Button buttonScanSingle;
        private System.Windows.Forms.ProgressBar progressBar1;
        private IsisXpress isisXpress1;
        private Button cmdScannerNames;
        private ListBox lstScanners;
        private IContainer components;

		public Form1()
		{
	
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
				// Call dispose to clean up the ISISXpress instance
				isisXpress1.Dispose();

			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstScanners = new System.Windows.Forms.ListBox();
            this.cmdScannerNames = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.labelMax = new System.Windows.Forms.Label();
            this.labelMin = new System.Windows.Forms.Label();
            this.hScrollBarSetting = new System.Windows.Forms.HScrollBar();
            this.comboBoxSetting = new System.Windows.Forms.ComboBox();
            this.comboBoxTypes = new System.Windows.Forms.ComboBox();
            this.labelSetting = new System.Windows.Forms.Label();
            this.buttonScanSet = new System.Windows.Forms.Button();
            this.buttonSelectScanner = new System.Windows.Forms.Button();
            this.labelScanner = new System.Windows.Forms.Label();
            this.textBoxScannerName = new System.Windows.Forms.TextBox();
            this.groupBoxFileName = new System.Windows.Forms.GroupBox();
            this.buttonScanSingle = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonFiles = new System.Windows.Forms.Button();
            this.textBoxFilename = new System.Windows.Forms.TextBox();
            this.textBoxdir = new System.Windows.Forms.TextBox();
            this.textBoxroot = new System.Windows.Forms.TextBox();
            this.comboBoxcompress = new System.Windows.Forms.ComboBox();
            this.comboBoxschema = new System.Windows.Forms.ComboBox();
            this.checkBoxMulti = new System.Windows.Forms.CheckBox();
            this.checkBoxView = new System.Windows.Forms.CheckBox();
            this.checkBoxsave = new System.Windows.Forms.CheckBox();
            this.checkBoxschema = new System.Windows.Forms.CheckBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonScanBatch = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.isisXpress1 = new Accusoft.ISISXpressSdk.IsisXpress();
            this.groupBox1.SuspendLayout();
            this.groupBoxFileName.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lstScanners);
            this.groupBox1.Controls.Add(this.cmdScannerNames);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.labelMax);
            this.groupBox1.Controls.Add(this.labelMin);
            this.groupBox1.Controls.Add(this.hScrollBarSetting);
            this.groupBox1.Controls.Add(this.comboBoxSetting);
            this.groupBox1.Controls.Add(this.comboBoxTypes);
            this.groupBox1.Controls.Add(this.labelSetting);
            this.groupBox1.Controls.Add(this.buttonScanSet);
            this.groupBox1.Controls.Add(this.buttonSelectScanner);
            this.groupBox1.Controls.Add(this.labelScanner);
            this.groupBox1.Controls.Add(this.textBoxScannerName);
            this.groupBox1.Location = new System.Drawing.Point(9, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(630, 184);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scanner Settings";
            // 
            // lstScanners
            // 
            this.lstScanners.FormattingEnabled = true;
            this.lstScanners.Location = new System.Drawing.Point(382, 75);
            this.lstScanners.Name = "lstScanners";
            this.lstScanners.Size = new System.Drawing.Size(221, 69);
            this.lstScanners.TabIndex = 12;
            // 
            // cmdScannerNames
            // 
            this.cmdScannerNames.Location = new System.Drawing.Point(409, 22);
            this.cmdScannerNames.Name = "cmdScannerNames";
            this.cmdScannerNames.Size = new System.Drawing.Size(168, 33);
            this.cmdScannerNames.TabIndex = 11;
            this.cmdScannerNames.Text = "Get Scanner and Driver Names";
            this.cmdScannerNames.UseVisualStyleBackColor = true;
            this.cmdScannerNames.Click += new System.EventHandler(this.cmdScannerNames_Click);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(192, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 32);
            this.label6.TabIndex = 10;
            this.label6.Text = "label6";
            this.label6.Visible = false;
            // 
            // labelMax
            // 
            this.labelMax.Location = new System.Drawing.Point(328, 128);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(48, 24);
            this.labelMax.TabIndex = 9;
            this.labelMax.Text = "labelMax";
            this.labelMax.Visible = false;
            // 
            // labelMin
            // 
            this.labelMin.Location = new System.Drawing.Point(56, 128);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(48, 24);
            this.labelMin.TabIndex = 8;
            this.labelMin.Text = "labelMin";
            this.labelMin.Visible = false;
            // 
            // hScrollBarSetting
            // 
            this.hScrollBarSetting.Location = new System.Drawing.Point(136, 128);
            this.hScrollBarSetting.Name = "hScrollBarSetting";
            this.hScrollBarSetting.Size = new System.Drawing.Size(160, 16);
            this.hScrollBarSetting.TabIndex = 7;
            this.hScrollBarSetting.Visible = false;
            this.hScrollBarSetting.ValueChanged += new System.EventHandler(this.hScrollBarSetting_ValueChanged);
            // 
            // comboBoxSetting
            // 
            this.comboBoxSetting.Location = new System.Drawing.Point(80, 128);
            this.comboBoxSetting.Name = "comboBoxSetting";
            this.comboBoxSetting.Size = new System.Drawing.Size(280, 21);
            this.comboBoxSetting.TabIndex = 6;
            this.comboBoxSetting.Visible = false;
            this.comboBoxSetting.SelectedIndexChanged += new System.EventHandler(this.comboBoxSetting_SelectedIndexChanged);
            // 
            // comboBoxTypes
            // 
            this.comboBoxTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTypes.Items.AddRange(new object[] {
            "0282 - XResolution",
            "0283 - YResolution",
            "1294 - PageSize",
            "1282 - Brightness",
            "1281 - Contrast",
            "0286 - XPosition",
            "0287 - YPosition",
            "0256 - ImageWidth",
            "0257 - ImageLength",
            "1300 - ScanType",
            "1293 - ScanAhead",
            "1301 - ScanAheadPages",
            "1404 - ScanAheadMaxPages",
            "0277- SamplesPerPixel",
            "0258 - BitsPerSample",
            "1690-Multi-Stream"});
            this.comboBoxTypes.Location = new System.Drawing.Point(80, 100);
            this.comboBoxTypes.Name = "comboBoxTypes";
            this.comboBoxTypes.Size = new System.Drawing.Size(152, 21);
            this.comboBoxTypes.TabIndex = 5;
            this.comboBoxTypes.SelectedIndexChanged += new System.EventHandler(this.comboBoxTypes_SelectedIndexChanged);
            // 
            // labelSetting
            // 
            this.labelSetting.Location = new System.Drawing.Point(16, 104);
            this.labelSetting.Name = "labelSetting";
            this.labelSetting.Size = new System.Drawing.Size(48, 16);
            this.labelSetting.TabIndex = 4;
            this.labelSetting.Text = "Setting";
            // 
            // buttonScanSet
            // 
            this.buttonScanSet.Location = new System.Drawing.Point(231, 59);
            this.buttonScanSet.Name = "buttonScanSet";
            this.buttonScanSet.Size = new System.Drawing.Size(104, 23);
            this.buttonScanSet.TabIndex = 3;
            this.buttonScanSet.Text = "Scanner Settings";
            this.buttonScanSet.Click += new System.EventHandler(this.buttonScanSet_Click);
            // 
            // buttonSelectScanner
            // 
            this.buttonSelectScanner.Location = new System.Drawing.Point(231, 30);
            this.buttonSelectScanner.Name = "buttonSelectScanner";
            this.buttonSelectScanner.Size = new System.Drawing.Size(104, 23);
            this.buttonSelectScanner.TabIndex = 2;
            this.buttonSelectScanner.Text = "Select Scanner";
            this.buttonSelectScanner.Click += new System.EventHandler(this.buttonSelectScanner_Click);
            // 
            // labelScanner
            // 
            this.labelScanner.Location = new System.Drawing.Point(16, 32);
            this.labelScanner.Name = "labelScanner";
            this.labelScanner.Size = new System.Drawing.Size(56, 16);
            this.labelScanner.TabIndex = 1;
            this.labelScanner.Text = "Scanner:";
            // 
            // textBoxScannerName
            // 
            this.textBoxScannerName.Location = new System.Drawing.Point(80, 32);
            this.textBoxScannerName.Name = "textBoxScannerName";
            this.textBoxScannerName.Size = new System.Drawing.Size(120, 20);
            this.textBoxScannerName.TabIndex = 0;
            // 
            // groupBoxFileName
            // 
            this.groupBoxFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFileName.Controls.Add(this.buttonScanSingle);
            this.groupBoxFileName.Controls.Add(this.label5);
            this.groupBoxFileName.Controls.Add(this.label4);
            this.groupBoxFileName.Controls.Add(this.label3);
            this.groupBoxFileName.Controls.Add(this.label2);
            this.groupBoxFileName.Controls.Add(this.label1);
            this.groupBoxFileName.Controls.Add(this.buttonFiles);
            this.groupBoxFileName.Controls.Add(this.textBoxFilename);
            this.groupBoxFileName.Controls.Add(this.textBoxdir);
            this.groupBoxFileName.Controls.Add(this.textBoxroot);
            this.groupBoxFileName.Controls.Add(this.comboBoxcompress);
            this.groupBoxFileName.Controls.Add(this.comboBoxschema);
            this.groupBoxFileName.Controls.Add(this.checkBoxMulti);
            this.groupBoxFileName.Controls.Add(this.checkBoxView);
            this.groupBoxFileName.Controls.Add(this.checkBoxsave);
            this.groupBoxFileName.Controls.Add(this.checkBoxschema);
            this.groupBoxFileName.Controls.Add(this.buttonCancel);
            this.groupBoxFileName.Controls.Add(this.buttonScanBatch);
            this.groupBoxFileName.Location = new System.Drawing.Point(8, 192);
            this.groupBoxFileName.Name = "groupBoxFileName";
            this.groupBoxFileName.Size = new System.Drawing.Size(631, 245);
            this.groupBoxFileName.TabIndex = 3;
            this.groupBoxFileName.TabStop = false;
            // 
            // buttonScanSingle
            // 
            this.buttonScanSingle.Location = new System.Drawing.Point(32, 208);
            this.buttonScanSingle.Name = "buttonScanSingle";
            this.buttonScanSingle.Size = new System.Drawing.Size(80, 30);
            this.buttonScanSingle.TabIndex = 15;
            this.buttonScanSingle.Text = "Scan Single";
            this.buttonScanSingle.Click += new System.EventHandler(this.buttonScanSingle_Click_1);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 14;
            this.label5.Text = "FileName";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(136, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Compression";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(160, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "File Dir";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(152, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "File Root";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(160, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Schema";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonFiles
            // 
            this.buttonFiles.Location = new System.Drawing.Point(352, 160);
            this.buttonFiles.Name = "buttonFiles";
            this.buttonFiles.Size = new System.Drawing.Size(24, 24);
            this.buttonFiles.TabIndex = 9;
            this.buttonFiles.Text = "...";
            this.buttonFiles.Click += new System.EventHandler(this.buttonFiles_Click);
            // 
            // textBoxFilename
            // 
            this.textBoxFilename.Location = new System.Drawing.Point(112, 160);
            this.textBoxFilename.Name = "textBoxFilename";
            this.textBoxFilename.Size = new System.Drawing.Size(224, 20);
            this.textBoxFilename.TabIndex = 8;
            // 
            // textBoxdir
            // 
            this.textBoxdir.Location = new System.Drawing.Point(224, 88);
            this.textBoxdir.Name = "textBoxdir";
            this.textBoxdir.Size = new System.Drawing.Size(152, 20);
            this.textBoxdir.TabIndex = 7;
            // 
            // textBoxroot
            // 
            this.textBoxroot.Location = new System.Drawing.Point(224, 56);
            this.textBoxroot.Name = "textBoxroot";
            this.textBoxroot.Size = new System.Drawing.Size(152, 20);
            this.textBoxroot.TabIndex = 6;
            // 
            // comboBoxcompress
            // 
            this.comboBoxcompress.Items.AddRange(new object[] {
            "TIFF Raw",
            "TIFF PackBits",
            "TIFF G3",
            "TIFF G3M",
            "TIFF G4",
            "JPEG Color",
            "JPEG Grey",
            "DCX"});
            this.comboBoxcompress.Location = new System.Drawing.Point(224, 120);
            this.comboBoxcompress.Name = "comboBoxcompress";
            this.comboBoxcompress.Size = new System.Drawing.Size(152, 21);
            this.comboBoxcompress.TabIndex = 5;
            // 
            // comboBoxschema
            // 
            this.comboBoxschema.Items.AddRange(new object[] {
            "$####",
            "$###",
            "$##"});
            this.comboBoxschema.Location = new System.Drawing.Point(224, 24);
            this.comboBoxschema.Name = "comboBoxschema";
            this.comboBoxschema.Size = new System.Drawing.Size(152, 21);
            this.comboBoxschema.TabIndex = 4;
            // 
            // checkBoxMulti
            // 
            this.checkBoxMulti.Location = new System.Drawing.Point(16, 120);
            this.checkBoxMulti.Name = "checkBoxMulti";
            this.checkBoxMulti.Size = new System.Drawing.Size(104, 24);
            this.checkBoxMulti.TabIndex = 3;
            this.checkBoxMulti.Text = "Multi Page";
            // 
            // checkBoxView
            // 
            this.checkBoxView.Checked = true;
            this.checkBoxView.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxView.Location = new System.Drawing.Point(16, 88);
            this.checkBoxView.Name = "checkBoxView";
            this.checkBoxView.Size = new System.Drawing.Size(104, 24);
            this.checkBoxView.TabIndex = 2;
            this.checkBoxView.Text = "View Image";
            // 
            // checkBoxsave
            // 
            this.checkBoxsave.Location = new System.Drawing.Point(16, 56);
            this.checkBoxsave.Name = "checkBoxsave";
            this.checkBoxsave.Size = new System.Drawing.Size(104, 24);
            this.checkBoxsave.TabIndex = 1;
            this.checkBoxsave.Text = "Save File";
            this.checkBoxsave.CheckedChanged += new System.EventHandler(this.checkBoxsave_CheckedChanged);
            // 
            // checkBoxschema
            // 
            this.checkBoxschema.Location = new System.Drawing.Point(16, 24);
            this.checkBoxschema.Name = "checkBoxschema";
            this.checkBoxschema.Size = new System.Drawing.Size(104, 24);
            this.checkBoxschema.TabIndex = 0;
            this.checkBoxschema.Text = "Use Schema";
            this.checkBoxschema.CheckedChanged += new System.EventHandler(this.checkBoxschema_CheckedChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(312, 208);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 30);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonScanBatch
            // 
            this.buttonScanBatch.Location = new System.Drawing.Point(168, 208);
            this.buttonScanBatch.Name = "buttonScanBatch";
            this.buttonScanBatch.Size = new System.Drawing.Size(80, 30);
            this.buttonScanBatch.TabIndex = 5;
            this.buttonScanBatch.Text = "Scan Batch";
            this.buttonScanBatch.Click += new System.EventHandler(this.buttonScanBatch_Click);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.mnuAbout});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2});
            this.menuItem1.Text = "File";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Exit";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 1;
            this.mnuAbout.Text = "About";
            this.mnuAbout.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // statusBar1
            // 
            this.statusBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusBar1.Location = new System.Drawing.Point(0, 475);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Size = new System.Drawing.Size(647, 34);
            this.statusBar1.TabIndex = 5;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar1.Location = new System.Drawing.Point(0, 451);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(647, 24);
            this.progressBar1.TabIndex = 6;
            // 
            // isisXpress1
            // 
            this.isisXpress1.ErrorLevel = Accusoft.ISISXpressSdk.ErrorLevelInfo.Production;
            this.isisXpress1.FeederEmpty += new Accusoft.ISISXpressSdk.IsisXpress.FeederEmptyEventHandler(this.isisXpress1_FeederEmpty_1);
            this.isisXpress1.Scanned += new Accusoft.ISISXpressSdk.IsisXpress.ScannedEventHandler(this.isisXpress1_Scanned_1);
            this.isisXpress1.Progress += new Accusoft.ISISXpressSdk.IsisXpress.ProgressEventHandler(this.isisXpress1_Progress_1);
            this.isisXpress1.Scanning += new Accusoft.ISISXpressSdk.IsisXpress.ScanningEventHandler(this.isisXpress1_Scanning_1);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(647, 509);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.groupBoxFileName);
            this.Controls.Add(this.groupBox1);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ISIS Xpress Advanced Capability C# Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxFileName.ResumeLayout(false);
            this.groupBoxFileName.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{

			Application.Run(new Form1());
		}

	
		private void buttonSelectScanner_Click(object sender, System.EventArgs e)
		{
			try
			{
				statusBar1.Text = "";
				isisXpress1.Scanner.Select();
				textBoxScannerName.Text = isisXpress1.Scanner.Name;
				comboBoxTypes.SelectedIndex = 0;
			}
			catch(Exception ex)
			{
				
				statusBar1.Text = "The Error Reported Is: " + ex.Message;
			
			}

		}

		private void buttonScanSet_Click(object sender, System.EventArgs e)
		{
			try
			{
				isisXpress1.Scanner.Setup(Form1.ActiveForm.Handle.ToInt32(), Accusoft.ISISXpressSdk.SetupOptions.Normal);
				comboBoxTypes_Clicker();
			}
			catch(Exception ex)
			{
				
				statusBar1.Text = "The Error Reported Is: " + ex.Message;
				
			}
		
		}

		private void comboBoxTypes_Clicker()
		{
			String ScanSet;
			try
			{
				statusBar1.Text = "";
				ScanSet = comboBoxTypes.Items[comboBoxTypes.SelectedIndex].ToString();
				ScanSet = ScanSet.Remove(4,(ScanSet.Length - 4));  // Grab the 4 digits to get the TAG number

				// Set the enumeration so we always get 'Advanced Tags', this line is optional.
				isisXpress1.Scanner.Setting.ScanSetting = Accusoft.ISISXpressSdk.ScanSettingInfo.AdvancedTag;

				// By setting the Scanner.Setting.Advanced it will automatically set the enum in the line above.
				isisXpress1.Scanner.Setting.Advanced = Int32.Parse(ScanSet);  // Set the actual tag number

				if (isisXpress1.Scanner.Setting.Supported == true)
				{
					if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.RangeLong)
					{ // RangeLong
						labelMin.Text =  isisXpress1.Scanner.Setting.RangeMinimumLong.ToString();
						labelMax.Text = isisXpress1.Scanner.Setting.RangeMaximumLong.ToString();
						label6.Text = isisXpress1.Scanner.Setting.LongValue.ToString();
						hScrollBarSetting.Tag = false;
						hScrollBarSetting.Minimum = isisXpress1.Scanner.Setting.RangeMinimumLong;
						hScrollBarSetting.Maximum = isisXpress1.Scanner.Setting.RangeMaximumLong;
						hScrollBarSetting.Value = isisXpress1.Scanner.Setting.LongValue;
						hScrollBarSetting.Tag = true;
						if (hScrollBarSetting.Minimum == hScrollBarSetting.Maximum) 
							hScrollBarSetting.Enabled = false;
						else
							hScrollBarSetting.Enabled = true;
				
						comboBoxSetting.Visible = false;
						labelMin.Visible = true;
						labelMax.Visible = true;
						label6.Visible = true;
						hScrollBarSetting.Visible = true;
					} // if range
					else if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.RangeRational)
					{
					// This demo code deals with a simplistic set of the Denominator always being 1!

						RationalType MyRational;
						// For advanced the X & Y Resolution is normally encoded this way.
						MyRational = isisXpress1.Scanner.Setting.RangeMinimumRational();
						labelMin.Text = (MyRational.Numerator / MyRational.Denominator).ToString() ;

						MyRational = isisXpress1.Scanner.Setting.RangeMaximumRational();
						labelMax.Text = (MyRational.Numerator / MyRational.Denominator).ToString() ;

						MyRational = isisXpress1.Scanner.Setting.RationalValue;
						label6.Text = (MyRational.Numerator / MyRational.Denominator).ToString() ;

						hScrollBarSetting.Tag = false;
						

						MyRational = isisXpress1.Scanner.Setting.RangeMinimumRational();
						hScrollBarSetting.Minimum = (int) (MyRational.Numerator / MyRational.Denominator);

						MyRational = isisXpress1.Scanner.Setting.RangeMaximumRational();
						hScrollBarSetting.Maximum = (int) (MyRational.Numerator / MyRational.Denominator);

						MyRational = isisXpress1.Scanner.Setting.RationalValue;
						hScrollBarSetting.Value = (int) (MyRational.Numerator / MyRational.Denominator);

						hScrollBarSetting.Tag = true;
						if (hScrollBarSetting.Minimum == hScrollBarSetting.Maximum) 
							hScrollBarSetting.Enabled = false;
						else
							hScrollBarSetting.Enabled = true;
				
						comboBoxSetting.Visible = false;
						labelMin.Visible = true;
						labelMax.Visible = true;
						label6.Visible = true;
						hScrollBarSetting.Visible = true;

					}
					else if ((isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListString) ||
						(isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListLong) ||
						(isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListRational))
					{
						int Index;

						comboBoxSetting.Items.Clear();
					
						for (Index = 0; Index < isisXpress1.Scanner.Setting.ListCount ; Index++)
				
							if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListString)
							{
								comboBoxSetting.Items.Add(isisXpress1.Scanner.Setting.ListString(Index));
								//								isisXpress1.Scanner.Setting.ListIndex = Index;
								//								comboBoxSetting.Items.Add(isisXpress1.Scanner.Setting.ListString());
								if (isisXpress1.Scanner.Setting.ListString(Index) == isisXpress1.Scanner.Setting.StringValue)
									comboBoxSetting.SelectedIndex = Index;
							}
								// not ascii list long
							else if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListLong) 
							{
								
								comboBoxSetting.Items.Add(isisXpress1.Scanner.Setting.ListLong(Index));
								if (isisXpress1.Scanner.Setting.ListLong(Index) == isisXpress1.Scanner.Setting.LongValue)
									comboBoxSetting.SelectedIndex  = Index;
							}	
							else if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListRational) 
							{
								RationalType MyRational;
								MyRational = isisXpress1.Scanner.Setting.ListRational(Index);
								comboBoxSetting.Items.Add((MyRational.Numerator / MyRational.Denominator).ToString());
								if (isisXpress1.Scanner.Setting.ListRational(Index).Numerator  == MyRational.Numerator)
								    comboBoxSetting.SelectedIndex  = Index;

							}	
							else
							{
								
								statusBar1.Text = "Uncoded Data Type";
							}
						labelMax.Visible = false;
						labelMin.Visible = false;
						label6.Visible = false;
						hScrollBarSetting.Visible = false;
						comboBoxSetting.Visible = true;
					} 
					else
					{
						
						statusBar1.Text = "Uncoded Data Type";
					}
				}
				else 
				{
					labelMax.Visible = false;
					labelMin.Visible = false;
					label6.Visible = false;
					hScrollBarSetting.Visible = false;
					comboBoxSetting.Visible = false;
					
					statusBar1.Text = "Unsupported Data Type";

				}
	
			}
			catch ( Exception ex)
			{
				statusBar1.Text = "The Error Reported Is:" + ex.Message;
				
			}	
					
		
		}


		private void comboBoxTypes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			comboBoxTypes_Clicker();
		}

		private void hScrollBarSetting_ValueChanged(object sender, System.EventArgs e)
		{

			// Rat vs long here
			if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.RangeRational)
			{
				RationalType MyRational, Step;
				System.Int64 iStep;
				
				if ((bool)hScrollBarSetting.Tag == true)
				{
					MyRational = isisXpress1.Scanner.Setting.RationalValue;
					label6.Text = (MyRational.Numerator / MyRational.Denominator).ToString() ;
					
					// Find the step we need the rational to be near
					Step = isisXpress1.Scanner.Setting.RationalStep();
					iStep = (Step.Numerator / Step.Denominator);

					// Round down to a good value
					System.Int64  nMod = (hScrollBarSetting.Value % iStep);
					System.Int64  up, down;
					System.Int64   nLimit = 0;

					if (nMod != 0)
					{
						up   = hScrollBarSetting.Value + iStep - nMod;
						down = hScrollBarSetting.Value - nMod;
						if (down >= nLimit)
							hScrollBarSetting.Value = (System.Int32) down;
						else
							hScrollBarSetting.Value = (System.Int32)up;
					}
					// We seem to be able to set a long here
					MyRational.Numerator = hScrollBarSetting.Value;
					MyRational.Denominator = 1;
					isisXpress1.Scanner.Setting.RationalValue = MyRational;
				}
			}
			else //RangeLong 
			{
				if ((bool)hScrollBarSetting.Tag == true)
				{
					label6.Text = hScrollBarSetting.Value.ToString();
					isisXpress1.Scanner.Setting.LongValue = hScrollBarSetting.Value;
				}
			}

		}

		private void comboBoxSetting_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			RationalType MyRational = new RationalType();

			if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListString )
				isisXpress1.Scanner.Setting.StringValue = comboBoxSetting.Text;
			else if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListLong )
				isisXpress1.Scanner.Setting.LongValue = Int32.Parse (comboBoxSetting.Text);
			else if (isisXpress1.Scanner.Setting.DataType == Accusoft.ISISXpressSdk.DataTypeInfo.ListRational)
			{
				MyRational.Numerator = Int32.Parse (comboBoxSetting.Text);
				MyRational.Denominator = 1;
				isisXpress1.Scanner.Setting.RationalValue  = MyRational;
			}
	
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{

            //***Must call the UnlockRuntime to distribute runtime.
           // isisXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

			//set some default dialog settings
			saveFileDialog1.Filter =  "TIFF(*.tif)|*.tif|DCX(*.dcx)|*.dcx|JPEG(*.jpg)|*.jpg|RAW(*.raw)|*.raw";
			saveFileDialog1.DefaultExt = ".tif"; 
			
			textBoxScannerName.Text = isisXpress1.Scanner.Name;
			comboBoxTypes.SelectedIndex = 0;
			comboBoxschema.SelectedIndex = 0;
			comboBoxcompress.SelectedIndex = 4;
			comboBoxTypes_Clicker();
			checkBoxschema.Checked = false;
			checkBoxsave.Checked = false;
			ChkSchema_Clicker();
		}

		private void ChkSchema_Clicker()
		{
			if (checkBoxschema.Checked == true)
			{
				label5.Enabled = false;
				label2.Enabled = true;
				label3.Enabled = true;
				label1.Enabled = true;
				//set some basic properties
				isisXpress1.Output.UseSchema = true;
				isisXpress1.Output.CreateFiles = true;
				textBoxFilename.Enabled = false;
				comboBoxschema.Enabled = true;
				textBoxroot.Enabled = true;
				textBoxdir.Enabled = true;
                checkBoxsave.Enabled = false;
                buttonFiles.Enabled = false;
			}
			else
			{
				label5.Enabled = true;
				label2.Enabled = false;
				label3.Enabled = false;
				label1.Enabled = false;
				isisXpress1.Output.UseSchema = false;
				textBoxFilename.Enabled = true;
				comboBoxschema.Enabled = false;
				textBoxroot.Enabled = false;
				textBoxdir.Enabled = false;
                checkBoxsave.Enabled = true;
                buttonFiles.Enabled = true;
			}
		}

		private void checkBoxschema_CheckedChanged(object sender, System.EventArgs e)
		{
			ChkSchema_Clicker();
		}

		private void buttonFiles_Click(object sender, System.EventArgs e)
		{
   	   
			if(saveFileDialog1.ShowDialog() == DialogResult.OK)
				textBoxFilename.Text = saveFileDialog1.FileName;
		}

		private bool ScanUpdate()
		{
			bool result;
			
			//set the default settings for the scanners
			isisXpress1.Output.Directory = textBoxdir.Text;
			isisXpress1.Output.Filename = textBoxFilename.Text;
			isisXpress1.Output.Root = textBoxroot.Text;
			isisXpress1.Output.FileSchema = comboBoxschema.Text;
			isisXpress1.Output.FileType = (FileTypeInfo) comboBoxcompress.SelectedIndex;
			isisXpress1.Output.CreateFiles = checkBoxsave.Checked;
			isisXpress1.Output.CreateDib = checkBoxView.Checked;
			isisXpress1.Output.MultiplePages = checkBoxMulti.Checked;

			result = true;
			if ((isisXpress1.Output.CreateFiles == true) && (isisXpress1.Output.UseSchema == false) && (textBoxFilename.Text.Length == 0))
			{
				MessageBox.Show ("Please enter a filename.");
				result = false;
			}

			if ((isisXpress1.Output.CreateFiles == true) && (isisXpress1.Output.UseSchema == true) && (textBoxdir.Text.Length == 0))
			{
				MessageBox.Show ("Please enter a file directory.");
				result = false;
			}

			if ((isisXpress1.Output.CreateFiles == true) && (isisXpress1.Output.UseSchema == true) && (textBoxroot.Text.Length == 0))
			{
				MessageBox.Show ("Please enter a file root.");
				result = false;
			}
		
			return result;

		}

		private void buttonScanBatch_Click(object sender, System.EventArgs e)
		{
	
			try
			{
				progressBar1.Value = 0;
				if (ScanUpdate() == false) 
					return;
				buttonScanBatch.Enabled = false;
				buttonScanSingle.Enabled = false;
				buttonCancel.Enabled = true;
						
				//scan in batch mode
				isisXpress1.Scanner.ScanBatch();

			
				buttonScanBatch.Enabled =  true;
				buttonScanSingle.Enabled =  true;
				buttonCancel.Enabled = false;
			}
			catch(Exception ex)
			{
				buttonScanBatch.Enabled =  true;
				buttonScanSingle.Enabled =  true;
				buttonCancel.Enabled = false;
				statusBar1.Text = "The Error Reported Is:" + ex.Message;
				

			}
		}

		private void buttonCancel_Click(object sender, System.EventArgs e)
		{
			buttonCancel.Enabled = false; 
			isisXpress1.Scanner.Cancel(false);
		}


		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			Application.Exit();

		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			isisXpress1.AboutBox();
		}

		private void buttonScanSingle_Click_1(object sender, System.EventArgs e)
		{
			try
			{
				progressBar1.Value = 0;
				if (ScanUpdate() == false) 
					return;
				buttonScanBatch.Enabled = false;
				buttonScanSingle.Enabled = false;
				buttonCancel.Enabled = true;

				isisXpress1.Scanner.ScanSingle();

				buttonScanBatch.Enabled =  true;
				buttonScanSingle.Enabled =  true;
				buttonCancel.Enabled = false;
			}
			catch( Exception ex)
			{
				buttonScanBatch.Enabled =  true;
				buttonScanSingle.Enabled =  true;
				buttonCancel.Enabled = false;
				statusBar1.Text = "The Error Reported Is:" + ex.Message;
			}
		
		}

        private void isisXpress1_FeederEmpty_1(object sender, FeederEmptyEventArgs e)
        {
          
            MessageBox.Show("Click OK when scanner is ready");
        }

        private void isisXpress1_Progress_1(object sender, ProgressEventArgs e)
        {
           
            progressBar1.Maximum = 100;
            progressBar1.Minimum = 0;
            progressBar1.Value = e.Percent;
        }

        private void isisXpress1_Scanned_1(object sender, ScannedEventArgs e)
        {
        
            if (isisXpress1.Output.CreateDib == true && checkBoxView.Checked == true)
            {
                FormView viewer = new FormView();
                //**Please note: You could also use the ToBitMap method to transfer the image data from the ISISXpress control.
                viewer.AddImage(isisXpress1.Output.ToHdib());
                Application.DoEvents();
            }
        }

        private void isisXpress1_Scanning_1(object sender, ScanningEventArgs e)
        {
           
            textBoxFilename.Text = isisXpress1.Output.Filename;     
        }

        private void cmdScannerNames_Click(object sender, EventArgs e)
        {

          
            Accusoft.ISISXpressSdk.DriverAndScannerList driverscannerlist = new Accusoft.ISISXpressSdk.DriverAndScannerList();

            isisXpress1.Scanner.GetDriversAndScannerModels(out driverscannerlist);
            string scanners;

            for (int count = 0; count <= driverscannerlist.Count - 1; count++)
            {
                scanners = driverscannerlist[count].DriverName + " " + driverscannerlist[count].ScannerModel;
                lstScanners.Items.Add(scanners);
            }




        }

        private void checkBoxsave_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxsave.Checked) checkBoxschema.Enabled = false;
            else checkBoxschema.Enabled = true;
        }

    }
}

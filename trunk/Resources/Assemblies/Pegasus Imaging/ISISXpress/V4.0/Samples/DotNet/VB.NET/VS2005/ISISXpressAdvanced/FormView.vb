Public Class FormView
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Private WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormView))
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(16, 80)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(745, 472)
        Me.ImageXView1.TabIndex = 3
        '
        'label1
        '
        Me.label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(229, Byte), Integer), CType(CType(83, Byte), Integer), CType(CType(2, Byte), Integer))
        Me.label1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(13, 9)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(724, 64)
        Me.label1.TabIndex = 5
        Me.label1.Text = resources.GetString("label1.Text")
        '
        'FormView
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(784, 564)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.ImageXView1)
        Me.Name = "FormView"
        Me.Text = "FormView"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " Custom function code "
    Public Sub AddImage(ByVal DibHandle As System.IntPtr)
        'get the scanned DIB from ISIS 
        ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(ImagXpress1, DibHandle)
        If (Me.Visible = False) Then
            Me.ShowDialog()
        End If
    End Sub
#End Region
#Region " Form Event code "
    Private Sub FormView_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        FormView.ActiveForm.Close()
    End Sub
#End Region

End Class

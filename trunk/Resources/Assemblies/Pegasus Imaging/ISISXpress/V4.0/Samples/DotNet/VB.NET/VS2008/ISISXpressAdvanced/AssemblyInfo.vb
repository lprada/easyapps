Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ISIS Xpress CSharp Advanced")> 
<Assembly: AssemblyDescription("http://www.accusoft.com")> 
<Assembly: AssemblyCompany("Accusoft")> 
<Assembly: AssemblyProduct("ISIS Xpress CSharp Advanced")> 
<Assembly: AssemblyCopyright("Copyright � 2009 Pegasus Imaging Corporation")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 


'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("F40B87D8-B20E-4DF7-A69A-20588FE2927D")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 


Accusoft.PrintPro5.Net.Changes.txt

=====================================================================
Version 5.0.23.0 May 19th, 2010

Service Pack 2

- Copyright updated.


=====================================================================
Version 5.0.22.0 January 12th, 2010

- Fixed an issue where the paper size would not be maintained when set using the
  select printer dialog.
- Added Printer.SetAsDefault method.
- Fixed an issue where the paper size would reset when printing to file.

=====================================================================
Version 5.0.12.0 July 14th, 2009

Initial Release

- New namespace of Accusoft.PrintProSdk
- Use PrintPRO to create 64-bit, 32-bit, or AnyCPU-compiled applications.
- Fixed an issue where the template file was not closed after it is read.
- Fixed an issue where PrintPro would always throw a SystemException rather than a
  PrintProException.
- Fixed an issue where an exception would occur when displaying the printer dialog in
  a windowless application.
- Fixed an issue where the orientation would not be maintained when set using the
  select printer dialog.
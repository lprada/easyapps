<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.txtInfo = New System.Windows.Forms.TextBox
        Me.radioScale1to2 = New System.Windows.Forms.RadioButton
        Me.radioPrintPreview = New System.Windows.Forms.RadioButton
        Me.grpPrintPreview = New System.Windows.Forms.GroupBox
        Me.radioScale1to5 = New System.Windows.Forms.RadioButton
        Me.radioScale1to4 = New System.Windows.Forms.RadioButton
        Me.radioScale1to3 = New System.Windows.Forms.RadioButton
        Me.radioScale1to1 = New System.Windows.Forms.RadioButton
        Me.buttonPrintPreview = New System.Windows.Forms.Button
        Me.mnuMain = New System.Windows.Forms.MainMenu(Me.components)
        Me.miFile = New System.Windows.Forms.MenuItem
        Me.miOpenBitmap = New System.Windows.Forms.MenuItem
        Me.menuItem3 = New System.Windows.Forms.MenuItem
        Me.miQuit = New System.Windows.Forms.MenuItem
        Me.miAbout = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.radioPrintToFile = New System.Windows.Forms.RadioButton
        Me.btnPrint = New System.Windows.Forms.Button
        Me.grpPrintTo = New System.Windows.Forms.GroupBox
        Me.radioPrintToPrinter = New System.Windows.Forms.RadioButton
        Me.lblError = New System.Windows.Forms.Label
        Me.ix = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.ixvPreview = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.pp = New Accusoft.PrintProSdk.PrintPro(Me.components)
        Me.grpPrintPreview.SuspendLayout()
        Me.grpPrintTo.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtInfo
        '
        Me.txtInfo.BackColor = System.Drawing.SystemColors.Window
        Me.txtInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInfo.Location = New System.Drawing.Point(12, 6)
        Me.txtInfo.Multiline = True
        Me.txtInfo.Name = "txtInfo"
        Me.txtInfo.ReadOnly = True
        Me.txtInfo.Size = New System.Drawing.Size(630, 158)
        Me.txtInfo.TabIndex = 21
        Me.txtInfo.Text = resources.GetString("txtInfo.Text")
        '
        'radioScale1to2
        '
        Me.radioScale1to2.AutoSize = True
        Me.radioScale1to2.Location = New System.Drawing.Point(16, 57)
        Me.radioScale1to2.Name = "radioScale1to2"
        Me.radioScale1to2.Size = New System.Drawing.Size(70, 17)
        Me.radioScale1to2.TabIndex = 2
        Me.radioScale1to2.Text = "1:2 Scale"
        Me.radioScale1to2.UseVisualStyleBackColor = True
        '
        'radioPrintPreview
        '
        Me.radioPrintPreview.Location = New System.Drawing.Point(16, 68)
        Me.radioPrintPreview.Name = "radioPrintPreview"
        Me.radioPrintPreview.Size = New System.Drawing.Size(114, 20)
        Me.radioPrintPreview.TabIndex = 2
        Me.radioPrintPreview.Text = "Preview (to right)"
        Me.radioPrintPreview.UseVisualStyleBackColor = True
        '
        'grpPrintPreview
        '
        Me.grpPrintPreview.Controls.Add(Me.radioScale1to5)
        Me.grpPrintPreview.Controls.Add(Me.radioScale1to4)
        Me.grpPrintPreview.Controls.Add(Me.radioScale1to3)
        Me.grpPrintPreview.Controls.Add(Me.radioScale1to2)
        Me.grpPrintPreview.Controls.Add(Me.radioScale1to1)
        Me.grpPrintPreview.Controls.Add(Me.buttonPrintPreview)
        Me.grpPrintPreview.Enabled = False
        Me.grpPrintPreview.Location = New System.Drawing.Point(12, 339)
        Me.grpPrintPreview.Name = "grpPrintPreview"
        Me.grpPrintPreview.Size = New System.Drawing.Size(200, 225)
        Me.grpPrintPreview.TabIndex = 19
        Me.grpPrintPreview.TabStop = False
        Me.grpPrintPreview.Text = "Print Preview"
        '
        'radioScale1to5
        '
        Me.radioScale1to5.AutoSize = True
        Me.radioScale1to5.Location = New System.Drawing.Point(16, 126)
        Me.radioScale1to5.Name = "radioScale1to5"
        Me.radioScale1to5.Size = New System.Drawing.Size(70, 17)
        Me.radioScale1to5.TabIndex = 5
        Me.radioScale1to5.Text = "1:5 Scale"
        Me.radioScale1to5.UseVisualStyleBackColor = True
        '
        'radioScale1to4
        '
        Me.radioScale1to4.AutoSize = True
        Me.radioScale1to4.Location = New System.Drawing.Point(16, 103)
        Me.radioScale1to4.Name = "radioScale1to4"
        Me.radioScale1to4.Size = New System.Drawing.Size(70, 17)
        Me.radioScale1to4.TabIndex = 4
        Me.radioScale1to4.Text = "1:4 Scale"
        Me.radioScale1to4.UseVisualStyleBackColor = True
        '
        'radioScale1to3
        '
        Me.radioScale1to3.AutoSize = True
        Me.radioScale1to3.Location = New System.Drawing.Point(16, 80)
        Me.radioScale1to3.Name = "radioScale1to3"
        Me.radioScale1to3.Size = New System.Drawing.Size(70, 17)
        Me.radioScale1to3.TabIndex = 3
        Me.radioScale1to3.Text = "1:3 Scale"
        Me.radioScale1to3.UseVisualStyleBackColor = True
        '
        'radioScale1to1
        '
        Me.radioScale1to1.AutoSize = True
        Me.radioScale1to1.Checked = True
        Me.radioScale1to1.Location = New System.Drawing.Point(16, 34)
        Me.radioScale1to1.Name = "radioScale1to1"
        Me.radioScale1to1.Size = New System.Drawing.Size(70, 17)
        Me.radioScale1to1.TabIndex = 1
        Me.radioScale1to1.TabStop = True
        Me.radioScale1to1.Text = "1:1 Scale"
        Me.radioScale1to1.UseVisualStyleBackColor = True
        '
        'buttonPrintPreview
        '
        Me.buttonPrintPreview.Location = New System.Drawing.Point(16, 163)
        Me.buttonPrintPreview.Name = "buttonPrintPreview"
        Me.buttonPrintPreview.Size = New System.Drawing.Size(152, 32)
        Me.buttonPrintPreview.TabIndex = 0
        Me.buttonPrintPreview.Text = "Print Preview"
        '
        'mnuMain
        '
        Me.mnuMain.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miFile, Me.miAbout})
        '
        'miFile
        '
        Me.miFile.Index = 0
        Me.miFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.miOpenBitmap, Me.menuItem3, Me.miQuit})
        Me.miFile.Text = "&File"
        '
        'miOpenBitmap
        '
        Me.miOpenBitmap.Index = 0
        Me.miOpenBitmap.Text = "&Open Image to print..."
        '
        'menuItem3
        '
        Me.menuItem3.Index = 1
        Me.menuItem3.Text = "-"
        '
        'miQuit
        '
        Me.miQuit.Index = 2
        Me.miQuit.Text = "&Quit"
        '
        'miAbout
        '
        Me.miAbout.Index = 1
        Me.miAbout.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        Me.miAbout.Text = "&About"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Imag&Xpress"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "&PrintPro"
        '
        'radioPrintToFile
        '
        Me.radioPrintToFile.Location = New System.Drawing.Point(16, 16)
        Me.radioPrintToFile.Name = "radioPrintToFile"
        Me.radioPrintToFile.Size = New System.Drawing.Size(168, 20)
        Me.radioPrintToFile.TabIndex = 0
        Me.radioPrintToFile.Text = "File"
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(36, 284)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(152, 32)
        Me.btnPrint.TabIndex = 18
        Me.btnPrint.Text = "Print"
        '
        'grpPrintTo
        '
        Me.grpPrintTo.Controls.Add(Me.radioPrintToPrinter)
        Me.grpPrintTo.Controls.Add(Me.radioPrintPreview)
        Me.grpPrintTo.Controls.Add(Me.radioPrintToFile)
        Me.grpPrintTo.Location = New System.Drawing.Point(12, 170)
        Me.grpPrintTo.Name = "grpPrintTo"
        Me.grpPrintTo.Size = New System.Drawing.Size(200, 96)
        Me.grpPrintTo.TabIndex = 20
        Me.grpPrintTo.TabStop = False
        Me.grpPrintTo.Text = "Print To?"
        '
        'radioPrintToPrinter
        '
        Me.radioPrintToPrinter.Checked = True
        Me.radioPrintToPrinter.Location = New System.Drawing.Point(16, 42)
        Me.radioPrintToPrinter.Name = "radioPrintToPrinter"
        Me.radioPrintToPrinter.Size = New System.Drawing.Size(160, 20)
        Me.radioPrintToPrinter.TabIndex = 1
        Me.radioPrintToPrinter.TabStop = True
        Me.radioPrintToPrinter.Text = "Printer"
        '
        'lblError
        '
        Me.lblError.Location = New System.Drawing.Point(442, 537)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(200, 40)
        Me.lblError.TabIndex = 0
        '
        'ixvPreview
        '
        Me.ixvPreview.Location = New System.Drawing.Point(223, 170)
        Me.ixvPreview.Name = "ixvPreview"
        Me.ixvPreview.Size = New System.Drawing.Size(419, 330)
        Me.ixvPreview.TabIndex = 22
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 602)
        Me.Controls.Add(Me.ixvPreview)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.txtInfo)
        Me.Controls.Add(Me.grpPrintPreview)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.grpPrintTo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.mnuMain
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PrintPRO Demo"
        Me.grpPrintPreview.ResumeLayout(False)
        Me.grpPrintPreview.PerformLayout()
        Me.grpPrintTo.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents txtInfo As System.Windows.Forms.TextBox
    Private WithEvents radioScale1to2 As System.Windows.Forms.RadioButton
    Private WithEvents radioPrintPreview As System.Windows.Forms.RadioButton
    Private WithEvents grpPrintPreview As System.Windows.Forms.GroupBox
    Private WithEvents radioScale1to5 As System.Windows.Forms.RadioButton
    Private WithEvents radioScale1to4 As System.Windows.Forms.RadioButton
    Private WithEvents radioScale1to3 As System.Windows.Forms.RadioButton
    Private WithEvents radioScale1to1 As System.Windows.Forms.RadioButton
    Private WithEvents buttonPrintPreview As System.Windows.Forms.Button
    Private WithEvents mnuMain As System.Windows.Forms.MainMenu
    Private WithEvents miFile As System.Windows.Forms.MenuItem
    Private WithEvents miOpenBitmap As System.Windows.Forms.MenuItem
    Private WithEvents menuItem3 As System.Windows.Forms.MenuItem
    Private WithEvents miQuit As System.Windows.Forms.MenuItem
    Private WithEvents miAbout As System.Windows.Forms.MenuItem
    Private WithEvents radioPrintToFile As System.Windows.Forms.RadioButton
    Private WithEvents btnPrint As System.Windows.Forms.Button
    Private WithEvents grpPrintTo As System.Windows.Forms.GroupBox
    Private WithEvents radioPrintToPrinter As System.Windows.Forms.RadioButton
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents ix As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents ixvPreview As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents pp As Accusoft.PrintProSdk.PrintPro

End Class

/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DrawingCSharp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();

            /*** You must unlock the control with your provided unlock codes for distribution. ***/
            //pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();

                if (pp != null)
                    pp.Dispose();
            }
            base.Dispose(disposing);
        }

        private void miAbout_Click(object sender, EventArgs e)
        {
            pp.AboutBox();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPrintCircles_Click(object sender, EventArgs e)
        {

            try
            {
                Accusoft.PrintProSdk.Printer printer;
                Accusoft.PrintProSdk.PrintJob job;

                /* Prompt the user for which printer to use and then create a PrintJob for that printer. */
                printer = Accusoft.PrintProSdk.Printer.SelectPrinter(pp, true);
                if (printer != null)
                {
                    printer.RequestedDpi = 96;
                    job = new Accusoft.PrintProSdk.PrintJob(printer);

                    /* Draw a border around the display area. */
                    job.Draw.Rectangle(new System.Drawing.RectangleF(job.DisplayX, job.DisplayY, job.PrintWidth - 1, job.PrintHeight - 1), false);

                    /* This code below draws 5 concentric circles, starting with a large one
                     * covering nearly the whole page, then printing successively smaller ones
                     * on top of it (with alternating colors) to form a bullseye. */
                    float r = job.PrintWidth / 10;
                    System.Drawing.PointF center =
                        new System.Drawing.PointF(job.DisplayX + job.PrintWidth / 2, job.DisplayY + job.PrintHeight / 2);  //centers the circles on the page
                    job.Draw.FillStyle = Accusoft.PrintProSdk.FillStyle.Solid;
                    job.Draw.BackStyle = Accusoft.PrintProSdk.BackStyle.Transparent;
                    for (int i = 5; i > 0; i--)
                    {
                        if (i % 2 != 0) //odd circles black
                        {
                            job.Draw.FillColor = System.Drawing.Color.Black;
                        }
                        else //even circles white
                        {
                            job.Draw.FillColor = System.Drawing.Color.White;
                        }

                        job.Draw.Circle(center, r * i, System.Drawing.Color.White, 1);
                    }

                    /* Print a message describing the shape at the upper-left corner of the page. */
                    System.Drawing.Font font = new System.Drawing.Font("Times New Roman", 14, System.Drawing.FontStyle.Bold);
                    job.Draw.Text(new System.Drawing.PointF(job.DisplayX, job.DisplayY), "PrintPRO Bullseye Example", font);

                    /* End the current page and print. */
                    job.Finish();

                }

            }
            catch (System.Exception ex)
            {

                PegasusError(ex, lblError);
            }

        }

        private void btnPrintGrid_Click(object sender, EventArgs e)
        {

            try
            {
                Accusoft.PrintProSdk.Printer printer;
                Accusoft.PrintProSdk.PrintJob job;

                /* Prompt the user for which printer to use and then create a PrintJob for that printer. */
                printer = Accusoft.PrintProSdk.Printer.SelectPrinter(pp, true);
                if (printer != null)
                {

                    printer.RequestedDpi = 96;
                    job = new Accusoft.PrintProSdk.PrintJob(printer);

                    /* Make a RectangleF struct for the whole print area. */
                    System.Drawing.RectangleF printBounds =
                        new System.Drawing.RectangleF(job.DisplayX, job.DisplayY, job.PrintWidth - 1, job.PrintHeight - 1);

                    /* And draw a border around it. */
                    job.Draw.Rectangle(printBounds, false);

                    /* Draw the vertical lines. */
                    float x = job.PrintWidth / 50;
                    for (int i = 1; i < 50; i++)
                    {
                        job.Draw.Line(
                            new System.Drawing.PointF(job.DisplayX + x * i, job.DisplayY),
                            new System.Drawing.PointF(job.DisplayX + x * i, job.DisplayY + job.PrintHeight),
                            false, false);
                    }

                    /* Draw the horizontal lines. */
                    float y = job.PrintHeight / 50;
                    for (int i = 1; i < 50; i++)
                    {
                        job.Draw.Line(
                            new System.Drawing.PointF(job.DisplayX, job.DisplayY + y * i),
                            new System.Drawing.PointF(job.DisplayX + job.PrintWidth, job.DisplayY + y * i),
                            false, false);
                    }

                    /* Print a message describing the shape. */
                    System.Drawing.Font font = new System.Drawing.Font("Times New Roman", 72, System.Drawing.FontStyle.Bold);
                    job.Draw.TextAligned(printBounds, "PrintPRO 50x50 grid example", font,
                        Accusoft.PrintProSdk.Alignment.CenterJustifyMiddle, true);

                    /* End the current page and print. */
                    job.Finish();
                }

            }
            catch (System.Exception ex)
            {

                PegasusError(ex, lblError);
            }
        }

        #region Pegasus Imaging Sample Application Standard Functions
        /*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

        private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
        private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
        System.String currentDir = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");

        private const System.String strNoImageError = "You must select an image file to open.";
        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }
        string PegasusOpenFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        string PegasusOpenFile(System.String strFilter)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
        {
            System.Int32 iTmp;
            try
            {
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber);
            }
            catch (System.NullReferenceException ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
            {
                scrScroll.Value = iTmp;
            }
            else
            {
                iTmp = scrScroll.Value;
            }
            textTextBox.Text = iTmp.ToString(cultNumber);
        }
        #endregion

        private void frmMain_Load(object sender, EventArgs e)
        {   
            //**Must call the UnlockRuntime to Distribute application 
            //pp.Licensing.UnlockRuntime(1234,1234,1234,1234);
            Application.EnableVisualStyles();
        }

    }
}

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnPrintGrid = New System.Windows.Forms.Button
        Me.btnPrintCircles = New System.Windows.Forms.Button
        Me.miFile = New System.Windows.Forms.ToolStripMenuItem
        Me.miExit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuMain = New System.Windows.Forms.MenuStrip
        Me.miAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.lblerror = New System.Windows.Forms.Label
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.pp = New Accusoft.PrintProSdk.PrintPro(Me.components)
        Me.mnuMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnPrintGrid
        '
        Me.btnPrintGrid.Location = New System.Drawing.Point(12, 155)
        Me.btnPrintGrid.Name = "btnPrintGrid"
        Me.btnPrintGrid.Size = New System.Drawing.Size(74, 29)
        Me.btnPrintGrid.TabIndex = 7
        Me.btnPrintGrid.Text = "Print Grid"
        Me.btnPrintGrid.UseVisualStyleBackColor = True
        '
        'btnPrintCircles
        '
        Me.btnPrintCircles.Location = New System.Drawing.Point(12, 120)
        Me.btnPrintCircles.Name = "btnPrintCircles"
        Me.btnPrintCircles.Size = New System.Drawing.Size(74, 29)
        Me.btnPrintCircles.TabIndex = 0
        Me.btnPrintCircles.Text = "Print Circles"
        Me.btnPrintCircles.UseVisualStyleBackColor = True
        '
        'miFile
        '
        Me.miFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miExit})
        Me.miFile.Name = "miFile"
        Me.miFile.Size = New System.Drawing.Size(37, 20)
        Me.miFile.Text = "&File"
        '
        'miExit
        '
        Me.miExit.Name = "miExit"
        Me.miExit.Size = New System.Drawing.Size(92, 22)
        Me.miExit.Text = "E&xit"
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miFile, Me.miAbout})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(357, 24)
        Me.mnuMain.TabIndex = 4
        Me.mnuMain.Text = "menuStrip1"
        '
        'miAbout
        '
        Me.miAbout.Name = "miAbout"
        Me.miAbout.Size = New System.Drawing.Size(52, 20)
        Me.miAbout.Text = "&About"
        '
        'lblerror
        '
        Me.lblerror.Location = New System.Drawing.Point(136, 210)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(144, 40)
        Me.lblerror.TabIndex = 8
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates the following functionality of PrintPRO:", "1) Printing concentric circles on a page.", "2) Printing a 50x50 grid on a page."})
        Me.ListBox1.Location = New System.Drawing.Point(12, 27)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(333, 69)
        Me.ListBox1.TabIndex = 9
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(357, 273)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.lblerror)
        Me.Controls.Add(Me.btnPrintGrid)
        Me.Controls.Add(Me.btnPrintCircles)
        Me.Controls.Add(Me.mnuMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PrintPRO Drawing Sample"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents btnPrintGrid As System.Windows.Forms.Button
    Private WithEvents btnPrintCircles As System.Windows.Forms.Button
    Private WithEvents miFile As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents miExit As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Private WithEvents miAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents pp As Accusoft.PrintProSdk.PrintPro

End Class

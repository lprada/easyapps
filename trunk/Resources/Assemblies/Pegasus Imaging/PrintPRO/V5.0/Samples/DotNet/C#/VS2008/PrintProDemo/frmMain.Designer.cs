namespace PrintPRODemoCSharp
{
    public partial class frmMain
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnPrint = new System.Windows.Forms.Button();
            this.grpPrintPreview = new System.Windows.Forms.GroupBox();
            this.radioScale1to5 = new System.Windows.Forms.RadioButton();
            this.radioScale1to4 = new System.Windows.Forms.RadioButton();
            this.radioScale1to3 = new System.Windows.Forms.RadioButton();
            this.radioScale1to2 = new System.Windows.Forms.RadioButton();
            this.radioScale1to1 = new System.Windows.Forms.RadioButton();
            this.buttonPrintPreview = new System.Windows.Forms.Button();
            this.grpPrintTo = new System.Windows.Forms.GroupBox();
            this.radioPrintToPrinter = new System.Windows.Forms.RadioButton();
            this.radioPrintPreview = new System.Windows.Forms.RadioButton();
            this.radioPrintToFile = new System.Windows.Forms.RadioButton();
            this.mnuMain = new System.Windows.Forms.MainMenu(this.components);
            this.miFile = new System.Windows.Forms.MenuItem();
            this.miOpenBitmap = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.miQuit = new System.Windows.Forms.MenuItem();
            this.miAbout = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.lblError = new System.Windows.Forms.Label();
            this.ix = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.ixvPreview = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.pp = new Accusoft.PrintProSdk.PrintPro(this.components);
            this.grpPrintPreview.SuspendLayout();
            this.grpPrintTo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(40, 290);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(152, 32);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // grpPrintPreview
            // 
            this.grpPrintPreview.Controls.Add(this.radioScale1to5);
            this.grpPrintPreview.Controls.Add(this.radioScale1to4);
            this.grpPrintPreview.Controls.Add(this.radioScale1to3);
            this.grpPrintPreview.Controls.Add(this.radioScale1to2);
            this.grpPrintPreview.Controls.Add(this.radioScale1to1);
            this.grpPrintPreview.Controls.Add(this.buttonPrintPreview);
            this.grpPrintPreview.Enabled = false;
            this.grpPrintPreview.Location = new System.Drawing.Point(16, 345);
            this.grpPrintPreview.Name = "grpPrintPreview";
            this.grpPrintPreview.Size = new System.Drawing.Size(200, 225);
            this.grpPrintPreview.TabIndex = 3;
            this.grpPrintPreview.TabStop = false;
            this.grpPrintPreview.Text = "Print Preview";
            // 
            // radioScale1to5
            // 
            this.radioScale1to5.AutoSize = true;
            this.radioScale1to5.Location = new System.Drawing.Point(16, 126);
            this.radioScale1to5.Name = "radioScale1to5";
            this.radioScale1to5.Size = new System.Drawing.Size(70, 17);
            this.radioScale1to5.TabIndex = 5;
            this.radioScale1to5.Text = "1:5 Scale";
            this.radioScale1to5.UseVisualStyleBackColor = true;
            // 
            // radioScale1to4
            // 
            this.radioScale1to4.AutoSize = true;
            this.radioScale1to4.Location = new System.Drawing.Point(16, 103);
            this.radioScale1to4.Name = "radioScale1to4";
            this.radioScale1to4.Size = new System.Drawing.Size(70, 17);
            this.radioScale1to4.TabIndex = 4;
            this.radioScale1to4.Text = "1:4 Scale";
            this.radioScale1to4.UseVisualStyleBackColor = true;
            // 
            // radioScale1to3
            // 
            this.radioScale1to3.AutoSize = true;
            this.radioScale1to3.Location = new System.Drawing.Point(16, 80);
            this.radioScale1to3.Name = "radioScale1to3";
            this.radioScale1to3.Size = new System.Drawing.Size(70, 17);
            this.radioScale1to3.TabIndex = 3;
            this.radioScale1to3.Text = "1:3 Scale";
            this.radioScale1to3.UseVisualStyleBackColor = true;
            // 
            // radioScale1to2
            // 
            this.radioScale1to2.AutoSize = true;
            this.radioScale1to2.Location = new System.Drawing.Point(16, 57);
            this.radioScale1to2.Name = "radioScale1to2";
            this.radioScale1to2.Size = new System.Drawing.Size(70, 17);
            this.radioScale1to2.TabIndex = 2;
            this.radioScale1to2.Text = "1:2 Scale";
            this.radioScale1to2.UseVisualStyleBackColor = true;
            // 
            // radioScale1to1
            // 
            this.radioScale1to1.AutoSize = true;
            this.radioScale1to1.Checked = true;
            this.radioScale1to1.Location = new System.Drawing.Point(16, 34);
            this.radioScale1to1.Name = "radioScale1to1";
            this.radioScale1to1.Size = new System.Drawing.Size(70, 17);
            this.radioScale1to1.TabIndex = 1;
            this.radioScale1to1.TabStop = true;
            this.radioScale1to1.Text = "1:1 Scale";
            this.radioScale1to1.UseVisualStyleBackColor = true;
            // 
            // buttonPrintPreview
            // 
            this.buttonPrintPreview.Location = new System.Drawing.Point(16, 163);
            this.buttonPrintPreview.Name = "buttonPrintPreview";
            this.buttonPrintPreview.Size = new System.Drawing.Size(152, 32);
            this.buttonPrintPreview.TabIndex = 0;
            this.buttonPrintPreview.Text = "Print Preview";
            this.buttonPrintPreview.Click += new System.EventHandler(this.buttonPrintPreview_Click);
            // 
            // grpPrintTo
            // 
            this.grpPrintTo.Controls.Add(this.radioPrintToPrinter);
            this.grpPrintTo.Controls.Add(this.radioPrintPreview);
            this.grpPrintTo.Controls.Add(this.radioPrintToFile);
            this.grpPrintTo.Location = new System.Drawing.Point(16, 176);
            this.grpPrintTo.Name = "grpPrintTo";
            this.grpPrintTo.Size = new System.Drawing.Size(200, 96);
            this.grpPrintTo.TabIndex = 6;
            this.grpPrintTo.TabStop = false;
            this.grpPrintTo.Text = "Print To Options";
            // 
            // radioPrintToPrinter
            // 
            this.radioPrintToPrinter.Checked = true;
            this.radioPrintToPrinter.Location = new System.Drawing.Point(16, 42);
            this.radioPrintToPrinter.Name = "radioPrintToPrinter";
            this.radioPrintToPrinter.Size = new System.Drawing.Size(160, 20);
            this.radioPrintToPrinter.TabIndex = 1;
            this.radioPrintToPrinter.TabStop = true;
            this.radioPrintToPrinter.Text = "Printer";
            this.radioPrintToPrinter.CheckedChanged += new System.EventHandler(this.radioPrintToPrinter_CheckedChanged);
            // 
            // radioPrintPreview
            // 
            this.radioPrintPreview.Location = new System.Drawing.Point(16, 68);
            this.radioPrintPreview.Name = "radioPrintPreview";
            this.radioPrintPreview.Size = new System.Drawing.Size(114, 20);
            this.radioPrintPreview.TabIndex = 2;
            this.radioPrintPreview.Text = "Preview (to right)";
            this.radioPrintPreview.UseVisualStyleBackColor = true;
            this.radioPrintPreview.CheckedChanged += new System.EventHandler(this.radioPrintPreview_CheckedChanged);
            // 
            // radioPrintToFile
            // 
            this.radioPrintToFile.Location = new System.Drawing.Point(16, 16);
            this.radioPrintToFile.Name = "radioPrintToFile";
            this.radioPrintToFile.Size = new System.Drawing.Size(168, 20);
            this.radioPrintToFile.TabIndex = 0;
            this.radioPrintToFile.Text = "File";
            this.radioPrintToFile.CheckedChanged += new System.EventHandler(this.radioPrintToFile_CheckedChanged);
            // 
            // mnuMain
            // 
            this.mnuMain.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miFile,
            this.miAbout});
            // 
            // miFile
            // 
            this.miFile.Index = 0;
            this.miFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miOpenBitmap,
            this.menuItem3,
            this.miQuit});
            this.miFile.Text = "&File";
            // 
            // miOpenBitmap
            // 
            this.miOpenBitmap.Index = 0;
            this.miOpenBitmap.Text = "&Open Image to print...";
            this.miOpenBitmap.Click += new System.EventHandler(this.miOpenBitmap_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "-";
            // 
            // miQuit
            // 
            this.miQuit.Index = 2;
            this.miQuit.Text = "&Quit";
            this.miQuit.Click += new System.EventHandler(this.miQuit_Click);
            // 
            // miAbout
            // 
            this.miAbout.Index = 1;
            this.miAbout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem2});
            this.miAbout.Text = "&About";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "Imag&Xpress";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "&PrintPro";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // txtInfo
            // 
            this.txtInfo.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfo.Location = new System.Drawing.Point(16, 12);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            this.txtInfo.Size = new System.Drawing.Size(630, 158);
            this.txtInfo.TabIndex = 13;
            this.txtInfo.Text = resources.GetString("txtInfo.Text");
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(413, 557);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(233, 70);
            this.lblError.TabIndex = 0;
            // 
            // ixvPreview
            // 
            this.ixvPreview.Location = new System.Drawing.Point(264, 192);
            this.ixvPreview.Name = "ixvPreview";
            this.ixvPreview.Size = new System.Drawing.Size(381, 348);
            this.ixvPreview.TabIndex = 14;
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(681, 650);
            this.Controls.Add(this.ixvPreview);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.grpPrintTo);
            this.Controls.Add(this.grpPrintPreview);
            this.Controls.Add(this.btnPrint);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mnuMain;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintPRO Demo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.grpPrintPreview.ResumeLayout(false);
            this.grpPrintPreview.PerformLayout();
            this.grpPrintTo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.GroupBox grpPrintPreview;
        private System.Windows.Forms.Button buttonPrintPreview;

        private System.Windows.Forms.GroupBox grpPrintTo;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.RadioButton radioPrintToFile;
        private System.Windows.Forms.RadioButton radioPrintToPrinter;
        private System.Windows.Forms.MainMenu mnuMain;
        private System.Windows.Forms.MenuItem miFile;
        private System.Windows.Forms.MenuItem miOpenBitmap;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem miQuit;
        private System.Windows.Forms.MenuItem miAbout;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.RadioButton radioPrintPreview;
        private System.Windows.Forms.RadioButton radioScale1to5;
        private System.Windows.Forms.RadioButton radioScale1to4;
        private System.Windows.Forms.RadioButton radioScale1to3;
        private System.Windows.Forms.RadioButton radioScale1to2;
        private System.Windows.Forms.RadioButton radioScale1to1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.Label lblError;
        private Accusoft.ImagXpressSdk.ImagXpress ix;
        private Accusoft.ImagXpressSdk.ImageXView ixvPreview;
        private Accusoft.PrintProSdk.PrintPro pp;
    }
}
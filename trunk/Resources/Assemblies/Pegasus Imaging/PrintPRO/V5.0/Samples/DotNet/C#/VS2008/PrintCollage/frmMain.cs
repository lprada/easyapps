/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PrintCollageCSharp
{
    public partial class frmMain : Form
    {
        /// <summary>
        /// Returns the path to the specified sample image.
        /// </summary>
        static string getSampleImage(string image)
        {
            return Application.StartupPath + @"..\..\..\..\..\..\..\..\..\..\..\Common\Images\" + image;
        }

        public frmMain()
        {
            InitializeComponent();

            /*** You must unlock the controls with your provided unlock codes for distribution. ***/
            //ix.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
            //pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();
                if (ix != null)
                    ix.Dispose();
                if (pp != null)
                    pp.Dispose();
            }
            base.Dispose(disposing);
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {

            try
            {
                Accusoft.PrintProSdk.Printer printer;
                Accusoft.PrintProSdk.PrintJob job;
                System.Drawing.Bitmap bitmap;
                Accusoft.ImagXpressSdk.ImageX imagex;

                /* create a file printer and a job to go with it */
                printer = Accusoft.PrintProSdk.Printer.CreateFilePrinter(pp, "collage.tif",
                    Accusoft.PrintProSdk.SaveFileType.Tif);
                printer.RequestedDpi = 120;
                job = new Accusoft.PrintProSdk.PrintJob(printer);

                /* set the template file */
                job.TemplateFileName = getSampleImage("template.txt");

                /* print a few images using a .NET Bitmap object and PrintTemplateImage */
                bitmap = new System.Drawing.Bitmap(getSampleImage("dome.jpg"));
                job.PrintTemplateImage(bitmap, 1);

                bitmap = new System.Drawing.Bitmap(getSampleImage("water.jpg"));
                job.PrintTemplateImage(bitmap, 2);

                bitmap = new System.Drawing.Bitmap(getSampleImage("pic1.bmp"));
                job.PrintTemplateImage(bitmap, 3);

                /* print one using an HDIB and ImagXpress */
                imagex = Accusoft.ImagXpressSdk.ImageX.FromFile(ix, getSampleImage("pic1.bmp"));
                job.PrintTemplateDib(imagex.ToHdib(true), 4);

                /* end the page and "print" the image */
                job.Finish();

                /* display the printed image file on the form */
                if (ixvCollage.Image != null)
                    ixvCollage.Image.Dispose();
                ixvCollage.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(ix, "collage.tif");
                ixvCollage.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;

            }
            catch(Exception ex)
            {
                PegasusError(ex, lblError);
                      
            }
        }

        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ix.AboutBox();
        }

        private void printProToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pp.AboutBox();
        }

        #region Pegasus Imaging Sample Application Standard Functions
        /*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

        private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
        private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
        System.String currentDir = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");

        private const System.String strNoImageError = "You must select an image file to open.";
        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }
        string PegasusOpenFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        string PegasusOpenFile(System.String strFilter)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
        {
            System.Int32 iTmp;
            try
            {
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber);
            }
            catch (System.NullReferenceException ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
            {
                scrScroll.Value = iTmp;
            }
            else
            {
                iTmp = scrScroll.Value;
            }
            textTextBox.Text = iTmp.ToString(cultNumber);
        }
        #endregion

        private void frmMain_Load(object sender, EventArgs e)
        {
            //**Must call the UnlockRuntime to Distru
            //pp.Licensing.UnlockRuntime(1234,1234,1234,1234);
     
            Application.EnableVisualStyles();
        }
    }
}

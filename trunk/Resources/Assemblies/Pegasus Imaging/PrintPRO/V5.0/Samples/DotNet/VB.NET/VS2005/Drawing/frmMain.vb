'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Public Class frmMain

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "..\..\..\..\..\..\..\..\..\..\Common\Images")

    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub
    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Application.EnableVisualStyles()
        ' *** You must unlock the control with your provided unlock codes for distribution. ***
        ' pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)

        Application.EnableVisualStyles()
    End Sub

    Private Sub miExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miExit.Click
        Application.Exit()
    End Sub

    Private Sub miAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miAbout.Click
        pp.AboutBox()
    End Sub

    Private Sub btnPrintCircles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintCircles.Click

        Try

            Dim printer As Accusoft.PrintProSdk.Printer
            Dim job As Accusoft.PrintProSdk.PrintJob

            ' Prompt the user for which printer to use and then create a PrintJob for that printer.
            printer = Accusoft.PrintProSdk.Printer.SelectPrinter(pp, True)
            printer.RequestedDpi = 96
            job = New Accusoft.PrintProSdk.PrintJob(printer)

            ' Draw a border around the display area.
            job.Draw.Rectangle(New System.Drawing.RectangleF(job.DisplayX, job.DisplayY, job.PrintWidth - 1, job.PrintHeight - 1), False)

            ' This code below draws 5 concentric circles, starting with a large one
            ' covering nearly the whole page, then printing successively smaller ones
            ' on top of it (with alternating colors) to form a bullseye.
            Dim r As Single = job.PrintWidth / 10
            Dim center As PointF = _
                New System.Drawing.PointF(job.DisplayX + job.PrintWidth / 2, job.DisplayY + job.PrintHeight / 2)  'centers the circles on the page
            job.Draw.FillStyle = Accusoft.PrintProSdk.FillStyle.Solid
            job.Draw.BackStyle = Accusoft.PrintProSdk.BackStyle.Transparent
            For i As Integer = 5 To 1 Step -1

                If i Mod 2 <> 0 Then 'odd circles black

                    job.Draw.FillColor = System.Drawing.Color.Black

                Else 'even circles white

                    job.Draw.FillColor = System.Drawing.Color.White

                End If

                job.Draw.Circle(center, r * i, System.Drawing.Color.White, 1)

            Next

            ' Print a message describing the shape at the upper-left corner of the page.
            Dim font As Font = New System.Drawing.Font("Times New Roman", 14, System.Drawing.FontStyle.Bold)
            job.Draw.Text(New System.Drawing.PointF(job.DisplayX, job.DisplayY), "PrintPRO Bullseye Example", font)

            ' End the current page and print.
            job.Finish()

        Catch ex As Exception
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub btnPrintGrid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintGrid.Click

        Try

            Dim printer As Accusoft.PrintProSdk.Printer
            Dim job As Accusoft.PrintProSdk.PrintJob

            ' Prompt the user for which printer to use and then create a PrintJob for that printer.
            printer = Accusoft.PrintProSdk.Printer.SelectPrinter(pp, True)
            printer.RequestedDpi = 96
            job = New Accusoft.PrintProSdk.PrintJob(printer)

            ' Make a RectangleF struct for the whole print area.
            Dim printBounds As System.Drawing.RectangleF = _
                New System.Drawing.RectangleF(job.DisplayX, job.DisplayY, job.PrintWidth - 1, job.PrintHeight - 1)

            ' And draw a border around it.
            job.Draw.Rectangle(printBounds, False)

            ' Draw the vertical lines.
            Dim x As Single = job.PrintWidth / 50
            For i As Integer = 1 To 49

                job.Draw.Line( _
                    New System.Drawing.PointF(job.DisplayX + x * i, job.DisplayY), _
                    New System.Drawing.PointF(job.DisplayX + x * i, job.DisplayY + job.PrintHeight), _
                    False, False)

            Next

            ' Draw the horizontal lines.
            Dim y As Single = job.PrintHeight / 50
            For i As Integer = 1 To 49

                job.Draw.Line( _
                    New System.Drawing.PointF(job.DisplayX, job.DisplayY + y * i), _
                    New System.Drawing.PointF(job.DisplayX + job.PrintWidth, job.DisplayY + y * i), _
                    False, False)

            Next

            ' Print a message describing the shape.
            Dim font As System.Drawing.Font = New System.Drawing.Font("Times New Roman", 72, System.Drawing.FontStyle.Bold)
            job.Draw.TextAligned(printBounds, "PrintPRO 50x50 grid example", font, _
                Accusoft.PrintProSdk.Alignment.CenterJustifyMiddle, True)

            ' End the current page and print.
            job.Finish()

        Catch ex As Exception
            PegasusError(ex, lblerror)
        End Try

    End Sub
End Class

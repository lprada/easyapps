
// PrintPRO COM VC++ Event Handlers.
// Copyright (c) 2000-2003 Pegasus Imaging Corp

// PrintPRO doesn't have any events and therefore it does not need an event handler.
// This event handler class is provided for 2 reasons:
// 1. To make the PrintPRO COM interface identical to all of the other Pegasus Products
//    This means that you would go through the exact same steps to add a PrintPRO
//    COM object as you would for ImagXpress, NotateXpress, TwainPRO, etc.
// 2. For upward compatibility.
//    In the case that events are added to PrintPRO in the future, events and event handlers 
//    could be added to this class without any changes to the interface portion of your application.


class CPrintPROEventHandler : public IDispatch
{
	public:

	long m_dwRef;
	DWORD m_dwCookie;


	CPrintPROEventHandler()
	{
	  m_dwRef = 0;
	}
	
	~CPrintPROEventHandler()
	{
	}


	long __stdcall GetTypeInfoCount(unsigned int *)
	{
		return 0;

	}

	long __stdcall GetTypeInfo(unsigned int,unsigned long,struct ITypeInfo ** )
	{
		return 0;
	}

	long __stdcall GetIDsOfNames(const struct _GUID &,BSTR *,unsigned int,unsigned long,long *)
	{
		return 0;
	}
	
	long __stdcall GetIID()
	{
		return 0;
	}

	HRESULT __stdcall Invoke (DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult, EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr)
	{
		 return S_OK;
	}


	HRESULT __stdcall QueryInterface(REFIID a_riid, void** a_ppv)
	{
		return S_OK;
	}

	ULONG __stdcall AddRef()
	{
		return InterlockedIncrement(&m_dwRef);
	}

	ULONG __stdcall Release()
	{
		if (InterlockedDecrement(&m_dwRef) == 0)
		{
			return 0;
		}
		return m_dwRef;
	}

 };

 class CPrintPRO : CPrintPROEventHandler
{
  private:
    DWORD m_dwCookie;

    // Event Handlers

  public:
   DWORD        m_objID;
   DWORD        m_classPtr;
   IPrintPROPtr pPrintPRO;

   CPrintPRO()
   {
     CPrintPRO(0, 0);
   }

   CPrintPRO(DWORD classPtr, DWORD objID)
   {
     // Initialize our instance pointer and object id.
     m_classPtr = classPtr;
     m_objID    = objID;
     // Create an instance of the PrintPRO COM object
#ifdef __BORLANDC__
     pPrintPRO.CreateInstance(CLSID_PrintPRO, NULL, CLSCTX_INPROC_SERVER);
#else
     pPrintPRO.CreateInstance(__uuidof(PrintPRO), NULL, CLSCTX_INPROC_SERVER);
#endif
   }

   ~CPrintPRO()
   {
   }

};


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using PegasusImaging.WinForms.PrintPro3;
using PegasusImaging.WinForms.ImagXpress8;

namespace PrintPRODemoCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonPrintDemoPage;
		private System.Windows.Forms.GroupBox groupBoxPrintPreview;
		private System.Windows.Forms.Button buttonPrintPreview;
		private System.Windows.Forms.Button buttonScale12;
		private System.Windows.Forms.Button buttonScale11;
		private System.Windows.Forms.Button buttonScale13;
		private System.Windows.Forms.Button buttonScale14;
		private System.Windows.Forms.Button buttonScale15;
		
		private System.Windows.Forms.GroupBox groupBox1;
		

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.CheckBox checkBox1;
		public bool preview;
		private PegasusImaging.WinForms.PrintPro3.PICPrintPro picPrintPro1;
		private System.Windows.Forms.ListBox lstInfo;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_OpenBitmap;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;

		private System.String strCurrentDir = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		
	
		public bool bPrintToTiff ;
		public FormMain()
		{
			UnlockPICPrintPro.PS_Unlock (12345, 12345, 12345, 12345);

			
			// call the unlock function with dummy values
			// PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345, 12345, 12345, 12345);

			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				// Don't forget to dispose IX
				//

				if (imageXView1.Image != null)
				{
					imageXView1.Image.Dispose();
					imageXView1.Image = null;
				}
			
				
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}               

				if (components != null) 
				{
					components.Dispose();
				}
			picPrintPro1.Dispose ();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonPrintDemoPage = new System.Windows.Forms.Button();
			this.groupBoxPrintPreview = new System.Windows.Forms.GroupBox();
			this.buttonScale15 = new System.Windows.Forms.Button();
			this.buttonScale14 = new System.Windows.Forms.Button();
			this.buttonScale13 = new System.Windows.Forms.Button();
			this.buttonScale11 = new System.Windows.Forms.Button();
			this.buttonScale12 = new System.Windows.Forms.Button();
			this.buttonPrintPreview = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.picPrintPro1 = new PegasusImaging.WinForms.PrintPro3.PICPrintPro();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_OpenBitmap = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.groupBoxPrintPreview.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonPrintDemoPage
			// 
			this.buttonPrintDemoPage.Location = new System.Drawing.Point(56, 320);
			this.buttonPrintDemoPage.Name = "buttonPrintDemoPage";
			this.buttonPrintDemoPage.Size = new System.Drawing.Size(144, 32);
			this.buttonPrintDemoPage.TabIndex = 2;
			this.buttonPrintDemoPage.Text = "PrintToFile";
			this.buttonPrintDemoPage.Click += new System.EventHandler(this.buttonPrintDemoPage_Click);
			// 
			// groupBoxPrintPreview
			// 
			this.groupBoxPrintPreview.Controls.AddRange(new System.Windows.Forms.Control[] {
																							   this.buttonScale15,
																							   this.buttonScale14,
																							   this.buttonScale13,
																							   this.buttonScale11,
																							   this.buttonScale12,
																							   this.buttonPrintPreview});
			this.groupBoxPrintPreview.Location = new System.Drawing.Point(16, 360);
			this.groupBoxPrintPreview.Name = "groupBoxPrintPreview";
			this.groupBoxPrintPreview.Size = new System.Drawing.Size(232, 248);
			this.groupBoxPrintPreview.TabIndex = 3;
			this.groupBoxPrintPreview.TabStop = false;
			this.groupBoxPrintPreview.Text = "Print Preview";
			// 
			// buttonScale15
			// 
			this.buttonScale15.Location = new System.Drawing.Point(68, 214);
			this.buttonScale15.Name = "buttonScale15";
			this.buttonScale15.Size = new System.Drawing.Size(104, 24);
			this.buttonScale15.TabIndex = 5;
			this.buttonScale15.Text = "Scale 1:5";
			this.buttonScale15.Click += new System.EventHandler(this.buttonScale15_Click);
			// 
			// buttonScale14
			// 
			this.buttonScale14.Location = new System.Drawing.Point(68, 176);
			this.buttonScale14.Name = "buttonScale14";
			this.buttonScale14.Size = new System.Drawing.Size(104, 24);
			this.buttonScale14.TabIndex = 4;
			this.buttonScale14.Text = "Scale 1:4";
			this.buttonScale14.Click += new System.EventHandler(this.buttonScale14_Click);
			// 
			// buttonScale13
			// 
			this.buttonScale13.Location = new System.Drawing.Point(68, 138);
			this.buttonScale13.Name = "buttonScale13";
			this.buttonScale13.Size = new System.Drawing.Size(104, 24);
			this.buttonScale13.TabIndex = 3;
			this.buttonScale13.Text = "Scale 1:3";
			this.buttonScale13.Click += new System.EventHandler(this.buttonScale13_Click);
			// 
			// buttonScale11
			// 
			this.buttonScale11.Location = new System.Drawing.Point(68, 62);
			this.buttonScale11.Name = "buttonScale11";
			this.buttonScale11.Size = new System.Drawing.Size(104, 24);
			this.buttonScale11.TabIndex = 2;
			this.buttonScale11.Text = "Scale 1:1";
			this.buttonScale11.Click += new System.EventHandler(this.buttonScale11_Click);
			// 
			// buttonScale12
			// 
			this.buttonScale12.Location = new System.Drawing.Point(68, 100);
			this.buttonScale12.Name = "buttonScale12";
			this.buttonScale12.Size = new System.Drawing.Size(104, 24);
			this.buttonScale12.TabIndex = 1;
			this.buttonScale12.Text = "Scale 1:2";
			this.buttonScale12.Click += new System.EventHandler(this.buttonScale12_Click);
			// 
			// buttonPrintPreview
			// 
			this.buttonPrintPreview.Location = new System.Drawing.Point(44, 24);
			this.buttonPrintPreview.Name = "buttonPrintPreview";
			this.buttonPrintPreview.Size = new System.Drawing.Size(152, 24);
			this.buttonPrintPreview.TabIndex = 0;
			this.buttonPrintPreview.Text = "Print Preview";
			this.buttonPrintPreview.Click += new System.EventHandler(this.buttonPrintPreview_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.radioButton2,
																					this.radioButton1});
			this.groupBox1.Location = new System.Drawing.Point(40, 216);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(200, 80);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "PrintToFile";
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(16, 48);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(160, 24);
			this.radioButton2.TabIndex = 1;
			this.radioButton2.Text = "Disable PrintToFile";
			this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
			// 
			// radioButton1
			// 
			this.radioButton1.Location = new System.Drawing.Point(16, 16);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(168, 24);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.Text = "Enable PrintToFile";
			this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(48, 184);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(184, 16);
			this.checkBox1.TabIndex = 8;
			this.checkBox1.Text = "Choose PrintToFile or Preview";
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// picPrintPro1
			// 
			this.picPrintPro1.AddCR = true;
			this.picPrintPro1.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop;
			this.picPrintPro1.AutoMargin = true;
			this.picPrintPro1.BackColor = System.Drawing.Color.White;
			this.picPrintPro1.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent;
			this.picPrintPro1.BMargin = 249.6F;
			this.picPrintPro1.CurrentX = 249.6F;
			this.picPrintPro1.CurrentY = 249.6F;
			this.picPrintPro1.DocName = "PrintPRO Document";
			this.picPrintPro1.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen;
			this.picPrintPro1.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid;
			this.picPrintPro1.DrawWidth = 1;
			this.picPrintPro1.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic;
			this.picPrintPro1.FillColor = System.Drawing.Color.Black;
			this.picPrintPro1.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent;
			this.picPrintPro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picPrintPro1.ForeColor = System.Drawing.Color.Black;
			this.picPrintPro1.Lmargin = 249.6F;
			this.picPrintPro1.OutputFileName = "";
			this.picPrintPro1.OwnDIB = false;
			this.picPrintPro1.Picture = null;
			this.picPrintPro1.PictureTransparent = false;
			this.picPrintPro1.PictureTransparentColor = System.Drawing.Color.White;
			this.picPrintPro1.PrinterDriverName = "winspool";
			this.picPrintPro1.PrinterName = "Dell Laser Printer 1700n PS3";
			this.picPrintPro1.PrinterPortName = "Ne05:";
			this.picPrintPro1.PrintPreviewOnly = false;
			this.picPrintPro1.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
			this.picPrintPro1.PrintToFile = false;
			this.picPrintPro1.RequestedDPI = 200;
			this.picPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4;
			this.picPrintPro1.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip;
			this.picPrintPro1.SN = "PEXHJ700AA-PEG0Q000195";
			this.picPrintPro1.TMargin = 249.6F;
			this.picPrintPro1.UseDefaultPrinter = true;
			this.picPrintPro1.WordWrap = true;
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Using PrintPRO to print text, graphics and bitmaps.",
														 "2)Calling the PrintToFile method which enables printing a TIFF image directly to " +
														 "file instead of the printer.",
														 "",
														 "Instructions:",
														 "1)Choose either the PrintToFile option or PrintPreview option.",
														 "2)If the PrintToFile option is chosen there are 2 options:",
														 "   a)Enable PrintToFile which sends the printed data to a file.",
														 "   b)Disable PrintToFile which sends the printed data to the printer."});
			this.lstInfo.Location = new System.Drawing.Point(16, 16);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(688, 121);
			this.lstInfo.TabIndex = 11;
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.Location = new System.Drawing.Point(288, 176);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(392, 409);
			this.imageXView1.TabIndex = 12;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_OpenBitmap,
																					 this.menuItem3,
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_OpenBitmap
			// 
			this.mnu_OpenBitmap.Index = 0;
			this.mnu_OpenBitmap.Text = "Open Bitmap to print...";
			this.mnu_OpenBitmap.Click += new System.EventHandler(this.mnu_OpenBitmap_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "-";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 2;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(714, 617);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.imageXView1,
																		  this.lstInfo,
																		  this.checkBox1,
																		  this.groupBox1,
																		  this.groupBoxPrintPreview,
																		  this.buttonPrintDemoPage});
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "FormMain";
			this.Text = "PrintPRO v3.0 Demo";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.groupBoxPrintPreview.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		

		private void buttonPrintDemoPage_Click(object sender, System.EventArgs e)
		{
			if (picPrintPro1.PrintError == 0)
			{
				//    'Note: all of the dimensions in this demo are in twips.
				
				picPrintPro1.PrintPreviewOnly = false;
				this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

				if (bPrintToTiff == true)
				{	
					imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
					picPrintPro1.DocName = "Pegasus's PrintPRO Demo";
					picPrintPro1.OutputFileName = System.IO.Path.Combine(Application.StartupPath , "PrintToFile.tif");
					picPrintPro1.PrintToFile = true;
					picPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF;
					picPrintPro1.RequestedDPI = 100;
				}
				else
				{	
					picPrintPro1.DocName = "Pegasus's PrintPRO Demo";
					picPrintPro1.OutputFileName = "";
					picPrintPro1.PrintToFile = false;
					picPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF;
				}
				picPrintPro1.DocName = "Pegasus PrintPRO Demo";
				
				// Start a print job by selecting a printer and starting a
				// new page. If you don't select a printer or start a new
				// page, PrintPRO will select the default printer and start
				// a new page when you first print text, graphics or a bitmap.
				picPrintPro1.PrintDialog ();
				picPrintPro1.StartPrintDoc ();
				picPrintPro1.PrintPreviewOnly = false;
				
				// Print all of the information on the page
				PrintPage ();
				// End the print job.  This will end the current page and print the document
				picPrintPro1.EndPrintDoc ();
				
				if (bPrintToTiff == true) 
				{
					try
					{
						
						imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(System.IO.Path.Combine(Application.StartupPath , "PrintToFile.tif"));
					}
					catch(PegasusImaging.WinForms.ImagXpress8.ImageXException ex)
					{

						MessageBox.Show(ex.Result.ToString());

					}
					
					
					}

				this.Cursor = System.Windows.Forms.Cursors.Default;
			}
		}

		private void PrintPage ()
		{
			// Note: all of the dimensions in this demo are in twips.
  
			// Print a banner at the top of the page, white on black.
			picPrintPro1.BackStyle = peBackStyle.BACK_Opaque;

			// TODO: Fix these lines...
			//picPrintPro1.CtlForeColor = System.Drawing.Color.FromArgb (255, 255, 255); // White text
			//picPrintPro1.CtlBackColor = System.Drawing.Color.FromArgb (0, 0, 0); // Black background

			System.Drawing.Font font = new System.Drawing.Font ("Arial", 24, System.Drawing.FontStyle.Bold);
			picPrintPro1.Font = font;
			picPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle;
			float prWidth = picPrintPro1.PWidth;
			picPrintPro1.PrintTextAligned ("  Hello! Check out PrintPRO 3.0  ", 1000, picPrintPro1.TMargin, prWidth - 1000, 1100);
    
			// The following code demonstrates how PrintPRO can print text
			// aligned inside a rectangular area.  First, a box is drawn
			// then text is drawn aligned inside the box.
			picPrintPro1.DrawWidth = 1;
			picPrintPro1.BackStyle = peBackStyle.BACK_Transparent;
			//picPrintPro1.CtlForeColor = System.Drawing.Color.FromArgb (0, 0, 0);	//Black text
			System.Drawing.Font fontAgain = new System.Drawing.Font ("Arial", 8, System.Drawing.FontStyle.Bold);
			picPrintPro1.Font = fontAgain;

			picPrintPro1.Alignment = peAlignment.ALIGN_LeftJustifyTop;
			picPrintPro1.DrawLine(  1440, 1440, 2880, 2880,System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - left justified top", 1440, 1440, 2880, 2880);
    
		    picPrintPro1.Alignment = peAlignment.ALIGN_LeftJustifyMiddle;
			picPrintPro1.DrawLine( 3100, 1440, 4540, 2880, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - left justified middle", 3100, 1440, 4540, 2880);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_LeftJustifyBottom;
			picPrintPro1.DrawLine (4760, 1440, 6200, 2880, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - left justified bottom", 4760, 1440, 6200, 2880);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_RightJustifyTop;
			picPrintPro1.DrawLine (6420, 1440, 7860, 2880, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - right justified top", 6420, 1440, 7860, 2880);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_RightJustifyMiddle;
			picPrintPro1.DrawLine (1440, 3100, 2880, 4540, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - right justified middle", 1440, 3100, 2880, 4540);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_RightJustifyBottom;
			picPrintPro1.DrawLine (3100, 3100, 4540, 4540, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - right justified bottom", 3100, 3100, 4540, 4540);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyTop;
			picPrintPro1.DrawLine (4760, 3100, 6200, 4540, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - center justified top", 4760, 3100, 6200, 4540);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle;
			picPrintPro1.DrawLine (6420, 3100, 7860, 4540, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - center justified middle", 6420, 3100, 7860, 4540);
    
			picPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyBottom;
			picPrintPro1.DrawLine (1440, 4760, 7860, 5200, System.Drawing.Color.Black, true, false);
			picPrintPro1.PrintTextAligned ("This is an experiment to see if the printer supports formatted text - center justified bottom", 1440, 4760, 7860, 5200);
    
			// PrintPRo can print images loaded into the control using the
			// Picture property.  An hDC property is also available so
			// that other controls (like FXTools and PhotoPRO) can print
			// to the current document.
			picPrintPro1.PrintPicture (1640, 5700, 5000, 4000, 0,0, picPrintPro1.Picture.Width, picPrintPro1.Picture.Height, false);
			
			// Use graphic methods to draw an arrow
			picPrintPro1.FillStyle = peFillStyle.FILL_Solid;
			picPrintPro1.DrawWidth = 15;
			picPrintPro1.DrawPolygon (8390, 7575, 6770, 7575, 6870, 7475, 6870, 7675, 6770, 7575, 0, 0,System.Drawing.Color.Black,5);
			
			// Draw text inside a gray box with a black border
			picPrintPro1.FillStyle = peFillStyle.FILL_Transparent;
			picPrintPro1.FillColor = System.Drawing.Color.Gray;
			picPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle;

			// Draw the gray box
			picPrintPro1.DrawLine (8550, 5700, 10550, 9450,System.Drawing.Color.Gray, true, true);
			
			// Put a black border on the box
			picPrintPro1.DrawLine (8550, 5700, 10550, 9450,System.Drawing.Color.Black, true, false);
			
			// Print text inside the box
			picPrintPro1.Font = new System.Drawing.Font ("Times", 18);
			picPrintPro1.BackStyle = peBackStyle.BACK_Transparent;
			picPrintPro1.PrintTextAligned ("Print\rGraphics\rand\rBitmaps\rand\rtext", 8550, 5500, 10550, 9250);
    
			// Print the features
			picPrintPro1.Font = new System.Drawing.Font ("Arial", 24, System.Drawing.FontStyle.Underline | System.Drawing.FontStyle.Bold);
			picPrintPro1.Lmargin= 1440;
			picPrintPro1.CurrentX = 1440;
			picPrintPro1.CurrentY = 10000;
			picPrintPro1.PrintText ("Plus!");
			picPrintPro1.Font = new System.Drawing.Font ("Arial", 24, System.Drawing.FontStyle.Bold);
			picPrintPro1.PrintText ("Lines, rectangles, circles, ellipse, arc,");
			picPrintPro1.PrintText ("curves, polygons, pie, rounded rectangles,");
			picPrintPro1.PrintText ("Print Dialog, and much, much more!");
			picPrintPro1.PrintText ("PrintPRO is an ATL control so you can use");
			picPrintPro1.PrintText ("it with your favorite dev. tool including IE.\r");
			picPrintPro1.PrintText ("The PrintPRO Team\r");
			picPrintPro1.Font = new System.Drawing.Font ("Arial", 8);
			picPrintPro1.CurrentY = picPrintPro1.ScaleHeight - picPrintPro1.TextHeight("This page");
			picPrintPro1.PrintText ("This page was created in Microsoft Visual C# .NET using the Pegasus Software PrintPRO .NET control.");
		}

		private void buttonPrintPreview_Click(object sender, System.EventArgs e)
		{
			
			if (picPrintPro1.PrintError == 0)
			{
				
				imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
				
				this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
				picPrintPro1.PrintDialog ();
				picPrintPro1.PrintPreviewOnly = true;
				picPrintPro1.StartPrintPreview ();
		
				if (picPrintPro1.PrintError != 0)
					MessageBox.Show ("PrintPRO Error: " + picPrintPro1.PrintError, "PrintPRO Demo", System.Windows.Forms.MessageBoxButtons.OKCancel);
				
				PrintPage ();

				// End the print preview
				picPrintPro1.EndPrintPreview ();
				if (picPrintPro1.PrintError != 0)
					MessageBox.Show ("PrintPRO Error: " + picPrintPro1.PrintError, "PrintPRO Demo", System.Windows.Forms.MessageBoxButtons.OKCancel);
				
				picPrintPro1.GetPrintPreview (1);
				if (picPrintPro1.PrintError != 0)
					MessageBox.Show ("PrintPRO Error: " + picPrintPro1.PrintError, "PrintPRO Demo", System.Windows.Forms.MessageBoxButtons.OKCancel);
				
				 // get the image from the PrintPRO control, and assign in to the image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)(picPrintPro1.PrintPreviewDIB));

				
				this.Cursor = System.Windows.Forms.Cursors.Default;
			}
		}

		private void buttonScale11_Click(object sender, System.EventArgs e)
		{
			picPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_1;
			picPrintPro1.GetPrintPreview (1);
			if (picPrintPro1.PrintError == 0) 
			{
				// get the image from the PrintPRO control, and assign in to the image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)(picPrintPro1.PrintPreviewDIB));
			}
		}

		private void buttonScale12_Click(object sender, System.EventArgs e)
		{
			picPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_2;
			picPrintPro1.GetPrintPreview (1);
			if (picPrintPro1.PrintError == 0) 
			{

				// get the image from the PrintPRO control, and assign in to the image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)(picPrintPro1.PrintPreviewDIB));
			}
		}

		private void buttonScale13_Click(object sender, System.EventArgs e)
		{
			picPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_3;
			picPrintPro1.GetPrintPreview (1);
			if (picPrintPro1.PrintError == 0) 
			{
				// get the image from the PrintPRO control, and assign in to the image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)(picPrintPro1.PrintPreviewDIB));
			}
		}

		private void buttonScale14_Click(object sender, System.EventArgs e)
		{
			picPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_4;
			picPrintPro1.GetPrintPreview (1);
			if (picPrintPro1.PrintError == 0) 
			{
				// get the image from the PrintPRO control, and assign in to the image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)(picPrintPro1.PrintPreviewDIB));
			}
		}

		private void buttonScale15_Click(object sender, System.EventArgs e)
		{
			picPrintPro1.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PSCALE_1_5;
			picPrintPro1.GetPrintPreview (1);
			if (picPrintPro1.PrintError == 0) 
			{
				// get the image from the PrintPRO control, and assign in to the image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)(picPrintPro1.PrintPreviewDIB));
				
			}
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{	

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);


			bPrintToTiff = false;
			checkBox1.Checked = true;
			imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.CropImage;

			imageXView1.AutoScroll = true;

		
			this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
			this.Left = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;
			
			string strCurrentDir = System.IO.Directory.GetCurrentDirectory ();
			string strImagePath = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\Pic1.bmp");
			picPrintPro1.Picture = System.Drawing.Image.FromFile (strImagePath);
		}

		private void radioButton1_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioButton1.Checked == true)
			{
				bPrintToTiff = true;
			}
			else
				bPrintToTiff = false;
		}

		private void radioButton2_CheckedChanged(object sender, System.EventArgs e)
		{
			if (radioButton2.Checked == true)
			{
				bPrintToTiff = false;
			}
			else
				bPrintToTiff = true;

		}

		private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
		{
			if(checkBox1.Checked == true)
			{
				radioButton1.Checked = true;
				groupBox1.Enabled = true;
				groupBoxPrintPreview.Enabled = false;
			}
			else
			{
				groupBox1.Enabled = false;
				groupBoxPrintPreview.Enabled = true;

			}


		}

		private void button1_Click(object sender, System.EventArgs e)
		{
				System.Environment.Exit (0);
		}

		private void mnu_OpenBitmap_Click(object sender, System.EventArgs e)
		{
		
			openFileDialog1.Title = "Open BMP File";
			openFileDialog1.Filter = "Windows Bitmap (*.BMP) | *.bmp";
			openFileDialog1.DefaultExt = ".bmp";
			openFileDialog1.Multiselect = false;
			openFileDialog1.InitialDirectory = strCurrentDir;
			openFileDialog1.ShowDialog(this);

			string strFilePath  = openFileDialog1.FileName;

			picPrintPro1.Picture = System.Drawing.Image.FromFile(strFilePath);

		}

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
			 Application.Exit();
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			 picPrintPro1.AboutBox();
		}

		

		
		
	}
}

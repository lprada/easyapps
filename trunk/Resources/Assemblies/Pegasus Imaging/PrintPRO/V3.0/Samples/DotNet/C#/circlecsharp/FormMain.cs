//****************************************************************
//* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *
//* This sample code is provided to Pegasus licensees "as is"    *
//* with no restrictions on use or modification. No warranty for *
//* use of this sample code is provided by Pegasus.              *
//****************************************************************

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.PrintPro3;

namespace CircleCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonPrint;
		private PegasusImaging.WinForms.PrintPro3.PICPrintPro picPrintPro1;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_About;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			UnlockPICPrintPro.PS_Unlock(12345, 12345, 12345, 12345);

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			picPrintPro1.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonPrint = new System.Windows.Forms.Button();
			this.picPrintPro1 = new PegasusImaging.WinForms.PrintPro3.PICPrintPro();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// buttonPrint
			// 
			this.buttonPrint.Location = new System.Drawing.Point(136, 200);
			this.buttonPrint.Name = "buttonPrint";
			this.buttonPrint.Size = new System.Drawing.Size(88, 23);
			this.buttonPrint.TabIndex = 2;
			this.buttonPrint.Text = "Print";
			this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
			// 
			// picPrintPro1
			// 
			this.picPrintPro1.AddCR = true;
			this.picPrintPro1.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop;
			this.picPrintPro1.AutoMargin = true;
			this.picPrintPro1.BackColor = System.Drawing.Color.White;
			this.picPrintPro1.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent;
			this.picPrintPro1.BMargin = 249.6F;
			this.picPrintPro1.CurrentX = 249.6F;
			this.picPrintPro1.CurrentY = 249.6F;
			this.picPrintPro1.DocName = "PrintPRO Document";
			this.picPrintPro1.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen;
			this.picPrintPro1.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid;
			this.picPrintPro1.DrawWidth = 1;
			this.picPrintPro1.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic;
			this.picPrintPro1.FillColor = System.Drawing.Color.Black;
			this.picPrintPro1.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent;
			this.picPrintPro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picPrintPro1.ForeColor = System.Drawing.Color.Black;
			this.picPrintPro1.Lmargin = 249.6F;
			this.picPrintPro1.OutputFileName = "";
			this.picPrintPro1.OwnDIB = false;
			this.picPrintPro1.Picture = null;
			this.picPrintPro1.PictureTransparent = false;
			this.picPrintPro1.PictureTransparentColor = System.Drawing.Color.Black;
			this.picPrintPro1.PrinterDriverName = "winspool";
			this.picPrintPro1.PrinterName = "Dell Laser Printer 1700n PS3";
			this.picPrintPro1.PrinterPortName = "Ne05:";
			this.picPrintPro1.PrintPreviewOnly = false;
			this.picPrintPro1.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
			this.picPrintPro1.PrintToFile = false;
			this.picPrintPro1.RequestedDPI = 200;
			this.picPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4;
			this.picPrintPro1.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip;
			this.picPrintPro1.SN = "PEXHJ700AA-PEG0Q000195";
			this.picPrintPro1.TMargin = 249.6F;
			this.picPrintPro1.UseDefaultPrinter = true;
			this.picPrintPro1.WordWrap = true;
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Printing concentric circles inside the display area ",
														 "   of the printer."});
			this.lstInfo.Location = new System.Drawing.Point(16, 24);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(312, 95);
			this.lstInfo.TabIndex = 3;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(346, 261);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstInfo,
																		  this.buttonPrint});
			this.Menu = this.mainMenu1;
			this.Name = "FormMain";
			this.Text = "Print PRO Circle Example";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void buttonPrint_Click(object sender, System.EventArgs e)
		{
			float fDelta;

			// select a printer
			picPrintPro1.PrintDialog ();
			
			// Draw a border around the display area
			picPrintPro1.DrawLine (picPrintPro1.DisplayX, picPrintPro1.DisplayY, picPrintPro1.ScaleWidth + picPrintPro1.DisplayX, picPrintPro1.ScaleHeight + picPrintPro1.DisplayY, System.Drawing.Color.Black, true, false);

			// Draw the circles
			fDelta = picPrintPro1.ScaleWidth / 10;
			picPrintPro1.FillStyle = peFillStyle.FILL_Solid;
			picPrintPro1.BackStyle = peBackStyle.BACK_Transparent;

			for (int i=5; i>=1; i--)
			{
				if (i % 2 > 0)
				{
					picPrintPro1.FillColor = System.Drawing.Color.FromArgb (0, 0, 0);
					picPrintPro1.DrawCircle (picPrintPro1.DisplayX + picPrintPro1.ScaleWidth / 2, picPrintPro1.DisplayY + picPrintPro1.ScaleHeight / 2, fDelta * i, System.Drawing.Color.FromArgb (255, 255, 255),1);
					
				}
				else
				{
					picPrintPro1.FillColor = System.Drawing.Color.FromArgb (255, 255, 255);
					picPrintPro1.DrawCircle (picPrintPro1.DisplayX + picPrintPro1.ScaleWidth / 2, picPrintPro1.DisplayY + picPrintPro1.ScaleHeight / 2, fDelta * i, System.Drawing.Color.FromArgb (255, 255, 255), 1);
					
				}
			}

			// Print a message in the center of the page
			picPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle;

			System.Drawing.Font font = new System.Drawing.Font ("Times New Roman", 14, System.Drawing.FontStyle.Bold);
			picPrintPro1.Font = font;
			picPrintPro1.CurrentX = picPrintPro1.DisplayX;
			picPrintPro1.CurrentY = picPrintPro1.DisplayY;
			picPrintPro1.PrintText ("PrintPRO Bullseye Example");
			
			// Print the document
			picPrintPro1.EndPrintDoc ();
		}

		

		private void FormMain_Load(object sender, System.EventArgs e)
		{
		
		}

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
		
			System.Environment.Exit(0);
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			picPrintPro1.AboutBox();
		}

		
		
	}
}

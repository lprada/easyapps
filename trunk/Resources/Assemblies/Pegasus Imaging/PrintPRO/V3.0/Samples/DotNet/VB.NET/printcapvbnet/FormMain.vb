Public Class FormMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            PicPrintPro1.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents labelDuplex As System.Windows.Forms.Label
    Friend WithEvents labelNumCopies As System.Windows.Forms.Label
    Friend WithEvents label6 As System.Windows.Forms.Label
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents button As System.Windows.Forms.Button
    Friend WithEvents listBoxPaperID As System.Windows.Forms.ListBox
    Friend WithEvents listBoxPaper As System.Windows.Forms.ListBox
    Friend WithEvents listBoxBinID As System.Windows.Forms.ListBox
    Friend WithEvents listBoxBins As System.Windows.Forms.ListBox
    Friend WithEvents PicPrintPro1 As PegasusImaging.WinForms.PrintPro3.PICPrintPro
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnu_File As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Quit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_About As System.Windows.Forms.MenuItem
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox




    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.labelDuplex = New System.Windows.Forms.Label()
        Me.labelNumCopies = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.button = New System.Windows.Forms.Button()
        Me.listBoxPaperID = New System.Windows.Forms.ListBox()
        Me.listBoxPaper = New System.Windows.Forms.ListBox()
        Me.listBoxBinID = New System.Windows.Forms.ListBox()
        Me.listBoxBins = New System.Windows.Forms.ListBox()
        Me.PicPrintPro1 = New PegasusImaging.WinForms.PrintPro3.PICPrintPro()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnu_File = New System.Windows.Forms.MenuItem()
        Me.mnu_Quit = New System.Windows.Forms.MenuItem()
        Me.mnu_About = New System.Windows.Forms.MenuItem()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'labelDuplex
        '
        Me.labelDuplex.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelDuplex.Location = New System.Drawing.Point(112, 592)
        Me.labelDuplex.Name = "labelDuplex"
        Me.labelDuplex.Size = New System.Drawing.Size(72, 16)
        Me.labelDuplex.TabIndex = 26
        '
        'labelNumCopies
        '
        Me.labelNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelNumCopies.Location = New System.Drawing.Point(16, 592)
        Me.labelNumCopies.Name = "labelNumCopies"
        Me.labelNumCopies.Size = New System.Drawing.Size(72, 16)
        Me.labelNumCopies.TabIndex = 25
        '
        'label6
        '
        Me.label6.Location = New System.Drawing.Point(112, 560)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(80, 16)
        Me.label6.TabIndex = 24
        Me.label6.Text = "Duplex?"
        '
        'label5
        '
        Me.label5.Location = New System.Drawing.Point(16, 560)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(72, 16)
        Me.label5.TabIndex = 23
        Me.label5.Text = "Num Copies:"
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(616, 136)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(96, 16)
        Me.label4.TabIndex = 22
        Me.label4.Text = "PaperID:"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(336, 136)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(152, 16)
        Me.label3.TabIndex = 21
        Me.label3.Text = "Supported Paper:"
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(48, 344)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(120, 16)
        Me.label2.TabIndex = 20
        Me.label2.Text = "Bin ID:"
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(40, 136)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(120, 16)
        Me.label1.TabIndex = 19
        Me.label1.Text = "Supported Bins:"
        '
        'button
        '
        Me.button.Location = New System.Drawing.Point(296, 88)
        Me.button.Name = "button"
        Me.button.Size = New System.Drawing.Size(184, 24)
        Me.button.TabIndex = 18
        Me.button.Text = "Get Printer Capabilties"
        '
        'listBoxPaperID
        '
        Me.listBoxPaperID.Location = New System.Drawing.Point(576, 168)
        Me.listBoxPaperID.Name = "listBoxPaperID"
        Me.listBoxPaperID.Size = New System.Drawing.Size(184, 381)
        Me.listBoxPaperID.TabIndex = 17
        '
        'listBoxPaper
        '
        Me.listBoxPaper.Location = New System.Drawing.Point(304, 168)
        Me.listBoxPaper.Name = "listBoxPaper"
        Me.listBoxPaper.Size = New System.Drawing.Size(184, 381)
        Me.listBoxPaper.TabIndex = 16
        '
        'listBoxBinID
        '
        Me.listBoxBinID.Location = New System.Drawing.Point(32, 368)
        Me.listBoxBinID.Name = "listBoxBinID"
        Me.listBoxBinID.Size = New System.Drawing.Size(184, 173)
        Me.listBoxBinID.TabIndex = 15
        '
        'listBoxBins
        '
        Me.listBoxBins.Location = New System.Drawing.Point(32, 168)
        Me.listBoxBins.Name = "listBoxBins"
        Me.listBoxBins.Size = New System.Drawing.Size(184, 160)
        Me.listBoxBins.TabIndex = 14
        '
        'PicPrintPro1
        '
        Me.PicPrintPro1.AddCR = True
        Me.PicPrintPro1.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop
        Me.PicPrintPro1.AutoMargin = True
        Me.PicPrintPro1.BackColor = System.Drawing.Color.White
        Me.PicPrintPro1.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent
        Me.PicPrintPro1.BMargin = 249.6!
        Me.PicPrintPro1.CurrentX = 249.6!
        Me.PicPrintPro1.CurrentY = 249.6!
        Me.PicPrintPro1.DocName = "PrintPRO Document"
        Me.PicPrintPro1.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen
        Me.PicPrintPro1.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid
        Me.PicPrintPro1.DrawWidth = 1
        Me.PicPrintPro1.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic
        Me.PicPrintPro1.FillColor = System.Drawing.Color.Black
        Me.PicPrintPro1.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent
        Me.PicPrintPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicPrintPro1.ForeColor = System.Drawing.Color.Black
        Me.PicPrintPro1.Lmargin = 249.6!
        Me.PicPrintPro1.OutputFileName = ""
        Me.PicPrintPro1.OwnDIB = False
        Me.PicPrintPro1.Picture = Nothing
        Me.PicPrintPro1.PictureTransparent = False
        Me.PicPrintPro1.PictureTransparentColor = System.Drawing.Color.White
        Me.PicPrintPro1.PrinterDriverName = "winspool"
        Me.PicPrintPro1.PrinterName = "Dell Laser Printer 1700n PS3"
        Me.PicPrintPro1.PrinterPortName = "Ne05:"
        Me.PicPrintPro1.PrintPreviewOnly = False
        Me.PicPrintPro1.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1
        Me.PicPrintPro1.PrintToFile = False
        Me.PicPrintPro1.RequestedDPI = 200
        Me.PicPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4
        Me.PicPrintPro1.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip
        Me.PicPrintPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicPrintPro1.TMargin = 249.6!
        Me.PicPrintPro1.UseDefaultPrinter = True
        Me.PicPrintPro1.WordWrap = True
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_File, Me.mnu_About})
        '
        'mnu_File
        '
        Me.mnu_File.Index = 0
        Me.mnu_File.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_Quit})
        Me.mnu_File.Text = "File"
        '
        'mnu_Quit
        '
        Me.mnu_Quit.Index = 0
        Me.mnu_Quit.Text = "Quit"
        '
        'mnu_About
        '
        Me.mnu_About.Index = 1
        Me.mnu_About.Text = "About"
        '
        'lstInfo
        '
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Retrieving the printer capabilities via the NumBins, BinName, BinID, NumPapers," & _
        " PaperName, PaperID, NumCopies", "   and DuplexCap properties."})
        Me.lstInfo.Location = New System.Drawing.Point(24, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(744, 69)
        Me.lstInfo.TabIndex = 27
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(784, 630)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstInfo, Me.labelDuplex, Me.labelNumCopies, Me.label6, Me.label5, Me.label4, Me.label3, Me.label2, Me.label1, Me.button, Me.listBoxPaperID, Me.listBoxPaper, Me.listBoxBinID, Me.listBoxBins})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Menu = Me.MainMenu1
        Me.Name = "FormMain"
        Me.Text = "Printer Device Capabilities"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button.Click
        ' Select a printer
        PicPrintPro1.PrintDialog()
        listBoxBinID.Items.Clear()
        listBoxPaper.Items.Clear()
        listBoxPaperID.Items.Clear()
        listBoxBins.Items.Clear()

        ' Get all of the bin names and IDs
        Dim i As Integer
        For i = 1 To PicPrintPro1.NumBins - 1 Step 1
            listBoxBins.Items.Add(PicPrintPro1.BinName(CShort(i)))
            listBoxBinID.Items.Add(PicPrintPro1.BinID(CShort(i)))
        Next

        For i = 1 To PicPrintPro1.NumBins - 1 Step 1
            listBoxBins.Items.Add(PicPrintPro1.BinName(CShort(i)))
            listBoxBinID.Items.Add(PicPrintPro1.BinID(CShort(i)))
        Next

        ' Get all of the paper names and IDs
        For i = 1 To PicPrintPro1.NumPapers - 1 Step 1
            listBoxPaper.Items.Add(PicPrintPro1.PaperName(CShort(i)))
            listBoxPaperID.Items.Add(PicPrintPro1.PaperID(CShort(i)))
        Next

        labelNumCopies.Text = PicPrintPro1.NumCopies.ToString()
        labelDuplex.Text = PicPrintPro1.DuplexCap.ToString()
    End Sub

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub

    Private Sub mnu_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Quit.Click

        System.Environment.Exit(0)

    End Sub

    Private Sub mnu_About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_About.Click

        PicPrintPro1.AboutBox()

    End Sub


End Class

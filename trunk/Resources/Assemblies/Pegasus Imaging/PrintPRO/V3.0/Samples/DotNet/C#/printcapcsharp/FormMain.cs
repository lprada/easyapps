//****************************************************************
//* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *
//* This sample code is provided to Pegasus licensees "as is"    *
//* with no restrictions on use or modification. No warranty for *
//* use of this sample code is provided by Pegasus.              *
//****************************************************************

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;


namespace PrintCapCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox listBoxBins;
		private System.Windows.Forms.ListBox listBoxBinID;
		private System.Windows.Forms.ListBox listBoxPaper;
		private System.Windows.Forms.ListBox listBoxPaperID;
		private System.Windows.Forms.Button button;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label labelNumCopies;
		private System.Windows.Forms.Label labelDuplex;
		private PegasusImaging.WinForms.PrintPro3.PICPrintPro picPrintPro2;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;

		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			
			UnlockPICPrintPro.PS_Unlock (12345, 12345, 12345, 12345);
	
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			picPrintPro2.Dispose ();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBoxBins = new System.Windows.Forms.ListBox();
			this.listBoxBinID = new System.Windows.Forms.ListBox();
			this.listBoxPaper = new System.Windows.Forms.ListBox();
			this.listBoxPaperID = new System.Windows.Forms.ListBox();
			this.button = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.labelNumCopies = new System.Windows.Forms.Label();
			this.labelDuplex = new System.Windows.Forms.Label();
			this.picPrintPro2 = new PegasusImaging.WinForms.PrintPro3.PICPrintPro();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// listBoxBins
			// 
			this.listBoxBins.Location = new System.Drawing.Point(24, 216);
			this.listBoxBins.Name = "listBoxBins";
			this.listBoxBins.Size = new System.Drawing.Size(184, 173);
			this.listBoxBins.TabIndex = 0;
			// 
			// listBoxBinID
			// 
			this.listBoxBinID.Location = new System.Drawing.Point(24, 464);
			this.listBoxBinID.Name = "listBoxBinID";
			this.listBoxBinID.Size = new System.Drawing.Size(184, 173);
			this.listBoxBinID.TabIndex = 1;
			// 
			// listBoxPaper
			// 
			this.listBoxPaper.Location = new System.Drawing.Point(264, 216);
			this.listBoxPaper.Name = "listBoxPaper";
			this.listBoxPaper.Size = new System.Drawing.Size(200, 420);
			this.listBoxPaper.TabIndex = 2;
			// 
			// listBoxPaperID
			// 
			this.listBoxPaperID.Location = new System.Drawing.Point(504, 208);
			this.listBoxPaperID.Name = "listBoxPaperID";
			this.listBoxPaperID.Size = new System.Drawing.Size(184, 420);
			this.listBoxPaperID.TabIndex = 3;
			// 
			// button
			// 
			this.button.Location = new System.Drawing.Point(256, 120);
			this.button.Name = "button";
			this.button.Size = new System.Drawing.Size(184, 24);
			this.button.TabIndex = 4;
			this.button.Text = "Get Printer Capabilties";
			this.button.Click += new System.EventHandler(this.button_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(32, 184);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 16);
			this.label1.TabIndex = 6;
			this.label1.Text = "Supported Bins:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(32, 424);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(120, 16);
			this.label2.TabIndex = 7;
			this.label2.Text = "Bin ID:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(288, 184);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(152, 16);
			this.label3.TabIndex = 8;
			this.label3.Text = "Supported Paper:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(504, 184);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 16);
			this.label4.TabIndex = 9;
			this.label4.Text = "PaperID:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(24, 656);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 10;
			this.label5.Text = "Num Copies:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(120, 656);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(80, 16);
			this.label6.TabIndex = 11;
			this.label6.Text = "Duplex?";
			// 
			// labelNumCopies
			// 
			this.labelNumCopies.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.labelNumCopies.Location = new System.Drawing.Point(24, 680);
			this.labelNumCopies.Name = "labelNumCopies";
			this.labelNumCopies.Size = new System.Drawing.Size(72, 16);
			this.labelNumCopies.TabIndex = 12;
			// 
			// labelDuplex
			// 
			this.labelDuplex.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.labelDuplex.Location = new System.Drawing.Point(120, 680);
			this.labelDuplex.Name = "labelDuplex";
			this.labelDuplex.Size = new System.Drawing.Size(72, 16);
			this.labelDuplex.TabIndex = 13;
			// 
			// picPrintPro2
			// 
			this.picPrintPro2.AddCR = true;
			this.picPrintPro2.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop;
			this.picPrintPro2.AutoMargin = true;
			this.picPrintPro2.BackColor = System.Drawing.Color.White;
			this.picPrintPro2.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent;
			this.picPrintPro2.BMargin = 249.6F;
			this.picPrintPro2.CurrentX = 249.6F;
			this.picPrintPro2.CurrentY = 249.6F;
			this.picPrintPro2.DocName = "PrintPRO Document";
			this.picPrintPro2.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen;
			this.picPrintPro2.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid;
			this.picPrintPro2.DrawWidth = 1;
			this.picPrintPro2.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic;
			this.picPrintPro2.FillColor = System.Drawing.Color.Black;
			this.picPrintPro2.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent;
			this.picPrintPro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picPrintPro2.ForeColor = System.Drawing.Color.Black;
			this.picPrintPro2.Lmargin = 249.6F;
			this.picPrintPro2.OutputFileName = "";
			this.picPrintPro2.OwnDIB = false;
			this.picPrintPro2.Picture = null;
			this.picPrintPro2.PictureTransparent = false;
			this.picPrintPro2.PictureTransparentColor = System.Drawing.Color.Black;
			this.picPrintPro2.PrinterDriverName = "winspool";
			this.picPrintPro2.PrinterName = "Dell Laser Printer 1700n PS3";
			this.picPrintPro2.PrinterPortName = "Ne05:";
			this.picPrintPro2.PrintPreviewOnly = false;
			this.picPrintPro2.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
			this.picPrintPro2.PrintToFile = false;
			this.picPrintPro2.RequestedDPI = 200;
			this.picPrintPro2.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4;
			this.picPrintPro2.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip;
			this.picPrintPro2.SN = "PEXHJ700AA-PEG0Q000195";
			this.picPrintPro2.TMargin = 249.6F;
			this.picPrintPro2.UseDefaultPrinter = true;
			this.picPrintPro2.WordWrap = true;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// lstInfo
			// 
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Retrieving the printer capabilities via the NumBins, BinName, BinID, NumPapers," +
														 " PaperName, PaperID, NumCopies",
														 "   and DuplexCap properties."});
			this.lstInfo.Location = new System.Drawing.Point(24, 16);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(664, 69);
			this.lstInfo.TabIndex = 14;
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(714, 704);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstInfo,
																		  this.labelDuplex,
																		  this.labelNumCopies,
																		  this.label6,
																		  this.label5,
																		  this.label4,
																		  this.label3,
																		  this.label2,
																		  this.label1,
																		  this.button,
																		  this.listBoxPaperID,
																		  this.listBoxPaper,
																		  this.listBoxBinID,
																		  this.listBoxBins});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "FormMain";
			this.Text = "Printer Device Capabilities";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void button_Click(object sender, System.EventArgs e)
		{
			// Select a printer
			picPrintPro2.PrintDialog ();
			listBoxBinID.Items.Clear ();
			listBoxPaper.Items.Clear ();
			listBoxPaperID.Items.Clear ();
			listBoxBins.Items.Clear ();

			// Get all of the bin names and IDs
			for (int i=1; i<picPrintPro2.NumBins; i++)
			{
				listBoxBins.Items.Add (picPrintPro2.BinName ((short) i));
				listBoxBinID.Items.Add (picPrintPro2.BinID((short) i));
			}

			// Get all of the paper names and IDs
			for (int i=1; i<picPrintPro2.NumPapers; i++)
			{
				listBoxPaper.Items.Add (picPrintPro2.PaperName((short) i));
				listBoxPaperID.Items.Add (picPrintPro2.PaperID((short) i));
			}
			
			labelNumCopies.Text = picPrintPro2.NumCopies.ToString ();
			labelDuplex.Text = picPrintPro2.DuplexCap.ToString ();
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
			
		}

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
			 System.Environment.Exit(0);
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			 picPrintPro2.AboutBox();
		}

		
	}
}

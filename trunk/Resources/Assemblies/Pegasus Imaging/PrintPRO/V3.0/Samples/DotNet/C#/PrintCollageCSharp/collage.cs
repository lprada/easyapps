//****************************************************************
//* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *
//* This sample code is provided to Pegasus licensees "as is"    *
//* with no restrictions on use or modification. No warranty for *
//* use of this sample code is provided by Pegasus.              *
//****************************************************************

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.PrintPro3;
using System.Data;

namespace PrintCollage
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class collage : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.PrintPro3.PICPrintPro print;
		private System.Windows.Forms.Button cmdCreate;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private System.Windows.Forms.ListBox lstBox;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXViewHidden;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public collage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.print = new PegasusImaging.WinForms.PrintPro3.PICPrintPro();
			this.cmdCreate = new System.Windows.Forms.Button();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXViewHidden = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lstBox = new System.Windows.Forms.ListBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// print
			// 
			this.print.AddCR = true;
			this.print.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop;
			this.print.AutoMargin = true;
			this.print.BackColor = System.Drawing.Color.White;
			this.print.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent;
			this.print.BMargin = 249.6F;
			this.print.CurrentX = 249.6F;
			this.print.CurrentY = 249.6F;
			this.print.DocName = "PrintPRO Document";
			this.print.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen;
			this.print.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid;
			this.print.DrawWidth = 1;
			this.print.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic;
			this.print.FillColor = System.Drawing.Color.Black;
			this.print.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent;
			this.print.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.print.ForeColor = System.Drawing.Color.Black;
			this.print.Lmargin = 249.6F;
			this.print.OutputFileName = "";
			this.print.OwnDIB = false;
			this.print.Picture = null;
			this.print.PictureTransparent = false;
			this.print.PictureTransparentColor = System.Drawing.Color.Black;
			this.print.PrinterDriverName = "winspool";
			this.print.PrinterName = "Dell Laser Printer 1700n PS3";
			this.print.PrinterPortName = "Ne05:";
			this.print.PrintPreviewOnly = false;
			this.print.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
			this.print.PrintToFile = false;
			this.print.RequestedDPI = 200;
			this.print.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4;
			this.print.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip;
			this.print.SN = "PEXHJ700AA-PEG0Q000195";
			this.print.TMargin = 249.6F;
			this.print.UseDefaultPrinter = true;
			this.print.WordWrap = true;
			// 
			// cmdCreate
			// 
			this.cmdCreate.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdCreate.Location = new System.Drawing.Point(400, 176);
			this.cmdCreate.Name = "cmdCreate";
			this.cmdCreate.Size = new System.Drawing.Size(176, 72);
			this.cmdCreate.TabIndex = 2;
			this.cmdCreate.Text = "Create Collage";
			this.cmdCreate.Click += new System.EventHandler(this.cmdCreate_Click);
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.Location = new System.Drawing.Point(32, 136);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(304, 392);
			this.imageXView1.TabIndex = 4;
			// 
			// imageXViewHidden
			// 
			this.imageXViewHidden.Location = new System.Drawing.Point(480, 424);
			this.imageXViewHidden.Name = "imageXViewHidden";
			this.imageXViewHidden.Size = new System.Drawing.Size(96, 40);
			this.imageXViewHidden.TabIndex = 5;
			this.imageXViewHidden.Visible = false;
			// 
			// lstBox
			// 
			this.lstBox.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstBox.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstBox.Items.AddRange(new object[] {
														"This sample demonstrates the following functionality:",
														"1)Using the Photograph Collage Interface which includes the TemplateFile property" +
														" to load in the",
														"    template file containing the coordinates and the PrintTemplateDIB method to p" +
														"rint the images."});
			this.lstBox.Location = new System.Drawing.Point(24, 16);
			this.lstBox.Name = "lstBox";
			this.lstBox.Size = new System.Drawing.Size(568, 69);
			this.lstBox.TabIndex = 6;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click_1);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// collage
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 558);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstBox,
																		  this.imageXViewHidden,
																		  this.imageXView1,
																		  this.cmdCreate});
			this.Menu = this.mainMenu1;
			this.Name = "collage";
			this.Text = "Print Collage";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new collage());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
		   		
			System.String strCurrentDir4 = System.IO.Directory.GetCurrentDirectory();
			System.String strImagePath4 = System.IO.Path.Combine (strCurrentDir4, @"..\..\..\..\..\..\..\..\Common\Images\template.txt");
			print.TemplateFile = strImagePath4;
		}

		private void cmdCreate_Click(object sender, System.EventArgs e)
		{
			print.SelectDefaultPrinter();
			print.PrintToFile = true;
			print.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF;
			print.OutputFileName = "collage.tif";
			print.StartPrintDoc();

			System.String strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString ();
			System.String strImagePath = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\dome.jpg");
			imageXViewHidden.Image =  PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath);
			print.hDIB = (System.Int32)(imageXViewHidden.Image.ToHdib(true));
			print.PrintTemplateDIB(1);
			
			string strCurrentDir1 = System.IO.Directory.GetCurrentDirectory().ToString();
			string strImagePath1 = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\water.jpg");
			
			imageXViewHidden.Image =  PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath1);
			print.hDIB = (System.Int32)(imageXViewHidden.Image.ToHdib(true));
			print.PrintTemplateDIB(2);
			

		    string strCurrentDir2 = System.IO.Directory.GetCurrentDirectory ().ToString();
			string strImagePath2 = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\pic1.bmp");

			imageXViewHidden.Image =  PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath2);
			print.hDIB = (System.Int32)(imageXViewHidden.Image.ToHdib(true));
			print.PrintTemplateDIB(3);
			

			string strCurrentDir3 = System.IO.Directory.GetCurrentDirectory ().ToString();
			string strImagePath3 = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\pic1.bmp");

			imageXViewHidden.Image =  PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath3);
			print.hDIB = (System.Int32)(imageXViewHidden.Image.ToHdib(true));
			print.PrintTemplateDIB(4);
			print.EndPrintDoc();
			
			//show the output image here
			imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile("collage.tif");
			imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;

		}

		
		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			print.AboutBox();
		
		}

		private void mnu_Quit_Click_1(object sender, System.EventArgs e)
		{
		 Application.Exit();
		}

		

	}
}

Option Explicit On 
Option Strict On

Imports PegasusImaging.WinForms.PrintPro3

Public Class FormMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        UnlockPICPrintPro.PS_Unlock(12345, 12345, 12345, 12345)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If

        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonPrint As System.Windows.Forms.Button
    Friend WithEvents PicPrintPro As PegasusImaging.WinForms.PrintPro3.PICPrintPro
    Friend WithEvents PicPrintPro1 As PegasusImaging.WinForms.PrintPro3.PICPrintPro
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnu_File As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Quit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_About As System.Windows.Forms.MenuItem
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonPrint = New System.Windows.Forms.Button()
        Me.PicPrintPro1 = New PegasusImaging.WinForms.PrintPro3.PICPrintPro()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnu_File = New System.Windows.Forms.MenuItem()
        Me.mnu_Quit = New System.Windows.Forms.MenuItem()
        Me.mnu_About = New System.Windows.Forms.MenuItem()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'buttonPrint
        '
        Me.buttonPrint.Location = New System.Drawing.Point(120, 176)
        Me.buttonPrint.Name = "buttonPrint"
        Me.buttonPrint.Size = New System.Drawing.Size(88, 23)
        Me.buttonPrint.TabIndex = 8
        Me.buttonPrint.Text = "Print"
        '
        'PicPrintPro1
        '
        Me.PicPrintPro1.AddCR = True
        Me.PicPrintPro1.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop
        Me.PicPrintPro1.AutoMargin = True
        Me.PicPrintPro1.BackColor = System.Drawing.Color.White
        Me.PicPrintPro1.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent
        Me.PicPrintPro1.BMargin = 249.6!
        Me.PicPrintPro1.CurrentX = 249.6!
        Me.PicPrintPro1.CurrentY = 249.6!
        Me.PicPrintPro1.DocName = "PrintPRO Document"
        Me.PicPrintPro1.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen
        Me.PicPrintPro1.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid
        Me.PicPrintPro1.DrawWidth = 1
        Me.PicPrintPro1.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic
        Me.PicPrintPro1.FillColor = System.Drawing.Color.Black
        Me.PicPrintPro1.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent
        Me.PicPrintPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicPrintPro1.ForeColor = System.Drawing.Color.Black
        Me.PicPrintPro1.Lmargin = 249.6!
        Me.PicPrintPro1.OutputFileName = ""
        Me.PicPrintPro1.OwnDIB = False
        Me.PicPrintPro1.Picture = Nothing
        Me.PicPrintPro1.PictureTransparent = False
        Me.PicPrintPro1.PictureTransparentColor = System.Drawing.Color.White
        Me.PicPrintPro1.PrinterDriverName = "winspool"
        Me.PicPrintPro1.PrinterName = "Dell Laser Printer 1700n PS3"
        Me.PicPrintPro1.PrinterPortName = "Ne05:"
        Me.PicPrintPro1.PrintPreviewOnly = False
        Me.PicPrintPro1.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1
        Me.PicPrintPro1.PrintToFile = False
        Me.PicPrintPro1.RequestedDPI = 200
        Me.PicPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4
        Me.PicPrintPro1.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip
        Me.PicPrintPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicPrintPro1.TMargin = 249.6!
        Me.PicPrintPro1.UseDefaultPrinter = True
        Me.PicPrintPro1.WordWrap = True
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_File, Me.mnu_About})
        '
        'mnu_File
        '
        Me.mnu_File.Index = 0
        Me.mnu_File.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_Quit})
        Me.mnu_File.Text = "File"
        '
        'mnu_Quit
        '
        Me.mnu_Quit.Index = 0
        Me.mnu_Quit.Text = "Quit"
        '
        'mnu_About
        '
        Me.mnu_About.Index = 1
        Me.mnu_About.Text = "About"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates the following functionality: ", "1)How to print a 50 by 50 grid inside the display area ", "   of the printer."})
        Me.ListBox1.Location = New System.Drawing.Point(8, 8)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(320, 95)
        Me.ListBox1.TabIndex = 9
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(344, 245)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.ListBox1, Me.buttonPrint})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Menu = Me.MainMenu1
        Me.Name = "FormMain"
        Me.Text = "PrintPRO Grid Example"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub buttonExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        System.Environment.Exit(0)
    End Sub

    Private Sub buttonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrint.Click
        ' select a printer
        PicPrintPro1.PrintDialog()

        ' Draw a border around the display area
        PicPrintPro1.DrawLine(PicPrintPro1.DisplayX, PicPrintPro1.DisplayY, PicPrintPro1.ScaleWidth + PicPrintPro1.DisplayX, PicPrintPro1.ScaleHeight + PicPrintPro1.DisplayY, System.Drawing.Color.Black, True, False)

        ' Draw the vertical lines
        Dim fDelta As Single = PicPrintPro1.ScaleWidth / 50
        Dim i As Integer
        For i = 1 To 49 Step 1
            PicPrintPro1.DrawLine(PicPrintPro1.DisplayX + fDelta * i, PicPrintPro1.DisplayY, PicPrintPro1.DisplayX + fDelta * i, PicPrintPro1.DisplayY + PicPrintPro1.ScaleHeight, System.Drawing.Color.Black, False, False)
        Next

        ' Draw the horizontal lines
        fDelta = PicPrintPro1.ScaleHeight / 50
        For i = 1 To 49 Step 1
            PicPrintPro1.DrawLine(PicPrintPro1.DisplayX, PicPrintPro1.DisplayY + fDelta * i, PicPrintPro1.DisplayX + PicPrintPro1.ScaleWidth, PicPrintPro1.DisplayY + fDelta * i, System.Drawing.Color.Black, False, False)
        Next

        ' Print a message in the center of the page
        Dim Font As System.Drawing.Font = New System.Drawing.Font("Times New Roman", 72, System.Drawing.FontStyle.Bold)
        PicPrintPro1.Font = Font
        PicPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle
        PicPrintPro1.PrintTextAligned("PrintPRO 50x50 Grid Example", PicPrintPro1.DisplayX, PicPrintPro1.DisplayY, PicPrintPro1.DisplayX + PicPrintPro1.ScaleWidth, PicPrintPro1.DisplayY + PicPrintPro1.ScaleHeight)

        ' Print the document
        PicPrintPro1.EndPrintDoc()
    End Sub

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
    End Sub


    Private Sub mnu_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Quit.Click

        System.Environment.Exit(0)

    End Sub

    Private Sub mnu_About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_About.Click

        PicPrintPro1.AboutBox()

    End Sub
End Class

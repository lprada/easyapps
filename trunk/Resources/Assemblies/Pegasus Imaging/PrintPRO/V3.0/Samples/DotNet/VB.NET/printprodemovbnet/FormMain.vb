'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.PrintPro3
Imports PegasusImaging.WinForms.ImagXpress8

Public Class FormMain
    Inherits System.Windows.Forms.Form


    Dim strCurrentDir As System.String = "..\..\..\..\..\..\..\Common\Images\"
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

        UnlockPICPrintPro.PS_Unlock(12345, 12345, 12345, 12345)



        InitializeComponent()

        'Add any initialization after the InitializeComponent() call


    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then


            ' Don't forget to dispose IX
            '
            If Not (ImagXpress1 Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (PicPrintPro1 Is Nothing) Then

                PicPrintPro1.Dispose()
                PicPrintPro1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If

        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonPrintDemoPage As System.Windows.Forms.Button
    Friend WithEvents groupBoxPrintPreview As System.Windows.Forms.GroupBox
    Friend WithEvents buttonScale15 As System.Windows.Forms.Button
    Friend WithEvents buttonScale14 As System.Windows.Forms.Button
    Friend WithEvents buttonScale13 As System.Windows.Forms.Button
    Friend WithEvents buttonScale11 As System.Windows.Forms.Button
    Friend WithEvents buttonScale12 As System.Windows.Forms.Button
    Friend WithEvents buttonPrintPreview As System.Windows.Forms.Button
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents PicPrintPro As PegasusImaging.WinForms.PrintPro3.PICPrintPro
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox

    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox

    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEventspicprintpro1 As PegasusImaging.WinForms.PrintPro3.PICPrintPro
    Friend WithEvents PicPrintPro1 As PegasusImaging.WinForms.PrintPro3.PICPrintPro
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnu_File As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_OpenBitmap As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_About As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Quit As System.Windows.Forms.MenuItem
    Friend WithEvents lslInfo As System.Windows.Forms.ListBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonPrintDemoPage = New System.Windows.Forms.Button()
        Me.groupBoxPrintPreview = New System.Windows.Forms.GroupBox()
        Me.buttonScale15 = New System.Windows.Forms.Button()
        Me.buttonScale14 = New System.Windows.Forms.Button()
        Me.buttonScale13 = New System.Windows.Forms.Button()
        Me.buttonScale11 = New System.Windows.Forms.Button()
        Me.buttonScale12 = New System.Windows.Forms.Button()
        Me.buttonPrintPreview = New System.Windows.Forms.Button()
        Me.label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.PicPrintPro1 = New PegasusImaging.WinForms.PrintPro3.PICPrintPro()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnu_File = New System.Windows.Forms.MenuItem()
        Me.mnu_OpenBitmap = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.mnu_Quit = New System.Windows.Forms.MenuItem()
        Me.mnu_About = New System.Windows.Forms.MenuItem()
        Me.lslInfo = New System.Windows.Forms.ListBox()
        Me.groupBoxPrintPreview.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'buttonPrintDemoPage
        '
        Me.buttonPrintDemoPage.Location = New System.Drawing.Point(40, 272)
        Me.buttonPrintDemoPage.Name = "buttonPrintDemoPage"
        Me.buttonPrintDemoPage.Size = New System.Drawing.Size(192, 32)
        Me.buttonPrintDemoPage.TabIndex = 7
        Me.buttonPrintDemoPage.Text = "PrintToTiff"
        '
        'groupBoxPrintPreview
        '
        Me.groupBoxPrintPreview.Controls.AddRange(New System.Windows.Forms.Control() {Me.buttonScale15, Me.buttonScale14, Me.buttonScale13, Me.buttonScale11, Me.buttonScale12, Me.buttonPrintPreview})
        Me.groupBoxPrintPreview.Location = New System.Drawing.Point(16, 320)
        Me.groupBoxPrintPreview.Name = "groupBoxPrintPreview"
        Me.groupBoxPrintPreview.Size = New System.Drawing.Size(240, 248)
        Me.groupBoxPrintPreview.TabIndex = 8
        Me.groupBoxPrintPreview.TabStop = False
        Me.groupBoxPrintPreview.Text = "Print Preview"
        '
        'buttonScale15
        '
        Me.buttonScale15.Location = New System.Drawing.Point(68, 214)
        Me.buttonScale15.Name = "buttonScale15"
        Me.buttonScale15.Size = New System.Drawing.Size(104, 24)
        Me.buttonScale15.TabIndex = 5
        Me.buttonScale15.Text = "Scale 1:5"
        '
        'buttonScale14
        '
        Me.buttonScale14.Location = New System.Drawing.Point(68, 176)
        Me.buttonScale14.Name = "buttonScale14"
        Me.buttonScale14.Size = New System.Drawing.Size(104, 24)
        Me.buttonScale14.TabIndex = 4
        Me.buttonScale14.Text = "Scale 1:4"
        '
        'buttonScale13
        '
        Me.buttonScale13.Location = New System.Drawing.Point(68, 138)
        Me.buttonScale13.Name = "buttonScale13"
        Me.buttonScale13.Size = New System.Drawing.Size(104, 24)
        Me.buttonScale13.TabIndex = 3
        Me.buttonScale13.Text = "Scale 1:3"
        '
        'buttonScale11
        '
        Me.buttonScale11.Location = New System.Drawing.Point(68, 62)
        Me.buttonScale11.Name = "buttonScale11"
        Me.buttonScale11.Size = New System.Drawing.Size(104, 24)
        Me.buttonScale11.TabIndex = 2
        Me.buttonScale11.Text = "Scale 1:1"
        '
        'buttonScale12
        '
        Me.buttonScale12.Location = New System.Drawing.Point(68, 100)
        Me.buttonScale12.Name = "buttonScale12"
        Me.buttonScale12.Size = New System.Drawing.Size(104, 24)
        Me.buttonScale12.TabIndex = 1
        Me.buttonScale12.Text = "Scale 1:2"
        '
        'buttonPrintPreview
        '
        Me.buttonPrintPreview.Location = New System.Drawing.Point(44, 16)
        Me.buttonPrintPreview.Name = "buttonPrintPreview"
        Me.buttonPrintPreview.Size = New System.Drawing.Size(152, 24)
        Me.buttonPrintPreview.TabIndex = 0
        Me.buttonPrintPreview.Text = "Print Preview"
        '
        'label1
        '
        Me.label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(12, 7)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(240, 168)
        Me.label1.TabIndex = 6
        Me.label1.Text = "This sample demonstrates how you can use PrintPRO to print text, graphics and bit" & _
        "maps. This  sample also demonstrates a new feature of PrintPRO v3, ""PrintToTIFF " & _
        "which enables printing a TIFF image directly to file instead of the printer."
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.CheckBox1, Me.CheckBox2})
        Me.GroupBox1.Location = New System.Drawing.Point(8, 184)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(248, 80)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PrintToTiff"
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(16, 16)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(160, 16)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Enable PrintToTiff"
        '
        'CheckBox2
        '
        Me.CheckBox2.Location = New System.Drawing.Point(16, 48)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(160, 24)
        Me.CheckBox2.TabIndex = 12
        Me.CheckBox2.Text = "Disable PrintToTiff"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(40, 296)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(192, 32)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "PrintToFile"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(240, 168)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "This sample demonstrates how you can use PrintPRO to print text, graphics and bit" & _
        "maps.  Since PrintPRO is an ATL control, you can use it in any development envir" & _
        "onment that supports 32-bit COM controls including Internet Explorer."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.AddRange(New System.Windows.Forms.Control() {Me.RadioButton2, Me.RadioButton1})
        Me.GroupBox2.Location = New System.Drawing.Point(40, 208)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 80)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "PrintToFile"
        '
        'RadioButton2
        '
        Me.RadioButton2.Location = New System.Drawing.Point(16, 48)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(160, 24)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.Text = "Disable PrintToFile"
        '
        'RadioButton1
        '
        Me.RadioButton1.Location = New System.Drawing.Point(16, 16)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(160, 24)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.Text = "Enable PrintToFile"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button2, Me.Button3, Me.Button4, Me.Button5, Me.Button6, Me.Button7})
        Me.GroupBox3.Location = New System.Drawing.Point(24, 336)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(232, 248)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Print Preview"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(68, 214)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(104, 24)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Scale 1:5"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(68, 176)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(104, 24)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Scale 1:4"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(68, 138)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(104, 24)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Scale 1:3"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(68, 62)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(104, 24)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "Scale 1:1"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(68, 100)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(104, 24)
        Me.Button6.TabIndex = 1
        Me.Button6.Text = "Scale 1:2"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(44, 24)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(152, 24)
        Me.Button7.TabIndex = 0
        Me.Button7.Text = "Print Preview"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(608, 560)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 24)
        Me.Button8.TabIndex = 12
        Me.Button8.Text = "Exit"
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(32, 176)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(208, 24)
        Me.CheckBox3.TabIndex = 13
        Me.CheckBox3.Text = "Choose PrintToFile or Preview"
        '
        'CheckBox4
        '
        Me.CheckBox4.Location = New System.Drawing.Point(16, 160)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(208, 32)
        Me.CheckBox4.TabIndex = 3
        Me.CheckBox4.Text = "Choose PrintToFile or Preview"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button9, Me.RadioButton4, Me.RadioButton3})
        Me.GroupBox4.Location = New System.Drawing.Point(8, 208)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(216, 178)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "PrintToFile"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(32, 128)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(128, 24)
        Me.Button9.TabIndex = 2
        Me.Button9.Text = "Print Image"
        '
        'RadioButton4
        '
        Me.RadioButton4.Location = New System.Drawing.Point(24, 80)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(144, 24)
        Me.RadioButton4.TabIndex = 1
        Me.RadioButton4.Text = "DisablePrintToFile"
        '
        'RadioButton3
        '
        Me.RadioButton3.Location = New System.Drawing.Point(24, 24)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(152, 32)
        Me.RadioButton3.TabIndex = 0
        Me.RadioButton3.Text = "Enable PrintToFile"
        '
        'GroupBox5
        '
        Me.GroupBox5.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.GroupBox5.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button16, Me.Button15, Me.Button13, Me.Button12, Me.Button11, Me.Button10})
        Me.GroupBox5.Location = New System.Drawing.Point(16, 394)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(216, 256)
        Me.GroupBox5.TabIndex = 5
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Print Preview"
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(32, 216)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(128, 24)
        Me.Button16.TabIndex = 5
        Me.Button16.Text = "Scale 1:5"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(32, 184)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(128, 24)
        Me.Button15.TabIndex = 4
        Me.Button15.Text = "Scale 1:4"
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(32, 144)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(128, 24)
        Me.Button13.TabIndex = 3
        Me.Button13.Text = "Scale 1:3"
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(32, 104)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(128, 24)
        Me.Button12.TabIndex = 2
        Me.Button12.Text = "Scale 1:2"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(32, 72)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(128, 24)
        Me.Button11.TabIndex = 1
        Me.Button11.Text = "Scale 1:1"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(16, 24)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(160, 32)
        Me.Button10.TabIndex = 0
        Me.Button10.Text = "Print Preview"
        '
        'PicPrintPro1
        '
        Me.PicPrintPro1.AddCR = True
        Me.PicPrintPro1.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop
        Me.PicPrintPro1.AutoMargin = True
        Me.PicPrintPro1.BackColor = System.Drawing.Color.White
        Me.PicPrintPro1.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent
        Me.PicPrintPro1.BMargin = 249.6!
        Me.PicPrintPro1.CurrentX = 249.6!
        Me.PicPrintPro1.CurrentY = 249.6!
        Me.PicPrintPro1.DocName = "PrintPRO Document"
        Me.PicPrintPro1.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen
        Me.PicPrintPro1.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid
        Me.PicPrintPro1.DrawWidth = 1
        Me.PicPrintPro1.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic
        Me.PicPrintPro1.FillColor = System.Drawing.Color.Black
        Me.PicPrintPro1.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent
        Me.PicPrintPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicPrintPro1.ForeColor = System.Drawing.Color.Black
        Me.PicPrintPro1.Lmargin = 249.6!
        Me.PicPrintPro1.OutputFileName = ""
        Me.PicPrintPro1.OwnDIB = False
        Me.PicPrintPro1.Picture = Nothing
        Me.PicPrintPro1.PictureTransparent = False
        Me.PicPrintPro1.PictureTransparentColor = System.Drawing.Color.White
        Me.PicPrintPro1.PrinterDriverName = "winspool"
        Me.PicPrintPro1.PrinterName = "Dell Laser Printer 1700n PS3"
        Me.PicPrintPro1.PrinterPortName = "Ne05:"
        Me.PicPrintPro1.PrintPreviewOnly = False
        Me.PicPrintPro1.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1
        Me.PicPrintPro1.PrintToFile = False
        Me.PicPrintPro1.RequestedDPI = 200
        Me.PicPrintPro1.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4
        Me.PicPrintPro1.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip
        Me.PicPrintPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicPrintPro1.TMargin = 249.6!
        Me.PicPrintPro1.UseDefaultPrinter = True
        Me.PicPrintPro1.WordWrap = True
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.Location = New System.Drawing.Point(328, 152)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(392, 482)
        Me.ImageXView1.TabIndex = 6
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_File, Me.mnu_About})
        '
        'mnu_File
        '
        Me.mnu_File.Index = 0
        Me.mnu_File.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_OpenBitmap, Me.MenuItem2, Me.mnu_Quit})
        Me.mnu_File.Text = "File"
        '
        'mnu_OpenBitmap
        '
        Me.mnu_OpenBitmap.Index = 0
        Me.mnu_OpenBitmap.Text = "Open Bitmap to print..."
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'mnu_Quit
        '
        Me.mnu_Quit.Index = 2
        Me.mnu_Quit.Text = "Quit"
        '
        'mnu_About
        '
        Me.mnu_About.Index = 1
        Me.mnu_About.Text = "About"
        '
        'lslInfo
        '
        Me.lslInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lslInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lslInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using PrintPRO to print text, graphics and bitmaps.", "2)Calling the PrintToFile method which enables printing a TIFF image directly to " & _
        "file instead of the printer.", "", "Instructions:", "1)Choose either the PrintToFile option or PrintPreview option.", "2)If the PrintToFile option is chosen there are 2 options:", "   a)Enable PrintToFile which sends the printed data to a file.", "   b)Disable PrintToFile which sends the printed data to the printer."})
        Me.lslInfo.Location = New System.Drawing.Point(16, 16)
        Me.lslInfo.Name = "lslInfo"
        Me.lslInfo.Size = New System.Drawing.Size(704, 121)
        Me.lslInfo.TabIndex = 7
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(754, 657)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lslInfo, Me.ImageXView1, Me.GroupBox5, Me.GroupBox4, Me.CheckBox4})
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "FormMain"
        Me.Text = "PrintPRO v3.0 Demo"
        Me.groupBoxPrintPreview.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub PrintPage()

        ' Print a banner at the top of the page, white on black.
        PicPrintPro1.BackStyle = peBackStyle.BACK_Opaque
        '""Picpicprintpro1.CtlForeColor = System.Drawing.Color.FromArgb(255, 255, 255)   ' White text
        '""Picpicprintpro1.CtlBackColor = System.Drawing.Color.FromArgb(0, 0, 0)   ' Black background

        PicPrintPro1.Font = New System.Drawing.Font("Arial", 16, System.Drawing.FontStyle.Bold)
        PicPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle

        Dim prWidth As Single = PicPrintPro1.PictureWidth
        PicPrintPro1.PrintTextAligned("Hello! Check out PrintPRO 3.0  ", 1000, PicPrintPro1.TMargin, prWidth - 1000, 1100)

        ' The following code demonstrates how PrintPRO can print text
        ' aligned inside a rectangular area.  First, a box is drawn
        ' then text is drawn aligned inside the box.
        PicPrintPro1.DrawWidth = 1
        PicPrintPro1.BackStyle = peBackStyle.BACK_Transparent
        '""Picpicprintpro1.CtlForeColor = System.Drawing.Color.FromArgb(0, 0, 0)  ' Black text
        PicPrintPro1.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold)

        PicPrintPro1.Alignment = peAlignment.ALIGN_LeftJustifyTop
        PicPrintPro1.DrawLine(1440, 1440, 2880, 2880, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - left justified top", 1440, 1440, 2880, 2880)

        PicPrintPro1.Alignment = peAlignment.ALIGN_LeftJustifyMiddle
        PicPrintPro1.DrawLine(3100, 1440, 4540, 2880, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see i the printer supports formatted text - left justified middle", 3100, 1440, 4540, 2880)

        PicPrintPro1.Alignment = peAlignment.ALIGN_LeftJustifyBottom
        PicPrintPro1.DrawLine(4760, 1440, 6200, 2880, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - left justified bottom", 4760, 1440, 6200, 2880)

        PicPrintPro1.Alignment = peAlignment.ALIGN_RightJustifyTop
        PicPrintPro1.DrawLine(6420, 1440, 7860, 2880, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - right justified top", 6420, 1440, 7860, 2880)

        PicPrintPro1.Alignment = peAlignment.ALIGN_RightJustifyMiddle
        PicPrintPro1.DrawLine(1440, 3100, 2880, 4540, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - right justified middle", 1440, 3100, 2880, 4540)

        PicPrintPro1.Alignment = peAlignment.ALIGN_RightJustifyBottom
        PicPrintPro1.DrawLine(3100, 3100, 4540, 4540, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - right justified bottom", 3100, 3100, 4540, 4540)

        PicPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyTop
        PicPrintPro1.DrawLine(4760, 3100, 6200, 4540, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - center justified top", 4760, 3100, 6200, 4540)

        PicPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle
        PicPrintPro1.DrawLine(6420, 3100, 7860, 4540, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - center justified middle", 6420, 3100, 7860, 4540)

        PicPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyBottom
        PicPrintPro1.DrawLine(1440, 4760, 7860, 5200, System.Drawing.Color.Black, True, False)
        PicPrintPro1.PrintTextAligned("This is an experiment to see if the printer supports formatted text - center justified bottom", 1440, 4760, 7860, 5200)

        PicPrintPro1.PrintPicture(1640, 5700, 5000, 4000, 0, 0, 0, 0, False)


        ' Use graphic methods to draw an arrow
        PicPrintPro1.FillStyle = peFillStyle.FILL_Solid
        PicPrintPro1.DrawWidth = 15
        PicPrintPro1.DrawPolygon(8390, 7575, 6770, 7575, 6870, 7475, 6870, 7675, 6770, 7575, 0, 0, System.Drawing.Color.Black, 5)

        ' Draw text inside a gray box with a black border
        PicPrintPro1.FillStyle = peFillStyle.FILL_Transparent
        PicPrintPro1.Alignment = peAlignment.ALIGN_CenterJustifyMiddle

        ' Draw the gray box
        PicPrintPro1.DrawLine(8550, 5700, 10550, 9450, System.Drawing.Color.Gray, True, True)

        ' Put a black border on the box
        PicPrintPro1.DrawLine(8550, 5700, 10550, 9450, System.Drawing.Color.Black, True, False)

        ' Print text inside the box
        PicPrintPro1.Font = New System.Drawing.Font("Times", 18)
        PicPrintPro1.BackStyle = peBackStyle.BACK_Transparent
        PicPrintPro1.PrintTextAligned("Print" + Chr(13) + "Graphics" + Chr(13) + "and" + Chr(13) + "Bitmaps" + Chr(13) + "and" + Chr(13) + "text", 8550, 5500, 10550, 9250)

        ' Print the features
        PicPrintPro1.Font = New System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Underline Or System.Drawing.FontStyle.Bold)
        PicPrintPro1.Lmargin = 1440
        PicPrintPro1.CurrentX = 1440
        PicPrintPro1.CurrentY = 10000
        PicPrintPro1.PrintText("Plus!")
        PicPrintPro1.Font = New System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Bold)
        PicPrintPro1.PrintText("Lines, rectangles, circles, ellipse, arc,")
        PicPrintPro1.PrintText("curves, polygons, pie, rounded rectangles,")
        PicPrintPro1.PrintText("Print Dialog, and much, much more!")
        PicPrintPro1.PrintText("PrintPRO is an ATL control so you can use")
        PicPrintPro1.PrintText("it with your favorite dev. tool including IE." + Chr(13))
        PicPrintPro1.PrintText("The PrintPRO Team" + Chr(13))
        PicPrintPro1.Font = New System.Drawing.Font("Arial", 8)
        PicPrintPro1.CurrentY = PicPrintPro1.ScaleHeight - PicPrintPro1.TextHeight("This page")
        PicPrintPro1.PrintText("This page was created in Microsoft Visual Basic .NET using the Pegasus Software PrintPRO .NET control.")
    End Sub

    Private Sub buttonPrintDemoPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonPrintDemoPage.Click
        If (PicPrintPro1.PrintError = 0) Then


            PicPrintPro1.PrintPreviewOnly = False
            ' Give the document a name.  The default document
            ' name is PrintPRO Document.
            If (bPrintToTiff = True) Then
                PicPrintPro1.DocName = "Pegasus's PrintPRO Demo"
                PicPrintPro1.OutputFileName = "PegasusTst.tif"
                PicPrintPro1.PrintToFile = True
                PicPrintPro1.SaveFileType = peSaveFileType.PP_TIF
                PicPrintPro1.RequestedDPI = 100

            Else
                PicPrintPro1.DocName = "Pegasus's PrintPRO Demo"
                PicPrintPro1.OutputFileName = ""
                PicPrintPro1.PrintToFile = False
                PicPrintPro1.SaveFileType = peSaveFileType.PP_TIF
            End If


            ' Start a print job by selecting a printer and starting a
            ' new page. If you don't select a printer or start a new
            ' page, PrintPRO will select the default printer and start
            ' a new page when you first print text, graphics or a bitmap.
            PicPrintPro1.PrintDialog()
            PicPrintPro1.StartPrintDoc()
            PicPrintPro1.PrintPreviewOnly = False

            ' Print all of the information on the page
            Call PrintPage()

            ' End the print job.  This will end the current page and print the document
            PicPrintPro1.EndPrintDoc()
            If (bPrintToTiff = True) Then
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile("PegasusTst.tif")
            End If

            Me.Cursor = System.Windows.Forms.Cursors.Default
        End If
    End Sub

    Private Sub buttonExit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        System.Environment.Exit(0)
    End Sub

    Private Sub buttonScale15_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonScale15.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PSCALE_1_5
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError = 0 Then
            ' get the image from the PrintPRO control, and assign in to the image
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

        End If
    End Sub

    Private Sub buttonScale14_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonScale14.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_4
        PicPrintPro1.GetPrintPreview(1)
        If (PicPrintPro1.PrintError = 0) Then
            ' get the image from the PrintPRO control, and assign in to the image
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

        End If
    End Sub

    Private Sub buttonScale13_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonScale13.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_3
        PicPrintPro1.GetPrintPreview(1)
        If (PicPrintPro1.PrintError = 0) Then
            ' get the image from the PrintPRO control, and assign in to the image
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

        End If
    End Sub

    Private Sub buttonScale11_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonScale11.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_1
        PicPrintPro1.GetPrintPreview(1)
        If (PicPrintPro1.PrintError = 0) Then
            ' get the image from the PrintPRO control, and assign in to the image
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

        End If
    End Sub

    Private Sub buttonScale12_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonScale12.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_2
        PicPrintPro1.GetPrintPreview(1)
        If (PicPrintPro1.PrintError = 0) Then
            ' get the image from the PrintPRO control, and assign in to the image
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

        End If
    End Sub

    Private Sub buttonPrintPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonPrintPreview.Click
        If (PicPrintPro1.PrintError = 0) Then
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            PicPrintPro1.PrintDialog()
            PicPrintPro1.PrintPreviewOnly = True
            PicPrintPro1.StartPrintPreview()

            If (PicPrintPro1.PrintError <> 0) Then
                MessageBox.Show("PrintPRO Error: " + PicPrintPro1.PrintError, "PrintPRO Demo", System.Windows.Forms.MessageBoxButtons.OKCancel)
            End If

            PrintPage()

            ' End the print preview
            PicPrintPro1.EndPrintPreview()
            If (PicPrintPro1.PrintError <> 0) Then
                MessageBox.Show("PrintPRO Error: " + PicPrintPro1.PrintError, "PrintPRO Demo", System.Windows.Forms.MessageBoxButtons.OKCancel)
            End If

            PicPrintPro1.GetPrintPreview(1)
            If (PicPrintPro1.PrintError <> 0) Then
                MessageBox.Show("PrintPRO Error: " + PicPrintPro1.PrintError, "PrintPRO Demo", System.Windows.Forms.MessageBoxButtons.OKCancel)
            End If

            ' get the image from the PrintPRO control, and assign in to the image
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))


            Me.Cursor = System.Windows.Forms.Cursors.Default
        End If
    End Sub
    Dim bPrintToTiff As Boolean
    Private Sub FormMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CheckBox4.Checked = True


        '**The UnlockRuntime function must be called to distribute the runtime**
        'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)
        

        ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.CropImage

        ImageXView1.AutoScroll = True

        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2
        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2

        Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
        Dim strImagePath As String = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\pic1.bmp")

        PicPrintPro1.Picture = System.Drawing.Image.FromFile(strImagePath)
        bPrintToTiff = False

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            bPrintToTiff = True
        End If

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked Then
            bPrintToTiff = False
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If PicPrintPro1.PrintError = 0 Then
            'Note: all of the dimensions in this demo are in twips.


            ' Give the document a name.  The default document
            ' name is PrintPRO Document.
            If (bPrintToTiff = True) Then
                PicPrintPro1.DocName = "Pegasus's PrintPRO Demo"
                PicPrintPro1.OutputFileName = "PrintToFile.tif"
                PicPrintPro1.PrintToFile = True
                PicPrintPro1.SaveFileType = peSaveFileType.PP_TIF
                PicPrintPro1.RequestedDPI = 100
            Else
                PicPrintPro1.DocName = "Pegasus's PrintPRO Demo"
                PicPrintPro1.OutputFileName = ""
                PicPrintPro1.PrintToFile = False
                PicPrintPro1.SaveFileType = peSaveFileType.PP_TIF
            End If

            ' Start a print job by selecting a printer and starting a
            ' new page. If you don't select a printer or start a new
            ' page, PrintPRO will select the default printer and start
            ' a new page when you first print text, graphics or a bitmap.
            PicPrintPro1.PrintDialog()
            PicPrintPro1.StartPrintDoc()
            PicPrintPro1.PrintPreviewOnly = False
            ' Print all of the information on the page
            PrintPage()
            ' End the print job.  This will end the current page and print the document
            PicPrintPro1.EndPrintDoc()
            If (bPrintToTiff = True) Then
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile("PrintToFile.tif")
            End If

        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click

        ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.CropImage
        PicPrintPro1.PrintPreviewOnly = True
        PicPrintPro1.StartPrintPreview()
        If PicPrintPro1.PrintError <> 0 Then MsgBox("PrintPRO error: " & PicPrintPro1.PrintError, vbOKCancel, "PrintPRO Demo")
        PrintPage()
        ' End the print preview
        PicPrintPro1.EndPrintPreview()
        If PicPrintPro1.PrintError <> 0 Then MsgBox("PrintPRO error: " & PicPrintPro1.PrintError, vbOKCancel, "PrintPRO Demo")
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError <> 0 Then MsgBox("PrintPRO error: " & PicPrintPro1.PrintError, vbOKCancel, "PrintPRO Demo")

        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))


    End Sub




    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            bPrintToTiff = True
        Else
            bPrintToTiff = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            bPrintToTiff = False
        Else
            bPrintToTiff = True
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        System.Environment.Exit(0)

    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            GroupBox2.Enabled = True
            GroupBox3.Enabled = False
        Else
            GroupBox3.Enabled = True
            GroupBox2.Enabled = False
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If PicPrintPro1.PrintError = 0 Then
            'Note: all of the dimensions in this demo are in twips.


            ' Give the document a name.  The default document
            ' name is PrintPRO Document.
            If (bPrintToTiff = True) Then
                PicPrintPro1.DocName = "Pegasus's PrintPRO Demo"
                PicPrintPro1.OutputFileName = "PrintToFile.tif"
                PicPrintPro1.PrintToFile = True
                PicPrintPro1.SaveFileType = peSaveFileType.PP_TIF
                PicPrintPro1.RequestedDPI = 100
            Else
                PicPrintPro1.DocName = "Pegasus's PrintPRO Demo"
                PicPrintPro1.OutputFileName = ""
                PicPrintPro1.PrintToFile = False
                PicPrintPro1.SaveFileType = peSaveFileType.PP_TIF
            End If

            ' Start a print job by selecting a printer and starting a
            ' new page. If you don't select a printer or start a new
            ' page, PrintPRO will select the default printer and start
            ' a new page when you first print text, graphics or a bitmap.
            PicPrintPro1.PrintDialog()
            PicPrintPro1.StartPrintDoc()
            PicPrintPro1.PrintPreviewOnly = False
            ' Print all of the information on the page
            PrintPage()
            ' End the print job.  This will end the current page and print the document
            PicPrintPro1.EndPrintDoc()
            If (bPrintToTiff = True) Then
                ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile("PrintToFile.tif")
            End If

        End If

    End Sub


    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            GroupBox4.Enabled = True
            GroupBox5.Enabled = False
        Else
            GroupBox4.Enabled = False
            GroupBox5.Enabled = True
        End If
    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        End

    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            bPrintToTiff = True
        Else
            bPrintToTiff = False
        End If
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If RadioButton4.Checked = True Then
            bPrintToTiff = False
        Else
            bPrintToTiff = True
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.CropImage
        If PicPrintPro1.PrintError = 0 Then


            PicPrintPro1.PrintPreviewOnly = True
            PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_1
            PicPrintPro1.StartPrintPreview()
            If PicPrintPro1.PrintError <> 0 Then MsgBox("PrintPRO error: " & PicPrintPro1.PrintError, vbOKCancel, "PrintPRO Demo")
            PrintPage()
            ' End the print preview
            PicPrintPro1.EndPrintPreview()
            If PicPrintPro1.PrintError <> 0 Then MsgBox("PrintPRO error: " & PicPrintPro1.PrintError, vbOKCancel, "PrintPRO Demo")
            PicPrintPro1.GetPrintPreview(1)
            If PicPrintPro1.PrintError <> 0 Then MsgBox("PrintPRO error: " & PicPrintPro1.PrintError, vbOKCancel, "PrintPRO Demo")

            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

        End If
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_1
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError = 0 Then ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_2
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError = 0 Then ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_3
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError = 0 Then ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PPSCALE_1_4
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError = 0 Then ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        PicPrintPro1.PrintPreviewScale = pePPScaleMode.PSCALE_1_5
        PicPrintPro1.GetPrintPreview(1)
        If PicPrintPro1.PrintError = 0 Then ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(PicPrintPro1.PrintPreviewDIB))

    End Sub

    Private Sub mnu_OpenBitmap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_OpenBitmap.Click
        OpenFileDialog1.Title = "Open Tiff File"
        OpenFileDialog1.Filter = "Windows Bitmap (*.BMP) | *.bmp"
        OpenFileDialog1.DefaultExt = ".tif"
        OpenFileDialog1.Multiselect = False
        OpenFileDialog1.InitialDirectory = strCurrentDir
        OpenFileDialog1.ShowDialog(Me)

        Dim strFilePath As String = OpenFileDialog1.FileName

        PicPrintPro1.Picture = System.Drawing.Image.FromFile(strFilePath)
    End Sub


    Private Sub mnu_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Quit.Click

        Application.Exit()
    End Sub

    Private Sub mnu_About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_About.Click

        PicPrintPro1.AboutBox()

    End Sub
End Class

//****************************************************************
//* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *
//* This sample code is provided to Pegasus licensees "as is"    *
//* with no restrictions on use or modification. No warranty for *
//* use of this sample code is provided by Pegasus.              *
//****************************************************************
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.PrintPro3;
namespace ICMProfiles
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	/// 



	public class Form1 : System.Windows.Forms.Form
	{
		
		private System.Windows.Forms.MainMenu menu;
		private System.Windows.Forms.OpenFileDialog cd;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem5;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		
		
		string gStrSavedICMMonitorProfile; 
		string gStrSavedICMPrinterrProfile;
		private System.Windows.Forms.GroupBox renderintent;
		private System.Windows.Forms.RadioButton optImage;
		private System.Windows.Forms.RadioButton optGraph;
		private System.Windows.Forms.RadioButton OptMetric;
		private System.Windows.Forms.RadioButton OptNone;
		private PegasusImaging.WinForms.PrintPro3.PICPrintPro print;
		private System.Windows.Forms.CheckBox chkproof;
		private System.Windows.Forms.Button cmdpreview;
		private System.Windows.Forms.RadioButton optbus;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private System.Windows.Forms.ListBox lstInfo;

		
		
		public Form1()
		{


			//****Must call the UnlockControl function *****
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.menu = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.cd = new System.Windows.Forms.OpenFileDialog();
			this.renderintent = new System.Windows.Forms.GroupBox();
			this.OptNone = new System.Windows.Forms.RadioButton();
			this.OptMetric = new System.Windows.Forms.RadioButton();
			this.optbus = new System.Windows.Forms.RadioButton();
			this.optGraph = new System.Windows.Forms.RadioButton();
			this.optImage = new System.Windows.Forms.RadioButton();
			this.print = new PegasusImaging.WinForms.PrintPro3.PICPrintPro();
			this.chkproof = new System.Windows.Forms.CheckBox();
			this.cmdpreview = new System.Windows.Forms.Button();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.renderintent.SuspendLayout();
			this.SuspendLayout();
			// 
			// menu
			// 
			this.menu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				 this.menuItem1,
																				 this.mnu_About});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2,
																					  this.menuItem3,
																					  this.menuItem5,
																					  this.menuItem6,
																					  this.mnu_Quit});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "Load Monitor Profile";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "Load Printer Profile";
			this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 2;
			this.menuItem5.Text = "Open Image";
			this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 3;
			this.menuItem6.Text = "-";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 4;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// renderintent
			// 
			this.renderintent.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.renderintent.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.OptNone,
																					   this.OptMetric,
																					   this.optbus,
																					   this.optGraph,
																					   this.optImage});
			this.renderintent.Location = new System.Drawing.Point(408, 248);
			this.renderintent.Name = "renderintent";
			this.renderintent.Size = new System.Drawing.Size(232, 224);
			this.renderintent.TabIndex = 2;
			this.renderintent.TabStop = false;
			this.renderintent.Text = "ICM Render Intent";
			// 
			// OptNone
			// 
			this.OptNone.Location = new System.Drawing.Point(24, 176);
			this.OptNone.Name = "OptNone";
			this.OptNone.Size = new System.Drawing.Size(160, 32);
			this.OptNone.TabIndex = 4;
			this.OptNone.Text = "RI_GM_NONE";
			this.OptNone.CheckedChanged += new System.EventHandler(this.OptNone_CheckedChanged);
			// 
			// OptMetric
			// 
			this.OptMetric.Location = new System.Drawing.Point(24, 136);
			this.OptMetric.Name = "OptMetric";
			this.OptMetric.Size = new System.Drawing.Size(184, 32);
			this.OptMetric.TabIndex = 3;
			this.OptMetric.Text = "RI_GM_ABS_COLORIMETRIC ";
			this.OptMetric.CheckedChanged += new System.EventHandler(this.OptMetric_CheckedChanged);
			// 
			// optbus
			// 
			this.optbus.Location = new System.Drawing.Point(24, 104);
			this.optbus.Name = "optbus";
			this.optbus.Size = new System.Drawing.Size(144, 24);
			this.optbus.TabIndex = 2;
			this.optbus.Text = "RI_GM_BUSINESS";
			this.optbus.CheckedChanged += new System.EventHandler(this.OptBus_CheckedChanged);
			// 
			// optGraph
			// 
			this.optGraph.Location = new System.Drawing.Point(24, 72);
			this.optGraph.Name = "optGraph";
			this.optGraph.Size = new System.Drawing.Size(144, 24);
			this.optGraph.TabIndex = 1;
			this.optGraph.Text = "RI_GM_GRAPHICS";
			this.optGraph.CheckedChanged += new System.EventHandler(this.optGraph_CheckedChanged);
			// 
			// optImage
			// 
			this.optImage.Location = new System.Drawing.Point(24, 24);
			this.optImage.Name = "optImage";
			this.optImage.Size = new System.Drawing.Size(152, 32);
			this.optImage.TabIndex = 0;
			this.optImage.Text = "RI_GM_IMAGES";
			this.optImage.CheckedChanged += new System.EventHandler(this.optImage_CheckedChanged);
			// 
			// print
			// 
			this.print.AddCR = true;
			this.print.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop;
			this.print.AutoMargin = true;
			this.print.BackColor = System.Drawing.Color.White;
			this.print.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent;
			this.print.BMargin = 316.8F;
			this.print.CurrentX = 360F;
			this.print.CurrentY = 316.8F;
			this.print.DocName = "PrintPRO Document";
			this.print.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen;
			this.print.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid;
			this.print.DrawWidth = 1;
			this.print.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic;
			this.print.FillColor = System.Drawing.Color.Black;
			this.print.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent;
			this.print.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.print.ForeColor = System.Drawing.Color.Black;
			this.print.Lmargin = 360F;
			this.print.OutputFileName = "";
			this.print.OwnDIB = false;
			this.print.Picture = null;
			this.print.PictureTransparent = false;
			this.print.PictureTransparentColor = System.Drawing.Color.Black;
			this.print.PrinterDriverName = "winspool";
			this.print.PrinterName = "Dell Laser Printer 1700n";
			this.print.PrinterPortName = "Ne06:";
			this.print.PrintPreviewOnly = false;
			this.print.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
			this.print.PrintToFile = false;
			this.print.RequestedDPI = 200;
			this.print.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4;
			this.print.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip;
			this.print.SN = "PEXHJ700AA-PEG0Q000195";
			this.print.TMargin = 316.8F;
			this.print.UseDefaultPrinter = true;
			this.print.WordWrap = true;
			// 
			// chkproof
			// 
			this.chkproof.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.chkproof.Location = new System.Drawing.Point(400, 496);
			this.chkproof.Name = "chkproof";
			this.chkproof.Size = new System.Drawing.Size(136, 32);
			this.chkproof.TabIndex = 3;
			this.chkproof.Text = "Enable/Disable Proof";
			this.chkproof.CheckedChanged += new System.EventHandler(this.chkproof_CheckedChanged);
			// 
			// cmdpreview
			// 
			this.cmdpreview.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdpreview.Location = new System.Drawing.Point(552, 488);
			this.cmdpreview.Name = "cmdpreview";
			this.cmdpreview.Size = new System.Drawing.Size(136, 40);
			this.cmdpreview.TabIndex = 4;
			this.cmdpreview.Text = "Preview the Image";
			this.cmdpreview.Click += new System.EventHandler(this.button1_Click);
			// 
			// imageXView2
			// 
			this.imageXView2.Location = new System.Drawing.Point(344, 464);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(56, 32);
			this.imageXView2.TabIndex = 7;
			this.imageXView2.Visible = false;
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Using the IMCProfile capabilities of the PrintPRO Photo Edition control with th" +
														 "e resulting image being displayed ",
														 "   using the Print Preview functionality in PrintPRO.",
														 "",
														 "Instructions:",
														 "1)Choose the appropriate Load Profile option from the pull-down menu. The Printer" +
														 " Profile is used if the default",
														 "    Printer Profile is not desired or if you want to proof the image page against" +
														 " another printer profile. This requires ",
														 "    that the Monitor Profile be set or the monitor have a profile already associa" +
														 "ted with it. This sample has the default",
														 "    printer profile loaded and the PrintPreview is enabled.",
														 "2)The Enable/Disable proofing can be used to disable the proofing functionality r" +
														 "espectively.",
														 "3)Choose the Preview the Image button to see a print preview of the color matched" +
														 " image."});
			this.lstInfo.Location = new System.Drawing.Point(8, 24);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(688, 160);
			this.lstInfo.TabIndex = 8;
			// 
			// imageXView1
			// 
			this.imageXView1.Location = new System.Drawing.Point(24, 216);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(368, 312);
			this.imageXView1.TabIndex = 9;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(712, 545);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.imageXView1,
																		  this.lstInfo,
																		  this.imageXView2,
																		  this.cmdpreview,
																		  this.chkproof,
																		  this.renderintent});
			this.Menu = this.menu;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ICM Profiles";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.renderintent.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
			 *     Pegasus Imaging Corporation Standard Function Definitions     *
			 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		
		private System.String strCurrentDir = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";

		string GetFileName(System.String FullName) 
		{
			return (FullName.Substring(FullName.LastIndexOf("\\")+1,FullName.Length - FullName.LastIndexOf("\\") - 1));
		}
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}
	
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);
			
			imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			System.String strCurrentDir = System.IO.Directory.GetCurrentDirectory ();
			System.String strImageFile = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\window.jpg");
			
			imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile);

					
			print.hDIB = (System.Int32)(imageXView2.Image.ToHdib(true));         
            chkproof.Checked = true;
			gStrSavedICMMonitorProfile = print.ICMMonitorProfileName;
			gStrSavedICMPrinterrProfile = print.ICMPrinterProfileName;
			print.ICMIntent = PegasusImaging.WinForms.PrintPro3.peICMIntent.ICMI_Contrast;
			optImage.Checked = true;
			button1_Click(sender,e);
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog cd = new OpenFileDialog();
			String strprofile;
			String lpctstrProfileDirectory = "\\spool\\drivers\\color\\";
			String strProfileDirectory = System.Environment.SystemDirectory + lpctstrProfileDirectory ;
			cd.InitialDirectory = strProfileDirectory;
			cd.Filter = "Image Files(*.ICC;*.ICM;)|*.ICC;*.ICM;|All files (*.*)|*.*";
			cd.ShowDialog();
			
			strprofile = cd.FileName;
			print.ICMMonitorProfileName = strprofile; 

			chkproof.Enabled = true;
			print.ICMWantProof = true;
		}

		private void optGraph_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optGraph.Checked == true)
			{
				print.ICMProfileRenderIntent = PegasusImaging.WinForms.PrintPro3.peEnumIntentMode.RI_GM_GRAPHICS;
			}
		}

		
		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog cd = new OpenFileDialog();
			String strprofile;
			String lpctstrProfileDirectory = "\\spool\\drivers\\color\\";
			String strProfileDirectory = System.Environment.SystemDirectory + lpctstrProfileDirectory ;
			cd.InitialDirectory = strProfileDirectory;
			cd.Filter = "Image Files(*.ICC;*.ICM;)|*.ICC;*.ICM;|All files (*.*)|*.*";
			cd.ShowDialog();
			strprofile = cd.FileName;
			print.ICMPrinterProfileName = strprofile; 
			chkproof.Enabled = true;
			print.ICMWantProof = true;
		}

		private void menuItem5_Click(object sender, System.EventArgs e)
		{
						
			try
			{
				imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(PegasusOpenFile());
			}

			catch(PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX)
			{
				if(eX.Result != 0)
				{
					MessageBox.Show("An error occurred opening the file");
				}
			}

			print.hDIB = (System.Int32)(imageXView2.Image.ToHdib(true));
			chkproof.Checked = true; 
			
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			    print.PrintPreviewOnly = true;
				print.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
				print.StartPrintPreview();
				float a;
			    float b;
				print.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Pixel;
				a = print.ScaleWidth - print.Lmargin;
				b = print.ScaleHeight - print.TMargin - print.BMargin;
				print.PrintDIB(print.Lmargin, print.TMargin, a, b, 0, 0, 0, 0, true);
				print.EndPrintPreview();
				print.GetPrintPreview(1);
								
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(new System.IntPtr(print.PrintPreviewDIB));

		}

		private void chkproof_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkproof.Checked == true)
			{
				print.ICMWantProof = false;

			}
			else
			{
				print.ICMWantProof = true;
				
			}
		}

		private void optImage_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optImage.Checked == true)
			{
				print.ICMProfileRenderIntent = PegasusImaging.WinForms.PrintPro3.peEnumIntentMode.RI_GM_IMAGES;
			}
		}

		private void OptBus_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optbus.Checked == true)
			{
				print.ICMProfileRenderIntent = PegasusImaging.WinForms.PrintPro3.peEnumIntentMode.RI_GM_BUSINESS;
			}
		}

		private void OptMetric_CheckedChanged(object sender, System.EventArgs e)
		{
			if(OptMetric.Checked == true)
			{
				print.ICMProfileRenderIntent = PegasusImaging.WinForms.PrintPro3.peEnumIntentMode.RI_GM_ABS_COLORIMETRIC;
			}
		}

		private void OptNone_CheckedChanged(object sender, System.EventArgs e)
		{
			if(OptNone.Checked == true)
			{
				print.ICMProfileRenderIntent = PegasusImaging.WinForms.PrintPro3.peEnumIntentMode.RI_GM_NONE;
			}
		}

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
		
			 Application.Exit();
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			print.AboutBox();
		}

		

		

		
	}
}

//****************************************************************
//* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *
//* This sample code is provided to Pegasus licensees "as is"    *
//* with no restrictions on use or modification. No warranty for *
//* use of this sample code is provided by Pegasus.              *
//****************************************************************

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.PrintPro3;

namespace GridCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonPrint;
		private PegasusImaging.WinForms.PrintPro3.PICPrintPro picPrintPro2;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		
	
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			UnlockPICPrintPro.PS_Unlock (12345, 12345, 12345, 12345);
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			picPrintPro2.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonPrint = new System.Windows.Forms.Button();
			this.picPrintPro2 = new PegasusImaging.WinForms.PrintPro3.PICPrintPro();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// buttonPrint
			// 
			this.buttonPrint.Location = new System.Drawing.Point(120, 200);
			this.buttonPrint.Name = "buttonPrint";
			this.buttonPrint.Size = new System.Drawing.Size(88, 23);
			this.buttonPrint.TabIndex = 5;
			this.buttonPrint.Text = "Print";
			this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
			// 
			// picPrintPro2
			// 
			this.picPrintPro2.AddCR = true;
			this.picPrintPro2.Alignment = PegasusImaging.WinForms.PrintPro3.peAlignment.ALIGN_LeftJustifyTop;
			this.picPrintPro2.AutoMargin = true;
			this.picPrintPro2.BackColor = System.Drawing.Color.White;
			this.picPrintPro2.BackStyle = PegasusImaging.WinForms.PrintPro3.peBackStyle.BACK_Transparent;
			this.picPrintPro2.BMargin = 249.6F;
			this.picPrintPro2.CurrentX = 249.6F;
			this.picPrintPro2.CurrentY = 249.6F;
			this.picPrintPro2.DocName = "PrintPRO Document";
			this.picPrintPro2.DrawMode = PegasusImaging.WinForms.PrintPro3.peDrawMode.PEN_CopyPen;
			this.picPrintPro2.DrawStyle = PegasusImaging.WinForms.PrintPro3.peDrawStyle.STYLE_Solid;
			this.picPrintPro2.DrawWidth = 1;
			this.picPrintPro2.EvalMode = PegasusImaging.WinForms.PrintPro3.peEvaluationMode.EVAL_Automatic;
			this.picPrintPro2.FillColor = System.Drawing.Color.Black;
			this.picPrintPro2.FillStyle = PegasusImaging.WinForms.PrintPro3.peFillStyle.FILL_Transparent;
			this.picPrintPro2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picPrintPro2.ForeColor = System.Drawing.Color.Black;
			this.picPrintPro2.Lmargin = 249.6F;
			this.picPrintPro2.OutputFileName = "";
			this.picPrintPro2.OwnDIB = false;
			this.picPrintPro2.Picture = null;
			this.picPrintPro2.PictureTransparent = false;
			this.picPrintPro2.PictureTransparentColor = System.Drawing.Color.Black;
			this.picPrintPro2.PrinterDriverName = "winspool";
			this.picPrintPro2.PrinterName = "Dell Laser Printer 1700n PS3";
			this.picPrintPro2.PrinterPortName = "Ne05:";
			this.picPrintPro2.PrintPreviewOnly = false;
			this.picPrintPro2.PrintPreviewScale = PegasusImaging.WinForms.PrintPro3.pePPScaleMode.PPSCALE_1_1;
			this.picPrintPro2.PrintToFile = false;
			this.picPrintPro2.RequestedDPI = 200;
			this.picPrintPro2.SaveFileType = PegasusImaging.WinForms.PrintPro3.peSaveFileType.PP_TIF_G4;
			this.picPrintPro2.ScaleMode = PegasusImaging.WinForms.PrintPro3.peScaleMode.SCALE_Twip;
			this.picPrintPro2.SN = "PEXHJ700AA-PEG0Q000195";
			this.picPrintPro2.TMargin = 249.6F;
			this.picPrintPro2.UseDefaultPrinter = true;
			this.picPrintPro2.WordWrap = true;
			// 
			// lstInfo
			// 
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality: ",
														 "1)How to print a 50 by 50 grid inside the display area ",
														 "   of the printer."});
			this.lstInfo.Location = new System.Drawing.Point(8, 24);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(320, 69);
			this.lstInfo.TabIndex = 6;
			this.lstInfo.SelectedIndexChanged += new System.EventHandler(this.lstInfo_SelectedIndexChanged);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(344, 261);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstInfo,
																		  this.buttonPrint});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "PrintPRO Grid Example";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void buttonExit_Click(object sender, System.EventArgs e)
		{
			System.Environment.Exit (0);
		}

		private void buttonPrint_Click(object sender, System.EventArgs e)
		{
			// select a printer
			picPrintPro2.PrintDialog ();

			// Draw a border around the display area
			picPrintPro2.DrawLine( picPrintPro2.DisplayX, picPrintPro2.DisplayY, picPrintPro2.ScaleWidth + picPrintPro2.DisplayX, picPrintPro2.ScaleHeight + picPrintPro2.DisplayY, System.Drawing.Color.Black, true, false);

			// Draw the vertical lines
			float fDelta = picPrintPro2.ScaleWidth / 50;
			for (int i=1; i<= 49; i++)
			{
				picPrintPro2.DrawLine (picPrintPro2.DisplayX + fDelta * i, picPrintPro2.DisplayY, picPrintPro2.DisplayX + fDelta * i, picPrintPro2.DisplayY + picPrintPro2.ScaleHeight, System.Drawing.Color.Black, false, false);
			}
			
			// Draw the horizontal lines
			fDelta = picPrintPro2.ScaleHeight / 50;
			for (int i=1; i<= 49; i++)
			{
				picPrintPro2.DrawLine (picPrintPro2.DisplayX, picPrintPro2.DisplayY + fDelta * i, picPrintPro2.DisplayX + picPrintPro2.ScaleWidth, picPrintPro2.DisplayY + fDelta * i, System.Drawing.Color.Black, false, false);
			}

			// Print a message in the center of the page
			System.Drawing.Font font = new System.Drawing.Font ("Times New Roman", 72, System.Drawing.FontStyle.Bold);
			picPrintPro2.Font = font;
			picPrintPro2.Alignment = peAlignment.ALIGN_CenterJustifyMiddle;
			picPrintPro2.PrintTextAligned ("PrintPRO 50x50 Grid Example", picPrintPro2.DisplayX, picPrintPro2.DisplayY, picPrintPro2.DisplayX + picPrintPro2.ScaleWidth, picPrintPro2.DisplayY + picPrintPro2.ScaleHeight);
	
			// Print the document
			picPrintPro2.EndPrintDoc ();
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
		
		}

	   private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
			 System.Environment.Exit(0);
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			picPrintPro2.AboutBox();
		}

		private void lstInfo_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}

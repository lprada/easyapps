
// PrintPRO COM VC++ Initialization routines.
// Copyright (c) 2000 -2003 Pegasus Imaging Corp.

// ----------------------------------------------------------------------------------
// Queries the system registry to get the path for registered version of PrntPRO3.DLL
// Performs LoadLibrary on that DLL.
// ----------------------------------------------------------------------------------
HINSTANCE LoadLibraryPrntPRO3DLL()
{
  HANDLE  hKey;
  long    len=255;
  long    result;
  char    buf[256];
  memset(&buf, 0x00, sizeof(buf));

  // Query the system registry for the PrntPRO3.dll path
#ifdef __BORLANDC__
  result = RegOpenKey(HKEY_LOCAL_MACHINE, "Software\\Classes\\CLSID\\{66157B4F-9E4A-488C-92A4-4434A16FCBF2}", &hKey);
#else
  result = RegOpenKey(HKEY_LOCAL_MACHINE, "Software\\Classes\\CLSID\\{66157B4F-9E4A-488C-92A4-4434A16FCBF2}", (PHKEY)&hKey);
#endif
  if (result==ERROR_SUCCESS)
  {
    result = RegQueryValue((HKEY)hKey, "InprocServer32\0", buf, &len);
    RegCloseKey((HKEY)hKey);
  }

  // If we found the path from the registry; LoadLibrary from that path
  // Otherwise attempt to load it from the system path.
  if (result==ERROR_SUCCESS)
    return LoadLibrary(buf);  
  else
    return LoadLibrary("PrintPRO3.dll");
}

typedef HRESULT (WINAPI* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE PP_Open()
{
  HINSTANCE    hDLL;            // Handle to PrintPRO COM Component
  LPFNDLL_PSU  lpfnDllFunc1;    // Function pointer to the PS_Unlock function
  HRESULT      hr;

  hDLL = LoadLibraryPrntPRO3DLL();   // Registered PrintPRO ATL COM control
  if (hDLL)
  {
    // Get the address of the PS_Unlock function.  
    lpfnDllFunc1 = (LPFNDLL_PSU)GetProcAddress(hDLL, "PS_Unlock");

    if (lpfnDllFunc1)
      // Call the unlock function with the integrator key
      // that you received from Pegasus Software.
      // NOTE: The integrator key shown below is for
      // illustration purposes only.
      hr = lpfnDllFunc1(123456789, 123456789, 123456789, 123456789);
    else
    {
      FreeLibrary(hDLL);
      hDLL = NULL;
    }
  }
  return hDLL;
}

void PP_Close(HINSTANCE hDLL)
{
  if (hDLL)
    FreeLibrary(hDLL);
}

unit PrintSample;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, AxCtrls, OleCtrls, Menus,
  PrintPRO3_TLB;

type
  TForm1 = class(TForm)
    Button1: TButton;
    GroupBox1: TGroupBox;
    img: TImage;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    MainMenu1: TMainMenu;
    cd: TOpenDialog;
    N1: TMenuItem;
    OpenBitmaptoprint1: TMenuItem;
    Quit1: TMenuItem;
    About1: TMenuItem;
    ListBox1: TListBox;
    PrintPRO1: TPrintPRO;
    procedure Button1Click(Sender: TObject);

    procedure FormCreate(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure OpenBitmaptoprint1Click(Sender: TObject);
    procedure Quit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
   
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
    bPrintToTiff : Boolean;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
        prWidth: Integer;
        
begin
begin
  PrintPRO1.Tag:= PrintPRO1.PrintError;    // Clear PrintError
  If PrintPRO1.PrintError = 0 Then
  begin
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
     If bPrintToTiff = True Then
     begin
        PrintPRO1.DocName := 'Pegasus PrintPRO 3 Demo';
        PrintPRO1.OutputFileName := ExtractFilePath(Application.ExeName) + '\PrintToTiff.tif';
        PrintPRO1.PrintToFile := True;
        PrintPRO1.SaveFileType := PP_TIF;
        PrintPRO1.RequestedDPI := 100 ;
     end
    else if bPrintToTiff = False then


    PrintPRO1.DocName := 'Pegasus PrintPRO 3 Demo';
    // Start a print job by selecting a printer and starting a
    // new page. If you don't select a printer or start a new
    // page, PrintPRO will select the default printer and start
    // a new page when you first print text, graphics or a bitmap.
    PrintPRO1.SelectDefaultPrinter;
    PrintPRO1.StartPrintDoc;
    //Print a banner at the top of the page, white on black.
    PrintPRO1.BackStyle := 1;  //Opaque
    PrintPRO1.ForeColor := RGB(255, 255, 255); //White text
    PrintPRO1.BackColor := RGB(0, 0, 0); //Black background
    PrintPRO1.Font.Name := 'Arial';
    PrintPRO1.Font.Style := [fsBold];
    PrintPRO1.Font.Size := 24;
    PrintPRO1.Alignment := ALIGN_CenterJustifyMiddle; // Center justify the text
    prWidth := Trunc(PrintPRO1.PWidth);
    PrintPRO1.PrintTextAligned('Hello! Check out PrintPRO 3.0  ', 1000, 360, prWidth - 1000, 1100);

    // The following code demonstrates how PrintPRO can print text
    // aligned inside a rectangular area.  First, a box is drawn
    // then text is drawn aligned inside the box.
    PrintPRO1.BackStyle := 0; //Transparent background
    PrintPRO1.ForeColor := RGB(0, 0, 0); //Black text
    PrintPRO1.Font.Size := 8;
    PrintPRO1.Alignment := ALIGN_LeftJustifyTop;
    PrintPRO1.DrawLine(1440, 1440, 2880, 2880, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - left justified top', 1440, 1440, 2880, 2880);

    PrintPRO1.Alignment := ALIGN_LeftJustifyMiddle;
    PrintPRO1.DrawLine(3100, 1440, 4540, 2880, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - left justified middle', 3100, 1440, 4540, 2880);

    PrintPRO1.Alignment := ALIGN_LeftJustifyBottom;
    PrintPRO1.DrawLine(4760, 1440, 6200, 2880, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - left justified bottom', 4760, 1440, 6200, 2880);

    PrintPRO1.Alignment := ALIGN_RightJustifyTop;
    PrintPRO1.DrawLine(6420, 1440, 7860, 2880, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - right justified top', 6420, 1440, 7860, 2880);

    PrintPRO1.Alignment := ALIGN_RightJustifyMiddle;
    PrintPRO1.DrawLine(1440, 3100, 2880, 4540, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - right justified middle', 1440, 3100, 2880, 4540);

    PrintPRO1.Alignment := ALIGN_RightJustifyBottom;
    PrintPRO1.DrawLine(3100, 3100, 4540, 4540, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - right justified bottom', 3100, 3100, 4540, 4540);

    PrintPRO1.Alignment := ALIGN_CenterJustifyTop;
    PrintPRO1.DrawLine(4760, 3100, 6200, 4540, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - center justified top', 4760, 3100, 6200, 4540);

    PrintPRO1.Alignment := ALIGN_CenterJustifyMiddle;
    PrintPRO1.DrawLine(6420, 3100, 7860, 4540, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - center justified middle', 6420, 3100, 7860, 4540);

    PrintPRO1.Alignment := ALIGN_CenterJustifyBottom;
    PrintPRO1.DrawLine(1440, 4760, 7860, 5200, 0, True, False);
    PrintPRO1.PrintTextAligned('This is an experiment to see if the printer supports formatted text - center justified bottom', 1440, 4760, 7860, 5200);

    PrintPRO1.Picture := img.Picture;
    PrintPRO1.PrintPicture(1640, 5700, 5000, 4000, 0, 0,320, 240, False);

    //sTemp :=  IntToStr(PrintPRO1.PrintError);

    //ShowMessage(sTemp);

    // Use graphic methods to draw an arrow
    PrintPRO1.FillStyle := FILL_Solid;
    PrintPRO1.DrawWidth := 15;
    PrintPRO1.DrawPolygon(8390, 7575, 6770, 7575, 6870, 7475, 6870, 7675, 6770, 7575, 0, 0, 0, 5);

    // Draw text inside a gray box with a black border
    PrintPRO1.FillStyle := FILL_Transparent;
    PrintPRO1.Alignment := ALIGN_CenterJustifyMiddle;
    // Draw the gray box
    PrintPRO1.DrawLine(8550, 5700, 10550, 9450, RGB(225, 225, 225), True, True);
    // Put a black border on the box
    PrintPRO1.DrawLine(8550, 5700, 10550, 9450, 0, True, False);
    // Print text inside the box
    PrintPRO1.Font.Name := 'Times New Roman';
    PrintPRO1.Font.Size := 18;
    PrintPRO1.BackStyle := 0;
    PrintPRO1.PrintTextAligned('Print' + Chr(13) + 'Graphics' + Chr(13) + 'and' + Chr(13) + 'Bitmaps' + Chr(13) + 'and' + Chr(13) + 'Text', 8550, 5500, 10550, 9250);

    // Print the features
    PrintPRO1.Font.Name := 'Arial';
    PrintPRO1.Font.Size := 24;
    PrintPRO1.Font.Style := [fsBold,fsUnderline];

    PrintPRO1.LMargin := 1440;
    PrintPRO1.CurrentX := 1440;
    PrintPRO1.CurrentY := 10000;
    PrintPRO1.PrintText('Plus!');
    PrintPRO1.Font.Style := [fsBold];
    PrintPRO1.PrintText('Lines, rectangles, circles, ellipse, arc,');
    PrintPRO1.PrintText('curves, polygons, pie, rounded rectangles,');
    PrintPRO1.PrintText('Print Dialog, and much, much more!');
    PrintPRO1.PrintText('PrintPRO is an ATL control so you can use');
    PrintPRO1.PrintText('it with your favorite dev. tool including IE.' + Chr(13));
    PrintPRO1.PrintText('The PrintPRO Team' + Chr(13));
    PrintPRO1.Font.Size := 8;
    PrintPRO1.PrintText('This page was created in Delphi 5 using the Pegasus Software PrintPRO VCL control.');
    // End the print job.  This will end the current page and print the document
    PrintPRO1.EndPrintDoc;
    Screen.Cursor := crDefault;

end;
end;

end;


procedure TForm1.FormCreate(Sender: TObject);
begin

        bPrintToTiff := false;
        img.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + '..\..\..\..\..\..\Common\Images\pic1.bmp');
        RadioButton1.Checked := True;
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
         If RadioButton1.Checked = True Then bPrintToTiff := True
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
         If RadioButton2.Checked = True Then bPrintToTiff := False
end;

procedure TForm1.OpenBitmaptoprint1Click(Sender: TObject);
begin
       cd.Filter :=   'Windows Bitmap (*.BMP) | *.bmp';
       cd.InitialDir := '..\..\..\..\..\..\Common\Images\';
       cd.Execute;
       img.Picture.LoadFromFile(cd.FileName);


       
end;

procedure TForm1.Quit1Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.About1Click(Sender: TObject);
begin

PrintPRO1.AboutBox;

end;

end.

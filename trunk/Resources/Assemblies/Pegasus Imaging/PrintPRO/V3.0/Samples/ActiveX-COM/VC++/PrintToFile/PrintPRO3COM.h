// PrintPRO3COM.h : main header file for the PRINTPRO3COM application
//

#if !defined(AFX_PRINTPRO3COM_H__63CBD6F3_3503_412F_86F7_A8CD9BEFDF91__INCLUDED_)
#define AFX_PRINTPRO3COM_H__63CBD6F3_3503_412F_86F7_A8CD9BEFDF91__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CPrintPRO3COMApp:
// See PrintPRO3COM.cpp for the implementation of this class
//

class CPrintPRO3COMApp : public CWinApp
{
public:
	CPrintPRO3COMApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintPRO3COMApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPrintPRO3COMApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTPRO3COM_H__63CBD6F3_3503_412F_86F7_A8CD9BEFDF91__INCLUDED_)

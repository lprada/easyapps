VERSION 5.00
Object = "{89F92CAF-B12C-4D3B-B92B-F75DDAB357B6}#1.0#0"; "PrintPRO3.dll"
Begin VB.Form Grid 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "PrintPRO Grid Example"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   8445
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   8445
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      ItemData        =   "grid.frx":0000
      Left            =   240
      List            =   "grid.frx":0016
      TabIndex        =   4
      Top             =   120
      Width           =   7935
   End
   Begin VB.Frame Frame1 
      Caption         =   "PrintToFile"
      Height          =   1575
      Left            =   240
      TabIndex        =   1
      Top             =   2280
      Width           =   2415
      Begin VB.OptionButton Option2 
         Caption         =   "Disable PrintToFile"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   960
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Enable PrintToFile"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Print Grid"
      Height          =   495
      Left            =   3240
      TabIndex        =   0
      Top             =   3120
      Width           =   1335
   End
   Begin PrintPRO3Ctl.PrintPRO pp 
      Left            =   5400
      Top             =   2880
      _ExtentX        =   953
      _ExtentY        =   794
      ErrStr          =   "ABD6FBDDE9408B2C3AC98C2E7273A90A"
      ErrCode         =   950154521
      ErrInfo         =   -496424546
      _cx             =   953
      _cy             =   794
      AddCR           =   -1  'True
      Alignment       =   0
      BackColor       =   16777215
      BackStyle       =   0
      BMargin         =   249.6
      CurrentX        =   249.6
      CurrentY        =   249.6
      DocName         =   "PrintPRO Document"
      DrawMode        =   13
      DrawStyle       =   1
      DrawWidth       =   1
      FillColor       =   0
      FillStyle       =   1
      ForeColor       =   0
      LMargin         =   249.6
      ScaleMode       =   1
      TMargin         =   249.6
      WordWrap        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoMargin      =   -1  'True
      OutputFileName  =   ""
      PicTransparent  =   0   'False
      PicTransparentColor=   0
      UseDefaultPrinter=   -1  'True
      PrintPreviewScale=   1
      PrintPreviewOnly=   0   'False
      OwnDIB          =   0   'False
      PrintToFile     =   0   'False
      SaveFileType    =   4
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Grid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim bPrintTiff As Boolean
Dim MaxDPI As Long

Private Sub Command1_Click()
  Dim Delta As Single
  Dim i As Integer
  
  If (bPrintTiff = True) Then
      pp.OutputFileName = App.Path & "\grid.tif"
      pp.PrintToFile = True
  Else
      pp.OutputFileName = ""
      pp.PrintToFile = False

  End If
  pp.StartPrintDoc
  
  ' Select a printer
  pp.PrintDialog
  
  ' Draw a border around the display area
  pp.DrawLine pp.DisplayX, pp.DisplayY, pp.ScaleWidth + pp.DisplayX, pp.ScaleHeight + pp.DisplayY, 0, True, False

  ' Draw the vertical lines
  Delta = pp.ScaleWidth / 50

  For i = 1 To 49
    pp.DrawLine pp.DisplayX + Delta * i, pp.DisplayY, pp.DisplayX + Delta * i, pp.DisplayY + pp.ScaleHeight, 0, False, False
  Next i

  ' Draw the horizontal lines
  Delta = pp.ScaleHeight / 50

  For i = 1 To 49
    pp.DrawLine pp.DisplayX, pp.DisplayY + Delta * i, pp.DisplayX + pp.ScaleWidth, pp.DisplayY + Delta * i, 0, False, False
  Next i

  ' Print a message in the center of the page
  pp.Alignment = ALIGN_CenterJustifyMiddle
  pp.Font.Name = "Times New Roman"
  pp.Font.Size = 72
  pp.Font.Bold = True
  pp.PrintTextAligned "PrintPRO 50x50 Grid Example", pp.DisplayX, pp.DisplayY, pp.DisplayX + pp.ScaleWidth, pp.DisplayY + pp.ScaleHeight

  ' Print the document
  pp.EndPrintDoc
End Sub

Private Sub Command2_Click()
  End
End Sub

Private Sub Command3_Click()
    bPrintTiff = True
End Sub

Private Sub Command4_Click()
    bPrintTiff = False
End Sub

Private Sub Form_Load()

    Option2.Value = True
    bPrintTiff = False
    pp.PrintToFile = False
    pp.OutputFileName = ""
    MaxDPI = pp.GetMaxIDP
    If (MaxDPI > 600) Then
        pp.RequestedDPI = 600
    Else
        pp.RequestedDPI = MaxDPI
    End If
End Sub

Private Sub Option1_Click()
      If Option1.Value = True Then
        bPrintTiff = True
    Else
        bPrintTiff = False
    End If
End Sub

Private Sub Option2_Click()
      If Option1.Value = True Then
        bPrintTiff = True
    Else
        bPrintTiff = False
    End If
End Sub
Private Sub mnuFileQuit_Click()
    End
End Sub
Private Sub mnuAbout_Click()
    pp.AboutBox

End Sub

VERSION 5.00
Object = "{89F92CAF-B12C-4D3B-B92B-F75DDAB357B6}#1.0#0"; "PrintPRO3.dll"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form fmrProfiles 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ICM Profiles"
   ClientHeight    =   8175
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   12405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8175
   ScaleWidth      =   12405
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      ItemData        =   "fmrProfiles.frx":0000
      Left            =   240
      List            =   "fmrProfiles.frx":0022
      TabIndex        =   10
      Top             =   240
      Width           =   11895
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   615
      Left            =   5880
      TabIndex        =   9
      Top             =   7200
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   1085
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   748773455
      ErrInfo         =   2021196726
      Persistence     =   -1  'True
      _cx             =   2566
      _cy             =   1085
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4935
      Left            =   240
      TabIndex        =   8
      Top             =   2640
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   8705
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   748773455
      ErrInfo         =   2021196726
      Persistence     =   -1  'True
      _cx             =   9340
      _cy             =   8705
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Frame fmrICMMethod 
      Caption         =   "ICM Render Intent"
      Height          =   3375
      Left            =   7920
      TabIndex        =   2
      Top             =   2640
      Width           =   2895
      Begin VB.OptionButton optNone 
         Caption         =   "RI_GM_NONE"
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   2760
         Width           =   2535
      End
      Begin VB.OptionButton optMetric 
         Caption         =   "RI_GM_ABS_COLORIMETRIC "
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   2160
         Width           =   2535
      End
      Begin VB.OptionButton optBus 
         Caption         =   "RI_GM_BUSINESS"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   1680
         Width           =   2175
      End
      Begin VB.OptionButton optGraph 
         Caption         =   "RI_GM_GRAPHICS"
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   1935
      End
      Begin VB.OptionButton optImage 
         Caption         =   "RI_GM_IMAGES"
         Height          =   495
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   2055
      End
   End
   Begin VB.CheckBox chkProof 
      Caption         =   "Enable/Disable Proof"
      Height          =   495
      Left            =   8400
      TabIndex        =   1
      Top             =   6360
      Width           =   2055
   End
   Begin VB.CommandButton cmdPreview 
      Caption         =   "Preview the Image"
      Height          =   495
      Left            =   8520
      TabIndex        =   0
      Top             =   7080
      Width           =   1935
   End
   Begin PrintPRO3Ctl.PrintPRO pp3 
      Left            =   10680
      Top             =   7440
      _ExtentX        =   953
      _ExtentY        =   794
      ErrStr          =   "ABD6FBDDE9408B2C3AC98C2E7273A90A"
      ErrCode         =   748773455
      ErrInfo         =   -1556242284
      _cx             =   953
      _cy             =   794
      AddCR           =   -1  'True
      Alignment       =   0
      BackColor       =   16777215
      BackStyle       =   0
      BMargin         =   249.6
      CurrentX        =   249.6
      CurrentY        =   249.6
      DocName         =   "PrintPRO Document"
      DrawMode        =   13
      DrawStyle       =   1
      DrawWidth       =   1
      FillColor       =   0
      FillStyle       =   1
      ForeColor       =   0
      LMargin         =   249.6
      ScaleMode       =   1
      TMargin         =   249.6
      WordWrap        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoMargin      =   -1  'True
      OutputFileName  =   ""
      PicTransparent  =   0   'False
      PicTransparentColor=   0
      UseDefaultPrinter=   -1  'True
      PrintPreviewScale=   1
      PrintPreviewOnly=   0   'False
      OwnDIB          =   0   'False
      PrintToFile     =   0   'False
      SaveFileType    =   4
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuMonitor 
         Caption         =   "Load Monitor Profile"
      End
      Begin VB.Menu mnuPrinter 
         Caption         =   "Load Printer Profile"
      End
      Begin VB.Menu mnuOpenImage 
         Caption         =   "Open Image"
      End
      Begin VB.Menu mnuSpace 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "fmrProfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Dim renderint As Long
Dim txt As String
Public gStrSavedICMMonitorProfile As String
Public gStrSavedICMPrinterrProfile As String
Public geSavedRenderIntent As peICMIntent
Private Sub chkProof_Click()
    If chkProof.Value = 1 Then
        pp3.ICMWantProof = False
    Else
        pp3.ICMWantProof = True
    End If
End Sub

Private Sub cmdPreview_Click()
 If pp3.PrintError = 0 Then
    pp3.PrintPreviewOnly = True
    pp3.PrintPreviewScale = PPSCALE_1_1
    pp3.StartPrintPreview
    If pp3.PrintError <> 0 Then MsgBox "PrintPRO error: " & pp3.PrintError, vbOKCancel, "PrintPRO Demo"
        Dim a As Integer
        Dim b As Integer
        pp3.ScaleMode = SCALE_Pixel
         a = pp3.ScaleWidth - pp3.Lmargin
         b = pp3.ScaleHeight - pp3.TMargin - pp3.BMargin
         pp3.PrintDIB pp3.Lmargin, pp3.TMargin, a, b, 0, 0, 0, 0, True
    pp3.EndPrintPreview
    If pp3.PrintError <> 0 Then MsgBox "PrintPRO error: " & pp3.PrintError, vbOKCancel, "PrintPRO Demo"
    pp3.GetPrintPreview 1
    If pp3.PrintError <> 0 Then MsgBox "PrintPRO error: " & pp3.PrintError, vbOKCancel, "PrintPRO Demo"
    'pass the resulting image here as the preview
     ImagXpress1.hDIB = pp3.PrintPreviewDIB

    End If
End Sub
Private Sub LoadFile()
      
      ImagXpress2.FileName = imgFileName
                       
      pp3.hDIB = ImagXpress2.CopyDIB
      chkProof.Value = 1
      gStrSavedICMMonitorProfile = pp3.ICMMonitorProfileName
      gStrSavedICMPrinterrProfile = pp3.ICMPrinterProfileName
      pp3.ICMIntent = ICMI_Contrast
      optImage.Value = True
      cmdPreview_Click
        
End Sub

Private Sub Form_Load()
      
        
      
      imgParentDir = App.Path
      imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\window.jpg"
      
      LoadFile
      
   End Sub

Private Sub mnuAbout_Click()
    pp3.AboutBox
End Sub

Private Sub mnuQuit_Click()
    End
End Sub

Private Sub mnuOpenImage_Click()
     
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
  
End Sub

Private Sub mnuMonitor_Click()
    Dim strProfile As String
    On Error GoTo ErrLoadPrinterProfile
    ' Display a file open dialog for profiles
    strProfile = GetICMProfile(Me.hWnd, "OPEN")
    If Len(strProfile) <> 0 Then
        '**load the monitor profile here
        pp3.ICMMonitorProfileName = strProfile
        chkProof.Enabled = True
         pp3.ICMWantProof = True
    End If
  '  updateProfileLbls
    Exit Sub
    
ErrLoadPrinterProfile:
End Sub

Private Sub mnuPrinter_Click()
     Dim strProfile As String
    On Error GoTo ErrLoadPrinterProfile

    ' Display a file open dialog for profiles
    strProfile = GetICMProfile(Me.hWnd, "OPEN")
    If Len(strProfile) <> 0 Then
        pp3.ICMPrinterProfileName = strProfile
       chkProof.Enabled = True
       pp3.ICMWantProof = True
    End If
   ' updateProfileLbls
    Exit Sub
    
ErrLoadPrinterProfile:
    
End Sub

Private Sub optBus_Click()
     If optBus.Value = True Then
        pp3.ICMProfileRenderIntent = RI_GM_BUSINESS
    End If
End Sub

Private Sub optGraph_Click()
     If optGraph.Value = True Then
        pp3.ICMProfileRenderIntent = RI_GM_GRAPHICS
    End If
End Sub

Private Sub optImage_Click()
    If optImage.Value = True Then
        pp3.ICMProfileRenderIntent = RI_GM_IMAGES
    End If
End Sub
Private Sub optMetric_Click()
       If optMetric.Value = True Then
        pp3.ICMProfileRenderIntent = RI_GM_ABS_COLORIMETRIC
    End If
End Sub

Private Sub optNone_Click()
    If optNone.Value = True Then
        pp3.ICMProfileRenderIntent = RI_GM_NONE
    End If
End Sub






object Form1: TForm1
  Left = 247
  Top = 139
  Width = 523
  Height = 510
  Caption = 'PrintPRO3'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object img: TImage
    Left = 32
    Top = 392
    Width = 105
    Height = 57
    Visible = False
  end
  object Button1: TButton
    Left = 208
    Top = 384
    Width = 105
    Height = 65
    Caption = 'Print Sample Page'
    TabOrder = 0
    OnClick = Button1Click
  end
  object GroupBox1: TGroupBox
    Left = 96
    Top = 152
    Width = 321
    Height = 185
    Caption = 'PrintToFile'
    TabOrder = 1
    object RadioButton1: TRadioButton
      Left = 32
      Top = 24
      Width = 177
      Height = 41
      Caption = 'Enable PrintToFile'
      TabOrder = 0
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 32
      Top = 80
      Width = 145
      Height = 49
      Caption = 'Disable PrintToFile'
      TabOrder = 1
      OnClick = RadioButton2Click
    end
  end
  object ListBox1: TListBox
    Left = 16
    Top = 16
    Width = 481
    Height = 65
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ItemHeight = 13
    Items.Strings = (
      'This sample demonstrates the following functionality:'
      
        '1)Printing to file or printer by setting the PrintToFile propert' +
        'y.')
    ParentFont = False
    TabOrder = 2
  end
  object PrintPRO1: TPrintPRO
    Left = 448
    Top = 184
    Width = 32
    Height = 30
    ControlData = {
      0800420000003800460038004500370042003800310035004400460044004200
      3100380035003500440030003800320034004100340044003300340033003200
      4500370031000000651E9E5AC305EA2110070000B90300001A0300000B000100
      0300000000001300FFFFFF000300000000000400000000000400000000000400
      000000000800240000005000720069006E007400500052004F00200044006F00
      630075006D0065006E007400000003000D000000030001000000030001000000
      1300000000000300010000001300000000000400000000000300010000000400
      000000000B00010009000352E30B918FCE119DE300AA004BB85101000000BC02
      444201000D4D532053616E7320536572696609000452E30B918FCE119DE300AA
      004BB8516C740000AC020000010000006C0000000000000000000000FFFFFFFF
      FFFFFFFF00000000000000007C9200003075000020454D4600000100AC020000
      1000000004000000000000000000000000000000000500000004000077010000
      2C010000000000000000000000000000D8B80500E09304001B00000010000000
      0000000000000000520000007001000001000000F5FFFFFF0000000000000000
      000000009001000000000001000000004D0053002000530061006E0073002000
      5300650072006900660000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000601A180040FD13001395917C
      5195917C00000000783A180000200000E40000001CC4B8014D00530020005300
      61006E0073002000530065007200690066000000E030887C000015004CFD1300
      AB37917CC737917C08061500080030016D05917C6D05917C8CFD1300C7FC807C
      CFFC807C00000000783A180000200000000030011CC4B80100000000783A1800
      58FD1300C066D20014000000A3330000A3330000C8AD0240B6C7004000040000
      01000000B4D5B8010A000000EB4FE68A0000000022529DCE24F5E26BD8FD1300
      D823917CEB4FE68AA70B00000EB967FAEB50D7C61A000000C066D2001C000000
      046FB8012843D20010FE13000D0000006476000800000000250000000C000000
      01000000180000000C00000000000000260000001C0000000200000000000000
      010000000000000000000000250000000C00000002000000140000000C000000
      0D00000027000000180000000300000000000000FFFFFF000000000025000000
      0C00000003000000190000000C000000FFFFFF00120000000C00000002000000
      250000000C00000007000080250000000C00000005000080250000000C000000
      0D0000800E000000140000000000000010000000140000000B00010008000200
      000000000B0000001300000000000B0001000300010000000B0000000B000000
      0B000000030004000000030000000000}
  end
  object MainMenu1: TMainMenu
    Left = 384
    Top = 88
    object N1: TMenuItem
      Caption = 'File'
      object OpenBitmaptoprint1: TMenuItem
        Caption = 'Open Bitmap to print...'
        OnClick = OpenBitmaptoprint1Click
      end
      object Quit1: TMenuItem
        Caption = 'Quit'
        OnClick = Quit1Click
      end
    end
    object About1: TMenuItem
      Caption = 'About'
      OnClick = About1Click
    end
  end
  object cd: TOpenDialog
    Left = 304
    Top = 88
  end
end

VERSION 5.00
Object = "{89F92CAF-B12C-4D3B-B92B-F75DDAB357B6}#1.0#0"; "PrintPRO3.dll"
Begin VB.Form frmPrintCap 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Printer Device Capabilities"
   ClientHeight    =   8430
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   11475
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8430
   ScaleWidth      =   11475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1230
      ItemData        =   "frmPrintCap.frx":0000
      Left            =   720
      List            =   "frmPrintCap.frx":000D
      TabIndex        =   13
      Top             =   240
      Width           =   9855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Get Printer Capabilities"
      Height          =   675
      Left            =   4320
      TabIndex        =   4
      Top             =   1680
      Width           =   2775
   End
   Begin VB.ListBox List4 
      Height          =   4350
      Left            =   8760
      TabIndex        =   3
      Top             =   3240
      Width           =   1815
   End
   Begin VB.ListBox List3 
      Height          =   4350
      Left            =   3960
      TabIndex        =   2
      Top             =   3240
      Width           =   3375
   End
   Begin VB.ListBox List2 
      Height          =   2010
      Left            =   840
      TabIndex        =   1
      Top             =   5640
      Width           =   2055
   End
   Begin VB.ListBox List1 
      Height          =   1815
      Left            =   840
      TabIndex        =   0
      Top             =   3240
      Width           =   2295
   End
   Begin PrintPRO3Ctl.PrintPRO pp 
      Left            =   10560
      Top             =   7920
      _ExtentX        =   953
      _ExtentY        =   794
      ErrStr          =   "ABD6FBDDE9408B2C3AC98C2E7273A90A"
      ErrCode         =   778180516
      ErrInfo         =   -295698374
      _cx             =   953
      _cy             =   794
      AddCR           =   -1  'True
      Alignment       =   0
      BackColor       =   16777215
      BackStyle       =   0
      BMargin         =   316.8
      CurrentX        =   360
      CurrentY        =   316.8
      DocName         =   "PrintPRO Document"
      DrawMode        =   13
      DrawStyle       =   1
      DrawWidth       =   1
      FillColor       =   0
      FillStyle       =   1
      ForeColor       =   0
      LMargin         =   360
      ScaleMode       =   1
      TMargin         =   316.8
      WordWrap        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoMargin      =   -1  'True
      OutputFileName  =   ""
      PicTransparent  =   0   'False
      PicTransparentColor=   0
      UseDefaultPrinter=   -1  'True
      PrintPreviewScale=   1
      PrintPreviewOnly=   0   'False
      OwnDIB          =   0   'False
      PrintToFile     =   0   'False
      SaveFileType    =   4
      EvalMode        =   0
   End
   Begin VB.Label Label8 
      Caption         =   "Paper ID"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   9240
      TabIndex        =   12
      Top             =   2805
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "Supported Paper Sizes"
      ForeColor       =   &H00FF0000&
      Height          =   375
      Left            =   4800
      TabIndex        =   11
      Top             =   2640
      Width           =   1815
   End
   Begin VB.Label Label6 
      Caption         =   "Bin ID"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   1320
      TabIndex        =   10
      Top             =   5280
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "Supported Bins"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   1440
      TabIndex        =   9
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label Label4 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   3840
      TabIndex        =   8
      Top             =   8040
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Duplex?"
      Height          =   255
      Left            =   3120
      TabIndex        =   7
      Top             =   8040
      Width           =   615
   End
   Begin VB.Label Label2 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1200
      TabIndex        =   6
      Top             =   8040
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Num Copies:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   8040
      Width           =   975
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmPrintCap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
  ' Select a printer
  pp.PrintDialog
  List1.Clear
  List2.Clear
  List3.Clear
  List4.Clear
  ' Get all of the bin names and IDs
  For i = 1 To pp.NumBins
    List1.AddItem pp.BinName(i)
    List2.AddItem pp.BinID(i)
  Next i
  ' Get all of the paper names and IDs
  For i = 1 To pp.NumPapers
    List3.AddItem pp.PaperName(i)
    List4.AddItem pp.PaperID(i)
  Next i
  Label2.Caption = pp.NumCopies
  Label4.Caption = pp.DuplexCap
End Sub


Private Sub Form_Load()
  
  
  Top = (Screen.Height - Height) / 2
  Left = (Screen.Width - Width) / 2
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub
Private Sub mnuAbout_Click()
    pp.AboutBox

End Sub

Private Sub mnuQuit_Click()
    End
End Sub

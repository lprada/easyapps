// PrintPRO3COMDlg.h : header file
//

#if !defined(AFX_PRINTPRO3COMDLG_H__DBC95790_BDC5_4B3A_AFB7_8C2E51A4AC78__INCLUDED_)
#define AFX_PRINTPRO3COMDLG_H__DBC95790_BDC5_4B3A_AFB7_8C2E51A4AC78__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace PrintPRO3;
using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"
#include "..\Include\printpro3events.h"

/////////////////////////////////////////////////////////////////////////////
// CPrintPRO3COMDlg dialog

class CPrintPRO3COMDlg : public CDialog
{
// Construction
public:
	CPrintPRO3COMDlg(CWnd* pParent = NULL);	// standard constructor

  CPrintPRO      *ppCPrintPRO;     // Pointer to the CPrintPRO class. 
  IPrintPROPtr   pPrintPRO;        //Pointer to the PrintPRO3 COM object.

  CImagXpress    *ppCImagXpress1;  // Pointer to the CImagXpress class.
  IImagXpressPtr pImagXpress1;     // Pointer to the ImagXpress COM object.

// Dialog Data
	//{{AFX_DATA(CPrintPRO3COMDlg)
	enum { IDD = IDD_PRINTPRO3COM_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintPRO3COMDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CPrintPRO3COMDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnPrint();
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnFileQuit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTPRO3COMDLG_H__DBC95790_BDC5_4B3A_AFB7_8C2E51A4AC78__INCLUDED_)

// PrintPRO3COMDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PrintPRO3COM.h"
#include "PrintPRO3COMDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// The following typedef is required by PP_OPEN.CPP unless you include the PP_OPEN.H file
typedef HRESULT (WINAPI* LPFNDLL_PSUnlock)(long, long, long, long);
// Before you distribute applications that use the PrintPRO COM object, you need to edit
// the PP_OPEN.CPP file and change the integrator key passed to the PS_Unlock function.
// The integrator key is obtained from Pegasus Software.
#include "..\Include\pp3_Open.cpp"
#include "..\Include\ix8_open.cpp"
bool bPrintToTiff;
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintPRO3COMDlg dialog

CPrintPRO3COMDlg::CPrintPRO3COMDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintPRO3COMDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintPRO3COMDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPrintPRO3COMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintPRO3COMDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPrintPRO3COMDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintPRO3COMDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON1, OnPrint)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintPRO3COMDlg message handlers

BOOL CPrintPRO3COMDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	  // Initialize PrintPRO.  You must call PP_Open before creating the first PrintPRO COM object
  // The PS_Unlock function in PP_OPEN.CPP requires an integrator key that you need to obtain
  // from Pegasus Software.  You cannot distribute applications using
  // PrintPRO COM objects without a valid integrator key.

  bPrintToTiff = false;
  HINSTANCE hDLL = PP_Open();

  // Create a PrintPRO class object.  The PrintPRO class object
  // automatically creates a PrintPRO COM object.
  ppCPrintPRO = new CPrintPRO((DWORD)this, 1);
	// Get the pointer to the created PrintPRO COM object.  This is the pointer that
	// you will use to access PrintPRO properties and methods.
  pPrintPRO = ppCPrintPRO->pPrintPRO;
  
  // Close the PrintPRO initialization.  You must call this AFTER creating the first PrintPRO COM object
  PP_Close(hDLL);

  // Create an ImagXpress instance
  HINSTANCE hIXDLL = IX_Open();

  ///*****uses IX 8 now
  ppCImagXpress1 = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 10, 30, 50, 50);
  pImagXpress1 = ppCImagXpress1->pImagXpress;




  IX_Close(hIXDLL);

  // ImagXpress Initialization
  if (pImagXpress1)
  {
    // Make the ImagXpress control invisible
	 	::ShowWindow((HWND)pImagXpress1->hWnd, SW_HIDE);
    // This path needs to be changed if PrintPRO was installed in a different directory
	    
		pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg";
	    pImagXpress1->AutoSize = ISIZE_BestFit;

		if (pPrintPRO) pPrintPRO->hDIB = pImagXpress1->hDIB;	
  }
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CPrintPRO3COMDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPrintPRO3COMDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPrintPRO3COMDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPrintPRO3COMDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
 pPrintPRO = NULL;
  if (ppCPrintPRO) 
    delete ppCPrintPRO;

  pImagXpress1 = NULL;
	if (ppCImagXpress1)
		delete ppCImagXpress1;
	
}

void CPrintPRO3COMDlg::OnPrint() 
{
	 float          prWidth;
	_bstr_t        fontName;
   VARIANT        var;
  
 VARIANT_BOOL rob;
 rob = true;

//	var.vt = VT_ERROR;
	
  // Don't print unless we have a PrintPRO COM object
	if (pPrintPRO)
	{
    // Don't print if the printer reports an error
		if (pPrintPRO->PrintError == 0)
		{
			if (bPrintToTiff == true)
				{
					pPrintPRO->DocName = "Pegasus's PrintPRO Demo";
					pPrintPRO->OutputFileName = "PrintToTif.tif";
					pPrintPRO->PrintToFile = true;
					pPrintPRO->SaveFileType = PP_TIF;
					pPrintPRO->RequestedDPI = 100;
				}
				else
				{	
						pPrintPRO->DocName = "Pegasus's PrintPRO Demo";
						pPrintPRO->OutputFileName = "";
						pPrintPRO->PrintToFile = false;
						pPrintPRO->SaveFileType = PP_TIF;
				}
	
      pPrintPRO->DocName = "PrintPRO 3.0";

			// Start a print job by selecting a printer and starting a
      // new page. If you don't select a printer or start a new
      // page, PrintPRO will automatically select the default printer and start
      // a new page when you first print text, graphics or a bitmap.
      pPrintPRO->SelectDefaultPrinter();
      pPrintPRO->StartPrintDoc();
      
			// Print a banner at the top of the page, white on black.
      pPrintPRO->BackStyle = BACK_Opaque;  // Opaque
      pPrintPRO->ForeColor = RGB(255, 255, 255);  // White text
      pPrintPRO->BackColor = RGB(0, 0, 0);  // Black background
	  fontName = _T("Arial");
	  pPrintPRO->SetCtlFontName(fontName);
      pPrintPRO->SetCtlFontSize(24);          // 24 point font
      pPrintPRO->Alignment = ALIGN_CenterJustifyMiddle;  // Center justify the text
      prWidth = pPrintPRO->PWidth;
      pPrintPRO->PrintTextAligned("Hello! Check out PrintPRO 3.0  ", 1000, pPrintPRO->TMargin, prWidth - 1000, 1100);
      
			// The following code demonstrates how PrintPRO can print text
		  // aligned inside a rectangular area.  First, a box is drawn
			// then text is drawn aligned inside the box.
			pPrintPRO->BackStyle = BACK_Transparent;   // Transparent background
			pPrintPRO->ForeColor = RGB(0, 0, 0);  // Black text
            pPrintPRO->SetCtlFontSize(8);         // 8 point font
	  		pPrintPRO->SetCtlFontStyle(0, TRUE);   // Font bold on
			pPrintPRO->Alignment = ALIGN_LeftJustifyTop;
			pPrintPRO->DrawLine(1440, 1440, 2880, 2880, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - left justified top", 1440, 1440, 2880, 2880);
    
			pPrintPRO->Alignment = ALIGN_LeftJustifyMiddle;
			pPrintPRO->DrawLine(3100, 1440, 4540, 2880, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - left justified middle", 3100, 1440, 4540, 2880);
    
			pPrintPRO->Alignment = ALIGN_LeftJustifyBottom;
			pPrintPRO->DrawLine(4760, 1440, 6200, 2880, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - left justified bottom", 4760, 1440, 6200, 2880);
    
			pPrintPRO->Alignment = ALIGN_RightJustifyTop;
			pPrintPRO->DrawLine(6420, 1440, 7860, 2880, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - right justified top", 6420, 1440, 7860, 2880);
    
			pPrintPRO->Alignment = ALIGN_RightJustifyMiddle;
			pPrintPRO->DrawLine(1440, 3100, 2880, 4540, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - right justified middle", 1440, 3100, 2880, 4540);
    
			pPrintPRO->Alignment = ALIGN_RightJustifyBottom;
			pPrintPRO->DrawLine(3100, 3100, 4540, 4540, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - right justified bottom", 3100, 3100, 4540, 4540);
    
			pPrintPRO->Alignment = ALIGN_CenterJustifyTop;
			pPrintPRO->DrawLine(4760, 3100, 6200, 4540, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - center justified top", 4760, 3100, 6200, 4540);
    
			pPrintPRO->Alignment = ALIGN_CenterJustifyMiddle;
			pPrintPRO->DrawLine(6420, 3100, 7860, 4540, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - center justified middle", 6420, 3100, 7860, 4540);
    
			pPrintPRO->Alignment = ALIGN_CenterJustifyBottom;
			pPrintPRO->DrawLine(1440, 4760, 7860, 5200, 0, TRUE, FALSE);
			pPrintPRO->PrintTextAligned("This is an experiment to see if the printer supports formatted text - center justified bottom", 1440, 4760, 7860, 5200);

			pPrintPRO->PrintDIB(1640, 5700, 0, 0, 0, 0, 0, 0, rob);
			
		  // Use graphic methods to draw an arrow
			pPrintPRO->FillStyle = (peFillStyle)0;
			pPrintPRO->DrawWidth = 15;
			pPrintPRO->DrawPolygon(8390, 7575, 6770, 7575, 6870, 7475, 6870, 7675, 6770, 7575, 0, 0, 0, 5);

			// Draw text inside a gray box with a black border
			pPrintPRO->FillStyle = (peFillStyle)1;
			pPrintPRO->Alignment = ALIGN_CenterJustifyMiddle;
			// Draw the gray box
			pPrintPRO->DrawLine(8550, 5700, 10550, 9450, RGB(225, 225, 225), TRUE, TRUE);
			// Put a black border on the box
			pPrintPRO->DrawLine(8550, 5700, 10550, 9450, 0, TRUE, FALSE);
			// Print text inside the box
			fontName = _T("Times New Roman");
			pPrintPRO->SetCtlFontName(fontName);
			pPrintPRO->SetCtlFontSize(18);
			pPrintPRO->BackStyle = BACK_Transparent;
			pPrintPRO->PrintTextAligned("Print\nGraphics\nand\nBitmaps\nand\nText", 8550, 5500, 10550, 9250);

			// Print the features
			fontName = _T("Arial");
			pPrintPRO->SetCtlFontName(fontName);
			pPrintPRO->SetCtlFontSize(24);
			pPrintPRO->SetCtlFontStyle(2, TRUE);   // Font underline on

			pPrintPRO->Lmargin = 1440;             // Left margin = 1 inch since there are 1440 twips/inch
			pPrintPRO->CurrentX = 1440;
			pPrintPRO->CurrentY = 10000;
			pPrintPRO->PrintText("Plus!");
			pPrintPRO->SetCtlFontStyle(2, FALSE);   // Font underline off
			pPrintPRO->PrintText("Lines, rectangles, circles, ellipse, arc,");
			pPrintPRO->PrintText("curves, polygons, pie, rounded rectangles,");
			pPrintPRO->PrintText("Print Dialog, and much, much more!");
			pPrintPRO->PrintText("PrintPRO is an ATL control so you can use");
			pPrintPRO->PrintText("it with your favorite dev. tool including IE.\n");
			pPrintPRO->PrintText("The PrintPRO Team\n");
			pPrintPRO->SetCtlFontSize(8);
			pPrintPRO->PrintText("This page was created in VC++ 6.0 using the Pegasus Software PrintPRO COM control.");
			// End the print job.  This will end the current page and print the document
			pPrintPRO->EndPrintDoc();
		}
	}
	
}

void CPrintPRO3COMDlg::OnRadio1() 
{
	
	if(IDC_RADIO1)
	{
		bPrintToTiff = true;

	}
	else
	{
		bPrintToTiff = false;

	}

}

void CPrintPRO3COMDlg::OnRadio2() 
{
	if(IDC_RADIO2)
	{
		bPrintToTiff = false;

	}
	else
	{
		bPrintToTiff = true;

	}
	
}

void CPrintPRO3COMDlg::OnFileQuit() 
{
	 EndDialog(IDOK);
	
}

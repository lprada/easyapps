VERSION 5.00
Object = "{89F92CAF-B12C-4D3B-B92B-F75DDAB357B6}#1.0#0"; "PrintPRO3.dll"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "PrintPRO v3.0 Demo"
   ClientHeight    =   9345
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   12375
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9345
   ScaleWidth      =   12375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstInfo 
      Height          =   2010
      ItemData        =   "prntPROdemo.frx":0000
      Left            =   240
      List            =   "prntPROdemo.frx":001F
      TabIndex        =   13
      Top             =   240
      Width           =   11895
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   6615
      Left            =   5280
      TabIndex        =   12
      Top             =   2400
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   11668
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   956187775
      ErrInfo         =   -2001924850
      Persistence     =   -1  'True
      _cx             =   11668
      _cy             =   11668
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Choose PrintToFile or Preview"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   2520
      Width           =   2775
   End
   Begin VB.Frame TiffFrame 
      Caption         =   "Print To File"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2355
      Left            =   240
      TabIndex        =   7
      Top             =   3120
      Width           =   2745
      Begin VB.OptionButton Option2 
         Caption         =   "Disable PrintToFile"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   1935
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Enable PrintToFile"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   2055
      End
      Begin VB.CommandButton cmdPrintDemo 
         Caption         =   "Print Image"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   8
         Top             =   1800
         Width           =   2055
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Print Preview"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3360
      Left            =   240
      TabIndex        =   0
      Top             =   5640
      Width           =   2700
      Begin VB.CommandButton cmdScale1_5 
         Caption         =   "Scale 1:5"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   495
         TabIndex        =   6
         Top             =   2895
         Width           =   990
      End
      Begin VB.CommandButton cmdScale1_4 
         Caption         =   "Scale 1:4"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   495
         TabIndex        =   5
         Top             =   2445
         Width           =   990
      End
      Begin VB.CommandButton cmdScale1_3 
         Caption         =   "Scale 1:3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   495
         TabIndex        =   4
         Top             =   1980
         Width           =   990
      End
      Begin VB.CommandButton cmdScale1_2 
         Caption         =   "Scale 1:2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   495
         TabIndex        =   3
         Top             =   1515
         Width           =   990
      End
      Begin VB.CommandButton cmdScale1_1 
         Caption         =   "Scale 1:1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   495
         TabIndex        =   2
         Top             =   1080
         Width           =   990
      End
      Begin VB.CommandButton cmdPrintPreview 
         Caption         =   "Print Preview"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   225
         TabIndex        =   1
         Top             =   480
         Width           =   1545
      End
   End
   Begin PrintPRO3Ctl.PrintPRO PrintPRO1 
      Left            =   4200
      Top             =   8760
      _ExtentX        =   953
      _ExtentY        =   794
      ErrStr          =   "ABD6FBDDE9408B2C3AC98C2E7273A90A"
      ErrCode         =   956187775
      ErrInfo         =   -1399597752
      _cx             =   953
      _cy             =   794
      AddCR           =   -1  'True
      Alignment       =   0
      BackColor       =   16777215
      BackStyle       =   0
      BMargin         =   316.8
      CurrentX        =   360
      CurrentY        =   316.8
      DocName         =   "PrintPRO Document"
      DrawMode        =   13
      DrawStyle       =   1
      DrawWidth       =   1
      FillColor       =   0
      FillStyle       =   1
      ForeColor       =   0
      LMargin         =   360
      ScaleMode       =   1
      TMargin         =   316.8
      WordWrap        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoMargin      =   -1  'True
      OutputFileName  =   ""
      PicTransparent  =   0   'False
      PicTransparentColor=   0
      UseDefaultPrinter=   -1  'True
      PrintPreviewScale=   2
      PrintPreviewOnly=   0   'False
      OwnDIB          =   0   'False
      PrintToFile     =   0   'False
      SaveFileType    =   6
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open bitmap to print..."
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Dim bPrintToTiff As Boolean

Private Sub PrintPage()
  Dim prWidth As Single
  'Note: all of the dimensions in this demo are in twips.
  
  'Print a banner at the top of the page, white on black.
  PrintPRO1.BackStyle = 1  'Opaque
  PrintPRO1.ForeColor = RGB(255, 255, 255) 'White text
  PrintPRO1.BackColor = RGB(0, 0, 0) 'Black background
  PrintPRO1.Font.Name = "arial"
  PrintPRO1.Font.Bold = True
  PrintPRO1.Font.Size = 24
  PrintPRO1.Alignment = ALIGN_CenterJustifyMiddle ' Center justify the text
  prWidth = PrintPRO1.PWidth
  PrintPRO1.PrintTextAligned "  Hello! Check out PrintPRO 3.0  ", 1000, PrintPRO1.TMargin, prWidth - 1000, 1100
    
    ' The following code demonstrates how PrintPRO can print text
    ' aligned inside a rectangular area.  First, a box is drawn
    ' then text is drawn aligned inside the box.
    PrintPRO1.DrawWidth = 1
    PrintPRO1.BackStyle = 0 'Transparent background
    PrintPRO1.ForeColor = RGB(0, 0, 0) 'Black text
    PrintPRO1.Font.Size = 8
    PrintPRO1.Alignment = ALIGN_LeftJustifyTop
    PrintPRO1.DrawLine 1440, 1440, 2880, 2880, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - left justified top", 1440, 1440, 2880, 2880
    
    PrintPRO1.Alignment = ALIGN_LeftJustifyMiddle
    PrintPRO1.DrawLine 3100, 1440, 4540, 2880, 0, True, False
    
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - left justified middle", 3100, 1440, 4540, 2880
    
    PrintPRO1.Alignment = ALIGN_LeftJustifyBottom
    PrintPRO1.DrawLine 4760, 1440, 6200, 2880, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - left justified bottom", 4760, 1440, 6200, 2880
    
    PrintPRO1.Alignment = ALIGN_RightJustifyTop
    PrintPRO1.DrawLine 6420, 1440, 7860, 2880, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - right justified top", 6420, 1440, 7860, 2880
    
    PrintPRO1.Alignment = ALIGN_RightJustifyMiddle
    PrintPRO1.DrawLine 1440, 3100, 2880, 4540, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - right justified middle", 1440, 3100, 2880, 4540
    
    PrintPRO1.Alignment = ALIGN_RightJustifyBottom
    PrintPRO1.DrawLine 3100, 3100, 4540, 4540, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - right justified bottom", 3100, 3100, 4540, 4540
    
    PrintPRO1.Alignment = ALIGN_CenterJustifyTop
    PrintPRO1.DrawLine 4760, 3100, 6200, 4540, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - center justified top", 4760, 3100, 6200, 4540
    
    PrintPRO1.Alignment = ALIGN_CenterJustifyMiddle
    PrintPRO1.DrawLine 6420, 3100, 7860, 4540, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - center justified middle", 6420, 3100, 7860, 4540
    
    PrintPRO1.Alignment = ALIGN_CenterJustifyBottom
    PrintPRO1.DrawLine 1440, 4760, 7860, 5200, 0, True, False
    PrintPRO1.PrintTextAligned "This is an experiment to see if the printer supports formatted text - center justified bottom", 1440, 4760, 7860, 5200
    
    PrintPRO1.PrintPicture 1640, 5700, 5000, 4000, 0, 0, 0, 0, False
        
    ' Use graphic methods to draw an arrow
    PrintPRO1.FillStyle = FILL_Solid
    PrintPRO1.DrawWidth = 15
    PrintPRO1.DrawPolygon 8390, 7575, 6770, 7575, 6870, 7475, 6870, 7675, 6770, 7575, 0, 0, 0, 5
    
    ' Draw text inside a gray box with a black border
    PrintPRO1.FillStyle = FILL_Transparent
    PrintPRO1.Alignment = ALIGN_CenterJustifyMiddle
    ' Draw the gray box
    PrintPRO1.DrawLine 8550, 5700, 10550, 9450, RGB(225, 225, 225), True, True
    ' Put a black border on the box
    PrintPRO1.DrawLine 8550, 5700, 10550, 9450, 0, True, False
    ' Print text inside the box
    PrintPRO1.Font.Name = "Times"
    PrintPRO1.Font.Size = 18
    PrintPRO1.BackStyle = 0
    PrintPRO1.PrintTextAligned "Print" & Chr$(13) & "Graphics" & Chr$(13) & "and" & Chr$(13) & "Bitmaps" & Chr$(13) & "and" & Chr$(13) & "text", 8550, 5500, 10550, 9250
    
    ' Print the features
    PrintPRO1.Font.Name = "Arial"
    PrintPRO1.Font.Size = 24
    PrintPRO1.Font.Underline = True
    
    PrintPRO1.Lmargin = 1440
    PrintPRO1.CurrentX = 1440
    PrintPRO1.CurrentY = 10000
    PrintPRO1.PrintText "Plus!"
    PrintPRO1.Font.Underline = False
    PrintPRO1.PrintText "Lines, rectangles, circles, ellipse, arc,"
    PrintPRO1.PrintText "curves, polygons, pie, rounded rectangles,"
    PrintPRO1.PrintText "Print Dialog, and much, much more!"
    PrintPRO1.PrintText "PrintPRO is an ATL control so you can use"
    PrintPRO1.PrintText "it with your favorite dev. tool including IE." & Chr$(13)
    PrintPRO1.PrintText "The PrintPRO Team" & Chr$(13)
    PrintPRO1.Font.Size = 8
    PrintPRO1.CurrentY = PrintPRO1.ScaleHeight - PrintPRO1.TextHeight("This page")
    PrintPRO1.PrintText "This page was created in Visual Basic 6.0 using the Pegasus Software PrintPRO COM control."
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        TiffFrame.Enabled = True
        Frame1.Enabled = False
    Else
         TiffFrame.Enabled = False
         Frame1.Enabled = True
    End If
End Sub


Private Sub cmdPrintDemo_Click()
 
  If PrintPRO1.PrintError = 0 Then
    'Note: all of the dimensions in this demo are in twips.
    
    Form1.MousePointer = 11
    ' Give the document a name.  The default document
    ' name is PrintPRO Document.
    If (bPrintToTiff = True) Then
        PrintPRO1.DocName = "Pegasus's PrintPRO Demo"
        PrintPRO1.OutputFileName = App.Path & "\PrintToFile.tif"
        PrintPRO1.PrintToFile = True
        PrintPRO1.SaveFileType = PP_TIF
        PrintPRO1.RequestedDPI = 100
    Else
        PrintPRO1.DocName = "Pegasus's PrintPRO Demo"
        PrintPRO1.OutputFileName = ""
        PrintPRO1.PrintToFile = False
        PrintPRO1.SaveFileType = PP_TIF
   End If
    
    ' Start a print job by selecting a printer and starting a
    ' new page. If you don't select a printer or start a new
    ' page, PrintPRO will select the default printer and start
    ' a new page when you first print text, graphics or a bitmap.
    PrintPRO1.PrintDialog
    PrintPRO1.StartPrintDoc
    PrintPRO1.PrintPreviewOnly = False
    ' Print all of the information on the page
    PrintPage
    ' End the print job.  This will end the current page and print the document
    PrintPRO1.EndPrintDoc
    If (bPrintToTiff = True) Then
    
        ImagXpress1.PageNbr = ImagXpress1.NumPages(App.Path + "\PrintToFile.tif")
        ImagXpress1.FileName = App.Path + "\PrintToFile.tif"
    End If
    Form1.MousePointer = 0
  End If
End Sub

Private Sub cmdPrintPreview_Click()

   ImagXpress1.AutoSize = ISIZE_CropImage
   If PrintPRO1.PrintError = 0 Then
    Form1.MousePointer = 11
    'PrintPRO1.PrintDialog
    PrintPRO1.PrintPreviewOnly = True
    PrintPRO1.PrintPreviewScale = PPSCALE_1_1
    PrintPRO1.StartPrintPreview
    If PrintPRO1.PrintError <> 0 Then MsgBox "PrintPRO error: " & PrintPRO1.PrintError, vbOKCancel, "PrintPRO Demo"
    PrintPage
    ' End the print preview
    PrintPRO1.EndPrintPreview
    If PrintPRO1.PrintError <> 0 Then MsgBox "PrintPRO error: " & PrintPRO1.PrintError, vbOKCancel, "PrintPRO Demo"
    PrintPRO1.GetPrintPreview 1
    If PrintPRO1.PrintError <> 0 Then MsgBox "PrintPRO error: " & PrintPRO1.PrintError, vbOKCancel, "PrintPRO Demo"
    
    ImagXpress1.Picture = PrintPRO1.PrintPreviewPicture
    PrintPRO1.PrintPreviewOnly = False
    Form1.MousePointer = 0
  End If
End Sub

Private Sub cmdScale1_1_Click()
  
  PrintPRO1.PrintPreviewScale = PPSCALE_1_1
  PrintPRO1.GetPrintPreview 1
  If PrintPRO1.PrintError = 0 Then ImagXpress1.Picture = PrintPRO1.PrintPreviewPicture
End Sub



Private Sub cmdScale1_2_Click()
  
  PrintPRO1.PrintPreviewScale = PPSCALE_1_2
  PrintPRO1.GetPrintPreview 1
  If PrintPRO1.PrintError = 0 Then ImagXpress1.Picture = PrintPRO1.PrintPreviewPicture
End Sub



Private Sub cmdScale1_3_Click()
  
  PrintPRO1.PrintPreviewScale = PPSCALE_1_3
  PrintPRO1.GetPrintPreview 1
  If PrintPRO1.PrintError = 0 Then ImagXpress1.Picture = PrintPRO1.PrintPreviewPicture
End Sub


Private Sub cmdScale1_4_Click()
  
  PrintPRO1.PrintPreviewScale = PPSCALE_1_4
  PrintPRO1.GetPrintPreview 1
  If PrintPRO1.PrintError = 0 Then ImagXpress1.Picture = PrintPRO1.PrintPreviewPicture
End Sub


Private Sub cmdScale1_5_Click()

  PrintPRO1.PrintPreviewScale = PSCALE_1_5
  PrintPRO1.GetPrintPreview 1
  If PrintPRO1.PrintError = 0 Then ImagXpress1.Picture = PrintPRO1.PrintPreviewPicture
End Sub

Private Sub Form_Load()
  
  Dim sDesc As String

 

  Check1.Value = 1
  Top = (Screen.Height - Height) / 2
  Left = (Screen.Width - Width) / 2
  PrintPRO1.Picture = LoadPicture(App.Path & "\..\..\..\..\..\..\Common\Images\pic1.bmp")
  bPrintToTiff = False
  Option1.Value = True
  
  ImagXpress1.ScrollBars = SB_Both
  imgParentDir = App.Path
End Sub



Private Sub TiffOff_Click()
    bPrintToTiff = False
    cmdPrintDemo.Enabled = False
End Sub

Private Sub Option1_Click()
    If Option1.Value = True Then
        bPrintToTiff = True
    Else
        bPrintToTiff = False
    End If
End Sub

Private Sub Option2_Click()
    If Option2.Value = True Then
        bPrintToTiff = False
    Else
        bPrintToTiff = True
    End If
End Sub

Private Sub mnuAbout_Click()
    PrintPRO1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFilePF(imgParentDir, "Windows Bitmap (*.BMP)" + vbNullChar + "*.bmp" + vbNullChar)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        
        PrintPRO1.Picture = LoadPicture(imgFileName)
        
    End If
End Sub
Private Sub mnuFileQuit_Click()
    End
End Sub

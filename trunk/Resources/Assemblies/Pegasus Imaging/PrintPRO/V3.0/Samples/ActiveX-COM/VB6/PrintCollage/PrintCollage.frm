VERSION 5.00
Object = "{89F92CAF-B12C-4D3B-B92B-F75DDAB357B6}#1.0#0"; "PrintPRO3.dll"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmCollage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Collage Sample"
   ClientHeight    =   7485
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   9945
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7485
   ScaleWidth      =   9945
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1230
      ItemData        =   "PrintCollage.frx":0000
      Left            =   240
      List            =   "PrintCollage.frx":000D
      TabIndex        =   3
      Top             =   120
      Width           =   9615
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   5895
      Left            =   240
      TabIndex        =   2
      Top             =   1440
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   10398
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1465851373
      ErrInfo         =   2062288839
      Persistence     =   -1  'True
      _cx             =   9551
      _cy             =   10398
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   615
      Left            =   8400
      TabIndex        =   1
      Top             =   5760
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   1085
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1465851373
      ErrInfo         =   2062288839
      Persistence     =   -1  'True
      _cx             =   2355
      _cy             =   1085
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdCreate 
      Caption         =   "Create Collage"
      Height          =   975
      Left            =   7080
      TabIndex        =   0
      Top             =   2040
      Width           =   2175
   End
   Begin PrintPRO3Ctl.PrintPRO PrintPRO1 
      Left            =   7560
      Top             =   5520
      _ExtentX        =   953
      _ExtentY        =   794
      ErrStr          =   "ABD6FBDDE9408B2C3AC98C2E7273A90A"
      ErrCode         =   1465851373
      ErrInfo         =   1590878081
      _cx             =   953
      _cy             =   794
      AddCR           =   -1  'True
      Alignment       =   0
      BackColor       =   16777215
      BackStyle       =   0
      BMargin         =   316.8
      CurrentX        =   360
      CurrentY        =   316.8
      DocName         =   "PrintPRO Document"
      DrawMode        =   13
      DrawStyle       =   1
      DrawWidth       =   1
      FillColor       =   0
      FillStyle       =   1
      ForeColor       =   0
      LMargin         =   360
      ScaleMode       =   1
      TMargin         =   316.8
      WordWrap        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoMargin      =   -1  'True
      OutputFileName  =   ""
      PicTransparent  =   0   'False
      PicTransparentColor=   0
      UseDefaultPrinter=   -1  'True
      PrintPreviewScale=   1
      PrintPreviewOnly=   0   'False
      OwnDIB          =   0   'False
      PrintToFile     =   0   'False
      SaveFileType    =   4
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmCollage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Private Sub cmdCreate_Click()
PrintPRO1.SelectDefaultPrinter
PrintPRO1.PrintToFile = True
PrintPRO1.SaveFileType = PP_TIF
PrintPRO1.OutputFileName = App.Path & "\Collage.tif"
PrintPRO1.StartPrintDoc

ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\dome.jpg"
PrintPRO1.hDIB = ImagXpress1.CopyDIB
PrintPRO1.PrintTemplateDIB 1

ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\water.jpg"
PrintPRO1.hDIB = ImagXpress1.CopyDIB
PrintPRO1.PrintTemplateDIB 2

ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\pic1.bmp"
PrintPRO1.hDIB = ImagXpress1.hDIB
PrintPRO1.PrintTemplateDIB 3

ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\pic1.bmp"
PrintPRO1.hDIB = ImagXpress1.hDIB
PrintPRO1.PrintTemplateDIB 4

PrintPRO1.EndPrintDoc

'show the resulting image
ImagXpress2.FileName = App.Path & "\collage.tif"
ImagXpress2.AutoSize = ISIZE_BestFit

End Sub

Private Sub Form_Load()
   PrintPRO1.TemplateFile = App.Path & "\..\..\..\..\..\..\Common\Images\template.txt"
End Sub


Private Sub mnuAbout_Click()
    PrintPRO1.AboutBox

End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

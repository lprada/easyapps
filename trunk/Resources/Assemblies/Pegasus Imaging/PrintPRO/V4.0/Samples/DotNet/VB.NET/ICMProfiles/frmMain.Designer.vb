<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.chkProof = New System.Windows.Forms.CheckBox
        Me.OptMetric = New System.Windows.Forms.RadioButton
        Me.optbus = New System.Windows.Forms.RadioButton
        Me.optGraph = New System.Windows.Forms.RadioButton
        Me.optImage = New System.Windows.Forms.RadioButton
        Me.txtInfo = New System.Windows.Forms.TextBox
        Me.btnPreview = New System.Windows.Forms.Button
        Me.renderintent = New System.Windows.Forms.GroupBox
        Me.OptNone = New System.Windows.Forms.RadioButton
        Me.ix = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ixvPreview = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.mnuMain = New System.Windows.Forms.MenuStrip
        Me.fileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.miLoadMonitor = New System.Windows.Forms.ToolStripMenuItem
        Me.miLoadPrinter = New System.Windows.Forms.ToolStripMenuItem
        Me.miOpenImage = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.miExit = New System.Windows.Forms.ToolStripMenuItem
        Me.miAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.ImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintProToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.pp = New PegasusImaging.WinForms.PrintPro4.PrintPro(Me.components)
        Me.lblError = New System.Windows.Forms.Label
        Me.renderintent.SuspendLayout()
        Me.mnuMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkProof
        '
        Me.chkProof.Checked = True
        Me.chkProof.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkProof.Location = New System.Drawing.Point(403, 381)
        Me.chkProof.Name = "chkProof"
        Me.chkProof.Size = New System.Drawing.Size(136, 32)
        Me.chkProof.TabIndex = 12
        Me.chkProof.Text = "Want Proof?"
        '
        'OptMetric
        '
        Me.OptMetric.Location = New System.Drawing.Point(6, 109)
        Me.OptMetric.Name = "OptMetric"
        Me.OptMetric.Size = New System.Drawing.Size(184, 24)
        Me.OptMetric.TabIndex = 3
        Me.OptMetric.Text = "AbsoluteColormetric"
        '
        'optbus
        '
        Me.optbus.Location = New System.Drawing.Point(6, 79)
        Me.optbus.Name = "optbus"
        Me.optbus.Size = New System.Drawing.Size(144, 24)
        Me.optbus.TabIndex = 2
        Me.optbus.Text = "Business"
        '
        'optGraph
        '
        Me.optGraph.Location = New System.Drawing.Point(6, 49)
        Me.optGraph.Name = "optGraph"
        Me.optGraph.Size = New System.Drawing.Size(144, 24)
        Me.optGraph.TabIndex = 1
        Me.optGraph.Text = "Graphics"
        '
        'optImage
        '
        Me.optImage.Checked = True
        Me.optImage.Location = New System.Drawing.Point(6, 19)
        Me.optImage.Name = "optImage"
        Me.optImage.Size = New System.Drawing.Size(152, 24)
        Me.optImage.TabIndex = 0
        Me.optImage.TabStop = True
        Me.optImage.Text = "Images"
        '
        'txtInfo
        '
        Me.txtInfo.BackColor = System.Drawing.SystemColors.Window
        Me.txtInfo.Location = New System.Drawing.Point(12, 27)
        Me.txtInfo.Multiline = True
        Me.txtInfo.Name = "txtInfo"
        Me.txtInfo.ReadOnly = True
        Me.txtInfo.Size = New System.Drawing.Size(599, 172)
        Me.txtInfo.TabIndex = 14
        Me.txtInfo.Text = resources.GetString("txtInfo.Text")
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(403, 433)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(136, 40)
        Me.btnPreview.TabIndex = 13
        Me.btnPreview.Text = "Preview the Image"
        '
        'renderintent
        '
        Me.renderintent.Controls.Add(Me.OptNone)
        Me.renderintent.Controls.Add(Me.OptMetric)
        Me.renderintent.Controls.Add(Me.optbus)
        Me.renderintent.Controls.Add(Me.optGraph)
        Me.renderintent.Controls.Add(Me.optImage)
        Me.renderintent.Location = New System.Drawing.Point(403, 205)
        Me.renderintent.Name = "renderintent"
        Me.renderintent.Size = New System.Drawing.Size(208, 170)
        Me.renderintent.TabIndex = 11
        Me.renderintent.TabStop = False
        Me.renderintent.Text = "ICM Render Intent"
        '
        'OptNone
        '
        Me.OptNone.Location = New System.Drawing.Point(6, 139)
        Me.OptNone.Name = "OptNone"
        Me.OptNone.Size = New System.Drawing.Size(160, 24)
        Me.OptNone.TabIndex = 4
        Me.OptNone.Text = "None"
        '
        'ixvPreview
        '
        Me.ixvPreview.AutoScroll = True
        Me.ixvPreview.Location = New System.Drawing.Point(13, 206)
        Me.ixvPreview.MouseWheelCapture = False
        Me.ixvPreview.Name = "ixvPreview"
        Me.ixvPreview.Size = New System.Drawing.Size(370, 341)
        Me.ixvPreview.TabIndex = 15
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.fileToolStripMenuItem, Me.miAbout})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(652, 24)
        Me.mnuMain.TabIndex = 16
        Me.mnuMain.Text = "menuStrip1"
        '
        'fileToolStripMenuItem
        '
        Me.fileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miLoadMonitor, Me.miLoadPrinter, Me.miOpenImage, Me.toolStripSeparator1, Me.miExit})
        Me.fileToolStripMenuItem.Name = "fileToolStripMenuItem"
        Me.fileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.fileToolStripMenuItem.Text = "&File"
        '
        'miLoadMonitor
        '
        Me.miLoadMonitor.Name = "miLoadMonitor"
        Me.miLoadMonitor.Size = New System.Drawing.Size(181, 22)
        Me.miLoadMonitor.Text = "Load Monitor Profile..."
        '
        'miLoadPrinter
        '
        Me.miLoadPrinter.Name = "miLoadPrinter"
        Me.miLoadPrinter.Size = New System.Drawing.Size(181, 22)
        Me.miLoadPrinter.Text = "Load Printer Profile..."
        '
        'miOpenImage
        '
        Me.miOpenImage.Name = "miOpenImage"
        Me.miOpenImage.Size = New System.Drawing.Size(181, 22)
        Me.miOpenImage.Text = "Open Image..."
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(178, 6)
        '
        'miExit
        '
        Me.miExit.Name = "miExit"
        Me.miExit.Size = New System.Drawing.Size(181, 22)
        Me.miExit.Text = "E&xit"
        '
        'miAbout
        '
        Me.miAbout.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImagXpressToolStripMenuItem, Me.PrintProToolStripMenuItem})
        Me.miAbout.Name = "miAbout"
        Me.miAbout.Size = New System.Drawing.Size(48, 20)
        Me.miAbout.Text = "About"
        '
        'ImagXpressToolStripMenuItem
        '
        Me.ImagXpressToolStripMenuItem.Name = "ImagXpressToolStripMenuItem"
        Me.ImagXpressToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.ImagXpressToolStripMenuItem.Text = "Imag&Xpress"
        '
        'PrintProToolStripMenuItem
        '
        Me.PrintProToolStripMenuItem.Name = "PrintProToolStripMenuItem"
        Me.PrintProToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.PrintProToolStripMenuItem.Text = "&PrintPro"
        '
        'lblError
        '
        Me.lblError.Location = New System.Drawing.Point(433, 492)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(207, 39)
        Me.lblError.TabIndex = 17
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(652, 555)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.mnuMain)
        Me.Controls.Add(Me.ixvPreview)
        Me.Controls.Add(Me.chkProof)
        Me.Controls.Add(Me.txtInfo)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.renderintent)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PrintPRO ICM Profiles Sample"
        Me.renderintent.ResumeLayout(False)
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents chkProof As System.Windows.Forms.CheckBox
    Private WithEvents OptMetric As System.Windows.Forms.RadioButton
    Private WithEvents optbus As System.Windows.Forms.RadioButton
    Private WithEvents optGraph As System.Windows.Forms.RadioButton
    Private WithEvents optImage As System.Windows.Forms.RadioButton
    Private WithEvents txtInfo As System.Windows.Forms.TextBox
    Private WithEvents btnPreview As System.Windows.Forms.Button
    Private WithEvents renderintent As System.Windows.Forms.GroupBox
    Private WithEvents OptNone As System.Windows.Forms.RadioButton
    Friend WithEvents ix As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ixvPreview As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Private WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Private WithEvents fileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents miLoadMonitor As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents miLoadPrinter As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents miOpenImage As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents miExit As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents miAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintProToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pp As PegasusImaging.WinForms.PrintPro4.PrintPro
    Friend WithEvents lblError As System.Windows.Forms.Label

End Class

/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PrintCapsCSharp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();

                if (pp != null)
                    pp.Dispose();
            }
            base.Dispose(disposing);
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void miAbout_Click(object sender, EventArgs e)
        {
            pp.AboutBox();
        }

        #region Pegasus Imaging Sample Application Standard Functions
        /*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

        private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
        private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
        System.String currentDir = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");

        private const System.String strNoImageError = "You must select an image file to open.";
        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }
        string PegasusOpenFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        string PegasusOpenFile(System.String strFilter)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
        {
            System.Int32 iTmp;
            try
            {
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber);
            }
            catch (System.NullReferenceException ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
            {
                scrScroll.Value = iTmp;
            }
            else
            {
                iTmp = scrScroll.Value;
            }
            textTextBox.Text = iTmp.ToString(cultNumber);
        }
        #endregion

        private void btnGo_Click(object sender, EventArgs e)
        {

            try
            {
                PegasusImaging.WinForms.PrintPro4.Printer printer;

                /* Clear any previous data. */
                lbBins.Items.Clear();
                lbPapers.Items.Clear();

                /* Prompt the user to select a printer. */
                printer = PegasusImaging.WinForms.PrintPro4.Printer.SelectPrinter(pp, true);
                if (printer != null)
                {


                    /* Printer.PaperBins implements IEnumerable so foreach() is the recommended method of
                     * parsing through it. */
                    foreach (PegasusImaging.WinForms.PrintPro4.PaperBin bin in printer.PaperBins)
                    {
                        lbBins.Items.Add(String.Format("{0}({1})", bin.Name, bin.Id));  //Adds the string "Name(Id)" to the listbox
                    }

                    /* Printer.Papers also implements IEnumerable so foreach() is the recommended method of
                     * parsing through it. */
                    foreach (PegasusImaging.WinForms.PrintPro4.Paper paper in printer.Papers)
                    {
                        lbPapers.Items.Add(String.Format("{0}({1})", paper.Name, paper.Id));  //Adds the string "Name(Id)" to the listbox
                    }

                    txtCopies.Text = printer.Copies.ToString();
                    txtDuplex.Text = printer.DuplexAvailable.ToString();

                }
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);

            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            /*** Must unlock the control with your provided unlock codes for distribution. ***/
            //pp.Licensing.UnlockRuntime(12345, 13245, 12345, 12345);
           
        }
    }
}

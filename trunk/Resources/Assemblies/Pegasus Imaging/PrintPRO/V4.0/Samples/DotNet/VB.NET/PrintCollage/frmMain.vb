'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************

Public Class frmMain


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "..\..\..\..\..\..\..\..\..\Common\Images")

    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Application.EnableVisualStyles()
        ' *** You must unlock the controls with your provided unlock codes for distribution. ***
        'ix.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)
        'pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)

    End Sub

    Private Function getSampleImage(ByVal image As String) As String
        Return Application.StartupPath + "..\..\..\..\..\..\..\..\..\Common\Images\" + image
    End Function

    Private Sub exitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click

        Try
            Dim printer As PegasusImaging.WinForms.PrintPro4.Printer
            Dim job As PegasusImaging.WinForms.PrintPro4.PrintJob
            Dim bitmap As System.Drawing.Bitmap
            Dim imagex As PegasusImaging.WinForms.ImagXpress9.ImageX

            ' Prompt the user for the desired printer.
            printer = PegasusImaging.WinForms.PrintPro4.Printer.SelectPrinter(pp, True)

            ' create a file printer and a job to go with it
            printer = PegasusImaging.WinForms.PrintPro4.Printer.CreateFilePrinter(pp, "collage.tif", _
                PegasusImaging.WinForms.PrintPro4.SaveFileType.Tif)
            printer.RequestedDpi = 120
            job = New PegasusImaging.WinForms.PrintPro4.PrintJob(printer)

            ' set the template file
            job.TemplateFileName = getSampleImage("template.txt")

            ' print a few images using a .NET Bitmap object and PrintTemplateImage
            bitmap = New System.Drawing.Bitmap(getSampleImage("dome.jpg"))
            job.PrintTemplateImage(bitmap, 1)

            bitmap = New System.Drawing.Bitmap(getSampleImage("water.jpg"))
            job.PrintTemplateImage(bitmap, 2)

            bitmap = New System.Drawing.Bitmap(getSampleImage("pic1.bmp"))
            job.PrintTemplateImage(bitmap, 3)

            ' print one using an HDIB and ImagXpress
            imagex = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, getSampleImage("pic1.bmp"))
            job.PrintTemplateDib(imagex.ToHdib(True), 4)

            ' end the page and "print" the image
            job.Finish()

            ' display the printed image file on the form
            ixvCollage.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, "collage.tif")
            ixvCollage.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit

        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub ImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressToolStripMenuItem.Click
        ix.AboutBox()
    End Sub

    Private Sub PrintProToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintProToolStripMenuItem.Click
        pp.AboutBox()
    End Sub
End Class

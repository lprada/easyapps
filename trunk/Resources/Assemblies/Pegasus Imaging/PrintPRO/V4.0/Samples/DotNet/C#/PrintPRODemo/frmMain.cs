/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
 
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace PrintPRODemoCSharp
{
	/// <summary>
	/// The main form for the sample.
	/// </summary>
	public partial class frmMain : System.Windows.Forms.Form
    {
        /// <summary>
        /// Returns the path to the specified sample image.
        /// </summary>
        static string getSampleImage(string image)
        {
            return Application.StartupPath + @"..\..\..\..\..\..\..\..\..\Common\Images\" + image;
        }

		bool printToFile;
        string samplePicture;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new frmMain());
        }

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if (disposing)
			{
		
				if (ixvPreview.Image != null)
				{
					ixvPreview.Image.Dispose();
					ixvPreview.Image = null;
				}
				if (ix != null)
				{
					ix.Dispose();
					ix = null;
				}

                if (ix != null)
                {
                    pp.Dispose();
                    pp = null;
                }  

				if (components != null) 
				{
					components.Dispose();
				}

			    
			}
			base.Dispose( disposing );
        }

        private void FormMain_Load(object sender, System.EventArgs e)
        {
            try
            {
                /*** You must unlock the control with your provided unlock codes for distribution. ***/
                //ix.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
                //pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
                samplePicture = getSampleImage("Pic1.bmp");
                printToFile = false;
                ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.CropImage;

            }
            catch(Exception ex)  
            {
                PegasusError(ex, lblError);

            }
            
            }

        /// <summary>
        /// Prints some sample text and graphics to the given job.
        /// </summary>
        void PrintPage(PegasusImaging.WinForms.PrintPro4.PrintJob job)
        {
            // Note: all of the dimensions in this demo are in twips.

            System.Drawing.Image picture = System.Drawing.Image.FromFile(samplePicture);

            System.Drawing.Font font;
            System.Drawing.Rectangle bounds;

            font = new System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Bold);

            /* Print a banner at the top of the page, white on black. */
            job.Draw.BackStyle = PegasusImaging.WinForms.PrintPro4.BackStyle.Opaque;
            job.Draw.ForeColor = System.Drawing.Color.White;    // white text
            job.Draw.BackColor = System.Drawing.Color.Black;    // on a black background
            job.Draw.TextAligned(
                new System.Drawing.RectangleF(1000, job.TopMargin, job.PaperWidth - 2000, 1100 - job.TopMargin),
                " Hello! Check out PrintPRO v4! ", font,
                PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyMiddle);

            /* The following code demonstrates how PrintPRO can print text aligned inside
             * a rectangular area. First a box is drawn then text drawn aligned inside
             * the box.
             * 
             * Note: In previous versions of PrintPRO, one drew a rectangle with DrawLine() and passing
             * true for the "box" parameter. PrintPRO v4 supports similar functionality with Draw.Line()
             * but using Draw.Rectangle() as below is the recommended method.
             */
            job.Draw.Width = 1;
            job.Draw.BackStyle = PegasusImaging.WinForms.PrintPro4.BackStyle.Transparent;
            job.Draw.ForeColor = System.Drawing.Color.Black;
            font = new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold);

            bounds = new System.Drawing.Rectangle(1440, 1440, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - left justified top",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.LeftJustifyTop, true);

            bounds = new System.Drawing.Rectangle(3100, 1440, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - left justified middle",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.LeftJustifyMiddle, true);

            bounds = new System.Drawing.Rectangle(4760, 1440, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - left justified bottom",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.LeftJustifyBottom, true);

            bounds = new System.Drawing.Rectangle(6420, 1440, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - right justified top",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.RightJustifyTop, true);

            bounds = new System.Drawing.Rectangle(1440, 3100, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - right justified middle",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.RightJustifyMiddle, true);

            bounds = new System.Drawing.Rectangle(3100, 3100, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - right justified bottom",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.RightJustifyBottom, true);

            bounds = new System.Drawing.Rectangle(4760, 3100, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - center justified top",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyTop, true);

            bounds = new System.Drawing.Rectangle(6420, 3100, 1440, 1440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - center justified middle",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyMiddle, true);

            bounds = new System.Drawing.Rectangle(1440, 4760, 6420, 440);
            job.Draw.Rectangle(bounds, false);
            job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - center justified bottom",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyBottom, true);

             //* Print a sample image onto the page using a .NET Image. There is also a PrintDib() method for printing
             //* bitmaps from ImagXpress or other components.
             //*
             //* Note: Since "aspect" is true, the source and source size parameters are assumed to be (0,0) and the
             //* size of the original image, respectively. So the below call to PrintImage() is equivalent to:
          
            job.PrintImage(picture, new System.Drawing.PointF(1640, 5700), new System.Drawing.SizeF(5000, 4000), true);

            //Use drawing methods to draw an arrow */
            job.Draw.FillStyle = PegasusImaging.WinForms.PrintPro4.FillStyle.Solid;
            job.Draw.Width = 15;
            job.Draw.Polygon(
                new System.Drawing.Point[] {
                    new System.Drawing.Point(8390, 7575),
                    new System.Drawing.Point(6770, 7575),
                    new System.Drawing.Point(6870, 7475),
                    new System.Drawing.Point(6870, 7675),
                    new System.Drawing.Point(6770, 7575) }
                );

            /* Draw text inside a gray box with a black border */
            job.Draw.FillStyle = PegasusImaging.WinForms.PrintPro4.FillStyle.Transparent;
            job.Draw.FillColor = System.Drawing.Color.Gray;

            bounds = new System.Drawing.Rectangle(8550, 5700, 2000, 3750);
            job.Draw.Rectangle(bounds, System.Drawing.Color.Gray, true);    //draws the filled gray rectangle
            job.Draw.Rectangle(bounds, System.Drawing.Color.Black, false);  //draws an unfilled black rectangle as a border

            font = new System.Drawing.Font("Times", 18);
            job.Draw.BackStyle = PegasusImaging.WinForms.PrintPro4.BackStyle.Transparent;
            job.Draw.TextAligned(new System.Drawing.Rectangle(8550, 5500, 2000, 3750), "Print\rGraphics\rand\rBitmaps\rand\rtext",
                font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyMiddle);

            /* Print the features */
            font = new System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Underline | System.Drawing.FontStyle.Bold);
            job.LeftMargin = 1440;
            job.Draw.CurrentX = 1440;
            job.Draw.CurrentY = 10000;
            job.Draw.Text("Plus!", font);
            font = new System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Bold);
            job.Draw.Text("Lines, rectangles, circles, ellipse, arc,", font);
            job.Draw.Text("curves, polygons, pie, rounded rectangles,", font);
            job.Draw.Text("Print Dialog, and much, much more!", font);
            job.Draw.Text("PrintPRO is a .NET managed control", font);
            job.Draw.Text("it with your favorite dev. tool including IE.\r", font);
            job.Draw.Text("The PrintPRO Team\r", font);
            font = new System.Drawing.Font("Arial", 8);
            job.Draw.CurrentY = job.PrintHeight - job.Draw.TextHeight(font, "This page");
            job.Draw.Text("This page was created in Microsoft Visual C# .NET using the Pegasus Imaging PrintPRO .NET control.", font);
        }

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			// Note: all of the dimensions in this demo are in twips.
            try
            {
                PegasusImaging.WinForms.PrintPro4.Printer printer;  // generic Printer object, this is a real printer or a file depending on the user's selection
                PegasusImaging.WinForms.PrintPro4.PrintJob job;     // object representation of a print job, all properties in PrintPRO3 having to do with the
                
                /* Prompt the user for the desired printer.
                   The dialog can be circumvented by specifying a printer name as a string
                   or by specifying only the PrintPro object to select the default printer. */
                printer = PegasusImaging.WinForms.PrintPro4.Printer.SelectPrinter(pp, true);

                string destinationFile = System.IO.Path.Combine(Application.StartupPath, "PrintToFile.tif");
                if (System.IO.File.Exists(destinationFile))
                {
                    System.IO.File.Delete(destinationFile);    
                }

                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                if (printToFile == true)
                {
                    ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;

                    /* create a file printer */
                    printer = PegasusImaging.WinForms.PrintPro4.Printer.CreateFilePrinter(pp,
                        destinationFile, PegasusImaging.WinForms.PrintPro4.SaveFileType.Tif);

                    /* Set the dpi so the generated file is not too large. The resulting image will be the selected DPI
                     * times the printable area (slightly smaller than 8.5x11 because of margins). */
                    printer.RequestedDpi = 100;
                }
                
                if(printer != null)
                {
                /* Create a PrintJob object for the above-selected Printer. If the printer is not specified, PrintPRO
                 * will just use Windows's default printer. */
                job = new PegasusImaging.WinForms.PrintPro4.PrintJob(printer);
                job.Name = "Pegasus PrintPRO Demo";

       
                /* Print all of the information on the page */
                PrintPage(job);

                /* Finish the print job to end the current page and print the document */
                job.Finish();
            
                if (printToFile == true)
                {
                    try
                    {
                        ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, destinationFile);
                    }
                    catch (PegasusImaging.WinForms.ImagXpress9.ImageXException ex)
                    {
                        MessageBox.Show(ex.Result.ToString());
                    }
                }
                
               }
            }
            catch (System.Exception ex)
            {

                PegasusError(ex, lblError);
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
		}

		private void buttonPrintPreview_Click(object sender, System.EventArgs e)
        {
            PegasusImaging.WinForms.PrintPro4.Printer printer;
            PegasusImaging.WinForms.PrintPro4.PrintPreviewJob job;

            /* set the wait cursor */
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

            try
            {
                ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.CropImage;

                /* prompt the user for the desired printer */
                printer = PegasusImaging.WinForms.PrintPro4.Printer.SelectPrinter(pp, true);

                /* create a print preview job for the above-specified printer */
                job = new PegasusImaging.WinForms.PrintPro4.PrintPreviewJob(printer);

                /* draw some sample text and graphics on the page */
                PrintPage(job);

                /* set the preview scale that the user requested */
                if (radioScale1to1.Checked)
                {
                    job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to1;
                }
                else if (radioScale1to2.Checked)
                {
                    job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to2;
                }
                else if (radioScale1to3.Checked)
                {
                    job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to3;
                }
                else if (radioScale1to4.Checked)
                {
                    job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to4;
                }
                else if (radioScale1to5.Checked)
                {
                    job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to5;
                }

                /* Finish the print job to end the current page and "print" the document */
                job.Finish();

                /* display the generated preview image by getting a .NET Bitmap from the PrintPreviewJob
                 * and converting it into an ImagXpress ImageX object. */
                ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromBitmap(ix, job.GetPrintPreviewBitmap(1));
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);
            }

			/* restore normal cursor */
			this.Cursor = System.Windows.Forms.Cursors.Default;
		}
        
		private void miOpenBitmap_Click(object sender, System.EventArgs e)
		{

            System.String sTmp = PegasusOpenFile();
            if (sTmp.Length != 0)
            {
                try
                {
                    samplePicture = sTmp;
                }
                catch (Exception ex)
                {
                    PegasusError(ex, lblError);
                }
                
            }

            
		}

		private void miQuit_Click(object sender, System.EventArgs e)
		{
            Application.Exit();
		}

        private void radioPrintToFile_CheckedChanged(object sender, EventArgs e)
        {
            if (radioPrintToFile.Checked)
            {
                printToFile = true;
                btnPrint.Enabled = true;
                grpPrintPreview.Enabled = false;
            }
        }

        private void radioPrintToPrinter_CheckedChanged(object sender, EventArgs e)
        {
            if (radioPrintToPrinter.Checked)
            {
                printToFile = false;
                btnPrint.Enabled = true;
                grpPrintPreview.Enabled = false;
            }
        }

        private void radioPrintPreview_CheckedChanged(object sender, EventArgs e)
        {
            if (radioPrintPreview.Checked)
            {
                printToFile = false;
                btnPrint.Enabled = false;
                grpPrintPreview.Enabled = true;
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            pp.AboutBox();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            ix.AboutBox();

        }

        #region Pegasus Imaging Sample Application Standard Functions
        /*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

        private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
        private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
        System.String currentDir = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");
            
        private const System.String strNoImageError = "You must select an image file to open.";
        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }
        string PegasusOpenFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        string PegasusOpenFile(System.String strFilter)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strFilter;
            dlg.InitialDirectory = currentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"), dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
        {
            System.Int32 iTmp;
            try
            {
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber);
            }
            catch (System.NullReferenceException ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);
                textTextBox.Text = scrScroll.Value.ToString(cultNumber);
                return;
            }
            if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
            {
                scrScroll.Value = iTmp;
            }
            else
            {
                iTmp = scrScroll.Value;
            }
            textTextBox.Text = iTmp.ToString(cultNumber);
        }
        #endregion

      

	}
}

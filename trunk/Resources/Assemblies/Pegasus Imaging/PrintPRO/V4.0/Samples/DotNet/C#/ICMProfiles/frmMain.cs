/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace ICMProfiles
{
    /// <summary>
    /// The main form presented to the user.
    /// </summary>
	public partial class frmMain : System.Windows.Forms.Form
	{
        static string icmFilter = "Image Files(*.ICC;*.ICM;)|*.ICC;*.ICM;|All files (*.*)|*.*";
        private PegasusImaging.WinForms.ImagXpress9.ImageX sourceImage;
        private System.String strImagePath;
        string icmMonitor;
        string icmPrinter;

        /// <summary>
        /// Returns the path to the specified sample image.
        /// </summary>
        static string getSampleImage(string image)
        {
            return Application.StartupPath + @"..\..\..\..\..\..\..\..\..\Common\Images\" + image;
        }

		public frmMain()
		{
			InitializeComponent();

            /*** You must unlock the control with your provided unlock codes for distribution. ***/
            //pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null)
					components.Dispose();

                if (ix != null)
                    ix.Dispose();

                if (pp != null)
                    pp.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/
        
		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;

        private System.String strCurrentDir = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");

        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
		
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}

		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}


		#endregion

        /// <summary>
        /// Print previews a sample bitmap using the user-specified ICM profile(s).
        /// </summary>
        void DoPreview()
        {
            try
            {
                PegasusImaging.WinForms.PrintPro4.PrintPreviewJob job;
                System.Drawing.SizeF size;

                /* set ICM values */
                PegasusImaging.WinForms.PrintPro4.Icm icm = new PegasusImaging.WinForms.PrintPro4.Icm();

                if (String.IsNullOrEmpty(icmMonitor))
                    icmMonitor = icm.MonitorProfile;
                else
                    icm.MonitorProfile = icmMonitor;

                if (String.IsNullOrEmpty(icmPrinter))
                    icmPrinter = icm.PrinterProfile;
                else
                    icm.PrinterProfile = icmPrinter;

                icm.WantProof = chkProof.Checked;

                if (optImage.Checked)
                    icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.Images;
                else if (optbus.Checked)
                    icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.Business;
                else if (optGraph.Checked)
                    icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.Graphics;
                else if (OptMetric.Checked)
                    icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.AbsoluteColormetric;
                else if (OptNone.Checked)
                    icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.None;

                /* Do it */

                job = new PegasusImaging.WinForms.PrintPro4.PrintPreviewJob(pp, icm);

                job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to1;
                size = new System.Drawing.SizeF(job.PrintWidth - job.LeftMargin, job.PrintHeight - job.TopMargin - job.BottomMargin);

                job.PrintBitmap(sourceImage.ToBitmap(false), new System.Drawing.PointF(job.LeftMargin, job.TopMargin),
                    size, true);

                ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromBitmap(ix,
                    job.GetPrintPreviewBitmap(1));
            }
            catch (System.Exception ex)
            {

                PegasusError(ex, lblError);
            }
        }

	
		private void miOpenImage_Click(object sender, System.EventArgs e)
		{

            System.String sTmp = PegasusOpenFile();
            if (sTmp.Length != 0)
            {
                try
                {

                    ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, sTmp);
			
                }
                catch (Exception ex)
                {
                    PegasusError(ex, lblError);
                }
                
            }

			chkProof.Checked = true; 
		}

		private void btnPreview_Click(object sender, System.EventArgs e)
        {
            DoPreview();
		}

		private void miExit_Click(object sender, System.EventArgs e)
		{
            Application.Exit();
		}

        private void printProToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pp.AboutBox();
        }

        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ix.AboutBox();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void openImageToPrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.String sTmp = PegasusOpenFile();
            if (sTmp.Length != 0)
            {
                try
                {
                   
                    sourceImage = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, sTmp);
                    ixvPreview.Image = sourceImage;
                }
                catch (Exception ex)
                {
                    PegasusError(ex, lblError);
                }

            }

            chkProof.Checked = true;      
        }

        private void loadMonitorProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog cd = new OpenFileDialog();
            cd.InitialDirectory = System.Environment.SystemDirectory + "\\spool\\drivers\\color\\";
            cd.Filter = icmFilter;
            cd.ShowDialog();

     
            icmMonitor = cd.FileName;

            chkProof.Checked = true;     
        }

        private void loadPrinterProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog cd = new OpenFileDialog();
            cd.InitialDirectory = System.Environment.SystemDirectory + "\\spool\\drivers\\color\\";
            cd.Filter = icmFilter;
            cd.ShowDialog();

            icmPrinter = cd.FileName;

            chkProof.Checked = true;
        }

        private void imagXpressToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ix.AboutBox();
        }

        private void printProToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            pp.AboutBox();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {

                //**Must call the UnlockRuntime to Distribute your application
                //pp.Licensing.UnlockRuntime(1234,1234,1234,1234);

  
                //Set the default directory to the common images directory
                strCurrentDir = System.IO.Path.Combine(System.Environment.CurrentDirectory, strCurrentDir);

                System.String strCurrentdir = System.IO.Directory.GetCurrentDirectory().ToString();

                strImagePath = System.IO.Path.Combine(strCurrentdir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\window.jpg");
                //Load the image
                sourceImage = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, strImagePath);
                ixvPreview.Image = sourceImage;
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblError);

            }
        }

        private void lblError_Click(object sender, EventArgs e)
        {

        }
           
	}
}

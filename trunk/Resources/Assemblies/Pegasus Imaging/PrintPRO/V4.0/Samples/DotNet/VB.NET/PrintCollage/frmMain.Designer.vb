<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnGo = New System.Windows.Forms.Button
        Me.fileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.exitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.menuStrip1 = New System.Windows.Forms.MenuStrip
        Me.aboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintProToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ix = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ixvCollage = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.pp = New PegasusImaging.WinForms.PrintPro4.PrintPro(Me.components)
        Me.lblError = New System.Windows.Forms.Label
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.menuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(349, 110)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(88, 40)
        Me.btnGo.TabIndex = 6
        Me.btnGo.Text = "Create Collage"
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'fileToolStripMenuItem
        '
        Me.fileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.exitToolStripMenuItem})
        Me.fileToolStripMenuItem.Name = "fileToolStripMenuItem"
        Me.fileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.fileToolStripMenuItem.Text = "&File"
        '
        'exitToolStripMenuItem
        '
        Me.exitToolStripMenuItem.Name = "exitToolStripMenuItem"
        Me.exitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.exitToolStripMenuItem.Text = "E&xit"
        '
        'menuStrip1
        '
        Me.menuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.fileToolStripMenuItem, Me.aboutToolStripMenuItem})
        Me.menuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.menuStrip1.Name = "menuStrip1"
        Me.menuStrip1.Size = New System.Drawing.Size(460, 24)
        Me.menuStrip1.TabIndex = 4
        Me.menuStrip1.Text = "menuStrip1"
        '
        'aboutToolStripMenuItem
        '
        Me.aboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImagXpressToolStripMenuItem, Me.PrintProToolStripMenuItem})
        Me.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem"
        Me.aboutToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.aboutToolStripMenuItem.Text = "&About"
        '
        'ImagXpressToolStripMenuItem
        '
        Me.ImagXpressToolStripMenuItem.Name = "ImagXpressToolStripMenuItem"
        Me.ImagXpressToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.ImagXpressToolStripMenuItem.Text = "Imag&Xpress"
        '
        'PrintProToolStripMenuItem
        '
        Me.PrintProToolStripMenuItem.Name = "PrintProToolStripMenuItem"
        Me.PrintProToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.PrintProToolStripMenuItem.Text = "&PrintPro"
        '
        'ixvCollage
        '
        Me.ixvCollage.AutoScroll = True
        Me.ixvCollage.Location = New System.Drawing.Point(13, 110)
        Me.ixvCollage.MouseWheelCapture = False
        Me.ixvCollage.Name = "ixvCollage"
        Me.ixvCollage.Size = New System.Drawing.Size(316, 313)
        Me.ixvCollage.TabIndex = 7
        '
        'lblError
        '
        Me.lblError.Location = New System.Drawing.Point(349, 213)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(99, 210)
        Me.lblError.TabIndex = 0
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1) Using the template interface which includes the TemplateFile property to load", " in a file containing the coordinates and the PrintTemplateImage and ", "PrintTemplateDib methods to print the images."})
        Me.ListBox1.Location = New System.Drawing.Point(12, 27)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(436, 69)
        Me.ListBox1.TabIndex = 8
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 435)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.ixvCollage)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.menuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PrintPRO Collage Sample"
        Me.menuStrip1.ResumeLayout(False)
        Me.menuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents btnGo As System.Windows.Forms.Button
    Private WithEvents fileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents exitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents menuStrip1 As System.Windows.Forms.MenuStrip
    Private WithEvents aboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ix As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ixvCollage As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents ImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintProToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pp As PegasusImaging.WinForms.PrintPro4.PrintPro
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox

End Class

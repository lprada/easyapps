namespace DrawingCSharp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPrintCirlces = new System.Windows.Forms.Button();
            this.btnPrintGrid = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.pp = new PegasusImaging.WinForms.PrintPro4.PrintPro(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.mnuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miAbout});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(417, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExit});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(35, 20);
            this.miFile.Text = "File";
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(92, 22);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // miAbout
            // 
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(60, 20);
            this.miAbout.Text = "About...";
            this.miAbout.Click += new System.EventHandler(this.miAbout_Click);
            // 
            // btnPrintCirlces
            // 
            this.btnPrintCirlces.Location = new System.Drawing.Point(12, 120);
            this.btnPrintCirlces.Name = "btnPrintCirlces";
            this.btnPrintCirlces.Size = new System.Drawing.Size(74, 29);
            this.btnPrintCirlces.TabIndex = 2;
            this.btnPrintCirlces.Text = "Print Circles";
            this.btnPrintCirlces.UseVisualStyleBackColor = true;
            this.btnPrintCirlces.Click += new System.EventHandler(this.btnPrintCirlces_Click);
            // 
            // btnPrintGrid
            // 
            this.btnPrintGrid.Location = new System.Drawing.Point(12, 172);
            this.btnPrintGrid.Name = "btnPrintGrid";
            this.btnPrintGrid.Size = new System.Drawing.Size(74, 29);
            this.btnPrintGrid.TabIndex = 3;
            this.btnPrintGrid.Text = "Print Grid";
            this.btnPrintGrid.UseVisualStyleBackColor = true;
            this.btnPrintGrid.Click += new System.EventHandler(this.btnPrintGrid_Click);
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(238, 214);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(156, 46);
            this.lblError.TabIndex = 4;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality of PrintPRO:",
            "1) Printing concentric circles on a page.",
            "2) Printing a 50x50 grid on a page."});
            this.listBox1.Location = new System.Drawing.Point(12, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(393, 69);
            this.listBox1.TabIndex = 5;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 273);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnPrintGrid);
            this.Controls.Add(this.btnPrintCirlces);
            this.Controls.Add(this.mnuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mnuMain;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintPRO Drawing Sample";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
        private System.Windows.Forms.Button btnPrintCirlces;
        private System.Windows.Forms.Button btnPrintGrid;
        private System.Windows.Forms.Label lblError;
        private PegasusImaging.WinForms.PrintPro4.PrintPro pp;
        private System.Windows.Forms.ListBox listBox1;
    }
}


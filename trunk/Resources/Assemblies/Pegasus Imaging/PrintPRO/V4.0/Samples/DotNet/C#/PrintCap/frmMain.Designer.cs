namespace PrintCapsCSharp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.miFile = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.btnGo = new System.Windows.Forms.Button();
            this.lbBins = new System.Windows.Forms.ListBox();
            this.lbPapers = new System.Windows.Forms.ListBox();
            this.lblBins = new System.Windows.Forms.Label();
            this.lblPapers = new System.Windows.Forms.Label();
            this.lblCopies = new System.Windows.Forms.Label();
            this.txtCopies = new System.Windows.Forms.TextBox();
            this.txtDuplex = new System.Windows.Forms.TextBox();
            this.lblDuplex = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.pp = new PegasusImaging.WinForms.PrintPro4.PrintPro(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.mnuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miFile,
            this.miAbout});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(589, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // miFile
            // 
            this.miFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExit});
            this.miFile.Name = "miFile";
            this.miFile.Size = new System.Drawing.Size(35, 20);
            this.miFile.Text = "&File";
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(92, 22);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // miAbout
            // 
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(60, 20);
            this.miAbout.Text = "About...";
            this.miAbout.Click += new System.EventHandler(this.miAbout_Click);
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(138, 91);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(204, 29);
            this.btnGo.TabIndex = 2;
            this.btnGo.Text = "Get Printer Capabilities...";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // lbBins
            // 
            this.lbBins.FormattingEnabled = true;
            this.lbBins.Location = new System.Drawing.Point(12, 151);
            this.lbBins.Name = "lbBins";
            this.lbBins.Size = new System.Drawing.Size(128, 238);
            this.lbBins.TabIndex = 3;
            // 
            // lbPapers
            // 
            this.lbPapers.FormattingEnabled = true;
            this.lbPapers.Location = new System.Drawing.Point(179, 151);
            this.lbPapers.Name = "lbPapers";
            this.lbPapers.Size = new System.Drawing.Size(128, 238);
            this.lbPapers.TabIndex = 4;
            // 
            // lblBins
            // 
            this.lblBins.Location = new System.Drawing.Point(9, 135);
            this.lblBins.Name = "lblBins";
            this.lblBins.Size = new System.Drawing.Size(99, 13);
            this.lblBins.TabIndex = 7;
            this.lblBins.Text = "Supported Bins(ID):";
            // 
            // lblPapers
            // 
            this.lblPapers.Location = new System.Drawing.Point(176, 135);
            this.lblPapers.Name = "lblPapers";
            this.lblPapers.Size = new System.Drawing.Size(112, 13);
            this.lblPapers.TabIndex = 8;
            this.lblPapers.Text = "Supported Papers(ID):";
            // 
            // lblCopies
            // 
            this.lblCopies.AutoSize = true;
            this.lblCopies.Location = new System.Drawing.Point(343, 135);
            this.lblCopies.Name = "lblCopies";
            this.lblCopies.Size = new System.Drawing.Size(52, 13);
            this.lblCopies.TabIndex = 9;
            this.lblCopies.Text = "# Copies:";
            // 
            // txtCopies
            // 
            this.txtCopies.Location = new System.Drawing.Point(346, 151);
            this.txtCopies.Name = "txtCopies";
            this.txtCopies.ReadOnly = true;
            this.txtCopies.Size = new System.Drawing.Size(77, 20);
            this.txtCopies.TabIndex = 10;
            // 
            // txtDuplex
            // 
            this.txtDuplex.Location = new System.Drawing.Point(346, 201);
            this.txtDuplex.Name = "txtDuplex";
            this.txtDuplex.ReadOnly = true;
            this.txtDuplex.Size = new System.Drawing.Size(77, 20);
            this.txtDuplex.TabIndex = 12;
            // 
            // lblDuplex
            // 
            this.lblDuplex.AutoSize = true;
            this.lblDuplex.Location = new System.Drawing.Point(343, 185);
            this.lblDuplex.Name = "lblDuplex";
            this.lblDuplex.Size = new System.Drawing.Size(46, 13);
            this.lblDuplex.TabIndex = 11;
            this.lblDuplex.Text = "Duplex?";
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(343, 352);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(212, 37);
            this.lblError.TabIndex = 13;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1) Retrieving the printer capabilities via the PaperBins, Papers, Copies and Dupl" +
                "exAvailable properties."});
            this.listBox1.Location = new System.Drawing.Point(12, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(565, 56);
            this.listBox1.TabIndex = 14;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 413);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtDuplex);
            this.Controls.Add(this.lblDuplex);
            this.Controls.Add(this.txtCopies);
            this.Controls.Add(this.lblCopies);
            this.Controls.Add(this.lblPapers);
            this.Controls.Add(this.lblBins);
            this.Controls.Add(this.lbPapers);
            this.Controls.Add(this.lbBins);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.mnuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mnuMain;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintPRO Print Capabilities Sample";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem miFile;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.ListBox lbBins;
        private System.Windows.Forms.ListBox lbPapers;
        private System.Windows.Forms.Label lblBins;
        private System.Windows.Forms.Label lblPapers;
        private System.Windows.Forms.Label lblCopies;
        private System.Windows.Forms.TextBox txtCopies;
        private System.Windows.Forms.TextBox txtDuplex;
        private System.Windows.Forms.Label lblDuplex;
        private System.Windows.Forms.Label lblError;
        private PegasusImaging.WinForms.PrintPro4.PrintPro pp;
        private System.Windows.Forms.ListBox listBox1;
    }
}


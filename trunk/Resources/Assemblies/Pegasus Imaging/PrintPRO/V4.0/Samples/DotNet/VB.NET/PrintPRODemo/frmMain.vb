'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************

Public Class frmMain

    Dim printToFile As Boolean = False
    Dim samplePicture As String
    Dim destinationFile As String

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "..\..\..\..\..\..\..\..\..\Common\Images")

    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            'Must call UnlockRuntime with your license codes for distribution **
            'ix.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)
            'pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)

            samplePicture = Application.StartupPath & "..\..\..\..\..\..\..\..\..\Common\Images\pic1.bmp"
            printToFile = False

            ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.CropImage

        Catch ex As Exception
            PegasusError(ex, lblError)
        End Try


        Application.EnableVisualStyles()
    End Sub

    Private Sub PrintPage(ByVal job As PegasusImaging.WinForms.PrintPro4.PrintJob)

        ' Note: All of the dimensions in this demo are in twips.

        Dim picture As Image = System.Drawing.Image.FromFile(samplePicture)

        Dim font As System.Drawing.Font
        Dim bounds As System.Drawing.Rectangle

        font = New System.Drawing.Font("Arial", 24, FontStyle.Bold)

        ' Print a banner at the top of the page, white on black.
        job.Draw.BackStyle = PegasusImaging.WinForms.PrintPro4.BackStyle.Opaque
        job.Draw.ForeColor = Color.White
        job.Draw.BackColor = Color.Black
        job.Draw.TextAligned(New RectangleF(100, job.TopMargin, job.PaperWidth - 2000, 1100 - job.TopMargin), " Hello! Check out PrintPRO v4! ", font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyMiddle)

        ' The following code demonstrates how PrintPRO can print text aligned inside a 
        ' rectangular area. First a box is drawn then text drawn aligned inside the box.
        '
        ' Note: In previous versions of PrintPRO, one drew a rectangle with DrawLine()
        ' and passing true for the "box" parameter. PrintPRO v4 supports a similar
        ' functionality with Draw.Line() but using Draw.Rectangle() as below is the
        ' recommended method.
        job.Draw.Width = 1
        job.Draw.BackStyle = PegasusImaging.WinForms.PrintPro4.BackStyle.Transparent
        job.Draw.ForeColor = Color.Black
        font = New Font("Arial", 8, FontStyle.Bold)

        bounds = New Rectangle(1440, 1440, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - left justified top", font, PegasusImaging.WinForms.PrintPro4.Alignment.LeftJustifyTop, True)

        bounds = New Rectangle(3100, 1440, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - left justified middle", font, PegasusImaging.WinForms.PrintPro4.Alignment.LeftJustifyMiddle, True)

        bounds = New Rectangle(4760, 1440, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - left justified bottom", font, PegasusImaging.WinForms.PrintPro4.Alignment.LeftJustifyBottom, True)

        bounds = New Rectangle(6420, 1440, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - right justified top", font, PegasusImaging.WinForms.PrintPro4.Alignment.RightJustifyTop, True)

        bounds = New Rectangle(1440, 3100, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - right justified middle", font, PegasusImaging.WinForms.PrintPro4.Alignment.RightJustifyMiddle, True)

        bounds = New Rectangle(3100, 3100, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - right justified bottom", font, PegasusImaging.WinForms.PrintPro4.Alignment.RightJustifyBottom, True)

        bounds = New Rectangle(4760, 3100, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - center justified top", font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyTop, True)

        bounds = New Rectangle(6420, 3100, 1440, 1440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - center justified middle", font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyMiddle, True)

        bounds = New Rectangle(1440, 4760, 6420, 440)
        job.Draw.Rectangle(bounds, False)
        job.Draw.TextAligned(bounds, "This is an experiment to see if the printer supports formatted text - center justified bottom", font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyBottom, True)

        ' Print a sample image onto the page using a .NET Image. There is also a PrintDib() method from printing
        ' bitmaps from ImagXpress or other components.
        '
        ' Note: Since "aspect" is true, the source and source size parameters are assumed to be (0,0) and the
        ' size of the original image, respectively. So the below call to PrintImage() is equivalent to:
        ' 
        ' job.PrintImage(picture, new PointF(1640, 5700), new SizeF(5000, 4000), new PointF(0, 0), new SizeF(picture.Width, picture.Height), false)
        job.PrintImage(picture, New PointF(1640, 5700), New SizeF(5000, 4000), True)

        ' Use drawing methods to draw an arrow
        job.Draw.FillStyle = PegasusImaging.WinForms.PrintPro4.FillStyle.Solid
        job.Draw.Width = 15
        Dim polyPoints As Point() = {New Point(8390, 7575), New Point(6770, 7575), New Point(6870, 7475), New Point(6870, 7675), New Point(6770, 7575)}
        job.Draw.Polygon(polyPoints)

        ' Draw text inside a gray box with a black border
        job.Draw.FillStyle = PegasusImaging.WinForms.PrintPro4.FillStyle.Transparent
        job.Draw.FillColor = System.Drawing.Color.Gray

        bounds = New Rectangle(8550, 5700, 2000, 3750)
        job.Draw.Rectangle(bounds, System.Drawing.Color.Gray, True)    ' draws the filled gray rectangle
        job.Draw.Rectangle(bounds, System.Drawing.Color.Black, False)  ' draws an unfilled black rectangle as a border

        font = New System.Drawing.Font("Times", 18)
        job.Draw.BackStyle = PegasusImaging.WinForms.PrintPro4.BackStyle.Transparent
        job.Draw.TextAligned(New Rectangle(8550, 5500, 2000, 3750), "Print" + Chr(13) + "Graphics" + Chr(13) + "and" + Chr(13) + "Bitmaps" + Chr(13) + "and" + Chr(13) + "text", font, PegasusImaging.WinForms.PrintPro4.Alignment.CenterJustifyMiddle)

        ' Print the features
        font = New System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Underline Or System.Drawing.FontStyle.Bold)
        job.LeftMargin = 1440
        job.Draw.CurrentX = 1440
        job.Draw.CurrentY = 10000
        job.Draw.Text("Plus!", font)
        font = New System.Drawing.Font("Arial", 24, System.Drawing.FontStyle.Bold)
        job.Draw.Text("Lines, rectangles, circles, ellipse, arc,", font)
        job.Draw.Text("curves, polygons, pie, rounded rectangles,", font)
        job.Draw.Text("Print Dialog, and much, much more!", font)
        job.Draw.Text("PrintPRO is an ATL control so you can use", font)
        job.Draw.Text("it with your favorite dev. tool including IE." + Chr(13), font)
        job.Draw.Text("The PrintPRO Team" + Chr(13), font)
        font = New System.Drawing.Font("Arial", 8)
        job.Draw.CurrentY = job.PrintHeight - job.Draw.TextHeight("This page")
        job.Draw.Text("This page was created in Microsoft Visual Basic .NET using the Pegasus Software PrintPRO .NET control.")

    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        Try
            Dim printer As PegasusImaging.WinForms.PrintPro4.Printer
            Dim job As PegasusImaging.WinForms.PrintPro4.PrintJob

            ' Prompt the user for the desired printer.
            ' The dialog can be circumvented by specifying a printer name as a string
            ' or by specifying only the PrintPro object to select the default printer.
            printer = PegasusImaging.WinForms.PrintPro4.Printer.SelectPrinter(pp, True)

            ' Set wait cursor

            Dim destinationfile As System.String = System.IO.Path.Combine(Application.StartupPath, "PrintToFile.tif")
            If (System.IO.File.Exists(destinationfile)) Then
                System.IO.File.Delete(destinationfile)
            End If
            Me.Cursor = Cursors.WaitCursor

            If printToFile Then

                ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit

                ' Create a file printer
                printer = PegasusImaging.WinForms.PrintPro4.Printer.CreateFilePrinter(pp, destinationfile, PegasusImaging.WinForms.PrintPro4.SaveFileType.Tif)

                ' Set the DPI so the generated file is not too large. The resulting image will be the selected
                ' DPI times the printable area (slightly smaller than 8.5x11 because of margins.
                printer.RequestedDpi = 100

            End If

            If Not printer Is Nothing Then


                ' Create a PrintJob object for the above-selected Printer. If the printer is not specified, PrintPRO
                ' will just use Windows's default printer.
                job = New PegasusImaging.WinForms.PrintPro4.PrintJob(printer)
                job.Name = "Pegasus PrintPRO Demo"

                ' Print all of the information on the page
                PrintPage(job)

                ' Finish the print job to end the current page and print the document.
                job.Finish()

                If printToFile Then

                    Try
                        ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, destinationfile)
                    Catch ex As PegasusImaging.WinForms.ImagXpress9.ImageXException
                        MessageBox.Show(ex.Result)
                    End Try

                End If
            End If

        Catch ex As System.Exception
            PegasusError(ex, lblError)
        End Try
        ' Restore default cursor
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub buttonPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrintPreview.Click



        Dim printer As PegasusImaging.WinForms.PrintPro4.Printer
        Dim job As PegasusImaging.WinForms.PrintPro4.PrintPreviewJob

        ' Set wait cursor
        Me.Cursor = Cursors.WaitCursor

        Try
            ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.CropImage
            ' Prompt the user for the desired printer.
            printer = PegasusImaging.WinForms.PrintPro4.Printer.SelectPrinter(pp, True)

            ' Create a print preview job for the above-specified printer.
            job = New PegasusImaging.WinForms.PrintPro4.PrintPreviewJob(printer)

            ' Draw some sample text and graphics on the page.
            PrintPage(job)

            ' Set the preview scale that the user requested.
            If radioScale1to1.Checked Then
                job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to1
            ElseIf radioScale1to2.Checked Then
                job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to2
            ElseIf radioScale1to3.Checked Then
                job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to3
            ElseIf radioScale1to4.Checked Then
                job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to4
            ElseIf radioScale1to5.Checked Then
                job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to5
            End If

            ' Finish the print job to end the current page and "print" the document.
            job.Finish()

            ' Display the generated preview image by getting a .NET Bitmap from the PrintPreviewJob
            ' and converting it into an ImagXpress ImageX object.
            ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromBitmap(ix, job.GetPrintPreviewBitmap(1))

        Catch ex As Exception
            PegasusError(ex, lblError)
        End Try

        ' Restore default cursor
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub radioPrintToFile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radioPrintToFile.CheckedChanged

        If radioPrintToFile.Checked Then
            printToFile = True
            btnPrint.Enabled = True
            grpPrintPreview.Enabled = False
        End If

    End Sub

    Private Sub radioPrintToPrinter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radioPrintToPrinter.CheckedChanged

        If radioPrintToPrinter.Checked Then
            printToFile = False
            btnPrint.Enabled = True
            grpPrintPreview.Enabled = False
        End If

    End Sub

    Private Sub radioPrintPreview_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radioPrintPreview.CheckedChanged

        If radioPrintPreview.Checked Then
            printToFile = False
            btnPrint.Enabled = False
            grpPrintPreview.Enabled = True
        End If

    End Sub

    Private Sub miOpenBitmap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miOpenBitmap.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()

        If strtemp.Length <> 0 Then
            Try
                samplePicture = strtemp
            Catch ex As System.Exception
                PegasusError(ex, lblError)
            End Try
        End If

    End Sub

    Private Sub miQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miQuit.Click
        Application.Exit()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        pp.AboutBox()
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        ix.AboutBox()
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.txtDuplex = New System.Windows.Forms.TextBox
        Me.lblDuplex = New System.Windows.Forms.Label
        Me.txtCopies = New System.Windows.Forms.TextBox
        Me.lblCopies = New System.Windows.Forms.Label
        Me.lblPapers = New System.Windows.Forms.Label
        Me.lblBins = New System.Windows.Forms.Label
        Me.lbPapers = New System.Windows.Forms.ListBox
        Me.miAbout = New System.Windows.Forms.ToolStripMenuItem
        Me.exitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.miFile = New System.Windows.Forms.ToolStripMenuItem
        Me.lbBins = New System.Windows.Forms.ListBox
        Me.btnGo = New System.Windows.Forms.Button
        Me.mnuMain = New System.Windows.Forms.MenuStrip
        Me.lblError = New System.Windows.Forms.Label
        Me.pp = New PegasusImaging.WinForms.PrintPro4.PrintPro(Me.components)
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.mnuMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDuplex
        '
        Me.txtDuplex.Location = New System.Drawing.Point(346, 201)
        Me.txtDuplex.Name = "txtDuplex"
        Me.txtDuplex.ReadOnly = True
        Me.txtDuplex.Size = New System.Drawing.Size(77, 20)
        Me.txtDuplex.TabIndex = 23
        '
        'lblDuplex
        '
        Me.lblDuplex.AutoSize = True
        Me.lblDuplex.Location = New System.Drawing.Point(343, 185)
        Me.lblDuplex.Name = "lblDuplex"
        Me.lblDuplex.Size = New System.Drawing.Size(46, 13)
        Me.lblDuplex.TabIndex = 22
        Me.lblDuplex.Text = "Duplex?"
        '
        'txtCopies
        '
        Me.txtCopies.Location = New System.Drawing.Point(346, 151)
        Me.txtCopies.Name = "txtCopies"
        Me.txtCopies.ReadOnly = True
        Me.txtCopies.Size = New System.Drawing.Size(77, 20)
        Me.txtCopies.TabIndex = 21
        '
        'lblCopies
        '
        Me.lblCopies.AutoSize = True
        Me.lblCopies.Location = New System.Drawing.Point(343, 135)
        Me.lblCopies.Name = "lblCopies"
        Me.lblCopies.Size = New System.Drawing.Size(52, 13)
        Me.lblCopies.TabIndex = 20
        Me.lblCopies.Text = "# Copies:"
        '
        'lblPapers
        '
        Me.lblPapers.AutoSize = True
        Me.lblPapers.Location = New System.Drawing.Point(176, 135)
        Me.lblPapers.Name = "lblPapers"
        Me.lblPapers.Size = New System.Drawing.Size(112, 13)
        Me.lblPapers.TabIndex = 19
        Me.lblPapers.Text = "Supported Papers(ID):"
        '
        'lblBins
        '
        Me.lblBins.AutoSize = True
        Me.lblBins.Location = New System.Drawing.Point(9, 135)
        Me.lblBins.Name = "lblBins"
        Me.lblBins.Size = New System.Drawing.Size(99, 13)
        Me.lblBins.TabIndex = 18
        Me.lblBins.Text = "Supported Bins(ID):"
        '
        'lbPapers
        '
        Me.lbPapers.FormattingEnabled = True
        Me.lbPapers.Location = New System.Drawing.Point(179, 151)
        Me.lbPapers.Name = "lbPapers"
        Me.lbPapers.Size = New System.Drawing.Size(128, 238)
        Me.lbPapers.TabIndex = 17
        '
        'miAbout
        '
        Me.miAbout.Name = "miAbout"
        Me.miAbout.Size = New System.Drawing.Size(48, 20)
        Me.miAbout.Text = "&About"
        '
        'exitToolStripMenuItem
        '
        Me.exitToolStripMenuItem.Name = "exitToolStripMenuItem"
        Me.exitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.exitToolStripMenuItem.Text = "E&xit"
        '
        'miFile
        '
        Me.miFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.exitToolStripMenuItem})
        Me.miFile.Name = "miFile"
        Me.miFile.Size = New System.Drawing.Size(35, 20)
        Me.miFile.Text = "&File"
        '
        'lbBins
        '
        Me.lbBins.FormattingEnabled = True
        Me.lbBins.Location = New System.Drawing.Point(12, 151)
        Me.lbBins.Name = "lbBins"
        Me.lbBins.Size = New System.Drawing.Size(128, 238)
        Me.lbBins.TabIndex = 16
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(138, 91)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(222, 29)
        Me.btnGo.TabIndex = 15
        Me.btnGo.Text = "Get Printer Capabilities..."
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miFile, Me.miAbout})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(478, 24)
        Me.mnuMain.TabIndex = 13
        Me.mnuMain.Text = "menuStrip1"
        '
        'lblError
        '
        Me.lblError.Location = New System.Drawing.Point(297, 430)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(154, 23)
        Me.lblError.TabIndex = 24
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1) Retrieving the printer capabilities via the PaperBins, Papers, Copies and", "DuplexAvailable properties."})
        Me.ListBox1.Location = New System.Drawing.Point(12, 27)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(454, 56)
        Me.ListBox1.TabIndex = 25
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 480)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.txtDuplex)
        Me.Controls.Add(Me.lblDuplex)
        Me.Controls.Add(Me.txtCopies)
        Me.Controls.Add(Me.lblCopies)
        Me.Controls.Add(Me.lblPapers)
        Me.Controls.Add(Me.lblBins)
        Me.Controls.Add(Me.lbPapers)
        Me.Controls.Add(Me.lbBins)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.mnuMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PrintPRO Printer Capabilities Sample"
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents txtDuplex As System.Windows.Forms.TextBox
    Private WithEvents lblDuplex As System.Windows.Forms.Label
    Private WithEvents txtCopies As System.Windows.Forms.TextBox
    Private WithEvents lblCopies As System.Windows.Forms.Label
    Private WithEvents lblPapers As System.Windows.Forms.Label
    Private WithEvents lblBins As System.Windows.Forms.Label
    Private WithEvents lbPapers As System.Windows.Forms.ListBox
    Private WithEvents miAbout As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents exitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents miFile As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents lbBins As System.Windows.Forms.ListBox
    Private WithEvents btnGo As System.Windows.Forms.Button
    Private WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents pp As PegasusImaging.WinForms.PrintPro4.PrintPro
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox

End Class

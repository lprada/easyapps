namespace ICMProfiles
{
    public partial class frmMain
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.renderintent = new System.Windows.Forms.GroupBox();
            this.OptNone = new System.Windows.Forms.RadioButton();
            this.OptMetric = new System.Windows.Forms.RadioButton();
            this.optbus = new System.Windows.Forms.RadioButton();
            this.optGraph = new System.Windows.Forms.RadioButton();
            this.optImage = new System.Windows.Forms.RadioButton();
            this.chkProof = new System.Windows.Forms.CheckBox();
            this.btnPreview = new System.Windows.Forms.Button();
            this.ix = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ixvPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.lblError = new System.Windows.Forms.Label();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToPrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMonitorProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadPrinterProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printProToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pp = new PegasusImaging.WinForms.PrintPro4.PrintPro(this.components);
            this.renderintent.SuspendLayout();
            this.mnuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // renderintent
            // 
            this.renderintent.Controls.Add(this.OptNone);
            this.renderintent.Controls.Add(this.OptMetric);
            this.renderintent.Controls.Add(this.optbus);
            this.renderintent.Controls.Add(this.optGraph);
            this.renderintent.Controls.Add(this.optImage);
            this.renderintent.Location = new System.Drawing.Point(403, 205);
            this.renderintent.Name = "renderintent";
            this.renderintent.Size = new System.Drawing.Size(208, 170);
            this.renderintent.TabIndex = 2;
            this.renderintent.TabStop = false;
            this.renderintent.Text = "ICM Render Intent";
            // 
            // OptNone
            // 
            this.OptNone.Location = new System.Drawing.Point(6, 139);
            this.OptNone.Name = "OptNone";
            this.OptNone.Size = new System.Drawing.Size(160, 24);
            this.OptNone.TabIndex = 4;
            this.OptNone.Text = "None";
            // 
            // OptMetric
            // 
            this.OptMetric.Location = new System.Drawing.Point(6, 109);
            this.OptMetric.Name = "OptMetric";
            this.OptMetric.Size = new System.Drawing.Size(184, 24);
            this.OptMetric.TabIndex = 3;
            this.OptMetric.Text = "AbsoluteColormetric";
            // 
            // optbus
            // 
            this.optbus.Location = new System.Drawing.Point(6, 79);
            this.optbus.Name = "optbus";
            this.optbus.Size = new System.Drawing.Size(144, 24);
            this.optbus.TabIndex = 2;
            this.optbus.Text = "Business";
            // 
            // optGraph
            // 
            this.optGraph.Location = new System.Drawing.Point(6, 49);
            this.optGraph.Name = "optGraph";
            this.optGraph.Size = new System.Drawing.Size(144, 24);
            this.optGraph.TabIndex = 1;
            this.optGraph.Text = "Graphics";
            // 
            // optImage
            // 
            this.optImage.Checked = true;
            this.optImage.Location = new System.Drawing.Point(6, 19);
            this.optImage.Name = "optImage";
            this.optImage.Size = new System.Drawing.Size(152, 24);
            this.optImage.TabIndex = 0;
            this.optImage.TabStop = true;
            this.optImage.Text = "Images";
            // 
            // chkProof
            // 
            this.chkProof.Checked = true;
            this.chkProof.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProof.Location = new System.Drawing.Point(403, 381);
            this.chkProof.Name = "chkProof";
            this.chkProof.Size = new System.Drawing.Size(136, 32);
            this.chkProof.TabIndex = 3;
            this.chkProof.Text = "Want Proof?";
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(403, 433);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(136, 40);
            this.btnPreview.TabIndex = 4;
            this.btnPreview.Text = "Preview the Image";
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // ixvPreview
            // 
            this.ixvPreview.AutoScroll = true;
            this.ixvPreview.Location = new System.Drawing.Point(12, 205);
            this.ixvPreview.MouseWheelCapture = false;
            this.ixvPreview.Name = "ixvPreview";
            this.ixvPreview.Size = new System.Drawing.Size(373, 338);
            this.ixvPreview.TabIndex = 9;
            // 
            // txtInfo
            // 
            this.txtInfo.BackColor = System.Drawing.SystemColors.Window;
            this.txtInfo.Location = new System.Drawing.Point(12, 27);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            this.txtInfo.Size = new System.Drawing.Size(599, 172);
            this.txtInfo.TabIndex = 10;
            this.txtInfo.Text = resources.GetString("txtInfo.Text");
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(409, 493);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(184, 43);
            this.lblError.TabIndex = 12;
            this.lblError.Click += new System.EventHandler(this.lblError_Click);
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(625, 24);
            this.mnuMain.TabIndex = 13;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToPrintToolStripMenuItem,
            this.loadMonitorProfileToolStripMenuItem,
            this.loadPrinterProfileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openImageToPrintToolStripMenuItem
            // 
            this.openImageToPrintToolStripMenuItem.Name = "openImageToPrintToolStripMenuItem";
            this.openImageToPrintToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.openImageToPrintToolStripMenuItem.Text = "&Open Image to print...";
            this.openImageToPrintToolStripMenuItem.Click += new System.EventHandler(this.openImageToPrintToolStripMenuItem_Click);
            // 
            // loadMonitorProfileToolStripMenuItem
            // 
            this.loadMonitorProfileToolStripMenuItem.Name = "loadMonitorProfileToolStripMenuItem";
            this.loadMonitorProfileToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.loadMonitorProfileToolStripMenuItem.Text = "&Load Monitor Profile";
            this.loadMonitorProfileToolStripMenuItem.Click += new System.EventHandler(this.loadMonitorProfileToolStripMenuItem_Click);
            // 
            // loadPrinterProfileToolStripMenuItem
            // 
            this.loadPrinterProfileToolStripMenuItem.Name = "loadPrinterProfileToolStripMenuItem";
            this.loadPrinterProfileToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.loadPrinterProfileToolStripMenuItem.Text = "&Load Printer Profile";
            this.loadPrinterProfileToolStripMenuItem.Click += new System.EventHandler(this.loadPrinterProfileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imagXpressToolStripMenuItem,
            this.printProToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.imagXpressToolStripMenuItem.Text = "Imag&Xpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click_1);
            // 
            // printProToolStripMenuItem
            // 
            this.printProToolStripMenuItem.Name = "printProToolStripMenuItem";
            this.printProToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.printProToolStripMenuItem.Text = "&PrintPro";
            this.printProToolStripMenuItem.Click += new System.EventHandler(this.printProToolStripMenuItem_Click_1);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(625, 545);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.ixvPreview);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.chkProof);
            this.Controls.Add(this.renderintent);
            this.Controls.Add(this.mnuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ICM Profiles";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.renderintent.ResumeLayout(false);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.ComponentModel.IContainer components;

        private System.Windows.Forms.GroupBox renderintent;
        private System.Windows.Forms.RadioButton optImage;
        private System.Windows.Forms.RadioButton optGraph;
        private System.Windows.Forms.RadioButton OptMetric;
        private System.Windows.Forms.RadioButton OptNone;
        private System.Windows.Forms.CheckBox chkProof;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.RadioButton optbus;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress ix;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView ixvPreview;

        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToPrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadMonitorProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadPrinterProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printProToolStripMenuItem;
        private PegasusImaging.WinForms.PrintPro4.PrintPro pp;

    }
}
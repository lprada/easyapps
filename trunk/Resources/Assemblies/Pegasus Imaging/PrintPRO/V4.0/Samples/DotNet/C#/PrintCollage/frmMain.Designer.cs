namespace PrintCollageCSharp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printProToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ix = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ixvCollage = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.btnGo = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.pp = new PegasusImaging.WinForms.PrintPro4.PrintPro(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.mnuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.miAbout});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(478, 24);
            this.mnuMain.TabIndex = 0;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(92, 22);
            this.miExit.Text = "E&xit";
            this.miExit.Click += new System.EventHandler(this.miExit_Click);
            // 
            // miAbout
            // 
            this.miAbout.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imagXpressToolStripMenuItem,
            this.printProToolStripMenuItem});
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(48, 20);
            this.miAbout.Text = "About";
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.imagXpressToolStripMenuItem.Text = "Imag&Xpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click);
            // 
            // printProToolStripMenuItem
            // 
            this.printProToolStripMenuItem.Name = "printProToolStripMenuItem";
            this.printProToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.printProToolStripMenuItem.Text = "&PrintPro";
            this.printProToolStripMenuItem.Click += new System.EventHandler(this.printProToolStripMenuItem_Click);
            // 
            // ixvCollage
            // 
            this.ixvCollage.AutoScroll = true;
            this.ixvCollage.Location = new System.Drawing.Point(12, 110);
            this.ixvCollage.MouseWheelCapture = false;
            this.ixvCollage.Name = "ixvCollage";
            this.ixvCollage.Size = new System.Drawing.Size(237, 243);
            this.ixvCollage.TabIndex = 2;
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(285, 121);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(127, 40);
            this.btnGo.TabIndex = 3;
            this.btnGo.Text = "Create Collage";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(282, 287);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(166, 53);
            this.lblError.TabIndex = 4;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1) Using the template interface which includes the TemplateFile property to load " +
                "in a",
            " file containing the coordinates and the PrintTemplateImage and PrintTemplateDib " +
                "",
            "methods to print the images."});
            this.listBox1.Location = new System.Drawing.Point(12, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(454, 69);
            this.listBox1.TabIndex = 5;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 365);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.ixvCollage);
            this.Controls.Add(this.mnuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mnuMain;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PrintPRO Collage Sample";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miExit;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress ix;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView ixvCollage;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.ToolStripMenuItem imagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printProToolStripMenuItem;
        private System.Windows.Forms.Label lblError;
        private PegasusImaging.WinForms.PrintPro4.PrintPro pp;
        private System.Windows.Forms.ListBox listBox1;
    }
}


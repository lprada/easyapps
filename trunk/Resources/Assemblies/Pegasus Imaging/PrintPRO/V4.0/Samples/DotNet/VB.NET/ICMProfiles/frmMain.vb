'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************
Public Class frmMain

    Dim defaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"
    Dim icmFilter As String = "Image Files(*.ICC;*.ICM;)|*.ICC;*.ICM;|All files (*.*)|*.*"

    Dim sourceImage As PegasusImaging.WinForms.ImagXpress9.ImageX
    Dim samplepicture As String
    Dim icmMonitor, icmPrinter As String


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "..\..\..\..\..\..\..\..\..\Common\Images")

    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region


    Private Function getSampleImage(ByVal image As String) As String
        Return Application.StartupPath + "..\..\..\..\..\..\..\..\..\Common\Images\" + image
    End Function

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' *** You must unlock the controls with your provided unlock codes for distribution. ***
        'ix.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)
        'pp.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)

        ixvPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit

        sourceImage = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ix, getSampleImage("window.jpg"))

        DoPreview()

    End Sub

    ''' <summary>
    ''' Print previews a sample bitmap using the user-specified ICM profile(s).
    ''' </summary>
    Private Sub DoPreview()

        Try
            Dim job As PegasusImaging.WinForms.PrintPro4.PrintPreviewJob
            Dim size As SizeF
            Dim icm As PegasusImaging.WinForms.PrintPro4.Icm

            ' Set the user-specified ICM values
            icm = New PegasusImaging.WinForms.PrintPro4.Icm()

            If String.IsNullOrEmpty(icmMonitor) Then
                icmMonitor = icm.MonitorProfile   ' Just save the default value
            Else
                icm.MonitorProfile = icmMonitor   ' User the user-specified value
            End If

            If String.IsNullOrEmpty(icmPrinter) Then
                icmPrinter = icm.PrinterProfile   ' Just save the default value
            Else
                icm.PrinterProfile = icmPrinter   ' User the user-specified value
            End If

            icm.WantProof = chkProof.Checked

            ' Find out what ICM intent the user picked
            If optImage.Checked Then
                icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.Images
            ElseIf optbus.Checked Then
                icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.Business
            ElseIf optGraph.Checked Then
                icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.Graphics
            ElseIf OptMetric.Checked Then
                icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.AbsoluteColormetric
            ElseIf OptNone.Checked Then
                icm.RenderIntent = PegasusImaging.WinForms.PrintPro4.IcmRenderIntent.None
            End If

            ' Do the preview

            job = New PegasusImaging.WinForms.PrintPro4.PrintPreviewJob(pp, icm)
            job.PrintPreviewScale = PegasusImaging.WinForms.PrintPro4.PrintPreviewScale.Scale1to1

            size = New SizeF(job.PrintWidth - job.LeftMargin, job.PrintHeight - job.TopMargin - job.BottomMargin)
            job.PrintBitmap(sourceImage.ToBitmap(False), New PointF(job.LeftMargin, job.TopMargin), size, True)

            ixvPreview.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromBitmap(ix, job.GetPrintPreviewBitmap(1))

        Catch ex As Exception
            PegasusError(ex, lblError)
        End Try
    End Sub

    Private Sub miLoadMonitor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miLoadMonitor.Click

        Dim ofd As OpenFileDialog = New OpenFileDialog()

        ofd.InitialDirectory = System.Environment.SystemDirectory + "\spool\drivers\color"
        ofd.Filter = icmFilter
        ofd.ShowDialog()

        icmMonitor = ofd.FileName

        chkProof.Checked = True

    End Sub

    Private Sub miLoadPrinter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miLoadPrinter.Click

        Dim ofd As OpenFileDialog = New OpenFileDialog()

        ofd.InitialDirectory = System.Environment.SystemDirectory + "\spool\drivers\color"
        ofd.Filter = icmFilter
        ofd.ShowDialog()

        icmPrinter = ofd.FileName

        chkProof.Checked = True

    End Sub

    Private Sub miOpenImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miOpenImage.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()

        If strtemp.Length <> 0 Then
            Try
                samplePicture = strtemp
            Catch ex As System.Exception
                PegasusError(ex, lblError)
            End Try
        End If

        chkProof.Checked = True

    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        DoPreview()
    End Sub

    Private Sub miExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miExit.Click
        Application.Exit()
    End Sub

    Private Sub PrintProToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintProToolStripMenuItem.Click
        pp.AboutBox()
    End Sub

    Private Sub ImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressToolStripMenuItem.Click
        ix.AboutBox()
    End Sub
End Class

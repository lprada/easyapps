'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Public Class MainScreen
    Inherits System.Windows.Forms.Form

    Dim regOpts As PegasusImaging.WinForms.ScanFix5.ImageRegistrationOptions
    Dim regResults As PegasusImaging.WinForms.ScanFix5.ImageRegistrationResults

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()


        '***Must call the UnlockControl method before the InitializeComponent statement
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        'PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234)


        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub
    '** Create the options variables here**
    Dim deskewopt As PegasusImaging.WinForms.ScanFix5.DeskewOptions
    Dim deskewres As PegasusImaging.WinForms.ScanFix5.DeskewResults

    Dim blobopt As PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions
    Dim despeckopt As PegasusImaging.WinForms.ScanFix5.DespeckleOptions
    Dim inverseopt As PegasusImaging.WinForms.ScanFix5.InverseTextOptions

    'Form overrides dispose to clean up the component list.

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            If Not ImageXView1.Image Is Nothing Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If

            If Not ImageXView2.Image Is Nothing Then
                ImageXView2.Image.Dispose()
                ImageXView2.Image = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (ImageXView2 Is Nothing) Then
                ImageXView2.Dispose()
                ImageXView2 = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ScanFix1 Is Nothing) Then
                ScanFix1.Dispose()
                ScanFix1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If

        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents cd As System.Windows.Forms.OpenFileDialog
    Private WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents options As System.Windows.Forms.GroupBox
    Friend WithEvents lblinput As System.Windows.Forms.Label
    Friend WithEvents lblout As System.Windows.Forms.Label
    'Friend WithEvents ScanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents HScrollBar1 As System.Windows.Forms.HScrollBar
    Friend WithEvents lblPad As System.Windows.Forms.Label
    Friend WithEvents txtangle As System.Windows.Forms.TextBox
    Friend WithEvents lblcon As System.Windows.Forms.Label
    Friend WithEvents lblval As System.Windows.Forms.Label
    Friend WithEvents lblmax As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabdeskew As System.Windows.Forms.TabPage
    Friend WithEvents tabdespeck As System.Windows.Forms.TabPage
    Friend WithEvents tabblob As System.Windows.Forms.TabPage
    Friend WithEvents tabinverse As System.Windows.Forms.TabPage
    Friend WithEvents cmddekew As System.Windows.Forms.Button
    Friend WithEvents cmdblob As System.Windows.Forms.Button
    Friend WithEvents scrollm As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollmin As System.Windows.Forms.HScrollBar
    Friend WithEvents scrolh As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollw As System.Windows.Forms.HScrollBar
    Friend WithEvents scrolly As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollx As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollden As System.Windows.Forms.HScrollBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents speckheight As System.Windows.Forms.HScrollBar
    Friend WithEvents speckwidth As System.Windows.Forms.HScrollBar
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cmdd As System.Windows.Forms.Button
    Friend WithEvents minwidth As System.Windows.Forms.HScrollBar
    Friend WithEvents minheight As System.Windows.Forms.HScrollBar
    Friend WithEvents minblack As System.Windows.Forms.HScrollBar
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cmdinverse As System.Windows.Forms.Button
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImageXView2 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents tabblank As System.Windows.Forms.TabPage
    Friend WithEvents tabcomb As System.Windows.Forms.TabPage
    Friend WithEvents tabdotshade As System.Windows.Forms.TabPage
    Friend WithEvents tabnegpage As System.Windows.Forms.TabPage
    Friend WithEvents tabline As System.Windows.Forms.TabPage
    Friend WithEvents tabdilate As System.Windows.Forms.TabPage
    Friend WithEvents taberode As System.Windows.Forms.TabPage
    Friend WithEvents tabsmooth As System.Windows.Forms.TabPage
    Friend WithEvents tabborder As System.Windows.Forms.TabPage
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar3 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar4 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar5 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar6 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar7 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar8 As System.Windows.Forms.HScrollBar
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents cmdBlank As System.Windows.Forms.Button
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar9 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar10 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar11 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar12 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar13 As System.Windows.Forms.HScrollBar
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar2 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar14 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar15 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar16 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar17 As System.Windows.Forms.HScrollBar
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar18 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar19 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar20 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar21 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar22 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar23 As System.Windows.Forms.HScrollBar
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar24 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar25 As System.Windows.Forms.HScrollBar
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar26 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar27 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar28 As System.Windows.Forms.HScrollBar
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar29 As System.Windows.Forms.HScrollBar
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents HScrollBar30 As System.Windows.Forms.HScrollBar
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar31 As System.Windows.Forms.HScrollBar
    Friend WithEvents CheckBox10 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox11 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox12 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox13 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox14 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox15 As System.Windows.Forms.CheckBox
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar33 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar34 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar35 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar36 As System.Windows.Forms.HScrollBar
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar38 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar39 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar40 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar41 As System.Windows.Forms.HScrollBar
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents CheckBox16 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox17 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox18 As System.Windows.Forms.CheckBox
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar32 As System.Windows.Forms.HScrollBar
    Friend WithEvents listBox1 As System.Windows.Forms.ListBox
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents OpenMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents AboutMenu As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ScanFixMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ScanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Friend WithEvents RegisterTab As System.Windows.Forms.TabPage
    Friend WithEvents HorizontalGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents VerticalGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents RegisterButton As System.Windows.Forms.Button
    Friend WithEvents HorizontalActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalAddOnlyCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalIgnoreHolesCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalCentralFocusCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalToLineActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalSkipActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalToLineCheckThinLinesActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents HorizontalMinimumActivity As System.Windows.Forms.HScrollBar
    Friend WithEvents HorizontalMinimumActivityLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalMinimumActivityLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalMinimumBackgroundLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMaximumLineThicknessLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMaximumLineThickness As System.Windows.Forms.HScrollBar
    Friend WithEvents HorizontalToLineMaximumLineGapLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMaximumLineGap As System.Windows.Forms.HScrollBar
    Friend WithEvents HorizontalSkipDistanceLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalSkipDistanceLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalResultantMarginLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalResultantMarginLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalResultantMargin As System.Windows.Forms.HScrollBar
    Friend WithEvents HorizontalMinimumForegroundLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalMinimumForegroundLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalMinimumForeground As System.Windows.Forms.HScrollBar
    Friend WithEvents HorizontalMinimumBackgroundLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalMinimumBackground As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VerticalAddOnlyCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VerticalToLineCheckThinLinesActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VerticalSkipActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VerticalToLineActiveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VerticalCentralFocusCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents VerticalMinimumActivity As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalMinimumActivityLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalMinimumActivityLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalMinimumBackground As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalMinimumBackgroundLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalMinimumBackgroundLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalMinimumForeground As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalMinimumForegroundLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalMinimumForegroundLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalResultantMargin As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalSkipDistanceLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMaximumLineGapLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMaximumLineThickness As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalToLineMaximumLineThicknessLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMinimumLineLengthLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMaximumLineThicknessLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMinimumLineLengthLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMinimumLineLength As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalResultantMarginLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalSkipDistance As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalSkipDistanceLabel As System.Windows.Forms.Label
    Friend WithEvents VerticalResultantMarginLabel2 As System.Windows.Forms.Label
    Friend WithEvents VerticalToLineMaximumLineGap As System.Windows.Forms.HScrollBar
    Friend WithEvents VerticalToLineMaximumLineGapLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMinimumLineLengthLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMinimumLineLengthLabel As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMinimumLineLength As System.Windows.Forms.HScrollBar
    Friend WithEvents HorizontalToLineMaximumLineThicknessLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalToLineMaximumLineGapLabel2 As System.Windows.Forms.Label
    Friend WithEvents HorizontalSkipDistance As System.Windows.Forms.HScrollBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cd = New System.Windows.Forms.OpenFileDialog()
        Me.lblinput = New System.Windows.Forms.Label()
        Me.lblout = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.FileMenu = New System.Windows.Forms.MenuItem()
        Me.OpenMenuItem = New System.Windows.Forms.MenuItem()
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem()
        Me.AboutMenu = New System.Windows.Forms.MenuItem()
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem()
        Me.ScanFixMenuItem = New System.Windows.Forms.MenuItem()
        Me.options = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabdeskew = New System.Windows.Forms.TabPage()
        Me.cmddekew = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.lblPad = New System.Windows.Forms.Label()
        Me.lblcon = New System.Windows.Forms.Label()
        Me.lblval = New System.Windows.Forms.Label()
        Me.txtangle = New System.Windows.Forms.TextBox()
        Me.HScrollBar1 = New System.Windows.Forms.HScrollBar()
        Me.lblmax = New System.Windows.Forms.Label()
        Me.tabblob = New System.Windows.Forms.TabPage()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.scrollden = New System.Windows.Forms.HScrollBar()
        Me.scrollm = New System.Windows.Forms.HScrollBar()
        Me.scrollmin = New System.Windows.Forms.HScrollBar()
        Me.scrolh = New System.Windows.Forms.HScrollBar()
        Me.scrollw = New System.Windows.Forms.HScrollBar()
        Me.scrolly = New System.Windows.Forms.HScrollBar()
        Me.scrollx = New System.Windows.Forms.HScrollBar()
        Me.cmdblob = New System.Windows.Forms.Button()
        Me.tabdilate = New System.Windows.Forms.TabPage()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.HScrollBar30 = New System.Windows.Forms.HScrollBar()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.tabborder = New System.Windows.Forms.TabPage()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.CheckBox18 = New System.Windows.Forms.CheckBox()
        Me.CheckBox17 = New System.Windows.Forms.CheckBox()
        Me.CheckBox16 = New System.Windows.Forms.CheckBox()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.HScrollBar33 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar34 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar35 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar36 = New System.Windows.Forms.HScrollBar()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.HScrollBar38 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar39 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar40 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar41 = New System.Windows.Forms.HScrollBar()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.tabdotshade = New System.Windows.Forms.TabPage()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.HScrollBar18 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar19 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar20 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar21 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar22 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar23 = New System.Windows.Forms.HScrollBar()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tabblank = New System.Windows.Forms.TabPage()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.HScrollBar3 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar4 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar5 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar6 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar7 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar8 = New System.Windows.Forms.HScrollBar()
        Me.cmdBlank = New System.Windows.Forms.Button()
        Me.tabsmooth = New System.Windows.Forms.TabPage()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.HScrollBar32 = New System.Windows.Forms.HScrollBar()
        Me.tabnegpage = New System.Windows.Forms.TabPage()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.HScrollBar24 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar25 = New System.Windows.Forms.HScrollBar()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.tabcomb = New System.Windows.Forms.TabPage()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.HScrollBar2 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar14 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar15 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar16 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar17 = New System.Windows.Forms.HScrollBar()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.HScrollBar9 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar10 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar11 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar12 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar13 = New System.Windows.Forms.HScrollBar()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tabinverse = New System.Windows.Forms.TabPage()
        Me.cmdinverse = New System.Windows.Forms.Button()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.minblack = New System.Windows.Forms.HScrollBar()
        Me.minheight = New System.Windows.Forms.HScrollBar()
        Me.minwidth = New System.Windows.Forms.HScrollBar()
        Me.taberode = New System.Windows.Forms.TabPage()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.HScrollBar31 = New System.Windows.Forms.HScrollBar()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.CheckBox11 = New System.Windows.Forms.CheckBox()
        Me.CheckBox12 = New System.Windows.Forms.CheckBox()
        Me.CheckBox13 = New System.Windows.Forms.CheckBox()
        Me.CheckBox14 = New System.Windows.Forms.CheckBox()
        Me.CheckBox15 = New System.Windows.Forms.CheckBox()
        Me.tabdespeck = New System.Windows.Forms.TabPage()
        Me.cmdd = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.speckwidth = New System.Windows.Forms.HScrollBar()
        Me.speckheight = New System.Windows.Forms.HScrollBar()
        Me.RegisterTab = New System.Windows.Forms.TabPage()
        Me.RegisterButton = New System.Windows.Forms.Button()
        Me.VerticalGroupBox = New System.Windows.Forms.GroupBox()
        Me.VerticalActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.VerticalAddOnlyCheckBox = New System.Windows.Forms.CheckBox()
        Me.VerticalToLineCheckThinLinesActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.VerticalSkipActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.VerticalToLineActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.VerticalCentralFocusCheckBox = New System.Windows.Forms.CheckBox()
        Me.VerticalMinimumActivity = New System.Windows.Forms.HScrollBar()
        Me.VerticalMinimumActivityLabel = New System.Windows.Forms.Label()
        Me.VerticalMinimumActivityLabel2 = New System.Windows.Forms.Label()
        Me.VerticalMinimumBackground = New System.Windows.Forms.HScrollBar()
        Me.VerticalMinimumBackgroundLabel = New System.Windows.Forms.Label()
        Me.VerticalMinimumBackgroundLabel2 = New System.Windows.Forms.Label()
        Me.VerticalMinimumForeground = New System.Windows.Forms.HScrollBar()
        Me.VerticalMinimumForegroundLabel = New System.Windows.Forms.Label()
        Me.VerticalMinimumForegroundLabel2 = New System.Windows.Forms.Label()
        Me.VerticalResultantMargin = New System.Windows.Forms.HScrollBar()
        Me.VerticalSkipDistanceLabel2 = New System.Windows.Forms.Label()
        Me.VerticalToLineMaximumLineGapLabel = New System.Windows.Forms.Label()
        Me.VerticalToLineMaximumLineThickness = New System.Windows.Forms.HScrollBar()
        Me.VerticalToLineMaximumLineThicknessLabel2 = New System.Windows.Forms.Label()
        Me.VerticalToLineMinimumLineLengthLabel2 = New System.Windows.Forms.Label()
        Me.VerticalToLineMaximumLineThicknessLabel = New System.Windows.Forms.Label()
        Me.VerticalToLineMinimumLineLengthLabel = New System.Windows.Forms.Label()
        Me.VerticalToLineMinimumLineLength = New System.Windows.Forms.HScrollBar()
        Me.VerticalResultantMarginLabel = New System.Windows.Forms.Label()
        Me.VerticalSkipDistance = New System.Windows.Forms.HScrollBar()
        Me.VerticalSkipDistanceLabel = New System.Windows.Forms.Label()
        Me.VerticalResultantMarginLabel2 = New System.Windows.Forms.Label()
        Me.VerticalToLineMaximumLineGap = New System.Windows.Forms.HScrollBar()
        Me.VerticalToLineMaximumLineGapLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalGroupBox = New System.Windows.Forms.GroupBox()
        Me.HorizontalToLineMinimumLineLengthLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalToLineMinimumLineLengthLabel = New System.Windows.Forms.Label()
        Me.HorizontalToLineMinimumLineLength = New System.Windows.Forms.HScrollBar()
        Me.HorizontalToLineMaximumLineThicknessLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalToLineMaximumLineThicknessLabel = New System.Windows.Forms.Label()
        Me.HorizontalToLineMaximumLineThickness = New System.Windows.Forms.HScrollBar()
        Me.HorizontalToLineMaximumLineGapLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalToLineMaximumLineGapLabel = New System.Windows.Forms.Label()
        Me.HorizontalToLineMaximumLineGap = New System.Windows.Forms.HScrollBar()
        Me.HorizontalSkipDistanceLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalSkipDistanceLabel = New System.Windows.Forms.Label()
        Me.HorizontalSkipDistance = New System.Windows.Forms.HScrollBar()
        Me.HorizontalResultantMarginLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalResultantMarginLabel = New System.Windows.Forms.Label()
        Me.HorizontalResultantMargin = New System.Windows.Forms.HScrollBar()
        Me.HorizontalMinimumForegroundLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalMinimumForegroundLabel = New System.Windows.Forms.Label()
        Me.HorizontalMinimumForeground = New System.Windows.Forms.HScrollBar()
        Me.HorizontalMinimumBackgroundLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalMinimumBackgroundLabel = New System.Windows.Forms.Label()
        Me.HorizontalMinimumBackground = New System.Windows.Forms.HScrollBar()
        Me.HorizontalMinimumActivityLabel2 = New System.Windows.Forms.Label()
        Me.HorizontalMinimumActivityLabel = New System.Windows.Forms.Label()
        Me.HorizontalMinimumActivity = New System.Windows.Forms.HScrollBar()
        Me.HorizontalToLineActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.HorizontalSkipActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.HorizontalIgnoreHolesCheckBox = New System.Windows.Forms.CheckBox()
        Me.HorizontalCentralFocusCheckBox = New System.Windows.Forms.CheckBox()
        Me.HorizontalAddOnlyCheckBox = New System.Windows.Forms.CheckBox()
        Me.HorizontalActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.HorizontalToLineCheckThinLinesActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.tabline = New System.Windows.Forms.TabPage()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.HScrollBar29 = New System.Windows.Forms.HScrollBar()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.HScrollBar26 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar27 = New System.Windows.Forms.HScrollBar()
        Me.HScrollBar28 = New System.Windows.Forms.HScrollBar()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImageXView2 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.listBox1 = New System.Windows.Forms.ListBox()
        Me.ScanFix1 = New PegasusImaging.WinForms.ScanFix5.ScanFix(Me.components)
        Me.options.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabdeskew.SuspendLayout()
        Me.tabblob.SuspendLayout()
        Me.tabdilate.SuspendLayout()
        Me.tabborder.SuspendLayout()
        Me.tabdotshade.SuspendLayout()
        Me.tabblank.SuspendLayout()
        Me.tabsmooth.SuspendLayout()
        Me.tabnegpage.SuspendLayout()
        Me.tabcomb.SuspendLayout()
        Me.tabinverse.SuspendLayout()
        Me.taberode.SuspendLayout()
        Me.tabdespeck.SuspendLayout()
        Me.RegisterTab.SuspendLayout()
        Me.VerticalGroupBox.SuspendLayout()
        Me.HorizontalGroupBox.SuspendLayout()
        Me.tabline.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblinput
        '
        Me.lblinput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblinput.Location = New System.Drawing.Point(112, 88)
        Me.lblinput.Name = "lblinput"
        Me.lblinput.Size = New System.Drawing.Size(128, 16)
        Me.lblinput.TabIndex = 3
        Me.lblinput.Text = "Input Image"
        Me.lblinput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblout
        '
        Me.lblout.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblout.Location = New System.Drawing.Point(424, 88)
        Me.lblout.Name = "lblout"
        Me.lblout.Size = New System.Drawing.Size(184, 16)
        Me.lblout.TabIndex = 4
        Me.lblout.Text = "Output Image"
        Me.lblout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.AboutMenu})
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.OpenMenuItem, Me.ExitMenuItem})
        Me.FileMenu.Text = "&File"
        '
        'OpenMenuItem
        '
        Me.OpenMenuItem.Index = 0
        Me.OpenMenuItem.Text = "&Open"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 1
        Me.ExitMenuItem.Text = "E&xit"
        '
        'AboutMenu
        '
        Me.AboutMenu.Index = 1
        Me.AboutMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ImagXpressMenuItem, Me.ScanFixMenuItem})
        Me.AboutMenu.Text = "&About"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 0
        Me.ImagXpressMenuItem.Text = "Imag&Xpress"
        '
        'ScanFixMenuItem
        '
        Me.ScanFixMenuItem.Index = 1
        Me.ScanFixMenuItem.Text = "Scan&Fix"
        '
        'options
        '
        Me.options.Controls.AddRange(New System.Windows.Forms.Control() {Me.TabControl1})
        Me.options.Location = New System.Drawing.Point(8, 384)
        Me.options.Name = "options"
        Me.options.Size = New System.Drawing.Size(664, 528)
        Me.options.TabIndex = 7
        Me.options.TabStop = False
        Me.options.Text = "ScanFix Processing Options"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.AddRange(New System.Windows.Forms.Control() {Me.tabdeskew, Me.tabblob, Me.tabdilate, Me.tabborder, Me.tabdotshade, Me.tabblank, Me.tabsmooth, Me.tabnegpage, Me.tabcomb, Me.tabinverse, Me.taberode, Me.tabdespeck, Me.RegisterTab, Me.tabline})
        Me.TabControl1.Location = New System.Drawing.Point(8, 16)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(648, 504)
        Me.TabControl1.TabIndex = 13
        '
        'tabdeskew
        '
        Me.tabdeskew.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmddekew, Me.CheckBox1, Me.ComboBox1, Me.lblPad, Me.lblcon, Me.lblval, Me.txtangle, Me.HScrollBar1, Me.lblmax})
        Me.tabdeskew.Location = New System.Drawing.Point(4, 40)
        Me.tabdeskew.Name = "tabdeskew"
        Me.tabdeskew.Size = New System.Drawing.Size(640, 460)
        Me.tabdeskew.TabIndex = 0
        Me.tabdeskew.Text = "Deskew"
        '
        'cmddekew
        '
        Me.cmddekew.Location = New System.Drawing.Point(328, 96)
        Me.cmddekew.Name = "cmddekew"
        Me.cmddekew.Size = New System.Drawing.Size(64, 40)
        Me.cmddekew.TabIndex = 9
        Me.cmddekew.Text = "Process Image"
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(96, 16)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(136, 16)
        Me.CheckBox1.TabIndex = 1
        Me.CheckBox1.Text = "Maintain Original Size"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"White", "Black"})
        Me.ComboBox1.Location = New System.Drawing.Point(192, 48)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(112, 21)
        Me.ComboBox1.TabIndex = 2
        '
        'lblPad
        '
        Me.lblPad.Location = New System.Drawing.Point(96, 48)
        Me.lblPad.Name = "lblPad"
        Me.lblPad.Size = New System.Drawing.Size(64, 16)
        Me.lblPad.TabIndex = 4
        Me.lblPad.Text = "Pad Color:"
        '
        'lblcon
        '
        Me.lblcon.Location = New System.Drawing.Point(96, 80)
        Me.lblcon.Name = "lblcon"
        Me.lblcon.Size = New System.Drawing.Size(128, 24)
        Me.lblcon.TabIndex = 6
        Me.lblcon.Text = "Minimum Confidence:"
        '
        'lblval
        '
        Me.lblval.Location = New System.Drawing.Point(232, 80)
        Me.lblval.Name = "lblval"
        Me.lblval.Size = New System.Drawing.Size(48, 16)
        Me.lblval.TabIndex = 7
        Me.lblval.Text = "0"
        '
        'txtangle
        '
        Me.txtangle.Location = New System.Drawing.Point(96, 176)
        Me.txtangle.Name = "txtangle"
        Me.txtangle.Size = New System.Drawing.Size(200, 21)
        Me.txtangle.TabIndex = 5
        Me.txtangle.Text = "0.2"
        '
        'HScrollBar1
        '
        Me.HScrollBar1.LargeChange = 1
        Me.HScrollBar1.Location = New System.Drawing.Point(96, 120)
        Me.HScrollBar1.Name = "HScrollBar1"
        Me.HScrollBar1.Size = New System.Drawing.Size(208, 16)
        Me.HScrollBar1.TabIndex = 3
        '
        'lblmax
        '
        Me.lblmax.Location = New System.Drawing.Point(96, 152)
        Me.lblmax.Name = "lblmax"
        Me.lblmax.Size = New System.Drawing.Size(200, 16)
        Me.lblmax.TabIndex = 8
        Me.lblmax.Text = "Maximum Acceptable Angle"
        '
        'tabblob
        '
        Me.tabblob.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label14, Me.Label13, Me.Label12, Me.Label11, Me.Label10, Me.Label9, Me.Label8, Me.Label7, Me.Label6, Me.Label5, Me.Label4, Me.Label3, Me.Label2, Me.Label1, Me.scrollden, Me.scrollm, Me.scrollmin, Me.scrolh, Me.scrollw, Me.scrolly, Me.scrollx, Me.cmdblob})
        Me.tabblob.Location = New System.Drawing.Point(4, 40)
        Me.tabblob.Name = "tabblob"
        Me.tabblob.Size = New System.Drawing.Size(640, 460)
        Me.tabblob.TabIndex = 2
        Me.tabblob.Text = "Blob Remove"
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(360, 208)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(88, 16)
        Me.Label14.TabIndex = 21
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(360, 176)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 16)
        Me.Label13.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(360, 144)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 16)
        Me.Label12.TabIndex = 19
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(360, 112)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 16)
        Me.Label11.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(360, 80)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(64, 16)
        Me.Label10.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(360, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(360, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 16)
        Me.Label8.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(16, 208)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 16)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Minimum Density"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(16, 176)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Max Pixel Count"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(16, 144)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Min Pixel Count"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Area Height"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Area Width"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Area Y"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 16)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Area X"
        '
        'scrollden
        '
        Me.scrollden.LargeChange = 1
        Me.scrollden.Location = New System.Drawing.Point(152, 208)
        Me.scrollden.Name = "scrollden"
        Me.scrollden.Size = New System.Drawing.Size(192, 16)
        Me.scrollden.TabIndex = 7
        Me.scrollden.Value = 50
        '
        'scrollm
        '
        Me.scrollm.LargeChange = 1
        Me.scrollm.Location = New System.Drawing.Point(152, 176)
        Me.scrollm.Maximum = 9999
        Me.scrollm.Name = "scrollm"
        Me.scrollm.Size = New System.Drawing.Size(192, 16)
        Me.scrollm.TabIndex = 6
        Me.scrollm.Value = 9999
        '
        'scrollmin
        '
        Me.scrollmin.LargeChange = 1
        Me.scrollmin.Location = New System.Drawing.Point(152, 144)
        Me.scrollmin.Maximum = 9999
        Me.scrollmin.Minimum = 1
        Me.scrollmin.Name = "scrollmin"
        Me.scrollmin.Size = New System.Drawing.Size(192, 16)
        Me.scrollmin.TabIndex = 5
        Me.scrollmin.Value = 300
        '
        'scrolh
        '
        Me.scrolh.LargeChange = 1
        Me.scrolh.Location = New System.Drawing.Point(152, 112)
        Me.scrolh.Maximum = 9999
        Me.scrolh.Name = "scrolh"
        Me.scrolh.Size = New System.Drawing.Size(192, 16)
        Me.scrolh.TabIndex = 4
        '
        'scrollw
        '
        Me.scrollw.LargeChange = 1
        Me.scrollw.Location = New System.Drawing.Point(152, 80)
        Me.scrollw.Maximum = 9999
        Me.scrollw.Name = "scrollw"
        Me.scrollw.Size = New System.Drawing.Size(192, 16)
        Me.scrollw.TabIndex = 3
        '
        'scrolly
        '
        Me.scrolly.LargeChange = 1
        Me.scrolly.Location = New System.Drawing.Point(152, 48)
        Me.scrolly.Maximum = 9999
        Me.scrolly.Name = "scrolly"
        Me.scrolly.Size = New System.Drawing.Size(192, 16)
        Me.scrolly.TabIndex = 2
        '
        'scrollx
        '
        Me.scrollx.LargeChange = 1
        Me.scrollx.Location = New System.Drawing.Point(152, 16)
        Me.scrollx.Maximum = 9999
        Me.scrollx.Name = "scrollx"
        Me.scrollx.Size = New System.Drawing.Size(192, 16)
        Me.scrollx.TabIndex = 1
        '
        'cmdblob
        '
        Me.cmdblob.Location = New System.Drawing.Point(448, 88)
        Me.cmdblob.Name = "cmdblob"
        Me.cmdblob.Size = New System.Drawing.Size(64, 40)
        Me.cmdblob.TabIndex = 0
        Me.cmdblob.Text = "Process Image"
        '
        'tabdilate
        '
        Me.tabdilate.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button5, Me.Label83, Me.Label82, Me.HScrollBar30, Me.CheckBox9, Me.CheckBox8, Me.CheckBox7, Me.CheckBox6, Me.CheckBox5, Me.CheckBox4})
        Me.tabdilate.Location = New System.Drawing.Point(4, 40)
        Me.tabdilate.Name = "tabdilate"
        Me.tabdilate.Size = New System.Drawing.Size(640, 460)
        Me.tabdilate.TabIndex = 9
        Me.tabdilate.Text = "Dilate"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(472, 96)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(64, 40)
        Me.Button5.TabIndex = 77
        Me.Button5.Text = "Process Image"
        '
        'Label83
        '
        Me.Label83.Location = New System.Drawing.Point(264, 72)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(56, 16)
        Me.Label83.TabIndex = 9
        Me.Label83.Text = "Label83"
        '
        'Label82
        '
        Me.Label82.Location = New System.Drawing.Point(184, 72)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(56, 16)
        Me.Label82.TabIndex = 8
        Me.Label82.Text = "Amount"
        '
        'HScrollBar30
        '
        Me.HScrollBar30.Location = New System.Drawing.Point(184, 96)
        Me.HScrollBar30.Minimum = 1
        Me.HScrollBar30.Name = "HScrollBar30"
        Me.HScrollBar30.Size = New System.Drawing.Size(200, 16)
        Me.HScrollBar30.TabIndex = 6
        Me.HScrollBar30.Value = 1
        '
        'CheckBox9
        '
        Me.CheckBox9.Location = New System.Drawing.Point(328, 168)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(56, 16)
        Me.CheckBox9.TabIndex = 5
        Me.CheckBox9.Text = "Right"
        '
        'CheckBox8
        '
        Me.CheckBox8.Location = New System.Drawing.Point(328, 136)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(56, 16)
        Me.CheckBox8.TabIndex = 4
        Me.CheckBox8.Text = "Down"
        '
        'CheckBox7
        '
        Me.CheckBox7.Location = New System.Drawing.Point(256, 168)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(48, 16)
        Me.CheckBox7.TabIndex = 3
        Me.CheckBox7.Text = "Left"
        '
        'CheckBox6
        '
        Me.CheckBox6.Location = New System.Drawing.Point(256, 136)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(40, 16)
        Me.CheckBox6.TabIndex = 2
        Me.CheckBox6.Text = "Up"
        '
        'CheckBox5
        '
        Me.CheckBox5.Checked = True
        Me.CheckBox5.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox5.Location = New System.Drawing.Point(184, 152)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(48, 16)
        Me.CheckBox5.TabIndex = 1
        Me.CheckBox5.Text = "All"
        '
        'CheckBox4
        '
        Me.CheckBox4.Enabled = False
        Me.CheckBox4.Location = New System.Drawing.Point(184, 40)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(136, 24)
        Me.CheckBox4.TabIndex = 0
        Me.CheckBox4.Text = "Diagonal Only"
        '
        'tabborder
        '
        Me.tabborder.Controls.AddRange(New System.Windows.Forms.Control() {Me.ComboBox2, Me.Label106, Me.CheckBox18, Me.CheckBox17, Me.CheckBox16, Me.Label87, Me.Label88, Me.Label89, Me.Label90, Me.Label92, Me.Label93, Me.Label94, Me.Label95, Me.HScrollBar33, Me.HScrollBar34, Me.HScrollBar35, Me.HScrollBar36, Me.Label97, Me.Label98, Me.Label99, Me.Label100, Me.Label102, Me.Label103, Me.Label104, Me.Label105, Me.HScrollBar38, Me.HScrollBar39, Me.HScrollBar40, Me.HScrollBar41, Me.Button7})
        Me.tabborder.Location = New System.Drawing.Point(4, 40)
        Me.tabborder.Name = "tabborder"
        Me.tabborder.Size = New System.Drawing.Size(640, 460)
        Me.tabborder.TabIndex = 12
        Me.tabborder.Text = "Border Removal"
        '
        'ComboBox2
        '
        Me.ComboBox2.Items.AddRange(New Object() {"White", "Black"})
        Me.ComboBox2.Location = New System.Drawing.Point(16, 120)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(80, 21)
        Me.ComboBox2.TabIndex = 111
        '
        'Label106
        '
        Me.Label106.Location = New System.Drawing.Point(16, 104)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(72, 16)
        Me.Label106.TabIndex = 110
        Me.Label106.Text = "Pad Color"
        '
        'CheckBox18
        '
        Me.CheckBox18.Checked = True
        Me.CheckBox18.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox18.Location = New System.Drawing.Point(16, 72)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(88, 16)
        Me.CheckBox18.TabIndex = 109
        Me.CheckBox18.Text = "Deskew"
        '
        'CheckBox17
        '
        Me.CheckBox17.Location = New System.Drawing.Point(16, 48)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(104, 16)
        Me.CheckBox17.TabIndex = 108
        Me.CheckBox17.Text = "Replace Border"
        '
        'CheckBox16
        '
        Me.CheckBox16.Checked = True
        Me.CheckBox16.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox16.Location = New System.Drawing.Point(16, 24)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(88, 16)
        Me.CheckBox16.TabIndex = 107
        Me.CheckBox16.Text = "Crop Border"
        '
        'Label87
        '
        Me.Label87.Location = New System.Drawing.Point(512, 152)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(72, 16)
        Me.Label87.TabIndex = 105
        '
        'Label88
        '
        Me.Label88.Location = New System.Drawing.Point(512, 120)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(64, 16)
        Me.Label88.TabIndex = 104
        '
        'Label89
        '
        Me.Label89.Location = New System.Drawing.Point(512, 80)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(80, 16)
        Me.Label89.TabIndex = 103
        '
        'Label90
        '
        Me.Label90.Location = New System.Drawing.Point(512, 40)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(72, 16)
        Me.Label90.TabIndex = 102
        '
        'Label92
        '
        Me.Label92.Location = New System.Drawing.Point(376, 152)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(136, 16)
        Me.Label92.TabIndex = 100
        Me.Label92.Text = "Max Page Height"
        '
        'Label93
        '
        Me.Label93.Location = New System.Drawing.Point(376, 120)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(96, 16)
        Me.Label93.TabIndex = 99
        Me.Label93.Text = "Max Page Width"
        '
        'Label94
        '
        Me.Label94.Location = New System.Drawing.Point(376, 80)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(96, 16)
        Me.Label94.TabIndex = 98
        Me.Label94.Text = "Min Page Height"
        '
        'Label95
        '
        Me.Label95.Location = New System.Drawing.Point(376, 40)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(96, 16)
        Me.Label95.TabIndex = 97
        Me.Label95.Text = "Min Page Width"
        '
        'HScrollBar33
        '
        Me.HScrollBar33.LargeChange = 1
        Me.HScrollBar33.Location = New System.Drawing.Point(376, 168)
        Me.HScrollBar33.Maximum = 9999
        Me.HScrollBar33.Name = "HScrollBar33"
        Me.HScrollBar33.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar33.TabIndex = 95
        '
        'HScrollBar34
        '
        Me.HScrollBar34.LargeChange = 1
        Me.HScrollBar34.Location = New System.Drawing.Point(376, 136)
        Me.HScrollBar34.Maximum = 9999
        Me.HScrollBar34.Name = "HScrollBar34"
        Me.HScrollBar34.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar34.TabIndex = 94
        '
        'HScrollBar35
        '
        Me.HScrollBar35.LargeChange = 1
        Me.HScrollBar35.Location = New System.Drawing.Point(376, 96)
        Me.HScrollBar35.Maximum = 9999
        Me.HScrollBar35.Name = "HScrollBar35"
        Me.HScrollBar35.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar35.TabIndex = 93
        '
        'HScrollBar36
        '
        Me.HScrollBar36.LargeChange = 1
        Me.HScrollBar36.Location = New System.Drawing.Point(376, 56)
        Me.HScrollBar36.Maximum = 9999
        Me.HScrollBar36.Name = "HScrollBar36"
        Me.HScrollBar36.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar36.TabIndex = 92
        '
        'Label97
        '
        Me.Label97.Location = New System.Drawing.Point(264, 152)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(72, 16)
        Me.Label97.TabIndex = 90
        '
        'Label98
        '
        Me.Label98.Location = New System.Drawing.Point(264, 120)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(64, 16)
        Me.Label98.TabIndex = 89
        '
        'Label99
        '
        Me.Label99.Location = New System.Drawing.Point(264, 80)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(80, 16)
        Me.Label99.TabIndex = 88
        '
        'Label100
        '
        Me.Label100.Location = New System.Drawing.Point(264, 40)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(72, 16)
        Me.Label100.TabIndex = 87
        '
        'Label102
        '
        Me.Label102.Location = New System.Drawing.Point(128, 152)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(104, 16)
        Me.Label102.TabIndex = 85
        Me.Label102.Text = "Min Confidence"
        '
        'Label103
        '
        Me.Label103.Location = New System.Drawing.Point(128, 120)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(96, 16)
        Me.Label103.TabIndex = 84
        Me.Label103.Text = "Quality"
        '
        'Label104
        '
        Me.Label104.Location = New System.Drawing.Point(128, 80)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(104, 16)
        Me.Label104.TabIndex = 83
        Me.Label104.Text = "Page Speck Size"
        '
        'Label105
        '
        Me.Label105.Location = New System.Drawing.Point(128, 40)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(104, 16)
        Me.Label105.TabIndex = 82
        Me.Label105.Text = "Border Speck Size"
        '
        'HScrollBar38
        '
        Me.HScrollBar38.LargeChange = 1
        Me.HScrollBar38.Location = New System.Drawing.Point(128, 168)
        Me.HScrollBar38.Minimum = 1
        Me.HScrollBar38.Name = "HScrollBar38"
        Me.HScrollBar38.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar38.TabIndex = 80
        Me.HScrollBar38.Value = 50
        '
        'HScrollBar39
        '
        Me.HScrollBar39.LargeChange = 1
        Me.HScrollBar39.Location = New System.Drawing.Point(128, 136)
        Me.HScrollBar39.Minimum = 1
        Me.HScrollBar39.Name = "HScrollBar39"
        Me.HScrollBar39.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar39.TabIndex = 79
        Me.HScrollBar39.Value = 80
        '
        'HScrollBar40
        '
        Me.HScrollBar40.LargeChange = 1
        Me.HScrollBar40.Location = New System.Drawing.Point(128, 96)
        Me.HScrollBar40.Name = "HScrollBar40"
        Me.HScrollBar40.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar40.TabIndex = 78
        Me.HScrollBar40.Value = 2
        '
        'HScrollBar41
        '
        Me.HScrollBar41.LargeChange = 1
        Me.HScrollBar41.Location = New System.Drawing.Point(128, 56)
        Me.HScrollBar41.Name = "HScrollBar41"
        Me.HScrollBar41.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar41.TabIndex = 77
        Me.HScrollBar41.Value = 3
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(24, 160)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(64, 40)
        Me.Button7.TabIndex = 76
        Me.Button7.Text = "Process Image"
        '
        'tabdotshade
        '
        Me.tabdotshade.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label57, Me.Label58, Me.Label59, Me.Label60, Me.Label61, Me.Label62, Me.Label63, Me.Label64, Me.Label65, Me.Label66, Me.Label67, Me.Label68, Me.HScrollBar18, Me.HScrollBar19, Me.HScrollBar20, Me.HScrollBar21, Me.HScrollBar22, Me.HScrollBar23, Me.Button2})
        Me.tabdotshade.Location = New System.Drawing.Point(4, 40)
        Me.tabdotshade.Name = "tabdotshade"
        Me.tabdotshade.Size = New System.Drawing.Size(640, 460)
        Me.tabdotshade.TabIndex = 6
        Me.tabdotshade.Text = "Dot Shading Removal"
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(384, 182)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(72, 16)
        Me.Label57.TabIndex = 61
        '
        'Label58
        '
        Me.Label58.Location = New System.Drawing.Point(384, 150)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(64, 16)
        Me.Label58.TabIndex = 60
        '
        'Label59
        '
        Me.Label59.Location = New System.Drawing.Point(384, 118)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(72, 16)
        Me.Label59.TabIndex = 59
        '
        'Label60
        '
        Me.Label60.Location = New System.Drawing.Point(384, 94)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(64, 16)
        Me.Label60.TabIndex = 58
        '
        'Label61
        '
        Me.Label61.Location = New System.Drawing.Point(384, 62)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(80, 16)
        Me.Label61.TabIndex = 57
        '
        'Label62
        '
        Me.Label62.Location = New System.Drawing.Point(384, 22)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(72, 16)
        Me.Label62.TabIndex = 56
        '
        'Label63
        '
        Me.Label63.Location = New System.Drawing.Point(40, 182)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(104, 16)
        Me.Label63.TabIndex = 55
        Me.Label63.Text = "Max Dot Size"
        '
        'Label64
        '
        Me.Label64.Location = New System.Drawing.Point(40, 150)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(116, 16)
        Me.Label64.TabIndex = 54
        Me.Label64.Text = "Dot Shading Density"
        '
        'Label65
        '
        Me.Label65.Location = New System.Drawing.Point(40, 118)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(88, 26)
        Me.Label65.TabIndex = 53
        Me.Label65.Text = "Horizontal Size Adjustment"
        '
        'Label66
        '
        Me.Label66.Location = New System.Drawing.Point(40, 86)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(128, 16)
        Me.Label66.TabIndex = 52
        Me.Label66.Text = "Vertical Size Adjustment"
        '
        'Label67
        '
        Me.Label67.Location = New System.Drawing.Point(40, 54)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(96, 16)
        Me.Label67.TabIndex = 51
        Me.Label67.Text = "Min Area Height"
        '
        'Label68
        '
        Me.Label68.Location = New System.Drawing.Point(40, 22)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(88, 16)
        Me.Label68.TabIndex = 50
        Me.Label68.Text = "Min Area Width"
        '
        'HScrollBar18
        '
        Me.HScrollBar18.LargeChange = 1
        Me.HScrollBar18.Location = New System.Drawing.Point(176, 182)
        Me.HScrollBar18.Maximum = 20
        Me.HScrollBar18.Name = "HScrollBar18"
        Me.HScrollBar18.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar18.TabIndex = 49
        Me.HScrollBar18.Value = 5
        '
        'HScrollBar19
        '
        Me.HScrollBar19.LargeChange = 1
        Me.HScrollBar19.Location = New System.Drawing.Point(176, 150)
        Me.HScrollBar19.Maximum = 50
        Me.HScrollBar19.Minimum = -50
        Me.HScrollBar19.Name = "HScrollBar19"
        Me.HScrollBar19.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar19.TabIndex = 48
        '
        'HScrollBar20
        '
        Me.HScrollBar20.LargeChange = 1
        Me.HScrollBar20.Location = New System.Drawing.Point(176, 118)
        Me.HScrollBar20.Minimum = -100
        Me.HScrollBar20.Name = "HScrollBar20"
        Me.HScrollBar20.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar20.TabIndex = 47
        '
        'HScrollBar21
        '
        Me.HScrollBar21.LargeChange = 1
        Me.HScrollBar21.Location = New System.Drawing.Point(176, 86)
        Me.HScrollBar21.Minimum = -100
        Me.HScrollBar21.Name = "HScrollBar21"
        Me.HScrollBar21.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar21.TabIndex = 46
        '
        'HScrollBar22
        '
        Me.HScrollBar22.LargeChange = 1
        Me.HScrollBar22.Location = New System.Drawing.Point(176, 54)
        Me.HScrollBar22.Maximum = 9999
        Me.HScrollBar22.Name = "HScrollBar22"
        Me.HScrollBar22.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar22.TabIndex = 45
        Me.HScrollBar22.Value = 50
        '
        'HScrollBar23
        '
        Me.HScrollBar23.LargeChange = 1
        Me.HScrollBar23.Location = New System.Drawing.Point(176, 22)
        Me.HScrollBar23.Maximum = 9999
        Me.HScrollBar23.Name = "HScrollBar23"
        Me.HScrollBar23.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar23.TabIndex = 44
        Me.HScrollBar23.Value = 300
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(496, 94)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(64, 40)
        Me.Button2.TabIndex = 43
        Me.Button2.Text = "Process Image"
        '
        'tabblank
        '
        Me.tabblank.Controls.AddRange(New System.Windows.Forms.Control() {Me.CheckBox2, Me.Label26, Me.Label27, Me.Label28, Me.Label29, Me.Label30, Me.Label31, Me.Label33, Me.Label34, Me.Label35, Me.Label36, Me.Label37, Me.Label38, Me.HScrollBar3, Me.HScrollBar4, Me.HScrollBar5, Me.HScrollBar6, Me.HScrollBar7, Me.HScrollBar8, Me.cmdBlank})
        Me.tabblank.Location = New System.Drawing.Point(4, 40)
        Me.tabblank.Name = "tabblank"
        Me.tabblank.Size = New System.Drawing.Size(640, 460)
        Me.tabblank.TabIndex = 4
        Me.tabblank.Text = "Blank Page Detect"
        '
        'CheckBox2
        '
        Me.CheckBox2.Location = New System.Drawing.Point(472, 24)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(96, 24)
        Me.CheckBox2.TabIndex = 43
        Me.CheckBox2.Text = "Ignore Border"
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(376, 184)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(72, 16)
        Me.Label26.TabIndex = 42
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(376, 152)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(64, 16)
        Me.Label27.TabIndex = 41
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(376, 120)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(72, 16)
        Me.Label28.TabIndex = 40
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(376, 88)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(64, 16)
        Me.Label29.TabIndex = 39
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(376, 64)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(80, 16)
        Me.Label30.TabIndex = 38
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(376, 24)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(72, 16)
        Me.Label31.TabIndex = 37
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(32, 184)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(104, 16)
        Me.Label33.TabIndex = 35
        Me.Label33.Text = "Gap Fill"
        '
        'Label34
        '
        Me.Label34.Location = New System.Drawing.Point(32, 152)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(116, 16)
        Me.Label34.TabIndex = 34
        Me.Label34.Text = "Min Object Dimension"
        '
        'Label35
        '
        Me.Label35.Location = New System.Drawing.Point(32, 120)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(88, 16)
        Me.Label35.TabIndex = 33
        Me.Label35.Text = "Right Margin"
        '
        'Label36
        '
        Me.Label36.Location = New System.Drawing.Point(32, 88)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(96, 16)
        Me.Label36.TabIndex = 32
        Me.Label36.Text = "Bottom Margin"
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(32, 56)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(64, 16)
        Me.Label37.TabIndex = 31
        Me.Label37.Text = "Left Margin"
        '
        'Label38
        '
        Me.Label38.Location = New System.Drawing.Point(32, 24)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(64, 16)
        Me.Label38.TabIndex = 30
        Me.Label38.Text = "Top Margin"
        '
        'HScrollBar3
        '
        Me.HScrollBar3.LargeChange = 1
        Me.HScrollBar3.Location = New System.Drawing.Point(168, 184)
        Me.HScrollBar3.Maximum = 9999
        Me.HScrollBar3.Name = "HScrollBar3"
        Me.HScrollBar3.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar3.TabIndex = 28
        Me.HScrollBar3.Value = 5
        '
        'HScrollBar4
        '
        Me.HScrollBar4.LargeChange = 1
        Me.HScrollBar4.Location = New System.Drawing.Point(168, 152)
        Me.HScrollBar4.Maximum = 9999
        Me.HScrollBar4.Name = "HScrollBar4"
        Me.HScrollBar4.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar4.TabIndex = 27
        Me.HScrollBar4.Value = 10
        '
        'HScrollBar5
        '
        Me.HScrollBar5.LargeChange = 1
        Me.HScrollBar5.Location = New System.Drawing.Point(168, 120)
        Me.HScrollBar5.Maximum = 9999
        Me.HScrollBar5.Name = "HScrollBar5"
        Me.HScrollBar5.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar5.TabIndex = 26
        '
        'HScrollBar6
        '
        Me.HScrollBar6.LargeChange = 1
        Me.HScrollBar6.Location = New System.Drawing.Point(168, 88)
        Me.HScrollBar6.Maximum = 9999
        Me.HScrollBar6.Name = "HScrollBar6"
        Me.HScrollBar6.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar6.TabIndex = 25
        '
        'HScrollBar7
        '
        Me.HScrollBar7.LargeChange = 1
        Me.HScrollBar7.Location = New System.Drawing.Point(168, 56)
        Me.HScrollBar7.Maximum = 9999
        Me.HScrollBar7.Name = "HScrollBar7"
        Me.HScrollBar7.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar7.TabIndex = 24
        '
        'HScrollBar8
        '
        Me.HScrollBar8.LargeChange = 1
        Me.HScrollBar8.Location = New System.Drawing.Point(168, 24)
        Me.HScrollBar8.Maximum = 9999
        Me.HScrollBar8.Name = "HScrollBar8"
        Me.HScrollBar8.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar8.TabIndex = 23
        '
        'cmdBlank
        '
        Me.cmdBlank.Location = New System.Drawing.Point(488, 96)
        Me.cmdBlank.Name = "cmdBlank"
        Me.cmdBlank.Size = New System.Drawing.Size(64, 40)
        Me.cmdBlank.TabIndex = 22
        Me.cmdBlank.Text = "Process Image"
        '
        'tabsmooth
        '
        Me.tabsmooth.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button8, Me.Label86, Me.Label91, Me.HScrollBar32})
        Me.tabsmooth.Location = New System.Drawing.Point(4, 40)
        Me.tabsmooth.Name = "tabsmooth"
        Me.tabsmooth.Size = New System.Drawing.Size(640, 460)
        Me.tabsmooth.TabIndex = 11
        Me.tabsmooth.Text = "Smoothing"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(432, 96)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(80, 48)
        Me.Button8.TabIndex = 10
        Me.Button8.Text = "Process Image"
        '
        'Label86
        '
        Me.Label86.Location = New System.Drawing.Point(264, 90)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(72, 16)
        Me.Label86.TabIndex = 9
        '
        'Label91
        '
        Me.Label91.Location = New System.Drawing.Point(128, 90)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(96, 16)
        Me.Label91.TabIndex = 8
        Me.Label91.Text = "Smoothing Size"
        '
        'HScrollBar32
        '
        Me.HScrollBar32.Location = New System.Drawing.Point(120, 130)
        Me.HScrollBar32.Minimum = 1
        Me.HScrollBar32.Name = "HScrollBar32"
        Me.HScrollBar32.Size = New System.Drawing.Size(240, 16)
        Me.HScrollBar32.TabIndex = 7
        Me.HScrollBar32.Value = 1
        '
        'tabnegpage
        '
        Me.tabnegpage.Controls.AddRange(New System.Windows.Forms.Control() {Me.CheckBox3, Me.Label69, Me.Label70, Me.Label71, Me.Label72, Me.HScrollBar24, Me.HScrollBar25, Me.Button3})
        Me.tabnegpage.Location = New System.Drawing.Point(4, 40)
        Me.tabnegpage.Name = "tabnegpage"
        Me.tabnegpage.Size = New System.Drawing.Size(640, 460)
        Me.tabnegpage.TabIndex = 7
        Me.tabnegpage.Text = "Negative Page Correction"
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(32, 64)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(96, 24)
        Me.CheckBox3.TabIndex = 51
        Me.CheckBox3.Text = "Apply Correction"
        '
        'Label69
        '
        Me.Label69.Location = New System.Drawing.Point(376, 136)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(80, 16)
        Me.Label69.TabIndex = 50
        '
        'Label70
        '
        Me.Label70.Location = New System.Drawing.Point(376, 104)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(72, 16)
        Me.Label70.TabIndex = 49
        '
        'Label71
        '
        Me.Label71.Location = New System.Drawing.Point(32, 136)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(64, 16)
        Me.Label71.TabIndex = 48
        Me.Label71.Text = "Quality"
        '
        'Label72
        '
        Me.Label72.Location = New System.Drawing.Point(32, 104)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(112, 16)
        Me.Label72.TabIndex = 47
        Me.Label72.Text = "Minimum Confidence"
        '
        'HScrollBar24
        '
        Me.HScrollBar24.LargeChange = 1
        Me.HScrollBar24.Location = New System.Drawing.Point(168, 136)
        Me.HScrollBar24.Minimum = 1
        Me.HScrollBar24.Name = "HScrollBar24"
        Me.HScrollBar24.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar24.TabIndex = 46
        Me.HScrollBar24.Value = 80
        '
        'HScrollBar25
        '
        Me.HScrollBar25.LargeChange = 1
        Me.HScrollBar25.Location = New System.Drawing.Point(168, 104)
        Me.HScrollBar25.Name = "HScrollBar25"
        Me.HScrollBar25.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar25.TabIndex = 45
        Me.HScrollBar25.Value = 50
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(496, 104)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(64, 40)
        Me.Button3.TabIndex = 44
        Me.Button3.Text = "Process Image"
        '
        'tabcomb
        '
        Me.tabcomb.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label25, Me.Label43, Me.Label49, Me.Label50, Me.Label51, Me.Label52, Me.Label53, Me.Label54, Me.Label55, Me.Label56, Me.HScrollBar2, Me.HScrollBar14, Me.HScrollBar15, Me.HScrollBar16, Me.HScrollBar17, Me.Label32, Me.Label39, Me.Label40, Me.Label41, Me.Label42, Me.Label44, Me.Label45, Me.Label46, Me.Label47, Me.Label48, Me.HScrollBar9, Me.HScrollBar10, Me.HScrollBar11, Me.HScrollBar12, Me.HScrollBar13, Me.Button1})
        Me.tabcomb.Location = New System.Drawing.Point(4, 40)
        Me.tabcomb.Name = "tabcomb"
        Me.tabcomb.Size = New System.Drawing.Size(640, 460)
        Me.tabcomb.TabIndex = 5
        Me.tabcomb.Text = "Comb Removal"
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(400, 168)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(64, 16)
        Me.Label25.TabIndex = 75
        '
        'Label43
        '
        Me.Label43.Location = New System.Drawing.Point(400, 128)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(72, 16)
        Me.Label43.TabIndex = 74
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(400, 96)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(64, 16)
        Me.Label49.TabIndex = 73
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(400, 56)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(80, 16)
        Me.Label50.TabIndex = 72
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(400, 16)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(72, 16)
        Me.Label51.TabIndex = 71
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(264, 168)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(128, 16)
        Me.Label52.TabIndex = 70
        Me.Label52.Text = "Vertical Line Thickness"
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(264, 128)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(136, 16)
        Me.Label53.TabIndex = 69
        Me.Label53.Text = "Horizontal Line Thickness"
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(264, 96)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(96, 16)
        Me.Label54.TabIndex = 68
        Me.Label54.Text = "Min Confidence"
        '
        'Label55
        '
        Me.Label55.Location = New System.Drawing.Point(264, 56)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(96, 16)
        Me.Label55.TabIndex = 67
        Me.Label55.Text = "Min Comb Length"
        '
        'Label56
        '
        Me.Label56.Location = New System.Drawing.Point(264, 16)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(80, 16)
        Me.Label56.TabIndex = 66
        Me.Label56.Text = "Comb Spacing"
        '
        'HScrollBar2
        '
        Me.HScrollBar2.LargeChange = 1
        Me.HScrollBar2.Location = New System.Drawing.Point(264, 184)
        Me.HScrollBar2.Maximum = 500
        Me.HScrollBar2.Minimum = 1
        Me.HScrollBar2.Name = "HScrollBar2"
        Me.HScrollBar2.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar2.TabIndex = 65
        Me.HScrollBar2.Value = 4
        '
        'HScrollBar14
        '
        Me.HScrollBar14.LargeChange = 1
        Me.HScrollBar14.Location = New System.Drawing.Point(264, 144)
        Me.HScrollBar14.Maximum = 500
        Me.HScrollBar14.Minimum = 1
        Me.HScrollBar14.Name = "HScrollBar14"
        Me.HScrollBar14.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar14.TabIndex = 64
        Me.HScrollBar14.Value = 4
        '
        'HScrollBar15
        '
        Me.HScrollBar15.LargeChange = 1
        Me.HScrollBar15.Location = New System.Drawing.Point(264, 112)
        Me.HScrollBar15.Minimum = 1
        Me.HScrollBar15.Name = "HScrollBar15"
        Me.HScrollBar15.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar15.TabIndex = 63
        Me.HScrollBar15.Value = 50
        '
        'HScrollBar16
        '
        Me.HScrollBar16.LargeChange = 1
        Me.HScrollBar16.Location = New System.Drawing.Point(264, 72)
        Me.HScrollBar16.Maximum = 500
        Me.HScrollBar16.Minimum = 10
        Me.HScrollBar16.Name = "HScrollBar16"
        Me.HScrollBar16.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar16.TabIndex = 62
        Me.HScrollBar16.Value = 50
        '
        'HScrollBar17
        '
        Me.HScrollBar17.LargeChange = 1
        Me.HScrollBar17.Location = New System.Drawing.Point(264, 32)
        Me.HScrollBar17.Maximum = 500
        Me.HScrollBar17.Minimum = 1
        Me.HScrollBar17.Name = "HScrollBar17"
        Me.HScrollBar17.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar17.TabIndex = 61
        Me.HScrollBar17.Value = 25
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(152, 168)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(64, 16)
        Me.Label32.TabIndex = 60
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(152, 128)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(72, 16)
        Me.Label39.TabIndex = 59
        '
        'Label40
        '
        Me.Label40.Location = New System.Drawing.Point(152, 96)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(64, 16)
        Me.Label40.TabIndex = 58
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(152, 56)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(80, 16)
        Me.Label41.TabIndex = 57
        '
        'Label42
        '
        Me.Label42.Location = New System.Drawing.Point(152, 16)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(72, 16)
        Me.Label42.TabIndex = 56
        '
        'Label44
        '
        Me.Label44.Location = New System.Drawing.Point(16, 168)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(116, 16)
        Me.Label44.TabIndex = 54
        Me.Label44.Text = "Comb Height"
        '
        'Label45
        '
        Me.Label45.Location = New System.Drawing.Point(16, 128)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(88, 16)
        Me.Label45.TabIndex = 53
        Me.Label45.Text = "Area Height"
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(16, 96)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(96, 16)
        Me.Label46.TabIndex = 52
        Me.Label46.Text = "Area Width"
        '
        'Label47
        '
        Me.Label47.Location = New System.Drawing.Point(16, 56)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(64, 16)
        Me.Label47.TabIndex = 51
        Me.Label47.Text = "Area Y"
        '
        'Label48
        '
        Me.Label48.Location = New System.Drawing.Point(16, 16)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(64, 16)
        Me.Label48.TabIndex = 50
        Me.Label48.Text = "Area X"
        '
        'HScrollBar9
        '
        Me.HScrollBar9.LargeChange = 1
        Me.HScrollBar9.Location = New System.Drawing.Point(16, 184)
        Me.HScrollBar9.Maximum = 500
        Me.HScrollBar9.Minimum = 4
        Me.HScrollBar9.Name = "HScrollBar9"
        Me.HScrollBar9.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar9.TabIndex = 48
        Me.HScrollBar9.Value = 20
        '
        'HScrollBar10
        '
        Me.HScrollBar10.LargeChange = 1
        Me.HScrollBar10.Location = New System.Drawing.Point(16, 144)
        Me.HScrollBar10.Maximum = 9999
        Me.HScrollBar10.Name = "HScrollBar10"
        Me.HScrollBar10.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar10.TabIndex = 47
        '
        'HScrollBar11
        '
        Me.HScrollBar11.LargeChange = 1
        Me.HScrollBar11.Location = New System.Drawing.Point(16, 112)
        Me.HScrollBar11.Maximum = 9999
        Me.HScrollBar11.Name = "HScrollBar11"
        Me.HScrollBar11.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar11.TabIndex = 46
        '
        'HScrollBar12
        '
        Me.HScrollBar12.LargeChange = 1
        Me.HScrollBar12.Location = New System.Drawing.Point(16, 72)
        Me.HScrollBar12.Maximum = 9999
        Me.HScrollBar12.Name = "HScrollBar12"
        Me.HScrollBar12.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar12.TabIndex = 45
        '
        'HScrollBar13
        '
        Me.HScrollBar13.LargeChange = 1
        Me.HScrollBar13.Location = New System.Drawing.Point(16, 32)
        Me.HScrollBar13.Maximum = 9999
        Me.HScrollBar13.Name = "HScrollBar13"
        Me.HScrollBar13.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar13.TabIndex = 44
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(504, 94)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 40)
        Me.Button1.TabIndex = 43
        Me.Button1.Text = "Process Image"
        '
        'tabinverse
        '
        Me.tabinverse.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdinverse, Me.Label24, Me.Label23, Me.Label22, Me.Label21, Me.Label20, Me.Label19, Me.minblack, Me.minheight, Me.minwidth})
        Me.tabinverse.Location = New System.Drawing.Point(4, 40)
        Me.tabinverse.Name = "tabinverse"
        Me.tabinverse.Size = New System.Drawing.Size(640, 460)
        Me.tabinverse.TabIndex = 3
        Me.tabinverse.Text = "Inverse Text Correction"
        '
        'cmdinverse
        '
        Me.cmdinverse.Location = New System.Drawing.Point(424, 112)
        Me.cmdinverse.Name = "cmdinverse"
        Me.cmdinverse.Size = New System.Drawing.Size(72, 40)
        Me.cmdinverse.TabIndex = 9
        Me.cmdinverse.Text = "Process Image"
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(312, 160)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(56, 16)
        Me.Label24.TabIndex = 8
        Me.Label24.Text = "Label24"
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(312, 96)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(56, 16)
        Me.Label23.TabIndex = 7
        Me.Label23.Text = "Label23"
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(312, 32)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 6
        Me.Label22.Text = "Label22"
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(88, 160)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(200, 16)
        Me.Label21.TabIndex = 5
        Me.Label21.Text = "Minimum Black On Edges:"
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(80, 96)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(192, 16)
        Me.Label20.TabIndex = 4
        Me.Label20.Text = "Minimum Area Height:"
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(80, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(184, 16)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Minimum Area Width:"
        '
        'minblack
        '
        Me.minblack.LargeChange = 1
        Me.minblack.Location = New System.Drawing.Point(80, 192)
        Me.minblack.Minimum = 1
        Me.minblack.Name = "minblack"
        Me.minblack.Size = New System.Drawing.Size(312, 16)
        Me.minblack.TabIndex = 2
        Me.minblack.Value = 1
        '
        'minheight
        '
        Me.minheight.LargeChange = 1
        Me.minheight.Location = New System.Drawing.Point(80, 128)
        Me.minheight.Maximum = 9999
        Me.minheight.Minimum = 1
        Me.minheight.Name = "minheight"
        Me.minheight.Size = New System.Drawing.Size(312, 16)
        Me.minheight.TabIndex = 1
        Me.minheight.Value = 1
        '
        'minwidth
        '
        Me.minwidth.LargeChange = 1
        Me.minwidth.Location = New System.Drawing.Point(80, 72)
        Me.minwidth.Maximum = 9999
        Me.minwidth.Minimum = 1
        Me.minwidth.Name = "minwidth"
        Me.minwidth.Size = New System.Drawing.Size(312, 16)
        Me.minwidth.TabIndex = 0
        Me.minwidth.Value = 1
        '
        'taberode
        '
        Me.taberode.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button6, Me.Label84, Me.Label85, Me.HScrollBar31, Me.CheckBox10, Me.CheckBox11, Me.CheckBox12, Me.CheckBox13, Me.CheckBox14, Me.CheckBox15})
        Me.taberode.Location = New System.Drawing.Point(4, 40)
        Me.taberode.Name = "taberode"
        Me.taberode.Size = New System.Drawing.Size(640, 460)
        Me.taberode.TabIndex = 10
        Me.taberode.Text = "Erode"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(412, 94)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(64, 40)
        Me.Button6.TabIndex = 87
        Me.Button6.Text = "Process Image"
        '
        'Label84
        '
        Me.Label84.Location = New System.Drawing.Point(204, 70)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(56, 16)
        Me.Label84.TabIndex = 86
        Me.Label84.Text = "Label84"
        '
        'Label85
        '
        Me.Label85.Location = New System.Drawing.Point(124, 70)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(56, 16)
        Me.Label85.TabIndex = 85
        Me.Label85.Text = "Amount"
        '
        'HScrollBar31
        '
        Me.HScrollBar31.Location = New System.Drawing.Point(124, 94)
        Me.HScrollBar31.Minimum = 1
        Me.HScrollBar31.Name = "HScrollBar31"
        Me.HScrollBar31.Size = New System.Drawing.Size(200, 16)
        Me.HScrollBar31.TabIndex = 84
        Me.HScrollBar31.Value = 1
        '
        'CheckBox10
        '
        Me.CheckBox10.Location = New System.Drawing.Point(268, 166)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(56, 16)
        Me.CheckBox10.TabIndex = 83
        Me.CheckBox10.Text = "Right"
        '
        'CheckBox11
        '
        Me.CheckBox11.Location = New System.Drawing.Point(268, 134)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(56, 16)
        Me.CheckBox11.TabIndex = 82
        Me.CheckBox11.Text = "Down"
        '
        'CheckBox12
        '
        Me.CheckBox12.Location = New System.Drawing.Point(196, 166)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(48, 16)
        Me.CheckBox12.TabIndex = 81
        Me.CheckBox12.Text = "Left"
        '
        'CheckBox13
        '
        Me.CheckBox13.Location = New System.Drawing.Point(196, 134)
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.Size = New System.Drawing.Size(40, 16)
        Me.CheckBox13.TabIndex = 80
        Me.CheckBox13.Text = "Up"
        '
        'CheckBox14
        '
        Me.CheckBox14.Checked = True
        Me.CheckBox14.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox14.Location = New System.Drawing.Point(124, 150)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(48, 16)
        Me.CheckBox14.TabIndex = 79
        Me.CheckBox14.Text = "All"
        '
        'CheckBox15
        '
        Me.CheckBox15.Enabled = False
        Me.CheckBox15.Location = New System.Drawing.Point(124, 38)
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.Size = New System.Drawing.Size(136, 24)
        Me.CheckBox15.TabIndex = 78
        Me.CheckBox15.Text = "Diagonal Only"
        '
        'tabdespeck
        '
        Me.tabdespeck.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdd, Me.Label18, Me.Label17, Me.Label16, Me.Label15, Me.speckwidth, Me.speckheight})
        Me.tabdespeck.Location = New System.Drawing.Point(4, 40)
        Me.tabdespeck.Name = "tabdespeck"
        Me.tabdespeck.Size = New System.Drawing.Size(640, 460)
        Me.tabdespeck.TabIndex = 1
        Me.tabdespeck.Text = "Despeckle"
        '
        'cmdd
        '
        Me.cmdd.Location = New System.Drawing.Point(416, 96)
        Me.cmdd.Name = "cmdd"
        Me.cmdd.Size = New System.Drawing.Size(80, 48)
        Me.cmdd.TabIndex = 6
        Me.cmdd.Text = "Process Image"
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(280, 112)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(72, 16)
        Me.Label18.TabIndex = 5
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(144, 112)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(96, 16)
        Me.Label17.TabIndex = 4
        Me.Label17.Text = "Speck Width:"
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(280, 40)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(56, 16)
        Me.Label16.TabIndex = 3
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(136, 40)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 16)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Speck Height:"
        '
        'speckwidth
        '
        Me.speckwidth.Location = New System.Drawing.Point(136, 152)
        Me.speckwidth.Minimum = 1
        Me.speckwidth.Name = "speckwidth"
        Me.speckwidth.Size = New System.Drawing.Size(240, 16)
        Me.speckwidth.TabIndex = 1
        Me.speckwidth.Value = 1
        '
        'speckheight
        '
        Me.speckheight.Location = New System.Drawing.Point(136, 72)
        Me.speckheight.Minimum = 1
        Me.speckheight.Name = "speckheight"
        Me.speckheight.Size = New System.Drawing.Size(240, 16)
        Me.speckheight.TabIndex = 0
        Me.speckheight.Value = 1
        '
        'RegisterTab
        '
        Me.RegisterTab.Controls.AddRange(New System.Windows.Forms.Control() {Me.RegisterButton, Me.VerticalGroupBox, Me.HorizontalGroupBox})
        Me.RegisterTab.Location = New System.Drawing.Point(4, 40)
        Me.RegisterTab.Name = "RegisterTab"
        Me.RegisterTab.Size = New System.Drawing.Size(640, 460)
        Me.RegisterTab.TabIndex = 13
        Me.RegisterTab.Text = "RegisterImage"
        '
        'RegisterButton
        '
        Me.RegisterButton.Location = New System.Drawing.Point(568, 208)
        Me.RegisterButton.Name = "RegisterButton"
        Me.RegisterButton.Size = New System.Drawing.Size(64, 40)
        Me.RegisterButton.TabIndex = 2
        Me.RegisterButton.Text = "Register Image"
        '
        'VerticalGroupBox
        '
        Me.VerticalGroupBox.Controls.AddRange(New System.Windows.Forms.Control() {Me.VerticalActiveCheckBox, Me.VerticalAddOnlyCheckBox, Me.VerticalToLineCheckThinLinesActiveCheckBox, Me.VerticalSkipActiveCheckBox, Me.VerticalToLineActiveCheckBox, Me.VerticalCentralFocusCheckBox, Me.VerticalMinimumActivity, Me.VerticalMinimumActivityLabel, Me.VerticalMinimumActivityLabel2, Me.VerticalMinimumBackground, Me.VerticalMinimumBackgroundLabel, Me.VerticalMinimumBackgroundLabel2, Me.VerticalMinimumForeground, Me.VerticalMinimumForegroundLabel, Me.VerticalMinimumForegroundLabel2, Me.VerticalResultantMargin, Me.VerticalSkipDistanceLabel2, Me.VerticalToLineMaximumLineGapLabel, Me.VerticalToLineMaximumLineThickness, Me.VerticalToLineMaximumLineThicknessLabel2, Me.VerticalToLineMinimumLineLengthLabel2, Me.VerticalToLineMaximumLineThicknessLabel, Me.VerticalToLineMinimumLineLengthLabel, Me.VerticalToLineMinimumLineLength, Me.VerticalResultantMarginLabel, Me.VerticalSkipDistance, Me.VerticalSkipDistanceLabel, Me.VerticalResultantMarginLabel2, Me.VerticalToLineMaximumLineGap, Me.VerticalToLineMaximumLineGapLabel2})
        Me.VerticalGroupBox.Location = New System.Drawing.Point(8, 232)
        Me.VerticalGroupBox.Name = "VerticalGroupBox"
        Me.VerticalGroupBox.Size = New System.Drawing.Size(544, 224)
        Me.VerticalGroupBox.TabIndex = 1
        Me.VerticalGroupBox.TabStop = False
        Me.VerticalGroupBox.Text = "Vertical Lines"
        '
        'VerticalActiveCheckBox
        '
        Me.VerticalActiveCheckBox.Checked = True
        Me.VerticalActiveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.VerticalActiveCheckBox.Location = New System.Drawing.Point(16, 32)
        Me.VerticalActiveCheckBox.Name = "VerticalActiveCheckBox"
        Me.VerticalActiveCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.VerticalActiveCheckBox.TabIndex = 5
        Me.VerticalActiveCheckBox.Text = "Edge Active"
        '
        'VerticalAddOnlyCheckBox
        '
        Me.VerticalAddOnlyCheckBox.Location = New System.Drawing.Point(16, 64)
        Me.VerticalAddOnlyCheckBox.Name = "VerticalAddOnlyCheckBox"
        Me.VerticalAddOnlyCheckBox.TabIndex = 6
        Me.VerticalAddOnlyCheckBox.Text = "Only Add Pixels"
        '
        'VerticalToLineCheckThinLinesActiveCheckBox
        '
        Me.VerticalToLineCheckThinLinesActiveCheckBox.Location = New System.Drawing.Point(16, 192)
        Me.VerticalToLineCheckThinLinesActiveCheckBox.Name = "VerticalToLineCheckThinLinesActiveCheckBox"
        Me.VerticalToLineCheckThinLinesActiveCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.VerticalToLineCheckThinLinesActiveCheckBox.TabIndex = 11
        Me.VerticalToLineCheckThinLinesActiveCheckBox.Text = "Thin Lines"
        '
        'VerticalSkipActiveCheckBox
        '
        Me.VerticalSkipActiveCheckBox.Location = New System.Drawing.Point(16, 128)
        Me.VerticalSkipActiveCheckBox.Name = "VerticalSkipActiveCheckBox"
        Me.VerticalSkipActiveCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.VerticalSkipActiveCheckBox.TabIndex = 9
        Me.VerticalSkipActiveCheckBox.Text = "Skip Lines"
        '
        'VerticalToLineActiveCheckBox
        '
        Me.VerticalToLineActiveCheckBox.Location = New System.Drawing.Point(16, 160)
        Me.VerticalToLineActiveCheckBox.Name = "VerticalToLineActiveCheckBox"
        Me.VerticalToLineActiveCheckBox.Size = New System.Drawing.Size(136, 24)
        Me.VerticalToLineActiveCheckBox.TabIndex = 10
        Me.VerticalToLineActiveCheckBox.Text = "Position Based on Line"
        '
        'VerticalCentralFocusCheckBox
        '
        Me.VerticalCentralFocusCheckBox.Location = New System.Drawing.Point(16, 96)
        Me.VerticalCentralFocusCheckBox.Name = "VerticalCentralFocusCheckBox"
        Me.VerticalCentralFocusCheckBox.TabIndex = 8
        Me.VerticalCentralFocusCheckBox.Text = "Focus on Center"
        '
        'VerticalMinimumActivity
        '
        Me.VerticalMinimumActivity.LargeChange = 1
        Me.VerticalMinimumActivity.Location = New System.Drawing.Point(168, 80)
        Me.VerticalMinimumActivity.Maximum = 1000
        Me.VerticalMinimumActivity.Minimum = 1
        Me.VerticalMinimumActivity.Name = "VerticalMinimumActivity"
        Me.VerticalMinimumActivity.Size = New System.Drawing.Size(168, 16)
        Me.VerticalMinimumActivity.TabIndex = 32
        Me.VerticalMinimumActivity.Value = 3
        '
        'VerticalMinimumActivityLabel
        '
        Me.VerticalMinimumActivityLabel.Location = New System.Drawing.Point(168, 64)
        Me.VerticalMinimumActivityLabel.Name = "VerticalMinimumActivityLabel"
        Me.VerticalMinimumActivityLabel.Size = New System.Drawing.Size(104, 16)
        Me.VerticalMinimumActivityLabel.TabIndex = 33
        Me.VerticalMinimumActivityLabel.Text = "Minimum Activity:"
        '
        'VerticalMinimumActivityLabel2
        '
        Me.VerticalMinimumActivityLabel2.Location = New System.Drawing.Point(304, 64)
        Me.VerticalMinimumActivityLabel2.Name = "VerticalMinimumActivityLabel2"
        Me.VerticalMinimumActivityLabel2.Size = New System.Drawing.Size(32, 16)
        Me.VerticalMinimumActivityLabel2.TabIndex = 34
        Me.VerticalMinimumActivityLabel2.Text = "3"
        '
        'VerticalMinimumBackground
        '
        Me.VerticalMinimumBackground.LargeChange = 1
        Me.VerticalMinimumBackground.Location = New System.Drawing.Point(168, 120)
        Me.VerticalMinimumBackground.Maximum = 1000
        Me.VerticalMinimumBackground.Minimum = 2
        Me.VerticalMinimumBackground.Name = "VerticalMinimumBackground"
        Me.VerticalMinimumBackground.Size = New System.Drawing.Size(168, 16)
        Me.VerticalMinimumBackground.TabIndex = 35
        Me.VerticalMinimumBackground.Value = 5
        '
        'VerticalMinimumBackgroundLabel
        '
        Me.VerticalMinimumBackgroundLabel.Location = New System.Drawing.Point(168, 104)
        Me.VerticalMinimumBackgroundLabel.Name = "VerticalMinimumBackgroundLabel"
        Me.VerticalMinimumBackgroundLabel.Size = New System.Drawing.Size(120, 16)
        Me.VerticalMinimumBackgroundLabel.TabIndex = 36
        Me.VerticalMinimumBackgroundLabel.Text = "Minimum Background:"
        '
        'VerticalMinimumBackgroundLabel2
        '
        Me.VerticalMinimumBackgroundLabel2.Location = New System.Drawing.Point(304, 104)
        Me.VerticalMinimumBackgroundLabel2.Name = "VerticalMinimumBackgroundLabel2"
        Me.VerticalMinimumBackgroundLabel2.Size = New System.Drawing.Size(32, 16)
        Me.VerticalMinimumBackgroundLabel2.TabIndex = 37
        Me.VerticalMinimumBackgroundLabel2.Text = "5"
        '
        'VerticalMinimumForeground
        '
        Me.VerticalMinimumForeground.LargeChange = 1
        Me.VerticalMinimumForeground.Location = New System.Drawing.Point(168, 160)
        Me.VerticalMinimumForeground.Maximum = 1000
        Me.VerticalMinimumForeground.Minimum = 1
        Me.VerticalMinimumForeground.Name = "VerticalMinimumForeground"
        Me.VerticalMinimumForeground.Size = New System.Drawing.Size(168, 16)
        Me.VerticalMinimumForeground.TabIndex = 38
        Me.VerticalMinimumForeground.Value = 5
        '
        'VerticalMinimumForegroundLabel
        '
        Me.VerticalMinimumForegroundLabel.Location = New System.Drawing.Point(168, 144)
        Me.VerticalMinimumForegroundLabel.Name = "VerticalMinimumForegroundLabel"
        Me.VerticalMinimumForegroundLabel.Size = New System.Drawing.Size(128, 16)
        Me.VerticalMinimumForegroundLabel.TabIndex = 39
        Me.VerticalMinimumForegroundLabel.Text = "Minimum Foreground:"
        '
        'VerticalMinimumForegroundLabel2
        '
        Me.VerticalMinimumForegroundLabel2.Location = New System.Drawing.Point(304, 144)
        Me.VerticalMinimumForegroundLabel2.Name = "VerticalMinimumForegroundLabel2"
        Me.VerticalMinimumForegroundLabel2.Size = New System.Drawing.Size(32, 16)
        Me.VerticalMinimumForegroundLabel2.TabIndex = 40
        Me.VerticalMinimumForegroundLabel2.Text = "5"
        '
        'VerticalResultantMargin
        '
        Me.VerticalResultantMargin.LargeChange = 1
        Me.VerticalResultantMargin.Location = New System.Drawing.Point(168, 200)
        Me.VerticalResultantMargin.Maximum = 20000
        Me.VerticalResultantMargin.Minimum = -20000
        Me.VerticalResultantMargin.Name = "VerticalResultantMargin"
        Me.VerticalResultantMargin.Size = New System.Drawing.Size(168, 16)
        Me.VerticalResultantMargin.TabIndex = 41
        Me.VerticalResultantMargin.Value = 150
        '
        'VerticalSkipDistanceLabel2
        '
        Me.VerticalSkipDistanceLabel2.Location = New System.Drawing.Point(480, 64)
        Me.VerticalSkipDistanceLabel2.Name = "VerticalSkipDistanceLabel2"
        Me.VerticalSkipDistanceLabel2.Size = New System.Drawing.Size(40, 16)
        Me.VerticalSkipDistanceLabel2.TabIndex = 46
        Me.VerticalSkipDistanceLabel2.Text = "300"
        '
        'VerticalToLineMaximumLineGapLabel
        '
        Me.VerticalToLineMaximumLineGapLabel.Location = New System.Drawing.Point(352, 104)
        Me.VerticalToLineMaximumLineGapLabel.Name = "VerticalToLineMaximumLineGapLabel"
        Me.VerticalToLineMaximumLineGapLabel.Size = New System.Drawing.Size(112, 16)
        Me.VerticalToLineMaximumLineGapLabel.TabIndex = 48
        Me.VerticalToLineMaximumLineGapLabel.Text = "Maximum Line Gap:"
        '
        'VerticalToLineMaximumLineThickness
        '
        Me.VerticalToLineMaximumLineThickness.LargeChange = 1
        Me.VerticalToLineMaximumLineThickness.Location = New System.Drawing.Point(352, 160)
        Me.VerticalToLineMaximumLineThickness.Name = "VerticalToLineMaximumLineThickness"
        Me.VerticalToLineMaximumLineThickness.Size = New System.Drawing.Size(168, 16)
        Me.VerticalToLineMaximumLineThickness.TabIndex = 50
        Me.VerticalToLineMaximumLineThickness.Value = 22
        '
        'VerticalToLineMaximumLineThicknessLabel2
        '
        Me.VerticalToLineMaximumLineThicknessLabel2.Location = New System.Drawing.Point(488, 144)
        Me.VerticalToLineMaximumLineThicknessLabel2.Name = "VerticalToLineMaximumLineThicknessLabel2"
        Me.VerticalToLineMaximumLineThicknessLabel2.Size = New System.Drawing.Size(32, 16)
        Me.VerticalToLineMaximumLineThicknessLabel2.TabIndex = 52
        Me.VerticalToLineMaximumLineThicknessLabel2.Text = "22"
        '
        'VerticalToLineMinimumLineLengthLabel2
        '
        Me.VerticalToLineMinimumLineLengthLabel2.Location = New System.Drawing.Point(480, 184)
        Me.VerticalToLineMinimumLineLengthLabel2.Name = "VerticalToLineMinimumLineLengthLabel2"
        Me.VerticalToLineMinimumLineLengthLabel2.Size = New System.Drawing.Size(40, 16)
        Me.VerticalToLineMinimumLineLengthLabel2.TabIndex = 55
        Me.VerticalToLineMinimumLineLengthLabel2.Text = "300"
        '
        'VerticalToLineMaximumLineThicknessLabel
        '
        Me.VerticalToLineMaximumLineThicknessLabel.Location = New System.Drawing.Point(352, 144)
        Me.VerticalToLineMaximumLineThicknessLabel.Name = "VerticalToLineMaximumLineThicknessLabel"
        Me.VerticalToLineMaximumLineThicknessLabel.Size = New System.Drawing.Size(136, 16)
        Me.VerticalToLineMaximumLineThicknessLabel.TabIndex = 51
        Me.VerticalToLineMaximumLineThicknessLabel.Text = "Maximum Line Thickness:"
        '
        'VerticalToLineMinimumLineLengthLabel
        '
        Me.VerticalToLineMinimumLineLengthLabel.Location = New System.Drawing.Point(352, 184)
        Me.VerticalToLineMinimumLineLengthLabel.Name = "VerticalToLineMinimumLineLengthLabel"
        Me.VerticalToLineMinimumLineLengthLabel.Size = New System.Drawing.Size(120, 16)
        Me.VerticalToLineMinimumLineLengthLabel.TabIndex = 54
        Me.VerticalToLineMinimumLineLengthLabel.Text = "Minimum Line Length:"
        '
        'VerticalToLineMinimumLineLength
        '
        Me.VerticalToLineMinimumLineLength.LargeChange = 1
        Me.VerticalToLineMinimumLineLength.Location = New System.Drawing.Point(352, 200)
        Me.VerticalToLineMinimumLineLength.Maximum = 20000
        Me.VerticalToLineMinimumLineLength.Name = "VerticalToLineMinimumLineLength"
        Me.VerticalToLineMinimumLineLength.Size = New System.Drawing.Size(168, 16)
        Me.VerticalToLineMinimumLineLength.TabIndex = 53
        Me.VerticalToLineMinimumLineLength.Value = 300
        '
        'VerticalResultantMarginLabel
        '
        Me.VerticalResultantMarginLabel.Location = New System.Drawing.Point(168, 184)
        Me.VerticalResultantMarginLabel.Name = "VerticalResultantMarginLabel"
        Me.VerticalResultantMarginLabel.Size = New System.Drawing.Size(104, 16)
        Me.VerticalResultantMarginLabel.TabIndex = 42
        Me.VerticalResultantMarginLabel.Text = "Resultant Margin:"
        '
        'VerticalSkipDistance
        '
        Me.VerticalSkipDistance.LargeChange = 1
        Me.VerticalSkipDistance.Location = New System.Drawing.Point(352, 80)
        Me.VerticalSkipDistance.Maximum = 20000
        Me.VerticalSkipDistance.Name = "VerticalSkipDistance"
        Me.VerticalSkipDistance.Size = New System.Drawing.Size(168, 16)
        Me.VerticalSkipDistance.TabIndex = 44
        Me.VerticalSkipDistance.Value = 300
        '
        'VerticalSkipDistanceLabel
        '
        Me.VerticalSkipDistanceLabel.Location = New System.Drawing.Point(352, 64)
        Me.VerticalSkipDistanceLabel.Name = "VerticalSkipDistanceLabel"
        Me.VerticalSkipDistanceLabel.Size = New System.Drawing.Size(96, 16)
        Me.VerticalSkipDistanceLabel.TabIndex = 45
        Me.VerticalSkipDistanceLabel.Text = "Skip Distance:"
        '
        'VerticalResultantMarginLabel2
        '
        Me.VerticalResultantMarginLabel2.Location = New System.Drawing.Point(296, 184)
        Me.VerticalResultantMarginLabel2.Name = "VerticalResultantMarginLabel2"
        Me.VerticalResultantMarginLabel2.Size = New System.Drawing.Size(40, 16)
        Me.VerticalResultantMarginLabel2.TabIndex = 43
        Me.VerticalResultantMarginLabel2.Text = "150"
        '
        'VerticalToLineMaximumLineGap
        '
        Me.VerticalToLineMaximumLineGap.LargeChange = 1
        Me.VerticalToLineMaximumLineGap.Location = New System.Drawing.Point(352, 120)
        Me.VerticalToLineMaximumLineGap.Maximum = 50
        Me.VerticalToLineMaximumLineGap.Name = "VerticalToLineMaximumLineGap"
        Me.VerticalToLineMaximumLineGap.Size = New System.Drawing.Size(168, 16)
        Me.VerticalToLineMaximumLineGap.TabIndex = 47
        Me.VerticalToLineMaximumLineGap.Value = 2
        '
        'VerticalToLineMaximumLineGapLabel2
        '
        Me.VerticalToLineMaximumLineGapLabel2.Location = New System.Drawing.Point(488, 104)
        Me.VerticalToLineMaximumLineGapLabel2.Name = "VerticalToLineMaximumLineGapLabel2"
        Me.VerticalToLineMaximumLineGapLabel2.Size = New System.Drawing.Size(32, 16)
        Me.VerticalToLineMaximumLineGapLabel2.TabIndex = 49
        Me.VerticalToLineMaximumLineGapLabel2.Text = "2"
        '
        'HorizontalGroupBox
        '
        Me.HorizontalGroupBox.Controls.AddRange(New System.Windows.Forms.Control() {Me.HorizontalToLineMinimumLineLengthLabel2, Me.HorizontalToLineMinimumLineLengthLabel, Me.HorizontalToLineMinimumLineLength, Me.HorizontalToLineMaximumLineThicknessLabel2, Me.HorizontalToLineMaximumLineThicknessLabel, Me.HorizontalToLineMaximumLineThickness, Me.HorizontalToLineMaximumLineGapLabel2, Me.HorizontalToLineMaximumLineGapLabel, Me.HorizontalToLineMaximumLineGap, Me.HorizontalSkipDistanceLabel2, Me.HorizontalSkipDistanceLabel, Me.HorizontalSkipDistance, Me.HorizontalResultantMarginLabel2, Me.HorizontalResultantMarginLabel, Me.HorizontalResultantMargin, Me.HorizontalMinimumForegroundLabel2, Me.HorizontalMinimumForegroundLabel, Me.HorizontalMinimumForeground, Me.HorizontalMinimumBackgroundLabel2, Me.HorizontalMinimumBackgroundLabel, Me.HorizontalMinimumBackground, Me.HorizontalMinimumActivityLabel2, Me.HorizontalMinimumActivityLabel, Me.HorizontalMinimumActivity, Me.HorizontalToLineActiveCheckBox, Me.HorizontalSkipActiveCheckBox, Me.HorizontalIgnoreHolesCheckBox, Me.HorizontalCentralFocusCheckBox, Me.HorizontalAddOnlyCheckBox, Me.HorizontalActiveCheckBox, Me.HorizontalToLineCheckThinLinesActiveCheckBox})
        Me.HorizontalGroupBox.Location = New System.Drawing.Point(8, 8)
        Me.HorizontalGroupBox.Name = "HorizontalGroupBox"
        Me.HorizontalGroupBox.Size = New System.Drawing.Size(544, 216)
        Me.HorizontalGroupBox.TabIndex = 0
        Me.HorizontalGroupBox.TabStop = False
        Me.HorizontalGroupBox.Text = "Horizontal Lines"
        '
        'HorizontalToLineMinimumLineLengthLabel2
        '
        Me.HorizontalToLineMinimumLineLengthLabel2.Location = New System.Drawing.Point(472, 176)
        Me.HorizontalToLineMinimumLineLengthLabel2.Name = "HorizontalToLineMinimumLineLengthLabel2"
        Me.HorizontalToLineMinimumLineLengthLabel2.Size = New System.Drawing.Size(48, 16)
        Me.HorizontalToLineMinimumLineLengthLabel2.TabIndex = 31
        Me.HorizontalToLineMinimumLineLengthLabel2.Text = "300"
        '
        'HorizontalToLineMinimumLineLengthLabel
        '
        Me.HorizontalToLineMinimumLineLengthLabel.Location = New System.Drawing.Point(352, 176)
        Me.HorizontalToLineMinimumLineLengthLabel.Name = "HorizontalToLineMinimumLineLengthLabel"
        Me.HorizontalToLineMinimumLineLengthLabel.Size = New System.Drawing.Size(120, 16)
        Me.HorizontalToLineMinimumLineLengthLabel.TabIndex = 30
        Me.HorizontalToLineMinimumLineLengthLabel.Text = "Minimum Line Length:"
        '
        'HorizontalToLineMinimumLineLength
        '
        Me.HorizontalToLineMinimumLineLength.LargeChange = 1
        Me.HorizontalToLineMinimumLineLength.Location = New System.Drawing.Point(352, 192)
        Me.HorizontalToLineMinimumLineLength.Maximum = 20000
        Me.HorizontalToLineMinimumLineLength.Name = "HorizontalToLineMinimumLineLength"
        Me.HorizontalToLineMinimumLineLength.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalToLineMinimumLineLength.TabIndex = 29
        Me.HorizontalToLineMinimumLineLength.Value = 300
        '
        'HorizontalToLineMaximumLineThicknessLabel2
        '
        Me.HorizontalToLineMaximumLineThicknessLabel2.Location = New System.Drawing.Point(488, 136)
        Me.HorizontalToLineMaximumLineThicknessLabel2.Name = "HorizontalToLineMaximumLineThicknessLabel2"
        Me.HorizontalToLineMaximumLineThicknessLabel2.Size = New System.Drawing.Size(32, 16)
        Me.HorizontalToLineMaximumLineThicknessLabel2.TabIndex = 28
        Me.HorizontalToLineMaximumLineThicknessLabel2.Text = "22"
        '
        'HorizontalToLineMaximumLineThicknessLabel
        '
        Me.HorizontalToLineMaximumLineThicknessLabel.Location = New System.Drawing.Point(352, 136)
        Me.HorizontalToLineMaximumLineThicknessLabel.Name = "HorizontalToLineMaximumLineThicknessLabel"
        Me.HorizontalToLineMaximumLineThicknessLabel.Size = New System.Drawing.Size(136, 16)
        Me.HorizontalToLineMaximumLineThicknessLabel.TabIndex = 27
        Me.HorizontalToLineMaximumLineThicknessLabel.Text = "Maximum Line Thickness:"
        '
        'HorizontalToLineMaximumLineThickness
        '
        Me.HorizontalToLineMaximumLineThickness.LargeChange = 1
        Me.HorizontalToLineMaximumLineThickness.Location = New System.Drawing.Point(352, 152)
        Me.HorizontalToLineMaximumLineThickness.Name = "HorizontalToLineMaximumLineThickness"
        Me.HorizontalToLineMaximumLineThickness.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalToLineMaximumLineThickness.TabIndex = 26
        Me.HorizontalToLineMaximumLineThickness.Value = 22
        '
        'HorizontalToLineMaximumLineGapLabel2
        '
        Me.HorizontalToLineMaximumLineGapLabel2.Location = New System.Drawing.Point(488, 96)
        Me.HorizontalToLineMaximumLineGapLabel2.Name = "HorizontalToLineMaximumLineGapLabel2"
        Me.HorizontalToLineMaximumLineGapLabel2.Size = New System.Drawing.Size(32, 16)
        Me.HorizontalToLineMaximumLineGapLabel2.TabIndex = 25
        Me.HorizontalToLineMaximumLineGapLabel2.Text = "2"
        '
        'HorizontalToLineMaximumLineGapLabel
        '
        Me.HorizontalToLineMaximumLineGapLabel.Location = New System.Drawing.Point(352, 96)
        Me.HorizontalToLineMaximumLineGapLabel.Name = "HorizontalToLineMaximumLineGapLabel"
        Me.HorizontalToLineMaximumLineGapLabel.Size = New System.Drawing.Size(112, 16)
        Me.HorizontalToLineMaximumLineGapLabel.TabIndex = 24
        Me.HorizontalToLineMaximumLineGapLabel.Text = "Maximum Line Gap:"
        '
        'HorizontalToLineMaximumLineGap
        '
        Me.HorizontalToLineMaximumLineGap.LargeChange = 1
        Me.HorizontalToLineMaximumLineGap.Location = New System.Drawing.Point(352, 112)
        Me.HorizontalToLineMaximumLineGap.Maximum = 50
        Me.HorizontalToLineMaximumLineGap.Name = "HorizontalToLineMaximumLineGap"
        Me.HorizontalToLineMaximumLineGap.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalToLineMaximumLineGap.TabIndex = 23
        Me.HorizontalToLineMaximumLineGap.Value = 2
        '
        'HorizontalSkipDistanceLabel2
        '
        Me.HorizontalSkipDistanceLabel2.Location = New System.Drawing.Point(480, 56)
        Me.HorizontalSkipDistanceLabel2.Name = "HorizontalSkipDistanceLabel2"
        Me.HorizontalSkipDistanceLabel2.Size = New System.Drawing.Size(40, 16)
        Me.HorizontalSkipDistanceLabel2.TabIndex = 22
        Me.HorizontalSkipDistanceLabel2.Text = "300"
        '
        'HorizontalSkipDistanceLabel
        '
        Me.HorizontalSkipDistanceLabel.Location = New System.Drawing.Point(352, 56)
        Me.HorizontalSkipDistanceLabel.Name = "HorizontalSkipDistanceLabel"
        Me.HorizontalSkipDistanceLabel.Size = New System.Drawing.Size(96, 16)
        Me.HorizontalSkipDistanceLabel.TabIndex = 21
        Me.HorizontalSkipDistanceLabel.Text = "Skip Distance:"
        '
        'HorizontalSkipDistance
        '
        Me.HorizontalSkipDistance.LargeChange = 1
        Me.HorizontalSkipDistance.Location = New System.Drawing.Point(352, 72)
        Me.HorizontalSkipDistance.Maximum = 20000
        Me.HorizontalSkipDistance.Name = "HorizontalSkipDistance"
        Me.HorizontalSkipDistance.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalSkipDistance.TabIndex = 20
        Me.HorizontalSkipDistance.Value = 300
        '
        'HorizontalResultantMarginLabel2
        '
        Me.HorizontalResultantMarginLabel2.Location = New System.Drawing.Point(296, 176)
        Me.HorizontalResultantMarginLabel2.Name = "HorizontalResultantMarginLabel2"
        Me.HorizontalResultantMarginLabel2.Size = New System.Drawing.Size(40, 16)
        Me.HorizontalResultantMarginLabel2.TabIndex = 19
        Me.HorizontalResultantMarginLabel2.Text = "150"
        '
        'HorizontalResultantMarginLabel
        '
        Me.HorizontalResultantMarginLabel.Location = New System.Drawing.Point(168, 176)
        Me.HorizontalResultantMarginLabel.Name = "HorizontalResultantMarginLabel"
        Me.HorizontalResultantMarginLabel.Size = New System.Drawing.Size(112, 16)
        Me.HorizontalResultantMarginLabel.TabIndex = 18
        Me.HorizontalResultantMarginLabel.Text = "Resultant Margin:"
        '
        'HorizontalResultantMargin
        '
        Me.HorizontalResultantMargin.LargeChange = 1
        Me.HorizontalResultantMargin.Location = New System.Drawing.Point(168, 192)
        Me.HorizontalResultantMargin.Maximum = 20000
        Me.HorizontalResultantMargin.Minimum = -20000
        Me.HorizontalResultantMargin.Name = "HorizontalResultantMargin"
        Me.HorizontalResultantMargin.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalResultantMargin.TabIndex = 17
        Me.HorizontalResultantMargin.Value = 150
        '
        'HorizontalMinimumForegroundLabel2
        '
        Me.HorizontalMinimumForegroundLabel2.Location = New System.Drawing.Point(304, 136)
        Me.HorizontalMinimumForegroundLabel2.Name = "HorizontalMinimumForegroundLabel2"
        Me.HorizontalMinimumForegroundLabel2.Size = New System.Drawing.Size(32, 16)
        Me.HorizontalMinimumForegroundLabel2.TabIndex = 16
        Me.HorizontalMinimumForegroundLabel2.Text = "5"
        '
        'HorizontalMinimumForegroundLabel
        '
        Me.HorizontalMinimumForegroundLabel.Location = New System.Drawing.Point(168, 136)
        Me.HorizontalMinimumForegroundLabel.Name = "HorizontalMinimumForegroundLabel"
        Me.HorizontalMinimumForegroundLabel.Size = New System.Drawing.Size(120, 16)
        Me.HorizontalMinimumForegroundLabel.TabIndex = 15
        Me.HorizontalMinimumForegroundLabel.Text = "Minimum Foreground:"
        '
        'HorizontalMinimumForeground
        '
        Me.HorizontalMinimumForeground.LargeChange = 1
        Me.HorizontalMinimumForeground.Location = New System.Drawing.Point(168, 152)
        Me.HorizontalMinimumForeground.Maximum = 1000
        Me.HorizontalMinimumForeground.Minimum = 1
        Me.HorizontalMinimumForeground.Name = "HorizontalMinimumForeground"
        Me.HorizontalMinimumForeground.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalMinimumForeground.TabIndex = 14
        Me.HorizontalMinimumForeground.Value = 5
        '
        'HorizontalMinimumBackgroundLabel2
        '
        Me.HorizontalMinimumBackgroundLabel2.Location = New System.Drawing.Point(304, 96)
        Me.HorizontalMinimumBackgroundLabel2.Name = "HorizontalMinimumBackgroundLabel2"
        Me.HorizontalMinimumBackgroundLabel2.Size = New System.Drawing.Size(32, 16)
        Me.HorizontalMinimumBackgroundLabel2.TabIndex = 13
        Me.HorizontalMinimumBackgroundLabel2.Text = "5"
        '
        'HorizontalMinimumBackgroundLabel
        '
        Me.HorizontalMinimumBackgroundLabel.Location = New System.Drawing.Point(168, 96)
        Me.HorizontalMinimumBackgroundLabel.Name = "HorizontalMinimumBackgroundLabel"
        Me.HorizontalMinimumBackgroundLabel.Size = New System.Drawing.Size(120, 16)
        Me.HorizontalMinimumBackgroundLabel.TabIndex = 12
        Me.HorizontalMinimumBackgroundLabel.Text = "Minimum Background:"
        '
        'HorizontalMinimumBackground
        '
        Me.HorizontalMinimumBackground.LargeChange = 1
        Me.HorizontalMinimumBackground.Location = New System.Drawing.Point(168, 112)
        Me.HorizontalMinimumBackground.Maximum = 1000
        Me.HorizontalMinimumBackground.Minimum = 2
        Me.HorizontalMinimumBackground.Name = "HorizontalMinimumBackground"
        Me.HorizontalMinimumBackground.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalMinimumBackground.TabIndex = 11
        Me.HorizontalMinimumBackground.Value = 2
        '
        'HorizontalMinimumActivityLabel2
        '
        Me.HorizontalMinimumActivityLabel2.Location = New System.Drawing.Point(304, 56)
        Me.HorizontalMinimumActivityLabel2.Name = "HorizontalMinimumActivityLabel2"
        Me.HorizontalMinimumActivityLabel2.Size = New System.Drawing.Size(32, 16)
        Me.HorizontalMinimumActivityLabel2.TabIndex = 10
        Me.HorizontalMinimumActivityLabel2.Text = "3"
        '
        'HorizontalMinimumActivityLabel
        '
        Me.HorizontalMinimumActivityLabel.Location = New System.Drawing.Point(168, 56)
        Me.HorizontalMinimumActivityLabel.Name = "HorizontalMinimumActivityLabel"
        Me.HorizontalMinimumActivityLabel.Size = New System.Drawing.Size(96, 16)
        Me.HorizontalMinimumActivityLabel.TabIndex = 9
        Me.HorizontalMinimumActivityLabel.Text = "Minimum Activity:"
        '
        'HorizontalMinimumActivity
        '
        Me.HorizontalMinimumActivity.LargeChange = 1
        Me.HorizontalMinimumActivity.Location = New System.Drawing.Point(168, 72)
        Me.HorizontalMinimumActivity.Maximum = 1000
        Me.HorizontalMinimumActivity.Minimum = 1
        Me.HorizontalMinimumActivity.Name = "HorizontalMinimumActivity"
        Me.HorizontalMinimumActivity.Size = New System.Drawing.Size(168, 16)
        Me.HorizontalMinimumActivity.TabIndex = 8
        Me.HorizontalMinimumActivity.Value = 3
        '
        'HorizontalToLineActiveCheckBox
        '
        Me.HorizontalToLineActiveCheckBox.Location = New System.Drawing.Point(16, 184)
        Me.HorizontalToLineActiveCheckBox.Name = "HorizontalToLineActiveCheckBox"
        Me.HorizontalToLineActiveCheckBox.Size = New System.Drawing.Size(136, 24)
        Me.HorizontalToLineActiveCheckBox.TabIndex = 7
        Me.HorizontalToLineActiveCheckBox.Text = "Position Based on Line"
        '
        'HorizontalSkipActiveCheckBox
        '
        Me.HorizontalSkipActiveCheckBox.Location = New System.Drawing.Point(16, 152)
        Me.HorizontalSkipActiveCheckBox.Name = "HorizontalSkipActiveCheckBox"
        Me.HorizontalSkipActiveCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.HorizontalSkipActiveCheckBox.TabIndex = 4
        Me.HorizontalSkipActiveCheckBox.Text = "Skip Lines"
        '
        'HorizontalIgnoreHolesCheckBox
        '
        Me.HorizontalIgnoreHolesCheckBox.Location = New System.Drawing.Point(16, 120)
        Me.HorizontalIgnoreHolesCheckBox.Name = "HorizontalIgnoreHolesCheckBox"
        Me.HorizontalIgnoreHolesCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.HorizontalIgnoreHolesCheckBox.TabIndex = 3
        Me.HorizontalIgnoreHolesCheckBox.Text = "Ignore Holes"
        '
        'HorizontalCentralFocusCheckBox
        '
        Me.HorizontalCentralFocusCheckBox.Location = New System.Drawing.Point(16, 88)
        Me.HorizontalCentralFocusCheckBox.Name = "HorizontalCentralFocusCheckBox"
        Me.HorizontalCentralFocusCheckBox.TabIndex = 2
        Me.HorizontalCentralFocusCheckBox.Text = "Focus on Center"
        '
        'HorizontalAddOnlyCheckBox
        '
        Me.HorizontalAddOnlyCheckBox.Location = New System.Drawing.Point(16, 56)
        Me.HorizontalAddOnlyCheckBox.Name = "HorizontalAddOnlyCheckBox"
        Me.HorizontalAddOnlyCheckBox.TabIndex = 1
        Me.HorizontalAddOnlyCheckBox.Text = "Only Add Pixels"
        '
        'HorizontalActiveCheckBox
        '
        Me.HorizontalActiveCheckBox.Checked = True
        Me.HorizontalActiveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.HorizontalActiveCheckBox.Location = New System.Drawing.Point(16, 24)
        Me.HorizontalActiveCheckBox.Name = "HorizontalActiveCheckBox"
        Me.HorizontalActiveCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.HorizontalActiveCheckBox.TabIndex = 0
        Me.HorizontalActiveCheckBox.Text = "Edge Active"
        '
        'HorizontalToLineCheckThinLinesActiveCheckBox
        '
        Me.HorizontalToLineCheckThinLinesActiveCheckBox.Location = New System.Drawing.Point(168, 24)
        Me.HorizontalToLineCheckThinLinesActiveCheckBox.Name = "HorizontalToLineCheckThinLinesActiveCheckBox"
        Me.HorizontalToLineCheckThinLinesActiveCheckBox.Size = New System.Drawing.Size(96, 24)
        Me.HorizontalToLineCheckThinLinesActiveCheckBox.TabIndex = 56
        Me.HorizontalToLineCheckThinLinesActiveCheckBox.Text = "Thin Lines"
        '
        'tabline
        '
        Me.tabline.Controls.AddRange(New System.Windows.Forms.Control() {Me.TextBox1, Me.Label81, Me.Label79, Me.Label80, Me.HScrollBar29, Me.Label73, Me.Label74, Me.Label75, Me.Label76, Me.Label77, Me.Label78, Me.HScrollBar26, Me.HScrollBar27, Me.HScrollBar28, Me.Button4})
        Me.tabline.Location = New System.Drawing.Point(4, 40)
        Me.tabline.Name = "tabline"
        Me.tabline.Size = New System.Drawing.Size(640, 460)
        Me.tabline.TabIndex = 8
        Me.tabline.Text = "Line Removal"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(160, 184)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(200, 21)
        Me.TextBox1.TabIndex = 89
        Me.TextBox1.Text = "10"
        '
        'Label81
        '
        Me.Label81.Location = New System.Drawing.Point(160, 168)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(184, 16)
        Me.Label81.TabIndex = 90
        Me.Label81.Text = "Min Aspect Ratio"
        '
        'Label79
        '
        Me.Label79.Location = New System.Drawing.Point(320, 128)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(64, 16)
        Me.Label79.TabIndex = 88
        '
        'Label80
        '
        Me.Label80.Location = New System.Drawing.Point(160, 128)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(144, 16)
        Me.Label80.TabIndex = 87
        Me.Label80.Text = "Max Character Repair Size"
        '
        'HScrollBar29
        '
        Me.HScrollBar29.LargeChange = 1
        Me.HScrollBar29.Location = New System.Drawing.Point(160, 144)
        Me.HScrollBar29.Maximum = 500
        Me.HScrollBar29.Minimum = 1
        Me.HScrollBar29.Name = "HScrollBar29"
        Me.HScrollBar29.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar29.TabIndex = 86
        Me.HScrollBar29.Value = 4
        '
        'Label73
        '
        Me.Label73.Location = New System.Drawing.Point(320, 88)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(64, 16)
        Me.Label73.TabIndex = 85
        '
        'Label74
        '
        Me.Label74.Location = New System.Drawing.Point(320, 48)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(72, 16)
        Me.Label74.TabIndex = 84
        '
        'Label75
        '
        Me.Label75.Location = New System.Drawing.Point(320, 16)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(64, 16)
        Me.Label75.TabIndex = 83
        '
        'Label76
        '
        Me.Label76.Location = New System.Drawing.Point(160, 88)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(128, 16)
        Me.Label76.TabIndex = 82
        Me.Label76.Text = "Max Gap"
        '
        'Label77
        '
        Me.Label77.Location = New System.Drawing.Point(160, 48)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(136, 16)
        Me.Label77.TabIndex = 81
        Me.Label77.Text = "Max Thickness"
        '
        'Label78
        '
        Me.Label78.Location = New System.Drawing.Point(160, 16)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(96, 16)
        Me.Label78.TabIndex = 80
        Me.Label78.Text = "Min Length"
        '
        'HScrollBar26
        '
        Me.HScrollBar26.LargeChange = 1
        Me.HScrollBar26.Location = New System.Drawing.Point(160, 104)
        Me.HScrollBar26.Maximum = 500
        Me.HScrollBar26.Minimum = 1
        Me.HScrollBar26.Name = "HScrollBar26"
        Me.HScrollBar26.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar26.TabIndex = 79
        Me.HScrollBar26.Value = 4
        '
        'HScrollBar27
        '
        Me.HScrollBar27.LargeChange = 1
        Me.HScrollBar27.Location = New System.Drawing.Point(160, 64)
        Me.HScrollBar27.Maximum = 500
        Me.HScrollBar27.Minimum = 1
        Me.HScrollBar27.Name = "HScrollBar27"
        Me.HScrollBar27.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar27.TabIndex = 78
        Me.HScrollBar27.Value = 4
        '
        'HScrollBar28
        '
        Me.HScrollBar28.LargeChange = 1
        Me.HScrollBar28.Location = New System.Drawing.Point(160, 32)
        Me.HScrollBar28.Minimum = 1
        Me.HScrollBar28.Name = "HScrollBar28"
        Me.HScrollBar28.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar28.TabIndex = 77
        Me.HScrollBar28.Value = 50
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(464, 88)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(64, 40)
        Me.Button4.TabIndex = 76
        Me.Button4.Text = "Process Image"
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(8, 104)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(320, 264)
        Me.ImageXView1.TabIndex = 8
        '
        'ImageXView2
        '
        Me.ImageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.ImageXView2.AutoScroll = True
        Me.ImageXView2.Location = New System.Drawing.Point(344, 104)
        Me.ImageXView2.Name = "ImageXView2"
        Me.ImageXView2.Size = New System.Drawing.Size(328, 264)
        Me.ImageXView2.TabIndex = 9
        '
        'listBox1
        '
        Me.listBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listBox1.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "", "Using the Deskew, Despeckle, RemoveLines, RemoveBorder, SmoothObjects, RemoveBlob" & _
        "s, CorrectInverseText,", "RemoveDotShading, RemoveCombs, DetectBlankPage, CorrectNegativePage, Dilate, and " & _
        "Erode methods.", "" & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(9) & Microsoft.VisualBasic.ChrW(9)})
        Me.listBox1.Location = New System.Drawing.Point(8, 8)
        Me.listBox1.Name = "listBox1"
        Me.listBox1.Size = New System.Drawing.Size(664, 69)
        Me.listBox1.TabIndex = 10
        '
        'ScanFix1
        '
        Me.ScanFix1.Debug = False
        Me.ScanFix1.DebugLogFile = "C:\Documents and Settings\jargento.JPG.COM\My Documents\ScanFix5.log"
        Me.ScanFix1.ErrorLevel = PegasusImaging.WinForms.ScanFix5.ErrorLevel.Production
        Me.ScanFix1.ResolutionUnits = System.Drawing.GraphicsUnit.Inch
        '
        'MainScreen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(680, 915)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.listBox1, Me.ImageXView2, Me.ImageXView1, Me.options, Me.lblout, Me.lblinput})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "MainScreen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Image Cleanup"
        Me.options.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.tabdeskew.ResumeLayout(False)
        Me.tabblob.ResumeLayout(False)
        Me.tabdilate.ResumeLayout(False)
        Me.tabborder.ResumeLayout(False)
        Me.tabdotshade.ResumeLayout(False)
        Me.tabblank.ResumeLayout(False)
        Me.tabsmooth.ResumeLayout(False)
        Me.tabnegpage.ResumeLayout(False)
        Me.tabcomb.ResumeLayout(False)
        Me.tabinverse.ResumeLayout(False)
        Me.taberode.ResumeLayout(False)
        Me.tabdespeck.ResumeLayout(False)
        Me.RegisterTab.ResumeLayout(False)
        Me.VerticalGroupBox.ResumeLayout(False)
        Me.HorizontalGroupBox.ResumeLayout(False)
        Me.tabline.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub MainScreen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        '***Must call the UnlockRuntime method before the application begins
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        'ScanFix1.License.UnlockRuntime(1234, 1234, 1234, 1234)


        ImageXView1.AutoScroll = True
        ImageXView2.AutoScroll = True

        ScanFix1.License.LicenseEdition = PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.BitonalEdition

        ComboBox1.SelectedIndex = 0
        ComboBox2.SelectedIndex = 0
    End Sub

    Private Sub OpenMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenMenuItem.Click
        Try

            cd.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.TIF)|*.BMP;*.JPG;*.GIF;*.TIF|All files (*.*)|*.*"
            cd.FilterIndex = 0

            cd.InitialDirectory = Application.ExecutablePath & "\..\..\..\..\..\..\..\..\Common\Images"

            cd.ShowDialog(Me)

            If Not cd.FileName = "" Then

                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(cd.FileName)

                '**pass the image data to ScanFix for processing    
                ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))

                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If Not ImageXView1.Image.ImageXData.BitsPerPixel = 1 Then
                    ScanFix1.AutoBinarize()
                End If

            Else
                If Not ImageXView1.Image Is Nothing Then
                    ImageXView1.Image.Dispose()
                    ImageXView1.Image = Nothing
                End If
                If Not ImageXView2.Image Is Nothing Then
                    ImageXView2.Image.Dispose()
                    ImageXView2.Image = Nothing
                End If
            End If
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        End
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub ScanFixMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ScanFixMenuItem.Click
        ScanFix1.AboutBox()
    End Sub

    Private Sub HScrollBar1_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar1.Scroll
        Dim a As Integer
        a = HScrollBar1.Value
        lblval.Text = a.ToString()
    End Sub

    Private Sub cmddekew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmddekew.Click

        Try
            '** Create the DeskewOptions object and deskew image
            Dim deskewopt As New PegasusImaging.WinForms.ScanFix5.DeskewOptions()
            Dim deskewres As New PegasusImaging.WinForms.ScanFix5.DeskewResults()

            deskewopt.MaintainOriginalSize = CheckBox1.CheckState
            deskewopt.MaximumAcceptableAngle = CDbl(txtangle.Text)
            deskewopt.MinimumConfidence = HScrollBar1.Value
            If ComboBox1.SelectedIndex = 0 Then
                deskewopt.PadColor = System.Drawing.Color.White
            Else
                deskewopt.PadColor = System.Drawing.Color.Black
            End If

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                deskewres = ScanFix1.Deskew(deskewopt)
                MsgBox("Confidence: " & deskewres.Confidence & Chr(10) & "Image was modified:" & deskewres.ImageWasModified & Chr(10) & "Rotation Angle:" & deskewres.RotationAngle & Chr(10) & "Variation:" & deskewres.Variation, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Deskew")

                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))


                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If


                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub scrollx_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrollx.ValueChanged
        If scrollx.Value > scrollw.Value Then
            scrollx.Value = scrollw.Value
        End If
        If scrollx.Value > ImageXView1.Image.ImageXData.Width Then
            scrollx.Value = ImageXView1.Image.ImageXData.Width
        End If
        Label8.Text = scrollx.Value
    End Sub

    Private Sub scrolly_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrolly.ValueChanged
        If scrolly.Value > scrolh.Value Then
            scrolly.Value = scrolh.Value
        End If
        If scrolly.Value > ImageXView1.Image.ImageXData.Height Then
            scrolly.Value = ImageXView1.Image.ImageXData.Height
        End If
        Label9.Text = scrolly.Value
    End Sub

    Private Sub scrollw_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrollw.ValueChanged
        If scrollw.Value < scrollx.Value Then
            scrollw.Value = scrollx.Value
        End If
        If scrollw.Value > ImageXView1.Image.ImageXData.Width Then
            scrollw.Value = ImageXView1.Image.ImageXData.Width - 1
        End If
        Label10.Text = scrollw.Value
    End Sub

    Private Sub scrolh_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrolh.ValueChanged
        If scrolh.Value < scrollx.Value Then
            scrolh.Value = scrollx.Value
        End If
        If scrolh.Value > ImageXView1.Image.ImageXData.Height Then
            scrolh.Value = ImageXView1.Image.ImageXData.Height - 1
        End If
        Label11.Text = scrolh.Value
    End Sub

    Private Sub scrollmin_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrollmin.ValueChanged
        If scrollmin.Value > scrollm.Value Then
            scrollmin.Value = scrollm.Value
        End If
        Label12.Text = scrollmin.Value
    End Sub

    Private Sub scrollm_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles scrollm.Scroll
        If scrollm.Value < scrollmin.Value Then
            scrollm.Value = scrollmin.Value
        End If
        Label13.Text = scrollm.Value
    End Sub

    Private Sub scrollden_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles scrollden.Scroll
        Label14.Text = scrollden.Value
    End Sub

    Private Sub cmdblob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdblob.Click
        Try
            '** Create the DeskewOptions object and deskew image
            Dim blobopt As New PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions()
            Dim blobres As New PegasusImaging.WinForms.ScanFix5.BlobRemovalResults()
            Dim rec As New System.Drawing.Rectangle()
            rec.X = scrollx.Value
            rec.Y = scrolly.Value
            rec.Width = scrollw.Value
            rec.Height = scrolh.Value
            blobopt.Area = rec
            blobopt.MaximumPixelCount = scrollm.Value
            blobopt.MinimumDensity = scrollden.Value
            blobopt.MinimumPixelCount = scrollmin.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                blobres = ScanFix1.RemoveBlobs(blobopt)
                MsgBox("Count of Blobs:" & blobres.CountOfBlobsFound & Chr(10) & "Image was modified:" & blobres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Deskew")
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub tabblob_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabblob.Paint
        Label8.Text = scrollx.Value
        Label9.Text = scrolly.Value
        Label10.Text = scrollw.Value
        Label11.Text = scrolh.Value
        Label12.Text = scrollmin.Value
        Label13.Text = scrollm.Value
        Label14.Text = scrollden.Value
    End Sub

    Private Sub speckheight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles speckheight.Scroll
        Label16.Text = speckheight.Value
    End Sub

    Private Sub cmdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdd.Click
        Try
            '** Create the DespeckleOptions object and despeckle image
            Dim despeckopt As New PegasusImaging.WinForms.ScanFix5.DespeckleOptions()
            Dim despeckres As New PegasusImaging.WinForms.ScanFix5.DespeckleResults()
            despeckopt.SpeckHeight = speckheight.Value
            despeckopt.SpeckWidth = speckwidth.Value


            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                despeckres = ScanFix1.Despeckle(despeckopt)
                MsgBox("Count of Specks:" & despeckres.CountOfSpecksFound & Chr(10) & "Image was modified:" & despeckres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Despeckle")

                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub speckwidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles speckwidth.Scroll
        Label18.Text = speckwidth.Value
    End Sub

    Private Sub tabdespeck_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabdespeck.Paint
        Label16.Text = speckwidth.Value
        Label18.Text = speckheight.Value
    End Sub

    Private Sub minwidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles minwidth.Scroll
        Label22.Text = minwidth.Value
    End Sub

    Private Sub minheight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles minheight.Scroll
        Label23.Text = minheight.Value
    End Sub

    Private Sub minblack_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles minblack.Scroll
        Label24.Text = minblack.Value
    End Sub

    Private Sub cmdinverse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdinverse.Click
        Try
            '** Create the InverseTextOptions object and find inverse text
            Dim inverseopt As New PegasusImaging.WinForms.ScanFix5.InverseTextOptions()
            Dim inverseres As New PegasusImaging.WinForms.ScanFix5.InverseTextResults()
            inverseopt.MinimumAreaWidth = minwidth.Value
            inverseopt.MinimumAreaHeight = minheight.Value
            inverseopt.MinimumBlankOnEdges = minblack.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                inverseres = ScanFix1.CorrectInverseText(inverseopt)
                MsgBox("Count of Areas:" & inverseres.CountOfAreasFound & Chr(10) & "Image was modified:" & inverseres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Inverse Text Correction")
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub tabinverse_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabinverse.Paint
        Label22.Text = minwidth.Value
        Label23.Text = minheight.Value
        Label24.Text = minblack.Value
    End Sub

    Private Sub HScrollBar3_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar3.Scroll
        Label26.Text = HScrollBar3.Value
    End Sub

    Private Sub HScrollBar4_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar4.Scroll
        Label27.Text = HScrollBar4.Value
    End Sub

    Private Sub HScrollBar5_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar5.Scroll
        Label28.Text = HScrollBar5.Value
    End Sub

    Private Sub HScrollBar6_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar6.Scroll
        Label29.Text = HScrollBar6.Value
    End Sub

    Private Sub HScrollBar7_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar7.Scroll
        Label30.Text = HScrollBar7.Value
    End Sub

    Private Sub HScrollBar8_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar8.Scroll
        Label31.Text = HScrollBar8.Value
    End Sub

    Private Sub cmdBlank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBlank.Click
        Try
            '** Create the BlankPageDetectOptions object and see if page is blank
            Dim blankopt As New PegasusImaging.WinForms.ScanFix5.BlankPageDetectOptions()
            Dim blankres As New PegasusImaging.WinForms.ScanFix5.BlankPageDetectResults()

            blankopt.IgnoreBorder = CheckBox2.Checked
            blankopt.TopMargin = HScrollBar8.Value
            blankopt.LeftMargin = HScrollBar7.Value
            blankopt.BottomMargin = HScrollBar6.Value
            blankopt.RightMargin = HScrollBar5.Value
            blankopt.MinimumObjectDimension = HScrollBar4.Value
            blankopt.GapFill = HScrollBar3.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                blankres = ScanFix1.DetectBlankPage(blankopt)
                If blankres.IsBlank Then
                    MsgBox("The image is blank and the confidence is " & blankres.Confidence, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Blank Page Detection")
                Else
                    MsgBox("The image is not blank and the confidence is " & blankres.Confidence, MsgBoxStyle.OKOnly + MsgBoxStyle.Information, "Blank Page Detection")
                End If
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub tabblank_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabblank.Paint
        Label26.Text = HScrollBar3.Value
        Label27.Text = HScrollBar4.Value
        Label28.Text = HScrollBar5.Value
        Label29.Text = HScrollBar6.Value
        Label30.Text = HScrollBar7.Value
        Label31.Text = HScrollBar8.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            '** Create the CombRemovalOptions object and remove combs
            Dim combopt As New PegasusImaging.WinForms.ScanFix5.CombRemovalOptions()
            Dim combres As New PegasusImaging.WinForms.ScanFix5.CombRemovalResults()

            Dim rec As New System.Drawing.Rectangle()
            rec.X = HScrollBar13.Value
            rec.Height = HScrollBar10.Value
            rec.Width = HScrollBar11.Value
            rec.Y = HScrollBar12.Value
            combopt.Area = rec

            combopt.CombHeight = HScrollBar9.Value
            combopt.CombSpacing = HScrollBar17.Value
            combopt.MinimumCombLength = HScrollBar16.Value
            combopt.MinimumConfidence = HScrollBar15.Value
            combopt.VerticalLineThickness = HScrollBar14.Value
            combopt.HorizontalLineThickness = HScrollBar2.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                combres = ScanFix1.RemoveCombs(combopt)
                MsgBox("Count of Combs:" & combres.CountOfCombsFound & Chr(10) & "Image was modified:" & combres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Comb Removal")
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub HScrollBar13_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar13.Scroll
        Label42.Text = HScrollBar13.Value
    End Sub

    Private Sub HScrollBar12_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar12.Scroll
        Label41.Text = HScrollBar12.Value
    End Sub

    Private Sub HScrollBar11_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar11.Scroll
        Label40.Text = HScrollBar11.Value
    End Sub

    Private Sub HScrollBar10_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar10.Scroll
        Label39.Text = HScrollBar10.Value
    End Sub

    Private Sub HScrollBar9_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar9.Scroll
        Label32.Text = HScrollBar9.Value
    End Sub

    Private Sub HScrollBar17_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar17.Scroll
        Label51.Text = HScrollBar17.Value
    End Sub

    Private Sub HScrollBar16_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar16.Scroll
        Label50.Text = HScrollBar16.Value
    End Sub

    Private Sub HScrollBar15_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar15.Scroll
        Label49.Text = HScrollBar15.Value
    End Sub

    Private Sub HScrollBar14_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar14.Scroll
        Label43.Text = HScrollBar14.Value
    End Sub

    Private Sub HScrollBar2_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar2.Scroll
        Label25.Text = HScrollBar2.Value
    End Sub

    Private Sub tabcomb_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabcomb.Paint
        Label42.Text = HScrollBar13.Value
        Label41.Text = HScrollBar12.Value
        Label40.Text = HScrollBar11.Value
        Label39.Text = HScrollBar10.Value
        Label32.Text = HScrollBar9.Value
        Label51.Text = HScrollBar17.Value
        Label50.Text = HScrollBar16.Value
        Label49.Text = HScrollBar15.Value
        Label43.Text = HScrollBar14.Value
        Label25.Text = HScrollBar2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            '** Create the DotShadingRemovalOptions object and remove dot shading
            Dim dotopt As New PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions()
            Dim dotres As New PegasusImaging.WinForms.ScanFix5.DotShadingRemovalResults()

            dotopt.DensityAdjustment = HScrollBar19.Value
            dotopt.HorizontalSizeAdjustment = HScrollBar20.Value
            dotopt.MaximumDotSize = HScrollBar18.Value
            dotopt.MinimumAreaHeight = HScrollBar22.Value
            dotopt.MinimumAreaWidth = HScrollBar23.Value
            dotopt.VerticalSizeAdjustment = HScrollBar21.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                dotres = ScanFix1.RemoveDotShading(dotopt)
                MsgBox("Count of Areas:" & dotres.CountOfAreasFound & Chr(10) & "Image was modified:" & dotres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Dot Shading Removal")
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub HScrollBar23_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar23.Scroll
        Label62.Text = HScrollBar23.Value
    End Sub

    Private Sub HScrollBar22_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar22.Scroll
        Label61.Text = HScrollBar22.Value
    End Sub

    Private Sub HScrollBar21_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar21.Scroll
        Label60.Text = HScrollBar21.Value
    End Sub

    Private Sub HScrollBar20_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar20.Scroll
        Label59.Text = HScrollBar20.Value
    End Sub

    Private Sub HScrollBar19_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar19.Scroll
        Label58.Text = HScrollBar19.Value
    End Sub

    Private Sub HScrollBar18_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar18.Scroll
        Label57.Text = HScrollBar18.Value
    End Sub

    Private Sub tabdotshade_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabdotshade.Paint
        Label62.Text = HScrollBar23.Value
        Label61.Text = HScrollBar22.Value
        Label60.Text = HScrollBar21.Value
        Label59.Text = HScrollBar20.Value
        Label58.Text = HScrollBar19.Value
        Label57.Text = HScrollBar18.Value
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            '** Create the NegativeCorrectionOptions object and see if the page is negative
            Dim negpageopt As New PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions()
            Dim negpageres As New PegasusImaging.WinForms.ScanFix5.NegativeCorrectionResults()

            negpageopt.ApplyCorrection = CheckBox3.Checked
            negpageopt.MinimumConfidence = HScrollBar25.Value
            negpageopt.Quality = HScrollBar24.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                negpageres = ScanFix1.CorrectNegativePage(negpageopt)
                MsgBox("Confidence:" & negpageres.Confidence & Chr(10) & "Image is normal: " & negpageres.ImageIsNormal & Chr(10) & "Image was modified: " & negpageres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Negative Page Correction")
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub tabnegpage_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabnegpage.Paint
        Label69.Text = HScrollBar24.Value
        Label70.Text = HScrollBar25.Value
    End Sub

    Private Sub HScrollBar25_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar25.Scroll
        Label70.Text = HScrollBar25.Value
    End Sub

    Private Sub HScrollBar24_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar24.Scroll
        Label69.Text = HScrollBar24.Value
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Try
            '** Create the LineRemovalOptions object and remove lines
            Dim lineopt As New PegasusImaging.WinForms.ScanFix5.LineRemovalOptions()
            Dim lineres As New PegasusImaging.WinForms.ScanFix5.LineRemovalResults()

            lineopt.MinimumAspectRatio = CDbl(TextBox1.Text)
            lineopt.MinimumLength = HScrollBar28.Value
            lineopt.MaximumThickness = HScrollBar27.Value
            lineopt.MaximumGap = HScrollBar26.Value
            lineopt.MaximumCharacterRepairSize = HScrollBar29.Value

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                lineres = ScanFix1.RemoveLines(lineopt)
                MsgBox("Count of Lines: " & lineres.CountOfLinesFound & Chr(10) & "Image was modified:" & lineres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Line Removal")

                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub HScrollBar28_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar28.Scroll
        Label75.Text = HScrollBar28.Value
    End Sub

    Private Sub HScrollBar27_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar27.Scroll
        Label74.Text = HScrollBar27.Value
    End Sub

    Private Sub HScrollBar26_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar26.Scroll
        Label73.Text = HScrollBar26.Value
    End Sub

    Private Sub HScrollBar29_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar29.Scroll
        Label79.Text = HScrollBar29.Value
    End Sub

    Private Sub tabline_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabline.Paint
        Label75.Text = HScrollBar28.Value
        Label74.Text = HScrollBar27.Value
        Label73.Text = HScrollBar26.Value
        Label79.Text = HScrollBar29.Value
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            '** Create the DilateOptions object and dilate image
            Dim dilateopt As New PegasusImaging.WinForms.ScanFix5.DilateOptions()

            dilateopt.Amount = HScrollBar30.Value

            dilateopt.Direction = 0

            If CheckBox5.Checked Then
                dilateopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.All
            Else
                If CheckBox6.Checked Then
                    dilateopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Up
                End If
                If CheckBox8.Checked Then
                    dilateopt.Direction += PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Down
                End If
                If CheckBox7.Checked Then
                    dilateopt.Direction += PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Left
                End If
                If CheckBox9.Checked Then
                    dilateopt.Direction += PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Right
                End If

            End If

            dilateopt.OnlyDiagonal = CheckBox4.Checked

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                ScanFix1.Dilate(dilateopt)
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub HScrollBar30_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar30.Scroll
        Label83.Text = HScrollBar30.Value
    End Sub

    Private Sub tabdilate_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabdilate.Paint
        Label83.Text = HScrollBar30.Value
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        CheckBox6.Enabled = Not CheckBox6.Enabled
        CheckBox7.Enabled = Not CheckBox7.Enabled
        CheckBox8.Enabled = Not CheckBox8.Enabled
        CheckBox9.Enabled = Not CheckBox9.Enabled
    End Sub

    Private Sub CheckDilateDiag()

        If (CheckBox6.Checked = True And CheckBox8.Checked = False And CheckBox7.Checked = True And CheckBox9.Checked = False) _
        Or (CheckBox6.Checked = True And CheckBox8.Checked = False And CheckBox7.Checked = False And CheckBox9.Checked = True) _
        Or (CheckBox6.Checked = False And CheckBox8.Checked = True And CheckBox7.Checked = True And CheckBox9.Checked = False) _
        Or (CheckBox6.Checked = False And CheckBox8.Checked = True And CheckBox7.Checked = False And CheckBox9.Checked = True) Then

            CheckBox4.Enabled = True
        Else
            CheckBox4.Checked = False
            CheckBox4.Enabled = False
        End If

    End Sub

    Private Sub CheckBox9_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox9.CheckedChanged
        CheckDilateDiag()
    End Sub

    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged
        CheckDilateDiag()
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        CheckDilateDiag()
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        CheckDilateDiag()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            '** Create the ErodeOptions object and erode image
            Dim erodeopt As New PegasusImaging.WinForms.ScanFix5.ErodeOptions()

            erodeopt.Amount = HScrollBar31.Value

            erodeopt.Direction = 0

            If CheckBox14.Checked Then
                erodeopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.All
            Else
                If CheckBox13.Checked Then
                    erodeopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Up
                End If
                If CheckBox12.Checked Then
                    erodeopt.Direction += PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Left
                End If
                If CheckBox11.Checked Then
                    erodeopt.Direction += PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Down
                End If
                If CheckBox10.Checked Then
                    erodeopt.Direction += PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Right
                End If

            End If

            erodeopt.OnlyDiagonal = CheckBox15.Checked

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                ScanFix1.Erode(erodeopt)
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CheckErodeDiag()

        If (CheckBox13.Checked = True And CheckBox11.Checked = False And CheckBox12.Checked = True And CheckBox10.Checked = False) _
        Or (CheckBox13.Checked = True And CheckBox11.Checked = False And CheckBox12.Checked = False And CheckBox10.Checked = True) _
        Or (CheckBox13.Checked = False And CheckBox11.Checked = True And CheckBox12.Checked = True And CheckBox10.Checked = False) _
        Or (CheckBox13.Checked = False And CheckBox11.Checked = True And CheckBox12.Checked = False And CheckBox10.Checked = True) Then

            CheckBox15.Enabled = True
        Else
            CheckBox15.Checked = False
            CheckBox15.Enabled = False
        End If

    End Sub

    Private Sub HScrollBar31_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar31.Scroll
        Label84.Text = HScrollBar31.Value
    End Sub

    Private Sub taberode_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles taberode.Paint
        Label84.Text = HScrollBar31.Value
    End Sub

    Private Sub CheckBox14_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox14.CheckedChanged
        CheckBox10.Enabled = Not CheckBox10.Enabled
        CheckBox11.Enabled = Not CheckBox11.Enabled
        CheckBox12.Enabled = Not CheckBox12.Enabled
        CheckBox13.Enabled = Not CheckBox13.Enabled
    End Sub

    Private Sub CheckBox13_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox13.CheckedChanged
        CheckErodeDiag()
    End Sub

    Private Sub CheckBox11_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox11.CheckedChanged
        CheckErodeDiag()
    End Sub

    Private Sub CheckBox12_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox12.CheckedChanged
        CheckErodeDiag()
    End Sub

    Private Sub CheckBox10_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox10.CheckedChanged
        CheckErodeDiag()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            '** Create the BorderRemovalOptions object and remove border
            Dim borderopt As New PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions()
            Dim borderres As New PegasusImaging.WinForms.ScanFix5.BorderRemovalResults()

            borderopt.CropBorder = CheckBox16.Checked
            borderopt.ReplaceBorder = CheckBox17.Checked
            borderopt.DeskewBorder = CheckBox18.Checked
            borderopt.BorderSpeckSize = HScrollBar41.Value
            borderopt.PageSpeckSize = HScrollBar40.Value
            borderopt.Quality = HScrollBar39.Value
            borderopt.MinimumConfidence = HScrollBar38.Value
            borderopt.MinimumPageWidth = HScrollBar36.Value
            borderopt.MinimumPageHeight = HScrollBar35.Value
            borderopt.MaximumPageWidth = HScrollBar34.Value
            borderopt.MaximumPageHeight = HScrollBar33.Value

            If ComboBox2.SelectedIndex = 0 Then
                borderopt.PadColor = System.Drawing.Color.White
            Else
                borderopt.PadColor = System.Drawing.Color.Black
            End If

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                borderres = ScanFix1.RemoveBorder(borderopt)
                MsgBox("Confidence:" & borderres.Confidence _
                & Chr(10) & "Horizontal angle: " & borderres.DetectedHorizontalAngle _
                & Chr(10) & "Vertical angle: " & borderres.DetectedVerticalAngle _
                & Chr(10) & "Page X: " & borderres.PageX _
                & Chr(10) & "Page Y: " & borderres.PageY _
                & Chr(10) & "Page Width: " & borderres.PageWidth _
                & Chr(10) & "Page Height: " & borderres.PageHeight _
                & Chr(10) & "Image was modified: " & borderres.ImageWasModified, MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "Border Removal")
                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub HScrollBar41_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar41.Scroll
        Label100.Text = HScrollBar41.Value
    End Sub

    Private Sub HScrollBar40_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar40.Scroll
        Label99.Text = HScrollBar40.Value
    End Sub

    Private Sub HScrollBar39_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar39.Scroll
        Label98.Text = HScrollBar39.Value
    End Sub

    Private Sub HScrollBar38_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar38.Scroll
        Label97.Text = HScrollBar38.Value
    End Sub

    Private Sub HScrollBar36_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar36.Scroll
        Label90.Text = HScrollBar36.Value
    End Sub

    Private Sub HScrollBar35_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar35.Scroll
        Label89.Text = HScrollBar35.Value
    End Sub

    Private Sub HScrollBar34_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar34.Scroll
        Label88.Text = HScrollBar34.Value
    End Sub

    Private Sub HScrollBar33_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar33.Scroll
        Label87.Text = HScrollBar33.Value
    End Sub

    Private Sub tabborder_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabborder.Paint
        Label100.Text = HScrollBar41.Value
        Label99.Text = HScrollBar40.Value
        Label98.Text = HScrollBar39.Value
        Label97.Text = HScrollBar38.Value
        Label90.Text = HScrollBar36.Value
        Label89.Text = HScrollBar35.Value
        Label88.Text = HScrollBar34.Value
        Label87.Text = HScrollBar33.Value
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Try
            '** Smooth image

            If (ImageXView1.Image Is Nothing) Then
                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                ScanFix1.SmoothObjects(HScrollBar32.Value)

                '*** Code here to display the image
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))
                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
                Application.DoEvents()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub tabsmooth_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabsmooth.Paint
        Label86.Text = HScrollBar32.Value
    End Sub

    Private Sub HScrollBar32_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar32.Scroll
        Label86.Text = HScrollBar32.Value
    End Sub

    Private Sub RegisterButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegisterButton.Click
        Try

            regOpts = New PegasusImaging.WinForms.ScanFix5.ImageRegistrationOptions()
            regResults = New PegasusImaging.WinForms.ScanFix5.ImageRegistrationResults()

            If (ImageXView1.Image Is Nothing) Then

                MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else

                If (HorizontalActiveCheckBox.Checked = True) Then
                    regOpts.HorizontalActive = True
                Else
                    regOpts.HorizontalActive = False
                End If


                If (HorizontalAddOnlyCheckBox.Checked = True) Then
                    regOpts.HorizontalAddOnly = True
                Else
                    regOpts.HorizontalAddOnly = False
                End If


                If (HorizontalCentralFocusCheckBox.Checked = True) Then
                    regOpts.HorizontalCentralFocus = True
                Else
                    regOpts.HorizontalCentralFocus = False
                End If


                If (HorizontalIgnoreHolesCheckBox.Checked = True) Then
                    regOpts.HorizontalIgnoreHoles = True

                Else
                    regOpts.HorizontalIgnoreHoles = False
                End If

                regOpts.HorizontalMinimumActivity = HorizontalMinimumActivity.Value
                regOpts.HorizontalMinimumBackground = HorizontalMinimumBackground.Value
                regOpts.HorizontalMinimumForeground = HorizontalMinimumForeground.Value
                regOpts.HorizontalResultantMargin = HorizontalResultantMargin.Value

                If (HorizontalSkipActiveCheckBox.Checked = True) Then
                    regOpts.HorizontalSkipActive = True

                Else
                    regOpts.HorizontalSkipActive = False
                End If

                regOpts.HorizontalSkipDistance = HorizontalSkipDistance.Value

                If (HorizontalToLineActiveCheckBox.Checked = True) Then
                    regOpts.HorizontalToLineActive = True
                Else
                    regOpts.HorizontalToLineActive = False
                End If

                If (HorizontalToLineCheckThinLinesActiveCheckBox.Checked = True) Then
                    regOpts.HorizontalToLineCheckThinLinesActive = True
                Else
                    regOpts.HorizontalToLineCheckThinLinesActive = False
                End If

                regOpts.HorizontalToLineMaximumLineGap = HorizontalToLineMaximumLineGap.Value
                regOpts.HorizontalToLineMaximumLineThickness = HorizontalToLineMaximumLineThickness.Value
                regOpts.HorizontalToLineMinimumLineLength = HorizontalToLineMinimumLineLength.Value


                If (VerticalActiveCheckBox.Checked = True) Then
                    regOpts.VerticalActive = True
                Else
                    regOpts.VerticalActive = False
                End If


                If (VerticalAddOnlyCheckBox.Checked = True) Then
                    regOpts.VerticalAddOnly = True

                Else
                    regOpts.VerticalAddOnly = False
                End If



                If (VerticalCentralFocusCheckBox.Checked = True) Then
                    regOpts.VerticalCentralFocus = True

                Else
                    regOpts.VerticalCentralFocus = False
                End If


                regOpts.VerticalMinimumActivity = VerticalMinimumActivity.Value
                regOpts.VerticalMinimumBackground = VerticalMinimumBackground.Value
                regOpts.VerticalMinimumForeground = VerticalMinimumForeground.Value
                regOpts.VerticalResultantMargin = VerticalResultantMargin.Value

                If (VerticalSkipActiveCheckBox.Checked = True) Then
                    regOpts.VerticalSkipActive = True
                Else
                    regOpts.VerticalSkipActive = False
                End If

                regOpts.VerticalSkipDistance = VerticalSkipDistance.Value

                If (VerticalToLineActiveCheckBox.Checked = True) Then
                    regOpts.VerticalToLineActive = True
                Else
                    regOpts.VerticalToLineActive = False
                End If

                If (VerticalToLineCheckThinLinesActiveCheckBox.Checked = True) Then
                    regOpts.VerticalToLineCheckThinLinesActive = True
                Else
                    regOpts.VerticalToLineCheckThinLinesActive = False
                End If


                regOpts.VerticalToLineMaximumLineGap = VerticalToLineMaximumLineGap.Value

                regOpts.VerticalToLineMaximumLineThickness = VerticalToLineMaximumLineThickness.Value

                regOpts.VerticalToLineMinimumLineLength = VerticalToLineMinimumLineLength.Value


                regResults = ScanFix1.RegisterImage(regOpts)

                MessageBox.Show("Horizontal Margin Adjustment: " + regResults.HorizontalMarginAdjustment.ToString() + Chr(13) + "Vertical Margin Adjustment: " + regResults.VerticalMarginAdjustment.ToString() + Chr(13) + "Image was modified: " + regResults.ImageWasModified.ToString(), "Image Registration Results", MessageBoxButtons.OK, MessageBoxIcon.Information)

                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False), True)

                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                    '**pass the image data to ScanFix for processing
                    ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub RegisterTab_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles RegisterTab.Paint
        HorizontalMinimumActivityLabel2.Text = HorizontalMinimumActivity.Value.ToString()
        HorizontalMinimumBackgroundLabel2.Text = HorizontalMinimumBackground.Value.ToString()
        HorizontalMinimumForegroundLabel2.Text = HorizontalMinimumForeground.Value.ToString()
        HorizontalResultantMarginLabel2.Text = HorizontalResultantMargin.Value.ToString()
        HorizontalSkipDistanceLabel2.Text = HorizontalSkipDistance.Value.ToString()
        HorizontalToLineMaximumLineGapLabel2.Text = HorizontalToLineMaximumLineGap.Value.ToString()
        HorizontalToLineMaximumLineThicknessLabel2.Text = HorizontalToLineMaximumLineThickness.Value.ToString()
        HorizontalToLineMinimumLineLengthLabel2.Text = HorizontalToLineMinimumLineLength.Value.ToString()

        VerticalMinimumActivityLabel2.Text = VerticalMinimumActivity.Value.ToString()
        VerticalMinimumBackgroundLabel2.Text = VerticalMinimumBackground.Value.ToString()
        VerticalMinimumForegroundLabel2.Text = VerticalMinimumForeground.Value.ToString()
        VerticalResultantMarginLabel2.Text = VerticalResultantMargin.Value.ToString()
        VerticalSkipDistanceLabel2.Text = VerticalSkipDistance.Value.ToString()
        VerticalToLineMaximumLineGapLabel2.Text = VerticalToLineMaximumLineGap.Value.ToString()
        VerticalToLineMaximumLineThicknessLabel2.Text = VerticalToLineMaximumLineThickness.Value.ToString()
        VerticalToLineMinimumLineLengthLabel2.Text = VerticalToLineMinimumLineLength.Value.ToString()
    End Sub

    Private Sub HorizontalToLineMaximumLineGap_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalToLineMaximumLineGap.Scroll
        HorizontalToLineMaximumLineGapLabel2.Text = HorizontalToLineMaximumLineGap.Value.ToString()
    End Sub

    Private Sub VerticalMinimumActivity_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalMinimumActivity.Scroll
        VerticalMinimumActivityLabel2.Text = VerticalMinimumActivity.Value.ToString()
    End Sub

    Private Sub VerticalToLineMaximumLineThickness_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalToLineMaximumLineThickness.Scroll
        VerticalToLineMaximumLineThicknessLabel2.Text = VerticalToLineMaximumLineThickness.Value.ToString()
    End Sub

    Private Sub HorizontalSkipDistance_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalSkipDistance.Scroll
        HorizontalSkipDistanceLabel2.Text = HorizontalSkipDistance.Value.ToString()
    End Sub

    Private Sub HorizontalToLineMaximumLineThickness_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalToLineMaximumLineThickness.Scroll
        HorizontalToLineMaximumLineThicknessLabel2.Text = HorizontalToLineMaximumLineThickness.Value.ToString()
    End Sub

    Private Sub VerticalResultantMargin_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalResultantMargin.Scroll
        VerticalResultantMarginLabel2.Text = VerticalResultantMargin.Value.ToString()
    End Sub

    Private Sub VerticalToLineMinimumLineLength_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalToLineMinimumLineLength.Scroll
        VerticalToLineMinimumLineLengthLabel2.Text = VerticalToLineMinimumLineLength.Value.ToString()
    End Sub

    Private Sub VerticalMinimumForeground_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalMinimumForeground.Scroll
        VerticalMinimumForegroundLabel2.Text = VerticalMinimumForeground.Value.ToString()
    End Sub

    Private Sub VerticalSkipDistance_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalSkipDistance.Scroll
        VerticalSkipDistanceLabel2.Text = VerticalSkipDistance.Value.ToString()
    End Sub

    Private Sub VerticalMinimumBackground_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalMinimumBackground.Scroll
        VerticalMinimumBackgroundLabel2.Text = VerticalMinimumBackground.Value.ToString()
    End Sub

    Private Sub HorizontalMinimumActivity_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalMinimumActivity.Scroll
        HorizontalMinimumActivityLabel2.Text = HorizontalMinimumActivity.Value.ToString()
    End Sub

    Private Sub HorizontalMinimumForeground_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalMinimumForeground.Scroll
        HorizontalMinimumForegroundLabel2.Text = HorizontalMinimumForeground.Value.ToString()
    End Sub

    Private Sub HorizontalMinimumBackground_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalMinimumBackground.Scroll
        HorizontalMinimumBackgroundLabel2.Text = HorizontalMinimumBackground.Value.ToString()
    End Sub

    Private Sub HorizontalResultantMargin_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalResultantMargin.Scroll
        HorizontalResultantMarginLabel2.Text = HorizontalResultantMargin.Value.ToString()
    End Sub

    Private Sub VerticalToLineMaximumLineGap_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles VerticalToLineMaximumLineGap.Scroll
        VerticalToLineMaximumLineGapLabel2.Text = VerticalToLineMaximumLineGap.Value.ToString()
    End Sub

    Private Sub HorizontalToLineMinimumLineLength_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HorizontalToLineMinimumLineLength.Scroll
        HorizontalToLineMinimumLineLengthLabel2.Text = HorizontalToLineMinimumLineLength.Value.ToString()
    End Sub
End Class
/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.ScanFix5;


namespace ImageCleanUp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainScreen : System.Windows.Forms.Form
	{
		private System.Windows.Forms.OpenFileDialog cd;
		private System.Windows.Forms.MainMenu menu;
		private System.Windows.Forms.TabControl settings;
		private System.Windows.Forms.TabPage tabborder;
		private System.Windows.Forms.TabPage tabdeskew;
		private System.Windows.Forms.TabPage tabdespeck;
		private System.Windows.Forms.TabPage tabsmooth;
		private System.Windows.Forms.TabPage tabline;
		private System.Windows.Forms.CheckBox chkcrop;
		private System.Windows.Forms.CheckBox chkreplace;
		private System.Windows.Forms.CheckBox chkdeskew;
		private System.Windows.Forms.ComboBox cmbpad;
		private System.Windows.Forms.HScrollBar hScrollBar1;
		private System.Windows.Forms.HScrollBar hScrollBar2;
		private System.Windows.Forms.HScrollBar hScrollBar3;
		private System.Windows.Forms.HScrollBar hScrollBar4;
		private System.Windows.Forms.Button cmdborder;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.HScrollBar hScrollBar5;
		private System.Windows.Forms.HScrollBar hScrollBar6;
		private System.Windows.Forms.HScrollBar hScrollBar7;
		private System.Windows.Forms.HScrollBar hScrollBar8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.HScrollBar hScrollBar9;
		private System.Windows.Forms.Button cmdprocess;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.HScrollBar hScrollBar10;
		private System.Windows.Forms.HScrollBar hScrollBar11;
		private System.Windows.Forms.HScrollBar hScrollBar12;
		private System.Windows.Forms.HScrollBar hScrollBar13;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;

		string strImageFile;
		string strCurrentDir;

		private PegasusImaging.WinForms.ScanFix5.ScanFix scanFix1;

		//**create the options and results variables here
		PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions borderopt;
		PegasusImaging.WinForms.ScanFix5.BorderRemovalResults borderres;

		PegasusImaging.WinForms.ScanFix5.LineRemovalOptions lineopt;
		PegasusImaging.WinForms.ScanFix5.LineRemovalResults lineres;

		PegasusImaging.WinForms.ScanFix5.DeskewOptions deskewopt;
	    PegasusImaging.WinForms.ScanFix5.DeskewResults deskewres;

		PegasusImaging.WinForms.ScanFix5.DespeckleOptions speckopt;
		PegasusImaging.WinForms.ScanFix5.DespeckleResults speckres;

		PegasusImaging.WinForms.ScanFix5.BlankPageDetectOptions blankopt;
		PegasusImaging.WinForms.ScanFix5.BlankPageDetectResults blankres;

		PegasusImaging.WinForms.ScanFix5.CombRemovalOptions combopt;
		PegasusImaging.WinForms.ScanFix5.CombRemovalResults combres;

		PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions dotopt;
		PegasusImaging.WinForms.ScanFix5.DotShadingRemovalResults dotres;

		PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions blobopt;
		PegasusImaging.WinForms.ScanFix5.BlobRemovalResults blobres;

		PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions negpageopt;
		PegasusImaging.WinForms.ScanFix5.NegativeCorrectionResults negpageres;

		PegasusImaging.WinForms.ScanFix5.InverseTextOptions inverseopt;
		PegasusImaging.WinForms.ScanFix5.InverseTextResults inverseres;

		PegasusImaging.WinForms.ScanFix5.DilateOptions dilateopt;
		PegasusImaging.WinForms.ScanFix5.ErodeOptions erodeopt;

		PegasusImaging.WinForms.ScanFix5.ImageRegistrationOptions regOpts;
		PegasusImaging.WinForms.ScanFix5.ImageRegistrationResults regResults;

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.HScrollBar hScrollBar14;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.HScrollBar hScrollBar15;
		private System.Windows.Forms.HScrollBar hScrollBar16;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.TabPage tabblob;
		private System.Windows.Forms.TabPage tabdilate;
		private System.Windows.Forms.TabPage tabdotshade;
		private System.Windows.Forms.TabPage tabblank;
		private System.Windows.Forms.TabPage tabnegpage;
		private System.Windows.Forms.TabPage tabcomb;
		private System.Windows.Forms.TabPage tabinverse;
		private System.Windows.Forms.TabPage taberode;
		internal System.Windows.Forms.Label label37;
		internal System.Windows.Forms.Label label38;
		internal System.Windows.Forms.Label label39;
		internal System.Windows.Forms.Label label40;
		internal System.Windows.Forms.Label label41;
		internal System.Windows.Forms.Label label42;
		internal System.Windows.Forms.Label label43;
		internal System.Windows.Forms.Label label44;
		internal System.Windows.Forms.Label label45;
		internal System.Windows.Forms.Label label46;
		internal System.Windows.Forms.Label label47;
		internal System.Windows.Forms.Label label48;
		internal System.Windows.Forms.Label label49;
		internal System.Windows.Forms.Label label50;
		internal System.Windows.Forms.HScrollBar scrollden;
		internal System.Windows.Forms.HScrollBar scrollm;
		internal System.Windows.Forms.HScrollBar scrollmin;
		internal System.Windows.Forms.HScrollBar scrolh;
		internal System.Windows.Forms.HScrollBar scrollw;
		internal System.Windows.Forms.HScrollBar scrolly;
		internal System.Windows.Forms.HScrollBar scrollx;
		internal System.Windows.Forms.Button cmdblob;
		internal System.Windows.Forms.Button Button5;
		internal System.Windows.Forms.Label Label83;
		internal System.Windows.Forms.Label Label82;
		internal System.Windows.Forms.HScrollBar HScrollBar30;
		internal System.Windows.Forms.CheckBox CheckBox9;
		internal System.Windows.Forms.CheckBox CheckBox8;
		internal System.Windows.Forms.CheckBox CheckBox7;
		internal System.Windows.Forms.CheckBox CheckBox6;
		internal System.Windows.Forms.CheckBox CheckBox5;
		internal System.Windows.Forms.CheckBox CheckBox4;
		internal System.Windows.Forms.Label Label57;
		internal System.Windows.Forms.Label Label58;
		internal System.Windows.Forms.Label Label59;
		internal System.Windows.Forms.Label Label60;
		internal System.Windows.Forms.Label Label61;
		internal System.Windows.Forms.Label Label62;
		internal System.Windows.Forms.Label Label63;
		internal System.Windows.Forms.Label Label64;
		internal System.Windows.Forms.Label Label65;
		internal System.Windows.Forms.Label Label66;
		internal System.Windows.Forms.Label Label67;
		internal System.Windows.Forms.Label Label68;
		internal System.Windows.Forms.HScrollBar HScrollBar18;
		internal System.Windows.Forms.HScrollBar HScrollBar19;
		internal System.Windows.Forms.HScrollBar HScrollBar20;
		internal System.Windows.Forms.HScrollBar HScrollBar21;
		internal System.Windows.Forms.HScrollBar HScrollBar22;
		internal System.Windows.Forms.HScrollBar HScrollBar23;
		internal System.Windows.Forms.Button button4;
		internal System.Windows.Forms.CheckBox CheckBox2;
		internal System.Windows.Forms.Label label51;
		internal System.Windows.Forms.Label label52;
		internal System.Windows.Forms.Label label53;
		internal System.Windows.Forms.Label label54;
		internal System.Windows.Forms.Label label55;
		internal System.Windows.Forms.Label label56;
		internal System.Windows.Forms.Label label69;
		internal System.Windows.Forms.Label label70;
		internal System.Windows.Forms.Label label71;
		internal System.Windows.Forms.Label label72;
		internal System.Windows.Forms.Label label73;
		internal System.Windows.Forms.Label label74;
		internal System.Windows.Forms.HScrollBar hScrollBar17;
		internal System.Windows.Forms.HScrollBar hScrollBar24;
		internal System.Windows.Forms.HScrollBar hScrollBar25;
		internal System.Windows.Forms.HScrollBar hScrollBar26;
		internal System.Windows.Forms.HScrollBar hScrollBar27;
		internal System.Windows.Forms.HScrollBar hScrollBar28;
		internal System.Windows.Forms.Button cmdBlank;
		internal System.Windows.Forms.CheckBox CheckBox3;
		internal System.Windows.Forms.Label label75;
		internal System.Windows.Forms.Label label76;
		internal System.Windows.Forms.Label label77;
		internal System.Windows.Forms.Label label78;
		internal System.Windows.Forms.HScrollBar hScrollBar29;
		internal System.Windows.Forms.HScrollBar hScrollBar31;
		internal System.Windows.Forms.Button button6;
		internal System.Windows.Forms.Label label79;
		internal System.Windows.Forms.Label label80;
		internal System.Windows.Forms.Label label81;
		internal System.Windows.Forms.Label label84;
		internal System.Windows.Forms.Label label85;
		internal System.Windows.Forms.Label label86;
		internal System.Windows.Forms.Label label87;
		internal System.Windows.Forms.Label label88;
		internal System.Windows.Forms.Label label89;
		internal System.Windows.Forms.Label label90;
		internal System.Windows.Forms.HScrollBar hScrollBar32;
		internal System.Windows.Forms.HScrollBar hScrollBar33;
		internal System.Windows.Forms.HScrollBar hScrollBar34;
		internal System.Windows.Forms.HScrollBar hScrollBar35;
		internal System.Windows.Forms.HScrollBar hScrollBar36;
		internal System.Windows.Forms.Label label91;
		internal System.Windows.Forms.Label label92;
		internal System.Windows.Forms.Label label93;
		internal System.Windows.Forms.Label label94;
		internal System.Windows.Forms.Label label95;
		internal System.Windows.Forms.Label label96;
		internal System.Windows.Forms.Label label97;
		internal System.Windows.Forms.Label label98;
		internal System.Windows.Forms.Label label99;
		internal System.Windows.Forms.Label label100;
		internal System.Windows.Forms.HScrollBar hScrollBar37;
		internal System.Windows.Forms.HScrollBar hScrollBar38;
		internal System.Windows.Forms.HScrollBar hScrollBar39;
		internal System.Windows.Forms.HScrollBar hScrollBar40;
		internal System.Windows.Forms.HScrollBar hScrollBar41;
		internal System.Windows.Forms.Button button7;
		internal System.Windows.Forms.Button cmdinverse;
		internal System.Windows.Forms.Label label101;
		internal System.Windows.Forms.Label label102;
		internal System.Windows.Forms.Label label103;
		internal System.Windows.Forms.Label label104;
		internal System.Windows.Forms.Label label105;
		internal System.Windows.Forms.Label label106;
		internal System.Windows.Forms.HScrollBar minblack;
		internal System.Windows.Forms.HScrollBar minheight;
		internal System.Windows.Forms.HScrollBar minwidth;
		internal System.Windows.Forms.Button button8;
		internal System.Windows.Forms.Label label107;
		internal System.Windows.Forms.Label label108;
		internal System.Windows.Forms.HScrollBar hScrollBar42;
		internal System.Windows.Forms.CheckBox CheckBox10;
		internal System.Windows.Forms.CheckBox CheckBox11;
		internal System.Windows.Forms.CheckBox CheckBox12;
		internal System.Windows.Forms.CheckBox CheckBox13;
		internal System.Windows.Forms.CheckBox CheckBox14;
		internal System.Windows.Forms.CheckBox CheckBox15;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;
		internal System.Windows.Forms.Label lblout;
		internal System.Windows.Forms.Label lblinput;
		private System.Windows.Forms.ListBox DescriptionListBox;
		private System.Windows.Forms.MenuItem FileMenu;
		private System.Windows.Forms.MenuItem ExitMenuItem;
		private System.Windows.Forms.MenuItem AboutMenu;
		private System.Windows.Forms.MenuItem ScanFixMenuItem;
		private System.Windows.Forms.MenuItem ImagXpressMenuItem;
		private System.Windows.Forms.MenuItem OpenMenuItem;
		private System.Windows.Forms.TabPage RegisterTab;
		private System.Windows.Forms.CheckBox HorizontalActiveCheckBox;
		private System.Windows.Forms.Button RegisterButton;
		private System.Windows.Forms.CheckBox HorizontalAddOnlyCheckBox;
		private System.Windows.Forms.CheckBox HorizontalCentralFocusCheckBox;
		private System.Windows.Forms.CheckBox HorizontalIgnoreHolesCheckBox;
		internal System.Windows.Forms.HScrollBar HorizontalMinimumActivity;
		internal System.Windows.Forms.Label HorizontalMinimumActivityLabel;
		internal System.Windows.Forms.Label HorizontalMinimumActivityLabel2;
		internal System.Windows.Forms.Label HorizontalMinimumBackgroundLabel2;
		internal System.Windows.Forms.Label HorizontalMinimumBackgroundLabel;
		internal System.Windows.Forms.HScrollBar HorizontalMinimumBackground;
		internal System.Windows.Forms.Label HorizontalMinimumForegroundLabel2;
		internal System.Windows.Forms.Label HorizontalMinimumForegroundLabel;
		internal System.Windows.Forms.HScrollBar HorizontalMinimumForeground;
		internal System.Windows.Forms.Label HorizontalResultantMarginLabel2;
		internal System.Windows.Forms.Label HorizontalResultantMarginLabel;
		internal System.Windows.Forms.HScrollBar HorizontalResultantMargin;
		private System.Windows.Forms.CheckBox HorizontalSkipActiveCheckBox;
		internal System.Windows.Forms.Label HorizontalSkipDistanceLabel2;
		internal System.Windows.Forms.HScrollBar HorizontalSkipDistance;
		private System.Windows.Forms.CheckBox HorizontalToLineActiveCheckBox;
		private System.Windows.Forms.CheckBox HorizontalToLineCheckThinLinesActiveCheckBox;
		internal System.Windows.Forms.Label HorizontalToLineMaximumLineGapLabel2;
		internal System.Windows.Forms.Label HorizontalToLineMaximumLineGapLabel;
		internal System.Windows.Forms.HScrollBar HorizontalToLineMaximumLineGap;
		internal System.Windows.Forms.Label HorizontalToLineMaximumLineThicknessLabel2;
		internal System.Windows.Forms.Label HorizontalToLineMaximumLineThicknessLabel;
		internal System.Windows.Forms.HScrollBar HorizontalToLineMaximumLineThickness;
		private System.Windows.Forms.GroupBox VerticalLinesGroupBox;
		private System.Windows.Forms.GroupBox HorizontalLinesGroupBox;
		private System.Windows.Forms.CheckBox VerticalToLineCheckThinLinesActiveCheckBox;
		private System.Windows.Forms.CheckBox VerticalToLineActiveCheckBox;
		private System.Windows.Forms.CheckBox VerticalSkipActiveCheckBox;
		internal System.Windows.Forms.Label VerticalResultantMarginLabel2;
		internal System.Windows.Forms.Label VerticalResultantMarginLabel;
		internal System.Windows.Forms.HScrollBar VerticalResultantMargin;
		internal System.Windows.Forms.Label VerticalMinimumForegroundLabel2;
		internal System.Windows.Forms.Label VerticalMinimumForegroundLabel;
		internal System.Windows.Forms.HScrollBar VerticalMinimumForeground;
		internal System.Windows.Forms.Label VerticalMinimumBackgroundLabel2;
		internal System.Windows.Forms.Label VerticalMinimumBackgroundLabel;
		internal System.Windows.Forms.HScrollBar VerticalMinimumBackground;
		internal System.Windows.Forms.Label VerticalMinimumActivityLabel2;
		internal System.Windows.Forms.Label VerticalMinimumActivityLabel;
		internal System.Windows.Forms.HScrollBar VerticalMinimumActivity;
		private System.Windows.Forms.CheckBox VerticalCentralFocusCheckBox;
		private System.Windows.Forms.CheckBox VerticalAddOnlyCheckBox;
		private System.Windows.Forms.CheckBox VerticalActiveCheckBox;
		internal System.Windows.Forms.Label VerticalToLineMaximumLineThicknessLabel2;
		internal System.Windows.Forms.Label VerticalToLineMaximumLineThicknessLabel;
		internal System.Windows.Forms.HScrollBar VerticalToLineMaximumLineThickness;
		internal System.Windows.Forms.Label VerticalToLineMaximumLineGapLabel2;
		internal System.Windows.Forms.Label VerticalToLineMaximumLineGapLabel;
		internal System.Windows.Forms.HScrollBar VerticalToLineMaximumLineGap;
		internal System.Windows.Forms.Label VerticalSkipDistanceLabel2;
		internal System.Windows.Forms.Label VerticalSkipDistanceLabel;
		internal System.Windows.Forms.HScrollBar VerticalSkipDistance;
		internal System.Windows.Forms.Label HorizontalToLineMinimumLineLengthLabel2;
		internal System.Windows.Forms.Label HorizontalToLineMinimumLineLengthLabel;
		internal System.Windows.Forms.HScrollBar HorizontalToLineMinimumLineLength;
		internal System.Windows.Forms.Label HorizontalSkipDistanceLabel;
		internal System.Windows.Forms.Label VerticalToLineMinimumLineLengthLabel2;
		internal System.Windows.Forms.Label VerticalToLineMinimumLineLengthLabel;
		internal System.Windows.Forms.HScrollBar VerticalToLineMinimumLineLength;
		private System.Windows.Forms.ComboBox cmbcolor;

		public MainScreen()
		{
			//
			// Required for Windows Form Designer support
			//


			//***Must call UnlockControl before InitializeComponent statement
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234);
			//PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234);


			InitializeComponent();

			scanFix1 = new PegasusImaging.WinForms.ScanFix5.ScanFix();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (imageXView1.Image != null)
				{
					imageXView1.Image.Dispose();
					imageXView1.Image = null;
				}
				if (imageXView2.Image != null)
				{
					imageXView2.Image.Dispose();
					imageXView2.Image = null;
				}

				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}

				if (!(imageXView2 == null)) 
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}

				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}

				if (scanFix1 != null)
				{
					scanFix1.Dispose();
					scanFix1 = null;
				}

				if (components != null)
				{
					components.Dispose();
				}

			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cd = new System.Windows.Forms.OpenFileDialog();
			this.menu = new System.Windows.Forms.MainMenu();
			this.FileMenu = new System.Windows.Forms.MenuItem();
			this.OpenMenuItem = new System.Windows.Forms.MenuItem();
			this.ExitMenuItem = new System.Windows.Forms.MenuItem();
			this.AboutMenu = new System.Windows.Forms.MenuItem();
			this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
			this.ScanFixMenuItem = new System.Windows.Forms.MenuItem();
			this.settings = new System.Windows.Forms.TabControl();
			this.tabborder = new System.Windows.Forms.TabPage();
			this.label36 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.hScrollBar8 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar7 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar6 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar5 = new System.Windows.Forms.HScrollBar();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cmdborder = new System.Windows.Forms.Button();
			this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar3 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar2 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
			this.cmbpad = new System.Windows.Forms.ComboBox();
			this.chkdeskew = new System.Windows.Forms.CheckBox();
			this.chkreplace = new System.Windows.Forms.CheckBox();
			this.chkcrop = new System.Windows.Forms.CheckBox();
			this.tabdotshade = new System.Windows.Forms.TabPage();
			this.Label57 = new System.Windows.Forms.Label();
			this.Label58 = new System.Windows.Forms.Label();
			this.Label59 = new System.Windows.Forms.Label();
			this.Label60 = new System.Windows.Forms.Label();
			this.Label61 = new System.Windows.Forms.Label();
			this.Label62 = new System.Windows.Forms.Label();
			this.Label63 = new System.Windows.Forms.Label();
			this.Label64 = new System.Windows.Forms.Label();
			this.Label65 = new System.Windows.Forms.Label();
			this.Label66 = new System.Windows.Forms.Label();
			this.Label67 = new System.Windows.Forms.Label();
			this.Label68 = new System.Windows.Forms.Label();
			this.HScrollBar18 = new System.Windows.Forms.HScrollBar();
			this.HScrollBar19 = new System.Windows.Forms.HScrollBar();
			this.HScrollBar20 = new System.Windows.Forms.HScrollBar();
			this.HScrollBar21 = new System.Windows.Forms.HScrollBar();
			this.HScrollBar22 = new System.Windows.Forms.HScrollBar();
			this.HScrollBar23 = new System.Windows.Forms.HScrollBar();
			this.button4 = new System.Windows.Forms.Button();
			this.tabblank = new System.Windows.Forms.TabPage();
			this.CheckBox2 = new System.Windows.Forms.CheckBox();
			this.label51 = new System.Windows.Forms.Label();
			this.label52 = new System.Windows.Forms.Label();
			this.label53 = new System.Windows.Forms.Label();
			this.label54 = new System.Windows.Forms.Label();
			this.label55 = new System.Windows.Forms.Label();
			this.label56 = new System.Windows.Forms.Label();
			this.label69 = new System.Windows.Forms.Label();
			this.label70 = new System.Windows.Forms.Label();
			this.label71 = new System.Windows.Forms.Label();
			this.label72 = new System.Windows.Forms.Label();
			this.label73 = new System.Windows.Forms.Label();
			this.label74 = new System.Windows.Forms.Label();
			this.hScrollBar17 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar24 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar25 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar26 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar27 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar28 = new System.Windows.Forms.HScrollBar();
			this.cmdBlank = new System.Windows.Forms.Button();
			this.tabdilate = new System.Windows.Forms.TabPage();
			this.Button5 = new System.Windows.Forms.Button();
			this.Label83 = new System.Windows.Forms.Label();
			this.Label82 = new System.Windows.Forms.Label();
			this.HScrollBar30 = new System.Windows.Forms.HScrollBar();
			this.CheckBox9 = new System.Windows.Forms.CheckBox();
			this.CheckBox8 = new System.Windows.Forms.CheckBox();
			this.CheckBox7 = new System.Windows.Forms.CheckBox();
			this.CheckBox6 = new System.Windows.Forms.CheckBox();
			this.CheckBox5 = new System.Windows.Forms.CheckBox();
			this.CheckBox4 = new System.Windows.Forms.CheckBox();
			this.tabblob = new System.Windows.Forms.TabPage();
			this.label37 = new System.Windows.Forms.Label();
			this.label38 = new System.Windows.Forms.Label();
			this.label39 = new System.Windows.Forms.Label();
			this.label40 = new System.Windows.Forms.Label();
			this.label41 = new System.Windows.Forms.Label();
			this.label42 = new System.Windows.Forms.Label();
			this.label43 = new System.Windows.Forms.Label();
			this.label44 = new System.Windows.Forms.Label();
			this.label45 = new System.Windows.Forms.Label();
			this.label46 = new System.Windows.Forms.Label();
			this.label47 = new System.Windows.Forms.Label();
			this.label48 = new System.Windows.Forms.Label();
			this.label49 = new System.Windows.Forms.Label();
			this.label50 = new System.Windows.Forms.Label();
			this.scrollden = new System.Windows.Forms.HScrollBar();
			this.scrollm = new System.Windows.Forms.HScrollBar();
			this.scrollmin = new System.Windows.Forms.HScrollBar();
			this.scrolh = new System.Windows.Forms.HScrollBar();
			this.scrollw = new System.Windows.Forms.HScrollBar();
			this.scrolly = new System.Windows.Forms.HScrollBar();
			this.scrollx = new System.Windows.Forms.HScrollBar();
			this.cmdblob = new System.Windows.Forms.Button();
			this.tabdespeck = new System.Windows.Forms.TabPage();
			this.label35 = new System.Windows.Forms.Label();
			this.label34 = new System.Windows.Forms.Label();
			this.label33 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this.hScrollBar16 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar15 = new System.Windows.Forms.HScrollBar();
			this.button3 = new System.Windows.Forms.Button();
			this.tabdeskew = new System.Windows.Forms.TabPage();
			this.button2 = new System.Windows.Forms.Button();
			this.label31 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.hScrollBar14 = new System.Windows.Forms.HScrollBar();
			this.cmbcolor = new System.Windows.Forms.ComboBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.tabline = new System.Windows.Forms.TabPage();
			this.button1 = new System.Windows.Forms.Button();
			this.label27 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.hScrollBar13 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar12 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar11 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar10 = new System.Windows.Forms.HScrollBar();
			this.tabnegpage = new System.Windows.Forms.TabPage();
			this.CheckBox3 = new System.Windows.Forms.CheckBox();
			this.label75 = new System.Windows.Forms.Label();
			this.label76 = new System.Windows.Forms.Label();
			this.label77 = new System.Windows.Forms.Label();
			this.label78 = new System.Windows.Forms.Label();
			this.hScrollBar29 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar31 = new System.Windows.Forms.HScrollBar();
			this.button6 = new System.Windows.Forms.Button();
			this.tabcomb = new System.Windows.Forms.TabPage();
			this.label79 = new System.Windows.Forms.Label();
			this.label80 = new System.Windows.Forms.Label();
			this.label81 = new System.Windows.Forms.Label();
			this.label84 = new System.Windows.Forms.Label();
			this.label85 = new System.Windows.Forms.Label();
			this.label86 = new System.Windows.Forms.Label();
			this.label87 = new System.Windows.Forms.Label();
			this.label88 = new System.Windows.Forms.Label();
			this.label89 = new System.Windows.Forms.Label();
			this.label90 = new System.Windows.Forms.Label();
			this.hScrollBar32 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar33 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar34 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar35 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar36 = new System.Windows.Forms.HScrollBar();
			this.label91 = new System.Windows.Forms.Label();
			this.label92 = new System.Windows.Forms.Label();
			this.label93 = new System.Windows.Forms.Label();
			this.label94 = new System.Windows.Forms.Label();
			this.label95 = new System.Windows.Forms.Label();
			this.label96 = new System.Windows.Forms.Label();
			this.label97 = new System.Windows.Forms.Label();
			this.label98 = new System.Windows.Forms.Label();
			this.label99 = new System.Windows.Forms.Label();
			this.label100 = new System.Windows.Forms.Label();
			this.hScrollBar37 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar38 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar39 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar40 = new System.Windows.Forms.HScrollBar();
			this.hScrollBar41 = new System.Windows.Forms.HScrollBar();
			this.button7 = new System.Windows.Forms.Button();
			this.tabinverse = new System.Windows.Forms.TabPage();
			this.cmdinverse = new System.Windows.Forms.Button();
			this.label101 = new System.Windows.Forms.Label();
			this.label102 = new System.Windows.Forms.Label();
			this.label103 = new System.Windows.Forms.Label();
			this.label104 = new System.Windows.Forms.Label();
			this.label105 = new System.Windows.Forms.Label();
			this.label106 = new System.Windows.Forms.Label();
			this.minblack = new System.Windows.Forms.HScrollBar();
			this.minheight = new System.Windows.Forms.HScrollBar();
			this.minwidth = new System.Windows.Forms.HScrollBar();
			this.tabsmooth = new System.Windows.Forms.TabPage();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.cmdprocess = new System.Windows.Forms.Button();
			this.hScrollBar9 = new System.Windows.Forms.HScrollBar();
			this.taberode = new System.Windows.Forms.TabPage();
			this.button8 = new System.Windows.Forms.Button();
			this.label107 = new System.Windows.Forms.Label();
			this.label108 = new System.Windows.Forms.Label();
			this.hScrollBar42 = new System.Windows.Forms.HScrollBar();
			this.CheckBox10 = new System.Windows.Forms.CheckBox();
			this.CheckBox11 = new System.Windows.Forms.CheckBox();
			this.CheckBox12 = new System.Windows.Forms.CheckBox();
			this.CheckBox13 = new System.Windows.Forms.CheckBox();
			this.CheckBox14 = new System.Windows.Forms.CheckBox();
			this.CheckBox15 = new System.Windows.Forms.CheckBox();
			this.RegisterTab = new System.Windows.Forms.TabPage();
			this.VerticalToLineMinimumLineLengthLabel2 = new System.Windows.Forms.Label();
			this.VerticalToLineMinimumLineLengthLabel = new System.Windows.Forms.Label();
			this.VerticalToLineMinimumLineLength = new System.Windows.Forms.HScrollBar();
			this.VerticalToLineMaximumLineThicknessLabel2 = new System.Windows.Forms.Label();
			this.VerticalToLineMaximumLineThicknessLabel = new System.Windows.Forms.Label();
			this.VerticalToLineMaximumLineThickness = new System.Windows.Forms.HScrollBar();
			this.VerticalToLineMaximumLineGapLabel2 = new System.Windows.Forms.Label();
			this.VerticalToLineMaximumLineGapLabel = new System.Windows.Forms.Label();
			this.VerticalToLineMaximumLineGap = new System.Windows.Forms.HScrollBar();
			this.VerticalSkipDistanceLabel2 = new System.Windows.Forms.Label();
			this.VerticalSkipDistanceLabel = new System.Windows.Forms.Label();
			this.VerticalSkipDistance = new System.Windows.Forms.HScrollBar();
			this.VerticalResultantMarginLabel2 = new System.Windows.Forms.Label();
			this.VerticalResultantMarginLabel = new System.Windows.Forms.Label();
			this.VerticalResultantMargin = new System.Windows.Forms.HScrollBar();
			this.VerticalMinimumForegroundLabel2 = new System.Windows.Forms.Label();
			this.VerticalMinimumForegroundLabel = new System.Windows.Forms.Label();
			this.VerticalMinimumForeground = new System.Windows.Forms.HScrollBar();
			this.VerticalMinimumBackgroundLabel2 = new System.Windows.Forms.Label();
			this.VerticalMinimumBackgroundLabel = new System.Windows.Forms.Label();
			this.VerticalMinimumBackground = new System.Windows.Forms.HScrollBar();
			this.VerticalMinimumActivityLabel2 = new System.Windows.Forms.Label();
			this.VerticalMinimumActivityLabel = new System.Windows.Forms.Label();
			this.VerticalMinimumActivity = new System.Windows.Forms.HScrollBar();
			this.VerticalCentralFocusCheckBox = new System.Windows.Forms.CheckBox();
			this.VerticalAddOnlyCheckBox = new System.Windows.Forms.CheckBox();
			this.VerticalActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalToLineMinimumLineLengthLabel2 = new System.Windows.Forms.Label();
			this.HorizontalToLineMinimumLineLengthLabel = new System.Windows.Forms.Label();
			this.HorizontalToLineMinimumLineLength = new System.Windows.Forms.HScrollBar();
			this.HorizontalToLineMaximumLineThicknessLabel2 = new System.Windows.Forms.Label();
			this.HorizontalToLineMaximumLineThicknessLabel = new System.Windows.Forms.Label();
			this.HorizontalToLineMaximumLineThickness = new System.Windows.Forms.HScrollBar();
			this.HorizontalToLineMaximumLineGapLabel2 = new System.Windows.Forms.Label();
			this.HorizontalToLineMaximumLineGapLabel = new System.Windows.Forms.Label();
			this.HorizontalToLineMaximumLineGap = new System.Windows.Forms.HScrollBar();
			this.HorizontalToLineCheckThinLinesActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalToLineActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalSkipDistanceLabel2 = new System.Windows.Forms.Label();
			this.HorizontalSkipDistanceLabel = new System.Windows.Forms.Label();
			this.HorizontalSkipDistance = new System.Windows.Forms.HScrollBar();
			this.HorizontalSkipActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalResultantMarginLabel2 = new System.Windows.Forms.Label();
			this.HorizontalResultantMarginLabel = new System.Windows.Forms.Label();
			this.HorizontalResultantMargin = new System.Windows.Forms.HScrollBar();
			this.HorizontalMinimumForegroundLabel2 = new System.Windows.Forms.Label();
			this.HorizontalMinimumForegroundLabel = new System.Windows.Forms.Label();
			this.HorizontalMinimumForeground = new System.Windows.Forms.HScrollBar();
			this.HorizontalMinimumBackgroundLabel2 = new System.Windows.Forms.Label();
			this.HorizontalMinimumBackgroundLabel = new System.Windows.Forms.Label();
			this.HorizontalMinimumBackground = new System.Windows.Forms.HScrollBar();
			this.HorizontalMinimumActivityLabel2 = new System.Windows.Forms.Label();
			this.HorizontalMinimumActivityLabel = new System.Windows.Forms.Label();
			this.HorizontalMinimumActivity = new System.Windows.Forms.HScrollBar();
			this.HorizontalIgnoreHolesCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalCentralFocusCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalAddOnlyCheckBox = new System.Windows.Forms.CheckBox();
			this.RegisterButton = new System.Windows.Forms.Button();
			this.HorizontalActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.VerticalLinesGroupBox = new System.Windows.Forms.GroupBox();
			this.VerticalSkipActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.VerticalToLineActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.VerticalToLineCheckThinLinesActiveCheckBox = new System.Windows.Forms.CheckBox();
			this.HorizontalLinesGroupBox = new System.Windows.Forms.GroupBox();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.DescriptionListBox = new System.Windows.Forms.ListBox();
			this.lblout = new System.Windows.Forms.Label();
			this.lblinput = new System.Windows.Forms.Label();
			this.settings.SuspendLayout();
			this.tabborder.SuspendLayout();
			this.tabdotshade.SuspendLayout();
			this.tabblank.SuspendLayout();
			this.tabdilate.SuspendLayout();
			this.tabblob.SuspendLayout();
			this.tabdespeck.SuspendLayout();
			this.tabdeskew.SuspendLayout();
			this.tabline.SuspendLayout();
			this.tabnegpage.SuspendLayout();
			this.tabcomb.SuspendLayout();
			this.tabinverse.SuspendLayout();
			this.tabsmooth.SuspendLayout();
			this.taberode.SuspendLayout();
			this.RegisterTab.SuspendLayout();
			this.VerticalLinesGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// menu
			// 
			this.menu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				 this.FileMenu,
																				 this.AboutMenu});
			// 
			// FileMenu
			// 
			this.FileMenu.Index = 0;
			this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.OpenMenuItem,
																					 this.ExitMenuItem});
			this.FileMenu.Text = "&File";
			// 
			// OpenMenuItem
			// 
			this.OpenMenuItem.Index = 0;
			this.OpenMenuItem.Text = "&Open";
			this.OpenMenuItem.Click += new System.EventHandler(this.OpenMenuItem_Click);
			// 
			// ExitMenuItem
			// 
			this.ExitMenuItem.Index = 1;
			this.ExitMenuItem.Text = "E&xit";
			this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
			// 
			// AboutMenu
			// 
			this.AboutMenu.Index = 1;
			this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.ImagXpressMenuItem,
																					  this.ScanFixMenuItem});
			this.AboutMenu.Text = "&About";
			// 
			// ImagXpressMenuItem
			// 
			this.ImagXpressMenuItem.Index = 0;
			this.ImagXpressMenuItem.Text = "Imag&Xpress";
			this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
			// 
			// ScanFixMenuItem
			// 
			this.ScanFixMenuItem.Index = 1;
			this.ScanFixMenuItem.Text = "Scan&Fix";
			this.ScanFixMenuItem.Click += new System.EventHandler(this.ScanFixMenuItem_Click);
			// 
			// settings
			// 
			this.settings.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.tabborder,
																				   this.tabdotshade,
																				   this.tabblank,
																				   this.tabdilate,
																				   this.tabblob,
																				   this.tabdespeck,
																				   this.tabdeskew,
																				   this.tabline,
																				   this.tabnegpage,
																				   this.tabcomb,
																				   this.tabinverse,
																				   this.tabsmooth,
																				   this.taberode,
																				   this.RegisterTab});
			this.settings.Location = new System.Drawing.Point(8, 368);
			this.settings.Multiline = true;
			this.settings.Name = "settings";
			this.settings.SelectedIndex = 0;
			this.settings.Size = new System.Drawing.Size(752, 488);
			this.settings.TabIndex = 3;
			// 
			// tabborder
			// 
			this.tabborder.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.label36,
																					this.label16,
																					this.label15,
																					this.label14,
																					this.label13,
																					this.label12,
																					this.label11,
																					this.label10,
																					this.label9,
																					this.hScrollBar8,
																					this.hScrollBar7,
																					this.hScrollBar6,
																					this.hScrollBar5,
																					this.label8,
																					this.label7,
																					this.label6,
																					this.label5,
																					this.label4,
																					this.label3,
																					this.label2,
																					this.label1,
																					this.cmdborder,
																					this.hScrollBar4,
																					this.hScrollBar3,
																					this.hScrollBar2,
																					this.hScrollBar1,
																					this.cmbpad,
																					this.chkdeskew,
																					this.chkreplace,
																					this.chkcrop});
			this.tabborder.Location = new System.Drawing.Point(4, 40);
			this.tabborder.Name = "tabborder";
			this.tabborder.Size = new System.Drawing.Size(744, 444);
			this.tabborder.TabIndex = 0;
			this.tabborder.Text = "Border Removal";
			this.tabborder.Paint += new System.Windows.Forms.PaintEventHandler(this.tabborder_Paint);
			// 
			// label36
			// 
			this.label36.Location = new System.Drawing.Point(16, 112);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(56, 16);
			this.label36.TabIndex = 30;
			this.label36.Text = "Pad Color";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(648, 160);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(64, 16);
			this.label16.TabIndex = 29;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(648, 112);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(48, 16);
			this.label15.TabIndex = 28;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(648, 64);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(40, 16);
			this.label14.TabIndex = 27;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(648, 8);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(40, 24);
			this.label13.TabIndex = 26;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(520, 160);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(96, 16);
			this.label12.TabIndex = 25;
			this.label12.Text = "Max Page Height";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(520, 112);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(88, 16);
			this.label11.TabIndex = 24;
			this.label11.Text = "Max Page Width";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(520, 64);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(88, 16);
			this.label10.TabIndex = 23;
			this.label10.Text = "Min Page Height";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(520, 8);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(96, 16);
			this.label9.TabIndex = 22;
			this.label9.Text = "Min Page Width";
			// 
			// hScrollBar8
			// 
			this.hScrollBar8.Location = new System.Drawing.Point(520, 184);
			this.hScrollBar8.Maximum = 9999;
			this.hScrollBar8.Name = "hScrollBar8";
			this.hScrollBar8.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar8.TabIndex = 21;
			this.hScrollBar8.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar8_Scroll);
			// 
			// hScrollBar7
			// 
			this.hScrollBar7.Location = new System.Drawing.Point(520, 136);
			this.hScrollBar7.Maximum = 9999;
			this.hScrollBar7.Name = "hScrollBar7";
			this.hScrollBar7.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar7.TabIndex = 20;
			this.hScrollBar7.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar7_Scroll);
			// 
			// hScrollBar6
			// 
			this.hScrollBar6.LargeChange = 5;
			this.hScrollBar6.Location = new System.Drawing.Point(520, 88);
			this.hScrollBar6.Maximum = 9999;
			this.hScrollBar6.Name = "hScrollBar6";
			this.hScrollBar6.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar6.TabIndex = 19;
			this.hScrollBar6.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar6_Scroll);
			// 
			// hScrollBar5
			// 
			this.hScrollBar5.LargeChange = 5;
			this.hScrollBar5.Location = new System.Drawing.Point(520, 40);
			this.hScrollBar5.Maximum = 9999;
			this.hScrollBar5.Name = "hScrollBar5";
			this.hScrollBar5.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar5.TabIndex = 18;
			this.hScrollBar5.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar5_Scroll);
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(336, 160);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(56, 16);
			this.label8.TabIndex = 17;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(336, 112);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(56, 16);
			this.label7.TabIndex = 16;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(336, 64);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(48, 16);
			this.label6.TabIndex = 15;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(336, 8);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(56, 16);
			this.label5.TabIndex = 14;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(192, 160);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(120, 16);
			this.label4.TabIndex = 13;
			this.label4.Text = "Minimum Confidence";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(192, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 16);
			this.label3.TabIndex = 12;
			this.label3.Text = "Quality";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(192, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 11;
			this.label2.Text = "Page Speck Size";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(184, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 24);
			this.label1.TabIndex = 10;
			this.label1.Text = "Border Speck Size";
			// 
			// cmdborder
			// 
			this.cmdborder.Location = new System.Drawing.Point(24, 168);
			this.cmdborder.Name = "cmdborder";
			this.cmdborder.Size = new System.Drawing.Size(112, 24);
			this.cmdborder.TabIndex = 9;
			this.cmdborder.Text = "Process";
			this.cmdborder.Click += new System.EventHandler(this.cmdborder_Click);
			// 
			// hScrollBar4
			// 
			this.hScrollBar4.LargeChange = 1;
			this.hScrollBar4.Location = new System.Drawing.Point(192, 184);
			this.hScrollBar4.Minimum = 1;
			this.hScrollBar4.Name = "hScrollBar4";
			this.hScrollBar4.Size = new System.Drawing.Size(200, 16);
			this.hScrollBar4.TabIndex = 8;
			this.hScrollBar4.Value = 50;
			this.hScrollBar4.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar4_Scroll);
			// 
			// hScrollBar3
			// 
			this.hScrollBar3.LargeChange = 1;
			this.hScrollBar3.Location = new System.Drawing.Point(192, 136);
			this.hScrollBar3.Minimum = 1;
			this.hScrollBar3.Name = "hScrollBar3";
			this.hScrollBar3.Size = new System.Drawing.Size(200, 16);
			this.hScrollBar3.TabIndex = 7;
			this.hScrollBar3.Value = 80;
			this.hScrollBar3.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar3_Scroll);
			// 
			// hScrollBar2
			// 
			this.hScrollBar2.LargeChange = 1;
			this.hScrollBar2.Location = new System.Drawing.Point(192, 88);
			this.hScrollBar2.Name = "hScrollBar2";
			this.hScrollBar2.Size = new System.Drawing.Size(200, 16);
			this.hScrollBar2.TabIndex = 6;
			this.hScrollBar2.Value = 2;
			this.hScrollBar2.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar2_Scroll);
			// 
			// hScrollBar1
			// 
			this.hScrollBar1.LargeChange = 1;
			this.hScrollBar1.Location = new System.Drawing.Point(192, 40);
			this.hScrollBar1.Name = "hScrollBar1";
			this.hScrollBar1.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar1.TabIndex = 5;
			this.hScrollBar1.Value = 3;
			this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
			// 
			// cmbpad
			// 
			this.cmbpad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbpad.Items.AddRange(new object[] {
														"White",
														"Black"});
			this.cmbpad.Location = new System.Drawing.Point(80, 112);
			this.cmbpad.Name = "cmbpad";
			this.cmbpad.Size = new System.Drawing.Size(80, 21);
			this.cmbpad.TabIndex = 4;
			// 
			// chkdeskew
			// 
			this.chkdeskew.Checked = true;
			this.chkdeskew.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkdeskew.Location = new System.Drawing.Point(16, 80);
			this.chkdeskew.Name = "chkdeskew";
			this.chkdeskew.Size = new System.Drawing.Size(64, 16);
			this.chkdeskew.TabIndex = 2;
			this.chkdeskew.Text = "Deskew Border";
			// 
			// chkreplace
			// 
			this.chkreplace.Location = new System.Drawing.Point(16, 48);
			this.chkreplace.Name = "chkreplace";
			this.chkreplace.Size = new System.Drawing.Size(104, 16);
			this.chkreplace.TabIndex = 1;
			this.chkreplace.Text = "Replace Border";
			// 
			// chkcrop
			// 
			this.chkcrop.Checked = true;
			this.chkcrop.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkcrop.Location = new System.Drawing.Point(16, 16);
			this.chkcrop.Name = "chkcrop";
			this.chkcrop.Size = new System.Drawing.Size(88, 16);
			this.chkcrop.TabIndex = 0;
			this.chkcrop.Text = "Crop Border";
			// 
			// tabdotshade
			// 
			this.tabdotshade.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.Label57,
																					  this.Label58,
																					  this.Label59,
																					  this.Label60,
																					  this.Label61,
																					  this.Label62,
																					  this.Label63,
																					  this.Label64,
																					  this.Label65,
																					  this.Label66,
																					  this.Label67,
																					  this.Label68,
																					  this.HScrollBar18,
																					  this.HScrollBar19,
																					  this.HScrollBar20,
																					  this.HScrollBar21,
																					  this.HScrollBar22,
																					  this.HScrollBar23,
																					  this.button4});
			this.tabdotshade.Location = new System.Drawing.Point(4, 40);
			this.tabdotshade.Name = "tabdotshade";
			this.tabdotshade.Size = new System.Drawing.Size(744, 444);
			this.tabdotshade.TabIndex = 8;
			this.tabdotshade.Text = "Dot Shading Removal";
			this.tabdotshade.Paint += new System.Windows.Forms.PaintEventHandler(this.tabdotshade_Paint);
			// 
			// Label57
			// 
			this.Label57.Location = new System.Drawing.Point(456, 174);
			this.Label57.Name = "Label57";
			this.Label57.Size = new System.Drawing.Size(72, 16);
			this.Label57.TabIndex = 80;
			// 
			// Label58
			// 
			this.Label58.Location = new System.Drawing.Point(456, 142);
			this.Label58.Name = "Label58";
			this.Label58.Size = new System.Drawing.Size(64, 16);
			this.Label58.TabIndex = 79;
			// 
			// Label59
			// 
			this.Label59.Location = new System.Drawing.Point(456, 110);
			this.Label59.Name = "Label59";
			this.Label59.Size = new System.Drawing.Size(72, 16);
			this.Label59.TabIndex = 78;
			// 
			// Label60
			// 
			this.Label60.Location = new System.Drawing.Point(456, 86);
			this.Label60.Name = "Label60";
			this.Label60.Size = new System.Drawing.Size(64, 16);
			this.Label60.TabIndex = 77;
			// 
			// Label61
			// 
			this.Label61.Location = new System.Drawing.Point(456, 54);
			this.Label61.Name = "Label61";
			this.Label61.Size = new System.Drawing.Size(80, 16);
			this.Label61.TabIndex = 76;
			// 
			// Label62
			// 
			this.Label62.Location = new System.Drawing.Point(456, 14);
			this.Label62.Name = "Label62";
			this.Label62.Size = new System.Drawing.Size(72, 16);
			this.Label62.TabIndex = 75;
			// 
			// Label63
			// 
			this.Label63.Location = new System.Drawing.Point(112, 174);
			this.Label63.Name = "Label63";
			this.Label63.Size = new System.Drawing.Size(104, 16);
			this.Label63.TabIndex = 74;
			this.Label63.Text = "Max Dot Size";
			// 
			// Label64
			// 
			this.Label64.Location = new System.Drawing.Point(112, 142);
			this.Label64.Name = "Label64";
			this.Label64.Size = new System.Drawing.Size(116, 16);
			this.Label64.TabIndex = 73;
			this.Label64.Text = "Dot Shading Density";
			// 
			// Label65
			// 
			this.Label65.Location = new System.Drawing.Point(112, 110);
			this.Label65.Name = "Label65";
			this.Label65.Size = new System.Drawing.Size(88, 26);
			this.Label65.TabIndex = 72;
			this.Label65.Text = "Horizontal Size Adjustment";
			// 
			// Label66
			// 
			this.Label66.Location = new System.Drawing.Point(112, 78);
			this.Label66.Name = "Label66";
			this.Label66.Size = new System.Drawing.Size(128, 16);
			this.Label66.TabIndex = 71;
			this.Label66.Text = "Vertical Size Adjustment";
			// 
			// Label67
			// 
			this.Label67.Location = new System.Drawing.Point(112, 46);
			this.Label67.Name = "Label67";
			this.Label67.Size = new System.Drawing.Size(96, 16);
			this.Label67.TabIndex = 70;
			this.Label67.Text = "Min Area Height";
			// 
			// Label68
			// 
			this.Label68.Location = new System.Drawing.Point(112, 14);
			this.Label68.Name = "Label68";
			this.Label68.Size = new System.Drawing.Size(88, 16);
			this.Label68.TabIndex = 69;
			this.Label68.Text = "Min Area Width";
			// 
			// HScrollBar18
			// 
			this.HScrollBar18.LargeChange = 1;
			this.HScrollBar18.Location = new System.Drawing.Point(248, 174);
			this.HScrollBar18.Maximum = 20;
			this.HScrollBar18.Name = "HScrollBar18";
			this.HScrollBar18.Size = new System.Drawing.Size(192, 16);
			this.HScrollBar18.TabIndex = 68;
			this.HScrollBar18.Value = 5;
			this.HScrollBar18.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar18_Scroll);
			// 
			// HScrollBar19
			// 
			this.HScrollBar19.LargeChange = 1;
			this.HScrollBar19.Location = new System.Drawing.Point(248, 142);
			this.HScrollBar19.Maximum = 50;
			this.HScrollBar19.Minimum = -50;
			this.HScrollBar19.Name = "HScrollBar19";
			this.HScrollBar19.Size = new System.Drawing.Size(192, 16);
			this.HScrollBar19.TabIndex = 67;
			this.HScrollBar19.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar19_Scroll);
			// 
			// HScrollBar20
			// 
			this.HScrollBar20.LargeChange = 1;
			this.HScrollBar20.Location = new System.Drawing.Point(248, 110);
			this.HScrollBar20.Minimum = -100;
			this.HScrollBar20.Name = "HScrollBar20";
			this.HScrollBar20.Size = new System.Drawing.Size(192, 16);
			this.HScrollBar20.TabIndex = 66;
			this.HScrollBar20.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar20_Scroll);
			// 
			// HScrollBar21
			// 
			this.HScrollBar21.LargeChange = 1;
			this.HScrollBar21.Location = new System.Drawing.Point(248, 78);
			this.HScrollBar21.Minimum = -100;
			this.HScrollBar21.Name = "HScrollBar21";
			this.HScrollBar21.Size = new System.Drawing.Size(192, 16);
			this.HScrollBar21.TabIndex = 65;
			this.HScrollBar21.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar21_Scroll);
			// 
			// HScrollBar22
			// 
			this.HScrollBar22.LargeChange = 1;
			this.HScrollBar22.Location = new System.Drawing.Point(248, 46);
			this.HScrollBar22.Maximum = 9999;
			this.HScrollBar22.Name = "HScrollBar22";
			this.HScrollBar22.Size = new System.Drawing.Size(192, 16);
			this.HScrollBar22.TabIndex = 64;
			this.HScrollBar22.Value = 50;
			this.HScrollBar22.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar22_Scroll);
			// 
			// HScrollBar23
			// 
			this.HScrollBar23.LargeChange = 1;
			this.HScrollBar23.Location = new System.Drawing.Point(248, 14);
			this.HScrollBar23.Maximum = 9999;
			this.HScrollBar23.Name = "HScrollBar23";
			this.HScrollBar23.Size = new System.Drawing.Size(192, 16);
			this.HScrollBar23.TabIndex = 63;
			this.HScrollBar23.Value = 300;
			this.HScrollBar23.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar23_Scroll);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(568, 86);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(64, 40);
			this.button4.TabIndex = 62;
			this.button4.Text = "Process Image";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// tabblank
			// 
			this.tabblank.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.CheckBox2,
																				   this.label51,
																				   this.label52,
																				   this.label53,
																				   this.label54,
																				   this.label55,
																				   this.label56,
																				   this.label69,
																				   this.label70,
																				   this.label71,
																				   this.label72,
																				   this.label73,
																				   this.label74,
																				   this.hScrollBar17,
																				   this.hScrollBar24,
																				   this.hScrollBar25,
																				   this.hScrollBar26,
																				   this.hScrollBar27,
																				   this.hScrollBar28,
																				   this.cmdBlank});
			this.tabblank.Location = new System.Drawing.Point(4, 40);
			this.tabblank.Name = "tabblank";
			this.tabblank.Size = new System.Drawing.Size(744, 444);
			this.tabblank.TabIndex = 9;
			this.tabblank.Text = "Blank Page Detection";
			this.tabblank.Paint += new System.Windows.Forms.PaintEventHandler(this.tabblank_Paint);
			// 
			// CheckBox2
			// 
			this.CheckBox2.Location = new System.Drawing.Point(544, 14);
			this.CheckBox2.Name = "CheckBox2";
			this.CheckBox2.Size = new System.Drawing.Size(96, 24);
			this.CheckBox2.TabIndex = 63;
			this.CheckBox2.Text = "Ignore Border";
			// 
			// label51
			// 
			this.label51.Location = new System.Drawing.Point(448, 174);
			this.label51.Name = "label51";
			this.label51.Size = new System.Drawing.Size(72, 16);
			this.label51.TabIndex = 62;
			// 
			// label52
			// 
			this.label52.Location = new System.Drawing.Point(448, 142);
			this.label52.Name = "label52";
			this.label52.Size = new System.Drawing.Size(64, 16);
			this.label52.TabIndex = 61;
			// 
			// label53
			// 
			this.label53.Location = new System.Drawing.Point(448, 110);
			this.label53.Name = "label53";
			this.label53.Size = new System.Drawing.Size(72, 16);
			this.label53.TabIndex = 60;
			// 
			// label54
			// 
			this.label54.Location = new System.Drawing.Point(448, 80);
			this.label54.Name = "label54";
			this.label54.Size = new System.Drawing.Size(64, 16);
			this.label54.TabIndex = 59;
			// 
			// label55
			// 
			this.label55.Location = new System.Drawing.Point(448, 54);
			this.label55.Name = "label55";
			this.label55.Size = new System.Drawing.Size(80, 16);
			this.label55.TabIndex = 58;
			// 
			// label56
			// 
			this.label56.Location = new System.Drawing.Point(448, 14);
			this.label56.Name = "label56";
			this.label56.Size = new System.Drawing.Size(72, 16);
			this.label56.TabIndex = 57;
			// 
			// label69
			// 
			this.label69.Location = new System.Drawing.Point(104, 174);
			this.label69.Name = "label69";
			this.label69.Size = new System.Drawing.Size(104, 16);
			this.label69.TabIndex = 56;
			this.label69.Text = "Gap Fill";
			// 
			// label70
			// 
			this.label70.Location = new System.Drawing.Point(104, 142);
			this.label70.Name = "label70";
			this.label70.Size = new System.Drawing.Size(116, 16);
			this.label70.TabIndex = 55;
			this.label70.Text = "Min Object Dimension";
			// 
			// label71
			// 
			this.label71.Location = new System.Drawing.Point(104, 110);
			this.label71.Name = "label71";
			this.label71.Size = new System.Drawing.Size(88, 16);
			this.label71.TabIndex = 54;
			this.label71.Text = "Right Margin";
			// 
			// label72
			// 
			this.label72.Location = new System.Drawing.Point(104, 78);
			this.label72.Name = "label72";
			this.label72.Size = new System.Drawing.Size(96, 16);
			this.label72.TabIndex = 53;
			this.label72.Text = "Bottom Margin";
			// 
			// label73
			// 
			this.label73.Location = new System.Drawing.Point(104, 46);
			this.label73.Name = "label73";
			this.label73.Size = new System.Drawing.Size(64, 16);
			this.label73.TabIndex = 52;
			this.label73.Text = "Left Margin";
			// 
			// label74
			// 
			this.label74.Location = new System.Drawing.Point(104, 14);
			this.label74.Name = "label74";
			this.label74.Size = new System.Drawing.Size(64, 16);
			this.label74.TabIndex = 51;
			this.label74.Text = "Top Margin";
			// 
			// hScrollBar17
			// 
			this.hScrollBar17.LargeChange = 1;
			this.hScrollBar17.Location = new System.Drawing.Point(240, 174);
			this.hScrollBar17.Maximum = 9999;
			this.hScrollBar17.Name = "hScrollBar17";
			this.hScrollBar17.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar17.TabIndex = 50;
			this.hScrollBar17.Value = 5;
			this.hScrollBar17.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar17_Scroll);
			// 
			// hScrollBar24
			// 
			this.hScrollBar24.LargeChange = 1;
			this.hScrollBar24.Location = new System.Drawing.Point(240, 142);
			this.hScrollBar24.Maximum = 9999;
			this.hScrollBar24.Name = "hScrollBar24";
			this.hScrollBar24.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar24.TabIndex = 49;
			this.hScrollBar24.Value = 10;
			this.hScrollBar24.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar24_Scroll);
			// 
			// hScrollBar25
			// 
			this.hScrollBar25.LargeChange = 1;
			this.hScrollBar25.Location = new System.Drawing.Point(240, 110);
			this.hScrollBar25.Maximum = 9999;
			this.hScrollBar25.Name = "hScrollBar25";
			this.hScrollBar25.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar25.TabIndex = 48;
			this.hScrollBar25.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar25_Scroll);
			// 
			// hScrollBar26
			// 
			this.hScrollBar26.LargeChange = 1;
			this.hScrollBar26.Location = new System.Drawing.Point(240, 78);
			this.hScrollBar26.Maximum = 9999;
			this.hScrollBar26.Name = "hScrollBar26";
			this.hScrollBar26.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar26.TabIndex = 47;
			this.hScrollBar26.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar26_Scroll);
			// 
			// hScrollBar27
			// 
			this.hScrollBar27.LargeChange = 1;
			this.hScrollBar27.Location = new System.Drawing.Point(240, 46);
			this.hScrollBar27.Maximum = 9999;
			this.hScrollBar27.Name = "hScrollBar27";
			this.hScrollBar27.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar27.TabIndex = 46;
			this.hScrollBar27.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar27_Scroll);
			// 
			// hScrollBar28
			// 
			this.hScrollBar28.LargeChange = 1;
			this.hScrollBar28.Location = new System.Drawing.Point(240, 14);
			this.hScrollBar28.Maximum = 9999;
			this.hScrollBar28.Name = "hScrollBar28";
			this.hScrollBar28.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar28.TabIndex = 45;
			this.hScrollBar28.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar28_Scroll);
			// 
			// cmdBlank
			// 
			this.cmdBlank.Location = new System.Drawing.Point(560, 86);
			this.cmdBlank.Name = "cmdBlank";
			this.cmdBlank.Size = new System.Drawing.Size(64, 40);
			this.cmdBlank.TabIndex = 44;
			this.cmdBlank.Text = "Process Image";
			this.cmdBlank.Click += new System.EventHandler(this.cmdBlank_Click);
			// 
			// tabdilate
			// 
			this.tabdilate.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.Button5,
																					this.Label83,
																					this.Label82,
																					this.HScrollBar30,
																					this.CheckBox9,
																					this.CheckBox8,
																					this.CheckBox7,
																					this.CheckBox6,
																					this.CheckBox5,
																					this.CheckBox4});
			this.tabdilate.Location = new System.Drawing.Point(4, 40);
			this.tabdilate.Name = "tabdilate";
			this.tabdilate.Size = new System.Drawing.Size(744, 444);
			this.tabdilate.TabIndex = 7;
			this.tabdilate.Text = "Dilate";
			this.tabdilate.Paint += new System.Windows.Forms.PaintEventHandler(this.tabdilate_Paint);
			// 
			// Button5
			// 
			this.Button5.Location = new System.Drawing.Point(484, 86);
			this.Button5.Name = "Button5";
			this.Button5.Size = new System.Drawing.Size(64, 40);
			this.Button5.TabIndex = 87;
			this.Button5.Text = "Process Image";
			this.Button5.Click += new System.EventHandler(this.Button5_Click);
			// 
			// Label83
			// 
			this.Label83.Location = new System.Drawing.Point(276, 62);
			this.Label83.Name = "Label83";
			this.Label83.Size = new System.Drawing.Size(56, 16);
			this.Label83.TabIndex = 86;
			this.Label83.Text = "Label83";
			// 
			// Label82
			// 
			this.Label82.Location = new System.Drawing.Point(196, 62);
			this.Label82.Name = "Label82";
			this.Label82.Size = new System.Drawing.Size(56, 16);
			this.Label82.TabIndex = 85;
			this.Label82.Text = "Amount";
			// 
			// HScrollBar30
			// 
			this.HScrollBar30.Location = new System.Drawing.Point(196, 86);
			this.HScrollBar30.Minimum = 1;
			this.HScrollBar30.Name = "HScrollBar30";
			this.HScrollBar30.Size = new System.Drawing.Size(200, 16);
			this.HScrollBar30.TabIndex = 84;
			this.HScrollBar30.Value = 1;
			this.HScrollBar30.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HScrollBar30_Scroll);
			// 
			// CheckBox9
			// 
			this.CheckBox9.Enabled = false;
			this.CheckBox9.Location = new System.Drawing.Point(340, 158);
			this.CheckBox9.Name = "CheckBox9";
			this.CheckBox9.Size = new System.Drawing.Size(56, 16);
			this.CheckBox9.TabIndex = 83;
			this.CheckBox9.Text = "Right";
			this.CheckBox9.CheckedChanged += new System.EventHandler(this.CheckBox9_CheckedChanged);
			// 
			// CheckBox8
			// 
			this.CheckBox8.Enabled = false;
			this.CheckBox8.Location = new System.Drawing.Point(340, 126);
			this.CheckBox8.Name = "CheckBox8";
			this.CheckBox8.Size = new System.Drawing.Size(56, 16);
			this.CheckBox8.TabIndex = 82;
			this.CheckBox8.Text = "Down";
			this.CheckBox8.CheckedChanged += new System.EventHandler(this.CheckBox8_CheckedChanged);
			// 
			// CheckBox7
			// 
			this.CheckBox7.Enabled = false;
			this.CheckBox7.Location = new System.Drawing.Point(268, 158);
			this.CheckBox7.Name = "CheckBox7";
			this.CheckBox7.Size = new System.Drawing.Size(48, 16);
			this.CheckBox7.TabIndex = 81;
			this.CheckBox7.Text = "Left";
			this.CheckBox7.CheckedChanged += new System.EventHandler(this.CheckBox7_CheckedChanged);
			// 
			// CheckBox6
			// 
			this.CheckBox6.Enabled = false;
			this.CheckBox6.Location = new System.Drawing.Point(268, 126);
			this.CheckBox6.Name = "CheckBox6";
			this.CheckBox6.Size = new System.Drawing.Size(40, 16);
			this.CheckBox6.TabIndex = 80;
			this.CheckBox6.Text = "Up";
			this.CheckBox6.CheckedChanged += new System.EventHandler(this.CheckBox6_CheckedChanged);
			// 
			// CheckBox5
			// 
			this.CheckBox5.Checked = true;
			this.CheckBox5.CheckState = System.Windows.Forms.CheckState.Checked;
			this.CheckBox5.Location = new System.Drawing.Point(196, 142);
			this.CheckBox5.Name = "CheckBox5";
			this.CheckBox5.Size = new System.Drawing.Size(48, 16);
			this.CheckBox5.TabIndex = 79;
			this.CheckBox5.Text = "All";
			this.CheckBox5.CheckedChanged += new System.EventHandler(this.CheckBox5_CheckedChanged);
			// 
			// CheckBox4
			// 
			this.CheckBox4.Enabled = false;
			this.CheckBox4.Location = new System.Drawing.Point(196, 30);
			this.CheckBox4.Name = "CheckBox4";
			this.CheckBox4.Size = new System.Drawing.Size(136, 24);
			this.CheckBox4.TabIndex = 78;
			this.CheckBox4.Text = "Diagonal Only";
			// 
			// tabblob
			// 
			this.tabblob.Controls.AddRange(new System.Windows.Forms.Control[] {
																				  this.label37,
																				  this.label38,
																				  this.label39,
																				  this.label40,
																				  this.label41,
																				  this.label42,
																				  this.label43,
																				  this.label44,
																				  this.label45,
																				  this.label46,
																				  this.label47,
																				  this.label48,
																				  this.label49,
																				  this.label50,
																				  this.scrollden,
																				  this.scrollm,
																				  this.scrollmin,
																				  this.scrolh,
																				  this.scrollw,
																				  this.scrolly,
																				  this.scrollx,
																				  this.cmdblob});
			this.tabblob.Location = new System.Drawing.Point(4, 40);
			this.tabblob.Name = "tabblob";
			this.tabblob.Size = new System.Drawing.Size(744, 444);
			this.tabblob.TabIndex = 6;
			this.tabblob.Text = "Blob Removal";
			this.tabblob.Paint += new System.Windows.Forms.PaintEventHandler(this.tabblob_paint);
			// 
			// label37
			// 
			this.label37.Location = new System.Drawing.Point(696, 104);
			this.label37.Name = "label37";
			this.label37.Size = new System.Drawing.Size(88, 16);
			this.label37.TabIndex = 43;
			// 
			// label38
			// 
			this.label38.Location = new System.Drawing.Point(688, 72);
			this.label38.Name = "label38";
			this.label38.Size = new System.Drawing.Size(72, 16);
			this.label38.TabIndex = 42;
			// 
			// label39
			// 
			this.label39.Location = new System.Drawing.Point(688, 40);
			this.label39.Name = "label39";
			this.label39.Size = new System.Drawing.Size(64, 16);
			this.label39.TabIndex = 41;
			// 
			// label40
			// 
			this.label40.Location = new System.Drawing.Point(304, 136);
			this.label40.Name = "label40";
			this.label40.Size = new System.Drawing.Size(72, 16);
			this.label40.TabIndex = 40;
			// 
			// label41
			// 
			this.label41.Location = new System.Drawing.Point(304, 104);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(64, 16);
			this.label41.TabIndex = 39;
			// 
			// label42
			// 
			this.label42.Location = new System.Drawing.Point(304, 80);
			this.label42.Name = "label42";
			this.label42.Size = new System.Drawing.Size(80, 16);
			this.label42.TabIndex = 38;
			// 
			// label43
			// 
			this.label43.Location = new System.Drawing.Point(304, 40);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(72, 16);
			this.label43.TabIndex = 37;
			// 
			// label44
			// 
			this.label44.Location = new System.Drawing.Point(384, 104);
			this.label44.Name = "label44";
			this.label44.Size = new System.Drawing.Size(120, 16);
			this.label44.TabIndex = 36;
			this.label44.Text = "Minimum Density";
			// 
			// label45
			// 
			this.label45.Location = new System.Drawing.Point(384, 72);
			this.label45.Name = "label45";
			this.label45.Size = new System.Drawing.Size(104, 16);
			this.label45.TabIndex = 35;
			this.label45.Text = "Max Pixel Count";
			// 
			// label46
			// 
			this.label46.Location = new System.Drawing.Point(384, 40);
			this.label46.Name = "label46";
			this.label46.Size = new System.Drawing.Size(96, 16);
			this.label46.TabIndex = 34;
			this.label46.Text = "Min Pixel Count";
			// 
			// label47
			// 
			this.label47.Location = new System.Drawing.Point(8, 136);
			this.label47.Name = "label47";
			this.label47.Size = new System.Drawing.Size(88, 16);
			this.label47.TabIndex = 33;
			this.label47.Text = "Area Height";
			// 
			// label48
			// 
			this.label48.Location = new System.Drawing.Point(8, 104);
			this.label48.Name = "label48";
			this.label48.Size = new System.Drawing.Size(96, 16);
			this.label48.TabIndex = 32;
			this.label48.Text = "Area Width";
			// 
			// label49
			// 
			this.label49.Location = new System.Drawing.Point(8, 72);
			this.label49.Name = "label49";
			this.label49.Size = new System.Drawing.Size(64, 16);
			this.label49.TabIndex = 31;
			this.label49.Text = "Area Y";
			// 
			// label50
			// 
			this.label50.Location = new System.Drawing.Point(8, 40);
			this.label50.Name = "label50";
			this.label50.Size = new System.Drawing.Size(64, 16);
			this.label50.TabIndex = 30;
			this.label50.Text = "Area X";
			// 
			// scrollden
			// 
			this.scrollden.LargeChange = 1;
			this.scrollden.Location = new System.Drawing.Point(520, 104);
			this.scrollden.Name = "scrollden";
			this.scrollden.Size = new System.Drawing.Size(156, 16);
			this.scrollden.TabIndex = 29;
			this.scrollden.Value = 50;
			this.scrollden.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrollden_Scroll);
			// 
			// scrollm
			// 
			this.scrollm.LargeChange = 1;
			this.scrollm.Location = new System.Drawing.Point(520, 72);
			this.scrollm.Maximum = 9999;
			this.scrollm.Name = "scrollm";
			this.scrollm.Size = new System.Drawing.Size(156, 16);
			this.scrollm.TabIndex = 28;
			this.scrollm.Value = 9999;
			this.scrollm.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrollm_Scroll);
			// 
			// scrollmin
			// 
			this.scrollmin.LargeChange = 1;
			this.scrollmin.Location = new System.Drawing.Point(520, 40);
			this.scrollmin.Maximum = 9999;
			this.scrollmin.Minimum = 1;
			this.scrollmin.Name = "scrollmin";
			this.scrollmin.Size = new System.Drawing.Size(156, 16);
			this.scrollmin.TabIndex = 27;
			this.scrollmin.Value = 300;
			this.scrollmin.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrollmin_Scroll);
			// 
			// scrolh
			// 
			this.scrolh.LargeChange = 1;
			this.scrolh.Location = new System.Drawing.Point(144, 136);
			this.scrolh.Maximum = 9999;
			this.scrolh.Name = "scrolh";
			this.scrolh.Size = new System.Drawing.Size(156, 16);
			this.scrolh.TabIndex = 26;
			this.scrolh.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrolh_Scroll);
			// 
			// scrollw
			// 
			this.scrollw.LargeChange = 1;
			this.scrollw.Location = new System.Drawing.Point(144, 104);
			this.scrollw.Maximum = 9999;
			this.scrollw.Name = "scrollw";
			this.scrollw.Size = new System.Drawing.Size(156, 16);
			this.scrollw.TabIndex = 25;
			this.scrollw.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrollw_Scroll);
			// 
			// scrolly
			// 
			this.scrolly.LargeChange = 1;
			this.scrolly.Location = new System.Drawing.Point(144, 72);
			this.scrolly.Maximum = 9999;
			this.scrolly.Name = "scrolly";
			this.scrolly.Size = new System.Drawing.Size(156, 16);
			this.scrolly.TabIndex = 24;
			this.scrolly.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrolly_Scroll);
			// 
			// scrollx
			// 
			this.scrollx.LargeChange = 1;
			this.scrollx.Location = new System.Drawing.Point(144, 40);
			this.scrollx.Maximum = 9999;
			this.scrollx.Name = "scrollx";
			this.scrollx.Size = new System.Drawing.Size(156, 16);
			this.scrollx.TabIndex = 23;
			this.scrollx.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scrollx_Scroll);
			// 
			// cmdblob
			// 
			this.cmdblob.Location = new System.Drawing.Point(568, 144);
			this.cmdblob.Name = "cmdblob";
			this.cmdblob.Size = new System.Drawing.Size(64, 40);
			this.cmdblob.TabIndex = 22;
			this.cmdblob.Text = "Process Image";
			this.cmdblob.Click += new System.EventHandler(this.cmdblob_Click);
			// 
			// tabdespeck
			// 
			this.tabdespeck.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.label35,
																					 this.label34,
																					 this.label33,
																					 this.label32,
																					 this.hScrollBar16,
																					 this.hScrollBar15,
																					 this.button3});
			this.tabdespeck.Location = new System.Drawing.Point(4, 40);
			this.tabdespeck.Name = "tabdespeck";
			this.tabdespeck.Size = new System.Drawing.Size(744, 444);
			this.tabdespeck.TabIndex = 2;
			this.tabdespeck.Text = "Despeckle";
			// 
			// label35
			// 
			this.label35.Location = new System.Drawing.Point(288, 96);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(88, 16);
			this.label35.TabIndex = 6;
			// 
			// label34
			// 
			this.label34.Location = new System.Drawing.Point(288, 32);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(80, 16);
			this.label34.TabIndex = 5;
			// 
			// label33
			// 
			this.label33.Location = new System.Drawing.Point(88, 96);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(152, 24);
			this.label33.TabIndex = 4;
			this.label33.Text = "Speck Width:";
			// 
			// label32
			// 
			this.label32.Location = new System.Drawing.Point(88, 32);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(152, 16);
			this.label32.TabIndex = 3;
			this.label32.Text = "Speck Height:";
			// 
			// hScrollBar16
			// 
			this.hScrollBar16.LargeChange = 1;
			this.hScrollBar16.Location = new System.Drawing.Point(88, 128);
			this.hScrollBar16.Minimum = 1;
			this.hScrollBar16.Name = "hScrollBar16";
			this.hScrollBar16.Size = new System.Drawing.Size(304, 16);
			this.hScrollBar16.TabIndex = 2;
			this.hScrollBar16.Value = 1;
			this.hScrollBar16.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar16_Scroll);
			// 
			// hScrollBar15
			// 
			this.hScrollBar15.LargeChange = 1;
			this.hScrollBar15.Location = new System.Drawing.Point(88, 64);
			this.hScrollBar15.Minimum = 1;
			this.hScrollBar15.Name = "hScrollBar15";
			this.hScrollBar15.Size = new System.Drawing.Size(304, 16);
			this.hScrollBar15.TabIndex = 1;
			this.hScrollBar15.Value = 1;
			this.hScrollBar15.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar15_Scroll);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(472, 88);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(128, 32);
			this.button3.TabIndex = 0;
			this.button3.Text = "Process Image";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// tabdeskew
			// 
			this.tabdeskew.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.button2,
																					this.label31,
																					this.label30,
																					this.label29,
																					this.label28,
																					this.textBox2,
																					this.hScrollBar14,
																					this.cmbcolor,
																					this.checkBox1});
			this.tabdeskew.Location = new System.Drawing.Point(4, 40);
			this.tabdeskew.Name = "tabdeskew";
			this.tabdeskew.Size = new System.Drawing.Size(744, 444);
			this.tabdeskew.TabIndex = 1;
			this.tabdeskew.Text = "Deskew";
			this.tabdeskew.Paint += new System.Windows.Forms.PaintEventHandler(this.tabdeskew_Paint);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(608, 80);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(112, 48);
			this.button2.TabIndex = 8;
			this.button2.Text = "Process";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label31
			// 
			this.label31.Location = new System.Drawing.Point(312, 32);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(184, 16);
			this.label31.TabIndex = 7;
			this.label31.Text = "Max Acceptable Angle:";
			// 
			// label30
			// 
			this.label30.Location = new System.Drawing.Point(208, 104);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(72, 32);
			this.label30.TabIndex = 6;
			// 
			// label29
			// 
			this.label29.Location = new System.Drawing.Point(88, 104);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(88, 32);
			this.label29.TabIndex = 5;
			this.label29.Text = "Minimum Confidence:";
			// 
			// label28
			// 
			this.label28.Location = new System.Drawing.Point(88, 64);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(64, 24);
			this.label28.TabIndex = 4;
			this.label28.Text = "Pad Color:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(312, 64);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(160, 20);
			this.textBox2.TabIndex = 3;
			this.textBox2.Text = "0.2";
			// 
			// hScrollBar14
			// 
			this.hScrollBar14.LargeChange = 2;
			this.hScrollBar14.Location = new System.Drawing.Point(88, 144);
			this.hScrollBar14.Name = "hScrollBar14";
			this.hScrollBar14.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar14.TabIndex = 2;
			this.hScrollBar14.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar14_Scroll);
			// 
			// cmbcolor
			// 
			this.cmbcolor.Items.AddRange(new object[] {
														  "White",
														  "Black"});
			this.cmbcolor.Location = new System.Drawing.Point(160, 64);
			this.cmbcolor.Name = "cmbcolor";
			this.cmbcolor.Size = new System.Drawing.Size(112, 21);
			this.cmbcolor.TabIndex = 1;
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(88, 32);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(152, 16);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "Maintain Original Size";
			// 
			// tabline
			// 
			this.tabline.Controls.AddRange(new System.Windows.Forms.Control[] {
																				  this.button1,
																				  this.label27,
																				  this.label26,
																				  this.label25,
																				  this.label24,
																				  this.label23,
																				  this.label22,
																				  this.label21,
																				  this.label20,
																				  this.label19,
																				  this.textBox1,
																				  this.hScrollBar13,
																				  this.hScrollBar12,
																				  this.hScrollBar11,
																				  this.hScrollBar10});
			this.tabline.Location = new System.Drawing.Point(4, 40);
			this.tabline.Name = "tabline";
			this.tabline.Size = new System.Drawing.Size(744, 444);
			this.tabline.TabIndex = 5;
			this.tabline.Text = "Line Removal";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(624, 88);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(80, 32);
			this.button1.TabIndex = 14;
			this.button1.Text = "Process";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label27
			// 
			this.label27.Location = new System.Drawing.Point(16, 168);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(112, 16);
			this.label27.TabIndex = 13;
			this.label27.Text = "Min Aspect Ratio:";
			// 
			// label26
			// 
			this.label26.Location = new System.Drawing.Point(504, 128);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(80, 16);
			this.label26.TabIndex = 12;
			// 
			// label25
			// 
			this.label25.Location = new System.Drawing.Point(504, 88);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(96, 16);
			this.label25.TabIndex = 11;
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(504, 56);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(88, 16);
			this.label24.TabIndex = 10;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(504, 24);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(80, 16);
			this.label23.TabIndex = 9;
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(16, 128);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(144, 16);
			this.label22.TabIndex = 8;
			this.label22.Text = "Max Character Repair Size:";
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(16, 88);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(72, 16);
			this.label21.TabIndex = 7;
			this.label21.Text = "Max Gap:";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(16, 64);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(96, 16);
			this.label20.TabIndex = 6;
			this.label20.Text = "Max ThickNess:";
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(16, 24);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(88, 24);
			this.label19.TabIndex = 5;
			this.label19.Text = "Min Line Length:";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(160, 168);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(160, 20);
			this.textBox1.TabIndex = 4;
			this.textBox1.Text = "10";
			// 
			// hScrollBar13
			// 
			this.hScrollBar13.Location = new System.Drawing.Point(160, 128);
			this.hScrollBar13.Name = "hScrollBar13";
			this.hScrollBar13.Size = new System.Drawing.Size(320, 16);
			this.hScrollBar13.TabIndex = 3;
			this.hScrollBar13.Value = 20;
			this.hScrollBar13.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar13_Scroll);
			// 
			// hScrollBar12
			// 
			this.hScrollBar12.LargeChange = 2;
			this.hScrollBar12.Location = new System.Drawing.Point(160, 88);
			this.hScrollBar12.Maximum = 20;
			this.hScrollBar12.Name = "hScrollBar12";
			this.hScrollBar12.Size = new System.Drawing.Size(320, 16);
			this.hScrollBar12.TabIndex = 2;
			this.hScrollBar12.Value = 1;
			this.hScrollBar12.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar12_Scroll);
			// 
			// hScrollBar11
			// 
			this.hScrollBar11.LargeChange = 1;
			this.hScrollBar11.Location = new System.Drawing.Point(160, 56);
			this.hScrollBar11.Maximum = 50;
			this.hScrollBar11.Minimum = 1;
			this.hScrollBar11.Name = "hScrollBar11";
			this.hScrollBar11.Size = new System.Drawing.Size(320, 16);
			this.hScrollBar11.TabIndex = 1;
			this.hScrollBar11.Value = 20;
			this.hScrollBar11.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar11_Scroll);
			// 
			// hScrollBar10
			// 
			this.hScrollBar10.Location = new System.Drawing.Point(160, 24);
			this.hScrollBar10.Maximum = 20000;
			this.hScrollBar10.Minimum = 10;
			this.hScrollBar10.Name = "hScrollBar10";
			this.hScrollBar10.Size = new System.Drawing.Size(320, 16);
			this.hScrollBar10.TabIndex = 0;
			this.hScrollBar10.Value = 50;
			this.hScrollBar10.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar10_Scroll);
			// 
			// tabnegpage
			// 
			this.tabnegpage.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.CheckBox3,
																					 this.label75,
																					 this.label76,
																					 this.label77,
																					 this.label78,
																					 this.hScrollBar29,
																					 this.hScrollBar31,
																					 this.button6});
			this.tabnegpage.Location = new System.Drawing.Point(4, 40);
			this.tabnegpage.Name = "tabnegpage";
			this.tabnegpage.Size = new System.Drawing.Size(744, 444);
			this.tabnegpage.TabIndex = 10;
			this.tabnegpage.Text = "Negative Page Correction";
			this.tabnegpage.Paint += new System.Windows.Forms.PaintEventHandler(this.tabnegpage_Paint);
			// 
			// CheckBox3
			// 
			this.CheckBox3.Location = new System.Drawing.Point(108, 54);
			this.CheckBox3.Name = "CheckBox3";
			this.CheckBox3.Size = new System.Drawing.Size(96, 24);
			this.CheckBox3.TabIndex = 59;
			this.CheckBox3.Text = "Apply Correction";
			// 
			// label75
			// 
			this.label75.Location = new System.Drawing.Point(452, 128);
			this.label75.Name = "label75";
			this.label75.Size = new System.Drawing.Size(80, 16);
			this.label75.TabIndex = 58;
			// 
			// label76
			// 
			this.label76.Location = new System.Drawing.Point(452, 94);
			this.label76.Name = "label76";
			this.label76.Size = new System.Drawing.Size(72, 16);
			this.label76.TabIndex = 57;
			// 
			// label77
			// 
			this.label77.Location = new System.Drawing.Point(108, 126);
			this.label77.Name = "label77";
			this.label77.Size = new System.Drawing.Size(64, 16);
			this.label77.TabIndex = 56;
			this.label77.Text = "Quality";
			// 
			// label78
			// 
			this.label78.Location = new System.Drawing.Point(108, 94);
			this.label78.Name = "label78";
			this.label78.Size = new System.Drawing.Size(112, 16);
			this.label78.TabIndex = 55;
			this.label78.Text = "Minimum Confidence";
			// 
			// hScrollBar29
			// 
			this.hScrollBar29.LargeChange = 1;
			this.hScrollBar29.Location = new System.Drawing.Point(244, 126);
			this.hScrollBar29.Minimum = 1;
			this.hScrollBar29.Name = "hScrollBar29";
			this.hScrollBar29.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar29.TabIndex = 54;
			this.hScrollBar29.Value = 80;
			this.hScrollBar29.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar29_Scroll);
			// 
			// hScrollBar31
			// 
			this.hScrollBar31.LargeChange = 1;
			this.hScrollBar31.Location = new System.Drawing.Point(244, 94);
			this.hScrollBar31.Name = "hScrollBar31";
			this.hScrollBar31.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar31.TabIndex = 53;
			this.hScrollBar31.Value = 50;
			this.hScrollBar31.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar31_Scroll);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(572, 94);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(64, 40);
			this.button6.TabIndex = 52;
			this.button6.Text = "Process Image";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// tabcomb
			// 
			this.tabcomb.Controls.AddRange(new System.Windows.Forms.Control[] {
																				  this.label79,
																				  this.label80,
																				  this.label81,
																				  this.label84,
																				  this.label85,
																				  this.label86,
																				  this.label87,
																				  this.label88,
																				  this.label89,
																				  this.label90,
																				  this.hScrollBar32,
																				  this.hScrollBar33,
																				  this.hScrollBar34,
																				  this.hScrollBar35,
																				  this.hScrollBar36,
																				  this.label91,
																				  this.label92,
																				  this.label93,
																				  this.label94,
																				  this.label95,
																				  this.label96,
																				  this.label97,
																				  this.label98,
																				  this.label99,
																				  this.label100,
																				  this.hScrollBar37,
																				  this.hScrollBar38,
																				  this.hScrollBar39,
																				  this.hScrollBar40,
																				  this.hScrollBar41,
																				  this.button7});
			this.tabcomb.Location = new System.Drawing.Point(4, 40);
			this.tabcomb.Name = "tabcomb";
			this.tabcomb.Size = new System.Drawing.Size(744, 444);
			this.tabcomb.TabIndex = 11;
			this.tabcomb.Text = "Comb Removal";
			this.tabcomb.Paint += new System.Windows.Forms.PaintEventHandler(this.tabcomb_Paint);
			// 
			// label79
			// 
			this.label79.Location = new System.Drawing.Point(480, 162);
			this.label79.Name = "label79";
			this.label79.Size = new System.Drawing.Size(64, 16);
			this.label79.TabIndex = 106;
			// 
			// label80
			// 
			this.label80.Location = new System.Drawing.Point(480, 122);
			this.label80.Name = "label80";
			this.label80.Size = new System.Drawing.Size(72, 16);
			this.label80.TabIndex = 105;
			// 
			// label81
			// 
			this.label81.Location = new System.Drawing.Point(480, 90);
			this.label81.Name = "label81";
			this.label81.Size = new System.Drawing.Size(64, 16);
			this.label81.TabIndex = 104;
			// 
			// label84
			// 
			this.label84.Location = new System.Drawing.Point(480, 50);
			this.label84.Name = "label84";
			this.label84.Size = new System.Drawing.Size(80, 16);
			this.label84.TabIndex = 103;
			// 
			// label85
			// 
			this.label85.Location = new System.Drawing.Point(480, 10);
			this.label85.Name = "label85";
			this.label85.Size = new System.Drawing.Size(72, 16);
			this.label85.TabIndex = 102;
			// 
			// label86
			// 
			this.label86.Location = new System.Drawing.Point(344, 162);
			this.label86.Name = "label86";
			this.label86.Size = new System.Drawing.Size(128, 16);
			this.label86.TabIndex = 101;
			this.label86.Text = "Vertical Line Thickness";
			// 
			// label87
			// 
			this.label87.Location = new System.Drawing.Point(344, 122);
			this.label87.Name = "label87";
			this.label87.Size = new System.Drawing.Size(136, 16);
			this.label87.TabIndex = 100;
			this.label87.Text = "Horizontal Line Thickness";
			// 
			// label88
			// 
			this.label88.Location = new System.Drawing.Point(344, 90);
			this.label88.Name = "label88";
			this.label88.Size = new System.Drawing.Size(96, 16);
			this.label88.TabIndex = 99;
			this.label88.Text = "Min Confidence";
			// 
			// label89
			// 
			this.label89.Location = new System.Drawing.Point(344, 50);
			this.label89.Name = "label89";
			this.label89.Size = new System.Drawing.Size(96, 16);
			this.label89.TabIndex = 98;
			this.label89.Text = "Min Comb Length";
			// 
			// label90
			// 
			this.label90.Location = new System.Drawing.Point(344, 10);
			this.label90.Name = "label90";
			this.label90.Size = new System.Drawing.Size(80, 16);
			this.label90.TabIndex = 97;
			this.label90.Text = "Comb Spacing";
			// 
			// hScrollBar32
			// 
			this.hScrollBar32.LargeChange = 1;
			this.hScrollBar32.Location = new System.Drawing.Point(344, 178);
			this.hScrollBar32.Maximum = 500;
			this.hScrollBar32.Minimum = 1;
			this.hScrollBar32.Name = "hScrollBar32";
			this.hScrollBar32.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar32.TabIndex = 96;
			this.hScrollBar32.Value = 4;
			this.hScrollBar32.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar32_Scroll);
			// 
			// hScrollBar33
			// 
			this.hScrollBar33.LargeChange = 1;
			this.hScrollBar33.Location = new System.Drawing.Point(344, 138);
			this.hScrollBar33.Maximum = 500;
			this.hScrollBar33.Minimum = 1;
			this.hScrollBar33.Name = "hScrollBar33";
			this.hScrollBar33.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar33.TabIndex = 95;
			this.hScrollBar33.Value = 4;
			this.hScrollBar33.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar33_Scroll);
			// 
			// hScrollBar34
			// 
			this.hScrollBar34.LargeChange = 1;
			this.hScrollBar34.Location = new System.Drawing.Point(344, 106);
			this.hScrollBar34.Minimum = 1;
			this.hScrollBar34.Name = "hScrollBar34";
			this.hScrollBar34.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar34.TabIndex = 94;
			this.hScrollBar34.Value = 50;
			this.hScrollBar34.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar34_Scroll);
			// 
			// hScrollBar35
			// 
			this.hScrollBar35.LargeChange = 1;
			this.hScrollBar35.Location = new System.Drawing.Point(344, 66);
			this.hScrollBar35.Maximum = 500;
			this.hScrollBar35.Minimum = 10;
			this.hScrollBar35.Name = "hScrollBar35";
			this.hScrollBar35.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar35.TabIndex = 93;
			this.hScrollBar35.Value = 50;
			this.hScrollBar35.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar35_Scroll);
			// 
			// hScrollBar36
			// 
			this.hScrollBar36.LargeChange = 1;
			this.hScrollBar36.Location = new System.Drawing.Point(344, 26);
			this.hScrollBar36.Maximum = 500;
			this.hScrollBar36.Minimum = 1;
			this.hScrollBar36.Name = "hScrollBar36";
			this.hScrollBar36.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar36.TabIndex = 92;
			this.hScrollBar36.Value = 25;
			this.hScrollBar36.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar36_Scroll);
			// 
			// label91
			// 
			this.label91.Location = new System.Drawing.Point(232, 162);
			this.label91.Name = "label91";
			this.label91.Size = new System.Drawing.Size(64, 16);
			this.label91.TabIndex = 91;
			// 
			// label92
			// 
			this.label92.Location = new System.Drawing.Point(232, 122);
			this.label92.Name = "label92";
			this.label92.Size = new System.Drawing.Size(72, 16);
			this.label92.TabIndex = 90;
			// 
			// label93
			// 
			this.label93.Location = new System.Drawing.Point(232, 90);
			this.label93.Name = "label93";
			this.label93.Size = new System.Drawing.Size(64, 16);
			this.label93.TabIndex = 89;
			// 
			// label94
			// 
			this.label94.Location = new System.Drawing.Point(232, 50);
			this.label94.Name = "label94";
			this.label94.Size = new System.Drawing.Size(80, 16);
			this.label94.TabIndex = 88;
			// 
			// label95
			// 
			this.label95.Location = new System.Drawing.Point(232, 10);
			this.label95.Name = "label95";
			this.label95.Size = new System.Drawing.Size(72, 16);
			this.label95.TabIndex = 87;
			// 
			// label96
			// 
			this.label96.Location = new System.Drawing.Point(96, 162);
			this.label96.Name = "label96";
			this.label96.Size = new System.Drawing.Size(116, 16);
			this.label96.TabIndex = 86;
			this.label96.Text = "Comb Height";
			// 
			// label97
			// 
			this.label97.Location = new System.Drawing.Point(96, 122);
			this.label97.Name = "label97";
			this.label97.Size = new System.Drawing.Size(88, 16);
			this.label97.TabIndex = 85;
			this.label97.Text = "Area Height";
			// 
			// label98
			// 
			this.label98.Location = new System.Drawing.Point(96, 90);
			this.label98.Name = "label98";
			this.label98.Size = new System.Drawing.Size(96, 16);
			this.label98.TabIndex = 84;
			this.label98.Text = "Area Width";
			// 
			// label99
			// 
			this.label99.Location = new System.Drawing.Point(96, 50);
			this.label99.Name = "label99";
			this.label99.Size = new System.Drawing.Size(64, 16);
			this.label99.TabIndex = 83;
			this.label99.Text = "Area Y";
			// 
			// label100
			// 
			this.label100.Location = new System.Drawing.Point(96, 10);
			this.label100.Name = "label100";
			this.label100.Size = new System.Drawing.Size(64, 16);
			this.label100.TabIndex = 82;
			this.label100.Text = "Area X";
			// 
			// hScrollBar37
			// 
			this.hScrollBar37.LargeChange = 1;
			this.hScrollBar37.Location = new System.Drawing.Point(96, 178);
			this.hScrollBar37.Maximum = 500;
			this.hScrollBar37.Minimum = 4;
			this.hScrollBar37.Name = "hScrollBar37";
			this.hScrollBar37.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar37.TabIndex = 81;
			this.hScrollBar37.Value = 20;
			this.hScrollBar37.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar37_Scroll);
			// 
			// hScrollBar38
			// 
			this.hScrollBar38.LargeChange = 1;
			this.hScrollBar38.Location = new System.Drawing.Point(96, 138);
			this.hScrollBar38.Maximum = 9999;
			this.hScrollBar38.Name = "hScrollBar38";
			this.hScrollBar38.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar38.TabIndex = 80;
			this.hScrollBar38.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar38_Scroll);
			// 
			// hScrollBar39
			// 
			this.hScrollBar39.LargeChange = 1;
			this.hScrollBar39.Location = new System.Drawing.Point(96, 106);
			this.hScrollBar39.Maximum = 9999;
			this.hScrollBar39.Name = "hScrollBar39";
			this.hScrollBar39.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar39.TabIndex = 79;
			this.hScrollBar39.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar39_Scroll);
			// 
			// hScrollBar40
			// 
			this.hScrollBar40.LargeChange = 1;
			this.hScrollBar40.Location = new System.Drawing.Point(96, 66);
			this.hScrollBar40.Maximum = 9999;
			this.hScrollBar40.Name = "hScrollBar40";
			this.hScrollBar40.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar40.TabIndex = 78;
			this.hScrollBar40.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar40_Scroll);
			// 
			// hScrollBar41
			// 
			this.hScrollBar41.LargeChange = 1;
			this.hScrollBar41.Location = new System.Drawing.Point(96, 26);
			this.hScrollBar41.Maximum = 9999;
			this.hScrollBar41.Name = "hScrollBar41";
			this.hScrollBar41.Size = new System.Drawing.Size(192, 16);
			this.hScrollBar41.TabIndex = 77;
			this.hScrollBar41.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar41_Scroll);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(584, 88);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(64, 40);
			this.button7.TabIndex = 76;
			this.button7.Text = "Process Image";
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// tabinverse
			// 
			this.tabinverse.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.cmdinverse,
																					 this.label101,
																					 this.label102,
																					 this.label103,
																					 this.label104,
																					 this.label105,
																					 this.label106,
																					 this.minblack,
																					 this.minheight,
																					 this.minwidth});
			this.tabinverse.Location = new System.Drawing.Point(4, 40);
			this.tabinverse.Name = "tabinverse";
			this.tabinverse.Size = new System.Drawing.Size(744, 444);
			this.tabinverse.TabIndex = 12;
			this.tabinverse.Text = "Inverse Text Correction";
			this.tabinverse.Paint += new System.Windows.Forms.PaintEventHandler(this.tabinverse_Paint);
			// 
			// cmdinverse
			// 
			this.cmdinverse.Location = new System.Drawing.Point(508, 94);
			this.cmdinverse.Name = "cmdinverse";
			this.cmdinverse.Size = new System.Drawing.Size(72, 40);
			this.cmdinverse.TabIndex = 19;
			this.cmdinverse.Text = "Process Image";
			this.cmdinverse.Click += new System.EventHandler(this.cmdinverse_Click);
			// 
			// label101
			// 
			this.label101.Location = new System.Drawing.Point(396, 142);
			this.label101.Name = "label101";
			this.label101.Size = new System.Drawing.Size(56, 16);
			this.label101.TabIndex = 18;
			this.label101.Text = "label101";
			// 
			// label102
			// 
			this.label102.Location = new System.Drawing.Point(396, 78);
			this.label102.Name = "label102";
			this.label102.Size = new System.Drawing.Size(56, 16);
			this.label102.TabIndex = 17;
			this.label102.Text = "label102";
			// 
			// label103
			// 
			this.label103.Location = new System.Drawing.Point(396, 14);
			this.label103.Name = "label103";
			this.label103.Size = new System.Drawing.Size(56, 16);
			this.label103.TabIndex = 16;
			this.label103.Text = "label103";
			// 
			// label104
			// 
			this.label104.Location = new System.Drawing.Point(172, 142);
			this.label104.Name = "label104";
			this.label104.Size = new System.Drawing.Size(200, 16);
			this.label104.TabIndex = 15;
			this.label104.Text = "Minimum Black On Edges:";
			// 
			// label105
			// 
			this.label105.Location = new System.Drawing.Point(164, 78);
			this.label105.Name = "label105";
			this.label105.Size = new System.Drawing.Size(192, 16);
			this.label105.TabIndex = 14;
			this.label105.Text = "Minimum Area Height:";
			// 
			// label106
			// 
			this.label106.Location = new System.Drawing.Point(164, 14);
			this.label106.Name = "label106";
			this.label106.Size = new System.Drawing.Size(184, 16);
			this.label106.TabIndex = 13;
			this.label106.Text = "Minimum Area Width:";
			// 
			// minblack
			// 
			this.minblack.LargeChange = 1;
			this.minblack.Location = new System.Drawing.Point(164, 174);
			this.minblack.Minimum = 1;
			this.minblack.Name = "minblack";
			this.minblack.Size = new System.Drawing.Size(312, 16);
			this.minblack.TabIndex = 12;
			this.minblack.Value = 1;
			this.minblack.Scroll += new System.Windows.Forms.ScrollEventHandler(this.minblack_Scroll);
			// 
			// minheight
			// 
			this.minheight.LargeChange = 1;
			this.minheight.Location = new System.Drawing.Point(164, 110);
			this.minheight.Maximum = 9999;
			this.minheight.Minimum = 1;
			this.minheight.Name = "minheight";
			this.minheight.Size = new System.Drawing.Size(312, 16);
			this.minheight.TabIndex = 11;
			this.minheight.Value = 1;
			this.minheight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.minheight_Scroll);
			// 
			// minwidth
			// 
			this.minwidth.LargeChange = 1;
			this.minwidth.Location = new System.Drawing.Point(164, 54);
			this.minwidth.Maximum = 9999;
			this.minwidth.Minimum = 1;
			this.minwidth.Name = "minwidth";
			this.minwidth.Size = new System.Drawing.Size(312, 16);
			this.minwidth.TabIndex = 10;
			this.minwidth.Value = 1;
			this.minwidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.minwidth_Scroll);
			// 
			// tabsmooth
			// 
			this.tabsmooth.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.label18,
																					this.label17,
																					this.cmdprocess,
																					this.hScrollBar9});
			this.tabsmooth.Location = new System.Drawing.Point(4, 40);
			this.tabsmooth.Name = "tabsmooth";
			this.tabsmooth.Size = new System.Drawing.Size(744, 444);
			this.tabsmooth.TabIndex = 4;
			this.tabsmooth.Text = "Smoothing";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(376, 48);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(80, 24);
			this.label18.TabIndex = 3;
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(192, 48);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(144, 24);
			this.label17.TabIndex = 2;
			this.label17.Text = "Smoothing Size:";
			// 
			// cmdprocess
			// 
			this.cmdprocess.Location = new System.Drawing.Point(280, 136);
			this.cmdprocess.Name = "cmdprocess";
			this.cmdprocess.Size = new System.Drawing.Size(104, 32);
			this.cmdprocess.TabIndex = 1;
			this.cmdprocess.Text = "Process";
			this.cmdprocess.Click += new System.EventHandler(this.cmdprocess_Click);
			// 
			// hScrollBar9
			// 
			this.hScrollBar9.LargeChange = 2;
			this.hScrollBar9.Location = new System.Drawing.Point(192, 96);
			this.hScrollBar9.Minimum = 1;
			this.hScrollBar9.Name = "hScrollBar9";
			this.hScrollBar9.Size = new System.Drawing.Size(280, 16);
			this.hScrollBar9.TabIndex = 0;
			this.hScrollBar9.Value = 1;
			this.hScrollBar9.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar9_Scroll);
			// 
			// taberode
			// 
			this.taberode.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.button8,
																				   this.label107,
																				   this.label108,
																				   this.hScrollBar42,
																				   this.CheckBox10,
																				   this.CheckBox11,
																				   this.CheckBox12,
																				   this.CheckBox13,
																				   this.CheckBox14,
																				   this.CheckBox15});
			this.taberode.Location = new System.Drawing.Point(4, 40);
			this.taberode.Name = "taberode";
			this.taberode.Size = new System.Drawing.Size(744, 444);
			this.taberode.TabIndex = 13;
			this.taberode.Text = "Erode";
			this.taberode.Paint += new System.Windows.Forms.PaintEventHandler(this.taberode_Paint);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(484, 86);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(64, 40);
			this.button8.TabIndex = 97;
			this.button8.Text = "Process Image";
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// label107
			// 
			this.label107.Location = new System.Drawing.Point(276, 62);
			this.label107.Name = "label107";
			this.label107.Size = new System.Drawing.Size(56, 16);
			this.label107.TabIndex = 96;
			this.label107.Text = "label107";
			// 
			// label108
			// 
			this.label108.Location = new System.Drawing.Point(196, 62);
			this.label108.Name = "label108";
			this.label108.Size = new System.Drawing.Size(56, 16);
			this.label108.TabIndex = 95;
			this.label108.Text = "Amount";
			// 
			// hScrollBar42
			// 
			this.hScrollBar42.Location = new System.Drawing.Point(196, 86);
			this.hScrollBar42.Minimum = 1;
			this.hScrollBar42.Name = "hScrollBar42";
			this.hScrollBar42.Size = new System.Drawing.Size(200, 16);
			this.hScrollBar42.TabIndex = 94;
			this.hScrollBar42.Value = 1;
			this.hScrollBar42.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar42_Scroll);
			// 
			// CheckBox10
			// 
			this.CheckBox10.Enabled = false;
			this.CheckBox10.Location = new System.Drawing.Point(340, 158);
			this.CheckBox10.Name = "CheckBox10";
			this.CheckBox10.Size = new System.Drawing.Size(56, 16);
			this.CheckBox10.TabIndex = 93;
			this.CheckBox10.Text = "Right";
			this.CheckBox10.CheckedChanged += new System.EventHandler(this.CheckBox10_CheckedChanged);
			// 
			// CheckBox11
			// 
			this.CheckBox11.Enabled = false;
			this.CheckBox11.Location = new System.Drawing.Point(340, 126);
			this.CheckBox11.Name = "CheckBox11";
			this.CheckBox11.Size = new System.Drawing.Size(56, 16);
			this.CheckBox11.TabIndex = 92;
			this.CheckBox11.Text = "Down";
			this.CheckBox11.CheckedChanged += new System.EventHandler(this.CheckBox11_CheckedChanged);
			// 
			// CheckBox12
			// 
			this.CheckBox12.Enabled = false;
			this.CheckBox12.Location = new System.Drawing.Point(268, 158);
			this.CheckBox12.Name = "CheckBox12";
			this.CheckBox12.Size = new System.Drawing.Size(48, 16);
			this.CheckBox12.TabIndex = 91;
			this.CheckBox12.Text = "Left";
			this.CheckBox12.CheckedChanged += new System.EventHandler(this.CheckBox12_CheckedChanged);
			// 
			// CheckBox13
			// 
			this.CheckBox13.Enabled = false;
			this.CheckBox13.Location = new System.Drawing.Point(268, 126);
			this.CheckBox13.Name = "CheckBox13";
			this.CheckBox13.Size = new System.Drawing.Size(40, 16);
			this.CheckBox13.TabIndex = 90;
			this.CheckBox13.Text = "Up";
			this.CheckBox13.CheckedChanged += new System.EventHandler(this.CheckBox13_CheckedChanged);
			// 
			// CheckBox14
			// 
			this.CheckBox14.Checked = true;
			this.CheckBox14.CheckState = System.Windows.Forms.CheckState.Checked;
			this.CheckBox14.Location = new System.Drawing.Point(196, 142);
			this.CheckBox14.Name = "CheckBox14";
			this.CheckBox14.Size = new System.Drawing.Size(48, 16);
			this.CheckBox14.TabIndex = 89;
			this.CheckBox14.Text = "All";
			this.CheckBox14.CheckedChanged += new System.EventHandler(this.CheckBox14_CheckedChanged);
			// 
			// CheckBox15
			// 
			this.CheckBox15.Enabled = false;
			this.CheckBox15.Location = new System.Drawing.Point(196, 30);
			this.CheckBox15.Name = "CheckBox15";
			this.CheckBox15.Size = new System.Drawing.Size(136, 24);
			this.CheckBox15.TabIndex = 88;
			this.CheckBox15.Text = "Diagonal Only";
			// 
			// RegisterTab
			// 
			this.RegisterTab.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.VerticalToLineMinimumLineLengthLabel2,
																					  this.VerticalToLineMinimumLineLengthLabel,
																					  this.VerticalToLineMinimumLineLength,
																					  this.VerticalToLineMaximumLineThicknessLabel2,
																					  this.VerticalToLineMaximumLineThicknessLabel,
																					  this.VerticalToLineMaximumLineThickness,
																					  this.VerticalToLineMaximumLineGapLabel2,
																					  this.VerticalToLineMaximumLineGapLabel,
																					  this.VerticalToLineMaximumLineGap,
																					  this.VerticalSkipDistanceLabel2,
																					  this.VerticalSkipDistanceLabel,
																					  this.VerticalSkipDistance,
																					  this.VerticalResultantMarginLabel2,
																					  this.VerticalResultantMarginLabel,
																					  this.VerticalResultantMargin,
																					  this.VerticalMinimumForegroundLabel2,
																					  this.VerticalMinimumForegroundLabel,
																					  this.VerticalMinimumForeground,
																					  this.VerticalMinimumBackgroundLabel2,
																					  this.VerticalMinimumBackgroundLabel,
																					  this.VerticalMinimumBackground,
																					  this.VerticalMinimumActivityLabel2,
																					  this.VerticalMinimumActivityLabel,
																					  this.VerticalMinimumActivity,
																					  this.VerticalCentralFocusCheckBox,
																					  this.VerticalAddOnlyCheckBox,
																					  this.VerticalActiveCheckBox,
																					  this.HorizontalToLineMinimumLineLengthLabel2,
																					  this.HorizontalToLineMinimumLineLengthLabel,
																					  this.HorizontalToLineMinimumLineLength,
																					  this.HorizontalToLineMaximumLineThicknessLabel2,
																					  this.HorizontalToLineMaximumLineThicknessLabel,
																					  this.HorizontalToLineMaximumLineThickness,
																					  this.HorizontalToLineMaximumLineGapLabel2,
																					  this.HorizontalToLineMaximumLineGapLabel,
																					  this.HorizontalToLineMaximumLineGap,
																					  this.HorizontalToLineCheckThinLinesActiveCheckBox,
																					  this.HorizontalToLineActiveCheckBox,
																					  this.HorizontalSkipDistanceLabel2,
																					  this.HorizontalSkipDistanceLabel,
																					  this.HorizontalSkipDistance,
																					  this.HorizontalSkipActiveCheckBox,
																					  this.HorizontalResultantMarginLabel2,
																					  this.HorizontalResultantMarginLabel,
																					  this.HorizontalResultantMargin,
																					  this.HorizontalMinimumForegroundLabel2,
																					  this.HorizontalMinimumForegroundLabel,
																					  this.HorizontalMinimumForeground,
																					  this.HorizontalMinimumBackgroundLabel2,
																					  this.HorizontalMinimumBackgroundLabel,
																					  this.HorizontalMinimumBackground,
																					  this.HorizontalMinimumActivityLabel2,
																					  this.HorizontalMinimumActivityLabel,
																					  this.HorizontalMinimumActivity,
																					  this.HorizontalIgnoreHolesCheckBox,
																					  this.HorizontalCentralFocusCheckBox,
																					  this.HorizontalAddOnlyCheckBox,
																					  this.RegisterButton,
																					  this.HorizontalActiveCheckBox,
																					  this.VerticalLinesGroupBox,
																					  this.HorizontalLinesGroupBox});
			this.RegisterTab.Location = new System.Drawing.Point(4, 40);
			this.RegisterTab.Name = "RegisterTab";
			this.RegisterTab.Size = new System.Drawing.Size(744, 444);
			this.RegisterTab.TabIndex = 14;
			this.RegisterTab.Text = "RegisterImage";
			this.RegisterTab.Paint += new System.Windows.Forms.PaintEventHandler(this.RegisterTab_Paint);
			// 
			// VerticalToLineMinimumLineLengthLabel2
			// 
			this.VerticalToLineMinimumLineLengthLabel2.Location = new System.Drawing.Point(528, 392);
			this.VerticalToLineMinimumLineLengthLabel2.Name = "VerticalToLineMinimumLineLengthLabel2";
			this.VerticalToLineMinimumLineLengthLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalToLineMinimumLineLengthLabel2.TabIndex = 153;
			this.VerticalToLineMinimumLineLengthLabel2.Text = "300";
			// 
			// VerticalToLineMinimumLineLengthLabel
			// 
			this.VerticalToLineMinimumLineLengthLabel.Location = new System.Drawing.Point(376, 392);
			this.VerticalToLineMinimumLineLengthLabel.Name = "VerticalToLineMinimumLineLengthLabel";
			this.VerticalToLineMinimumLineLengthLabel.Size = new System.Drawing.Size(144, 16);
			this.VerticalToLineMinimumLineLengthLabel.TabIndex = 152;
			this.VerticalToLineMinimumLineLengthLabel.Text = "Minimum Line Length:";
			// 
			// VerticalToLineMinimumLineLength
			// 
			this.VerticalToLineMinimumLineLength.LargeChange = 1;
			this.VerticalToLineMinimumLineLength.Location = new System.Drawing.Point(376, 408);
			this.VerticalToLineMinimumLineLength.Maximum = 20000;
			this.VerticalToLineMinimumLineLength.Name = "VerticalToLineMinimumLineLength";
			this.VerticalToLineMinimumLineLength.Size = new System.Drawing.Size(192, 16);
			this.VerticalToLineMinimumLineLength.TabIndex = 151;
			this.VerticalToLineMinimumLineLength.Value = 300;
			this.VerticalToLineMinimumLineLength.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalToLineMinimumLineLength_Scroll);
			// 
			// VerticalToLineMaximumLineThicknessLabel2
			// 
			this.VerticalToLineMaximumLineThicknessLabel2.Location = new System.Drawing.Point(528, 352);
			this.VerticalToLineMaximumLineThicknessLabel2.Name = "VerticalToLineMaximumLineThicknessLabel2";
			this.VerticalToLineMaximumLineThicknessLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalToLineMaximumLineThicknessLabel2.TabIndex = 150;
			this.VerticalToLineMaximumLineThicknessLabel2.Text = "22";
			// 
			// VerticalToLineMaximumLineThicknessLabel
			// 
			this.VerticalToLineMaximumLineThicknessLabel.Location = new System.Drawing.Point(376, 352);
			this.VerticalToLineMaximumLineThicknessLabel.Name = "VerticalToLineMaximumLineThicknessLabel";
			this.VerticalToLineMaximumLineThicknessLabel.Size = new System.Drawing.Size(144, 16);
			this.VerticalToLineMaximumLineThicknessLabel.TabIndex = 149;
			this.VerticalToLineMaximumLineThicknessLabel.Text = "Maximum Line Thickness:";
			// 
			// VerticalToLineMaximumLineThickness
			// 
			this.VerticalToLineMaximumLineThickness.LargeChange = 1;
			this.VerticalToLineMaximumLineThickness.Location = new System.Drawing.Point(376, 368);
			this.VerticalToLineMaximumLineThickness.Name = "VerticalToLineMaximumLineThickness";
			this.VerticalToLineMaximumLineThickness.Size = new System.Drawing.Size(192, 16);
			this.VerticalToLineMaximumLineThickness.TabIndex = 148;
			this.VerticalToLineMaximumLineThickness.Value = 22;
			this.VerticalToLineMaximumLineThickness.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalToLineMaximumLineThickness_Scroll);
			// 
			// VerticalToLineMaximumLineGapLabel2
			// 
			this.VerticalToLineMaximumLineGapLabel2.Location = new System.Drawing.Point(528, 312);
			this.VerticalToLineMaximumLineGapLabel2.Name = "VerticalToLineMaximumLineGapLabel2";
			this.VerticalToLineMaximumLineGapLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalToLineMaximumLineGapLabel2.TabIndex = 147;
			this.VerticalToLineMaximumLineGapLabel2.Text = "2";
			// 
			// VerticalToLineMaximumLineGapLabel
			// 
			this.VerticalToLineMaximumLineGapLabel.Location = new System.Drawing.Point(376, 312);
			this.VerticalToLineMaximumLineGapLabel.Name = "VerticalToLineMaximumLineGapLabel";
			this.VerticalToLineMaximumLineGapLabel.Size = new System.Drawing.Size(144, 16);
			this.VerticalToLineMaximumLineGapLabel.TabIndex = 146;
			this.VerticalToLineMaximumLineGapLabel.Text = "Maximum Line Gap:";
			// 
			// VerticalToLineMaximumLineGap
			// 
			this.VerticalToLineMaximumLineGap.LargeChange = 1;
			this.VerticalToLineMaximumLineGap.Location = new System.Drawing.Point(376, 328);
			this.VerticalToLineMaximumLineGap.Maximum = 50;
			this.VerticalToLineMaximumLineGap.Name = "VerticalToLineMaximumLineGap";
			this.VerticalToLineMaximumLineGap.Size = new System.Drawing.Size(192, 16);
			this.VerticalToLineMaximumLineGap.TabIndex = 145;
			this.VerticalToLineMaximumLineGap.Value = 2;
			this.VerticalToLineMaximumLineGap.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalToLineMaximumLineGap_Scroll);
			// 
			// VerticalSkipDistanceLabel2
			// 
			this.VerticalSkipDistanceLabel2.Location = new System.Drawing.Point(528, 272);
			this.VerticalSkipDistanceLabel2.Name = "VerticalSkipDistanceLabel2";
			this.VerticalSkipDistanceLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalSkipDistanceLabel2.TabIndex = 142;
			this.VerticalSkipDistanceLabel2.Text = "300";
			// 
			// VerticalSkipDistanceLabel
			// 
			this.VerticalSkipDistanceLabel.Location = new System.Drawing.Point(376, 272);
			this.VerticalSkipDistanceLabel.Name = "VerticalSkipDistanceLabel";
			this.VerticalSkipDistanceLabel.Size = new System.Drawing.Size(144, 16);
			this.VerticalSkipDistanceLabel.TabIndex = 141;
			this.VerticalSkipDistanceLabel.Text = "Skip Distance:";
			// 
			// VerticalSkipDistance
			// 
			this.VerticalSkipDistance.LargeChange = 1;
			this.VerticalSkipDistance.Location = new System.Drawing.Point(376, 288);
			this.VerticalSkipDistance.Maximum = 20000;
			this.VerticalSkipDistance.Name = "VerticalSkipDistance";
			this.VerticalSkipDistance.Size = new System.Drawing.Size(192, 16);
			this.VerticalSkipDistance.TabIndex = 140;
			this.VerticalSkipDistance.Value = 300;
			this.VerticalSkipDistance.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalSkipDistance_Scroll);
			// 
			// VerticalResultantMarginLabel2
			// 
			this.VerticalResultantMarginLabel2.Location = new System.Drawing.Point(320, 392);
			this.VerticalResultantMarginLabel2.Name = "VerticalResultantMarginLabel2";
			this.VerticalResultantMarginLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalResultantMarginLabel2.TabIndex = 138;
			this.VerticalResultantMarginLabel2.Text = "150";
			// 
			// VerticalResultantMarginLabel
			// 
			this.VerticalResultantMarginLabel.Location = new System.Drawing.Point(168, 392);
			this.VerticalResultantMarginLabel.Name = "VerticalResultantMarginLabel";
			this.VerticalResultantMarginLabel.Size = new System.Drawing.Size(144, 16);
			this.VerticalResultantMarginLabel.TabIndex = 137;
			this.VerticalResultantMarginLabel.Text = "Resultant Margin:";
			// 
			// VerticalResultantMargin
			// 
			this.VerticalResultantMargin.LargeChange = 1;
			this.VerticalResultantMargin.Location = new System.Drawing.Point(168, 408);
			this.VerticalResultantMargin.Maximum = 20000;
			this.VerticalResultantMargin.Minimum = -20000;
			this.VerticalResultantMargin.Name = "VerticalResultantMargin";
			this.VerticalResultantMargin.Size = new System.Drawing.Size(192, 16);
			this.VerticalResultantMargin.TabIndex = 136;
			this.VerticalResultantMargin.Value = 150;
			this.VerticalResultantMargin.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalResultantMargin_Scroll);
			// 
			// VerticalMinimumForegroundLabel2
			// 
			this.VerticalMinimumForegroundLabel2.Location = new System.Drawing.Point(320, 352);
			this.VerticalMinimumForegroundLabel2.Name = "VerticalMinimumForegroundLabel2";
			this.VerticalMinimumForegroundLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalMinimumForegroundLabel2.TabIndex = 135;
			this.VerticalMinimumForegroundLabel2.Text = "5";
			// 
			// VerticalMinimumForegroundLabel
			// 
			this.VerticalMinimumForegroundLabel.Location = new System.Drawing.Point(168, 352);
			this.VerticalMinimumForegroundLabel.Name = "VerticalMinimumForegroundLabel";
			this.VerticalMinimumForegroundLabel.Size = new System.Drawing.Size(136, 16);
			this.VerticalMinimumForegroundLabel.TabIndex = 134;
			this.VerticalMinimumForegroundLabel.Text = "Minimum Foreground:";
			// 
			// VerticalMinimumForeground
			// 
			this.VerticalMinimumForeground.LargeChange = 1;
			this.VerticalMinimumForeground.Location = new System.Drawing.Point(168, 368);
			this.VerticalMinimumForeground.Maximum = 1000;
			this.VerticalMinimumForeground.Minimum = 1;
			this.VerticalMinimumForeground.Name = "VerticalMinimumForeground";
			this.VerticalMinimumForeground.Size = new System.Drawing.Size(192, 16);
			this.VerticalMinimumForeground.TabIndex = 133;
			this.VerticalMinimumForeground.Value = 5;
			this.VerticalMinimumForeground.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalMinimumForeground_Scroll);
			// 
			// VerticalMinimumBackgroundLabel2
			// 
			this.VerticalMinimumBackgroundLabel2.Location = new System.Drawing.Point(320, 312);
			this.VerticalMinimumBackgroundLabel2.Name = "VerticalMinimumBackgroundLabel2";
			this.VerticalMinimumBackgroundLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalMinimumBackgroundLabel2.TabIndex = 132;
			this.VerticalMinimumBackgroundLabel2.Text = "5";
			// 
			// VerticalMinimumBackgroundLabel
			// 
			this.VerticalMinimumBackgroundLabel.Location = new System.Drawing.Point(168, 312);
			this.VerticalMinimumBackgroundLabel.Name = "VerticalMinimumBackgroundLabel";
			this.VerticalMinimumBackgroundLabel.Size = new System.Drawing.Size(136, 16);
			this.VerticalMinimumBackgroundLabel.TabIndex = 131;
			this.VerticalMinimumBackgroundLabel.Text = "Minimum Background:";
			// 
			// VerticalMinimumBackground
			// 
			this.VerticalMinimumBackground.LargeChange = 1;
			this.VerticalMinimumBackground.Location = new System.Drawing.Point(168, 328);
			this.VerticalMinimumBackground.Maximum = 1000;
			this.VerticalMinimumBackground.Minimum = 2;
			this.VerticalMinimumBackground.Name = "VerticalMinimumBackground";
			this.VerticalMinimumBackground.Size = new System.Drawing.Size(192, 16);
			this.VerticalMinimumBackground.TabIndex = 130;
			this.VerticalMinimumBackground.Value = 5;
			this.VerticalMinimumBackground.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalMinimumBackground_Scroll);
			// 
			// VerticalMinimumActivityLabel2
			// 
			this.VerticalMinimumActivityLabel2.Location = new System.Drawing.Point(312, 272);
			this.VerticalMinimumActivityLabel2.Name = "VerticalMinimumActivityLabel2";
			this.VerticalMinimumActivityLabel2.Size = new System.Drawing.Size(40, 16);
			this.VerticalMinimumActivityLabel2.TabIndex = 129;
			this.VerticalMinimumActivityLabel2.Text = "3";
			// 
			// VerticalMinimumActivityLabel
			// 
			this.VerticalMinimumActivityLabel.Location = new System.Drawing.Point(168, 272);
			this.VerticalMinimumActivityLabel.Name = "VerticalMinimumActivityLabel";
			this.VerticalMinimumActivityLabel.Size = new System.Drawing.Size(104, 16);
			this.VerticalMinimumActivityLabel.TabIndex = 128;
			this.VerticalMinimumActivityLabel.Text = "Minimum Activity:";
			// 
			// VerticalMinimumActivity
			// 
			this.VerticalMinimumActivity.LargeChange = 1;
			this.VerticalMinimumActivity.Location = new System.Drawing.Point(168, 288);
			this.VerticalMinimumActivity.Maximum = 1000;
			this.VerticalMinimumActivity.Minimum = 1;
			this.VerticalMinimumActivity.Name = "VerticalMinimumActivity";
			this.VerticalMinimumActivity.Size = new System.Drawing.Size(192, 16);
			this.VerticalMinimumActivity.TabIndex = 127;
			this.VerticalMinimumActivity.Value = 3;
			this.VerticalMinimumActivity.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VerticalMinimumActivity_Scroll);
			// 
			// VerticalCentralFocusCheckBox
			// 
			this.VerticalCentralFocusCheckBox.Location = new System.Drawing.Point(16, 312);
			this.VerticalCentralFocusCheckBox.Name = "VerticalCentralFocusCheckBox";
			this.VerticalCentralFocusCheckBox.Size = new System.Drawing.Size(120, 24);
			this.VerticalCentralFocusCheckBox.TabIndex = 125;
			this.VerticalCentralFocusCheckBox.Text = "Focus on Center";
			// 
			// VerticalAddOnlyCheckBox
			// 
			this.VerticalAddOnlyCheckBox.Location = new System.Drawing.Point(16, 280);
			this.VerticalAddOnlyCheckBox.Name = "VerticalAddOnlyCheckBox";
			this.VerticalAddOnlyCheckBox.Size = new System.Drawing.Size(120, 24);
			this.VerticalAddOnlyCheckBox.TabIndex = 124;
			this.VerticalAddOnlyCheckBox.Text = "Only Add Pixels";
			// 
			// VerticalActiveCheckBox
			// 
			this.VerticalActiveCheckBox.Checked = true;
			this.VerticalActiveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.VerticalActiveCheckBox.Location = new System.Drawing.Point(16, 248);
			this.VerticalActiveCheckBox.Name = "VerticalActiveCheckBox";
			this.VerticalActiveCheckBox.Size = new System.Drawing.Size(88, 24);
			this.VerticalActiveCheckBox.TabIndex = 122;
			this.VerticalActiveCheckBox.Text = "Edge Active";
			// 
			// HorizontalToLineMinimumLineLengthLabel2
			// 
			this.HorizontalToLineMinimumLineLengthLabel2.Location = new System.Drawing.Point(528, 168);
			this.HorizontalToLineMinimumLineLengthLabel2.Name = "HorizontalToLineMinimumLineLengthLabel2";
			this.HorizontalToLineMinimumLineLengthLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalToLineMinimumLineLengthLabel2.TabIndex = 121;
			this.HorizontalToLineMinimumLineLengthLabel2.Text = "300";
			// 
			// HorizontalToLineMinimumLineLengthLabel
			// 
			this.HorizontalToLineMinimumLineLengthLabel.Location = new System.Drawing.Point(376, 168);
			this.HorizontalToLineMinimumLineLengthLabel.Name = "HorizontalToLineMinimumLineLengthLabel";
			this.HorizontalToLineMinimumLineLengthLabel.Size = new System.Drawing.Size(144, 16);
			this.HorizontalToLineMinimumLineLengthLabel.TabIndex = 120;
			this.HorizontalToLineMinimumLineLengthLabel.Text = "Minimum Line Length:";
			// 
			// HorizontalToLineMinimumLineLength
			// 
			this.HorizontalToLineMinimumLineLength.LargeChange = 1;
			this.HorizontalToLineMinimumLineLength.Location = new System.Drawing.Point(376, 184);
			this.HorizontalToLineMinimumLineLength.Maximum = 20000;
			this.HorizontalToLineMinimumLineLength.Name = "HorizontalToLineMinimumLineLength";
			this.HorizontalToLineMinimumLineLength.Size = new System.Drawing.Size(192, 16);
			this.HorizontalToLineMinimumLineLength.TabIndex = 119;
			this.HorizontalToLineMinimumLineLength.Value = 300;
			this.HorizontalToLineMinimumLineLength.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalToLineMinimumLineLength_Scroll);
			// 
			// HorizontalToLineMaximumLineThicknessLabel2
			// 
			this.HorizontalToLineMaximumLineThicknessLabel2.Location = new System.Drawing.Point(528, 128);
			this.HorizontalToLineMaximumLineThicknessLabel2.Name = "HorizontalToLineMaximumLineThicknessLabel2";
			this.HorizontalToLineMaximumLineThicknessLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalToLineMaximumLineThicknessLabel2.TabIndex = 118;
			this.HorizontalToLineMaximumLineThicknessLabel2.Text = "22";
			// 
			// HorizontalToLineMaximumLineThicknessLabel
			// 
			this.HorizontalToLineMaximumLineThicknessLabel.Location = new System.Drawing.Point(376, 128);
			this.HorizontalToLineMaximumLineThicknessLabel.Name = "HorizontalToLineMaximumLineThicknessLabel";
			this.HorizontalToLineMaximumLineThicknessLabel.Size = new System.Drawing.Size(144, 16);
			this.HorizontalToLineMaximumLineThicknessLabel.TabIndex = 117;
			this.HorizontalToLineMaximumLineThicknessLabel.Text = "Maximum Line Thickness:";
			// 
			// HorizontalToLineMaximumLineThickness
			// 
			this.HorizontalToLineMaximumLineThickness.LargeChange = 1;
			this.HorizontalToLineMaximumLineThickness.Location = new System.Drawing.Point(376, 144);
			this.HorizontalToLineMaximumLineThickness.Name = "HorizontalToLineMaximumLineThickness";
			this.HorizontalToLineMaximumLineThickness.Size = new System.Drawing.Size(192, 16);
			this.HorizontalToLineMaximumLineThickness.TabIndex = 116;
			this.HorizontalToLineMaximumLineThickness.Value = 22;
			this.HorizontalToLineMaximumLineThickness.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalToLineMaximumLineThickness_Scroll);
			// 
			// HorizontalToLineMaximumLineGapLabel2
			// 
			this.HorizontalToLineMaximumLineGapLabel2.Location = new System.Drawing.Point(528, 88);
			this.HorizontalToLineMaximumLineGapLabel2.Name = "HorizontalToLineMaximumLineGapLabel2";
			this.HorizontalToLineMaximumLineGapLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalToLineMaximumLineGapLabel2.TabIndex = 115;
			this.HorizontalToLineMaximumLineGapLabel2.Text = "2";
			// 
			// HorizontalToLineMaximumLineGapLabel
			// 
			this.HorizontalToLineMaximumLineGapLabel.Location = new System.Drawing.Point(376, 88);
			this.HorizontalToLineMaximumLineGapLabel.Name = "HorizontalToLineMaximumLineGapLabel";
			this.HorizontalToLineMaximumLineGapLabel.Size = new System.Drawing.Size(144, 16);
			this.HorizontalToLineMaximumLineGapLabel.TabIndex = 114;
			this.HorizontalToLineMaximumLineGapLabel.Text = "Maximum Line Gap:";
			// 
			// HorizontalToLineMaximumLineGap
			// 
			this.HorizontalToLineMaximumLineGap.LargeChange = 1;
			this.HorizontalToLineMaximumLineGap.Location = new System.Drawing.Point(376, 104);
			this.HorizontalToLineMaximumLineGap.Maximum = 50;
			this.HorizontalToLineMaximumLineGap.Name = "HorizontalToLineMaximumLineGap";
			this.HorizontalToLineMaximumLineGap.Size = new System.Drawing.Size(192, 16);
			this.HorizontalToLineMaximumLineGap.TabIndex = 113;
			this.HorizontalToLineMaximumLineGap.Value = 2;
			this.HorizontalToLineMaximumLineGap.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalToLineMaximumLineGap_Scroll);
			// 
			// HorizontalToLineCheckThinLinesActiveCheckBox
			// 
			this.HorizontalToLineCheckThinLinesActiveCheckBox.Location = new System.Drawing.Point(168, 16);
			this.HorizontalToLineCheckThinLinesActiveCheckBox.Name = "HorizontalToLineCheckThinLinesActiveCheckBox";
			this.HorizontalToLineCheckThinLinesActiveCheckBox.Size = new System.Drawing.Size(88, 24);
			this.HorizontalToLineCheckThinLinesActiveCheckBox.TabIndex = 112;
			this.HorizontalToLineCheckThinLinesActiveCheckBox.Text = "Thin Lines";
			// 
			// HorizontalToLineActiveCheckBox
			// 
			this.HorizontalToLineActiveCheckBox.Location = new System.Drawing.Point(16, 176);
			this.HorizontalToLineActiveCheckBox.Name = "HorizontalToLineActiveCheckBox";
			this.HorizontalToLineActiveCheckBox.Size = new System.Drawing.Size(144, 24);
			this.HorizontalToLineActiveCheckBox.TabIndex = 111;
			this.HorizontalToLineActiveCheckBox.Text = "Position Based on Line";
			// 
			// HorizontalSkipDistanceLabel2
			// 
			this.HorizontalSkipDistanceLabel2.Location = new System.Drawing.Point(528, 48);
			this.HorizontalSkipDistanceLabel2.Name = "HorizontalSkipDistanceLabel2";
			this.HorizontalSkipDistanceLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalSkipDistanceLabel2.TabIndex = 110;
			this.HorizontalSkipDistanceLabel2.Text = "300";
			// 
			// HorizontalSkipDistanceLabel
			// 
			this.HorizontalSkipDistanceLabel.Location = new System.Drawing.Point(376, 48);
			this.HorizontalSkipDistanceLabel.Name = "HorizontalSkipDistanceLabel";
			this.HorizontalSkipDistanceLabel.Size = new System.Drawing.Size(144, 16);
			this.HorizontalSkipDistanceLabel.TabIndex = 109;
			this.HorizontalSkipDistanceLabel.Text = "Skip Distance:";
			// 
			// HorizontalSkipDistance
			// 
			this.HorizontalSkipDistance.LargeChange = 1;
			this.HorizontalSkipDistance.Location = new System.Drawing.Point(376, 64);
			this.HorizontalSkipDistance.Maximum = 20000;
			this.HorizontalSkipDistance.Name = "HorizontalSkipDistance";
			this.HorizontalSkipDistance.Size = new System.Drawing.Size(192, 16);
			this.HorizontalSkipDistance.TabIndex = 108;
			this.HorizontalSkipDistance.Value = 300;
			this.HorizontalSkipDistance.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalSkipDistance_Scroll);
			// 
			// HorizontalSkipActiveCheckBox
			// 
			this.HorizontalSkipActiveCheckBox.Location = new System.Drawing.Point(16, 144);
			this.HorizontalSkipActiveCheckBox.Name = "HorizontalSkipActiveCheckBox";
			this.HorizontalSkipActiveCheckBox.Size = new System.Drawing.Size(120, 24);
			this.HorizontalSkipActiveCheckBox.TabIndex = 107;
			this.HorizontalSkipActiveCheckBox.Text = "Skip Lines";
			// 
			// HorizontalResultantMarginLabel2
			// 
			this.HorizontalResultantMarginLabel2.Location = new System.Drawing.Point(320, 168);
			this.HorizontalResultantMarginLabel2.Name = "HorizontalResultantMarginLabel2";
			this.HorizontalResultantMarginLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalResultantMarginLabel2.TabIndex = 106;
			this.HorizontalResultantMarginLabel2.Text = "150";
			// 
			// HorizontalResultantMarginLabel
			// 
			this.HorizontalResultantMarginLabel.Location = new System.Drawing.Point(168, 168);
			this.HorizontalResultantMarginLabel.Name = "HorizontalResultantMarginLabel";
			this.HorizontalResultantMarginLabel.Size = new System.Drawing.Size(144, 16);
			this.HorizontalResultantMarginLabel.TabIndex = 105;
			this.HorizontalResultantMarginLabel.Text = "Resultant Margin:";
			// 
			// HorizontalResultantMargin
			// 
			this.HorizontalResultantMargin.LargeChange = 1;
			this.HorizontalResultantMargin.Location = new System.Drawing.Point(168, 184);
			this.HorizontalResultantMargin.Maximum = 20000;
			this.HorizontalResultantMargin.Minimum = -20000;
			this.HorizontalResultantMargin.Name = "HorizontalResultantMargin";
			this.HorizontalResultantMargin.Size = new System.Drawing.Size(192, 16);
			this.HorizontalResultantMargin.TabIndex = 104;
			this.HorizontalResultantMargin.Value = 150;
			this.HorizontalResultantMargin.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalResultantMargin_Scroll);
			// 
			// HorizontalMinimumForegroundLabel2
			// 
			this.HorizontalMinimumForegroundLabel2.Location = new System.Drawing.Point(320, 128);
			this.HorizontalMinimumForegroundLabel2.Name = "HorizontalMinimumForegroundLabel2";
			this.HorizontalMinimumForegroundLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalMinimumForegroundLabel2.TabIndex = 103;
			this.HorizontalMinimumForegroundLabel2.Text = "5";
			// 
			// HorizontalMinimumForegroundLabel
			// 
			this.HorizontalMinimumForegroundLabel.Location = new System.Drawing.Point(168, 128);
			this.HorizontalMinimumForegroundLabel.Name = "HorizontalMinimumForegroundLabel";
			this.HorizontalMinimumForegroundLabel.Size = new System.Drawing.Size(136, 16);
			this.HorizontalMinimumForegroundLabel.TabIndex = 102;
			this.HorizontalMinimumForegroundLabel.Text = "Minimum Foreground:";
			// 
			// HorizontalMinimumForeground
			// 
			this.HorizontalMinimumForeground.LargeChange = 1;
			this.HorizontalMinimumForeground.Location = new System.Drawing.Point(168, 144);
			this.HorizontalMinimumForeground.Maximum = 1000;
			this.HorizontalMinimumForeground.Minimum = 1;
			this.HorizontalMinimumForeground.Name = "HorizontalMinimumForeground";
			this.HorizontalMinimumForeground.Size = new System.Drawing.Size(192, 16);
			this.HorizontalMinimumForeground.TabIndex = 101;
			this.HorizontalMinimumForeground.Value = 5;
			this.HorizontalMinimumForeground.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalMinimumForeground_Scroll);
			// 
			// HorizontalMinimumBackgroundLabel2
			// 
			this.HorizontalMinimumBackgroundLabel2.Location = new System.Drawing.Point(320, 88);
			this.HorizontalMinimumBackgroundLabel2.Name = "HorizontalMinimumBackgroundLabel2";
			this.HorizontalMinimumBackgroundLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalMinimumBackgroundLabel2.TabIndex = 100;
			this.HorizontalMinimumBackgroundLabel2.Text = "5";
			// 
			// HorizontalMinimumBackgroundLabel
			// 
			this.HorizontalMinimumBackgroundLabel.Location = new System.Drawing.Point(168, 88);
			this.HorizontalMinimumBackgroundLabel.Name = "HorizontalMinimumBackgroundLabel";
			this.HorizontalMinimumBackgroundLabel.Size = new System.Drawing.Size(136, 16);
			this.HorizontalMinimumBackgroundLabel.TabIndex = 99;
			this.HorizontalMinimumBackgroundLabel.Text = "Minimum Background:";
			// 
			// HorizontalMinimumBackground
			// 
			this.HorizontalMinimumBackground.LargeChange = 1;
			this.HorizontalMinimumBackground.Location = new System.Drawing.Point(168, 104);
			this.HorizontalMinimumBackground.Maximum = 1000;
			this.HorizontalMinimumBackground.Minimum = 2;
			this.HorizontalMinimumBackground.Name = "HorizontalMinimumBackground";
			this.HorizontalMinimumBackground.Size = new System.Drawing.Size(192, 16);
			this.HorizontalMinimumBackground.TabIndex = 98;
			this.HorizontalMinimumBackground.Value = 5;
			this.HorizontalMinimumBackground.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalMinimumBackground_Scroll);
			// 
			// HorizontalMinimumActivityLabel2
			// 
			this.HorizontalMinimumActivityLabel2.Location = new System.Drawing.Point(312, 48);
			this.HorizontalMinimumActivityLabel2.Name = "HorizontalMinimumActivityLabel2";
			this.HorizontalMinimumActivityLabel2.Size = new System.Drawing.Size(40, 16);
			this.HorizontalMinimumActivityLabel2.TabIndex = 97;
			this.HorizontalMinimumActivityLabel2.Text = "3";
			// 
			// HorizontalMinimumActivityLabel
			// 
			this.HorizontalMinimumActivityLabel.Location = new System.Drawing.Point(168, 48);
			this.HorizontalMinimumActivityLabel.Name = "HorizontalMinimumActivityLabel";
			this.HorizontalMinimumActivityLabel.Size = new System.Drawing.Size(104, 16);
			this.HorizontalMinimumActivityLabel.TabIndex = 96;
			this.HorizontalMinimumActivityLabel.Text = "Minimum Activity:";
			// 
			// HorizontalMinimumActivity
			// 
			this.HorizontalMinimumActivity.LargeChange = 1;
			this.HorizontalMinimumActivity.Location = new System.Drawing.Point(168, 64);
			this.HorizontalMinimumActivity.Maximum = 1000;
			this.HorizontalMinimumActivity.Minimum = 1;
			this.HorizontalMinimumActivity.Name = "HorizontalMinimumActivity";
			this.HorizontalMinimumActivity.Size = new System.Drawing.Size(192, 16);
			this.HorizontalMinimumActivity.TabIndex = 95;
			this.HorizontalMinimumActivity.Value = 3;
			this.HorizontalMinimumActivity.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HorizontalMinimumActivity_Scroll);
			// 
			// HorizontalIgnoreHolesCheckBox
			// 
			this.HorizontalIgnoreHolesCheckBox.Location = new System.Drawing.Point(16, 112);
			this.HorizontalIgnoreHolesCheckBox.Name = "HorizontalIgnoreHolesCheckBox";
			this.HorizontalIgnoreHolesCheckBox.Size = new System.Drawing.Size(120, 24);
			this.HorizontalIgnoreHolesCheckBox.TabIndex = 4;
			this.HorizontalIgnoreHolesCheckBox.Text = "Ignore Holes";
			// 
			// HorizontalCentralFocusCheckBox
			// 
			this.HorizontalCentralFocusCheckBox.Location = new System.Drawing.Point(16, 80);
			this.HorizontalCentralFocusCheckBox.Name = "HorizontalCentralFocusCheckBox";
			this.HorizontalCentralFocusCheckBox.Size = new System.Drawing.Size(120, 24);
			this.HorizontalCentralFocusCheckBox.TabIndex = 3;
			this.HorizontalCentralFocusCheckBox.Text = "Focus on Center";
			// 
			// HorizontalAddOnlyCheckBox
			// 
			this.HorizontalAddOnlyCheckBox.Location = new System.Drawing.Point(16, 48);
			this.HorizontalAddOnlyCheckBox.Name = "HorizontalAddOnlyCheckBox";
			this.HorizontalAddOnlyCheckBox.Size = new System.Drawing.Size(120, 24);
			this.HorizontalAddOnlyCheckBox.TabIndex = 2;
			this.HorizontalAddOnlyCheckBox.Text = "Only Add Pixels";
			// 
			// RegisterButton
			// 
			this.RegisterButton.Location = new System.Drawing.Point(632, 208);
			this.RegisterButton.Name = "RegisterButton";
			this.RegisterButton.Size = new System.Drawing.Size(80, 40);
			this.RegisterButton.TabIndex = 1;
			this.RegisterButton.Text = "Register Image";
			this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
			// 
			// HorizontalActiveCheckBox
			// 
			this.HorizontalActiveCheckBox.Checked = true;
			this.HorizontalActiveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.HorizontalActiveCheckBox.Location = new System.Drawing.Point(16, 16);
			this.HorizontalActiveCheckBox.Name = "HorizontalActiveCheckBox";
			this.HorizontalActiveCheckBox.Size = new System.Drawing.Size(88, 24);
			this.HorizontalActiveCheckBox.TabIndex = 0;
			this.HorizontalActiveCheckBox.Text = "Edge Active";
			// 
			// VerticalLinesGroupBox
			// 
			this.VerticalLinesGroupBox.Controls.AddRange(new System.Windows.Forms.Control[] {
																								this.VerticalSkipActiveCheckBox,
																								this.VerticalToLineActiveCheckBox,
																								this.VerticalToLineCheckThinLinesActiveCheckBox});
			this.VerticalLinesGroupBox.Location = new System.Drawing.Point(8, 224);
			this.VerticalLinesGroupBox.Name = "VerticalLinesGroupBox";
			this.VerticalLinesGroupBox.Size = new System.Drawing.Size(576, 216);
			this.VerticalLinesGroupBox.TabIndex = 154;
			this.VerticalLinesGroupBox.TabStop = false;
			this.VerticalLinesGroupBox.Text = "Vertical Lines";
			// 
			// VerticalSkipActiveCheckBox
			// 
			this.VerticalSkipActiveCheckBox.Location = new System.Drawing.Point(8, 120);
			this.VerticalSkipActiveCheckBox.Name = "VerticalSkipActiveCheckBox";
			this.VerticalSkipActiveCheckBox.Size = new System.Drawing.Size(120, 24);
			this.VerticalSkipActiveCheckBox.TabIndex = 139;
			this.VerticalSkipActiveCheckBox.Text = "Skip Lines";
			// 
			// VerticalToLineActiveCheckBox
			// 
			this.VerticalToLineActiveCheckBox.Location = new System.Drawing.Point(8, 152);
			this.VerticalToLineActiveCheckBox.Name = "VerticalToLineActiveCheckBox";
			this.VerticalToLineActiveCheckBox.Size = new System.Drawing.Size(144, 24);
			this.VerticalToLineActiveCheckBox.TabIndex = 143;
			this.VerticalToLineActiveCheckBox.Text = "Position Based on Line";
			// 
			// VerticalToLineCheckThinLinesActiveCheckBox
			// 
			this.VerticalToLineCheckThinLinesActiveCheckBox.Location = new System.Drawing.Point(8, 184);
			this.VerticalToLineCheckThinLinesActiveCheckBox.Name = "VerticalToLineCheckThinLinesActiveCheckBox";
			this.VerticalToLineCheckThinLinesActiveCheckBox.Size = new System.Drawing.Size(88, 24);
			this.VerticalToLineCheckThinLinesActiveCheckBox.TabIndex = 144;
			this.VerticalToLineCheckThinLinesActiveCheckBox.Text = "Thin Lines";
			// 
			// HorizontalLinesGroupBox
			// 
			this.HorizontalLinesGroupBox.Location = new System.Drawing.Point(8, 0);
			this.HorizontalLinesGroupBox.Name = "HorizontalLinesGroupBox";
			this.HorizontalLinesGroupBox.Size = new System.Drawing.Size(576, 216);
			this.HorizontalLinesGroupBox.TabIndex = 155;
			this.HorizontalLinesGroupBox.TabStop = false;
			this.HorizontalLinesGroupBox.Text = "Horizontal Lines";
			// 
			// imageXView1
			// 
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView1.AutoScroll = true;
			this.imageXView1.Location = new System.Drawing.Point(8, 96);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(360, 248);
			this.imageXView1.TabIndex = 4;
			// 
			// imageXView2
			// 
			this.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView2.AutoScroll = true;
			this.imageXView2.Location = new System.Drawing.Point(384, 96);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(376, 248);
			this.imageXView2.TabIndex = 5;
			// 
			// DescriptionListBox
			// 
			this.DescriptionListBox.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.DescriptionListBox.Items.AddRange(new object[] {
																	"This sample demonstrates the following functionality:",
																	"",
																	"Using the Deskew, Despeckle, RemoveLines, RemoveBorder, SmoothObjects, RemoveBlob" +
																	"s, CorrectInverseText,",
																	"RemoveDotShading,RemoveCombs, DetectBlankPage, CorrectNegativePage, Dilate, Erode" +
																	", and Register Image methods."});
			this.DescriptionListBox.Location = new System.Drawing.Point(8, 8);
			this.DescriptionListBox.Name = "DescriptionListBox";
			this.DescriptionListBox.Size = new System.Drawing.Size(752, 56);
			this.DescriptionListBox.TabIndex = 6;
			// 
			// lblout
			// 
			this.lblout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblout.Location = new System.Drawing.Point(480, 80);
			this.lblout.Name = "lblout";
			this.lblout.Size = new System.Drawing.Size(184, 16);
			this.lblout.TabIndex = 8;
			this.lblout.Text = "Output Image";
			this.lblout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblinput
			// 
			this.lblinput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblinput.Location = new System.Drawing.Point(80, 80);
			this.lblinput.Name = "lblinput";
			this.lblinput.Size = new System.Drawing.Size(216, 16);
			this.lblinput.TabIndex = 7;
			this.lblinput.Text = "Input Image";
			this.lblinput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// MainScreen
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(768, 859);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblout,
																		  this.lblinput,
																		  this.DescriptionListBox,
																		  this.imageXView2,
																		  this.imageXView1,
																		  this.settings});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Menu = this.menu;
			this.Name = "MainScreen";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Image Cleanup";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.settings.ResumeLayout(false);
			this.tabborder.ResumeLayout(false);
			this.tabdotshade.ResumeLayout(false);
			this.tabblank.ResumeLayout(false);
			this.tabdilate.ResumeLayout(false);
			this.tabblob.ResumeLayout(false);
			this.tabdespeck.ResumeLayout(false);
			this.tabdeskew.ResumeLayout(false);
			this.tabline.ResumeLayout(false);
			this.tabnegpage.ResumeLayout(false);
			this.tabcomb.ResumeLayout(false);
			this.tabinverse.ResumeLayout(false);
			this.tabsmooth.ResumeLayout(false);
			this.taberode.ResumeLayout(false);
			this.RegisterTab.ResumeLayout(false);
			this.VerticalLinesGroupBox.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.Run(new MainScreen());
		}

		private void ExitMenuItem_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void ImagXpressMenuItem_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void ScanFixMenuItem_Click(object sender, System.EventArgs e)
		{
			scanFix1.AboutBox();
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{


			//***Must call UnlockRuntime when application first begins
			//imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);
			//scanFix1.License.UnlockRuntime(1234, 1234, 1234, 1234);
			

			imageXView1.AutoScroll = true;
			imageXView2.AutoScroll = true;

			scanFix1.License.LicenseEdition = PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.BitonalEdition;

			label5.Text = hScrollBar1.Value.ToString();
			label6.Text = hScrollBar2.Value.ToString();
			label7.Text = hScrollBar3.Value.ToString();
			label8.Text = hScrollBar4.Value.ToString();
			label13.Text = hScrollBar5.Value.ToString();
			label14.Text = hScrollBar6.Value.ToString();
			label15.Text = hScrollBar7.Value.ToString();
			label16.Text = hScrollBar8.Value.ToString();
			label18.Text = hScrollBar9.Value.ToString();
			label34.Text = hScrollBar15.Value.ToString();
			label35.Text = hScrollBar16.Value.ToString();
			label30.Text = hScrollBar14.Value.ToString();

			label23.Text = hScrollBar10.Value.ToString();
			label24.Text = hScrollBar11.Value.ToString();
			label25.Text = hScrollBar12.Value.ToString();
			label26.Text = hScrollBar13.Value.ToString();
		}

		private void OpenMenuItem_Click(object sender, System.EventArgs e)
		{
			try
			{
				cd.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.TIF)|*.BMP;*.JPG;*.GIF;*.TIF|All files (*.*)|*.*";
				cd.FilterIndex = 0;
				strCurrentDir = System.IO.Directory.GetCurrentDirectory();
				strImageFile = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");
				cd.InitialDirectory = strImageFile;

				cd.ShowDialog();

				if (cd.FileName != "")
				{
		
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(cd.FileName);
					
					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}
				}
				else
				{
					if (imageXView1.Image != null)
					{
						imageXView1.Image.Dispose();
						imageXView1.Image = null;
					}
					if (imageXView2.Image != null)
					{
						imageXView2.Image.Dispose();
						imageXView2.Image = null;
					}
				}
			}
			catch(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void hScrollBar1_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label5.Text = hScrollBar1.Value.ToString();
		}

		private void hScrollBar2_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label6.Text = hScrollBar2.Value.ToString();
		}

		private void hScrollBar3_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label7.Text = hScrollBar3.Value.ToString();
		}

		private void hScrollBar4_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label8.Text = hScrollBar4.Value.ToString();
		}

		private void tabborder_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			cmbpad.SelectedIndex = 0;
		}

		private void cmdborder_Click(object sender, System.EventArgs e)
		{
			try
			{
				//** Create the border removal object and deskew image
				borderopt = new PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions();
				borderres = new PegasusImaging.WinForms.ScanFix5.BorderRemovalResults();
				borderopt.BorderSpeckSize = hScrollBar1.Value;
				borderopt.PageSpeckSize = hScrollBar2.Value;
				borderopt.MinimumConfidence = hScrollBar4.Value;
				borderopt.Quality = hScrollBar3.Value;
				if (chkcrop.Checked)
				{
					borderopt.CropBorder = true;
				}
				else
				{
					borderopt.CropBorder = false;
				}
				if(chkreplace.Checked)
				{
					borderopt.ReplaceBorder = true;
				}
				else
				{
					borderopt.ReplaceBorder = false;

				}
				if(chkdeskew.Checked)
				{
					borderopt.DeskewBorder = true;
				}
				else
				{
					borderopt.DeskewBorder = false;

				}

				if( cmbpad.SelectedIndex == 0 )
				{
					borderopt.PadColor = System.Drawing.Color.White;
				}
				else
				{
					borderopt.PadColor = System.Drawing.Color.Black;

				}

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					try
					{
						borderres  = scanFix1.RemoveBorder(borderopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}

					MessageBox.Show("Image was modified="  + borderres.ImageWasModified + Environment.NewLine + "Confidence=" + borderres.Confidence + Environment.NewLine + "Horizontal Angle=" + borderres.DetectedHorizontalAngle + Environment.NewLine + "Vertical Angle=" + borderres.DetectedVerticalAngle + Environment.NewLine + "Page Height=" + borderres.PageHeight + Environment.NewLine + "Page Width=" + borderres.PageWidth + Environment.NewLine + "PageX=" + borderres.PageX + Environment.NewLine + "PageY=" + borderres.PageY, "Border Removal", MessageBoxButtons.OK, MessageBoxIcon.Information);
					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void hScrollBar5_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label13.Text = hScrollBar5.Value.ToString();
		}

		private void hScrollBar6_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label14.Text = hScrollBar6.Value.ToString();
		}

		private void hScrollBar7_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label15.Text = hScrollBar7.Value.ToString();
		}

		private void hScrollBar8_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label16.Text = hScrollBar8.Value.ToString();
		}

		private void hScrollBar9_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label18.Text = hScrollBar9.Value.ToString();
		}

		private void cmdprocess_Click(object sender, System.EventArgs e)
		{
			try
			{
				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					try
					{
						scanFix1.SmoothObjects(hScrollBar9.Value);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					


					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}


					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void hScrollBar11_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label24.Text =  hScrollBar11.Value.ToString();
		}

		private void hScrollBar10_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label23.Text =  hScrollBar10.Value.ToString();
		}

		private void hScrollBar12_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label25.Text =  hScrollBar12.Value.ToString();
		}

		private void hScrollBar13_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label26.Text =  hScrollBar13.Value.ToString();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				//** Create the line removal object
				lineopt = new PegasusImaging.WinForms.ScanFix5.LineRemovalOptions();
				lineres = new PegasusImaging.WinForms.ScanFix5.LineRemovalResults();

				lineopt.MaximumCharacterRepairSize = hScrollBar13.Value;
				lineopt.MaximumGap = hScrollBar12.Value;
				lineopt.MaximumThickness = hScrollBar11.Value;
				lineopt.MinimumLength = hScrollBar10.Value;

				lineopt.MinimumAspectRatio = Convert.ToDouble(textBox1.Text);
				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					try
					{
						lineres = scanFix1.RemoveLines(lineopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + lineres.ImageWasModified + Environment.NewLine + "Line Count:" + lineres.CountOfLinesFound, "Line Removal", MessageBoxButtons.OK, MessageBoxIcon.Information);


					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}


					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void hScrollBar14_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label30.Text = hScrollBar14.Value.ToString();
		}

		private void tabdeskew_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			cmbcolor.SelectedIndex = 0;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			try
			{
				//** Create the deskew object and deskew image
				deskewopt = new PegasusImaging.WinForms.ScanFix5.DeskewOptions();
				deskewres = new PegasusImaging.WinForms.ScanFix5.DeskewResults();

				deskewopt.MaintainOriginalSize = checkBox1.Checked;
				deskewopt.MaximumAcceptableAngle = Convert.ToDouble(textBox2.Text);
				deskewopt.MinimumConfidence = hScrollBar14.Value;
				if( cmbcolor.SelectedIndex == 0)
				{
					deskewopt.PadColor = System.Drawing.Color.White;
				}
				else
				{
					deskewopt.PadColor = System.Drawing.Color.Black;
				}

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					try
					{
						deskewres =  scanFix1.Deskew(deskewopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{

						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					}
					MessageBox.Show("Image was modified="  + deskewres.ImageWasModified + Environment.NewLine + "Confidence=" + deskewres.Confidence + Environment.NewLine + "Angle=" + deskewres.RotationAngle + Environment.NewLine + "Variation=" + deskewres.Variation + Environment.NewLine, "Deskew", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}


					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void hScrollBar15_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label34.Text = hScrollBar15.Value.ToString();
		}

		private void hScrollBar16_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label35.Text = hScrollBar16.Value.ToString();
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			try
			{
				speckopt = new PegasusImaging.WinForms.ScanFix5.DespeckleOptions();
				speckres = new PegasusImaging.WinForms.ScanFix5.DespeckleResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					speckopt.SpeckHeight = hScrollBar15.Value;
					speckopt.SpeckWidth = hScrollBar16.Value;
					try
					{
						speckres = scanFix1.Despeckle(speckopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + speckres.ImageWasModified + Environment.NewLine + "Speck Count:" + speckres.CountOfSpecksFound, "Despeckle", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}


					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}


		private void cmdblob_Click(object sender, System.EventArgs e)
		{
			try
			{
				blobopt = new PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions();
				blobres = new PegasusImaging.WinForms.ScanFix5.BlobRemovalResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					System.Drawing.Rectangle rec = new System.Drawing.Rectangle();
					rec.X = scrollx.Value;
					rec.Y = scrolly.Value;
					rec.Width = scrollw.Value;
					rec.Height = scrolh.Value;
					blobopt.Area = rec;
					blobopt.MaximumPixelCount = scrollm.Value;
					blobopt.MinimumDensity = scrollden.Value;
					blobopt.MinimumPixelCount = scrollmin.Value;

					try
					{
						blobres = scanFix1.RemoveBlobs(blobopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + blobres.ImageWasModified + Environment.NewLine + "Blob Count:" + blobres.CountOfBlobsFound, "Blob Removal", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}


					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void Button5_Click(object sender, System.EventArgs e)
		{
			try
			{
				//** Create the dilate object and dilate image
				dilateopt = new PegasusImaging.WinForms.ScanFix5.DilateOptions();

				dilateopt.Amount = HScrollBar30.Value;
				dilateopt.Direction = 0;

				if(CheckBox5.Checked)
				{
					dilateopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.All;
				}
				else
				{
					if(CheckBox6.Checked)
					{
						dilateopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Up;
					}
					if(CheckBox8.Checked)
					{
						dilateopt.Direction += (int)PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Down;
					}
					if(CheckBox7.Checked)
					{
						dilateopt.Direction += (int)PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Left;
					}
					if(CheckBox9.Checked)
					{
						dilateopt.Direction += (int)PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Right;
					}
				}

				dilateopt.OnlyDiagonal = CheckBox4.Checked;

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					try
					{
						scanFix1.Dilate(dilateopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}


					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					


					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			try
			{
				//** Create the dilate object and dilate image
				erodeopt = new PegasusImaging.WinForms.ScanFix5.ErodeOptions();

				erodeopt.Amount = hScrollBar42.Value;
				erodeopt.Direction = 0;

				if(CheckBox14.Checked)
				{
					erodeopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.All;
				}
				else
				{
					if(CheckBox13.Checked)
					{
						erodeopt.Direction = PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Up;
					}
					if(CheckBox12.Checked)
					{
						erodeopt.Direction += (int)PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Left;
					}
					if(CheckBox11.Checked)
					{
						erodeopt.Direction += (int)PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Down;
					}
					if(CheckBox10.Checked)
					{
						erodeopt.Direction += (int)PegasusImaging.WinForms.ScanFix5.EnhancementDirections.Right;
					}
				}

				erodeopt.OnlyDiagonal = CheckBox15.Checked;

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					try
					{
						scanFix1.Erode(erodeopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					


					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			try
			{
				dotopt = new PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions();
				dotres = new PegasusImaging.WinForms.ScanFix5.DotShadingRemovalResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					dotopt.DensityAdjustment = HScrollBar19.Value;
					dotopt.HorizontalSizeAdjustment = HScrollBar20.Value;
					dotopt.MaximumDotSize = HScrollBar18.Value;
					dotopt.MinimumAreaHeight = HScrollBar22.Value;
					dotopt.VerticalSizeAdjustment = HScrollBar21.Value;

					try
					{
						dotres = scanFix1.RemoveDotShading(dotopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + dotres.ImageWasModified + Environment.NewLine + "Area Count:" + dotres.CountOfAreasFound, "Dot Shading Removal", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void cmdBlank_Click(object sender, System.EventArgs e)
		{
			try
			{
				blankopt = new PegasusImaging.WinForms.ScanFix5.BlankPageDetectOptions();
				blankres = new PegasusImaging.WinForms.ScanFix5.BlankPageDetectResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					blankopt.IgnoreBorder = CheckBox2.Checked;
					blankopt.TopMargin = hScrollBar28.Value;
					blankopt.LeftMargin = hScrollBar27.Value;
					blankopt.BottomMargin = hScrollBar26.Value;
					blankopt.RightMargin = hScrollBar25.Value;
					blankopt.MinimumObjectDimension = hScrollBar24.Value;
					blankopt.GapFill = hScrollBar17.Value;

					try
					{
						blankres = scanFix1.DetectBlankPage(blankopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Confidence: "  + blankres.Confidence + Environment.NewLine + "Page is blank: " + blankres.IsBlank, "Blank Page Detection", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			try
			{
				combopt = new PegasusImaging.WinForms.ScanFix5.CombRemovalOptions();
				combres = new PegasusImaging.WinForms.ScanFix5.CombRemovalResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					System.Drawing.Rectangle rec = new System.Drawing.Rectangle();
					rec.X = hScrollBar41.Value;
					rec.Y = hScrollBar40.Value;
					rec.Width = hScrollBar39.Value;
					rec.Height = hScrollBar38.Value;
					combopt.Area = rec;
					combopt.CombHeight = hScrollBar37.Value;
					combopt.CombSpacing = hScrollBar36.Value;
					combopt.MinimumCombLength = hScrollBar35.Value;
					combopt.MinimumConfidence = hScrollBar34.Value;
					combopt.HorizontalLineThickness = hScrollBar33.Value;
					combopt.VerticalLineThickness = hScrollBar32.Value;

					try
					{
						combres = scanFix1.RemoveCombs(combopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + combres.ImageWasModified + Environment.NewLine + "Comb Count:" + combres.CountOfCombsFound, "Comb Removal", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			try
			{
				negpageopt = new PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions();
				negpageres = new PegasusImaging.WinForms.ScanFix5.NegativeCorrectionResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{

					negpageopt.ApplyCorrection = CheckBox3.Checked;
					negpageopt.MinimumConfidence = hScrollBar31.Value;
					negpageopt.Quality = hScrollBar29.Value;

					try
					{
						negpageres = scanFix1.CorrectNegativePage(negpageopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + negpageres.ImageWasModified + Environment.NewLine + "Confidence: " + negpageres.Confidence + Environment.NewLine + "Image Is Normal=" + negpageres.ImageIsNormal, "Negative Page Correction", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					


					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void cmdinverse_Click(object sender, System.EventArgs e)
		{
			try
			{
				inverseopt = new PegasusImaging.WinForms.ScanFix5.InverseTextOptions();
				inverseres = new PegasusImaging.WinForms.ScanFix5.InverseTextResults();

				if( imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					inverseopt.MinimumAreaHeight = minheight.Value;
					inverseopt.MinimumAreaWidth = minwidth.Value;
					inverseopt.MinimumBlankOnEdges = minblack.Value;

					try
					{
						inverseres = scanFix1.CorrectInverseText(inverseopt);
					}
					catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
					{
						MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					MessageBox.Show("Image was modified="  + inverseres.ImageWasModified + Environment.NewLine + "Area Count:" + inverseres.CountOfAreasFound, "Inverse Text Correction", MessageBoxButtons.OK, MessageBoxIcon.Information);

					//*** Code here to display the image
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
					

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void scrollx_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label43.Text = scrollx.Value.ToString();
		}

		private void scrolly_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label42.Text = scrolly.Value.ToString();
		}

		private void scrollw_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label41.Text = scrollw.Value.ToString();
		}

		private void scrolh_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label40.Text = scrolh.Value.ToString();
		}

		private void scrollmin_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label39.Text = scrollmin.Value.ToString();
		}

		private void scrollm_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label38.Text = scrollm.Value.ToString();
		}

		private void scrollden_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label37.Text = scrollden.Value.ToString();
		}

		private void HScrollBar30_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label83.Text = HScrollBar30.Value.ToString();
		}

		private void CheckBox5_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckBox6.Enabled = !CheckBox6.Enabled;
			CheckBox7.Enabled = !CheckBox7.Enabled;
			CheckBox8.Enabled = !CheckBox8.Enabled;
			CheckBox9.Enabled = !CheckBox9.Enabled;
		}

		private void CheckDilateDiag()
		{
			if ((((CheckBox6.Checked == true) 
				&& ((CheckBox8.Checked == false) 
				&& ((CheckBox7.Checked == true) 
				&& (CheckBox9.Checked == false)))) 
				|| (((CheckBox6.Checked == true) 
				&& ((CheckBox8.Checked == false) 
				&& ((CheckBox7.Checked == false) 
				&& (CheckBox9.Checked == true)))) 
				|| (((CheckBox6.Checked == false) 
				&& ((CheckBox8.Checked == true) 
				&& ((CheckBox7.Checked == true) 
				&& (CheckBox9.Checked == false)))) 
				|| ((CheckBox6.Checked == false) 
				&& ((CheckBox8.Checked == true) 
				&& ((CheckBox7.Checked == false) 
				&& (CheckBox9.Checked == true))))))))
			{
				CheckBox4.Enabled = true;
			}
			else
			{
				CheckBox4.Checked = false;
				CheckBox4.Enabled = false;
			}
		}

		private void CheckBox6_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckDilateDiag();
		}

		private void CheckBox8_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckDilateDiag();
		}

		private void CheckBox7_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckDilateDiag();
		}

		private void CheckBox9_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckDilateDiag();
		}

		private void hScrollBar42_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label107.Text = hScrollBar42.Value.ToString();
		}

		private void CheckBox14_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckBox10.Enabled = !CheckBox10.Enabled;
			CheckBox11.Enabled = !CheckBox11.Enabled;
			CheckBox12.Enabled = !CheckBox12.Enabled;
			CheckBox13.Enabled = !CheckBox13.Enabled;
		}

		private void CheckErodeDiag()
		{
			if ((((CheckBox13.Checked == true) 
				&& ((CheckBox11.Checked == false) 
				&& ((CheckBox12.Checked == true) 
				&& (CheckBox10.Checked == false)))) 
				|| (((CheckBox13.Checked == true) 
				&& ((CheckBox11.Checked == false) 
				&& ((CheckBox12.Checked == false) 
				&& (CheckBox10.Checked == true)))) 
				|| (((CheckBox13.Checked == false) 
				&& ((CheckBox11.Checked == true) 
				&& ((CheckBox12.Checked == true) 
				&& (CheckBox10.Checked == false)))) 
				|| ((CheckBox13.Checked == false) 
				&& ((CheckBox11.Checked == true) 
				&& ((CheckBox12.Checked == false) 
				&& (CheckBox10.Checked == true))))))))
			{
				CheckBox15.Enabled = true;
			}
			else
			{
				CheckBox15.Checked = false;
				CheckBox15.Enabled = false;
			}
		}

		private void CheckBox13_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckErodeDiag();
		}

		private void CheckBox11_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckErodeDiag();
		}

		private void CheckBox12_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckErodeDiag();
		}

		private void CheckBox10_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckErodeDiag();
		}

		private void HScrollBar23_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label62.Text = HScrollBar23.Value.ToString();
		}

		private void HScrollBar22_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label61.Text = HScrollBar22.Value.ToString();
		}

		private void HScrollBar21_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label60.Text = HScrollBar21.Value.ToString();
		}

		private void HScrollBar20_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label59.Text = HScrollBar20.Value.ToString();
		}

		private void HScrollBar19_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label58.Text = HScrollBar19.Value.ToString();
		}

		private void HScrollBar18_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			Label57.Text = HScrollBar18.Value.ToString();
		}

		private void hScrollBar28_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label56.Text = hScrollBar28.Value.ToString();
		}

		private void hScrollBar27_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label55.Text = hScrollBar27.Value.ToString();
		}

		private void hScrollBar26_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label54.Text = hScrollBar26.Value.ToString();
		}

		private void hScrollBar25_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label53.Text = hScrollBar25.Value.ToString();
		}

		private void hScrollBar24_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label52.Text = hScrollBar24.Value.ToString();
		}

		private void hScrollBar17_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label51.Text = hScrollBar17.Value.ToString();
		}

		private void hScrollBar31_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label76.Text = hScrollBar31.Value.ToString();
		}

		private void hScrollBar29_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label75.Text = hScrollBar29.Value.ToString();
		}

		private void hScrollBar41_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label95.Text = hScrollBar41.Value.ToString();
		}

		private void hScrollBar40_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label94.Text = hScrollBar40.Value.ToString();
		}

		private void hScrollBar39_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label93.Text = hScrollBar39.Value.ToString();
		}

		private void hScrollBar38_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label92.Text = hScrollBar38.Value.ToString();
		}

		private void hScrollBar37_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label91.Text = hScrollBar37.Value.ToString();
		}

		private void hScrollBar36_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label85.Text = hScrollBar36.Value.ToString();
		}

		private void hScrollBar35_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label84.Text = hScrollBar35.Value.ToString();
		}

		private void hScrollBar34_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label81.Text = hScrollBar34.Value.ToString();
		}

		private void hScrollBar33_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label80.Text = hScrollBar33.Value.ToString();
		}

		private void hScrollBar32_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label79.Text = hScrollBar32.Value.ToString();
		}

		private void minwidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label103.Text = minwidth.Value.ToString();
		}

		private void minheight_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label102.Text = minheight.Value.ToString();
		}

		private void minblack_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			label101.Text = minblack.Value.ToString();
		}

		private void tabblob_paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			label43.Text = scrollx.Value.ToString();
			label42.Text = scrolly.Value.ToString();
			label41.Text = scrollw.Value.ToString();
			label40.Text = scrolh.Value.ToString();
			label39.Text = scrollmin.Value.ToString();
			label38.Text = scrollm.Value.ToString();
			label37.Text = scrollden.Value.ToString();
		}

		private void tabdilate_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Label83.Text = HScrollBar30.Value.ToString();
		}

		private void tabdotshade_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Label62.Text = HScrollBar23.Value.ToString();
			Label61.Text = HScrollBar22.Value.ToString();
			Label60.Text = HScrollBar21.Value.ToString();
			Label59.Text = HScrollBar20.Value.ToString();
			Label58.Text = HScrollBar19.Value.ToString();
			Label57.Text = HScrollBar18.Value.ToString();
		}

		private void tabblank_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			label56.Text = hScrollBar28.Value.ToString();
			label55.Text = hScrollBar27.Value.ToString();
			label54.Text = hScrollBar26.Value.ToString();
			label53.Text = hScrollBar25.Value.ToString();
			label52.Text = hScrollBar24.Value.ToString();
			label51.Text = hScrollBar17.Value.ToString();
		}

		private void tabnegpage_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			label76.Text = hScrollBar31.Value.ToString();
			label75.Text = hScrollBar29.Value.ToString();
		}

		private void tabcomb_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			label95.Text = hScrollBar41.Value.ToString();
			label94.Text = hScrollBar40.Value.ToString();
			label93.Text = hScrollBar39.Value.ToString();
			label92.Text = hScrollBar38.Value.ToString();
			label91.Text = hScrollBar37.Value.ToString();
			label85.Text = hScrollBar36.Value.ToString();
			label84.Text = hScrollBar35.Value.ToString();
			label81.Text = hScrollBar34.Value.ToString();
			label80.Text = hScrollBar33.Value.ToString();
			label79.Text = hScrollBar32.Value.ToString();
		}

		private void tabinverse_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			label103.Text = minwidth.Value.ToString();
			label102.Text = minheight.Value.ToString();
			label101.Text = minblack.Value.ToString();
		}

		private void taberode_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			label107.Text = hScrollBar42.Value.ToString();
		}

		private void RegisterButton_Click(object sender, System.EventArgs e)
		{
			try
			{
				regOpts = new PegasusImaging.WinForms.ScanFix5.ImageRegistrationOptions();
				regResults = new PegasusImaging.WinForms.ScanFix5.ImageRegistrationResults();

				if(imageXView1.Image == null)
				{
					MessageBox.Show("Please choose an image first", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					if (HorizontalActiveCheckBox.Checked == true)
					{
						regOpts.HorizontalActive = true;
					}
					else
					{
						regOpts.HorizontalActive = false;
					}

					if (HorizontalAddOnlyCheckBox.Checked == true)
					{
						regOpts.HorizontalAddOnly = true;
					}
					else
					{
						regOpts.HorizontalAddOnly = false;
					}

					if (HorizontalCentralFocusCheckBox.Checked == true)
					{
						regOpts.HorizontalCentralFocus = true;
					}
					else
					{
						regOpts.HorizontalCentralFocus = false;
					}

					if(HorizontalIgnoreHolesCheckBox.Checked == true)
					{
						regOpts.HorizontalIgnoreHoles = true;
					}
					else
					{
						regOpts.HorizontalIgnoreHoles = false;
					}

					regOpts.HorizontalMinimumActivity = HorizontalMinimumActivity.Value;
					regOpts.HorizontalMinimumBackground = HorizontalMinimumBackground.Value;
					regOpts.HorizontalMinimumForeground = HorizontalMinimumForeground.Value;
					regOpts.HorizontalResultantMargin = HorizontalResultantMargin.Value;

					if(HorizontalSkipActiveCheckBox.Checked == true)
					{
						regOpts.HorizontalSkipActive = true;
					}
					else
					{
						regOpts.HorizontalSkipActive = false;
					}

					regOpts.HorizontalSkipDistance = HorizontalSkipDistance.Value;

					if (HorizontalToLineActiveCheckBox.Checked == true)
					{
						regOpts.HorizontalToLineActive = true;
					}
					else
					{
						regOpts.HorizontalToLineActive = false;
					}

					if(HorizontalToLineCheckThinLinesActiveCheckBox.Checked == true)
					{
						regOpts.HorizontalToLineCheckThinLinesActive = true;
					}
					else
					{
						regOpts.HorizontalToLineCheckThinLinesActive = false;
					}

					regOpts.HorizontalToLineMaximumLineGap = HorizontalToLineMaximumLineGap.Value;

					regOpts.HorizontalToLineMaximumLineThickness = HorizontalToLineMaximumLineThickness.Value;

					regOpts.HorizontalToLineMinimumLineLength = HorizontalToLineMinimumLineLength.Value;


					if (VerticalActiveCheckBox.Checked == true)
					{
						regOpts.VerticalActive = true;
					}
					else
					{
						regOpts.VerticalActive = false;
					}

					if (VerticalAddOnlyCheckBox.Checked == true)
					{
						regOpts.VerticalAddOnly = true;
					}
					else
					{
						regOpts.VerticalAddOnly = false;
					}

					if (VerticalCentralFocusCheckBox.Checked == true)
					{
						regOpts.VerticalCentralFocus = true;
					}
					else
					{
						regOpts.VerticalCentralFocus = false;
					}

					regOpts.VerticalMinimumActivity = VerticalMinimumActivity.Value;
					regOpts.VerticalMinimumBackground = VerticalMinimumBackground.Value;
					regOpts.VerticalMinimumForeground = VerticalMinimumForeground.Value;
					regOpts.VerticalResultantMargin = VerticalResultantMargin.Value;

					if(VerticalSkipActiveCheckBox.Checked == true)
					{
						regOpts.VerticalSkipActive = true;
					}
					else
					{
						regOpts.VerticalSkipActive = false;
					}

					regOpts.VerticalSkipDistance = VerticalSkipDistance.Value;

					if (VerticalToLineActiveCheckBox.Checked == true)
					{
						regOpts.VerticalToLineActive = true;
					}
					else
					{
						regOpts.VerticalToLineActive = false;
					}

					if(VerticalToLineCheckThinLinesActiveCheckBox.Checked == true)
					{
						regOpts.VerticalToLineCheckThinLinesActive = true;
					}
					else
					{
						regOpts.VerticalToLineCheckThinLinesActive = false;
					}

					regOpts.VerticalToLineMaximumLineGap = VerticalToLineMaximumLineGap.Value;

					regOpts.VerticalToLineMaximumLineThickness = VerticalToLineMaximumLineThickness.Value;

					regOpts.VerticalToLineMinimumLineLength = VerticalToLineMinimumLineLength.Value;


					regResults = scanFix1.RegisterImage(regOpts);

					MessageBox.Show("Horizonal Margin Adjustment: " + regResults.HorizontalMarginAdjustment.ToString() + "\n" + "Vertical Margin Adjustment: " + regResults.VerticalMarginAdjustment + "\n" + "Image was modified: " + regResults.ImageWasModified.ToString(), "Image Registration Results", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false), true);;

					//**pass the image data to ScanFix for processing    
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
					if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{    
						scanFix1.AutoBinarize();
					}

					Application.DoEvents();
					
				}

			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		
		}

		private void RegisterTab_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			HorizontalMinimumActivityLabel2.Text = HorizontalMinimumActivity.Value.ToString();
			HorizontalMinimumBackgroundLabel2.Text = HorizontalMinimumBackground.Value.ToString();
			HorizontalMinimumForegroundLabel2.Text = HorizontalMinimumForeground.Value.ToString();
			HorizontalResultantMarginLabel2.Text = HorizontalResultantMargin.Value.ToString();
			HorizontalSkipDistanceLabel2.Text = HorizontalSkipDistance.Value.ToString();
			HorizontalToLineMaximumLineGapLabel2.Text = HorizontalToLineMaximumLineGap.Value.ToString();
			HorizontalToLineMaximumLineThicknessLabel2.Text = HorizontalToLineMaximumLineThickness.Value.ToString();
			HorizontalToLineMinimumLineLengthLabel2.Text = HorizontalToLineMinimumLineLength.Value.ToString();

			VerticalMinimumActivityLabel2.Text = VerticalMinimumActivity.Value.ToString();
			VerticalMinimumBackgroundLabel2.Text = VerticalMinimumBackground.Value.ToString();
			VerticalMinimumForegroundLabel2.Text = VerticalMinimumForeground.Value.ToString();
			VerticalResultantMarginLabel2.Text = VerticalResultantMargin.Value.ToString();
			VerticalSkipDistanceLabel2.Text = VerticalSkipDistance.Value.ToString();
			VerticalToLineMaximumLineGapLabel2.Text = VerticalToLineMaximumLineGap.Value.ToString();
			VerticalToLineMaximumLineThicknessLabel2.Text = VerticalToLineMaximumLineThickness.Value.ToString();
			VerticalToLineMinimumLineLengthLabel2.Text = VerticalToLineMinimumLineLength.Value.ToString();
		}

		private void HorizontalMinimumActivity_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalMinimumActivityLabel2.Text = HorizontalMinimumActivity.Value.ToString();
		}

		private void HorizontalMinimumBackground_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalMinimumBackgroundLabel2.Text = HorizontalMinimumBackground.Value.ToString();
		}

		private void HorizontalMinimumForeground_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalMinimumForegroundLabel2.Text = HorizontalMinimumForeground.Value.ToString();
		}

		private void HorizontalResultantMargin_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalResultantMarginLabel2.Text = HorizontalResultantMargin.Value.ToString();
		}

		private void HorizontalSkipDistance_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalSkipDistanceLabel2.Text = HorizontalSkipDistance.Value.ToString();
		}

		private void HorizontalToLineMaximumLineGap_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalToLineMaximumLineGapLabel2.Text = HorizontalToLineMaximumLineGap.Value.ToString();
		}

		private void HorizontalToLineMaximumLineThickness_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalToLineMaximumLineThicknessLabel2.Text = HorizontalToLineMaximumLineThickness.Value.ToString();
		}

		private void VerticalToLineMaximumLineThickness_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalToLineMaximumLineThicknessLabel2.Text = VerticalToLineMaximumLineThickness.Value.ToString();
		}

		private void HorizontalToLineMinimumLineLength_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HorizontalToLineMinimumLineLengthLabel2.Text = HorizontalToLineMinimumLineLength.Value.ToString();
		}

		private void VerticalToLineMaximumLineGap_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalToLineMaximumLineGapLabel2.Text = VerticalToLineMaximumLineGap.Value.ToString();
		}

		private void VerticalSkipDistance_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalSkipDistanceLabel2.Text = VerticalSkipDistance.Value.ToString();
		}

		private void VerticalResultantMargin_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalResultantMarginLabel2.Text = VerticalResultantMargin.Value.ToString();
		}

		private void VerticalMinimumForeground_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalMinimumForegroundLabel2.Text = VerticalMinimumForeground.Value.ToString();
		}

		private void VerticalMinimumBackground_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalMinimumBackgroundLabel2.Text = VerticalMinimumBackground.Value.ToString();
		}

		private void VerticalMinimumActivity_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalMinimumActivityLabel2.Text = VerticalMinimumActivity.Value.ToString();
		}

		private void VerticalToLineMinimumLineLength_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VerticalToLineMinimumLineLengthLabel2.Text = VerticalToLineMinimumLineLength.Value.ToString();
		}
	}
}

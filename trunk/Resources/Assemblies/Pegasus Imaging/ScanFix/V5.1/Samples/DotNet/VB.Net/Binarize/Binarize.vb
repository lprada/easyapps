'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Public Class Binarize
    Inherits System.Windows.Forms.Form

    '**Create a binarize options object **
    Dim settings As PegasusImaging.WinForms.ScanFix5.BinarizeOptions
    Dim strCurrentDir As String
    Dim strImageFile As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()


        '***Must call UnlockControl before InitializeComponent statement
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        'PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234)


        'This call is required by the Windows Form Designer.
        InitializeComponent()


        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            If Not ImageXView1.Image Is Nothing Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If
            If Not ImageXView2.Image Is Nothing Then
                ImageXView2.Image.Dispose()
                ImageXView2.Image = Nothing

            End If
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (ImageXView2 Is Nothing) Then
                ImageXView2.Dispose()
                ImageXView2 = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ScanFix1 Is Nothing) Then
                ScanFix1.Dispose()
                ScanFix1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    'Friend WithEvents ScanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents cd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents frmprocess As System.Windows.Forms.GroupBox
    Friend WithEvents lbllow As System.Windows.Forms.Label
    Friend WithEvents lblhigh As System.Windows.Forms.Label
    Friend WithEvents lblcontrast As System.Windows.Forms.Label
    Friend WithEvents sliderl As System.Windows.Forms.TrackBar
    Friend WithEvents sliderh As System.Windows.Forms.TrackBar
    Friend WithEvents slidercon As System.Windows.Forms.TrackBar
    Friend WithEvents labellow As System.Windows.Forms.Label
    Friend WithEvents labelhigh As System.Windows.Forms.Label
    Friend WithEvents lblcon As System.Windows.Forms.Label
    Friend WithEvents optnone As System.Windows.Forms.RadioButton
    Friend WithEvents optauto As System.Windows.Forms.RadioButton
    Friend WithEvents optgaussian As System.Windows.Forms.RadioButton
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImageXView2 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents InputLabel As System.Windows.Forms.Label
    Friend WithEvents OutputLabel As System.Windows.Forms.Label
    Friend WithEvents ScanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ScanFixMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents OpenMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents AboutMenu As System.Windows.Forms.MenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        settings = New PegasusImaging.WinForms.ScanFix5.BinarizeOptions()

        Me.components = New System.ComponentModel.Container()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.FileMenu = New System.Windows.Forms.MenuItem()
        Me.OpenMenuItem = New System.Windows.Forms.MenuItem()
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem()
        Me.AboutMenu = New System.Windows.Forms.MenuItem()
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem()
        Me.ScanFixMenuItem = New System.Windows.Forms.MenuItem()
        Me.cd = New System.Windows.Forms.OpenFileDialog()
        Me.frmprocess = New System.Windows.Forms.GroupBox()
        Me.optgaussian = New System.Windows.Forms.RadioButton()
        Me.optauto = New System.Windows.Forms.RadioButton()
        Me.optnone = New System.Windows.Forms.RadioButton()
        Me.lblcon = New System.Windows.Forms.Label()
        Me.labelhigh = New System.Windows.Forms.Label()
        Me.labellow = New System.Windows.Forms.Label()
        Me.slidercon = New System.Windows.Forms.TrackBar()
        Me.sliderh = New System.Windows.Forms.TrackBar()
        Me.sliderl = New System.Windows.Forms.TrackBar()
        Me.lblcontrast = New System.Windows.Forms.Label()
        Me.lblhigh = New System.Windows.Forms.Label()
        Me.lbllow = New System.Windows.Forms.Label()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImageXView2 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.InputLabel = New System.Windows.Forms.Label()
        Me.OutputLabel = New System.Windows.Forms.Label()
        Me.ScanFix1 = New PegasusImaging.WinForms.ScanFix5.ScanFix(Me.components)
        Me.frmprocess.SuspendLayout()
        CType(Me.slidercon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sliderh, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sliderl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.AboutMenu})
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.OpenMenuItem, Me.ExitMenuItem})
        Me.FileMenu.Text = "&File"
        '
        'OpenMenuItem
        '
        Me.OpenMenuItem.Index = 0
        Me.OpenMenuItem.Text = "&Open"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 1
        Me.ExitMenuItem.Text = "E&xit"
        '
        'AboutMenu
        '
        Me.AboutMenu.Index = 1
        Me.AboutMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ImagXpressMenuItem, Me.ScanFixMenuItem})
        Me.AboutMenu.Text = "&About"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 0
        Me.ImagXpressMenuItem.Text = "Imag&Xpress"
        '
        'ScanFixMenuItem
        '
        Me.ScanFixMenuItem.Index = 1
        Me.ScanFixMenuItem.Text = "Scan&Fix"
        '
        'frmprocess
        '
        Me.frmprocess.Controls.AddRange(New System.Windows.Forms.Control() {Me.optgaussian, Me.optauto, Me.optnone, Me.lblcon, Me.labelhigh, Me.labellow, Me.slidercon, Me.sliderh, Me.sliderl, Me.lblcontrast, Me.lblhigh, Me.lbllow})
        Me.frmprocess.Location = New System.Drawing.Point(8, 360)
        Me.frmprocess.Name = "frmprocess"
        Me.frmprocess.Size = New System.Drawing.Size(760, 224)
        Me.frmprocess.TabIndex = 3
        Me.frmprocess.TabStop = False
        Me.frmprocess.Text = "Binarize Settings"
        '
        'optgaussian
        '
        Me.optgaussian.Location = New System.Drawing.Point(496, 184)
        Me.optgaussian.Name = "optgaussian"
        Me.optgaussian.TabIndex = 12
        Me.optgaussian.Text = "Gaussian Blur"
        '
        'optauto
        '
        Me.optauto.Location = New System.Drawing.Point(368, 184)
        Me.optauto.Name = "optauto"
        Me.optauto.TabIndex = 11
        Me.optauto.Text = "Auto Blur"
        '
        'optnone
        '
        Me.optnone.Checked = True
        Me.optnone.Location = New System.Drawing.Point(248, 184)
        Me.optnone.Name = "optnone"
        Me.optnone.TabIndex = 10
        Me.optnone.TabStop = True
        Me.optnone.Text = "No Blur"
        '
        'lblcon
        '
        Me.lblcon.Location = New System.Drawing.Point(544, 136)
        Me.lblcon.Name = "lblcon"
        Me.lblcon.Size = New System.Drawing.Size(80, 16)
        Me.lblcon.TabIndex = 9
        Me.lblcon.Text = "Label3"
        '
        'labelhigh
        '
        Me.labelhigh.Location = New System.Drawing.Point(544, 72)
        Me.labelhigh.Name = "labelhigh"
        Me.labelhigh.Size = New System.Drawing.Size(80, 16)
        Me.labelhigh.TabIndex = 8
        Me.labelhigh.Text = "Label2"
        '
        'labellow
        '
        Me.labellow.Location = New System.Drawing.Point(544, 24)
        Me.labellow.Name = "labellow"
        Me.labellow.Size = New System.Drawing.Size(80, 24)
        Me.labellow.TabIndex = 7
        Me.labellow.Text = "Label1"
        '
        'slidercon
        '
        Me.slidercon.Location = New System.Drawing.Point(272, 120)
        Me.slidercon.Maximum = 255
        Me.slidercon.Name = "slidercon"
        Me.slidercon.Size = New System.Drawing.Size(256, 45)
        Me.slidercon.TabIndex = 6
        Me.slidercon.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'sliderh
        '
        Me.sliderh.Location = New System.Drawing.Point(272, 64)
        Me.sliderh.Maximum = 255
        Me.sliderh.Name = "sliderh"
        Me.sliderh.Size = New System.Drawing.Size(256, 45)
        Me.sliderh.TabIndex = 5
        Me.sliderh.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'sliderl
        '
        Me.sliderl.Location = New System.Drawing.Point(272, 16)
        Me.sliderl.Maximum = 255
        Me.sliderl.Name = "sliderl"
        Me.sliderl.Size = New System.Drawing.Size(256, 45)
        Me.sliderl.TabIndex = 4
        Me.sliderl.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'lblcontrast
        '
        Me.lblcontrast.Location = New System.Drawing.Point(144, 120)
        Me.lblcontrast.Name = "lblcontrast"
        Me.lblcontrast.Size = New System.Drawing.Size(104, 32)
        Me.lblcontrast.TabIndex = 3
        Me.lblcontrast.Text = "Contrast"
        '
        'lblhigh
        '
        Me.lblhigh.Location = New System.Drawing.Point(144, 64)
        Me.lblhigh.Name = "lblhigh"
        Me.lblhigh.Size = New System.Drawing.Size(104, 32)
        Me.lblhigh.TabIndex = 2
        Me.lblhigh.Text = "High Threshold"
        '
        'lbllow
        '
        Me.lbllow.Location = New System.Drawing.Point(144, 24)
        Me.lbllow.Name = "lbllow"
        Me.lbllow.Size = New System.Drawing.Size(104, 24)
        Me.lbllow.TabIndex = 1
        Me.lbllow.Text = "Low Threshold"
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(8, 120)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(384, 224)
        Me.ImageXView1.TabIndex = 4
        '
        'ImageXView2
        '
        Me.ImageXView2.AutoScroll = True
        Me.ImageXView2.Location = New System.Drawing.Point(408, 120)
        Me.ImageXView2.Name = "ImageXView2"
        Me.ImageXView2.Size = New System.Drawing.Size(360, 224)
        Me.ImageXView2.TabIndex = 5
        '
        'lstInfo
        '
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:  Using the Binarize method " & _
        "on the ScanFix control.", "", "Please Note:", "The sample starts up with the defaults for the AutoBinarize method. ", "The AutoBinarize method automatically sets the various parameters based on the co" & _
        "lordepth of the input image.", "It is also possible to adjust the various settings via the sliders."})
        Me.lstInfo.Location = New System.Drawing.Point(8, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(760, 82)
        Me.lstInfo.TabIndex = 7
        '
        'InputLabel
        '
        Me.InputLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InputLabel.Location = New System.Drawing.Point(136, 104)
        Me.InputLabel.Name = "InputLabel"
        Me.InputLabel.Size = New System.Drawing.Size(128, 16)
        Me.InputLabel.TabIndex = 8
        Me.InputLabel.Text = "Input Image"
        Me.InputLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'OutputLabel
        '
        Me.OutputLabel.Font = New System.Drawing.Font("Tahoma", 9.75!, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OutputLabel.Location = New System.Drawing.Point(536, 104)
        Me.OutputLabel.Name = "OutputLabel"
        Me.OutputLabel.Size = New System.Drawing.Size(128, 16)
        Me.OutputLabel.TabIndex = 9
        Me.OutputLabel.Text = "Output Image"
        Me.OutputLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ScanFix1
        '
        Me.ScanFix1.Debug = False
        Me.ScanFix1.DebugLogFile = "C:\Documents and Settings\jargento.JPG.COM\My Documents\ScanFix5.log"
        Me.ScanFix1.ErrorLevel = PegasusImaging.WinForms.ScanFix5.ErrorLevel.Production
        Me.ScanFix1.ResolutionUnits = System.Drawing.GraphicsUnit.Inch
        '
        'Binarize
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(776, 587)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.OutputLabel, Me.InputLabel, Me.lstInfo, Me.ImageXView2, Me.ImageXView1, Me.frmprocess})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "Binarize"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Binarize"
        Me.frmprocess.ResumeLayout(False)
        CType(Me.slidercon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sliderh, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sliderl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)




    End Sub

#End Region

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub ScanFixMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ScanFixMenuItem.Click
        ScanFix1.AboutBox()
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub OpenMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenMenuItem.Click
        Try
            cd.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.TIF)|*.BMP;*.JPG;*.GIF;*.TIF|All files (*.*)|*.*"
            cd.FilterIndex = 0

            cd.InitialDirectory = Application.ExecutablePath & "\..\..\..\..\..\..\..\..\Common\Images"

            cd.ShowDialog(Me)

            If Not cd.FileName = "" Then

                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(cd.FileName)

                '**pass the image data to ScanFix for processing    
                ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))

                '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
                If Not ImageXView1.Image.ImageXData.BitsPerPixel = 1 Then
                    ScanFix1.AutoBinarize()
                Else
                    'ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
                    MessageBox.Show("Please load in an image that is NOT 1-bit per pixel.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    If Not ImageXView1.Image Is Nothing Then
                        ImageXView1.Image.Dispose()
                        ImageXView1.Image = Nothing
                    End If
                    If Not ImageXView2.Image Is Nothing Then
                        ImageXView2.Image.Dispose()
                        ImageXView2.Image = Nothing
                    End If
                End If

            Else
                If Not ImageXView1.Image Is Nothing Then
                    ImageXView1.Image.Dispose()
                    ImageXView1.Image = Nothing
                End If
                If Not ImageXView2.Image Is Nothing Then
                    ImageXView2.Image.Dispose()
                    ImageXView2.Image = Nothing
                End If
            End If


        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Binarize_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try


            '***Must call UnlockRuntime when application first begins
            'imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
            'scanFix1.License.UnlockRuntime(1234, 1234, 1234, 1234)


            ImageXView1.AutoScroll = True
            ImageXView2.AutoScroll = True

            ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
            ImageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit


            strCurrentDir = System.IO.Directory.GetCurrentDirectory()
            strImageFile = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\binarize.jpg")

            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile)
            '**pass the image data to ScanFix for processing    
            ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
            '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
            If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                ScanFix1.AutoBinarize()
            End If

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '**These are the defaults if using the AutoBinarize method for image greater than _
        ' 1 bit per pixel
        sliderh.Value = 190
        sliderl.Value = 170
        slidercon.Value = 0

        settings.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.NoBlur

        labellow.Text = sliderl.Value
        labelhigh.Text = sliderh.Value
        lblcon.Text = slidercon.Value
        settings.LceFactor = slidercon.Value
        settings.HighThreshold = sliderh.Value
        settings.LowThreshold = sliderl.Value

        Call dobinarize()
    End Sub

    Public Function dobinarize()

        Try
            '*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
            If ImageXView1.Image.ImageXData.BitsPerPixel <> 1 Then
                '**pass the image data to ScanFix for processing
                ScanFix1.FromHdib(ImageXView1.Image.ToHdib(False))
            End If

            ScanFix1.Binarize(settings)

            '*** Code here to display the image
            ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(ScanFix1.ToHdib(False))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub sliderl_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sliderl.Scroll
        If sliderh.Value = 0 Then
            sliderl.Value = 0
        Else
            If sliderl.Value >= sliderh.Value Then
                sliderl.Value = (sliderh.Value - 1)
            End If
        End If

        settings.LowThreshold = sliderl.Value
        labellow.Text = sliderl.Value

        Try
            Call dobinarize()
        Catch ex As PegasusImaging.WinForms.ScanFix5.ScanFixException
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub sliderh_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sliderh.Scroll
        If sliderh.Value < sliderl.Value Then
            sliderh.Value = (sliderl.Value + 1)
        End If
        settings.HighThreshold = sliderh.Value
        labelhigh.Text = sliderh.Value

        Try
            Call dobinarize()
        Catch ex As PegasusImaging.WinForms.ScanFix5.ScanFixException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub slidercon_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles slidercon.Scroll
        settings.LceFactor = slidercon.Value
        lblcon.Text = slidercon.Value

        Try
            Call dobinarize()
        Catch ex As PegasusImaging.WinForms.ScanFix5.ScanFixException
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub optnone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optnone.CheckedChanged
        If optnone.Checked = True Then
            settings.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.NoBlur
        End If
    End Sub

    Private Sub optauto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optauto.CheckedChanged
        If optauto.Checked = True Then
            settings.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.SmartBlur
        End If
    End Sub

    Private Sub optgaussian_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optgaussian.CheckedChanged
        If optgaussian.Checked = True Then
            settings.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.GaussianBlur
        End If
    End Sub
End Class
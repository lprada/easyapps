/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.ScanFix5;


namespace Binarize
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Binarize : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ScanFix5.ScanFix scanFix1;
		private System.Windows.Forms.GroupBox frmprocess;
		private System.Windows.Forms.Label labelhigh;
		private System.Windows.Forms.Label lblcon;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.OpenFileDialog cd;
		private System.ComponentModel.IContainer components;

		//**Create a binarize options object **
		private PegasusImaging.WinForms.ScanFix5.BinarizeOptions binopt;
		string strCurrentDir;
		private System.Windows.Forms.TrackBar slidercon;
		private System.Windows.Forms.TrackBar sliderh;
		private System.Windows.Forms.TrackBar sliderl;
		private System.Windows.Forms.Label labellow;
		private System.Windows.Forms.RadioButton optnone;
		private System.Windows.Forms.RadioButton optauto;
		private System.Windows.Forms.RadioButton optgaussian;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MenuItem FileMenu;
		private System.Windows.Forms.MenuItem OpenMenuItem;
		private System.Windows.Forms.MenuItem ExitMenuItem;
		private System.Windows.Forms.MenuItem AboutMenuItem;
		private System.Windows.Forms.MenuItem ImagXpressMenuItem;
		private System.Windows.Forms.MenuItem ScanFixMenuItem;
		private System.Windows.Forms.Label ContrastLabel;
		private System.Windows.Forms.Label HighThresLabel;
		private System.Windows.Forms.Label LowThreshLabel;
		private System.Windows.Forms.Label InputLabel;
		private System.Windows.Forms.Label OutputLabel;
		string strImageFile;

		public Binarize()
		{
			//
			// Required for Windows Form Designer support
			//


			//***Must call UnlockControl before InitializeComponent statement
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234);
			//PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234);
			InitializeComponent();

			scanFix1 = new ScanFix();
			binopt = new PegasusImaging.WinForms.ScanFix5.BinarizeOptions();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (!(imageXView1.Image == null))
				{
					imageXView1.Image.Dispose();
					imageXView1.Image = null;
				}
			
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}

				if (!(imageXView2.Image == null))
				{
					imageXView2.Image.Dispose();
					imageXView2.Image = null;
				}

				if (!(imageXView2 == null)) 
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}
				
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}

				if (scanFix1 != null)
				{
					scanFix1.Dispose();
					scanFix1 = null;
				}
				
				if (components != null)
				{
					components.Dispose();
				}

			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.frmprocess = new System.Windows.Forms.GroupBox();
			this.optgaussian = new System.Windows.Forms.RadioButton();
			this.optauto = new System.Windows.Forms.RadioButton();
			this.optnone = new System.Windows.Forms.RadioButton();
			this.lblcon = new System.Windows.Forms.Label();
			this.labelhigh = new System.Windows.Forms.Label();
			this.labellow = new System.Windows.Forms.Label();
			this.slidercon = new System.Windows.Forms.TrackBar();
			this.sliderh = new System.Windows.Forms.TrackBar();
			this.sliderl = new System.Windows.Forms.TrackBar();
			this.ContrastLabel = new System.Windows.Forms.Label();
			this.HighThresLabel = new System.Windows.Forms.Label();
			this.LowThreshLabel = new System.Windows.Forms.Label();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.FileMenu = new System.Windows.Forms.MenuItem();
			this.OpenMenuItem = new System.Windows.Forms.MenuItem();
			this.ExitMenuItem = new System.Windows.Forms.MenuItem();
			this.AboutMenuItem = new System.Windows.Forms.MenuItem();
			this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
			this.ScanFixMenuItem = new System.Windows.Forms.MenuItem();
			this.cd = new System.Windows.Forms.OpenFileDialog();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.InputLabel = new System.Windows.Forms.Label();
			this.OutputLabel = new System.Windows.Forms.Label();
			this.frmprocess.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.slidercon)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sliderh)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sliderl)).BeginInit();
			this.SuspendLayout();
			// 
			// frmprocess
			// 
			this.frmprocess.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.optgaussian,
																					 this.optauto,
																					 this.optnone,
																					 this.lblcon,
																					 this.labelhigh,
																					 this.labellow,
																					 this.slidercon,
																					 this.sliderh,
																					 this.sliderl,
																					 this.ContrastLabel,
																					 this.HighThresLabel,
																					 this.LowThreshLabel});
			this.frmprocess.Location = new System.Drawing.Point(8, 368);
			this.frmprocess.Name = "frmprocess";
			this.frmprocess.Size = new System.Drawing.Size(768, 256);
			this.frmprocess.TabIndex = 3;
			this.frmprocess.TabStop = false;
			this.frmprocess.Text = "Binarize Settings";
			// 
			// optgaussian
			// 
			this.optgaussian.Location = new System.Drawing.Point(472, 216);
			this.optgaussian.Name = "optgaussian";
			this.optgaussian.Size = new System.Drawing.Size(96, 32);
			this.optgaussian.TabIndex = 12;
			this.optgaussian.Text = "Gaussian Blur";
			this.optgaussian.CheckedChanged += new System.EventHandler(this.optgaussian_CheckedChanged);
			// 
			// optauto
			// 
			this.optauto.Location = new System.Drawing.Point(368, 216);
			this.optauto.Name = "optauto";
			this.optauto.Size = new System.Drawing.Size(72, 32);
			this.optauto.TabIndex = 11;
			this.optauto.Text = "Auto Blur";
			this.optauto.CheckedChanged += new System.EventHandler(this.optauto_CheckedChanged);
			// 
			// optnone
			// 
			this.optnone.Checked = true;
			this.optnone.Location = new System.Drawing.Point(256, 216);
			this.optnone.Name = "optnone";
			this.optnone.Size = new System.Drawing.Size(72, 32);
			this.optnone.TabIndex = 10;
			this.optnone.TabStop = true;
			this.optnone.Text = "No Blur";
			this.optnone.CheckedChanged += new System.EventHandler(this.optnone_CheckedChanged);
			// 
			// lblcon
			// 
			this.lblcon.Location = new System.Drawing.Point(552, 168);
			this.lblcon.Name = "lblcon";
			this.lblcon.Size = new System.Drawing.Size(80, 32);
			this.lblcon.TabIndex = 8;
			// 
			// labelhigh
			// 
			this.labelhigh.Location = new System.Drawing.Point(552, 104);
			this.labelhigh.Name = "labelhigh";
			this.labelhigh.Size = new System.Drawing.Size(80, 32);
			this.labelhigh.TabIndex = 7;
			// 
			// labellow
			// 
			this.labellow.Location = new System.Drawing.Point(552, 32);
			this.labellow.Name = "labellow";
			this.labellow.Size = new System.Drawing.Size(72, 40);
			this.labellow.TabIndex = 6;
			// 
			// slidercon
			// 
			this.slidercon.Location = new System.Drawing.Point(280, 152);
			this.slidercon.Maximum = 255;
			this.slidercon.Name = "slidercon";
			this.slidercon.Size = new System.Drawing.Size(248, 45);
			this.slidercon.TabIndex = 5;
			this.slidercon.TickStyle = System.Windows.Forms.TickStyle.None;
			this.slidercon.Scroll += new System.EventHandler(this.slidercon_Scroll);
			// 
			// sliderh
			// 
			this.sliderh.Location = new System.Drawing.Point(280, 88);
			this.sliderh.Maximum = 255;
			this.sliderh.Name = "sliderh";
			this.sliderh.Size = new System.Drawing.Size(248, 45);
			this.sliderh.TabIndex = 4;
			this.sliderh.TickStyle = System.Windows.Forms.TickStyle.None;
			this.sliderh.Scroll += new System.EventHandler(this.sliderh_Scroll);
			// 
			// sliderl
			// 
			this.sliderl.Location = new System.Drawing.Point(280, 24);
			this.sliderl.Maximum = 255;
			this.sliderl.Name = "sliderl";
			this.sliderl.Size = new System.Drawing.Size(248, 45);
			this.sliderl.TabIndex = 3;
			this.sliderl.TickStyle = System.Windows.Forms.TickStyle.None;
			this.sliderl.Scroll += new System.EventHandler(this.sliderl_Scroll);
			// 
			// ContrastLabel
			// 
			this.ContrastLabel.Location = new System.Drawing.Point(136, 152);
			this.ContrastLabel.Name = "ContrastLabel";
			this.ContrastLabel.Size = new System.Drawing.Size(96, 16);
			this.ContrastLabel.TabIndex = 2;
			this.ContrastLabel.Text = "Contrast";
			// 
			// HighThresLabel
			// 
			this.HighThresLabel.Location = new System.Drawing.Point(136, 88);
			this.HighThresLabel.Name = "HighThresLabel";
			this.HighThresLabel.Size = new System.Drawing.Size(104, 32);
			this.HighThresLabel.TabIndex = 1;
			this.HighThresLabel.Text = "High Threshold";
			// 
			// LowThreshLabel
			// 
			this.LowThreshLabel.Location = new System.Drawing.Point(136, 24);
			this.LowThreshLabel.Name = "LowThreshLabel";
			this.LowThreshLabel.Size = new System.Drawing.Size(104, 16);
			this.LowThreshLabel.TabIndex = 0;
			this.LowThreshLabel.Text = "Low Threshold";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.FileMenu,
																					  this.AboutMenuItem});
			// 
			// FileMenu
			// 
			this.FileMenu.Index = 0;
			this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.OpenMenuItem,
																					 this.ExitMenuItem});
			this.FileMenu.Text = "&File";
			// 
			// OpenMenuItem
			// 
			this.OpenMenuItem.Index = 0;
			this.OpenMenuItem.Text = "&Open";
			this.OpenMenuItem.Click += new System.EventHandler(this.OpenMenuItem_Click);
			// 
			// ExitMenuItem
			// 
			this.ExitMenuItem.Index = 1;
			this.ExitMenuItem.Text = "E&xit";
			this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
			// 
			// AboutMenuItem
			// 
			this.AboutMenuItem.Index = 1;
			this.AboutMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.ImagXpressMenuItem,
																						  this.ScanFixMenuItem});
			this.AboutMenuItem.Text = "&About";
			// 
			// ImagXpressMenuItem
			// 
			this.ImagXpressMenuItem.Index = 0;
			this.ImagXpressMenuItem.Text = "Imag&Xpress";
			this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
			// 
			// ScanFixMenuItem
			// 
			this.ScanFixMenuItem.Index = 1;
			this.ScanFixMenuItem.Text = "Scan&Fix";
			this.ScanFixMenuItem.Click += new System.EventHandler(this.ScanFixMenuItem_Click);
			// 
			// imageXView1
			// 
			this.imageXView1.AutoScroll = true;
			this.imageXView1.Location = new System.Drawing.Point(8, 120);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(376, 232);
			this.imageXView1.TabIndex = 4;
			// 
			// imageXView2
			// 
			this.imageXView2.AutoScroll = true;
			this.imageXView2.Location = new System.Drawing.Point(408, 120);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(368, 232);
			this.imageXView2.TabIndex = 5;
			// 
			// lstInfo
			// 
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "Using the Binarize method on the ScanFix control.",
														 "",
														 "Please Note: The sample starts up with the defaults for the AutoBinarize method. " +
														 "",
														 "The AutoBinarize method automatically sets the various parameters based on the co" +
														 "lordepth of the input image.",
														 "It is also possible to adjust the various settings via the sliders and then choos" +
														 "e the Process button."});
			this.lstInfo.Location = new System.Drawing.Point(8, 8);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(768, 82);
			this.lstInfo.TabIndex = 6;
			// 
			// InputLabel
			// 
			this.InputLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.InputLabel.Location = new System.Drawing.Point(128, 104);
			this.InputLabel.Name = "InputLabel";
			this.InputLabel.Size = new System.Drawing.Size(152, 16);
			this.InputLabel.TabIndex = 7;
			this.InputLabel.Text = "Input Image";
			this.InputLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// OutputLabel
			// 
			this.OutputLabel.Font = new System.Drawing.Font("Tahoma", 9.75F, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.OutputLabel.Location = new System.Drawing.Point(520, 104);
			this.OutputLabel.Name = "OutputLabel";
			this.OutputLabel.Size = new System.Drawing.Size(152, 16);
			this.OutputLabel.TabIndex = 8;
			this.OutputLabel.Text = "Output Image";
			this.OutputLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Binarize
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(784, 635);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.OutputLabel,
																		  this.InputLabel,
																		  this.lstInfo,
																		  this.imageXView2,
																		  this.imageXView1,
																		  this.frmprocess});
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.Name = "Binarize";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Binarize";
			this.Load += new System.EventHandler(this.Binarize_Load);
			this.frmprocess.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.slidercon)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sliderh)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sliderl)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.Run(new Binarize());
		}

		private void ImagXpressMenuItem_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void ScanFixMenuItem_Click(object sender, System.EventArgs e)
		{
			scanFix1.AboutBox();
		}

		private void ExitMenuItem_Click(object sender, System.EventArgs e)
		{
			Application.Exit();		
		}

		private void Binarize_Load(object sender, System.EventArgs e)
		{


			//***Must call UnlockRuntime when application first begins
			//imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);
			//scanFix1.License.UnlockRuntime(1234, 1234, 1234, 1234);

			try
			{
				imageXView1.AutoScroll = true;
				imageXView2.AutoScroll = true;

				imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
				imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;

				strCurrentDir = System.IO.Directory.GetCurrentDirectory();
				strImageFile = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\binarize.jpg");

				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile);
				
				//**pass the image data to ScanFix for processing    
				scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
				//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
				if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
				{    
					scanFix1.AutoBinarize();
				}

			}
			catch(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			//**These are the defaults if using the AutoBinarize method for image greater than _
			// 1 bit per pixel
			sliderh.Value = 190;
			sliderl.Value = 170;
			slidercon.Value = 0;

			binopt.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.NoBlur;

			labellow.Text = Convert.ToString(sliderl.Value);
			labelhigh.Text = Convert.ToString(sliderh.Value);
			lblcon.Text = Convert.ToString(slidercon.Value);
			binopt.LceFactor = slidercon.Value;
			binopt.HighThreshold = sliderh.Value;
			binopt.LowThreshold = sliderl.Value;

			try
			{
				dobinarize(sender, e);
			}
			catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void OpenMenuItem_Click(object sender, System.EventArgs e)
		{
			try
			{
				cd.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.TIF)|*.BMP;*.JPG;*.GIF;*.TIF|All files (*.*)|*.*";
				cd.FilterIndex = 0;
				strCurrentDir = System.IO.Directory.GetCurrentDirectory();
				strImageFile = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");
				cd.InitialDirectory = strImageFile;

				cd.ShowDialog();

				if (cd.FileName != "")
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(cd.FileName);
					//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not	
				    if(imageXView1.Image.ImageXData.BitsPerPixel != 1)
					{
						//**pass the image data to ScanFix for processing
						scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
						scanFix1.AutoBinarize();
					}
					else
					{
						//scanFix1.FromHdib(imageXView1.Image.ToHdib(false));

						MessageBox.Show("Please load in an image that is NOT 1-bit per pixel.", "Error",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						
						if (imageXView1.Image != null)
						{
							imageXView1.Image.Dispose();
							imageXView1.Image = null;
						}
						if (imageXView2.Image != null)
						{
							imageXView2.Image.Dispose();
							imageXView2.Image = null;
						}
					}
				}
				else
				{
					if (imageXView1.Image != null)
					{
						imageXView1.Image.Dispose();
						imageXView1.Image = null;
					}
					if (imageXView2.Image != null)
					{
						imageXView2.Image.Dispose();
						imageXView2.Image = null;
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void dobinarize(object sender, System.EventArgs e)
		{
			try
			{
				//*** perform a check to see if image is not 1 bpp and use SF AutoBinarize if not
				if( imageXView1.Image.ImageXData.BitsPerPixel != 1)
				{
					//**pass the image data to ScanFix for processing
					scanFix1.FromHdib(imageXView1.Image.ToHdib(false));
				}
				
				
				scanFix1.Binarize(binopt);
				
				//*** Code here to display the image
				imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void sliderl_Scroll(object sender, System.EventArgs e)
		{
			if( sliderh.Value == 0)
			{
				sliderl.Value = 0;
			}
			else
			{
				if (sliderl.Value >= sliderh.Value)
				{
					sliderl.Value = (sliderh.Value - 1);
				}
			}

			  binopt.LowThreshold = sliderl.Value;
			  labellow.Text = Convert.ToString(sliderl.Value);

			try
			{
				dobinarize(sender, e);
			}
			catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void sliderh_Scroll(object sender, System.EventArgs e)
		{
			if( sliderh.Value < sliderl.Value)
			{
				sliderh.Value = (sliderl.Value + 1);
			}
			binopt.HighThreshold = sliderh.Value;
			labelhigh.Text = Convert.ToString(sliderh.Value);

			try
			{
				dobinarize(sender, e);
			}
			catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void slidercon_Scroll(object sender, System.EventArgs e)
		{
			binopt.LceFactor = slidercon.Value;
			lblcon.Text = Convert.ToString(slidercon.Value);

			try
			{
				dobinarize(sender, e);
			}
			catch( PegasusImaging.WinForms.ScanFix5.ScanFixException ex )
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void optnone_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optnone.Checked == true)
			{
				binopt.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.NoBlur;
			}
		}

		private void optauto_CheckedChanged(object sender, System.EventArgs e)
		{
			if( optauto.Checked == true)
			{
				binopt.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.SmartBlur;
				}
		}

		private void optgaussian_CheckedChanged(object sender, System.EventArgs e)
		{
			if(optgaussian.Checked == true)
			{
				binopt.PreBlurType = PegasusImaging.WinForms.ScanFix5.BinarizeBlur.GaussianBlur;
			}
		}
	}
}

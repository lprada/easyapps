
'****************************************************************
' * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida.*
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/

Imports PegasusImaging.WinForms.ImagXpress9
Imports PegasusImaging.WinForms.ThumbnailXpress2
Imports System.Windows.Forms
Imports System.IO

Public Class MainForm
    Inherits System.Windows.Forms.Form

    Private filteritem As PegasusImaging.WinForms.ThumbnailXpress2.Filter
    Private point As System.Drawing.Point
    Private tdata As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem
    Private dragtarget As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem
    Private array As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem()
    Private external1 As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem


    Private cd As System.Windows.Forms.ColorDialog
    Private a As System.Int32

    'file I/O variables
    Private strCurrentDir As System.String
    Private strImageFile As System.String

    Private dragindex As System.Int32

    'Private dragsource As System.Boolean


    Private filteritemCreated As Boolean
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents ckbOleDrag As System.Windows.Forms.CheckBox
    Friend WithEvents ckbDrillDown As System.Windows.Forms.CheckBox
    Friend WithEvents ckbTNShift As System.Windows.Forms.CheckBox
    Friend WithEvents ckbTNXDrag As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents btScroll As System.Windows.Forms.Button
    Friend WithEvents gbScrolling As System.Windows.Forms.GroupBox
    Friend WithEvents cbbScrollTo As System.Windows.Forms.ComboBox
    Friend WithEvents tbScrollAmount As System.Windows.Forms.TextBox
    Friend WithEvents gbScrollInc As System.Windows.Forms.GroupBox
    Friend WithEvents btLIncrement As System.Windows.Forms.Button
    Friend WithEvents btLDecrement As System.Windows.Forms.Button
    Friend WithEvents btSmDecrement As System.Windows.Forms.Button
    Friend WithEvents tbSmIncrement As System.Windows.Forms.Button
    Friend WithEvents ThumbnailXpress3 As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress
    Friend WithEvents tbFileNamePattern As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label

    Private indexOfItemUnderMouseToDrag As System.Int32

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        '***must call these UnlockControl functions 
        'PegasusImaging.WinForms.ImagXpress9.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        'PegasusImaging.WinForms.ThumbnailXpress2.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then


            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ThumbnailXpress2 As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnufile As System.Windows.Forms.MenuItem
    Friend WithEvents mnudir As System.Windows.Forms.MenuItem
    Friend WithEvents mnusingle As System.Windows.Forms.MenuItem
    Friend WithEvents mnuexit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuhelp As System.Windows.Forms.MenuItem
    Friend WithEvents mnuabout As System.Windows.Forms.MenuItem
    Friend WithEvents chkexpand As System.Windows.Forms.CheckBox
    Friend WithEvents tabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Filters As System.Windows.Forms.TabPage
    Friend WithEvents txtsize As System.Windows.Forms.TextBox
    Friend WithEvents lblsize As System.Windows.Forms.Label
    Friend WithEvents dateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents chksub As System.Windows.Forms.CheckBox
    Friend WithEvents chkparent As System.Windows.Forms.CheckBox
    Friend WithEvents chkenable As System.Windows.Forms.CheckBox
    Friend WithEvents chkhid As System.Windows.Forms.CheckBox
    Friend WithEvents chksen As System.Windows.Forms.CheckBox
    Friend WithEvents chkdir As System.Windows.Forms.CheckBox
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents lblcompare As System.Windows.Forms.Label
    Friend WithEvents lbltype As System.Windows.Forms.Label
    Friend WithEvents cmbchoice As System.Windows.Forms.ComboBox
    Friend WithEvents cmbfiles As System.Windows.Forms.ComboBox
    Friend WithEvents cmbcompare As System.Windows.Forms.ComboBox
    Friend WithEvents tabselect As System.Windows.Forms.TabPage
    Friend WithEvents lblselect As System.Windows.Forms.Label
    Friend WithEvents cmbselect As System.Windows.Forms.ComboBox
    Friend WithEvents cells As System.Windows.Forms.TabPage
    Friend WithEvents lblCellWidth As System.Windows.Forms.Label
    Friend WithEvents lblcw As System.Windows.Forms.Label
    Friend WithEvents lblCVS As System.Windows.Forms.Label
    Friend WithEvents lblv As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents vcw As System.Windows.Forms.HScrollBar
    Friend WithEvents vbh As System.Windows.Forms.HScrollBar
    Friend WithEvents hbh As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCHS As System.Windows.Forms.Label
    Friend WithEvents lblheight As System.Windows.Forms.Label
    Friend WithEvents hbheight As System.Windows.Forms.HScrollBar
    Friend WithEvents lblh As System.Windows.Forms.Label
    Friend WithEvents lblw As System.Windows.Forms.Label
    Friend WithEvents cmdcolor As System.Windows.Forms.Button
    Friend WithEvents cmdset As System.Windows.Forms.Button
    Friend WithEvents lblbwidth As System.Windows.Forms.Label
    Friend WithEvents hbwidth As System.Windows.Forms.HScrollBar
    Friend WithEvents errdesc As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents sampdesc As System.Windows.Forms.RichTextBox
    Friend WithEvents cmdremove As System.Windows.Forms.Button
    Friend WithEvents lstselect As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdsel As System.Windows.Forms.Button
    Friend WithEvents cmdclear As System.Windows.Forms.Button
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents tabThread As System.Windows.Forms.TabPage
    Friend WithEvents maxthreads As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblThreads As System.Windows.Forms.Label
    Friend WithEvents grpThreads As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSThread As System.Windows.Forms.TextBox
    Friend WithEvents lblHung As System.Windows.Forms.Label
    Friend WithEvents txtHungT As System.Windows.Forms.TextBox
    Friend WithEvents cmdSetThreads As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents chkPreserve As System.Windows.Forms.CheckBox
    Friend WithEvents chkRaw As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.ThumbnailXpress2 = New PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnufile = New System.Windows.Forms.MenuItem
        Me.mnudir = New System.Windows.Forms.MenuItem
        Me.mnusingle = New System.Windows.Forms.MenuItem
        Me.mnuexit = New System.Windows.Forms.MenuItem
        Me.mnuhelp = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuabout = New System.Windows.Forms.MenuItem
        Me.chkexpand = New System.Windows.Forms.CheckBox
        Me.tabControl1 = New System.Windows.Forms.TabControl
        Me.Filters = New System.Windows.Forms.TabPage
        Me.tbFileNamePattern = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtsize = New System.Windows.Forms.TextBox
        Me.lblsize = New System.Windows.Forms.Label
        Me.dateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.chksub = New System.Windows.Forms.CheckBox
        Me.chkparent = New System.Windows.Forms.CheckBox
        Me.chkenable = New System.Windows.Forms.CheckBox
        Me.chkhid = New System.Windows.Forms.CheckBox
        Me.chksen = New System.Windows.Forms.CheckBox
        Me.chkdir = New System.Windows.Forms.CheckBox
        Me.label4 = New System.Windows.Forms.Label
        Me.lblcompare = New System.Windows.Forms.Label
        Me.lbltype = New System.Windows.Forms.Label
        Me.cmbchoice = New System.Windows.Forms.ComboBox
        Me.cmbfiles = New System.Windows.Forms.ComboBox
        Me.cmbcompare = New System.Windows.Forms.ComboBox
        Me.tabThread = New System.Windows.Forms.TabPage
        Me.grpThreads = New System.Windows.Forms.GroupBox
        Me.cmdSetThreads = New System.Windows.Forms.Button
        Me.txtHungT = New System.Windows.Forms.TextBox
        Me.lblHung = New System.Windows.Forms.Label
        Me.txtSThread = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblThreads = New System.Windows.Forms.Label
        Me.maxthreads = New System.Windows.Forms.NumericUpDown
        Me.tabselect = New System.Windows.Forms.TabPage
        Me.lblselect = New System.Windows.Forms.Label
        Me.cmbselect = New System.Windows.Forms.ComboBox
        Me.cells = New System.Windows.Forms.TabPage
        Me.lblCellWidth = New System.Windows.Forms.Label
        Me.lblcw = New System.Windows.Forms.Label
        Me.lblCVS = New System.Windows.Forms.Label
        Me.lblv = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.vcw = New System.Windows.Forms.HScrollBar
        Me.vbh = New System.Windows.Forms.HScrollBar
        Me.hbh = New System.Windows.Forms.HScrollBar
        Me.lblCHS = New System.Windows.Forms.Label
        Me.lblheight = New System.Windows.Forms.Label
        Me.hbheight = New System.Windows.Forms.HScrollBar
        Me.lblh = New System.Windows.Forms.Label
        Me.lblw = New System.Windows.Forms.Label
        Me.cmdcolor = New System.Windows.Forms.Button
        Me.cmdset = New System.Windows.Forms.Button
        Me.lblbwidth = New System.Windows.Forms.Label
        Me.hbwidth = New System.Windows.Forms.HScrollBar
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.gbScrollInc = New System.Windows.Forms.GroupBox
        Me.btLIncrement = New System.Windows.Forms.Button
        Me.btLDecrement = New System.Windows.Forms.Button
        Me.btSmDecrement = New System.Windows.Forms.Button
        Me.tbSmIncrement = New System.Windows.Forms.Button
        Me.gbScrolling = New System.Windows.Forms.GroupBox
        Me.btScroll = New System.Windows.Forms.Button
        Me.cbbScrollTo = New System.Windows.Forms.ComboBox
        Me.tbScrollAmount = New System.Windows.Forms.TextBox
        Me.errdesc = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.sampdesc = New System.Windows.Forms.RichTextBox
        Me.cmdremove = New System.Windows.Forms.Button
        Me.lstselect = New System.Windows.Forms.CheckedListBox
        Me.cmdsel = New System.Windows.Forms.Button
        Me.cmdclear = New System.Windows.Forms.Button
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.chkPreserve = New System.Windows.Forms.CheckBox
        Me.chkRaw = New System.Windows.Forms.CheckBox
        Me.ckbOleDrag = New System.Windows.Forms.CheckBox
        Me.ckbDrillDown = New System.Windows.Forms.CheckBox
        Me.ckbTNShift = New System.Windows.Forms.CheckBox
        Me.ckbTNXDrag = New System.Windows.Forms.CheckBox
        Me.ThumbnailXpress3 = New PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress
        Me.tabControl1.SuspendLayout()
        Me.Filters.SuspendLayout()
        Me.tabThread.SuspendLayout()
        Me.grpThreads.SuspendLayout()
        CType(Me.maxthreads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabselect.SuspendLayout()
        Me.cells.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.gbScrollInc.SuspendLayout()
        Me.gbScrolling.SuspendLayout()
        Me.SuspendLayout()
        '
        'ThumbnailXpress2
        '
        Me.ThumbnailXpress2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ThumbnailXpress2.BottomMargin = 5
        Me.ThumbnailXpress2.CameraRaw = False
        Me.ThumbnailXpress2.CellBorderColor = System.Drawing.Color.Blue
        Me.ThumbnailXpress2.CellBorderWidth = 1
        Me.ThumbnailXpress2.CellHeight = 80
        Me.ThumbnailXpress2.CellHorizontalSpacing = 5
        Me.ThumbnailXpress2.CellSpacingColor = System.Drawing.Color.White
        Me.ThumbnailXpress2.CellVerticalSpacing = 5
        Me.ThumbnailXpress2.CellWidth = 80
        Me.ThumbnailXpress2.DblClickDirectoryDrillDown = True
        Me.ThumbnailXpress2.DescriptorAlignment = CType((PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignCenter Or PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignBottom), PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments)
        Me.ThumbnailXpress2.DescriptorDisplayMethod = PegasusImaging.WinForms.ThumbnailXpress2.DescriptorDisplayMethods.[Default]
        Me.ThumbnailXpress2.EnableAsDragSourceForExternalDragDrop = True
        Me.ThumbnailXpress2.EnableAsDropTargetForExternalDragDrop = True
        Me.ThumbnailXpress2.ErrorAction = PegasusImaging.WinForms.ThumbnailXpress2.ErrorAction.UseErrorIcon
        Me.ThumbnailXpress2.FtpPassword = ""
        Me.ThumbnailXpress2.FtpUserName = ""
        Me.ThumbnailXpress2.InterComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress2.IntraComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress2.LeftMargin = 5
        Me.ThumbnailXpress2.Location = New System.Drawing.Point(304, 152)
        Me.ThumbnailXpress2.MaximumThumbnailBitDepth = 24
        Me.ThumbnailXpress2.Name = "ThumbnailXpress2"
        Me.ThumbnailXpress2.PreserveBlack = False
        Me.ThumbnailXpress2.ProxyServer = ""
        Me.ThumbnailXpress2.RightMargin = 5
        Me.ThumbnailXpress2.ScrollDirection = PegasusImaging.WinForms.ThumbnailXpress2.ScrollDirection.Vertical
        Me.ThumbnailXpress2.SelectBackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.ThumbnailXpress2.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.MultiExtended
        Me.ThumbnailXpress2.ShowHourglass = True
        Me.ThumbnailXpress2.ShowImagePlaceholders = False
        Me.ThumbnailXpress2.Size = New System.Drawing.Size(520, 352)
        Me.ThumbnailXpress2.TabIndex = 0
        Me.ThumbnailXpress2.TextBackColor = System.Drawing.Color.White
        Me.ThumbnailXpress2.TopMargin = 5
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnufile, Me.mnuhelp})
        '
        'mnufile
        '
        Me.mnufile.Index = 0
        Me.mnufile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnudir, Me.mnusingle, Me.mnuexit})
        Me.mnufile.Text = "File"
        '
        'mnudir
        '
        Me.mnudir.Index = 0
        Me.mnudir.Text = "Open Directory of Images"
        '
        'mnusingle
        '
        Me.mnusingle.Index = 1
        Me.mnusingle.Text = "Open Image"
        '
        'mnuexit
        '
        Me.mnuexit.Index = 2
        Me.mnuexit.Text = "Exit"
        '
        'mnuhelp
        '
        Me.mnuhelp.Index = 1
        Me.mnuhelp.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.mnuabout})
        Me.mnuhelp.Text = "Help"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Imag&Xpress"
        '
        'mnuabout
        '
        Me.mnuabout.Index = 1
        Me.mnuabout.Text = "&ThumbnailXpress"
        '
        'chkexpand
        '
        Me.chkexpand.Location = New System.Drawing.Point(8, 8)
        Me.chkexpand.Name = "chkexpand"
        Me.chkexpand.Size = New System.Drawing.Size(120, 16)
        Me.chkexpand.TabIndex = 33
        Me.chkexpand.Text = "Expand Multipage"
        '
        'tabControl1
        '
        Me.tabControl1.Controls.Add(Me.Filters)
        Me.tabControl1.Controls.Add(Me.tabThread)
        Me.tabControl1.Controls.Add(Me.tabselect)
        Me.tabControl1.Controls.Add(Me.cells)
        Me.tabControl1.Controls.Add(Me.TabPage1)
        Me.tabControl1.Location = New System.Drawing.Point(0, 88)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(296, 416)
        Me.tabControl1.TabIndex = 32
        '
        'Filters
        '
        Me.Filters.Controls.Add(Me.tbFileNamePattern)
        Me.Filters.Controls.Add(Me.Label2)
        Me.Filters.Controls.Add(Me.txtsize)
        Me.Filters.Controls.Add(Me.lblsize)
        Me.Filters.Controls.Add(Me.dateTimePicker1)
        Me.Filters.Controls.Add(Me.chksub)
        Me.Filters.Controls.Add(Me.chkparent)
        Me.Filters.Controls.Add(Me.chkenable)
        Me.Filters.Controls.Add(Me.chkhid)
        Me.Filters.Controls.Add(Me.chksen)
        Me.Filters.Controls.Add(Me.chkdir)
        Me.Filters.Controls.Add(Me.label4)
        Me.Filters.Controls.Add(Me.lblcompare)
        Me.Filters.Controls.Add(Me.lbltype)
        Me.Filters.Controls.Add(Me.cmbchoice)
        Me.Filters.Controls.Add(Me.cmbfiles)
        Me.Filters.Controls.Add(Me.cmbcompare)
        Me.Filters.Location = New System.Drawing.Point(4, 22)
        Me.Filters.Name = "Filters"
        Me.Filters.Size = New System.Drawing.Size(288, 390)
        Me.Filters.TabIndex = 1
        Me.Filters.Text = "Filters"
        Me.Filters.UseVisualStyleBackColor = True
        '
        'tbFileNamePattern
        '
        Me.tbFileNamePattern.Location = New System.Drawing.Point(118, 178)
        Me.tbFileNamePattern.Name = "tbFileNamePattern"
        Me.tbFileNamePattern.Size = New System.Drawing.Size(152, 20)
        Me.tbFileNamePattern.TabIndex = 18
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 182)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "File Name Pattern"
        '
        'txtsize
        '
        Me.txtsize.Enabled = False
        Me.txtsize.Location = New System.Drawing.Point(96, 80)
        Me.txtsize.Name = "txtsize"
        Me.txtsize.Size = New System.Drawing.Size(144, 20)
        Me.txtsize.TabIndex = 16
        Me.txtsize.Visible = False
        '
        'lblsize
        '
        Me.lblsize.Location = New System.Drawing.Point(8, 80)
        Me.lblsize.Name = "lblsize"
        Me.lblsize.Size = New System.Drawing.Size(72, 16)
        Me.lblsize.TabIndex = 15
        Me.lblsize.Text = "File Size"
        '
        'dateTimePicker1
        '
        Me.dateTimePicker1.Enabled = False
        Me.dateTimePicker1.Location = New System.Drawing.Point(48, 144)
        Me.dateTimePicker1.Name = "dateTimePicker1"
        Me.dateTimePicker1.Size = New System.Drawing.Size(208, 20)
        Me.dateTimePicker1.TabIndex = 14
        '
        'chksub
        '
        Me.chksub.Enabled = False
        Me.chksub.Location = New System.Drawing.Point(24, 335)
        Me.chksub.Name = "chksub"
        Me.chksub.Size = New System.Drawing.Size(128, 16)
        Me.chksub.TabIndex = 13
        Me.chksub.Text = "Include SubDir"
        '
        'chkparent
        '
        Me.chkparent.Enabled = False
        Me.chkparent.Location = New System.Drawing.Point(24, 311)
        Me.chkparent.Name = "chkparent"
        Me.chkparent.Size = New System.Drawing.Size(136, 16)
        Me.chkparent.TabIndex = 12
        Me.chkparent.Text = "Include Parent Dir"
        '
        'chkenable
        '
        Me.chkenable.Checked = True
        Me.chkenable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkenable.Location = New System.Drawing.Point(24, 287)
        Me.chkenable.Name = "chkenable"
        Me.chkenable.Size = New System.Drawing.Size(136, 16)
        Me.chkenable.TabIndex = 11
        Me.chkenable.Text = "Enabled"
        '
        'chkhid
        '
        Me.chkhid.Enabled = False
        Me.chkhid.Location = New System.Drawing.Point(24, 263)
        Me.chkhid.Name = "chkhid"
        Me.chkhid.Size = New System.Drawing.Size(144, 16)
        Me.chkhid.TabIndex = 10
        Me.chkhid.Text = "Include Hidden"
        '
        'chksen
        '
        Me.chksen.Enabled = False
        Me.chksen.Location = New System.Drawing.Point(24, 239)
        Me.chksen.Name = "chksen"
        Me.chksen.Size = New System.Drawing.Size(152, 16)
        Me.chksen.TabIndex = 9
        Me.chksen.Text = "Match is Case Sensitive"
        '
        'chkdir
        '
        Me.chkdir.Enabled = False
        Me.chkdir.Location = New System.Drawing.Point(24, 215)
        Me.chkdir.Name = "chkdir"
        Me.chkdir.Size = New System.Drawing.Size(160, 16)
        Me.chkdir.TabIndex = 8
        Me.chkdir.Text = "Match Applies to Directory"
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(8, 112)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(72, 16)
        Me.label4.TabIndex = 7
        Me.label4.Text = "File Filters:"
        '
        'lblcompare
        '
        Me.lblcompare.Location = New System.Drawing.Point(8, 48)
        Me.lblcompare.Name = "lblcompare"
        Me.lblcompare.Size = New System.Drawing.Size(72, 24)
        Me.lblcompare.TabIndex = 6
        Me.lblcompare.Text = "Compare Type:"
        '
        'lbltype
        '
        Me.lbltype.Location = New System.Drawing.Point(8, 16)
        Me.lbltype.Name = "lbltype"
        Me.lbltype.Size = New System.Drawing.Size(80, 16)
        Me.lbltype.TabIndex = 5
        Me.lbltype.Text = "Filter Type:"
        '
        'cmbchoice
        '
        Me.cmbchoice.Items.AddRange(New Object() {"File Name Match", "File Attributes and Directory", "File Creation Date", "File Modification Date", "FileSize", "ImagXpress Supported File"})
        Me.cmbchoice.Location = New System.Drawing.Point(96, 16)
        Me.cmbchoice.Name = "cmbchoice"
        Me.cmbchoice.Size = New System.Drawing.Size(144, 21)
        Me.cmbchoice.TabIndex = 4
        '
        'cmbfiles
        '
        Me.cmbfiles.Enabled = False
        Me.cmbfiles.Items.AddRange(New Object() {"All Files(*.*)", "Windows Bitmap (*.BMP)", "|CALS (*.CAL)", "Windows Device Independent Bitmap(*.DIB)", "MO:DCA (*.DCA & *.MOD)", "Zsoft Multiple Page (*.DCX)", "CompuServe GIF (*.GIF)", "JPEG 2000 (*.JP2)", "JPEG LS (*.JLS)", "JFIF Compliant JPEG (*.JPG)", "Lossless JPEG (*.LJP)", "Portable Bitmap (*.PBM)", "Zsoft PaintBrush (*.PCX)", "Portable Graymap (*.PGM)", "Pegasus PIC(*.PIC)", "Portable Network Graphics (*.PNG)", "Portable Pixmap (*.PPM)", "TIFF (*.TIFF)|", "Truevision TARGA(*.TGA)", "WSQ Fingerprint File (*.WSQ)", "JBIG2 File (*.JB2)"})
        Me.cmbfiles.Location = New System.Drawing.Point(96, 112)
        Me.cmbfiles.Name = "cmbfiles"
        Me.cmbfiles.Size = New System.Drawing.Size(152, 21)
        Me.cmbfiles.TabIndex = 3
        Me.cmbfiles.Text = "All Files(*.*)"
        '
        'cmbcompare
        '
        Me.cmbcompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbcompare.Enabled = False
        Me.cmbcompare.Items.AddRange(New Object() {"LessThan", "LessThan or Equal", "Equal", "GreaterThan or Equal", "GreaterThan"})
        Me.cmbcompare.Location = New System.Drawing.Point(96, 48)
        Me.cmbcompare.Name = "cmbcompare"
        Me.cmbcompare.Size = New System.Drawing.Size(144, 21)
        Me.cmbcompare.TabIndex = 0
        '
        'tabThread
        '
        Me.tabThread.Controls.Add(Me.grpThreads)
        Me.tabThread.Controls.Add(Me.lblThreads)
        Me.tabThread.Controls.Add(Me.maxthreads)
        Me.tabThread.Location = New System.Drawing.Point(4, 22)
        Me.tabThread.Name = "tabThread"
        Me.tabThread.Size = New System.Drawing.Size(288, 390)
        Me.tabThread.TabIndex = 2
        Me.tabThread.Text = "Thread Processing"
        Me.tabThread.UseVisualStyleBackColor = True
        Me.tabThread.Visible = False
        '
        'grpThreads
        '
        Me.grpThreads.Controls.Add(Me.cmdSetThreads)
        Me.grpThreads.Controls.Add(Me.txtHungT)
        Me.grpThreads.Controls.Add(Me.lblHung)
        Me.grpThreads.Controls.Add(Me.txtSThread)
        Me.grpThreads.Controls.Add(Me.Label1)
        Me.grpThreads.Location = New System.Drawing.Point(16, 72)
        Me.grpThreads.Name = "grpThreads"
        Me.grpThreads.Size = New System.Drawing.Size(248, 208)
        Me.grpThreads.TabIndex = 2
        Me.grpThreads.TabStop = False
        Me.grpThreads.Text = "Thread Time Thresholds"
        '
        'cmdSetThreads
        '
        Me.cmdSetThreads.Location = New System.Drawing.Point(128, 24)
        Me.cmdSetThreads.Name = "cmdSetThreads"
        Me.cmdSetThreads.Size = New System.Drawing.Size(96, 32)
        Me.cmdSetThreads.TabIndex = 4
        Me.cmdSetThreads.Text = "Set Thread Values"
        '
        'txtHungT
        '
        Me.txtHungT.Location = New System.Drawing.Point(128, 120)
        Me.txtHungT.Name = "txtHungT"
        Me.txtHungT.Size = New System.Drawing.Size(96, 20)
        Me.txtHungT.TabIndex = 3
        '
        'lblHung
        '
        Me.lblHung.Location = New System.Drawing.Point(8, 120)
        Me.lblHung.Name = "lblHung"
        Me.lblHung.Size = New System.Drawing.Size(96, 32)
        Me.lblHung.TabIndex = 2
        Me.lblHung.Text = "Hung Thread Threshold:"
        '
        'txtSThread
        '
        Me.txtSThread.Location = New System.Drawing.Point(128, 72)
        Me.txtSThread.Name = "txtSThread"
        Me.txtSThread.Size = New System.Drawing.Size(96, 20)
        Me.txtSThread.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 32)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Start Thread Threshold:"
        '
        'lblThreads
        '
        Me.lblThreads.Location = New System.Drawing.Point(32, 24)
        Me.lblThreads.Name = "lblThreads"
        Me.lblThreads.Size = New System.Drawing.Size(112, 16)
        Me.lblThreads.TabIndex = 1
        Me.lblThreads.Text = "Max Threads:"
        '
        'maxthreads
        '
        Me.maxthreads.Location = New System.Drawing.Point(168, 24)
        Me.maxthreads.Maximum = New Decimal(New Integer() {33, 0, 0, 0})
        Me.maxthreads.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.maxthreads.Name = "maxthreads"
        Me.maxthreads.Size = New System.Drawing.Size(40, 20)
        Me.maxthreads.TabIndex = 0
        Me.maxthreads.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'tabselect
        '
        Me.tabselect.Controls.Add(Me.lblselect)
        Me.tabselect.Controls.Add(Me.cmbselect)
        Me.tabselect.Location = New System.Drawing.Point(4, 22)
        Me.tabselect.Name = "tabselect"
        Me.tabselect.Size = New System.Drawing.Size(288, 390)
        Me.tabselect.TabIndex = 3
        Me.tabselect.Text = "SelectionMode"
        Me.tabselect.UseVisualStyleBackColor = True
        Me.tabselect.Visible = False
        '
        'lblselect
        '
        Me.lblselect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblselect.Location = New System.Drawing.Point(72, 48)
        Me.lblselect.Name = "lblselect"
        Me.lblselect.Size = New System.Drawing.Size(128, 32)
        Me.lblselect.TabIndex = 1
        Me.lblselect.Text = "SelectionMode Options"
        Me.lblselect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmbselect
        '
        Me.cmbselect.Items.AddRange(New Object() {"MultiExtended", "MultiSimple", "None", "Single"})
        Me.cmbselect.Location = New System.Drawing.Point(24, 96)
        Me.cmbselect.Name = "cmbselect"
        Me.cmbselect.Size = New System.Drawing.Size(216, 21)
        Me.cmbselect.TabIndex = 0
        '
        'cells
        '
        Me.cells.Controls.Add(Me.lblCellWidth)
        Me.cells.Controls.Add(Me.lblcw)
        Me.cells.Controls.Add(Me.lblCVS)
        Me.cells.Controls.Add(Me.lblv)
        Me.cells.Controls.Add(Me.label3)
        Me.cells.Controls.Add(Me.vcw)
        Me.cells.Controls.Add(Me.vbh)
        Me.cells.Controls.Add(Me.hbh)
        Me.cells.Controls.Add(Me.lblCHS)
        Me.cells.Controls.Add(Me.lblheight)
        Me.cells.Controls.Add(Me.hbheight)
        Me.cells.Controls.Add(Me.lblh)
        Me.cells.Controls.Add(Me.lblw)
        Me.cells.Controls.Add(Me.cmdcolor)
        Me.cells.Controls.Add(Me.cmdset)
        Me.cells.Controls.Add(Me.lblbwidth)
        Me.cells.Controls.Add(Me.hbwidth)
        Me.cells.Location = New System.Drawing.Point(4, 22)
        Me.cells.Name = "cells"
        Me.cells.Size = New System.Drawing.Size(288, 390)
        Me.cells.TabIndex = 0
        Me.cells.Text = "Thumbnail Attributes"
        Me.cells.UseVisualStyleBackColor = True
        Me.cells.Visible = False
        '
        'lblCellWidth
        '
        Me.lblCellWidth.Location = New System.Drawing.Point(200, 272)
        Me.lblCellWidth.Name = "lblCellWidth"
        Me.lblCellWidth.Size = New System.Drawing.Size(56, 16)
        Me.lblCellWidth.TabIndex = 17
        '
        'lblcw
        '
        Me.lblcw.Location = New System.Drawing.Point(24, 248)
        Me.lblcw.Name = "lblcw"
        Me.lblcw.Size = New System.Drawing.Size(144, 16)
        Me.lblcw.TabIndex = 16
        Me.lblcw.Text = "Cell Width"
        '
        'lblCVS
        '
        Me.lblCVS.Location = New System.Drawing.Point(200, 224)
        Me.lblCVS.Name = "lblCVS"
        Me.lblCVS.Size = New System.Drawing.Size(56, 16)
        Me.lblCVS.TabIndex = 15
        '
        'lblv
        '
        Me.lblv.Location = New System.Drawing.Point(24, 200)
        Me.lblv.Name = "lblv"
        Me.lblv.Size = New System.Drawing.Size(160, 16)
        Me.lblv.TabIndex = 14
        Me.lblv.Text = "Cell Vertical Spacing"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(24, 152)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(128, 16)
        Me.label3.TabIndex = 13
        Me.label3.Text = "Cell Horizontal Spacing"
        '
        'vcw
        '
        Me.vcw.Location = New System.Drawing.Point(24, 272)
        Me.vcw.Minimum = 32
        Me.vcw.Name = "vcw"
        Me.vcw.Size = New System.Drawing.Size(160, 16)
        Me.vcw.TabIndex = 12
        Me.vcw.Value = 32
        '
        'vbh
        '
        Me.vbh.Location = New System.Drawing.Point(24, 224)
        Me.vbh.Maximum = 90
        Me.vbh.Minimum = 1
        Me.vbh.Name = "vbh"
        Me.vbh.Size = New System.Drawing.Size(160, 16)
        Me.vbh.TabIndex = 11
        Me.vbh.Value = 1
        '
        'hbh
        '
        Me.hbh.Location = New System.Drawing.Point(24, 176)
        Me.hbh.Maximum = 90
        Me.hbh.Minimum = 1
        Me.hbh.Name = "hbh"
        Me.hbh.Size = New System.Drawing.Size(160, 16)
        Me.hbh.TabIndex = 10
        Me.hbh.Value = 1
        '
        'lblCHS
        '
        Me.lblCHS.Location = New System.Drawing.Point(200, 176)
        Me.lblCHS.Name = "lblCHS"
        Me.lblCHS.Size = New System.Drawing.Size(56, 16)
        Me.lblCHS.TabIndex = 9
        '
        'lblheight
        '
        Me.lblheight.Location = New System.Drawing.Point(200, 136)
        Me.lblheight.Name = "lblheight"
        Me.lblheight.Size = New System.Drawing.Size(56, 16)
        Me.lblheight.TabIndex = 7
        '
        'hbheight
        '
        Me.hbheight.LargeChange = 2
        Me.hbheight.Location = New System.Drawing.Point(24, 128)
        Me.hbheight.Minimum = 32
        Me.hbheight.Name = "hbheight"
        Me.hbheight.Size = New System.Drawing.Size(160, 16)
        Me.hbheight.TabIndex = 6
        Me.hbheight.Value = 32
        '
        'lblh
        '
        Me.lblh.Location = New System.Drawing.Point(24, 104)
        Me.lblh.Name = "lblh"
        Me.lblh.Size = New System.Drawing.Size(104, 16)
        Me.lblh.TabIndex = 5
        Me.lblh.Text = "Cell Height"
        '
        'lblw
        '
        Me.lblw.Location = New System.Drawing.Point(24, 56)
        Me.lblw.Name = "lblw"
        Me.lblw.Size = New System.Drawing.Size(136, 16)
        Me.lblw.TabIndex = 4
        Me.lblw.Text = "CellBorderWidth "
        '
        'cmdcolor
        '
        Me.cmdcolor.Location = New System.Drawing.Point(24, 16)
        Me.cmdcolor.Name = "cmdcolor"
        Me.cmdcolor.Size = New System.Drawing.Size(168, 24)
        Me.cmdcolor.TabIndex = 3
        Me.cmdcolor.Text = "Choose Cell Color"
        '
        'cmdset
        '
        Me.cmdset.Location = New System.Drawing.Point(40, 320)
        Me.cmdset.Name = "cmdset"
        Me.cmdset.Size = New System.Drawing.Size(128, 24)
        Me.cmdset.TabIndex = 2
        Me.cmdset.Text = "Invoke Settings"
        '
        'lblbwidth
        '
        Me.lblbwidth.Location = New System.Drawing.Point(200, 80)
        Me.lblbwidth.Name = "lblbwidth"
        Me.lblbwidth.Size = New System.Drawing.Size(56, 24)
        Me.lblbwidth.TabIndex = 1
        '
        'hbwidth
        '
        Me.hbwidth.LargeChange = 2
        Me.hbwidth.Location = New System.Drawing.Point(24, 80)
        Me.hbwidth.Maximum = 20
        Me.hbwidth.Minimum = 1
        Me.hbwidth.Name = "hbwidth"
        Me.hbwidth.Size = New System.Drawing.Size(160, 16)
        Me.hbwidth.TabIndex = 0
        Me.hbwidth.Value = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.gbScrollInc)
        Me.TabPage1.Controls.Add(Me.gbScrolling)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(288, 390)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "Scrolling"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'gbScrollInc
        '
        Me.gbScrollInc.Controls.Add(Me.btLIncrement)
        Me.gbScrollInc.Controls.Add(Me.btLDecrement)
        Me.gbScrollInc.Controls.Add(Me.btSmDecrement)
        Me.gbScrollInc.Controls.Add(Me.tbSmIncrement)
        Me.gbScrollInc.Location = New System.Drawing.Point(20, 160)
        Me.gbScrollInc.Name = "gbScrollInc"
        Me.gbScrollInc.Size = New System.Drawing.Size(242, 56)
        Me.gbScrollInc.TabIndex = 10
        Me.gbScrollInc.TabStop = False
        Me.gbScrollInc.Text = "Scroll by lines and pages"
        '
        'btLIncrement
        '
        Me.btLIncrement.Location = New System.Drawing.Point(186, 19)
        Me.btLIncrement.Name = "btLIncrement"
        Me.btLIncrement.Size = New System.Drawing.Size(35, 23)
        Me.btLIncrement.TabIndex = 10
        Me.btLIncrement.Text = ">>"
        Me.btLIncrement.UseVisualStyleBackColor = True
        '
        'btLDecrement
        '
        Me.btLDecrement.Location = New System.Drawing.Point(21, 19)
        Me.btLDecrement.Name = "btLDecrement"
        Me.btLDecrement.Size = New System.Drawing.Size(35, 23)
        Me.btLDecrement.TabIndex = 10
        Me.btLDecrement.Text = "<<"
        Me.btLDecrement.UseVisualStyleBackColor = True
        '
        'btSmDecrement
        '
        Me.btSmDecrement.Location = New System.Drawing.Point(77, 19)
        Me.btSmDecrement.Name = "btSmDecrement"
        Me.btSmDecrement.Size = New System.Drawing.Size(35, 23)
        Me.btSmDecrement.TabIndex = 10
        Me.btSmDecrement.Text = "<"
        Me.btSmDecrement.UseVisualStyleBackColor = True
        '
        'tbSmIncrement
        '
        Me.tbSmIncrement.Location = New System.Drawing.Point(133, 19)
        Me.tbSmIncrement.Name = "tbSmIncrement"
        Me.tbSmIncrement.Size = New System.Drawing.Size(35, 23)
        Me.tbSmIncrement.TabIndex = 10
        Me.tbSmIncrement.Text = ">"
        Me.tbSmIncrement.UseVisualStyleBackColor = True
        '
        'gbScrolling
        '
        Me.gbScrolling.Controls.Add(Me.btScroll)
        Me.gbScrolling.Controls.Add(Me.cbbScrollTo)
        Me.gbScrolling.Controls.Add(Me.tbScrollAmount)
        Me.gbScrolling.Location = New System.Drawing.Point(20, 30)
        Me.gbScrolling.Name = "gbScrolling"
        Me.gbScrolling.Size = New System.Drawing.Size(242, 97)
        Me.gbScrolling.TabIndex = 9
        Me.gbScrolling.TabStop = False
        Me.gbScrolling.Text = "Scroll to Position"
        '
        'btScroll
        '
        Me.btScroll.Location = New System.Drawing.Point(55, 57)
        Me.btScroll.Name = "btScroll"
        Me.btScroll.Size = New System.Drawing.Size(134, 23)
        Me.btScroll.TabIndex = 3
        Me.btScroll.Text = "Scroll"
        Me.btScroll.UseVisualStyleBackColor = True
        '
        'cbbScrollTo
        '
        Me.cbbScrollTo.FormattingEnabled = True
        Me.cbbScrollTo.Items.AddRange(New Object() {"Scroll to Item...", "Scroll to Row..."})
        Me.cbbScrollTo.Location = New System.Drawing.Point(6, 19)
        Me.cbbScrollTo.Name = "cbbScrollTo"
        Me.cbbScrollTo.Size = New System.Drawing.Size(153, 21)
        Me.cbbScrollTo.TabIndex = 2
        Me.cbbScrollTo.Text = "(Select scrolling method)"
        '
        'tbScrollAmount
        '
        Me.tbScrollAmount.Location = New System.Drawing.Point(175, 19)
        Me.tbScrollAmount.Name = "tbScrollAmount"
        Me.tbScrollAmount.Size = New System.Drawing.Size(46, 20)
        Me.tbScrollAmount.TabIndex = 0
        Me.tbScrollAmount.Text = "0"
        '
        'errdesc
        '
        Me.errdesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.errdesc.Location = New System.Drawing.Point(604, 618)
        Me.errdesc.Name = "errdesc"
        Me.errdesc.Size = New System.Drawing.Size(136, 24)
        Me.errdesc.TabIndex = 31
        Me.errdesc.Text = "Last Error Reported:"
        Me.errdesc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblError
        '
        Me.lblError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblError.Location = New System.Drawing.Point(512, 544)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(312, 72)
        Me.lblError.TabIndex = 30
        '
        'sampdesc
        '
        Me.sampdesc.Location = New System.Drawing.Point(304, 0)
        Me.sampdesc.Name = "sampdesc"
        Me.sampdesc.Size = New System.Drawing.Size(520, 144)
        Me.sampdesc.TabIndex = 29
        Me.sampdesc.Text = resources.GetString("sampdesc.Text")
        '
        'cmdremove
        '
        Me.cmdremove.Location = New System.Drawing.Point(408, 600)
        Me.cmdremove.Name = "cmdremove"
        Me.cmdremove.Size = New System.Drawing.Size(88, 32)
        Me.cmdremove.TabIndex = 27
        Me.cmdremove.Text = "Remove Selected Items"
        '
        'lstselect
        '
        Me.lstselect.Location = New System.Drawing.Point(168, 544)
        Me.lstselect.Name = "lstselect"
        Me.lstselect.Size = New System.Drawing.Size(224, 94)
        Me.lstselect.TabIndex = 26
        '
        'cmdsel
        '
        Me.cmdsel.Location = New System.Drawing.Point(408, 552)
        Me.cmdsel.Name = "cmdsel"
        Me.cmdsel.Size = New System.Drawing.Size(88, 32)
        Me.cmdsel.TabIndex = 25
        Me.cmdsel.Text = "List Selected Items"
        '
        'cmdclear
        '
        Me.cmdclear.Location = New System.Drawing.Point(28, 600)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.Size = New System.Drawing.Size(112, 32)
        Me.cmdclear.TabIndex = 23
        Me.cmdclear.Text = "Clear Images"
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(136, 0)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(144, 32)
        Me.cmdCancel.TabIndex = 36
        Me.cmdCancel.Text = "Cancel Thumbnail Loading"
        '
        'chkPreserve
        '
        Me.chkPreserve.Location = New System.Drawing.Point(8, 40)
        Me.chkPreserve.Name = "chkPreserve"
        Me.chkPreserve.Size = New System.Drawing.Size(104, 32)
        Me.chkPreserve.TabIndex = 37
        Me.chkPreserve.Text = "PreserveBlack option"
        '
        'chkRaw
        '
        Me.chkRaw.Location = New System.Drawing.Point(128, 40)
        Me.chkRaw.Name = "chkRaw"
        Me.chkRaw.Size = New System.Drawing.Size(152, 32)
        Me.chkRaw.TabIndex = 38
        Me.chkRaw.Text = "CameraRaw option"
        '
        'ckbOleDrag
        '
        Me.ckbOleDrag.AutoSize = True
        Me.ckbOleDrag.Location = New System.Drawing.Point(512, 510)
        Me.ckbOleDrag.Name = "ckbOleDrag"
        Me.ckbOleDrag.Size = New System.Drawing.Size(121, 17)
        Me.ckbOleDrag.TabIndex = 39
        Me.ckbOleDrag.Text = "Allow OLE Dragging"
        Me.ckbOleDrag.UseVisualStyleBackColor = True
        '
        'ckbDrillDown
        '
        Me.ckbDrillDown.AutoSize = True
        Me.ckbDrillDown.Location = New System.Drawing.Point(76, 510)
        Me.ckbDrillDown.Name = "ckbDrillDown"
        Me.ckbDrillDown.Size = New System.Drawing.Size(147, 17)
        Me.ckbDrillDown.TabIndex = 40
        Me.ckbDrillDown.Text = "Allow Directory Drill-Down"
        Me.ckbDrillDown.UseVisualStyleBackColor = True
        '
        'ckbTNShift
        '
        Me.ckbTNShift.AutoSize = True
        Me.ckbTNShift.Location = New System.Drawing.Point(667, 510)
        Me.ckbTNShift.Name = "ckbTNShift"
        Me.ckbTNShift.Size = New System.Drawing.Size(141, 17)
        Me.ckbTNShift.TabIndex = 41
        Me.ckbTNShift.Text = "Allow Thumbnail Shifting"
        Me.ckbTNShift.UseVisualStyleBackColor = True
        '
        'ckbTNXDrag
        '
        Me.ckbTNXDrag.AutoSize = True
        Me.ckbTNXDrag.Location = New System.Drawing.Point(265, 510)
        Me.ckbTNXDrag.Name = "ckbTNXDrag"
        Me.ckbTNXDrag.Size = New System.Drawing.Size(216, 17)
        Me.ckbTNXDrag.TabIndex = 42
        Me.ckbTNXDrag.Text = "Allow Dragging between TNX 2 Controls"
        Me.ckbTNXDrag.UseVisualStyleBackColor = True
        '
        'ThumbnailXpress3
        '
        Me.ThumbnailXpress3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ThumbnailXpress3.BottomMargin = 5
        Me.ThumbnailXpress3.CameraRaw = False
        Me.ThumbnailXpress3.CellBorderColor = System.Drawing.Color.Black
        Me.ThumbnailXpress3.CellBorderWidth = 1
        Me.ThumbnailXpress3.CellHeight = 80
        Me.ThumbnailXpress3.CellHorizontalSpacing = 5
        Me.ThumbnailXpress3.CellSpacingColor = System.Drawing.Color.White
        Me.ThumbnailXpress3.CellVerticalSpacing = 5
        Me.ThumbnailXpress3.CellWidth = 80
        Me.ThumbnailXpress3.DblClickDirectoryDrillDown = True
        Me.ThumbnailXpress3.DescriptorAlignment = CType((PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignCenter Or PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignBottom), PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments)
        Me.ThumbnailXpress3.DescriptorDisplayMethod = PegasusImaging.WinForms.ThumbnailXpress2.DescriptorDisplayMethods.[Default]
        Me.ThumbnailXpress3.EnableAsDragSourceForExternalDragDrop = True
        Me.ThumbnailXpress3.EnableAsDropTargetForExternalDragDrop = True
        Me.ThumbnailXpress3.ErrorAction = PegasusImaging.WinForms.ThumbnailXpress2.ErrorAction.UseErrorIcon
        Me.ThumbnailXpress3.FtpPassword = ""
        Me.ThumbnailXpress3.FtpUserName = ""
        Me.ThumbnailXpress3.InterComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress3.IntraComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress3.LeftMargin = 5
        Me.ThumbnailXpress3.Location = New System.Drawing.Point(837, 152)
        Me.ThumbnailXpress3.MaximumThumbnailBitDepth = 24
        Me.ThumbnailXpress3.Name = "ThumbnailXpress3"
        Me.ThumbnailXpress3.PreserveBlack = False
        Me.ThumbnailXpress3.ProxyServer = ""
        Me.ThumbnailXpress3.RightMargin = 5
        Me.ThumbnailXpress3.ScrollDirection = PegasusImaging.WinForms.ThumbnailXpress2.ScrollDirection.Vertical
        Me.ThumbnailXpress3.SelectBackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(36, Byte), Integer), CType(CType(106, Byte), Integer))
        Me.ThumbnailXpress3.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.MultiExtended
        Me.ThumbnailXpress3.ShowHourglass = True
        Me.ThumbnailXpress3.ShowImagePlaceholders = False
        Me.ThumbnailXpress3.Size = New System.Drawing.Size(145, 352)
        Me.ThumbnailXpress3.TabIndex = 43
        Me.ThumbnailXpress3.TextBackColor = System.Drawing.Color.White
        Me.ThumbnailXpress3.TopMargin = 5
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(994, 648)
        Me.Controls.Add(Me.ThumbnailXpress3)
        Me.Controls.Add(Me.ckbTNXDrag)
        Me.Controls.Add(Me.ckbTNShift)
        Me.Controls.Add(Me.ckbDrillDown)
        Me.Controls.Add(Me.ckbOleDrag)
        Me.Controls.Add(Me.chkRaw)
        Me.Controls.Add(Me.chkPreserve)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.chkexpand)
        Me.Controls.Add(Me.tabControl1)
        Me.Controls.Add(Me.errdesc)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.sampdesc)
        Me.Controls.Add(Me.cmdremove)
        Me.Controls.Add(Me.lstselect)
        Me.Controls.Add(Me.cmdsel)
        Me.Controls.Add(Me.cmdclear)
        Me.Controls.Add(Me.ThumbnailXpress2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ThumbnailXpress Viewer"
        Me.tabControl1.ResumeLayout(False)
        Me.Filters.ResumeLayout(False)
        Me.Filters.PerformLayout()
        Me.tabThread.ResumeLayout(False)
        Me.grpThreads.ResumeLayout(False)
        Me.grpThreads.PerformLayout()
        CType(Me.maxthreads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabselect.ResumeLayout(False)
        Me.cells.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.gbScrollInc.ResumeLayout(False)
        Me.gbScrolling.ResumeLayout(False)
        Me.gbScrolling.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'create the color dialog object here
        cd = New ColorDialog()

        Try
            '********MUST CALL UNLOCKRUNTIME at STARTUP*******
            'ThumbnailXpress2.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
            'ThumbnailXpress2.Licensing.UnlockIXRuntime(1234,1234,1234,1234)
            'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234

            'thread settings
            maxthreads.Value = ThumbnailXpress2.MaximumThreadCount
            txtSThread.Text = ThumbnailXpress2.ThreadStartThreshold.ToString()
            txtHungT.Text = ThumbnailXpress2.ThreadHungThreshold.ToString()

            strCurrentDir = System.IO.Path.GetFullPath(Application.StartupPath & "\..\..\..\..\..\..\..\Common\\Images\\")

            ThumbnailXpress2.DescriptorDisplayMethod = DescriptorDisplayMethods.ShortFilename
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)

        End Try
        lstselect.CheckOnClick = True

        'Disable the special options (Dragging and drill-down)
        ThumbnailXpress2.DblClickDirectoryDrillDown = False
        ThumbnailXpress2.InterComponentThumbnailDragDropEnabled = False
        ThumbnailXpress2.EnableAsDragSourceForExternalDragDrop = False
        ThumbnailXpress2.EnableAsDropTargetForExternalDragDrop = False
        ThumbnailXpress2.IntraComponentThumbnailDragDropEnabled = False

        'Enable the dragging of thumbnails to the control
        ThumbnailXpress3.DblClickDirectoryDrillDown = False
        ThumbnailXpress3.InterComponentThumbnailDragDropEnabled = True
        ThumbnailXpress3.EnableAsDragSourceForExternalDragDrop = False
        ThumbnailXpress3.EnableAsDropTargetForExternalDragDrop = False
        ThumbnailXpress3.IntraComponentThumbnailDragDropEnabled = False

        'Hide the second ThumbnailXpress control
        Me.Width = 840

        'set some control defaults
        Try
            ThumbnailXpress2.Licensing.AccessPdfXpress2()
        Catch eX As Exception
            PegasusError(New Exception("Unable to find PdfXpress. PDF abilities disabled."), lblError)
        End Try
        ThumbnailXpress2.CameraRaw = False
        ThumbnailXpress2.AllowDrop = True
        cmbcompare.SelectedIndex = 0
        cmbfiles.SelectedIndex = 0
        cmbchoice.SelectedIndex = 0
        cmbselect.SelectedIndex = 0
        hbwidth.Value = 1
        hbheight.Value = 65
        hbh.Value = 1
        vbh.Value = 1
        vcw.Value = 65
        lblbwidth.Text = hbwidth.Value.ToString
        lblheight.Text = hbheight.Value.ToString
        lblCHS.Text = hbh.Value.ToString
        lblCVS.Text = vbh.Value.ToString
        lblCellWidth.Text = vcw.Value.ToString

        'date time value set here
        dateTimePicker1.Value = System.DateTime.Now

        Application.EnableVisualStyles()
    End Sub



    Private Sub cmbchoice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbchoice.SelectedIndexChanged
        Select Case cmbchoice.SelectedIndex
            Case 0
                chkenable.Enabled = True
                chkdir.Enabled = True
                chksen.Enabled = True
                chksub.Enabled = False
                chkparent.Enabled = False
                dateTimePicker1.Enabled = False
                cmbcompare.Enabled = False
                chkhid.Enabled = False
                tbFileNamePattern.Enabled = True
                ' break 
            Case 1
                chkdir.Enabled = True
                chkparent.Enabled = True
                chksub.Enabled = True
                chkparent.Enabled = True
                chkhid.Enabled = True
                chkdir.Enabled = False
                chksen.Enabled = False
                dateTimePicker1.Enabled = False
                cmbcompare.Enabled = False
                txtsize.Enabled = False
                tbFileNamePattern.Enabled = False
                ' break 
            Case 2
                chksen.Enabled = False
                chkdir.Enabled = False
                chkenable.Enabled = True
                cmbcompare.Enabled = True
                dateTimePicker1.Enabled = True
                txtsize.Enabled = False
                chksub.Enabled = False
                chkparent.Enabled = False
                chkhid.Enabled = False
                tbFileNamePattern.Enabled = False
                ' break 
            Case 3
                chksen.Enabled = False
                chkdir.Enabled = False
                chkenable.Enabled = True
                dateTimePicker1.Enabled = True
                cmbcompare.Enabled = True
                txtsize.Enabled = False
                chksub.Enabled = False
                chkparent.Enabled = False
                chkhid.Enabled = False
                tbFileNamePattern.Enabled = False
                ' break 
            Case 4
                chksen.Enabled = False
                chkdir.Enabled = False
                chkenable.Enabled = True
                dateTimePicker1.Enabled = False
                txtsize.Visible = True
                txtsize.Enabled = True
                cmbcompare.Enabled = True
                chksub.Enabled = False
                chkparent.Enabled = False
                tbFileNamePattern.Enabled = False
                ' break 
            Case 5
                chkenable.Enabled = False
                dateTimePicker1.Enabled = False
                cmbcompare.Enabled = False
                chksen.Enabled = False
                chkdir.Enabled = False
                chkparent.Enabled = False
                chksub.Enabled = False
                chkhid.Enabled = False
                txtsize.Enabled = False
                tbFileNamePattern.Enabled = False
                ' break 
        End Select
    End Sub

    Private Sub cmbcompare_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbcompare.SelectedIndexChanged
        Select Case cmbcompare.SelectedIndex
            Case 0
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress2.FilterComparison.LessThan
                ' break 
            Case 1
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress2.FilterComparison.LessThanEquals
                ' break 
            Case 2
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress2.FilterComparison.Equal
                ' break 
            Case 3
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress2.FilterComparison.GreaterThanEquals
                ' break 
            Case 4
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress2.FilterComparison.GreaterThan
                ' break 
        End Select
    End Sub


    Private Sub cmbfiles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbfiles.SelectedIndexChanged
        filteritem = ThumbnailXpress2.CreateFilter("test")
        Select Case cmbfiles.SelectedIndex
            Case 0
                filteritem.FilenamePattern = "*.*"
                ' break 
            Case 1
                filteritem.FilenamePattern = "*.bmp"
                ' break 
            Case 2
                filteritem.FilenamePattern = "*.cal"
                ' break 
            Case 3
                filteritem.FilenamePattern = "*.dib"
                ' break 
            Case 4
                filteritem.FilenamePattern = "*.dca"
                ' break 
            Case 5
                filteritem.FilenamePattern = "*.mod"
                ' break 
            Case 6
                filteritem.FilenamePattern = "*.gif"
                ' break 
            Case 7
                filteritem.FilenamePattern = "*.jp2"
                ' break 
            Case 8
                filteritem.FilenamePattern = "*.jls"
                ' break 
            Case 9
                filteritem.FilenamePattern = "*.jpg"
                ' break 
            Case 10
                filteritem.FilenamePattern = "*.jls"
                ' break 
            Case 11
                filteritem.FilenamePattern = "*.pmm"
                ' break 
            Case 12
                filteritem.FilenamePattern = "*.pcx"
                ' break 
            Case 13
                filteritem.FilenamePattern = "*.pgm"
                ' break 
            Case 14
                filteritem.FilenamePattern = "*.pic"
                ' break 
            Case 15
                filteritem.FilenamePattern = "*.png"
                ' break 
            Case 16
                filteritem.FilenamePattern = "*.ppm"
                ' break 
            Case 17
                filteritem.FilenamePattern = "*.tif"
                ' break 
            Case 18
                filteritem.FilenamePattern = "*.tga"
                ' break 
            Case 19
                filteritem.FilenamePattern = "*.wsq"
                ' break 
            Case 20
                filteritem.FilenamePattern = "*.jb2"
                ' break 
            Case 21
                ' break 
        End Select
    End Sub

    Private Sub dateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dateTimePicker1.ValueChanged
        filteritem.FileDate = dateTimePicker1.Value
    End Sub

    Private Sub cmbselect_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbselect.SelectedIndexChanged
        Select Case cmbselect.SelectedIndex
            Case 0
                ThumbnailXpress2.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.MultiExtended
                ' break 
            Case 1
                ThumbnailXpress2.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.MultiSimple
                ' break 
            Case 2
                ThumbnailXpress2.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.None
                ' break 
            Case 3
                ThumbnailXpress2.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.Single
                ' break 
        End Select
    End Sub

    Private Sub cmdcolor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcolor.Click
        cd.ShowDialog()
    End Sub

    Private Sub hbwidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hbwidth.Scroll
        lblbwidth.Text = hbwidth.Value.ToString
    End Sub

    Private Sub hbheight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hbheight.Scroll
        lblheight.Text = hbheight.Value.ToString
    End Sub

    Private Sub hbh_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hbh.Scroll
        lblCHS.Text = hbh.Value.ToString
    End Sub

    Private Sub vbh_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles vbh.Scroll
        lblCVS.Text = vbh.Value.ToString
    End Sub

    Private Sub vcw_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles vcw.Scroll
        lblCellWidth.Text = vcw.Value.ToString
    End Sub

    Private Sub cmdset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdset.Click
        ThumbnailXpress2.BeginUpdate()
        ThumbnailXpress2.CellBorderWidth = hbwidth.Value
        ThumbnailXpress2.CellBorderColor = cd.Color

        ThumbnailXpress2.CellHeight = hbheight.Value
        ThumbnailXpress2.CellHorizontalSpacing = hbh.Value
        ThumbnailXpress2.CellVerticalSpacing = vbh.Value
        ThumbnailXpress2.CellWidth = vcw.Value
        ThumbnailXpress2.EndUpdate()
    End Sub

    Private Sub cmdclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        ThumbnailXpress2.Items.Clear()
        ThumbnailXpress3.Items.Clear()
    End Sub

    Private Sub cmdsel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsel.Click
        Try
            'get count of selected items
            lstselect.Items.Clear()
            a = ThumbnailXpress2.SelectedItems.Count
            'create a array with the size of the selected TN's
            array = New PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem(a - 1) {}
            'copy items to array
            ThumbnailXpress2.SelectedItems.CopyTo(array, 0)
            'add all the items to a list box
            lstselect.Items.AddRange(array)

        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try
    End Sub


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Private cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Private cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Private cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.gif;*.hdp;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wdp;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*" & _
 ".dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.h" & _
 "dp;*.wdp|All Files (*.*)|*.*"

    Private Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        lblError.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Private Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        lblError.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Private Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        lblError.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Function PegasusOpenDir() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir

        If dlg.ShowDialog = DialogResult.OK Then

            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), dlg.FileName.Length - dlg.FileName.LastIndexOf("\"))

            Select Case dlg.FilterIndex
                Case 1
                    ' break 
                Case 2
                    filteritem.FilenamePattern = "*.bmp"
                    ' break 
                Case 3
                    filteritem.FilenamePattern = "*.cal"
                    ' break 
                Case 4
                    filteritem.FilenamePattern = "*.dib"
                    ' break 
                Case 5
                    filteritem.FilenamePattern = "*.dca"
                    ' break 
                Case 6
                    filteritem.FilenamePattern = "*.mod"
                    ' break 
                Case 7
                    filteritem.FilenamePattern = "*.gif"
                    ' break 
                Case 8
                    filteritem.FilenamePattern = "*.jp2"
                    ' break 
                Case 9
                    filteritem.FilenamePattern = "*.jls"
                    ' break 
                Case 10
                    filteritem.FilenamePattern = "*.jpg"
                    ' break 
                Case 11
                    filteritem.FilenamePattern = "*.jls"
                    ' break 
                Case 12
                    filteritem.FilenamePattern = "*.pmm"
                    ' break 
                Case 13
                    filteritem.FilenamePattern = "*.pcx"
                    ' break 
                Case 14
                    filteritem.FilenamePattern = "*.pgm"
                    ' break 
                Case 15
                    filteritem.FilenamePattern = "*.pic"
                    ' break 
                Case 16
                    filteritem.FilenamePattern = "*.png"
                    ' break 
                Case 17
                    filteritem.FilenamePattern = "*.ppm"
                    ' break 
                Case 18
                    filteritem.FilenamePattern = "*.tif"
                    ' break 
                Case 19
                    filteritem.FilenamePattern = "*.tga"
                    ' break 
                Case 20
                    filteritem.FilenamePattern = "*.wsq"
                    ' break 
                Case 21
                    filteritem.FilenamePattern = "*.jb2"
                    ' break 
                Case 22
                    ' break 
            End Select

            If chkenable.Checked = True Then
                ThumbnailXpress2.Filters.Clear()
                filteritem.Type = CType(cmbchoice.SelectedIndex, FilterType)
                filteritem.Comparison = CType(cmbcompare.SelectedIndex, FilterComparison)
                filteritem.FileDate = dateTimePicker1.Value
                If Not (txtsize.Text = "") Then
                    filteritem.FileSize = System.Convert.ToInt32(txtsize.Text)
                End If
                filteritem.CaseSensitive = chksen.Checked
                filteritem.MatchAppliesToDirectory = chkdir.Checked
                filteritem.IncludeHidden = chkhid.Checked
                filteritem.IncludeParentDirectory = chkparent.Checked
                filteritem.IncludeSubDirectory = chksub.Checked
                filteritem.FilenamePattern = tbFileNamePattern.Text

                ThumbnailXpress2.Filters.Add(filteritem)
                ThumbnailXpress2.Items.AddItemsFromDirectory(strCurrentDir + "\", 0)
            Else
                ThumbnailXpress2.Items.AddItemsFromDirectory(strCurrentDir + "\", 0)
            End If
            Return strCurrentDir
        Else

            Return ""
        End If
    End Function

    'Open single files here or multi-page TIFF Image
    Private Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If dlg.ShowDialog() = DialogResult.OK Then
            'strImageFile = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"),dlg.FileName.Length - dlg.FileName.LastIndexOf("\"));
            strImageFile = dlg.FileName
            If chkexpand.Checked = True Then
                ThumbnailXpress2.Items.AddItemsFromFile(strImageFile, 0, True)
            Else
                ThumbnailXpress2.Items.AddItemsFromFile(strImageFile, 0, False)
            End If
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
        Dim iTmp As System.Int32
        Try
            iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
        Catch ex As System.NullReferenceException
            PegasusError(ex, lblError)
            textTextBox.Text = scrScroll.Value.ToString(cultNumber)
            Return
        Catch ex As System.Exception
            PegasusError(ex, lblError)
            textTextBox.Text = scrScroll.Value.ToString(cultNumber)
            Return
        End Try
        If (iTmp < scrScroll.Maximum) And (iTmp > scrScroll.Minimum) Then
            scrScroll.Value = iTmp
        Else
            iTmp = scrScroll.Value
        End If
        textTextBox.Text = iTmp.ToString(cultNumber)
    End Sub

#End Region


    Private Sub mnudir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnudir.Click


        Try

            'clear out the error before the next opertation
            lblError.Text = ""

            'First we grab the filename of the image we want to open\
            'create a filter item here

            filteritem = ThumbnailXpress2.CreateFilter("test")
            filteritemCreated = True
            Dim strloadres As System.String = PegasusOpenDir()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub mnusingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnusingle.Click
        Try

            'clear out the error before the next opertation
            lblError.Text = ""

            Dim strloadres As System.String = PegasusOpenFile()

        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException

            PegasusError(ex, lblError)

        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub mnuabout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuabout.Click
        ThumbnailXpress2.AboutBox()
    End Sub

    Private Sub mnuexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuexit.Click
        Application.Exit()
    End Sub

    Private Sub cmdremove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdremove.Click
        Try
            Dim citem As Integer
            citem = lstselect.CheckedItems.Count

            Dim i As Integer
            For i = 0 To citem - 1 Step i + 1
                ThumbnailXpress2.Items.Remove(array(i))
                'lstselect.Items.RemoveAt(i);
            Next
            'recopy the array
            'get count of selected items
            lstselect.Items.Clear()
            a = ThumbnailXpress2.SelectedItems.Count
            'create a array with the size of the selected TN's
            array = New PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem(a - 1) {}
            'copy items to array
            ThumbnailXpress2.SelectedItems.CopyTo(array, 0)
            'add all the items to a list box
            lstselect.Items.AddRange(array)
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException

            PegasusError(ex, lblError)
            ' Least specific:
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub



    Private Sub chkdir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkdir.CheckedChanged
        If chkdir.Checked = True Then
            filteritem.MatchAppliesToDirectory = True

        Else
            filteritem.MatchAppliesToDirectory = False

        End If

    End Sub

    Private Sub chksen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chksen.CheckedChanged
        If chkdir.Checked = True Then
            filteritem.CaseSensitive = True

        Else

            filteritem.CaseSensitive = False
        End If


    End Sub

    Private Sub chkhid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkhid.CheckedChanged
        If chkhid.Checked = True Then
            filteritem.IncludeHidden = True

        Else
            filteritem.IncludeHidden = False

        End If

    End Sub

    Private Sub chkenable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkenable.CheckedChanged

        'make sure filteritem has been initialized
        If filteritemCreated = True Then
            If chkenable.Checked = True Then
                filteritem.Enabled = True
                ThumbnailXpress2.Filters.Clear()
                ThumbnailXpress2.Filters.Add(filteritem)
            Else
                filteritem.Enabled = False

            End If
        End If
    End Sub

    Private Sub chkparent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkparent.CheckedChanged
        If chkenable.Checked = True Then
            filteritem.IncludeParentDirectory = True

        Else
            filteritem.IncludeParentDirectory = False

        End If

    End Sub

    Private Sub chksub_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chksub.CheckedChanged
        If chkenable.Checked = True Then
            filteritem.IncludeSubDirectory = True

        Else
            filteritem.IncludeSubDirectory = False

        End If

    End Sub

    Private Sub ThumbnailXpress2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ThumbnailXpress2.DoubleClick
        Try
            
            'need to verify if we are on a folder etc.
            Dim info As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailItem
            info = ThumbnailXpress2.GetItemFromPoint(point)

            If Not (info Is Nothing) Then
                If info.Type = ThumbnailType.SubDirectory Or info.Type = ThumbnailType.ParentDirectory Then
                    Exit Sub
                End If

                'implement the viewer via the Doubleclick event
                tdata = ThumbnailXpress2.GetItemFromPoint(point)
                Dim viewer As New TNViewer()
                viewer.AddImage(tdata.Filename)
                viewer.ShowDialog(Me)

                Application.DoEvents()

            End If
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException

            PegasusError(ex, lblError)

        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try
    End Sub


    Private Sub maxthreads_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maxthreads.ValueChanged
        Try
            ThumbnailXpress2.MaximumThreadCount = maxthreads.Value

        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
            PegasusError(ex, lblError)

        End Try
    End Sub

    Private Sub cmdSetThreads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSetThreads.Click
        Try
            ThumbnailXpress2.ThreadStartThreshold = System.Convert.ToInt32(txtSThread.Text)
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
            txtSThread.Text = ThumbnailXpress2.ThreadStartThreshold.ToString()
        End Try

        Try
            ThumbnailXpress2.ThreadHungThreshold = System.Convert.ToInt32(txtHungT.Text)
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
            txtHungT.Text = ThumbnailXpress2.ThreadHungThreshold.ToString()
        End Try

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Try
            ThumbnailXpress2.Cancel()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException
        End Try
    End Sub

    Private Sub chkPreserve_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPreserve.CheckedChanged
        If chkPreserve.Checked = True Then
            ThumbnailXpress2.PreserveBlack = True
        Else
            ThumbnailXpress2.PreserveBlack = False
        End If
    End Sub

    Private Sub chkRaw_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRaw.CheckedChanged
        If chkRaw.Checked = True Then
            ThumbnailXpress2.CameraRaw = True
        Else
            ThumbnailXpress2.CameraRaw = False
        End If
    End Sub

    Private Sub cmdFlush_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            ThumbnailXpress2.ThumbnailCache.CacheFlushMemory()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            ThumbnailXpress2.ThumbnailCache.CacheClear()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpressException

        End Try
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub ckbDrillDown_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbDrillDown.CheckedChanged
        'This enables/disables the Directory Drill-Down feature
        If ckbDrillDown.Checked Then
            ThumbnailXpress2.DblClickDirectoryDrillDown = True

            'Enable the adding of parent and subdirectories for navigation
            filteritem.Enabled = True
            filteritem.IncludeSubDirectory = True
            filteritem.IncludeParentDirectory = True

            cmbchoice.SelectedIndex = 1

            chkenable.Checked = True
            chksub.Checked = True
            chkparent.Checked = True
        Else
            ThumbnailXpress2.DblClickDirectoryDrillDown = False
        End If

        ThumbnailXpress2.Filters.Clear()
        ThumbnailXpress2.Filters.Add(filteritem)
    End Sub

    Private Sub ckbTNXDrag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbTNXDrag.CheckedChanged
        'Enables/disables dragging between TNX 2 controls.
        If ckbTNXDrag.Checked Then
            ThumbnailXpress2.InterComponentThumbnailDragDropEnabled = True
            Me.Width = 1000 'Show the second ThumbnailXpress control
        Else
            ThumbnailXpress2.InterComponentThumbnailDragDropEnabled = False
            Me.Width = 840 'Hide the second ThumbnailXpress control
        End If
    End Sub

    Private Sub ckbOleDrag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbOleDrag.CheckedChanged
        'Enables OLE dragging
        If ckbOleDrag.Checked Then
            ThumbnailXpress2.EnableAsDragSourceForExternalDragDrop = True
            ThumbnailXpress2.EnableAsDropTargetForExternalDragDrop = True
        Else
            ThumbnailXpress2.EnableAsDragSourceForExternalDragDrop = False
            ThumbnailXpress2.EnableAsDropTargetForExternalDragDrop = False
        End If
    End Sub

    Private Sub ckbTNShift_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbTNShift.CheckedChanged
        'Allows swapping thumbnail positions within the control by mouse
        If ckbTNShift.Checked Then
            ThumbnailXpress2.IntraComponentThumbnailDragDropEnabled = True
        Else
            ThumbnailXpress2.IntraComponentThumbnailDragDropEnabled = False
        End If
    End Sub

    Private Sub btScroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btScroll.Click
        Try
            Select Case cbbScrollTo.SelectedIndex
                Case 0  'Scroll to item (Highlight it just to indicate which one it is)
                    ThumbnailXpress2.ScrollItems(PegasusImaging.WinForms.ThumbnailXpress2.ScrollItemsType.ItemIndex, System.Convert.ToInt32(tbScrollAmount.Text))
                    ThumbnailXpress2.SelectedItems.Clear()
                    ThumbnailXpress2.SelectedItems.Add(ThumbnailXpress2.Items(System.Convert.ToInt32(tbScrollAmount.Text)))
                Case 1  'Scroll to row or column
                    ThumbnailXpress2.ScrollItems(PegasusImaging.WinForms.ThumbnailXpress2.ScrollItemsType.ThumbPosition, System.Convert.ToInt32(tbScrollAmount.Text))
            End Select
        Catch ex As Exception
            MsgBox("Cannot scroll. " & ex.Message)
        End Try
    End Sub

    Private Sub btLDecrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLDecrement.Click
        'Scroll by a large decrease
        ThumbnailXpress2.ScrollItems(PegasusImaging.WinForms.ThumbnailXpress2.ScrollItemsType.LargeDecrease, 3)
    End Sub

    Private Sub btLIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLIncrement.Click
        'Scroll by a large increase
        ThumbnailXpress2.ScrollItems(PegasusImaging.WinForms.ThumbnailXpress2.ScrollItemsType.LargeIncrease, 3)
    End Sub

    Private Sub btSmDecrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSmDecrement.Click
        'Scroll by a small decrease
        ThumbnailXpress2.ScrollItems(PegasusImaging.WinForms.ThumbnailXpress2.ScrollItemsType.SmallDecrease, 1)
    End Sub

    Private Sub tbSmIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbSmIncrement.Click
        'Scroll by a small increase
        ThumbnailXpress2.ScrollItems(PegasusImaging.WinForms.ThumbnailXpress2.ScrollItemsType.SmallIncrease, 1)
    End Sub

    Private Sub ThumbnailXpress2_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ThumbnailXpress2.MouseDown
        point = New System.Drawing.Point(e.X, e.Y)
    End Sub
End Class

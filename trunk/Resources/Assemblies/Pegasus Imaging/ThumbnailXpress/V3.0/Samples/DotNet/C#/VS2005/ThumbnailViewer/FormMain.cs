/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Accusoft.ThumbnailXpressSdk;
using Accusoft.ImagXpressSdk;
using System.IO;
using System.Globalization;


namespace ThumbNailXpress
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class formMain : System.Windows.Forms.Form
    {
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItemFile;
        private System.Windows.Forms.MenuItem menuItemOpenDir;
        private System.Windows.Forms.MenuItem menuItemExit;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.MenuItem menuItemOpenImage;
        private System.ComponentModel.IContainer components;

        //file I/O variables

        private System.String strImageFile;
        //General Variables for sample

        System.Drawing.Point selectedThumbnailPoint;

        System.String strCurrentDir;
        bool filterItemCreated;
        //ThumbnailXpress Filter
        Accusoft.ThumbnailXpressSdk.Filter filterItem;

        private System.Windows.Forms.Button buttonListSelected;
        private System.Windows.Forms.CheckedListBox checkedListBoxSelected;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Label labelLastErrorDescription;
        private System.Windows.Forms.Label labelLastError;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCells;
        private System.Windows.Forms.TabPage tabFilters;
        private System.Windows.Forms.Label labelCellBorderWidthValue;
        private System.Windows.Forms.HScrollBar hScrollBarCellBorderWidth;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Button buttonChooseCellColor;
        private System.Windows.Forms.ColorDialog cd;
        private System.Windows.Forms.Label labelCellBorderWidth;
        private System.Windows.Forms.Label labelCellHeight;
        private System.Windows.Forms.Label labelCellHeightValue;
        private System.Windows.Forms.HScrollBar hScrollBarCellHeight;
        private System.Windows.Forms.HScrollBar hScrollBarCellHorizontalSpacing;
        private System.Windows.Forms.ComboBox comboBoxCompare;
        private System.Windows.Forms.ComboBox comboBoxFilter;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.Label labelCompare;
        private System.Windows.Forms.MenuItem menuItemAbout;
        private System.Windows.Forms.MenuItem menuItemAboutTN;
        private System.Windows.Forms.CheckBox checkBoxMatchCaseSensitive;
        private System.Windows.Forms.CheckBox checkBoxMatchDir;
        private System.Windows.Forms.CheckBox checkBoxIncludeHidden;
        private System.Windows.Forms.CheckBox checkBoxEnabled;
        private System.Windows.Forms.CheckBox checkBoxIncludeSubDir;
        private System.Windows.Forms.DateTimePicker dateTimePickerFileDate;
        private System.Windows.Forms.Label labelFileSize;
        private System.Windows.Forms.TextBox textBoxFileSize;
        private System.Windows.Forms.Label labelCellVerticalSpacing;
        private System.Windows.Forms.CheckBox checkBoxExpand;
        private System.Windows.Forms.TabPage tabSelectionMode;
        private System.Windows.Forms.ComboBox comboBoxSelectionMode;
        private System.Windows.Forms.Label labelSelectionMode;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RichTextBox textBoxSampleDescription;
        private System.Windows.Forms.Label labelCellHorizontalSpacingValue;
        private System.Windows.Forms.Label labelCellVerticalSpacingValue;
        private System.Windows.Forms.CheckBox checkBoxPreserveBlack;
        private System.Windows.Forms.CheckBox checkBoxCameraRaw;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabPage tabProcessing;
        private System.Windows.Forms.NumericUpDown numericUpDownMaxThreads;
        private System.Windows.Forms.Label labelMaxThreads;
        private System.Windows.Forms.GroupBox groupBoxThreads;
        private System.Windows.Forms.Button buttonSetThreads;
        private System.Windows.Forms.Label labelStartThreadThreshold;
        private System.Windows.Forms.Label labelHungThreadThreshold;
        private System.Windows.Forms.TextBox textBoxStartThreadThreshold;
        private System.Windows.Forms.TextBox textBoxHungThreadThreshold;
        private System.Windows.Forms.CheckBox checkBoxIncludeParentDir;
        private System.Windows.Forms.HScrollBar hScrollBarCellVerticalSpacing;
        private System.Windows.Forms.Label labelCellHorizontalSpacing;
        private MenuItem menuItemAboutIX;
        private TabPage tabScrolling;
        internal GroupBox groupBoxScrollInc;
        internal Button buttonLargeIncrement;
        internal Button buttonLargeDecrement;
        internal Button buttonSmallDecrement;
        internal Button buttonSmallIncrement;
        internal GroupBox groupBoxScrolling;
        internal Button buttonScroll;
        internal ComboBox comboBoxScrollTo;
        internal TextBox textBoxScrollAmount;
        internal CheckBox checkBoxTNDrag;
        internal CheckBox checkBoxThumbnailShift;
        internal CheckBox checkBoxDrillDown;
        internal CheckBox checkBoxOleDrag;
        private TextBox textBoxFileNamePattern;
        private Accusoft.ThumbnailXpressSdk.ThumbnailXpress thumbnailXpress1;
        private Accusoft.ThumbnailXpressSdk.ThumbnailXpress thumbnailXpress2;
        private ImagXpress imagXpress1;
        private Label labelCellWidthValue;
        private Label labelCellWidth;
        private HScrollBar hScrollBarCellWidth;
        private Label labelFileDate;
        private Label labelFileNamePattern;

        public formMain()
        {
            //
            // Required for Windows Form Designer support
            //

            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

                if (thumbnailXpress1 != null)
                {
                    thumbnailXpress1.Dispose();
                    thumbnailXpress1 = null;
                }

                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }

                if (components != null)
                {
                    components.Dispose();
                }

            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemFile = new System.Windows.Forms.MenuItem();
            this.menuItemOpenDir = new System.Windows.Forms.MenuItem();
            this.menuItemOpenImage = new System.Windows.Forms.MenuItem();
            this.menuItemExit = new System.Windows.Forms.MenuItem();
            this.menuItemAbout = new System.Windows.Forms.MenuItem();
            this.menuItemAboutIX = new System.Windows.Forms.MenuItem();
            this.menuItemAboutTN = new System.Windows.Forms.MenuItem();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonListSelected = new System.Windows.Forms.Button();
            this.checkedListBoxSelected = new System.Windows.Forms.CheckedListBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxSampleDescription = new System.Windows.Forms.RichTextBox();
            this.labelLastErrorDescription = new System.Windows.Forms.Label();
            this.labelLastError = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabFilters = new System.Windows.Forms.TabPage();
            this.labelFileDate = new System.Windows.Forms.Label();
            this.labelFileNamePattern = new System.Windows.Forms.Label();
            this.textBoxFileNamePattern = new System.Windows.Forms.TextBox();
            this.checkBoxIncludeParentDir = new System.Windows.Forms.CheckBox();
            this.textBoxFileSize = new System.Windows.Forms.TextBox();
            this.labelFileSize = new System.Windows.Forms.Label();
            this.dateTimePickerFileDate = new System.Windows.Forms.DateTimePicker();
            this.checkBoxIncludeSubDir = new System.Windows.Forms.CheckBox();
            this.checkBoxEnabled = new System.Windows.Forms.CheckBox();
            this.checkBoxIncludeHidden = new System.Windows.Forms.CheckBox();
            this.checkBoxMatchCaseSensitive = new System.Windows.Forms.CheckBox();
            this.checkBoxMatchDir = new System.Windows.Forms.CheckBox();
            this.labelCompare = new System.Windows.Forms.Label();
            this.labelFilter = new System.Windows.Forms.Label();
            this.comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.comboBoxCompare = new System.Windows.Forms.ComboBox();
            this.tabCells = new System.Windows.Forms.TabPage();
            this.labelCellWidthValue = new System.Windows.Forms.Label();
            this.labelCellWidth = new System.Windows.Forms.Label();
            this.hScrollBarCellWidth = new System.Windows.Forms.HScrollBar();
            this.labelCellHorizontalSpacing = new System.Windows.Forms.Label();
            this.hScrollBarCellVerticalSpacing = new System.Windows.Forms.HScrollBar();
            this.labelCellVerticalSpacingValue = new System.Windows.Forms.Label();
            this.labelCellVerticalSpacing = new System.Windows.Forms.Label();
            this.hScrollBarCellHorizontalSpacing = new System.Windows.Forms.HScrollBar();
            this.labelCellHorizontalSpacingValue = new System.Windows.Forms.Label();
            this.labelCellHeightValue = new System.Windows.Forms.Label();
            this.hScrollBarCellHeight = new System.Windows.Forms.HScrollBar();
            this.labelCellHeight = new System.Windows.Forms.Label();
            this.labelCellBorderWidth = new System.Windows.Forms.Label();
            this.buttonChooseCellColor = new System.Windows.Forms.Button();
            this.buttonSet = new System.Windows.Forms.Button();
            this.labelCellBorderWidthValue = new System.Windows.Forms.Label();
            this.hScrollBarCellBorderWidth = new System.Windows.Forms.HScrollBar();
            this.tabProcessing = new System.Windows.Forms.TabPage();
            this.groupBoxThreads = new System.Windows.Forms.GroupBox();
            this.textBoxHungThreadThreshold = new System.Windows.Forms.TextBox();
            this.textBoxStartThreadThreshold = new System.Windows.Forms.TextBox();
            this.labelHungThreadThreshold = new System.Windows.Forms.Label();
            this.labelStartThreadThreshold = new System.Windows.Forms.Label();
            this.buttonSetThreads = new System.Windows.Forms.Button();
            this.labelMaxThreads = new System.Windows.Forms.Label();
            this.numericUpDownMaxThreads = new System.Windows.Forms.NumericUpDown();
            this.tabSelectionMode = new System.Windows.Forms.TabPage();
            this.labelSelectionMode = new System.Windows.Forms.Label();
            this.comboBoxSelectionMode = new System.Windows.Forms.ComboBox();
            this.tabScrolling = new System.Windows.Forms.TabPage();
            this.groupBoxScrollInc = new System.Windows.Forms.GroupBox();
            this.buttonLargeIncrement = new System.Windows.Forms.Button();
            this.buttonLargeDecrement = new System.Windows.Forms.Button();
            this.buttonSmallDecrement = new System.Windows.Forms.Button();
            this.buttonSmallIncrement = new System.Windows.Forms.Button();
            this.groupBoxScrolling = new System.Windows.Forms.GroupBox();
            this.buttonScroll = new System.Windows.Forms.Button();
            this.comboBoxScrollTo = new System.Windows.Forms.ComboBox();
            this.textBoxScrollAmount = new System.Windows.Forms.TextBox();
            this.cd = new System.Windows.Forms.ColorDialog();
            this.checkBoxExpand = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBoxPreserveBlack = new System.Windows.Forms.CheckBox();
            this.checkBoxCameraRaw = new System.Windows.Forms.CheckBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxTNDrag = new System.Windows.Forms.CheckBox();
            this.checkBoxThumbnailShift = new System.Windows.Forms.CheckBox();
            this.checkBoxDrillDown = new System.Windows.Forms.CheckBox();
            this.checkBoxOleDrag = new System.Windows.Forms.CheckBox();
            this.thumbnailXpress1 = new Accusoft.ThumbnailXpressSdk.ThumbnailXpress();
            this.thumbnailXpress2 = new Accusoft.ThumbnailXpressSdk.ThumbnailXpress();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.tabControl1.SuspendLayout();
            this.tabFilters.SuspendLayout();
            this.tabCells.SuspendLayout();
            this.tabProcessing.SuspendLayout();
            this.groupBoxThreads.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxThreads)).BeginInit();
            this.tabSelectionMode.SuspendLayout();
            this.tabScrolling.SuspendLayout();
            this.groupBoxScrollInc.SuspendLayout();
            this.groupBoxScrolling.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemAbout});
            // 
            // menuItemFile
            // 
            this.menuItemFile.Index = 0;
            this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemOpenDir,
            this.menuItemOpenImage,
            this.menuItemExit});
            this.menuItemFile.Text = "File";
            // 
            // menuItemOpenDir
            // 
            this.menuItemOpenDir.Index = 0;
            this.menuItemOpenDir.Text = "Open Directory of Images";
            this.menuItemOpenDir.Click += new System.EventHandler(this.menuItemOpenDir_Click);
            // 
            // menuItemOpenImage
            // 
            this.menuItemOpenImage.Index = 1;
            this.menuItemOpenImage.Text = "Open Image";
            this.menuItemOpenImage.Click += new System.EventHandler(this.menuItemOpenImage_Click);
            // 
            // menuItemExit
            // 
            this.menuItemExit.Index = 2;
            this.menuItemExit.Text = "Exit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Index = 1;
            this.menuItemAbout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemAboutIX,
            this.menuItemAboutTN});
            this.menuItemAbout.Text = "&About";
            // 
            // menuItemAboutIX
            // 
            this.menuItemAboutIX.Index = 0;
            this.menuItemAboutIX.Text = "Imag&Xpress";
            this.menuItemAboutIX.Click += new System.EventHandler(this.menuItemAboutIX_Click);
            // 
            // menuItemAboutTN
            // 
            this.menuItemAboutTN.Index = 1;
            this.menuItemAboutTN.Text = "&ThumbnailXpress";
            this.menuItemAboutTN.Click += new System.EventHandler(this.menuItemAboutTN_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(8, 462);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(92, 29);
            this.buttonClear.TabIndex = 1;
            this.buttonClear.Text = "Clear Images";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonListSelected
            // 
            this.buttonListSelected.Location = new System.Drawing.Point(8, 497);
            this.buttonListSelected.Name = "buttonListSelected";
            this.buttonListSelected.Size = new System.Drawing.Size(134, 30);
            this.buttonListSelected.TabIndex = 9;
            this.buttonListSelected.Text = "List Selected Items";
            this.buttonListSelected.Click += new System.EventHandler(this.buttonListSelected_Click);
            // 
            // checkedListBoxSelected
            // 
            this.checkedListBoxSelected.Location = new System.Drawing.Point(148, 497);
            this.checkedListBoxSelected.Name = "checkedListBoxSelected";
            this.checkedListBoxSelected.Size = new System.Drawing.Size(708, 79);
            this.checkedListBoxSelected.TabIndex = 11;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(8, 533);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(134, 30);
            this.buttonRemove.TabIndex = 12;
            this.buttonRemove.Text = "Remove Selected Items";
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // textBoxSampleDescription
            // 
            this.textBoxSampleDescription.Location = new System.Drawing.Point(344, 0);
            this.textBoxSampleDescription.Name = "textBoxSampleDescription";
            this.textBoxSampleDescription.Size = new System.Drawing.Size(512, 152);
            this.textBoxSampleDescription.TabIndex = 16;
            this.textBoxSampleDescription.Text = resources.GetString("textBoxSampleDescription.Text");
            // 
            // labelLastErrorDescription
            // 
            this.labelLastErrorDescription.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelLastErrorDescription.Location = new System.Drawing.Point(148, 595);
            this.labelLastErrorDescription.Name = "labelLastErrorDescription";
            this.labelLastErrorDescription.Size = new System.Drawing.Size(708, 35);
            this.labelLastErrorDescription.TabIndex = 17;
            // 
            // labelLastError
            // 
            this.labelLastError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLastError.Location = new System.Drawing.Point(8, 595);
            this.labelLastError.Name = "labelLastError";
            this.labelLastError.Size = new System.Drawing.Size(136, 24);
            this.labelLastError.TabIndex = 18;
            this.labelLastError.Text = "Last Error Reported:";
            this.labelLastError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabFilters);
            this.tabControl1.Controls.Add(this.tabCells);
            this.tabControl1.Controls.Add(this.tabProcessing);
            this.tabControl1.Controls.Add(this.tabSelectionMode);
            this.tabControl1.Controls.Add(this.tabScrolling);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(8, 96);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(328, 360);
            this.tabControl1.TabIndex = 21;
            // 
            // tabFilters
            // 
            this.tabFilters.Controls.Add(this.labelFileDate);
            this.tabFilters.Controls.Add(this.labelFileNamePattern);
            this.tabFilters.Controls.Add(this.textBoxFileNamePattern);
            this.tabFilters.Controls.Add(this.checkBoxIncludeParentDir);
            this.tabFilters.Controls.Add(this.textBoxFileSize);
            this.tabFilters.Controls.Add(this.labelFileSize);
            this.tabFilters.Controls.Add(this.dateTimePickerFileDate);
            this.tabFilters.Controls.Add(this.checkBoxIncludeSubDir);
            this.tabFilters.Controls.Add(this.checkBoxEnabled);
            this.tabFilters.Controls.Add(this.checkBoxIncludeHidden);
            this.tabFilters.Controls.Add(this.checkBoxMatchCaseSensitive);
            this.tabFilters.Controls.Add(this.checkBoxMatchDir);
            this.tabFilters.Controls.Add(this.labelCompare);
            this.tabFilters.Controls.Add(this.labelFilter);
            this.tabFilters.Controls.Add(this.comboBoxFilter);
            this.tabFilters.Controls.Add(this.comboBoxCompare);
            this.tabFilters.Location = new System.Drawing.Point(4, 22);
            this.tabFilters.Name = "tabFilters";
            this.tabFilters.Size = new System.Drawing.Size(320, 334);
            this.tabFilters.TabIndex = 1;
            this.tabFilters.Text = "Filters";
            this.tabFilters.UseVisualStyleBackColor = true;
            // 
            // labelFileDate
            // 
            this.labelFileDate.AutoSize = true;
            this.labelFileDate.Location = new System.Drawing.Point(8, 119);
            this.labelFileDate.Name = "labelFileDate";
            this.labelFileDate.Size = new System.Drawing.Size(52, 13);
            this.labelFileDate.TabIndex = 20;
            this.labelFileDate.Text = "File Date:";
            // 
            // labelFileNamePattern
            // 
            this.labelFileNamePattern.AutoSize = true;
            this.labelFileNamePattern.Location = new System.Drawing.Point(8, 149);
            this.labelFileNamePattern.Name = "labelFileNamePattern";
            this.labelFileNamePattern.Size = new System.Drawing.Size(94, 13);
            this.labelFileNamePattern.TabIndex = 19;
            this.labelFileNamePattern.Text = "File Name Pattern:";
            // 
            // textBoxFileNamePattern
            // 
            this.textBoxFileNamePattern.Location = new System.Drawing.Point(112, 146);
            this.textBoxFileNamePattern.Name = "textBoxFileNamePattern";
            this.textBoxFileNamePattern.Size = new System.Drawing.Size(191, 20);
            this.textBoxFileNamePattern.TabIndex = 18;
            // 
            // checkBoxIncludeParentDir
            // 
            this.checkBoxIncludeParentDir.Location = new System.Drawing.Point(24, 270);
            this.checkBoxIncludeParentDir.Name = "checkBoxIncludeParentDir";
            this.checkBoxIncludeParentDir.Size = new System.Drawing.Size(160, 23);
            this.checkBoxIncludeParentDir.TabIndex = 17;
            this.checkBoxIncludeParentDir.Text = "Include Parent Directory";
            this.checkBoxIncludeParentDir.CheckedChanged += new System.EventHandler(this.checkBoxIncludeParentDir_CheckedChanged);
            // 
            // textBoxFileSize
            // 
            this.textBoxFileSize.Enabled = false;
            this.textBoxFileSize.Location = new System.Drawing.Point(112, 80);
            this.textBoxFileSize.Name = "textBoxFileSize";
            this.textBoxFileSize.Size = new System.Drawing.Size(191, 20);
            this.textBoxFileSize.TabIndex = 16;
            // 
            // labelFileSize
            // 
            this.labelFileSize.Location = new System.Drawing.Point(8, 83);
            this.labelFileSize.Name = "labelFileSize";
            this.labelFileSize.Size = new System.Drawing.Size(114, 16);
            this.labelFileSize.TabIndex = 15;
            this.labelFileSize.Text = "File Size (in bytes):";
            // 
            // dateTimePickerFileDate
            // 
            this.dateTimePickerFileDate.Enabled = false;
            this.dateTimePickerFileDate.Location = new System.Drawing.Point(112, 113);
            this.dateTimePickerFileDate.Name = "dateTimePickerFileDate";
            this.dateTimePickerFileDate.Size = new System.Drawing.Size(191, 20);
            this.dateTimePickerFileDate.TabIndex = 14;
            this.dateTimePickerFileDate.ValueChanged += new System.EventHandler(this.dateTimePickerFileDate_ValueChanged);
            // 
            // checkBoxIncludeSubDir
            // 
            this.checkBoxIncludeSubDir.Enabled = false;
            this.checkBoxIncludeSubDir.Location = new System.Drawing.Point(24, 294);
            this.checkBoxIncludeSubDir.Name = "checkBoxIncludeSubDir";
            this.checkBoxIncludeSubDir.Size = new System.Drawing.Size(136, 21);
            this.checkBoxIncludeSubDir.TabIndex = 13;
            this.checkBoxIncludeSubDir.Text = "Include Subdirectory";
            this.checkBoxIncludeSubDir.CheckedChanged += new System.EventHandler(this.checkBoxIncludeSubDir_CheckedChanged);
            // 
            // checkBoxEnabled
            // 
            this.checkBoxEnabled.Checked = true;
            this.checkBoxEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxEnabled.Location = new System.Drawing.Point(24, 248);
            this.checkBoxEnabled.Name = "checkBoxEnabled";
            this.checkBoxEnabled.Size = new System.Drawing.Size(136, 16);
            this.checkBoxEnabled.TabIndex = 11;
            this.checkBoxEnabled.Text = "Enabled";
            this.checkBoxEnabled.CheckedChanged += new System.EventHandler(this.checkBoxEnabled_CheckedChanged);
            // 
            // checkBoxIncludeHidden
            // 
            this.checkBoxIncludeHidden.Enabled = false;
            this.checkBoxIncludeHidden.Location = new System.Drawing.Point(24, 224);
            this.checkBoxIncludeHidden.Name = "checkBoxIncludeHidden";
            this.checkBoxIncludeHidden.Size = new System.Drawing.Size(144, 16);
            this.checkBoxIncludeHidden.TabIndex = 10;
            this.checkBoxIncludeHidden.Text = "Include Hidden";
            this.checkBoxIncludeHidden.CheckedChanged += new System.EventHandler(this.checkBoxIncludeHiddenDir_CheckedChanged);
            // 
            // checkBoxMatchCaseSensitive
            // 
            this.checkBoxMatchCaseSensitive.Enabled = false;
            this.checkBoxMatchCaseSensitive.Location = new System.Drawing.Point(24, 200);
            this.checkBoxMatchCaseSensitive.Name = "checkBoxMatchCaseSensitive";
            this.checkBoxMatchCaseSensitive.Size = new System.Drawing.Size(152, 16);
            this.checkBoxMatchCaseSensitive.TabIndex = 9;
            this.checkBoxMatchCaseSensitive.Text = "Match is Case Sensitive";
            this.checkBoxMatchCaseSensitive.CheckedChanged += new System.EventHandler(this.checkBoxMatchCaseSensitive_CheckedChanged);
            // 
            // checkBoxMatchDir
            // 
            this.checkBoxMatchDir.Enabled = false;
            this.checkBoxMatchDir.Location = new System.Drawing.Point(24, 176);
            this.checkBoxMatchDir.Name = "checkBoxMatchDir";
            this.checkBoxMatchDir.Size = new System.Drawing.Size(160, 18);
            this.checkBoxMatchDir.TabIndex = 8;
            this.checkBoxMatchDir.Text = "Match Applies to Directory";
            this.checkBoxMatchDir.Click += new System.EventHandler(this.checkBoxMatchDir_Click);
            this.checkBoxMatchDir.CheckedChanged += new System.EventHandler(this.checkBoxMatchDir_CheckedChanged);
            // 
            // labelCompare
            // 
            this.labelCompare.Location = new System.Drawing.Point(8, 51);
            this.labelCompare.Name = "labelCompare";
            this.labelCompare.Size = new System.Drawing.Size(94, 18);
            this.labelCompare.TabIndex = 6;
            this.labelCompare.Text = "Compare Type:";
            // 
            // labelFilter
            // 
            this.labelFilter.Location = new System.Drawing.Point(8, 18);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(80, 16);
            this.labelFilter.TabIndex = 5;
            this.labelFilter.Text = "Filter Type:";
            // 
            // comboBoxFilter
            // 
            this.comboBoxFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFilter.Items.AddRange(new object[] {
            "File Name Match",
            "File Attributes and Directory",
            "File Creation Date",
            "File Modification Date",
            "FileSize",
            "ImagXpress Supported File"});
            this.comboBoxFilter.Location = new System.Drawing.Point(112, 16);
            this.comboBoxFilter.Name = "comboBoxFilter";
            this.comboBoxFilter.Size = new System.Drawing.Size(191, 21);
            this.comboBoxFilter.TabIndex = 4;
            this.comboBoxFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxFilter_SelectedIndexChanged);
            // 
            // comboBoxCompare
            // 
            this.comboBoxCompare.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCompare.Enabled = false;
            this.comboBoxCompare.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboBoxCompare.Items.AddRange(new object[] {
            "LessThan",
            "LessThan or Equal",
            "Equal",
            "GreaterThan or Equal",
            "GreaterThan"});
            this.comboBoxCompare.Location = new System.Drawing.Point(112, 48);
            this.comboBoxCompare.Name = "comboBoxCompare";
            this.comboBoxCompare.Size = new System.Drawing.Size(191, 21);
            this.comboBoxCompare.TabIndex = 0;
            // 
            // tabCells
            // 
            this.tabCells.Controls.Add(this.labelCellWidthValue);
            this.tabCells.Controls.Add(this.labelCellWidth);
            this.tabCells.Controls.Add(this.hScrollBarCellWidth);
            this.tabCells.Controls.Add(this.labelCellHorizontalSpacing);
            this.tabCells.Controls.Add(this.hScrollBarCellVerticalSpacing);
            this.tabCells.Controls.Add(this.labelCellVerticalSpacingValue);
            this.tabCells.Controls.Add(this.labelCellVerticalSpacing);
            this.tabCells.Controls.Add(this.hScrollBarCellHorizontalSpacing);
            this.tabCells.Controls.Add(this.labelCellHorizontalSpacingValue);
            this.tabCells.Controls.Add(this.labelCellHeightValue);
            this.tabCells.Controls.Add(this.hScrollBarCellHeight);
            this.tabCells.Controls.Add(this.labelCellHeight);
            this.tabCells.Controls.Add(this.labelCellBorderWidth);
            this.tabCells.Controls.Add(this.buttonChooseCellColor);
            this.tabCells.Controls.Add(this.buttonSet);
            this.tabCells.Controls.Add(this.labelCellBorderWidthValue);
            this.tabCells.Controls.Add(this.hScrollBarCellBorderWidth);
            this.tabCells.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCells.Location = new System.Drawing.Point(4, 22);
            this.tabCells.Name = "tabCells";
            this.tabCells.Size = new System.Drawing.Size(320, 334);
            this.tabCells.TabIndex = 0;
            this.tabCells.Text = "Thumbnail Attributes";
            this.tabCells.UseVisualStyleBackColor = true;
            // 
            // labelCellWidthValue
            // 
            this.labelCellWidthValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelCellWidthValue.Location = new System.Drawing.Point(197, 70);
            this.labelCellWidthValue.Name = "labelCellWidthValue";
            this.labelCellWidthValue.Size = new System.Drawing.Size(56, 16);
            this.labelCellWidthValue.TabIndex = 22;
            // 
            // labelCellWidth
            // 
            this.labelCellWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellWidth.Location = new System.Drawing.Point(21, 46);
            this.labelCellWidth.Name = "labelCellWidth";
            this.labelCellWidth.Size = new System.Drawing.Size(144, 16);
            this.labelCellWidth.TabIndex = 21;
            this.labelCellWidth.Text = "Cell Width";
            // 
            // hScrollBarCellWidth
            // 
            this.hScrollBarCellWidth.Location = new System.Drawing.Point(21, 70);
            this.hScrollBarCellWidth.Minimum = 32;
            this.hScrollBarCellWidth.Name = "hScrollBarCellWidth";
            this.hScrollBarCellWidth.Size = new System.Drawing.Size(160, 16);
            this.hScrollBarCellWidth.TabIndex = 20;
            this.hScrollBarCellWidth.Value = 32;
            this.hScrollBarCellWidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBarCellWidth_Scroll);
            // 
            // labelCellHorizontalSpacing
            // 
            this.labelCellHorizontalSpacing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellHorizontalSpacing.Location = new System.Drawing.Point(21, 142);
            this.labelCellHorizontalSpacing.Name = "labelCellHorizontalSpacing";
            this.labelCellHorizontalSpacing.Size = new System.Drawing.Size(136, 16);
            this.labelCellHorizontalSpacing.TabIndex = 19;
            this.labelCellHorizontalSpacing.Text = "Cell Horizontal Spacing";
            // 
            // hScrollBarCellVerticalSpacing
            // 
            this.hScrollBarCellVerticalSpacing.Location = new System.Drawing.Point(21, 214);
            this.hScrollBarCellVerticalSpacing.Name = "hScrollBarCellVerticalSpacing";
            this.hScrollBarCellVerticalSpacing.Size = new System.Drawing.Size(160, 16);
            this.hScrollBarCellVerticalSpacing.TabIndex = 18;
            this.hScrollBarCellVerticalSpacing.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBarCellVerticalSpacing_Scroll);
            // 
            // labelCellVerticalSpacingValue
            // 
            this.labelCellVerticalSpacingValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelCellVerticalSpacingValue.Location = new System.Drawing.Point(197, 214);
            this.labelCellVerticalSpacingValue.Name = "labelCellVerticalSpacingValue";
            this.labelCellVerticalSpacingValue.Size = new System.Drawing.Size(56, 16);
            this.labelCellVerticalSpacingValue.TabIndex = 15;
            // 
            // labelCellVerticalSpacing
            // 
            this.labelCellVerticalSpacing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellVerticalSpacing.Location = new System.Drawing.Point(21, 190);
            this.labelCellVerticalSpacing.Name = "labelCellVerticalSpacing";
            this.labelCellVerticalSpacing.Size = new System.Drawing.Size(160, 16);
            this.labelCellVerticalSpacing.TabIndex = 14;
            this.labelCellVerticalSpacing.Text = "Cell Vertical Spacing";
            // 
            // hScrollBarCellHorizontalSpacing
            // 
            this.hScrollBarCellHorizontalSpacing.Location = new System.Drawing.Point(21, 166);
            this.hScrollBarCellHorizontalSpacing.Maximum = 90;
            this.hScrollBarCellHorizontalSpacing.Minimum = 1;
            this.hScrollBarCellHorizontalSpacing.Name = "hScrollBarCellHorizontalSpacing";
            this.hScrollBarCellHorizontalSpacing.Size = new System.Drawing.Size(160, 16);
            this.hScrollBarCellHorizontalSpacing.TabIndex = 10;
            this.hScrollBarCellHorizontalSpacing.Value = 1;
            this.hScrollBarCellHorizontalSpacing.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBarCellHorizontalSpacing_Scroll);
            // 
            // labelCellHorizontalSpacingValue
            // 
            this.labelCellHorizontalSpacingValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelCellHorizontalSpacingValue.Location = new System.Drawing.Point(197, 166);
            this.labelCellHorizontalSpacingValue.Name = "labelCellHorizontalSpacingValue";
            this.labelCellHorizontalSpacingValue.Size = new System.Drawing.Size(56, 16);
            this.labelCellHorizontalSpacingValue.TabIndex = 9;
            // 
            // labelCellHeightValue
            // 
            this.labelCellHeightValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellHeightValue.Location = new System.Drawing.Point(197, 118);
            this.labelCellHeightValue.Name = "labelCellHeightValue";
            this.labelCellHeightValue.Size = new System.Drawing.Size(56, 16);
            this.labelCellHeightValue.TabIndex = 7;
            // 
            // hScrollBarCellHeight
            // 
            this.hScrollBarCellHeight.LargeChange = 2;
            this.hScrollBarCellHeight.Location = new System.Drawing.Point(21, 118);
            this.hScrollBarCellHeight.Minimum = 32;
            this.hScrollBarCellHeight.Name = "hScrollBarCellHeight";
            this.hScrollBarCellHeight.Size = new System.Drawing.Size(160, 16);
            this.hScrollBarCellHeight.TabIndex = 6;
            this.hScrollBarCellHeight.Value = 32;
            this.hScrollBarCellHeight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBarCellHeight_Scroll);
            // 
            // labelCellHeight
            // 
            this.labelCellHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellHeight.Location = new System.Drawing.Point(21, 94);
            this.labelCellHeight.Name = "labelCellHeight";
            this.labelCellHeight.Size = new System.Drawing.Size(104, 16);
            this.labelCellHeight.TabIndex = 5;
            this.labelCellHeight.Text = "Cell Height";
            // 
            // labelCellBorderWidth
            // 
            this.labelCellBorderWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellBorderWidth.Location = new System.Drawing.Point(21, 241);
            this.labelCellBorderWidth.Name = "labelCellBorderWidth";
            this.labelCellBorderWidth.Size = new System.Drawing.Size(136, 16);
            this.labelCellBorderWidth.TabIndex = 4;
            this.labelCellBorderWidth.Text = "Cell Border Width ";
            // 
            // buttonChooseCellColor
            // 
            this.buttonChooseCellColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChooseCellColor.Location = new System.Drawing.Point(24, 16);
            this.buttonChooseCellColor.Name = "buttonChooseCellColor";
            this.buttonChooseCellColor.Size = new System.Drawing.Size(157, 24);
            this.buttonChooseCellColor.TabIndex = 3;
            this.buttonChooseCellColor.Text = "Choose Cell Color";
            this.buttonChooseCellColor.Click += new System.EventHandler(this.buttonChooseCellColor_Click);
            // 
            // buttonSet
            // 
            this.buttonSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSet.Location = new System.Drawing.Point(24, 295);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(157, 24);
            this.buttonSet.TabIndex = 2;
            this.buttonSet.Text = "Invoke Settings";
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // labelCellBorderWidthValue
            // 
            this.labelCellBorderWidthValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCellBorderWidthValue.Location = new System.Drawing.Point(197, 265);
            this.labelCellBorderWidthValue.Name = "labelCellBorderWidthValue";
            this.labelCellBorderWidthValue.Size = new System.Drawing.Size(56, 16);
            this.labelCellBorderWidthValue.TabIndex = 1;
            // 
            // hScrollBarCellBorderWidth
            // 
            this.hScrollBarCellBorderWidth.LargeChange = 2;
            this.hScrollBarCellBorderWidth.Location = new System.Drawing.Point(21, 265);
            this.hScrollBarCellBorderWidth.Maximum = 20;
            this.hScrollBarCellBorderWidth.Minimum = 1;
            this.hScrollBarCellBorderWidth.Name = "hScrollBarCellBorderWidth";
            this.hScrollBarCellBorderWidth.Size = new System.Drawing.Size(160, 16);
            this.hScrollBarCellBorderWidth.TabIndex = 0;
            this.hScrollBarCellBorderWidth.Value = 1;
            this.hScrollBarCellBorderWidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBarCellBorderWidth_Scroll);
            // 
            // tabProcessing
            // 
            this.tabProcessing.Controls.Add(this.groupBoxThreads);
            this.tabProcessing.Controls.Add(this.labelMaxThreads);
            this.tabProcessing.Controls.Add(this.numericUpDownMaxThreads);
            this.tabProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabProcessing.Location = new System.Drawing.Point(4, 22);
            this.tabProcessing.Name = "tabProcessing";
            this.tabProcessing.Size = new System.Drawing.Size(320, 334);
            this.tabProcessing.TabIndex = 4;
            this.tabProcessing.Text = "Thread Processing";
            this.tabProcessing.UseVisualStyleBackColor = true;
            // 
            // groupBoxThreads
            // 
            this.groupBoxThreads.Controls.Add(this.textBoxHungThreadThreshold);
            this.groupBoxThreads.Controls.Add(this.textBoxStartThreadThreshold);
            this.groupBoxThreads.Controls.Add(this.labelHungThreadThreshold);
            this.groupBoxThreads.Controls.Add(this.labelStartThreadThreshold);
            this.groupBoxThreads.Controls.Add(this.buttonSetThreads);
            this.groupBoxThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxThreads.Location = new System.Drawing.Point(16, 56);
            this.groupBoxThreads.Name = "groupBoxThreads";
            this.groupBoxThreads.Size = new System.Drawing.Size(240, 152);
            this.groupBoxThreads.TabIndex = 2;
            this.groupBoxThreads.TabStop = false;
            this.groupBoxThreads.Text = "Thread Time Thresholds";
            // 
            // textBoxHungThreadThreshold
            // 
            this.textBoxHungThreadThreshold.Location = new System.Drawing.Point(152, 88);
            this.textBoxHungThreadThreshold.Name = "textBoxHungThreadThreshold";
            this.textBoxHungThreadThreshold.Size = new System.Drawing.Size(64, 20);
            this.textBoxHungThreadThreshold.TabIndex = 4;
            // 
            // textBoxStartThreadThreshold
            // 
            this.textBoxStartThreadThreshold.Location = new System.Drawing.Point(152, 56);
            this.textBoxStartThreadThreshold.Name = "textBoxStartThreadThreshold";
            this.textBoxStartThreadThreshold.Size = new System.Drawing.Size(64, 20);
            this.textBoxStartThreadThreshold.TabIndex = 3;
            // 
            // labelHungThreadThreshold
            // 
            this.labelHungThreadThreshold.Location = new System.Drawing.Point(8, 88);
            this.labelHungThreadThreshold.Name = "labelHungThreadThreshold";
            this.labelHungThreadThreshold.Size = new System.Drawing.Size(112, 32);
            this.labelHungThreadThreshold.TabIndex = 2;
            this.labelHungThreadThreshold.Text = "Hung Thread Threshold";
            // 
            // labelStartThreadThreshold
            // 
            this.labelStartThreadThreshold.Location = new System.Drawing.Point(8, 56);
            this.labelStartThreadThreshold.Name = "labelStartThreadThreshold";
            this.labelStartThreadThreshold.Size = new System.Drawing.Size(120, 16);
            this.labelStartThreadThreshold.TabIndex = 1;
            this.labelStartThreadThreshold.Text = "Start Thread Threshold";
            // 
            // buttonSetThreads
            // 
            this.buttonSetThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSetThreads.Location = new System.Drawing.Point(128, 16);
            this.buttonSetThreads.Name = "buttonSetThreads";
            this.buttonSetThreads.Size = new System.Drawing.Size(104, 32);
            this.buttonSetThreads.TabIndex = 0;
            this.buttonSetThreads.Text = "Set Thread Values";
            this.buttonSetThreads.Click += new System.EventHandler(this.buttonSetThreads_Click);
            // 
            // labelMaxThreads
            // 
            this.labelMaxThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaxThreads.Location = new System.Drawing.Point(24, 16);
            this.labelMaxThreads.Name = "labelMaxThreads";
            this.labelMaxThreads.Size = new System.Drawing.Size(80, 16);
            this.labelMaxThreads.TabIndex = 1;
            this.labelMaxThreads.Text = "Max Threads:";
            // 
            // numericUpDownMaxThreads
            // 
            this.numericUpDownMaxThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMaxThreads.Location = new System.Drawing.Point(128, 16);
            this.numericUpDownMaxThreads.Maximum = new decimal(new int[] {
            33,
            0,
            0,
            0});
            this.numericUpDownMaxThreads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxThreads.Name = "numericUpDownMaxThreads";
            this.numericUpDownMaxThreads.Size = new System.Drawing.Size(48, 20);
            this.numericUpDownMaxThreads.TabIndex = 0;
            this.numericUpDownMaxThreads.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMaxThreads.ValueChanged += new System.EventHandler(this.numericUpDownMaxThreads_ValueChanged);
            // 
            // tabSelectionMode
            // 
            this.tabSelectionMode.Controls.Add(this.labelSelectionMode);
            this.tabSelectionMode.Controls.Add(this.comboBoxSelectionMode);
            this.tabSelectionMode.Location = new System.Drawing.Point(4, 22);
            this.tabSelectionMode.Name = "tabSelectionMode";
            this.tabSelectionMode.Size = new System.Drawing.Size(320, 334);
            this.tabSelectionMode.TabIndex = 3;
            this.tabSelectionMode.Text = "SelectionMode";
            this.tabSelectionMode.UseVisualStyleBackColor = true;
            // 
            // labelSelectionMode
            // 
            this.labelSelectionMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectionMode.Location = new System.Drawing.Point(72, 48);
            this.labelSelectionMode.Name = "labelSelectionMode";
            this.labelSelectionMode.Size = new System.Drawing.Size(128, 32);
            this.labelSelectionMode.TabIndex = 1;
            this.labelSelectionMode.Text = "Selection Mode";
            this.labelSelectionMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxSelectionMode
            // 
            this.comboBoxSelectionMode.Items.AddRange(new object[] {
            "MultiExtended",
            "MultiSimple",
            "None",
            "Single"});
            this.comboBoxSelectionMode.Location = new System.Drawing.Point(24, 96);
            this.comboBoxSelectionMode.Name = "comboBoxSelectionMode";
            this.comboBoxSelectionMode.Size = new System.Drawing.Size(216, 21);
            this.comboBoxSelectionMode.TabIndex = 0;
            this.comboBoxSelectionMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxSelectionMode_SelectedIndexChanged);
            // 
            // tabScrolling
            // 
            this.tabScrolling.Controls.Add(this.groupBoxScrollInc);
            this.tabScrolling.Controls.Add(this.groupBoxScrolling);
            this.tabScrolling.Location = new System.Drawing.Point(4, 22);
            this.tabScrolling.Name = "tabScrolling";
            this.tabScrolling.Padding = new System.Windows.Forms.Padding(3);
            this.tabScrolling.Size = new System.Drawing.Size(320, 334);
            this.tabScrolling.TabIndex = 5;
            this.tabScrolling.Text = "Scrolling";
            this.tabScrolling.UseVisualStyleBackColor = true;
            // 
            // groupBoxScrollInc
            // 
            this.groupBoxScrollInc.Controls.Add(this.buttonLargeIncrement);
            this.groupBoxScrollInc.Controls.Add(this.buttonLargeDecrement);
            this.groupBoxScrollInc.Controls.Add(this.buttonSmallDecrement);
            this.groupBoxScrollInc.Controls.Add(this.buttonSmallIncrement);
            this.groupBoxScrollInc.Location = new System.Drawing.Point(31, 159);
            this.groupBoxScrollInc.Name = "groupBoxScrollInc";
            this.groupBoxScrollInc.Size = new System.Drawing.Size(242, 56);
            this.groupBoxScrollInc.TabIndex = 12;
            this.groupBoxScrollInc.TabStop = false;
            this.groupBoxScrollInc.Text = "Scroll by lines and pages";
            // 
            // buttonLargeIncrement
            // 
            this.buttonLargeIncrement.Location = new System.Drawing.Point(186, 19);
            this.buttonLargeIncrement.Name = "buttonLargeIncrement";
            this.buttonLargeIncrement.Size = new System.Drawing.Size(35, 23);
            this.buttonLargeIncrement.TabIndex = 10;
            this.buttonLargeIncrement.Text = ">>";
            this.buttonLargeIncrement.UseVisualStyleBackColor = true;
            this.buttonLargeIncrement.Click += new System.EventHandler(this.buttonLargeIncrement_Click);
            // 
            // buttonLargeDecrement
            // 
            this.buttonLargeDecrement.Location = new System.Drawing.Point(21, 19);
            this.buttonLargeDecrement.Name = "buttonLargeDecrement";
            this.buttonLargeDecrement.Size = new System.Drawing.Size(35, 23);
            this.buttonLargeDecrement.TabIndex = 10;
            this.buttonLargeDecrement.Text = "<<";
            this.buttonLargeDecrement.UseVisualStyleBackColor = true;
            this.buttonLargeDecrement.Click += new System.EventHandler(this.buttonLargeDecrement_Click);
            // 
            // buttonSmallDecrement
            // 
            this.buttonSmallDecrement.Location = new System.Drawing.Point(77, 19);
            this.buttonSmallDecrement.Name = "buttonSmallDecrement";
            this.buttonSmallDecrement.Size = new System.Drawing.Size(35, 23);
            this.buttonSmallDecrement.TabIndex = 10;
            this.buttonSmallDecrement.Text = "<";
            this.buttonSmallDecrement.UseVisualStyleBackColor = true;
            this.buttonSmallDecrement.Click += new System.EventHandler(this.buttonSmallDecrement_Click);
            // 
            // buttonSmallIncrement
            // 
            this.buttonSmallIncrement.Location = new System.Drawing.Point(133, 19);
            this.buttonSmallIncrement.Name = "buttonSmallIncrement";
            this.buttonSmallIncrement.Size = new System.Drawing.Size(35, 23);
            this.buttonSmallIncrement.TabIndex = 10;
            this.buttonSmallIncrement.Text = ">";
            this.buttonSmallIncrement.UseVisualStyleBackColor = true;
            this.buttonSmallIncrement.Click += new System.EventHandler(this.buttonSmallIncrement_Click);
            // 
            // groupBoxScrolling
            // 
            this.groupBoxScrolling.Controls.Add(this.buttonScroll);
            this.groupBoxScrolling.Controls.Add(this.comboBoxScrollTo);
            this.groupBoxScrolling.Controls.Add(this.textBoxScrollAmount);
            this.groupBoxScrolling.Location = new System.Drawing.Point(31, 29);
            this.groupBoxScrolling.Name = "groupBoxScrolling";
            this.groupBoxScrolling.Size = new System.Drawing.Size(242, 97);
            this.groupBoxScrolling.TabIndex = 11;
            this.groupBoxScrolling.TabStop = false;
            this.groupBoxScrolling.Text = "Scroll to Position";
            // 
            // buttonScroll
            // 
            this.buttonScroll.Location = new System.Drawing.Point(55, 57);
            this.buttonScroll.Name = "buttonScroll";
            this.buttonScroll.Size = new System.Drawing.Size(134, 23);
            this.buttonScroll.TabIndex = 3;
            this.buttonScroll.Text = "Scroll";
            this.buttonScroll.UseVisualStyleBackColor = true;
            this.buttonScroll.Click += new System.EventHandler(this.buttonScroll_Click);
            // 
            // comboBoxScrollTo
            // 
            this.comboBoxScrollTo.FormattingEnabled = true;
            this.comboBoxScrollTo.Items.AddRange(new object[] {
            "Scroll to Item...",
            "Scroll to Row..."});
            this.comboBoxScrollTo.Location = new System.Drawing.Point(6, 19);
            this.comboBoxScrollTo.Name = "comboBoxScrollTo";
            this.comboBoxScrollTo.Size = new System.Drawing.Size(153, 21);
            this.comboBoxScrollTo.TabIndex = 2;
            this.comboBoxScrollTo.Text = "(Select scrolling method)";
            // 
            // textBoxScrollAmount
            // 
            this.textBoxScrollAmount.Location = new System.Drawing.Point(175, 19);
            this.textBoxScrollAmount.Name = "textBoxScrollAmount";
            this.textBoxScrollAmount.Size = new System.Drawing.Size(46, 20);
            this.textBoxScrollAmount.TabIndex = 0;
            this.textBoxScrollAmount.Text = "0";
            // 
            // checkBoxExpand
            // 
            this.checkBoxExpand.Location = new System.Drawing.Point(8, 8);
            this.checkBoxExpand.Name = "checkBoxExpand";
            this.checkBoxExpand.Size = new System.Drawing.Size(136, 40);
            this.checkBoxExpand.TabIndex = 22;
            this.checkBoxExpand.Text = "Expand MultiPage";
            // 
            // checkBoxPreserveBlack
            // 
            this.checkBoxPreserveBlack.Checked = true;
            this.checkBoxPreserveBlack.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPreserveBlack.Location = new System.Drawing.Point(8, 56);
            this.checkBoxPreserveBlack.Name = "checkBoxPreserveBlack";
            this.checkBoxPreserveBlack.Size = new System.Drawing.Size(144, 24);
            this.checkBoxPreserveBlack.TabIndex = 26;
            this.checkBoxPreserveBlack.Text = "Preserve Black";
            this.checkBoxPreserveBlack.CheckedChanged += new System.EventHandler(this.checkBoxPreserveBlack_CheckedChanged);
            // 
            // checkBoxCameraRaw
            // 
            this.checkBoxCameraRaw.Checked = true;
            this.checkBoxCameraRaw.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCameraRaw.Location = new System.Drawing.Point(168, 56);
            this.checkBoxCameraRaw.Name = "checkBoxCameraRaw";
            this.checkBoxCameraRaw.Size = new System.Drawing.Size(136, 24);
            this.checkBoxCameraRaw.TabIndex = 27;
            this.checkBoxCameraRaw.Text = "Camera Raw";
            this.checkBoxCameraRaw.CheckedChanged += new System.EventHandler(this.checkBoxCameraRaw_CheckedChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Enabled = false;
            this.buttonCancel.Location = new System.Drawing.Point(160, 8);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(144, 32);
            this.buttonCancel.TabIndex = 28;
            this.buttonCancel.Text = "Cancel Thumbnail Loading";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkBoxTNDrag
            // 
            this.checkBoxTNDrag.AutoSize = true;
            this.checkBoxTNDrag.Location = new System.Drawing.Point(313, 462);
            this.checkBoxTNDrag.Name = "checkBoxTNDrag";
            this.checkBoxTNDrag.Size = new System.Drawing.Size(200, 17);
            this.checkBoxTNDrag.TabIndex = 46;
            this.checkBoxTNDrag.Text = "Allow Dragging between TN Controls";
            this.checkBoxTNDrag.UseVisualStyleBackColor = true;
            this.checkBoxTNDrag.CheckedChanged += new System.EventHandler(this.checkBoxTNDrag_CheckedChanged);
            // 
            // checkBoxThumbnailShift
            // 
            this.checkBoxThumbnailShift.AutoSize = true;
            this.checkBoxThumbnailShift.Location = new System.Drawing.Point(715, 462);
            this.checkBoxThumbnailShift.Name = "checkBoxThumbnailShift";
            this.checkBoxThumbnailShift.Size = new System.Drawing.Size(141, 17);
            this.checkBoxThumbnailShift.TabIndex = 45;
            this.checkBoxThumbnailShift.Text = "Allow Thumbnail Shifting";
            this.checkBoxThumbnailShift.UseVisualStyleBackColor = true;
            this.checkBoxThumbnailShift.CheckedChanged += new System.EventHandler(this.checkBoxThumbnailShift_CheckedChanged);
            // 
            // checkBoxDrillDown
            // 
            this.checkBoxDrillDown.AutoSize = true;
            this.checkBoxDrillDown.Location = new System.Drawing.Point(124, 462);
            this.checkBoxDrillDown.Name = "checkBoxDrillDown";
            this.checkBoxDrillDown.Size = new System.Drawing.Size(147, 17);
            this.checkBoxDrillDown.TabIndex = 44;
            this.checkBoxDrillDown.Text = "Allow Directory Drill-Down";
            this.checkBoxDrillDown.UseVisualStyleBackColor = true;
            this.checkBoxDrillDown.CheckedChanged += new System.EventHandler(this.checkBoxDrillDown_CheckedChanged);
            // 
            // checkBoxOleDrag
            // 
            this.checkBoxOleDrag.AutoSize = true;
            this.checkBoxOleDrag.Location = new System.Drawing.Point(560, 462);
            this.checkBoxOleDrag.Name = "checkBoxOleDrag";
            this.checkBoxOleDrag.Size = new System.Drawing.Size(121, 17);
            this.checkBoxOleDrag.TabIndex = 43;
            this.checkBoxOleDrag.Text = "Allow OLE Dragging";
            this.checkBoxOleDrag.UseVisualStyleBackColor = true;
            this.checkBoxOleDrag.CheckedChanged += new System.EventHandler(this.checkBoxOleDrag_CheckedChanged);
            // 
            // thumbnailXpress1
            // 
            this.thumbnailXpress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpress1.BottomMargin = 5;
            this.thumbnailXpress1.CameraRaw = false;
            this.thumbnailXpress1.CellBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.thumbnailXpress1.CellBorderWidth = 1;
            this.thumbnailXpress1.CellHeight = 80;
            this.thumbnailXpress1.CellHorizontalSpacing = 5;
            this.thumbnailXpress1.CellSpacingColor = System.Drawing.Color.White;
            this.thumbnailXpress1.CellVerticalSpacing = 5;
            this.thumbnailXpress1.CellWidth = 80;
            this.thumbnailXpress1.DblClickDirectoryDrillDown = true;
            this.thumbnailXpress1.DescriptorAlignment = ((Accusoft.ThumbnailXpressSdk.DescriptorAlignments)((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter | Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpress1.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.Default;
            this.thumbnailXpress1.EnableAsDragSourceForExternalDragDrop = true;
            this.thumbnailXpress1.EnableAsDropTargetForExternalDragDrop = true;
            this.thumbnailXpress1.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon;
            this.thumbnailXpress1.FtpPassword = "";
            this.thumbnailXpress1.FtpUserName = "";
            this.thumbnailXpress1.InterComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpress1.IntraComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpress1.LeftMargin = 5;
            this.thumbnailXpress1.Location = new System.Drawing.Point(345, 164);
            this.thumbnailXpress1.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpress1.Name = "thumbnailXpress1";
            this.thumbnailXpress1.PreserveBlack = false;
            this.thumbnailXpress1.ProxyServer = "";
            this.thumbnailXpress1.RightMargin = 5;
            this.thumbnailXpress1.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical;
            this.thumbnailXpress1.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
            this.thumbnailXpress1.ShowHourglass = true;
            this.thumbnailXpress1.ShowImagePlaceholders = false;
            this.thumbnailXpress1.Size = new System.Drawing.Size(511, 288);
            this.thumbnailXpress1.TabIndex = 47;
            this.thumbnailXpress1.TextBackColor = System.Drawing.Color.White;
            this.thumbnailXpress1.TopMargin = 5;
            this.thumbnailXpress1.DoubleClick += new System.EventHandler(this.thumbnailXpress1_DoubleClick);
            this.thumbnailXpress1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.thumbnailXpress1_MouseDown);
            // 
            // thumbnailXpress2
            // 
            this.thumbnailXpress2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpress2.BottomMargin = 5;
            this.thumbnailXpress2.CameraRaw = false;
            this.thumbnailXpress2.CellBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.thumbnailXpress2.CellBorderWidth = 1;
            this.thumbnailXpress2.CellHeight = 80;
            this.thumbnailXpress2.CellHorizontalSpacing = 5;
            this.thumbnailXpress2.CellSpacingColor = System.Drawing.Color.White;
            this.thumbnailXpress2.CellVerticalSpacing = 5;
            this.thumbnailXpress2.CellWidth = 80;
            this.thumbnailXpress2.DblClickDirectoryDrillDown = true;
            this.thumbnailXpress2.DescriptorAlignment = ((Accusoft.ThumbnailXpressSdk.DescriptorAlignments)((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter | Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpress2.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.Default;
            this.thumbnailXpress2.EnableAsDragSourceForExternalDragDrop = true;
            this.thumbnailXpress2.EnableAsDropTargetForExternalDragDrop = true;
            this.thumbnailXpress2.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon;
            this.thumbnailXpress2.FtpPassword = "";
            this.thumbnailXpress2.FtpUserName = "";
            this.thumbnailXpress2.InterComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpress2.IntraComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpress2.LeftMargin = 5;
            this.thumbnailXpress2.Location = new System.Drawing.Point(879, 164);
            this.thumbnailXpress2.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpress2.Name = "thumbnailXpress2";
            this.thumbnailXpress2.PreserveBlack = false;
            this.thumbnailXpress2.ProxyServer = "";
            this.thumbnailXpress2.RightMargin = 5;
            this.thumbnailXpress2.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical;
            this.thumbnailXpress2.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.thumbnailXpress2.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
            this.thumbnailXpress2.ShowHourglass = true;
            this.thumbnailXpress2.ShowImagePlaceholders = false;
            this.thumbnailXpress2.Size = new System.Drawing.Size(160, 287);
            this.thumbnailXpress2.TabIndex = 48;
            this.thumbnailXpress2.TextBackColor = System.Drawing.Color.White;
            this.thumbnailXpress2.TopMargin = 5;
            // 
            // formMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1051, 639);
            this.Controls.Add(this.thumbnailXpress2);
            this.Controls.Add(this.thumbnailXpress1);
            this.Controls.Add(this.checkBoxTNDrag);
            this.Controls.Add(this.checkBoxThumbnailShift);
            this.Controls.Add(this.checkBoxDrillDown);
            this.Controls.Add(this.checkBoxOleDrag);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.checkBoxCameraRaw);
            this.Controls.Add(this.checkBoxPreserveBlack);
            this.Controls.Add(this.checkBoxExpand);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.labelLastError);
            this.Controls.Add(this.labelLastErrorDescription);
            this.Controls.Add(this.textBoxSampleDescription);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.checkedListBoxSelected);
            this.Controls.Add(this.buttonListSelected);
            this.Controls.Add(this.buttonClear);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Menu = this.mainMenu1;
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thumbnail Viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabFilters.ResumeLayout(false);
            this.tabFilters.PerformLayout();
            this.tabCells.ResumeLayout(false);
            this.tabProcessing.ResumeLayout(false);
            this.groupBoxThreads.ResumeLayout(false);
            this.groupBoxThreads.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaxThreads)).EndInit();
            this.tabSelectionMode.ResumeLayout(false);
            this.tabScrolling.ResumeLayout(false);
            this.groupBoxScrollInc.ResumeLayout(false);
            this.groupBoxScrolling.ResumeLayout(false);
            this.groupBoxScrolling.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new formMain());
        }

        private void menuItemExit_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void menuItemOpenDir_Click(object sender, System.EventArgs e)
        {
            try
            {
                //clear out the error before the next operation
                labelLastErrorDescription.Text = "";

                //First we grab the filename of the image we want to open
                //create a filter item here

                buttonCancel.Enabled = true;
                if(filterItem == null) filterItem = thumbnailXpress2.CreateFilter("test");

                filterItemCreated = true;
                //open the directory here
                System.String result = OpenThumbnailDir();
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }

            catch (NullReferenceException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void buttonClear_Click(object sender, System.EventArgs e)
        {
            try
            {
                //clear the TN display
                thumbnailXpress1.Items.Clear();
                thumbnailXpress2.Items.Clear();
                checkedListBoxSelected.Items.Clear();
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
            try
            {
                //********Must call unlock methods at startup*******      
                //thumbnailXpress1.Licensing.UnlockRuntime(1111, 1111, 1111, 1111);
                //thumbnailXpress1.Licensing.UnlockIXRuntime(2222, 2222, 2222, 2222);       
                try
                {
                    thumbnailXpress1.Licensing.AccessPdfXpress3();
                }
                catch (Exception)
                {
                    PegasusError(new Exception("Unable to access PDFXpress. PDF functionality is disabled."), labelLastErrorDescription);
                }
                //thumbnailXpress1.Licensing.UnlockPdfXpressRuntime(3333, 3333, 3333, 3333);
                //imagXpress1.Licensing.UnlockRuntime(2222, 2222, 2222, 2222);

                //thread settings
                numericUpDownMaxThreads.Value = thumbnailXpress1.MaximumThreadCount;
                textBoxStartThreadThreshold.Text = thumbnailXpress1.ThreadStartThreshold.ToString();
                textBoxHungThreadThreshold.Text = thumbnailXpress1.ThreadHungThreshold.ToString();

                //here we set the current directory and image so that the file open dialog box works well
                strCurrentDir = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images\");

                if (System.IO.Directory.Exists(strCurrentDir))
                    System.IO.Directory.SetCurrentDirectory(strCurrentDir);

                checkedListBoxSelected.CheckOnClick = true;
                //enable drag and drop in the control and set some defaults

                //Disable the special options (dragging and drill-down)
                thumbnailXpress1.DblClickDirectoryDrillDown = false;
                thumbnailXpress1.InterComponentThumbnailDragDropEnabled = false;
                thumbnailXpress1.EnableAsDragSourceForExternalDragDrop = false;
                thumbnailXpress1.EnableAsDropTargetForExternalDragDrop = false;
                thumbnailXpress1.IntraComponentThumbnailDragDropEnabled = false;

                //Enable the dragging of thumbnails to the control
                thumbnailXpress2.DblClickDirectoryDrillDown = false;
                thumbnailXpress2.InterComponentThumbnailDragDropEnabled = true;
                thumbnailXpress2.EnableAsDragSourceForExternalDragDrop = false;
                thumbnailXpress2.EnableAsDropTargetForExternalDragDrop = false;
                thumbnailXpress2.IntraComponentThumbnailDragDropEnabled = false;

                //Hide the second ThumbnailXpress control
                this.Width = 872;

                thumbnailXpress1.AllowDrop = true;
                thumbnailXpress1.CameraRaw = false;

                thumbnailXpress1.DescriptorDisplayMethod = DescriptorDisplayMethods.ShortFilename;

                comboBoxCompare.SelectedIndex = 0;

                comboBoxFilter.SelectedIndex = 0;
                comboBoxSelectionMode.SelectedIndex = 0;
                //TN cell settings
                hScrollBarCellBorderWidth.Value = 1;
                hScrollBarCellHeight.Value = 65;
                hScrollBarCellHorizontalSpacing.Value = 1;
                hScrollBarCellVerticalSpacing.Value = 1;
                hScrollBarCellWidth.Value = 65;
                labelCellBorderWidthValue.Text = hScrollBarCellBorderWidth.Value.ToString();
                labelCellHeightValue.Text = hScrollBarCellHeight.Value.ToString();
                labelCellHorizontalSpacingValue.Text = hScrollBarCellHorizontalSpacing.Value.ToString();
                labelCellVerticalSpacingValue.Text = hScrollBarCellVerticalSpacing.Value.ToString();
                labelCellWidthValue.Text = hScrollBarCellWidth.Value.ToString();
                //date time value set here
                dateTimePickerFileDate.Value = System.DateTime.Now;

                Application.EnableVisualStyles();
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }

            catch (NullReferenceException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void thumbnailXpress1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
               selectedThumbnailPoint = new System.Drawing.Point(e.X, e.Y);
        }  


        #region Pegasus Imaging Sample Application Standard Functions
        /*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

        private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
        private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }

        //Error Trapping routine
        private void PegasusError(Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            labelLastErrorDescription.Text = ErrorException.Message + "\n" + ErrorException.Source;
        }

        #endregion

        //Open single files here or multi-page TIFF files
        string OpenThumbnailFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = strCurrentDir;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                strImageFile = dlg.FileName;
                if (checkBoxExpand.Checked == true)
                {
                    thumbnailXpress1.Items.AddItemsFromFile(strImageFile, 0, true);
                }
                else
                {
                    thumbnailXpress1.Items.AddItemsFromFile(strImageFile, 0, false);
                }
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }


        string OpenThumbnailDir()
        {

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = strCurrentDir;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                //open to the directory selected
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf('\\'), dlg.FileName.Length - dlg.FileName.LastIndexOf('\\'));

                if (checkBoxEnabled.Checked == true)
                {
                    thumbnailXpress1.Filters.Clear();
                    //Filter Type
                    filterItem.Type = (FilterType)comboBoxFilter.SelectedIndex;
                    //Filter Compare
                    filterItem.Comparison = (FilterComparison)comboBoxCompare.SelectedIndex;
                    //Filter Size
                    filterItem.FileDate = dateTimePickerFileDate.Value;
                    if (textBoxFileSize.Text != "")
                    {
                        string s = textBoxFileSize.Text;
                        int temp = Convert.ToInt32(s);
                        filterItem.FileSize = temp;
                    }

                    filterItem.CaseSensitive = checkBoxMatchCaseSensitive.Checked;
                    filterItem.MatchAppliesToDirectory = checkBoxMatchDir.Checked;
                    filterItem.IncludeHidden = checkBoxIncludeHidden.Checked;
                    filterItem.IncludeParentDirectory = checkBoxIncludeParentDir.Checked;
                    filterItem.IncludeSubDirectory = checkBoxIncludeSubDir.Checked;
                    filterItem.FilenamePattern = textBoxFileNamePattern.Text;

                    //add the filter with desired settings from above
                    thumbnailXpress1.Filters.Add(filterItem);

                    //Open the Directory of Images
                    thumbnailXpress1.BeginUpdate();
                    thumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\\", 0);
                    thumbnailXpress1.EndUpdate();
                }
                else
                {
                    //No Filters applied here
                    thumbnailXpress1.BeginUpdate();
                    thumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\\", 0);
                    thumbnailXpress1.EndUpdate();
                }

                return strCurrentDir;
            }
            else
            {
                return "";
            }
        }


        private void buttonListSelected_Click(object sender, System.EventArgs e)
        {
            try
            {
                //clear the list box
                checkedListBoxSelected.Items.Clear();
                //add the selected items to the list box
                for (int i = 0; i < thumbnailXpress1.SelectedItems.Count; i++)
                {
                    checkedListBoxSelected.Items.Add(thumbnailXpress1.SelectedItems[i]);
                }
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
            // Least specific:
            catch (NullReferenceException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void buttonRemove_Click(object sender, System.EventArgs e)
        {
            try
            {
                int listBoxIndex = 0;
                while (listBoxIndex < checkedListBoxSelected.Items.Count)
                {
                    if (checkedListBoxSelected.GetItemChecked(listBoxIndex) == true)
                    {
                        //remove this item from the ThumbnailXpress control and the list box
                        thumbnailXpress1.Items.Remove((Accusoft.ThumbnailXpressSdk.ThumbnailItem)checkedListBoxSelected.Items[listBoxIndex]);
                        checkedListBoxSelected.Items.RemoveAt(listBoxIndex);
                    }
                    else
                    {
                        //this item is not checked, move to the next item
                        listBoxIndex++;
                    }
                }
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
            // Least specific:
            catch (NullReferenceException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void comboBoxFilter_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            switch (comboBoxFilter.SelectedIndex)
            {
                case 0:
                    //File Name Match
                    checkBoxEnabled.Enabled = true;
                    checkBoxMatchDir.Enabled = true;
                    checkBoxMatchCaseSensitive.Enabled = true;
                    checkBoxIncludeSubDir.Enabled = false;
                    checkBoxIncludeParentDir.Enabled = false;
                    dateTimePickerFileDate.Enabled = false;
                    comboBoxCompare.Enabled = false;
                    checkBoxIncludeHidden.Enabled = false;
                    textBoxFileSize.Enabled = false;
                    textBoxFileNamePattern.Enabled = true;
                    break;
                case 1:
                    //File Attributes
                    checkBoxMatchDir.Enabled = true;
                    checkBoxIncludeParentDir.Enabled = true;
                    checkBoxIncludeSubDir.Enabled = true;
                    checkBoxIncludeHidden.Enabled = true;
                    checkBoxMatchDir.Enabled = false;
                    checkBoxMatchCaseSensitive.Enabled = false;
                    dateTimePickerFileDate.Enabled = false;
                    comboBoxCompare.Enabled = false;
                    textBoxFileSize.Enabled = false;
                    textBoxFileNamePattern.Enabled = false;
                    break;
                case 2:
                    //File Creation Date
                    checkBoxEnabled.Enabled = true;
                    comboBoxCompare.Enabled = true;
                    dateTimePickerFileDate.Enabled = true;
                    textBoxFileSize.Enabled = false;
                    checkBoxIncludeSubDir.Enabled = false;
                    checkBoxIncludeParentDir.Enabled = false;
                    checkBoxIncludeHidden.Enabled = false;
                    textBoxFileNamePattern.Enabled = false;
                    break;
                case 3:
                    //File Mod Date
                    checkBoxEnabled.Enabled = true;
                    dateTimePickerFileDate.Enabled = true;
                    comboBoxCompare.Enabled = true;
                    textBoxFileSize.Enabled = false;
                    checkBoxIncludeSubDir.Enabled = false;
                    checkBoxIncludeParentDir.Enabled = false;
                    checkBoxIncludeHidden.Enabled = false;
                    textBoxFileNamePattern.Enabled = false;
                    break;
                case 4:
                    //File Size
                    checkBoxEnabled.Enabled = true;
                    dateTimePickerFileDate.Enabled = false;
                    textBoxFileSize.Enabled = true;
                    comboBoxCompare.Enabled = true;
                    checkBoxIncludeSubDir.Enabled = false;
                    checkBoxIncludeParentDir.Enabled = false;
                    textBoxFileNamePattern.Enabled = false;
                    break;
                case 5:
                    //all IX supported file types
                    checkBoxEnabled.Enabled = false;
                    dateTimePickerFileDate.Enabled = false;
                    comboBoxCompare.Enabled = false;
                    checkBoxMatchCaseSensitive.Enabled = false;
                    checkBoxMatchDir.Enabled = false;
                    checkBoxIncludeParentDir.Enabled = false;
                    checkBoxIncludeSubDir.Enabled = false;
                    checkBoxIncludeHidden.Enabled = false;
                    textBoxFileSize.Enabled = false;
                    textBoxFileNamePattern.Enabled = false;
                    break;
            }
        }

        private void checkBoxMatchDir_Click(object sender, System.EventArgs e)
        {
            if (checkBoxMatchDir.Checked == true)
            {
                filterItem.MatchAppliesToDirectory = true;
            }
            else
            {
                filterItem.MatchAppliesToDirectory = false;
            }
        }

        private void checkBoxMatchDir_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxMatchDir.Checked == true)
            {
                filterItem.MatchAppliesToDirectory = true;
            }
            else
            {
                filterItem.MatchAppliesToDirectory = false;
            }
        }

        private void checkBoxMatchCaseSensitive_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxMatchDir.Checked == true)
            {
                filterItem.CaseSensitive = true;
            }
            else
            {
                filterItem.CaseSensitive = false;
            }
        }

        private void checkBoxIncludeHiddenDir_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxIncludeHidden.Checked == true)
            {
                filterItem.IncludeHidden = true;
            }
            else
            {
                filterItem.IncludeHidden = false;
            }
        }

        private void checkBoxIncludeSubDir_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxEnabled.Checked == true)
            {
                filterItem.IncludeSubDirectory = true;
            }
            else
            {
                filterItem.IncludeSubDirectory = false;
            }
        }

        private void checkBoxIncludeParentDir_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxEnabled.Checked == true)
            {
                filterItem.IncludeParentDirectory = true;
            }
            else
            {
                filterItem.IncludeParentDirectory = false;
            }
        }

        private void dateTimePickerFileDate_ValueChanged(object sender, System.EventArgs e)
        {
            if(filterItem != null)
                filterItem.FileDate = dateTimePickerFileDate.Value;
        }

        private void menuItemOpenImage_Click(object sender, System.EventArgs e)
        {
            try
            {
                //clear out the error before the next opertation
                labelLastErrorDescription.Text = "";

                System.String result = OpenThumbnailFile();
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
            catch (NullReferenceException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void hScrollBarCellBorderWidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            labelCellBorderWidthValue.Text = hScrollBarCellBorderWidth.Value.ToString();
        }

        private void hScrollBarCellHeight_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            labelCellHeightValue.Text = hScrollBarCellHeight.Value.ToString();
        }

        private void hScrollBarCellHorizontalSpacing_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            labelCellHorizontalSpacingValue.Text = hScrollBarCellHorizontalSpacing.Value.ToString();
        }

        private void hScrollBarCellVerticalSpacing_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            labelCellVerticalSpacingValue.Text = hScrollBarCellVerticalSpacing.Value.ToString();
        }

        private void hScrollBarCellWidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            labelCellWidthValue.Text = hScrollBarCellWidth.Value.ToString();
        }

        private void buttonChooseCellColor_Click(object sender, System.EventArgs e)
        {
            cd.ShowDialog();
        }

        private void buttonSet_Click(object sender, System.EventArgs e)
        {
            //Set the TN Cell properties
            thumbnailXpress1.BeginUpdate();
            thumbnailXpress1.CellBorderWidth = hScrollBarCellBorderWidth.Value;
            thumbnailXpress1.CellBorderColor = cd.Color;
            thumbnailXpress1.CellHeight = hScrollBarCellHeight.Value;
            thumbnailXpress1.CellHorizontalSpacing = hScrollBarCellHorizontalSpacing.Value;
            thumbnailXpress1.CellVerticalSpacing = hScrollBarCellVerticalSpacing.Value;
            thumbnailXpress1.CellWidth = hScrollBarCellWidth.Value;
            thumbnailXpress1.EndUpdate();
        }

        private void comboBoxSelectionMode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            switch (comboBoxSelectionMode.SelectedIndex)
            {
                case 0:
                    thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
                    break;
                case 1:
                    thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiSimple;
                    break;
                case 2:
                    thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.None;
                    break;
                case 3:
                    thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.Single;
                    break;
            }
        }

        private void menuItemAboutTN_Click(object sender, System.EventArgs e)
        {
            thumbnailXpress1.AboutBox();
        }

        private void checkBoxEnabled_CheckedChanged(object sender, System.EventArgs e)
        {
            if (filterItemCreated == true)
            {
                if (checkBoxEnabled.Checked == true)
                {
                    filterItem.Enabled = true;
                    thumbnailXpress1.Filters.Clear();
                    thumbnailXpress1.Filters.Add(filterItem);
                }
                else
                {
                    filterItem.Enabled = false;
                }
            }
        }
 
        private void thumbnailXpress1_DoubleClick(object sender, System.EventArgs e)
        {
            labelLastErrorDescription.Text = "";

            try
            {
                //need to verify if we are on a folder, etc. and disable internal drag drop

                Accusoft.ThumbnailXpressSdk.ThumbnailItem info;
                info = thumbnailXpress1.GetItemFromPoint(selectedThumbnailPoint);

                if (!(info == null))
                {
                    if (info.Type == Accusoft.ThumbnailXpressSdk.ThumbnailType.ParentDirectory || info.Type == Accusoft.ThumbnailXpressSdk.ThumbnailType.SubDirectory)
                        return; //Displaying directory items is now handled in the Drill-down feature
                    else
                    {
                        //Call Image Viewer Dialog here

                        Accusoft.ThumbnailXpressSdk.ThumbnailItem selectedThumbnailItem;
                        selectedThumbnailItem = thumbnailXpress1.GetItemFromPoint(selectedThumbnailPoint);
                        imageInfoDialog viewer = new imageInfoDialog();
                        viewer.AddImage(selectedThumbnailItem.Filename);
                        Application.DoEvents();
                    }
                }
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
            catch (NullReferenceException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void checkBoxPreserveBlack_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxPreserveBlack.Checked == true)
            {
                //enable preserve black viewing option
                thumbnailXpress1.PreserveBlack = true;
            }
            else
            {
                //disable preserve black viewing option
                thumbnailXpress1.PreserveBlack = false;
            }
        }

        private void checkBoxCameraRaw_CheckedChanged(object sender, System.EventArgs e)
        {
            if (checkBoxCameraRaw.Checked == true)
            {
                //enable camera raw image viewing
                thumbnailXpress1.CameraRaw = true;
            }
            else
            {
                //disable camera raw image viewing
                thumbnailXpress1.CameraRaw = false;
            }
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            try
            {
                thumbnailXpress1.Cancel();
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void buttonSetThreads_Click(object sender, System.EventArgs e)
        {
            try
            {
                thumbnailXpress1.ThreadStartThreshold = System.Convert.ToInt32(textBoxStartThreadThreshold.Text);
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
                textBoxStartThreadThreshold.Text = thumbnailXpress1.ThreadStartThreshold.ToString();
            }

            try
            {
                thumbnailXpress1.ThreadHungThreshold = System.Convert.ToInt32(textBoxHungThreadThreshold.Text);
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
                textBoxHungThreadThreshold.Text = thumbnailXpress1.ThreadHungThreshold.ToString();
            }

        }

        private void numericUpDownMaxThreads_ValueChanged(object sender, System.EventArgs e)
        {
            try
            {
                thumbnailXpress1.MaximumThreadCount = (System.Int32)numericUpDownMaxThreads.Value;
            }
            catch (Accusoft.ThumbnailXpressSdk.ThumbnailXpressException ex)
            {
                PegasusError(ex, labelLastErrorDescription);
            }
        }

        private void menuItemAboutIX_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void checkBoxDrillDown_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxDrillDown.Checked)
            {
                thumbnailXpress1.DblClickDirectoryDrillDown = true;

                //Enable the adding of parent and subdirectories for navigation
                if (filterItem == null) filterItem = thumbnailXpress1.CreateFilter("test");
                filterItem.Enabled = true;
                filterItem.IncludeSubDirectory = true;
                filterItem.IncludeParentDirectory = true;

                comboBoxFilter.SelectedIndex = 1;

                checkBoxEnabled.Checked = true;
                checkBoxIncludeParentDir.Checked = true;
                checkBoxIncludeSubDir.Checked = true;
            }
            else
            {
                thumbnailXpress1.DblClickDirectoryDrillDown = false;
            }
        }

        private void checkBoxTNDrag_CheckedChanged(object sender, EventArgs e)
        {
            //Enable/disable dragging between TN controls
            if (checkBoxTNDrag.Checked)
            {
                thumbnailXpress1.InterComponentThumbnailDragDropEnabled = true;
                this.Width = 1057; //Show the second ThumbnailXpress control
            }
            else
            {
                thumbnailXpress1.InterComponentThumbnailDragDropEnabled = false;
                this.Width = 872; //Hide the second ThumbnailXpress control
            }
        }

        private void checkBoxOleDrag_CheckedChanged(object sender, EventArgs e)
        {
            //Enable OLE dragging
            if (checkBoxOleDrag.Checked)
            {
                thumbnailXpress1.EnableAsDragSourceForExternalDragDrop = true;
                thumbnailXpress1.EnableAsDropTargetForExternalDragDrop = true;
            }
            else 
            {
                thumbnailXpress1.EnableAsDragSourceForExternalDragDrop = false;
                thumbnailXpress1.EnableAsDropTargetForExternalDragDrop = false;
            }
        }

        private void checkBoxThumbnailShift_CheckedChanged(object sender, EventArgs e)
        {
            //Allows swapping thumbnail positions within the control by mouse
            if (checkBoxThumbnailShift.Checked)
                thumbnailXpress1.IntraComponentThumbnailDragDropEnabled = true;
            else
                thumbnailXpress1.IntraComponentThumbnailDragDropEnabled = false;
        }

        private void buttonScroll_Click(object sender, EventArgs e)
        {
            try
            {
                switch (comboBoxScrollTo.SelectedIndex)
                {
                    case 0: //Scroll to item (Highlight it just to indicate which one it is)
                        thumbnailXpress1.ScrollItems(ScrollItemsType.ItemIndex, System.Convert.ToInt32(textBoxScrollAmount.Text));
                        thumbnailXpress1.SelectedItems.Clear();
                        thumbnailXpress1.SelectedItems.Add(thumbnailXpress1.Items[System.Convert.ToInt32(textBoxScrollAmount.Text)]);
                        break;
                    case 1:
                        thumbnailXpress1.ScrollItems(ScrollItemsType.ThumbPosition, System.Convert.ToInt32(textBoxScrollAmount.Text));
                    break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot scroll. " + ex.Message);
            }
        }

        private void buttonLargeDecrement_Click(object sender, EventArgs e)
        {
            //Scroll by a large decrease
            thumbnailXpress1.ScrollItems(ScrollItemsType.LargeDecrease, 3);
        }

        private void buttonLargeIncrement_Click(object sender, EventArgs e)
        {
            //Scroll by a large increase
            thumbnailXpress1.ScrollItems(ScrollItemsType.LargeIncrease, 3);
        }

        private void buttonSmallDecrement_Click(object sender, EventArgs e)
        {
            //Scroll by a small decrease
            thumbnailXpress1.ScrollItems(ScrollItemsType.SmallDecrease, 1);
        }

        private void buttonSmallIncrement_Click(object sender, EventArgs e)
        {
            //Scroll by a small increase
            thumbnailXpress1.ScrollItems(ScrollItemsType.SmallIncrease, 1);
        }

    }
}


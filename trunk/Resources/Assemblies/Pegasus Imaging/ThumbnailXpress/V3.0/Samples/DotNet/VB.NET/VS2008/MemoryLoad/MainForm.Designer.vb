<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.mainMenu = New System.Windows.Forms.MenuStrip
        Me.fileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.loadImageViaStreamToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.loadDirectoryViaStreamsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.exitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.helpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.aboutThumbnailXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.aboutImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.imagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.imageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.cbLoadSource = New System.Windows.Forms.ComboBox
        Me.gbLastError = New System.Windows.Forms.GroupBox
        Me.lblLastError = New System.Windows.Forms.Label
        Me.lstbDescription = New System.Windows.Forms.ListBox
        Me.btRotate = New System.Windows.Forms.Button
        Me.btPgDown = New System.Windows.Forms.Button
        Me.btRowDown = New System.Windows.Forms.Button
        Me.btRowUp = New System.Windows.Forms.Button
        Me.btPgUp = New System.Windows.Forms.Button
        Me.thumbnailXpress1 = New Accusoft.ThumbnailXpressSdk.ThumbnailXpress
        Me.mainMenu.SuspendLayout()
        Me.gbLastError.SuspendLayout()
        Me.SuspendLayout()
        '
        'mainMenu
        '
        Me.mainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.fileToolStripMenuItem, Me.helpToolStripMenuItem})
        Me.mainMenu.Location = New System.Drawing.Point(0, 0)
        Me.mainMenu.Name = "mainMenu"
        Me.mainMenu.Size = New System.Drawing.Size(916, 24)
        Me.mainMenu.TabIndex = 2
        Me.mainMenu.Text = "menuStrip1"
        '
        'fileToolStripMenuItem
        '
        Me.fileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.loadImageViaStreamToolStripMenuItem, Me.loadDirectoryViaStreamsToolStripMenuItem, Me.toolStripMenuItem1, Me.toolStripSeparator1, Me.exitToolStripMenuItem})
        Me.fileToolStripMenuItem.Name = "fileToolStripMenuItem"
        Me.fileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.fileToolStripMenuItem.Text = "File"
        '
        'loadImageViaStreamToolStripMenuItem
        '
        Me.loadImageViaStreamToolStripMenuItem.Name = "loadImageViaStreamToolStripMenuItem"
        Me.loadImageViaStreamToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.loadImageViaStreamToolStripMenuItem.Text = "Load image via stream..."
        '
        'loadDirectoryViaStreamsToolStripMenuItem
        '
        Me.loadDirectoryViaStreamsToolStripMenuItem.Name = "loadDirectoryViaStreamsToolStripMenuItem"
        Me.loadDirectoryViaStreamsToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.loadDirectoryViaStreamsToolStripMenuItem.Text = "Load directory via stream..."
        '
        'toolStripMenuItem1
        '
        Me.toolStripMenuItem1.Name = "toolStripMenuItem1"
        Me.toolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.toolStripMenuItem1.Text = "Clear Thumbnails"
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(213, 6)
        '
        'exitToolStripMenuItem
        '
        Me.exitToolStripMenuItem.Name = "exitToolStripMenuItem"
        Me.exitToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.exitToolStripMenuItem.Text = "Exit"
        '
        'helpToolStripMenuItem
        '
        Me.helpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.aboutThumbnailXpressToolStripMenuItem, Me.aboutImagXpressToolStripMenuItem})
        Me.helpToolStripMenuItem.Name = "helpToolStripMenuItem"
        Me.helpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.helpToolStripMenuItem.Text = "Help"
        '
        'aboutThumbnailXpressToolStripMenuItem
        '
        Me.aboutThumbnailXpressToolStripMenuItem.Name = "aboutThumbnailXpressToolStripMenuItem"
        Me.aboutThumbnailXpressToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.aboutThumbnailXpressToolStripMenuItem.Text = "About ThumbnailXpress..."
        '
        'aboutImagXpressToolStripMenuItem
        '
        Me.aboutImagXpressToolStripMenuItem.Name = "aboutImagXpressToolStripMenuItem"
        Me.aboutImagXpressToolStripMenuItem.Size = New System.Drawing.Size(211, 22)
        Me.aboutImagXpressToolStripMenuItem.Text = "About ImagXpress..."
        '
        'imageXView1
        '
        Me.imageXView1.Location = New System.Drawing.Point(625, 149)
        Me.imageXView1.Name = "imageXView1"
        Me.imageXView1.Size = New System.Drawing.Size(279, 266)
        Me.imageXView1.TabIndex = 18
        '
        'cbLoadSource
        '
        Me.cbLoadSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLoadSource.FormattingEnabled = True
        Me.cbLoadSource.Items.AddRange(New Object() {"Load Image from Thumbnail Item into ImagXpress", "Load Image from Source Image File into ImagXpress"})
        Me.cbLoadSource.Location = New System.Drawing.Point(615, 458)
        Me.cbLoadSource.Name = "cbLoadSource"
        Me.cbLoadSource.Size = New System.Drawing.Size(290, 21)
        Me.cbLoadSource.TabIndex = 17
        '
        'gbLastError
        '
        Me.gbLastError.Controls.Add(Me.lblLastError)
        Me.gbLastError.Location = New System.Drawing.Point(625, 28)
        Me.gbLastError.Name = "gbLastError"
        Me.gbLastError.Size = New System.Drawing.Size(279, 106)
        Me.gbLastError.TabIndex = 16
        Me.gbLastError.TabStop = False
        Me.gbLastError.Text = "Last Error:"
        '
        'lblLastError
        '
        Me.lblLastError.Location = New System.Drawing.Point(16, 25)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(243, 66)
        Me.lblLastError.TabIndex = 0
        Me.lblLastError.Text = "No Error"
        '
        'lstbDescription
        '
        Me.lstbDescription.FormattingEnabled = True
        Me.lstbDescription.Items.AddRange(New Object() {"This sample demonstrates using Streams to load thumbnails into the ThumbnailXpres" & _
                        "s control.", "The following are featured:", "", "1) Loading a single file by stream into ThumbnailXpress", "2) Loading a directory of files into ThumbnailXpress by streams.", "3) Programmatic scrolling.", "4) Passing thumbnail data back and forth to ImagXpress", "5) Utilizing the UserTag feature for thumbnail items"})
        Me.lstbDescription.Location = New System.Drawing.Point(12, 27)
        Me.lstbDescription.Name = "lstbDescription"
        Me.lstbDescription.Size = New System.Drawing.Size(597, 108)
        Me.lstbDescription.TabIndex = 15
        '
        'btRotate
        '
        Me.btRotate.Location = New System.Drawing.Point(674, 421)
        Me.btRotate.Name = "btRotate"
        Me.btRotate.Size = New System.Drawing.Size(172, 27)
        Me.btRotate.TabIndex = 10
        Me.btRotate.Text = "Rotate 90 degrees"
        Me.btRotate.UseVisualStyleBackColor = True
        '
        'btPgDown
        '
        Me.btPgDown.Location = New System.Drawing.Point(463, 421)
        Me.btPgDown.Name = "btPgDown"
        Me.btPgDown.Size = New System.Drawing.Size(146, 27)
        Me.btPgDown.TabIndex = 11
        Me.btPgDown.Text = "Page Down"
        Me.btPgDown.UseVisualStyleBackColor = True
        '
        'btRowDown
        '
        Me.btRowDown.Location = New System.Drawing.Point(317, 421)
        Me.btRowDown.Name = "btRowDown"
        Me.btRowDown.Size = New System.Drawing.Size(146, 27)
        Me.btRowDown.TabIndex = 12
        Me.btRowDown.Text = "Row Down"
        Me.btRowDown.UseVisualStyleBackColor = True
        '
        'btRowUp
        '
        Me.btRowUp.Location = New System.Drawing.Point(169, 421)
        Me.btRowUp.Name = "btRowUp"
        Me.btRowUp.Size = New System.Drawing.Size(146, 27)
        Me.btRowUp.TabIndex = 13
        Me.btRowUp.Text = "Row Up"
        Me.btRowUp.UseVisualStyleBackColor = True
        '
        'btPgUp
        '
        Me.btPgUp.Location = New System.Drawing.Point(20, 421)
        Me.btPgUp.Name = "btPgUp"
        Me.btPgUp.Size = New System.Drawing.Size(146, 27)
        Me.btPgUp.TabIndex = 14
        Me.btPgUp.Text = "Page Up"
        Me.btPgUp.UseVisualStyleBackColor = True
        '
        'thumbnailXpress1
        '
        Me.thumbnailXpress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.thumbnailXpress1.BottomMargin = 5
        Me.thumbnailXpress1.CameraRaw = False
        Me.thumbnailXpress1.CellBorderColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.thumbnailXpress1.CellBorderWidth = 1
        Me.thumbnailXpress1.CellHeight = 80
        Me.thumbnailXpress1.CellHorizontalSpacing = 5
        Me.thumbnailXpress1.CellSpacingColor = System.Drawing.Color.White
        Me.thumbnailXpress1.CellVerticalSpacing = 5
        Me.thumbnailXpress1.CellWidth = 80
        Me.thumbnailXpress1.DblClickDirectoryDrillDown = True
        Me.thumbnailXpress1.DescriptorAlignment = CType((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter Or Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom), Accusoft.ThumbnailXpressSdk.DescriptorAlignments)
        Me.thumbnailXpress1.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.[Default]
        Me.thumbnailXpress1.EnableAsDragSourceForExternalDragDrop = True
        Me.thumbnailXpress1.EnableAsDropTargetForExternalDragDrop = True
        Me.thumbnailXpress1.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon
        Me.thumbnailXpress1.FtpPassword = ""
        Me.thumbnailXpress1.FtpUserName = ""
        Me.thumbnailXpress1.InterComponentThumbnailDragDropEnabled = True
        Me.thumbnailXpress1.IntraComponentThumbnailDragDropEnabled = True
        Me.thumbnailXpress1.LeftMargin = 5
        Me.thumbnailXpress1.Location = New System.Drawing.Point(12, 149)
        Me.thumbnailXpress1.MaximumThumbnailBitDepth = 24
        Me.thumbnailXpress1.Name = "thumbnailXpress1"
        Me.thumbnailXpress1.PreserveBlack = False
        Me.thumbnailXpress1.ProxyServer = ""
        Me.thumbnailXpress1.RightMargin = 5
        Me.thumbnailXpress1.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical
        Me.thumbnailXpress1.SelectBackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended
        Me.thumbnailXpress1.ShowHourglass = True
        Me.thumbnailXpress1.ShowImagePlaceholders = False
        Me.thumbnailXpress1.Size = New System.Drawing.Size(597, 266)
        Me.thumbnailXpress1.TabIndex = 19
        Me.thumbnailXpress1.TextBackColor = System.Drawing.Color.White
        Me.thumbnailXpress1.TopMargin = 5
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(916, 498)
        Me.Controls.Add(Me.thumbnailXpress1)
        Me.Controls.Add(Me.imageXView1)
        Me.Controls.Add(Me.cbLoadSource)
        Me.Controls.Add(Me.gbLastError)
        Me.Controls.Add(Me.lstbDescription)
        Me.Controls.Add(Me.btRotate)
        Me.Controls.Add(Me.btPgDown)
        Me.Controls.Add(Me.btRowDown)
        Me.Controls.Add(Me.btRowUp)
        Me.Controls.Add(Me.btPgUp)
        Me.Controls.Add(Me.mainMenu)
        Me.Name = "MainForm"
        Me.Text = "Memory Sample"
        Me.mainMenu.ResumeLayout(False)
        Me.mainMenu.PerformLayout()
        Me.gbLastError.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents mainMenu As System.Windows.Forms.MenuStrip
    Private WithEvents fileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents loadImageViaStreamToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents loadDirectoryViaStreamsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents exitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents helpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents aboutThumbnailXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents aboutImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents imagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Private WithEvents imageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Private WithEvents cbLoadSource As System.Windows.Forms.ComboBox
    Private WithEvents gbLastError As System.Windows.Forms.GroupBox
    Private WithEvents lblLastError As System.Windows.Forms.Label
    Private WithEvents lstbDescription As System.Windows.Forms.ListBox
    Private WithEvents btRotate As System.Windows.Forms.Button
    Private WithEvents btPgDown As System.Windows.Forms.Button
    Private WithEvents btRowDown As System.Windows.Forms.Button
    Private WithEvents btRowUp As System.Windows.Forms.Button
    Private WithEvents btPgUp As System.Windows.Forms.Button
    Friend WithEvents thumbnailXpress1 As Accusoft.ThumbnailXpressSdk.ThumbnailXpress

End Class

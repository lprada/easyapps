'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/
Imports Accusoft.ImagXpressSdk


Public Class ImageInfoDialog
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents Labelerror As System.Windows.Forms.Label
    Friend WithEvents Labelerrordesc As System.Windows.Forms.Label
    Friend WithEvents LabelFname As System.Windows.Forms.Label
    Friend WithEvents LabelFType As System.Windows.Forms.Label
    Friend WithEvents LabelDimen As System.Windows.Forms.Label
    Friend WithEvents Labelbpp As System.Windows.Forms.Label
    Friend WithEvents Labelsize As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.Labelerror = New System.Windows.Forms.Label
        Me.Labelerrordesc = New System.Windows.Forms.Label
        Me.LabelFname = New System.Windows.Forms.Label
        Me.LabelFType = New System.Windows.Forms.Label
        Me.LabelDimen = New System.Windows.Forms.Label
        Me.Labelbpp = New System.Windows.Forms.Label
        Me.Labelsize = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit
        Me.ImageXView1.Location = New System.Drawing.Point(24, 16)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(232, 264)
        Me.ImageXView1.TabIndex = 0
        '
        'Labelerror
        '
        Me.Labelerror.Location = New System.Drawing.Point(280, 248)
        Me.Labelerror.Name = "Labelerror"
        Me.Labelerror.Size = New System.Drawing.Size(392, 40)
        Me.Labelerror.TabIndex = 1
        '
        'Labelerrordesc
        '
        Me.Labelerrordesc.Location = New System.Drawing.Point(280, 224)
        Me.Labelerrordesc.Name = "Labelerrordesc"
        Me.Labelerrordesc.Size = New System.Drawing.Size(280, 16)
        Me.Labelerrordesc.TabIndex = 2
        Me.Labelerrordesc.Text = "Last Error Reported:"
        '
        'LabelFname
        '
        Me.LabelFname.Location = New System.Drawing.Point(512, 24)
        Me.LabelFname.Name = "LabelFname"
        Me.LabelFname.Size = New System.Drawing.Size(192, 24)
        Me.LabelFname.TabIndex = 3
        '
        'LabelFType
        '
        Me.LabelFType.Location = New System.Drawing.Point(512, 64)
        Me.LabelFType.Name = "LabelFType"
        Me.LabelFType.Size = New System.Drawing.Size(184, 24)
        Me.LabelFType.TabIndex = 4
        '
        'LabelDimen
        '
        Me.LabelDimen.Location = New System.Drawing.Point(512, 104)
        Me.LabelDimen.Name = "LabelDimen"
        Me.LabelDimen.Size = New System.Drawing.Size(192, 24)
        Me.LabelDimen.TabIndex = 5
        '
        'Labelbpp
        '
        Me.Labelbpp.Location = New System.Drawing.Point(512, 144)
        Me.Labelbpp.Name = "Labelbpp"
        Me.Labelbpp.Size = New System.Drawing.Size(176, 32)
        Me.Labelbpp.TabIndex = 6
        '
        'Labelsize
        '
        Me.Labelsize.Location = New System.Drawing.Point(512, 192)
        Me.Labelsize.Name = "Labelsize"
        Me.Labelsize.Size = New System.Drawing.Size(176, 16)
        Me.Labelsize.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(304, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 24)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "FileName:"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(304, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 32)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "File Type:"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(304, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(160, 24)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Dimensions:"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(304, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(168, 24)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Bits Per Pixel:"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(304, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(168, 24)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Size:"
        '
        'ImageInfoDialog
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(720, 301)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Labelsize)
        Me.Controls.Add(Me.Labelbpp)
        Me.Controls.Add(Me.LabelDimen)
        Me.Controls.Add(Me.LabelFType)
        Me.Controls.Add(Me.LabelFname)
        Me.Controls.Add(Me.Labelerrordesc)
        Me.Controls.Add(Me.Labelerror)
        Me.Controls.Add(Me.ImageXView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "ImageInfoDialog"
        Me.Text = "Image Info Dialog"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Sub AddImage(ByVal FName As System.String)
        Try
            Dim lo As New Accusoft.ImagXpressSdk.LoadOptions()
            lo.CameraRawEnabled = True
            lo.Jpeg.Enhanced = False
            If Not ImageXView1.Image Is Nothing Then
                ImageXView1.Image.Dispose()
            End If
            ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(ImagXpress1, FName, lo)
            ImageXView1.AutoScroll = True
            LabelFname.Text = System.IO.Path.GetFileName(FName)
            LabelFType.Text = ImageXView1.Image.ImageXData.Format.ToString()
            LabelDimen.Text = ImageXView1.Image.ImageXData.Width.ToString() + "x" + ImageXView1.Image.ImageXData.Height.ToString()
            Labelbpp.Text = ImageXView1.Image.ImageXData.BitsPerPixel.ToString()
            Labelsize.Text = ImageXView1.Image.ImageXData.Size.ToString()

        Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
            Labelerror.Text = ex.Message
        End Try
    End Sub

    Private Sub ImageInfoDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ImageXView1.AutoImageDispose = True
    End Sub
End Class

/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ThumbNailXpress
{
	/// <summary>
	/// Summary description for TNViewer.
	/// </summary>
	public class imageInfoDialog : System.Windows.Forms.Form
    {
		private System.Windows.Forms.Label labelFilenameValue;
		private System.Windows.Forms.Label labelFilename;
		private System.Windows.Forms.Label labelFileType;
		private System.Windows.Forms.Label labelFileTypeValue;
		private System.Windows.Forms.Label labelDimensions;
		private System.Windows.Forms.Label labelDimensionsValue;
		private System.Windows.Forms.Label labelBppValue;
		private System.Windows.Forms.Label labelBpp;
		private System.Windows.Forms.Label labelSizeValue;
		private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Label labelError;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private IContainer components;

		public imageInfoDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.labelFilenameValue = new System.Windows.Forms.Label();
            this.labelFilename = new System.Windows.Forms.Label();
            this.labelFileType = new System.Windows.Forms.Label();
            this.labelFileTypeValue = new System.Windows.Forms.Label();
            this.labelDimensions = new System.Windows.Forms.Label();
            this.labelDimensionsValue = new System.Windows.Forms.Label();
            this.labelBppValue = new System.Windows.Forms.Label();
            this.labelBpp = new System.Windows.Forms.Label();
            this.labelSizeValue = new System.Windows.Forms.Label();
            this.labelSize = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.SuspendLayout();
            // 
            // labelFilenameValue
            // 
            this.labelFilenameValue.Location = new System.Drawing.Point(464, 24);
            this.labelFilenameValue.Name = "labelFilenameValue";
            this.labelFilenameValue.Size = new System.Drawing.Size(152, 16);
            this.labelFilenameValue.TabIndex = 1;
            // 
            // labelFilename
            // 
            this.labelFilename.Location = new System.Drawing.Point(296, 24);
            this.labelFilename.Name = "labelFilename";
            this.labelFilename.Size = new System.Drawing.Size(136, 16);
            this.labelFilename.TabIndex = 2;
            this.labelFilename.Text = "FileName:";
            // 
            // labelFileType
            // 
            this.labelFileType.Location = new System.Drawing.Point(296, 66);
            this.labelFileType.Name = "labelFileType";
            this.labelFileType.Size = new System.Drawing.Size(128, 16);
            this.labelFileType.TabIndex = 3;
            this.labelFileType.Text = "File Type:";
            // 
            // labelFileTypeValue
            // 
            this.labelFileTypeValue.Location = new System.Drawing.Point(464, 66);
            this.labelFileTypeValue.Name = "labelFileTypeValue";
            this.labelFileTypeValue.Size = new System.Drawing.Size(160, 24);
            this.labelFileTypeValue.TabIndex = 4;
            // 
            // labelDimensions
            // 
            this.labelDimensions.Location = new System.Drawing.Point(296, 104);
            this.labelDimensions.Name = "labelDimensions";
            this.labelDimensions.Size = new System.Drawing.Size(128, 24);
            this.labelDimensions.TabIndex = 5;
            this.labelDimensions.Text = "Dimensions:";
            // 
            // labelDimensionsValue
            // 
            this.labelDimensionsValue.Location = new System.Drawing.Point(464, 104);
            this.labelDimensionsValue.Name = "labelDimensionsValue";
            this.labelDimensionsValue.Size = new System.Drawing.Size(152, 24);
            this.labelDimensionsValue.TabIndex = 6;
            // 
            // labelBppValue
            // 
            this.labelBppValue.Location = new System.Drawing.Point(464, 144);
            this.labelBppValue.Name = "labelBppValue";
            this.labelBppValue.Size = new System.Drawing.Size(152, 24);
            this.labelBppValue.TabIndex = 7;
            // 
            // labelBpp
            // 
            this.labelBpp.Location = new System.Drawing.Point(296, 144);
            this.labelBpp.Name = "labelBpp";
            this.labelBpp.Size = new System.Drawing.Size(136, 24);
            this.labelBpp.TabIndex = 8;
            this.labelBpp.Text = "Bits Per Pixel:";
            // 
            // labelSizeValue
            // 
            this.labelSizeValue.Location = new System.Drawing.Point(464, 184);
            this.labelSizeValue.Name = "labelSizeValue";
            this.labelSizeValue.Size = new System.Drawing.Size(152, 24);
            this.labelSizeValue.TabIndex = 9;
            // 
            // labelSize
            // 
            this.labelSize.Location = new System.Drawing.Point(296, 184);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(144, 24);
            this.labelSize.TabIndex = 10;
            this.labelSize.Text = "Size:";
            // 
            // labelError
            // 
            this.labelError.Location = new System.Drawing.Point(288, 240);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(336, 48);
            this.labelError.TabIndex = 11;
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;
            this.imageXView1.Location = new System.Drawing.Point(12, 9);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(256, 278);
            this.imageXView1.TabIndex = 12;
            // 
            // imageInfoDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(640, 301);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.labelSizeValue);
            this.Controls.Add(this.labelBpp);
            this.Controls.Add(this.labelBppValue);
            this.Controls.Add(this.labelDimensionsValue);
            this.Controls.Add(this.labelDimensions);
            this.Controls.Add(this.labelFileTypeValue);
            this.Controls.Add(this.labelFileType);
            this.Controls.Add(this.labelFilename);
            this.Controls.Add(this.labelFilenameValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "imageInfoDialog";
            this.Text = "Image Info Dialog";
            this.Load += new System.EventHandler(this.TNViewer_Load);
            this.ResumeLayout(false);

		}
		#endregion

		private void TNViewer_Load(object sender, System.EventArgs e)
		{
            imageXView1.AutoImageDispose = true;
		}

		//viewer function
		public void AddImage (System.String DibHandle)
		{
			try
			{		
				Accusoft.ImagXpressSdk.LoadOptions lo = new Accusoft.ImagXpressSdk.LoadOptions();
				lo.CameraRawEnabled = true;
				lo.Jpeg.Enhanced = false;
				imageXView1.AutoScroll = true;
                if (imageXView1.Image != null)
                    imageXView1.Image.Dispose();
				imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, DibHandle,lo);
				labelFilenameValue.Text = System.IO.Path.GetFileName(DibHandle);
				labelFileTypeValue.Text = imageXView1.Image.ImageXData.Format.ToString();
				labelDimensionsValue.Text = imageXView1.Image.ImageXData.Width.ToString() + "x" + imageXView1.Image.ImageXData.Height.ToString();
				labelBppValue.Text = imageXView1.Image.ImageXData.BitsPerPixel.ToString();
				labelSizeValue.Text = imageXView1.Image.ImageXData.Size.ToString();
				if (this.Visible == false)
				{
					this.ShowDialog ();	
				}
			}
			catch(Accusoft.ImagXpressSdk.ImagXpressException ex)
			{			
				PegasusError(ex,labelError);
				if (this.Visible == false)
				{
					this.ShowDialog ();	
				}
			}
		}
			
        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}	
    }	
}


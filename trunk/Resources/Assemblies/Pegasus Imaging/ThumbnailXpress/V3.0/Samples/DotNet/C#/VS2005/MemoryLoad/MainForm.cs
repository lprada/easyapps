/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MemoryLoad
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        //Create a new processor to for working on the thumbnails
        private Accusoft.ImagXpressSdk.Processor theProc;
        string currentDirectory;
        private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

        private struct RotatedFile
        {
            public System.String Path;
            public System.Int32 Rotation;
        }

        private void loadImageViaStreamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            System.IO.FileStream theFile;
            Accusoft.ThumbnailXpressSdk.ThumbnailItem theThumbnail;

            //Add a thumbnail item from a stream
            dlg.Title = "Select image to load";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = currentDirectory;
            if (dlg.ShowDialog() != DialogResult.OK) return;

            try
            {
                theFile = new System.IO.FileStream(dlg.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image);
                theFile.Seek(0, System.IO.SeekOrigin.Begin);
                theThumbnail.FromStream(theFile);
                theThumbnail.Descriptor = System.IO.Path.GetFileName(dlg.FileName);

                //In this sample, the UserTag is being given the path name of the file.
                //This will be used for when the user specifies to load the image from the source file.
                //Typically the UserTag property must be given a value (for example an empty string "")
                //before calling the AddItemFromStream method or an exception will be thrown.
                RotatedFile theRotatedFile;
                theRotatedFile.Path = theFile.Name;
                theRotatedFile.Rotation = 0;
                theThumbnail.UserTag = theRotatedFile;

                thumbnailXpress1.Items.AddItemFromStream(theThumbnail);
                theFile.Close();
            }
            catch (System.Exception ex)
            {
                lblLastError.Text = ex.Message + "\n\n" + ex.Source + "\n";
            }
        }

        private void loadDirectoryViaStreamsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            System.IO.FileStream theFile;
            System.String[] files;
            System.String[] directories;
            Accusoft.ThumbnailXpressSdk.ThumbnailItem theThumbnail;
            long i;

            //Add a thumbnail items from a directory
            dlg.Title = "Select image to load";
            dlg.Filter = strDefaultImageFilter;
            dlg.InitialDirectory = currentDirectory;
            if (dlg.ShowDialog() != DialogResult.OK) return;

            try
            {
                //Get a list of all the files (and sub directories) in the directory
                directories = System.IO.Directory.GetDirectories(System.IO.Path.GetDirectoryName(dlg.FileName) + @"\",  @"*.*");
                files = System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(dlg.FileName) + @"\", @"*.*");

                //First, add all of the directories to ThumbnailXpress
                for(i = 0; i < directories.Length; i++)
                {
                    theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.SubDirectory);
                    theThumbnail = thumbnailXpress1.CreateThumbnailItem(directories[i], 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.SubDirectory);
                    theThumbnail.Descriptor = System.IO.Path.GetFileName(directories[i]);
                    theThumbnail.UserTag = "";
                    thumbnailXpress1.Items.Add(theThumbnail);
                }

                //Now, add all of the files
                for (i = 0; i < files.Length; i++)
                {
                    theFile = new System.IO.FileStream(files[i], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                    theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image);
                    theFile.Seek(0, System.IO.SeekOrigin.Begin);
                    theThumbnail.FromStream(theFile);
                    theThumbnail.Descriptor = System.IO.Path.GetFileName(files[i]);
                    RotatedFile theRotatedFile;
                    theRotatedFile.Rotation = 0;
                    theRotatedFile.Path = files[i]; //Put the path of the file in the UserTag field
                    theThumbnail.UserTag = theRotatedFile; 
                    thumbnailXpress1.Items.AddItemFromStream(theThumbnail);
                    theFile.Close();
                }

                //Finally, add an item for the parent directory
                theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image);
                theThumbnail = thumbnailXpress1.CreateThumbnailItem(System.IO.Path.GetDirectoryName(dlg.FileName) + @"\..", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.ParentDirectory);
                theThumbnail.Descriptor = ".";
                theThumbnail.UserTag = "";
                thumbnailXpress1.Items.Insert(0, theThumbnail);              
            }
            catch (System.Exception ex)
            {
                lblLastError.Text = ex.Message + "\n\n" + ex.Source + "\n";
            }
        }

        private void aboutThumbnailXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            thumbnailXpress1.AboutBox();
        }

        private void aboutImagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //Clear all the Thumbnails
            thumbnailXpress1.Items.Clear();
            if (imageXView1.Image != null) imageXView1.Image.Dispose();
        }

        private void btPgUp_Click(object sender, EventArgs e)
        {
            //Scroll by a "page" or a large decrease
            thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.LargeDecrease, 1);
        }

        private void btRowUp_Click(object sender, EventArgs e)
        {
            //Scroll by a "row" or a small decrease
            thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.SmallDecrease, 1);
        }

        private void btRowDown_Click(object sender, EventArgs e)
        {
            //Scroll downward by a "row" or a small increase
            thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.SmallIncrease, 1);
        }

        private void btPgDown_Click(object sender, EventArgs e)
        {
            //Scroll downward by a large increase
            thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.LargeIncrease, 1);
        }

        private void btRotate_Click(object sender, EventArgs e)
        {
            Accusoft.ThumbnailXpressSdk.ThumbnailItem theItem = null;
            System.IO.MemoryStream newImage;
            int theIndex = 0;
            System.String theDescriptor;

            try
            {
                //Check for whether a thumbnail is selected
                if (thumbnailXpress1.SelectedItems.Count == 0)
                {
                    lblLastError.Text = "No thumbnail selected. Please select a thumbnail.";
                    return;
                }

                //Make sure an image is in the control
                if (imageXView1.Image == null) return;

                theItem = thumbnailXpress1.SelectedItems[0]; //Get the first selected thumbnail
                if (theItem.Type == Accusoft.ThumbnailXpressSdk.ThumbnailType.Unsupported)
                    return; //Doesn't have an image that we could use

                RotatedFile theRotatedFile;
                theRotatedFile.Rotation = 90;
                theRotatedFile.Path = "";
                if (theItem.UserTag is RotatedFile)
                {
                    theRotatedFile.Path = ((RotatedFile)theItem.UserTag).Path;
                    theRotatedFile.Rotation = ((RotatedFile)theItem.UserTag).Rotation + 90;
                    if (theRotatedFile.Rotation == 360)
                        theRotatedFile.Rotation = 0;
                }

                theIndex = thumbnailXpress1.Items.IndexOf(theItem); //Get its position in the index
                theDescriptor = theItem.Descriptor;

                theProc.Image = imageXView1.Image;
                theProc.Rotate(90); 

                //Now, pass this back to ThumbnailXpress (in the place of where it was)
                newImage = new System.IO.MemoryStream();
                imageXView1.Image = theProc.Image.Copy();
                theProc.Image.SaveStream(newImage, Accusoft.ImagXpressSdk.ImageXFormat.Bmp);
                newImage.Seek(0, System.IO.SeekOrigin.Begin);

                theItem = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image);
                theItem.FromStream(newImage);
                theItem.Descriptor = theDescriptor;

                //Store the path and rotation in the UserTag so that it can be used later to
                //load the original image with the rotation
                theItem.UserTag = theRotatedFile;
                thumbnailXpress1.Items.RemoveAt(theIndex);
                thumbnailXpress1.Items.InsertItemFromStream(theIndex, theItem);
                newImage.Close();
                thumbnailXpress1.SelectedItems.Add(thumbnailXpress1.Items[theIndex]);
            }
            catch (System.Exception ex)
            {
                lblLastError.Text = ex.Message + "\n\n" + ex.Source + "\n";
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //Demonstrate unlocking the controls
            //imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);
            //thumbnailXpress1.Licensing.UnlockRuntime(5678, 5678, 5678, 5678);
            //thumbnailXpress1.Licensing.UnlockIXRuntime(1234, 1234, 1234, 1234);
            
            Accusoft.ThumbnailXpressSdk.Filter theFilter;

            //Add directories so that they can be navigated when using directory drill-down
            theFilter = thumbnailXpress1.CreateFilter("Directories");
            theFilter.Type = Accusoft.ThumbnailXpressSdk.FilterType.FileAttributes;
            theFilter.Enabled = true;
            theFilter.IncludeParentDirectory = true;
            theFilter.IncludeSubDirectory = true;

            thumbnailXpress1.Filters.Add(theFilter);
            thumbnailXpress1.DblClickDirectoryDrillDown = false;

            //Initialize the Processor object
            theProc = new Accusoft.ImagXpressSdk.Processor(imagXpress1);

            currentDirectory = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images\");

            if (System.IO.Directory.Exists(currentDirectory))
                System.IO.Directory.SetCurrentDirectory(currentDirectory);

            //Set the image loading source
            cbLoadSource.SelectedIndex = 0;
        }

        private void thumbnailXpress1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Accusoft.ThumbnailXpressSdk.ThumbnailItem theItem;

            //Make sure that we have a thumbnail item selected
            if (thumbnailXpress1.SelectedItems.Count == 0) return;
            lblLastError.Text = "No Error.";

            try
            {
                //Get ready to load the new image
                theItem = thumbnailXpress1.SelectedItems[0];
                theItem = thumbnailXpress1.Items[thumbnailXpress1.Items.IndexOf(theItem)];
                if (imageXView1.Image != null) imageXView1.Image.Dispose();
                
                switch(cbLoadSource.SelectedIndex)
                {
                    case 0: //Load the image from the ThumbnailItem
                        imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress1, theItem.Hdib);
                    break;
                    case 1: //Load the image from the source image file
                        //First, check to see if the UserTag has an empty string.
                        if (theItem.UserTag.ToString() == "")
                        {
                            imageXView1.Image = null;
                            lblLastError.Text = "The selected thumbnail does not have an associated source image file.";
                        }
                        else
                        {
                            // Load the image from the path stored in the UserTag using the rotation stored in the UserTag.
                            Accusoft.ImagXpressSdk.LoadOptions loadOptions = new Accusoft.ImagXpressSdk.LoadOptions();
                            switch(((RotatedFile)theItem.UserTag).Rotation)
                            {
                                case 90:
                                    loadOptions.Rotation = Accusoft.ImagXpressSdk.RotateAngle.Rotate90;
                                    break;
                                case 180:
                                    loadOptions.Rotation = Accusoft.ImagXpressSdk.RotateAngle.Rotate180;
                                    break;
                                case 270:
                                    loadOptions.Rotation = Accusoft.ImagXpressSdk.RotateAngle.Rotate270;
                                    break;
                                default:
                                    break;
                            }
                            imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, ((RotatedFile)theItem.UserTag).Path.ToString(), loadOptions);
                        }
                        break;
                }

                imageXView1.ZoomToFit(Accusoft.ImagXpressSdk.ZoomToFitType.FitBest);
            }
            catch(Exception ex)
            {
                //Display the error and clear whatever may be in the view
                lblLastError.Text = ex.Message + "\n\n" + ex.Source + "\n";
                if(imageXView1.Image != null) imageXView1.Image.Dispose();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
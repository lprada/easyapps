'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Imports Accusoft.ImagXpressSdk
Imports Accusoft.ThumbnailXpressSdk
Imports System.Windows.Forms
Imports System.IO

Public Class MainForm
    Inherits System.Windows.Forms.Form

    Private filterItem As Accusoft.ThumbnailXpressSdk.Filter
    Private selectedThumbnailPoint As System.Drawing.Point
    Private dragtarget As Accusoft.ThumbnailXpressSdk.ThumbnailItem

    Private cd As System.Windows.Forms.ColorDialog

    'file I/O variables
    Private strCurrentDir As System.String
    Private strImageFile As System.String

    Private filterItemCreated As Boolean
    Friend WithEvents MenuItemAboutIX As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBoxOleDrag As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxDrillDown As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxThumbnailShift As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxTNDrag As System.Windows.Forms.CheckBox
    Friend WithEvents TabScrolling As System.Windows.Forms.TabPage
    Friend WithEvents ButtonScroll As System.Windows.Forms.Button
    Friend WithEvents GroupBoxScrolling As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBoxScrollTo As System.Windows.Forms.ComboBox
    Friend WithEvents TextBoxScrollAmount As System.Windows.Forms.TextBox
    Friend WithEvents GroupBoxScrollInc As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonLargeIncrement As System.Windows.Forms.Button
    Friend WithEvents ButtonLargeDecrement As System.Windows.Forms.Button
    Friend WithEvents ButtonSmallDecrement As System.Windows.Forms.Button
    Friend WithEvents ButtonSmallIncrement As System.Windows.Forms.Button
    Friend WithEvents TextBoxFileNamePattern As System.Windows.Forms.TextBox
    Friend WithEvents LabelFileNamePattern As System.Windows.Forms.Label
    Friend WithEvents ThumbnailXpress2 As Accusoft.ThumbnailXpressSdk.ThumbnailXpress
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents ThumbnailXpress1 As Accusoft.ThumbnailXpressSdk.ThumbnailXpress
    Friend WithEvents LabelFileDate As System.Windows.Forms.Label

    Private indexOfItemUnderMouseToDrag As System.Int32

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then


            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItemFile As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOpenDir As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemOpenImage As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemExit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemHelp As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemAboutTN As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBoxExpand As System.Windows.Forms.CheckBox
    Friend WithEvents tabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabFilters As System.Windows.Forms.TabPage
    Friend WithEvents TextBoxFileSize As System.Windows.Forms.TextBox
    Friend WithEvents LabelFileSize As System.Windows.Forms.Label
    Friend WithEvents DateTimePickerFileDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents CheckBoxIncludeSubDir As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxIncludeParentDir As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxIncludeHidden As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxMatchCaseSensitive As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxMatchDirectory As System.Windows.Forms.CheckBox
    Friend WithEvents LabelCompare As System.Windows.Forms.Label
    Friend WithEvents LabelFilter As System.Windows.Forms.Label
    Friend WithEvents ComboBoxFilter As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxCompare As System.Windows.Forms.ComboBox
    Friend WithEvents TabSelectionMode As System.Windows.Forms.TabPage
    Friend WithEvents LabelSelectionMode As System.Windows.Forms.Label
    Friend WithEvents ComboBoxSelectionMode As System.Windows.Forms.ComboBox
    Friend WithEvents TabCells As System.Windows.Forms.TabPage
    Friend WithEvents LabelCellWidthValue As System.Windows.Forms.Label
    Friend WithEvents LabelCellWidth As System.Windows.Forms.Label
    Friend WithEvents LabelCellVerticalSpacingValue As System.Windows.Forms.Label
    Friend WithEvents LabelCellVerticalSpacing As System.Windows.Forms.Label
    Friend WithEvents LabelCellHorizontalSpacing As System.Windows.Forms.Label
    Friend WithEvents HScrollBarCellWidth As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBarCellVerticalSpacing As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBarCellHorizontalSpacing As System.Windows.Forms.HScrollBar
    Friend WithEvents LabelCellHorizontalSpacingValue As System.Windows.Forms.Label
    Friend WithEvents LabelCellHeightValue As System.Windows.Forms.Label
    Friend WithEvents HScrollBarCellHeight As System.Windows.Forms.HScrollBar
    Friend WithEvents LabelCellHeight As System.Windows.Forms.Label
    Friend WithEvents LabelCellBorderWidth As System.Windows.Forms.Label
    Friend WithEvents ButtonChooseCellColor As System.Windows.Forms.Button
    Friend WithEvents ButtonSet As System.Windows.Forms.Button
    Friend WithEvents LabelCellBorderWidthValue As System.Windows.Forms.Label
    Friend WithEvents HScrollBarCellBorderWidth As System.Windows.Forms.HScrollBar
    Friend WithEvents LabelLastError As System.Windows.Forms.Label
    Friend WithEvents LabelLastErrorDescription As System.Windows.Forms.Label
    Friend WithEvents TextBoxSampleDescription As System.Windows.Forms.RichTextBox
    Friend WithEvents ButtonRemove As System.Windows.Forms.Button
    Friend WithEvents CheckedListBoxSelected As System.Windows.Forms.CheckedListBox
    Friend WithEvents ButtonListSelected As System.Windows.Forms.Button
    Friend WithEvents ButtonClear As System.Windows.Forms.Button
    Friend WithEvents TabThread As System.Windows.Forms.TabPage
    Friend WithEvents NumericUpDownMaxThreads As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelThreads As System.Windows.Forms.Label
    Friend WithEvents GroupBoxThreads As System.Windows.Forms.GroupBox
    Friend WithEvents LabelStartThreadThreshold As System.Windows.Forms.Label
    Friend WithEvents TextBoxStartThreadThreshold As System.Windows.Forms.TextBox
    Friend WithEvents LabelHungThreadThreshold As System.Windows.Forms.Label
    Friend WithEvents TextBoxHungThreadThreshold As System.Windows.Forms.TextBox
    Friend WithEvents ButtonSetThreads As System.Windows.Forms.Button
    Friend WithEvents ButtonCancel As System.Windows.Forms.Button
    Friend WithEvents CheckBoxPreserve As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxRaw As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItemFile = New System.Windows.Forms.MenuItem
        Me.MenuItemOpenDir = New System.Windows.Forms.MenuItem
        Me.MenuItemOpenImage = New System.Windows.Forms.MenuItem
        Me.MenuItemExit = New System.Windows.Forms.MenuItem
        Me.MenuItemHelp = New System.Windows.Forms.MenuItem
        Me.MenuItemAboutIX = New System.Windows.Forms.MenuItem
        Me.MenuItemAboutTN = New System.Windows.Forms.MenuItem
        Me.CheckBoxExpand = New System.Windows.Forms.CheckBox
        Me.tabControl1 = New System.Windows.Forms.TabControl
        Me.TabFilters = New System.Windows.Forms.TabPage
        Me.LabelFileDate = New System.Windows.Forms.Label
        Me.TextBoxFileNamePattern = New System.Windows.Forms.TextBox
        Me.LabelFileNamePattern = New System.Windows.Forms.Label
        Me.TextBoxFileSize = New System.Windows.Forms.TextBox
        Me.LabelFileSize = New System.Windows.Forms.Label
        Me.DateTimePickerFileDate = New System.Windows.Forms.DateTimePicker
        Me.CheckBoxIncludeSubDir = New System.Windows.Forms.CheckBox
        Me.CheckBoxIncludeParentDir = New System.Windows.Forms.CheckBox
        Me.CheckBoxEnabled = New System.Windows.Forms.CheckBox
        Me.CheckBoxIncludeHidden = New System.Windows.Forms.CheckBox
        Me.CheckBoxMatchCaseSensitive = New System.Windows.Forms.CheckBox
        Me.CheckBoxMatchDirectory = New System.Windows.Forms.CheckBox
        Me.LabelCompare = New System.Windows.Forms.Label
        Me.LabelFilter = New System.Windows.Forms.Label
        Me.ComboBoxFilter = New System.Windows.Forms.ComboBox
        Me.ComboBoxCompare = New System.Windows.Forms.ComboBox
        Me.TabThread = New System.Windows.Forms.TabPage
        Me.GroupBoxThreads = New System.Windows.Forms.GroupBox
        Me.ButtonSetThreads = New System.Windows.Forms.Button
        Me.TextBoxHungThreadThreshold = New System.Windows.Forms.TextBox
        Me.LabelHungThreadThreshold = New System.Windows.Forms.Label
        Me.TextBoxStartThreadThreshold = New System.Windows.Forms.TextBox
        Me.LabelStartThreadThreshold = New System.Windows.Forms.Label
        Me.LabelThreads = New System.Windows.Forms.Label
        Me.NumericUpDownMaxThreads = New System.Windows.Forms.NumericUpDown
        Me.TabSelectionMode = New System.Windows.Forms.TabPage
        Me.LabelSelectionMode = New System.Windows.Forms.Label
        Me.ComboBoxSelectionMode = New System.Windows.Forms.ComboBox
        Me.TabCells = New System.Windows.Forms.TabPage
        Me.LabelCellWidthValue = New System.Windows.Forms.Label
        Me.LabelCellWidth = New System.Windows.Forms.Label
        Me.LabelCellVerticalSpacingValue = New System.Windows.Forms.Label
        Me.LabelCellVerticalSpacing = New System.Windows.Forms.Label
        Me.LabelCellHorizontalSpacing = New System.Windows.Forms.Label
        Me.HScrollBarCellWidth = New System.Windows.Forms.HScrollBar
        Me.HScrollBarCellVerticalSpacing = New System.Windows.Forms.HScrollBar
        Me.HScrollBarCellHorizontalSpacing = New System.Windows.Forms.HScrollBar
        Me.LabelCellHorizontalSpacingValue = New System.Windows.Forms.Label
        Me.LabelCellHeightValue = New System.Windows.Forms.Label
        Me.HScrollBarCellHeight = New System.Windows.Forms.HScrollBar
        Me.LabelCellHeight = New System.Windows.Forms.Label
        Me.LabelCellBorderWidth = New System.Windows.Forms.Label
        Me.ButtonChooseCellColor = New System.Windows.Forms.Button
        Me.ButtonSet = New System.Windows.Forms.Button
        Me.LabelCellBorderWidthValue = New System.Windows.Forms.Label
        Me.HScrollBarCellBorderWidth = New System.Windows.Forms.HScrollBar
        Me.TabScrolling = New System.Windows.Forms.TabPage
        Me.GroupBoxScrollInc = New System.Windows.Forms.GroupBox
        Me.ButtonLargeIncrement = New System.Windows.Forms.Button
        Me.ButtonLargeDecrement = New System.Windows.Forms.Button
        Me.ButtonSmallDecrement = New System.Windows.Forms.Button
        Me.ButtonSmallIncrement = New System.Windows.Forms.Button
        Me.GroupBoxScrolling = New System.Windows.Forms.GroupBox
        Me.ButtonScroll = New System.Windows.Forms.Button
        Me.ComboBoxScrollTo = New System.Windows.Forms.ComboBox
        Me.TextBoxScrollAmount = New System.Windows.Forms.TextBox
        Me.LabelLastError = New System.Windows.Forms.Label
        Me.LabelLastErrorDescription = New System.Windows.Forms.Label
        Me.TextBoxSampleDescription = New System.Windows.Forms.RichTextBox
        Me.ButtonRemove = New System.Windows.Forms.Button
        Me.CheckedListBoxSelected = New System.Windows.Forms.CheckedListBox
        Me.ButtonListSelected = New System.Windows.Forms.Button
        Me.ButtonClear = New System.Windows.Forms.Button
        Me.ButtonCancel = New System.Windows.Forms.Button
        Me.CheckBoxPreserve = New System.Windows.Forms.CheckBox
        Me.CheckBoxRaw = New System.Windows.Forms.CheckBox
        Me.CheckBoxOleDrag = New System.Windows.Forms.CheckBox
        Me.CheckBoxDrillDown = New System.Windows.Forms.CheckBox
        Me.CheckBoxThumbnailShift = New System.Windows.Forms.CheckBox
        Me.CheckBoxTNDrag = New System.Windows.Forms.CheckBox
        Me.ThumbnailXpress2 = New Accusoft.ThumbnailXpressSdk.ThumbnailXpress
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.ThumbnailXpress1 = New Accusoft.ThumbnailXpressSdk.ThumbnailXpress
        Me.tabControl1.SuspendLayout()
        Me.TabFilters.SuspendLayout()
        Me.TabThread.SuspendLayout()
        Me.GroupBoxThreads.SuspendLayout()
        CType(Me.NumericUpDownMaxThreads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSelectionMode.SuspendLayout()
        Me.TabCells.SuspendLayout()
        Me.TabScrolling.SuspendLayout()
        Me.GroupBoxScrollInc.SuspendLayout()
        Me.GroupBoxScrolling.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemFile, Me.MenuItemHelp})
        '
        'MenuItemFile
        '
        Me.MenuItemFile.Index = 0
        Me.MenuItemFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemOpenDir, Me.MenuItemOpenImage, Me.MenuItemExit})
        Me.MenuItemFile.Text = "File"
        '
        'MenuItemOpenDir
        '
        Me.MenuItemOpenDir.Index = 0
        Me.MenuItemOpenDir.Text = "Open Directory of Images"
        '
        'MenuItemOpenImage
        '
        Me.MenuItemOpenImage.Index = 1
        Me.MenuItemOpenImage.Text = "Open Image"
        '
        'MenuItemExit
        '
        Me.MenuItemExit.Index = 2
        Me.MenuItemExit.Text = "Exit"
        '
        'MenuItemHelp
        '
        Me.MenuItemHelp.Index = 1
        Me.MenuItemHelp.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemAboutIX, Me.MenuItemAboutTN})
        Me.MenuItemHelp.Text = "Help"
        '
        'MenuItemAboutIX
        '
        Me.MenuItemAboutIX.Index = 0
        Me.MenuItemAboutIX.Text = "Imag&Xpress"
        '
        'MenuItemAboutTN
        '
        Me.MenuItemAboutTN.Index = 1
        Me.MenuItemAboutTN.Text = "&ThumbnailXpress"
        '
        'CheckBoxExpand
        '
        Me.CheckBoxExpand.Location = New System.Drawing.Point(8, 8)
        Me.CheckBoxExpand.Name = "CheckBoxExpand"
        Me.CheckBoxExpand.Size = New System.Drawing.Size(120, 24)
        Me.CheckBoxExpand.TabIndex = 33
        Me.CheckBoxExpand.Text = "Expand Multipage"
        '
        'tabControl1
        '
        Me.tabControl1.Controls.Add(Me.TabFilters)
        Me.tabControl1.Controls.Add(Me.TabThread)
        Me.tabControl1.Controls.Add(Me.TabSelectionMode)
        Me.tabControl1.Controls.Add(Me.TabCells)
        Me.tabControl1.Controls.Add(Me.TabScrolling)
        Me.tabControl1.Location = New System.Drawing.Point(0, 88)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(296, 416)
        Me.tabControl1.TabIndex = 32
        '
        'TabFilters
        '
        Me.TabFilters.Controls.Add(Me.LabelFileDate)
        Me.TabFilters.Controls.Add(Me.TextBoxFileNamePattern)
        Me.TabFilters.Controls.Add(Me.LabelFileNamePattern)
        Me.TabFilters.Controls.Add(Me.TextBoxFileSize)
        Me.TabFilters.Controls.Add(Me.LabelFileSize)
        Me.TabFilters.Controls.Add(Me.DateTimePickerFileDate)
        Me.TabFilters.Controls.Add(Me.CheckBoxIncludeSubDir)
        Me.TabFilters.Controls.Add(Me.CheckBoxIncludeParentDir)
        Me.TabFilters.Controls.Add(Me.CheckBoxEnabled)
        Me.TabFilters.Controls.Add(Me.CheckBoxIncludeHidden)
        Me.TabFilters.Controls.Add(Me.CheckBoxMatchCaseSensitive)
        Me.TabFilters.Controls.Add(Me.CheckBoxMatchDirectory)
        Me.TabFilters.Controls.Add(Me.LabelCompare)
        Me.TabFilters.Controls.Add(Me.LabelFilter)
        Me.TabFilters.Controls.Add(Me.ComboBoxFilter)
        Me.TabFilters.Controls.Add(Me.ComboBoxCompare)
        Me.TabFilters.Location = New System.Drawing.Point(4, 22)
        Me.TabFilters.Name = "TabFilters"
        Me.TabFilters.Size = New System.Drawing.Size(288, 390)
        Me.TabFilters.TabIndex = 1
        Me.TabFilters.Text = "Filters"
        Me.TabFilters.UseVisualStyleBackColor = True
        '
        'LabelFileDate
        '
        Me.LabelFileDate.AutoSize = True
        Me.LabelFileDate.Location = New System.Drawing.Point(8, 123)
        Me.LabelFileDate.Name = "LabelFileDate"
        Me.LabelFileDate.Size = New System.Drawing.Size(52, 13)
        Me.LabelFileDate.TabIndex = 19
        Me.LabelFileDate.Text = "File Date:"
        '
        'TextBoxFileNamePattern
        '
        Me.TextBoxFileNamePattern.Location = New System.Drawing.Point(118, 153)
        Me.TextBoxFileNamePattern.Name = "TextBoxFileNamePattern"
        Me.TextBoxFileNamePattern.Size = New System.Drawing.Size(152, 20)
        Me.TextBoxFileNamePattern.TabIndex = 18
        '
        'LabelFileNamePattern
        '
        Me.LabelFileNamePattern.AutoSize = True
        Me.LabelFileNamePattern.Location = New System.Drawing.Point(8, 156)
        Me.LabelFileNamePattern.Name = "LabelFileNamePattern"
        Me.LabelFileNamePattern.Size = New System.Drawing.Size(94, 13)
        Me.LabelFileNamePattern.TabIndex = 17
        Me.LabelFileNamePattern.Text = "File Name Pattern:"
        '
        'TextBoxFileSize
        '
        Me.TextBoxFileSize.Enabled = False
        Me.TextBoxFileSize.Location = New System.Drawing.Point(118, 80)
        Me.TextBoxFileSize.Name = "TextBoxFileSize"
        Me.TextBoxFileSize.Size = New System.Drawing.Size(152, 20)
        Me.TextBoxFileSize.TabIndex = 16
        Me.TextBoxFileSize.Visible = False
        '
        'LabelFileSize
        '
        Me.LabelFileSize.Location = New System.Drawing.Point(8, 83)
        Me.LabelFileSize.Name = "LabelFileSize"
        Me.LabelFileSize.Size = New System.Drawing.Size(100, 20)
        Me.LabelFileSize.TabIndex = 15
        Me.LabelFileSize.Text = "File Size (in bytes):"
        '
        'DateTimePickerFileDate
        '
        Me.DateTimePickerFileDate.Enabled = False
        Me.DateTimePickerFileDate.Location = New System.Drawing.Point(72, 117)
        Me.DateTimePickerFileDate.Name = "DateTimePickerFileDate"
        Me.DateTimePickerFileDate.Size = New System.Drawing.Size(198, 20)
        Me.DateTimePickerFileDate.TabIndex = 14
        '
        'CheckBoxIncludeSubDir
        '
        Me.CheckBoxIncludeSubDir.Enabled = False
        Me.CheckBoxIncludeSubDir.Location = New System.Drawing.Point(24, 310)
        Me.CheckBoxIncludeSubDir.Name = "CheckBoxIncludeSubDir"
        Me.CheckBoxIncludeSubDir.Size = New System.Drawing.Size(136, 19)
        Me.CheckBoxIncludeSubDir.TabIndex = 13
        Me.CheckBoxIncludeSubDir.Text = "Include Subdirectory"
        '
        'CheckBoxIncludeParentDir
        '
        Me.CheckBoxIncludeParentDir.Enabled = False
        Me.CheckBoxIncludeParentDir.Location = New System.Drawing.Point(24, 286)
        Me.CheckBoxIncludeParentDir.Name = "CheckBoxIncludeParentDir"
        Me.CheckBoxIncludeParentDir.Size = New System.Drawing.Size(160, 18)
        Me.CheckBoxIncludeParentDir.TabIndex = 12
        Me.CheckBoxIncludeParentDir.Text = "Include Parent Directory"
        '
        'CheckBoxEnabled
        '
        Me.CheckBoxEnabled.Checked = True
        Me.CheckBoxEnabled.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxEnabled.Location = New System.Drawing.Point(24, 262)
        Me.CheckBoxEnabled.Name = "CheckBoxEnabled"
        Me.CheckBoxEnabled.Size = New System.Drawing.Size(136, 16)
        Me.CheckBoxEnabled.TabIndex = 11
        Me.CheckBoxEnabled.Text = "Enabled"
        '
        'CheckBoxIncludeHidden
        '
        Me.CheckBoxIncludeHidden.Enabled = False
        Me.CheckBoxIncludeHidden.Location = New System.Drawing.Point(24, 238)
        Me.CheckBoxIncludeHidden.Name = "CheckBoxIncludeHidden"
        Me.CheckBoxIncludeHidden.Size = New System.Drawing.Size(144, 16)
        Me.CheckBoxIncludeHidden.TabIndex = 10
        Me.CheckBoxIncludeHidden.Text = "Include Hidden"
        '
        'CheckBoxMatchCaseSensitive
        '
        Me.CheckBoxMatchCaseSensitive.Enabled = False
        Me.CheckBoxMatchCaseSensitive.Location = New System.Drawing.Point(24, 214)
        Me.CheckBoxMatchCaseSensitive.Name = "CheckBoxMatchCaseSensitive"
        Me.CheckBoxMatchCaseSensitive.Size = New System.Drawing.Size(152, 16)
        Me.CheckBoxMatchCaseSensitive.TabIndex = 9
        Me.CheckBoxMatchCaseSensitive.Text = "Match is Case Sensitive"
        '
        'CheckBoxMatchDirectory
        '
        Me.CheckBoxMatchDirectory.Enabled = False
        Me.CheckBoxMatchDirectory.Location = New System.Drawing.Point(24, 190)
        Me.CheckBoxMatchDirectory.Name = "CheckBoxMatchDirectory"
        Me.CheckBoxMatchDirectory.Size = New System.Drawing.Size(160, 18)
        Me.CheckBoxMatchDirectory.TabIndex = 8
        Me.CheckBoxMatchDirectory.Text = "Match Applies to Directory"
        '
        'LabelCompare
        '
        Me.LabelCompare.Location = New System.Drawing.Point(8, 51)
        Me.LabelCompare.Name = "LabelCompare"
        Me.LabelCompare.Size = New System.Drawing.Size(80, 24)
        Me.LabelCompare.TabIndex = 6
        Me.LabelCompare.Text = "Compare Type:"
        '
        'LabelFilter
        '
        Me.LabelFilter.Location = New System.Drawing.Point(8, 19)
        Me.LabelFilter.Name = "LabelFilter"
        Me.LabelFilter.Size = New System.Drawing.Size(80, 16)
        Me.LabelFilter.TabIndex = 5
        Me.LabelFilter.Text = "Filter Type:"
        '
        'ComboBoxFilter
        '
        Me.ComboBoxFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilter.Items.AddRange(New Object() {"File Name Match", "File Attributes and Directory", "File Creation Date", "File Modification Date", "FileSize", "ImagXpress Supported File"})
        Me.ComboBoxFilter.Location = New System.Drawing.Point(118, 16)
        Me.ComboBoxFilter.Name = "ComboBoxFilter"
        Me.ComboBoxFilter.Size = New System.Drawing.Size(152, 21)
        Me.ComboBoxFilter.TabIndex = 4
        '
        'ComboBoxCompare
        '
        Me.ComboBoxCompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxCompare.Enabled = False
        Me.ComboBoxCompare.Items.AddRange(New Object() {"LessThan", "LessThan or Equal", "Equal", "GreaterThan or Equal", "GreaterThan"})
        Me.ComboBoxCompare.Location = New System.Drawing.Point(118, 48)
        Me.ComboBoxCompare.Name = "ComboBoxCompare"
        Me.ComboBoxCompare.Size = New System.Drawing.Size(152, 21)
        Me.ComboBoxCompare.TabIndex = 0
        '
        'TabThread
        '
        Me.TabThread.Controls.Add(Me.GroupBoxThreads)
        Me.TabThread.Controls.Add(Me.LabelThreads)
        Me.TabThread.Controls.Add(Me.NumericUpDownMaxThreads)
        Me.TabThread.Location = New System.Drawing.Point(4, 22)
        Me.TabThread.Name = "TabThread"
        Me.TabThread.Size = New System.Drawing.Size(288, 390)
        Me.TabThread.TabIndex = 2
        Me.TabThread.Text = "Thread Processing"
        Me.TabThread.UseVisualStyleBackColor = True
        Me.TabThread.Visible = False
        '
        'GroupBoxThreads
        '
        Me.GroupBoxThreads.Controls.Add(Me.ButtonSetThreads)
        Me.GroupBoxThreads.Controls.Add(Me.TextBoxHungThreadThreshold)
        Me.GroupBoxThreads.Controls.Add(Me.LabelHungThreadThreshold)
        Me.GroupBoxThreads.Controls.Add(Me.TextBoxStartThreadThreshold)
        Me.GroupBoxThreads.Controls.Add(Me.LabelStartThreadThreshold)
        Me.GroupBoxThreads.Location = New System.Drawing.Point(16, 72)
        Me.GroupBoxThreads.Name = "GroupBoxThreads"
        Me.GroupBoxThreads.Size = New System.Drawing.Size(248, 208)
        Me.GroupBoxThreads.TabIndex = 2
        Me.GroupBoxThreads.TabStop = False
        Me.GroupBoxThreads.Text = "Thread Time Thresholds"
        '
        'ButtonSetThreads
        '
        Me.ButtonSetThreads.Location = New System.Drawing.Point(114, 24)
        Me.ButtonSetThreads.Name = "ButtonSetThreads"
        Me.ButtonSetThreads.Size = New System.Drawing.Size(110, 32)
        Me.ButtonSetThreads.TabIndex = 4
        Me.ButtonSetThreads.Text = "Set Thread Values"
        '
        'TextBoxHungThreadThreshold
        '
        Me.TextBoxHungThreadThreshold.Location = New System.Drawing.Point(128, 120)
        Me.TextBoxHungThreadThreshold.Name = "TextBoxHungThreadThreshold"
        Me.TextBoxHungThreadThreshold.Size = New System.Drawing.Size(96, 20)
        Me.TextBoxHungThreadThreshold.TabIndex = 3
        '
        'LabelHungThreadThreshold
        '
        Me.LabelHungThreadThreshold.Location = New System.Drawing.Point(8, 120)
        Me.LabelHungThreadThreshold.Name = "LabelHungThreadThreshold"
        Me.LabelHungThreadThreshold.Size = New System.Drawing.Size(96, 32)
        Me.LabelHungThreadThreshold.TabIndex = 2
        Me.LabelHungThreadThreshold.Text = "Hung Thread Threshold:"
        '
        'TextBoxStartThreadThreshold
        '
        Me.TextBoxStartThreadThreshold.Location = New System.Drawing.Point(128, 72)
        Me.TextBoxStartThreadThreshold.Name = "TextBoxStartThreadThreshold"
        Me.TextBoxStartThreadThreshold.Size = New System.Drawing.Size(96, 20)
        Me.TextBoxStartThreadThreshold.TabIndex = 1
        '
        'LabelStartThreadThreshold
        '
        Me.LabelStartThreadThreshold.Location = New System.Drawing.Point(8, 72)
        Me.LabelStartThreadThreshold.Name = "LabelStartThreadThreshold"
        Me.LabelStartThreadThreshold.Size = New System.Drawing.Size(88, 32)
        Me.LabelStartThreadThreshold.TabIndex = 0
        Me.LabelStartThreadThreshold.Text = "Start Thread Threshold:"
        '
        'LabelThreads
        '
        Me.LabelThreads.Location = New System.Drawing.Point(32, 24)
        Me.LabelThreads.Name = "LabelThreads"
        Me.LabelThreads.Size = New System.Drawing.Size(112, 16)
        Me.LabelThreads.TabIndex = 1
        Me.LabelThreads.Text = "Max Threads:"
        '
        'NumericUpDownMaxThreads
        '
        Me.NumericUpDownMaxThreads.Location = New System.Drawing.Point(169, 20)
        Me.NumericUpDownMaxThreads.Maximum = New Decimal(New Integer() {33, 0, 0, 0})
        Me.NumericUpDownMaxThreads.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownMaxThreads.Name = "NumericUpDownMaxThreads"
        Me.NumericUpDownMaxThreads.Size = New System.Drawing.Size(40, 20)
        Me.NumericUpDownMaxThreads.TabIndex = 0
        Me.NumericUpDownMaxThreads.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TabSelectionMode
        '
        Me.TabSelectionMode.Controls.Add(Me.LabelSelectionMode)
        Me.TabSelectionMode.Controls.Add(Me.ComboBoxSelectionMode)
        Me.TabSelectionMode.Location = New System.Drawing.Point(4, 22)
        Me.TabSelectionMode.Name = "TabSelectionMode"
        Me.TabSelectionMode.Size = New System.Drawing.Size(288, 390)
        Me.TabSelectionMode.TabIndex = 3
        Me.TabSelectionMode.Text = "SelectionMode"
        Me.TabSelectionMode.UseVisualStyleBackColor = True
        Me.TabSelectionMode.Visible = False
        '
        'LabelSelectionMode
        '
        Me.LabelSelectionMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSelectionMode.Location = New System.Drawing.Point(72, 48)
        Me.LabelSelectionMode.Name = "LabelSelectionMode"
        Me.LabelSelectionMode.Size = New System.Drawing.Size(128, 32)
        Me.LabelSelectionMode.TabIndex = 1
        Me.LabelSelectionMode.Text = "Selection Mode"
        Me.LabelSelectionMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBoxSelectionMode
        '
        Me.ComboBoxSelectionMode.Items.AddRange(New Object() {"MultiExtended", "MultiSimple", "None", "Single"})
        Me.ComboBoxSelectionMode.Location = New System.Drawing.Point(24, 96)
        Me.ComboBoxSelectionMode.Name = "ComboBoxSelectionMode"
        Me.ComboBoxSelectionMode.Size = New System.Drawing.Size(216, 21)
        Me.ComboBoxSelectionMode.TabIndex = 0
        '
        'TabCells
        '
        Me.TabCells.Controls.Add(Me.LabelCellWidthValue)
        Me.TabCells.Controls.Add(Me.LabelCellWidth)
        Me.TabCells.Controls.Add(Me.LabelCellVerticalSpacingValue)
        Me.TabCells.Controls.Add(Me.LabelCellVerticalSpacing)
        Me.TabCells.Controls.Add(Me.LabelCellHorizontalSpacing)
        Me.TabCells.Controls.Add(Me.HScrollBarCellWidth)
        Me.TabCells.Controls.Add(Me.HScrollBarCellVerticalSpacing)
        Me.TabCells.Controls.Add(Me.HScrollBarCellHorizontalSpacing)
        Me.TabCells.Controls.Add(Me.LabelCellHorizontalSpacingValue)
        Me.TabCells.Controls.Add(Me.LabelCellHeightValue)
        Me.TabCells.Controls.Add(Me.HScrollBarCellHeight)
        Me.TabCells.Controls.Add(Me.LabelCellHeight)
        Me.TabCells.Controls.Add(Me.LabelCellBorderWidth)
        Me.TabCells.Controls.Add(Me.ButtonChooseCellColor)
        Me.TabCells.Controls.Add(Me.ButtonSet)
        Me.TabCells.Controls.Add(Me.LabelCellBorderWidthValue)
        Me.TabCells.Controls.Add(Me.HScrollBarCellBorderWidth)
        Me.TabCells.Location = New System.Drawing.Point(4, 22)
        Me.TabCells.Name = "TabCells"
        Me.TabCells.Size = New System.Drawing.Size(288, 390)
        Me.TabCells.TabIndex = 0
        Me.TabCells.Text = "Thumbnail Attributes"
        Me.TabCells.UseVisualStyleBackColor = True
        Me.TabCells.Visible = False
        '
        'LabelCellWidthValue
        '
        Me.LabelCellWidthValue.Location = New System.Drawing.Point(200, 80)
        Me.LabelCellWidthValue.Name = "LabelCellWidthValue"
        Me.LabelCellWidthValue.Size = New System.Drawing.Size(56, 16)
        Me.LabelCellWidthValue.TabIndex = 17
        '
        'LabelCellWidth
        '
        Me.LabelCellWidth.Location = New System.Drawing.Point(24, 56)
        Me.LabelCellWidth.Name = "LabelCellWidth"
        Me.LabelCellWidth.Size = New System.Drawing.Size(144, 16)
        Me.LabelCellWidth.TabIndex = 16
        Me.LabelCellWidth.Text = "Cell Width"
        '
        'LabelCellVerticalSpacingValue
        '
        Me.LabelCellVerticalSpacingValue.Location = New System.Drawing.Point(200, 224)
        Me.LabelCellVerticalSpacingValue.Name = "LabelCellVerticalSpacingValue"
        Me.LabelCellVerticalSpacingValue.Size = New System.Drawing.Size(56, 16)
        Me.LabelCellVerticalSpacingValue.TabIndex = 15
        '
        'LabelCellVerticalSpacing
        '
        Me.LabelCellVerticalSpacing.Location = New System.Drawing.Point(24, 200)
        Me.LabelCellVerticalSpacing.Name = "LabelCellVerticalSpacing"
        Me.LabelCellVerticalSpacing.Size = New System.Drawing.Size(160, 16)
        Me.LabelCellVerticalSpacing.TabIndex = 14
        Me.LabelCellVerticalSpacing.Text = "Cell Vertical Spacing"
        '
        'LabelCellHorizontalSpacing
        '
        Me.LabelCellHorizontalSpacing.Location = New System.Drawing.Point(24, 152)
        Me.LabelCellHorizontalSpacing.Name = "LabelCellHorizontalSpacing"
        Me.LabelCellHorizontalSpacing.Size = New System.Drawing.Size(128, 16)
        Me.LabelCellHorizontalSpacing.TabIndex = 13
        Me.LabelCellHorizontalSpacing.Text = "Cell Horizontal Spacing"
        '
        'HScrollBarCellWidth
        '
        Me.HScrollBarCellWidth.Location = New System.Drawing.Point(24, 80)
        Me.HScrollBarCellWidth.Minimum = 32
        Me.HScrollBarCellWidth.Name = "HScrollBarCellWidth"
        Me.HScrollBarCellWidth.Size = New System.Drawing.Size(160, 16)
        Me.HScrollBarCellWidth.TabIndex = 12
        Me.HScrollBarCellWidth.Value = 32
        '
        'HScrollBarCellVerticalSpacing
        '
        Me.HScrollBarCellVerticalSpacing.Location = New System.Drawing.Point(24, 224)
        Me.HScrollBarCellVerticalSpacing.Maximum = 90
        Me.HScrollBarCellVerticalSpacing.Minimum = 1
        Me.HScrollBarCellVerticalSpacing.Name = "HScrollBarCellVerticalSpacing"
        Me.HScrollBarCellVerticalSpacing.Size = New System.Drawing.Size(160, 16)
        Me.HScrollBarCellVerticalSpacing.TabIndex = 11
        Me.HScrollBarCellVerticalSpacing.Value = 1
        '
        'HScrollBarCellHorizontalSpacing
        '
        Me.HScrollBarCellHorizontalSpacing.Location = New System.Drawing.Point(24, 176)
        Me.HScrollBarCellHorizontalSpacing.Maximum = 90
        Me.HScrollBarCellHorizontalSpacing.Minimum = 1
        Me.HScrollBarCellHorizontalSpacing.Name = "HScrollBarCellHorizontalSpacing"
        Me.HScrollBarCellHorizontalSpacing.Size = New System.Drawing.Size(160, 16)
        Me.HScrollBarCellHorizontalSpacing.TabIndex = 10
        Me.HScrollBarCellHorizontalSpacing.Value = 1
        '
        'LabelCellHorizontalSpacingValue
        '
        Me.LabelCellHorizontalSpacingValue.Location = New System.Drawing.Point(200, 176)
        Me.LabelCellHorizontalSpacingValue.Name = "LabelCellHorizontalSpacingValue"
        Me.LabelCellHorizontalSpacingValue.Size = New System.Drawing.Size(56, 16)
        Me.LabelCellHorizontalSpacingValue.TabIndex = 9
        '
        'LabelCellHeightValue
        '
        Me.LabelCellHeightValue.Location = New System.Drawing.Point(200, 128)
        Me.LabelCellHeightValue.Name = "LabelCellHeightValue"
        Me.LabelCellHeightValue.Size = New System.Drawing.Size(56, 16)
        Me.LabelCellHeightValue.TabIndex = 7
        '
        'HScrollBarCellHeight
        '
        Me.HScrollBarCellHeight.LargeChange = 2
        Me.HScrollBarCellHeight.Location = New System.Drawing.Point(24, 128)
        Me.HScrollBarCellHeight.Minimum = 32
        Me.HScrollBarCellHeight.Name = "HScrollBarCellHeight"
        Me.HScrollBarCellHeight.Size = New System.Drawing.Size(160, 16)
        Me.HScrollBarCellHeight.TabIndex = 6
        Me.HScrollBarCellHeight.Value = 32
        '
        'LabelCellHeight
        '
        Me.LabelCellHeight.Location = New System.Drawing.Point(24, 104)
        Me.LabelCellHeight.Name = "LabelCellHeight"
        Me.LabelCellHeight.Size = New System.Drawing.Size(104, 16)
        Me.LabelCellHeight.TabIndex = 5
        Me.LabelCellHeight.Text = "Cell Height"
        '
        'LabelCellBorderWidth
        '
        Me.LabelCellBorderWidth.Location = New System.Drawing.Point(21, 252)
        Me.LabelCellBorderWidth.Name = "LabelCellBorderWidth"
        Me.LabelCellBorderWidth.Size = New System.Drawing.Size(136, 16)
        Me.LabelCellBorderWidth.TabIndex = 4
        Me.LabelCellBorderWidth.Text = "Cell Border Width "
        '
        'ButtonChooseCellColor
        '
        Me.ButtonChooseCellColor.Location = New System.Drawing.Point(24, 16)
        Me.ButtonChooseCellColor.Name = "ButtonChooseCellColor"
        Me.ButtonChooseCellColor.Size = New System.Drawing.Size(168, 24)
        Me.ButtonChooseCellColor.TabIndex = 3
        Me.ButtonChooseCellColor.Text = "Choose Cell Color"
        '
        'ButtonSet
        '
        Me.ButtonSet.Location = New System.Drawing.Point(40, 320)
        Me.ButtonSet.Name = "ButtonSet"
        Me.ButtonSet.Size = New System.Drawing.Size(128, 24)
        Me.ButtonSet.TabIndex = 2
        Me.ButtonSet.Text = "Invoke Settings"
        '
        'LabelCellBorderWidthValue
        '
        Me.LabelCellBorderWidthValue.Location = New System.Drawing.Point(197, 276)
        Me.LabelCellBorderWidthValue.Name = "LabelCellBorderWidthValue"
        Me.LabelCellBorderWidthValue.Size = New System.Drawing.Size(56, 24)
        Me.LabelCellBorderWidthValue.TabIndex = 1
        '
        'HScrollBarCellBorderWidth
        '
        Me.HScrollBarCellBorderWidth.LargeChange = 2
        Me.HScrollBarCellBorderWidth.Location = New System.Drawing.Point(21, 276)
        Me.HScrollBarCellBorderWidth.Maximum = 20
        Me.HScrollBarCellBorderWidth.Minimum = 1
        Me.HScrollBarCellBorderWidth.Name = "HScrollBarCellBorderWidth"
        Me.HScrollBarCellBorderWidth.Size = New System.Drawing.Size(160, 16)
        Me.HScrollBarCellBorderWidth.TabIndex = 0
        Me.HScrollBarCellBorderWidth.Value = 1
        '
        'TabScrolling
        '
        Me.TabScrolling.Controls.Add(Me.GroupBoxScrollInc)
        Me.TabScrolling.Controls.Add(Me.GroupBoxScrolling)
        Me.TabScrolling.Location = New System.Drawing.Point(4, 22)
        Me.TabScrolling.Name = "TabScrolling"
        Me.TabScrolling.Padding = New System.Windows.Forms.Padding(3)
        Me.TabScrolling.Size = New System.Drawing.Size(288, 390)
        Me.TabScrolling.TabIndex = 4
        Me.TabScrolling.Text = "Scrolling"
        Me.TabScrolling.UseVisualStyleBackColor = True
        '
        'GroupBoxScrollInc
        '
        Me.GroupBoxScrollInc.Controls.Add(Me.ButtonLargeIncrement)
        Me.GroupBoxScrollInc.Controls.Add(Me.ButtonLargeDecrement)
        Me.GroupBoxScrollInc.Controls.Add(Me.ButtonSmallDecrement)
        Me.GroupBoxScrollInc.Controls.Add(Me.ButtonSmallIncrement)
        Me.GroupBoxScrollInc.Location = New System.Drawing.Point(20, 160)
        Me.GroupBoxScrollInc.Name = "GroupBoxScrollInc"
        Me.GroupBoxScrollInc.Size = New System.Drawing.Size(242, 56)
        Me.GroupBoxScrollInc.TabIndex = 10
        Me.GroupBoxScrollInc.TabStop = False
        Me.GroupBoxScrollInc.Text = "Scroll by lines and pages"
        '
        'ButtonLargeIncrement
        '
        Me.ButtonLargeIncrement.Location = New System.Drawing.Point(186, 19)
        Me.ButtonLargeIncrement.Name = "ButtonLargeIncrement"
        Me.ButtonLargeIncrement.Size = New System.Drawing.Size(35, 23)
        Me.ButtonLargeIncrement.TabIndex = 10
        Me.ButtonLargeIncrement.Text = ">>"
        Me.ButtonLargeIncrement.UseVisualStyleBackColor = True
        '
        'ButtonLargeDecrement
        '
        Me.ButtonLargeDecrement.Location = New System.Drawing.Point(21, 19)
        Me.ButtonLargeDecrement.Name = "ButtonLargeDecrement"
        Me.ButtonLargeDecrement.Size = New System.Drawing.Size(35, 23)
        Me.ButtonLargeDecrement.TabIndex = 10
        Me.ButtonLargeDecrement.Text = "<<"
        Me.ButtonLargeDecrement.UseVisualStyleBackColor = True
        '
        'ButtonSmallDecrement
        '
        Me.ButtonSmallDecrement.Location = New System.Drawing.Point(77, 19)
        Me.ButtonSmallDecrement.Name = "ButtonSmallDecrement"
        Me.ButtonSmallDecrement.Size = New System.Drawing.Size(35, 23)
        Me.ButtonSmallDecrement.TabIndex = 10
        Me.ButtonSmallDecrement.Text = "<"
        Me.ButtonSmallDecrement.UseVisualStyleBackColor = True
        '
        'ButtonSmallIncrement
        '
        Me.ButtonSmallIncrement.Location = New System.Drawing.Point(133, 19)
        Me.ButtonSmallIncrement.Name = "ButtonSmallIncrement"
        Me.ButtonSmallIncrement.Size = New System.Drawing.Size(35, 23)
        Me.ButtonSmallIncrement.TabIndex = 10
        Me.ButtonSmallIncrement.Text = ">"
        Me.ButtonSmallIncrement.UseVisualStyleBackColor = True
        '
        'GroupBoxScrolling
        '
        Me.GroupBoxScrolling.Controls.Add(Me.ButtonScroll)
        Me.GroupBoxScrolling.Controls.Add(Me.ComboBoxScrollTo)
        Me.GroupBoxScrolling.Controls.Add(Me.TextBoxScrollAmount)
        Me.GroupBoxScrolling.Location = New System.Drawing.Point(20, 30)
        Me.GroupBoxScrolling.Name = "GroupBoxScrolling"
        Me.GroupBoxScrolling.Size = New System.Drawing.Size(242, 97)
        Me.GroupBoxScrolling.TabIndex = 9
        Me.GroupBoxScrolling.TabStop = False
        Me.GroupBoxScrolling.Text = "Scroll to Position"
        '
        'ButtonScroll
        '
        Me.ButtonScroll.Location = New System.Drawing.Point(55, 57)
        Me.ButtonScroll.Name = "ButtonScroll"
        Me.ButtonScroll.Size = New System.Drawing.Size(134, 23)
        Me.ButtonScroll.TabIndex = 3
        Me.ButtonScroll.Text = "Scroll"
        Me.ButtonScroll.UseVisualStyleBackColor = True
        '
        'ComboBoxScrollTo
        '
        Me.ComboBoxScrollTo.FormattingEnabled = True
        Me.ComboBoxScrollTo.Items.AddRange(New Object() {"Scroll to Item...", "Scroll to Row..."})
        Me.ComboBoxScrollTo.Location = New System.Drawing.Point(6, 19)
        Me.ComboBoxScrollTo.Name = "ComboBoxScrollTo"
        Me.ComboBoxScrollTo.Size = New System.Drawing.Size(153, 21)
        Me.ComboBoxScrollTo.TabIndex = 2
        Me.ComboBoxScrollTo.Text = "(Select scrolling method)"
        '
        'TextBoxScrollAmount
        '
        Me.TextBoxScrollAmount.Location = New System.Drawing.Point(175, 19)
        Me.TextBoxScrollAmount.Name = "TextBoxScrollAmount"
        Me.TextBoxScrollAmount.Size = New System.Drawing.Size(46, 20)
        Me.TextBoxScrollAmount.TabIndex = 0
        Me.TextBoxScrollAmount.Text = "0"
        '
        'LabelLastError
        '
        Me.LabelLastError.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLastError.Location = New System.Drawing.Point(1, 638)
        Me.LabelLastError.Name = "LabelLastError"
        Me.LabelLastError.Size = New System.Drawing.Size(136, 24)
        Me.LabelLastError.TabIndex = 31
        Me.LabelLastError.Text = "Last Error Reported:"
        Me.LabelLastError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelLastErrorDescription
        '
        Me.LabelLastErrorDescription.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelLastErrorDescription.Location = New System.Drawing.Point(146, 638)
        Me.LabelLastErrorDescription.Name = "LabelLastErrorDescription"
        Me.LabelLastErrorDescription.Size = New System.Drawing.Size(678, 44)
        Me.LabelLastErrorDescription.TabIndex = 30
        '
        'TextBoxSampleDescription
        '
        Me.TextBoxSampleDescription.Location = New System.Drawing.Point(304, 0)
        Me.TextBoxSampleDescription.Name = "TextBoxSampleDescription"
        Me.TextBoxSampleDescription.Size = New System.Drawing.Size(520, 144)
        Me.TextBoxSampleDescription.TabIndex = 29
        Me.TextBoxSampleDescription.Text = resources.GetString("TextBoxSampleDescription.Text")
        '
        'ButtonRemove
        '
        Me.ButtonRemove.Location = New System.Drawing.Point(8, 586)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(129, 32)
        Me.ButtonRemove.TabIndex = 27
        Me.ButtonRemove.Text = "Remove Selected Items"
        '
        'CheckedListBoxSelected
        '
        Me.CheckedListBoxSelected.Location = New System.Drawing.Point(146, 538)
        Me.CheckedListBoxSelected.Name = "CheckedListBoxSelected"
        Me.CheckedListBoxSelected.Size = New System.Drawing.Size(678, 79)
        Me.CheckedListBoxSelected.TabIndex = 26
        '
        'ButtonListSelected
        '
        Me.ButtonListSelected.Location = New System.Drawing.Point(8, 548)
        Me.ButtonListSelected.Name = "ButtonListSelected"
        Me.ButtonListSelected.Size = New System.Drawing.Size(129, 32)
        Me.ButtonListSelected.TabIndex = 25
        Me.ButtonListSelected.Text = "List Selected Items"
        '
        'ButtonClear
        '
        Me.ButtonClear.Location = New System.Drawing.Point(8, 506)
        Me.ButtonClear.Name = "ButtonClear"
        Me.ButtonClear.Size = New System.Drawing.Size(106, 36)
        Me.ButtonClear.TabIndex = 23
        Me.ButtonClear.Text = "Clear Images"
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(128, 8)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(144, 32)
        Me.ButtonCancel.TabIndex = 36
        Me.ButtonCancel.Text = "Cancel Thumbnail Loading"
        '
        'CheckBoxPreserve
        '
        Me.CheckBoxPreserve.Location = New System.Drawing.Point(8, 40)
        Me.CheckBoxPreserve.Name = "CheckBoxPreserve"
        Me.CheckBoxPreserve.Size = New System.Drawing.Size(104, 32)
        Me.CheckBoxPreserve.TabIndex = 37
        Me.CheckBoxPreserve.Text = "Preserve Black"
        '
        'CheckBoxRaw
        '
        Me.CheckBoxRaw.Location = New System.Drawing.Point(128, 40)
        Me.CheckBoxRaw.Name = "CheckBoxRaw"
        Me.CheckBoxRaw.Size = New System.Drawing.Size(152, 32)
        Me.CheckBoxRaw.TabIndex = 38
        Me.CheckBoxRaw.Text = "Camera Raw"
        '
        'CheckBoxOleDrag
        '
        Me.CheckBoxOleDrag.AutoSize = True
        Me.CheckBoxOleDrag.Location = New System.Drawing.Point(536, 510)
        Me.CheckBoxOleDrag.Name = "CheckBoxOleDrag"
        Me.CheckBoxOleDrag.Size = New System.Drawing.Size(121, 17)
        Me.CheckBoxOleDrag.TabIndex = 39
        Me.CheckBoxOleDrag.Text = "Allow OLE Dragging"
        Me.CheckBoxOleDrag.UseVisualStyleBackColor = True
        '
        'CheckBoxDrillDown
        '
        Me.CheckBoxDrillDown.AutoSize = True
        Me.CheckBoxDrillDown.Location = New System.Drawing.Point(122, 510)
        Me.CheckBoxDrillDown.Name = "CheckBoxDrillDown"
        Me.CheckBoxDrillDown.Size = New System.Drawing.Size(147, 17)
        Me.CheckBoxDrillDown.TabIndex = 40
        Me.CheckBoxDrillDown.Text = "Allow Directory Drill-Down"
        Me.CheckBoxDrillDown.UseVisualStyleBackColor = True
        '
        'CheckBoxThumbnailShift
        '
        Me.CheckBoxThumbnailShift.AutoSize = True
        Me.CheckBoxThumbnailShift.Location = New System.Drawing.Point(681, 510)
        Me.CheckBoxThumbnailShift.Name = "CheckBoxThumbnailShift"
        Me.CheckBoxThumbnailShift.Size = New System.Drawing.Size(141, 17)
        Me.CheckBoxThumbnailShift.TabIndex = 41
        Me.CheckBoxThumbnailShift.Text = "Allow Thumbnail Shifting"
        Me.CheckBoxThumbnailShift.UseVisualStyleBackColor = True
        '
        'CheckBoxTNDrag
        '
        Me.CheckBoxTNDrag.AutoSize = True
        Me.CheckBoxTNDrag.Location = New System.Drawing.Point(304, 510)
        Me.CheckBoxTNDrag.Name = "CheckBoxTNDrag"
        Me.CheckBoxTNDrag.Size = New System.Drawing.Size(200, 17)
        Me.CheckBoxTNDrag.TabIndex = 42
        Me.CheckBoxTNDrag.Text = "Allow Dragging between TN Controls"
        Me.CheckBoxTNDrag.UseVisualStyleBackColor = True
        '
        'ThumbnailXpress2
        '
        Me.ThumbnailXpress2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ThumbnailXpress2.BottomMargin = 5
        Me.ThumbnailXpress2.CameraRaw = False
        Me.ThumbnailXpress2.CellBorderColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.ThumbnailXpress2.CellBorderWidth = 1
        Me.ThumbnailXpress2.CellHeight = 80
        Me.ThumbnailXpress2.CellHorizontalSpacing = 5
        Me.ThumbnailXpress2.CellSpacingColor = System.Drawing.Color.White
        Me.ThumbnailXpress2.CellVerticalSpacing = 5
        Me.ThumbnailXpress2.CellWidth = 80
        Me.ThumbnailXpress2.DblClickDirectoryDrillDown = True
        Me.ThumbnailXpress2.DescriptorAlignment = CType((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter Or Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom), Accusoft.ThumbnailXpressSdk.DescriptorAlignments)
        Me.ThumbnailXpress2.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.[Default]
        Me.ThumbnailXpress2.EnableAsDragSourceForExternalDragDrop = True
        Me.ThumbnailXpress2.EnableAsDropTargetForExternalDragDrop = True
        Me.ThumbnailXpress2.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon
        Me.ThumbnailXpress2.FtpPassword = ""
        Me.ThumbnailXpress2.FtpUserName = ""
        Me.ThumbnailXpress2.InterComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress2.IntraComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress2.LeftMargin = 5
        Me.ThumbnailXpress2.Location = New System.Drawing.Point(836, 157)
        Me.ThumbnailXpress2.MaximumThumbnailBitDepth = 24
        Me.ThumbnailXpress2.Name = "ThumbnailXpress2"
        Me.ThumbnailXpress2.PreserveBlack = False
        Me.ThumbnailXpress2.ProxyServer = ""
        Me.ThumbnailXpress2.RightMargin = 5
        Me.ThumbnailXpress2.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical
        Me.ThumbnailXpress2.SelectBackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ThumbnailXpress2.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended
        Me.ThumbnailXpress2.ShowHourglass = True
        Me.ThumbnailXpress2.ShowImagePlaceholders = False
        Me.ThumbnailXpress2.Size = New System.Drawing.Size(146, 343)
        Me.ThumbnailXpress2.TabIndex = 44
        Me.ThumbnailXpress2.TextBackColor = System.Drawing.Color.White
        Me.ThumbnailXpress2.TopMargin = 5
        '
        'ThumbnailXpress1
        '
        Me.ThumbnailXpress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ThumbnailXpress1.BottomMargin = 5
        Me.ThumbnailXpress1.CameraRaw = False
        Me.ThumbnailXpress1.CellBorderColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.ThumbnailXpress1.CellBorderWidth = 1
        Me.ThumbnailXpress1.CellHeight = 80
        Me.ThumbnailXpress1.CellHorizontalSpacing = 5
        Me.ThumbnailXpress1.CellSpacingColor = System.Drawing.Color.White
        Me.ThumbnailXpress1.CellVerticalSpacing = 5
        Me.ThumbnailXpress1.CellWidth = 80
        Me.ThumbnailXpress1.DblClickDirectoryDrillDown = True
        Me.ThumbnailXpress1.DescriptorAlignment = CType((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter Or Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom), Accusoft.ThumbnailXpressSdk.DescriptorAlignments)
        Me.ThumbnailXpress1.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.[Default]
        Me.ThumbnailXpress1.EnableAsDragSourceForExternalDragDrop = True
        Me.ThumbnailXpress1.EnableAsDropTargetForExternalDragDrop = True
        Me.ThumbnailXpress1.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon
        Me.ThumbnailXpress1.FtpPassword = ""
        Me.ThumbnailXpress1.FtpUserName = ""
        Me.ThumbnailXpress1.InterComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress1.IntraComponentThumbnailDragDropEnabled = True
        Me.ThumbnailXpress1.LeftMargin = 5
        Me.ThumbnailXpress1.Location = New System.Drawing.Point(306, 157)
        Me.ThumbnailXpress1.MaximumThumbnailBitDepth = 24
        Me.ThumbnailXpress1.Name = "ThumbnailXpress1"
        Me.ThumbnailXpress1.PreserveBlack = False
        Me.ThumbnailXpress1.ProxyServer = ""
        Me.ThumbnailXpress1.RightMargin = 5
        Me.ThumbnailXpress1.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical
        Me.ThumbnailXpress1.SelectBackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(153, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ThumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended
        Me.ThumbnailXpress1.ShowHourglass = True
        Me.ThumbnailXpress1.ShowImagePlaceholders = False
        Me.ThumbnailXpress1.Size = New System.Drawing.Size(518, 343)
        Me.ThumbnailXpress1.TabIndex = 45
        Me.ThumbnailXpress1.TextBackColor = System.Drawing.Color.White
        Me.ThumbnailXpress1.TopMargin = 5
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(994, 695)
        Me.Controls.Add(Me.ThumbnailXpress1)
        Me.Controls.Add(Me.ThumbnailXpress2)
        Me.Controls.Add(Me.CheckBoxTNDrag)
        Me.Controls.Add(Me.CheckBoxThumbnailShift)
        Me.Controls.Add(Me.CheckBoxDrillDown)
        Me.Controls.Add(Me.CheckBoxOleDrag)
        Me.Controls.Add(Me.CheckBoxRaw)
        Me.Controls.Add(Me.CheckBoxPreserve)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.CheckBoxExpand)
        Me.Controls.Add(Me.tabControl1)
        Me.Controls.Add(Me.LabelLastError)
        Me.Controls.Add(Me.LabelLastErrorDescription)
        Me.Controls.Add(Me.TextBoxSampleDescription)
        Me.Controls.Add(Me.ButtonRemove)
        Me.Controls.Add(Me.CheckedListBoxSelected)
        Me.Controls.Add(Me.ButtonListSelected)
        Me.Controls.Add(Me.ButtonClear)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Thumbnail Viewer"
        Me.tabControl1.ResumeLayout(False)
        Me.TabFilters.ResumeLayout(False)
        Me.TabFilters.PerformLayout()
        Me.TabThread.ResumeLayout(False)
        Me.GroupBoxThreads.ResumeLayout(False)
        Me.GroupBoxThreads.PerformLayout()
        CType(Me.NumericUpDownMaxThreads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSelectionMode.ResumeLayout(False)
        Me.TabCells.ResumeLayout(False)
        Me.TabScrolling.ResumeLayout(False)
        Me.GroupBoxScrollInc.ResumeLayout(False)
        Me.GroupBoxScrolling.ResumeLayout(False)
        Me.GroupBoxScrolling.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'create the color dialog object here
        cd = New ColorDialog()

        Try
            '********MUST CALL UNLOCKRUNTIME at STARTUP*******
            'ThumbnailXpress1.Licensing.UnlockRuntime(1111, 1111, 1111, 1111)
            'ThumbnailXpress1.Licensing.UnlockIXRuntime(2222, 2222, 2222, 2222)
            Try
                ThumbnailXpress1.Licensing.AccessPdfXpress3()
            Catch eX As Exception
                PegasusError(New Exception("Unable to find PdfXpress. PDF abilities disabled."), LabelLastErrorDescription)
            End Try
            'ThumbnailXpress1.Licensing.UnlockPdfXpressRuntime(3333, 3333, 3333, 3333)
            'ImagXpress1.Licensing.UnlockRuntime(2222, 2222, 2222, 2222)

            'thread settings
            NumericUpDownMaxThreads.Value = ThumbnailXpress1.MaximumThreadCount
            TextBoxStartThreadThreshold.Text = ThumbnailXpress1.ThreadStartThreshold.ToString()
            TextBoxHungThreadThreshold.Text = ThumbnailXpress1.ThreadHungThreshold.ToString()

            strCurrentDir = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\..\Common\Images\")

            If (System.IO.Directory.Exists(strCurrentDir)) Then
                System.IO.Directory.SetCurrentDirectory(strCurrentDir)
            End If


            ThumbnailXpress1.DescriptorDisplayMethod = DescriptorDisplayMethods.ShortFilename
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            PegasusError(ex, LabelLastErrorDescription)
        Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
            PegasusError(ex, LabelLastErrorDescription)
        End Try
        CheckedListBoxSelected.CheckOnClick = True

        'Disable the special options (Dragging and drill-down)
        ThumbnailXpress1.DblClickDirectoryDrillDown = False
        ThumbnailXpress1.InterComponentThumbnailDragDropEnabled = False
        ThumbnailXpress1.EnableAsDragSourceForExternalDragDrop = False
        ThumbnailXpress1.EnableAsDropTargetForExternalDragDrop = False
        ThumbnailXpress1.IntraComponentThumbnailDragDropEnabled = False

        'Enable the dragging of thumbnails to the control
        ThumbnailXpress2.DblClickDirectoryDrillDown = False
        ThumbnailXpress2.InterComponentThumbnailDragDropEnabled = True
        ThumbnailXpress2.EnableAsDragSourceForExternalDragDrop = False
        ThumbnailXpress2.EnableAsDropTargetForExternalDragDrop = False
        ThumbnailXpress2.IntraComponentThumbnailDragDropEnabled = False

        'Hide the second ThumbnailXpress control
        Me.Width = 840

        'set some control defaults
        ThumbnailXpress1.CameraRaw = False
        ThumbnailXpress1.AllowDrop = True
        ComboBoxCompare.SelectedIndex = 0
        ComboBoxFilter.SelectedIndex = 0
        ComboBoxSelectionMode.SelectedIndex = 0
        HScrollBarCellBorderWidth.Value = 1
        HScrollBarCellHeight.Value = 65
        HScrollBarCellHorizontalSpacing.Value = 1
        HScrollBarCellVerticalSpacing.Value = 1
        HScrollBarCellWidth.Value = 65
        LabelCellBorderWidthValue.Text = HScrollBarCellBorderWidth.Value.ToString
        LabelCellHeightValue.Text = HScrollBarCellHeight.Value.ToString
        LabelCellHorizontalSpacingValue.Text = HScrollBarCellHorizontalSpacing.Value.ToString
        LabelCellVerticalSpacingValue.Text = HScrollBarCellVerticalSpacing.Value.ToString
        LabelCellWidthValue.Text = HScrollBarCellWidth.Value.ToString

        'date time value set here
        DateTimePickerFileDate.Value = System.DateTime.Now

        Application.EnableVisualStyles()
    End Sub



    Private Sub cmbchoice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxFilter.SelectedIndexChanged
        Select Case ComboBoxFilter.SelectedIndex
            Case 0
                CheckBoxEnabled.Enabled = True
                CheckBoxMatchDirectory.Enabled = True
                CheckBoxMatchCaseSensitive.Enabled = True
                CheckBoxIncludeSubDir.Enabled = False
                CheckBoxIncludeParentDir.Enabled = False
                DateTimePickerFileDate.Enabled = False
                ComboBoxCompare.Enabled = False
                CheckBoxIncludeHidden.Enabled = False
                TextBoxFileNamePattern.Enabled = True
                ' break 
            Case 1
                CheckBoxMatchDirectory.Enabled = True
                CheckBoxIncludeParentDir.Enabled = True
                CheckBoxIncludeSubDir.Enabled = True
                CheckBoxIncludeParentDir.Enabled = True
                CheckBoxIncludeHidden.Enabled = True
                CheckBoxMatchDirectory.Enabled = False
                CheckBoxMatchCaseSensitive.Enabled = False
                DateTimePickerFileDate.Enabled = False
                ComboBoxCompare.Enabled = False
                TextBoxFileSize.Enabled = False
                TextBoxFileNamePattern.Enabled = False
                ' break 
            Case 2
                CheckBoxMatchCaseSensitive.Enabled = False
                CheckBoxMatchDirectory.Enabled = False
                CheckBoxEnabled.Enabled = True
                ComboBoxCompare.Enabled = True
                DateTimePickerFileDate.Enabled = True
                TextBoxFileSize.Enabled = False
                CheckBoxIncludeSubDir.Enabled = False
                CheckBoxIncludeParentDir.Enabled = False
                CheckBoxIncludeHidden.Enabled = False
                TextBoxFileNamePattern.Enabled = False
                ' break 
            Case 3
                CheckBoxMatchCaseSensitive.Enabled = False
                CheckBoxMatchDirectory.Enabled = False
                CheckBoxEnabled.Enabled = True
                DateTimePickerFileDate.Enabled = True
                ComboBoxCompare.Enabled = True
                TextBoxFileSize.Enabled = False
                CheckBoxIncludeSubDir.Enabled = False
                CheckBoxIncludeParentDir.Enabled = False
                CheckBoxIncludeHidden.Enabled = False
                TextBoxFileNamePattern.Enabled = False
                ' break 
            Case 4
                CheckBoxMatchCaseSensitive.Enabled = False
                CheckBoxMatchDirectory.Enabled = False
                CheckBoxEnabled.Enabled = True
                DateTimePickerFileDate.Enabled = False
                TextBoxFileSize.Visible = True
                TextBoxFileSize.Enabled = True
                ComboBoxCompare.Enabled = True
                CheckBoxIncludeSubDir.Enabled = False
                CheckBoxIncludeParentDir.Enabled = False
                TextBoxFileNamePattern.Enabled = False
                ' break 
            Case 5
                CheckBoxEnabled.Enabled = False
                DateTimePickerFileDate.Enabled = False
                ComboBoxCompare.Enabled = False
                CheckBoxMatchCaseSensitive.Enabled = False
                CheckBoxMatchDirectory.Enabled = False
                CheckBoxIncludeParentDir.Enabled = False
                CheckBoxIncludeSubDir.Enabled = False
                CheckBoxIncludeHidden.Enabled = False
                TextBoxFileSize.Enabled = False
                TextBoxFileNamePattern.Enabled = False
                ' break 
        End Select
    End Sub

    Private Sub dateTimePickerFileDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePickerFileDate.ValueChanged
        If (Not filterItem Is Nothing) Then
            filterItem.FileDate = DateTimePickerFileDate.Value
        End If
    End Sub

    Private Sub ComboBoxSelectionMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxSelectionMode.SelectedIndexChanged
        Select Case ComboBoxSelectionMode.SelectedIndex
            Case 0
                ThumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended
                ' break 
            Case 1
                ThumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiSimple
                ' break 
            Case 2
                ThumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.None
                ' break 
            Case 3
                ThumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.Single
                ' break 
        End Select
    End Sub

    Private Sub ButtonChooseCellColor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonChooseCellColor.Click
        cd.ShowDialog()
    End Sub

    Private Sub HScrollBarCellBorderWidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBarCellBorderWidth.Scroll
        LabelCellBorderWidthValue.Text = HScrollBarCellBorderWidth.Value.ToString
    End Sub

    Private Sub HScrollBarCellHeight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBarCellHeight.Scroll
        LabelCellHeightValue.Text = HScrollBarCellHeight.Value.ToString
    End Sub

    Private Sub HScrollBarCellHorizontalSpacing_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBarCellHorizontalSpacing.Scroll
        LabelCellHorizontalSpacingValue.Text = HScrollBarCellHorizontalSpacing.Value.ToString
    End Sub

    Private Sub HScrollBarCellVerticalSpacing_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBarCellVerticalSpacing.Scroll
        LabelCellVerticalSpacingValue.Text = HScrollBarCellVerticalSpacing.Value.ToString
    End Sub

    Private Sub HScrollBarCellWidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBarCellWidth.Scroll
        LabelCellWidthValue.Text = HScrollBarCellWidth.Value.ToString
    End Sub

    Private Sub ButtonSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSet.Click
        ThumbnailXpress1.BeginUpdate()
        ThumbnailXpress1.CellBorderWidth = HScrollBarCellBorderWidth.Value
        ThumbnailXpress1.CellBorderColor = cd.Color

        ThumbnailXpress1.CellHeight = HScrollBarCellHeight.Value
        ThumbnailXpress1.CellHorizontalSpacing = HScrollBarCellHorizontalSpacing.Value
        ThumbnailXpress1.CellVerticalSpacing = HScrollBarCellVerticalSpacing.Value
        ThumbnailXpress1.CellWidth = HScrollBarCellWidth.Value
        ThumbnailXpress1.EndUpdate()
    End Sub

    Private Sub ButtonClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonClear.Click
        ThumbnailXpress1.Items.Clear()
        ThumbnailXpress2.Items.Clear()
        CheckedListBoxSelected.Items.Clear()
    End Sub

    Private Sub ButtonListSelected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonListSelected.Click
        Try
            'clear the list box
            CheckedListBoxSelected.Items.Clear()
            'add the selected items to the list box
            For i As Integer = 0 To ThumbnailXpress1.SelectedItems.Count - 1
                CheckedListBoxSelected.Items.Add(ThumbnailXpress1.SelectedItems(i))
            Next
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            PegasusError(ex, LabelLastErrorDescription)
        Catch ex As NullReferenceException
            PegasusError(ex, LabelLastErrorDescription)
        End Try
    End Sub


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Private cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Private cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Private cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.gif;*.hdp;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wdp;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*" & _
 ".dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.h" & _
 "dp;*.wdp|All Files (*.*)|*.*"

    Private Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        LabelLastErrorDescription.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Private Sub PegasusError(ByVal ErrorException As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        LabelLastErrorDescription.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Private Sub PegasusError(ByVal ErrorException As Accusoft.ImagXpressSdk.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        LabelLastErrorDescription.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

#End Region

    Function OpenThumbnailDir() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir

        If dlg.ShowDialog = DialogResult.OK Then

            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), dlg.FileName.Length - dlg.FileName.LastIndexOf("\"))

            If CheckBoxEnabled.Checked = True Then
                ThumbnailXpress1.Filters.Clear()
                filterItem.Type = CType(ComboBoxFilter.SelectedIndex, FilterType)
                filterItem.Comparison = CType(ComboBoxCompare.SelectedIndex, FilterComparison)
                filterItem.FileDate = DateTimePickerFileDate.Value
                If Not (TextBoxFileSize.Text = "") Then
                    filterItem.FileSize = System.Convert.ToInt32(TextBoxFileSize.Text)
                End If
                filterItem.CaseSensitive = CheckBoxMatchCaseSensitive.Checked
                filterItem.MatchAppliesToDirectory = CheckBoxMatchDirectory.Checked
                filterItem.IncludeHidden = CheckBoxIncludeHidden.Checked
                filterItem.IncludeParentDirectory = CheckBoxIncludeParentDir.Checked
                filterItem.IncludeSubDirectory = CheckBoxIncludeSubDir.Checked
                filterItem.FilenamePattern = TextBoxFileNamePattern.Text

                ThumbnailXpress1.Filters.Add(filterItem)
                ThumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\", 0)
            Else
                ThumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\", 0)
            End If
            Return strCurrentDir
        Else

            Return ""
        End If
    End Function

    'Open single files here or multi-page TIFF Image
    Private Function OpenThumbnailFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If dlg.ShowDialog() = DialogResult.OK Then
            'strImageFile = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"),dlg.FileName.Length - dlg.FileName.LastIndexOf("\"));
            strImageFile = dlg.FileName
            If CheckBoxExpand.Checked = True Then
                ThumbnailXpress1.Items.AddItemsFromFile(strImageFile, 0, True)
            Else
                ThumbnailXpress1.Items.AddItemsFromFile(strImageFile, 0, False)
            End If
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


    Private Sub MenuItemOpenDir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOpenDir.Click


        Try

            'clear out the error before the next opertation
            LabelLastErrorDescription.Text = ""

            'First we grab the filename of the image we want to open\
            'create a filter item here

            filterItem = ThumbnailXpress1.CreateFilter("test")
            filterItemCreated = True
            Dim result As System.String = OpenThumbnailDir()
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            PegasusError(ex, LabelLastErrorDescription)
        Catch ex As NullReferenceException
            PegasusError(ex, LabelLastErrorDescription)
        End Try

    End Sub

    Private Sub MenuItemOpenImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemOpenImage.Click
        Try

            'clear out the error before the next opertation
            LabelLastErrorDescription.Text = ""

            Dim result As System.String = OpenThumbnailFile()

        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException

            PegasusError(ex, LabelLastErrorDescription)

        Catch ex As NullReferenceException
            PegasusError(ex, LabelLastErrorDescription)
        End Try

    End Sub

    Private Sub MenuItemAboutTN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAboutTN.Click
        ThumbnailXpress1.AboutBox()
    End Sub

    Private Sub MenuItemExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemExit.Click
        Application.Exit()
    End Sub

    Private Sub ButtonRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRemove.Click
        Try
            Dim listBoxIndex As Integer = 0
            While listBoxIndex < CheckedListBoxSelected.Items.Count
                If CheckedListBoxSelected.GetItemChecked(listBoxIndex) = True Then
                    'remove this item from the ThumbnailXpress control and the list box
                    ThumbnailXpress1.Items.Remove(DirectCast(CheckedListBoxSelected.Items(listBoxIndex), Accusoft.ThumbnailXpressSdk.ThumbnailItem))
                    CheckedListBoxSelected.Items.RemoveAt(listBoxIndex)
                Else
                    'this item is not checked, move to the next item
                    listBoxIndex += 1
                End If
            End While
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException

            PegasusError(ex, LabelLastErrorDescription)
            ' Least specific:
        Catch ex As NullReferenceException
            PegasusError(ex, LabelLastErrorDescription)
        End Try
    End Sub



    Private Sub CheckBoxMatchDirectory_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxMatchDirectory.CheckedChanged
        If CheckBoxMatchDirectory.Checked = True Then
            filteritem.MatchAppliesToDirectory = True
        Else
            filteritem.MatchAppliesToDirectory = False
        End If
    End Sub

    Private Sub CheckBoxMatchCaseSensitive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxMatchCaseSensitive.CheckedChanged
        If CheckBoxMatchDirectory.Checked = True Then
            filteritem.CaseSensitive = True
        Else
            filteritem.CaseSensitive = False
        End If
    End Sub

    Private Sub CheckBoxIncludeHidden_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxIncludeHidden.CheckedChanged
        If CheckBoxIncludeHidden.Checked = True Then
            filteritem.IncludeHidden = True
        Else
            filteritem.IncludeHidden = False
        End If
    End Sub

    Private Sub CheckBoxEnabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxEnabled.CheckedChanged
        'make sure filterItem has been initialized
        If filterItemCreated = True Then
            If CheckBoxEnabled.Checked = True Then
                filteritem.Enabled = True
                ThumbnailXpress1.Filters.Clear()
                ThumbnailXpress1.Filters.Add(filteritem)
            Else
                filteritem.Enabled = False
            End If
        End If
    End Sub

    Private Sub CheckBoxIncludeParentDir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxIncludeParentDir.CheckedChanged
        If CheckBoxEnabled.Checked = True Then
            filteritem.IncludeParentDirectory = True
        Else
            filteritem.IncludeParentDirectory = False

        End If
    End Sub

    Private Sub CheckBoxIncludeSubDir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxIncludeSubDir.CheckedChanged
        If CheckBoxEnabled.Checked = True Then
            filteritem.IncludeSubDirectory = True
        Else
            filteritem.IncludeSubDirectory = False
        End If
    End Sub

    Private Sub ThumbnailXpress1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ThumbnailXpress1.DoubleClick
        Try
            'need to verify if we are on a folder etc.
            Dim info As Accusoft.ThumbnailXpressSdk.ThumbnailItem
            info = ThumbnailXpress1.GetItemFromPoint(selectedThumbnailPoint)

            If Not (info Is Nothing) Then
                If info.Type = ThumbnailType.SubDirectory Or info.Type = ThumbnailType.ParentDirectory Then
                    Exit Sub
                End If

                'implement the viewer via the Doubleclick event
                Dim selectedThumbnailItem As Accusoft.ThumbnailXpressSdk.ThumbnailItem
                selectedThumbnailItem = ThumbnailXpress1.GetItemFromPoint(selectedThumbnailPoint)
                Dim viewer As New ImageInfoDialog()
                viewer.AddImage(selectedThumbnailItem.Filename)
                viewer.ShowDialog(Me)

                Application.DoEvents()

            End If
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            PegasusError(ex, LabelLastErrorDescription)
        Catch ex As NullReferenceException
            PegasusError(ex, LabelLastErrorDescription)
        End Try
    End Sub

    Private Sub NumericUpDownMaxThreads_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownMaxThreads.ValueChanged
        Try
            ThumbnailXpress1.MaximumThreadCount = NumericUpDownMaxThreads.Value

        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            PegasusError(ex, LabelLastErrorDescription)

        End Try
    End Sub

    Private Sub ButtonSetThreads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetThreads.Click
        Try
            ThumbnailXpress1.ThreadStartThreshold = System.Convert.ToInt32(TextBoxStartThreadThreshold.Text)
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            TextBoxStartThreadThreshold.Text = ThumbnailXpress1.ThreadStartThreshold.ToString()
        End Try

        Try
            ThumbnailXpress1.ThreadHungThreshold = System.Convert.ToInt32(TextBoxHungThreadThreshold.Text)
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
            TextBoxHungThreadThreshold.Text = ThumbnailXpress1.ThreadHungThreshold.ToString()
        End Try

    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Try
            ThumbnailXpress1.Cancel()
        Catch ex As Accusoft.ThumbnailXpressSdk.ThumbnailXpressException
        End Try
    End Sub

    Private Sub CheckBoxPreserve_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxPreserve.CheckedChanged
        If CheckBoxPreserve.Checked = True Then
            ThumbnailXpress1.PreserveBlack = True
        Else
            ThumbnailXpress1.PreserveBlack = False
        End If
    End Sub

    Private Sub CheckBoxRaw_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxRaw.CheckedChanged
        If CheckBoxRaw.Checked = True Then
            ThumbnailXpress1.CameraRaw = True
        Else
            ThumbnailXpress1.CameraRaw = False
        End If
    End Sub

    Private Sub MenuItemAboutIX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemAboutIX.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub CheckBoxDrillDown_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxDrillDown.CheckedChanged
        'This enables/disables the Directory Drill-Down feature
        If CheckBoxDrillDown.Checked Then
            ThumbnailXpress1.DblClickDirectoryDrillDown = True

            'Enable the adding of parent and subdirectories for navigation
            filteritem.Enabled = True
            filteritem.IncludeSubDirectory = True
            filteritem.IncludeParentDirectory = True

            ComboBoxFilter.SelectedIndex = 1

            CheckBoxEnabled.Checked = True
            CheckBoxIncludeSubDir.Checked = True
            CheckBoxIncludeParentDir.Checked = True
        Else
            ThumbnailXpress1.DblClickDirectoryDrillDown = False
        End If

        ThumbnailXpress1.Filters.Clear()
        ThumbnailXpress1.Filters.Add(filteritem)
    End Sub

    Private Sub CheckBoxTNDrag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxTNDrag.CheckedChanged
        'Enables/disables dragging between TNX 2 controls.
        If CheckBoxTNDrag.Checked Then
            ThumbnailXpress1.InterComponentThumbnailDragDropEnabled = True
            Me.Width = 1000 'Show the second ThumbnailXpress control
        Else
            ThumbnailXpress1.InterComponentThumbnailDragDropEnabled = False
            Me.Width = 840 'Hide the second ThumbnailXpress control
        End If
    End Sub

    Private Sub CheckBoxOleDrag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxOleDrag.CheckedChanged
        'Enables OLE dragging
        If CheckBoxOleDrag.Checked Then
            ThumbnailXpress1.EnableAsDragSourceForExternalDragDrop = True
            ThumbnailXpress1.EnableAsDropTargetForExternalDragDrop = True
        Else
            ThumbnailXpress1.EnableAsDragSourceForExternalDragDrop = False
            ThumbnailXpress1.EnableAsDropTargetForExternalDragDrop = False
        End If
    End Sub

    Private Sub CheckBoxThumbnailShift_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxThumbnailShift.CheckedChanged
        'Allows swapping thumbnail positions within the control by mouse
        If CheckBoxThumbnailShift.Checked Then
            ThumbnailXpress1.IntraComponentThumbnailDragDropEnabled = True
        Else
            ThumbnailXpress1.IntraComponentThumbnailDragDropEnabled = False
        End If
    End Sub

    Private Sub ButtonScroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonScroll.Click
        Try
            Select Case ComboBoxScrollTo.SelectedIndex
                Case 0  'Scroll to item (Highlight it just to indicate which one it is)
                    ThumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.ItemIndex, System.Convert.ToInt32(TextBoxScrollAmount.Text))
                    ThumbnailXpress1.SelectedItems.Clear()
                    ThumbnailXpress1.SelectedItems.Add(ThumbnailXpress1.Items(System.Convert.ToInt32(TextBoxScrollAmount.Text)))
                Case 1  'Scroll to row or column
                    ThumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.ThumbPosition, System.Convert.ToInt32(TextBoxScrollAmount.Text))
            End Select
        Catch ex As Exception
            MsgBox("Cannot scroll. " & ex.Message)
        End Try
    End Sub

    Private Sub ButtonLargeDecrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLargeDecrement.Click
        'Scroll by a large decrease
        ThumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.LargeDecrease, 3)
    End Sub

    Private Sub ButtonLargeIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLargeIncrement.Click
        'Scroll by a large increase
        ThumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.LargeIncrease, 3)
    End Sub

    Private Sub ButtonSmallDecrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSmallDecrement.Click
        'Scroll by a small decrease
        ThumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.SmallDecrease, 1)
    End Sub

    Private Sub ButtonSmallIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSmallIncrement.Click
        'Scroll by a small increase
        ThumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.SmallIncrease, 1)
    End Sub

    Private Sub ThumbnailXpress1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ThumbnailXpress1.MouseDown
        selectedThumbnailPoint = New System.Drawing.Point(e.X, e.Y)
    End Sub
End Class

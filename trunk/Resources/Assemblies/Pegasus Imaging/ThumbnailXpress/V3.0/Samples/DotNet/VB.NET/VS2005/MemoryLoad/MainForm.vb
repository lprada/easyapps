'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Public Class MainForm

    Private theProc As Accusoft.ImagXpressSdk.Processor
    Private currentDirectory As System.String
    Private Const strDefaultImageFilter As System.String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*"

    Private Structure RotatedFile
        Public Path As System.String
        Public Rotation As System.Int32
    End Structure

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Demonstrate unlocking the controls
        'imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
        'thumbnailXpress1.Licensing.UnlockRuntime(5678, 5678, 5678, 5678)
        'thumbnailXpress1.Licensing.UnlockIXRuntime(1234, 1234, 1234, 1234)

        Dim theFilter As Accusoft.ThumbnailXpressSdk.Filter

        ' Add directories so that they can be navigated when using directory drill-down
        theFilter = thumbnailXpress1.CreateFilter("Directories")
        theFilter.Type = Accusoft.ThumbnailXpressSdk.FilterType.FileAttributes
        theFilter.Enabled = True
        theFilter.IncludeParentDirectory = True
        theFilter.IncludeSubDirectory = True

        thumbnailXpress1.Filters.Add(theFilter)
        thumbnailXpress1.DblClickDirectoryDrillDown = False

        ' Initialize the Processor object
        theProc = New Accusoft.ImagXpressSdk.Processor(imagXpress1)

        currentDirectory = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\..\Common\Images\")
        If (System.IO.Directory.Exists(currentDirectory)) Then
            System.IO.Directory.SetCurrentDirectory(currentDirectory)
        End If
        currentDirectory = System.IO.Directory.GetCurrentDirectory()

        ' Set the image loading source
        cbLoadSource.SelectedIndex = 0

    End Sub

    Private Sub loadImageViaStreamToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles loadImageViaStreamToolStripMenuItem.Click
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        Dim theFile As System.IO.FileStream
        Dim theThumbnail As Accusoft.ThumbnailXpressSdk.ThumbnailItem

        ' Add a thumbnail item from a stream
        dlg.Title = "Select image to load"
        dlg.Filter = strDefaultImageFilter
        If (Not dlg.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            Return
        End If

        Try

            theFile = New System.IO.FileStream(dlg.FileName, System.IO.FileMode.Open, IO.FileAccess.Read)

            theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image)
            theFile.Seek(0, IO.SeekOrigin.Begin)
            theThumbnail.FromStream(theFile)
            theThumbnail.Descriptor = System.IO.Path.GetFileName(dlg.FileName)

            ' In this sample, the UserTag is being given the path name of the file.
            ' This will be used for when the user specifies to load the image from the source file.
            ' Typically the UserTag property must be given a value (for example an empty string "")
            ' before calling the AddItemFromStream method or an exception will be thrown.
            Dim theRotatedFile As RotatedFile
            theRotatedFile.Path = theFile.Name
            theRotatedFile.Rotation = 0
            theThumbnail.UserTag = theRotatedFile

            thumbnailXpress1.Items.AddItemFromStream(theThumbnail)
            theFile.Close()

        Catch ex As Exception
            lblLastError.Text = ex.Message & "\n\n" & ex.Source & "\n"
        End Try

    End Sub

    Private Sub loadDirectoryViaStreamsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles loadDirectoryViaStreamsToolStripMenuItem.Click
        Dim dlg As New System.Windows.Forms.OpenFileDialog()
        Dim theFile As System.IO.FileStream
        Dim files As System.String()
        Dim directories As System.String()
        Dim theThumbnail As Accusoft.ThumbnailXpressSdk.ThumbnailItem
        Dim i As Long

        'Add a thumbnail items from a directory
        dlg.Title = "Select image to load"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = currentDirectory
        If dlg.ShowDialog() <> DialogResult.OK Then
            Return
        End If

        Try
            'Get a list of all the files (and sub directories) in the directory
            directories = System.IO.Directory.GetDirectories(System.IO.Path.GetDirectoryName(dlg.FileName) & "\", "*.*")
            files = System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(dlg.FileName) & "\", "*.*")

            'First, add all of the directories to ThumbnailXpress
            For i = 0 To directories.Length - 1
                theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.SubDirectory)
                theThumbnail = thumbnailXpress1.CreateThumbnailItem(directories(i), 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.SubDirectory)
                theThumbnail.Descriptor = System.IO.Path.GetFileName(directories(i))
                theThumbnail.UserTag = ""
                thumbnailXpress1.Items.Add(theThumbnail)
            Next

            'Now, add all of the files
            For i = 0 To files.Length - 1
                theFile = New System.IO.FileStream(files(i), System.IO.FileMode.Open, System.IO.FileAccess.Read)

                theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image)
                theFile.Seek(0, System.IO.SeekOrigin.Begin)
                theThumbnail.FromStream(theFile)
                theThumbnail.Descriptor = System.IO.Path.GetFileName(files(i))
                'Put the path of the file in the UserTag field
                Dim theRotatedFile As RotatedFile
                theRotatedFile.Path = files(i)
                theRotatedFile.Rotation = 0
                theThumbnail.UserTag = theRotatedFile
                thumbnailXpress1.Items.AddItemFromStream(theThumbnail)
                theFile.Close()
            Next

            'Finally, add an item for the parent directory
            theThumbnail = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image)
            theThumbnail = thumbnailXpress1.CreateThumbnailItem(System.IO.Path.GetDirectoryName(dlg.FileName) & "\..", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.ParentDirectory)
            theThumbnail.Descriptor = "."
            theThumbnail.UserTag = ""
            thumbnailXpress1.Items.Insert(0, theThumbnail)
        Catch ex As System.Exception
            lblLastError.Text = (ex.Message & vbLf & vbLf) + ex.Source & vbLf
        End Try
    End Sub

    Private Sub toolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStripMenuItem1.Click
        ' Clear all the thumbnails
        thumbnailXpress1.Items.Clear()
        If (Not imageXView1.Image Is Nothing) Then
            imageXView1.Image.Dispose()
        End If
    End Sub

    Private Sub exitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub btPgUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btPgUp.Click
        ' Scroll by a "page" or a large decrease
        thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.LargeDecrease, 1)
    End Sub

    Private Sub btRowUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRowUp.Click
        ' Scroll by a "row" or a small decrease
        thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.SmallDecrease, 1)
    End Sub

    Private Sub btRowDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRowDown.Click
        ' Scroll by a "row" or a small increase
        thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.SmallIncrease, 1)
    End Sub

    Private Sub btPgDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btPgDown.Click
        ' Scroll by a "page" or a large increase
        thumbnailXpress1.ScrollItems(Accusoft.ThumbnailXpressSdk.ScrollItemsType.LargeIncrease, 1)
    End Sub

    Private Sub btRotate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRotate.Click
        Dim theItem As Accusoft.ThumbnailXpressSdk.ThumbnailItem = Nothing
        Dim newImage As System.IO.MemoryStream
        Dim theIndex As Integer = 0
        Dim theDescriptor As System.String

        Try
            'Check for whether a thumbnail is selected
            If thumbnailXpress1.SelectedItems.Count = 0 Then
                lblLastError.Text = "No thumbnail selected. Please select a thumbnail."
                Return
            End If

            'Make sure an image is in the control
            If imageXView1.Image Is Nothing Then
                Return
            End If

            theItem = thumbnailXpress1.SelectedItems(0)
            'Get the first selected thumbnail
            If theItem.Type = Accusoft.ThumbnailXpressSdk.ThumbnailType.Unsupported Then
                Return
            End If

            'Get the path and rotation of the image so that they can be stored in the UserTag
            Dim theRotatedFile As RotatedFile
            theRotatedFile.Rotation = 90
            theRotatedFile.Path = ""
            If TypeOf theItem.UserTag Is RotatedFile Then
                theRotatedFile.Path = DirectCast(theItem.UserTag, RotatedFile).Path
                theRotatedFile.Rotation = DirectCast(theItem.UserTag, RotatedFile).Rotation + 90
                If theRotatedFile.Rotation = 360 Then
                    theRotatedFile.Rotation = 0
                End If
            End If

            'Doesn't have an image that we could use
            theIndex = thumbnailXpress1.Items.IndexOf(theItem)
            'Get it's position in the index
            theDescriptor = theItem.Descriptor

            theProc.Image = imageXView1.Image
            theProc.Rotate(90)

            'Now, pass this back to ThumbnailXpress (in the place of where it was)
            newImage = New System.IO.MemoryStream()
            imageXView1.Image = theProc.Image.Copy()
            theProc.Image.SaveStream(newImage, Accusoft.ImagXpressSdk.ImageXFormat.Bmp)
            newImage.Seek(0, System.IO.SeekOrigin.Begin)

            theItem = thumbnailXpress1.CreateThumbnailItem("", 1, Accusoft.ThumbnailXpressSdk.ThumbnailType.Image)
            theItem.FromStream(newImage)
            theItem.Descriptor = theDescriptor

            'Store the path and rotation in the UserTag so that it can be used later to
            'load the original image with the rotation
            theItem.UserTag = theRotatedFile
            thumbnailXpress1.Items.RemoveAt(theIndex)
            thumbnailXpress1.Items.InsertItemFromStream(theIndex, theItem)
            newImage.Close()
            thumbnailXpress1.SelectedItems.Add(thumbnailXpress1.Items(theIndex))
        Catch ex As System.Exception
            lblLastError.Text = (ex.Message & vbLf & vbLf) + ex.Source & vbLf
        End Try
    End Sub

    Private Sub thumbnailXpress1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles thumbnailXpress1.SelectedIndexChanged
        Dim theItem As Accusoft.ThumbnailXpressSdk.ThumbnailItem

        'Make sure that we have a thumbnail item selected
        If thumbnailXpress1.SelectedItems.Count = 0 Then
            Return
        End If
        lblLastError.Text = "No Error."

        Try
            'Get ready to load the new image
            theItem = thumbnailXpress1.SelectedItems(0)
            theItem = thumbnailXpress1.Items(thumbnailXpress1.Items.IndexOf(theItem))
            If imageXView1.Image IsNot Nothing Then
                imageXView1.Image.Dispose()
            End If

            Select Case cbLoadSource.SelectedIndex
                Case 0
                    'Load the image from the ThumbnailItem
                    imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress1, theItem.Hdib)
                    Exit Select
                Case 1
                    'Load the image from the source image file
                    'First, check to see if the UserTag has an empty string.
                    If theItem.UserTag.ToString() = "" Then
                        imageXView1.Image = Nothing
                        lblLastError.Text = "The selected thumbnail does not have an associated source image file."
                    Else
                        ' Load the image from the path stored in the UserTag using the rotation stored in the UserTag.
                        Dim loadOptions As New Accusoft.ImagXpressSdk.LoadOptions()
                        Select Case DirectCast(theItem.UserTag, RotatedFile).Rotation
                            Case 90
                                loadOptions.Rotation = Accusoft.ImagXpressSdk.RotateAngle.Rotate90
                                Exit Select
                            Case 180
                                loadOptions.Rotation = Accusoft.ImagXpressSdk.RotateAngle.Rotate180
                                Exit Select
                            Case 270
                                loadOptions.Rotation = Accusoft.ImagXpressSdk.RotateAngle.Rotate270
                                Exit Select
                            Case Else
                                Exit Select
                        End Select
                        imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, DirectCast(theItem.UserTag, RotatedFile).Path.ToString(), loadOptions)
                    End If
                    Exit Select
            End Select

            imageXView1.ZoomToFit(Accusoft.ImagXpressSdk.ZoomToFitType.FitBest)
        Catch ex As Exception
            'Display the error and clear whatever may be in the view
            lblLastError.Text = (ex.Message & vbLf & vbLf) + ex.Source & vbLf
            If imageXView1.Image IsNot Nothing Then
                imageXView1.Image.Dispose()
            End If
        End Try
    End Sub

    Private Sub aboutThumbnailXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles aboutThumbnailXpressToolStripMenuItem.Click
        thumbnailXpress1.AboutBox()
    End Sub

    Private Sub aboutImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles aboutImagXpressToolStripMenuItem.Click
        imagXpress1.AboutBox()
    End Sub
End Class

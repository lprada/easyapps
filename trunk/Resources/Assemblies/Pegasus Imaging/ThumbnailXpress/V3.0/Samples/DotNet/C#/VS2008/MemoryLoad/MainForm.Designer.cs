namespace MemoryLoad
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImageViaStreamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDirectoryViaStreamsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutThumbnailXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutImagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btRotate = new System.Windows.Forms.Button();
            this.btPgUp = new System.Windows.Forms.Button();
            this.btRowUp = new System.Windows.Forms.Button();
            this.btRowDown = new System.Windows.Forms.Button();
            this.btPgDown = new System.Windows.Forms.Button();
            this.lstbDescription = new System.Windows.Forms.ListBox();
            this.gbLastError = new System.Windows.Forms.GroupBox();
            this.lblLastError = new System.Windows.Forms.Label();
            this.cbLoadSource = new System.Windows.Forms.ComboBox();
            this.thumbnailXpress1 = new Accusoft.ThumbnailXpressSdk.ThumbnailXpress();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.mainMenu.SuspendLayout();
            this.gbLastError.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(914, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageViaStreamToolStripMenuItem,
            this.loadDirectoryViaStreamsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadImageViaStreamToolStripMenuItem
            // 
            this.loadImageViaStreamToolStripMenuItem.Name = "loadImageViaStreamToolStripMenuItem";
            this.loadImageViaStreamToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.loadImageViaStreamToolStripMenuItem.Text = "Load image via stream...";
            this.loadImageViaStreamToolStripMenuItem.Click += new System.EventHandler(this.loadImageViaStreamToolStripMenuItem_Click);
            // 
            // loadDirectoryViaStreamsToolStripMenuItem
            // 
            this.loadDirectoryViaStreamsToolStripMenuItem.Name = "loadDirectoryViaStreamsToolStripMenuItem";
            this.loadDirectoryViaStreamsToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.loadDirectoryViaStreamsToolStripMenuItem.Text = "Load directory via stream...";
            this.loadDirectoryViaStreamsToolStripMenuItem.Click += new System.EventHandler(this.loadDirectoryViaStreamsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem1.Text = "Clear Thumbnails";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(213, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutThumbnailXpressToolStripMenuItem,
            this.aboutImagXpressToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutThumbnailXpressToolStripMenuItem
            // 
            this.aboutThumbnailXpressToolStripMenuItem.Name = "aboutThumbnailXpressToolStripMenuItem";
            this.aboutThumbnailXpressToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.aboutThumbnailXpressToolStripMenuItem.Text = "About ThumbnailXpress...";
            this.aboutThumbnailXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutThumbnailXpressToolStripMenuItem_Click);
            // 
            // aboutImagXpressToolStripMenuItem
            // 
            this.aboutImagXpressToolStripMenuItem.Name = "aboutImagXpressToolStripMenuItem";
            this.aboutImagXpressToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.aboutImagXpressToolStripMenuItem.Text = "About ImagXpress...";
            this.aboutImagXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutImagXpressToolStripMenuItem_Click);
            // 
            // btRotate
            // 
            this.btRotate.Location = new System.Drawing.Point(674, 426);
            this.btRotate.Name = "btRotate";
            this.btRotate.Size = new System.Drawing.Size(172, 27);
            this.btRotate.TabIndex = 0;
            this.btRotate.Text = "Rotate 90 degrees";
            this.btRotate.UseVisualStyleBackColor = true;
            this.btRotate.Click += new System.EventHandler(this.btRotate_Click);
            // 
            // btPgUp
            // 
            this.btPgUp.Location = new System.Drawing.Point(20, 426);
            this.btPgUp.Name = "btPgUp";
            this.btPgUp.Size = new System.Drawing.Size(146, 27);
            this.btPgUp.TabIndex = 3;
            this.btPgUp.Text = "Page Up";
            this.btPgUp.UseVisualStyleBackColor = true;
            this.btPgUp.Click += new System.EventHandler(this.btPgUp_Click);
            // 
            // btRowUp
            // 
            this.btRowUp.Location = new System.Drawing.Point(169, 426);
            this.btRowUp.Name = "btRowUp";
            this.btRowUp.Size = new System.Drawing.Size(146, 27);
            this.btRowUp.TabIndex = 3;
            this.btRowUp.Text = "Row Up";
            this.btRowUp.UseVisualStyleBackColor = true;
            this.btRowUp.Click += new System.EventHandler(this.btRowUp_Click);
            // 
            // btRowDown
            // 
            this.btRowDown.Location = new System.Drawing.Point(317, 426);
            this.btRowDown.Name = "btRowDown";
            this.btRowDown.Size = new System.Drawing.Size(146, 27);
            this.btRowDown.TabIndex = 3;
            this.btRowDown.Text = "Row Down";
            this.btRowDown.UseVisualStyleBackColor = true;
            this.btRowDown.Click += new System.EventHandler(this.btRowDown_Click);
            // 
            // btPgDown
            // 
            this.btPgDown.Location = new System.Drawing.Point(463, 426);
            this.btPgDown.Name = "btPgDown";
            this.btPgDown.Size = new System.Drawing.Size(146, 27);
            this.btPgDown.TabIndex = 3;
            this.btPgDown.Text = "Page Down";
            this.btPgDown.UseVisualStyleBackColor = true;
            this.btPgDown.Click += new System.EventHandler(this.btPgDown_Click);
            // 
            // lstbDescription
            // 
            this.lstbDescription.FormattingEnabled = true;
            this.lstbDescription.Items.AddRange(new object[] {
            "This sample demonstrates using Streams to load thumbnails into the ThumbnailXpres" +
                "s control.",
            "The following are featured:",
            "",
            "1) Loading a single file by stream into ThumbnailXpress",
            "2) Loading a directory of files into ThumbnailXpress by streams.",
            "3) Programmatic scrolling.",
            "4) Passing thumbnail data back and forth to ImagXpress",
            "5) Utilizing the UserTag feature for thumbnail items"});
            this.lstbDescription.Location = new System.Drawing.Point(12, 32);
            this.lstbDescription.Name = "lstbDescription";
            this.lstbDescription.Size = new System.Drawing.Size(597, 108);
            this.lstbDescription.TabIndex = 5;
            // 
            // gbLastError
            // 
            this.gbLastError.Controls.Add(this.lblLastError);
            this.gbLastError.Location = new System.Drawing.Point(625, 33);
            this.gbLastError.Name = "gbLastError";
            this.gbLastError.Size = new System.Drawing.Size(279, 106);
            this.gbLastError.TabIndex = 6;
            this.gbLastError.TabStop = false;
            this.gbLastError.Text = "Last Error:";
            // 
            // lblLastError
            // 
            this.lblLastError.Location = new System.Drawing.Point(16, 25);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(243, 66);
            this.lblLastError.TabIndex = 0;
            this.lblLastError.Text = "No Error";
            // 
            // cbLoadSource
            // 
            this.cbLoadSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLoadSource.FormattingEnabled = true;
            this.cbLoadSource.Items.AddRange(new object[] {
            "Load Image from Thumbnail Item into ImagXpress",
            "Load Image from Source Image File into ImagXpress"});
            this.cbLoadSource.Location = new System.Drawing.Point(615, 463);
            this.cbLoadSource.Name = "cbLoadSource";
            this.cbLoadSource.Size = new System.Drawing.Size(290, 21);
            this.cbLoadSource.TabIndex = 7;
            // 
            // thumbnailXpress1
            // 
            this.thumbnailXpress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpress1.BottomMargin = 5;
            this.thumbnailXpress1.CameraRaw = false;
            this.thumbnailXpress1.CellBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.thumbnailXpress1.CellBorderWidth = 1;
            this.thumbnailXpress1.CellHeight = 80;
            this.thumbnailXpress1.CellHorizontalSpacing = 5;
            this.thumbnailXpress1.CellSpacingColor = System.Drawing.Color.White;
            this.thumbnailXpress1.CellVerticalSpacing = 5;
            this.thumbnailXpress1.CellWidth = 80;
            this.thumbnailXpress1.DblClickDirectoryDrillDown = true;
            this.thumbnailXpress1.DescriptorAlignment = ((Accusoft.ThumbnailXpressSdk.DescriptorAlignments)((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter | Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpress1.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.Default;
            this.thumbnailXpress1.EnableAsDragSourceForExternalDragDrop = true;
            this.thumbnailXpress1.EnableAsDropTargetForExternalDragDrop = true;
            this.thumbnailXpress1.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon;
            this.thumbnailXpress1.FtpPassword = "";
            this.thumbnailXpress1.FtpUserName = "";
            this.thumbnailXpress1.InterComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpress1.IntraComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpress1.LeftMargin = 5;
            this.thumbnailXpress1.Location = new System.Drawing.Point(14, 154);
            this.thumbnailXpress1.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpress1.Name = "thumbnailXpress1";
            this.thumbnailXpress1.PreserveBlack = false;
            this.thumbnailXpress1.ProxyServer = "";
            this.thumbnailXpress1.RightMargin = 5;
            this.thumbnailXpress1.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical;
            this.thumbnailXpress1.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.thumbnailXpress1.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
            this.thumbnailXpress1.ShowHourglass = true;
            this.thumbnailXpress1.ShowImagePlaceholders = false;
            this.thumbnailXpress1.Size = new System.Drawing.Size(594, 266);
            this.thumbnailXpress1.TabIndex = 8;
            this.thumbnailXpress1.TextBackColor = System.Drawing.Color.White;
            this.thumbnailXpress1.TopMargin = 5;
            this.thumbnailXpress1.SelectedIndexChanged += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.SelectedIndexChangedEventHandler(this.thumbnailXpress1_SelectedIndexChanged);
            // 
            // imageXView1
            // 
            this.imageXView1.Location = new System.Drawing.Point(625, 154);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(279, 266);
            this.imageXView1.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 496);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.thumbnailXpress1);
            this.Controls.Add(this.cbLoadSource);
            this.Controls.Add(this.gbLastError);
            this.Controls.Add(this.lstbDescription);
            this.Controls.Add(this.btRotate);
            this.Controls.Add(this.btPgDown);
            this.Controls.Add(this.btRowDown);
            this.Controls.Add(this.btRowUp);
            this.Controls.Add(this.btPgUp);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MemoryLoad Sample";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.gbLastError.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImageViaStreamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDirectoryViaStreamsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutThumbnailXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutImagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btPgUp;
        private System.Windows.Forms.Button btRowUp;
        private System.Windows.Forms.Button btRowDown;
        private System.Windows.Forms.Button btPgDown;
        private System.Windows.Forms.Button btRotate;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ListBox lstbDescription;
        private System.Windows.Forms.GroupBox gbLastError;
        private System.Windows.Forms.Label lblLastError;
        private System.Windows.Forms.ComboBox cbLoadSource;
        private Accusoft.ThumbnailXpressSdk.ThumbnailXpress thumbnailXpress1;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
    }
}


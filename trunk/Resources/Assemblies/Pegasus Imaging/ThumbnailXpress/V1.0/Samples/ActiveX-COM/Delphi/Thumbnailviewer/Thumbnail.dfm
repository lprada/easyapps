object Form1: TForm1
  Left = 154
  Top = 138
  Width = 1012
  Height = 766
  Caption = 'ThumbnailXpress Viewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = menu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 824
    Top = 24
    Width = 77
    Height = 13
    Caption = 'Selection Mode:'
  end
  object lbldesc: TLabel
    Left = 32
    Top = 8
    Width = 721
    Height = 48
    Caption = 
      'This sample demonstrates the use of the ThumbnailXpress control ' +
      'to open a directory of images or individual items. It also demon' +
      'strates selecting certain items, removing items and clearing the' +
      ' display of all items. The Double Click event can be used to dis' +
      'play the selected item in an ImagXpress control.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object lblError: TLabel
    Left = 696
    Top = 656
    Width = 3
    Height = 13
  end
  object thumb: TThumbnailXpress
    Left = 32
    Top = 128
    Width = 753
    Height = 513
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    OnDblClick = thumbDblClick
    ControlData = {
      0800420000003100410032003100310037004100370038003100350044003400
      3900430042003500340035003400340037003600360034004600320033003000
      4400420039000000080042000000410039004200380030003700380035003100
      4200330044003900350033003900420043003500380033003000310045003000
      460045003200310045004500320000009645B43673C758DC00070000D34D0000
      053500001300FFFFFF000300020000000B00010009000352E30B918FCE119DE3
      00AA004BB85101000000BC02444201000D4D532053616E732053657269661300
      0000000009000452E30B918FCE119DE300AA004BB8516C740000AC0200000100
      00006C0000000000000000000000FFFFFFFFFFFFFFFF0000000000000000D084
      00007869000020454D4600000100AC0200001000000004000000000000000000
      0000000000000004000000030000540100000E01000000000000000000000000
      000020300500B01E04001B000000100000000000000000000000520000007001
      000001000000F5FFFFFF00000000000000000000000090010000000000010000
      00004D0053002000530061006E00730020005300650072006900660000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000040FD13001395917C5195917C00000000D01918000020
      00007207000078056E014D0053002000530061006E0073002000530065007200
      690066000000E030887C000015004CFD1300AB37917CC737917C080615000800
      25016D05917C6D05917C8CFD1300C7FE807CCFFE807C00000000D01918000020
      00000000250178056E0100000000D019180058FD1300C066D200140000008321
      000083210000C8AD0240B6C70040000400000100000084376E010A0000001053
      84D400000000DCB496EA8C1611A5D8FD1300D823917C105384D4A20B00000EB9
      67FAEB50D7C6C066D200C066D2001C0000002C066E012843D20010FE13000D00
      00006476000800000000250000000C00000001000000180000000C0000000000
      0000260000001C00000002000000000000000100000000000000000000002500
      00000C00000002000000140000000C0000000D00000027000000180000000300
      000000000000FFFFFF0000000000250000000C00000003000000190000000C00
      0000FFFFFF00120000000C00000002000000250000000C000000070000802500
      00000C00000005000080250000000C0000000D0000800E000000140000000000
      00001000000014000000}
  end
  object cmbSelected: TComboBox
    Left = 816
    Top = 56
    Width = 137
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnChange = cmbSelectedChange
  end
  object Button1: TButton
    Left = 816
    Top = 88
    Width = 137
    Height = 33
    Caption = 'Retrieve Selected Items'
    TabOrder = 2
    OnClick = Button1Click
  end
  object lstSelect: TListBox
    Left = 808
    Top = 128
    Width = 137
    Height = 145
    ItemHeight = 13
    TabOrder = 3
  end
  object Button2: TButton
    Left = 816
    Top = 288
    Width = 137
    Height = 41
    Caption = 'Remove Selected Items'
    TabOrder = 4
    OnClick = Button2Click
  end
  object cmdclear: TButton
    Left = 816
    Top = 344
    Width = 137
    Height = 33
    Caption = 'Clear Items'
    TabOrder = 5
    OnClick = cmdclearClick
  end
  object menu: TMainMenu
    Left = 848
    Top = 432
    object File1: TMenuItem
      Caption = '&File'
      object OpenDirectory1: TMenuItem
        Caption = 'OpenDirectory'
        OnClick = OpenDirectory1Click
      end
      object OpenFile1: TMenuItem
        Caption = 'OpenFile'
        OnClick = OpenFile1Click
      end
      object Quit1: TMenuItem
        Caption = 'Quit'
        OnClick = Quit1Click
      end
    end
    object About1: TMenuItem
      Caption = '&About'
      OnClick = About1Click
    end
  end
  object od: TOpenDialog
    Left = 848
    Top = 488
  end
end

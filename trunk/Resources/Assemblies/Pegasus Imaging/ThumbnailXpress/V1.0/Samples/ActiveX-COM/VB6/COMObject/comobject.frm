VERSION 5.00
Begin VB.Form comobject 
   Caption         =   "ThumbnailXpress 1  COM Object Example"
   ClientHeight    =   9210
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   10740
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   9210
   ScaleWidth      =   10740
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete 2nd Control"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   8040
      TabIndex        =   2
      Top             =   1800
      Width           =   2055
   End
   Begin VB.CommandButton cmdCreate2 
      Caption         =   "Create 2nd Control"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   5640
      TabIndex        =   1
      Top             =   1800
      Width           =   2055
   End
   Begin VB.ListBox lstDesc 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "comobject.frx":0000
      Left            =   120
      List            =   "comobject.frx":0010
      TabIndex        =   0
      Top             =   120
      Width           =   10335
   End
   Begin VB.Label lblLasterror 
      Caption         =   "Last Error Reported:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4080
      TabIndex        =   4
      Top             =   1200
      Width           =   1695
   End
   Begin VB.Label lblerror 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6120
      TabIndex        =   3
      Top             =   1080
      Width           =   4335
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "comobject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

' SETUP:
' In VB 5 or VB 6, select Project/References
' Check  "Pegasus ThumbnailXpress v1.0 ActiveX Control
' This sets up the reference for PegasusImagingActiveXThumbnailXpress1Lib

' ThumbnailXpress1 is created once at startup.  This is the simpler case
' because the control is always available as long as the form is loaded,
' and the form takes care of removing the object when it unloades.
 Dim ThumbnailXpress1 As PegasusImagingActiveXThumbnailXpress1.ThumbnailXpress
' ThumbnailXpress2 gets allocated later, but we're responsible for destroying
' it prior to the form unloading.  If we don't destroy it, then we could leave
' a dangling reference and we'll get a nasty Assertion Error
 Dim ThumbnailXpress2 As PegasusImagingActiveXThumbnailXpress1.ThumbnailXpress
Attribute ThumbnailXpress2.VB_VarHelpID = -1
' Unlock function - required to remove registration dialog when control created dynamically
Private Declare Sub TN_Unlock Lib "PegasusImaging.ActiveX.ThumbnailXpress1.dll" Alias "UnlockControl" (ByVal pw1 As Long, ByVal pw2 As Long, ByVal pw3 As Long, ByVal pw4 As Long)

Dim imgFileName As String
Dim imgParentDir As String

Private Sub Command1_Click()

End Sub

Private Sub cmdCreate2_Click()
  
On Error GoTo errorhandler
                     
  CreateThumbnailXpress ThumbnailXpress2
    
  '*****for distribution, place your Tnail unlock codes here******
  ThumbnailXpress2.UnlockRuntime 1234, 1234, 1234, 1234
  
  '*****for distribution, place your IX unlock codes here******
  ThumbnailXpress2.SetIXLicenseCodes 1234, 1234, 1234, 1234
    
  cmdDelete.Enabled = True
  ThumbnailXpress2.ScrollDirection = TN_Horizontal
  ThumbnailXpress2.Debug = True
  ThumbnailXpress2.DebugErrorLevel = TN_Detailed
 ThumbnailXpress2.DebugLogFile = "C:\test.txt"
    
   
  imgFileName = App.Path & "\..\..\..\..\..\..\Common\Images\"
  ThumbnailXpress2.AddDirectoryItems imgFileName, -1
  
errorhandler:
  
    lblerror.Caption = Err.Description
    
End Sub

Private Sub mnuAbout_Click()
    ThumbnailXpress1.AboutBox
End Sub
    
Private Sub mnuFileQuit_Click()
    End
End Sub

' Initialize the ThumbnailXpress1 Control
' Notice that after creating the Object, we must call
' CreateCtlWindow to manually create the control window.
' This is because we've bypassed ActiveX which normally does this for you.
Private Sub Form_Load()

On Error GoTo errorhandler
  ' Must unlock the object using YOUR unlock codes!
      
  cmdDelete.Enabled = False
  
  imgParentDir = App.Path
  
  'cmdDelete.Enabled = False
  
  '*****place your unlock codes here******
  TN_Unlock 1234, 1234, 1234, 1234
    
  Set ThumbnailXpress1 = New PegasusImagingActiveXThumbnailXpress1.ThumbnailXpress
  
  '*****for distribution, place your Tnail unlock codes here******
  ThumbnailXpress1.UnlockRuntime 1234, 1234, 1234, 1234
  
  '*****for distribution, place your IX unlock codes here******
  ThumbnailXpress1.SetIXLicenseCodes 1234, 1234, 1234, 1234
  
  ThumbnailXpress1.CreateCtlWindow comobject.hWnd, 10, 170, 300, 400
  ThumbnailXpress1.ScrollDirection = TN_Vertical
  
  imgFileName = App.Path & "\..\..\..\..\..\..\Common\Images\"
  
  ThumbnailXpress1.AddDirectoryItems imgFileName, -1
  
     
errorhandler:
  
    lblerror.Caption = Err.Description
 
End Sub


' Delete the ThumbnailXpress2 Object
Private Sub cmdDelete_Click()
  DestroyThumbnailXpress ThumbnailXpress2
   Set ThumbnailXpress2 = Nothing
End Sub


Private Sub Form_Unload(Cancel As Integer)
  DestroyThumbnailXpress ThumbnailXpress2
   DestroyThumbnailXpress ThumbnailXpress1
End Sub

Private Sub mnuQuit_Click()
    End
End Sub

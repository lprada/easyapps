// EventDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Event.h"
#include "EventDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
	
	// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
//{{AFX_MSG_MAP(CAboutDlg)
ON_WM_DESTROY()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventDlg dialog

CEventDlg::CEventDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEventDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEventDlg)
	m_ShowMouseMove = FALSE;
	m_ShowPaintEvents = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEventDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEventDlg)
	DDX_Check(pDX, IDC_CHECK1, m_ShowMouseMove);
	DDX_Check(pDX, IDC_CHECKPAINT, m_ShowPaintEvents);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CEventDlg, CDialog)
//{{AFX_MSG_MAP(CEventDlg)
ON_WM_SYSCOMMAND()
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(IDButton1, OnButton1)
ON_BN_CLICKED(IDButton2, OnButton2)
ON_BN_CLICKED(IDQuit, OnQuit)
ON_WM_DESTROY()
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_BN_CLICKED(IDC_CHECK1, OnClickedCheck1)
ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_CHECKPAINT, OnClickedCheckPaint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventDlg message handlers

// Include the ThumbnailXpress COM initialization routines
#include "..\Include\TN1_open.cpp"


BOOL CEventDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Add "About..." menu item to system menu.
	
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	

	ppCThumbnailXpress1 = NULL;
	ppCThumbnailXpress2 = NULL;

	// Initialize the ThumbnailXpress COM interface.  MUST be done before creating the first object.
	HINSTANCE hDLL = TN_Open();
	
	// Create ThumbnailXpress Object 1
	ppCThumbnailXpress1 = new CThumbnailXpress((DWORD)this, 1, (long)m_hWnd, 10, 120, 530, 200);

    // NOTE: The Unlock codes shown below are for illustration purposes only.
    // For distribution, uncomment and call the UnlockRuntime function passing the ThumbnailXpress license unlock codes	
	//ppCThumbnailXpress1->pThumbnailXpress->UnlockRuntime(123456789, 123456789, 123456789, 123456789);

    // For distribution, uncomment and call call the UnlockRuntime function passing the ImagXpress license unlock codes	
	//ppCThumbnailXpress1->pThumbnailXpress->SetIXLicenseCodes(123456789, 123456789, 123456789, 123456789);
	
	// initialize control related parameters
	ppCThumbnailXpress1->pThumbnailXpress->ScrollDirection = TN_Horizontal;
	
	// initialize event related parameters
	ppCThumbnailXpress1->SetPaintEvent(Paint);
	ppCThumbnailXpress1->SetSelectionIndexChangedEvent(SelectionIndexChanged);
	ppCThumbnailXpress1->pThumbnailXpress->EnableEvent(TN_SelectedIndexChange, TRUE);
	ppCThumbnailXpress1->SetErrorEvent(Error);
	ppCThumbnailXpress1->pThumbnailXpress->EnableEvent(TN_Error , TRUE);
	ppCThumbnailXpress1->SetItemCompleteEvent(ItemComplete);
	ppCThumbnailXpress1->pThumbnailXpress->EnableEvent(TN_ItemComplete , TRUE);
	ppCThumbnailXpress1->SetClickEvent(Click);
	ppCThumbnailXpress1->SetDblClickEvent(DblClick);

	
	// Create ThumbnailXpress Object 2
	ppCThumbnailXpress2 = new CThumbnailXpress((DWORD)this, 2, (long)m_hWnd, 10, 330, 530, 200);

    // NOTE: The Unlock codes shown below are for illustration purposes only.
    // For distribution, uncomment and call the UnlockRuntime function passing the ThumbnailXpress license unlock codes	
	//ppCThumbnailXpress2->pThumbnailXpress->UnlockRuntime(123456789, 123456789, 123456789, 123456789);

    // For distribution, uncomment and call call the UnlockRuntime function passing the ImagXpress license unlock codes	
	//ppCThumbnailXpress2->pThumbnailXpress->SetIXLicenseCodes(123456789, 123456789, 123456789, 123456789);

	// initialize control related parameters
	ppCThumbnailXpress2->pThumbnailXpress->ScrollDirection = TN_Vertical;

	// initialize event related parameters
	ppCThumbnailXpress2->SetClickEvent(Click);
	ppCThumbnailXpress2->SetDblClickEvent(DblClick);
	ppCThumbnailXpress2->SetPaintEvent(Paint);
	ppCThumbnailXpress2->SetSelectionIndexChangedEvent(SelectionIndexChanged);
	ppCThumbnailXpress2->pThumbnailXpress->EnableEvent(TN_SelectedIndexChange, TRUE);
	ppCThumbnailXpress2->SetErrorEvent(Error);
	ppCThumbnailXpress2->pThumbnailXpress->EnableEvent(TN_Error , TRUE);
	ppCThumbnailXpress2->SetItemCompleteEvent(ItemComplete);
	ppCThumbnailXpress2->pThumbnailXpress->EnableEvent(TN_ItemComplete , TRUE);

	// Close the ThumbnailXpress COM initialization.  MUST be done anytime
	// after the first ThumbnailXpress object is created.
	TN_Close(hDLL);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEventDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CEventDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//Paint Event Handler
HRESULT CEventDlg::Paint(DWORD instancePtr, DWORD objID, LONG hDC, LONG left, LONG top, LONG width, LONG height) 
{
	char buffer[100];
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		sprintf(buffer, "ThumbnailXpress #1, Paint rect left=%d, top=%d, width=%d, height=%d", left, top, width, height);
	if (objID == p->ppCThumbnailXpress2->m_objID)
		sprintf(buffer, "ThumbnailXpress #2, Paint rect left=%d, top=%d, width=%d, height=%d", left, top, width, height);
	pL->SetCurSel(pL->AddString( buffer));
	return S_OK;
}

// IndexChanged Event Handler
HRESULT CEventDlg::SelectionIndexChanged(DWORD instancePtr, DWORD objID) 
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		pL->SetCurSel(pL->AddString( "Selection Index changed - ThumbnailXpress #1"));
	if (objID == p->ppCThumbnailXpress2->m_objID)
		pL->SetCurSel(pL->AddString( "Selection Index changed - ThumbnailXpress #2"));
	return S_OK;
}

// Error Event Handler
HRESULT CEventDlg::Error(DWORD instancePtr, DWORD objID, LONG ItemIndex, LONG ErrorNum) 
{
	char buffer[100];
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		sprintf(buffer, "ThumbnailXpress #1, *** ERROR ***, item #%d, errornum=%d", ItemIndex, ErrorNum);
	if (objID == p->ppCThumbnailXpress2->m_objID)
		sprintf(buffer, "ThumbnailXpress #2, *** ERROR ***, item #%d, errornum=%d", ItemIndex, ErrorNum);
	pL->SetCurSel(pL->AddString( buffer));
	return S_OK;
}

// ItemComplete Event Handler
HRESULT CEventDlg::ItemComplete(DWORD instancePtr, DWORD objID, LONG ItemIndex) 
{
	char buffer[100];
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		sprintf(buffer, "ThumbnailXpress #1, Item #%d complete", ItemIndex);
	if (objID == p->ppCThumbnailXpress2->m_objID)
		sprintf(buffer, "ThumbnailXpress #2, Item #%d complete", ItemIndex);
	pL->SetCurSel(pL->AddString( buffer));
	return S_OK;
}

// Mouse Move Event Handler
HRESULT CEventDlg::MouseMove(DWORD instancePtr, DWORD objID, short Button, short Shift, OLE_XPOS_PIXELS x, OLE_YPOS_PIXELS y)
{
	char buffer[40];
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		sprintf(buffer, "(%d,%d) Mouse Move ThumbnailXpress #1", x, y);
	if (objID == p->ppCThumbnailXpress2->m_objID)
		sprintf(buffer, "(%d,%d) Mouse Move ThumbnailXpress #2", x, y);
	pL->SetCurSel(pL->AddString( buffer));
	return S_OK;
}

// Click Event Handler
HRESULT CEventDlg::Click(DWORD instancePtr, DWORD objID)
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		pL->SetCurSel(pL->AddString( "Click - ThumbnailXpress #1"));
	if (objID == p->ppCThumbnailXpress2->m_objID)
		pL->SetCurSel(pL->AddString( "Click - ThumbnailXpress #2"));
	return S_OK;
}

// DblClick Event Handler
HRESULT CEventDlg::DblClick(DWORD instancePtr, DWORD objID)
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if (objID == p->ppCThumbnailXpress1->m_objID)
		pL->SetCurSel(pL->AddString( "Double Click - ThumbnailXpress #1"));
	if (objID == p->ppCThumbnailXpress2->m_objID)
		pL->SetCurSel(pL->AddString( "Double Click - ThumbnailXpress #2"));
	return S_OK;
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEventDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CEventDlg::OnButton1() 
{
	CEventDlg *p = (CEventDlg *) this;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if(ppCThumbnailXpress1) 
	{
		ppCThumbnailXpress1->pThumbnailXpress->AddDirectoryItems("..\\..\\..\\..\\..\\..\\Common\\Images\\*.tif", -1);
		if(ppCThumbnailXpress1->pThumbnailXpress->ErrorCode)
			pL->SetCurSel(pL->AddString( "***ERROR*** unable to load images from ..\\..\\..\\..\\..\\..\\Common\\Images - ThumbnailXpress #1"));
	}
}

void CEventDlg::OnButton2() 
{
	CEventDlg *p = (CEventDlg *) this;
	CListBox *pL = (CListBox *)p->GetDlgItem(IDC_LIST1);
	if(ppCThumbnailXpress2) 
	{
		ppCThumbnailXpress2->pThumbnailXpress->AddDirectoryItems("..\\..\\..\\..\\..\\..\\Common\\Images\\*.jpg", -1);
		if(ppCThumbnailXpress2->pThumbnailXpress->ErrorCode)
			pL->SetCurSel(pL->AddString( "***ERROR*** unable to load images from ..\\..\\..\\..\\..\\..\\Common\\Images - ThumbnailXpress #2"));
	}
}

void CEventDlg::OnQuit() 
{
	EndDialog(IDOK);
}

void CEventDlg::OnDestroy() 
{

	CDialog::OnDestroy();	

	// Delete our ThumbnailXpress Objects
	if(ppCThumbnailXpress1) 
	{
		delete ppCThumbnailXpress1;
	}

	if(ppCThumbnailXpress2) 
	{
		delete ppCThumbnailXpress2;
	}

}

void CEventDlg::OnFileQuit() 
{
	EndDialog(IDOK);
	
}

void CEventDlg::OnAppAbout() 
{
	if(ppCThumbnailXpress1) 
		if(ppCThumbnailXpress1->pThumbnailXpress)
			ppCThumbnailXpress1->pThumbnailXpress->AboutBox();
	
}

void CEventDlg::OnClickedCheck1() 
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECK1);
	if(pCheck->GetCheck())

	{
		if(ppCThumbnailXpress1) 
			ppCThumbnailXpress1->SetMouseMoveEvent(MouseMove);

		if(ppCThumbnailXpress2) 
			ppCThumbnailXpress2->SetMouseMoveEvent(MouseMove);
	}
	else
	{
		if(ppCThumbnailXpress1) 
			ppCThumbnailXpress1->SetMouseMoveEvent(NULL);

		if(ppCThumbnailXpress2) 
			ppCThumbnailXpress2->SetMouseMoveEvent(NULL);

	}	
}

void CEventDlg::OnClickedCheckPaint() 
{
	CButton* pCheck = (CButton*)GetDlgItem(IDC_CHECKPAINT);
	if(pCheck->GetCheck())

	{
		if(ppCThumbnailXpress1) 
			ppCThumbnailXpress1->pThumbnailXpress->EnableEvent(TN_Paint, TRUE);


		if(ppCThumbnailXpress2) 
			ppCThumbnailXpress2->pThumbnailXpress->EnableEvent(TN_Paint, TRUE);
	}
	else
	{
		if(ppCThumbnailXpress1) 
			ppCThumbnailXpress1->pThumbnailXpress->EnableEvent(TN_Paint, FALSE);


		if(ppCThumbnailXpress2) 
			ppCThumbnailXpress2->pThumbnailXpress->EnableEvent(TN_Paint, FALSE);

	}	
}

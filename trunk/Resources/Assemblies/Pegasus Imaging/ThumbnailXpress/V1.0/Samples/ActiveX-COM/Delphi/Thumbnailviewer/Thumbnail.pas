unit Thumbnail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AxCtrls, OleCtrls,PegasusImagingActiveXThumbnailXpress1_TLB, Menus,
  PegasusImagingActiveXImagXpress8_TLB,Unit2, ComCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    thumb: TThumbnailXpress;
    menu: TMainMenu;
    File1: TMenuItem;
    OpenDirectory1: TMenuItem;
    OpenFile1: TMenuItem;
    Quit1: TMenuItem;
    od: TOpenDialog;
    cmbSelected: TComboBox;
    Button1: TButton;
    lstSelect: TListBox;
    Button2: TButton;
    cmdclear: TButton;
    Label1: TLabel;
    About1: TMenuItem;
    lbldesc: TLabel;
    lblError: TLabel;
    procedure OpenDirectory1Click(Sender: TObject);
    procedure Quit1Click(Sender: TObject);
    procedure OpenFile1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbSelectedChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure thumbDblClick(Sender: TObject);
    procedure cmdclearClick(Sender: TObject);
    procedure lstFilesDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TabControl1Change(Sender: TObject);
    procedure humbnailXpress1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  Form1: TForm1;
  imgParentDir : string;
  filtertest : string;
  selcount : integer;
  indexselectedret : integer;
  indexselected : integer;

implementation


{$R *.dfm}
procedure TForm1.OpenDirectory1Click(Sender: TObject);
begin

try

    //Open A Directory of Images
  od.Filter := 'Image Files|*.BMP;*TIF;*.TIFF;*.CAL;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.JPG;*.JIF;*.LJP;PCX;*.PIC;*.PNG;*.JB2*.WSQ|All File (*.*)|*.*';
  od.InitialDir := '..\..\..\..\..\..\Common\Images\';
  od.Execute;
  imgparentdir := ExtractFileDir(od.FileName);

  //create a temporary filter
  filtertest := 'Peg1';
  thumb.ClearFilters;
  thumb.AddFilterItem(filtertest);
  thumb.AddDirectoryItems(imgparentDir + '\' ,0);


except
      on E: Exception do showmessage(e.Message);
  end;
end;

procedure TForm1.Quit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.OpenFile1Click(Sender: TObject);
begin
try
      //Open a File
  od.filter := 'Image Files|*.BMP;*TIF;*.TIFF;*.CAL;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.JPG;*.JIF;*.LJP;PCX;*.PIC;*.PNG;*.JB2*.WSQ|All File (*.*)|*.*';
  od.InitialDir := '..\..\..\..\..\..\Common\Images\';
  od.Execute;
  imgparentdir := ExtractFileDir(od.FileName);
  //add the Directory Items
  filtertest := 'Peg1';

  //open the individual TN item
  thumb.AddFileItems(od.FileName,0,true);

  except
      on E: Exception do showmessage(e.Message);
  end;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
try
     //selection mode
        cmbSelected.Items.add('MultiExtended');
        cmbSelected.Items.add('MultiSimple');
        cmbSelected.Items.add('None');
        cmbSelected.Items.add('Single');
        cmbselected.ItemIndex := 3;

        //default settings for TNXpress
        thumb.CameraRawEnabled := true;
        thumb.PreserveBlackEnabled := true;
        thumb.UsePDFXpress := true;

except
      on E: Exception do  lblerror.Caption := e.Message;
end;

end;

procedure TForm1.cmbSelectedChange(Sender: TObject);
begin
      Case cmbSelected.ItemIndex of
      0: thumb.SelectionMode := TN_MultiExtended;
      1: thumb.SelectionMode := TN_MultiSimple ;

      2: thumb.SelectionMode := TN_None;

      3: thumb.SelectionMode := TN_Single ;
        End;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
   counter : integer;
   index : integer;
begin
try
      lstselect.Clear;
      selcount := thumb.SelectedItemCount;
      for counter := 0 to selcount - 1 do
      begin
           index := thumb.GetIndexOfSelectedItem(counter);
           lstselect.Items.Add(IntToStr(index));
      end;

 except
      on E: Exception do showmessage(e.Message);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
 deletecounter : integer;
 testcounter1 : integer;
 index : integer;
 counter : integer;
begin
try
  deletecounter := thumb.SelectedItemCount - 1;
  for testcounter1 := deletecounter downto 0 do
   begin
          index := thumb.GetIndexOfSelectedItem(testcounter1);
          thumb.DeleteItem(index);
  end;
     lstselect.Clear;


except
      on E: Exception do showmessage(e.Message);
  end;
end;

procedure TForm1.thumbDblClick(Sender: TObject);
begin
      //pass the selected DIB to ImagXpress view
      indexselectedret := thumb.GetIndexOfSelectedItem(indexselected);
      frmview.AddImage(thumb.GetItemFilename(indexselectedret));
      frmview.ShowModal();
end;

procedure TForm1.cmdclearClick(Sender: TObject);
begin
try
       thumb.DeleteItems;


except
      on E: Exception do showmessage(e.Message);
  end;
end;

procedure TForm1.lstFilesDragDrop(Sender, Source: TObject; X, Y: Integer);
const
cnMaxFileNameLen = 255;
var
   i,
  nCount     : integer;
  acFileName : array [0..cnMaxFileNameLen] of char;
begin
    //Get the Files here
   // nCount := DragQueryFile(msg.WParam,$FFFFFFFF,acFileName,cnMaxFileNameLen );

end;

procedure TForm1.TabControl1Change(Sender: TObject);
var
index : integer;
begin


end;

procedure TForm1.humbnailXpress1Click(Sender: TObject);
begin
  thumb.AboutBox();
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  thumb.AboutBox();
end;

end.


procedure TForm1.Quit1Click(Sender: TObject);
begin
  Close;
end;

end.

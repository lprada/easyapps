unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AxCtrls, OleCtrls, PegasusImagingActiveXImagXpress8_TLB,
  StdCtrls;

type
  TfrmView = class(TForm)
    xpress: TImagXpress;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblfname: TLabel;
    lbldimen: TLabel;
    lblbitdepth: TLabel;
    procedure AddImage(fname : string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmView: TfrmView;

implementation

procedure TfrmView.AddImage(fname : string);
begin
    Xpress.FileName := fname;
     lblFname.Caption := ExtractFileName(fname);
     lbldimen.Caption := IntToStr(Xpress.IWidth) + 'x' + IntToStr(Xpress.IHeight);
     lblbitdepth.Caption :=   IntToStr(Xpress.ibpp);



end;

{$R *.dfm}

end.

Attribute VB_Name = "ComObjectMod"
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

' Creates an ThumbnailXpress Object
Public Sub CreateThumbnailXpress(ByRef ThumbnailXpressObj As PegasusImagingActiveXThumbnailXpress1.ThumbnailXpress)

Dim x As Long

On Error GoTo Done
  x = ThumbnailXpressObj.hWnd
  ' If there was no error (ie: ThumbnailXpress Object exists), tell us
  ' that it's already been created!
  MsgBox "Can't create ThumbnailXpress Object because it already exists!"
  Exit Sub
  ' There's no ThumbnailXpress so we can go ahead and create it
Done:
  Set ThumbnailXpressObj = New PegasusImagingActiveXThumbnailXpress1.ThumbnailXpress
  
    
  ThumbnailXpressObj.CreateCtlWindow comobject.hWnd, 325, 170, 300, 400
  
End Sub


' Destroys the ThumbnailXpress Object
Public Sub DestroyThumbnailXpress(ThumbnailXpressObj As PegasusImagingActiveXThumbnailXpress1.ThumbnailXpress)

Dim x As Long

On Error GoTo Done
  x = ThumbnailXpressObj.hWnd
  ' If there was an error (ie: No ThumbnailXpress Object), then jump over this
  ThumbnailXpressObj.DestroyCtlWindow
  Set ThumbnailXpressObj = Nothing
Done:
End Sub


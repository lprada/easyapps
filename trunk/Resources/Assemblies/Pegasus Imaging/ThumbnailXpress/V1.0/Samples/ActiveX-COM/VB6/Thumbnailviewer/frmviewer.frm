VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmviewer 
   Caption         =   "ImageViewer"
   ClientHeight    =   5430
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9465
   LinkTopic       =   "Form1"
   ScaleHeight     =   5430
   ScaleWidth      =   9465
   StartUpPosition =   3  'Windows Default
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4935
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   8705
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   1067985288
      ErrInfo         =   -2107356876
      Persistence     =   -1  'True
      _cx             =   6800
      _cy             =   8705
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblerror 
      Caption         =   "Label4"
      Height          =   495
      Left            =   5040
      TabIndex        =   7
      Top             =   4680
      Width           =   3375
   End
   Begin VB.Label Label3 
      Caption         =   "Image Bit Depth:"
      Height          =   495
      Left            =   4440
      TabIndex        =   6
      Top             =   2160
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Image Dimensions:"
      Height          =   615
      Left            =   4440
      TabIndex        =   5
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "FileName:"
      Height          =   495
      Left            =   4440
      TabIndex        =   4
      Top             =   600
      Width           =   1815
   End
   Begin VB.Label lbldimen 
      Height          =   615
      Left            =   6720
      TabIndex        =   3
      Top             =   1320
      Width           =   2535
   End
   Begin VB.Label lblbpp 
      Height          =   495
      Left            =   6720
      TabIndex        =   2
      Top             =   2160
      Width           =   2535
   End
   Begin VB.Label lblFname 
      Height          =   495
      Left            =   6720
      TabIndex        =   1
      Top             =   600
      Width           =   2535
   End
End
Attribute VB_Name = "frmviewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public Function AddImage(filename As String)
On Error GoTo errorhandler
            ImagXpress1.filename = filename
            lblFname.Caption = fs.GetFileName(filename)
            lblbpp.Caption = ImagXpress1.IBPP
            lbldimen.Caption = ImagXpress1.IWidth & "x" & ImagXpress1.IHeight

 
errorhandler:

    lblerror.Caption = Err.Description
End Function




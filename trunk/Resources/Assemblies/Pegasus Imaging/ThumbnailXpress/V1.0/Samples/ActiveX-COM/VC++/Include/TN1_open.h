
// ThumbnailXpress COM VC++ Initialization routines.
// Copyright (c) 1999-2006 Pegasus Imaging Corp.


typedef HRESULT (WINAPI* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE TN_Open();
void TN_Close(HINSTANCE hDLL);

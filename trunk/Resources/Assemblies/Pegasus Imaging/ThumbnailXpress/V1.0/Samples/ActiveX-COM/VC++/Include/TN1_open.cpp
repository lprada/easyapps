
// ThumbnailXpress COM VC++ Initialization routines.
// Copyright (c) 1999-2006 Pegasus Imaging Corp.

// ----------------------------------------------------------------------------------
// Queries the system registry to get the path for registered version of
// PegasusImaging.ActiveX.ThumbnailXpress1.dll. Performs LoadLibrary on that DLL.
// ----------------------------------------------------------------------------------
HINSTANCE LoadLibraryThumbnailXpress1DLL()
{
  HANDLE  hKey;
  long    len=255;
  long    result;
  TCHAR    buf[256];
  memset(&buf, 0x00, sizeof(buf));

  // Query the system registry for the PegasusImaging.ActiveX.ThumbnailXpress1.dll path
  #ifdef __BORLANDC__
    result = RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("Software\\Classes\\CLSID\\{F3379712-5D3C-4F57-92D9-09AC0657719A}"), (Windows::PHKEY)&hKey);
  #else
    result = RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("Software\\Classes\\CLSID\\{F3379712-5D3C-4F57-92D9-09AC0657719A}"), (PHKEY)&hKey);
  #endif

  if (result==ERROR_SUCCESS)
  {
    result = RegQueryValue((HKEY)hKey, TEXT("InprocServer32\0"), buf, &len);
    RegCloseKey((HKEY)hKey);
  }

  // If we found the path from the registry; LoadLibrary from that path
  // Otherwise attempt to load it from the system path.
  if (result==ERROR_SUCCESS)
    return LoadLibrary(buf);  
  else
    return LoadLibrary(TEXT("PegasusImaging.ActiveX.ThumbnailXpress1.dll"));
}

typedef HRESULT (WINAPI* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE TN_Open()
{
  HINSTANCE    hDLL;            // Handle to ThumbnailXpress ActiveX DLL
  LPFNDLL_PSU  lpfnDllFunc1;    // Function pointer to the UnlockControl function
  HRESULT      hr;
  
  hDLL = LoadLibraryThumbnailXpress1DLL();
  if (hDLL)
  {
    // Get the address of the UnlockControl function.  You need to give the COM object
    // the unlock codes that you received when
    // you purchased ThumbnailXpress
	lpfnDllFunc1 = (LPFNDLL_PSU)::GetProcAddress(hDLL, "UnlockControl");

    if (lpfnDllFunc1)
	  // Call the UnlockControl function passing the license unlock codes	
      // NOTE: The Unlock codes shown below are for trial illustration purposes only.
      hr = lpfnDllFunc1(123456789, 123456789, 123456789, 123456789);
    else
    {
	  ::FreeLibrary(hDLL);
      hDLL = NULL;
    }
  }
  return hDLL;
}

void TN_Close(HINSTANCE hDLL)
{
  if (hDLL)
	::FreeLibrary(hDLL);
}

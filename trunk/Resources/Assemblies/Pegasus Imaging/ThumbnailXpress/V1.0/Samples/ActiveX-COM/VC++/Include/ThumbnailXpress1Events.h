//-------------------------------------
// Copyright Pegasus Imaging Corp., 1997 - 2006
//   Header Created on 03/21/2006
//     For ThumbnailXpress Version 1
//         
//         
// Modification History
// ---------------------
//                      
//-------------------------------------

#ifndef THUMBNAILXPRESS1EVENTS_H
#define THUMBNAILXPRESS1EVENTS_H

#ifndef __BORLANDC__
using namespace PegasusImagingActiveXThumbnailXpress1;
#endif


class CThumbnailXpressEventHandler : public IDispatch
{
public:
  long m_dwRef;

  CThumbnailXpressEventHandler()
  {
     m_dwRef = 0;
  }

  ~CThumbnailXpressEventHandler()
  {
  }

   long __stdcall GetTypeInfoCount(unsigned int *)
   {
     return 0;
   }

   long __stdcall GetTypeInfo(unsigned int,unsigned long,struct ITypeInfo ** )
   {
     return 0;
   }

   long __stdcall GetIDsOfNames(const struct _GUID &, BSTR * ,unsigned int,unsigned long,long *)
   {
     return 0;
   }

   long __stdcall GetIID()
   {
     return 0;
   }

   HRESULT __stdcall Invoke (DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult, EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr)
   {
   // this is where all the events come -- just call directly into your methods exactly
   //   as the import wrapper will show you
   switch (dispidMember)
     {
     case -600:
        Click(   );
     break;

     case -601:
        DblClick(   );
     break;

     case -605:
        MouseDown(pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal);
     break;

     case -606:
        MouseMove(pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal);
     break;

     case -607:
        MouseUp(pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal);
     break;

     case -604:
        KeyUp(pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal);
     break;

     case -602:
        KeyDown(pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal);
     break;

     case -603:
        KeyPress(pdispparams->rgvarg[0].iVal);
     break;

     case 201:
        Paint(pdispparams->rgvarg[4].iVal, pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal);
     break;

     case 202:
		SelectionIndexChanged( );
     break;

     case 203:
		Error(pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal);
     break;

     case 204:
		ItemComplete(pdispparams->rgvarg[0].iVal);
     break;
  }
   return S_OK;
   }

   HRESULT __stdcall QueryInterface(REFIID a_riid, void** a_ppv)
   {
      if (a_ppv == NULL)
         return E_POINTER;
      *a_ppv = NULL;
#ifdef __BORLANDC__
      if (a_riid == DIID__IThumbnailXpressEvents)
#else
      if (a_riid == __uuidof(_IThumbnailXpressEvents))
#endif
         *a_ppv = static_cast<CThumbnailXpressEventHandler*>(this);
      else if (a_riid == IID_IUnknown)
         *a_ppv = static_cast<IUnknown *>(this);
      else
         return E_NOINTERFACE;
      AddRef();
      return S_OK;
   }

   ULONG __stdcall AddRef()
   {
	  return ::InterlockedIncrement(&m_dwRef);
   }

   ULONG __stdcall Release()
   {
	  if (::InterlockedDecrement(&m_dwRef) == 0)
         return 0;
      return m_dwRef;
   }

   virtual HRESULT Click(   )
   {
     return S_OK;
   }

   virtual HRESULT DblClick(   )
   {
     return S_OK;
   }

   virtual HRESULT MouseDown(short Button, short Shift, long x, long y)
   {
     return S_OK;
   }

   virtual HRESULT MouseMove(short Button, short Shift, long x, long y)
   {
     return S_OK;
   }

   virtual HRESULT MouseUp(short Button, short Shift, long x, long y)
   {
     return S_OK;
   }

   virtual HRESULT KeyUp(short KeyCode, short Shift)
   {
     return S_OK;
   }

   virtual HRESULT KeyDown(short KeyCode, short Shift)
   {
     return S_OK;
   }

   virtual HRESULT KeyPress(short KeyASCII)
   {
     return S_OK;
   }

   virtual HRESULT Paint(LONG hDC, LONG left, LONG top, LONG width, LONG height )
   {
     return S_OK;
   }

   virtual HRESULT SelectionIndexChanged( )
   {
     return S_OK;
   }

   virtual HRESULT Error( LONG ItemIndex, LONG ErrorNum)
   {
     return S_OK;
   }

   virtual HRESULT ItemComplete( LONG ItemIndex)
   {
     return S_OK;
   }

};

class CThumbnailXpress : CThumbnailXpressEventHandler
{
  private:
    DWORD m_dwCookie;

    // Event Handlers
   HRESULT (*pClick) (DWORD classPtr, DWORD objID);
   HRESULT (*pDblClick) (DWORD classPtr, DWORD objID);
   HRESULT (*pMouseDown) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y);
   HRESULT (*pMouseMove) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y);
   HRESULT (*pMouseUp) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y);
   HRESULT (*pKeyUp) (DWORD classPtr, DWORD objID, short KeyCode, short Shift);
   HRESULT (*pKeyDown) (DWORD classPtr, DWORD objID, short KeyCode, short Shift);
   HRESULT (*pKeyPress) (DWORD classPtr, DWORD objID, short KeyASCII);
   HRESULT (*pPaint) (DWORD classPtr, DWORD objID, LONG hDC, LONG left, LONG top, LONG width, LONG height);
   HRESULT (*pIndexChanged) (DWORD classPtr, DWORD objID);
   HRESULT (*pError) (DWORD classPtr, DWORD objID, LONG ItemIndex, LONG ErrorNum);
   HRESULT (*pItemComplete) (DWORD classPtr, DWORD objID, LONG ItemIndex);

public:
CThumbnailXpress()
{
  CThumbnailXpress(0,0,0,0,0,0,0);
}

CThumbnailXpress(DWORD classPtr, DWORD objID, long hWndParent, long left, long top, long width, long height)
{
   // Call the event connection function in the ThumbnailXpress ATL COM Control
   typedef bool (__cdecl* LPFN_TN_CONNECTEVENTS)(long, DWORD *, IUnknown *, long *);
   LPFN_TN_CONNECTEVENTS  lpfn_TN_ConnectEvents;

   HINSTANCE hDLL = ::LoadLibrary("PegasusImaging.ActiveX.ThumbnailXpress1.dll");   // ThumbnailXpress ATL COM control
   if (hDLL != NULL)
   {
	  lpfn_TN_ConnectEvents = (LPFN_TN_CONNECTEVENTS)::GetProcAddress(hDLL, "TN_ConnectEvents");
      if (lpfn_TN_ConnectEvents)
      {
         lpfn_TN_ConnectEvents((long)(&pThumbnailXpress), &m_dwCookie, this, &m_dwRef);
      }
	  ::FreeLibrary(hDLL);
   }

   // Initialize our instance pointer and object id.
   m_classPtr = classPtr;
   m_objID    = objID;

   // NULL out the Event procedures

   pClick = NULL;
   pDblClick = NULL;
   pMouseDown = NULL;
   pMouseMove = NULL;
   pMouseUp = NULL;
   pKeyUp = NULL;
   pKeyDown = NULL;
   pKeyPress = NULL;
   pPaint = NULL;
   pIndexChanged = NULL;
   pError = NULL;

   // Create the control's Window
   if (hWndParent)
      pThumbnailXpress->CreateCtlWindow(hWndParent, left, top, width, height);   
}

~CThumbnailXpress()
{
	// Call the event disconnection function in the ThumbnailXpress ATL COM Control
	typedef bool (__cdecl* LPFN_TN_DISCONNECTEVENTS)(long, DWORD);
	LPFN_TN_DISCONNECTEVENTS  lpfn_TN_DisConnectEvents;

   HINSTANCE hDLL = ::LoadLibrary("PegasusImaging.ActiveX.ThumbnailXpress1.dll");   // ThumbnailXpress ATL COM control
	if (hDLL != NULL)
	{
		lpfn_TN_DisConnectEvents = (LPFN_TN_DISCONNECTEVENTS)::GetProcAddress(hDLL, "TN_DisconnectEvents");
		if (lpfn_TN_DisConnectEvents)
		{
			lpfn_TN_DisConnectEvents((long)(static_cast<IThumbnailXpress *>(pThumbnailXpress)), m_dwCookie);
		}
		::FreeLibrary(hDLL);
	}
}

   void SetClickEvent(HRESULT (*pMyClick) (DWORD classPtr, DWORD objID))
   {
     pClick = pMyClick;
   }

   HRESULT Click()
   {
      if (pClick)
         (*pClick)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetDblClickEvent(HRESULT (*pMyDblClick) (DWORD classPtr, DWORD objID))
   {
     pDblClick = pMyDblClick;
   }

   HRESULT DblClick()
   {
      if (pDblClick)
         (*pDblClick)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetMouseDownEvent(HRESULT (*pMyMouseDown) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y))
   {
     pMouseDown = pMyMouseDown;
   }

   HRESULT MouseDown(short Button, short Shift, long x, long y)
   {
      if (pMouseDown)
         (*pMouseDown)(m_classPtr, m_objID, Button, Shift, x, y);
      return S_OK;
   }

   void SetMouseMoveEvent(HRESULT (*pMyMouseMove) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y))
   {
     pMouseMove = pMyMouseMove;
   }

   HRESULT MouseMove(short Button, short Shift, long x, long y)
   {
      if (pMouseMove)
         (*pMouseMove)(m_classPtr, m_objID, Button, Shift, x, y);
      return S_OK;
   }

   void SetMouseUpEvent(HRESULT (*pMyMouseUp) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y))
   {
     pMouseUp = pMyMouseUp;
   }

   HRESULT MouseUp(short Button, short Shift, long x, long y)
   {
      if (pMouseUp)
         (*pMouseUp)(m_classPtr, m_objID, Button, Shift, x, y);
      return S_OK;
   }

   void SetKeyUpEvent(HRESULT (*pMyKeyUp) (DWORD classPtr, DWORD objID, short KeyCode, short Shift))
   {
     pKeyUp = pMyKeyUp;
   }

   HRESULT KeyUp(short KeyCode, short Shift)
   {
      if (pKeyUp)
         (*pKeyUp)(m_classPtr, m_objID, KeyCode, Shift);
      return S_OK;
   }

   void SetKeyDownEvent(HRESULT (*pMyKeyDown) (DWORD classPtr, DWORD objID, short KeyCode, short Shift))
   {
     pKeyDown = pMyKeyDown;
   }

   HRESULT KeyDown(short KeyCode, short Shift)
   {
      if (pKeyDown)
         (*pKeyDown)(m_classPtr, m_objID, KeyCode, Shift);
      return S_OK;
   }

   void SetKeyPressEvent(HRESULT (*pMyKeyPress) (DWORD classPtr, DWORD objID, short KeyASCII))
   {
     pKeyPress = pMyKeyPress;
   }

   HRESULT KeyPress(short KeyASCII)
   {
      if (pKeyPress)
         (*pKeyPress)(m_classPtr, m_objID, KeyASCII);
      return S_OK;
   }

   void SetPaintEvent(HRESULT (*pMyPaint) (DWORD classPtr, DWORD objID, LONG hDC, LONG left, LONG top, LONG width, LONG height))
   {
     pPaint = pMyPaint;
   }

   HRESULT Paint(LONG hDC, LONG left, LONG top, LONG width, LONG height)
   {
      if (pPaint)
         (*pPaint)(m_classPtr, m_objID, hDC, left, top, width, height);
      return S_OK;
   }

   void SetSelectionIndexChangedEvent(HRESULT (*pMyIndexChanged) (DWORD classPtr, DWORD objID))
   {
     pIndexChanged = pMyIndexChanged;
   }

   HRESULT SelectionIndexChanged()
   {
      if (pIndexChanged)
         (*pIndexChanged)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetErrorEvent(HRESULT (*pMyError) (DWORD classPtr, DWORD objID, LONG ItemIndex, LONG ErrorNum))
   {
     pError = pMyError;
   }

   HRESULT Error(LONG ItemIndex, LONG ErrorNum)
   {
      if (pError)
         (*pError)(m_classPtr, m_objID, ItemIndex, ErrorNum);
      return S_OK;
   }

   void SetItemCompleteEvent(HRESULT (*pMyItemComplete) (DWORD classPtr, DWORD objID, LONG ItemIndex))
   {
     pItemComplete = pMyItemComplete;
   }

   HRESULT ItemComplete(LONG ItemIndex)
   {
      if (pItemComplete)
         (*pItemComplete)(m_classPtr, m_objID, ItemIndex);
      return S_OK;
   }


  DWORD m_objID;
  DWORD m_classPtr;
  IThumbnailXpressPtr pThumbnailXpress;
};

#endif // end ifndef THUMBNAILXPRESS1EVENTS_H
VERSION 5.00
Object = "{81E5FAC5-E135-4668-866B-92BEE1A020E9}#1.0#0"; "PegasusImaging.ActiveX.ThumbnailXpress1.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmthumb 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ThumbnailXpress Viewer"
   ClientHeight    =   10440
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   13530
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10440
   ScaleWidth      =   13530
   StartUpPosition =   1  'CenterOwner
   Begin PegasusImagingActiveXThumbnailXpress1Ctl.ThumbnailXpress tn 
      Height          =   6495
      Left            =   5760
      TabIndex        =   31
      Top             =   1920
      Width           =   5775
      IXErrorString   =   "3E9CA9144E27BA609D8F12924B60D000"
      _ExtentX        =   10186
      _ExtentY        =   11456
      ErrStr          =   "A9B807851B3D9539BC58301E0FE21EE2"
      ErrCode         =   1134848132
      ErrInfo         =   791006347
      _cx             =   10186
      _cy             =   11456
      BackColor       =   16777215
      BorderStyle     =   2
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   0
   End
   Begin MSComDlg.CommonDialog cd 
      Left            =   5880
      Top             =   9240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove Selected Items"
      Height          =   615
      Left            =   11640
      TabIndex        =   23
      Top             =   4200
      Width           =   1575
   End
   Begin VB.ListBox lstSelect 
      Height          =   1620
      Left            =   11640
      TabIndex        =   22
      Top             =   2520
      Width           =   1575
   End
   Begin VB.CommandButton cmdSelected 
      Caption         =   "Retrieve Selected Items"
      Height          =   615
      Left            =   11640
      TabIndex        =   21
      Top             =   1800
      Width           =   1575
   End
   Begin VB.ComboBox cmbSelect 
      Height          =   315
      Left            =   11640
      TabIndex        =   18
      Top             =   1320
      Width           =   1695
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7695
      Left            =   0
      TabIndex        =   4
      Top             =   1920
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   13573
      _Version        =   393216
      Tabs            =   4
      Tab             =   3
      TabsPerRow      =   4
      TabHeight       =   882
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Filters"
      TabPicture(0)   =   "thumbnailviewer.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lblFType"
      Tab(0).Control(1)=   "lblComareT"
      Tab(0).Control(2)=   "lblFsize"
      Tab(0).Control(3)=   "cmbChoice"
      Tab(0).Control(4)=   "cmbCompare"
      Tab(0).Control(5)=   "txtFileSize"
      Tab(0).Control(6)=   "datetime"
      Tab(0).Control(7)=   "chkDir"
      Tab(0).Control(8)=   "chkCase"
      Tab(0).Control(9)=   "chkHidden"
      Tab(0).Control(10)=   "chkEnabled"
      Tab(0).Control(11)=   "chkParent"
      Tab(0).Control(12)=   "chkSub"
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "Thumbnail Attributes"
      TabPicture(1)   =   "thumbnailviewer.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblCellWidth"
      Tab(1).Control(1)=   "lblCellHeight"
      Tab(1).Control(2)=   "lblCellHor"
      Tab(1).Control(3)=   "lblCellVert"
      Tab(1).Control(4)=   "lblCellW"
      Tab(1).Control(5)=   "lblScrollDir"
      Tab(1).Control(6)=   "cmdColor"
      Tab(1).Control(7)=   "scrCellBWidth"
      Tab(1).Control(8)=   "scrCellHeight"
      Tab(1).Control(9)=   "scrCellHS"
      Tab(1).Control(10)=   "scrCellVS"
      Tab(1).Control(11)=   "scrCellWidth"
      Tab(1).Control(12)=   "cmdInvoke"
      Tab(1).Control(13)=   "optHor"
      Tab(1).Control(14)=   "optVert"
      Tab(1).ControlCount=   15
      TabCaption(2)   =   "Thread Processing"
      TabPicture(2)   =   "thumbnailviewer.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lblMaxThread"
      Tab(2).Control(1)=   "scrMax"
      Tab(2).Control(2)=   "txtmax"
      Tab(2).Control(3)=   "frmThreads"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Cache Settings"
      TabPicture(3)   =   "thumbnailviewer.frx":0054
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "cmdFlush"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).Control(1)=   "cmdClearCache"
      Tab(3).Control(1).Enabled=   0   'False
      Tab(3).Control(2)=   "cmdSetCache"
      Tab(3).Control(2).Enabled=   0   'False
      Tab(3).Control(3)=   "frmCashe"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).ControlCount=   4
      Begin VB.Frame frmCashe 
         Caption         =   "Cache Parameters"
         Height          =   2775
         Left            =   360
         TabIndex        =   53
         Top             =   2520
         Width           =   4815
         Begin VB.TextBox txtFName 
            Height          =   375
            Left            =   600
            TabIndex        =   56
            Top             =   1920
            Width           =   3495
         End
         Begin VB.TextBox txtMemSize 
            Height          =   375
            Left            =   2400
            TabIndex        =   55
            Top             =   1080
            Width           =   1575
         End
         Begin VB.TextBox txtDiskSize 
            Height          =   375
            Left            =   600
            TabIndex        =   54
            Top             =   1080
            Width           =   1575
         End
         Begin VB.Label lblFileName 
            Caption         =   "FileName:"
            Height          =   255
            Left            =   600
            TabIndex        =   59
            Top             =   1560
            Width           =   3015
         End
         Begin VB.Label lblMemSize 
            Caption         =   "Memory Size"
            Height          =   255
            Left            =   2400
            TabIndex        =   58
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label lblDiskSize 
            Caption         =   "Disk Size"
            Height          =   255
            Left            =   600
            TabIndex        =   57
            Top             =   720
            Width           =   1335
         End
      End
      Begin VB.CommandButton cmdSetCache 
         Caption         =   "Set Cache Settings"
         Height          =   495
         Left            =   1440
         TabIndex        =   52
         Top             =   1560
         Width           =   2535
      End
      Begin VB.CommandButton cmdClearCache 
         Caption         =   "Clear Cache"
         Height          =   375
         Left            =   2880
         TabIndex        =   51
         Top             =   960
         Width           =   1815
      End
      Begin VB.CommandButton cmdFlush 
         Caption         =   "Flush Cache"
         Height          =   375
         Left            =   480
         TabIndex        =   50
         Top             =   960
         Width           =   1815
      End
      Begin VB.Frame frmThreads 
         Caption         =   "Thread Time Thresholds"
         Height          =   2895
         Left            =   -74520
         TabIndex        =   44
         Top             =   1980
         Width           =   4575
         Begin VB.TextBox txtHung 
            Height          =   375
            Left            =   2640
            TabIndex        =   49
            Top             =   1920
            Width           =   1455
         End
         Begin VB.TextBox txtStart 
            Height          =   375
            Left            =   2640
            TabIndex        =   48
            Top             =   1200
            Width           =   1455
         End
         Begin VB.CommandButton cmdThread 
            Caption         =   "Set Thread Values"
            Height          =   495
            Left            =   2520
            TabIndex        =   45
            Top             =   360
            Width           =   1695
         End
         Begin VB.Label lblHung 
            Caption         =   "Hung Thread Threshold"
            Height          =   375
            Left            =   360
            TabIndex        =   47
            Top             =   1920
            Width           =   1935
         End
         Begin VB.Label lblStart 
            Caption         =   "Start Thread Threshold"
            Height          =   495
            Left            =   360
            TabIndex        =   46
            Top             =   1200
            Width           =   1815
         End
      End
      Begin VB.TextBox txtmax 
         Height          =   285
         Left            =   -73200
         TabIndex        =   43
         Tag             =   "1"
         Text            =   "1"
         Top             =   900
         Width           =   1095
      End
      Begin VB.VScrollBar scrMax 
         Height          =   255
         Left            =   -72120
         Max             =   1
         Min             =   33
         TabIndex        =   41
         Top             =   900
         Value           =   1
         Width           =   255
      End
      Begin VB.OptionButton optVert 
         Caption         =   "Vertical Scroll"
         Height          =   375
         Left            =   -74760
         TabIndex        =   39
         Top             =   1380
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.OptionButton optHor 
         Caption         =   "Horizontal Scroll"
         Height          =   375
         Left            =   -74760
         TabIndex        =   38
         Top             =   1860
         Width           =   1575
      End
      Begin VB.CommandButton cmdInvoke 
         Caption         =   "Invoke Cell Settings"
         Height          =   495
         Left            =   -72480
         TabIndex        =   37
         Top             =   5820
         Width           =   2295
      End
      Begin VB.HScrollBar scrCellWidth 
         Height          =   255
         Left            =   -72600
         Max             =   100
         Min             =   32
         TabIndex        =   30
         Top             =   5340
         Value           =   40
         Width           =   2775
      End
      Begin VB.HScrollBar scrCellVS 
         Height          =   255
         Left            =   -72600
         Max             =   90
         Min             =   1
         TabIndex        =   29
         Top             =   4620
         Value           =   10
         Width           =   2775
      End
      Begin VB.HScrollBar scrCellHS 
         Height          =   255
         Left            =   -72600
         Max             =   90
         Min             =   1
         TabIndex        =   28
         Top             =   3660
         Value           =   10
         Width           =   2895
      End
      Begin VB.HScrollBar scrCellHeight 
         Height          =   255
         Left            =   -72600
         Max             =   100
         Min             =   32
         TabIndex        =   27
         Top             =   2700
         Value           =   32
         Width           =   2895
      End
      Begin VB.HScrollBar scrCellBWidth 
         Height          =   255
         Left            =   -72600
         Max             =   20
         Min             =   1
         TabIndex        =   26
         Top             =   1740
         Value           =   5
         Width           =   2895
      End
      Begin VB.CommandButton cmdColor 
         Caption         =   "Choose Item Border Color"
         Height          =   375
         Left            =   -72360
         TabIndex        =   25
         Top             =   780
         Width           =   2055
      End
      Begin VB.CheckBox chkSub 
         Caption         =   "Include SubDir"
         Height          =   255
         Left            =   -74640
         TabIndex        =   17
         Top             =   6660
         Width           =   2415
      End
      Begin VB.CheckBox chkParent 
         Caption         =   "Include Parent Dir"
         Height          =   255
         Left            =   -74640
         TabIndex        =   16
         Top             =   6300
         Width           =   2175
      End
      Begin VB.CheckBox chkEnabled 
         Caption         =   "Enabled"
         Height          =   255
         Left            =   -74640
         TabIndex        =   15
         Top             =   5940
         Value           =   1  'Checked
         Width           =   1575
      End
      Begin VB.CheckBox chkHidden 
         Caption         =   "Include Hidden"
         Height          =   375
         Left            =   -74640
         TabIndex        =   14
         Top             =   5460
         Width           =   3375
      End
      Begin VB.CheckBox chkCase 
         Caption         =   "Match is Case Sensitive"
         Height          =   375
         Left            =   -74640
         TabIndex        =   13
         Top             =   4980
         Width           =   3135
      End
      Begin VB.CheckBox chkDir 
         Caption         =   "Match Applies to Directory"
         Height          =   255
         Left            =   -74640
         TabIndex        =   12
         Top             =   4620
         Width           =   3015
      End
      Begin MSACAL.Calendar datetime 
         Height          =   2175
         Left            =   -73200
         TabIndex        =   11
         Top             =   2340
         Visible         =   0   'False
         Width           =   3495
         _Version        =   524288
         _ExtentX        =   6165
         _ExtentY        =   3836
         _StockProps     =   1
         BackColor       =   -2147483633
         Year            =   2006
         Month           =   4
         Day             =   24
         DayLength       =   1
         MonthLength     =   1
         DayFontColor    =   0
         FirstDay        =   7
         GridCellEffect  =   1
         GridFontColor   =   10485760
         GridLinesColor  =   -2147483632
         ShowDateSelectors=   -1  'True
         ShowDays        =   -1  'True
         ShowHorizontalGrid=   -1  'True
         ShowTitle       =   -1  'True
         ShowVerticalGrid=   -1  'True
         TitleFontColor  =   10485760
         ValueIsNull     =   0   'False
         BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtFileSize 
         Height          =   405
         Left            =   -73200
         TabIndex        =   10
         Top             =   1860
         Width           =   3135
      End
      Begin VB.ComboBox cmbCompare 
         Height          =   315
         Left            =   -73200
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1380
         Width           =   3135
      End
      Begin VB.ComboBox cmbChoice 
         Height          =   315
         ItemData        =   "thumbnailviewer.frx":0070
         Left            =   -73200
         List            =   "thumbnailviewer.frx":0072
         TabIndex        =   5
         Text            =   "cmbChoice"
         Top             =   840
         Width           =   3015
      End
      Begin VB.Label lblMaxThread 
         Caption         =   "Max Threads:"
         Height          =   375
         Left            =   -74520
         TabIndex        =   42
         Top             =   900
         Width           =   975
      End
      Begin VB.Label lblScrollDir 
         Caption         =   "ScrollDirection"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   -74760
         TabIndex        =   40
         Top             =   900
         Width           =   1455
      End
      Begin VB.Label lblCellW 
         Caption         =   "Cell Width"
         Height          =   255
         Left            =   -72960
         TabIndex        =   36
         Top             =   4980
         Width           =   2895
      End
      Begin VB.Label lblCellVert 
         Caption         =   "Cell Vertical Spacing"
         Height          =   375
         Left            =   -72960
         TabIndex        =   35
         Top             =   4140
         Width           =   3135
      End
      Begin VB.Label lblCellHor 
         Caption         =   "Cell Horizontal Spacing"
         Height          =   375
         Left            =   -72960
         TabIndex        =   34
         Top             =   3180
         Width           =   2655
      End
      Begin VB.Label lblCellHeight 
         Caption         =   "Cell Height"
         Height          =   375
         Left            =   -72960
         TabIndex        =   33
         Top             =   2220
         Width           =   2535
      End
      Begin VB.Label lblCellWidth 
         Caption         =   "Cell Border Width"
         Height          =   255
         Left            =   -72960
         TabIndex        =   32
         Top             =   1260
         Width           =   2535
      End
      Begin VB.Label lblFsize 
         Caption         =   "File Size:"
         Height          =   375
         Left            =   -74640
         TabIndex        =   9
         Top             =   1860
         Width           =   1215
      End
      Begin VB.Label lblComareT 
         Caption         =   "Compare Type:"
         Height          =   375
         Left            =   -74640
         TabIndex        =   8
         Top             =   1380
         Width           =   1215
      End
      Begin VB.Label lblFType 
         Caption         =   "Filter Type:"
         Height          =   255
         Left            =   -74640
         TabIndex        =   7
         Top             =   900
         Width           =   1215
      End
   End
   Begin VB.ListBox lstFiles 
      Height          =   2790
      ItemData        =   "thumbnailviewer.frx":0074
      Left            =   11640
      List            =   "thumbnailviewer.frx":0076
      OLEDragMode     =   1  'Automatic
      OLEDropMode     =   1  'Manual
      TabIndex        =   2
      Top             =   5400
      Width           =   1695
   End
   Begin VB.ListBox lstdesc 
      Height          =   1815
      ItemData        =   "thumbnailviewer.frx":0078
      Left            =   120
      List            =   "thumbnailviewer.frx":0097
      TabIndex        =   1
      Top             =   0
      Width           =   11415
   End
   Begin VB.CommandButton cmdclear 
      Caption         =   "Clear Items"
      Height          =   375
      Left            =   11640
      TabIndex        =   0
      Top             =   8400
      Width           =   1695
   End
   Begin VB.Label lblerrorlast 
      Caption         =   "Last Error Reported:"
      Height          =   495
      Left            =   6000
      TabIndex        =   24
      Top             =   8520
      Width           =   1455
   End
   Begin VB.Label lblError 
      Height          =   615
      Left            =   7560
      TabIndex        =   20
      Top             =   8520
      Width           =   3735
   End
   Begin VB.Label lblSelection 
      Caption         =   "Selection Mode:"
      Height          =   375
      Left            =   11640
      TabIndex        =   19
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label lstDragTarget 
      Caption         =   "Drag File Target:"
      Height          =   255
      Left            =   11640
      TabIndex        =   3
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpenDir 
         Caption         =   "&OpenDirectory"
      End
      Begin VB.Menu mnuOpenFile 
         Caption         =   "&OpenFile"
      End
      Begin VB.Menu mnuSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
      Begin VB.Menu mnuTNXpress 
         Caption         =   "&ThumbnailXpress AboutBox"
      End
      Begin VB.Menu mnuIXAbout 
         Caption         =   "&ImagXpress AboutBox"
      End
   End
End
Attribute VB_Name = "frmthumb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim lstcounter As Integer

Dim filtertest As String

Dim selcount As Long
Dim counter As Integer
Dim index As Long
Dim fname As String
Dim indexselected As Long
Dim indexselectedret As Long

 Dim testcounter As Integer
 Dim testcounter1 As Integer
 Dim hidecounter As Integer
 Dim deletecounter As Integer
 Dim indexdeletecounter As Integer
 Private Declare Function GetCurrentDirectory Lib "kernel32" Alias "GetCurrentDirectoryA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
 Private Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long


Private Sub chkEnabled_Click()
   
    'Enable/Disable the filter
        If chkEnabled.Value = 1 Then
            'enable the filter
            tn.ClearFilters
            tn.AddFilterItem filtertest
            tn.SetFilterEnabled filtertest, True
        Else
            'disable the filter
            tn.ClearFilters
            tn.AddFilterItem filtertest
            tn.SetFilterEnabled filtertest, False
        End If
End Sub

Private Sub cmbDateTime_Click()
        datetime.Visible = True
End Sub




Private Sub cmbChoice_Click()
        Select Case cmbChoice.ListIndex
        
        'FileName Match
            Case 0
                chkEnabled.Enabled = True
                chkDir.Enabled = True
                chkCase.Enabled = True
                chkSub.Enabled = False
                chkParent.Enabled = False
                datetime.Visible = False
                cmbCompare.Enabled = False
                chkHidden.Enabled = False
                txtFileSize.Visible = False
              
                'File Attributes Directory
            Case 1
                chkDir.Enabled = False
                chkParent.Enabled = True
                chkSub.Enabled = True
                chkParent.Enabled = True
                chkHidden.Enabled = True
                chkCase.Enabled = False
                datetime.Visible = False
                cmbCompare.Enabled = False
                txtFileSize.Visible = False
                
               'File Creation Date
            Case 2
                chkEnabled.Enabled = True
                cmbCompare.Enabled = True
                datetime.Visible = True
                txtFileSize.Visible = False
                chkSub.Enabled = False
                chkParent.Enabled = False
                chkHidden.Enabled = False
                'File Mod Date
            Case 3
                chkEnabled.Enabled = True
                datetime.Visible = True
                cmbCompare.Enabled = True
                txtFileSize.Enabled = False
                chkSub.Enabled = False
                chkParent.Enabled = False
                chkHidden.Enabled = False
               'File Size
            Case 4
                chkEnabled.Enabled = True
                txtFileSize.Visible = True
                cmbCompare.Enabled = True
                chkSub.Enabled = False
                chkParent.Enabled = False
                datetime.Visible = True
                 datetime.Visible = False
            Case 5
            'IX Supported File
                chkEnabled.Enabled = True
                datetime.Visible = False
                cmbCompare.Enabled = False
                chkCase.Enabled = False
                chkDir.Enabled = False
                chkParent.Enabled = False
                chkSub.Enabled = False
                chkHidden.Enabled = False
                txtFileSize.Visible = False
                
        End Select
End Sub



Private Sub cmbSelect_Click()
       Select Case cmbSelect.ListIndex
            Case 0
                tn.SelectionMode = TN_MultiExtended
           
            Case 1
               tn.SelectionMode = TN_MultiSimple
            
            Case 2
                tn.SelectionMode = TN_None
               
            Case 3
                tn.SelectionMode = TN_Single
              
        End Select
End Sub

Private Sub cmdclear_Click()
 On Error GoTo errorhandler
   tn.DeleteItems
   lstSelect.Clear
   lstFiles.Clear
errorhandler:

    lblError.Caption = Err.Description
End Sub

Private Sub cmdClearCache_Click()
 On Error GoTo errorhandler
    tn.EmptyCache
    
errorhandler:
  
    lblError.Caption = Err.Description

End Sub

Private Sub cmdColor_Click()
 On Error GoTo errorhandler
    cd.ShowColor

 
errorhandler:
  
    lblError.Caption = Err.Description
End Sub

Private Sub cmdFlush_Click()
On Error GoTo errorhandler
    tn.FlushCache
    
errorhandler:
  
    lblError.Caption = Err.Description
End Sub

Private Sub cmdInvoke_Click()
On Error GoTo errorhandler
    tn.BeginUpdate
    tn.CellBorderColor = cd.Color
     'cell border width
    tn.CellBorderWidth = scrCellBWidth.Value
    'cell height
    tn.CellHeight = scrCellHeight.Value
    'cell horizontal spacing
    tn.CellHorizontalSpacing = scrCellHS.Value
    'cell vertical spacing
    tn.CellVerticalSpacing = scrCellVS.Value
    'cell width
    tn.CellWidth = scrCellWidth.Value
    tn.EndUpdate
    
errorhandler:
  
    lblError.Caption = Err.Description
End Sub

Private Sub cmdremove_Click()
 On Error GoTo errorhandler

'code for just deselecting a selected items(s)
'hidecounter = tn.SelectedItemCount - 1
'  For testcounter = hidecounter To 0 Step -1
'  'remove the item from being selected
'        tn.DeleteSelectedItem testcounter
'Next
    
'code for removing selected items(s)
deletecounter = tn.SelectedItemCount - 1
For testcounter1 = deletecounter To 0 Step -1
          indexdeletecounter = tn.GetIndexOfSelectedItem(testcounter1)
          tn.DeleteItem indexdeletecounter
          
Next
lstSelect.Clear
errorhandler:
  
    lblError.Caption = Err.Description
End Sub
Private Sub cmdSelected_Click()

On Error GoTo errorhandler
   'get the selecteditem count
   lstSelect.Clear
   selcount = tn.SelectedItemCount
   'iterate through retrieving the selected items
    For counter = 0 To selcount - 1
          index = tn.GetIndexOfSelectedItem(counter)
          lstSelect.AddItem index
    Next
    
errorhandler:
  
    lblError.Caption = Err.Description
End Sub



Private Sub cmdSetCache_Click()
      On Error GoTo errorhandler
          
      tn.CacheDiskLimit = Val(txtDiskSize.Text)
      tn.CacheMemoryLimit = Val(txtMemSize.Text)
      tn.CacheFile = txtFName.Text
      
errorhandler:
  
    lblError.Caption = Err.Description
End Sub

Private Sub cmdThread_Click()
    On Error GoTo errorhandler
    
    tn.ThreadStartThreshold = Val(txtStart.Text)
    tn.ThreadHungThreshold = Val(txtHung.Text)

errorhandler:
  
    lblError.Caption = Err.Description
End Sub



Private Sub Form_Load()

On Error GoTo errorhandler
                   
 '*****Must use these functions at application startup
 ' tn.UnlockRuntime 1234, 1234, 1234, 1234
  '*****for distribution, place your IX unlock codes here******
  'tn.SetIXLicenseCodes 1234, 1234, 1234, 1234
    imgParentDir = App.Path
    
    Set fs = New FileSystemObject
    
    'type of filter
    cmbChoice.AddItem "File Name Match"
    cmbChoice.AddItem "File Attributes and Directory"
    cmbChoice.AddItem "File Creation Date"
    cmbChoice.AddItem "File Modification Date"
    cmbChoice.AddItem "FileSize"
    cmbChoice.AddItem "ImagXpress Supported File"
    
    cmbChoice.ListIndex = 0
    
    'compare filter
    cmbCompare.AddItem "LessThan"
    cmbCompare.AddItem "LessThan or Equal"
    cmbCompare.AddItem "Equal"
    cmbCompare.AddItem "GreaterThan or Equal"
    cmbCompare.AddItem "GreaterThan"
    
    cmbCompare.ListIndex = 0
  
    'datetime filter
    datetime.Today
    
    'selection mode
    cmbSelect.AddItem "MultiExtended"
    cmbSelect.AddItem "MultiSimple"
    cmbSelect.AddItem "None"
    cmbSelect.AddItem "Single"
    
    cmbSelect.ListIndex = 3
    
    'set the viewable tab item
    SSTab1.Tab = 0
    
    'threading values
    txtmax.Text = tn.MaximumThreadCount
    txtStart.Text = tn.ThreadStartThreshold
    txtHung.Text = tn.ThreadHungThreshold

    'cache settings
    txtDiskSize.Text = tn.CacheDiskLimit
    txtMemSize.Text = tn.CacheMemoryLimit
    txtFName.Text = tn.CacheFile

    'default filter name here
    filtertest = "Peg1"
    'enable exception handling in TNXpress and set some defaults
    tn.RaiseExceptions = True
    frmviewer.ImagXpress1.RaiseExceptions = True
    tn.BorderStyle = TN_BorderFixed3D
    tn.CellBorderColor = RGB(0, 0, 255)
    tn.CellBorderWidth = 2
    

    tn.UsePDFXpress = True
errorhandler:
  
    lblError.Caption = Err.Description
End Sub


Private Sub lstFiles_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
        'clear the items each time
        lstFiles.Clear
        'Get a list of the files from the external source(for example, windows explorer)
        Dim temp As String
        For lstcounter = 1 To Data.Files.Count

               lstFiles.AddItem fs.GetFileName(Data.Files(lstcounter))
               'load the items into the TNXpress control
              tn.AddFileItems Data.Files(lstcounter), lstcounter - 1, False
        Next
End Sub

Private Sub mnuIXAbout_Click()
    frmviewer.ImagXpress1.AboutBox
End Sub

Private Sub mnuOpenDir_Click()
'add a Directory of TN items here
    
 On Error GoTo errorhandler
    Dim tmpFN As String
    
    'create a new filter here
    tn.ClearFilters
    filtertest = "Peg1"
    tn.AddFilterItem filtertest
    tmpFN = PegasusOpenFileP(imgParentDir)
   
    '******need to return the filter index here for the filter by filename filter
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        'filters enabled
        
    Select Case indexret
                Case 1
                    'all IX file types
                Case 2
                    tn.SetFilterFilenamePattern filtertest, "*.bmp"
                Case 3
                   tn.SetFilterFilenamePattern filtertest, "*.cal"
                Case 4
                   tn.SetFilterFilenamePattern filtertest, "*.dib"
                Case 5
                    tn.SetFilterFilenamePattern filtertest, "*.mod"
                Case 6
                    tn.SetFilterFilenamePattern filtertest, "*.dcx"
                Case 7
                   tn.SetFilterFilenamePattern filtertest, "*.gif"
                Case 8
                     tn.SetFilterFilenamePattern filtertest, "*.jp2"
                Case 9
                     tn.SetFilterFilenamePattern filtertest, "*.jls"
                Case 10
                     tn.SetFilterFilenamePattern filtertest, "*.jpg"
                Case 11
                    tn.SetFilterFilenamePattern filtertest, "*.ljp"
                Case 12
                     tn.SetFilterFilenamePattern filtertest, "*.pbm"
                Case 13
                    tn.SetFilterFilenamePattern filtertest, "*.pcx"
                Case 14
                     tn.SetFilterFilenamePattern filtertest, "*.pgm"
                Case 15
                     tn.SetFilterFilenamePattern filtertest, "*.pic"
                Case 16
                     tn.SetFilterFilenamePattern filtertest, "*.png"
                Case 17
                     tn.SetFilterFilenamePattern filtertest, "*.ppm"
                Case 18
                    tn.SetFilterFilenamePattern filtertest, "*.tiff"
                
                Case 19
                     tn.SetFilterFilenamePattern filtertest, "*.tga"
            
                Case 20
                     tn.SetFilterFilenamePattern filtertest, "*.wsq"
                    
                Case 21
                      tn.SetFilterFilenamePattern filtertest, "*.jb2"

                Case 22
                     tn.SetFilterFilenamePattern filtertest, "*.*"
                    
            End Select
    If chkEnabled.Value = 1 Then
                tn.SetFilterEnabled filtertest, True

                tn.SetFilterType filtertest, Val(cmbChoice.ListIndex)
                tn.SetFilterComparison filtertest, Val(cmbCompare.ListIndex)
        
                tn.SetFilterDate filtertest, datetime.Value
                If Not (txtFileSize.Text = "") Then
                    tn.SetFilterSize filtertest, Val(txtFileSize.Text)
                End If
                
                tn.SetFilterCaseSensitive filtertest, chkCase.Value
                tn.SetFilterMatchAppliesToDirectory filtertest, chkDir.Value
                tn.SetFilterIncludeHidden filtertest, chkHidden.Value
                tn.SetFilterIncludeParentDirectory filtertest, chkParent.Value
                tn.SetFilterIncludeSubDirectory filtertest, chkSub.Value
                
                'filters enabled/ add the director items
                tn.AddDirectoryItems imgParentDir, 0
              
        Else
              'filters disabled
            tn.SetFilterEnabled filtertest, False
            tn.AddDirectoryItems imgParentDir, 0
        End If
        
       
    End If
    

errorhandler:
  
    lblError.Caption = Err.Description
    
End Sub

Private Sub mnuOpenFile_Click()
'add individual TN items here

On Error GoTo errorhandler
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        'set to open a multi page TIFF if loaded
        tn.AddFileItems imgFileName, 0, True
    End If

errorhandler:
    Debug.Print Err.Description
    lblError.Caption = Err.Description
End Sub

Private Sub mnuQuit_Click()
    End
End Sub

Private Sub LoadFile()
   
   Err = tn.ErrorCode
   PegasusError Err, lblError
   
End Sub

Public Function GetFileName(flname As String) As String
    

    Dim posn As Integer, i As Integer
    Dim fname As String
    
    posn = 0
    'find the position of the last "\" character in filename
    For i = 1 To Len(flname)
        If (Mid(flname, i, 1) = "\") Then posn = i
    Next i

    'get filename without path
    fname = Right(flname, Len(flname) - posn)

    'get filename without extension
    posn = InStr(fname, ".")
        If posn <> 0 Then
            fname = Left(fname, posn - 1)
        End If
    GetFileName = fname
End Function

Private Sub mnuTNXpress_Click()
    tn.AboutBox
End Sub

Private Sub optHor_Click()
    If optHor.Value = True Then
            tn.ScrollDirection = TN_Horizontal
    Else
            tn.ScrollDirection = TN_Vertical
    End If
End Sub

Private Sub optVert_Click()
    If optVert.Value = True Then
            tn.ScrollDirection = TN_Vertical
    Else
             tn.ScrollDirection = TN_Horizontal
    End If
End Sub

Private Sub scrMax_Change()
    txtmax.Text = scrMax.Value
    tn.MaximumThreadCount = scrMax.Value
End Sub


Private Sub tn_DoubleClick()

On Error GoTo errorhandler
    
Dim info As PegasusImagingActiveXThumbnailXpress1Ctl.TN_ThumbnailType

indexselectedret = tn.GetIndexOfSelectedItem(indexselected)
'******test code here to show different DIB values being returned for same index****

If tn.GetItemType(indexselectedret) = TN_ParentDirectory Or tn.GetItemType(indexselectedret) = TN_Subdirectory Then

Dim cdir As String
'create a buffer
cdir = String(255, 0)
'retrieve the current directory
GetCurrentDirectory 255, cdir
Dim fold As Folder
Dim sfold As Folders

Dim sfile As Files
Dim singlef As File

Dim foldt As Folder

tn.BeginUpdate
tn.DeleteItems

'get the folder
Set fold = fs.GetFolder(cdir)

'get the subfodler
 Set sfold = fold.SubFolders

 For Each foldt In sfold
    Set sfile = foldt.Files
      For Each singlef In sfile
          tn.AddFileItems singlef.Path, 0, False
    Next
   
 Next

tn.EndUpdate
 
Else
'     'obtain the TNXpress item' selected index and pass image to IX for viewing
    indexselectedret = tn.GetIndexOfSelectedItem(indexselected)
    'obtain the TNXpress item's DIB and pass to ImagXpress hDib property
    frmviewer.AddImage tn.GetItemFilename(indexselectedret)
    frmviewer.Show 1

End If

errorhandler:

    lblError.Caption = Err.Description
End Sub

Private Sub tn_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

On Error GoTo errorhandler
     'obtain the TNXpress item' selected index
   ' indexselectedret = tn.GetIndexOfSelectedItem(indexselected)
    'obtain the TNXpress item's DIB and pass to ImagXpress hDib property
    'ImagXpress1.hDIB = tn.GetItemHDib(indexselectedret)
    'obtain the TNXpress item's FileName
    'lblFileName.Caption = fs.GetFileName(tn.GetItemFilename(indexselectedret))
errorhandler:

    lblError.Caption = Err.Description
End Sub


Private Sub VScroll1_Change()
    txtmax.Text =
End Sub

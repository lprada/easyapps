
'****************************************************************
' * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida.*
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/

Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.ThumbnailXpress1
Imports System.Windows.Forms
Imports System.IO

Public Class Form1
    Inherits System.Windows.Forms.Form

    Private filteritem As PegasusImaging.WinForms.ThumbnailXpress1.Filter
    Private point As System.Drawing.Point
    Private tdata As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem
    Private dragtarget As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem
    Private array As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem()
    Private external1 As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem


    Private cd As System.Windows.Forms.ColorDialog
    Private a As System.Int32

    'file I/O variables
    Private strCurrentDir As System.String
    Private strImageFile As System.String

    Private dragindex As System.Int32

    Private dragsource As System.Boolean


    Private filteritemCreated As Boolean

    Private indexOfItemUnderMouseToDrag As System.Int32

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        '***must call these UnlockControl functions 
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        'PegasusImaging.WinForms.ThumbnailXpress1.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

         
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents thumbnailXpress1 As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpress
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnufile As System.Windows.Forms.MenuItem
    Friend WithEvents mnudir As System.Windows.Forms.MenuItem
    Friend WithEvents mnusingle As System.Windows.Forms.MenuItem
    Friend WithEvents mnuexit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuhelp As System.Windows.Forms.MenuItem
    Friend WithEvents mnuabout As System.Windows.Forms.MenuItem
    Friend WithEvents chkexpand As System.Windows.Forms.CheckBox
    Friend WithEvents tabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Filters As System.Windows.Forms.TabPage
    Friend WithEvents txtsize As System.Windows.Forms.TextBox
    Friend WithEvents lblsize As System.Windows.Forms.Label
    Friend WithEvents dateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents chksub As System.Windows.Forms.CheckBox
    Friend WithEvents chkparent As System.Windows.Forms.CheckBox
    Friend WithEvents chkenable As System.Windows.Forms.CheckBox
    Friend WithEvents chkhid As System.Windows.Forms.CheckBox
    Friend WithEvents chksen As System.Windows.Forms.CheckBox
    Friend WithEvents chkdir As System.Windows.Forms.CheckBox
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents lblcompare As System.Windows.Forms.Label
    Friend WithEvents lbltype As System.Windows.Forms.Label
    Friend WithEvents cmbchoice As System.Windows.Forms.ComboBox
    Friend WithEvents cmbfiles As System.Windows.Forms.ComboBox
    Friend WithEvents cmbcompare As System.Windows.Forms.ComboBox
    Friend WithEvents tabselect As System.Windows.Forms.TabPage
    Friend WithEvents lblselect As System.Windows.Forms.Label
    Friend WithEvents cmbselect As System.Windows.Forms.ComboBox
    Friend WithEvents cells As System.Windows.Forms.TabPage
    Friend WithEvents lblCellWidth As System.Windows.Forms.Label
    Friend WithEvents lblcw As System.Windows.Forms.Label
    Friend WithEvents lblCVS As System.Windows.Forms.Label
    Friend WithEvents lblv As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents vcw As System.Windows.Forms.HScrollBar
    Friend WithEvents vbh As System.Windows.Forms.HScrollBar
    Friend WithEvents hbh As System.Windows.Forms.HScrollBar
    Friend WithEvents lblCHS As System.Windows.Forms.Label
    Friend WithEvents lblheight As System.Windows.Forms.Label
    Friend WithEvents hbheight As System.Windows.Forms.HScrollBar
    Friend WithEvents lblh As System.Windows.Forms.Label
    Friend WithEvents lblw As System.Windows.Forms.Label
    Friend WithEvents cmdcolor As System.Windows.Forms.Button
    Friend WithEvents cmdset As System.Windows.Forms.Button
    Friend WithEvents lblbwidth As System.Windows.Forms.Label
    Friend WithEvents hbwidth As System.Windows.Forms.HScrollBar
    Friend WithEvents errdesc As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents sampdesc As System.Windows.Forms.RichTextBox
    Friend WithEvents cmdremove As System.Windows.Forms.Button
    Friend WithEvents lstselect As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdsel As System.Windows.Forms.Button
    Friend WithEvents cmdclear As System.Windows.Forms.Button
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents chkCreate As System.Windows.Forms.RadioButton
    Friend WithEvents chkDragDrop As System.Windows.Forms.RadioButton
    Friend WithEvents tabThread As System.Windows.Forms.TabPage
    Friend WithEvents maxthreads As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblThreads As System.Windows.Forms.Label
    Friend WithEvents grpThreads As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSThread As System.Windows.Forms.TextBox
    Friend WithEvents lblHung As System.Windows.Forms.Label
    Friend WithEvents txtHungT As System.Windows.Forms.TextBox
    Friend WithEvents cmdSetThreads As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents chkPreserve As System.Windows.Forms.CheckBox
    Friend WithEvents chkRaw As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.thumbnailXpress1 = New PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpress()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnufile = New System.Windows.Forms.MenuItem()
        Me.mnudir = New System.Windows.Forms.MenuItem()
        Me.mnusingle = New System.Windows.Forms.MenuItem()
        Me.mnuexit = New System.Windows.Forms.MenuItem()
        Me.mnuhelp = New System.Windows.Forms.MenuItem()
        Me.mnuabout = New System.Windows.Forms.MenuItem()
        Me.chkexpand = New System.Windows.Forms.CheckBox()
        Me.tabControl1 = New System.Windows.Forms.TabControl()
        Me.Filters = New System.Windows.Forms.TabPage()
        Me.txtsize = New System.Windows.Forms.TextBox()
        Me.lblsize = New System.Windows.Forms.Label()
        Me.dateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.chksub = New System.Windows.Forms.CheckBox()
        Me.chkparent = New System.Windows.Forms.CheckBox()
        Me.chkenable = New System.Windows.Forms.CheckBox()
        Me.chkhid = New System.Windows.Forms.CheckBox()
        Me.chksen = New System.Windows.Forms.CheckBox()
        Me.chkdir = New System.Windows.Forms.CheckBox()
        Me.label4 = New System.Windows.Forms.Label()
        Me.lblcompare = New System.Windows.Forms.Label()
        Me.lbltype = New System.Windows.Forms.Label()
        Me.cmbchoice = New System.Windows.Forms.ComboBox()
        Me.cmbfiles = New System.Windows.Forms.ComboBox()
        Me.cmbcompare = New System.Windows.Forms.ComboBox()
        Me.cells = New System.Windows.Forms.TabPage()
        Me.lblCellWidth = New System.Windows.Forms.Label()
        Me.lblcw = New System.Windows.Forms.Label()
        Me.lblCVS = New System.Windows.Forms.Label()
        Me.lblv = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.vcw = New System.Windows.Forms.HScrollBar()
        Me.vbh = New System.Windows.Forms.HScrollBar()
        Me.hbh = New System.Windows.Forms.HScrollBar()
        Me.lblCHS = New System.Windows.Forms.Label()
        Me.lblheight = New System.Windows.Forms.Label()
        Me.hbheight = New System.Windows.Forms.HScrollBar()
        Me.lblh = New System.Windows.Forms.Label()
        Me.lblw = New System.Windows.Forms.Label()
        Me.cmdcolor = New System.Windows.Forms.Button()
        Me.cmdset = New System.Windows.Forms.Button()
        Me.lblbwidth = New System.Windows.Forms.Label()
        Me.hbwidth = New System.Windows.Forms.HScrollBar()
        Me.tabselect = New System.Windows.Forms.TabPage()
        Me.lblselect = New System.Windows.Forms.Label()
        Me.cmbselect = New System.Windows.Forms.ComboBox()
        Me.tabThread = New System.Windows.Forms.TabPage()
        Me.grpThreads = New System.Windows.Forms.GroupBox()
        Me.cmdSetThreads = New System.Windows.Forms.Button()
        Me.txtHungT = New System.Windows.Forms.TextBox()
        Me.lblHung = New System.Windows.Forms.Label()
        Me.txtSThread = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblThreads = New System.Windows.Forms.Label()
        Me.maxthreads = New System.Windows.Forms.NumericUpDown()
        Me.errdesc = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.sampdesc = New System.Windows.Forms.RichTextBox()
        Me.cmdremove = New System.Windows.Forms.Button()
        Me.lstselect = New System.Windows.Forms.CheckedListBox()
        Me.cmdsel = New System.Windows.Forms.Button()
        Me.cmdclear = New System.Windows.Forms.Button()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.chkCreate = New System.Windows.Forms.RadioButton()
        Me.chkDragDrop = New System.Windows.Forms.RadioButton()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.chkPreserve = New System.Windows.Forms.CheckBox()
        Me.chkRaw = New System.Windows.Forms.CheckBox()
        Me.tabControl1.SuspendLayout()
        Me.Filters.SuspendLayout()
        Me.cells.SuspendLayout()
        Me.tabselect.SuspendLayout()
        Me.tabThread.SuspendLayout()
        Me.grpThreads.SuspendLayout()
        CType(Me.maxthreads, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'thumbnailXpress1
        '
        Me.thumbnailXpress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.thumbnailXpress1.BottomMargin = 5
        Me.thumbnailXpress1.CellBorderColor = System.Drawing.Color.Blue
        Me.thumbnailXpress1.CellBorderWidth = 1
        Me.thumbnailXpress1.CellHeight = 80
        Me.thumbnailXpress1.CellHorizontalSpacing = 5
        Me.thumbnailXpress1.CellVerticalSpacing = 5
        Me.thumbnailXpress1.CellWidth = 80
        Me.thumbnailXpress1.Debug = False
        Me.thumbnailXpress1.DebugLogFile = "C:\DOCUME~1\dmartin\LOCALS~1\Temp\ThumbnailXpress1.log"
        Me.thumbnailXpress1.DescriptorAlignment = (PegasusImaging.WinForms.ThumbnailXpress1.DescriptorAlignments.AlignCenter Or PegasusImaging.WinForms.ThumbnailXpress1.DescriptorAlignments.AlignBottom)
        Me.thumbnailXpress1.DescriptorDisplayMethod = PegasusImaging.WinForms.ThumbnailXpress1.DescriptorDisplayMethods.Default
        Me.thumbnailXpress1.ErrorAction = PegasusImaging.WinForms.ThumbnailXpress1.ErrorAction.UseErrorIcon
        Me.thumbnailXpress1.ErrorLevel = PegasusImaging.WinForms.ThumbnailXpress1.DebugErrorLevel.Production
        Me.thumbnailXpress1.LeftMargin = 5
        Me.thumbnailXpress1.Location = New System.Drawing.Point(304, 152)
        Me.thumbnailXpress1.MaximumThumbnailBitDepth = 24
        Me.thumbnailXpress1.Name = "thumbnailXpress1"
        Me.thumbnailXpress1.RightMargin = 5
        Me.thumbnailXpress1.ScrollDirection = PegasusImaging.WinForms.ThumbnailXpress1.ScrollDirection.Vertical
        Me.thumbnailXpress1.SelectBackColor = System.Drawing.Color.FromArgb(CType(49, Byte), CType(106, Byte), CType(197, Byte))
        Me.thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.MultiExtended
        Me.thumbnailXpress1.ShowHourglass = True
        Me.thumbnailXpress1.ShowImagePlaceholders = False
        Me.thumbnailXpress1.Size = New System.Drawing.Size(520, 352)
        Me.thumbnailXpress1.TabIndex = 0
        Me.thumbnailXpress1.TopMargin = 5
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnufile, Me.mnuhelp})
        '
        'mnufile
        '
        Me.mnufile.Index = 0
        Me.mnufile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnudir, Me.mnusingle, Me.mnuexit})
        Me.mnufile.Text = "File"
        '
        'mnudir
        '
        Me.mnudir.Index = 0
        Me.mnudir.Text = "Open Directory of Images"
        '
        'mnusingle
        '
        Me.mnusingle.Index = 1
        Me.mnusingle.Text = "Open Image"
        '
        'mnuexit
        '
        Me.mnuexit.Index = 2
        Me.mnuexit.Text = "Exit"
        '
        'mnuhelp
        '
        Me.mnuhelp.Index = 1
        Me.mnuhelp.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuabout})
        Me.mnuhelp.Text = "Help"
        '
        'mnuabout
        '
        Me.mnuabout.Index = 0
        Me.mnuabout.Text = "About"
        '
        'chkexpand
        '
        Me.chkexpand.Location = New System.Drawing.Point(8, 8)
        Me.chkexpand.Name = "chkexpand"
        Me.chkexpand.Size = New System.Drawing.Size(120, 16)
        Me.chkexpand.TabIndex = 33
        Me.chkexpand.Text = "Expand Multipage"
        '
        'tabControl1
        '
        Me.tabControl1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Filters, Me.tabThread, Me.tabselect, Me.cells})
        Me.tabControl1.Location = New System.Drawing.Point(0, 88)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(296, 416)
        Me.tabControl1.TabIndex = 32
        '
        'Filters
        '
        Me.Filters.Controls.AddRange(New System.Windows.Forms.Control() {Me.txtsize, Me.lblsize, Me.dateTimePicker1, Me.chksub, Me.chkparent, Me.chkenable, Me.chkhid, Me.chksen, Me.chkdir, Me.label4, Me.lblcompare, Me.lbltype, Me.cmbchoice, Me.cmbfiles, Me.cmbcompare})
        Me.Filters.Location = New System.Drawing.Point(4, 22)
        Me.Filters.Name = "Filters"
        Me.Filters.Size = New System.Drawing.Size(288, 358)
        Me.Filters.TabIndex = 1
        Me.Filters.Text = "Filters"
        '
        'txtsize
        '
        Me.txtsize.Enabled = False
        Me.txtsize.Location = New System.Drawing.Point(96, 80)
        Me.txtsize.Name = "txtsize"
        Me.txtsize.Size = New System.Drawing.Size(144, 20)
        Me.txtsize.TabIndex = 16
        Me.txtsize.Text = ""
        Me.txtsize.Visible = False
        '
        'lblsize
        '
        Me.lblsize.Location = New System.Drawing.Point(8, 80)
        Me.lblsize.Name = "lblsize"
        Me.lblsize.Size = New System.Drawing.Size(72, 16)
        Me.lblsize.TabIndex = 15
        Me.lblsize.Text = "File Size"
        '
        'dateTimePicker1
        '
        Me.dateTimePicker1.Enabled = False
        Me.dateTimePicker1.Location = New System.Drawing.Point(48, 144)
        Me.dateTimePicker1.Name = "dateTimePicker1"
        Me.dateTimePicker1.Size = New System.Drawing.Size(208, 20)
        Me.dateTimePicker1.TabIndex = 14
        '
        'chksub
        '
        Me.chksub.Enabled = False
        Me.chksub.Location = New System.Drawing.Point(24, 296)
        Me.chksub.Name = "chksub"
        Me.chksub.Size = New System.Drawing.Size(128, 16)
        Me.chksub.TabIndex = 13
        Me.chksub.Text = "Include SubDir"
        '
        'chkparent
        '
        Me.chkparent.Enabled = False
        Me.chkparent.Location = New System.Drawing.Point(24, 272)
        Me.chkparent.Name = "chkparent"
        Me.chkparent.Size = New System.Drawing.Size(136, 16)
        Me.chkparent.TabIndex = 12
        Me.chkparent.Text = "Include Parent Dir"
        '
        'chkenable
        '
        Me.chkenable.Checked = True
        Me.chkenable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkenable.Location = New System.Drawing.Point(24, 248)
        Me.chkenable.Name = "chkenable"
        Me.chkenable.Size = New System.Drawing.Size(136, 16)
        Me.chkenable.TabIndex = 11
        Me.chkenable.Text = "Enabled"
        '
        'chkhid
        '
        Me.chkhid.Enabled = False
        Me.chkhid.Location = New System.Drawing.Point(24, 224)
        Me.chkhid.Name = "chkhid"
        Me.chkhid.Size = New System.Drawing.Size(144, 16)
        Me.chkhid.TabIndex = 10
        Me.chkhid.Text = "Include Hidden"
        '
        'chksen
        '
        Me.chksen.Enabled = False
        Me.chksen.Location = New System.Drawing.Point(24, 200)
        Me.chksen.Name = "chksen"
        Me.chksen.Size = New System.Drawing.Size(152, 16)
        Me.chksen.TabIndex = 9
        Me.chksen.Text = "Match is Case Sensitive"
        '
        'chkdir
        '
        Me.chkdir.Enabled = False
        Me.chkdir.Location = New System.Drawing.Point(24, 176)
        Me.chkdir.Name = "chkdir"
        Me.chkdir.Size = New System.Drawing.Size(160, 16)
        Me.chkdir.TabIndex = 8
        Me.chkdir.Text = "Match Applies to Directory"
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(8, 112)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(72, 16)
        Me.label4.TabIndex = 7
        Me.label4.Text = "File Filters:"
        '
        'lblcompare
        '
        Me.lblcompare.Location = New System.Drawing.Point(8, 48)
        Me.lblcompare.Name = "lblcompare"
        Me.lblcompare.Size = New System.Drawing.Size(72, 24)
        Me.lblcompare.TabIndex = 6
        Me.lblcompare.Text = "Compare Type:"
        '
        'lbltype
        '
        Me.lbltype.Location = New System.Drawing.Point(8, 16)
        Me.lbltype.Name = "lbltype"
        Me.lbltype.Size = New System.Drawing.Size(80, 16)
        Me.lbltype.TabIndex = 5
        Me.lbltype.Text = "Filter Type:"
        '
        'cmbchoice
        '
        Me.cmbchoice.Items.AddRange(New Object() {"File Name Match", "File Attributes and Directory", "File Creation Date", "File Modification Date", "FileSize", "ImagXpress Supported File"})
        Me.cmbchoice.Location = New System.Drawing.Point(96, 16)
        Me.cmbchoice.Name = "cmbchoice"
        Me.cmbchoice.Size = New System.Drawing.Size(144, 21)
        Me.cmbchoice.TabIndex = 4
        '
        'cmbfiles
        '
        Me.cmbfiles.Enabled = False
        Me.cmbfiles.Items.AddRange(New Object() {"All Files(*.*)", "Windows Bitmap (*.BMP)", "|CALS (*.CAL)", "Windows Device Independent Bitmap(*.DIB)", "MO:DCA (*.DCA & *.MOD)", "Zsoft Multiple Page (*.DCX)", "CompuServe GIF (*.GIF)", "JPEG 2000 (*.JP2)", "JPEG LS (*.JLS)", "JFIF Compliant JPEG (*.JPG)", "Lossless JPEG (*.LJP)", "Portable Bitmap (*.PBM)", "Zsoft PaintBrush (*.PCX)", "Portable Graymap (*.PGM)", "Pegasus PIC(*.PIC)", "Portable Network Graphics (*.PNG)", "Portable Pixmap (*.PPM)", "TIFF (*.TIFF)|", "Truevision TARGA(*.TGA)", "WSQ Fingerprint File (*.WSQ)", "JBIG2 File (*.JB2)"})
        Me.cmbfiles.Location = New System.Drawing.Point(96, 112)
        Me.cmbfiles.Name = "cmbfiles"
        Me.cmbfiles.Size = New System.Drawing.Size(152, 21)
        Me.cmbfiles.TabIndex = 3
        Me.cmbfiles.Text = "All Files(*.*)"
        '
        'cmbcompare
        '
        Me.cmbcompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbcompare.Enabled = False
        Me.cmbcompare.Items.AddRange(New Object() {"LessThan", "LessThan or Equal", "Equal", "GreaterThan or Equal", "GreaterThan"})
        Me.cmbcompare.Location = New System.Drawing.Point(96, 48)
        Me.cmbcompare.Name = "cmbcompare"
        Me.cmbcompare.Size = New System.Drawing.Size(144, 21)
        Me.cmbcompare.TabIndex = 0
        '
        'cells
        '
        Me.cells.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblCellWidth, Me.lblcw, Me.lblCVS, Me.lblv, Me.label3, Me.vcw, Me.vbh, Me.hbh, Me.lblCHS, Me.lblheight, Me.hbheight, Me.lblh, Me.lblw, Me.cmdcolor, Me.cmdset, Me.lblbwidth, Me.hbwidth})
        Me.cells.Location = New System.Drawing.Point(4, 22)
        Me.cells.Name = "cells"
        Me.cells.Size = New System.Drawing.Size(288, 358)
        Me.cells.TabIndex = 0
        Me.cells.Text = "Thumbnail Attributes"
        Me.cells.Visible = False
        '
        'lblCellWidth
        '
        Me.lblCellWidth.Location = New System.Drawing.Point(200, 272)
        Me.lblCellWidth.Name = "lblCellWidth"
        Me.lblCellWidth.Size = New System.Drawing.Size(56, 16)
        Me.lblCellWidth.TabIndex = 17
        '
        'lblcw
        '
        Me.lblcw.Location = New System.Drawing.Point(24, 248)
        Me.lblcw.Name = "lblcw"
        Me.lblcw.Size = New System.Drawing.Size(144, 16)
        Me.lblcw.TabIndex = 16
        Me.lblcw.Text = "Cell Width"
        '
        'lblCVS
        '
        Me.lblCVS.Location = New System.Drawing.Point(200, 224)
        Me.lblCVS.Name = "lblCVS"
        Me.lblCVS.Size = New System.Drawing.Size(56, 16)
        Me.lblCVS.TabIndex = 15
        '
        'lblv
        '
        Me.lblv.Location = New System.Drawing.Point(24, 200)
        Me.lblv.Name = "lblv"
        Me.lblv.Size = New System.Drawing.Size(160, 16)
        Me.lblv.TabIndex = 14
        Me.lblv.Text = "Cell Vertical Spacing"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(24, 152)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(128, 16)
        Me.label3.TabIndex = 13
        Me.label3.Text = "Cell Horizontal Spacing"
        '
        'vcw
        '
        Me.vcw.Location = New System.Drawing.Point(24, 272)
        Me.vcw.Minimum = 32
        Me.vcw.Name = "vcw"
        Me.vcw.Size = New System.Drawing.Size(160, 16)
        Me.vcw.TabIndex = 12
        Me.vcw.Value = 32
        '
        'vbh
        '
        Me.vbh.Location = New System.Drawing.Point(24, 224)
        Me.vbh.Maximum = 90
        Me.vbh.Minimum = 1
        Me.vbh.Name = "vbh"
        Me.vbh.Size = New System.Drawing.Size(160, 16)
        Me.vbh.TabIndex = 11
        Me.vbh.Value = 1
        '
        'hbh
        '
        Me.hbh.Location = New System.Drawing.Point(24, 176)
        Me.hbh.Maximum = 90
        Me.hbh.Minimum = 1
        Me.hbh.Name = "hbh"
        Me.hbh.Size = New System.Drawing.Size(160, 16)
        Me.hbh.TabIndex = 10
        Me.hbh.Value = 1
        '
        'lblCHS
        '
        Me.lblCHS.Location = New System.Drawing.Point(200, 176)
        Me.lblCHS.Name = "lblCHS"
        Me.lblCHS.Size = New System.Drawing.Size(56, 16)
        Me.lblCHS.TabIndex = 9
        '
        'lblheight
        '
        Me.lblheight.Location = New System.Drawing.Point(200, 136)
        Me.lblheight.Name = "lblheight"
        Me.lblheight.Size = New System.Drawing.Size(56, 16)
        Me.lblheight.TabIndex = 7
        '
        'hbheight
        '
        Me.hbheight.LargeChange = 2
        Me.hbheight.Location = New System.Drawing.Point(24, 128)
        Me.hbheight.Minimum = 32
        Me.hbheight.Name = "hbheight"
        Me.hbheight.Size = New System.Drawing.Size(160, 16)
        Me.hbheight.TabIndex = 6
        Me.hbheight.Value = 32
        '
        'lblh
        '
        Me.lblh.Location = New System.Drawing.Point(24, 104)
        Me.lblh.Name = "lblh"
        Me.lblh.Size = New System.Drawing.Size(104, 16)
        Me.lblh.TabIndex = 5
        Me.lblh.Text = "Cell Height"
        '
        'lblw
        '
        Me.lblw.Location = New System.Drawing.Point(24, 56)
        Me.lblw.Name = "lblw"
        Me.lblw.Size = New System.Drawing.Size(136, 16)
        Me.lblw.TabIndex = 4
        Me.lblw.Text = "CellBorderWidth "
        '
        'cmdcolor
        '
        Me.cmdcolor.Location = New System.Drawing.Point(24, 16)
        Me.cmdcolor.Name = "cmdcolor"
        Me.cmdcolor.Size = New System.Drawing.Size(168, 24)
        Me.cmdcolor.TabIndex = 3
        Me.cmdcolor.Text = "Choose Cell Color"
        '
        'cmdset
        '
        Me.cmdset.Location = New System.Drawing.Point(40, 320)
        Me.cmdset.Name = "cmdset"
        Me.cmdset.Size = New System.Drawing.Size(128, 24)
        Me.cmdset.TabIndex = 2
        Me.cmdset.Text = "Invoke Settings"
        '
        'lblbwidth
        '
        Me.lblbwidth.Location = New System.Drawing.Point(200, 80)
        Me.lblbwidth.Name = "lblbwidth"
        Me.lblbwidth.Size = New System.Drawing.Size(56, 24)
        Me.lblbwidth.TabIndex = 1
        '
        'hbwidth
        '
        Me.hbwidth.LargeChange = 2
        Me.hbwidth.Location = New System.Drawing.Point(24, 80)
        Me.hbwidth.Maximum = 20
        Me.hbwidth.Minimum = 1
        Me.hbwidth.Name = "hbwidth"
        Me.hbwidth.Size = New System.Drawing.Size(160, 16)
        Me.hbwidth.TabIndex = 0
        Me.hbwidth.Value = 1
        '
        'tabselect
        '
        Me.tabselect.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblselect, Me.cmbselect})
        Me.tabselect.Location = New System.Drawing.Point(4, 22)
        Me.tabselect.Name = "tabselect"
        Me.tabselect.Size = New System.Drawing.Size(288, 358)
        Me.tabselect.TabIndex = 3
        Me.tabselect.Text = "SelectionMode"
        Me.tabselect.Visible = False
        '
        'lblselect
        '
        Me.lblselect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblselect.Location = New System.Drawing.Point(72, 48)
        Me.lblselect.Name = "lblselect"
        Me.lblselect.Size = New System.Drawing.Size(128, 32)
        Me.lblselect.TabIndex = 1
        Me.lblselect.Text = "SelectionMode Options"
        Me.lblselect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmbselect
        '
        Me.cmbselect.Items.AddRange(New Object() {"MultiExtended", "MultiSimple", "None", "Single"})
        Me.cmbselect.Location = New System.Drawing.Point(24, 96)
        Me.cmbselect.Name = "cmbselect"
        Me.cmbselect.Size = New System.Drawing.Size(216, 21)
        Me.cmbselect.TabIndex = 0
        '
        'tabThread
        '
        Me.tabThread.Controls.AddRange(New System.Windows.Forms.Control() {Me.grpThreads, Me.lblThreads, Me.maxthreads})
        Me.tabThread.Location = New System.Drawing.Point(4, 22)
        Me.tabThread.Name = "tabThread"
        Me.tabThread.Size = New System.Drawing.Size(288, 390)
        Me.tabThread.TabIndex = 2
        Me.tabThread.Text = "Thread Processing"
        Me.tabThread.Visible = False
        '
        'grpThreads
        '
        Me.grpThreads.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdSetThreads, Me.txtHungT, Me.lblHung, Me.txtSThread, Me.Label1})
        Me.grpThreads.Location = New System.Drawing.Point(16, 72)
        Me.grpThreads.Name = "grpThreads"
        Me.grpThreads.Size = New System.Drawing.Size(248, 208)
        Me.grpThreads.TabIndex = 2
        Me.grpThreads.TabStop = False
        Me.grpThreads.Text = "Thread Time Thresholds"
        '
        'cmdSetThreads
        '
        Me.cmdSetThreads.Location = New System.Drawing.Point(128, 24)
        Me.cmdSetThreads.Name = "cmdSetThreads"
        Me.cmdSetThreads.Size = New System.Drawing.Size(96, 32)
        Me.cmdSetThreads.TabIndex = 4
        Me.cmdSetThreads.Text = "Set Thread Values"
        '
        'txtHungT
        '
        Me.txtHungT.Location = New System.Drawing.Point(128, 120)
        Me.txtHungT.Name = "txtHungT"
        Me.txtHungT.Size = New System.Drawing.Size(96, 20)
        Me.txtHungT.TabIndex = 3
        Me.txtHungT.Text = ""
        '
        'lblHung
        '
        Me.lblHung.Location = New System.Drawing.Point(8, 120)
        Me.lblHung.Name = "lblHung"
        Me.lblHung.Size = New System.Drawing.Size(96, 32)
        Me.lblHung.TabIndex = 2
        Me.lblHung.Text = "Hung Thread Threshold:"
        '
        'txtSThread
        '
        Me.txtSThread.Location = New System.Drawing.Point(128, 72)
        Me.txtSThread.Name = "txtSThread"
        Me.txtSThread.Size = New System.Drawing.Size(96, 20)
        Me.txtSThread.TabIndex = 1
        Me.txtSThread.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 32)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Start Thread Threshold:"
        '
        'lblThreads
        '
        Me.lblThreads.Location = New System.Drawing.Point(32, 24)
        Me.lblThreads.Name = "lblThreads"
        Me.lblThreads.Size = New System.Drawing.Size(112, 16)
        Me.lblThreads.TabIndex = 1
        Me.lblThreads.Text = "Max Threads:"
        '
        'maxthreads
        '
        Me.maxthreads.Location = New System.Drawing.Point(168, 24)
        Me.maxthreads.Maximum = New Decimal(New Integer() {33, 0, 0, 0})
        Me.maxthreads.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.maxthreads.Name = "maxthreads"
        Me.maxthreads.Size = New System.Drawing.Size(40, 20)
        Me.maxthreads.TabIndex = 0
        Me.maxthreads.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'errdesc
        '
        Me.errdesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.errdesc.Location = New System.Drawing.Point(600, 520)
        Me.errdesc.Name = "errdesc"
        Me.errdesc.Size = New System.Drawing.Size(136, 24)
        Me.errdesc.TabIndex = 31
        Me.errdesc.Text = "Last Error Reported:"
        Me.errdesc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblError
        '
        Me.lblError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblError.Location = New System.Drawing.Point(512, 552)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(312, 72)
        Me.lblError.TabIndex = 30
        '
        'sampdesc
        '
        Me.sampdesc.Location = New System.Drawing.Point(304, 0)
        Me.sampdesc.Name = "sampdesc"
        Me.sampdesc.Size = New System.Drawing.Size(520, 144)
        Me.sampdesc.TabIndex = 29
        Me.sampdesc.Text = "This sample demonstrates using the ThumbnailXpress .NET control in C#. The sample" & _
        " demonstrates:" & Microsoft.VisualBasic.ChrW(10) & "1)Using the various filters available in the control and the thre" & _
        "ading operations." & Microsoft.VisualBasic.ChrW(10) & "2)Adjusting the attributes of the Thumbnails in the view." & Microsoft.VisualBasic.ChrW(10) & "3)Re" & _
        "trieving information about individual Thumbnails in the view." & Microsoft.VisualBasic.ChrW(10) & "4)Using the DragDr" & _
        "op method to drag and drop thumbnails from one index to another." & Microsoft.VisualBasic.ChrW(10) & "5)Please note t" & _
        "hat the sample by default will open to the Pegasus Common Images directory via t" & _
        "he File menu or you can select the Enable External Drag Drop option to drag imag" & _
        "es in " & Microsoft.VisualBasic.ChrW(10) & "from an external location." & Microsoft.VisualBasic.ChrW(10) & "6)Setting various view properties via the Pres" & _
        "erveBlack and CameraRaw properties." & Microsoft.VisualBasic.ChrW(10) & "7)Viewing the original image in the ImagXpre" & _
        "ss control. This is accessed by double-clicking a thumbnail item."
        '
        'cmdremove
        '
        Me.cmdremove.Location = New System.Drawing.Point(408, 592)
        Me.cmdremove.Name = "cmdremove"
        Me.cmdremove.Size = New System.Drawing.Size(88, 32)
        Me.cmdremove.TabIndex = 27
        Me.cmdremove.Text = "Remove Selected Items"
        '
        'lstselect
        '
        Me.lstselect.Location = New System.Drawing.Point(168, 536)
        Me.lstselect.Name = "lstselect"
        Me.lstselect.Size = New System.Drawing.Size(224, 94)
        Me.lstselect.TabIndex = 26
        '
        'cmdsel
        '
        Me.cmdsel.Location = New System.Drawing.Point(408, 544)
        Me.cmdsel.Name = "cmdsel"
        Me.cmdsel.Size = New System.Drawing.Size(88, 32)
        Me.cmdsel.TabIndex = 25
        Me.cmdsel.Text = "List Selected Items"
        '
        'cmdclear
        '
        Me.cmdclear.Location = New System.Drawing.Point(24, 592)
        Me.cmdclear.Name = "cmdclear"
        Me.cmdclear.Size = New System.Drawing.Size(112, 32)
        Me.cmdclear.TabIndex = 23
        Me.cmdclear.Text = "Clear Images"
        '
        'chkCreate
        '
        Me.chkCreate.Checked = True
        Me.chkCreate.Location = New System.Drawing.Point(16, 512)
        Me.chkCreate.Name = "chkCreate"
        Me.chkCreate.Size = New System.Drawing.Size(128, 24)
        Me.chkCreate.TabIndex = 34
        Me.chkCreate.TabStop = True
        Me.chkCreate.Text = "External Drag/Drop"
        '
        'chkDragDrop
        '
        Me.chkDragDrop.Location = New System.Drawing.Point(16, 560)
        Me.chkDragDrop.Name = "chkDragDrop"
        Me.chkDragDrop.Size = New System.Drawing.Size(128, 24)
        Me.chkDragDrop.TabIndex = 35
        Me.chkDragDrop.Text = "Internal Drag/Drop"
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(136, 0)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(144, 32)
        Me.cmdCancel.TabIndex = 36
        Me.cmdCancel.Text = "Cancel Thumbnail Loading"
        '
        'chkPreserve
        '
        Me.chkPreserve.Location = New System.Drawing.Point(8, 40)
        Me.chkPreserve.Name = "chkPreserve"
        Me.chkPreserve.Size = New System.Drawing.Size(104, 32)
        Me.chkPreserve.TabIndex = 37
        Me.chkPreserve.Text = "PreserveBlack option"
        '
        'chkRaw
        '
        Me.chkRaw.Location = New System.Drawing.Point(128, 40)
        Me.chkRaw.Name = "chkRaw"
        Me.chkRaw.Size = New System.Drawing.Size(152, 32)
        Me.chkRaw.TabIndex = 38
        Me.chkRaw.Text = "CameraRaw option"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(834, 635)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.chkRaw, Me.chkPreserve, Me.cmdCancel, Me.chkDragDrop, Me.chkCreate, Me.chkexpand, Me.tabControl1, Me.errdesc, Me.lblError, Me.sampdesc, Me.cmdremove, Me.lstselect, Me.cmdsel, Me.cmdclear, Me.thumbnailXpress1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ThumbNailXpress VB.NET Viewer"
        Me.tabControl1.ResumeLayout(False)
        Me.Filters.ResumeLayout(False)
        Me.cells.ResumeLayout(False)
        Me.tabselect.ResumeLayout(False)
        Me.tabThread.ResumeLayout(False)
        Me.grpThreads.ResumeLayout(False)
        CType(Me.maxthreads, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'create the color dialog object here
        cd = New ColorDialog()

        Try
            '********MUST CALL UNLOCKRUNTIME at STARTUP*******
            'thumbnailXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
            'thumbnailXpress1.Licensing.UnlockIXRuntime(1234,1234,1234,1234)
            'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234

            'thread settings
            maxthreads.Value = thumbnailXpress1.MaximumThreadCount
            txtSThread.Text = thumbnailXpress1.ThreadStartThreshold.ToString()
            txtHungT.Text = thumbnailXpress1.ThreadHungThreshold.ToString()

            strCurrentDir = System.IO.Directory.GetCurrentDirectory()
            strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")

            thumbnailXpress1.DescriptorDisplayMethod = DescriptorDisplayMethods.ShortFilename
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)

        End Try
        lstselect.CheckOnClick = True

        'enable drag and drop in the control
        dragsource = True
        'set some control defaults
        Try
            thumbnailXpress1.Licensing.AccessPdfXpress()
        Catch eX As Exception
            PegasusError(New Exception("Unable to find PdfXpress. PDF abilities disabled."), lblError)
        End Try
        thumbnailXpress1.CameraRaw = False
        thumbnailXpress1.AllowDrop = True
        cmbcompare.SelectedIndex = 0
        cmbfiles.SelectedIndex = 0
        cmbchoice.SelectedIndex = 0
        cmbselect.SelectedIndex = 0
        hbwidth.Value = 1
        hbheight.Value = 65
        hbh.Value = 1
        vbh.Value = 1
        vcw.Value = 65
        lblbwidth.Text = hbwidth.Value.ToString
        lblheight.Text = hbheight.Value.ToString
        lblCHS.Text = hbh.Value.ToString
        lblCVS.Text = vbh.Value.ToString
        lblCellWidth.Text = vcw.Value.ToString

        'date time value set here
        dateTimePicker1.Value = System.DateTime.Now
    End Sub

        
    
    Private Sub cmbchoice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbchoice.SelectedIndexChanged
        Select Case cmbchoice.SelectedIndex
            Case 0
                chkenable.Enabled = True
                chkdir.Enabled = False
                chksen.Enabled = False
                chksub.Enabled = False
                chkparent.Enabled = False
                dateTimePicker1.Enabled = False
                cmbcompare.Enabled = False
                chkhid.Enabled = False
                ' break 
            Case 1
                chkdir.Enabled = True
                chkparent.Enabled = True
                chksub.Enabled = True
                chkparent.Enabled = True
                chkhid.Enabled = True
                chkdir.Enabled = False
                chksen.Enabled = False
                dateTimePicker1.Enabled = False
                cmbcompare.Enabled = False
                txtsize.Enabled = False
                ' break 
            Case 2
                chkenable.Enabled = True
                cmbcompare.Enabled = True
                dateTimePicker1.Enabled = True
                txtsize.Enabled = False
                chksub.Enabled = False
                chkparent.Enabled = False
                chkhid.Enabled = False
                ' break 
            Case 3
                chkenable.Enabled = True
                dateTimePicker1.Enabled = True
                cmbcompare.Enabled = True
                txtsize.Enabled = False
                chksub.Enabled = False
                chkparent.Enabled = False
                chkhid.Enabled = False
                ' break 
            Case 4
                chkenable.Enabled = True
                dateTimePicker1.Enabled = False
                txtsize.Visible = True
                txtsize.Enabled = True
                cmbcompare.Enabled = True
                chksub.Enabled = False
                chkparent.Enabled = False
                ' break 
            Case 5
                chkenable.Enabled = False
                dateTimePicker1.Enabled = False
                cmbcompare.Enabled = False
                chksen.Enabled = False
                chkdir.Enabled = False
                chkparent.Enabled = False
                chksub.Enabled = False
                chkhid.Enabled = False
                txtsize.Enabled = False
                ' break 
        End Select
    End Sub

    Private Sub cmbcompare_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbcompare.SelectedIndexChanged
        Select Case cmbcompare.SelectedIndex
            Case 0
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.LessThan
                ' break 
            Case 1
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.LessThanEquals
                ' break 
            Case 2
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.Equal
                ' break 
            Case 3
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.GreaterThanEquals
                ' break 
            Case 4
                filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.GreaterThan
                ' break 
        End Select
    End Sub


    Private Sub cmbfiles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbfiles.SelectedIndexChanged
        filteritem = thumbnailXpress1.CreateFilter("test")
        Select Case cmbfiles.SelectedIndex
            Case 0
                filteritem.FilenamePattern = "*.*"
                ' break 
            Case 1
                filteritem.FilenamePattern = "*.bmp"
                ' break 
            Case 2
                filteritem.FilenamePattern = "*.cal"
                ' break 
            Case 3
                filteritem.FilenamePattern = "*.dib"
                ' break 
            Case 4
                filteritem.FilenamePattern = "*.dca"
                ' break 
            Case 5
                filteritem.FilenamePattern = "*.mod"
                ' break 
            Case 6
                filteritem.FilenamePattern = "*.gif"
                ' break 
            Case 7
                filteritem.FilenamePattern = "*.jp2"
                ' break 
            Case 8
                filteritem.FilenamePattern = "*.jls"
                ' break 
            Case 9
                filteritem.FilenamePattern = "*.jpg"
                ' break 
            Case 10
                filteritem.FilenamePattern = "*.jls"
                ' break 
            Case 11
                filteritem.FilenamePattern = "*.pmm"
                ' break 
            Case 12
                filteritem.FilenamePattern = "*.pcx"
                ' break 
            Case 13
                filteritem.FilenamePattern = "*.pgm"
                ' break 
            Case 14
                filteritem.FilenamePattern = "*.pic"
                ' break 
            Case 15
                filteritem.FilenamePattern = "*.png"
                ' break 
            Case 16
                filteritem.FilenamePattern = "*.ppm"
                ' break 
            Case 17
                filteritem.FilenamePattern = "*.tif"
                ' break 
            Case 18
                filteritem.FilenamePattern = "*.tga"
                ' break 
            Case 19
                filteritem.FilenamePattern = "*.wsq"
                ' break 
            Case 20
                filteritem.FilenamePattern = "*.jb2"
                ' break 
            Case 21
                ' break 
        End Select
    End Sub

    Private Sub dateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dateTimePicker1.ValueChanged
        filteritem.FileDate = dateTimePicker1.Value
    End Sub

    Private Sub cmbselect_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbselect.SelectedIndexChanged
        Select Case cmbselect.SelectedIndex
            Case 0
                thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.MultiExtended
                ' break 
            Case 1
                thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.MultiSimple
                ' break 
            Case 2
                thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.None
                ' break 
            Case 3
                thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.Single
                ' break 
        End Select
    End Sub

    Private Sub cmdcolor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdcolor.Click
        cd.ShowDialog()
    End Sub

    Private Sub hbwidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hbwidth.Scroll
        lblbwidth.Text = hbwidth.Value.ToString
    End Sub

    Private Sub hbheight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hbheight.Scroll
        lblheight.Text = hbheight.Value.ToString
    End Sub

    Private Sub hbh_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hbh.Scroll
        lblCHS.Text = hbh.Value.ToString
    End Sub

    Private Sub vbh_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles vbh.Scroll
        lblCVS.Text = vbh.Value.ToString
    End Sub

    Private Sub vcw_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles vcw.Scroll
        lblCellWidth.Text = vcw.Value.ToString
    End Sub

    Private Sub cmdset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdset.Click
        thumbnailXpress1.BeginUpdate()
        thumbnailXpress1.CellBorderWidth = hbwidth.Value
        thumbnailXpress1.CellBorderColor = cd.Color

        thumbnailXpress1.CellHeight = hbheight.Value
        thumbnailXpress1.CellHorizontalSpacing = hbh.Value
        thumbnailXpress1.CellVerticalSpacing = vbh.Value
        thumbnailXpress1.CellWidth = vcw.Value
        thumbnailXpress1.EndUpdate()
    End Sub

    Private Sub cmdclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdclear.Click
        thumbnailXpress1.Items.Clear()
    End Sub

    Private Sub cmdsel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdsel.Click
        Try
            'get count of selected items
            lstselect.Items.Clear()
            a = thumbnailXpress1.SelectedItems.Count
            'create a array with the size of the selected TN's
            array = New PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem(a - 1) {}
            'copy items to array
            thumbnailXpress1.SelectedItems.CopyTo(array, 0)
            'add all the items to a list box
            lstselect.Items.AddRange(array)

        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try
    End Sub


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Private cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Private cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Private cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        lblError.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Private Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        lblError.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Private Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        lblError.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source
    End Sub

    Function PegasusOpenDir() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir

        If dlg.ShowDialog = DialogResult.OK Then

            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), dlg.FileName.Length - dlg.FileName.LastIndexOf("\"))

            Select Case dlg.FilterIndex
                Case 1
                    ' break 
                Case 2
                    filteritem.FilenamePattern = "*.bmp"
                    ' break 
                Case 3
                    filteritem.FilenamePattern = "*.cal"
                    ' break 
                Case 4
                    filteritem.FilenamePattern = "*.dib"
                    ' break 
                Case 5
                    filteritem.FilenamePattern = "*.dca"
                    ' break 
                Case 6
                    filteritem.FilenamePattern = "*.mod"
                    ' break 
                Case 7
                    filteritem.FilenamePattern = "*.gif"
                    ' break 
                Case 8
                    filteritem.FilenamePattern = "*.jp2"
                    ' break 
                Case 9
                    filteritem.FilenamePattern = "*.jls"
                    ' break 
                Case 10
                    filteritem.FilenamePattern = "*.jpg"
                    ' break 
                Case 11
                    filteritem.FilenamePattern = "*.jls"
                    ' break 
                Case 12
                    filteritem.FilenamePattern = "*.pmm"
                    ' break 
                Case 13
                    filteritem.FilenamePattern = "*.pcx"
                    ' break 
                Case 14
                    filteritem.FilenamePattern = "*.pgm"
                    ' break 
                Case 15
                    filteritem.FilenamePattern = "*.pic"
                    ' break 
                Case 16
                    filteritem.FilenamePattern = "*.png"
                    ' break 
                Case 17
                    filteritem.FilenamePattern = "*.ppm"
                    ' break 
                Case 18
                    filteritem.FilenamePattern = "*.tif"
                    ' break 
                Case 19
                    filteritem.FilenamePattern = "*.tga"
                    ' break 
                Case 20
                    filteritem.FilenamePattern = "*.wsq"
                    ' break 
                Case 21
                    filteritem.FilenamePattern = "*.jb2"
                    ' break 
                Case 22
                    ' break 
            End Select

            If chkenable.Checked = True Then
                thumbnailXpress1.Filters.Clear()
                filteritem.Type = CType(cmbchoice.SelectedIndex, FilterType)
                filteritem.Comparison = CType(cmbcompare.SelectedIndex, FilterComparison)
                filteritem.FileDate = dateTimePicker1.Value
                If Not (txtsize.Text = "") Then
                    filteritem.FileSize = System.Convert.ToInt32(txtsize.Text)
                End If
                filteritem.CaseSensitive = chkdir.Checked
                filteritem.MatchAppliesToDirectory = chkdir.Checked
                filteritem.IncludeHidden = chkhid.Checked
                filteritem.IncludeParentDirectory = chkparent.Checked
                filteritem.IncludeSubDirectory = chksub.Checked
                thumbnailXpress1.Filters.Add(filteritem)
                thumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\", 0)
            Else
                thumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\", 0)
            End If
            Return strCurrentDir
        Else

            Return ""
        End If
    End Function

    'Open single files here or multi-page TIFF Image
    Private Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If dlg.ShowDialog() = DialogResult.OK Then
            'strImageFile = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"),dlg.FileName.Length - dlg.FileName.LastIndexOf("\"));
            strImageFile = dlg.FileName
            If chkexpand.Checked = True Then
                thumbnailXpress1.Items.AddItemsFromFile(strImageFile, 0, True)
            Else
                thumbnailXpress1.Items.AddItemsFromFile(strImageFile, 0, False)
            End If
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
        Dim iTmp As System.Int32
        Try
            iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
        Catch ex As System.NullReferenceException
            PegasusError(ex, lblError)
            textTextBox.Text = scrScroll.Value.ToString(cultNumber)
            Return
        Catch ex As System.Exception
            PegasusError(ex, lblError)
            textTextBox.Text = scrScroll.Value.ToString(cultNumber)
            Return
        End Try
        If (iTmp < scrScroll.Maximum) And (iTmp > scrScroll.Minimum) Then
            scrScroll.Value = iTmp
        Else
            iTmp = scrScroll.Value
        End If
        textTextBox.Text = iTmp.ToString(cultNumber)
    End Sub

#End Region

    Private Sub thumbnailXpress1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles thumbnailXpress1.MouseDown

        Try

            'clear out the error before the next opertation
            lblError.Text = ""

            point = New System.Drawing.Point(e.X, e.Y)
            tdata = thumbnailXpress1.GetItemFromPoint(point)

            '    'get the data to drag to the other TN control
            If dragsource = False And e.Button = MouseButtons.Left Then

                'Check to make sure there are items that have been selected
                If Not tdata Is Nothing Then


                    indexOfItemUnderMouseToDrag = thumbnailXpress1.Items.IndexOf(tdata)
                    If indexOfItemUnderMouseToDrag >= 0 Then
                        'start the drag drop to the source and set the data to do a move
                        thumbnailXpress1.DoDragDrop(tdata, System.Windows.Forms.DragDropEffects.Move)

                    End If
                End If
                'get the info from the individual TN here via the right click 

            ElseIf (dragsource = True And e.Button = MouseButtons.Left And Not tdata Is Nothing) Then


            End If


        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

            PegasusError(ex, lblError)

        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub



    Private Sub thumbnailXpress1_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles thumbnailXpress1.DragEnter
        'start the drag/drop here
        Try
            e.Effect = DragDropEffects.Move
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub thumbnailXpress1_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles thumbnailXpress1.DragDrop
        Try
            'This will perform a drag and drop external from the control
            If dragsource = True Then

                Dim files() As String = CType(e.Data.GetData(DataFormats.FileDrop), String())
                'iterate through the files adding them to the display which demonstrates using the
                'CreateThumbnailItem method to progromatically add the TN's
                Dim count1 As Integer
                For count1 = 0 To files.Length - 1 Step count1 + 1
                    external1 = thumbnailXpress1.CreateThumbnailItem(files(count1), 1, PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.Unknown)
                    thumbnailXpress1.Items.Add(external1, True)
                Next

                '******This performs an internal drag and drag
            Else

                Dim pointin As System.Drawing.Point = New System.Drawing.Point(e.X, e.Y)
                Dim pointout As System.Drawing.Point
                'translate to client coordinates	
                pointout = thumbnailXpress1.PointToClient(pointin)
                dragtarget = thumbnailXpress1.GetItemFromPoint(pointout)
                If Not dragtarget Is Nothing Then
                    dragindex = thumbnailXpress1.Items.IndexOf(dragtarget)

                    'insert item here
                    thumbnailXpress1.Items.RemoveAt(indexOfItemUnderMouseToDrag)
                    thumbnailXpress1.Items.Insert(dragindex, tdata, False)
                End If
            End If
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

            PegasusError(ex, lblError)
            ' Least specific:
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub



    Private Sub mnudir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnudir.Click

        
        Try

            'clear out the error before the next opertation
            lblError.Text = ""

            'First we grab the filename of the image we want to open\
            'create a filter item here

            filteritem = thumbnailXpress1.CreateFilter("test")
            filteritemCreated = True
            Dim strloadres As System.String = PegasusOpenDir()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            PegasusError(ex, lblError)
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub mnusingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnusingle.Click
        Try

            'clear out the error before the next opertation
            lblError.Text = ""

            Dim strloadres As System.String = PegasusOpenFile()

        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

            PegasusError(ex, lblError)

        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub mnuabout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuabout.Click
        thumbnailXpress1.AboutBox()
    End Sub

    Private Sub mnuexit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuexit.Click
        Application.Exit()
    End Sub

    Private Sub cmdremove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdremove.Click
        Try
            Dim citem As Integer
            citem = lstselect.CheckedItems.Count

            Dim i As Integer
            For i = 0 To citem - 1 Step i + 1
                thumbnailXpress1.Items.Remove(array(i))
                'lstselect.Items.RemoveAt(i);
            Next
            'recopy the array
            'get count of selected items
            lstselect.Items.Clear()
            a = thumbnailXpress1.SelectedItems.Count
            'create a array with the size of the selected TN's
            array = New PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem(a - 1) {}
            'copy items to array
            thumbnailXpress1.SelectedItems.CopyTo(array, 0)
            'add all the items to a list box
            lstselect.Items.AddRange(array)
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

            PegasusError(ex, lblError)
            ' Least specific:
        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try

    End Sub



    Private Sub chkdir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkdir.CheckedChanged
        If chkdir.Checked = True Then
            filteritem.MatchAppliesToDirectory = True

        Else
            filteritem.MatchAppliesToDirectory = False

        End If

    End Sub

    Private Sub chksen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chksen.CheckedChanged
        If chkdir.Checked = True Then
            filteritem.CaseSensitive = True

        Else

            filteritem.CaseSensitive = False
        End If


    End Sub

    Private Sub chkhid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkhid.CheckedChanged
        If chkhid.Checked = True Then
            filteritem.IncludeHidden = True

        Else
            filteritem.IncludeHidden = False

        End If

    End Sub

    Private Sub chkenable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkenable.CheckedChanged

        'make sure filteritem has been initialized
        If filteritemCreated = True Then
            If chkenable.Checked = True Then
                filteritem.Enabled = True
                thumbnailXpress1.Filters.Clear()
                thumbnailXpress1.Filters.Add(filteritem)
            Else
                filteritem.Enabled = False

            End If
        End If
    End Sub

    Private Sub chkparent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkparent.CheckedChanged
        If chkenable.Checked = True Then
            filteritem.IncludeParentDirectory = True

        Else
            filteritem.IncludeParentDirectory = False

        End If

    End Sub

    Private Sub chksub_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chksub.CheckedChanged
        If chkenable.Checked = True Then
            filteritem.IncludeSubDirectory = True

        Else
            filteritem.IncludeSubDirectory = False

        End If

    End Sub

    Private Sub thumbnailXpress1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles thumbnailXpress1.DoubleClick
        Try

            'need to verify if we are on a folder etc.
            Dim info As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem
            info = thumbnailXpress1.GetItemFromPoint(point)

            If Not (info Is Nothing) Then

                If info.Type = PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.ParentDirectory Or info.Type = PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.SubDirectory Then

                    Dim tester As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem

                    Dim path As String = System.IO.Directory.GetCurrentDirectory()
                    Dim directoryEntries() As String = System.IO.Directory.GetDirectories(System.IO.Path.GetFullPath(info.Filename()))
                    Dim fileEntries() As String = Directory.GetFiles(System.IO.Path.GetFullPath(info.Filename()))


                    'clear the dislay
                    thumbnailXpress1.BeginUpdate()
                    thumbnailXpress1.Items.Clear()

                    Dim count As Integer

                    For count = 0 To directoryEntries.Length - 1 Step count + 1
                        'display all the directories in the selected folder

                        tester = thumbnailXpress1.CreateThumbnailItem(System.IO.Path.GetFullPath(directoryEntries(count)), 1, PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.Unknown)

                        thumbnailXpress1.Items.Add(tester, True)

                    Next


                    For count = 0 To fileEntries.Length - 1 Step count + 1
                        'display all the Files in the selected folder

                        tester = thumbnailXpress1.CreateThumbnailItem(System.IO.Path.GetFullPath(fileEntries(count)), 1, PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.Unknown)

                        'we don't want to add unsupported file types as we will get an exception
                        If tester.ErrorNumber = 0 And Not (tester.Type = ThumbnailType.Unsupported) Then
                            thumbnailXpress1.Items.Add(tester, True)

                        End If
                    Next

                    thumbnailXpress1.EndUpdate()

                Else
                    'implement the viewer via the Doubleclick event
                    tdata = thumbnailXpress1.GetItemFromPoint(point)
                    Dim viewer As New TNViewer()
                    viewer.AddImage(tdata.Filename)
                    viewer.ShowDialog(Me)

                    Application.DoEvents()

                End If
            End If
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

            PegasusError(ex, lblError)

        Catch ex As NullReferenceException
            PegasusError(ex, lblError)
        End Try
    End Sub



    Private Sub chkCreate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCreate.CheckedChanged
        'external drag and drop
        If chkCreate.Checked = True Then
            cmbselect.Enabled = True
            dragsource = True
        Else
            dragsource = False
        End If
    End Sub

    Private Sub chkDragDrop_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDragDrop.CheckedChanged
        'external drag and drop
        If chkDragDrop.Checked = True Then
            cmbselect.Enabled = False
            dragsource = False
        Else
            dragsource = True
        End If
    End Sub

    Private Sub tabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabControl1.SelectedIndexChanged

        Select Case tabControl1.SelectedIndex
            Case 0
                'internal
                If chkCreate.Checked = True Then
                    dragsource = True
                    chkDragDrop.Enabled = True
                Else
                    dragsource = False
                    chkDragDrop.Enabled = True
                End If
            Case 1
                'external/info tab
                dragsource = True
                chkDragDrop.Enabled = False
                chkCreate.Enabled = True
                chkCreate.Checked = True

            Case 2

                If chkCreate.Checked = True Then
                    dragsource = True
                    chkDragDrop.Enabled = True
                Else
                    dragsource = False
                    chkDragDrop.Enabled = True
                End If

            Case 3
                If chkCreate.Checked = True Then
                    dragsource = True
                    chkDragDrop.Enabled = True
                Else
                    dragsource = False
                    chkDragDrop.Enabled = True
                End If

        End Select
    End Sub


    Private Sub maxthreads_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maxthreads.ValueChanged
        Try
            thumbnailXpress1.MaximumThreadCount = maxthreads.Value

        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            PegasusError(ex, lblError)

        End Try
    End Sub

    Private Sub cmdSetThreads_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSetThreads.Click
        Try
            thumbnailXpress1.ThreadStartThreshold = System.Convert.ToInt32(txtSThread.Text)
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            txtSThread.Text = thumbnailXpress1.ThreadStartThreshold.ToString()
        End Try

        Try
            thumbnailXpress1.ThreadHungThreshold = System.Convert.ToInt32(txtHungT.Text)
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
            txtHungT.Text = thumbnailXpress1.ThreadHungThreshold.ToString()
        End Try

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Try
            thumbnailXpress1.Cancel()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException
        End Try
    End Sub

    Private Sub chkPreserve_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPreserve.CheckedChanged
        If chkPreserve.Checked = True Then
            thumbnailXpress1.PreserveBlack = True
        Else
            thumbnailXpress1.PreserveBlack = False
        End If
    End Sub

    Private Sub chkRaw_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRaw.CheckedChanged
        If chkRaw.Checked = True Then
            thumbnailXpress1.CameraRaw = True
        Else
            thumbnailXpress1.CameraRaw = False
        End If
    End Sub

    Private Sub cmdFlush_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            thumbnailXpress1.ThumbnailCache.CacheFlushMemory()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            thumbnailXpress1.ThumbnailCache.CacheClear()
        Catch ex As PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException

        End Try
    End Sub

    Private Sub tabCache_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub thumbnailXpress1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles thumbnailXpress1.Load

    End Sub

End Class

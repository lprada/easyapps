/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ThumbnailXpress1;
using PegasusImaging.WinForms.ImagXpress8;
using System.IO;
using System.Globalization;


namespace ThumbNailXpress
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmthumb : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem mnudir;
		private System.Windows.Forms.MenuItem mnuexit;
		private System.Windows.Forms.Button cmdclear;
		private System.Windows.Forms.MenuItem mnusingle;
		private PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpress thumbnailXpress1;
		private System.ComponentModel.IContainer components;

		//file I/O variables
		
		private System.String strImageFile;
		//General Variables for sample
		
		System.Int32 a;
		System.Int32 dragindex;
		System.Boolean dragsource;
		System.Drawing.Point point;

		System.String strCurrentDir;
		bool filteritemCreated;
		PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem tdata;
		PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem dragtarget;
		PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem[] array;
		//ThumbnailXpress Filteritem
		PegasusImaging.WinForms.ThumbnailXpress1.Filter filteritem;
		PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem externalsource;
				
		//drag and drop variables
		private System.Int32 indexOfItemUnderMouseToDrag;
		
		private System.Windows.Forms.Button cmdsel;
		private System.Windows.Forms.CheckedListBox lstselect;
		private System.Windows.Forms.Button cmdremove;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.Label errdesc;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage cells;
		private System.Windows.Forms.TabPage Filters;
		private System.Windows.Forms.Label lblbwidth;
		private System.Windows.Forms.HScrollBar hbwidth;
		private System.Windows.Forms.Button cmdset;
		private System.Windows.Forms.Button cmdcolor;
		private System.Windows.Forms.ColorDialog cd;
		private System.Windows.Forms.Label lblw;
		private System.Windows.Forms.Label lblh;
		private System.Windows.Forms.Label lblheight;
		private System.Windows.Forms.HScrollBar hbheight;
		private System.Windows.Forms.HScrollBar hbh;
		private System.Windows.Forms.ComboBox cmbcompare;
		private System.Windows.Forms.ComboBox cmbchoice;
		private System.Windows.Forms.Label lbltype;
		private System.Windows.Forms.Label lblcompare;
		private System.Windows.Forms.MenuItem mnuabout;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.CheckBox chksen;
		private System.Windows.Forms.CheckBox chkdir;
		private System.Windows.Forms.CheckBox chkhid;
		private System.Windows.Forms.CheckBox chkenable;
////		private System.Windows.Forms.CheckBox chkparent;
		private System.Windows.Forms.CheckBox chksub;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.Label lblsize;
		private System.Windows.Forms.TextBox txtsize;
//		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblv;
//		private System.Windows.Forms.HScrollBar vbh;
		private System.Windows.Forms.HScrollBar vcw;
		private System.Windows.Forms.Label lblcw;
		private System.Windows.Forms.CheckBox chkexpand;
		private System.Windows.Forms.TabPage tabselect;
		private System.Windows.Forms.ComboBox cmbselect;
		private System.Windows.Forms.Label lblselect;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.RichTextBox sampdesc;
		private System.Windows.Forms.Label lblCHS;
		private System.Windows.Forms.Label lblCVS;
		private System.Windows.Forms.Label lblCellWidth;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private System.Windows.Forms.RadioButton chkCreate;
		private System.Windows.Forms.CheckBox chkPreserveBlack;
		private System.Windows.Forms.CheckBox chkCameraRaw;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.TabPage tabProcessing;
		private System.Windows.Forms.NumericUpDown maxThreads;
		private System.Windows.Forms.Label lblThreads;
		private System.Windows.Forms.GroupBox grpThreads;
		private System.Windows.Forms.Button cmdSetThreads;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtSThread;
		private System.Windows.Forms.TextBox txtHungT;
		private System.Windows.Forms.CheckBox chkparent;
		private System.Windows.Forms.HScrollBar vbh;
		private System.Windows.Forms.Label lblcwidth;

		private System.Windows.Forms.RadioButton chkDragDrop;
		
	

		public frmthumb()
		{
			//
			// Required for Windows Form Designer support
			//

			//*******Must call the UnlockControl method(s) here
			//PegasusImaging.WinForms.ThumbnailXpress1.Licensing.UnlockControl(1234,1234,1234,1234);
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				if (thumbnailXpress1 != null) 
				{
					thumbnailXpress1.Dispose();
					thumbnailXpress1 = null;
				}

				if (imagXpress1 != null) 
				{
					imagXpress1 .Dispose();
					imagXpress1  = null;
				}

			

				if (components != null) 
				{
					components.Dispose();
				}
			
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.mnudir = new System.Windows.Forms.MenuItem();
			this.mnusingle = new System.Windows.Forms.MenuItem();
			this.mnuexit = new System.Windows.Forms.MenuItem();
			this.mnuabout = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.cmdclear = new System.Windows.Forms.Button();
			this.thumbnailXpress1 = new PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpress();
			this.cmdsel = new System.Windows.Forms.Button();
			this.lstselect = new System.Windows.Forms.CheckedListBox();
			this.cmdremove = new System.Windows.Forms.Button();
			this.sampdesc = new System.Windows.Forms.RichTextBox();
			this.lblError = new System.Windows.Forms.Label();
			this.errdesc = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.Filters = new System.Windows.Forms.TabPage();
			this.chkparent = new System.Windows.Forms.CheckBox();
			this.txtsize = new System.Windows.Forms.TextBox();
			this.lblsize = new System.Windows.Forms.Label();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.chksub = new System.Windows.Forms.CheckBox();
			this.chkenable = new System.Windows.Forms.CheckBox();
			this.chkhid = new System.Windows.Forms.CheckBox();
			this.chksen = new System.Windows.Forms.CheckBox();
			this.chkdir = new System.Windows.Forms.CheckBox();
			this.lblcompare = new System.Windows.Forms.Label();
			this.lbltype = new System.Windows.Forms.Label();
			this.cmbchoice = new System.Windows.Forms.ComboBox();
			this.cmbcompare = new System.Windows.Forms.ComboBox();
			this.cells = new System.Windows.Forms.TabPage();
			this.lblcwidth = new System.Windows.Forms.Label();
			this.vbh = new System.Windows.Forms.HScrollBar();
			this.lblCellWidth = new System.Windows.Forms.Label();
			this.lblcw = new System.Windows.Forms.Label();
			this.lblCVS = new System.Windows.Forms.Label();
			this.lblv = new System.Windows.Forms.Label();
			this.vcw = new System.Windows.Forms.HScrollBar();
			this.hbh = new System.Windows.Forms.HScrollBar();
			this.lblCHS = new System.Windows.Forms.Label();
			this.lblheight = new System.Windows.Forms.Label();
			this.hbheight = new System.Windows.Forms.HScrollBar();
			this.lblh = new System.Windows.Forms.Label();
			this.lblw = new System.Windows.Forms.Label();
			this.cmdcolor = new System.Windows.Forms.Button();
			this.cmdset = new System.Windows.Forms.Button();
			this.lblbwidth = new System.Windows.Forms.Label();
			this.hbwidth = new System.Windows.Forms.HScrollBar();
			this.tabProcessing = new System.Windows.Forms.TabPage();
			this.grpThreads = new System.Windows.Forms.GroupBox();
			this.txtHungT = new System.Windows.Forms.TextBox();
			this.txtSThread = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cmdSetThreads = new System.Windows.Forms.Button();
			this.lblThreads = new System.Windows.Forms.Label();
			this.maxThreads = new System.Windows.Forms.NumericUpDown();
			this.tabselect = new System.Windows.Forms.TabPage();
			this.lblselect = new System.Windows.Forms.Label();
			this.cmbselect = new System.Windows.Forms.ComboBox();
			this.cd = new System.Windows.Forms.ColorDialog();
			this.chkexpand = new System.Windows.Forms.CheckBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.chkCreate = new System.Windows.Forms.RadioButton();
			this.chkDragDrop = new System.Windows.Forms.RadioButton();
			this.chkPreserveBlack = new System.Windows.Forms.CheckBox();
			this.chkCameraRaw = new System.Windows.Forms.CheckBox();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.Filters.SuspendLayout();
			this.cells.SuspendLayout();
			this.tabProcessing.SuspendLayout();
			this.grpThreads.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.maxThreads)).BeginInit();
			this.tabselect.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.mnuabout});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnudir,
																					  this.mnusingle,
																					  this.mnuexit});
			this.menuItem1.Text = "File";
			// 
			// mnudir
			// 
			this.mnudir.Index = 0;
			this.mnudir.Text = "Open Directory of Images";
			this.mnudir.Click += new System.EventHandler(this.mnudir_Click);
			// 
			// mnusingle
			// 
			this.mnusingle.Index = 1;
			this.mnusingle.Text = "Open Image";
			this.mnusingle.Click += new System.EventHandler(this.mnusingle_Click_1);
			// 
			// mnuexit
			// 
			this.mnuexit.Index = 2;
			this.mnuexit.Text = "Exit";
			this.mnuexit.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// mnuabout
			// 
			this.mnuabout.Index = 1;
			this.mnuabout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuItem2});
			this.mnuabout.Text = "Help";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "About";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// cmdclear
			// 
			this.cmdclear.Location = new System.Drawing.Point(32, 544);
			this.cmdclear.Name = "cmdclear";
			this.cmdclear.Size = new System.Drawing.Size(112, 24);
			this.cmdclear.TabIndex = 1;
			this.cmdclear.Text = "Clear Images";
			this.cmdclear.Click += new System.EventHandler(this.cmdclear_Click);
			// 
			// thumbnailXpress1
			// 
			this.thumbnailXpress1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.thumbnailXpress1.BottomMargin = 5;
			this.thumbnailXpress1.CellBorderColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.thumbnailXpress1.CellBorderWidth = 2;
			this.thumbnailXpress1.CellHeight = 80;
			this.thumbnailXpress1.CellHorizontalSpacing = 5;
			this.thumbnailXpress1.CellVerticalSpacing = 5;
			this.thumbnailXpress1.CellWidth = 80;
			this.thumbnailXpress1.Debug = false;
			this.thumbnailXpress1.DebugLogFile = "C:\\DOCUME~1\\ryoung\\LOCALS~1\\Temp\\ThumbnailXpress1.log";
			this.thumbnailXpress1.DescriptorAlignment = (PegasusImaging.WinForms.ThumbnailXpress1.DescriptorAlignments.AlignCenter | PegasusImaging.WinForms.ThumbnailXpress1.DescriptorAlignments.AlignBottom);
			this.thumbnailXpress1.DescriptorDisplayMethod = PegasusImaging.WinForms.ThumbnailXpress1.DescriptorDisplayMethods.None;
			this.thumbnailXpress1.ErrorAction = PegasusImaging.WinForms.ThumbnailXpress1.ErrorAction.UseErrorIcon;
			this.thumbnailXpress1.ErrorLevel = PegasusImaging.WinForms.ThumbnailXpress1.DebugErrorLevel.Production;
			this.thumbnailXpress1.LeftMargin = 5;
			this.thumbnailXpress1.Location = new System.Drawing.Point(344, 160);
			this.thumbnailXpress1.MaximumThumbnailBitDepth = 24;
			this.thumbnailXpress1.Name = "thumbnailXpress1";
			this.thumbnailXpress1.RightMargin = 5;
			this.thumbnailXpress1.ScrollDirection = PegasusImaging.WinForms.ThumbnailXpress1.ScrollDirection.Vertical;
			this.thumbnailXpress1.SelectBackColor = System.Drawing.Color.FromArgb(((System.Byte)(10)), ((System.Byte)(36)), ((System.Byte)(106)));
			this.thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.MultiExtended;
			this.thumbnailXpress1.ShowHourglass = true;
			this.thumbnailXpress1.ShowImagePlaceholders = false;
			this.thumbnailXpress1.Size = new System.Drawing.Size(512, 304);
			this.thumbnailXpress1.TabIndex = 23;
			this.thumbnailXpress1.TopMargin = 5;
			this.thumbnailXpress1.DragEnter += new System.Windows.Forms.DragEventHandler(this.thumbnailXpress1_DragEnter);
			this.thumbnailXpress1.DragDrop += new System.Windows.Forms.DragEventHandler(this.thumbnailXpress1_DragDrop);
			this.thumbnailXpress1.DoubleClick += new System.EventHandler(this.thumbnailXpress1_DoubleClick);
			this.thumbnailXpress1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.thumbnailXpress1_MouseDown);
			// 
			// cmdsel
			// 
			this.cmdsel.Location = new System.Drawing.Point(416, 496);
			this.cmdsel.Name = "cmdsel";
			this.cmdsel.Size = new System.Drawing.Size(88, 32);
			this.cmdsel.TabIndex = 9;
			this.cmdsel.Text = "List Selected Items";
			this.cmdsel.Click += new System.EventHandler(this.cmdsel_Click);
			// 
			// lstselect
			// 
			this.lstselect.Location = new System.Drawing.Point(184, 480);
			this.lstselect.Name = "lstselect";
			this.lstselect.Size = new System.Drawing.Size(224, 79);
			this.lstselect.TabIndex = 11;
			// 
			// cmdremove
			// 
			this.cmdremove.Location = new System.Drawing.Point(416, 536);
			this.cmdremove.Name = "cmdremove";
			this.cmdremove.Size = new System.Drawing.Size(88, 32);
			this.cmdremove.TabIndex = 12;
			this.cmdremove.Text = "Remove Selected Items";
			this.cmdremove.Click += new System.EventHandler(this.cmdremove_Click);
			// 
			// sampdesc
			// 
			this.sampdesc.Location = new System.Drawing.Point(344, 0);
			this.sampdesc.Name = "sampdesc";
			this.sampdesc.Size = new System.Drawing.Size(512, 152);
			this.sampdesc.TabIndex = 16;
			this.sampdesc.Text = @"This sample demonstrates using the ThumbnailXpress .NET control in C#. The sample demonstrates:
1)Using the various filters available in the control and the threading operations.
2)Adjusting the attributes of the Thumbnails in the view.
3)Retrieving information about individual Thumbnails in the view.
4)Using the DragDrop method to drag and drop thumbnails from one index to another.
5)Please note that the sample by default will open to the Pegasus Common Images directory via the File menu or you can select the Enable External Drag Drop option to drag images in 
from an external location.
6)Setting various view properties via the PreserveBlack and CameraRaw properties.
7)Viewing the original image in the ImagXpress control. This is accessed by double-clicking a thumbnail item.";
			// 
			// lblError
			// 
			this.lblError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblError.Location = new System.Drawing.Point(536, 528);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(288, 48);
			this.lblError.TabIndex = 17;
			// 
			// errdesc
			// 
			this.errdesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.errdesc.Location = new System.Drawing.Point(584, 488);
			this.errdesc.Name = "errdesc";
			this.errdesc.Size = new System.Drawing.Size(136, 24);
			this.errdesc.TabIndex = 18;
			this.errdesc.Text = "Last Error Reported:";
			this.errdesc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.Filters,
																					  this.cells,
																					  this.tabProcessing,
																					  this.tabselect});
			this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tabControl1.Location = new System.Drawing.Point(8, 96);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(328, 360);
			this.tabControl1.TabIndex = 21;
			this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
			// 
			// Filters
			// 
			this.Filters.Controls.AddRange(new System.Windows.Forms.Control[] {
																				  this.chkparent,
																				  this.txtsize,
																				  this.lblsize,
																				  this.dateTimePicker1,
																				  this.chksub,
																				  this.chkenable,
																				  this.chkhid,
																				  this.chksen,
																				  this.chkdir,
																				  this.lblcompare,
																				  this.lbltype,
																				  this.cmbchoice,
																				  this.cmbcompare});
			this.Filters.Location = new System.Drawing.Point(4, 22);
			this.Filters.Name = "Filters";
			this.Filters.Size = new System.Drawing.Size(320, 334);
			this.Filters.TabIndex = 1;
			this.Filters.Text = "Filters";
			// 
			// chkparent
			// 
			this.chkparent.Location = new System.Drawing.Point(24, 272);
			this.chkparent.Name = "chkparent";
			this.chkparent.Size = new System.Drawing.Size(136, 16);
			this.chkparent.TabIndex = 17;
			this.chkparent.Text = "Include Parent Dir";
			// 
			// txtsize
			// 
			this.txtsize.Enabled = false;
			this.txtsize.Location = new System.Drawing.Point(112, 80);
			this.txtsize.Name = "txtsize";
			this.txtsize.Size = new System.Drawing.Size(144, 20);
			this.txtsize.TabIndex = 16;
			this.txtsize.Text = "";
			this.txtsize.Visible = false;
			// 
			// lblsize
			// 
			this.lblsize.Location = new System.Drawing.Point(8, 80);
			this.lblsize.Name = "lblsize";
			this.lblsize.Size = new System.Drawing.Size(96, 16);
			this.lblsize.TabIndex = 15;
			this.lblsize.Text = "File Size(in bytes)";
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Enabled = false;
			this.dateTimePicker1.Location = new System.Drawing.Point(48, 120);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(208, 20);
			this.dateTimePicker1.TabIndex = 14;
			// 
			// chksub
			// 
			this.chksub.Enabled = false;
			this.chksub.Location = new System.Drawing.Point(24, 304);
			this.chksub.Name = "chksub";
			this.chksub.Size = new System.Drawing.Size(128, 16);
			this.chksub.TabIndex = 13;
			this.chksub.Text = "Include SubDir";
			// 
			// chkenable
			// 
			this.chkenable.Checked = true;
			this.chkenable.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkenable.Location = new System.Drawing.Point(24, 248);
			this.chkenable.Name = "chkenable";
			this.chkenable.Size = new System.Drawing.Size(136, 16);
			this.chkenable.TabIndex = 11;
			this.chkenable.Text = "Enabled";
			this.chkenable.CheckedChanged += new System.EventHandler(this.chkenable_CheckedChanged);
			// 
			// chkhid
			// 
			this.chkhid.Enabled = false;
			this.chkhid.Location = new System.Drawing.Point(24, 224);
			this.chkhid.Name = "chkhid";
			this.chkhid.Size = new System.Drawing.Size(144, 16);
			this.chkhid.TabIndex = 10;
			this.chkhid.Text = "Include Hidden";
			// 
			// chksen
			// 
			this.chksen.Enabled = false;
			this.chksen.Location = new System.Drawing.Point(24, 200);
			this.chksen.Name = "chksen";
			this.chksen.Size = new System.Drawing.Size(152, 16);
			this.chksen.TabIndex = 9;
			this.chksen.Text = "Match is Case Sensitive";
			// 
			// chkdir
			// 
			this.chkdir.Enabled = false;
			this.chkdir.Location = new System.Drawing.Point(24, 176);
			this.chkdir.Name = "chkdir";
			this.chkdir.Size = new System.Drawing.Size(160, 16);
			this.chkdir.TabIndex = 8;
			this.chkdir.Text = "Match Applies to Directory";
			this.chkdir.CheckedChanged += new System.EventHandler(this.chkdir_CheckedChanged_1);
			// 
			// lblcompare
			// 
			this.lblcompare.Location = new System.Drawing.Point(8, 40);
			this.lblcompare.Name = "lblcompare";
			this.lblcompare.Size = new System.Drawing.Size(72, 32);
			this.lblcompare.TabIndex = 6;
			this.lblcompare.Text = "Compare Type:";
			// 
			// lbltype
			// 
			this.lbltype.Location = new System.Drawing.Point(8, 16);
			this.lbltype.Name = "lbltype";
			this.lbltype.Size = new System.Drawing.Size(80, 16);
			this.lbltype.TabIndex = 5;
			this.lbltype.Text = "Filter Type:";
			// 
			// cmbchoice
			// 
			this.cmbchoice.Items.AddRange(new object[] {
														   "File Name Match",
														   "File Attributes and Directory",
														   "File Creation Date",
														   "File Modification Date",
														   "FileSize",
														   "ImagXpress Supported File"});
			this.cmbchoice.Location = new System.Drawing.Point(96, 16);
			this.cmbchoice.Name = "cmbchoice";
			this.cmbchoice.Size = new System.Drawing.Size(144, 21);
			this.cmbchoice.TabIndex = 4;
			this.cmbchoice.SelectedIndexChanged += new System.EventHandler(this.cmbchoice_SelectedIndexChanged_1);
			// 
			// cmbcompare
			// 
			this.cmbcompare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbcompare.Enabled = false;
			this.cmbcompare.Items.AddRange(new object[] {
															"LessThan",
															"LessThan or Equal",
															"Equal",
															"GreaterThan or Equal",
															"GreaterThan"});
			this.cmbcompare.Location = new System.Drawing.Point(96, 48);
			this.cmbcompare.Name = "cmbcompare";
			this.cmbcompare.Size = new System.Drawing.Size(144, 21);
			this.cmbcompare.TabIndex = 0;
			// 
			// cells
			// 
			this.cells.Controls.AddRange(new System.Windows.Forms.Control[] {
																				this.lblcwidth,
																				this.vbh,
																				this.lblCellWidth,
																				this.lblcw,
																				this.lblCVS,
																				this.lblv,
																				this.vcw,
																				this.hbh,
																				this.lblCHS,
																				this.lblheight,
																				this.hbheight,
																				this.lblh,
																				this.lblw,
																				this.cmdcolor,
																				this.cmdset,
																				this.lblbwidth,
																				this.hbwidth});
			this.cells.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cells.Location = new System.Drawing.Point(4, 22);
			this.cells.Name = "cells";
			this.cells.Size = new System.Drawing.Size(320, 334);
			this.cells.TabIndex = 0;
			this.cells.Text = "Thumbnail Attributes";
			// 
			// lblcwidth
			// 
			this.lblcwidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblcwidth.Location = new System.Drawing.Point(24, 152);
			this.lblcwidth.Name = "lblcwidth";
			this.lblcwidth.Size = new System.Drawing.Size(136, 16);
			this.lblcwidth.TabIndex = 19;
			this.lblcwidth.Text = "Cell Horizontal Spacing";
			// 
			// vbh
			// 
			this.vbh.Location = new System.Drawing.Point(24, 224);
			this.vbh.Name = "vbh";
			this.vbh.Size = new System.Drawing.Size(160, 16);
			this.vbh.TabIndex = 18;
			this.vbh.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vbh_Scroll);
			// 
			// lblCellWidth
			// 
			this.lblCellWidth.Location = new System.Drawing.Point(200, 272);
			this.lblCellWidth.Name = "lblCellWidth";
			this.lblCellWidth.Size = new System.Drawing.Size(56, 16);
			this.lblCellWidth.TabIndex = 17;
			// 
			// lblcw
			// 
			this.lblcw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblcw.Location = new System.Drawing.Point(24, 248);
			this.lblcw.Name = "lblcw";
			this.lblcw.Size = new System.Drawing.Size(144, 16);
			this.lblcw.TabIndex = 16;
			this.lblcw.Text = "Cell Width";
			// 
			// lblCVS
			// 
			this.lblCVS.Location = new System.Drawing.Point(200, 224);
			this.lblCVS.Name = "lblCVS";
			this.lblCVS.Size = new System.Drawing.Size(56, 16);
			this.lblCVS.TabIndex = 15;
			// 
			// lblv
			// 
			this.lblv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblv.Location = new System.Drawing.Point(24, 200);
			this.lblv.Name = "lblv";
			this.lblv.Size = new System.Drawing.Size(160, 16);
			this.lblv.TabIndex = 14;
			this.lblv.Text = "Cell Vertical Spacing";
			// 
			// vcw
			// 
			this.vcw.Location = new System.Drawing.Point(24, 272);
			this.vcw.Minimum = 32;
			this.vcw.Name = "vcw";
			this.vcw.Size = new System.Drawing.Size(160, 16);
			this.vcw.TabIndex = 12;
			this.vcw.Value = 32;
			this.vcw.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vcw_Scroll);
			// 
			// hbh
			// 
			this.hbh.Location = new System.Drawing.Point(24, 176);
			this.hbh.Maximum = 90;
			this.hbh.Minimum = 1;
			this.hbh.Name = "hbh";
			this.hbh.Size = new System.Drawing.Size(160, 16);
			this.hbh.TabIndex = 10;
			this.hbh.Value = 1;
			this.hbh.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hbh_Scroll);
			// 
			// lblCHS
			// 
			this.lblCHS.Location = new System.Drawing.Point(200, 176);
			this.lblCHS.Name = "lblCHS";
			this.lblCHS.Size = new System.Drawing.Size(56, 16);
			this.lblCHS.TabIndex = 9;
			// 
			// lblheight
			// 
			this.lblheight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblheight.Location = new System.Drawing.Point(200, 136);
			this.lblheight.Name = "lblheight";
			this.lblheight.Size = new System.Drawing.Size(56, 16);
			this.lblheight.TabIndex = 7;
			// 
			// hbheight
			// 
			this.hbheight.LargeChange = 2;
			this.hbheight.Location = new System.Drawing.Point(24, 128);
			this.hbheight.Minimum = 32;
			this.hbheight.Name = "hbheight";
			this.hbheight.Size = new System.Drawing.Size(160, 16);
			this.hbheight.TabIndex = 6;
			this.hbheight.Value = 32;
			this.hbheight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hbheight_Scroll);
			// 
			// lblh
			// 
			this.lblh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblh.Location = new System.Drawing.Point(24, 104);
			this.lblh.Name = "lblh";
			this.lblh.Size = new System.Drawing.Size(104, 16);
			this.lblh.TabIndex = 5;
			this.lblh.Text = "Cell Height";
			// 
			// lblw
			// 
			this.lblw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblw.Location = new System.Drawing.Point(24, 56);
			this.lblw.Name = "lblw";
			this.lblw.Size = new System.Drawing.Size(136, 16);
			this.lblw.TabIndex = 4;
			this.lblw.Text = "CellBorderWidth ";
			// 
			// cmdcolor
			// 
			this.cmdcolor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdcolor.Location = new System.Drawing.Point(24, 16);
			this.cmdcolor.Name = "cmdcolor";
			this.cmdcolor.Size = new System.Drawing.Size(168, 24);
			this.cmdcolor.TabIndex = 3;
			this.cmdcolor.Text = "Choose Cell Color";
			this.cmdcolor.Click += new System.EventHandler(this.cmdcolor_Click);
			// 
			// cmdset
			// 
			this.cmdset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdset.Location = new System.Drawing.Point(64, 296);
			this.cmdset.Name = "cmdset";
			this.cmdset.Size = new System.Drawing.Size(128, 24);
			this.cmdset.TabIndex = 2;
			this.cmdset.Text = "Invoke Settings";
			this.cmdset.Click += new System.EventHandler(this.cmdset_Click);
			// 
			// lblbwidth
			// 
			this.lblbwidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblbwidth.Location = new System.Drawing.Point(200, 80);
			this.lblbwidth.Name = "lblbwidth";
			this.lblbwidth.Size = new System.Drawing.Size(56, 24);
			this.lblbwidth.TabIndex = 1;
			// 
			// hbwidth
			// 
			this.hbwidth.LargeChange = 2;
			this.hbwidth.Location = new System.Drawing.Point(24, 80);
			this.hbwidth.Maximum = 20;
			this.hbwidth.Minimum = 1;
			this.hbwidth.Name = "hbwidth";
			this.hbwidth.Size = new System.Drawing.Size(160, 16);
			this.hbwidth.TabIndex = 0;
			this.hbwidth.Value = 1;
			this.hbwidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hbwidth_Scroll);
			// 
			// tabProcessing
			// 
			this.tabProcessing.Controls.AddRange(new System.Windows.Forms.Control[] {
																						this.grpThreads,
																						this.lblThreads,
																						this.maxThreads});
			this.tabProcessing.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tabProcessing.Location = new System.Drawing.Point(4, 22);
			this.tabProcessing.Name = "tabProcessing";
			this.tabProcessing.Size = new System.Drawing.Size(320, 334);
			this.tabProcessing.TabIndex = 4;
			this.tabProcessing.Text = "Thread Processing";
			// 
			// grpThreads
			// 
			this.grpThreads.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.txtHungT,
																					 this.txtSThread,
																					 this.label2,
																					 this.label1,
																					 this.cmdSetThreads});
			this.grpThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grpThreads.Location = new System.Drawing.Point(16, 56);
			this.grpThreads.Name = "grpThreads";
			this.grpThreads.Size = new System.Drawing.Size(240, 152);
			this.grpThreads.TabIndex = 2;
			this.grpThreads.TabStop = false;
			this.grpThreads.Text = "Thread Time Thresholds";
			this.grpThreads.Enter += new System.EventHandler(this.grpThreads_Enter);
			// 
			// txtHungT
			// 
			this.txtHungT.Location = new System.Drawing.Point(152, 88);
			this.txtHungT.Name = "txtHungT";
			this.txtHungT.Size = new System.Drawing.Size(64, 20);
			this.txtHungT.TabIndex = 4;
			this.txtHungT.Text = "";
			// 
			// txtSThread
			// 
			this.txtSThread.Location = new System.Drawing.Point(152, 56);
			this.txtSThread.Name = "txtSThread";
			this.txtSThread.Size = new System.Drawing.Size(64, 20);
			this.txtSThread.TabIndex = 3;
			this.txtSThread.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 88);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(112, 32);
			this.label2.TabIndex = 2;
			this.label2.Text = "Hung Thread Threshold";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Start Thread Threshold";
			// 
			// cmdSetThreads
			// 
			this.cmdSetThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdSetThreads.Location = new System.Drawing.Point(128, 16);
			this.cmdSetThreads.Name = "cmdSetThreads";
			this.cmdSetThreads.Size = new System.Drawing.Size(104, 32);
			this.cmdSetThreads.TabIndex = 0;
			this.cmdSetThreads.Text = "Set Thread Values";
			this.cmdSetThreads.Click += new System.EventHandler(this.cmdSetThreads_Click);
			// 
			// lblThreads
			// 
			this.lblThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblThreads.Location = new System.Drawing.Point(24, 16);
			this.lblThreads.Name = "lblThreads";
			this.lblThreads.Size = new System.Drawing.Size(80, 16);
			this.lblThreads.TabIndex = 1;
			this.lblThreads.Text = "Max Threads:";
			this.lblThreads.Click += new System.EventHandler(this.lblThreads_Click);
			// 
			// maxThreads
			// 
			this.maxThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.maxThreads.Location = new System.Drawing.Point(128, 16);
			this.maxThreads.Maximum = new System.Decimal(new int[] {
																	   33,
																	   0,
																	   0,
																	   0});
			this.maxThreads.Minimum = new System.Decimal(new int[] {
																	   1,
																	   0,
																	   0,
																	   0});
			this.maxThreads.Name = "maxThreads";
			this.maxThreads.Size = new System.Drawing.Size(48, 20);
			this.maxThreads.TabIndex = 0;
			this.maxThreads.Value = new System.Decimal(new int[] {
																	 1,
																	 0,
																	 0,
																	 0});
			this.maxThreads.ValueChanged += new System.EventHandler(this.maxThreads_ValueChanged);
			// 
			// tabselect
			// 
			this.tabselect.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.lblselect,
																					this.cmbselect});
			this.tabselect.Location = new System.Drawing.Point(4, 22);
			this.tabselect.Name = "tabselect";
			this.tabselect.Size = new System.Drawing.Size(320, 334);
			this.tabselect.TabIndex = 3;
			this.tabselect.Text = "SelectionMode";
			// 
			// lblselect
			// 
			this.lblselect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblselect.Location = new System.Drawing.Point(72, 48);
			this.lblselect.Name = "lblselect";
			this.lblselect.Size = new System.Drawing.Size(128, 32);
			this.lblselect.TabIndex = 1;
			this.lblselect.Text = "SelectionMode Options";
			this.lblselect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cmbselect
			// 
			this.cmbselect.Items.AddRange(new object[] {
														   "MultiExtended",
														   "MultiSimple",
														   "None",
														   "Single"});
			this.cmbselect.Location = new System.Drawing.Point(24, 96);
			this.cmbselect.Name = "cmbselect";
			this.cmbselect.Size = new System.Drawing.Size(216, 21);
			this.cmbselect.TabIndex = 0;
			this.cmbselect.SelectedIndexChanged += new System.EventHandler(this.cmbselect_SelectedIndexChanged);
			// 
			// chkexpand
			// 
			this.chkexpand.Location = new System.Drawing.Point(8, 8);
			this.chkexpand.Name = "chkexpand";
			this.chkexpand.Size = new System.Drawing.Size(136, 40);
			this.chkexpand.TabIndex = 22;
			this.chkexpand.Text = "Expand MultiPage";
			// 
			// chkCreate
			// 
			this.chkCreate.Checked = true;
			this.chkCreate.Location = new System.Drawing.Point(32, 456);
			this.chkCreate.Name = "chkCreate";
			this.chkCreate.Size = new System.Drawing.Size(144, 32);
			this.chkCreate.TabIndex = 24;
			this.chkCreate.TabStop = true;
			this.chkCreate.Text = "Enable External Drag Drop";
			this.chkCreate.CheckedChanged += new System.EventHandler(this.chkCreate_CheckedChanged);
			// 
			// chkDragDrop
			// 
			this.chkDragDrop.Location = new System.Drawing.Point(32, 504);
			this.chkDragDrop.Name = "chkDragDrop";
			this.chkDragDrop.Size = new System.Drawing.Size(128, 32);
			this.chkDragDrop.TabIndex = 25;
			this.chkDragDrop.Text = "Enable Internal Drag Drop";
			this.chkDragDrop.CheckedChanged += new System.EventHandler(this.chkDragDrop_CheckedChanged);
			// 
			// chkPreserveBlack
			// 
			this.chkPreserveBlack.Checked = true;
			this.chkPreserveBlack.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkPreserveBlack.Location = new System.Drawing.Point(8, 56);
			this.chkPreserveBlack.Name = "chkPreserveBlack";
			this.chkPreserveBlack.Size = new System.Drawing.Size(144, 24);
			this.chkPreserveBlack.TabIndex = 26;
			this.chkPreserveBlack.Text = "PreserveBlack option";
			this.chkPreserveBlack.CheckedChanged += new System.EventHandler(this.chkPreserveBlack_CheckedChanged);
			// 
			// chkCameraRaw
			// 
			this.chkCameraRaw.Checked = true;
			this.chkCameraRaw.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkCameraRaw.Location = new System.Drawing.Point(168, 56);
			this.chkCameraRaw.Name = "chkCameraRaw";
			this.chkCameraRaw.Size = new System.Drawing.Size(136, 24);
			this.chkCameraRaw.TabIndex = 27;
			this.chkCameraRaw.Text = "CameraRaw option";
			this.chkCameraRaw.CheckedChanged += new System.EventHandler(this.chkCameraRaw_CheckedChanged);
			// 
			// cmdCancel
			// 
			this.cmdCancel.Enabled = false;
			this.cmdCancel.Location = new System.Drawing.Point(160, 8);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(112, 32);
			this.cmdCancel.TabIndex = 28;
			this.cmdCancel.Text = "Cancel Thumbnail Loading";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// frmthumb
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(866, 579);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdCancel,
																		  this.chkCameraRaw,
																		  this.chkPreserveBlack,
																		  this.chkDragDrop,
																		  this.chkCreate,
																		  this.chkexpand,
																		  this.tabControl1,
																		  this.errdesc,
																		  this.lblError,
																		  this.sampdesc,
																		  this.cmdremove,
																		  this.lstselect,
																		  this.cmdsel,
																		  this.thumbnailXpress1,
																		  this.cmdclear});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Menu = this.mainMenu1;
			this.Name = "frmthumb";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ThumbNailXpress C# Viewer";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.tabControl1.ResumeLayout(false);
			this.Filters.ResumeLayout(false);
			this.cells.ResumeLayout(false);
			this.tabProcessing.ResumeLayout(false);
			this.grpThreads.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.maxThreads)).EndInit();
			this.tabselect.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmthumb());
		
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void mnudir_Click(object sender, System.EventArgs e)
		{
			
			try
			{

				//clear out the error before the next opertation
				lblError.Text = "";

				//First we grab the filename of the image we want to open
				//create a filter item here

				cmdCancel.Enabled = true;
				filteritem = thumbnailXpress1.CreateFilter("test");
				
				filteritemCreated = true;
				//open the dir here
				System.String strloadres = PegasusOpenDir();
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				PegasusError(ex,lblError);
			}
			
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}
			

		}
		private void cmdclear_Click(object sender, System.EventArgs e)
		{


			try
			{
				//clear the TN display
				thumbnailXpress1.Items.Clear();
			
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				PegasusError(ex,lblError);
			}
			
		}
		
		private void Form1_Load(object sender, System.EventArgs e)
		{	
			try
			{	
				//********MUST CALL UNLOCKRUNTIME methods at STARTUP*******
				//thumbnailXpress1.Licensing.UnlockRuntime(1234,1234,1234,1234);
				//thumbnailXpress1.Licensing.UnlockIXRuntime(1234,1234,1234,1234);
				
				//thread settings
				maxThreads.Value = thumbnailXpress1.MaximumThreadCount;
				txtSThread.Text =  thumbnailXpress1.ThreadStartThreshold.ToString();
				txtHungT.Text =  thumbnailXpress1.ThreadHungThreshold.ToString();


				//here we set the current directory and image so that the file open dialog box works well
				strCurrentDir = System.IO.Directory.GetCurrentDirectory();
				strCurrentDir = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\");
				
				lstselect.CheckOnClick = true;
				//enable drag and drop in the control and set some defaults

				dragsource = true;
				try
				{
					thumbnailXpress1.Licensing.AccessPdfXpress();
				} 
				catch (Exception)
				{
					PegasusError(new Exception("Unable to access PDFXpress. PDF functionality is disabled."),lblError);
				}
				thumbnailXpress1.AllowDrop = true;
				//thumbnailXpress1.PreserveBlack = false;
				thumbnailXpress1.CameraRaw = false;

				thumbnailXpress1.DescriptorDisplayMethod = DescriptorDisplayMethods.ShortFilename;
				
				cmbcompare.SelectedIndex = 0;

				cmbchoice.SelectedIndex = 0;
				cmbselect.SelectedIndex = 0;
				//TN cell settings
				hbwidth.Value = 1;
				hbheight.Value = 65;
				hbh.Value = 1;
				vbh.Value =1;
				vcw.Value = 65;
				lblbwidth.Text = hbwidth.Value.ToString();
				lblheight.Text = hbheight.Value.ToString();
				lblCHS.Text = hbh.Value.ToString();
				lblCVS.Text =vbh.Value.ToString();
				lblCellWidth.Text = vcw.Value.ToString();
				//date time value set here
				dateTimePicker1.Value =  System.DateTime.Now;
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
			
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}

		}
		
		private void thumbnailXpress1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
		
			lblError.Text = "";

			try
			{
				point = new System.Drawing.Point(e.X,e.Y);
				tdata = thumbnailXpress1.GetItemFromPoint(point);
			
				//get the data to drag to the other TN control
				
				if(dragsource == false && e.Button == MouseButtons.Left)
				{
		
					//***Check to make sure there are items that have been selected
					if(tdata != null)
					{
					
						indexOfItemUnderMouseToDrag = thumbnailXpress1.Items.IndexOf(tdata);
					
					}
					if(indexOfItemUnderMouseToDrag >= 0)
					{
						//start the drag drop to the source and set the data to do a move
						thumbnailXpress1.DoDragDrop(tdata,System.Windows.Forms.DragDropEffects.Move);
					}

				}
			
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
			
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}
		}
		private void thumbnailXpress1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			
			lblError.Text = "";
			string[] files;
			try
			{
				//This will perform a drag and drop external from the control
	
				if(dragsource == true)
				{
					files = (string[])e.Data.GetData(DataFormats.FileDrop);
			
					//iterate through the files adding them to the display which demonstrates using the
					//CreateThumbnailItem method to progromatically add the TN's
					for(int count1 = 0 ;count1 < files.Length ; count1++)
					{
						externalsource = thumbnailXpress1.CreateThumbnailItem(files[count1],1,PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.Unknown);
						thumbnailXpress1.Items.Add(externalsource,true);
					}
	
				}	
					//******This performs an internal drag and drag from 1 TNXpress item to another
				else
				{
			
					System.Drawing.Point pointin = new System.Drawing.Point(e.X,e.Y);
				
						System.Drawing.Point pointout;
						//translate to client coordinates	
						pointout = thumbnailXpress1.PointToClient(pointin);
						dragtarget= thumbnailXpress1.GetItemFromPoint(pointout);
					if(dragtarget != null)
					{
						dragindex = thumbnailXpress1.Items.IndexOf(dragtarget);
						thumbnailXpress1.Items.RemoveAt(indexOfItemUnderMouseToDrag);
						thumbnailXpress1.Items.Insert(dragindex ,tdata,false);
						
					}
				}
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
				// Least specific:
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}
			
		}
		private void thumbnailXpress1_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{	
			lblError.Text = "";
			//start the drag/drop here
			try
			{
				e.Effect = DragDropEffects.Move;
			}

			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				PegasusError(ex,lblError);
			}
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}
		}
		

		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		//Error Trapping routine
		private void PegasusError(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException  ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			
			lblError.Text = ErrorException.Message + "\n" + ErrorException.Source;
		}
		
		//Open single files here or multi-page TIFF Image
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				//strImageFile = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				strImageFile = dlg.FileName;
				if(chkexpand.Checked == true)
				{
					thumbnailXpress1.Items.AddItemsFromFile(strImageFile,0,true);
				}
				else
				{
					thumbnailXpress1.Items.AddItemsFromFile(strImageFile,0,false);
				}
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

			
		string PegasusOpenDir()
		{
		
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;

			if (dlg.ShowDialog() == DialogResult.OK)
			{
				//open to the directory selected
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf('\\'), dlg.FileName.Length - dlg.FileName.LastIndexOf('\\'));
			
				switch(dlg.FilterIndex)
				{
					case 1 :
						//all types
						break;
					case 2 :
						filteritem.FilenamePattern = "*.bmp";
						break;
					case 3 : 
						filteritem.FilenamePattern = "*.cal";
						break;
					case 4 : 
						filteritem.FilenamePattern = "*.dib";
						break;
					case 5 : 
						filteritem.FilenamePattern = "*.dca";
						break;
					case 6 : 
						filteritem.FilenamePattern = "*.mod";
						break;
					case 7 : 
						filteritem.FilenamePattern = "*.gif";
						break;
					case 8 : 
						filteritem.FilenamePattern = "*.jp2";
						break;
					case 9 : 
						filteritem.FilenamePattern = "*.jls";
						break;
					case 10 : 
						filteritem.FilenamePattern = "*.jpg";
						break;
					case 11 : 
						filteritem.FilenamePattern = "*.jls";
						break;
					case 12 : 
						filteritem.FilenamePattern = "*.pmm";
						break;
					case 13 : 
						filteritem.FilenamePattern = "*.pcx";
						break;
					case 14 : 
						filteritem.FilenamePattern = "*.pgm";
						break;
					case 15 : 
						filteritem.FilenamePattern = "*.pic";
						break;
					case 16 : 
						filteritem.FilenamePattern = "*.png";
						break;
					case 17 : 
						filteritem.FilenamePattern = "*.ppm";
						break;
					case 18 : 
						filteritem.FilenamePattern = "*.tif";
						break;
					case 19 : 
						filteritem.FilenamePattern = "*.tga";
						break;
					case 20 : 
						filteritem.FilenamePattern = "*.wsq";
						break;
					case 21 : 
						filteritem.FilenamePattern = "*.jb2";
						break;
					case 22 : 
						break;
				}

				if(chkenable.Checked == true)
				{	
					thumbnailXpress1.Filters.Clear();
					//FilterType
					filteritem.Type =  (FilterType)cmbchoice.SelectedIndex;
					//FilteCompare
					filteritem.Comparison =  (FilterComparison)cmbcompare.SelectedIndex;
					//FilterSize
					filteritem.FileDate = dateTimePicker1.Value;
					if(txtsize.Text != "")
					{	
						 string s = txtsize.Text;
						 int temp = Convert.ToInt32(s);
						 filteritem.FileSize = temp;
					
						
					}
					
					filteritem.CaseSensitive = chkdir.Checked;
					filteritem.MatchAppliesToDirectory = chkdir.Checked;
					filteritem.IncludeHidden = chkhid.Checked;
					filteritem.IncludeParentDirectory = chkparent.Checked;
					filteritem.IncludeSubDirectory = chksub.Checked;
						
					//add the filter with desired settings from above
					thumbnailXpress1.Filters.Add(filteritem);
	
					//Open the Directory of Images
					thumbnailXpress1.BeginUpdate();
					thumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\\",0);
					thumbnailXpress1.EndUpdate();
				}
				else
				{		//**No Filters applied here**
					thumbnailXpress1.BeginUpdate();
					thumbnailXpress1.Items.AddItemsFromDirectory(strCurrentDir + "\\",0);
					thumbnailXpress1.EndUpdate();
				}

				return strCurrentDir;
			} 
			else 

			{
			
				return "";
			
			}
			
		}

		

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion

		

		private void cmdsel_Click(object sender, System.EventArgs e)
		{
			try
			{
				//get count of selected items
				lstselect.Items.Clear();
				a = thumbnailXpress1.SelectedItems.Count;
				//create a array with the size of the selected TN's
				array = new PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem[a];
				//copy items to array
		       thumbnailXpress1.SelectedItems.CopyTo(array,0);
				//add all the items to a list box
				lstselect.Items.AddRange(array);

				
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
				// Least specific:
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}

		}
		private void cmdremove_Click(object sender, System.EventArgs e)
		{
			
			try
			{
				int citem;
				citem = lstselect.CheckedItems.Count;
		
				for (int i=0; i < citem; i++)
				{
					thumbnailXpress1.Items.Remove(array[i]);
					//lstselect.Items.RemoveAt(i);
				}
				//recopy the array
				//get count of selected items
				lstselect.Items.Clear();
				a = thumbnailXpress1.SelectedItems.Count;
				//create a array with the size of the selected TN's
				array = new PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem[a];
				//copy items to array
				thumbnailXpress1.SelectedItems.CopyTo(array,0);
				//add all the items to a list box
				lstselect.Items.AddRange(array);
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
				// Least specific:
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}

		}
				
		private void button1_Click_1(object sender, System.EventArgs e)
		{
			thumbnailXpress1.CellBorderColor = System.Drawing.Color.Bisque;
		}

		private void hbwdith_ValueChanged(object sender, System.EventArgs e)
		{
			lblbwidth.Text = hbwidth.Value.ToString();
			
		}
		
		private void cmbcompare_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			//filteritem = thumbnailXpress1.CreateFilter("test");
			switch(cmbcompare.SelectedIndex)
			{
					//FileName Filter
				case 0 : 
					
					filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.LessThan;
					break;
				case 1 : 
				
					filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.LessThanEquals;
					break;
				case 2 : 
					
					filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.Equal;
	
					break;
				case 3 : 
					
					filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.GreaterThanEquals;
					break;
				case 4 : 
					
					filteritem.Comparison = PegasusImaging.WinForms.ThumbnailXpress1.FilterComparison.GreaterThan;
					break;
			
			}
		}

		private void menuItem2_Click_1(object sender, System.EventArgs e)
		{
			thumbnailXpress1.AboutBox();
		}

		private void chkdir_Click(object sender, System.EventArgs e)
		{
			if(chkdir.Checked == true)
			{
				filteritem.MatchAppliesToDirectory = true;

			}
			else
			{
				filteritem.MatchAppliesToDirectory =false;

			}
		}

		private void chkdir_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkdir.Checked == true)
			{
				filteritem.MatchAppliesToDirectory = true;

			}
			else
			{
				filteritem.MatchAppliesToDirectory =false;

			}
		}

		private void chksen_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkdir.Checked == true)
			{
				filteritem.CaseSensitive = true;

			}
			else
			{
				filteritem.CaseSensitive =false;

			}
		}
		private void chkhid_CheckedChanged(object sender, System.EventArgs e)
		{

			if(chkhid.Checked == true)
			{
				filteritem.IncludeHidden = true;

			}
			else
			{
				filteritem.IncludeHidden = false;

			}
			
		}
		

		private void chksub_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkenable.Checked == true)
			{
				filteritem.IncludeSubDirectory= true;

			}
			else
			{
				filteritem.IncludeSubDirectory = false;

			}
		}

		private void chkparent_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkenable.Checked == true)
			{
				filteritem.IncludeParentDirectory = true;

			}
			else
			{
				filteritem.IncludeParentDirectory = false;

			}
		}

		private void dateTimePicker1_ValueChanged(object sender, System.EventArgs e)
		{
			filteritem.FileDate = dateTimePicker1.Value;

		}
		
		

		private void mnusingle_Click_1(object sender, System.EventArgs e)
		{
			try
			{	
				//clear out the error before the next opertation
				lblError.Text = "";

				System.String strloadres = PegasusOpenFile();
		
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}
		}
		private void cmbchoice_SelectedIndexChanged_1(object sender, System.EventArgs e)
		{
			//filteritem = thumbnailXpress1.CreateFilter("test");
			switch(cmbchoice.SelectedIndex)
			{
				case 0 : 
					//File Name Match
					chkenable.Enabled = true;
					chkdir.Enabled = false;
					chksen.Enabled = false;
					chksub.Enabled = false;
					chkparent.Enabled = false;
					dateTimePicker1.Enabled = false;
					cmbcompare.Enabled = false;
					chkhid.Enabled = false;
					txtsize.Enabled = false;
					
					break;
				case 1 : 
					//File Attributes
					chkdir.Enabled = true;
					chkparent.Enabled = true;
					chksub.Enabled = true;
					chkparent.Enabled = true;
					chkhid.Enabled = true;
					chkdir.Enabled = false;
					chksen.Enabled = false;
					dateTimePicker1.Enabled = false;
					cmbcompare.Enabled = false;
					txtsize.Enabled = false;
					break;
				case 2 : 
					//File Creation Date
					chkenable.Enabled = true;
					cmbcompare.Enabled = true;
					dateTimePicker1.Enabled = true;
					txtsize.Enabled = false;
					chksub.Enabled = false;
					chkparent.Enabled = false;
					chkhid.Enabled = false;
					break;
				case 3 :
					//File Mod Date
					chkenable.Enabled = true;
					dateTimePicker1.Enabled = true;
					cmbcompare.Enabled = true;
					txtsize.Enabled = false;
					chksub.Enabled = false;
					chkparent.Enabled = false;
					chkhid.Enabled = false;
					break;
				case 4 :
					//File Size
					chkenable.Enabled = true;
					dateTimePicker1.Enabled = false;
					txtsize.Visible = true;
					txtsize.Enabled = true;
					cmbcompare.Enabled = true;
					chksub.Enabled = false;
					chkparent.Enabled = false;
					break;
				case 5 :
					//all IX supported file types
					chkenable.Enabled = false;
					dateTimePicker1.Enabled = false;
					cmbcompare.Enabled = false;
					chksen.Enabled = false;
					chkdir.Enabled = false;
					chkparent.Enabled = false;
					chksub.Enabled  =false;
					chkhid.Enabled = false;
					txtsize.Enabled = false;
					break;
			}
		}

		private void hbwidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblbwidth.Text = hbwidth.Value.ToString();
		}

		private void hbheight_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblheight.Text = hbheight.Value.ToString();
		}

		private void hbh_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblCHS.Text = hbh.Value.ToString();
		}
		private void vbh_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblCVS.Text = vbh.Value.ToString();
		}
		

		private void vcw_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblCellWidth.Text = vcw.Value.ToString();
		}

		private void cmdcolor_Click(object sender, System.EventArgs e)
		{
			cd.ShowDialog();
		}

		
		private void cmdset_Click(object sender, System.EventArgs e)
		{
			//Set the TN Cell properties
			thumbnailXpress1.BeginUpdate();
			thumbnailXpress1.CellBorderWidth = hbwidth.Value;
			thumbnailXpress1.CellBorderColor = cd.Color;
			thumbnailXpress1.CellHeight = hbheight.Value;
			thumbnailXpress1.CellHorizontalSpacing = hbh.Value;
			thumbnailXpress1.CellVerticalSpacing = vbh.Value;
			thumbnailXpress1.CellWidth = vcw.Value;
			thumbnailXpress1.EndUpdate();
		}

		private void cmbselect_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//switch(cmbcompare.SelectedIndex)
			switch(cmbselect.SelectedIndex)
			{
				case 0 : 
					thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.MultiExtended;
					break;
				case 1 : 
					thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.MultiSimple;
					break;
				case 2 : 
					thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.None;
					break;
				case 3 :
					thumbnailXpress1.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress1.SelectionMode.Single;
					break;
				
			}
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			thumbnailXpress1.AboutBox();
		}

		private void chkenable_CheckedChanged(object sender, System.EventArgs e)
		{
			if (filteritemCreated == true)
			{
				if(chkenable.Checked == true)
				{
					filteritem.Enabled = true;
					thumbnailXpress1.Filters.Clear();
					thumbnailXpress1.Filters.Add(filteritem);
				}
				else
				{
					filteritem.Enabled = false;

				}
			}
		}

		private void chkCreate_CheckedChanged(object sender, System.EventArgs e)
		{
			//external drag and drop
			if (chkCreate.Checked == true)
			{
				cmbselect.Enabled = true;
				dragsource = true;
			}
			else
			{
				dragsource = false;
			}
		}

		private void chkDragDrop_CheckedChanged(object sender, System.EventArgs e)
		{
		
			if (chkDragDrop.Checked == true)
			{
				cmbselect.Enabled = false;
				dragsource = false;
			}
			else
			{
				dragsource = true;
			}
		}

		private void thumbnailXpress1_DoubleClick(object sender, System.EventArgs e)
		{

			long count;

			

			lblError.Text = "";

			dragsource = true;
			try
			{	
	
				//need to verify if we are on a folder etc and disable internal drag drop
				
				PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem info;
				info = thumbnailXpress1.GetItemFromPoint(point);

				if (!(info == null)) 
				{

				
					if(info.Type == PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.ParentDirectory || info.Type ==  PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.SubDirectory)
					{
			
						PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem tester;
				
						string path = System.IO.Directory.GetCurrentDirectory();
						string [] directoryEntries = System.IO.Directory.GetDirectories(System.IO.Path.GetFullPath(info.Filename)); 
						string [] fileEntries = Directory.GetFiles(System.IO.Path.GetFullPath(info.Filename));
						//clear the display

						thumbnailXpress1.BeginUpdate();
						thumbnailXpress1.Items.Clear();

						for (count = 0; count <= directoryEntries.Length - 1;count ++) 
						{
							// display all the directories in the selected folder
							tester = thumbnailXpress1.CreateThumbnailItem(System.IO.Path.GetFullPath(directoryEntries[count]), 1, PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.Unknown);
							thumbnailXpress1.Items.Add(tester, true);
						}


						for(count = 0; count <= fileEntries.Length - 1; count ++)
						{
							//display all the items in the selected folder

							tester = thumbnailXpress1.CreateThumbnailItem(System.IO.Path.GetFullPath(fileEntries[count]),1,PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailType.Unknown);
							
													
							// we don't want to add unsupported file types as we will get an exception
							//if (((tester.ErrorNumber == 0) && !(tester.Type == ThumbnailType.Unsupported) && (tester.Hdib.ToInt32() > 0))) 
							if ((tester.ErrorNumber == 0) && !(tester.Type == ThumbnailType.Unsupported)) 
							{
								thumbnailXpress1.Items.Add(tester, true);
							}
												
						}
							
						thumbnailXpress1.EndUpdate();
					}
					else
					{
						//Call Image Viewer Dialog here

						tdata = thumbnailXpress1.GetItemFromPoint(point);
						TNViewer  viewer = new TNViewer ();
						viewer.AddImage(tdata.Filename);
						Application.DoEvents();
					}
	
				}
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				
				PegasusError(ex,lblError);

			}
			catch (NullReferenceException ex) 
			{
				PegasusError(ex,lblError);
			}
		}


		private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
			switch(tabControl1.SelectedIndex)
			{
			
				case 0:
					//internal
					if( chkCreate.Checked == true)
					{
						dragsource = true;	
						chkDragDrop.Enabled = true;
					}
					else
					{
						dragsource = false;	
						chkDragDrop.Enabled = true;
					}	
					break;
				case 1:
					//internal
					
	
					if( chkCreate.Checked == true)
					{
						dragsource = true;	
						chkDragDrop.Enabled = true;
					}
					else
					{
						dragsource = false;	
						chkDragDrop.Enabled = true;
					}	
					break;
					
									
				case 2:
					//external/info tab attributes tab
				
						dragsource = true;	
						chkDragDrop.Enabled = false;
						chkCreate.Enabled = true;
						chkCreate.Checked = true;
					
					break;
				case 3:
					//internal
					if( chkCreate.Checked == true)
					{
						dragsource = true;	
						chkDragDrop.Enabled = true;
					}
					else
					{
						dragsource = false;	
						chkDragDrop.Enabled = true;
					}	
					break;
		
				}


		}
		private void chkPreserveBlack_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkPreserveBlack.Checked == true)
			{
				//enable preserve black viewing option
				thumbnailXpress1.PreserveBlack = true;	
				
			}
			else
			{
				//disable preserve black viewing option
				thumbnailXpress1.PreserveBlack = false;	
				
			}

		}

		private void chkCameraRaw_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkCameraRaw.Checked == true)
			{
				//enable camera raw image viewing
				thumbnailXpress1.CameraRaw = true;	
				
			}
			else
			{
				//disable camera raw image viewing
				thumbnailXpress1.CameraRaw = false;	
			}
			
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			try
			{
				thumbnailXpress1.Cancel();	
			}
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
					PegasusError(ex,lblError);
			}
			}

		private void tabCache_Click(object sender, System.EventArgs e)
		{
		
		}

		private void cmdFlushCache_Click(object sender, System.EventArgs e)
		{
			thumbnailXpress1.ThumbnailCache.CacheFlushMemory();
		}

		private void cmdClearCache_Click(object sender, System.EventArgs e)
		{
			thumbnailXpress1.ThumbnailCache.CacheClear();
		}


		private void cmdSetThreads_Click(object sender, System.EventArgs e)
		{
			try
			{
				thumbnailXpress1.ThreadStartThreshold = System.Convert.ToInt32(txtSThread.Text);
			}
			catch (PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				PegasusError(ex,lblError);
				txtSThread.Text = thumbnailXpress1.ThreadStartThreshold.ToString();
				
			}
			
			try
			{
				thumbnailXpress1.ThreadHungThreshold= System.Convert.ToInt32(txtHungT.Text);
			}
			catch (PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
						txtHungT.Text = thumbnailXpress1.ThreadHungThreshold.ToString();
							
			}

		}

		private void maxThreads_ValueChanged(object sender, System.EventArgs e)
		{
			try
			{
				thumbnailXpress1.MaximumThreadCount = (System.Int32)maxThreads.Value;
			}
			
			catch(PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpressException ex)
			{
				PegasusError(ex,lblError);
				
			}	
		}
		private void button1_Click(object sender, System.EventArgs e)
		{
			//get count of selected items
			lstselect.Items.Clear();
			long b;
			b = thumbnailXpress1.Items.Count;
			//create a array with the size of the selected TN's
			array = new PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem[b];
			//copy items to array
			thumbnailXpress1.Items.CopyTo(array,0);
		
			thumbnailXpress1.SelectedItems.Add(array[5]);

			
			
			
			//a = thumbnailXpress1.Items.Count;
			//create a array with the size of the selected TN's
			//array = new PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailItem[a];
			//copy items to array
			//thumbnailXpress1.SelectedItems.CopyTo(array,0);
			//add all the items to a list box
			//lstselect.Items.AddRange(array);
			
		
		}

		
		private void chkdir_CheckedChanged_1(object sender, System.EventArgs e)
		{
		
		}

		private void grpThreads_Enter(object sender, System.EventArgs e)
		{
		
		}

		private void lblThreads_Click(object sender, System.EventArgs e)
		{
		
		}

			
		
		}
	}


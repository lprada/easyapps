Imports PegasusImaging.WinForms.ImagXpress8


Public Class TNViewer
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents lblerrordesc As System.Windows.Forms.Label
    Friend WithEvents lblFname As System.Windows.Forms.Label
    Friend WithEvents lblFType As System.Windows.Forms.Label
    Friend WithEvents lblDimen As System.Windows.Forms.Label
    Friend WithEvents lblbpp As System.Windows.Forms.Label
    Friend WithEvents lblsize As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.lblerrordesc = New System.Windows.Forms.Label()
        Me.lblFname = New System.Windows.Forms.Label()
        Me.lblFType = New System.Windows.Forms.Label()
        Me.lblDimen = New System.Windows.Forms.Label()
        Me.lblbpp = New System.Windows.Forms.Label()
        Me.lblsize = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Location = New System.Drawing.Point(24, 16)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(232, 264)
        Me.ImageXView1.TabIndex = 0
        '
        'lblerror
        '
        Me.lblerror.Location = New System.Drawing.Point(280, 248)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(392, 40)
        Me.lblerror.TabIndex = 1
        '
        'lblerrordesc
        '
        Me.lblerrordesc.Location = New System.Drawing.Point(280, 224)
        Me.lblerrordesc.Name = "lblerrordesc"
        Me.lblerrordesc.Size = New System.Drawing.Size(280, 16)
        Me.lblerrordesc.TabIndex = 2
        Me.lblerrordesc.Text = "Last Error Reported:"
        '
        'lblFname
        '
        Me.lblFname.Location = New System.Drawing.Point(512, 24)
        Me.lblFname.Name = "lblFname"
        Me.lblFname.Size = New System.Drawing.Size(192, 24)
        Me.lblFname.TabIndex = 3
        '
        'lblFType
        '
        Me.lblFType.Location = New System.Drawing.Point(512, 64)
        Me.lblFType.Name = "lblFType"
        Me.lblFType.Size = New System.Drawing.Size(184, 24)
        Me.lblFType.TabIndex = 4
        '
        'lblDimen
        '
        Me.lblDimen.Location = New System.Drawing.Point(512, 104)
        Me.lblDimen.Name = "lblDimen"
        Me.lblDimen.Size = New System.Drawing.Size(192, 24)
        Me.lblDimen.TabIndex = 5
        '
        'lblbpp
        '
        Me.lblbpp.Location = New System.Drawing.Point(512, 144)
        Me.lblbpp.Name = "lblbpp"
        Me.lblbpp.Size = New System.Drawing.Size(176, 32)
        Me.lblbpp.TabIndex = 6
        '
        'lblsize
        '
        Me.lblsize.Location = New System.Drawing.Point(512, 192)
        Me.lblsize.Name = "lblsize"
        Me.lblsize.Size = New System.Drawing.Size(176, 16)
        Me.lblsize.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(304, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 24)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "FileName:"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(304, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 32)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "File Type:"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(304, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(160, 24)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Dimensions:"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(304, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(168, 24)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Bits Per Pixel:"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(304, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(168, 24)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Size:"
        '
        'TNViewer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(720, 301)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label5, Me.Label4, Me.Label3, Me.Label2, Me.Label1, Me.lblsize, Me.lblbpp, Me.lblDimen, Me.lblFType, Me.lblFname, Me.lblerrordesc, Me.lblerror, Me.ImageXView1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "TNViewer"
        Me.Text = "Image Info Dialog"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Sub AddImage(ByVal FName As System.String)
        Try
            Dim lo As New PegasusImaging.WinForms.ImagXpress8.LoadOptions()
            lo.CameraRawEnabled = True
            lo.Jpeg.Enhanced = False
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(FName, lo)
            ImageXView1.AutoScroll = True
            lblFname.Text = System.IO.Path.GetFileName(FName)
            lblFType.Text = ImageXView1.Image.ImageXData.Format.ToString()
            lblDimen.Text = ImageXView1.Image.ImageXData.Width.ToString() + "x" + ImageXView1.Image.ImageXData.Height.ToString()
            lblbpp.Text = ImageXView1.Image.ImageXData.BitsPerPixel.ToString()
            lblsize.Text = ImageXView1.Image.ImageXData.Size.ToString()

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            lblerror.Text = ex.Message
        End Try
    End Sub

    Private Sub ImageXView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageXView1.Load

    End Sub

    Private Sub lblFType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblFType.Click

    End Sub
End Class

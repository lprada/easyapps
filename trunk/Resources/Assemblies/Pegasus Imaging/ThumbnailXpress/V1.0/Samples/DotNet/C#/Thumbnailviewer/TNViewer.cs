using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ThumbNailXpress
{
	/// <summary>
	/// Summary description for TNViewer.
	/// </summary>
	public class TNViewer : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private System.Windows.Forms.Label lblFName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lbltype;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblDimen;
		private System.Windows.Forms.Label lblbpp;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblsize;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lblerror1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TNViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lblFName = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lbltype = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lblDimen = new System.Windows.Forms.Label();
			this.lblbpp = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.lblsize = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.lblerror1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView1.ForeColor = System.Drawing.Color.Blue;
			this.imageXView1.Location = new System.Drawing.Point(16, 24);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(232, 272);
			this.imageXView1.TabIndex = 0;
			// 
			// lblFName
			// 
			this.lblFName.Location = new System.Drawing.Point(464, 24);
			this.lblFName.Name = "lblFName";
			this.lblFName.Size = new System.Drawing.Size(152, 16);
			this.lblFName.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(296, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(136, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "FileName:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(296, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(128, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "File Type:";
			// 
			// lbltype
			// 
			this.lbltype.Location = new System.Drawing.Point(464, 72);
			this.lbltype.Name = "lbltype";
			this.lbltype.Size = new System.Drawing.Size(160, 24);
			this.lbltype.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(296, 104);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(128, 24);
			this.label3.TabIndex = 5;
			this.label3.Text = "Dimensions:";
			// 
			// lblDimen
			// 
			this.lblDimen.Location = new System.Drawing.Point(464, 104);
			this.lblDimen.Name = "lblDimen";
			this.lblDimen.Size = new System.Drawing.Size(152, 24);
			this.lblDimen.TabIndex = 6;
			// 
			// lblbpp
			// 
			this.lblbpp.Location = new System.Drawing.Point(464, 144);
			this.lblbpp.Name = "lblbpp";
			this.lblbpp.Size = new System.Drawing.Size(152, 24);
			this.lblbpp.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(296, 144);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(136, 24);
			this.label4.TabIndex = 8;
			this.label4.Text = "Bits Per Pixel:";
			// 
			// lblsize
			// 
			this.lblsize.Location = new System.Drawing.Point(456, 184);
			this.lblsize.Name = "lblsize";
			this.lblsize.Size = new System.Drawing.Size(152, 24);
			this.lblsize.TabIndex = 9;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(296, 184);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(144, 24);
			this.label5.TabIndex = 10;
			this.label5.Text = "Size:";
			// 
			// lblerror1
			// 
			this.lblerror1.Location = new System.Drawing.Point(288, 240);
			this.lblerror1.Name = "lblerror1";
			this.lblerror1.Size = new System.Drawing.Size(336, 48);
			this.lblerror1.TabIndex = 11;
			// 
			// TNViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(640, 301);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblerror1,
																		  this.label5,
																		  this.lblsize,
																		  this.label4,
																		  this.lblbpp,
																		  this.lblDimen,
																		  this.label3,
																		  this.lbltype,
																		  this.label2,
																		  this.label1,
																		  this.lblFName,
																		  this.imageXView1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "TNViewer";
			this.Text = "Image Info Dialog";
			this.Load += new System.EventHandler(this.TNViewer_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void TNViewer_Load(object sender, System.EventArgs e)
		{
		
		}

		//viewer function
		public void AddImage (System.String DibHandle)
		{
			try
			{
				
				PegasusImaging.WinForms.ImagXpress8.LoadOptions lo = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
				lo.CameraRawEnabled = true;
				lo.Jpeg.Enhanced = false;
				imageXView1.AutoScroll = true;
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(DibHandle,lo);
				lblFName.Text = System.IO.Path.GetFileName(DibHandle);
				lbltype.Text = imageXView1.Image.ImageXData.Format.ToString();
				lblDimen.Text = imageXView1.Image.ImageXData.Width.ToString() + "x" + imageXView1.Image.ImageXData.Height.ToString();
				lblbpp.Text = imageXView1.Image.ImageXData.BitsPerPixel.ToString();
				lblsize.Text = imageXView1.Image.ImageXData.Size.ToString();
				if (this.Visible == false)
				{
					this.ShowDialog ();	
				}
			}
			catch(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				
				PegasusError(ex,lblerror1);
				if (this.Visible == false)
				{
					this.ShowDialog ();	
				}
			}
		}
			static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
			{
				ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
			}

		}

	}


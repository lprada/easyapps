using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress7;
using PegasusImaging.WinForms.ThumbnailXpress1;
namespace ThumbNailXpress
{
	/// <summary>
	/// Summary description for ImageViewer.
	/// </summary>
	public class ImageViewer : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress7.PICImagXpress picImagXpress1;
		private PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpress  tn;  
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
	

		public ImageViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.picImagXpress1 = new PegasusImaging.WinForms.ImagXpress7.PICImagXpress();
			this.SuspendLayout();
			// 
			// picImagXpress1
			// 
			this.picImagXpress1.AddOnBehavior = PegasusImaging.WinForms.ImagXpress7.enumAddOnBehavior.ADDONBEHAVIOR_ShowEval;
			this.picImagXpress1.AlignH = PegasusImaging.WinForms.ImagXpress7.enumAlignH.ALIGNH_Center;
			this.picImagXpress1.AlignV = PegasusImaging.WinForms.ImagXpress7.enumAlignV.ALIGNV_Center;
			this.picImagXpress1.AspectX = 1;
			this.picImagXpress1.AspectY = 1;
			this.picImagXpress1.Async = false;
			this.picImagXpress1.AsyncCancelOnClose = false;
			this.picImagXpress1.AsyncMaxThreads = 4;
			this.picImagXpress1.AsyncPriority = PegasusImaging.WinForms.ImagXpress7.enumAsyncPriority.ASYNC_Normal;
			this.picImagXpress1.AutoClose = true;
			this.picImagXpress1.AutoInvalidate = true;
			this.picImagXpress1.AutoSize = PegasusImaging.WinForms.ImagXpress7.enumAutoSize.ISIZE_CropImage;
			this.picImagXpress1.BorderType = PegasusImaging.WinForms.ImagXpress7.enumBorderType.BORD_Inset;
			this.picImagXpress1.CancelLoad = false;
			this.picImagXpress1.CancelMode = PegasusImaging.WinForms.ImagXpress7.enumCancelMode.CM_None;
			this.picImagXpress1.CancelRemove = false;
			this.picImagXpress1.CompressInMemory = PegasusImaging.WinForms.ImagXpress7.enumCompressInMemory.CMEM_NONE;
			this.picImagXpress1.CropX = 0;
			this.picImagXpress1.CropY = 0;
			this.picImagXpress1.DIBXPos = -1;
			this.picImagXpress1.DIBYPos = -1;
			this.picImagXpress1.DisplayError = false;
			this.picImagXpress1.DisplayMode = PegasusImaging.WinForms.ImagXpress7.enumPicDisplayMode.DM_TrueColor;
			this.picImagXpress1.DrawFillColor = System.Drawing.Color.Black;
			this.picImagXpress1.DrawFillStyle = PegasusImaging.WinForms.ImagXpress7.enumFillStyle.FILL_Transparent;
			this.picImagXpress1.DrawMode = PegasusImaging.WinForms.ImagXpress7.enumDrawMode.PEN_CopyPen;
			this.picImagXpress1.DrawStyle = PegasusImaging.WinForms.ImagXpress7.enumBorderStyle.STYLE_Solid;
			this.picImagXpress1.DrawWidth = 1;
			this.picImagXpress1.Edition = PegasusImaging.WinForms.ImagXpress7.enumEdition.EDITION_Prof;
			this.picImagXpress1.EvalMode = PegasusImaging.WinForms.ImagXpress7.enumEvaluationMode.EVAL_Automatic;
			this.picImagXpress1.FileName = "";
			this.picImagXpress1.FileOffsetUse = PegasusImaging.WinForms.ImagXpress7.enumFileOffsetUse.FO_IMAGE;
			this.picImagXpress1.FileTimeout = 10000;
			this.picImagXpress1.FTPMode = PegasusImaging.WinForms.ImagXpress7.enumFTPMode.FTP_ACTIVE;
			this.picImagXpress1.FTPPassword = "";
			this.picImagXpress1.FTPUserName = "";
			this.picImagXpress1.IResUnits = PegasusImaging.WinForms.ImagXpress7.enumIRes.IRes_Inch;
			this.picImagXpress1.IResX = 0;
			this.picImagXpress1.IResY = 0;
			this.picImagXpress1.JPEGCosited = false;
			this.picImagXpress1.JPEGEnhDecomp = true;
			this.picImagXpress1.LoadCropEnabled = false;
			this.picImagXpress1.LoadCropHeight = 0;
			this.picImagXpress1.LoadCropWidth = 0;
			this.picImagXpress1.LoadCropX = 0;
			this.picImagXpress1.LoadCropY = 0;
			this.picImagXpress1.LoadResizeEnabled = false;
			this.picImagXpress1.LoadResizeHeight = 0;
			this.picImagXpress1.LoadResizeMaintainAspectRatio = true;
			this.picImagXpress1.LoadResizeWidth = 0;
			this.picImagXpress1.LoadRotated = PegasusImaging.WinForms.ImagXpress7.enumLoadRotated.LR_NONE;
			this.picImagXpress1.LoadThumbnail = PegasusImaging.WinForms.ImagXpress7.enumLoadThumbnail.THUMBNAIL_None;
			this.picImagXpress1.Location = new System.Drawing.Point(48, 32);
			this.picImagXpress1.LZWPassword = "";
			this.picImagXpress1.ManagePalette = true;
			this.picImagXpress1.MaxHeight = 0;
			this.picImagXpress1.MaxWidth = 0;
			this.picImagXpress1.MinHeight = 1;
			this.picImagXpress1.MinWidth = 1;
			this.picImagXpress1.MousePointer = PegasusImaging.WinForms.ImagXpress7.enumMousePointer.MP_Default;
			this.picImagXpress1.Multitask = false;
			this.picImagXpress1.Name = "picImagXpress1";
			this.picImagXpress1.Notify = false;
			this.picImagXpress1.NotifyDelay = 0;
			this.picImagXpress1.OLEDropMode = PegasusImaging.WinForms.ImagXpress7.enumOLEDropMode.OLEDROP_NONE;
			this.picImagXpress1.OwnDIB = true;
			this.picImagXpress1.PageNbr = 0;
			this.picImagXpress1.Pages = 0;
			this.picImagXpress1.Palette = PegasusImaging.WinForms.ImagXpress7.enumPalette.IPAL_Optimized;
			this.picImagXpress1.PDFSwapBlackandWhite = false;
			this.picImagXpress1.Persistence = true;
			this.picImagXpress1.PFileName = "";
			this.picImagXpress1.PICPassword = "";
			this.picImagXpress1.Picture = null;
			this.picImagXpress1.PictureEnabled = true;
			this.picImagXpress1.PrinterBanding = false;
			this.picImagXpress1.ProgressEnabled = false;
			this.picImagXpress1.ProgressPct = 10;
			this.picImagXpress1.ProxyServer = "";
			this.picImagXpress1.RaiseExceptions = false;
			this.picImagXpress1.SaveCompressSize = 0;
			this.picImagXpress1.SaveEXIFThumbnailSize = ((short)(0));
			this.picImagXpress1.SaveFileName = "";
			this.picImagXpress1.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_DEFAULT;
			this.picImagXpress1.SaveGIFInterlaced = false;
			this.picImagXpress1.SaveGIFType = PegasusImaging.WinForms.ImagXpress7.enumGIFType.GIF89a;
			this.picImagXpress1.SaveJLSInterLeave = 0;
			this.picImagXpress1.SaveJLSMaxValue = 0;
			this.picImagXpress1.SaveJLSNear = 0;
			this.picImagXpress1.SaveJLSPoint = 0;
			this.picImagXpress1.SaveJP2Order = PegasusImaging.WinForms.ImagXpress7.enumProgressionOrder.PO_DEFAULT;
			this.picImagXpress1.SaveJP2Type = PegasusImaging.WinForms.ImagXpress7.enumJP2Type.JP2_LOSSY;
			this.picImagXpress1.SaveJPEGChromFactor = ((short)(10));
			this.picImagXpress1.SaveJPEGCosited = false;
			this.picImagXpress1.SaveJPEGGrayscale = false;
			this.picImagXpress1.SaveJPEGLumFactor = ((short)(10));
			this.picImagXpress1.SaveJPEGProgressive = false;
			this.picImagXpress1.SaveJPEGSubSampling = PegasusImaging.WinForms.ImagXpress7.enumSaveJPEGSubSampling.SS_411;
			this.picImagXpress1.SaveLJPCompMethod = PegasusImaging.WinForms.ImagXpress7.enumSaveLJPCompMethod.LJPMETHOD_JPEG;
			this.picImagXpress1.SaveLJPCompType = PegasusImaging.WinForms.ImagXpress7.enumSaveLJPCompType.LJPTYPE_RGB;
			this.picImagXpress1.SaveLJPPrediction = ((short)(1));
			this.picImagXpress1.SaveMultiPage = false;
			this.picImagXpress1.SavePBMType = PegasusImaging.WinForms.ImagXpress7.enumPortableType.PT_Binary;
			this.picImagXpress1.SavePDFCompression = PegasusImaging.WinForms.ImagXpress7.enumSavePDFCompression.PDF_Compress_Default;
			this.picImagXpress1.SavePDFSwapBlackandWhite = false;
			this.picImagXpress1.SavePGMType = PegasusImaging.WinForms.ImagXpress7.enumPortableType.PT_Binary;
			this.picImagXpress1.SavePNGInterlaced = false;
			this.picImagXpress1.SavePPMType = PegasusImaging.WinForms.ImagXpress7.enumPortableType.PT_Binary;
			this.picImagXpress1.SaveTIFFByteOrder = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFByteOrder.TIFF_INTEL;
			this.picImagXpress1.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_Uncompressed;
			this.picImagXpress1.SaveTIFFRowsPerStrip = 0;
			this.picImagXpress1.SaveTileHeight = 0;
			this.picImagXpress1.SaveTileWidth = 0;
			this.picImagXpress1.SaveToBuffer = false;
			this.picImagXpress1.SaveTransparencyColor = System.Drawing.Color.Black;
			this.picImagXpress1.SaveTransparent = PegasusImaging.WinForms.ImagXpress7.enumTransparencyMatch.TRANSPARENT_None;
			this.picImagXpress1.SaveWSQBlack = ((short)(0));
			this.picImagXpress1.SaveWSQQuant = 1;
			this.picImagXpress1.SaveWSQWhite = ((short)(255));
			this.picImagXpress1.ScaleResizeToGray = false;
			this.picImagXpress1.ScrollBarLargeChangeH = 10;
			this.picImagXpress1.ScrollBarLargeChangeV = 10;
			this.picImagXpress1.ScrollBars = PegasusImaging.WinForms.ImagXpress7.enumScrollBars.SB_Both;
			this.picImagXpress1.ScrollBarSmallChangeH = 1;
			this.picImagXpress1.ScrollBarSmallChangeV = 1;
			this.picImagXpress1.ShowHourglass = false;
			this.picImagXpress1.Size = new System.Drawing.Size(328, 248);
			this.picImagXpress1.SN = "PEXP9700AZ-PEG0M0001N6";
			this.picImagXpress1.SpecialTIFFHandling = false;
			this.picImagXpress1.TabIndex = 0;
			this.picImagXpress1.Text = "picImagXpress1";
			this.picImagXpress1.TIFFIFDOffset = 0;
			this.picImagXpress1.Timer = 0;
			this.picImagXpress1.TWAINManufacturer = "";
			this.picImagXpress1.TWAINProductFamily = "";
			this.picImagXpress1.TWAINProductName = "";
			this.picImagXpress1.TWAINVersionInfo = "";
			this.picImagXpress1.UndoEnabled = false;
			this.picImagXpress1.ViewAntialias = true;
			this.picImagXpress1.ViewBrightness = ((short)(0));
			this.picImagXpress1.ViewContrast = ((short)(0));
			this.picImagXpress1.ViewDithered = true;
			this.picImagXpress1.ViewGrayMode = PegasusImaging.WinForms.ImagXpress7.enumGrayMode.GRAY_Standard;
			this.picImagXpress1.ViewProgressive = false;
			this.picImagXpress1.ViewSmoothing = true;
			this.picImagXpress1.ViewUpdate = true;
			this.picImagXpress1.WMFConvert = false;
			// 
			// ImageViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(416, 325);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.picImagXpress1});
			this.Name = "ImageViewer";
			this.Text = "ImageViewer";
			this.Load += new System.EventHandler(this.ImageViewer_Load);
			this.ResumeLayout(false);

		}
		#endregion

		
		public PegasusImaging.WinForms.ThumbnailXpress1.ThumbnailXpress primarytn
		{
			set { tn = value; }
		}
		public void AddImage (System.IntPtr a)
		{
			picImagXpress1.hDIB = (int)a  ;

			if (this.Visible == false)
			{
				this.ShowDialog ();	
			}
		}

		private void ImageViewer_Load(object sender, System.EventArgs e)
		{
			
		}

			}
		}

		
	

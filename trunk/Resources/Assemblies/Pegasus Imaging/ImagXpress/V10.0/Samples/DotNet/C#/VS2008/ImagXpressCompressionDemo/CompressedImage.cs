/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using Accusoft.ImagXpressSdk;
using System.Windows.Forms;
using System.Drawing;
using System.Xml;

namespace ImagXpressCompressionDemo
{
    public class CompressedImage
    {
        int tabIndex;
        ImageX imageSmall;
        ImageX imageActual;
        ImageXView imageView;
        SaveOptions saveOptionsColor;
        SaveOptions saveOptionsBW;
        SaveOptions saveOptionsGrayscale;

        double compressedToActualRatio = 1;
        double smallToActualRatio = 1;
        Point thumbLocation;

        // Label Info
        TabPage tabPage;
        Label labelFilesize;
        Label labelCompressionRatio;
        Label labelWidth;
        Label labelHeight;
        Label labelBitDepth;
        Label labelKbps56;
        Label labelKbps384;
        Label labelT1;
        Label labelName;

        MainForm mainForm;

        public CompressedImage(int theTabIndex, ImageXView theImageXView, TabPage theTabPage, Label theLabelFilesize, Label theLabelCompressionRatio, Label theLabelWidth, Label theLabelHeight, Label theLabelBitDepth, Label theLabelName, Label theLabelKbps56, Label theLabelKbps384, Label theLabelT1, MainForm theMainForm)
        {
            tabIndex = theTabIndex;
            imageView = theImageXView;

            tabPage = theTabPage;
            labelFilesize = theLabelFilesize;
            labelCompressionRatio = theLabelCompressionRatio;
            labelWidth = theLabelWidth;
            labelHeight = theLabelHeight;
            labelBitDepth = theLabelBitDepth;
            labelKbps56 = theLabelKbps56;
            labelKbps384 = theLabelKbps384;
            labelT1 = theLabelT1;
            labelName = theLabelName;
            mainForm = theMainForm;

            smallToActualRatio = (float)mainForm.GetThumbSize().Width / (float)theMainForm.GetOriginalImageView().Width;
            compressedToActualRatio = smallToActualRatio;

            thumbLocation = imageView.Location;

            saveOptionsColor = new SaveOptions();
            saveOptionsBW = new SaveOptions();
            saveOptionsGrayscale = new SaveOptions();
            saveOptionsGrayscale.Jpeg.Grayscale = true;
            saveOptionsGrayscale.Jp2.Grayscale = true;
        }

        #region Load Image

        public void Load(System.IO.Stream myStream)
        {
            myStream.Seek(0, System.IO.SeekOrigin.Begin);

            // Load the stream into the ImageXView control and query the stream.
            imageActual = ImageX.FromStream(mainForm.GetImagXpress(), myStream);
            imageView.Image = imageActual;

            myStream.Seek(0, System.IO.SeekOrigin.Begin);
            ImageXData data = ImageX.QueryFromStream(mainForm.GetImagXpress(), myStream, 1);

            // Display the compressed image data.
            labelFilesize.Text = data.Size.ToString() + " bytes";
            labelCompressionRatio.Text = ((data.Width * data.Height * data.BitsPerPixel) / data.Size).ToString() + " : 1";
            labelWidth.Text = data.Width.ToString();
            labelHeight.Text = data.Height.ToString();
            labelBitDepth.Text = data.BitsPerPixel.ToString();
            labelKbps56.Text = ((float)((float)data.Size / 57344)).ToString("F3") + " secs";
            labelKbps384.Text = ((float)((float)data.Size / 393216)).ToString("F3") + " secs";
            labelT1.Text = ((float)((float)data.Size / 1572864)).ToString("F3") + " secs";

            myStream.Seek(0, System.IO.SeekOrigin.Begin);
            if (data.Format == ImageXFormat.JpegLs)
            {
                // Note: There appears to be an issue with loading JLS when Resize is set.
                // Use Processor Resize instead.
                Processor prc = new Processor(mainForm.GetImagXpress());
                prc.Image = ImageX.FromStream(mainForm.GetImagXpress(), myStream);
                prc.Resize(new Size((int)(mainForm.GetOriginalImage().ImageXData.Width * smallToActualRatio), (int)(mainForm.GetOriginalImage().ImageXData.Height * smallToActualRatio)), ResizeType.Fast);
                imageSmall = prc.Image;
                prc.Dispose();
            }
            else
            {
                // Load the stream resized to use in the small control.
                LoadOptions loadOptions = new LoadOptions();
                loadOptions.Resize = new Size((int)(mainForm.GetOriginalImage().ImageXData.Width * smallToActualRatio), (int)(mainForm.GetOriginalImage().ImageXData.Height * smallToActualRatio));
                imageSmall = ImageX.FromStream(mainForm.GetImagXpress(), myStream, loadOptions);
            }
        }

        #endregion

        #region Compress Image

        public void CompressColorImage()
        {
            CompressImage(saveOptionsColor);
        }

        public void CompressBWImage()
        {
            CompressImage(saveOptionsBW);
        }

        public void CompressGrayscaleImage()
        {
            CompressImage(saveOptionsGrayscale);
        }

        public void CompressImage(SaveOptions theSaveOptions)
        {
            try
            {
                imageView.AllowUpdate = false;
                // Save the image to stream.
                System.IO.Stream myStream = new System.IO.MemoryStream();
                mainForm.GetOriginalImage().SaveStream(myStream, theSaveOptions);
                myStream.Seek(0, System.IO.SeekOrigin.Begin);
                Load(myStream);
                myStream.Close();
                imageView.AllowUpdate = true;
            }
            catch (ImagXpressException ex)
            {
                mainForm.DisplayStatusText(ex.Message);
            }
        }

        #endregion

        #region Animate Compressed Image

        public void Select(ImageXView sideImageXView, ImageXView verticalImageXView, ImageXView diagonalImageXView, Label sideLabel, Label verticalLabel, Label diagonalLabel)
        {
            if (imageView.Image != null)
            {
                if (imageView.Visible == false) // The image is being loaded; grow from small to thumbnail size.
                {
                    imageView.AllowUpdate = false;
                    imageView.Image = imageSmall;
                    Processor ixProcessor = new Processor(mainForm.GetImagXpress());
                    ixProcessor.Image = imageView.Image.Copy();
                    Rectangle rect = new Rectangle(0, 0, (int)((double)imageView.Width / mainForm.GetOriginalImageView().ZoomFactor), (int)((double)imageView.Height / mainForm.GetOriginalImageView().ZoomFactor));
                    ixProcessor.Crop(rect);
                    imageView.Image = ixProcessor.Image.Copy();
                    ixProcessor.Image.Dispose();
                    ixProcessor.Dispose();
                    imageView.AutoResize = AutoResizeType.ResizeImage;
                    imageView.AllowUpdate = true;

                    // Start the thumbnail at a small size and grow it to the thumbnail size.
                    imageView.Width = 1;
                    imageView.Height = 1;
                    imageView.Location = new Point(imageView.Location.X + mainForm.GetThumbSize().Width / 2, imageView.Location.Y + mainForm.GetThumbSize().Height / 2);
                    imageView.Visible = true;

                    Grow(sideImageXView, verticalImageXView, diagonalImageXView, sideLabel, verticalLabel, diagonalLabel, thumbLocation, mainForm.GetThumbSize());
                    labelName.Visible = true;

                    imageView.AllowUpdate = false;
                    imageView.Image = imageActual;
                    imageView.AutoResize = mainForm.GetOriginalImageView().AutoResize;
                    imageView.ZoomFactor = mainForm.GetOriginalImageView().ZoomFactor * compressedToActualRatio;
                    imageView.ScrollPosition = new Point((int)(mainForm.GetOriginalImageView().ScrollPosition.X * compressedToActualRatio), (int)(mainForm.GetOriginalImageView().ScrollPosition.Y * compressedToActualRatio));
                    imageView.AllowUpdate = true;
                }
                else if (mainForm.ShowingThumbs() == true) // The image is shrunk and needs to grow.
                {
                    imageView.AllowUpdate = false;
                    imageView.Image = imageSmall;
                    Processor ixProcessor = new Processor(mainForm.GetImagXpress());
                    ixProcessor.Image = imageView.Image.Copy();
                    Rectangle rect = new Rectangle((int)(mainForm.GetOriginalImageView().ScrollPosition.X * compressedToActualRatio / mainForm.GetOriginalImageView().ZoomFactor), (int)(mainForm.GetOriginalImageView().ScrollPosition.Y * compressedToActualRatio / mainForm.GetOriginalImageView().ZoomFactor), (int)((double)imageView.Width / mainForm.GetOriginalImageView().ZoomFactor), (int)((double)imageView.Height / mainForm.GetOriginalImageView().ZoomFactor));
                    ixProcessor.Crop(rect);
                    imageView.Image = ixProcessor.Image.Copy();
                    ixProcessor.Image.Dispose();
                    ixProcessor.Dispose();
                    imageView.AutoResize = AutoResizeType.ResizeImage;
                    imageView.AllowUpdate = true;

                    mainForm.GetTabControl().SelectedIndex = tabIndex;
                    Grow(sideImageXView, verticalImageXView, diagonalImageXView, sideLabel, verticalLabel, diagonalLabel, new Point(0, 0), mainForm.GetPanelSize());

                    imageView.AllowUpdate = false;
                    imageView.Image = imageActual;
                    imageView.AutoResize = mainForm.GetOriginalImageView().AutoResize;
                    compressedToActualRatio = 1;
                    imageView.ZoomFactor = mainForm.GetOriginalImageView().ZoomFactor * compressedToActualRatio;
                    imageView.ScrollPosition = new Point((int)(mainForm.GetOriginalImageView().ScrollPosition.X * compressedToActualRatio), (int)(mainForm.GetOriginalImageView().ScrollPosition.Y * compressedToActualRatio));
                    imageView.AllowUpdate = true;
                }
                else // The image is grown and needs to shrink.
                {
                    imageView.AllowUpdate = false;
                    imageView.Image = imageSmall;
                    Processor ixProcessor = new Processor(mainForm.GetImagXpress());
                    ixProcessor.Image = imageView.Image.Copy();
                    Rectangle rect = new Rectangle((int)(mainForm.GetOriginalImageView().ScrollPosition.X / mainForm.GetOriginalImageView().ZoomFactor * smallToActualRatio), (int)(mainForm.GetOriginalImageView().ScrollPosition.Y / mainForm.GetOriginalImageView().ZoomFactor * smallToActualRatio), (int)((double)imageView.Width / mainForm.GetOriginalImageView().ZoomFactor * smallToActualRatio), (int)((double)imageView.Height / mainForm.GetOriginalImageView().ZoomFactor * smallToActualRatio));
                    ixProcessor.Crop(rect);
                    imageView.Image = ixProcessor.Image.Copy();
                    ixProcessor.Image.Dispose();
                    ixProcessor.Dispose();
                    imageView.AutoResize = AutoResizeType.ResizeImage;
                    imageView.AllowUpdate = true;

                    mainForm.DisplayCompressedImagePanel3DBorder();
                    Shrink(sideImageXView, verticalImageXView, diagonalImageXView, sideLabel, verticalLabel, diagonalLabel);

                    imageView.AllowUpdate = false;
                    imageView.Image = imageActual;
                    imageView.AutoResize = mainForm.GetOriginalImageView().AutoResize;
                    compressedToActualRatio = smallToActualRatio;
                    imageView.ZoomFactor = mainForm.GetOriginalImageView().ZoomFactor * compressedToActualRatio;
                    imageView.ScrollPosition = new Point((int)(mainForm.GetOriginalImageView().ScrollPosition.X * compressedToActualRatio), (int)(mainForm.GetOriginalImageView().ScrollPosition.Y * compressedToActualRatio));
                    imageView.AllowUpdate = true;
                }
            }
        }

        private void Grow(ImageXView sideImageXView, ImageXView verticalImageXView, ImageXView diagonalImageXView, Label sideLabel, Label verticalLabel, Label diagonalLabel, Point destinationLocation, Size destinationSize)
        {
            imageView.BringToFront();
            labelName.Visible = false;

            // In 25 interations, how far left, right, up, and down
            // does the control need to be modified each iteration?
            int steps = 25;
            float changeInX = (float)(imageView.Location.X - destinationLocation.X) / (float)steps;
            float changeInY = (float)(imageView.Location.Y - destinationLocation.Y) / (float)steps;
            float changeInWidth = (float)(destinationSize.Width - imageView.Width) / (float)steps;
            float changeInHeight = (float)(destinationSize.Height - imageView.Height) / (float)steps;

            PointF fLoc = imageView.Location;
            SizeF fSize = imageView.Size;
            float fSleep = 2.0f;
            for (int i = 0; i < steps; i++)
            {
                fLoc.X -= changeInX;
                fLoc.Y -= changeInY;
                fSize.Width += changeInWidth;
                fSize.Height += changeInHeight;
                Point p = new Point((int)(fLoc.X + 0.5), (int)(fLoc.Y + 0.5));
                imageView.Location = p;
                imageView.Width = (int)(fSize.Width + 0.5);
                imageView.Height = (int)(fSize.Height + 0.5);

                if (sideImageXView.Visible == true && imageView.Location.X + imageView.Width > sideImageXView.Location.X && imageView.Location.X < sideImageXView.Location.X + sideImageXView.Width)//&& imageView.Location.X + imageView.Width == sideImageXView.Location.X)
                {
                    sideImageXView.Visible = false;
                    sideLabel.Visible = false;
                }
                if (verticalImageXView.Visible == true && imageView.Location.Y + imageView.Height > verticalImageXView.Location.Y && imageView.Location.Y < verticalImageXView.Location.Y + verticalImageXView.Height)
                {
                    verticalImageXView.Visible = false;
                    diagonalImageXView.Visible = false;
                    verticalLabel.Visible = false;
                    diagonalLabel.Visible = false;
                    mainForm.DisplayCompressedImagePanelNoBorder();
                }

                Application.DoEvents();
                fSleep -= 0.3f;
                int nSleep = (int)fSleep;
                if (nSleep > 0)
                    System.Threading.Thread.Sleep(nSleep);
            }
        }

        private void Shrink(ImageXView sideImageXView, ImageXView verticalImageXView, ImageXView diagonalImageXView, Label sideLabel, Label verticalLabel, Label diagonalLabel)
        {
            imageView.BringToFront();
            verticalImageXView.Visible = true;
            diagonalImageXView.Visible = true;
            verticalLabel.Visible = true;
            diagonalLabel.Visible = true;

            // In 25 interations, how far left, right, up, and down
            // does the control need to be modified each iteration?
            int steps = 25;
            int destinationWidth = mainForm.GetThumbSize().Width;
            int destinationHeight = mainForm.GetThumbSize().Height;
            float changeInX = (float)thumbLocation.X / (float)steps;
            float changeInY = (float)thumbLocation.Y / (float)steps;
            float changeInWidth = (float)(destinationWidth - imageView.Width) / (float)steps;
            float changeInHeight = (float)(destinationHeight - imageView.Height) / (float)steps;

            PointF fLoc = imageView.Location;
            SizeF fSize = imageView.Size;
            float fSleep = 2.0f;
            for (int i = 0; i < steps; i++)
            {
                fLoc.X += changeInX;
                fLoc.Y += changeInY;
                fSize.Width += changeInWidth;
                fSize.Height += changeInHeight;
                Point p = new Point((int)(fLoc.X + 0.5), (int)(fLoc.Y + 0.5));
                imageView.Location = p;
                imageView.Width = (int)(fSize.Width + 0.5);
                imageView.Height = (int)(fSize.Height + 0.5);
                if (sideImageXView.Visible == false && i > 12)
                {
                    sideLabel.Visible = true;
                    sideImageXView.Visible = true;
                }
                Application.DoEvents();
                fSleep += 0.3f;
                int nSleep = (int)fSleep;
                if (nSleep > 2)
                    nSleep = 2;
                if (nSleep > 0)
                    System.Threading.Thread.Sleep(nSleep);
            }

            labelName.Visible = true;

            imageView.Location = thumbLocation;
            imageView.Size = mainForm.GetThumbSize();
        }

        #endregion

        #region Get and Set Compressed Image Data

        public void UpdateSaveOptionsColor(SaveOptions newSaveOptions)
        {
            saveOptionsColor = newSaveOptions;
        }

        public void UpdateSaveOptionsBW(SaveOptions newSaveOptions)
        {
            saveOptionsBW = newSaveOptions;
        }

        public void UpdateSaveOptionsGrayscale(SaveOptions newSaveOptions)
        {
            saveOptionsGrayscale = newSaveOptions;
        }

        public SaveOptions GetSaveOptionsColor()
        {
            return saveOptionsColor;
        }

        public SaveOptions GetSaveOptionsBW()
        {
            return saveOptionsBW;
        }

        public SaveOptions GetSaveOptionsGrayscale()
        {
            return saveOptionsGrayscale;
        }

        public int GetWhichCompressedImage()
        {
            return tabIndex;
        }

        public double GetCompressedToActualRatio()
        {
            return compressedToActualRatio;
        }

        public void SetThumbLocation(Point newLocation)
        {
            thumbLocation = newLocation;
        }

        #endregion

        #region Read and Write Compression Settings

        public void ReadColorCompressionSettings(XmlReader fileReader)
        {
            fileReader.Read();
            fileReader.MoveToContent();
            switch (fileReader.Name)
            {
                case "Exif":
                    saveOptionsColor.Format = ImageXFormat.Exif;
                    saveOptionsColor.Exif.ThumbnailSize = Convert.ToInt32(fileReader.GetAttribute("ThumbnailSize"));
                    break;
                case "Gif":
                    {
                        saveOptionsColor.Format = ImageXFormat.Gif;
                        saveOptionsColor.Gif.Interlaced = Convert.ToBoolean(fileReader.GetAttribute("Interlaced"));
                        TransparencyMatch theTransparencyMatch = TransparencyMatch.None;
                        switch (fileReader.GetAttribute("TransparencyMatch"))
                        {
                            case "None":
                                theTransparencyMatch = TransparencyMatch.None;
                                break;
                            case "Closest":
                                theTransparencyMatch = TransparencyMatch.Closest;
                                break;
                            case "Exact":
                                theTransparencyMatch = TransparencyMatch.Exact;
                                break;
                        }
                        saveOptionsColor.Gif.TransparencyMatch = theTransparencyMatch;
                        GifType theType = GifType.Gif87a;
                        switch (fileReader.GetAttribute("Type"))
                        {
                            case "Gif87a":
                                theType = GifType.Gif87a;
                                break;
                            case "Gif89a":
                                theType = GifType.Gif89a;
                                break;
                        }
                        saveOptionsColor.Gif.Type = theType;
                        fileReader.ReadToDescendant("TransparencyColor");
                        saveOptionsColor.Gif.TransparencyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(fileReader.GetAttribute("Red")), Convert.ToInt32(fileReader.GetAttribute("Green")), Convert.ToInt32(fileReader.GetAttribute("Blue")));
                        fileReader.Read();
                        fileReader.MoveToContent();
                        break;
                    }
                case "HdPhoto":
                    {
                        saveOptionsColor.Format = ImageXFormat.HdPhoto;

                        HdpSubSampling theChromaSubSampling = HdpSubSampling.ChromaSubSampling400;
                        switch (fileReader.GetAttribute("ChromaSubSampling"))
                        {
                            case "ChromaSubSampling400":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling400;
                                break;
                            case "ChromaSubSampling420":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling420;
                                break;
                            case "ChromaSubSampling422":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling422;
                                break;
                            case "ChromaSubSampling444":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling444;
                                break;
                        }
                        saveOptionsColor.Hdp.ChromaSubSampling = theChromaSubSampling;
                        saveOptionsColor.Hdp.FrequencyOrder = Convert.ToInt32(fileReader.GetAttribute("Order"));
                        saveOptionsColor.Hdp.Quantization = Convert.ToInt32(fileReader.GetAttribute("Quantization"));
                        break;
                    }
                case "Jpeg":
                    {
                        saveOptionsColor.Format = ImageXFormat.Jpeg;
                        saveOptionsColor.Jpeg.Chrominance = Convert.ToInt32(fileReader.GetAttribute("Chrominance"));
                        saveOptionsColor.Jpeg.Luminance = Convert.ToInt32(fileReader.GetAttribute("Luminance"));
                        saveOptionsColor.Jpeg.Cosited = Convert.ToBoolean(fileReader.GetAttribute("Cosited"));
                        SubSampling theSubSampling = SubSampling.SubSampling411;
                        switch (fileReader.GetAttribute("SubSampling"))
                        {
                            case "SubSampling111":
                                theSubSampling = SubSampling.SubSampling111;
                                break;
                            case "SubSampling211":
                                theSubSampling = SubSampling.SubSampling211;
                                break;
                            case "SubSampling411":
                                theSubSampling = SubSampling.SubSampling411;
                                break;
                            case "SubSampling211v":
                                theSubSampling = SubSampling.SubSampling211v;
                                break;
                        }
                        saveOptionsColor.Jpeg.SubSampling = theSubSampling;
                        break;
                    }
                case "Jpeg2000":
                    {
                        saveOptionsColor.Format = ImageXFormat.Jpeg2000;
                        Jp2Type theType = Jp2Type.Lossy;
                        switch (fileReader.GetAttribute("Type"))
                        {
                            case "Lossy":
                                theType = Jp2Type.Lossy;
                                break;
                            case "Lossless":
                                theType = Jp2Type.Lossless;
                                break;
                        }
                        saveOptionsColor.Jp2.Type = theType;
                        saveOptionsColor.Jp2.CompressSize = Convert.ToInt32(fileReader.GetAttribute("CompressSize"));
                        saveOptionsColor.Jp2.PeakSignalToNoiseRatio = Convert.ToDouble(fileReader.GetAttribute("PeakSignalToNoiseRatio"));
                        ProgressionOrder theOrder = ProgressionOrder.Default;
                        switch (fileReader.GetAttribute("Order"))
                        {
                            case "Default":
                                theOrder = ProgressionOrder.Default;
                                break;
                            case "ComponentPositionResolutionLayer":
                                theOrder = ProgressionOrder.ComponentPositionResolutionLayer;
                                break;
                            case "LayerResolutionComponentPosition":
                                theOrder = ProgressionOrder.LayerResolutionComponentPosition;
                                break;
                            case "PositionComponentResolutionLayer":
                                theOrder = ProgressionOrder.PositionComponentResolutionLayer;
                                break;
                            case "ResolutionLayerComponentPosition":
                                theOrder = ProgressionOrder.ResolutionLayerComponentPosition;
                                break;
                            case "ResolutionPositionComponentLayer":
                                theOrder = ProgressionOrder.ResolutionPositionComponentLayer;
                                break;
                        }
                        saveOptionsColor.Jp2.Order = theOrder;
                        fileReader.ReadToDescendant("TileSize");
                        saveOptionsColor.Jp2.TileSize = new Size(Convert.ToInt32(fileReader.GetAttribute("Width")), Convert.ToInt32(fileReader.GetAttribute("Height")));
                        fileReader.Read();
                        fileReader.MoveToContent();
                        break;
                    }
                case "JpegLs":
                    saveOptionsColor.Format = ImageXFormat.JpegLs;
                    saveOptionsColor.Jls.Interleave = Convert.ToInt32(fileReader.GetAttribute("Interleave"));
                    saveOptionsColor.Jls.MaxValue = Convert.ToInt32(fileReader.GetAttribute("MaxValue"));
                    saveOptionsColor.Jls.Near = Convert.ToInt32(fileReader.GetAttribute("Near"));
                    saveOptionsColor.Jls.Point = Convert.ToInt32(fileReader.GetAttribute("Point"));
                    break;
                case "LosslessJpeg":
                    {
                        saveOptionsColor.Format = ImageXFormat.LosslessJpeg;
                        LjpMethod theMethod = LjpMethod.Jpeg;
                        switch (fileReader.GetAttribute("TransparencyMatch"))
                        {
                            case "Jpeg":
                                theMethod = LjpMethod.Jpeg;
                                break;
                            case "Loco":
                                theMethod = LjpMethod.Loco;
                                break;
                            case "Ppmd":
                                theMethod = LjpMethod.Ppmd;
                                break;
                        }
                        saveOptionsColor.Ljp.Method = theMethod;
                        saveOptionsColor.Ljp.Order = Convert.ToInt32(fileReader.GetAttribute("Order"));
                        saveOptionsColor.Ljp.Predictor = Convert.ToInt32(fileReader.GetAttribute("Predictor"));
                        break;
                    }
                case "Pcx":
                    saveOptionsColor.Format = ImageXFormat.Pcx;
                    break;
                case "Pdf":
                    {
                        saveOptionsColor.Format = ImageXFormat.Pdf;
                        saveOptionsColor.Pdf.Compression = Compression.Jpeg;
                        break;
                    }
                case "Png":
                    {
                        saveOptionsColor.Format = ImageXFormat.Png;
                        saveOptionsColor.Png.Interlaced = Convert.ToBoolean(fileReader.GetAttribute("Interlaced"));
                        TransparencyMatch theTransparencyMatch = TransparencyMatch.None;
                        switch (fileReader.GetAttribute("TransparencyMatch"))
                        {
                            case "None":
                                theTransparencyMatch = TransparencyMatch.None;
                                break;
                            case "Closest":
                                theTransparencyMatch = TransparencyMatch.Closest;
                                break;
                            case "Exact":
                                theTransparencyMatch = TransparencyMatch.Exact;
                                break;
                        }
                        saveOptionsColor.Png.TransparencyMatch = theTransparencyMatch;
                        fileReader.ReadToDescendant("TransparencyColor");
                        saveOptionsColor.Png.TransparencyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(fileReader.GetAttribute("Red")), Convert.ToInt32(fileReader.GetAttribute("Green")), Convert.ToInt32(fileReader.GetAttribute("Blue")));
                        fileReader.Read();
                        fileReader.MoveToContent();
                        break;
                    }
                case "Tiff":
                    {
                        saveOptionsColor.Format = ImageXFormat.Tiff;
                        saveOptionsColor.Tiff.Compression = Compression.Jpeg;
                        break;
                    }
            }
        }

        public void ReadGrayscaleCompressionSettings(XmlReader fileReader)
        {
            fileReader.Read();
            fileReader.MoveToContent();
            switch (fileReader.Name)
            {
                case "Exif":
                    saveOptionsGrayscale.Format = ImageXFormat.Exif;
                    saveOptionsGrayscale.Exif.ThumbnailSize = Convert.ToInt32(fileReader.GetAttribute("ThumbnailSize"));
                    break;
                case "HdPhoto":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.HdPhoto;

                        HdpSubSampling theChromaSubSampling = HdpSubSampling.ChromaSubSampling400;
                        switch (fileReader.GetAttribute("ChromaSubSampling"))
                        {
                            case "ChromaSubSampling400":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling400;
                                break;
                            case "ChromaSubSampling420":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling420;
                                break;
                            case "ChromaSubSampling422":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling422;
                                break;
                            case "ChromaSubSampling444":
                                theChromaSubSampling = HdpSubSampling.ChromaSubSampling444;
                                break;
                        }
                        saveOptionsGrayscale.Hdp.ChromaSubSampling = theChromaSubSampling;
                        saveOptionsGrayscale.Hdp.FrequencyOrder = Convert.ToInt32(fileReader.GetAttribute("Order"));
                        saveOptionsGrayscale.Hdp.Quantization = Convert.ToInt32(fileReader.GetAttribute("Quantization"));
                        break;
                    }
                case "Jpeg":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.Jpeg;
                        saveOptionsGrayscale.Jpeg.Chrominance = Convert.ToInt32(fileReader.GetAttribute("Chrominance"));
                        saveOptionsGrayscale.Jpeg.Luminance = Convert.ToInt32(fileReader.GetAttribute("Luminance"));
                        saveOptionsGrayscale.Jpeg.Cosited = Convert.ToBoolean(fileReader.GetAttribute("Cosited"));
                        SubSampling theSubSampling = SubSampling.SubSampling411;
                        switch (fileReader.GetAttribute("SubSampling"))
                        {
                            case "SubSampling111":
                                theSubSampling = SubSampling.SubSampling111;
                                break;
                            case "SubSampling211":
                                theSubSampling = SubSampling.SubSampling211;
                                break;
                            case "SubSampling411":
                                theSubSampling = SubSampling.SubSampling411;
                                break;
                            case "SubSampling211v":
                                theSubSampling = SubSampling.SubSampling211v;
                                break;
                        }
                        saveOptionsGrayscale.Jpeg.SubSampling = theSubSampling;
                        break;
                    }
                case "Jpeg2000":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.Jpeg2000;
                        Jp2Type theType = Jp2Type.Lossy;
                        switch (fileReader.GetAttribute("Type"))
                        {
                            case "Lossy":
                                theType = Jp2Type.Lossy;
                                break;
                            case "Lossless":
                                theType = Jp2Type.Lossless;
                                break;
                        }
                        saveOptionsGrayscale.Jp2.Type = theType;
                        saveOptionsGrayscale.Jp2.CompressSize = Convert.ToInt32(fileReader.GetAttribute("CompressSize"));
                        saveOptionsGrayscale.Jp2.PeakSignalToNoiseRatio = Convert.ToDouble(fileReader.GetAttribute("PeakSignalToNoiseRatio"));
                        ProgressionOrder theOrder = ProgressionOrder.Default;
                        switch (fileReader.GetAttribute("Order"))
                        {
                            case "Default":
                                theOrder = ProgressionOrder.Default;
                                break;
                            case "ComponentPositionResolutionLayer":
                                theOrder = ProgressionOrder.ComponentPositionResolutionLayer;
                                break;
                            case "LayerResolutionComponentPosition":
                                theOrder = ProgressionOrder.LayerResolutionComponentPosition;
                                break;
                            case "PositionComponentResolutionLayer":
                                theOrder = ProgressionOrder.PositionComponentResolutionLayer;
                                break;
                            case "ResolutionLayerComponentPosition":
                                theOrder = ProgressionOrder.ResolutionLayerComponentPosition;
                                break;
                            case "ResolutionPositionComponentLayer":
                                theOrder = ProgressionOrder.ResolutionPositionComponentLayer;
                                break;
                        }
                        saveOptionsGrayscale.Jp2.Order = theOrder;
                        fileReader.ReadToDescendant("TileSize");
                        saveOptionsGrayscale.Jp2.TileSize = new Size(Convert.ToInt32(fileReader.GetAttribute("Width")), Convert.ToInt32(fileReader.GetAttribute("Height")));
                        fileReader.Read();
                        fileReader.MoveToContent();
                        break;
                    }
                case "JpegLs":
                    saveOptionsGrayscale.Format = ImageXFormat.JpegLs;
                    saveOptionsGrayscale.Jls.Interleave = Convert.ToInt32(fileReader.GetAttribute("Interleave"));
                    saveOptionsGrayscale.Jls.MaxValue = Convert.ToInt32(fileReader.GetAttribute("MaxValue"));
                    saveOptionsGrayscale.Jls.Near = Convert.ToInt32(fileReader.GetAttribute("Near"));
                    saveOptionsGrayscale.Jls.Point = Convert.ToInt32(fileReader.GetAttribute("Point"));
                    break;
                case "LosslessJpeg":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.LosslessJpeg;
                        LjpMethod theMethod = LjpMethod.Jpeg;
                        switch (fileReader.GetAttribute("TransparencyMatch"))
                        {
                            case "Jpeg":
                                theMethod = LjpMethod.Jpeg;
                                break;
                            case "Loco":
                                theMethod = LjpMethod.Loco;
                                break;
                            case "Ppmd":
                                theMethod = LjpMethod.Ppmd;
                                break;
                        }
                        saveOptionsGrayscale.Ljp.Method = theMethod;
                        saveOptionsGrayscale.Ljp.Order = Convert.ToInt32(fileReader.GetAttribute("Order"));
                        saveOptionsGrayscale.Ljp.Predictor = Convert.ToInt32(fileReader.GetAttribute("Predictor"));
                        break;
                    }
                case "Pdf":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.Pdf;
                        saveOptionsGrayscale.Pdf.Compression = Compression.Jpeg;
                        break;
                    }
                case "Tiff":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.Tiff;
                        saveOptionsGrayscale.Tiff.Compression = Compression.Jpeg;
                        break;
                    }
                case "Wsq":
                    {
                        saveOptionsGrayscale.Format = ImageXFormat.Wsq;
                        saveOptionsGrayscale.Wsq.Black = Convert.ToInt32(fileReader.GetAttribute("Black"));
                        saveOptionsGrayscale.Wsq.White = Convert.ToInt32(fileReader.GetAttribute("White"));
                        saveOptionsGrayscale.Wsq.Quantization = Convert.ToDouble(fileReader.GetAttribute("Quantization"));
                        break;
                    }
            }
        }

        public void ReadBWCompressionSettings(XmlReader fileReader)
        {
            fileReader.Read();
            fileReader.MoveToContent();
            switch (fileReader.Name)
            {
                case "Cals":
                    saveOptionsBW.Format = ImageXFormat.Cals;
                    break;
                case "Jbig2":
                    {
                        saveOptionsBW.Format = ImageXFormat.Jbig2;
                        Jbig2EncodeModeCompression theEncodeModeCompression = Jbig2EncodeModeCompression.AutoDetect;
                        switch (fileReader.GetAttribute("EncodeModeCompression"))
                        {
                            case "AutoDetect":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.AutoDetect;
                                break;
                            case "LosslessGenericMmr":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LosslessGenericMmr;
                                break;
                            case "LosslessGenericMq":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LosslessGenericMq;
                                break;
                            case "LosslessTextMmr":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextMmr;
                                break;
                            case "LosslessTextMq":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextMq;
                                break;
                            case "LosslessTextSpmMmr":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextSpmMmr;
                                break;
                            case "LosslesstextSpmMq":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextSpmMq;
                                break;
                            case "LossyHalftoneMmr":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LossyHalftoneMmr;
                                break;
                            case "LossyHalftoneMq":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LossyHalftoneMq;
                                break;
                            case "LossyTextMmr":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LossyTextMmr;
                                break;
                            case "LossyTextMq":
                                theEncodeModeCompression = Jbig2EncodeModeCompression.LossyTextMq;
                                break;
                        }
                        saveOptionsBW.Jbig2.EncodeModeCompression = theEncodeModeCompression;
                        Jbig2FileOrganization theFileOrganization = Jbig2FileOrganization.Default;
                        switch (fileReader.GetAttribute("EncodeModeCompression"))
                        {
                            case "Default":
                                theFileOrganization = Jbig2FileOrganization.Default;
                                break;
                            case "RandomAccess":
                                theFileOrganization = Jbig2FileOrganization.RandomAccess;
                                break;
                            case "Sequential":
                                theFileOrganization = Jbig2FileOrganization.Sequential;
                                break;
                        }
                        saveOptionsBW.Jbig2.FileOrganization = theFileOrganization;
                        saveOptionsBW.Jbig2.InvertedRegion = Convert.ToBoolean(fileReader.GetAttribute("InvertedRegion"));
                        saveOptionsBW.Jbig2.LoosenessCompression = Convert.ToInt32(fileReader.GetAttribute("LoosnessCompression"));
                        break;
                    }
                case "Modca":
                    saveOptionsBW.Format = ImageXFormat.Modca;
                    break;
                case "Pdf":
                    {
                        saveOptionsBW.Format = ImageXFormat.Pdf;
                        saveOptionsBW.Pdf.SwapBlackAndWhite = Convert.ToBoolean(fileReader.GetAttribute("SwapBlackAndWhite"));
                        Compression theCompression = Compression.Jpeg;
                        switch (fileReader.GetAttribute("Compression"))
                        {
                            case "Deflate":
                                theCompression = Compression.Deflate;
                                break;
                            case "Group3Fax1d":
                                theCompression = Compression.Group3Fax1d;
                                break;
                            case "Group3Fax2d":
                                theCompression = Compression.Group3Fax2d;
                                break;
                            case "Group4":
                                theCompression = Compression.Group4;
                                break;
                            case "Jbig2":
                                theCompression = Compression.Jbig2;
                                break;
                            case "Jpeg":
                                theCompression = Compression.Jpeg;
                                break;
                            case "Jpeg2000":
                                theCompression = Compression.Jpeg2000;
                                break;
                            case "Jpeg7":
                                theCompression = Compression.Jpeg7;
                                break;
                            case "Lzw":
                                theCompression = Compression.Lzw;
                                break;
                            case "Native":
                                theCompression = Compression.Native;
                                break;
                            case "NoCompression":
                                theCompression = Compression.NoCompression;
                                break;
                            case "PackBits":
                                theCompression = Compression.PackBits;
                                break;
                            case "Rle":
                                theCompression = Compression.Rle;
                                break;
                            case "Unknown":
                                theCompression = Compression.Unknown;
                                break;
                        }
                        saveOptionsBW.Pdf.Compression = theCompression;
                        break;
                    }
                case "Tiff":
                    {
                        saveOptionsBW.Format = ImageXFormat.Tiff;
                        Compression theCompression = Compression.Jpeg;
                        switch (fileReader.GetAttribute("Compression"))
                        {
                            case "Deflate":
                                theCompression = Compression.Deflate;
                                break;
                            case "Group3Fax1d":
                                theCompression = Compression.Group3Fax1d;
                                break;
                            case "Group3Fax2d":
                                theCompression = Compression.Group3Fax2d;
                                break;
                            case "Group4":
                                theCompression = Compression.Group4;
                                break;
                            case "Jbig2":
                                theCompression = Compression.Jbig2;
                                break;
                            case "Jpeg":
                                theCompression = Compression.Jpeg;
                                break;
                            case "Jpeg2000":
                                theCompression = Compression.Jpeg2000;
                                break;
                            case "Jpeg7":
                                theCompression = Compression.Jpeg7;
                                break;
                            case "Lzw":
                                theCompression = Compression.Lzw;
                                break;
                            case "Native":
                                theCompression = Compression.Native;
                                break;
                            case "NoCompression":
                                theCompression = Compression.NoCompression;
                                break;
                            case "PackBits":
                                theCompression = Compression.PackBits;
                                break;
                            case "Rle":
                                theCompression = Compression.Rle;
                                break;
                            case "Unknown":
                                theCompression = Compression.Unknown;
                                break;
                        }
                        saveOptionsBW.Tiff.Compression = theCompression;
                        break;
                    }
            }
        }

        public void WriteColorCompressionSettings(XmlWriter fileWriter)
        {
            fileWriter.WriteStartElement(saveOptionsColor.Format.ToString());
            switch (saveOptionsColor.Format)
            {
                case ImageXFormat.Exif:
                    fileWriter.WriteAttributeString("ThumbnailSize", saveOptionsColor.Exif.ThumbnailSize.ToString());
                    break;
                case ImageXFormat.Gif:
                    fileWriter.WriteAttributeString("Interlaced", saveOptionsColor.Gif.Interlaced.ToString());
                    fileWriter.WriteAttributeString("TransparencyMatch", saveOptionsColor.Gif.TransparencyMatch.ToString());
                    fileWriter.WriteAttributeString("Type", saveOptionsColor.Gif.Type.ToString());
                    fileWriter.WriteStartElement("TransparencyColor");
                    fileWriter.WriteAttributeString("Red", saveOptionsColor.Gif.TransparencyColor.R.ToString());
                    fileWriter.WriteAttributeString("Green", saveOptionsColor.Gif.TransparencyColor.G.ToString());
                    fileWriter.WriteAttributeString("Blue", saveOptionsColor.Gif.TransparencyColor.B.ToString());
                    fileWriter.WriteEndElement();
                    break;
                case ImageXFormat.HdPhoto:
                    fileWriter.WriteAttributeString("ChromaSubSampling", saveOptionsColor.Hdp.ChromaSubSampling.ToString());
                    fileWriter.WriteAttributeString("Order", saveOptionsColor.Hdp.FrequencyOrder.ToString());
                    fileWriter.WriteAttributeString("Quantization", saveOptionsColor.Hdp.Quantization.ToString());
                    break;
                case ImageXFormat.Jpeg:
                    fileWriter.WriteAttributeString("Chrominance", saveOptionsColor.Jpeg.Chrominance.ToString());
                    fileWriter.WriteAttributeString("Luminance", saveOptionsColor.Jpeg.Luminance.ToString());
                    fileWriter.WriteAttributeString("Cosited", saveOptionsColor.Jpeg.Cosited.ToString());
                    fileWriter.WriteAttributeString("SubSampling", saveOptionsColor.Jpeg.SubSampling.ToString());
                    break;
                case ImageXFormat.Jpeg2000:
                    fileWriter.WriteAttributeString("Type", saveOptionsColor.Jp2.Type.ToString());
                    fileWriter.WriteAttributeString("CompressSize", saveOptionsColor.Jp2.CompressSize.ToString());
                    fileWriter.WriteAttributeString("PeakSignalToNoiseRatio", saveOptionsColor.Jp2.PeakSignalToNoiseRatio.ToString());
                    fileWriter.WriteAttributeString("Order", saveOptionsColor.Jp2.Order.ToString());
                    fileWriter.WriteStartElement("TileSize");
                    fileWriter.WriteAttributeString("Width", saveOptionsColor.Jp2.TileSize.Width.ToString());
                    fileWriter.WriteAttributeString("Height", saveOptionsColor.Jp2.TileSize.Height.ToString());
                    fileWriter.WriteEndElement();
                    break;
                case ImageXFormat.JpegLs:
                    fileWriter.WriteAttributeString("Interleave", saveOptionsColor.Jls.Interleave.ToString());
                    fileWriter.WriteAttributeString("MaxValue", saveOptionsColor.Jls.MaxValue.ToString());
                    fileWriter.WriteAttributeString("Near", saveOptionsColor.Jls.Near.ToString());
                    fileWriter.WriteAttributeString("Point", saveOptionsColor.Jls.Point.ToString());
                    break;
                case ImageXFormat.LosslessJpeg:
                    fileWriter.WriteAttributeString("Method", saveOptionsColor.Ljp.Method.ToString());
                    fileWriter.WriteAttributeString("Order", saveOptionsColor.Ljp.Order.ToString());
                    fileWriter.WriteAttributeString("Predictor", saveOptionsColor.Ljp.Predictor.ToString());
                    break;
                case ImageXFormat.Pcx:
                    break;
                case ImageXFormat.Pdf:
                    fileWriter.WriteAttributeString("Compression", saveOptionsColor.Pdf.Compression.ToString());
                    break;
                case ImageXFormat.Png:
                    fileWriter.WriteAttributeString("Interlaced", saveOptionsColor.Png.Interlaced.ToString());
                    fileWriter.WriteAttributeString("TransparencyMatch", saveOptionsColor.Png.TransparencyMatch.ToString());
                    fileWriter.WriteStartElement("TransparencyColor");
                    fileWriter.WriteAttributeString("Red", saveOptionsColor.Png.TransparencyColor.R.ToString());
                    fileWriter.WriteAttributeString("Green", saveOptionsColor.Png.TransparencyColor.G.ToString());
                    fileWriter.WriteAttributeString("Blue", saveOptionsColor.Png.TransparencyColor.B.ToString());
                    fileWriter.WriteEndElement();
                    break;
                case ImageXFormat.Tiff:
                    fileWriter.WriteAttributeString("Compression", saveOptionsColor.Tiff.Compression.ToString());
                    break;
            }
            fileWriter.WriteEndElement();
        }

        public void WriteGrayscaleCompressionSettings(XmlWriter fileWriter)
        {
            fileWriter.WriteStartElement(saveOptionsGrayscale.Format.ToString());
            switch (saveOptionsGrayscale.Format)
            {
                case ImageXFormat.Exif:
                    fileWriter.WriteAttributeString("ThumbnailSize", saveOptionsGrayscale.Exif.ThumbnailSize.ToString());
                    break;
                case ImageXFormat.HdPhoto:
                    fileWriter.WriteAttributeString("ChromaSubSampling", saveOptionsGrayscale.Hdp.ChromaSubSampling.ToString());
                    fileWriter.WriteAttributeString("Order", saveOptionsGrayscale.Hdp.FrequencyOrder.ToString());
                    fileWriter.WriteAttributeString("Quantization", saveOptionsGrayscale.Hdp.Quantization.ToString());
                    break;
                case ImageXFormat.Jpeg:
                    fileWriter.WriteAttributeString("Chrominance", saveOptionsGrayscale.Jpeg.Chrominance.ToString());
                    fileWriter.WriteAttributeString("Luminance", saveOptionsGrayscale.Jpeg.Luminance.ToString());
                    fileWriter.WriteAttributeString("Cosited", saveOptionsGrayscale.Jpeg.Cosited.ToString());
                    fileWriter.WriteAttributeString("SubSampling", saveOptionsGrayscale.Jpeg.SubSampling.ToString());
                    break;
                case ImageXFormat.Jpeg2000:
                    fileWriter.WriteAttributeString("Type", saveOptionsGrayscale.Jp2.Type.ToString());
                    fileWriter.WriteAttributeString("CompressSize", saveOptionsGrayscale.Jp2.CompressSize.ToString());
                    fileWriter.WriteAttributeString("PeakSignalToNoiseRatio", saveOptionsGrayscale.Jp2.PeakSignalToNoiseRatio.ToString());
                    fileWriter.WriteAttributeString("Order", saveOptionsGrayscale.Jp2.Order.ToString());
                    fileWriter.WriteStartElement("TileSize");
                    fileWriter.WriteAttributeString("Width", saveOptionsGrayscale.Jp2.TileSize.Width.ToString());
                    fileWriter.WriteAttributeString("Height", saveOptionsGrayscale.Jp2.TileSize.Height.ToString());
                    fileWriter.WriteEndElement();
                    break;
                case ImageXFormat.JpegLs:
                    fileWriter.WriteAttributeString("Interleave", saveOptionsGrayscale.Jls.Interleave.ToString());
                    fileWriter.WriteAttributeString("MaxValue", saveOptionsGrayscale.Jls.MaxValue.ToString());
                    fileWriter.WriteAttributeString("Near", saveOptionsGrayscale.Jls.Near.ToString());
                    fileWriter.WriteAttributeString("Point", saveOptionsGrayscale.Jls.Point.ToString());
                    break;
                case ImageXFormat.LosslessJpeg:
                    fileWriter.WriteAttributeString("Method", saveOptionsGrayscale.Ljp.Method.ToString());
                    fileWriter.WriteAttributeString("Order", saveOptionsGrayscale.Ljp.Order.ToString());
                    fileWriter.WriteAttributeString("Predictor", saveOptionsGrayscale.Ljp.Predictor.ToString());
                    break;
                case ImageXFormat.Pdf:
                    fileWriter.WriteAttributeString("Compression", saveOptionsGrayscale.Pdf.Compression.ToString());
                    break;
                case ImageXFormat.Tiff:
                    fileWriter.WriteAttributeString("Compression", saveOptionsGrayscale.Tiff.Compression.ToString());
                    break;
                case ImageXFormat.Wsq:
                    fileWriter.WriteAttributeString("Black", saveOptionsGrayscale.Wsq.Black.ToString());
                    fileWriter.WriteAttributeString("White", saveOptionsGrayscale.Wsq.White.ToString());
                    fileWriter.WriteAttributeString("Quantization", saveOptionsGrayscale.Wsq.Quantization.ToString());
                    break;
            }
            fileWriter.WriteEndElement();
        }

        public void WriteBWCompressionSettings(XmlWriter fileWriter)
        {
            fileWriter.WriteStartElement(saveOptionsBW.Format.ToString());
            switch (saveOptionsBW.Format)
            {
                case ImageXFormat.Cals:
                    break;
                case ImageXFormat.Jbig2:
                    fileWriter.WriteAttributeString("EncodeModeCompression", saveOptionsBW.Jbig2.EncodeModeCompression.ToString());
                    fileWriter.WriteAttributeString("FileOrganization", saveOptionsBW.Jbig2.FileOrganization.ToString());
                    fileWriter.WriteAttributeString("InvertedRegion", saveOptionsBW.Jbig2.InvertedRegion.ToString());
                    fileWriter.WriteAttributeString("LoosenessCompression", saveOptionsBW.Jbig2.LoosenessCompression.ToString());
                    break;
                case ImageXFormat.Modca:
                    break;
                case ImageXFormat.Pdf:
                    fileWriter.WriteAttributeString("SwapBlackAndWhite", saveOptionsBW.Pdf.SwapBlackAndWhite.ToString());
                    fileWriter.WriteAttributeString("Compression", saveOptionsBW.Pdf.Compression.ToString());
                    break;
                case ImageXFormat.Tiff:
                    fileWriter.WriteAttributeString("Compression", saveOptionsBW.Tiff.Compression.ToString());
                    break;
            }
            fileWriter.WriteEndElement();
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// Accusoft Pegasus 
using Accusoft.ImagXpressSdk;
using Accusoft.NotateXpressSdk;
using Accusoft.ThumbnailXpressSdk;


namespace ImagXpressViewerDemo
{
    public partial class FormConfigThumb : Form
    {
        private ThumbnailItem thumbAdd;
        private ThumbnailItem thumbAdd2;
        private ThumbnailItem thumbAdd3;
        private ThumbnailItem thumbAdd4;
        private ThumbnailXpress thumbToSave;
        private ImageXView viewToSave;

        public FormConfigThumb()
        {
            InitializeComponent();
            try
            {
                AccusoftUnlocks.UnlockRuntimes(null, thumbnailXpressSample, null);
            }
            catch
            {
                MessageBox.Show("Invalid Unlock Codes, See the UnlockRuntime calls");
            }
        }



        private void FormConfigThumb_Load(object sender, EventArgs e)
        {
            // Shows taking an embedded image, and streaming it into the thumbnail control.
            System.Reflection.Assembly thisExe;
            thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.NewOrleans.jpg");
            thumbAdd = thumbnailXpressSample.CreateThumbnailItem("", 1, ThumbnailType.Image);
            thumbAdd.UserTag = "1";
            thumbAdd.FromStream(embeddedFile);
            thumbAdd.Filename = "New Orleans.jpg";
            thumbnailXpressSample.Items.AddItemFromStream(thumbAdd);

            embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.Japan.jpg");
            thumbAdd2 = thumbnailXpressSample.CreateThumbnailItem("", 1, ThumbnailType.Image);
            thumbAdd2.UserTag = "2";
            thumbAdd2.FromStream(embeddedFile);
            thumbAdd2.Filename = "Japan.jpg";
            thumbnailXpressSample.Items.AddItemFromStream(thumbAdd2);

            embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.Nature1.jpg");
            thumbAdd3 = thumbnailXpressSample.CreateThumbnailItem("", 1, ThumbnailType.Image);
            thumbAdd3.UserTag = "3";
            thumbAdd3.FromStream(embeddedFile);
            thumbAdd3.Filename = "Nature1.jpg";
            thumbnailXpressSample.Items.AddItemFromStream(thumbAdd3);

            embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.Nature2.jpg");
            thumbAdd4 = thumbnailXpressSample.CreateThumbnailItem("", 1, ThumbnailType.Image);
            thumbAdd4.UserTag = "4";
            thumbAdd4.FromStream(embeddedFile);
            thumbAdd4.Filename = "Nature2.jpg";
            thumbnailXpressSample.Items.AddItemFromStream(thumbAdd4);

            // Select the first icon for looks.
            thumbnailXpressSample.SelectedItems.Add(thumbnailXpressSample.Items[0]);

        }

        public void loadExistingView(ImageXView viewToTweak)
        {
            viewToSave = viewToTweak;
        }

        public void loadExistingThumb(ThumbnailXpress thumbToTweak)
        {
            thumbToSave = thumbToTweak;

            // match the color schemes
            thumbnailXpressSample.CellBorderWidth = thumbToSave.CellBorderWidth;
            thumbnailXpressSample.SelectBackColor = thumbToSave.SelectBackColor;
            thumbnailXpressSample.CellBorderColor = thumbToSave.CellBorderColor;
            thumbnailXpressSample.CellSpacingColor = thumbToSave.CellSpacingColor;
            thumbnailXpressSample.BackColor = thumbToSave.BackColor;
            thumbnailXpressSample.BorderStyle = thumbToSave.BorderStyle;
            thumbnailXpressSample.CameraRaw = thumbToSave.CameraRaw;
            thumbnailXpressSample.SelectionMode = thumbToSave.SelectionMode;
            thumbnailXpressSample.CellHeight = thumbToSave.CellHeight;
            thumbnailXpressSample.CellWidth = thumbToSave.CellWidth;
            thumbnailXpressSample.Font = thumbToSave.Font;
            thumbnailXpressSample.ShowImagePlaceholders = thumbToSave.ShowImagePlaceholders;


            // set the data that we are allowing the user to configure
            // NOTE: there are MANY more options then presented
            numericUpDownBorderWidth.Value = thumbToSave.CellBorderWidth;
            buttonSelectedCellColor.BackColor = thumbToSave.SelectBackColor;
            buttonBorderColor.BackColor = thumbToSave.CellBorderColor;
            buttonCellSpacingColor.BackColor = thumbToSave.CellSpacingColor;
            buttonBackColor.BackColor = thumbToSave.BackColor;
            numericUpDownCellWidth.Value = thumbToSave.CellWidth;
            numericUpDownCellHeight.Value = thumbToSave.CellHeight;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            // don't save anything at all.
            Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            System.Int32 posChange;

            // Change the view size if the thumb size changed.
            // horizontal == filmstrip
            if (thumbToSave.ScrollDirection == ScrollDirection.Horizontal)
            {
                if (viewToSave != null)
                {
                    viewToSave.Height += (thumbToSave.CellHeight - thumbnailXpressSample.CellHeight);
                    viewToSave.Height += ((thumbToSave.CellBorderWidth - thumbnailXpressSample.CellBorderWidth) * 2);
                }
                posChange = (thumbToSave.CellHeight - thumbnailXpressSample.CellHeight);
                posChange += thumbToSave.Location.Y;
                posChange += ((thumbToSave.CellBorderWidth - thumbnailXpressSample.CellBorderWidth) * 2);
                thumbToSave.Location = new Point(thumbToSave.Location.X, posChange);
                thumbToSave.Height -= (thumbToSave.CellHeight - thumbnailXpressSample.CellHeight);
                thumbToSave.Height -= ((thumbToSave.CellBorderWidth - thumbnailXpressSample.CellBorderWidth) * 2);
            }
            // vertical == multipage
            else if (thumbToSave.ScrollDirection == ScrollDirection.Vertical)
            {
                if (viewToSave != null)
                {
                    viewToSave.Width += (thumbToSave.CellWidth - thumbnailXpressSample.CellWidth);
                    viewToSave.Width += ((thumbToSave.CellBorderWidth - thumbnailXpressSample.CellBorderWidth) * 2);
                    posChange = (thumbnailXpressSample.CellWidth - thumbToSave.CellWidth);
                    posChange += viewToSave.Location.X;
                    posChange -= ((thumbToSave.CellBorderWidth - thumbnailXpressSample.CellBorderWidth) * 2);
                    viewToSave.Location = new Point(posChange, viewToSave.Location.Y);
                }
                thumbToSave.Width -= (thumbToSave.CellWidth - thumbnailXpressSample.CellWidth);
                thumbToSave.Width -= ((thumbToSave.CellBorderWidth - thumbnailXpressSample.CellBorderWidth) * 2);
            }


            // get all settings back to the proper thumb control
            thumbToSave.SelectBackColor = thumbnailXpressSample.SelectBackColor;
            thumbToSave.CellBorderWidth = thumbnailXpressSample.CellBorderWidth;
            thumbToSave.CellBorderColor = thumbnailXpressSample.CellBorderColor;
            thumbToSave.CellSpacingColor = thumbnailXpressSample.CellSpacingColor;
            thumbToSave.BackColor = thumbnailXpressSample.BackColor;
            thumbToSave.CellWidth = thumbnailXpressSample.CellWidth;
            thumbToSave.CellHeight = thumbnailXpressSample.CellHeight;

            Close();
        }

        private void numericUpDownBorderWidth_ValueChanged(object sender, EventArgs e)
        {
            thumbnailXpressSample.CellBorderWidth = (int)numericUpDownBorderWidth.Value;
        }


        // take care that we loaded the test thumbnails from stream.
        // we may need to reload them again.
        private void thumbnailXpressSample_LoadFromStream(object sender, LoadFromStreamEventArgs e)
        {

            System.Reflection.Assembly thisExe;
            thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            if ((string)e.Item.UserTag == "1")
            {
                System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.NewOrleans.jpg");
                e.Item.FromStream(embeddedFile);
            }
            else if ((string)e.Item.UserTag == "2")
            {
                System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.Japan.jpg");
                e.Item.FromStream(embeddedFile);
            }
            else if ((string)e.Item.UserTag == "3")
            {
                System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.Nature1.jpg");
                e.Item.FromStream(embeddedFile);
            }
            else if ((string)e.Item.UserTag == "4")
            {
                System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.Nature2.jpg");
                e.Item.FromStream(embeddedFile);
            }
            else
                MessageBox.Show("Error in finding streamed thumbnail");
        }

        private void buttonSelectedCellColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = buttonSelectedCellColor.BackColor;
            colorDialog.AllowFullOpen = true;
            colorDialog.FullOpen = true;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                buttonSelectedCellColor.BackColor = colorDialog.Color;
                thumbnailXpressSample.SelectBackColor = colorDialog.Color;
            }
        }

        private void buttonBorderColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = buttonSelectedCellColor.BackColor;
            colorDialog.AllowFullOpen = true;
            colorDialog.FullOpen = true;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                buttonBorderColor.BackColor = colorDialog.Color;
                thumbnailXpressSample.CellBorderColor = colorDialog.Color;
            }

        }

        private void buttonCellSpacingColor_Click(object sender, EventArgs e)
        {
            // CellSpacingColor
            colorDialog.Color = buttonCellSpacingColor.BackColor;
            colorDialog.AllowFullOpen = true;
            colorDialog.FullOpen = true;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                buttonCellSpacingColor.BackColor = colorDialog.Color;
                thumbnailXpressSample.CellSpacingColor = colorDialog.Color;
            }
        }

        private void buttonBackColor_Click(object sender, EventArgs e)
        {
            // CellSpacingColor
            colorDialog.Color = buttonBackColor.BackColor;
            colorDialog.AllowFullOpen = true;
            colorDialog.FullOpen = true;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                buttonBackColor.BackColor = colorDialog.Color;
                thumbnailXpressSample.BackColor = colorDialog.Color;
            }
        }

        private void numericUpDownCellWidth_ValueChanged(object sender, EventArgs e)
        {
            thumbnailXpressSample.CellWidth = (int)numericUpDownCellWidth.Value;

        }

        private void numericUpDownCellHeight_ValueChanged(object sender, EventArgs e)
        {
            thumbnailXpressSample.CellHeight = (int)numericUpDownCellHeight.Value;
        }

        private void buttonMore_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You can change the appearance of the thumbnails in additional ways using the ThumbnailXpress SDK");
        }

    }
}

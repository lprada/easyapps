﻿namespace ImagXpressViewerDemo
{
    partial class FormConfigViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

                if (imageXViewSample.Image != null)
                {
                    imageXViewSample.Image.Dispose();
                    imageXViewSample.Image = null;
                }

                if (imageXViewSample != null)
                {
                    imageXViewSample.Dispose();
                    imageXViewSample = null;
                }

                if (imagXpressSample != null)
                {
                    imagXpressSample.Dispose();
                    imagXpressSample = null;
                }
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxAlignVertical = new System.Windows.Forms.ComboBox();
            this.comboBoxAlignHorizontal = new System.Windows.Forms.ComboBox();
            this.comboBoxScrollBars = new System.Windows.Forms.ComboBox();
            this.buttonMore = new System.Windows.Forms.Button();
            this.labelAlignVertical = new System.Windows.Forms.Label();
            this.buttonBackColor = new System.Windows.Forms.Button();
            this.labelBackColor = new System.Windows.Forms.Label();
            this.labelScrollBars = new System.Windows.Forms.Label();
            this.labelAlignHorizontal = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelSampleHeader = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelSelected = new System.Windows.Forms.Label();
            this.imagXpressSample = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXViewSample = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.panelBackgroundFrame1 = new AccusoftCustom.PanelBackgroundFrame();
            this.panelGradientNoFlare1 = new AccusoftCustom.PanelGradientNoFlare();
            this.panelBackgroundFrame2 = new AccusoftCustom.PanelBackgroundFrame();
            this.panelGradientNoFlare2 = new AccusoftCustom.PanelGradientNoFlare();
            this.panel1.SuspendLayout();
            this.panelBackgroundFrame1.SuspendLayout();
            this.panelGradientNoFlare1.SuspendLayout();
            this.panelBackgroundFrame2.SuspendLayout();
            this.panelGradientNoFlare2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.comboBoxAlignVertical);
            this.panel1.Controls.Add(this.comboBoxAlignHorizontal);
            this.panel1.Controls.Add(this.comboBoxScrollBars);
            this.panel1.Controls.Add(this.buttonMore);
            this.panel1.Controls.Add(this.labelAlignVertical);
            this.panel1.Controls.Add(this.buttonBackColor);
            this.panel1.Controls.Add(this.labelBackColor);
            this.panel1.Controls.Add(this.labelScrollBars);
            this.panel1.Controls.Add(this.labelAlignHorizontal);
            this.panel1.Location = new System.Drawing.Point(16, 14);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 295);
            this.panel1.TabIndex = 10;
            // 
            // comboBoxAlignVertical
            // 
            this.comboBoxAlignVertical.FormattingEnabled = true;
            this.comboBoxAlignVertical.Location = new System.Drawing.Point(132, 112);
            this.comboBoxAlignVertical.MaxDropDownItems = 4;
            this.comboBoxAlignVertical.Name = "comboBoxAlignVertical";
            this.comboBoxAlignVertical.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAlignVertical.TabIndex = 20;
            this.comboBoxAlignVertical.SelectedValueChanged += new System.EventHandler(this.comboBoxAlignVertical_SelectedValueChanged);
            // 
            // comboBoxAlignHorizontal
            // 
            this.comboBoxAlignHorizontal.FormattingEnabled = true;
            this.comboBoxAlignHorizontal.Location = new System.Drawing.Point(132, 78);
            this.comboBoxAlignHorizontal.MaxDropDownItems = 4;
            this.comboBoxAlignHorizontal.Name = "comboBoxAlignHorizontal";
            this.comboBoxAlignHorizontal.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAlignHorizontal.TabIndex = 19;
            this.comboBoxAlignHorizontal.SelectedValueChanged += new System.EventHandler(this.comboBoxAlignHorizontal_SelectedValueChanged);
            // 
            // comboBoxScrollBars
            // 
            this.comboBoxScrollBars.FormattingEnabled = true;
            this.comboBoxScrollBars.Location = new System.Drawing.Point(132, 13);
            this.comboBoxScrollBars.MaxDropDownItems = 4;
            this.comboBoxScrollBars.Name = "comboBoxScrollBars";
            this.comboBoxScrollBars.Size = new System.Drawing.Size(121, 21);
            this.comboBoxScrollBars.TabIndex = 18;
            this.comboBoxScrollBars.SelectedValueChanged += new System.EventHandler(this.comboBoxScrollBars_SelectedValueChanged);
            // 
            // buttonMore
            // 
            this.buttonMore.Location = new System.Drawing.Point(94, 232);
            this.buttonMore.Name = "buttonMore";
            this.buttonMore.Size = new System.Drawing.Size(75, 23);
            this.buttonMore.TabIndex = 17;
            this.buttonMore.Text = "More";
            this.buttonMore.UseVisualStyleBackColor = true;
            this.buttonMore.Click += new System.EventHandler(this.buttonMore_Click);
            // 
            // labelAlignVertical
            // 
            this.labelAlignVertical.AutoSize = true;
            this.labelAlignVertical.BackColor = System.Drawing.Color.Transparent;
            this.labelAlignVertical.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelAlignVertical.Location = new System.Drawing.Point(12, 112);
            this.labelAlignVertical.Name = "labelAlignVertical";
            this.labelAlignVertical.Size = new System.Drawing.Size(75, 13);
            this.labelAlignVertical.TabIndex = 11;
            this.labelAlignVertical.Text = "Align Vertical";
            // 
            // buttonBackColor
            // 
            this.buttonBackColor.BackColor = System.Drawing.Color.Maroon;
            this.buttonBackColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonBackColor.Location = new System.Drawing.Point(132, 42);
            this.buttonBackColor.Name = "buttonBackColor";
            this.buttonBackColor.Size = new System.Drawing.Size(49, 18);
            this.buttonBackColor.TabIndex = 10;
            this.buttonBackColor.UseVisualStyleBackColor = false;
            this.buttonBackColor.Click += new System.EventHandler(this.buttonBackColor_Click);
            // 
            // labelBackColor
            // 
            this.labelBackColor.AutoSize = true;
            this.labelBackColor.BackColor = System.Drawing.Color.Transparent;
            this.labelBackColor.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelBackColor.Location = new System.Drawing.Point(12, 45);
            this.labelBackColor.Name = "labelBackColor";
            this.labelBackColor.Size = new System.Drawing.Size(101, 13);
            this.labelBackColor.TabIndex = 9;
            this.labelBackColor.Text = "Background Color";
            // 
            // labelScrollBars
            // 
            this.labelScrollBars.AutoSize = true;
            this.labelScrollBars.BackColor = System.Drawing.Color.Transparent;
            this.labelScrollBars.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelScrollBars.Location = new System.Drawing.Point(12, 16);
            this.labelScrollBars.Name = "labelScrollBars";
            this.labelScrollBars.Size = new System.Drawing.Size(60, 13);
            this.labelScrollBars.TabIndex = 6;
            this.labelScrollBars.Text = "Scroll Bars";
            // 
            // labelAlignHorizontal
            // 
            this.labelAlignHorizontal.AutoSize = true;
            this.labelAlignHorizontal.BackColor = System.Drawing.Color.Transparent;
            this.labelAlignHorizontal.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelAlignHorizontal.Location = new System.Drawing.Point(12, 78);
            this.labelAlignHorizontal.Name = "labelAlignHorizontal";
            this.labelAlignHorizontal.Size = new System.Drawing.Size(91, 13);
            this.labelAlignHorizontal.TabIndex = 7;
            this.labelAlignHorizontal.Text = "Align Horizontal";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(177, 422);
            this.buttonCancel.MaximumSize = new System.Drawing.Size(108, 41);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(108, 30);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelSampleHeader
            // 
            this.labelSampleHeader.AutoSize = true;
            this.labelSampleHeader.BackColor = System.Drawing.Color.Transparent;
            this.labelSampleHeader.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSampleHeader.ForeColor = System.Drawing.Color.White;
            this.labelSampleHeader.Location = new System.Drawing.Point(15, 25);
            this.labelSampleHeader.Name = "labelSampleHeader";
            this.labelSampleHeader.Size = new System.Drawing.Size(179, 19);
            this.labelSampleHeader.TabIndex = 12;
            this.labelSampleHeader.Text = "Sample ImagXpress control ";
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Location = new System.Drawing.Point(53, 422);
            this.buttonOK.MaximumSize = new System.Drawing.Size(114, 41);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(114, 30);
            this.buttonOK.TabIndex = 13;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelSelected
            // 
            this.labelSelected.AutoSize = true;
            this.labelSelected.BackColor = System.Drawing.Color.Transparent;
            this.labelSelected.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelected.ForeColor = System.Drawing.Color.White;
            this.labelSelected.Location = new System.Drawing.Point(15, 15);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(181, 38);
            this.labelSelected.TabIndex = 14;
            this.labelSelected.Text = "Selected ImagXpress control\r\nproperties and methods";
            // 
            // imageXViewSample
            // 
            this.imageXViewSample.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewSample.Location = new System.Drawing.Point(14, 15);
            this.imageXViewSample.Name = "imageXViewSample";
            this.imageXViewSample.Size = new System.Drawing.Size(487, 396);
            this.imageXViewSample.TabIndex = 15;
            // 
            // panelBackgroundFrame1
            // 
            this.panelBackgroundFrame1.Controls.Add(this.labelSelected);
            this.panelBackgroundFrame1.Controls.Add(this.panelGradientNoFlare1);
            this.panelBackgroundFrame1.Location = new System.Drawing.Point(12, 12);
            this.panelBackgroundFrame1.Name = "panelBackgroundFrame1";
            this.panelBackgroundFrame1.Size = new System.Drawing.Size(320, 400);
            this.panelBackgroundFrame1.TabIndex = 31;
            // 
            // panelGradientNoFlare1
            // 
            this.panelGradientNoFlare1.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlare1.Controls.Add(this.panel1);
            this.panelGradientNoFlare1.Location = new System.Drawing.Point(10, 57);
            this.panelGradientNoFlare1.Name = "panelGradientNoFlare1";
            this.panelGradientNoFlare1.Size = new System.Drawing.Size(300, 332);
            this.panelGradientNoFlare1.TabIndex = 0;
            // 
            // panelBackgroundFrame2
            // 
            this.panelBackgroundFrame2.Controls.Add(this.labelSampleHeader);
            this.panelBackgroundFrame2.Controls.Add(this.panelGradientNoFlare2);
            this.panelBackgroundFrame2.Location = new System.Drawing.Point(340, 12);
            this.panelBackgroundFrame2.Name = "panelBackgroundFrame2";
            this.panelBackgroundFrame2.Size = new System.Drawing.Size(534, 500);
            this.panelBackgroundFrame2.TabIndex = 32;
            // 
            // panelGradientNoFlare2
            // 
            this.panelGradientNoFlare2.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlare2.Controls.Add(this.imageXViewSample);
            this.panelGradientNoFlare2.Location = new System.Drawing.Point(10, 57);
            this.panelGradientNoFlare2.Name = "panelGradientNoFlare2";
            this.panelGradientNoFlare2.Size = new System.Drawing.Size(514, 430);
            this.panelGradientNoFlare2.TabIndex = 0;
            // 
            // FormConfigViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(54)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(884, 524);
            this.Controls.Add(this.panelBackgroundFrame2);
            this.Controls.Add(this.panelBackgroundFrame1);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormConfigViewer";
            this.Text = "Configure Viewer";
            this.Load += new System.EventHandler(this.FormConfigViewer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelBackgroundFrame1.ResumeLayout(false);
            this.panelBackgroundFrame1.PerformLayout();
            this.panelGradientNoFlare1.ResumeLayout(false);
            this.panelBackgroundFrame2.ResumeLayout(false);
            this.panelBackgroundFrame2.PerformLayout();
            this.panelGradientNoFlare2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonMore;
        private System.Windows.Forms.Label labelAlignVertical;
        private System.Windows.Forms.Button buttonBackColor;
        private System.Windows.Forms.Label labelBackColor;
        private System.Windows.Forms.Label labelScrollBars;
        private System.Windows.Forms.Label labelAlignHorizontal;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelSampleHeader;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelSelected;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpressSample;
        private Accusoft.ImagXpressSdk.ImageXView imageXViewSample;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ComboBox comboBoxScrollBars;
        private System.Windows.Forms.ComboBox comboBoxAlignVertical;
        private System.Windows.Forms.ComboBox comboBoxAlignHorizontal;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrame1;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlare1;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrame2;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlare2;
    }
}
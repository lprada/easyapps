﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// Accusoft Pegasus 
using Accusoft.ImagXpressSdk;
using Accusoft.NotateXpressSdk;
using Accusoft.ThumbnailXpressSdk;

namespace ImagXpressViewerDemo
{
    public partial class FormMoreImageInfo : Form
    {
        private ImageXView viewToSave;

        public FormMoreImageInfo()
        {
            InitializeComponent();
        }

        public void loadExistingView(ImageXView viewToTweak)
        {
            // grab a copy, so we don't have to reprocess
            viewToSave = viewToTweak;

        }

        private void FormMoreImageInfo_Load(object sender, EventArgs e)
        {
            textBoxShortFileName.Text = viewToSave.Tag.ToString();
            textBoxWidth.Text = viewToSave.Image.ImageXData.Width.ToString();
            textBoxHeight.Text = viewToSave.Image.ImageXData.Height.ToString();
            textBoxBitsPerPixel.Text = viewToSave.Image.ImageXData.BitsPerPixel.ToString();
            textBoxResolution.Text = viewToSave.Image.ImageXData.Resolution.Dimensions.ToString();
            textBoxResolutionUnits.Text = viewToSave.Image.ImageXData.Resolution.Units.ToString();
            textBoxFormat.Text = viewToSave.Image.ImageXData.Format.ToString();
            textBoxCompression.Text = viewToSave.Image.ImageXData.Compression.ToString();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonMore_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You can find more information about the images using the ImagXpress SDK");
        }
 
    }
}

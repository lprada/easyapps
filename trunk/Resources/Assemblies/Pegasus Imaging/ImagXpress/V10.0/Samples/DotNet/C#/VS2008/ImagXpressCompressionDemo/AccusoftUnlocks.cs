﻿using System;
using System.Collections.Generic;
using System.Text;
using Accusoft.ImagXpressSdk;

namespace ImagXpressCompressionDemo
{
    /// <summary>
    /// Unlocks the Accusoft component runtimes.
    /// </summary>
    class AccusoftUnlocks
    {
        public static void UnlockRuntimes(ImagXpress ix)
        {
            //ix.Licensing.UnlockRuntime(xxxx,xxxx,xxxx,xxxx);
        }
    }
}

/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices; // DllImport

namespace MultiPageTiff
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MultiPageTIFF : System.Windows.Forms.Form
	{
		//private Accusoft.ImagXpressSdk.ImageX ixTiffFile;
		private System.Windows.Forms.Button cmdMake;
		private System.Windows.Forms.Button cmdRemove;
		private System.Windows.Forms.Button cmdInsert;
		private System.Windows.Forms.Button cmdCompact;
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem mnuFileFile;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.String strImageFile1;
		private System.String strImageFile2;
		private System.String strImageFile3;
		private System.String strMPFile;
		private System.String strTmpFile;
		private System.Windows.Forms.RichTextBox rtfInfo;
		private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Label lblError;
		private Accusoft.ImagXpressSdk.ImageXView imageXView1;
		private Accusoft.ImagXpressSdk.ImageXView imageXView2;
		private Accusoft.ImagXpressSdk.ImageXView imageXView3;
		private Label labelFileStats;
		private Label labelMore;
		private IContainer components;

		public MultiPageTIFF()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiPageTIFF));
			this.cmdMake = new System.Windows.Forms.Button();
			this.cmdRemove = new System.Windows.Forms.Button();
			this.cmdInsert = new System.Windows.Forms.Button();
			this.cmdCompact = new System.Windows.Forms.Button();
			this.mnuFile = new System.Windows.Forms.MainMenu(this.components);
			this.mnuFileFile = new System.Windows.Forms.MenuItem();
			this.mnuFileQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.rtfInfo = new System.Windows.Forms.RichTextBox();
			this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
			this.lblLastError = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
			this.imageXView2 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
			this.imageXView3 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
			this.labelFileStats = new System.Windows.Forms.Label();
			this.labelMore = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cmdMake
			// 
			this.cmdMake.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cmdMake.Location = new System.Drawing.Point(420, 152);
			this.cmdMake.Name = "cmdMake";
			this.cmdMake.Size = new System.Drawing.Size(163, 24);
			this.cmdMake.TabIndex = 4;
			this.cmdMake.Text = "Make MP.TIF";
			this.cmdMake.Click += new System.EventHandler(this.cmdMake_Click);
			// 
			// cmdRemove
			// 
			this.cmdRemove.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cmdRemove.Location = new System.Drawing.Point(420, 184);
			this.cmdRemove.Name = "cmdRemove";
			this.cmdRemove.Size = new System.Drawing.Size(163, 24);
			this.cmdRemove.TabIndex = 5;
			this.cmdRemove.Text = "Remove Page 2";
			this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
			// 
			// cmdInsert
			// 
			this.cmdInsert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cmdInsert.Location = new System.Drawing.Point(420, 216);
			this.cmdInsert.Name = "cmdInsert";
			this.cmdInsert.Size = new System.Drawing.Size(163, 24);
			this.cmdInsert.TabIndex = 6;
			this.cmdInsert.Text = "Insert Page 2";
			this.cmdInsert.Click += new System.EventHandler(this.cmdInsert_Click);
			// 
			// cmdCompact
			// 
			this.cmdCompact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.cmdCompact.Location = new System.Drawing.Point(420, 248);
			this.cmdCompact.Name = "cmdCompact";
			this.cmdCompact.Size = new System.Drawing.Size(163, 24);
			this.cmdCompact.TabIndex = 7;
			this.cmdCompact.Text = "Compact";
			this.cmdCompact.Click += new System.EventHandler(this.cmdCompact_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileFile,
            this.mnuToolbar,
            this.mnuAbout});
			// 
			// mnuFileFile
			// 
			this.mnuFileFile.Index = 0;
			this.mnuFileFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileQuit});
			this.mnuFileFile.Text = "&File";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 0;
			this.mnuFileQuit.Text = "&Quit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuToolbarShow});
			this.mnuToolbar.Text = "&Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "&Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// rtfInfo
			// 
			this.rtfInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.rtfInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtfInfo.Location = new System.Drawing.Point(16, 8);
			this.rtfInfo.Name = "rtfInfo";
			this.rtfInfo.ReadOnly = true;
			this.rtfInfo.Size = new System.Drawing.Size(567, 128);
			this.rtfInfo.TabIndex = 10;
			this.rtfInfo.Text = resources.GetString("rtfInfo.Text");
			// 
			// lblLastError
			// 
			this.lblLastError.Location = new System.Drawing.Point(13, 388);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(77, 40);
			this.lblLastError.TabIndex = 14;
			this.lblLastError.Text = "Last Error:";
			// 
			// lblError
			// 
			this.lblError.Location = new System.Drawing.Point(96, 377);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(491, 51);
			this.lblError.TabIndex = 15;
			// 
			// imageXView1
			// 
			this.imageXView1.Location = new System.Drawing.Point(16, 152);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(128, 152);
			this.imageXView1.TabIndex = 16;
			// 
			// imageXView2
			// 
			this.imageXView2.Location = new System.Drawing.Point(150, 152);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(128, 152);
			this.imageXView2.TabIndex = 17;
			// 
			// imageXView3
			// 
			this.imageXView3.Location = new System.Drawing.Point(284, 152);
			this.imageXView3.Name = "imageXView3";
			this.imageXView3.Size = new System.Drawing.Size(128, 152);
			this.imageXView3.TabIndex = 18;
			// 
			// labelFileStats
			// 
			this.labelFileStats.Location = new System.Drawing.Point(18, 322);
			this.labelFileStats.Name = "labelFileStats";
			this.labelFileStats.Size = new System.Drawing.Size(394, 33);
			this.labelFileStats.TabIndex = 19;
			// 
			// labelMore
			// 
			this.labelMore.Location = new System.Drawing.Point(420, 322);
			this.labelMore.Name = "labelMore";
			this.labelMore.Size = new System.Drawing.Size(163, 33);
			this.labelMore.TabIndex = 20;
			// 
			// MultiPageTIFF
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(599, 429);
			this.Controls.Add(this.labelMore);
			this.Controls.Add(this.labelFileStats);
			this.Controls.Add(this.imageXView3);
			this.Controls.Add(this.imageXView2);
			this.Controls.Add(this.imageXView1);
			this.Controls.Add(this.lblError);
			this.Controls.Add(this.lblLastError);
			this.Controls.Add(this.rtfInfo);
			this.Controls.Add(this.cmdCompact);
			this.Controls.Add(this.cmdInsert);
			this.Controls.Add(this.cmdRemove);
			this.Controls.Add(this.cmdMake);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Menu = this.mnuFile;
			this.Name = "MultiPageTIFF";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MultiPage TIFF";
			this.Load += new System.EventHandler(this.MultiPageTIFF_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MultiPageTIFF());
		}

		private void ReloadViews() 
		{
			try
			{
				//Get the number of pages in the MP TIFF here
				System.Int32 nNbrPages = Accusoft.ImagXpressSdk.ImageX.NumPages(imagXpress1, strMPFile);

				// Show the first 3 pages
				for (System.Int32 i = 1; i <= 3; i++)
				{
					// Dispose of any current images
					switch (i)
					{
						case 1:
							if (imageXView1.Image != null)
							{
								imageXView1.Image.Dispose();
								imageXView1.Image = null;
							}
							break;

						case 2:
							if (imageXView2.Image != null)
							{
								imageXView2.Image.Dispose();
								imageXView2.Image = null;
							}
							break;

						case 3:
							if (imageXView3.Image != null)
							{
								imageXView3.Image.Dispose();
								imageXView3.Image = null;
							}
							break;

					}

					// load the correct page into the correct view
					if (i <= nNbrPages)
					{
						switch (i)
						{
							case 1:
								imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strMPFile, i);
								break;

							case 2:
								imageXView2.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strMPFile, i);
								break;

							case 3:
								imageXView3.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strMPFile, i);
								break;
						}
					}
				}

				labelFileStats.Text = "File Size: " + imageXView1.Image.ImageXData.Size.ToString(cultNumber) + " bytes.\n" + "Pages: " + imageXView1.Image.PageCount;

				if ((imageXView1.Image.Page != 0))
				{
					if (imageXView1.Image.PageCount > 3)
					{
						labelMore.Text = "More...";
					}
					else
					{
						labelMore.Text = "";
					}
				}
				else
				{
					labelMore.Text = "";
				}
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException eX)
			{
				PegasusError(eX, lblError);
			}
		}
			
		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void cmdMake_Click(object sender, System.EventArgs e)
		{
			if (System.IO.File.Exists(strMPFile) )
			{
				System.IO.File.Delete(strMPFile);
			}
			try 
			{
				Accusoft.ImagXpressSdk.ImageX imgTmp = null;

				//clear out any error before next operation
				lblError.Text = "";

				imgTmp = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strImageFile1);
				Accusoft.ImagXpressSdk.SaveOptions soOpts = new Accusoft.ImagXpressSdk.SaveOptions();
				try 
				{
					soOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff;
					soOpts.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.NoCompression;
					soOpts.Tiff.MultiPage = true;
					imgTmp.Save(strMPFile,soOpts);
					imgTmp.Dispose();
					imgTmp = null;
				}
				catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
				try 
				{
					Accusoft.ImagXpressSdk.ImageX.InsertPage(imagXpress1, strImageFile2, strMPFile, 2);
				}
				catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
				try 
				{
					Accusoft.ImagXpressSdk.ImageX.InsertPage(imagXpress1, strImageFile3, strMPFile, 3);    
				}
				catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			} 
			catch (Accusoft.ImagXpressSdk.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
				return;
			}
			ReloadViews();
		}

		private void MultiPageTIFF_Load(object sender, System.EventArgs e)
		{
			//***Must call the UnlockRuntime method here***
			//imagXpress1.Licensing.UnlockRuntime(12345,12345,12345);

			// This sample uses ImagXpress Professional edition features,
			// make sure that ImagXpress is running in Professional edition.
			imagXpress1.Licensing.LicenseEditionChoice = Accusoft.ImagXpressSdk.LicenseEditionType.ImagXpressProfessional; 

			// Because in this sample the ImageX object is always located in the ImageXView
			// tell the ImageXView component to dispose of the ImageX object when itself is
			// disposed. Otherwise, we would have to dispose of the ImageX object in the Form's
			// Dispose method before the components.Dispose() section.
			imageXView1.AutoImageDispose = true;
			imageXView2.AutoImageDispose = true;
			imageXView3.AutoImageDispose = true;

			// Set the current directory to the Common\Images directory
			String strCurrentDir = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images\");
			if (System.IO.Directory.Exists(strCurrentDir))
				System.IO.Directory.SetCurrentDirectory(strCurrentDir);
			strCurrentDir = System.IO.Directory.GetCurrentDirectory();

			strImageFile1 = "page1.tif";
			strImageFile2 = "page2.tif";
			strImageFile3 = "page3.tif";
			strMPFile = Application.StartupPath + @"\mp.tif";
			strTmpFile = Application.StartupPath + @"\compact.tif";		
		}

		private void cmdRemove_Click(object sender, System.EventArgs e)
		{
			try 
			{
				//clear out any error before next operation
				lblError.Text = "";
				Accusoft.ImagXpressSdk.ImageX.DeletePage(imagXpress1, strMPFile, 2);
			} 
			catch (Accusoft.ImagXpressSdk.ImageXException ex)
			{
				PegasusError(ex,lblError);
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			ReloadViews();
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
		System.IntPtr hwndTB;
			RECT controlRect = new RECT();
			RECT toolbarRect = new RECT();
			this.mnuToolbarShow.Text = (imageXView1.Toolbar.Activated)? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			hwndTB = imageXView1.Toolbar.Hwnd;
			Win32.GetWindowRect(imageXView1.Handle.ToInt32(),ref controlRect);
			Win32.GetWindowRect(hwndTB.ToInt32(),ref toolbarRect);
			Win32.SetWindowText(hwndTB.ToInt32(),"Toolbar 1");
			Win32.MoveWindow(hwndTB.ToInt32(),controlRect.left+8,controlRect.bottom + 12,toolbarRect.right - toolbarRect.left,toolbarRect.bottom - toolbarRect.top,true);
			try 
			{
				imageXView2.Toolbar.Activated = !imageXView2.Toolbar.Activated;
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			hwndTB = imageXView2.Toolbar.Hwnd;
			Win32.GetWindowRect(imageXView2.Handle.ToInt32(),ref controlRect);
			Win32.GetWindowRect(hwndTB.ToInt32(),ref toolbarRect);
			Win32.SetWindowText(hwndTB.ToInt32(),"Toolbar 2");
			Win32.MoveWindow(hwndTB.ToInt32(),controlRect.left+8,controlRect.bottom + 12,toolbarRect.right - toolbarRect.left,toolbarRect.bottom - toolbarRect.top,true);
			try 
			{
				imageXView3.Toolbar.Activated = !imageXView3.Toolbar.Activated;
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			hwndTB = imageXView3.Toolbar.Hwnd;
			Win32.GetWindowRect(imageXView3.Handle.ToInt32(),ref controlRect);
			Win32.GetWindowRect(hwndTB.ToInt32(),ref toolbarRect);
			Win32.SetWindowText(hwndTB.ToInt32(),"Toolbar 3");
			Win32.MoveWindow(hwndTB.ToInt32(),controlRect.left+8,controlRect.bottom + 12,toolbarRect.right - toolbarRect.left,toolbarRect.bottom - toolbarRect.top,true);
		}

		private void cmdInsert_Click(object sender, System.EventArgs e)
		{
			try 
			{
				//clear out any error before next operation
				lblError.Text = "";
				Accusoft.ImagXpressSdk.ImageX.InsertPage(imagXpress1, strImageFile2, strMPFile, 2);
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			ReloadViews();
		}

		private void cmdCompact_Click(object sender, System.EventArgs e)
		{
			try 
			{
				//clear out any error before next operation
				lblError.Text = "";
				Accusoft.ImagXpressSdk.ImageX.CompactFile(imagXpress1, strMPFile, strTmpFile);
			} 
			catch (Accusoft.ImagXpressSdk.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);;
			}
			if (System.IO.File.Exists(strTmpFile)) 
			{
				System.IO.File.Delete(strMPFile);
				System.IO.File.Move(strTmpFile,strMPFile);
			}
			ReloadViews();
		}
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(Accusoft.ImagXpressSdk.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion

	}

	[StructLayout(LayoutKind.Sequential)]
	public struct RECT
	{
		public int left;
		public int top;
		public int right;
		public int bottom;
	}

	public class Win32
	{
		[DllImport("User32.Dll")]
		public static extern void SetWindowText(int hWnd, String s);
		[DllImport("User32.Dll")]
		public static extern bool MoveWindow(int hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
		[DllImport("User32.Dll")]
		public static extern bool GetWindowRect(int hWnd, ref RECT rc);
	}
}

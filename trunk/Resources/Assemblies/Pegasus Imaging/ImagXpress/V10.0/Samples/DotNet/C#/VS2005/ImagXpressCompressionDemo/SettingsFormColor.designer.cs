﻿namespace ImagXpressCompressionDemo
{
	partial class SettingsFormColor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.groupBoxImageType = new System.Windows.Forms.GroupBox();
            this.radioButtonPcx = new System.Windows.Forms.RadioButton();
            this.radioButtonJls = new System.Windows.Forms.RadioButton();
            this.radioButtonLjp = new System.Windows.Forms.RadioButton();
            this.radioButtonHdp = new System.Windows.Forms.RadioButton();
            this.radioButtonPng = new System.Windows.Forms.RadioButton();
            this.radioButtonGif = new System.Windows.Forms.RadioButton();
            this.radioButtonJpeg2000 = new System.Windows.Forms.RadioButton();
            this.radioButtonJpeg = new System.Windows.Forms.RadioButton();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabPagePng = new System.Windows.Forms.TabPage();
            this.labelPngTransparencyColor = new System.Windows.Forms.Label();
            this.buttonPngTransparencyColor = new System.Windows.Forms.Button();
            this.groupBoxPngTransparencyMatch = new System.Windows.Forms.GroupBox();
            this.radioButtonPngExact = new System.Windows.Forms.RadioButton();
            this.radioButtonPngClosest = new System.Windows.Forms.RadioButton();
            this.radioButtonPngNone = new System.Windows.Forms.RadioButton();
            this.checkBoxPngInterlaced = new System.Windows.Forms.CheckBox();
            this.tabPageGif = new System.Windows.Forms.TabPage();
            this.labelGifTransparencyColor = new System.Windows.Forms.Label();
            this.buttonGifTransparencyColor = new System.Windows.Forms.Button();
            this.groupBoxGifType = new System.Windows.Forms.GroupBox();
            this.radioButtonGif89a = new System.Windows.Forms.RadioButton();
            this.radioButtonGif87a = new System.Windows.Forms.RadioButton();
            this.groupBoxGifTransparencyMatch = new System.Windows.Forms.GroupBox();
            this.radioButtonGifExact = new System.Windows.Forms.RadioButton();
            this.radioButtonGifClosest = new System.Windows.Forms.RadioButton();
            this.radioButtonGifNone = new System.Windows.Forms.RadioButton();
            this.checkBoxGifInterlaced = new System.Windows.Forms.CheckBox();
            this.tabPageJpeg2000 = new System.Windows.Forms.TabPage();
            this.labelJp2Peak = new System.Windows.Forms.Label();
            this.numericUpDownJp2Peak = new System.Windows.Forms.NumericUpDown();
            this.groupBoxJp2TileSize = new System.Windows.Forms.GroupBox();
            this.labelJp2TileHeight = new System.Windows.Forms.Label();
            this.labelJp2TileWidth = new System.Windows.Forms.Label();
            this.numericUpDownJp2TileHeight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownJp2TileWidth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownJp2CompressSize = new System.Windows.Forms.NumericUpDown();
            this.labelJp2CompressSize = new System.Windows.Forms.Label();
            this.groupBoxJp2Type = new System.Windows.Forms.GroupBox();
            this.radioButtonJp2Lossy = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2Lossless = new System.Windows.Forms.RadioButton();
            this.groupBoxJp2Order = new System.Windows.Forms.GroupBox();
            this.radioButtonJp2OrderRpcl = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderRlcp = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderPcrl = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderLrcp = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderCprl = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderDefault = new System.Windows.Forms.RadioButton();
            this.tabPageJpeg = new System.Windows.Forms.TabPage();
            this.numericUpDownExifThumbnailSize = new System.Windows.Forms.NumericUpDown();
            this.labelExifThumbnailSize = new System.Windows.Forms.Label();
            this.groupBoxJpegWrap = new System.Windows.Forms.GroupBox();
            this.radioButtonJpegWrapInExif = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegNoWrap = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegWrapInPdf = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegWrapInTiff = new System.Windows.Forms.RadioButton();
            this.checkBoxJpegProgressive = new System.Windows.Forms.CheckBox();
            this.groupBoxSubSampling = new System.Windows.Forms.GroupBox();
            this.radioButtonJpegSubSampling411 = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegSubSampling211v = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegSubSampling211 = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegSubSampling111 = new System.Windows.Forms.RadioButton();
            this.checkBoxJpegCosited = new System.Windows.Forms.CheckBox();
            this.labelChrominanceValue = new System.Windows.Forms.Label();
            this.labelLuminanceValue = new System.Windows.Forms.Label();
            this.labelChrominance = new System.Windows.Forms.Label();
            this.labelLuminance = new System.Windows.Forms.Label();
            this.hScrollBarChrominance = new System.Windows.Forms.HScrollBar();
            this.hScrollBarLuminance = new System.Windows.Forms.HScrollBar();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.tabPageHdp = new System.Windows.Forms.TabPage();
            this.numericUpDownHdpQuantization = new System.Windows.Forms.NumericUpDown();
            this.labelHdpQuantization = new System.Windows.Forms.Label();
            this.groupBoxHdpOrder = new System.Windows.Forms.GroupBox();
            this.radioButtonHdpOrderFrequency = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpOrderSpacial = new System.Windows.Forms.RadioButton();
            this.groupBoxChromaSubSampling = new System.Windows.Forms.GroupBox();
            this.radioButtonHdpChromaSubSampling444 = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpChromaSubSampling422 = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpChromaSubSampling420 = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpChromaSubSampling400 = new System.Windows.Forms.RadioButton();
            this.tabPageLjp = new System.Windows.Forms.TabPage();
            this.labelLjpPredictor = new System.Windows.Forms.Label();
            this.labelLjpOrder = new System.Windows.Forms.Label();
            this.numericUpDownLjpPredictor = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLjpOrder = new System.Windows.Forms.NumericUpDown();
            this.tabPageJls = new System.Windows.Forms.TabPage();
            this.groupBoxJlsMax = new System.Windows.Forms.GroupBox();
            this.radioButtonJlsMax255 = new System.Windows.Forms.RadioButton();
            this.radioButtonJlsMax0 = new System.Windows.Forms.RadioButton();
            this.groupBoxJlsInterleave = new System.Windows.Forms.GroupBox();
            this.radioButtonJlsSampleInterleaved = new System.Windows.Forms.RadioButton();
            this.radioButtonJlsLineInterleaved = new System.Windows.Forms.RadioButton();
            this.radioButtonJlsNonInterleaved = new System.Windows.Forms.RadioButton();
            this.labelJlsPoint = new System.Windows.Forms.Label();
            this.labelJlsNear = new System.Windows.Forms.Label();
            this.numericUpDownJlsPoint = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownJlsNear = new System.Windows.Forms.NumericUpDown();
            this.tabPagePcx = new System.Windows.Forms.TabPage();
            this.labelPcxNoSettings = new System.Windows.Forms.Label();
            this.buttonRestoreDefaults = new System.Windows.Forms.Button();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.checkBoxPreview = new System.Windows.Forms.CheckBox();
            this.groupBoxCompressedImage = new System.Windows.Forms.GroupBox();
            this.radioButtonCompressedImage4 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage3 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage2 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage1 = new System.Windows.Forms.RadioButton();
            this.panelBackgroundFrameColor = new AccusoftCustom.PanelBackgroundFrame();
            this.groupBoxImageType.SuspendLayout();
            this.tabPagePng.SuspendLayout();
            this.groupBoxPngTransparencyMatch.SuspendLayout();
            this.tabPageGif.SuspendLayout();
            this.groupBoxGifType.SuspendLayout();
            this.groupBoxGifTransparencyMatch.SuspendLayout();
            this.tabPageJpeg2000.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2Peak)).BeginInit();
            this.groupBoxJp2TileSize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2CompressSize)).BeginInit();
            this.groupBoxJp2Type.SuspendLayout();
            this.groupBoxJp2Order.SuspendLayout();
            this.tabPageJpeg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExifThumbnailSize)).BeginInit();
            this.groupBoxJpegWrap.SuspendLayout();
            this.groupBoxSubSampling.SuspendLayout();
            this.tabControlSettings.SuspendLayout();
            this.tabPageHdp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHdpQuantization)).BeginInit();
            this.groupBoxHdpOrder.SuspendLayout();
            this.groupBoxChromaSubSampling.SuspendLayout();
            this.tabPageLjp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpPredictor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpOrder)).BeginInit();
            this.tabPageJls.SuspendLayout();
            this.groupBoxJlsMax.SuspendLayout();
            this.groupBoxJlsInterleave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsNear)).BeginInit();
            this.tabPagePcx.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            this.groupBoxCompressedImage.SuspendLayout();
            this.panelBackgroundFrameColor.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxImageType
            // 
            this.groupBoxImageType.BackColor = System.Drawing.Color.White;
            this.groupBoxImageType.Controls.Add(this.radioButtonPcx);
            this.groupBoxImageType.Controls.Add(this.radioButtonJls);
            this.groupBoxImageType.Controls.Add(this.radioButtonLjp);
            this.groupBoxImageType.Controls.Add(this.radioButtonHdp);
            this.groupBoxImageType.Controls.Add(this.radioButtonPng);
            this.groupBoxImageType.Controls.Add(this.radioButtonGif);
            this.groupBoxImageType.Controls.Add(this.radioButtonJpeg2000);
            this.groupBoxImageType.Controls.Add(this.radioButtonJpeg);
            this.groupBoxImageType.Location = new System.Drawing.Point(19, 70);
            this.groupBoxImageType.Name = "groupBoxImageType";
            this.groupBoxImageType.Size = new System.Drawing.Size(123, 148);
            this.groupBoxImageType.TabIndex = 1;
            this.groupBoxImageType.TabStop = false;
            this.groupBoxImageType.Text = "Image Type";
            // 
            // radioButtonPcx
            // 
            this.radioButtonPcx.AutoSize = true;
            this.radioButtonPcx.Location = new System.Drawing.Point(69, 121);
            this.radioButtonPcx.Name = "radioButtonPcx";
            this.radioButtonPcx.Size = new System.Drawing.Size(46, 17);
            this.radioButtonPcx.TabIndex = 8;
            this.radioButtonPcx.TabStop = true;
            this.radioButtonPcx.Text = "PCX";
            this.radioButtonPcx.UseVisualStyleBackColor = true;
            this.radioButtonPcx.CheckedChanged += new System.EventHandler(this.radioButtonPcx_CheckedChanged);
            // 
            // radioButtonJls
            // 
            this.radioButtonJls.AutoSize = true;
            this.radioButtonJls.Location = new System.Drawing.Point(11, 121);
            this.radioButtonJls.Name = "radioButtonJls";
            this.radioButtonJls.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJls.TabIndex = 7;
            this.radioButtonJls.TabStop = true;
            this.radioButtonJls.Text = "JLS";
            this.radioButtonJls.UseVisualStyleBackColor = true;
            this.radioButtonJls.CheckedChanged += new System.EventHandler(this.radioButtonJls_CheckedChanged);
            // 
            // radioButtonLjp
            // 
            this.radioButtonLjp.AutoSize = true;
            this.radioButtonLjp.Location = new System.Drawing.Point(69, 86);
            this.radioButtonLjp.Name = "radioButtonLjp";
            this.radioButtonLjp.Size = new System.Drawing.Size(43, 17);
            this.radioButtonLjp.TabIndex = 6;
            this.radioButtonLjp.TabStop = true;
            this.radioButtonLjp.Text = "LJP";
            this.radioButtonLjp.UseVisualStyleBackColor = true;
            this.radioButtonLjp.CheckedChanged += new System.EventHandler(this.radioButtonLjp_CheckedChanged);
            // 
            // radioButtonHdp
            // 
            this.radioButtonHdp.AutoSize = true;
            this.radioButtonHdp.Location = new System.Drawing.Point(11, 54);
            this.radioButtonHdp.Name = "radioButtonHdp";
            this.radioButtonHdp.Size = new System.Drawing.Size(48, 17);
            this.radioButtonHdp.TabIndex = 5;
            this.radioButtonHdp.TabStop = true;
            this.radioButtonHdp.Text = "HDP";
            this.radioButtonHdp.UseVisualStyleBackColor = true;
            this.radioButtonHdp.CheckedChanged += new System.EventHandler(this.radioButtonHdp_CheckedChanged);
            // 
            // radioButtonPng
            // 
            this.radioButtonPng.AutoSize = true;
            this.radioButtonPng.Location = new System.Drawing.Point(11, 86);
            this.radioButtonPng.Name = "radioButtonPng";
            this.radioButtonPng.Size = new System.Drawing.Size(48, 17);
            this.radioButtonPng.TabIndex = 4;
            this.radioButtonPng.Text = "PNG";
            this.radioButtonPng.UseVisualStyleBackColor = true;
            this.radioButtonPng.CheckedChanged += new System.EventHandler(this.radioButtonPng_CheckedChanged);
            // 
            // radioButtonGif
            // 
            this.radioButtonGif.AutoSize = true;
            this.radioButtonGif.Location = new System.Drawing.Point(69, 54);
            this.radioButtonGif.Name = "radioButtonGif";
            this.radioButtonGif.Size = new System.Drawing.Size(42, 17);
            this.radioButtonGif.TabIndex = 3;
            this.radioButtonGif.Text = "GIF";
            this.radioButtonGif.UseVisualStyleBackColor = true;
            this.radioButtonGif.CheckedChanged += new System.EventHandler(this.radioButtonGif_CheckedChanged);
            // 
            // radioButtonJpeg2000
            // 
            this.radioButtonJpeg2000.Location = new System.Drawing.Point(69, 9);
            this.radioButtonJpeg2000.Name = "radioButtonJpeg2000";
            this.radioButtonJpeg2000.Size = new System.Drawing.Size(61, 40);
            this.radioButtonJpeg2000.TabIndex = 2;
            this.radioButtonJpeg2000.Text = "JPEG 2000";
            this.radioButtonJpeg2000.UseVisualStyleBackColor = true;
            this.radioButtonJpeg2000.CheckedChanged += new System.EventHandler(this.radioButtonJpeg2000_CheckedChanged);
            // 
            // radioButtonJpeg
            // 
            this.radioButtonJpeg.AutoSize = true;
            this.radioButtonJpeg.Checked = true;
            this.radioButtonJpeg.Location = new System.Drawing.Point(11, 21);
            this.radioButtonJpeg.Name = "radioButtonJpeg";
            this.radioButtonJpeg.Size = new System.Drawing.Size(52, 17);
            this.radioButtonJpeg.TabIndex = 0;
            this.radioButtonJpeg.TabStop = true;
            this.radioButtonJpeg.Text = "JPEG";
            this.radioButtonJpeg.UseVisualStyleBackColor = true;
            this.radioButtonJpeg.CheckedChanged += new System.EventHandler(this.radioButtonJpeg_CheckedChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(177, 339);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(90, 32);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(279, 339);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(90, 32);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabPagePng
            // 
            this.tabPagePng.BackColor = System.Drawing.Color.White;
            this.tabPagePng.Controls.Add(this.labelPngTransparencyColor);
            this.tabPagePng.Controls.Add(this.buttonPngTransparencyColor);
            this.tabPagePng.Controls.Add(this.groupBoxPngTransparencyMatch);
            this.tabPagePng.Controls.Add(this.checkBoxPngInterlaced);
            this.tabPagePng.Location = new System.Drawing.Point(4, 22);
            this.tabPagePng.Name = "tabPagePng";
            this.tabPagePng.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePng.Size = new System.Drawing.Size(357, 249);
            this.tabPagePng.TabIndex = 3;
            this.tabPagePng.Text = "PNG";
            this.tabPagePng.UseVisualStyleBackColor = true;
            // 
            // labelPngTransparencyColor
            // 
            this.labelPngTransparencyColor.AutoSize = true;
            this.labelPngTransparencyColor.Location = new System.Drawing.Point(98, 162);
            this.labelPngTransparencyColor.Name = "labelPngTransparencyColor";
            this.labelPngTransparencyColor.Size = new System.Drawing.Size(102, 13);
            this.labelPngTransparencyColor.TabIndex = 7;
            this.labelPngTransparencyColor.Text = "Transparency Color:";
            // 
            // buttonPngTransparencyColor
            // 
            this.buttonPngTransparencyColor.BackColor = System.Drawing.SystemColors.Control;
            this.buttonPngTransparencyColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPngTransparencyColor.Location = new System.Drawing.Point(206, 154);
            this.buttonPngTransparencyColor.Name = "buttonPngTransparencyColor";
            this.buttonPngTransparencyColor.Size = new System.Drawing.Size(35, 28);
            this.buttonPngTransparencyColor.TabIndex = 6;
            this.buttonPngTransparencyColor.UseVisualStyleBackColor = true;
            this.buttonPngTransparencyColor.Click += new System.EventHandler(this.buttonPngTransparencyColor_Click);
            // 
            // groupBoxPngTransparencyMatch
            // 
            this.groupBoxPngTransparencyMatch.Controls.Add(this.radioButtonPngExact);
            this.groupBoxPngTransparencyMatch.Controls.Add(this.radioButtonPngClosest);
            this.groupBoxPngTransparencyMatch.Controls.Add(this.radioButtonPngNone);
            this.groupBoxPngTransparencyMatch.Location = new System.Drawing.Point(101, 46);
            this.groupBoxPngTransparencyMatch.Name = "groupBoxPngTransparencyMatch";
            this.groupBoxPngTransparencyMatch.Size = new System.Drawing.Size(140, 94);
            this.groupBoxPngTransparencyMatch.TabIndex = 5;
            this.groupBoxPngTransparencyMatch.TabStop = false;
            this.groupBoxPngTransparencyMatch.Text = "Transparency Match";
            // 
            // radioButtonPngExact
            // 
            this.radioButtonPngExact.AutoSize = true;
            this.radioButtonPngExact.Location = new System.Drawing.Point(14, 63);
            this.radioButtonPngExact.Name = "radioButtonPngExact";
            this.radioButtonPngExact.Size = new System.Drawing.Size(52, 17);
            this.radioButtonPngExact.TabIndex = 2;
            this.radioButtonPngExact.TabStop = true;
            this.radioButtonPngExact.Text = "Exact";
            this.radioButtonPngExact.UseVisualStyleBackColor = true;
            this.radioButtonPngExact.CheckedChanged += new System.EventHandler(this.radioButtonPngExact_CheckedChanged);
            // 
            // radioButtonPngClosest
            // 
            this.radioButtonPngClosest.AutoSize = true;
            this.radioButtonPngClosest.Location = new System.Drawing.Point(14, 42);
            this.radioButtonPngClosest.Name = "radioButtonPngClosest";
            this.radioButtonPngClosest.Size = new System.Drawing.Size(59, 17);
            this.radioButtonPngClosest.TabIndex = 1;
            this.radioButtonPngClosest.TabStop = true;
            this.radioButtonPngClosest.Text = "Closest";
            this.radioButtonPngClosest.UseVisualStyleBackColor = true;
            this.radioButtonPngClosest.CheckedChanged += new System.EventHandler(this.radioButtonPngClosest_CheckedChanged);
            // 
            // radioButtonPngNone
            // 
            this.radioButtonPngNone.AutoSize = true;
            this.radioButtonPngNone.Location = new System.Drawing.Point(14, 19);
            this.radioButtonPngNone.Name = "radioButtonPngNone";
            this.radioButtonPngNone.Size = new System.Drawing.Size(51, 17);
            this.radioButtonPngNone.TabIndex = 0;
            this.radioButtonPngNone.TabStop = true;
            this.radioButtonPngNone.Text = "None";
            this.radioButtonPngNone.UseVisualStyleBackColor = true;
            this.radioButtonPngNone.CheckedChanged += new System.EventHandler(this.radioButtonPngNone_CheckedChanged);
            // 
            // checkBoxPngInterlaced
            // 
            this.checkBoxPngInterlaced.AutoSize = true;
            this.checkBoxPngInterlaced.Location = new System.Drawing.Point(101, 189);
            this.checkBoxPngInterlaced.Name = "checkBoxPngInterlaced";
            this.checkBoxPngInterlaced.Size = new System.Drawing.Size(73, 17);
            this.checkBoxPngInterlaced.TabIndex = 0;
            this.checkBoxPngInterlaced.Text = "Interlaced";
            this.checkBoxPngInterlaced.UseVisualStyleBackColor = true;
            this.checkBoxPngInterlaced.CheckedChanged += new System.EventHandler(this.checkBoxPngInterlaced_CheckedChanged);
            // 
            // tabPageGif
            // 
            this.tabPageGif.BackColor = System.Drawing.Color.White;
            this.tabPageGif.Controls.Add(this.labelGifTransparencyColor);
            this.tabPageGif.Controls.Add(this.buttonGifTransparencyColor);
            this.tabPageGif.Controls.Add(this.groupBoxGifType);
            this.tabPageGif.Controls.Add(this.groupBoxGifTransparencyMatch);
            this.tabPageGif.Controls.Add(this.checkBoxGifInterlaced);
            this.tabPageGif.Location = new System.Drawing.Point(4, 22);
            this.tabPageGif.Name = "tabPageGif";
            this.tabPageGif.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGif.Size = new System.Drawing.Size(357, 249);
            this.tabPageGif.TabIndex = 4;
            this.tabPageGif.Text = "GIF";
            this.tabPageGif.UseVisualStyleBackColor = true;
            // 
            // labelGifTransparencyColor
            // 
            this.labelGifTransparencyColor.AutoSize = true;
            this.labelGifTransparencyColor.Location = new System.Drawing.Point(33, 169);
            this.labelGifTransparencyColor.Name = "labelGifTransparencyColor";
            this.labelGifTransparencyColor.Size = new System.Drawing.Size(102, 13);
            this.labelGifTransparencyColor.TabIndex = 4;
            this.labelGifTransparencyColor.Text = "Transparency Color:";
            // 
            // buttonGifTransparencyColor
            // 
            this.buttonGifTransparencyColor.BackColor = System.Drawing.SystemColors.Control;
            this.buttonGifTransparencyColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGifTransparencyColor.Location = new System.Drawing.Point(141, 161);
            this.buttonGifTransparencyColor.Name = "buttonGifTransparencyColor";
            this.buttonGifTransparencyColor.Size = new System.Drawing.Size(35, 28);
            this.buttonGifTransparencyColor.TabIndex = 3;
            this.buttonGifTransparencyColor.UseVisualStyleBackColor = true;
            this.buttonGifTransparencyColor.Click += new System.EventHandler(this.buttonGifTransparencyColor_Click);
            // 
            // groupBoxGifType
            // 
            this.groupBoxGifType.Controls.Add(this.radioButtonGif89a);
            this.groupBoxGifType.Controls.Add(this.radioButtonGif87a);
            this.groupBoxGifType.Location = new System.Drawing.Point(203, 53);
            this.groupBoxGifType.Name = "groupBoxGifType";
            this.groupBoxGifType.Size = new System.Drawing.Size(93, 80);
            this.groupBoxGifType.TabIndex = 2;
            this.groupBoxGifType.TabStop = false;
            this.groupBoxGifType.Text = "Type";
            // 
            // radioButtonGif89a
            // 
            this.radioButtonGif89a.AutoSize = true;
            this.radioButtonGif89a.Location = new System.Drawing.Point(16, 43);
            this.radioButtonGif89a.Name = "radioButtonGif89a";
            this.radioButtonGif89a.Size = new System.Drawing.Size(60, 17);
            this.radioButtonGif89a.TabIndex = 1;
            this.radioButtonGif89a.TabStop = true;
            this.radioButtonGif89a.Text = "GIF89a";
            this.radioButtonGif89a.UseVisualStyleBackColor = true;
            this.radioButtonGif89a.CheckedChanged += new System.EventHandler(this.radioButtonGif89a_CheckedChanged);
            // 
            // radioButtonGif87a
            // 
            this.radioButtonGif87a.AutoSize = true;
            this.radioButtonGif87a.Location = new System.Drawing.Point(16, 19);
            this.radioButtonGif87a.Name = "radioButtonGif87a";
            this.radioButtonGif87a.Size = new System.Drawing.Size(60, 17);
            this.radioButtonGif87a.TabIndex = 0;
            this.radioButtonGif87a.TabStop = true;
            this.radioButtonGif87a.Text = "GIF87a";
            this.radioButtonGif87a.UseVisualStyleBackColor = true;
            this.radioButtonGif87a.CheckedChanged += new System.EventHandler(this.radioButtonGif87a_CheckedChanged);
            // 
            // groupBoxGifTransparencyMatch
            // 
            this.groupBoxGifTransparencyMatch.Controls.Add(this.radioButtonGifExact);
            this.groupBoxGifTransparencyMatch.Controls.Add(this.radioButtonGifClosest);
            this.groupBoxGifTransparencyMatch.Controls.Add(this.radioButtonGifNone);
            this.groupBoxGifTransparencyMatch.Location = new System.Drawing.Point(36, 53);
            this.groupBoxGifTransparencyMatch.Name = "groupBoxGifTransparencyMatch";
            this.groupBoxGifTransparencyMatch.Size = new System.Drawing.Size(140, 94);
            this.groupBoxGifTransparencyMatch.TabIndex = 1;
            this.groupBoxGifTransparencyMatch.TabStop = false;
            this.groupBoxGifTransparencyMatch.Text = "Transparency Match";
            // 
            // radioButtonGifExact
            // 
            this.radioButtonGifExact.AutoSize = true;
            this.radioButtonGifExact.Location = new System.Drawing.Point(14, 63);
            this.radioButtonGifExact.Name = "radioButtonGifExact";
            this.radioButtonGifExact.Size = new System.Drawing.Size(52, 17);
            this.radioButtonGifExact.TabIndex = 2;
            this.radioButtonGifExact.TabStop = true;
            this.radioButtonGifExact.Text = "Exact";
            this.radioButtonGifExact.UseVisualStyleBackColor = true;
            this.radioButtonGifExact.CheckedChanged += new System.EventHandler(this.radioButtonGifExact_CheckedChanged);
            // 
            // radioButtonGifClosest
            // 
            this.radioButtonGifClosest.AutoSize = true;
            this.radioButtonGifClosest.Location = new System.Drawing.Point(14, 42);
            this.radioButtonGifClosest.Name = "radioButtonGifClosest";
            this.radioButtonGifClosest.Size = new System.Drawing.Size(59, 17);
            this.radioButtonGifClosest.TabIndex = 1;
            this.radioButtonGifClosest.TabStop = true;
            this.radioButtonGifClosest.Text = "Closest";
            this.radioButtonGifClosest.UseVisualStyleBackColor = true;
            this.radioButtonGifClosest.CheckedChanged += new System.EventHandler(this.radioButtonGifClosest_CheckedChanged);
            // 
            // radioButtonGifNone
            // 
            this.radioButtonGifNone.AutoSize = true;
            this.radioButtonGifNone.Location = new System.Drawing.Point(14, 19);
            this.radioButtonGifNone.Name = "radioButtonGifNone";
            this.radioButtonGifNone.Size = new System.Drawing.Size(51, 17);
            this.radioButtonGifNone.TabIndex = 0;
            this.radioButtonGifNone.TabStop = true;
            this.radioButtonGifNone.Text = "None";
            this.radioButtonGifNone.UseVisualStyleBackColor = true;
            this.radioButtonGifNone.CheckedChanged += new System.EventHandler(this.radioButtonGifNone_CheckedChanged);
            // 
            // checkBoxGifInterlaced
            // 
            this.checkBoxGifInterlaced.AutoSize = true;
            this.checkBoxGifInterlaced.Location = new System.Drawing.Point(36, 204);
            this.checkBoxGifInterlaced.Name = "checkBoxGifInterlaced";
            this.checkBoxGifInterlaced.Size = new System.Drawing.Size(73, 17);
            this.checkBoxGifInterlaced.TabIndex = 0;
            this.checkBoxGifInterlaced.Text = "Interlaced";
            this.checkBoxGifInterlaced.UseVisualStyleBackColor = true;
            this.checkBoxGifInterlaced.CheckedChanged += new System.EventHandler(this.checkBoxGifInterlaced_CheckedChanged);
            // 
            // tabPageJpeg2000
            // 
            this.tabPageJpeg2000.BackColor = System.Drawing.Color.White;
            this.tabPageJpeg2000.Controls.Add(this.labelJp2Peak);
            this.tabPageJpeg2000.Controls.Add(this.numericUpDownJp2Peak);
            this.tabPageJpeg2000.Controls.Add(this.groupBoxJp2TileSize);
            this.tabPageJpeg2000.Controls.Add(this.numericUpDownJp2CompressSize);
            this.tabPageJpeg2000.Controls.Add(this.labelJp2CompressSize);
            this.tabPageJpeg2000.Controls.Add(this.groupBoxJp2Type);
            this.tabPageJpeg2000.Controls.Add(this.groupBoxJp2Order);
            this.tabPageJpeg2000.Location = new System.Drawing.Point(4, 22);
            this.tabPageJpeg2000.Name = "tabPageJpeg2000";
            this.tabPageJpeg2000.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageJpeg2000.Size = new System.Drawing.Size(357, 249);
            this.tabPageJpeg2000.TabIndex = 1;
            this.tabPageJpeg2000.Text = "JPEG 2000";
            this.tabPageJpeg2000.UseVisualStyleBackColor = true;
            // 
            // labelJp2Peak
            // 
            this.labelJp2Peak.Location = new System.Drawing.Point(239, 119);
            this.labelJp2Peak.Name = "labelJp2Peak";
            this.labelJp2Peak.Size = new System.Drawing.Size(86, 27);
            this.labelJp2Peak.TabIndex = 11;
            this.labelJp2Peak.Text = "Peak Signal To Noise Ratio:";
            // 
            // numericUpDownJp2Peak
            // 
            this.numericUpDownJp2Peak.DecimalPlaces = 1;
            this.numericUpDownJp2Peak.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownJp2Peak.Location = new System.Drawing.Point(242, 149);
            this.numericUpDownJp2Peak.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownJp2Peak.Name = "numericUpDownJp2Peak";
            this.numericUpDownJp2Peak.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownJp2Peak.TabIndex = 10;
            this.numericUpDownJp2Peak.ValueChanged += new System.EventHandler(this.numericUpDownJp2Peak_ValueChanged);
            // 
            // groupBoxJp2TileSize
            // 
            this.groupBoxJp2TileSize.Controls.Add(this.labelJp2TileHeight);
            this.groupBoxJp2TileSize.Controls.Add(this.labelJp2TileWidth);
            this.groupBoxJp2TileSize.Controls.Add(this.numericUpDownJp2TileHeight);
            this.groupBoxJp2TileSize.Controls.Add(this.numericUpDownJp2TileWidth);
            this.groupBoxJp2TileSize.Location = new System.Drawing.Point(6, 175);
            this.groupBoxJp2TileSize.Name = "groupBoxJp2TileSize";
            this.groupBoxJp2TileSize.Size = new System.Drawing.Size(135, 64);
            this.groupBoxJp2TileSize.TabIndex = 9;
            this.groupBoxJp2TileSize.TabStop = false;
            this.groupBoxJp2TileSize.Text = "Tile Size";
            // 
            // labelJp2TileHeight
            // 
            this.labelJp2TileHeight.AutoSize = true;
            this.labelJp2TileHeight.Location = new System.Drawing.Point(67, 19);
            this.labelJp2TileHeight.Name = "labelJp2TileHeight";
            this.labelJp2TileHeight.Size = new System.Drawing.Size(38, 13);
            this.labelJp2TileHeight.TabIndex = 10;
            this.labelJp2TileHeight.Text = "Height";
            // 
            // labelJp2TileWidth
            // 
            this.labelJp2TileWidth.AutoSize = true;
            this.labelJp2TileWidth.Location = new System.Drawing.Point(5, 19);
            this.labelJp2TileWidth.Name = "labelJp2TileWidth";
            this.labelJp2TileWidth.Size = new System.Drawing.Size(35, 13);
            this.labelJp2TileWidth.TabIndex = 9;
            this.labelJp2TileWidth.Text = "Width";
            // 
            // numericUpDownJp2TileHeight
            // 
            this.numericUpDownJp2TileHeight.Location = new System.Drawing.Point(70, 35);
            this.numericUpDownJp2TileHeight.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownJp2TileHeight.Name = "numericUpDownJp2TileHeight";
            this.numericUpDownJp2TileHeight.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownJp2TileHeight.TabIndex = 8;
            this.numericUpDownJp2TileHeight.ValueChanged += new System.EventHandler(this.numericUpDownJp2TileHeight_ValueChanged);
            // 
            // numericUpDownJp2TileWidth
            // 
            this.numericUpDownJp2TileWidth.Location = new System.Drawing.Point(8, 35);
            this.numericUpDownJp2TileWidth.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownJp2TileWidth.Name = "numericUpDownJp2TileWidth";
            this.numericUpDownJp2TileWidth.Size = new System.Drawing.Size(57, 20);
            this.numericUpDownJp2TileWidth.TabIndex = 6;
            this.numericUpDownJp2TileWidth.ValueChanged += new System.EventHandler(this.numericUpDownJp2TileWidth_ValueChanged);
            // 
            // numericUpDownJp2CompressSize
            // 
            this.numericUpDownJp2CompressSize.Location = new System.Drawing.Point(242, 93);
            this.numericUpDownJp2CompressSize.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownJp2CompressSize.Name = "numericUpDownJp2CompressSize";
            this.numericUpDownJp2CompressSize.Size = new System.Drawing.Size(82, 20);
            this.numericUpDownJp2CompressSize.TabIndex = 0;
            this.numericUpDownJp2CompressSize.ValueChanged += new System.EventHandler(this.numericUpDownJp2CompressSize_ValueChanged);
            // 
            // labelJp2CompressSize
            // 
            this.labelJp2CompressSize.AutoSize = true;
            this.labelJp2CompressSize.Location = new System.Drawing.Point(239, 77);
            this.labelJp2CompressSize.Name = "labelJp2CompressSize";
            this.labelJp2CompressSize.Size = new System.Drawing.Size(79, 13);
            this.labelJp2CompressSize.TabIndex = 1;
            this.labelJp2CompressSize.Text = "Compress Size:";
            // 
            // groupBoxJp2Type
            // 
            this.groupBoxJp2Type.Controls.Add(this.radioButtonJp2Lossy);
            this.groupBoxJp2Type.Controls.Add(this.radioButtonJp2Lossless);
            this.groupBoxJp2Type.Location = new System.Drawing.Point(242, 6);
            this.groupBoxJp2Type.Name = "groupBoxJp2Type";
            this.groupBoxJp2Type.Size = new System.Drawing.Size(85, 65);
            this.groupBoxJp2Type.TabIndex = 5;
            this.groupBoxJp2Type.TabStop = false;
            this.groupBoxJp2Type.Text = "Type";
            // 
            // radioButtonJp2Lossy
            // 
            this.radioButtonJp2Lossy.AutoSize = true;
            this.radioButtonJp2Lossy.Location = new System.Drawing.Point(11, 41);
            this.radioButtonJp2Lossy.Name = "radioButtonJp2Lossy";
            this.radioButtonJp2Lossy.Size = new System.Drawing.Size(52, 17);
            this.radioButtonJp2Lossy.TabIndex = 1;
            this.radioButtonJp2Lossy.TabStop = true;
            this.radioButtonJp2Lossy.Text = "Lossy";
            this.radioButtonJp2Lossy.UseVisualStyleBackColor = true;
            this.radioButtonJp2Lossy.CheckedChanged += new System.EventHandler(this.radioButtonJp2Lossy_CheckedChanged);
            // 
            // radioButtonJp2Lossless
            // 
            this.radioButtonJp2Lossless.AutoSize = true;
            this.radioButtonJp2Lossless.Location = new System.Drawing.Point(11, 18);
            this.radioButtonJp2Lossless.Name = "radioButtonJp2Lossless";
            this.radioButtonJp2Lossless.Size = new System.Drawing.Size(65, 17);
            this.radioButtonJp2Lossless.TabIndex = 0;
            this.radioButtonJp2Lossless.TabStop = true;
            this.radioButtonJp2Lossless.Text = "Lossless";
            this.radioButtonJp2Lossless.UseVisualStyleBackColor = true;
            this.radioButtonJp2Lossless.CheckedChanged += new System.EventHandler(this.radioButtonJp2Lossless_CheckedChanged);
            // 
            // groupBoxJp2Order
            // 
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderRpcl);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderRlcp);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderPcrl);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderLrcp);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderCprl);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderDefault);
            this.groupBoxJp2Order.Location = new System.Drawing.Point(6, 6);
            this.groupBoxJp2Order.Name = "groupBoxJp2Order";
            this.groupBoxJp2Order.Size = new System.Drawing.Size(228, 164);
            this.groupBoxJp2Order.TabIndex = 3;
            this.groupBoxJp2Order.TabStop = false;
            this.groupBoxJp2Order.Text = "Order";
            // 
            // radioButtonJp2OrderRpcl
            // 
            this.radioButtonJp2OrderRpcl.AutoSize = true;
            this.radioButtonJp2OrderRpcl.Location = new System.Drawing.Point(16, 134);
            this.radioButtonJp2OrderRpcl.Name = "radioButtonJp2OrderRpcl";
            this.radioButtonJp2OrderRpcl.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderRpcl.TabIndex = 5;
            this.radioButtonJp2OrderRpcl.TabStop = true;
            this.radioButtonJp2OrderRpcl.Text = "Resolution Position Component Layer";
            this.radioButtonJp2OrderRpcl.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderRpcl.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderRpcl_CheckedChanged);
            // 
            // radioButtonJp2OrderRlcp
            // 
            this.radioButtonJp2OrderRlcp.AutoSize = true;
            this.radioButtonJp2OrderRlcp.Location = new System.Drawing.Point(16, 111);
            this.radioButtonJp2OrderRlcp.Name = "radioButtonJp2OrderRlcp";
            this.radioButtonJp2OrderRlcp.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderRlcp.TabIndex = 4;
            this.radioButtonJp2OrderRlcp.TabStop = true;
            this.radioButtonJp2OrderRlcp.Text = "Resolution Layer Component Position";
            this.radioButtonJp2OrderRlcp.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderRlcp.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderRlcp_CheckedChanged);
            // 
            // radioButtonJp2OrderPcrl
            // 
            this.radioButtonJp2OrderPcrl.AutoSize = true;
            this.radioButtonJp2OrderPcrl.Location = new System.Drawing.Point(16, 88);
            this.radioButtonJp2OrderPcrl.Name = "radioButtonJp2OrderPcrl";
            this.radioButtonJp2OrderPcrl.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderPcrl.TabIndex = 3;
            this.radioButtonJp2OrderPcrl.TabStop = true;
            this.radioButtonJp2OrderPcrl.Text = "Position Component Resolution Layer";
            this.radioButtonJp2OrderPcrl.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderPcrl.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderPcrl_CheckedChanged);
            // 
            // radioButtonJp2OrderLrcp
            // 
            this.radioButtonJp2OrderLrcp.AutoSize = true;
            this.radioButtonJp2OrderLrcp.Location = new System.Drawing.Point(16, 65);
            this.radioButtonJp2OrderLrcp.Name = "radioButtonJp2OrderLrcp";
            this.radioButtonJp2OrderLrcp.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderLrcp.TabIndex = 2;
            this.radioButtonJp2OrderLrcp.TabStop = true;
            this.radioButtonJp2OrderLrcp.Text = "Layer Resolution Component Position";
            this.radioButtonJp2OrderLrcp.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderLrcp.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderLrcp_CheckedChanged);
            // 
            // radioButtonJp2OrderCprl
            // 
            this.radioButtonJp2OrderCprl.AutoSize = true;
            this.radioButtonJp2OrderCprl.Location = new System.Drawing.Point(16, 42);
            this.radioButtonJp2OrderCprl.Name = "radioButtonJp2OrderCprl";
            this.radioButtonJp2OrderCprl.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderCprl.TabIndex = 1;
            this.radioButtonJp2OrderCprl.TabStop = true;
            this.radioButtonJp2OrderCprl.Text = "Component Position Resolution Layer";
            this.radioButtonJp2OrderCprl.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderCprl.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderCprl_CheckedChanged);
            // 
            // radioButtonJp2OrderDefault
            // 
            this.radioButtonJp2OrderDefault.AutoSize = true;
            this.radioButtonJp2OrderDefault.Location = new System.Drawing.Point(16, 19);
            this.radioButtonJp2OrderDefault.Name = "radioButtonJp2OrderDefault";
            this.radioButtonJp2OrderDefault.Size = new System.Drawing.Size(59, 17);
            this.radioButtonJp2OrderDefault.TabIndex = 0;
            this.radioButtonJp2OrderDefault.TabStop = true;
            this.radioButtonJp2OrderDefault.Text = "Default";
            this.radioButtonJp2OrderDefault.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderDefault.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderDefault_CheckedChanged);
            // 
            // tabPageJpeg
            // 
            this.tabPageJpeg.BackColor = System.Drawing.Color.White;
            this.tabPageJpeg.Controls.Add(this.numericUpDownExifThumbnailSize);
            this.tabPageJpeg.Controls.Add(this.labelExifThumbnailSize);
            this.tabPageJpeg.Controls.Add(this.groupBoxJpegWrap);
            this.tabPageJpeg.Controls.Add(this.checkBoxJpegProgressive);
            this.tabPageJpeg.Controls.Add(this.groupBoxSubSampling);
            this.tabPageJpeg.Controls.Add(this.checkBoxJpegCosited);
            this.tabPageJpeg.Controls.Add(this.labelChrominanceValue);
            this.tabPageJpeg.Controls.Add(this.labelLuminanceValue);
            this.tabPageJpeg.Controls.Add(this.labelChrominance);
            this.tabPageJpeg.Controls.Add(this.labelLuminance);
            this.tabPageJpeg.Controls.Add(this.hScrollBarChrominance);
            this.tabPageJpeg.Controls.Add(this.hScrollBarLuminance);
            this.tabPageJpeg.Location = new System.Drawing.Point(4, 22);
            this.tabPageJpeg.Name = "tabPageJpeg";
            this.tabPageJpeg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageJpeg.Size = new System.Drawing.Size(357, 249);
            this.tabPageJpeg.TabIndex = 0;
            this.tabPageJpeg.Text = "JPEG";
            this.tabPageJpeg.UseVisualStyleBackColor = true;
            // 
            // numericUpDownExifThumbnailSize
            // 
            this.numericUpDownExifThumbnailSize.Enabled = false;
            this.numericUpDownExifThumbnailSize.Location = new System.Drawing.Point(238, 180);
            this.numericUpDownExifThumbnailSize.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.numericUpDownExifThumbnailSize.Name = "numericUpDownExifThumbnailSize";
            this.numericUpDownExifThumbnailSize.Size = new System.Drawing.Size(61, 20);
            this.numericUpDownExifThumbnailSize.TabIndex = 5;
            this.numericUpDownExifThumbnailSize.ValueChanged += new System.EventHandler(this.numericUpDownExifThumbnailSize_ValueChanged);
            // 
            // labelExifThumbnailSize
            // 
            this.labelExifThumbnailSize.AutoSize = true;
            this.labelExifThumbnailSize.Location = new System.Drawing.Point(235, 164);
            this.labelExifThumbnailSize.Name = "labelExifThumbnailSize";
            this.labelExifThumbnailSize.Size = new System.Drawing.Size(102, 13);
            this.labelExifThumbnailSize.TabIndex = 6;
            this.labelExifThumbnailSize.Text = "Exif Thumbnail Size:";
            // 
            // groupBoxJpegWrap
            // 
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegWrapInExif);
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegNoWrap);
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegWrapInPdf);
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegWrapInTiff);
            this.groupBoxJpegWrap.Location = new System.Drawing.Point(113, 97);
            this.groupBoxJpegWrap.Name = "groupBoxJpegWrap";
            this.groupBoxJpegWrap.Size = new System.Drawing.Size(119, 108);
            this.groupBoxJpegWrap.TabIndex = 18;
            this.groupBoxJpegWrap.TabStop = false;
            this.groupBoxJpegWrap.Text = "Wrap";
            // 
            // radioButtonJpegWrapInExif
            // 
            this.radioButtonJpegWrapInExif.AutoSize = true;
            this.radioButtonJpegWrapInExif.Location = new System.Drawing.Point(9, 78);
            this.radioButtonJpegWrapInExif.Name = "radioButtonJpegWrapInExif";
            this.radioButtonJpegWrapInExif.Size = new System.Drawing.Size(103, 17);
            this.radioButtonJpegWrapInExif.TabIndex = 4;
            this.radioButtonJpegWrapInExif.Text = "Wrap in an EXIF";
            this.radioButtonJpegWrapInExif.UseVisualStyleBackColor = true;
            this.radioButtonJpegWrapInExif.CheckedChanged += new System.EventHandler(this.radioButtonJpegWrapInExif_CheckedChanged);
            // 
            // radioButtonJpegNoWrap
            // 
            this.radioButtonJpegNoWrap.AutoSize = true;
            this.radioButtonJpegNoWrap.Checked = true;
            this.radioButtonJpegNoWrap.Location = new System.Drawing.Point(9, 19);
            this.radioButtonJpegNoWrap.Name = "radioButtonJpegNoWrap";
            this.radioButtonJpegNoWrap.Size = new System.Drawing.Size(83, 17);
            this.radioButtonJpegNoWrap.TabIndex = 3;
            this.radioButtonJpegNoWrap.TabStop = true;
            this.radioButtonJpegNoWrap.Text = "Do not wrap";
            this.radioButtonJpegNoWrap.UseVisualStyleBackColor = true;
            this.radioButtonJpegNoWrap.CheckedChanged += new System.EventHandler(this.radioButtonJpegNoWrap_CheckedChanged);
            // 
            // radioButtonJpegWrapInPdf
            // 
            this.radioButtonJpegWrapInPdf.AutoSize = true;
            this.radioButtonJpegWrapInPdf.Location = new System.Drawing.Point(9, 59);
            this.radioButtonJpegWrapInPdf.Name = "radioButtonJpegWrapInPdf";
            this.radioButtonJpegWrapInPdf.Size = new System.Drawing.Size(95, 17);
            this.radioButtonJpegWrapInPdf.TabIndex = 2;
            this.radioButtonJpegWrapInPdf.Text = "Wrap in a PDF";
            this.radioButtonJpegWrapInPdf.UseVisualStyleBackColor = true;
            this.radioButtonJpegWrapInPdf.CheckedChanged += new System.EventHandler(this.radioButtonJpegWrapInPdf_CheckedChanged);
            // 
            // radioButtonJpegWrapInTiff
            // 
            this.radioButtonJpegWrapInTiff.AutoSize = true;
            this.radioButtonJpegWrapInTiff.Location = new System.Drawing.Point(9, 38);
            this.radioButtonJpegWrapInTiff.Name = "radioButtonJpegWrapInTiff";
            this.radioButtonJpegWrapInTiff.Size = new System.Drawing.Size(96, 17);
            this.radioButtonJpegWrapInTiff.TabIndex = 1;
            this.radioButtonJpegWrapInTiff.Text = "Wrap in a TIFF";
            this.radioButtonJpegWrapInTiff.UseVisualStyleBackColor = true;
            this.radioButtonJpegWrapInTiff.CheckedChanged += new System.EventHandler(this.radioButtonJpegWrapInTiff_CheckedChanged);
            // 
            // checkBoxJpegProgressive
            // 
            this.checkBoxJpegProgressive.AutoSize = true;
            this.checkBoxJpegProgressive.Location = new System.Drawing.Point(238, 139);
            this.checkBoxJpegProgressive.Name = "checkBoxJpegProgressive";
            this.checkBoxJpegProgressive.Size = new System.Drawing.Size(81, 17);
            this.checkBoxJpegProgressive.TabIndex = 17;
            this.checkBoxJpegProgressive.Text = "Progressive";
            this.checkBoxJpegProgressive.UseVisualStyleBackColor = true;
            this.checkBoxJpegProgressive.CheckedChanged += new System.EventHandler(this.checkBoxJpegProgressive_CheckedChanged);
            // 
            // groupBoxSubSampling
            // 
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling411);
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling211v);
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling211);
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling111);
            this.groupBoxSubSampling.Location = new System.Drawing.Point(17, 97);
            this.groupBoxSubSampling.Name = "groupBoxSubSampling";
            this.groupBoxSubSampling.Size = new System.Drawing.Size(88, 108);
            this.groupBoxSubSampling.TabIndex = 16;
            this.groupBoxSubSampling.TabStop = false;
            this.groupBoxSubSampling.Text = "Sub Sampling";
            // 
            // radioButtonJpegSubSampling411
            // 
            this.radioButtonJpegSubSampling411.AutoSize = true;
            this.radioButtonJpegSubSampling411.Location = new System.Drawing.Point(20, 78);
            this.radioButtonJpegSubSampling411.Name = "radioButtonJpegSubSampling411";
            this.radioButtonJpegSubSampling411.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJpegSubSampling411.TabIndex = 14;
            this.radioButtonJpegSubSampling411.TabStop = true;
            this.radioButtonJpegSubSampling411.Text = "411";
            this.radioButtonJpegSubSampling411.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling411.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling411_CheckedChanged);
            // 
            // radioButtonJpegSubSampling211v
            // 
            this.radioButtonJpegSubSampling211v.AutoSize = true;
            this.radioButtonJpegSubSampling211v.Location = new System.Drawing.Point(20, 59);
            this.radioButtonJpegSubSampling211v.Name = "radioButtonJpegSubSampling211v";
            this.radioButtonJpegSubSampling211v.Size = new System.Drawing.Size(49, 17);
            this.radioButtonJpegSubSampling211v.TabIndex = 13;
            this.radioButtonJpegSubSampling211v.TabStop = true;
            this.radioButtonJpegSubSampling211v.Text = "211v";
            this.radioButtonJpegSubSampling211v.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling211v.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling211v_CheckedChanged);
            // 
            // radioButtonJpegSubSampling211
            // 
            this.radioButtonJpegSubSampling211.AutoSize = true;
            this.radioButtonJpegSubSampling211.Location = new System.Drawing.Point(20, 38);
            this.radioButtonJpegSubSampling211.Name = "radioButtonJpegSubSampling211";
            this.radioButtonJpegSubSampling211.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJpegSubSampling211.TabIndex = 12;
            this.radioButtonJpegSubSampling211.TabStop = true;
            this.radioButtonJpegSubSampling211.Text = "211";
            this.radioButtonJpegSubSampling211.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling211.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling211_CheckedChanged);
            // 
            // radioButtonJpegSubSampling111
            // 
            this.radioButtonJpegSubSampling111.AutoSize = true;
            this.radioButtonJpegSubSampling111.Location = new System.Drawing.Point(20, 19);
            this.radioButtonJpegSubSampling111.Name = "radioButtonJpegSubSampling111";
            this.radioButtonJpegSubSampling111.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJpegSubSampling111.TabIndex = 11;
            this.radioButtonJpegSubSampling111.TabStop = true;
            this.radioButtonJpegSubSampling111.Text = "111";
            this.radioButtonJpegSubSampling111.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling111.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling111_CheckedChanged);
            // 
            // checkBoxJpegCosited
            // 
            this.checkBoxJpegCosited.AutoSize = true;
            this.checkBoxJpegCosited.Location = new System.Drawing.Point(238, 116);
            this.checkBoxJpegCosited.Name = "checkBoxJpegCosited";
            this.checkBoxJpegCosited.Size = new System.Drawing.Size(61, 17);
            this.checkBoxJpegCosited.TabIndex = 7;
            this.checkBoxJpegCosited.Text = "Cosited";
            this.checkBoxJpegCosited.UseVisualStyleBackColor = true;
            this.checkBoxJpegCosited.CheckedChanged += new System.EventHandler(this.checkBoxJpegCosited_CheckedChanged);
            // 
            // labelChrominanceValue
            // 
            this.labelChrominanceValue.AutoSize = true;
            this.labelChrominanceValue.Location = new System.Drawing.Point(294, 71);
            this.labelChrominanceValue.Name = "labelChrominanceValue";
            this.labelChrominanceValue.Size = new System.Drawing.Size(36, 13);
            this.labelChrominanceValue.TabIndex = 6;
            this.labelChrominanceValue.Text = "chrom";
            // 
            // labelLuminanceValue
            // 
            this.labelLuminanceValue.AutoSize = true;
            this.labelLuminanceValue.Location = new System.Drawing.Point(294, 36);
            this.labelLuminanceValue.Name = "labelLuminanceValue";
            this.labelLuminanceValue.Size = new System.Drawing.Size(23, 13);
            this.labelLuminanceValue.TabIndex = 5;
            this.labelLuminanceValue.Text = "lum";
            // 
            // labelChrominance
            // 
            this.labelChrominance.AutoSize = true;
            this.labelChrominance.Location = new System.Drawing.Point(14, 68);
            this.labelChrominance.Name = "labelChrominance";
            this.labelChrominance.Size = new System.Drawing.Size(72, 13);
            this.labelChrominance.TabIndex = 4;
            this.labelChrominance.Text = "Chrominance:";
            // 
            // labelLuminance
            // 
            this.labelLuminance.AutoSize = true;
            this.labelLuminance.Location = new System.Drawing.Point(15, 34);
            this.labelLuminance.Name = "labelLuminance";
            this.labelLuminance.Size = new System.Drawing.Size(62, 13);
            this.labelLuminance.TabIndex = 3;
            this.labelLuminance.Text = "Luminance:";
            // 
            // hScrollBarChrominance
            // 
            this.hScrollBarChrominance.LargeChange = 1;
            this.hScrollBarChrominance.Location = new System.Drawing.Point(89, 68);
            this.hScrollBarChrominance.Maximum = 255;
            this.hScrollBarChrominance.Name = "hScrollBarChrominance";
            this.hScrollBarChrominance.Size = new System.Drawing.Size(192, 19);
            this.hScrollBarChrominance.TabIndex = 2;
            this.hScrollBarChrominance.ValueChanged += new System.EventHandler(this.hScrollBarChrominance_ValueChanged);
            // 
            // hScrollBarLuminance
            // 
            this.hScrollBarLuminance.LargeChange = 1;
            this.hScrollBarLuminance.Location = new System.Drawing.Point(88, 34);
            this.hScrollBarLuminance.Maximum = 255;
            this.hScrollBarLuminance.Name = "hScrollBarLuminance";
            this.hScrollBarLuminance.Size = new System.Drawing.Size(193, 17);
            this.hScrollBarLuminance.TabIndex = 1;
            this.hScrollBarLuminance.ValueChanged += new System.EventHandler(this.hScrollBarLuminance_ValueChanged);
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Controls.Add(this.tabPageJpeg);
            this.tabControlSettings.Controls.Add(this.tabPageJpeg2000);
            this.tabControlSettings.Controls.Add(this.tabPageHdp);
            this.tabControlSettings.Controls.Add(this.tabPageGif);
            this.tabControlSettings.Controls.Add(this.tabPagePng);
            this.tabControlSettings.Controls.Add(this.tabPageLjp);
            this.tabControlSettings.Controls.Add(this.tabPageJls);
            this.tabControlSettings.Controls.Add(this.tabPagePcx);
            this.tabControlSettings.ItemSize = new System.Drawing.Size(42, 18);
            this.tabControlSettings.Location = new System.Drawing.Point(144, 23);
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(365, 275);
            this.tabControlSettings.TabIndex = 0;
            this.tabControlSettings.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageHdp
            // 
            this.tabPageHdp.BackColor = System.Drawing.Color.White;
            this.tabPageHdp.Controls.Add(this.numericUpDownHdpQuantization);
            this.tabPageHdp.Controls.Add(this.labelHdpQuantization);
            this.tabPageHdp.Controls.Add(this.groupBoxHdpOrder);
            this.tabPageHdp.Controls.Add(this.groupBoxChromaSubSampling);
            this.tabPageHdp.Location = new System.Drawing.Point(4, 22);
            this.tabPageHdp.Name = "tabPageHdp";
            this.tabPageHdp.Size = new System.Drawing.Size(357, 249);
            this.tabPageHdp.TabIndex = 5;
            this.tabPageHdp.Text = "HDP";
            this.tabPageHdp.UseVisualStyleBackColor = true;
            // 
            // numericUpDownHdpQuantization
            // 
            this.numericUpDownHdpQuantization.Location = new System.Drawing.Point(185, 165);
            this.numericUpDownHdpQuantization.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownHdpQuantization.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHdpQuantization.Name = "numericUpDownHdpQuantization";
            this.numericUpDownHdpQuantization.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownHdpQuantization.TabIndex = 9;
            this.numericUpDownHdpQuantization.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHdpQuantization.ValueChanged += new System.EventHandler(this.numericUpDownHdpQuantization_ValueChanged);
            // 
            // labelHdpQuantization
            // 
            this.labelHdpQuantization.AutoSize = true;
            this.labelHdpQuantization.Location = new System.Drawing.Point(182, 149);
            this.labelHdpQuantization.Name = "labelHdpQuantization";
            this.labelHdpQuantization.Size = new System.Drawing.Size(69, 13);
            this.labelHdpQuantization.TabIndex = 7;
            this.labelHdpQuantization.Text = "Quantization:";
            // 
            // groupBoxHdpOrder
            // 
            this.groupBoxHdpOrder.Controls.Add(this.radioButtonHdpOrderFrequency);
            this.groupBoxHdpOrder.Controls.Add(this.radioButtonHdpOrderSpacial);
            this.groupBoxHdpOrder.Location = new System.Drawing.Point(185, 68);
            this.groupBoxHdpOrder.Name = "groupBoxHdpOrder";
            this.groupBoxHdpOrder.Size = new System.Drawing.Size(121, 73);
            this.groupBoxHdpOrder.TabIndex = 1;
            this.groupBoxHdpOrder.TabStop = false;
            this.groupBoxHdpOrder.Text = "Order";
            // 
            // radioButtonHdpOrderFrequency
            // 
            this.radioButtonHdpOrderFrequency.AutoSize = true;
            this.radioButtonHdpOrderFrequency.Location = new System.Drawing.Point(17, 42);
            this.radioButtonHdpOrderFrequency.Name = "radioButtonHdpOrderFrequency";
            this.radioButtonHdpOrderFrequency.Size = new System.Drawing.Size(75, 17);
            this.radioButtonHdpOrderFrequency.TabIndex = 1;
            this.radioButtonHdpOrderFrequency.TabStop = true;
            this.radioButtonHdpOrderFrequency.Text = "Frequency";
            this.radioButtonHdpOrderFrequency.UseVisualStyleBackColor = true;
            this.radioButtonHdpOrderFrequency.CheckedChanged += new System.EventHandler(this.radioButtonHdpOrderFrequency_CheckedChanged);
            // 
            // radioButtonHdpOrderSpacial
            // 
            this.radioButtonHdpOrderSpacial.AutoSize = true;
            this.radioButtonHdpOrderSpacial.Location = new System.Drawing.Point(17, 19);
            this.radioButtonHdpOrderSpacial.Name = "radioButtonHdpOrderSpacial";
            this.radioButtonHdpOrderSpacial.Size = new System.Drawing.Size(60, 17);
            this.radioButtonHdpOrderSpacial.TabIndex = 0;
            this.radioButtonHdpOrderSpacial.TabStop = true;
            this.radioButtonHdpOrderSpacial.Text = "Spacial";
            this.radioButtonHdpOrderSpacial.UseVisualStyleBackColor = true;
            this.radioButtonHdpOrderSpacial.CheckedChanged += new System.EventHandler(this.radioButtonHdpOrderSpacial_CheckedChanged);
            // 
            // groupBoxChromaSubSampling
            // 
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling444);
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling422);
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling420);
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling400);
            this.groupBoxChromaSubSampling.Location = new System.Drawing.Point(27, 68);
            this.groupBoxChromaSubSampling.Name = "groupBoxChromaSubSampling";
            this.groupBoxChromaSubSampling.Size = new System.Drawing.Size(122, 117);
            this.groupBoxChromaSubSampling.TabIndex = 0;
            this.groupBoxChromaSubSampling.TabStop = false;
            this.groupBoxChromaSubSampling.Text = "Chroma SubSampling";
            // 
            // radioButtonHdpChromaSubSampling444
            // 
            this.radioButtonHdpChromaSubSampling444.AutoSize = true;
            this.radioButtonHdpChromaSubSampling444.Location = new System.Drawing.Point(21, 90);
            this.radioButtonHdpChromaSubSampling444.Name = "radioButtonHdpChromaSubSampling444";
            this.radioButtonHdpChromaSubSampling444.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling444.TabIndex = 3;
            this.radioButtonHdpChromaSubSampling444.TabStop = true;
            this.radioButtonHdpChromaSubSampling444.Text = "444";
            this.radioButtonHdpChromaSubSampling444.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling444.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling444_CheckedChanged);
            // 
            // radioButtonHdpChromaSubSampling422
            // 
            this.radioButtonHdpChromaSubSampling422.AutoSize = true;
            this.radioButtonHdpChromaSubSampling422.Location = new System.Drawing.Point(21, 67);
            this.radioButtonHdpChromaSubSampling422.Name = "radioButtonHdpChromaSubSampling422";
            this.radioButtonHdpChromaSubSampling422.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling422.TabIndex = 2;
            this.radioButtonHdpChromaSubSampling422.TabStop = true;
            this.radioButtonHdpChromaSubSampling422.Text = "422";
            this.radioButtonHdpChromaSubSampling422.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling422.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling422_CheckedChanged);
            // 
            // radioButtonHdpChromaSubSampling420
            // 
            this.radioButtonHdpChromaSubSampling420.AutoSize = true;
            this.radioButtonHdpChromaSubSampling420.Location = new System.Drawing.Point(21, 42);
            this.radioButtonHdpChromaSubSampling420.Name = "radioButtonHdpChromaSubSampling420";
            this.radioButtonHdpChromaSubSampling420.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling420.TabIndex = 1;
            this.radioButtonHdpChromaSubSampling420.TabStop = true;
            this.radioButtonHdpChromaSubSampling420.Text = "420";
            this.radioButtonHdpChromaSubSampling420.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling420.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling420_CheckedChanged);
            // 
            // radioButtonHdpChromaSubSampling400
            // 
            this.radioButtonHdpChromaSubSampling400.AutoSize = true;
            this.radioButtonHdpChromaSubSampling400.Location = new System.Drawing.Point(21, 19);
            this.radioButtonHdpChromaSubSampling400.Name = "radioButtonHdpChromaSubSampling400";
            this.radioButtonHdpChromaSubSampling400.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling400.TabIndex = 0;
            this.radioButtonHdpChromaSubSampling400.TabStop = true;
            this.radioButtonHdpChromaSubSampling400.Text = "400";
            this.radioButtonHdpChromaSubSampling400.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling400.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling400_CheckedChanged);
            // 
            // tabPageLjp
            // 
            this.tabPageLjp.BackColor = System.Drawing.Color.White;
            this.tabPageLjp.Controls.Add(this.labelLjpPredictor);
            this.tabPageLjp.Controls.Add(this.labelLjpOrder);
            this.tabPageLjp.Controls.Add(this.numericUpDownLjpPredictor);
            this.tabPageLjp.Controls.Add(this.numericUpDownLjpOrder);
            this.tabPageLjp.Location = new System.Drawing.Point(4, 22);
            this.tabPageLjp.Name = "tabPageLjp";
            this.tabPageLjp.Size = new System.Drawing.Size(357, 249);
            this.tabPageLjp.TabIndex = 6;
            this.tabPageLjp.Text = "LJP";
            this.tabPageLjp.UseVisualStyleBackColor = true;
            // 
            // labelLjpPredictor
            // 
            this.labelLjpPredictor.AutoSize = true;
            this.labelLjpPredictor.Location = new System.Drawing.Point(115, 130);
            this.labelLjpPredictor.Name = "labelLjpPredictor";
            this.labelLjpPredictor.Size = new System.Drawing.Size(52, 13);
            this.labelLjpPredictor.TabIndex = 4;
            this.labelLjpPredictor.Text = "Predictor:";
            // 
            // labelLjpOrder
            // 
            this.labelLjpOrder.AutoSize = true;
            this.labelLjpOrder.Location = new System.Drawing.Point(116, 100);
            this.labelLjpOrder.Name = "labelLjpOrder";
            this.labelLjpOrder.Size = new System.Drawing.Size(36, 13);
            this.labelLjpOrder.TabIndex = 3;
            this.labelLjpOrder.Text = "Order:";
            // 
            // numericUpDownLjpPredictor
            // 
            this.numericUpDownLjpPredictor.Location = new System.Drawing.Point(190, 128);
            this.numericUpDownLjpPredictor.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numericUpDownLjpPredictor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpPredictor.Name = "numericUpDownLjpPredictor";
            this.numericUpDownLjpPredictor.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownLjpPredictor.TabIndex = 2;
            this.numericUpDownLjpPredictor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpPredictor.ValueChanged += new System.EventHandler(this.numericUpDownLjpPredictor_ValueChanged);
            // 
            // numericUpDownLjpOrder
            // 
            this.numericUpDownLjpOrder.Location = new System.Drawing.Point(190, 98);
            this.numericUpDownLjpOrder.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownLjpOrder.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpOrder.Name = "numericUpDownLjpOrder";
            this.numericUpDownLjpOrder.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownLjpOrder.TabIndex = 1;
            this.numericUpDownLjpOrder.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpOrder.ValueChanged += new System.EventHandler(this.numericUpDownLjpOrder_ValueChanged);
            // 
            // tabPageJls
            // 
            this.tabPageJls.BackColor = System.Drawing.Color.White;
            this.tabPageJls.Controls.Add(this.groupBoxJlsMax);
            this.tabPageJls.Controls.Add(this.groupBoxJlsInterleave);
            this.tabPageJls.Controls.Add(this.labelJlsPoint);
            this.tabPageJls.Controls.Add(this.labelJlsNear);
            this.tabPageJls.Controls.Add(this.numericUpDownJlsPoint);
            this.tabPageJls.Controls.Add(this.numericUpDownJlsNear);
            this.tabPageJls.Location = new System.Drawing.Point(4, 22);
            this.tabPageJls.Name = "tabPageJls";
            this.tabPageJls.Size = new System.Drawing.Size(357, 249);
            this.tabPageJls.TabIndex = 7;
            this.tabPageJls.Text = "JLS";
            this.tabPageJls.UseVisualStyleBackColor = true;
            // 
            // groupBoxJlsMax
            // 
            this.groupBoxJlsMax.Controls.Add(this.radioButtonJlsMax255);
            this.groupBoxJlsMax.Controls.Add(this.radioButtonJlsMax0);
            this.groupBoxJlsMax.Location = new System.Drawing.Point(38, 99);
            this.groupBoxJlsMax.Name = "groupBoxJlsMax";
            this.groupBoxJlsMax.Size = new System.Drawing.Size(109, 64);
            this.groupBoxJlsMax.TabIndex = 20;
            this.groupBoxJlsMax.TabStop = false;
            this.groupBoxJlsMax.Text = "Max Value of Near";
            // 
            // radioButtonJlsMax255
            // 
            this.radioButtonJlsMax255.AutoSize = true;
            this.radioButtonJlsMax255.Location = new System.Drawing.Point(15, 40);
            this.radioButtonJlsMax255.Name = "radioButtonJlsMax255";
            this.radioButtonJlsMax255.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJlsMax255.TabIndex = 19;
            this.radioButtonJlsMax255.TabStop = true;
            this.radioButtonJlsMax255.Text = "255";
            this.radioButtonJlsMax255.UseVisualStyleBackColor = true;
            this.radioButtonJlsMax255.CheckedChanged += new System.EventHandler(this.radioButtonJlsMax255_CheckedChanged);
            // 
            // radioButtonJlsMax0
            // 
            this.radioButtonJlsMax0.AutoSize = true;
            this.radioButtonJlsMax0.Location = new System.Drawing.Point(15, 19);
            this.radioButtonJlsMax0.Name = "radioButtonJlsMax0";
            this.radioButtonJlsMax0.Size = new System.Drawing.Size(75, 17);
            this.radioButtonJlsMax0.TabIndex = 18;
            this.radioButtonJlsMax0.TabStop = true;
            this.radioButtonJlsMax0.Text = "0 - Optimal";
            this.radioButtonJlsMax0.UseVisualStyleBackColor = true;
            this.radioButtonJlsMax0.CheckedChanged += new System.EventHandler(this.radioButtonJlsMax0_CheckedChanged);
            // 
            // groupBoxJlsInterleave
            // 
            this.groupBoxJlsInterleave.Controls.Add(this.radioButtonJlsSampleInterleaved);
            this.groupBoxJlsInterleave.Controls.Add(this.radioButtonJlsLineInterleaved);
            this.groupBoxJlsInterleave.Controls.Add(this.radioButtonJlsNonInterleaved);
            this.groupBoxJlsInterleave.Location = new System.Drawing.Point(169, 53);
            this.groupBoxJlsInterleave.Name = "groupBoxJlsInterleave";
            this.groupBoxJlsInterleave.Size = new System.Drawing.Size(140, 96);
            this.groupBoxJlsInterleave.TabIndex = 17;
            this.groupBoxJlsInterleave.TabStop = false;
            this.groupBoxJlsInterleave.Text = "Interleave";
            // 
            // radioButtonJlsSampleInterleaved
            // 
            this.radioButtonJlsSampleInterleaved.AutoSize = true;
            this.radioButtonJlsSampleInterleaved.Location = new System.Drawing.Point(16, 66);
            this.radioButtonJlsSampleInterleaved.Name = "radioButtonJlsSampleInterleaved";
            this.radioButtonJlsSampleInterleaved.Size = new System.Drawing.Size(116, 17);
            this.radioButtonJlsSampleInterleaved.TabIndex = 2;
            this.radioButtonJlsSampleInterleaved.TabStop = true;
            this.radioButtonJlsSampleInterleaved.Text = "Sample Interleaved";
            this.radioButtonJlsSampleInterleaved.UseVisualStyleBackColor = true;
            this.radioButtonJlsSampleInterleaved.CheckedChanged += new System.EventHandler(this.radioButtonJlsSampleInterleaved_CheckedChanged);
            // 
            // radioButtonJlsLineInterleaved
            // 
            this.radioButtonJlsLineInterleaved.AutoSize = true;
            this.radioButtonJlsLineInterleaved.Location = new System.Drawing.Point(16, 43);
            this.radioButtonJlsLineInterleaved.Name = "radioButtonJlsLineInterleaved";
            this.radioButtonJlsLineInterleaved.Size = new System.Drawing.Size(101, 17);
            this.radioButtonJlsLineInterleaved.TabIndex = 1;
            this.radioButtonJlsLineInterleaved.TabStop = true;
            this.radioButtonJlsLineInterleaved.Text = "Line Interleaved";
            this.radioButtonJlsLineInterleaved.UseVisualStyleBackColor = true;
            this.radioButtonJlsLineInterleaved.CheckedChanged += new System.EventHandler(this.radioButtonJlsLineInterleaved_CheckedChanged);
            // 
            // radioButtonJlsNonInterleaved
            // 
            this.radioButtonJlsNonInterleaved.AutoSize = true;
            this.radioButtonJlsNonInterleaved.Location = new System.Drawing.Point(16, 19);
            this.radioButtonJlsNonInterleaved.Name = "radioButtonJlsNonInterleaved";
            this.radioButtonJlsNonInterleaved.Size = new System.Drawing.Size(100, 17);
            this.radioButtonJlsNonInterleaved.TabIndex = 0;
            this.radioButtonJlsNonInterleaved.TabStop = true;
            this.radioButtonJlsNonInterleaved.Text = "Non-interleaved";
            this.radioButtonJlsNonInterleaved.UseVisualStyleBackColor = true;
            this.radioButtonJlsNonInterleaved.CheckedChanged += new System.EventHandler(this.radioButtonJlsNonInterleaved_CheckedChanged);
            // 
            // labelJlsPoint
            // 
            this.labelJlsPoint.AutoSize = true;
            this.labelJlsPoint.Location = new System.Drawing.Point(35, 53);
            this.labelJlsPoint.Name = "labelJlsPoint";
            this.labelJlsPoint.Size = new System.Drawing.Size(92, 13);
            this.labelJlsPoint.TabIndex = 15;
            this.labelJlsPoint.Text = "Precision of Point:";
            // 
            // labelJlsNear
            // 
            this.labelJlsNear.AutoSize = true;
            this.labelJlsNear.Location = new System.Drawing.Point(35, 175);
            this.labelJlsNear.Name = "labelJlsNear";
            this.labelJlsNear.Size = new System.Drawing.Size(230, 13);
            this.labelJlsNear.TabIndex = 14;
            this.labelJlsNear.Text = "Error Tolerance for Near-Lossless Compression:";
            // 
            // numericUpDownJlsPoint
            // 
            this.numericUpDownJlsPoint.Location = new System.Drawing.Point(38, 69);
            this.numericUpDownJlsPoint.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownJlsPoint.Name = "numericUpDownJlsPoint";
            this.numericUpDownJlsPoint.Size = new System.Drawing.Size(89, 20);
            this.numericUpDownJlsPoint.TabIndex = 11;
            this.numericUpDownJlsPoint.ValueChanged += new System.EventHandler(this.numericUpDownJlsPoint_ValueChanged);
            // 
            // numericUpDownJlsNear
            // 
            this.numericUpDownJlsNear.Location = new System.Drawing.Point(38, 191);
            this.numericUpDownJlsNear.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownJlsNear.Name = "numericUpDownJlsNear";
            this.numericUpDownJlsNear.Size = new System.Drawing.Size(89, 20);
            this.numericUpDownJlsNear.TabIndex = 10;
            this.numericUpDownJlsNear.ValueChanged += new System.EventHandler(this.numericUpDownJlsNear_ValueChanged);
            // 
            // tabPagePcx
            // 
            this.tabPagePcx.Controls.Add(this.labelPcxNoSettings);
            this.tabPagePcx.Location = new System.Drawing.Point(4, 22);
            this.tabPagePcx.Name = "tabPagePcx";
            this.tabPagePcx.Size = new System.Drawing.Size(357, 249);
            this.tabPagePcx.TabIndex = 8;
            this.tabPagePcx.Text = "PCX";
            this.tabPagePcx.UseVisualStyleBackColor = true;
            // 
            // labelPcxNoSettings
            // 
            this.labelPcxNoSettings.AutoSize = true;
            this.labelPcxNoSettings.Location = new System.Drawing.Point(114, 120);
            this.labelPcxNoSettings.Name = "labelPcxNoSettings";
            this.labelPcxNoSettings.Size = new System.Drawing.Size(108, 13);
            this.labelPcxNoSettings.TabIndex = 0;
            this.labelPcxNoSettings.Text = "No settings available.";
            // 
            // buttonRestoreDefaults
            // 
            this.buttonRestoreDefaults.Location = new System.Drawing.Point(6, 38);
            this.buttonRestoreDefaults.Name = "buttonRestoreDefaults";
            this.buttonRestoreDefaults.Size = new System.Drawing.Size(109, 32);
            this.buttonRestoreDefaults.TabIndex = 4;
            this.buttonRestoreDefaults.Text = "Restore All Defaults";
            this.buttonRestoreDefaults.UseVisualStyleBackColor = true;
            this.buttonRestoreDefaults.Click += new System.EventHandler(this.buttonRestoreDefaults_Click);
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.BackColor = System.Drawing.Color.White;
            this.groupBoxSettings.Controls.Add(this.checkBoxPreview);
            this.groupBoxSettings.Controls.Add(this.buttonRestoreDefaults);
            this.groupBoxSettings.Location = new System.Drawing.Point(19, 220);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(123, 76);
            this.groupBoxSettings.TabIndex = 4;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // checkBoxPreview
            // 
            this.checkBoxPreview.AutoSize = true;
            this.checkBoxPreview.Checked = true;
            this.checkBoxPreview.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPreview.Location = new System.Drawing.Point(31, 19);
            this.checkBoxPreview.Name = "checkBoxPreview";
            this.checkBoxPreview.Size = new System.Drawing.Size(64, 17);
            this.checkBoxPreview.TabIndex = 5;
            this.checkBoxPreview.Text = "Preview";
            this.checkBoxPreview.UseVisualStyleBackColor = true;
            this.checkBoxPreview.CheckedChanged += new System.EventHandler(this.checkBoxPreview_CheckedChanged);
            // 
            // groupBoxCompressedImage
            // 
            this.groupBoxCompressedImage.BackColor = System.Drawing.Color.White;
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage4);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage3);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage2);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage1);
            this.groupBoxCompressedImage.Location = new System.Drawing.Point(19, 23);
            this.groupBoxCompressedImage.Name = "groupBoxCompressedImage";
            this.groupBoxCompressedImage.Size = new System.Drawing.Size(123, 45);
            this.groupBoxCompressedImage.TabIndex = 5;
            this.groupBoxCompressedImage.TabStop = false;
            this.groupBoxCompressedImage.Text = "Compressed Image";
            // 
            // radioButtonCompressedImage4
            // 
            this.radioButtonCompressedImage4.AutoSize = true;
            this.radioButtonCompressedImage4.Location = new System.Drawing.Point(92, 17);
            this.radioButtonCompressedImage4.Name = "radioButtonCompressedImage4";
            this.radioButtonCompressedImage4.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage4.TabIndex = 3;
            this.radioButtonCompressedImage4.TabStop = true;
            this.radioButtonCompressedImage4.Text = "4";
            this.radioButtonCompressedImage4.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage4.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage4_CheckedChanged);
            // 
            // radioButtonCompressedImage3
            // 
            this.radioButtonCompressedImage3.AutoSize = true;
            this.radioButtonCompressedImage3.Location = new System.Drawing.Point(62, 17);
            this.radioButtonCompressedImage3.Name = "radioButtonCompressedImage3";
            this.radioButtonCompressedImage3.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage3.TabIndex = 2;
            this.radioButtonCompressedImage3.TabStop = true;
            this.radioButtonCompressedImage3.Text = "3";
            this.radioButtonCompressedImage3.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage3.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage3_CheckedChanged);
            // 
            // radioButtonCompressedImage2
            // 
            this.radioButtonCompressedImage2.AutoSize = true;
            this.radioButtonCompressedImage2.Location = new System.Drawing.Point(32, 17);
            this.radioButtonCompressedImage2.Name = "radioButtonCompressedImage2";
            this.radioButtonCompressedImage2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage2.TabIndex = 1;
            this.radioButtonCompressedImage2.TabStop = true;
            this.radioButtonCompressedImage2.Text = "2";
            this.radioButtonCompressedImage2.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage2.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage2_CheckedChanged);
            // 
            // radioButtonCompressedImage1
            // 
            this.radioButtonCompressedImage1.AutoSize = true;
            this.radioButtonCompressedImage1.Location = new System.Drawing.Point(2, 17);
            this.radioButtonCompressedImage1.Name = "radioButtonCompressedImage1";
            this.radioButtonCompressedImage1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage1.TabIndex = 0;
            this.radioButtonCompressedImage1.TabStop = true;
            this.radioButtonCompressedImage1.Text = "1";
            this.radioButtonCompressedImage1.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage1.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage1_CheckedChanged);
            // 
            // panelBackgroundFrameColor
            // 
            this.panelBackgroundFrameColor.BackColor = System.Drawing.Color.Transparent;
            this.panelBackgroundFrameColor.Controls.Add(this.groupBoxCompressedImage);
            this.panelBackgroundFrameColor.Controls.Add(this.tabControlSettings);
            this.panelBackgroundFrameColor.Controls.Add(this.groupBoxSettings);
            this.panelBackgroundFrameColor.Controls.Add(this.groupBoxImageType);
            this.panelBackgroundFrameColor.Location = new System.Drawing.Point(12, 12);
            this.panelBackgroundFrameColor.Name = "panelBackgroundFrameColor";
            this.panelBackgroundFrameColor.Size = new System.Drawing.Size(525, 321);
            this.panelBackgroundFrameColor.TabIndex = 6;
            // 
            // SettingsFormColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ImagXpressCompressionDemo.Properties.Resources.ap_dark_stripes;
            this.ClientSize = new System.Drawing.Size(549, 375);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.panelBackgroundFrameColor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsFormColor";
            this.Text = "Color Compression Settings";
            this.Load += new System.EventHandler(this.SettingsFormColor_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SettingsFormColor_FormClosed);
            this.groupBoxImageType.ResumeLayout(false);
            this.groupBoxImageType.PerformLayout();
            this.tabPagePng.ResumeLayout(false);
            this.tabPagePng.PerformLayout();
            this.groupBoxPngTransparencyMatch.ResumeLayout(false);
            this.groupBoxPngTransparencyMatch.PerformLayout();
            this.tabPageGif.ResumeLayout(false);
            this.tabPageGif.PerformLayout();
            this.groupBoxGifType.ResumeLayout(false);
            this.groupBoxGifType.PerformLayout();
            this.groupBoxGifTransparencyMatch.ResumeLayout(false);
            this.groupBoxGifTransparencyMatch.PerformLayout();
            this.tabPageJpeg2000.ResumeLayout(false);
            this.tabPageJpeg2000.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2Peak)).EndInit();
            this.groupBoxJp2TileSize.ResumeLayout(false);
            this.groupBoxJp2TileSize.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2CompressSize)).EndInit();
            this.groupBoxJp2Type.ResumeLayout(false);
            this.groupBoxJp2Type.PerformLayout();
            this.groupBoxJp2Order.ResumeLayout(false);
            this.groupBoxJp2Order.PerformLayout();
            this.tabPageJpeg.ResumeLayout(false);
            this.tabPageJpeg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExifThumbnailSize)).EndInit();
            this.groupBoxJpegWrap.ResumeLayout(false);
            this.groupBoxJpegWrap.PerformLayout();
            this.groupBoxSubSampling.ResumeLayout(false);
            this.groupBoxSubSampling.PerformLayout();
            this.tabControlSettings.ResumeLayout(false);
            this.tabPageHdp.ResumeLayout(false);
            this.tabPageHdp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHdpQuantization)).EndInit();
            this.groupBoxHdpOrder.ResumeLayout(false);
            this.groupBoxHdpOrder.PerformLayout();
            this.groupBoxChromaSubSampling.ResumeLayout(false);
            this.groupBoxChromaSubSampling.PerformLayout();
            this.tabPageLjp.ResumeLayout(false);
            this.tabPageLjp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpPredictor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpOrder)).EndInit();
            this.tabPageJls.ResumeLayout(false);
            this.tabPageJls.PerformLayout();
            this.groupBoxJlsMax.ResumeLayout(false);
            this.groupBoxJlsMax.PerformLayout();
            this.groupBoxJlsInterleave.ResumeLayout(false);
            this.groupBoxJlsInterleave.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsNear)).EndInit();
            this.tabPagePcx.ResumeLayout(false);
            this.tabPagePcx.PerformLayout();
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.groupBoxCompressedImage.ResumeLayout(false);
            this.groupBoxCompressedImage.PerformLayout();
            this.panelBackgroundFrameColor.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.GroupBox groupBoxImageType;
		private System.Windows.Forms.RadioButton radioButtonPng;
		private System.Windows.Forms.RadioButton radioButtonGif;
        private System.Windows.Forms.RadioButton radioButtonJpeg2000;
		private System.Windows.Forms.RadioButton radioButtonJpeg;
		private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabPage tabPagePng;
        private System.Windows.Forms.TabPage tabPageGif;
        private System.Windows.Forms.TabPage tabPageJpeg2000;
        private System.Windows.Forms.TabPage tabPageJpeg;
        private System.Windows.Forms.TabControl tabControlSettings;
        private System.Windows.Forms.Button buttonRestoreDefaults;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label labelChrominance;
        private System.Windows.Forms.Label labelLuminance;
        private System.Windows.Forms.HScrollBar hScrollBarChrominance;
        private System.Windows.Forms.HScrollBar hScrollBarLuminance;
        private System.Windows.Forms.Label labelChrominanceValue;
        private System.Windows.Forms.Label labelLuminanceValue;
        private System.Windows.Forms.CheckBox checkBoxJpegCosited;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling411;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling211v;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling211;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling111;
        private System.Windows.Forms.GroupBox groupBoxSubSampling;
        private System.Windows.Forms.CheckBox checkBoxJpegProgressive;
        private System.Windows.Forms.CheckBox checkBoxPreview;
        private System.Windows.Forms.Label labelJp2CompressSize;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2CompressSize;
        private System.Windows.Forms.GroupBox groupBoxJp2Order;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderRpcl;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderRlcp;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderPcrl;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderLrcp;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderCprl;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderDefault;
        private System.Windows.Forms.GroupBox groupBoxJp2Type;
        private System.Windows.Forms.RadioButton radioButtonJp2Lossy;
        private System.Windows.Forms.RadioButton radioButtonJp2Lossless;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2TileWidth;
        private System.Windows.Forms.GroupBox groupBoxGifType;
        private System.Windows.Forms.GroupBox groupBoxGifTransparencyMatch;
        private System.Windows.Forms.CheckBox checkBoxGifInterlaced;
        private System.Windows.Forms.RadioButton radioButtonGifExact;
        private System.Windows.Forms.RadioButton radioButtonGifClosest;
        private System.Windows.Forms.RadioButton radioButtonGifNone;
        private System.Windows.Forms.RadioButton radioButtonGif89a;
        private System.Windows.Forms.RadioButton radioButtonGif87a;
        private System.Windows.Forms.Button buttonGifTransparencyColor;
        private System.Windows.Forms.Label labelGifTransparencyColor;
        private System.Windows.Forms.CheckBox checkBoxPngInterlaced;
        private System.Windows.Forms.Label labelPngTransparencyColor;
        private System.Windows.Forms.Button buttonPngTransparencyColor;
        private System.Windows.Forms.GroupBox groupBoxPngTransparencyMatch;
        private System.Windows.Forms.RadioButton radioButtonPngExact;
        private System.Windows.Forms.RadioButton radioButtonPngClosest;
        private System.Windows.Forms.RadioButton radioButtonPngNone;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2TileHeight;
        private System.Windows.Forms.GroupBox groupBoxJp2TileSize;
        private System.Windows.Forms.Label labelJp2TileHeight;
        private System.Windows.Forms.Label labelJp2TileWidth;
        private System.Windows.Forms.GroupBox groupBoxJpegWrap;
        private System.Windows.Forms.RadioButton radioButtonJpegNoWrap;
        private System.Windows.Forms.RadioButton radioButtonJpegWrapInPdf;
        private System.Windows.Forms.RadioButton radioButtonJpegWrapInTiff;
        private System.Windows.Forms.RadioButton radioButtonJpegWrapInExif;
        private System.Windows.Forms.NumericUpDown numericUpDownExifThumbnailSize;
        private System.Windows.Forms.Label labelExifThumbnailSize;
        private System.Windows.Forms.RadioButton radioButtonPcx;
        private System.Windows.Forms.RadioButton radioButtonJls;
        private System.Windows.Forms.RadioButton radioButtonLjp;
        private System.Windows.Forms.RadioButton radioButtonHdp;
        private System.Windows.Forms.TabPage tabPageHdp;
        private System.Windows.Forms.TabPage tabPageLjp;
        private System.Windows.Forms.TabPage tabPageJls;
        private System.Windows.Forms.GroupBox groupBoxHdpOrder;
        private System.Windows.Forms.RadioButton radioButtonHdpOrderFrequency;
        private System.Windows.Forms.RadioButton radioButtonHdpOrderSpacial;
        private System.Windows.Forms.GroupBox groupBoxChromaSubSampling;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling444;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling422;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling420;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling400;
        private System.Windows.Forms.Label labelHdpQuantization;
        private System.Windows.Forms.NumericUpDown numericUpDownHdpQuantization;
        private System.Windows.Forms.Label labelJlsPoint;
        private System.Windows.Forms.Label labelJlsNear;
        private System.Windows.Forms.NumericUpDown numericUpDownJlsPoint;
        private System.Windows.Forms.NumericUpDown numericUpDownJlsNear;
        private System.Windows.Forms.Label labelLjpPredictor;
        private System.Windows.Forms.Label labelLjpOrder;
        private System.Windows.Forms.NumericUpDown numericUpDownLjpPredictor;
        private System.Windows.Forms.NumericUpDown numericUpDownLjpOrder;
        private System.Windows.Forms.GroupBox groupBoxJlsInterleave;
        private System.Windows.Forms.RadioButton radioButtonJlsSampleInterleaved;
        private System.Windows.Forms.RadioButton radioButtonJlsLineInterleaved;
        private System.Windows.Forms.RadioButton radioButtonJlsNonInterleaved;
        private System.Windows.Forms.Label labelJp2Peak;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2Peak;
        private System.Windows.Forms.GroupBox groupBoxCompressedImage;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage4;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage3;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage2;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage1;
        private System.Windows.Forms.TabPage tabPagePcx;
        private System.Windows.Forms.Label labelPcxNoSettings;
        private System.Windows.Forms.GroupBox groupBoxJlsMax;
        private System.Windows.Forms.RadioButton radioButtonJlsMax255;
        private System.Windows.Forms.RadioButton radioButtonJlsMax0;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrameColor;
	}
}
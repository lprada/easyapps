/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress9;

namespace MergeCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonMerge;
		private System.Windows.Forms.CheckBox checkBoxTransparent;
		private System.Windows.Forms.ComboBox comboBoxMergeStyle;
		private System.Windows.Forms.ComboBox comboBoxTransparentColor;
		private System.Windows.Forms.ComboBox comboBoxMergeType;
		private System.Windows.Forms.HScrollBar hScrollBarMax;
		private System.Windows.Forms.HScrollBar hScrollBarMin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label labelPctMax;
		private System.Windows.Forms.Label labelPctMin;
		private System.String strCurrentDir;
		private System.String strImageFile1;
		private System.String strImageFile2;
		private System.Windows.Forms.RichTextBox rtfInfo;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView IxSource;
		private PegasusImaging.WinForms.ImagXpress9.ImageX     ImageSource;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Button buttonReloadDestination;
		private System.Boolean bSelectionActive;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView IxDest;
		private System.Windows.Forms.MenuItem menuToolbar;
		private System.Windows.Forms.MenuItem menuToolbarShow;
		private System.Windows.Forms.MenuItem menuFileOpenSource;
		private System.Windows.Forms.MenuItem menuFileOpenDest;
		private System.Windows.Forms.MenuItem menuFileQuit;
		private IContainer components;

		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			//***Dispose of the ImagXpress objects
			imagXpress1.Dispose();
			IxSource.Dispose();
			IxDest.Dispose();
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
			this.buttonMerge = new System.Windows.Forms.Button();
			this.checkBoxTransparent = new System.Windows.Forms.CheckBox();
			this.comboBoxMergeStyle = new System.Windows.Forms.ComboBox();
			this.IxSource = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
			this.comboBoxTransparentColor = new System.Windows.Forms.ComboBox();
			this.comboBoxMergeType = new System.Windows.Forms.ComboBox();
			this.hScrollBarMax = new System.Windows.Forms.HScrollBar();
			this.hScrollBarMin = new System.Windows.Forms.HScrollBar();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.labelPctMax = new System.Windows.Forms.Label();
			this.labelPctMin = new System.Windows.Forms.Label();
			this.rtfInfo = new System.Windows.Forms.RichTextBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
			this.menuFile = new System.Windows.Forms.MenuItem();
			this.menuFileOpenSource = new System.Windows.Forms.MenuItem();
			this.menuFileOpenDest = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuFileQuit = new System.Windows.Forms.MenuItem();
			this.menuToolbar = new System.Windows.Forms.MenuItem();
			this.menuToolbarShow = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.lblError = new System.Windows.Forms.Label();
			this.lblLastError = new System.Windows.Forms.Label();
			this.buttonReloadDestination = new System.Windows.Forms.Button();
			this.IxDest = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
			this.SuspendLayout();
			// 
			// buttonMerge
			// 
			this.buttonMerge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonMerge.Location = new System.Drawing.Point(403, 161);
			this.buttonMerge.Name = "buttonMerge";
			this.buttonMerge.Size = new System.Drawing.Size(202, 37);
			this.buttonMerge.TabIndex = 1;
			this.buttonMerge.Text = "Merge";
			this.buttonMerge.Click += new System.EventHandler(this.buttonMerge_Click);
			// 
			// checkBoxTransparent
			// 
			this.checkBoxTransparent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.checkBoxTransparent.Location = new System.Drawing.Point(19, 364);
			this.checkBoxTransparent.Name = "checkBoxTransparent";
			this.checkBoxTransparent.Size = new System.Drawing.Size(106, 18);
			this.checkBoxTransparent.TabIndex = 6;
			this.checkBoxTransparent.Text = "Transparent";
			this.checkBoxTransparent.Click += new System.EventHandler(this.checkBoxTransparent_Click);
			// 
			// comboBoxMergeStyle
			// 
			this.comboBoxMergeStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxMergeStyle.Items.AddRange(new object[] {
            "Normal",
            "If Darker",
            "If Lighter",
            "Additive",
            "Subtractive",
            "Superimpose",
            "Superimpose Bottom to Top",
            "Superimpose Horiz from Center",
            "Superimpose Horiz to Center",
            "Superimpose Left to Right",
            "Superimpose Right to Left",
            "Superimpose Top to Bottom",
            "Superimpose Vert. from Center",
            "Superimpose Vert. to Center"});
			this.comboBoxMergeStyle.Location = new System.Drawing.Point(307, 382);
			this.comboBoxMergeStyle.Name = "comboBoxMergeStyle";
			this.comboBoxMergeStyle.Size = new System.Drawing.Size(298, 24);
			this.comboBoxMergeStyle.TabIndex = 5;
			this.comboBoxMergeStyle.Text = "Merge Style";
			// 
			// IxSource
			// 
			this.IxSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.IxSource.Location = new System.Drawing.Point(403, 92);
			this.IxSource.Name = "IxSource";
			this.IxSource.Size = new System.Drawing.Size(202, 59);
			this.IxSource.TabIndex = 15;
			// 
			// comboBoxTransparentColor
			// 
			this.comboBoxTransparentColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.comboBoxTransparentColor.Enabled = false;
			this.comboBoxTransparentColor.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "White"});
			this.comboBoxTransparentColor.Location = new System.Drawing.Point(19, 382);
			this.comboBoxTransparentColor.Name = "comboBoxTransparentColor";
			this.comboBoxTransparentColor.Size = new System.Drawing.Size(125, 24);
			this.comboBoxTransparentColor.TabIndex = 3;
			this.comboBoxTransparentColor.Text = "Transparent Color";
			this.comboBoxTransparentColor.SelectedIndexChanged += new System.EventHandler(this.comboBoxTransparentColor_SelectedIndexChanged);
			// 
			// comboBoxMergeType
			// 
			this.comboBoxMergeType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxMergeType.Items.AddRange(new object[] {
            "Crop",
            "Resize Area",
            "Resize Image",
            "Tile Image"});
			this.comboBoxMergeType.Location = new System.Drawing.Point(182, 382);
			this.comboBoxMergeType.Name = "comboBoxMergeType";
			this.comboBoxMergeType.Size = new System.Drawing.Size(77, 24);
			this.comboBoxMergeType.TabIndex = 4;
			this.comboBoxMergeType.Text = "Merge Type";
			// 
			// hScrollBarMax
			// 
			this.hScrollBarMax.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.hScrollBarMax.LargeChange = 1;
			this.hScrollBarMax.Location = new System.Drawing.Point(211, 419);
			this.hScrollBarMax.Name = "hScrollBarMax";
			this.hScrollBarMax.Size = new System.Drawing.Size(394, 19);
			this.hScrollBarMax.TabIndex = 7;
			this.hScrollBarMax.Value = 100;
			this.hScrollBarMax.ValueChanged += new System.EventHandler(this.hScrollBarMax_ValueChanged);
			// 
			// hScrollBarMin
			// 
			this.hScrollBarMin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.hScrollBarMin.LargeChange = 1;
			this.hScrollBarMin.Location = new System.Drawing.Point(211, 456);
			this.hScrollBarMin.Name = "hScrollBarMin";
			this.hScrollBarMin.Size = new System.Drawing.Size(394, 18);
			this.hScrollBarMin.TabIndex = 7;
			this.hScrollBarMin.ValueChanged += new System.EventHandler(this.hScrollBarMin_ValueChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.Location = new System.Drawing.Point(10, 419);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(172, 19);
			this.label1.TabIndex = 18;
			this.label1.Text = "Superimpose Percent Max";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label2.Location = new System.Drawing.Point(10, 456);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(163, 18);
			this.label2.TabIndex = 19;
			this.label2.Text = "Superimpose Percent Min";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// labelPctMax
			// 
			this.labelPctMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelPctMax.Location = new System.Drawing.Point(173, 419);
			this.labelPctMax.Name = "labelPctMax";
			this.labelPctMax.Size = new System.Drawing.Size(29, 19);
			this.labelPctMax.TabIndex = 20;
			this.labelPctMax.Text = "100";
			this.labelPctMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// labelPctMin
			// 
			this.labelPctMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelPctMin.Location = new System.Drawing.Point(173, 456);
			this.labelPctMin.Name = "labelPctMin";
			this.labelPctMin.Size = new System.Drawing.Size(29, 18);
			this.labelPctMin.TabIndex = 21;
			this.labelPctMin.Text = "0";
			this.labelPctMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// rtfInfo
			// 
			this.rtfInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.rtfInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rtfInfo.Location = new System.Drawing.Point(19, 9);
			this.rtfInfo.Name = "rtfInfo";
			this.rtfInfo.Size = new System.Drawing.Size(586, 74);
			this.rtfInfo.TabIndex = 22;
			this.rtfInfo.Text = "This example demonstrates how to use the Area and Merge methods to combine images" +
				" together.";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuFile,
            this.menuToolbar,
            this.menuItem2});
			// 
			// menuFile
			// 
			this.menuFile.Index = 0;
			this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuFileOpenSource,
            this.menuFileOpenDest,
            this.menuItem5,
            this.menuFileQuit});
			this.menuFile.Text = "&File";
			// 
			// menuFileOpenSource
			// 
			this.menuFileOpenSource.Index = 0;
			this.menuFileOpenSource.Text = "Open &Source Image";
			this.menuFileOpenSource.Click += new System.EventHandler(this.menuFileOpenSource_Click);
			// 
			// menuFileOpenDest
			// 
			this.menuFileOpenDest.Index = 1;
			this.menuFileOpenDest.Text = "Open &Destination Image";
			this.menuFileOpenDest.Click += new System.EventHandler(this.menuFileOpenDest_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 2;
			this.menuItem5.Text = "-";
			// 
			// menuFileQuit
			// 
			this.menuFileQuit.Index = 3;
			this.menuFileQuit.Text = "&Quit";
			this.menuFileQuit.Click += new System.EventHandler(this.menuFileQuit_Click);
			// 
			// menuToolbar
			// 
			this.menuToolbar.Index = 1;
			this.menuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuToolbarShow});
			this.menuToolbar.Text = "&Toolbar";
			// 
			// menuToolbarShow
			// 
			this.menuToolbarShow.Index = 0;
			this.menuToolbarShow.Text = "&Show";
			this.menuToolbarShow.Click += new System.EventHandler(this.menuToolbarShow_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 2;
			this.menuItem2.Text = "&About";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// lblError
			// 
			this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lblError.Location = new System.Drawing.Point(86, 484);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(528, 32);
			this.lblError.TabIndex = 24;
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblLastError.Location = new System.Drawing.Point(10, 484);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(76, 18);
			this.lblLastError.TabIndex = 25;
			this.lblLastError.Text = "Last Error:";
			// 
			// buttonReloadDestination
			// 
			this.buttonReloadDestination.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonReloadDestination.Location = new System.Drawing.Point(403, 207);
			this.buttonReloadDestination.Name = "buttonReloadDestination";
			this.buttonReloadDestination.Size = new System.Drawing.Size(202, 37);
			this.buttonReloadDestination.TabIndex = 26;
			this.buttonReloadDestination.Text = "Reload Destination Image";
			this.buttonReloadDestination.Click += new System.EventHandler(this.buttonReloadDestination_Click);
			// 
			// IxDest
			// 
			this.IxDest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.IxDest.AutoScroll = true;
			this.IxDest.Location = new System.Drawing.Point(19, 92);
			this.IxDest.Name = "IxDest";
			this.IxDest.Size = new System.Drawing.Size(374, 262);
			this.IxDest.TabIndex = 27;
			this.IxDest.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IxDest_MouseUp);
			this.IxDest.MouseMove += new System.Windows.Forms.MouseEventHandler(this.IxDest_MouseMove);
			this.IxDest.MouseDown += new System.Windows.Forms.MouseEventHandler(this.IxDest_MouseDown);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(624, 545);
			this.Controls.Add(this.IxDest);
			this.Controls.Add(this.buttonReloadDestination);
			this.Controls.Add(this.lblLastError);
			this.Controls.Add(this.lblError);
			this.Controls.Add(this.rtfInfo);
			this.Controls.Add(this.labelPctMin);
			this.Controls.Add(this.labelPctMax);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.hScrollBarMin);
			this.Controls.Add(this.hScrollBarMax);
			this.Controls.Add(this.comboBoxMergeType);
			this.Controls.Add(this.comboBoxTransparentColor);
			this.Controls.Add(this.IxSource);
			this.Controls.Add(this.comboBoxMergeStyle);
			this.Controls.Add(this.checkBoxTransparent);
			this.Controls.Add(this.buttonMerge);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Menu = this.mainMenu1;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ImagXpress .NET Sample - Merge C#";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
			//***Must call the UnlokcRuntime to Distribute your application
			//imagXpress1.License.UnlockRuntime(1234,1234,1234,1234);
            bSelectionActive = false;
			strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString(cultNumber);
			strImageFile1 = System.IO.Path.Combine(strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\Dome.jpg");
			strImageFile2 = System.IO.Path.Combine(strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\ball1.bmp");
			strCurrentDir = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\");
			IxDest.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile1);
			ImageSource = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile2);
			IxSource.Image = ImageSource;
			comboBoxMergeType.SelectedIndex = 0;
			comboBoxMergeStyle.SelectedIndex = 0;

		}
		
		private void hScrollBarMax_ValueChanged(object sender, System.EventArgs e)
		{
			labelPctMax.Text = hScrollBarMax.Value.ToString(cultNumber);
		}

		private void hScrollBarMin_ValueChanged(object sender, System.EventArgs e)
		{
			labelPctMin.Text = hScrollBarMin.Value.ToString(cultNumber);
		}

	

		private void checkBoxTransparent_Click(object sender, System.EventArgs e)
		{
			comboBoxTransparentColor.Enabled = checkBoxTransparent.Checked;
		}

		private void buttonMerge_Click(object sender, System.EventArgs e)
		{
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
		
			PegasusImaging.WinForms.ImagXpress9.Processor process;
			process = new PegasusImaging.WinForms.ImagXpress9.Processor(imagXpress1, IxDest.Image);
			System.Drawing.PointF MergeTL = new System.Drawing.PointF(IxDest.Rubberband.Dimensions.X,IxDest.Rubberband.Dimensions.Y);
			System.Drawing.SizeF MergeSize = new System.Drawing.SizeF(IxDest.Rubberband.Dimensions.Width,IxDest.Rubberband.Dimensions.Height);
			System.Drawing.RectangleF MergeRegion = new System.Drawing.RectangleF(MergeTL,MergeSize);

			process.SetArea(MergeRegion);

			System.Drawing.Color daColor;
			switch (comboBoxTransparentColor.SelectedIndex) //red, green, blue, white
			{
				case 0: 
				{
					daColor = System.Drawing.Color.Red;
				} break;
				case 1: 
				{
					daColor = System.Drawing.Color.FromArgb(0, 255, 0);
				} break;
				case 2: 
				{
					daColor = System.Drawing.Color.Blue;
				} break;
				case 3:
				{
					daColor = System.Drawing.Color.White;
				} break;
				default: 
				{
					daColor = System.Drawing.Color.Black;
				} break;
			}

			process.Merge(ref ImageSource,(PegasusImaging.WinForms.ImagXpress9.MergeSize)comboBoxMergeType.SelectedIndex,(PegasusImaging.WinForms.ImagXpress9.MergeStyle)comboBoxMergeStyle.SelectedIndex,checkBoxTransparent.Checked,daColor,hScrollBarMax.Value,hScrollBarMin.Value);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			
		}


		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion

		private void buttonReloadDestination_Click(object sender, System.EventArgs e)
		{
			IxDest.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile1);
		}

		private void IxDest_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			IxDest.Rubberband.Enabled = false;
			IxDest.Rubberband.Start(new System.Drawing.Point(e.X,e.Y));
			IxDest.Rubberband.Enabled = true;
			bSelectionActive = true;
		}

		private void IxDest_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (bSelectionActive) 
			{
				IxDest.Rubberband.Update(new System.Drawing.Point(e.X,e.Y));
			}
		}

		private void IxDest_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			IxDest.Rubberband.Update(new System.Drawing.Point(e.X,e.Y));
			bSelectionActive = false;
		}

		private void menuToolbarShow_Click(object sender, System.EventArgs e)
		{
			menuToolbarShow.Text = (IxDest.Toolbar.Activated)? "&Show":"&Hide";
			try
			{
				IxDest.Toolbar.Activated = !IxDest.Toolbar.Activated;
			} catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
			{
				PegasusError(eX,lblError);
            }
		}

		private void menuFileOpenSource_Click(object sender, System.EventArgs e)
		{
			System.String strTmp = PegasusOpenFile();
			if (strTmp.Length != 0) 
			{
				strImageFile2 = strTmp;
				try 
				{
					ImageSource = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile2);
					IxSource.Image = ImageSource;
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
				{
					PegasusError(eX,lblError);
				}
				catch (System.IO.IOException eX)
				{
					PegasusError(eX,lblError);
				}
			}
		}

		private void menuFileOpenDest_Click(object sender, System.EventArgs e)
		{
			System.String strTmp = PegasusOpenFile();
			if (strTmp.Length != 0) 
			{
				strImageFile1 = strTmp;
				try 
				{
					IxDest.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile1);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
				{
					PegasusError(eX,lblError);
				}
			}
		}

		private void menuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
			{
				PegasusError(eX,lblError);
			}
		}

		private void comboBoxTransparentColor_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}

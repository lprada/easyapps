﻿namespace ImagXpressViewerDemo
{
    partial class FormConfigThumb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            // It's very important to dispose thumbnailXpress
            if (disposing)
            {
                if (thumbnailXpressSample != null)
                {
                    thumbnailXpressSample.Dispose();
                    thumbnailXpressSample = null;
                }
            }

            
            if (disposing && (components != null))
            {
                 components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.panelBackgroundFrame2 = new AccusoftCustom.PanelBackgroundFrame();
            this.panelGradientNoFlare2 = new AccusoftCustom.PanelGradientNoFlare();
            this.thumbnailXpressSample = new Accusoft.ThumbnailXpressSdk.ThumbnailXpress();
            this.labelSampleHeader = new System.Windows.Forms.Label();
            this.panelBackgroundFrame1 = new AccusoftCustom.PanelBackgroundFrame();
            this.panelGradientNoFlare1 = new AccusoftCustom.PanelGradientNoFlare();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonBackColor = new System.Windows.Forms.Button();
            this.labelBackColor = new System.Windows.Forms.Label();
            this.buttonMore = new System.Windows.Forms.Button();
            this.numericUpDownCellHeight = new System.Windows.Forms.NumericUpDown();
            this.labelCellHeight = new System.Windows.Forms.Label();
            this.numericUpDownCellWidth = new System.Windows.Forms.NumericUpDown();
            this.labelCellWidth = new System.Windows.Forms.Label();
            this.buttonCellSpacingColor = new System.Windows.Forms.Button();
            this.labelCellSpacingColor = new System.Windows.Forms.Label();
            this.buttonBorderColor = new System.Windows.Forms.Button();
            this.labelBorderColor = new System.Windows.Forms.Label();
            this.numericUpDownBorderWidth = new System.Windows.Forms.NumericUpDown();
            this.buttonSelectedCellColor = new System.Windows.Forms.Button();
            this.labelBorderWidth = new System.Windows.Forms.Label();
            this.labelSelectedCellColor = new System.Windows.Forms.Label();
            this.labelSelected = new System.Windows.Forms.Label();
            this.panelBackgroundFrame2.SuspendLayout();
            this.panelGradientNoFlare2.SuspendLayout();
            this.panelBackgroundFrame1.SuspendLayout();
            this.panelGradientNoFlare1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCellHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCellWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBorderWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonOK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Location = new System.Drawing.Point(264, 435);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(114, 30);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(384, 435);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(108, 30);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panelBackgroundFrame2
            // 
            this.panelBackgroundFrame2.Controls.Add(this.panelGradientNoFlare2);
            this.panelBackgroundFrame2.Controls.Add(this.labelSampleHeader);
            this.panelBackgroundFrame2.Location = new System.Drawing.Point(382, 12);
            this.panelBackgroundFrame2.Name = "panelBackgroundFrame2";
            this.panelBackgroundFrame2.Size = new System.Drawing.Size(360, 417);
            this.panelBackgroundFrame2.TabIndex = 35;
            // 
            // panelGradientNoFlare2
            // 
            this.panelGradientNoFlare2.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlare2.Controls.Add(this.thumbnailXpressSample);
            this.panelGradientNoFlare2.Location = new System.Drawing.Point(10, 57);
            this.panelGradientNoFlare2.Name = "panelGradientNoFlare2";
            this.panelGradientNoFlare2.Size = new System.Drawing.Size(340, 350);
            this.panelGradientNoFlare2.TabIndex = 2;
            // 
            // thumbnailXpressSample
            // 
            this.thumbnailXpressSample.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpressSample.BottomMargin = 5;
            this.thumbnailXpressSample.CameraRaw = false;
            this.thumbnailXpressSample.CellBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.thumbnailXpressSample.CellBorderWidth = 1;
            this.thumbnailXpressSample.CellHeight = 80;
            this.thumbnailXpressSample.CellHorizontalSpacing = 5;
            this.thumbnailXpressSample.CellSpacingColor = System.Drawing.Color.White;
            this.thumbnailXpressSample.CellVerticalSpacing = 5;
            this.thumbnailXpressSample.CellWidth = 80;
            this.thumbnailXpressSample.DblClickDirectoryDrillDown = true;
            this.thumbnailXpressSample.DescriptorAlignment = ((Accusoft.ThumbnailXpressSdk.DescriptorAlignments)((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter | Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpressSample.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.Default;
            this.thumbnailXpressSample.EnableAsDragSourceForExternalDragDrop = false;
            this.thumbnailXpressSample.EnableAsDropTargetForExternalDragDrop = false;
            this.thumbnailXpressSample.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon;
            this.thumbnailXpressSample.FtpPassword = "";
            this.thumbnailXpressSample.FtpUserName = "";
            this.thumbnailXpressSample.InterComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpressSample.IntraComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpressSample.LeftMargin = 5;
            this.thumbnailXpressSample.Location = new System.Drawing.Point(16, 16);
            this.thumbnailXpressSample.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpressSample.Name = "thumbnailXpressSample";
            this.thumbnailXpressSample.PreserveBlack = false;
            this.thumbnailXpressSample.ProxyServer = "";
            this.thumbnailXpressSample.RightMargin = 5;
            this.thumbnailXpressSample.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical;
            this.thumbnailXpressSample.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.thumbnailXpressSample.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
            this.thumbnailXpressSample.ShowHourglass = true;
            this.thumbnailXpressSample.ShowImagePlaceholders = false;
            this.thumbnailXpressSample.Size = new System.Drawing.Size(310, 310);
            this.thumbnailXpressSample.TabIndex = 0;
            this.thumbnailXpressSample.TextBackColor = System.Drawing.Color.White;
            this.thumbnailXpressSample.TopMargin = 5;
            this.thumbnailXpressSample.LoadFromStream += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.LoadFromStreamEventHandler(this.thumbnailXpressSample_LoadFromStream);
            // 
            // labelSampleHeader
            // 
            this.labelSampleHeader.AutoSize = true;
            this.labelSampleHeader.BackColor = System.Drawing.Color.Transparent;
            this.labelSampleHeader.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSampleHeader.ForeColor = System.Drawing.Color.White;
            this.labelSampleHeader.Location = new System.Drawing.Point(15, 25);
            this.labelSampleHeader.Name = "labelSampleHeader";
            this.labelSampleHeader.Size = new System.Drawing.Size(171, 19);
            this.labelSampleHeader.TabIndex = 1;
            this.labelSampleHeader.Text = "Sample Thumbnail Control\r\n";
            // 
            // panelBackgroundFrame1
            // 
            this.panelBackgroundFrame1.Controls.Add(this.panelGradientNoFlare1);
            this.panelBackgroundFrame1.Controls.Add(this.labelSelected);
            this.panelBackgroundFrame1.Location = new System.Drawing.Point(12, 12);
            this.panelBackgroundFrame1.Name = "panelBackgroundFrame1";
            this.panelBackgroundFrame1.Size = new System.Drawing.Size(360, 417);
            this.panelBackgroundFrame1.TabIndex = 34;
            // 
            // panelGradientNoFlare1
            // 
            this.panelGradientNoFlare1.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlare1.Controls.Add(this.panel1);
            this.panelGradientNoFlare1.Location = new System.Drawing.Point(10, 57);
            this.panelGradientNoFlare1.Name = "panelGradientNoFlare1";
            this.panelGradientNoFlare1.Size = new System.Drawing.Size(340, 350);
            this.panelGradientNoFlare1.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.buttonBackColor);
            this.panel1.Controls.Add(this.labelBackColor);
            this.panel1.Controls.Add(this.buttonMore);
            this.panel1.Controls.Add(this.numericUpDownCellHeight);
            this.panel1.Controls.Add(this.labelCellHeight);
            this.panel1.Controls.Add(this.numericUpDownCellWidth);
            this.panel1.Controls.Add(this.labelCellWidth);
            this.panel1.Controls.Add(this.buttonCellSpacingColor);
            this.panel1.Controls.Add(this.labelCellSpacingColor);
            this.panel1.Controls.Add(this.buttonBorderColor);
            this.panel1.Controls.Add(this.labelBorderColor);
            this.panel1.Controls.Add(this.numericUpDownBorderWidth);
            this.panel1.Controls.Add(this.buttonSelectedCellColor);
            this.panel1.Controls.Add(this.labelBorderWidth);
            this.panel1.Controls.Add(this.labelSelectedCellColor);
            this.panel1.Location = new System.Drawing.Point(18, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(294, 310);
            this.panel1.TabIndex = 9;
            // 
            // buttonBackColor
            // 
            this.buttonBackColor.BackColor = System.Drawing.Color.Maroon;
            this.buttonBackColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonBackColor.Location = new System.Drawing.Point(132, 144);
            this.buttonBackColor.Name = "buttonBackColor";
            this.buttonBackColor.Size = new System.Drawing.Size(49, 18);
            this.buttonBackColor.TabIndex = 19;
            this.buttonBackColor.UseVisualStyleBackColor = false;
            this.buttonBackColor.Click += new System.EventHandler(this.buttonBackColor_Click);
            // 
            // labelBackColor
            // 
            this.labelBackColor.AutoSize = true;
            this.labelBackColor.Location = new System.Drawing.Point(12, 149);
            this.labelBackColor.Name = "labelBackColor";
            this.labelBackColor.Size = new System.Drawing.Size(59, 13);
            this.labelBackColor.TabIndex = 18;
            this.labelBackColor.Text = "Back Color";
            // 
            // buttonMore
            // 
            this.buttonMore.Location = new System.Drawing.Point(106, 271);
            this.buttonMore.Name = "buttonMore";
            this.buttonMore.Size = new System.Drawing.Size(75, 23);
            this.buttonMore.TabIndex = 17;
            this.buttonMore.Text = "More";
            this.buttonMore.UseVisualStyleBackColor = true;
            this.buttonMore.Click += new System.EventHandler(this.buttonMore_Click);
            // 
            // numericUpDownCellHeight
            // 
            this.numericUpDownCellHeight.Location = new System.Drawing.Point(132, 216);
            this.numericUpDownCellHeight.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDownCellHeight.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDownCellHeight.Name = "numericUpDownCellHeight";
            this.numericUpDownCellHeight.Size = new System.Drawing.Size(66, 20);
            this.numericUpDownCellHeight.TabIndex = 15;
            this.numericUpDownCellHeight.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDownCellHeight.ValueChanged += new System.EventHandler(this.numericUpDownCellHeight_ValueChanged);
            // 
            // labelCellHeight
            // 
            this.labelCellHeight.AutoSize = true;
            this.labelCellHeight.BackColor = System.Drawing.Color.Transparent;
            this.labelCellHeight.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelCellHeight.Location = new System.Drawing.Point(12, 218);
            this.labelCellHeight.Name = "labelCellHeight";
            this.labelCellHeight.Size = new System.Drawing.Size(64, 13);
            this.labelCellHeight.TabIndex = 16;
            this.labelCellHeight.Text = "Cell Height";
            // 
            // numericUpDownCellWidth
            // 
            this.numericUpDownCellWidth.Location = new System.Drawing.Point(132, 181);
            this.numericUpDownCellWidth.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDownCellWidth.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDownCellWidth.Name = "numericUpDownCellWidth";
            this.numericUpDownCellWidth.Size = new System.Drawing.Size(66, 20);
            this.numericUpDownCellWidth.TabIndex = 13;
            this.numericUpDownCellWidth.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.numericUpDownCellWidth.ValueChanged += new System.EventHandler(this.numericUpDownCellWidth_ValueChanged);
            // 
            // labelCellWidth
            // 
            this.labelCellWidth.AutoSize = true;
            this.labelCellWidth.BackColor = System.Drawing.Color.Transparent;
            this.labelCellWidth.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelCellWidth.Location = new System.Drawing.Point(12, 183);
            this.labelCellWidth.Name = "labelCellWidth";
            this.labelCellWidth.Size = new System.Drawing.Size(61, 13);
            this.labelCellWidth.TabIndex = 14;
            this.labelCellWidth.Text = "Cell Width";
            // 
            // buttonCellSpacingColor
            // 
            this.buttonCellSpacingColor.BackColor = System.Drawing.Color.Maroon;
            this.buttonCellSpacingColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCellSpacingColor.Location = new System.Drawing.Point(132, 107);
            this.buttonCellSpacingColor.Name = "buttonCellSpacingColor";
            this.buttonCellSpacingColor.Size = new System.Drawing.Size(49, 18);
            this.buttonCellSpacingColor.TabIndex = 12;
            this.buttonCellSpacingColor.UseVisualStyleBackColor = false;
            this.buttonCellSpacingColor.Click += new System.EventHandler(this.buttonCellSpacingColor_Click);
            // 
            // labelCellSpacingColor
            // 
            this.labelCellSpacingColor.AutoSize = true;
            this.labelCellSpacingColor.BackColor = System.Drawing.Color.Transparent;
            this.labelCellSpacingColor.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelCellSpacingColor.Location = new System.Drawing.Point(12, 112);
            this.labelCellSpacingColor.Name = "labelCellSpacingColor";
            this.labelCellSpacingColor.Size = new System.Drawing.Size(101, 13);
            this.labelCellSpacingColor.TabIndex = 11;
            this.labelCellSpacingColor.Text = "Cell Spacing Color";
            // 
            // buttonBorderColor
            // 
            this.buttonBorderColor.BackColor = System.Drawing.Color.Maroon;
            this.buttonBorderColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonBorderColor.Location = new System.Drawing.Point(132, 40);
            this.buttonBorderColor.Name = "buttonBorderColor";
            this.buttonBorderColor.Size = new System.Drawing.Size(49, 18);
            this.buttonBorderColor.TabIndex = 10;
            this.buttonBorderColor.UseVisualStyleBackColor = false;
            this.buttonBorderColor.Click += new System.EventHandler(this.buttonBorderColor_Click);
            // 
            // labelBorderColor
            // 
            this.labelBorderColor.AutoSize = true;
            this.labelBorderColor.BackColor = System.Drawing.Color.Transparent;
            this.labelBorderColor.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelBorderColor.Location = new System.Drawing.Point(12, 45);
            this.labelBorderColor.Name = "labelBorderColor";
            this.labelBorderColor.Size = new System.Drawing.Size(73, 13);
            this.labelBorderColor.TabIndex = 9;
            this.labelBorderColor.Text = "Border Color";
            // 
            // numericUpDownBorderWidth
            // 
            this.numericUpDownBorderWidth.Location = new System.Drawing.Point(132, 14);
            this.numericUpDownBorderWidth.Name = "numericUpDownBorderWidth";
            this.numericUpDownBorderWidth.Size = new System.Drawing.Size(66, 20);
            this.numericUpDownBorderWidth.TabIndex = 5;
            this.numericUpDownBorderWidth.ValueChanged += new System.EventHandler(this.numericUpDownBorderWidth_ValueChanged);
            // 
            // buttonSelectedCellColor
            // 
            this.buttonSelectedCellColor.BackColor = System.Drawing.Color.Maroon;
            this.buttonSelectedCellColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSelectedCellColor.Location = new System.Drawing.Point(132, 73);
            this.buttonSelectedCellColor.Name = "buttonSelectedCellColor";
            this.buttonSelectedCellColor.Size = new System.Drawing.Size(49, 18);
            this.buttonSelectedCellColor.TabIndex = 8;
            this.buttonSelectedCellColor.UseVisualStyleBackColor = false;
            this.buttonSelectedCellColor.Click += new System.EventHandler(this.buttonSelectedCellColor_Click);
            // 
            // labelBorderWidth
            // 
            this.labelBorderWidth.AutoSize = true;
            this.labelBorderWidth.BackColor = System.Drawing.Color.Transparent;
            this.labelBorderWidth.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelBorderWidth.Location = new System.Drawing.Point(12, 16);
            this.labelBorderWidth.Name = "labelBorderWidth";
            this.labelBorderWidth.Size = new System.Drawing.Size(77, 13);
            this.labelBorderWidth.TabIndex = 6;
            this.labelBorderWidth.Text = "Border Width";
            // 
            // labelSelectedCellColor
            // 
            this.labelSelectedCellColor.AutoSize = true;
            this.labelSelectedCellColor.BackColor = System.Drawing.Color.Transparent;
            this.labelSelectedCellColor.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelSelectedCellColor.Location = new System.Drawing.Point(12, 78);
            this.labelSelectedCellColor.Name = "labelSelectedCellColor";
            this.labelSelectedCellColor.Size = new System.Drawing.Size(103, 13);
            this.labelSelectedCellColor.TabIndex = 7;
            this.labelSelectedCellColor.Text = "Selected Cell Color";
            // 
            // labelSelected
            // 
            this.labelSelected.AutoSize = true;
            this.labelSelected.BackColor = System.Drawing.Color.Transparent;
            this.labelSelected.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelected.ForeColor = System.Drawing.Color.White;
            this.labelSelected.Location = new System.Drawing.Point(15, 15);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(177, 57);
            this.labelSelected.TabIndex = 10;
            this.labelSelected.Text = "Selected Thumbnail Control\r\nproperties and methods\r\n\r\n";
            // 
            // FormConfigThumb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(54)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(754, 474);
            this.Controls.Add(this.panelBackgroundFrame2);
            this.Controls.Add(this.panelBackgroundFrame1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConfigThumb";
            this.Text = "Configure Thumbnails";
            this.Load += new System.EventHandler(this.FormConfigThumb_Load);
            this.panelBackgroundFrame2.ResumeLayout(false);
            this.panelBackgroundFrame2.PerformLayout();
            this.panelGradientNoFlare2.ResumeLayout(false);
            this.panelBackgroundFrame1.ResumeLayout(false);
            this.panelBackgroundFrame1.PerformLayout();
            this.panelGradientNoFlare1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCellHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCellWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBorderWidth)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Accusoft.ThumbnailXpressSdk.ThumbnailXpress thumbnailXpressSample;
        private System.Windows.Forms.Label labelSampleHeader;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.NumericUpDown numericUpDownBorderWidth;
        private System.Windows.Forms.Label labelBorderWidth;
        private System.Windows.Forms.Label labelSelectedCellColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button buttonSelectedCellColor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonBorderColor;
        private System.Windows.Forms.Label labelBorderColor;
        private System.Windows.Forms.Button buttonCellSpacingColor;
        private System.Windows.Forms.Label labelCellSpacingColor;
        private System.Windows.Forms.NumericUpDown numericUpDownCellHeight;
        private System.Windows.Forms.Label labelCellHeight;
        private System.Windows.Forms.NumericUpDown numericUpDownCellWidth;
        private System.Windows.Forms.Label labelCellWidth;
        private System.Windows.Forms.Button buttonMore;
        private System.Windows.Forms.Label labelSelected;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrame1;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrame2;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlare1;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlare2;
        private System.Windows.Forms.Button buttonBackColor;
        private System.Windows.Forms.Label labelBackColor;
    }
}
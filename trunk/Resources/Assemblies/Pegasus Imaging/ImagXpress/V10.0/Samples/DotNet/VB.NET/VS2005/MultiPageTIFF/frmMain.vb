'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.Runtime.InteropServices

Namespace MultipageTIFF


    ' <summary>
    ' Summary description for Form1.
    ' </summary>
    Public Class frmMain

        Inherits System.Windows.Forms.Form
        ' DllImport
        <StructLayout(LayoutKind.Sequential)> _
        Public Structure RECT

            Public left As Integer

            Public top As Integer

            Public right As Integer

            Public bottom As Integer
        End Structure
        Public Class Win32
            Public Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As System.Int32, ByVal lpString As String) As Long
            Public Declare Function MoveWindow Lib "User32.Dll" (ByVal hWnd As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal bRepaint As Boolean) As Boolean
            Public Declare Function GetWindowRect Lib "User32.Dll" (ByVal hWnd As Integer, ByRef rc As RECT) As Boolean
        End Class

        Private strImageFile1 As String
        Private strImageFile2 As String
        Private strImageFile3 As String
        Private strMPFile As String
        Private strTmpFile As String


#Region " Windows Form Designer generated code "
        ' <summary>
        ' Required designer variable.
        ' </summary>
        Private WithEvents cmdMake As System.Windows.Forms.Button
        Private WithEvents cmdRemove As System.Windows.Forms.Button
        Private WithEvents cmdInsert As System.Windows.Forms.Button
        Private WithEvents cmdCompact As System.Windows.Forms.Button
        Private mnuFile As System.Windows.Forms.MainMenu
        Private WithEvents mnuFileFile As System.Windows.Forms.MenuItem
        Private WithEvents mnuFileQuit As System.Windows.Forms.MenuItem
        Private imagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
        Private WithEvents mnuAbout As System.Windows.Forms.MenuItem
        Private components As System.ComponentModel.IContainer
        Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
        Friend WithEvents ImageXView2 As Accusoft.ImagXpressSdk.ImageXView
        Friend WithEvents ImageXView3 As Accusoft.ImagXpressSdk.ImageXView
        Friend WithEvents lstInfo As System.Windows.Forms.ListBox
        Friend WithEvents lblError As System.Windows.Forms.Label
        Friend WithEvents lblLastError As System.Windows.Forms.Label


        Public Sub New()
            MyBase.New()
            '
            ' Required for Windows Form Designer support
            InitializeComponent()
            '
            ' TODO: Add any constructor code after InitializeComponent call
            '
        End Sub


        ' <summary>
        ' Clean up any resources being used.
        ' </summary>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If (Not (components) Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.cmdMake = New System.Windows.Forms.Button
            Me.cmdRemove = New System.Windows.Forms.Button
            Me.cmdInsert = New System.Windows.Forms.Button
            Me.cmdCompact = New System.Windows.Forms.Button
            Me.mnuFile = New System.Windows.Forms.MainMenu(Me.components)
            Me.mnuFileFile = New System.Windows.Forms.MenuItem
            Me.mnuFileQuit = New System.Windows.Forms.MenuItem
            Me.mnuAbout = New System.Windows.Forms.MenuItem
            Me.imagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
            Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
            Me.ImageXView2 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
            Me.ImageXView3 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
            Me.lstInfo = New System.Windows.Forms.ListBox
            Me.lblError = New System.Windows.Forms.Label
            Me.lblLastError = New System.Windows.Forms.Label
            Me.lblFileStats = New System.Windows.Forms.Label
            Me.lblMore = New System.Windows.Forms.Label
            Me.SuspendLayout()
            '
            'cmdMake
            '
            Me.cmdMake.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdMake.Location = New System.Drawing.Point(506, 138)
            Me.cmdMake.Name = "cmdMake"
            Me.cmdMake.Size = New System.Drawing.Size(177, 24)
            Me.cmdMake.TabIndex = 4
            Me.cmdMake.Text = "Make MP.TIF"
            '
            'cmdRemove
            '
            Me.cmdRemove.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdRemove.Location = New System.Drawing.Point(506, 170)
            Me.cmdRemove.Name = "cmdRemove"
            Me.cmdRemove.Size = New System.Drawing.Size(177, 24)
            Me.cmdRemove.TabIndex = 5
            Me.cmdRemove.Text = "Remove Page 2"
            '
            'cmdInsert
            '
            Me.cmdInsert.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdInsert.Location = New System.Drawing.Point(506, 200)
            Me.cmdInsert.Name = "cmdInsert"
            Me.cmdInsert.Size = New System.Drawing.Size(177, 24)
            Me.cmdInsert.TabIndex = 6
            Me.cmdInsert.Text = "Insert Page 2"
            '
            'cmdCompact
            '
            Me.cmdCompact.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdCompact.Location = New System.Drawing.Point(506, 232)
            Me.cmdCompact.Name = "cmdCompact"
            Me.cmdCompact.Size = New System.Drawing.Size(177, 24)
            Me.cmdCompact.TabIndex = 7
            Me.cmdCompact.Text = "Compact"
            '
            'mnuFile
            '
            Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileFile, Me.mnuAbout})
            '
            'mnuFileFile
            '
            Me.mnuFileFile.Index = 0
            Me.mnuFileFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileQuit})
            Me.mnuFileFile.Text = "&File"
            '
            'mnuFileQuit
            '
            Me.mnuFileQuit.Index = 0
            Me.mnuFileQuit.Text = "&Quit"
            '
            'mnuAbout
            '
            Me.mnuAbout.Index = 1
            Me.mnuAbout.Text = "&About"
            '
            'ImageXView1
            '
            Me.ImageXView1.Location = New System.Drawing.Point(9, 138)
            Me.ImageXView1.Name = "ImageXView1"
            Me.ImageXView1.Size = New System.Drawing.Size(160, 168)
            Me.ImageXView1.TabIndex = 16
            '
            'ImageXView2
            '
            Me.ImageXView2.Location = New System.Drawing.Point(177, 138)
            Me.ImageXView2.Name = "ImageXView2"
            Me.ImageXView2.Size = New System.Drawing.Size(160, 168)
            Me.ImageXView2.TabIndex = 17
            '
            'ImageXView3
            '
            Me.ImageXView3.Location = New System.Drawing.Point(345, 138)
            Me.ImageXView3.Name = "ImageXView3"
            Me.ImageXView3.Size = New System.Drawing.Size(144, 168)
            Me.ImageXView3.TabIndex = 18
            '
            'lstInfo
            '
            Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates operations that ImagXpress can perform on Multiple page " & _
                            "Tiff files.", "1)Creating  multiple page Tiff files.", "2)Displaying individual pages.", "3)Obtaining the number of Pages.", "4)Deleting a page.", "5)Inserting a page.", "6)Compacting unused pages from a file."})
            Me.lstInfo.Location = New System.Drawing.Point(8, 16)
            Me.lstInfo.Name = "lstInfo"
            Me.lstInfo.Size = New System.Drawing.Size(675, 108)
            Me.lstInfo.TabIndex = 19
            '
            'lblError
            '
            Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblError.Location = New System.Drawing.Point(75, 354)
            Me.lblError.Name = "lblError"
            Me.lblError.Size = New System.Drawing.Size(608, 54)
            Me.lblError.TabIndex = 34
            '
            'lblLastError
            '
            Me.lblLastError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblLastError.Location = New System.Drawing.Point(12, 365)
            Me.lblLastError.Name = "lblLastError"
            Me.lblLastError.Size = New System.Drawing.Size(71, 34)
            Me.lblLastError.TabIndex = 33
            Me.lblLastError.Text = "Last Error:"
            '
            'lblFileStats
            '
            Me.lblFileStats.Location = New System.Drawing.Point(10, 315)
            Me.lblFileStats.Name = "lblFileStats"
            Me.lblFileStats.Size = New System.Drawing.Size(479, 39)
            Me.lblFileStats.TabIndex = 35
            '
            'lblMore
            '
            Me.lblMore.Location = New System.Drawing.Point(506, 315)
            Me.lblMore.Name = "lblMore"
            Me.lblMore.Size = New System.Drawing.Size(177, 39)
            Me.lblMore.TabIndex = 36
            '
            'frmMain
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(710, 408)
            Me.Controls.Add(Me.lblMore)
            Me.Controls.Add(Me.lblFileStats)
            Me.Controls.Add(Me.lblError)
            Me.Controls.Add(Me.lblLastError)
            Me.Controls.Add(Me.lstInfo)
            Me.Controls.Add(Me.ImageXView3)
            Me.Controls.Add(Me.ImageXView2)
            Me.Controls.Add(Me.ImageXView1)
            Me.Controls.Add(Me.cmdCompact)
            Me.Controls.Add(Me.cmdInsert)
            Me.Controls.Add(Me.cmdRemove)
            Me.Controls.Add(Me.cmdMake)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Menu = Me.mnuFile
            Me.Name = "frmMain"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "MultiPage TIFF"
            Me.ResumeLayout(False)

        End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
        '/*********************************************************************
        '*     Pegasus Imaging Corporation Standard Function Definitions     *
        ' *********************************************************************/

        Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
        Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
        Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
        Friend WithEvents lblFileStats As System.Windows.Forms.Label
        Friend WithEvents lblMore As System.Windows.Forms.Label
        Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
        ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
        "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
        "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
        "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
        ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
        "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
        "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
        "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


        Private Function GetFileName(ByVal FullName As String) As String

            Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
        End Function

        Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                        + (ErrorException.Source + "" & vbLf)))
        End Sub

        Private Overloads Shared Sub PegasusError(ByVal ErrorException As Accusoft.ImagXpressSdk.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                        + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
        End Sub

        Private Overloads Function PegasusOpenFile() As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strDefaultImageFilter
            If (dlg.ShowDialog = DialogResult.OK) Then
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

        Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strFilter
            If (dlg.ShowDialog = DialogResult.OK) Then
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

#End Region

        Private Sub ReloadViews()
            Dim pageCount As Integer

            Try
                ' Dispose any current images
                If (ImageXView1.Image IsNot Nothing) Then
                    ImageXView1.Image.Dispose()
                    ImageXView1.Image = Nothing
                End If
                If (ImageXView2.Image IsNot Nothing) Then
                    ImageXView2.Image.Dispose()
                    ImageXView3.Image = Nothing
                End If
                If (ImageXView3.Image IsNot Nothing) Then
                    ImageXView3.Image.Dispose()
                    ImageXView3.Image = Nothing
                End If

                ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strMPFile, 1)
                pageCount = ImageXView1.Image.PageCount

                If (pageCount > 1) Then
                    ImageXView2.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strMPFile, 2)
                End If

                If (pageCount > 2) Then
                    ImageXView3.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strMPFile, 3)
                End If

                lblFileStats.Text = ("File Size: " _
                            + (ImageXView1.Image.ImageXData.Size.ToString(cultNumber) + (" bytes." + Microsoft.VisualBasic.ChrW(10) + ("Pages: " + pageCount.ToString()))))

                If (pageCount > 3) Then
                    lblMore.Text = "More..."
                Else
                    lblMore.Text = ""
                End If

            Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(ex, lblError)
            End Try

        End Sub

        Private Sub mnuFileQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileQuit.Click
            Application.Exit()
        End Sub

        Private Sub cmdMake_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMake.Click
            Dim imgTmp As Accusoft.ImagXpressSdk.ImageX
            If System.IO.File.Exists(strMPFile) Then
                System.IO.File.Delete(strMPFile)
            End If
            Try
                imgTmp = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress1, strImageFile1)
                Dim soOpts As Accusoft.ImagXpressSdk.SaveOptions = New Accusoft.ImagXpressSdk.SaveOptions()
                Try
                    soOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff
                    soOpts.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.NoCompression
                    soOpts.Tiff.MultiPage = True
                    imgTmp.Save(strMPFile, soOpts)
                    imgTmp.Dispose()
                    imgTmp = Nothing
                Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                    PegasusError(eX, lblError)
                End Try

                Try
                    'clear out the error in case there was an error from a previous operation
                    lblError.Text = ""
                    Accusoft.ImagXpressSdk.ImageX.InsertPage(imagXpress1, strImageFile2, strMPFile, 2)
                Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                    PegasusError(eX, lblError)
                End Try

                Try
                    'clear out the error in case there was an error from a previous operation
                    lblError.Text = ""
                    Accusoft.ImagXpressSdk.ImageX.InsertPage(imagXpress1, strImageFile3, strMPFile, 3)
                Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                    PegasusError(eX, lblError)
                End Try

            Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(ex, lblError)
                Return
            End Try

            ReloadViews()
        End Sub

        Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim strCurrentDir As String

            '***Must call the UnlockRuntime method to unlock the control
            'imagxpress1.Licensing.UnlockRuntime(1234,1234,1234,1234)

            ' This sample uses ImagXpress Professional edition features,
            ' make sure that ImagXpress is running in Professional edition.
            imagXpress1.Licensing.LicenseEditionChoice = Accusoft.ImagXpressSdk.LicenseEditionType.ImagXpressProfessional

            'Because in this sample the ImageX object is always located in the ImageXView
            'tell the ImageXView component to dispose of the ImageX object when itself is
            'disposed. Otherwise, we would have to dispose of the ImageX object in the Form's
            'Dispose method before the components.Dispose() section.
            ImageXView1.AutoImageDispose = True
            ImageXView2.AutoImageDispose = True
            ImageXView3.AutoImageDispose = True

            'Set the current directory to the Common\Images directory
            strCurrentDir = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\..\Common\Images\")
            If (System.IO.Directory.Exists(strCurrentDir)) Then
                System.IO.Directory.SetCurrentDirectory(strCurrentDir)
            End If
            strCurrentDir = System.IO.Directory.GetCurrentDirectory()

            strImageFile1 = "page1.tif"
            strImageFile2 = "page2.tif"
            strImageFile3 = "page3.tif"
            strMPFile = Application.StartupPath + "\mp.tif"
            strTmpFile = Application.StartupPath + "\compact.tif"
        End Sub

        Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
            Try
                'clear out the error in case there was an error from a previous operation
                lblError.Text = ""
                Accusoft.ImagXpressSdk.ImageX.DeletePage(imagXpress1, strMPFile, 2)
            Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(ex, lblError)
            End Try
            ReloadViews()
        End Sub

        Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
            Try
                imagXpress1.AboutBox()
            Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(eX, lblError)
            End Try
        End Sub

        Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim hwndTB As System.IntPtr
            Dim controlRect As RECT = New RECT()
            Dim toolbarRect As RECT = New RECT()

            Try
                ImageXView1.Toolbar.Activated = Not ImageXView1.Toolbar.Activated
            Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            hwndTB = ImageXView1.Toolbar.Hwnd
            Win32.GetWindowRect(ImageXView1.Handle.ToInt32, controlRect)
            Win32.GetWindowRect(hwndTB.ToInt32, toolbarRect)
            Win32.SetWindowText(hwndTB.ToInt32, "Toolbar 1")
            Win32.MoveWindow(hwndTB.ToInt32, (controlRect.left + 8), (controlRect.bottom + 12), (toolbarRect.right - toolbarRect.left), (toolbarRect.bottom - toolbarRect.top), True)
            Try
                ImageXView2.Toolbar.Activated = Not ImageXView2.Toolbar.Activated
            Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            hwndTB = ImageXView2.Toolbar.Hwnd
            Win32.GetWindowRect(ImageXView2.Handle.ToInt32, controlRect)
            Win32.GetWindowRect(hwndTB.ToInt32, toolbarRect)
            Win32.SetWindowText(hwndTB.ToInt32, "Toolbar 2")
            Win32.MoveWindow(hwndTB.ToInt32, (controlRect.left + 8), (controlRect.bottom + 12), (toolbarRect.right - toolbarRect.left), (toolbarRect.bottom - toolbarRect.top), True)
            Try
                ImageXView3.Toolbar.Activated = Not ImageXView3.Toolbar.Activated
            Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            hwndTB = ImageXView3.Toolbar.Hwnd
            Win32.GetWindowRect(ImageXView3.Handle.ToInt32, controlRect)
            Win32.GetWindowRect(hwndTB.ToInt32, toolbarRect)
            Win32.SetWindowText(hwndTB.ToInt32, "Toolbar 3")
            Win32.MoveWindow(hwndTB.ToInt32, (controlRect.left + 8), (controlRect.bottom + 12), (toolbarRect.right - toolbarRect.left), (toolbarRect.bottom - toolbarRect.top), True)
        End Sub

        Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click
            Try
                'clear out the error in case there was an error from a previous operation
                lblError.Text = ""

                Accusoft.ImagXpressSdk.ImageX.InsertPage(imagXpress1, strImageFile2, strMPFile, 2)
            Catch eX As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            ReloadViews()
        End Sub

        Private Sub cmdCompact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCompact.Click
            Try
                'clear out the error in case there was an error from a previous operation
                lblError.Text = ""

                Accusoft.ImagXpressSdk.ImageX.CompactFile(imagXpress1, strMPFile, strTmpFile)
            Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
                PegasusError(ex, lblError)

            End Try
            If System.IO.File.Exists(strTmpFile) Then
                System.IO.File.Delete(strMPFile)
                System.IO.File.Move(strTmpFile, strMPFile)
            End If
            ReloadViews()
        End Sub


        Private Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
            Dim iTmp As Integer
            Try
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
            Catch ex As System.NullReferenceException
                PegasusError(ex, lblError)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            Catch ex As System.Exception
                PegasusError(ex, lblError)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            End Try
            If ((iTmp < scrScroll.Maximum) _
                        AndAlso (iTmp > scrScroll.Minimum)) Then
                scrScroll.Value = iTmp
            Else
                iTmp = scrScroll.Value
            End If
            textTextBox.Text = iTmp.ToString(cultNumber)
        End Sub

    End Class
End Namespace
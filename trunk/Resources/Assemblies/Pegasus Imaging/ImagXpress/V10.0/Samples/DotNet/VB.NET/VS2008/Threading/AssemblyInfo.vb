'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System

<Assembly: AssemblyTitle("Threading Sample")> 
<Assembly: AssemblyDescription("VB.NET ImagXpress 10 Threading Sample")> 
<assembly: AssemblyConfiguration("")> 
<Assembly: AssemblyCompany("Accusoft Pegasus")> 
<Assembly: AssemblyProduct("ImagXpress 10")> 
<Assembly: AssemblyCopyright("Copyright � 2008-2009 Pegasus Imaging Corporation")> 
<assembly: AssemblyTrademark("")> 
<assembly: AssemblyCulture("")> 
<assembly: System.Runtime.InteropServices.ComVisible(False)> 
<assembly: CLSCompliant(True)> 
<assembly: AssemblyVersion("1.0.0.0")> 
<assembly: AssemblyDelaySign(False)> 
<assembly: AssemblyKeyFile("")> 
<assembly: AssemblyKeyName("")> 


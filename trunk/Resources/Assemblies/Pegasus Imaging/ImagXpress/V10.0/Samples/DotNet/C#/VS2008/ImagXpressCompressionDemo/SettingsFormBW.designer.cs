﻿namespace ImagXpressCompressionDemo
{
	partial class SettingsFormBW
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panelBackgroundFrameBW = new AccusoftCustom.PanelBackgroundFrame();
            this.groupBoxCompressedImage = new System.Windows.Forms.GroupBox();
            this.radioButtonCompressedImage4 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage3 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage2 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage1 = new System.Windows.Forms.RadioButton();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.tabPageTiff = new System.Windows.Forms.TabPage();
            this.groupTiffCompression = new System.Windows.Forms.GroupBox();
            this.radioButtonTiffNoCompression = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffPackBits = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffLzw = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffRle = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffJbig2 = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffGroup4 = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffGroup31D = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffGroup32D = new System.Windows.Forms.RadioButton();
            this.radioButtonTiffDeflate = new System.Windows.Forms.RadioButton();
            this.tabPagePdf = new System.Windows.Forms.TabPage();
            this.groupBoxPdfCompression = new System.Windows.Forms.GroupBox();
            this.radioButtonPdfNoCompression = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfPackBits = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfLzw = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfRle = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfJbig2 = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfGroup4 = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfGroup31D = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfGroup32D = new System.Windows.Forms.RadioButton();
            this.radioButtonPdfDeflate = new System.Windows.Forms.RadioButton();
            this.checkBoxPdfSwapBlackAndWhite = new System.Windows.Forms.CheckBox();
            this.tabPageJbig2 = new System.Windows.Forms.TabPage();
            this.labelLooseness = new System.Windows.Forms.Label();
            this.checkBoxInvertedRegion = new System.Windows.Forms.CheckBox();
            this.numericUpDownLooseness = new System.Windows.Forms.NumericUpDown();
            this.groupBoxFileOrganization = new System.Windows.Forms.GroupBox();
            this.radioButtonRandomAccess = new System.Windows.Forms.RadioButton();
            this.radioButtonDefault = new System.Windows.Forms.RadioButton();
            this.radioButtonSequential = new System.Windows.Forms.RadioButton();
            this.groupBoxEncodeMode = new System.Windows.Forms.GroupBox();
            this.radioButtonLossyHalftoneMmr = new System.Windows.Forms.RadioButton();
            this.radioButtonLossyHalftoneMq = new System.Windows.Forms.RadioButton();
            this.radioButtonLossyTextMmr = new System.Windows.Forms.RadioButton();
            this.radioButtonLossyTextMq = new System.Windows.Forms.RadioButton();
            this.radioButtonLosslessTextSpmMmr = new System.Windows.Forms.RadioButton();
            this.radioButtonLosslessTextSpmMq = new System.Windows.Forms.RadioButton();
            this.radioButtonLosslessTextMmr = new System.Windows.Forms.RadioButton();
            this.radioButtonLosslessTextMq = new System.Windows.Forms.RadioButton();
            this.radioButtonLosslessGenericMmr = new System.Windows.Forms.RadioButton();
            this.radioButtonLosslessGenericMq = new System.Windows.Forms.RadioButton();
            this.radioButtonAutoDetect = new System.Windows.Forms.RadioButton();
            this.tabPageModca = new System.Windows.Forms.TabPage();
            this.labelModcaNoSettings = new System.Windows.Forms.Label();
            this.tabPageCals = new System.Windows.Forms.TabPage();
            this.labelCalsNoSettings = new System.Windows.Forms.Label();
            this.groupBoxImageType = new System.Windows.Forms.GroupBox();
            this.radioButtonCals = new System.Windows.Forms.RadioButton();
            this.radioButtonModca = new System.Windows.Forms.RadioButton();
            this.radioButtonJbig2 = new System.Windows.Forms.RadioButton();
            this.radioButtonPdf = new System.Windows.Forms.RadioButton();
            this.radioButtonTiff = new System.Windows.Forms.RadioButton();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.checkBoxPreview = new System.Windows.Forms.CheckBox();
            this.buttonRestoreDefaults = new System.Windows.Forms.Button();
            this.panelBackgroundFrameBW.SuspendLayout();
            this.groupBoxCompressedImage.SuspendLayout();
            this.tabControlSettings.SuspendLayout();
            this.tabPageTiff.SuspendLayout();
            this.groupTiffCompression.SuspendLayout();
            this.tabPagePdf.SuspendLayout();
            this.groupBoxPdfCompression.SuspendLayout();
            this.tabPageJbig2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLooseness)).BeginInit();
            this.groupBoxFileOrganization.SuspendLayout();
            this.groupBoxEncodeMode.SuspendLayout();
            this.tabPageModca.SuspendLayout();
            this.tabPageCals.SuspendLayout();
            this.groupBoxImageType.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(180, 335);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(90, 32);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(283, 335);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(90, 32);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // panelBackgroundFrameBW
            // 
            this.panelBackgroundFrameBW.BackColor = System.Drawing.Color.Transparent;
            this.panelBackgroundFrameBW.Controls.Add(this.groupBoxCompressedImage);
            this.panelBackgroundFrameBW.Controls.Add(this.tabControlSettings);
            this.panelBackgroundFrameBW.Controls.Add(this.groupBoxImageType);
            this.panelBackgroundFrameBW.Controls.Add(this.groupBoxSettings);
            this.panelBackgroundFrameBW.Location = new System.Drawing.Point(12, 12);
            this.panelBackgroundFrameBW.Name = "panelBackgroundFrameBW";
            this.panelBackgroundFrameBW.Size = new System.Drawing.Size(525, 323);
            this.panelBackgroundFrameBW.TabIndex = 7;
            // 
            // groupBoxCompressedImage
            // 
            this.groupBoxCompressedImage.BackColor = System.Drawing.Color.White;
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage4);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage3);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage2);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage1);
            this.groupBoxCompressedImage.Location = new System.Drawing.Point(20, 25);
            this.groupBoxCompressedImage.Name = "groupBoxCompressedImage";
            this.groupBoxCompressedImage.Size = new System.Drawing.Size(134, 40);
            this.groupBoxCompressedImage.TabIndex = 6;
            this.groupBoxCompressedImage.TabStop = false;
            this.groupBoxCompressedImage.Text = "Compressed Image";
            // 
            // radioButtonCompressedImage4
            // 
            this.radioButtonCompressedImage4.AutoSize = true;
            this.radioButtonCompressedImage4.Location = new System.Drawing.Point(99, 17);
            this.radioButtonCompressedImage4.Name = "radioButtonCompressedImage4";
            this.radioButtonCompressedImage4.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage4.TabIndex = 3;
            this.radioButtonCompressedImage4.TabStop = true;
            this.radioButtonCompressedImage4.Text = "4";
            this.radioButtonCompressedImage4.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage4.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage4_CheckedChanged);
            // 
            // radioButtonCompressedImage3
            // 
            this.radioButtonCompressedImage3.AutoSize = true;
            this.radioButtonCompressedImage3.Location = new System.Drawing.Point(68, 17);
            this.radioButtonCompressedImage3.Name = "radioButtonCompressedImage3";
            this.radioButtonCompressedImage3.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage3.TabIndex = 2;
            this.radioButtonCompressedImage3.TabStop = true;
            this.radioButtonCompressedImage3.Text = "3";
            this.radioButtonCompressedImage3.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage3.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage3_CheckedChanged);
            // 
            // radioButtonCompressedImage2
            // 
            this.radioButtonCompressedImage2.AutoSize = true;
            this.radioButtonCompressedImage2.Location = new System.Drawing.Point(36, 17);
            this.radioButtonCompressedImage2.Name = "radioButtonCompressedImage2";
            this.radioButtonCompressedImage2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage2.TabIndex = 1;
            this.radioButtonCompressedImage2.TabStop = true;
            this.radioButtonCompressedImage2.Text = "2";
            this.radioButtonCompressedImage2.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage2.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage2_CheckedChanged);
            // 
            // radioButtonCompressedImage1
            // 
            this.radioButtonCompressedImage1.AutoSize = true;
            this.radioButtonCompressedImage1.Location = new System.Drawing.Point(6, 17);
            this.radioButtonCompressedImage1.Name = "radioButtonCompressedImage1";
            this.radioButtonCompressedImage1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage1.TabIndex = 0;
            this.radioButtonCompressedImage1.TabStop = true;
            this.radioButtonCompressedImage1.Text = "1";
            this.radioButtonCompressedImage1.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage1.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage1_CheckedChanged);
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Controls.Add(this.tabPageTiff);
            this.tabControlSettings.Controls.Add(this.tabPagePdf);
            this.tabControlSettings.Controls.Add(this.tabPageJbig2);
            this.tabControlSettings.Controls.Add(this.tabPageModca);
            this.tabControlSettings.Controls.Add(this.tabPageCals);
            this.tabControlSettings.Location = new System.Drawing.Point(156, 25);
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(352, 275);
            this.tabControlSettings.TabIndex = 0;
            this.tabControlSettings.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageTiff
            // 
            this.tabPageTiff.BackColor = System.Drawing.Color.White;
            this.tabPageTiff.Controls.Add(this.groupTiffCompression);
            this.tabPageTiff.Location = new System.Drawing.Point(4, 22);
            this.tabPageTiff.Name = "tabPageTiff";
            this.tabPageTiff.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTiff.Size = new System.Drawing.Size(344, 249);
            this.tabPageTiff.TabIndex = 2;
            this.tabPageTiff.Text = "TIFF";
            this.tabPageTiff.UseVisualStyleBackColor = true;
            // 
            // groupTiffCompression
            // 
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffNoCompression);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffPackBits);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffLzw);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffRle);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffJbig2);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffGroup4);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffGroup31D);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffGroup32D);
            this.groupTiffCompression.Controls.Add(this.radioButtonTiffDeflate);
            this.groupTiffCompression.Location = new System.Drawing.Point(54, 30);
            this.groupTiffCompression.Name = "groupTiffCompression";
            this.groupTiffCompression.Size = new System.Drawing.Size(237, 181);
            this.groupTiffCompression.TabIndex = 0;
            this.groupTiffCompression.TabStop = false;
            this.groupTiffCompression.Text = "Compression";
            // 
            // radioButtonTiffNoCompression
            // 
            this.radioButtonTiffNoCompression.AutoSize = true;
            this.radioButtonTiffNoCompression.Location = new System.Drawing.Point(23, 29);
            this.radioButtonTiffNoCompression.Name = "radioButtonTiffNoCompression";
            this.radioButtonTiffNoCompression.Size = new System.Drawing.Size(102, 17);
            this.radioButtonTiffNoCompression.TabIndex = 11;
            this.radioButtonTiffNoCompression.TabStop = true;
            this.radioButtonTiffNoCompression.Text = "No Compression";
            this.radioButtonTiffNoCompression.UseVisualStyleBackColor = true;
            this.radioButtonTiffNoCompression.CheckedChanged += new System.EventHandler(this.radioButtonTiffNoCompression_CheckedChanged);
            // 
            // radioButtonTiffPackBits
            // 
            this.radioButtonTiffPackBits.AutoSize = true;
            this.radioButtonTiffPackBits.Location = new System.Drawing.Point(136, 76);
            this.radioButtonTiffPackBits.Name = "radioButtonTiffPackBits";
            this.radioButtonTiffPackBits.Size = new System.Drawing.Size(70, 17);
            this.radioButtonTiffPackBits.TabIndex = 9;
            this.radioButtonTiffPackBits.TabStop = true;
            this.radioButtonTiffPackBits.Text = "Pack Bits";
            this.radioButtonTiffPackBits.UseVisualStyleBackColor = true;
            this.radioButtonTiffPackBits.CheckedChanged += new System.EventHandler(this.radioButtonTiffPackBits_CheckedChanged);
            // 
            // radioButtonTiffLzw
            // 
            this.radioButtonTiffLzw.AutoSize = true;
            this.radioButtonTiffLzw.Location = new System.Drawing.Point(136, 52);
            this.radioButtonTiffLzw.Name = "radioButtonTiffLzw";
            this.radioButtonTiffLzw.Size = new System.Drawing.Size(49, 17);
            this.radioButtonTiffLzw.TabIndex = 8;
            this.radioButtonTiffLzw.TabStop = true;
            this.radioButtonTiffLzw.Text = "LZW";
            this.radioButtonTiffLzw.UseVisualStyleBackColor = true;
            this.radioButtonTiffLzw.CheckedChanged += new System.EventHandler(this.radioButtonTiffLzw_CheckedChanged);
            // 
            // radioButtonTiffRle
            // 
            this.radioButtonTiffRle.AutoSize = true;
            this.radioButtonTiffRle.Location = new System.Drawing.Point(136, 99);
            this.radioButtonTiffRle.Name = "radioButtonTiffRle";
            this.radioButtonTiffRle.Size = new System.Drawing.Size(46, 17);
            this.radioButtonTiffRle.TabIndex = 7;
            this.radioButtonTiffRle.TabStop = true;
            this.radioButtonTiffRle.Text = "RLE";
            this.radioButtonTiffRle.UseVisualStyleBackColor = true;
            this.radioButtonTiffRle.CheckedChanged += new System.EventHandler(this.radioButtonTiffRle_CheckedChanged);
            // 
            // radioButtonTiffJbig2
            // 
            this.radioButtonTiffJbig2.AutoSize = true;
            this.radioButtonTiffJbig2.Location = new System.Drawing.Point(136, 29);
            this.radioButtonTiffJbig2.Name = "radioButtonTiffJbig2";
            this.radioButtonTiffJbig2.Size = new System.Drawing.Size(54, 17);
            this.radioButtonTiffJbig2.TabIndex = 4;
            this.radioButtonTiffJbig2.TabStop = true;
            this.radioButtonTiffJbig2.Text = "JBIG2";
            this.radioButtonTiffJbig2.UseVisualStyleBackColor = true;
            this.radioButtonTiffJbig2.CheckedChanged += new System.EventHandler(this.radioButtonTiffJbig2_CheckedChanged);
            // 
            // radioButtonTiffGroup4
            // 
            this.radioButtonTiffGroup4.AutoSize = true;
            this.radioButtonTiffGroup4.Location = new System.Drawing.Point(23, 120);
            this.radioButtonTiffGroup4.Name = "radioButtonTiffGroup4";
            this.radioButtonTiffGroup4.Size = new System.Drawing.Size(63, 17);
            this.radioButtonTiffGroup4.TabIndex = 3;
            this.radioButtonTiffGroup4.TabStop = true;
            this.radioButtonTiffGroup4.Text = "Group 4";
            this.radioButtonTiffGroup4.UseVisualStyleBackColor = true;
            this.radioButtonTiffGroup4.CheckedChanged += new System.EventHandler(this.radioButtonTiffGroup4_CheckedChanged);
            // 
            // radioButtonTiffGroup31D
            // 
            this.radioButtonTiffGroup31D.AutoSize = true;
            this.radioButtonTiffGroup31D.Location = new System.Drawing.Point(23, 74);
            this.radioButtonTiffGroup31D.Name = "radioButtonTiffGroup31D";
            this.radioButtonTiffGroup31D.Size = new System.Drawing.Size(100, 17);
            this.radioButtonTiffGroup31D.TabIndex = 2;
            this.radioButtonTiffGroup31D.TabStop = true;
            this.radioButtonTiffGroup31D.Text = "Group 3 Fax 1D";
            this.radioButtonTiffGroup31D.UseVisualStyleBackColor = true;
            this.radioButtonTiffGroup31D.CheckedChanged += new System.EventHandler(this.radioButtonTiffGroup31D_CheckedChanged);
            // 
            // radioButtonTiffGroup32D
            // 
            this.radioButtonTiffGroup32D.AutoSize = true;
            this.radioButtonTiffGroup32D.Location = new System.Drawing.Point(23, 97);
            this.radioButtonTiffGroup32D.Name = "radioButtonTiffGroup32D";
            this.radioButtonTiffGroup32D.Size = new System.Drawing.Size(100, 17);
            this.radioButtonTiffGroup32D.TabIndex = 1;
            this.radioButtonTiffGroup32D.TabStop = true;
            this.radioButtonTiffGroup32D.Text = "Group 3 Fax 2D";
            this.radioButtonTiffGroup32D.UseVisualStyleBackColor = true;
            this.radioButtonTiffGroup32D.CheckedChanged += new System.EventHandler(this.radioButtonTiffGroup32D_CheckedChanged);
            // 
            // radioButtonTiffDeflate
            // 
            this.radioButtonTiffDeflate.AutoSize = true;
            this.radioButtonTiffDeflate.Location = new System.Drawing.Point(23, 52);
            this.radioButtonTiffDeflate.Name = "radioButtonTiffDeflate";
            this.radioButtonTiffDeflate.Size = new System.Drawing.Size(59, 17);
            this.radioButtonTiffDeflate.TabIndex = 0;
            this.radioButtonTiffDeflate.TabStop = true;
            this.radioButtonTiffDeflate.Text = "Deflate";
            this.radioButtonTiffDeflate.UseVisualStyleBackColor = true;
            this.radioButtonTiffDeflate.CheckedChanged += new System.EventHandler(this.radioButtonTiffDeflate_CheckedChanged);
            // 
            // tabPagePdf
            // 
            this.tabPagePdf.BackColor = System.Drawing.Color.White;
            this.tabPagePdf.Controls.Add(this.groupBoxPdfCompression);
            this.tabPagePdf.Controls.Add(this.checkBoxPdfSwapBlackAndWhite);
            this.tabPagePdf.Location = new System.Drawing.Point(4, 22);
            this.tabPagePdf.Name = "tabPagePdf";
            this.tabPagePdf.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePdf.Size = new System.Drawing.Size(344, 249);
            this.tabPagePdf.TabIndex = 1;
            this.tabPagePdf.Text = "PDF";
            this.tabPagePdf.UseVisualStyleBackColor = true;
            // 
            // groupBoxPdfCompression
            // 
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfNoCompression);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfPackBits);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfLzw);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfRle);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfJbig2);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfGroup4);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfGroup31D);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfGroup32D);
            this.groupBoxPdfCompression.Controls.Add(this.radioButtonPdfDeflate);
            this.groupBoxPdfCompression.Location = new System.Drawing.Point(25, 32);
            this.groupBoxPdfCompression.Name = "groupBoxPdfCompression";
            this.groupBoxPdfCompression.Size = new System.Drawing.Size(235, 140);
            this.groupBoxPdfCompression.TabIndex = 1;
            this.groupBoxPdfCompression.TabStop = false;
            this.groupBoxPdfCompression.Text = "Compression";
            // 
            // radioButtonPdfNoCompression
            // 
            this.radioButtonPdfNoCompression.AutoSize = true;
            this.radioButtonPdfNoCompression.Location = new System.Drawing.Point(26, 16);
            this.radioButtonPdfNoCompression.Name = "radioButtonPdfNoCompression";
            this.radioButtonPdfNoCompression.Size = new System.Drawing.Size(102, 17);
            this.radioButtonPdfNoCompression.TabIndex = 20;
            this.radioButtonPdfNoCompression.TabStop = true;
            this.radioButtonPdfNoCompression.Text = "No Compression";
            this.radioButtonPdfNoCompression.UseVisualStyleBackColor = true;
            this.radioButtonPdfNoCompression.CheckedChanged += new System.EventHandler(this.radioButtonPdfNoCompression_CheckedChanged);
            // 
            // radioButtonPdfPackBits
            // 
            this.radioButtonPdfPackBits.AutoSize = true;
            this.radioButtonPdfPackBits.Location = new System.Drawing.Point(139, 63);
            this.radioButtonPdfPackBits.Name = "radioButtonPdfPackBits";
            this.radioButtonPdfPackBits.Size = new System.Drawing.Size(70, 17);
            this.radioButtonPdfPackBits.TabIndex = 19;
            this.radioButtonPdfPackBits.TabStop = true;
            this.radioButtonPdfPackBits.Text = "Pack Bits";
            this.radioButtonPdfPackBits.UseVisualStyleBackColor = true;
            this.radioButtonPdfPackBits.CheckedChanged += new System.EventHandler(this.radioButtonPdfPackBits_CheckedChanged);
            // 
            // radioButtonPdfLzw
            // 
            this.radioButtonPdfLzw.AutoSize = true;
            this.radioButtonPdfLzw.Location = new System.Drawing.Point(139, 39);
            this.radioButtonPdfLzw.Name = "radioButtonPdfLzw";
            this.radioButtonPdfLzw.Size = new System.Drawing.Size(49, 17);
            this.radioButtonPdfLzw.TabIndex = 18;
            this.radioButtonPdfLzw.TabStop = true;
            this.radioButtonPdfLzw.Text = "LZW";
            this.radioButtonPdfLzw.UseVisualStyleBackColor = true;
            this.radioButtonPdfLzw.CheckedChanged += new System.EventHandler(this.radioButtonPdfLzw_CheckedChanged);
            // 
            // radioButtonPdfRle
            // 
            this.radioButtonPdfRle.AutoSize = true;
            this.radioButtonPdfRle.Location = new System.Drawing.Point(139, 86);
            this.radioButtonPdfRle.Name = "radioButtonPdfRle";
            this.radioButtonPdfRle.Size = new System.Drawing.Size(46, 17);
            this.radioButtonPdfRle.TabIndex = 17;
            this.radioButtonPdfRle.TabStop = true;
            this.radioButtonPdfRle.Text = "RLE";
            this.radioButtonPdfRle.UseVisualStyleBackColor = true;
            this.radioButtonPdfRle.CheckedChanged += new System.EventHandler(this.radioButtonPdfRle_CheckedChanged);
            // 
            // radioButtonPdfJbig2
            // 
            this.radioButtonPdfJbig2.AutoSize = true;
            this.radioButtonPdfJbig2.Location = new System.Drawing.Point(139, 16);
            this.radioButtonPdfJbig2.Name = "radioButtonPdfJbig2";
            this.radioButtonPdfJbig2.Size = new System.Drawing.Size(54, 17);
            this.radioButtonPdfJbig2.TabIndex = 16;
            this.radioButtonPdfJbig2.TabStop = true;
            this.radioButtonPdfJbig2.Text = "JBIG2";
            this.radioButtonPdfJbig2.UseVisualStyleBackColor = true;
            this.radioButtonPdfJbig2.CheckedChanged += new System.EventHandler(this.radioButtonPdfJbig2_CheckedChanged);
            // 
            // radioButtonPdfGroup4
            // 
            this.radioButtonPdfGroup4.AutoSize = true;
            this.radioButtonPdfGroup4.Location = new System.Drawing.Point(26, 107);
            this.radioButtonPdfGroup4.Name = "radioButtonPdfGroup4";
            this.radioButtonPdfGroup4.Size = new System.Drawing.Size(63, 17);
            this.radioButtonPdfGroup4.TabIndex = 15;
            this.radioButtonPdfGroup4.TabStop = true;
            this.radioButtonPdfGroup4.Text = "Group 4";
            this.radioButtonPdfGroup4.UseVisualStyleBackColor = true;
            this.radioButtonPdfGroup4.CheckedChanged += new System.EventHandler(this.radioButtonPdfGroup4_CheckedChanged);
            // 
            // radioButtonPdfGroup31D
            // 
            this.radioButtonPdfGroup31D.AutoSize = true;
            this.radioButtonPdfGroup31D.Location = new System.Drawing.Point(26, 61);
            this.radioButtonPdfGroup31D.Name = "radioButtonPdfGroup31D";
            this.radioButtonPdfGroup31D.Size = new System.Drawing.Size(100, 17);
            this.radioButtonPdfGroup31D.TabIndex = 14;
            this.radioButtonPdfGroup31D.TabStop = true;
            this.radioButtonPdfGroup31D.Text = "Group 3 Fax 1D";
            this.radioButtonPdfGroup31D.UseVisualStyleBackColor = true;
            this.radioButtonPdfGroup31D.CheckedChanged += new System.EventHandler(this.radioButtonPdfGroup31D_CheckedChanged);
            // 
            // radioButtonPdfGroup32D
            // 
            this.radioButtonPdfGroup32D.AutoSize = true;
            this.radioButtonPdfGroup32D.Location = new System.Drawing.Point(26, 84);
            this.radioButtonPdfGroup32D.Name = "radioButtonPdfGroup32D";
            this.radioButtonPdfGroup32D.Size = new System.Drawing.Size(100, 17);
            this.radioButtonPdfGroup32D.TabIndex = 13;
            this.radioButtonPdfGroup32D.TabStop = true;
            this.radioButtonPdfGroup32D.Text = "Group 3 Fax 2D";
            this.radioButtonPdfGroup32D.UseVisualStyleBackColor = true;
            this.radioButtonPdfGroup32D.CheckedChanged += new System.EventHandler(this.radioButtonPdfGroup32D_CheckedChanged);
            // 
            // radioButtonPdfDeflate
            // 
            this.radioButtonPdfDeflate.AutoSize = true;
            this.radioButtonPdfDeflate.Location = new System.Drawing.Point(26, 39);
            this.radioButtonPdfDeflate.Name = "radioButtonPdfDeflate";
            this.radioButtonPdfDeflate.Size = new System.Drawing.Size(59, 17);
            this.radioButtonPdfDeflate.TabIndex = 12;
            this.radioButtonPdfDeflate.TabStop = true;
            this.radioButtonPdfDeflate.Text = "Deflate";
            this.radioButtonPdfDeflate.UseVisualStyleBackColor = true;
            this.radioButtonPdfDeflate.CheckedChanged += new System.EventHandler(this.radioButtonPdfDeflate_CheckedChanged);
            // 
            // checkBoxPdfSwapBlackAndWhite
            // 
            this.checkBoxPdfSwapBlackAndWhite.AutoSize = true;
            this.checkBoxPdfSwapBlackAndWhite.Location = new System.Drawing.Point(25, 188);
            this.checkBoxPdfSwapBlackAndWhite.Name = "checkBoxPdfSwapBlackAndWhite";
            this.checkBoxPdfSwapBlackAndWhite.Size = new System.Drawing.Size(136, 17);
            this.checkBoxPdfSwapBlackAndWhite.TabIndex = 0;
            this.checkBoxPdfSwapBlackAndWhite.Text = "Swap Black And White";
            this.checkBoxPdfSwapBlackAndWhite.UseVisualStyleBackColor = true;
            this.checkBoxPdfSwapBlackAndWhite.Visible = false;
            this.checkBoxPdfSwapBlackAndWhite.CheckStateChanged += new System.EventHandler(this.checkBoxPdfSwapBlackAndWhite_CheckStateChanged);
            // 
            // tabPageJbig2
            // 
            this.tabPageJbig2.BackColor = System.Drawing.Color.White;
            this.tabPageJbig2.Controls.Add(this.labelLooseness);
            this.tabPageJbig2.Controls.Add(this.checkBoxInvertedRegion);
            this.tabPageJbig2.Controls.Add(this.numericUpDownLooseness);
            this.tabPageJbig2.Controls.Add(this.groupBoxFileOrganization);
            this.tabPageJbig2.Controls.Add(this.groupBoxEncodeMode);
            this.tabPageJbig2.Location = new System.Drawing.Point(4, 22);
            this.tabPageJbig2.Name = "tabPageJbig2";
            this.tabPageJbig2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageJbig2.Size = new System.Drawing.Size(344, 249);
            this.tabPageJbig2.TabIndex = 4;
            this.tabPageJbig2.Text = "JBIG2";
            this.tabPageJbig2.UseVisualStyleBackColor = true;
            // 
            // labelLooseness
            // 
            this.labelLooseness.AutoSize = true;
            this.labelLooseness.Location = new System.Drawing.Point(17, 226);
            this.labelLooseness.Name = "labelLooseness";
            this.labelLooseness.Size = new System.Drawing.Size(124, 13);
            this.labelLooseness.TabIndex = 5;
            this.labelLooseness.Text = "Looseness Compression:";
            // 
            // checkBoxInvertedRegion
            // 
            this.checkBoxInvertedRegion.AutoSize = true;
            this.checkBoxInvertedRegion.Location = new System.Drawing.Point(227, 225);
            this.checkBoxInvertedRegion.Name = "checkBoxInvertedRegion";
            this.checkBoxInvertedRegion.Size = new System.Drawing.Size(102, 17);
            this.checkBoxInvertedRegion.TabIndex = 4;
            this.checkBoxInvertedRegion.Text = "Inverted Region";
            this.checkBoxInvertedRegion.UseVisualStyleBackColor = true;
            this.checkBoxInvertedRegion.CheckedChanged += new System.EventHandler(this.checkBoxInvertedRegion_CheckedChanged);
            // 
            // numericUpDownLooseness
            // 
            this.numericUpDownLooseness.Location = new System.Drawing.Point(147, 224);
            this.numericUpDownLooseness.Name = "numericUpDownLooseness";
            this.numericUpDownLooseness.Size = new System.Drawing.Size(61, 20);
            this.numericUpDownLooseness.TabIndex = 3;
            this.numericUpDownLooseness.ValueChanged += new System.EventHandler(this.numericUpDownLooseness_ValueChanged);
            // 
            // groupBoxFileOrganization
            // 
            this.groupBoxFileOrganization.Controls.Add(this.radioButtonRandomAccess);
            this.groupBoxFileOrganization.Controls.Add(this.radioButtonDefault);
            this.groupBoxFileOrganization.Controls.Add(this.radioButtonSequential);
            this.groupBoxFileOrganization.Location = new System.Drawing.Point(20, 172);
            this.groupBoxFileOrganization.Name = "groupBoxFileOrganization";
            this.groupBoxFileOrganization.Size = new System.Drawing.Size(309, 46);
            this.groupBoxFileOrganization.TabIndex = 1;
            this.groupBoxFileOrganization.TabStop = false;
            this.groupBoxFileOrganization.Text = "File Organization";
            // 
            // radioButtonRandomAccess
            // 
            this.radioButtonRandomAccess.AutoSize = true;
            this.radioButtonRandomAccess.Location = new System.Drawing.Point(80, 21);
            this.radioButtonRandomAccess.Name = "radioButtonRandomAccess";
            this.radioButtonRandomAccess.Size = new System.Drawing.Size(103, 17);
            this.radioButtonRandomAccess.TabIndex = 1;
            this.radioButtonRandomAccess.TabStop = true;
            this.radioButtonRandomAccess.Text = "Random Access";
            this.radioButtonRandomAccess.UseVisualStyleBackColor = true;
            this.radioButtonRandomAccess.CheckedChanged += new System.EventHandler(this.radioButtonRandomAccess_CheckedChanged);
            // 
            // radioButtonDefault
            // 
            this.radioButtonDefault.AutoSize = true;
            this.radioButtonDefault.Location = new System.Drawing.Point(16, 21);
            this.radioButtonDefault.Name = "radioButtonDefault";
            this.radioButtonDefault.Size = new System.Drawing.Size(59, 17);
            this.radioButtonDefault.TabIndex = 0;
            this.radioButtonDefault.TabStop = true;
            this.radioButtonDefault.Text = "Default";
            this.radioButtonDefault.UseVisualStyleBackColor = true;
            this.radioButtonDefault.CheckedChanged += new System.EventHandler(this.radioButtonDefault_CheckedChanged);
            // 
            // radioButtonSequential
            // 
            this.radioButtonSequential.AutoSize = true;
            this.radioButtonSequential.Location = new System.Drawing.Point(189, 21);
            this.radioButtonSequential.Name = "radioButtonSequential";
            this.radioButtonSequential.Size = new System.Drawing.Size(75, 17);
            this.radioButtonSequential.TabIndex = 2;
            this.radioButtonSequential.Text = "Sequential";
            this.radioButtonSequential.UseVisualStyleBackColor = true;
            this.radioButtonSequential.CheckedChanged += new System.EventHandler(this.radioButtonSequential_CheckedChanged);
            // 
            // groupBoxEncodeMode
            // 
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLossyHalftoneMmr);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLossyHalftoneMq);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLossyTextMmr);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLossyTextMq);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLosslessTextSpmMmr);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLosslessTextSpmMq);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLosslessTextMmr);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLosslessTextMq);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLosslessGenericMmr);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonLosslessGenericMq);
            this.groupBoxEncodeMode.Controls.Add(this.radioButtonAutoDetect);
            this.groupBoxEncodeMode.Location = new System.Drawing.Point(20, 6);
            this.groupBoxEncodeMode.Name = "groupBoxEncodeMode";
            this.groupBoxEncodeMode.Size = new System.Drawing.Size(309, 163);
            this.groupBoxEncodeMode.TabIndex = 0;
            this.groupBoxEncodeMode.TabStop = false;
            this.groupBoxEncodeMode.Text = "Encode Mode Compression";
            // 
            // radioButtonLossyHalftoneMmr
            // 
            this.radioButtonLossyHalftoneMmr.AutoSize = true;
            this.radioButtonLossyHalftoneMmr.Location = new System.Drawing.Point(152, 111);
            this.radioButtonLossyHalftoneMmr.Name = "radioButtonLossyHalftoneMmr";
            this.radioButtonLossyHalftoneMmr.Size = new System.Drawing.Size(124, 17);
            this.radioButtonLossyHalftoneMmr.TabIndex = 10;
            this.radioButtonLossyHalftoneMmr.TabStop = true;
            this.radioButtonLossyHalftoneMmr.Text = "Lossy Halftone MMR";
            this.radioButtonLossyHalftoneMmr.UseVisualStyleBackColor = true;
            this.radioButtonLossyHalftoneMmr.CheckedChanged += new System.EventHandler(this.radioButtonLossyHalftoneMmr_CheckedChanged);
            // 
            // radioButtonLossyHalftoneMq
            // 
            this.radioButtonLossyHalftoneMq.AutoSize = true;
            this.radioButtonLossyHalftoneMq.Location = new System.Drawing.Point(152, 88);
            this.radioButtonLossyHalftoneMq.Name = "radioButtonLossyHalftoneMq";
            this.radioButtonLossyHalftoneMq.Size = new System.Drawing.Size(115, 17);
            this.radioButtonLossyHalftoneMq.TabIndex = 9;
            this.radioButtonLossyHalftoneMq.TabStop = true;
            this.radioButtonLossyHalftoneMq.Text = "Lossy Halftone MQ";
            this.radioButtonLossyHalftoneMq.UseVisualStyleBackColor = true;
            this.radioButtonLossyHalftoneMq.CheckedChanged += new System.EventHandler(this.radioButtonLossyHalftoneMq_CheckedChanged);
            // 
            // radioButtonLossyTextMmr
            // 
            this.radioButtonLossyTextMmr.AutoSize = true;
            this.radioButtonLossyTextMmr.Location = new System.Drawing.Point(152, 65);
            this.radioButtonLossyTextMmr.Name = "radioButtonLossyTextMmr";
            this.radioButtonLossyTextMmr.Size = new System.Drawing.Size(105, 17);
            this.radioButtonLossyTextMmr.TabIndex = 8;
            this.radioButtonLossyTextMmr.TabStop = true;
            this.radioButtonLossyTextMmr.Text = "Lossy Text MMR";
            this.radioButtonLossyTextMmr.UseVisualStyleBackColor = true;
            this.radioButtonLossyTextMmr.CheckedChanged += new System.EventHandler(this.radioButtonLossyTextMmr_CheckedChanged);
            // 
            // radioButtonLossyTextMq
            // 
            this.radioButtonLossyTextMq.AutoSize = true;
            this.radioButtonLossyTextMq.Location = new System.Drawing.Point(152, 42);
            this.radioButtonLossyTextMq.Name = "radioButtonLossyTextMq";
            this.radioButtonLossyTextMq.Size = new System.Drawing.Size(96, 17);
            this.radioButtonLossyTextMq.TabIndex = 7;
            this.radioButtonLossyTextMq.TabStop = true;
            this.radioButtonLossyTextMq.Text = "Lossy Text MQ";
            this.radioButtonLossyTextMq.UseVisualStyleBackColor = true;
            this.radioButtonLossyTextMq.CheckedChanged += new System.EventHandler(this.radioButtonLossyTextMq_CheckedChanged);
            // 
            // radioButtonLosslessTextSpmMmr
            // 
            this.radioButtonLosslessTextSpmMmr.AutoSize = true;
            this.radioButtonLosslessTextSpmMmr.Location = new System.Drawing.Point(152, 19);
            this.radioButtonLosslessTextSpmMmr.Name = "radioButtonLosslessTextSpmMmr";
            this.radioButtonLosslessTextSpmMmr.Size = new System.Drawing.Size(144, 17);
            this.radioButtonLosslessTextSpmMmr.TabIndex = 6;
            this.radioButtonLosslessTextSpmMmr.TabStop = true;
            this.radioButtonLosslessTextSpmMmr.Text = "Lossless Text SPM MMR";
            this.radioButtonLosslessTextSpmMmr.UseVisualStyleBackColor = true;
            this.radioButtonLosslessTextSpmMmr.CheckedChanged += new System.EventHandler(this.radioButtonLosslessTextSpmMmr_CheckedChanged);
            // 
            // radioButtonLosslessTextSpmMq
            // 
            this.radioButtonLosslessTextSpmMq.AutoSize = true;
            this.radioButtonLosslessTextSpmMq.Location = new System.Drawing.Point(16, 134);
            this.radioButtonLosslessTextSpmMq.Name = "radioButtonLosslessTextSpmMq";
            this.radioButtonLosslessTextSpmMq.Size = new System.Drawing.Size(135, 17);
            this.radioButtonLosslessTextSpmMq.TabIndex = 5;
            this.radioButtonLosslessTextSpmMq.TabStop = true;
            this.radioButtonLosslessTextSpmMq.Text = "Lossless Text SPM MQ";
            this.radioButtonLosslessTextSpmMq.UseVisualStyleBackColor = true;
            this.radioButtonLosslessTextSpmMq.CheckedChanged += new System.EventHandler(this.radioButtonLosslessTextSpmMq_CheckedChanged);
            // 
            // radioButtonLosslessTextMmr
            // 
            this.radioButtonLosslessTextMmr.AutoSize = true;
            this.radioButtonLosslessTextMmr.Location = new System.Drawing.Point(16, 111);
            this.radioButtonLosslessTextMmr.Name = "radioButtonLosslessTextMmr";
            this.radioButtonLosslessTextMmr.Size = new System.Drawing.Size(118, 17);
            this.radioButtonLosslessTextMmr.TabIndex = 4;
            this.radioButtonLosslessTextMmr.TabStop = true;
            this.radioButtonLosslessTextMmr.Text = "Lossless Text MMR";
            this.radioButtonLosslessTextMmr.UseVisualStyleBackColor = true;
            this.radioButtonLosslessTextMmr.CheckedChanged += new System.EventHandler(this.radioButtonLosslessTextMmr_CheckedChanged);
            // 
            // radioButtonLosslessTextMq
            // 
            this.radioButtonLosslessTextMq.AutoSize = true;
            this.radioButtonLosslessTextMq.Location = new System.Drawing.Point(16, 88);
            this.radioButtonLosslessTextMq.Name = "radioButtonLosslessTextMq";
            this.radioButtonLosslessTextMq.Size = new System.Drawing.Size(109, 17);
            this.radioButtonLosslessTextMq.TabIndex = 3;
            this.radioButtonLosslessTextMq.TabStop = true;
            this.radioButtonLosslessTextMq.Text = "Lossless Text MQ";
            this.radioButtonLosslessTextMq.UseVisualStyleBackColor = true;
            this.radioButtonLosslessTextMq.CheckedChanged += new System.EventHandler(this.radioButtonLosslessTextMq_CheckedChanged);
            // 
            // radioButtonLosslessGenericMmr
            // 
            this.radioButtonLosslessGenericMmr.AutoSize = true;
            this.radioButtonLosslessGenericMmr.Location = new System.Drawing.Point(16, 65);
            this.radioButtonLosslessGenericMmr.Name = "radioButtonLosslessGenericMmr";
            this.radioButtonLosslessGenericMmr.Size = new System.Drawing.Size(134, 17);
            this.radioButtonLosslessGenericMmr.TabIndex = 2;
            this.radioButtonLosslessGenericMmr.TabStop = true;
            this.radioButtonLosslessGenericMmr.Text = "Lossless Generic MMR";
            this.radioButtonLosslessGenericMmr.UseVisualStyleBackColor = true;
            this.radioButtonLosslessGenericMmr.CheckedChanged += new System.EventHandler(this.radioButtonLosslessGenericMmr_CheckedChanged);
            // 
            // radioButtonLosslessGenericMq
            // 
            this.radioButtonLosslessGenericMq.AutoSize = true;
            this.radioButtonLosslessGenericMq.Location = new System.Drawing.Point(16, 42);
            this.radioButtonLosslessGenericMq.Name = "radioButtonLosslessGenericMq";
            this.radioButtonLosslessGenericMq.Size = new System.Drawing.Size(125, 17);
            this.radioButtonLosslessGenericMq.TabIndex = 1;
            this.radioButtonLosslessGenericMq.TabStop = true;
            this.radioButtonLosslessGenericMq.Text = "Lossless Generic MQ";
            this.radioButtonLosslessGenericMq.UseVisualStyleBackColor = true;
            this.radioButtonLosslessGenericMq.CheckedChanged += new System.EventHandler(this.radioButtonLosslessGenericMq_CheckedChanged);
            // 
            // radioButtonAutoDetect
            // 
            this.radioButtonAutoDetect.AutoSize = true;
            this.radioButtonAutoDetect.Location = new System.Drawing.Point(16, 19);
            this.radioButtonAutoDetect.Name = "radioButtonAutoDetect";
            this.radioButtonAutoDetect.Size = new System.Drawing.Size(82, 17);
            this.radioButtonAutoDetect.TabIndex = 0;
            this.radioButtonAutoDetect.TabStop = true;
            this.radioButtonAutoDetect.Text = "Auto Detect";
            this.radioButtonAutoDetect.UseVisualStyleBackColor = true;
            this.radioButtonAutoDetect.CheckedChanged += new System.EventHandler(this.radioButtonAutoDetect_CheckedChanged);
            // 
            // tabPageModca
            // 
            this.tabPageModca.Controls.Add(this.labelModcaNoSettings);
            this.tabPageModca.Location = new System.Drawing.Point(4, 22);
            this.tabPageModca.Name = "tabPageModca";
            this.tabPageModca.Size = new System.Drawing.Size(344, 249);
            this.tabPageModca.TabIndex = 5;
            this.tabPageModca.Text = "MO:DCA";
            this.tabPageModca.UseVisualStyleBackColor = true;
            // 
            // labelModcaNoSettings
            // 
            this.labelModcaNoSettings.AutoSize = true;
            this.labelModcaNoSettings.Location = new System.Drawing.Point(117, 115);
            this.labelModcaNoSettings.Name = "labelModcaNoSettings";
            this.labelModcaNoSettings.Size = new System.Drawing.Size(108, 13);
            this.labelModcaNoSettings.TabIndex = 0;
            this.labelModcaNoSettings.Text = "No settings available.";
            // 
            // tabPageCals
            // 
            this.tabPageCals.Controls.Add(this.labelCalsNoSettings);
            this.tabPageCals.Location = new System.Drawing.Point(4, 22);
            this.tabPageCals.Name = "tabPageCals";
            this.tabPageCals.Size = new System.Drawing.Size(344, 249);
            this.tabPageCals.TabIndex = 6;
            this.tabPageCals.Text = "CALS";
            this.tabPageCals.UseVisualStyleBackColor = true;
            // 
            // labelCalsNoSettings
            // 
            this.labelCalsNoSettings.AutoSize = true;
            this.labelCalsNoSettings.Location = new System.Drawing.Point(117, 115);
            this.labelCalsNoSettings.Name = "labelCalsNoSettings";
            this.labelCalsNoSettings.Size = new System.Drawing.Size(108, 13);
            this.labelCalsNoSettings.TabIndex = 0;
            this.labelCalsNoSettings.Text = "No settings available.";
            // 
            // groupBoxImageType
            // 
            this.groupBoxImageType.BackColor = System.Drawing.Color.White;
            this.groupBoxImageType.Controls.Add(this.radioButtonCals);
            this.groupBoxImageType.Controls.Add(this.radioButtonModca);
            this.groupBoxImageType.Controls.Add(this.radioButtonJbig2);
            this.groupBoxImageType.Controls.Add(this.radioButtonPdf);
            this.groupBoxImageType.Controls.Add(this.radioButtonTiff);
            this.groupBoxImageType.Location = new System.Drawing.Point(20, 70);
            this.groupBoxImageType.Name = "groupBoxImageType";
            this.groupBoxImageType.Size = new System.Drawing.Size(134, 139);
            this.groupBoxImageType.TabIndex = 1;
            this.groupBoxImageType.TabStop = false;
            this.groupBoxImageType.Text = "Image Type";
            // 
            // radioButtonCals
            // 
            this.radioButtonCals.AutoSize = true;
            this.radioButtonCals.Location = new System.Drawing.Point(19, 112);
            this.radioButtonCals.Name = "radioButtonCals";
            this.radioButtonCals.Size = new System.Drawing.Size(52, 17);
            this.radioButtonCals.TabIndex = 5;
            this.radioButtonCals.Text = "CALS";
            this.radioButtonCals.UseVisualStyleBackColor = true;
            this.radioButtonCals.CheckedChanged += new System.EventHandler(this.radioButtonCals_CheckedChanged);
            // 
            // radioButtonModca
            // 
            this.radioButtonModca.AutoSize = true;
            this.radioButtonModca.Location = new System.Drawing.Point(19, 88);
            this.radioButtonModca.Name = "radioButtonModca";
            this.radioButtonModca.Size = new System.Drawing.Size(67, 17);
            this.radioButtonModca.TabIndex = 4;
            this.radioButtonModca.Text = "MO:DCA";
            this.radioButtonModca.UseVisualStyleBackColor = true;
            this.radioButtonModca.CheckedChanged += new System.EventHandler(this.radioButtonModca_CheckedChanged);
            // 
            // radioButtonJbig2
            // 
            this.radioButtonJbig2.AutoSize = true;
            this.radioButtonJbig2.Location = new System.Drawing.Point(19, 65);
            this.radioButtonJbig2.Name = "radioButtonJbig2";
            this.radioButtonJbig2.Size = new System.Drawing.Size(54, 17);
            this.radioButtonJbig2.TabIndex = 3;
            this.radioButtonJbig2.Text = "JBIG2";
            this.radioButtonJbig2.UseVisualStyleBackColor = true;
            this.radioButtonJbig2.CheckedChanged += new System.EventHandler(this.radioButtonJbig2_CheckedChanged);
            // 
            // radioButtonPdf
            // 
            this.radioButtonPdf.AutoSize = true;
            this.radioButtonPdf.Location = new System.Drawing.Point(19, 42);
            this.radioButtonPdf.Name = "radioButtonPdf";
            this.radioButtonPdf.Size = new System.Drawing.Size(46, 17);
            this.radioButtonPdf.TabIndex = 2;
            this.radioButtonPdf.Text = "PDF";
            this.radioButtonPdf.UseVisualStyleBackColor = true;
            this.radioButtonPdf.CheckedChanged += new System.EventHandler(this.radioButtonPdf_CheckedChanged);
            // 
            // radioButtonTiff
            // 
            this.radioButtonTiff.AutoSize = true;
            this.radioButtonTiff.Checked = true;
            this.radioButtonTiff.Location = new System.Drawing.Point(19, 19);
            this.radioButtonTiff.Name = "radioButtonTiff";
            this.radioButtonTiff.Size = new System.Drawing.Size(47, 17);
            this.radioButtonTiff.TabIndex = 1;
            this.radioButtonTiff.TabStop = true;
            this.radioButtonTiff.Text = "TIFF";
            this.radioButtonTiff.UseVisualStyleBackColor = true;
            this.radioButtonTiff.CheckedChanged += new System.EventHandler(this.radioButtonTiff_CheckedChanged);
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.BackColor = System.Drawing.Color.White;
            this.groupBoxSettings.Controls.Add(this.checkBoxPreview);
            this.groupBoxSettings.Controls.Add(this.buttonRestoreDefaults);
            this.groupBoxSettings.Location = new System.Drawing.Point(20, 212);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(134, 88);
            this.groupBoxSettings.TabIndex = 4;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // checkBoxPreview
            // 
            this.checkBoxPreview.AutoSize = true;
            this.checkBoxPreview.Checked = true;
            this.checkBoxPreview.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPreview.Location = new System.Drawing.Point(35, 23);
            this.checkBoxPreview.Name = "checkBoxPreview";
            this.checkBoxPreview.Size = new System.Drawing.Size(64, 17);
            this.checkBoxPreview.TabIndex = 5;
            this.checkBoxPreview.Text = "Preview";
            this.checkBoxPreview.UseVisualStyleBackColor = true;
            this.checkBoxPreview.CheckedChanged += new System.EventHandler(this.checkBoxPreview_CheckedChanged);
            // 
            // buttonRestoreDefaults
            // 
            this.buttonRestoreDefaults.Location = new System.Drawing.Point(6, 46);
            this.buttonRestoreDefaults.Name = "buttonRestoreDefaults";
            this.buttonRestoreDefaults.Size = new System.Drawing.Size(122, 32);
            this.buttonRestoreDefaults.TabIndex = 4;
            this.buttonRestoreDefaults.Text = "Restore All Defaults";
            this.buttonRestoreDefaults.UseVisualStyleBackColor = true;
            this.buttonRestoreDefaults.Click += new System.EventHandler(this.buttonRestoreDefaults_Click);
            // 
            // SettingsFormBW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ImagXpressCompressionDemo.Properties.Resources.ap_dark_stripes;
            this.ClientSize = new System.Drawing.Size(549, 375);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.panelBackgroundFrameBW);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsFormBW";
            this.Text = "Black and White Compression Settings";
            this.Load += new System.EventHandler(this.SettingsFormBW_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SettingsFormBW_FormClosed);
            this.panelBackgroundFrameBW.ResumeLayout(false);
            this.groupBoxCompressedImage.ResumeLayout(false);
            this.groupBoxCompressedImage.PerformLayout();
            this.tabControlSettings.ResumeLayout(false);
            this.tabPageTiff.ResumeLayout(false);
            this.groupTiffCompression.ResumeLayout(false);
            this.groupTiffCompression.PerformLayout();
            this.tabPagePdf.ResumeLayout(false);
            this.tabPagePdf.PerformLayout();
            this.groupBoxPdfCompression.ResumeLayout(false);
            this.groupBoxPdfCompression.PerformLayout();
            this.tabPageJbig2.ResumeLayout(false);
            this.tabPageJbig2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLooseness)).EndInit();
            this.groupBoxFileOrganization.ResumeLayout(false);
            this.groupBoxFileOrganization.PerformLayout();
            this.groupBoxEncodeMode.ResumeLayout(false);
            this.groupBoxEncodeMode.PerformLayout();
            this.tabPageModca.ResumeLayout(false);
            this.tabPageModca.PerformLayout();
            this.tabPageCals.ResumeLayout(false);
            this.tabPageCals.PerformLayout();
            this.groupBoxImageType.ResumeLayout(false);
            this.groupBoxImageType.PerformLayout();
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBoxImageType;
		private System.Windows.Forms.RadioButton radioButtonCals;
		private System.Windows.Forms.RadioButton radioButtonModca;
		private System.Windows.Forms.RadioButton radioButtonJbig2;
		private System.Windows.Forms.RadioButton radioButtonPdf;
        private System.Windows.Forms.RadioButton radioButtonTiff;
		private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabPage tabPageJbig2;
        private System.Windows.Forms.TabPage tabPagePdf;
		private System.Windows.Forms.TabControl tabControlSettings;
        private System.Windows.Forms.TabPage tabPageTiff;
        private System.Windows.Forms.Button buttonRestoreDefaults;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.CheckBox checkBoxPreview;
        private System.Windows.Forms.GroupBox groupTiffCompression;
        private System.Windows.Forms.RadioButton radioButtonTiffPackBits;
        private System.Windows.Forms.RadioButton radioButtonTiffLzw;
        private System.Windows.Forms.RadioButton radioButtonTiffJbig2;
        private System.Windows.Forms.RadioButton radioButtonTiffGroup4;
        private System.Windows.Forms.RadioButton radioButtonTiffGroup31D;
        private System.Windows.Forms.RadioButton radioButtonTiffGroup32D;
        private System.Windows.Forms.RadioButton radioButtonTiffDeflate;
        private System.Windows.Forms.RadioButton radioButtonTiffNoCompression;
        private System.Windows.Forms.RadioButton radioButtonTiffRle;
        private System.Windows.Forms.GroupBox groupBoxPdfCompression;
        private System.Windows.Forms.RadioButton radioButtonPdfNoCompression;
        private System.Windows.Forms.RadioButton radioButtonPdfPackBits;
        private System.Windows.Forms.RadioButton radioButtonPdfLzw;
        private System.Windows.Forms.RadioButton radioButtonPdfRle;
        private System.Windows.Forms.RadioButton radioButtonPdfJbig2;
        private System.Windows.Forms.RadioButton radioButtonPdfGroup4;
        private System.Windows.Forms.RadioButton radioButtonPdfGroup31D;
        private System.Windows.Forms.RadioButton radioButtonPdfGroup32D;
        private System.Windows.Forms.RadioButton radioButtonPdfDeflate;
        private System.Windows.Forms.CheckBox checkBoxPdfSwapBlackAndWhite;
        private System.Windows.Forms.GroupBox groupBoxFileOrganization;
        private System.Windows.Forms.RadioButton radioButtonSequential;
        private System.Windows.Forms.RadioButton radioButtonRandomAccess;
        private System.Windows.Forms.RadioButton radioButtonDefault;
        private System.Windows.Forms.GroupBox groupBoxEncodeMode;
        private System.Windows.Forms.RadioButton radioButtonLossyHalftoneMmr;
        private System.Windows.Forms.RadioButton radioButtonLossyHalftoneMq;
        private System.Windows.Forms.RadioButton radioButtonLossyTextMmr;
        private System.Windows.Forms.RadioButton radioButtonLossyTextMq;
        private System.Windows.Forms.RadioButton radioButtonLosslessTextSpmMmr;
        private System.Windows.Forms.RadioButton radioButtonLosslessTextSpmMq;
        private System.Windows.Forms.RadioButton radioButtonLosslessTextMmr;
        private System.Windows.Forms.RadioButton radioButtonLosslessTextMq;
        private System.Windows.Forms.RadioButton radioButtonLosslessGenericMmr;
        private System.Windows.Forms.RadioButton radioButtonLosslessGenericMq;
        private System.Windows.Forms.RadioButton radioButtonAutoDetect;
        private System.Windows.Forms.Label labelLooseness;
        private System.Windows.Forms.CheckBox checkBoxInvertedRegion;
        private System.Windows.Forms.NumericUpDown numericUpDownLooseness;
        private System.Windows.Forms.GroupBox groupBoxCompressedImage;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage4;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage3;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage2;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage1;
        private System.Windows.Forms.TabPage tabPageModca;
        private System.Windows.Forms.Label labelModcaNoSettings;
        private System.Windows.Forms.TabPage tabPageCals;
        private System.Windows.Forms.Label labelCalsNoSettings;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrameBW;
	}
}
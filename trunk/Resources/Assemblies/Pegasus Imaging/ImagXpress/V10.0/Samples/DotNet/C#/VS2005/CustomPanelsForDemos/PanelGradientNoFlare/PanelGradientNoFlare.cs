using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//
using System.Drawing.Drawing2D;


namespace AccusoftCustom
{
    public partial class PanelGradientNoFlare : Panel
    {
        public PanelGradientNoFlare()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor, true);
            UpdateStyles();
        }


        protected override void OnPaint(PaintEventArgs pe)
        {

            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
       


            Rectangle BaseRectangle = new Rectangle(0, 0, this.Width, this.Height);

            
            Brush Gradient_Brush =
                new LinearGradientBrush(
                BaseRectangle,
                Color.FromArgb(220, 220, 220), Color.FromArgb(180, 180, 180), 90);

            Brush WhiteBrush_2 =
                 new LinearGradientBrush(
                 BaseRectangle,
                 Color.FromArgb(240, 240, 240), Color.FromArgb(210, 210, 210), 90);

            Brush WhiteBrush =
                new LinearGradientBrush(
                BaseRectangle,
                Color.FromArgb(255, 255, 255), Color.FromArgb(230, 230, 230), 90);

            Pen hiLite1 = new Pen(Color.White);
            hiLite1.Width = 1;

            Pen shadow1 = new Pen(Color.Black);
            shadow1.Width = 10;

            GraphicsPath bodyPath = new GraphicsPath();
            GraphicsPath hilitePath = new GraphicsPath();
            GraphicsPath hilitePath2 = new GraphicsPath();
            pe.Graphics.FillPath(Gradient_Brush, bodyPath);
            pe.Graphics.FillPath(WhiteBrush, hilitePath);
            pe.Graphics.FillPath(WhiteBrush_2, hilitePath2);

           
            bodyPath.StartFigure();
            bodyPath.AddLine(4, this.Height - 16, 4, 16);    //bottom left to top left
            bodyPath.AddArc(4, 4, 12, 12, 180, 90);
            bodyPath.AddLine(16, 4, this.Width - 16, 4);     //top left to top right
            bodyPath.AddArc(this.Width - 16, 4, 12, 12, 270, 90);
            bodyPath.AddLine(this.Width - 4, 16, this.Width - 4, this.Height - 16);
            bodyPath.AddArc(this.Width - 16, this.Height - 16, 12, 12, 0, 90);
            bodyPath.AddLine(this.Width - 16, this.Height - 4, 16, this.Height - 4);
            bodyPath.AddArc(4, this.Height - 16, 12, 12, 90, 90);
            bodyPath.CloseFigure();
            pe.Graphics.FillPath(Gradient_Brush, bodyPath);
            pe.Graphics.DrawPath(Pens.Black, bodyPath);

            hilitePath.StartFigure();
            hilitePath.AddLine(8, this.Height - 16, 8, 16);    //bottom left to top left
            hilitePath.AddArc(8, 8, 8, 8, 180, 90);
            hilitePath.AddLine(16, 8, this.Width - 16, 8);     //top left to top right
            hilitePath.AddArc(this.Width - 16, 8, 8, 8, 270, 90);
            hilitePath.AddLine(this.Width - 8, 16, this.Width - 8, this.Height - 16);
            hilitePath.AddArc(this.Width - 16, this.Height - 16, 8, 8, 0, 90);
            hilitePath.AddLine(this.Width - 16, this.Height - 8, 16, this.Height - 8);
            hilitePath.AddArc(8, this.Height - 16, 8, 8, 90, 90);
            hilitePath.CloseFigure();
            pe.Graphics.FillPath(WhiteBrush_2, hilitePath);
            
            hilitePath2.StartFigure();
            hilitePath2.AddLine(10, this.Height - 18, 10, 16);    //bottom left to top left
            hilitePath2.AddArc(10, 10, 8, 8, 180, 90);
            hilitePath2.AddLine(18, 10, this.Width - 18, 10);     //top left to top right
            hilitePath2.AddArc(this.Width - 18, 10, 8, 8, 270, 90);
            hilitePath2.AddLine(this.Width - 10, 18, this.Width - 10, this.Height - 18);
            hilitePath2.AddArc(this.Width - 18, this.Height - 18, 8, 8, 0, 90);
            hilitePath2.AddLine(this.Width - 18, this.Height - 10, 18, this.Height - 10);
            hilitePath2.AddArc(10, this.Height - 18, 8, 8, 90, 90);
            hilitePath2.CloseFigure();
            pe.Graphics.FillPath(WhiteBrush, hilitePath2);
        }


    }
}

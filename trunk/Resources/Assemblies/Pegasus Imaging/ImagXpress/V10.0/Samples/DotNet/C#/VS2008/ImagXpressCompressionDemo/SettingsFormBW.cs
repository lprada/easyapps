﻿/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accusoft.ImagXpressSdk;

namespace ImagXpressCompressionDemo
{
	public partial class SettingsFormBW : Form
	{
        SaveOptions mySaveOptionsCurrent;
        SaveOptions mySaveOptions1;
        SaveOptions mySaveOptions2;
        SaveOptions mySaveOptions3;
        SaveOptions mySaveOptions4;
        MainForm mainForm;
        bool settingsMatchImage = true;
        bool imageHasBeenModified = false;
        bool enableUpdate = false; // For disabling main form image update when settings form loads.
        CompressedImage compressedImageCurrent;
        CompressedImage compressedImage1;
        CompressedImage compressedImage2;
        CompressedImage compressedImage3;
        CompressedImage compressedImage4;

        public SettingsFormBW(MainForm theMainForm, CompressedImage theCompressedImage1, CompressedImage theCompressedImage2, CompressedImage theCompressedImage3, CompressedImage theCompressedImage4)
		{
			InitializeComponent();

            // The image has not been modified yet.
            imageHasBeenModified = false;
            settingsMatchImage = true;

            compressedImage1 = theCompressedImage1;
            compressedImage2 = theCompressedImage2;
            compressedImage3 = theCompressedImage3;
            compressedImage4 = theCompressedImage4;

            mainForm = theMainForm;
            mainForm.EnableSettings(false);

            SetAllSaveOptions();

            SelectCompressedImage();
        }

        #region Load Compression Settings

        private void SettingsFormBW_Load(object sender, EventArgs e)
        {
            this.Text = "Compressed Image Settings (Black and White)";
            DisplaySettings();
        }

        private void buttonRestoreDefaults_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("This will reset the color, grayscale, and black and white compression settings for all 4 compressed images to their default values and cannot be undone.  Are you sure you want to restore all defaults?", "Confirm Restore All Defaults", MessageBoxButtons.OKCancel);
            if (res == DialogResult.OK)
            {
                mainForm.SetDefaultCompressionSettings();
                mainForm.ResetCompressedImages();
                mainForm.CompressImages();
                SetAllSaveOptions();
                // Update current compressed image and options
                if (radioButtonCompressedImage1.Checked == true)
                {
                    compressedImageCurrent = compressedImage1;
                    mySaveOptionsCurrent = mySaveOptions1;
                }
                else if (radioButtonCompressedImage2.Checked == true)
                {
                    compressedImageCurrent = compressedImage2;
                    mySaveOptionsCurrent = mySaveOptions2;
                }
                else if (radioButtonCompressedImage3.Checked == true)
                {
                    compressedImageCurrent = compressedImage3;
                    mySaveOptionsCurrent = mySaveOptions3;
                }
                else
                {
                    compressedImageCurrent = compressedImage4;
                    mySaveOptionsCurrent = mySaveOptions4;
                }
                DisplaySettings();
            }
        }

        private void SetAllSaveOptions()
        {
            mySaveOptions1 = new SaveOptions();
            SetSaveOptions(compressedImage1.GetSaveOptionsBW(), mySaveOptions1);
            mySaveOptions2 = new SaveOptions();
            SetSaveOptions(compressedImage2.GetSaveOptionsBW(), mySaveOptions2);
            mySaveOptions3 = new SaveOptions();
            SetSaveOptions(compressedImage3.GetSaveOptionsBW(), mySaveOptions3);
            mySaveOptions4 = new SaveOptions();
            SetSaveOptions(compressedImage4.GetSaveOptionsBW(), mySaveOptions4);
        }

        private void SetSaveOptions(SaveOptions compressedImageSaveOptions, SaveOptions settingsSaveOptions)
        {
            // Copy the save options.
            // It is only necessary to copy the settings that can be modified on this form.
            settingsSaveOptions.Format = compressedImageSaveOptions.Format;
            settingsSaveOptions.Tiff.Compression = compressedImageSaveOptions.Tiff.Compression;
            settingsSaveOptions.Pdf.SwapBlackAndWhite = compressedImageSaveOptions.Pdf.SwapBlackAndWhite;
            settingsSaveOptions.Pdf.Compression = compressedImageSaveOptions.Pdf.Compression;
            settingsSaveOptions.Jbig2.EncodeModeCompression = compressedImageSaveOptions.Jbig2.EncodeModeCompression;
            settingsSaveOptions.Jbig2.FileOrganization = compressedImageSaveOptions.Jbig2.FileOrganization;
            settingsSaveOptions.Jbig2.InvertedRegion = compressedImageSaveOptions.Jbig2.InvertedRegion;
            settingsSaveOptions.Jbig2.LoosenessCompression = compressedImageSaveOptions.Jbig2.LoosenessCompression;
        }

        private void DisplaySettings()
        {
            enableUpdate = false;

            // Display the save options.

            // Select the tab that corresponds to the save options format.
            switch (mySaveOptionsCurrent.Format)
            {
                case ImageXFormat.Tiff:
                    tabControlSettings.SelectedIndex = 0;
                    radioButtonTiff.Checked = true;
                    break;
                case ImageXFormat.Pdf:
                    tabControlSettings.SelectedIndex = 1;
                    radioButtonPdf.Checked = true;
                    break;
                case ImageXFormat.Jbig2:
                    tabControlSettings.SelectedIndex = 2;
                    radioButtonJbig2.Checked = true;
                    break;
                case ImageXFormat.Modca:
                    tabControlSettings.SelectedIndex = 3;
                    radioButtonModca.Checked = true;
                    break;
                case ImageXFormat.Cals:
                    tabControlSettings.SelectedIndex = 4;
                    radioButtonCals.Checked = true;
                    break;
            }

            switch (mySaveOptionsCurrent.Tiff.Compression)
            {
                case Compression.Deflate:
                    radioButtonTiffDeflate.Checked = true;
                    break;
                case Compression.Group3Fax1d:
                    radioButtonTiffGroup31D.Checked = true;
                    break;
                case Compression.Group3Fax2d:
                    radioButtonTiffGroup32D.Checked = true;
                    break;
                case Compression.Group4:
                    radioButtonTiffGroup4.Checked = true;
                    break;
                case Compression.Jbig2:
                    radioButtonTiffJbig2.Checked = true;
                    break;
                case Compression.Lzw:
                    radioButtonTiffLzw.Checked = true;
                    break;
                case Compression.NoCompression:
                    radioButtonTiffNoCompression.Checked = true;
                    break;
                case Compression.PackBits:
                    radioButtonTiffPackBits.Checked = true;
                    break;
                case Compression.Rle:
                    radioButtonTiffRle.Checked = true;
                    break;
            }

            switch (mySaveOptionsCurrent.Pdf.Compression)
            {
                case Compression.Deflate:
                    radioButtonPdfDeflate.Checked = true;
                    break;
                case Compression.Group3Fax1d:
                    radioButtonPdfGroup31D.Checked = true;
                    break;
                case Compression.Group3Fax2d:
                    radioButtonPdfGroup32D.Checked = true;
                    break;
                case Compression.Group4:
                    radioButtonPdfGroup4.Checked = true;
                    break;
                case Compression.Jbig2:
                    radioButtonPdfJbig2.Checked = true;
                    break;
                case Compression.Lzw:
                    radioButtonPdfLzw.Checked = true;
                    break;
                case Compression.NoCompression:
                    radioButtonPdfNoCompression.Checked = true;
                    break;
                case Compression.PackBits:
                    radioButtonPdfPackBits.Checked = true;
                    break;
                case Compression.Rle:
                    radioButtonPdfRle.Checked = true;
                    break;
            }
            checkBoxPdfSwapBlackAndWhite.Checked = mySaveOptionsCurrent.Pdf.SwapBlackAndWhite;

            switch (mySaveOptionsCurrent.Jbig2.EncodeModeCompression)
            {
                case Jbig2EncodeModeCompression.AutoDetect:
                    radioButtonAutoDetect.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LosslessGenericMmr:
                    radioButtonLosslessGenericMmr.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LosslessGenericMq:
                    radioButtonLosslessGenericMq.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LosslessTextMmr:
                    radioButtonLosslessTextMmr.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LosslessTextMq:
                    radioButtonLosslessTextMq.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LosslessTextSpmMmr:
                    radioButtonLosslessTextSpmMmr.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LosslessTextSpmMq:
                    radioButtonLosslessTextSpmMq.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LossyHalftoneMmr:
                    radioButtonLossyHalftoneMmr.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LossyHalftoneMq:
                    radioButtonLossyHalftoneMq.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LossyTextMmr:
                    radioButtonLossyTextMmr.Checked = true;
                    break;
                case Jbig2EncodeModeCompression.LossyTextMq:
                    radioButtonLossyTextMq.Checked = true;
                    break;
            }

            switch (mySaveOptionsCurrent.Jbig2.FileOrganization)
            {
                case Jbig2FileOrganization.Default:
                    radioButtonDefault.Checked = true;
                    break;
                case Jbig2FileOrganization.RandomAccess:
                    radioButtonRandomAccess.Checked = true;
                    break;
                case Jbig2FileOrganization.Sequential:
                    radioButtonSequential.Checked = true;
                    break;
            }

            numericUpDownLooseness.Value = mySaveOptionsCurrent.Jbig2.LoosenessCompression;
            checkBoxInvertedRegion.Checked = mySaveOptionsCurrent.Jbig2.InvertedRegion;

            enableUpdate = true;
        }

        #endregion

        #region Close

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            // Cancelled.  If the image was modified, revert the
            // image using its original save options.
            if (imageHasBeenModified == true)
            {
                compressedImage1.CompressBWImage();
                compressedImage2.CompressBWImage();
                compressedImage3.CompressBWImage();
                compressedImage4.CompressBWImage();
                mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsBW().Format, mainForm.GetCompressedImage2().GetSaveOptionsBW().Format, mainForm.GetCompressedImage3().GetSaveOptionsBW().Format, mainForm.GetCompressedImage4().GetSaveOptionsBW().Format);
            }
            imageHasBeenModified = false;
            settingsMatchImage = true;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            // Compress the image using the updated settings.
            // Update the main form's settings, but only compress
            // the main form's image if the settings do not match.
            compressedImage1.UpdateSaveOptionsBW(mySaveOptions1);
            compressedImage2.UpdateSaveOptionsBW(mySaveOptions2);
            compressedImage3.UpdateSaveOptionsBW(mySaveOptions3);
            compressedImage4.UpdateSaveOptionsBW(mySaveOptions4);
            if (settingsMatchImage == false)
            {
                mainForm.UpdateCompressedNames(mySaveOptions1.Format, mySaveOptions2.Format, mySaveOptions3.Format, mySaveOptions4.Format);
                compressedImage1.CompressBWImage();
                compressedImage2.CompressBWImage();
                compressedImage3.CompressBWImage();
                compressedImage4.CompressBWImage();
            }
            imageHasBeenModified = false;
            settingsMatchImage = true;
            this.Close();
        }

        private void SettingsFormBW_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Now that the settings form has closed,
            // enable the main form's settings buttons so that
            // this form can be opened again.
            mainForm.EnableSettings(true);
        }

        #endregion

        #region Update Form Image

        private void UpdateImage()
        {
            if (enableUpdate == true)
            {
                if (checkBoxPreview.Checked)
                {
                    compressedImageCurrent.CompressImage(mySaveOptionsCurrent);
                    // Settings were just applied.
                    settingsMatchImage = true;
                    imageHasBeenModified = true;
                }
                else
                {
                    // Settings have been modified, but were not applied to the form image.
                    settingsMatchImage = false;
                }
            }
        }

        private void UpdateImageFormat()
        {
            // When the settings form is first loaded, this is called when
            // the radio button is set, but it is not necessary to update the image
            // at that time because the format did not change.
            if (enableUpdate == true)
            {
                if (checkBoxPreview.Checked)
                {
                    compressedImageCurrent.CompressImage(mySaveOptionsCurrent);
                    switch (compressedImageCurrent.GetWhichCompressedImage())
                    {
                        case 1:
                            mainForm.UpdateCompressedNames(mySaveOptionsCurrent.Format, mainForm.GetCompressedImage2().GetSaveOptionsBW().Format, mainForm.GetCompressedImage3().GetSaveOptionsBW().Format, mainForm.GetCompressedImage4().GetSaveOptionsBW().Format);
                            break;
                        case 2:
                            mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsBW().Format, mySaveOptionsCurrent.Format, mainForm.GetCompressedImage3().GetSaveOptionsBW().Format, mainForm.GetCompressedImage4().GetSaveOptionsBW().Format);
                            break;
                        case 3:
                            mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsBW().Format, mainForm.GetCompressedImage2().GetSaveOptionsBW().Format, mySaveOptionsCurrent.Format, mainForm.GetCompressedImage4().GetSaveOptionsBW().Format);
                            break;
                        case 4:
                            mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsBW().Format, mainForm.GetCompressedImage2().GetSaveOptionsBW().Format, mainForm.GetCompressedImage3().GetSaveOptionsBW().Format, mySaveOptionsCurrent.Format);
                            break;
                    }
                    // Settings were just applied.
                    settingsMatchImage = true;
                    imageHasBeenModified = true;
                }
                else
                {
                    // Settings have been modified, but were not applied to the form image.
                    settingsMatchImage = false;
                }
            }
        }

        private void checkBoxPreview_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPreview.Checked)
                UpdateImageFormat();
        }

        #endregion

        #region Modify Compression Format

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControlSettings.SelectedIndex)
            {
                case 0:
                    radioButtonTiff.Checked = true;
                    break;
                case 1:
                    radioButtonPdf.Checked = true;
                    break;
                case 2:
                    radioButtonJbig2.Checked = true;
                    break;
                case 3:
                    radioButtonModca.Checked = true;
                    break;
                case 4:
                    radioButtonCals.Checked = true;
                    break;
            }
        }

        private void radioButtonTiff_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiff.Checked == true)
            {
                tabControlSettings.SelectedIndex = 0;
                mySaveOptionsCurrent.Format = ImageXFormat.Tiff;
                UpdateImageFormat();
            }
        }

        private void radioButtonPdf_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdf.Checked == true)
            {
                tabControlSettings.SelectedIndex = 1;
                mySaveOptionsCurrent.Format = ImageXFormat.Pdf;
                UpdateImageFormat();
            }
        }

        private void radioButtonJbig2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJbig2.Checked == true)
            {
                tabControlSettings.SelectedIndex = 2;
                mySaveOptionsCurrent.Format = ImageXFormat.Jbig2;
                UpdateImageFormat();
            }
        }

        private void radioButtonModca_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonModca.Checked == true)
            {
                tabControlSettings.SelectedIndex = 3;
                mySaveOptionsCurrent.Format = ImageXFormat.Modca;
                UpdateImageFormat();
            }
        }

        private void radioButtonCals_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCals.Checked == true)
            {
                tabControlSettings.SelectedIndex = 4;
                mySaveOptionsCurrent.Format = ImageXFormat.Cals;
                UpdateImageFormat();
            }
        }

        #endregion

        #region Modify Tiff Compression Settings

        private void radioButtonTiffNoCompression_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffNoCompression.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.NoCompression;
                UpdateImage();
            }
        }

        private void radioButtonTiffDeflate_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffDeflate.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Deflate;
                UpdateImage();
            }
        }

        private void radioButtonTiffGroup31D_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffGroup31D.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Group3Fax1d;
                UpdateImage();
            }
        }

        private void radioButtonTiffGroup32D_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffGroup32D.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Group3Fax2d;
                UpdateImage();
            }
        }

        private void radioButtonTiffGroup4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffGroup4.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Group4;
                UpdateImage();
            }
        }

        private void radioButtonTiffJbig2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffJbig2.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Jbig2;
                UpdateImage();
            }
        }

        private void radioButtonTiffLzw_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffLzw.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Lzw;
                UpdateImage();
            }
        }

        private void radioButtonTiffPackBits_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffPackBits.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.PackBits;
                UpdateImage();
            }
        }

        private void radioButtonTiffRle_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTiffRle.Checked)
            {
                mySaveOptionsCurrent.Tiff.Compression = Compression.Rle;
                UpdateImage();
            }
        }

        #endregion

        #region Modify Pdf Compression Settings

        private void radioButtonPdfNoCompression_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfNoCompression.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.NoCompression;
                UpdateImage();
            }
        }

        private void radioButtonPdfDeflate_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfDeflate.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.Deflate;
                UpdateImage();
            }
        }

        private void radioButtonPdfGroup31D_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfGroup31D.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.Group3Fax1d;
                UpdateImage();
            }
        }

        private void radioButtonPdfGroup32D_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfGroup32D.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.Group3Fax2d;
                UpdateImage();
            }
        }

        private void radioButtonPdfGroup4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfGroup4.Checked)
            {
                checkBoxPdfSwapBlackAndWhite.Visible = true;
                mySaveOptionsCurrent.Pdf.Compression = Compression.Group4;
                UpdateImage();
            }
            else
                checkBoxPdfSwapBlackAndWhite.Visible = false;
        }

        private void radioButtonPdfJbig2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfJbig2.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.Jbig2;
                UpdateImage();
            }
        }

        private void radioButtonPdfLzw_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfLzw.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.Lzw;
                UpdateImage();
            }
        }

        private void radioButtonPdfPackBits_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfPackBits.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.PackBits;
                UpdateImage();
            }
        }

        private void radioButtonPdfRle_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPdfRle.Checked)
            {
                mySaveOptionsCurrent.Pdf.Compression = Compression.Rle;
                UpdateImage();
            }
        }

        private void checkBoxPdfSwapBlackAndWhite_CheckStateChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Pdf.SwapBlackAndWhite = checkBoxPdfSwapBlackAndWhite.Checked;
            UpdateImage();
        }

        #endregion

        #region Modify Jbig2 Compression Settings

        private void numericUpDownLooseness_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jbig2.LoosenessCompression = (int)numericUpDownLooseness.Value;
            UpdateImage();
        }

        private void radioButtonAutoDetect_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonAutoDetect.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.AutoDetect;
                UpdateImage();
            }
        }

        private void radioButtonLosslessGenericMq_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLosslessGenericMq.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LosslessGenericMmr;
                UpdateImage();
            }
        }

        private void radioButtonLosslessGenericMmr_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLosslessGenericMmr.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LosslessGenericMq;
                UpdateImage();
            }
        }

        private void radioButtonLosslessTextMq_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLosslessTextMq.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextMmr;
                UpdateImage();
            }
        }

        private void radioButtonLosslessTextMmr_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLosslessTextMmr.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextMq;
                UpdateImage();
            }
        }

        private void radioButtonLosslessTextSpmMq_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLosslessTextSpmMq.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextSpmMmr;
                UpdateImage();
            }
        }

        private void radioButtonLosslessTextSpmMmr_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLosslessTextSpmMmr.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LosslessTextSpmMq;
                UpdateImage();
            }
        }

        private void radioButtonLossyTextMq_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLossyTextMq.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LossyHalftoneMmr;
                UpdateImage();
            }
        }

        private void radioButtonLossyTextMmr_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLossyTextMmr.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LossyHalftoneMq;
                UpdateImage();
            }
        }

        private void radioButtonLossyHalftoneMq_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLossyHalftoneMq.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LossyTextMmr;
                UpdateImage();
            }
        }

        private void radioButtonLossyHalftoneMmr_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLossyHalftoneMmr.Checked)
            {
                mySaveOptionsCurrent.Jbig2.EncodeModeCompression = Jbig2EncodeModeCompression.LossyTextMq;
                UpdateImage();
            }
        }

        private void radioButtonDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonDefault.Checked)
            {
                mySaveOptionsCurrent.Jbig2.FileOrganization = Jbig2FileOrganization.Default;
                UpdateImage();
            }
        }

        private void radioButtonRandomAccess_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonRandomAccess.Checked)
            {
                mySaveOptionsCurrent.Jbig2.FileOrganization = Jbig2FileOrganization.RandomAccess;
                UpdateImage();
            }
        }

        private void radioButtonSequential_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonSequential.Checked)
            {
                mySaveOptionsCurrent.Jbig2.FileOrganization = Jbig2FileOrganization.Sequential;
                UpdateImage();
            }
        }

        private void checkBoxInvertedRegion_CheckedChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jbig2.InvertedRegion = checkBoxInvertedRegion.Checked;
            UpdateImage();
        }

        #endregion

        #region Modify Which Compressed Image Settings Apply To

        public void SelectCompressedImage()
        {
            switch (mainForm.GetTabControl().SelectedIndex)
            {
                case 0:
                case 1:
                    radioButtonCompressedImage1.Checked = true;
                    break;
                case 2:
                    radioButtonCompressedImage2.Checked = true;
                    break;
                case 3:
                    radioButtonCompressedImage3.Checked = true;
                    break;
                case 4:
                    radioButtonCompressedImage4.Checked = true;
                    break;
            }
        }

        private void radioButtonCompressedImage1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage1.Checked == true)
            {
                compressedImageCurrent = compressedImage1;
                mySaveOptionsCurrent = mySaveOptions1;
                mainForm.GetTabControl().SelectedIndex = 1;
                DisplaySettings();
            }
        }

        private void radioButtonCompressedImage2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage2.Checked == true)
            {
                compressedImageCurrent = compressedImage2;
                mySaveOptionsCurrent = mySaveOptions2;
                mainForm.GetTabControl().SelectedIndex = 2;
                DisplaySettings();
            }
        }

        private void radioButtonCompressedImage3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage3.Checked == true)
            {
                compressedImageCurrent = compressedImage3;
                mySaveOptionsCurrent = mySaveOptions3;
                mainForm.GetTabControl().SelectedIndex = 3;
                DisplaySettings();
            }
        }

        private void radioButtonCompressedImage4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage4.Checked == true)
            {
                compressedImageCurrent = compressedImage4;
                mySaveOptionsCurrent = mySaveOptions4;
                mainForm.GetTabControl().SelectedIndex = 4;
                DisplaySettings();
            }
        }

        #endregion

    }
}

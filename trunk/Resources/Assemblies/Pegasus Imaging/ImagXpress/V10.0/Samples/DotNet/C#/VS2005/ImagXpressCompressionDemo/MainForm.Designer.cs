namespace ImagXpressCompressionDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCompressionSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCompressionSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelDownload = new System.Windows.Forms.Panel();
            this.panelBackgroundFrameImageInfo = new AccusoftCustom.PanelBackgroundFrame();
            this.tabControlImageInfo = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonOriginalSettings1 = new System.Windows.Forms.Button();
            this.labelCompressionRatioData = new System.Windows.Forms.Label();
            this.labelCompressionRatio = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelWidth = new System.Windows.Forms.Label();
            this.labelWidthData = new System.Windows.Forms.Label();
            this.labelBitDepthData = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.labelBitDepth = new System.Windows.Forms.Label();
            this.labelHeightData = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelT1Data = new System.Windows.Forms.Label();
            this.labelT1 = new System.Windows.Forms.Label();
            this.labelKbps384Data = new System.Windows.Forms.Label();
            this.labelKbps384 = new System.Windows.Forms.Label();
            this.labelKbps56Data = new System.Windows.Forms.Label();
            this.labelKbps56 = new System.Windows.Forms.Label();
            this.labelFilesizeData = new System.Windows.Forms.Label();
            this.labelFilesize = new System.Windows.Forms.Label();
            this.labelFilenameData = new System.Windows.Forms.Label();
            this.labelFilename = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.labelCompressed1CompressionRatioData = new System.Windows.Forms.Label();
            this.labelCompressed1CompressionRatio = new System.Windows.Forms.Label();
            this.buttonSettings1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelCompressed1Width = new System.Windows.Forms.Label();
            this.labelCompressed1WidthData = new System.Windows.Forms.Label();
            this.labelCompressed1BitDepthData = new System.Windows.Forms.Label();
            this.labelCompressed1Height = new System.Windows.Forms.Label();
            this.labelCompressed1BitDepth = new System.Windows.Forms.Label();
            this.labelCompressed1HeightData = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelCompressed1T1Data = new System.Windows.Forms.Label();
            this.labelCompressed1Kbps384Data = new System.Windows.Forms.Label();
            this.labelCompressed1T1 = new System.Windows.Forms.Label();
            this.labelCompressed1Kbps384 = new System.Windows.Forms.Label();
            this.labelCompressed1Kbps56Data = new System.Windows.Forms.Label();
            this.labelCompressed1Kbps56 = new System.Windows.Forms.Label();
            this.labelCompressed1FilesizeData = new System.Windows.Forms.Label();
            this.labelCompressed1Filesize = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.labelCompressed2CompressionRatioData = new System.Windows.Forms.Label();
            this.labelCompressed2CompressionRatio = new System.Windows.Forms.Label();
            this.buttonSettings2 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelCompressed2Width = new System.Windows.Forms.Label();
            this.labelCompressed2WidthData = new System.Windows.Forms.Label();
            this.labelCompressed2BitDepthData = new System.Windows.Forms.Label();
            this.labelCompressed2Height = new System.Windows.Forms.Label();
            this.labelCompressed2BitDepth = new System.Windows.Forms.Label();
            this.labelCompressed2HeightData = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.labelCompressed2T1Data = new System.Windows.Forms.Label();
            this.labelCompressed2Kbps384Data = new System.Windows.Forms.Label();
            this.labelCompressed2T1 = new System.Windows.Forms.Label();
            this.labelCompressed2Kbps384 = new System.Windows.Forms.Label();
            this.labelCompressed2Kbps56Data = new System.Windows.Forms.Label();
            this.labelCompressed2Kbps56 = new System.Windows.Forms.Label();
            this.labelCompressed2FilesizeData = new System.Windows.Forms.Label();
            this.labelCompressed2Filesize = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.labelCompressed3CompressionRatioData = new System.Windows.Forms.Label();
            this.labelCompressed3CompressionRatio = new System.Windows.Forms.Label();
            this.buttonSettings3 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.labelCompressed3Width = new System.Windows.Forms.Label();
            this.labelCompressed3WidthData = new System.Windows.Forms.Label();
            this.labelCompressed3BitDepthData = new System.Windows.Forms.Label();
            this.labelCompressed3Height = new System.Windows.Forms.Label();
            this.labelCompressed3BitDepth = new System.Windows.Forms.Label();
            this.labelCompressed3HeightData = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.labelCompressed3T1Data = new System.Windows.Forms.Label();
            this.labelCompressed3Kbps384Data = new System.Windows.Forms.Label();
            this.labelCompressed3T1 = new System.Windows.Forms.Label();
            this.labelCompressed3Kbps384 = new System.Windows.Forms.Label();
            this.labelCompressed3Kbps56Data = new System.Windows.Forms.Label();
            this.labelCompressed3Kbps56 = new System.Windows.Forms.Label();
            this.labelCompressed3FilesizeData = new System.Windows.Forms.Label();
            this.labelCompressed3Filesize = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.labelCompressed4CompressionRatioData = new System.Windows.Forms.Label();
            this.labelCompressed4CompressionRatio = new System.Windows.Forms.Label();
            this.buttonSettings4 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.labelCompressed4Width = new System.Windows.Forms.Label();
            this.labelCompressed4WidthData = new System.Windows.Forms.Label();
            this.labelCompressed4BitDepthData = new System.Windows.Forms.Label();
            this.labelCompressed4Height = new System.Windows.Forms.Label();
            this.labelCompressed4BitDepth = new System.Windows.Forms.Label();
            this.labelCompressed4HeightData = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.labelCompressed4T1Data = new System.Windows.Forms.Label();
            this.labelCompressed4Kbps384Data = new System.Windows.Forms.Label();
            this.labelCompressed4T1 = new System.Windows.Forms.Label();
            this.labelCompressed4Kbps384 = new System.Windows.Forms.Label();
            this.labelCompressed4Kbps56Data = new System.Windows.Forms.Label();
            this.labelCompressed4Kbps56 = new System.Windows.Forms.Label();
            this.labelCompressed4FilesizeData = new System.Windows.Forms.Label();
            this.labelCompressed4Filesize = new System.Windows.Forms.Label();
            this.panelBackgroundFrame = new AccusoftCustom.PanelBackgroundFrame();
            this.splitContainerImages = new System.Windows.Forms.SplitContainer();
            this.panelGradientNoFlareOriginal = new AccusoftCustom.PanelGradientNoFlare();
            this.originalImageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.panelGradientNoFlareCompressed = new AccusoftCustom.PanelGradientNoFlare();
            this.panelCompressedImage = new System.Windows.Forms.Panel();
            this.compressed1ImageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.compressed4ImageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.compressed3ImageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.labelCompressed2 = new System.Windows.Forms.Label();
            this.labelCompressed1 = new System.Windows.Forms.Label();
            this.labelCompressed3 = new System.Windows.Forms.Label();
            this.labelCompressed4 = new System.Windows.Forms.Label();
            this.compressed2ImageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panelBackgroundFrameImageInfo.SuspendLayout();
            this.tabControlImageInfo.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panelBackgroundFrame.SuspendLayout();
            this.splitContainerImages.Panel1.SuspendLayout();
            this.splitContainerImages.Panel2.SuspendLayout();
            this.splitContainerImages.SuspendLayout();
            this.panelGradientNoFlareOriginal.SuspendLayout();
            this.panelGradientNoFlareCompressed.SuspendLayout();
            this.panelCompressedImage.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(944, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.loadCompressionSettingsToolStripMenuItem,
            this.saveCompressionSettingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.openToolStripMenuItem.Text = "Open Image";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // loadCompressionSettingsToolStripMenuItem
            // 
            this.loadCompressionSettingsToolStripMenuItem.Name = "loadCompressionSettingsToolStripMenuItem";
            this.loadCompressionSettingsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.loadCompressionSettingsToolStripMenuItem.Text = "Load Compression Settings";
            this.loadCompressionSettingsToolStripMenuItem.Click += new System.EventHandler(this.loadCompressionSettingsToolStripMenuItem_Click);
            // 
            // saveCompressionSettingsToolStripMenuItem
            // 
            this.saveCompressionSettingsToolStripMenuItem.Name = "saveCompressionSettingsToolStripMenuItem";
            this.saveCompressionSettingsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.saveCompressionSettingsToolStripMenuItem.Text = "Save Compression Settings";
            this.saveCompressionSettingsToolStripMenuItem.Click += new System.EventHandler(this.saveCompressionSettingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 642);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(944, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel1.Text = "Status";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.Color.Transparent;
            this.panelLogo.BackgroundImage = global::ImagXpressCompressionDemo.Properties.Resources.ap_logo_transparent_153x64;
            this.panelLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelLogo.Location = new System.Drawing.Point(21, 40);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(208, 78);
            this.panelLogo.TabIndex = 26;
            // 
            // panelDownload
            // 
            this.panelDownload.BackColor = System.Drawing.Color.Transparent;
            this.panelDownload.BackgroundImage = global::ImagXpressCompressionDemo.Properties.Resources.btn_download_sdk;
            this.panelDownload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelDownload.Location = new System.Drawing.Point(247, 40);
            this.panelDownload.Name = "panelDownload";
            this.panelDownload.Size = new System.Drawing.Size(157, 46);
            this.panelDownload.TabIndex = 30;
            this.panelDownload.Click += new System.EventHandler(this.panelDownload_Click);
            // 
            // panelBackgroundFrameImageInfo
            // 
            this.panelBackgroundFrameImageInfo.BackColor = System.Drawing.Color.Transparent;
            this.panelBackgroundFrameImageInfo.Controls.Add(this.tabControlImageInfo);
            this.panelBackgroundFrameImageInfo.Location = new System.Drawing.Point(470, 40);
            this.panelBackgroundFrameImageInfo.Name = "panelBackgroundFrameImageInfo";
            this.panelBackgroundFrameImageInfo.Size = new System.Drawing.Size(466, 174);
            this.panelBackgroundFrameImageInfo.TabIndex = 34;
            // 
            // tabControlImageInfo
            // 
            this.tabControlImageInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlImageInfo.Controls.Add(this.tabPage1);
            this.tabControlImageInfo.Controls.Add(this.tabPage2);
            this.tabControlImageInfo.Controls.Add(this.tabPage3);
            this.tabControlImageInfo.Controls.Add(this.tabPage4);
            this.tabControlImageInfo.Controls.Add(this.tabPage5);
            this.tabControlImageInfo.Location = new System.Drawing.Point(18, 18);
            this.tabControlImageInfo.Name = "tabControlImageInfo";
            this.tabControlImageInfo.SelectedIndex = 0;
            this.tabControlImageInfo.Size = new System.Drawing.Size(430, 140);
            this.tabControlImageInfo.TabIndex = 15;
            this.tabControlImageInfo.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.buttonOriginalSettings1);
            this.tabPage1.Controls.Add(this.labelCompressionRatioData);
            this.tabPage1.Controls.Add(this.labelCompressionRatio);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.labelFilesizeData);
            this.tabPage1.Controls.Add(this.labelFilesize);
            this.tabPage1.Controls.Add(this.labelFilenameData);
            this.tabPage1.Controls.Add(this.labelFilename);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(422, 114);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Original Image";
            // 
            // buttonOriginalSettings1
            // 
            this.buttonOriginalSettings1.Location = new System.Drawing.Point(342, 6);
            this.buttonOriginalSettings1.Name = "buttonOriginalSettings1";
            this.buttonOriginalSettings1.Size = new System.Drawing.Size(72, 24);
            this.buttonOriginalSettings1.TabIndex = 19;
            this.buttonOriginalSettings1.Text = "Settings";
            this.buttonOriginalSettings1.UseVisualStyleBackColor = true;
            this.buttonOriginalSettings1.Click += new System.EventHandler(this.buttonSettings1_Click);
            // 
            // labelCompressionRatioData
            // 
            this.labelCompressionRatioData.AutoSize = true;
            this.labelCompressionRatioData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressionRatioData.Location = new System.Drawing.Point(276, 24);
            this.labelCompressionRatioData.Name = "labelCompressionRatioData";
            this.labelCompressionRatioData.Size = new System.Drawing.Size(0, 13);
            this.labelCompressionRatioData.TabIndex = 13;
            // 
            // labelCompressionRatio
            // 
            this.labelCompressionRatio.AutoSize = true;
            this.labelCompressionRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressionRatio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressionRatio.Location = new System.Drawing.Point(158, 24);
            this.labelCompressionRatio.Name = "labelCompressionRatio";
            this.labelCompressionRatio.Size = new System.Drawing.Size(116, 13);
            this.labelCompressionRatio.TabIndex = 12;
            this.labelCompressionRatio.Text = "Compression Ratio:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelWidth);
            this.groupBox2.Controls.Add(this.labelWidthData);
            this.groupBox2.Controls.Add(this.labelBitDepthData);
            this.groupBox2.Controls.Add(this.labelHeight);
            this.groupBox2.Controls.Add(this.labelBitDepth);
            this.groupBox2.Controls.Add(this.labelHeightData);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox2.Location = new System.Drawing.Point(9, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(180, 64);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Image Dimensions";
            // 
            // labelWidth
            // 
            this.labelWidth.AutoSize = true;
            this.labelWidth.Location = new System.Drawing.Point(6, 16);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(44, 13);
            this.labelWidth.TabIndex = 4;
            this.labelWidth.Text = "Width:";
            // 
            // labelWidthData
            // 
            this.labelWidthData.AutoSize = true;
            this.labelWidthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWidthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelWidthData.Location = new System.Drawing.Point(70, 16);
            this.labelWidthData.Name = "labelWidthData";
            this.labelWidthData.Size = new System.Drawing.Size(0, 13);
            this.labelWidthData.TabIndex = 5;
            // 
            // labelBitDepthData
            // 
            this.labelBitDepthData.AutoSize = true;
            this.labelBitDepthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBitDepthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelBitDepthData.Location = new System.Drawing.Point(70, 42);
            this.labelBitDepthData.Name = "labelBitDepthData";
            this.labelBitDepthData.Size = new System.Drawing.Size(0, 13);
            this.labelBitDepthData.TabIndex = 9;
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.Location = new System.Drawing.Point(6, 29);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(48, 13);
            this.labelHeight.TabIndex = 6;
            this.labelHeight.Text = "Height:";
            // 
            // labelBitDepth
            // 
            this.labelBitDepth.AutoSize = true;
            this.labelBitDepth.Location = new System.Drawing.Point(6, 42);
            this.labelBitDepth.Name = "labelBitDepth";
            this.labelBitDepth.Size = new System.Drawing.Size(64, 13);
            this.labelBitDepth.TabIndex = 8;
            this.labelBitDepth.Text = "Bit Depth:";
            // 
            // labelHeightData
            // 
            this.labelHeightData.AutoSize = true;
            this.labelHeightData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeightData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelHeightData.Location = new System.Drawing.Point(70, 29);
            this.labelHeightData.Name = "labelHeightData";
            this.labelHeightData.Size = new System.Drawing.Size(0, 13);
            this.labelHeightData.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelT1Data);
            this.groupBox1.Controls.Add(this.labelT1);
            this.groupBox1.Controls.Add(this.labelKbps384Data);
            this.groupBox1.Controls.Add(this.labelKbps384);
            this.groupBox1.Controls.Add(this.labelKbps56Data);
            this.groupBox1.Controls.Add(this.labelKbps56);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox1.Location = new System.Drawing.Point(206, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 64);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Download Time";
            // 
            // labelT1Data
            // 
            this.labelT1Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelT1Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelT1Data.Location = new System.Drawing.Point(60, 43);
            this.labelT1Data.Name = "labelT1Data";
            this.labelT1Data.Size = new System.Drawing.Size(83, 13);
            this.labelT1Data.TabIndex = 10;
            this.labelT1Data.Text = "2 secs";
            this.labelT1Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelT1
            // 
            this.labelT1.AutoSize = true;
            this.labelT1.Location = new System.Drawing.Point(6, 42);
            this.labelT1.Name = "labelT1";
            this.labelT1.Size = new System.Drawing.Size(26, 13);
            this.labelT1.TabIndex = 9;
            this.labelT1.Text = "T1:";
            // 
            // labelKbps384Data
            // 
            this.labelKbps384Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKbps384Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelKbps384Data.Location = new System.Drawing.Point(60, 30);
            this.labelKbps384Data.Name = "labelKbps384Data";
            this.labelKbps384Data.Size = new System.Drawing.Size(83, 13);
            this.labelKbps384Data.TabIndex = 8;
            this.labelKbps384Data.Text = "10 secs";
            this.labelKbps384Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelKbps384
            // 
            this.labelKbps384.AutoSize = true;
            this.labelKbps384.Location = new System.Drawing.Point(6, 29);
            this.labelKbps384.Name = "labelKbps384";
            this.labelKbps384.Size = new System.Drawing.Size(59, 13);
            this.labelKbps384.TabIndex = 7;
            this.labelKbps384.Text = "384kbps:";
            // 
            // labelKbps56Data
            // 
            this.labelKbps56Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKbps56Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelKbps56Data.Location = new System.Drawing.Point(60, 16);
            this.labelKbps56Data.Name = "labelKbps56Data";
            this.labelKbps56Data.Size = new System.Drawing.Size(83, 13);
            this.labelKbps56Data.TabIndex = 6;
            this.labelKbps56Data.Text = "120 secs";
            this.labelKbps56Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelKbps56
            // 
            this.labelKbps56.AutoSize = true;
            this.labelKbps56.Location = new System.Drawing.Point(6, 16);
            this.labelKbps56.Name = "labelKbps56";
            this.labelKbps56.Size = new System.Drawing.Size(52, 13);
            this.labelKbps56.TabIndex = 5;
            this.labelKbps56.Text = "56kbps:";
            // 
            // labelFilesizeData
            // 
            this.labelFilesizeData.AutoSize = true;
            this.labelFilesizeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelFilesizeData.Location = new System.Drawing.Point(59, 24);
            this.labelFilesizeData.Name = "labelFilesizeData";
            this.labelFilesizeData.Size = new System.Drawing.Size(0, 13);
            this.labelFilesizeData.TabIndex = 3;
            // 
            // labelFilesize
            // 
            this.labelFilesize.AutoSize = true;
            this.labelFilesize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilesize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelFilesize.Location = new System.Drawing.Point(6, 24);
            this.labelFilesize.Name = "labelFilesize";
            this.labelFilesize.Size = new System.Drawing.Size(53, 13);
            this.labelFilesize.TabIndex = 2;
            this.labelFilesize.Text = "Filesize:";
            // 
            // labelFilenameData
            // 
            this.labelFilenameData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelFilenameData.Location = new System.Drawing.Point(66, 6);
            this.labelFilenameData.Name = "labelFilenameData";
            this.labelFilenameData.Size = new System.Drawing.Size(242, 13);
            this.labelFilenameData.TabIndex = 1;
            // 
            // labelFilename
            // 
            this.labelFilename.AutoSize = true;
            this.labelFilename.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFilename.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelFilename.Location = new System.Drawing.Point(6, 6);
            this.labelFilename.Name = "labelFilename";
            this.labelFilename.Size = new System.Drawing.Size(61, 13);
            this.labelFilename.TabIndex = 0;
            this.labelFilename.Text = "Filename:";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.labelCompressed1CompressionRatioData);
            this.tabPage2.Controls.Add(this.labelCompressed1CompressionRatio);
            this.tabPage2.Controls.Add(this.buttonSettings1);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.labelCompressed1FilesizeData);
            this.tabPage2.Controls.Add(this.labelCompressed1Filesize);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(422, 114);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "compressed1";
            // 
            // labelCompressed1CompressionRatioData
            // 
            this.labelCompressed1CompressionRatioData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1CompressionRatioData.Location = new System.Drawing.Point(124, 24);
            this.labelCompressed1CompressionRatioData.Name = "labelCompressed1CompressionRatioData";
            this.labelCompressed1CompressionRatioData.Size = new System.Drawing.Size(60, 13);
            this.labelCompressed1CompressionRatioData.TabIndex = 20;
            // 
            // labelCompressed1CompressionRatio
            // 
            this.labelCompressed1CompressionRatio.AutoSize = true;
            this.labelCompressed1CompressionRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1CompressionRatio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed1CompressionRatio.Location = new System.Drawing.Point(6, 24);
            this.labelCompressed1CompressionRatio.Name = "labelCompressed1CompressionRatio";
            this.labelCompressed1CompressionRatio.Size = new System.Drawing.Size(116, 13);
            this.labelCompressed1CompressionRatio.TabIndex = 19;
            this.labelCompressed1CompressionRatio.Text = "Compression Ratio:";
            // 
            // buttonSettings1
            // 
            this.buttonSettings1.Location = new System.Drawing.Point(342, 6);
            this.buttonSettings1.Name = "buttonSettings1";
            this.buttonSettings1.Size = new System.Drawing.Size(72, 24);
            this.buttonSettings1.TabIndex = 18;
            this.buttonSettings1.Text = "Settings";
            this.buttonSettings1.UseVisualStyleBackColor = true;
            this.buttonSettings1.Click += new System.EventHandler(this.buttonSettings1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelCompressed1Width);
            this.groupBox3.Controls.Add(this.labelCompressed1WidthData);
            this.groupBox3.Controls.Add(this.labelCompressed1BitDepthData);
            this.groupBox3.Controls.Add(this.labelCompressed1Height);
            this.groupBox3.Controls.Add(this.labelCompressed1BitDepth);
            this.groupBox3.Controls.Add(this.labelCompressed1HeightData);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox3.Location = new System.Drawing.Point(9, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 64);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Image Dimensions";
            // 
            // labelCompressed1Width
            // 
            this.labelCompressed1Width.AutoSize = true;
            this.labelCompressed1Width.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed1Width.Name = "labelCompressed1Width";
            this.labelCompressed1Width.Size = new System.Drawing.Size(44, 13);
            this.labelCompressed1Width.TabIndex = 4;
            this.labelCompressed1Width.Text = "Width:";
            // 
            // labelCompressed1WidthData
            // 
            this.labelCompressed1WidthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1WidthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1WidthData.Location = new System.Drawing.Point(70, 16);
            this.labelCompressed1WidthData.Name = "labelCompressed1WidthData";
            this.labelCompressed1WidthData.Size = new System.Drawing.Size(70, 13);
            this.labelCompressed1WidthData.TabIndex = 5;
            // 
            // labelCompressed1BitDepthData
            // 
            this.labelCompressed1BitDepthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1BitDepthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1BitDepthData.Location = new System.Drawing.Point(70, 42);
            this.labelCompressed1BitDepthData.Name = "labelCompressed1BitDepthData";
            this.labelCompressed1BitDepthData.Size = new System.Drawing.Size(40, 13);
            this.labelCompressed1BitDepthData.TabIndex = 9;
            // 
            // labelCompressed1Height
            // 
            this.labelCompressed1Height.AutoSize = true;
            this.labelCompressed1Height.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed1Height.Name = "labelCompressed1Height";
            this.labelCompressed1Height.Size = new System.Drawing.Size(48, 13);
            this.labelCompressed1Height.TabIndex = 6;
            this.labelCompressed1Height.Text = "Height:";
            // 
            // labelCompressed1BitDepth
            // 
            this.labelCompressed1BitDepth.AutoSize = true;
            this.labelCompressed1BitDepth.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed1BitDepth.Name = "labelCompressed1BitDepth";
            this.labelCompressed1BitDepth.Size = new System.Drawing.Size(64, 13);
            this.labelCompressed1BitDepth.TabIndex = 8;
            this.labelCompressed1BitDepth.Text = "Bit Depth:";
            // 
            // labelCompressed1HeightData
            // 
            this.labelCompressed1HeightData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1HeightData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1HeightData.Location = new System.Drawing.Point(70, 29);
            this.labelCompressed1HeightData.Name = "labelCompressed1HeightData";
            this.labelCompressed1HeightData.Size = new System.Drawing.Size(56, 14);
            this.labelCompressed1HeightData.TabIndex = 7;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelCompressed1T1Data);
            this.groupBox4.Controls.Add(this.labelCompressed1Kbps384Data);
            this.groupBox4.Controls.Add(this.labelCompressed1T1);
            this.groupBox4.Controls.Add(this.labelCompressed1Kbps384);
            this.groupBox4.Controls.Add(this.labelCompressed1Kbps56Data);
            this.groupBox4.Controls.Add(this.labelCompressed1Kbps56);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox4.Location = new System.Drawing.Point(206, 42);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(180, 64);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Download Time";
            // 
            // labelCompressed1T1Data
            // 
            this.labelCompressed1T1Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1T1Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1T1Data.Location = new System.Drawing.Point(60, 43);
            this.labelCompressed1T1Data.Name = "labelCompressed1T1Data";
            this.labelCompressed1T1Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed1T1Data.TabIndex = 10;
            this.labelCompressed1T1Data.Text = "<1 sec";
            this.labelCompressed1T1Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed1Kbps384Data
            // 
            this.labelCompressed1Kbps384Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1Kbps384Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1Kbps384Data.Location = new System.Drawing.Point(60, 30);
            this.labelCompressed1Kbps384Data.Name = "labelCompressed1Kbps384Data";
            this.labelCompressed1Kbps384Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed1Kbps384Data.TabIndex = 8;
            this.labelCompressed1Kbps384Data.Text = "1 sec";
            this.labelCompressed1Kbps384Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed1T1
            // 
            this.labelCompressed1T1.AutoSize = true;
            this.labelCompressed1T1.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed1T1.Name = "labelCompressed1T1";
            this.labelCompressed1T1.Size = new System.Drawing.Size(26, 13);
            this.labelCompressed1T1.TabIndex = 9;
            this.labelCompressed1T1.Text = "T1:";
            // 
            // labelCompressed1Kbps384
            // 
            this.labelCompressed1Kbps384.AutoSize = true;
            this.labelCompressed1Kbps384.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed1Kbps384.MaximumSize = new System.Drawing.Size(64, 0);
            this.labelCompressed1Kbps384.Name = "labelCompressed1Kbps384";
            this.labelCompressed1Kbps384.Size = new System.Drawing.Size(59, 13);
            this.labelCompressed1Kbps384.TabIndex = 7;
            this.labelCompressed1Kbps384.Text = "384kbps:";
            // 
            // labelCompressed1Kbps56Data
            // 
            this.labelCompressed1Kbps56Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1Kbps56Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1Kbps56Data.Location = new System.Drawing.Point(60, 16);
            this.labelCompressed1Kbps56Data.Name = "labelCompressed1Kbps56Data";
            this.labelCompressed1Kbps56Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed1Kbps56Data.TabIndex = 6;
            this.labelCompressed1Kbps56Data.Text = "5 secs";
            this.labelCompressed1Kbps56Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed1Kbps56
            // 
            this.labelCompressed1Kbps56.AutoSize = true;
            this.labelCompressed1Kbps56.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed1Kbps56.Name = "labelCompressed1Kbps56";
            this.labelCompressed1Kbps56.Size = new System.Drawing.Size(52, 13);
            this.labelCompressed1Kbps56.TabIndex = 5;
            this.labelCompressed1Kbps56.Text = "56kbps:";
            // 
            // labelCompressed1FilesizeData
            // 
            this.labelCompressed1FilesizeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed1FilesizeData.Location = new System.Drawing.Point(60, 6);
            this.labelCompressed1FilesizeData.Name = "labelCompressed1FilesizeData";
            this.labelCompressed1FilesizeData.Size = new System.Drawing.Size(115, 13);
            this.labelCompressed1FilesizeData.TabIndex = 15;
            // 
            // labelCompressed1Filesize
            // 
            this.labelCompressed1Filesize.AutoSize = true;
            this.labelCompressed1Filesize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed1Filesize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed1Filesize.Location = new System.Drawing.Point(6, 6);
            this.labelCompressed1Filesize.Name = "labelCompressed1Filesize";
            this.labelCompressed1Filesize.Size = new System.Drawing.Size(53, 13);
            this.labelCompressed1Filesize.TabIndex = 14;
            this.labelCompressed1Filesize.Text = "Filesize:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.labelCompressed2CompressionRatioData);
            this.tabPage3.Controls.Add(this.labelCompressed2CompressionRatio);
            this.tabPage3.Controls.Add(this.buttonSettings2);
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.labelCompressed2FilesizeData);
            this.tabPage3.Controls.Add(this.labelCompressed2Filesize);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(422, 114);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "compressed2";
            // 
            // labelCompressed2CompressionRatioData
            // 
            this.labelCompressed2CompressionRatioData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2CompressionRatioData.Location = new System.Drawing.Point(124, 24);
            this.labelCompressed2CompressionRatioData.Name = "labelCompressed2CompressionRatioData";
            this.labelCompressed2CompressionRatioData.Size = new System.Drawing.Size(60, 13);
            this.labelCompressed2CompressionRatioData.TabIndex = 29;
            // 
            // labelCompressed2CompressionRatio
            // 
            this.labelCompressed2CompressionRatio.AutoSize = true;
            this.labelCompressed2CompressionRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2CompressionRatio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed2CompressionRatio.Location = new System.Drawing.Point(6, 24);
            this.labelCompressed2CompressionRatio.Name = "labelCompressed2CompressionRatio";
            this.labelCompressed2CompressionRatio.Size = new System.Drawing.Size(116, 13);
            this.labelCompressed2CompressionRatio.TabIndex = 28;
            this.labelCompressed2CompressionRatio.Text = "Compression Ratio:";
            // 
            // buttonSettings2
            // 
            this.buttonSettings2.Location = new System.Drawing.Point(342, 6);
            this.buttonSettings2.Name = "buttonSettings2";
            this.buttonSettings2.Size = new System.Drawing.Size(72, 24);
            this.buttonSettings2.TabIndex = 27;
            this.buttonSettings2.Text = "Settings";
            this.buttonSettings2.UseVisualStyleBackColor = true;
            this.buttonSettings2.Click += new System.EventHandler(this.buttonSettings2_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelCompressed2Width);
            this.groupBox5.Controls.Add(this.labelCompressed2WidthData);
            this.groupBox5.Controls.Add(this.labelCompressed2BitDepthData);
            this.groupBox5.Controls.Add(this.labelCompressed2Height);
            this.groupBox5.Controls.Add(this.labelCompressed2BitDepth);
            this.groupBox5.Controls.Add(this.labelCompressed2HeightData);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox5.Location = new System.Drawing.Point(9, 42);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(180, 64);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Image Dimensions";
            // 
            // labelCompressed2Width
            // 
            this.labelCompressed2Width.AutoSize = true;
            this.labelCompressed2Width.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed2Width.Name = "labelCompressed2Width";
            this.labelCompressed2Width.Size = new System.Drawing.Size(44, 13);
            this.labelCompressed2Width.TabIndex = 4;
            this.labelCompressed2Width.Text = "Width:";
            // 
            // labelCompressed2WidthData
            // 
            this.labelCompressed2WidthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2WidthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2WidthData.Location = new System.Drawing.Point(70, 16);
            this.labelCompressed2WidthData.Name = "labelCompressed2WidthData";
            this.labelCompressed2WidthData.Size = new System.Drawing.Size(53, 13);
            this.labelCompressed2WidthData.TabIndex = 5;
            // 
            // labelCompressed2BitDepthData
            // 
            this.labelCompressed2BitDepthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2BitDepthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2BitDepthData.Location = new System.Drawing.Point(70, 42);
            this.labelCompressed2BitDepthData.Name = "labelCompressed2BitDepthData";
            this.labelCompressed2BitDepthData.Size = new System.Drawing.Size(44, 13);
            this.labelCompressed2BitDepthData.TabIndex = 9;
            // 
            // labelCompressed2Height
            // 
            this.labelCompressed2Height.AutoSize = true;
            this.labelCompressed2Height.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed2Height.Name = "labelCompressed2Height";
            this.labelCompressed2Height.Size = new System.Drawing.Size(48, 13);
            this.labelCompressed2Height.TabIndex = 6;
            this.labelCompressed2Height.Text = "Height:";
            // 
            // labelCompressed2BitDepth
            // 
            this.labelCompressed2BitDepth.AutoSize = true;
            this.labelCompressed2BitDepth.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed2BitDepth.Name = "labelCompressed2BitDepth";
            this.labelCompressed2BitDepth.Size = new System.Drawing.Size(64, 13);
            this.labelCompressed2BitDepth.TabIndex = 8;
            this.labelCompressed2BitDepth.Text = "Bit Depth:";
            // 
            // labelCompressed2HeightData
            // 
            this.labelCompressed2HeightData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2HeightData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2HeightData.Location = new System.Drawing.Point(70, 29);
            this.labelCompressed2HeightData.Name = "labelCompressed2HeightData";
            this.labelCompressed2HeightData.Size = new System.Drawing.Size(47, 13);
            this.labelCompressed2HeightData.TabIndex = 7;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.labelCompressed2T1Data);
            this.groupBox6.Controls.Add(this.labelCompressed2Kbps384Data);
            this.groupBox6.Controls.Add(this.labelCompressed2T1);
            this.groupBox6.Controls.Add(this.labelCompressed2Kbps384);
            this.groupBox6.Controls.Add(this.labelCompressed2Kbps56Data);
            this.groupBox6.Controls.Add(this.labelCompressed2Kbps56);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox6.Location = new System.Drawing.Point(206, 42);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(180, 64);
            this.groupBox6.TabIndex = 25;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Download Time";
            // 
            // labelCompressed2T1Data
            // 
            this.labelCompressed2T1Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2T1Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2T1Data.Location = new System.Drawing.Point(60, 43);
            this.labelCompressed2T1Data.Name = "labelCompressed2T1Data";
            this.labelCompressed2T1Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed2T1Data.TabIndex = 10;
            this.labelCompressed2T1Data.Text = "<1 sec";
            this.labelCompressed2T1Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed2Kbps384Data
            // 
            this.labelCompressed2Kbps384Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2Kbps384Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2Kbps384Data.Location = new System.Drawing.Point(60, 30);
            this.labelCompressed2Kbps384Data.Name = "labelCompressed2Kbps384Data";
            this.labelCompressed2Kbps384Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed2Kbps384Data.TabIndex = 8;
            this.labelCompressed2Kbps384Data.Text = "1 sec";
            this.labelCompressed2Kbps384Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed2T1
            // 
            this.labelCompressed2T1.AutoSize = true;
            this.labelCompressed2T1.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed2T1.Name = "labelCompressed2T1";
            this.labelCompressed2T1.Size = new System.Drawing.Size(26, 13);
            this.labelCompressed2T1.TabIndex = 9;
            this.labelCompressed2T1.Text = "T1:";
            // 
            // labelCompressed2Kbps384
            // 
            this.labelCompressed2Kbps384.AutoSize = true;
            this.labelCompressed2Kbps384.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed2Kbps384.Name = "labelCompressed2Kbps384";
            this.labelCompressed2Kbps384.Size = new System.Drawing.Size(59, 13);
            this.labelCompressed2Kbps384.TabIndex = 7;
            this.labelCompressed2Kbps384.Text = "384kbps:";
            // 
            // labelCompressed2Kbps56Data
            // 
            this.labelCompressed2Kbps56Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2Kbps56Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2Kbps56Data.Location = new System.Drawing.Point(60, 16);
            this.labelCompressed2Kbps56Data.Name = "labelCompressed2Kbps56Data";
            this.labelCompressed2Kbps56Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed2Kbps56Data.TabIndex = 6;
            this.labelCompressed2Kbps56Data.Text = "5 secs";
            this.labelCompressed2Kbps56Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed2Kbps56
            // 
            this.labelCompressed2Kbps56.AutoSize = true;
            this.labelCompressed2Kbps56.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed2Kbps56.Name = "labelCompressed2Kbps56";
            this.labelCompressed2Kbps56.Size = new System.Drawing.Size(52, 13);
            this.labelCompressed2Kbps56.TabIndex = 5;
            this.labelCompressed2Kbps56.Text = "56kbps:";
            // 
            // labelCompressed2FilesizeData
            // 
            this.labelCompressed2FilesizeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed2FilesizeData.Location = new System.Drawing.Point(60, 6);
            this.labelCompressed2FilesizeData.Name = "labelCompressed2FilesizeData";
            this.labelCompressed2FilesizeData.Size = new System.Drawing.Size(115, 13);
            this.labelCompressed2FilesizeData.TabIndex = 24;
            // 
            // labelCompressed2Filesize
            // 
            this.labelCompressed2Filesize.AutoSize = true;
            this.labelCompressed2Filesize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed2Filesize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed2Filesize.Location = new System.Drawing.Point(6, 6);
            this.labelCompressed2Filesize.Name = "labelCompressed2Filesize";
            this.labelCompressed2Filesize.Size = new System.Drawing.Size(53, 13);
            this.labelCompressed2Filesize.TabIndex = 23;
            this.labelCompressed2Filesize.Text = "Filesize:";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.White;
            this.tabPage4.Controls.Add(this.labelCompressed3CompressionRatioData);
            this.tabPage4.Controls.Add(this.labelCompressed3CompressionRatio);
            this.tabPage4.Controls.Add(this.buttonSettings3);
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Controls.Add(this.groupBox8);
            this.tabPage4.Controls.Add(this.labelCompressed3FilesizeData);
            this.tabPage4.Controls.Add(this.labelCompressed3Filesize);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(422, 114);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "compressed3";
            // 
            // labelCompressed3CompressionRatioData
            // 
            this.labelCompressed3CompressionRatioData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3CompressionRatioData.Location = new System.Drawing.Point(124, 24);
            this.labelCompressed3CompressionRatioData.Name = "labelCompressed3CompressionRatioData";
            this.labelCompressed3CompressionRatioData.Size = new System.Drawing.Size(60, 13);
            this.labelCompressed3CompressionRatioData.TabIndex = 29;
            // 
            // labelCompressed3CompressionRatio
            // 
            this.labelCompressed3CompressionRatio.AutoSize = true;
            this.labelCompressed3CompressionRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3CompressionRatio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed3CompressionRatio.Location = new System.Drawing.Point(6, 24);
            this.labelCompressed3CompressionRatio.Name = "labelCompressed3CompressionRatio";
            this.labelCompressed3CompressionRatio.Size = new System.Drawing.Size(116, 13);
            this.labelCompressed3CompressionRatio.TabIndex = 28;
            this.labelCompressed3CompressionRatio.Text = "Compression Ratio:";
            // 
            // buttonSettings3
            // 
            this.buttonSettings3.Location = new System.Drawing.Point(342, 6);
            this.buttonSettings3.Name = "buttonSettings3";
            this.buttonSettings3.Size = new System.Drawing.Size(72, 24);
            this.buttonSettings3.TabIndex = 27;
            this.buttonSettings3.Text = "Settings";
            this.buttonSettings3.UseVisualStyleBackColor = true;
            this.buttonSettings3.Click += new System.EventHandler(this.buttonSettings3_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.labelCompressed3Width);
            this.groupBox7.Controls.Add(this.labelCompressed3WidthData);
            this.groupBox7.Controls.Add(this.labelCompressed3BitDepthData);
            this.groupBox7.Controls.Add(this.labelCompressed3Height);
            this.groupBox7.Controls.Add(this.labelCompressed3BitDepth);
            this.groupBox7.Controls.Add(this.labelCompressed3HeightData);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox7.Location = new System.Drawing.Point(9, 42);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(180, 64);
            this.groupBox7.TabIndex = 26;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Image Dimensions";
            // 
            // labelCompressed3Width
            // 
            this.labelCompressed3Width.AutoSize = true;
            this.labelCompressed3Width.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed3Width.Name = "labelCompressed3Width";
            this.labelCompressed3Width.Size = new System.Drawing.Size(44, 13);
            this.labelCompressed3Width.TabIndex = 4;
            this.labelCompressed3Width.Text = "Width:";
            // 
            // labelCompressed3WidthData
            // 
            this.labelCompressed3WidthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3WidthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3WidthData.Location = new System.Drawing.Point(70, 16);
            this.labelCompressed3WidthData.Name = "labelCompressed3WidthData";
            this.labelCompressed3WidthData.Size = new System.Drawing.Size(62, 13);
            this.labelCompressed3WidthData.TabIndex = 5;
            // 
            // labelCompressed3BitDepthData
            // 
            this.labelCompressed3BitDepthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3BitDepthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3BitDepthData.Location = new System.Drawing.Point(70, 42);
            this.labelCompressed3BitDepthData.Name = "labelCompressed3BitDepthData";
            this.labelCompressed3BitDepthData.Size = new System.Drawing.Size(40, 13);
            this.labelCompressed3BitDepthData.TabIndex = 9;
            // 
            // labelCompressed3Height
            // 
            this.labelCompressed3Height.AutoSize = true;
            this.labelCompressed3Height.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed3Height.Name = "labelCompressed3Height";
            this.labelCompressed3Height.Size = new System.Drawing.Size(48, 13);
            this.labelCompressed3Height.TabIndex = 6;
            this.labelCompressed3Height.Text = "Height:";
            // 
            // labelCompressed3BitDepth
            // 
            this.labelCompressed3BitDepth.AutoSize = true;
            this.labelCompressed3BitDepth.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed3BitDepth.Name = "labelCompressed3BitDepth";
            this.labelCompressed3BitDepth.Size = new System.Drawing.Size(64, 13);
            this.labelCompressed3BitDepth.TabIndex = 8;
            this.labelCompressed3BitDepth.Text = "Bit Depth:";
            // 
            // labelCompressed3HeightData
            // 
            this.labelCompressed3HeightData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3HeightData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3HeightData.Location = new System.Drawing.Point(70, 29);
            this.labelCompressed3HeightData.Name = "labelCompressed3HeightData";
            this.labelCompressed3HeightData.Size = new System.Drawing.Size(56, 14);
            this.labelCompressed3HeightData.TabIndex = 7;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.labelCompressed3T1Data);
            this.groupBox8.Controls.Add(this.labelCompressed3Kbps384Data);
            this.groupBox8.Controls.Add(this.labelCompressed3T1);
            this.groupBox8.Controls.Add(this.labelCompressed3Kbps384);
            this.groupBox8.Controls.Add(this.labelCompressed3Kbps56Data);
            this.groupBox8.Controls.Add(this.labelCompressed3Kbps56);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox8.Location = new System.Drawing.Point(206, 42);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(180, 64);
            this.groupBox8.TabIndex = 25;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Download Time";
            // 
            // labelCompressed3T1Data
            // 
            this.labelCompressed3T1Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3T1Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3T1Data.Location = new System.Drawing.Point(60, 43);
            this.labelCompressed3T1Data.Name = "labelCompressed3T1Data";
            this.labelCompressed3T1Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed3T1Data.TabIndex = 10;
            this.labelCompressed3T1Data.Text = "<1 sec";
            this.labelCompressed3T1Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed3Kbps384Data
            // 
            this.labelCompressed3Kbps384Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3Kbps384Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3Kbps384Data.Location = new System.Drawing.Point(60, 30);
            this.labelCompressed3Kbps384Data.Name = "labelCompressed3Kbps384Data";
            this.labelCompressed3Kbps384Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed3Kbps384Data.TabIndex = 8;
            this.labelCompressed3Kbps384Data.Text = "1 sec";
            this.labelCompressed3Kbps384Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed3T1
            // 
            this.labelCompressed3T1.AutoSize = true;
            this.labelCompressed3T1.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed3T1.Name = "labelCompressed3T1";
            this.labelCompressed3T1.Size = new System.Drawing.Size(26, 13);
            this.labelCompressed3T1.TabIndex = 9;
            this.labelCompressed3T1.Text = "T1:";
            // 
            // labelCompressed3Kbps384
            // 
            this.labelCompressed3Kbps384.AutoSize = true;
            this.labelCompressed3Kbps384.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed3Kbps384.Name = "labelCompressed3Kbps384";
            this.labelCompressed3Kbps384.Size = new System.Drawing.Size(59, 13);
            this.labelCompressed3Kbps384.TabIndex = 7;
            this.labelCompressed3Kbps384.Text = "384kbps:";
            // 
            // labelCompressed3Kbps56Data
            // 
            this.labelCompressed3Kbps56Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3Kbps56Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3Kbps56Data.Location = new System.Drawing.Point(60, 16);
            this.labelCompressed3Kbps56Data.Name = "labelCompressed3Kbps56Data";
            this.labelCompressed3Kbps56Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed3Kbps56Data.TabIndex = 6;
            this.labelCompressed3Kbps56Data.Text = "5 secs";
            this.labelCompressed3Kbps56Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed3Kbps56
            // 
            this.labelCompressed3Kbps56.AutoSize = true;
            this.labelCompressed3Kbps56.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed3Kbps56.Name = "labelCompressed3Kbps56";
            this.labelCompressed3Kbps56.Size = new System.Drawing.Size(52, 13);
            this.labelCompressed3Kbps56.TabIndex = 5;
            this.labelCompressed3Kbps56.Text = "56kbps:";
            // 
            // labelCompressed3FilesizeData
            // 
            this.labelCompressed3FilesizeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed3FilesizeData.Location = new System.Drawing.Point(60, 6);
            this.labelCompressed3FilesizeData.Name = "labelCompressed3FilesizeData";
            this.labelCompressed3FilesizeData.Size = new System.Drawing.Size(115, 13);
            this.labelCompressed3FilesizeData.TabIndex = 24;
            // 
            // labelCompressed3Filesize
            // 
            this.labelCompressed3Filesize.AutoSize = true;
            this.labelCompressed3Filesize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed3Filesize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed3Filesize.Location = new System.Drawing.Point(6, 6);
            this.labelCompressed3Filesize.Name = "labelCompressed3Filesize";
            this.labelCompressed3Filesize.Size = new System.Drawing.Size(53, 13);
            this.labelCompressed3Filesize.TabIndex = 23;
            this.labelCompressed3Filesize.Text = "Filesize:";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.White;
            this.tabPage5.Controls.Add(this.labelCompressed4CompressionRatioData);
            this.tabPage5.Controls.Add(this.labelCompressed4CompressionRatio);
            this.tabPage5.Controls.Add(this.buttonSettings4);
            this.tabPage5.Controls.Add(this.groupBox9);
            this.tabPage5.Controls.Add(this.groupBox10);
            this.tabPage5.Controls.Add(this.labelCompressed4FilesizeData);
            this.tabPage5.Controls.Add(this.labelCompressed4Filesize);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(422, 114);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "compressed4";
            // 
            // labelCompressed4CompressionRatioData
            // 
            this.labelCompressed4CompressionRatioData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4CompressionRatioData.Location = new System.Drawing.Point(124, 24);
            this.labelCompressed4CompressionRatioData.Name = "labelCompressed4CompressionRatioData";
            this.labelCompressed4CompressionRatioData.Size = new System.Drawing.Size(60, 13);
            this.labelCompressed4CompressionRatioData.TabIndex = 29;
            // 
            // labelCompressed4CompressionRatio
            // 
            this.labelCompressed4CompressionRatio.AutoSize = true;
            this.labelCompressed4CompressionRatio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4CompressionRatio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed4CompressionRatio.Location = new System.Drawing.Point(6, 24);
            this.labelCompressed4CompressionRatio.Name = "labelCompressed4CompressionRatio";
            this.labelCompressed4CompressionRatio.Size = new System.Drawing.Size(116, 13);
            this.labelCompressed4CompressionRatio.TabIndex = 28;
            this.labelCompressed4CompressionRatio.Text = "Compression Ratio:";
            // 
            // buttonSettings4
            // 
            this.buttonSettings4.Location = new System.Drawing.Point(342, 6);
            this.buttonSettings4.Name = "buttonSettings4";
            this.buttonSettings4.Size = new System.Drawing.Size(72, 24);
            this.buttonSettings4.TabIndex = 27;
            this.buttonSettings4.Text = "Settings";
            this.buttonSettings4.UseVisualStyleBackColor = true;
            this.buttonSettings4.Click += new System.EventHandler(this.buttonSettings4_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.labelCompressed4Width);
            this.groupBox9.Controls.Add(this.labelCompressed4WidthData);
            this.groupBox9.Controls.Add(this.labelCompressed4BitDepthData);
            this.groupBox9.Controls.Add(this.labelCompressed4Height);
            this.groupBox9.Controls.Add(this.labelCompressed4BitDepth);
            this.groupBox9.Controls.Add(this.labelCompressed4HeightData);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox9.Location = new System.Drawing.Point(9, 42);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(180, 64);
            this.groupBox9.TabIndex = 26;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Image Dimensions";
            // 
            // labelCompressed4Width
            // 
            this.labelCompressed4Width.AutoSize = true;
            this.labelCompressed4Width.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed4Width.Name = "labelCompressed4Width";
            this.labelCompressed4Width.Size = new System.Drawing.Size(44, 13);
            this.labelCompressed4Width.TabIndex = 4;
            this.labelCompressed4Width.Text = "Width:";
            // 
            // labelCompressed4WidthData
            // 
            this.labelCompressed4WidthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4WidthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4WidthData.Location = new System.Drawing.Point(70, 16);
            this.labelCompressed4WidthData.Name = "labelCompressed4WidthData";
            this.labelCompressed4WidthData.Size = new System.Drawing.Size(62, 13);
            this.labelCompressed4WidthData.TabIndex = 5;
            // 
            // labelCompressed4BitDepthData
            // 
            this.labelCompressed4BitDepthData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4BitDepthData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4BitDepthData.Location = new System.Drawing.Point(70, 42);
            this.labelCompressed4BitDepthData.Name = "labelCompressed4BitDepthData";
            this.labelCompressed4BitDepthData.Size = new System.Drawing.Size(40, 14);
            this.labelCompressed4BitDepthData.TabIndex = 9;
            // 
            // labelCompressed4Height
            // 
            this.labelCompressed4Height.AutoSize = true;
            this.labelCompressed4Height.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed4Height.Name = "labelCompressed4Height";
            this.labelCompressed4Height.Size = new System.Drawing.Size(48, 13);
            this.labelCompressed4Height.TabIndex = 6;
            this.labelCompressed4Height.Text = "Height:";
            // 
            // labelCompressed4BitDepth
            // 
            this.labelCompressed4BitDepth.AutoSize = true;
            this.labelCompressed4BitDepth.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed4BitDepth.Name = "labelCompressed4BitDepth";
            this.labelCompressed4BitDepth.Size = new System.Drawing.Size(64, 13);
            this.labelCompressed4BitDepth.TabIndex = 8;
            this.labelCompressed4BitDepth.Text = "Bit Depth:";
            // 
            // labelCompressed4HeightData
            // 
            this.labelCompressed4HeightData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4HeightData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4HeightData.Location = new System.Drawing.Point(70, 29);
            this.labelCompressed4HeightData.Name = "labelCompressed4HeightData";
            this.labelCompressed4HeightData.Size = new System.Drawing.Size(56, 13);
            this.labelCompressed4HeightData.TabIndex = 7;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.labelCompressed4T1Data);
            this.groupBox10.Controls.Add(this.labelCompressed4Kbps384Data);
            this.groupBox10.Controls.Add(this.labelCompressed4T1);
            this.groupBox10.Controls.Add(this.labelCompressed4Kbps384);
            this.groupBox10.Controls.Add(this.labelCompressed4Kbps56Data);
            this.groupBox10.Controls.Add(this.labelCompressed4Kbps56);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.groupBox10.Location = new System.Drawing.Point(206, 42);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(180, 64);
            this.groupBox10.TabIndex = 25;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Download Time";
            // 
            // labelCompressed4T1Data
            // 
            this.labelCompressed4T1Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4T1Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4T1Data.Location = new System.Drawing.Point(60, 43);
            this.labelCompressed4T1Data.Name = "labelCompressed4T1Data";
            this.labelCompressed4T1Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed4T1Data.TabIndex = 10;
            this.labelCompressed4T1Data.Text = "<1 sec";
            this.labelCompressed4T1Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed4Kbps384Data
            // 
            this.labelCompressed4Kbps384Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4Kbps384Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4Kbps384Data.Location = new System.Drawing.Point(60, 30);
            this.labelCompressed4Kbps384Data.Name = "labelCompressed4Kbps384Data";
            this.labelCompressed4Kbps384Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed4Kbps384Data.TabIndex = 8;
            this.labelCompressed4Kbps384Data.Text = "1 sec";
            this.labelCompressed4Kbps384Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed4T1
            // 
            this.labelCompressed4T1.AutoSize = true;
            this.labelCompressed4T1.Location = new System.Drawing.Point(6, 42);
            this.labelCompressed4T1.Name = "labelCompressed4T1";
            this.labelCompressed4T1.Size = new System.Drawing.Size(26, 13);
            this.labelCompressed4T1.TabIndex = 9;
            this.labelCompressed4T1.Text = "T1:";
            // 
            // labelCompressed4Kbps384
            // 
            this.labelCompressed4Kbps384.AutoSize = true;
            this.labelCompressed4Kbps384.Location = new System.Drawing.Point(6, 29);
            this.labelCompressed4Kbps384.Name = "labelCompressed4Kbps384";
            this.labelCompressed4Kbps384.Size = new System.Drawing.Size(59, 13);
            this.labelCompressed4Kbps384.TabIndex = 7;
            this.labelCompressed4Kbps384.Text = "384kbps:";
            // 
            // labelCompressed4Kbps56Data
            // 
            this.labelCompressed4Kbps56Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4Kbps56Data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4Kbps56Data.Location = new System.Drawing.Point(60, 16);
            this.labelCompressed4Kbps56Data.Name = "labelCompressed4Kbps56Data";
            this.labelCompressed4Kbps56Data.Size = new System.Drawing.Size(83, 13);
            this.labelCompressed4Kbps56Data.TabIndex = 6;
            this.labelCompressed4Kbps56Data.Text = "5 secs";
            this.labelCompressed4Kbps56Data.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelCompressed4Kbps56
            // 
            this.labelCompressed4Kbps56.AutoSize = true;
            this.labelCompressed4Kbps56.Location = new System.Drawing.Point(6, 16);
            this.labelCompressed4Kbps56.Name = "labelCompressed4Kbps56";
            this.labelCompressed4Kbps56.Size = new System.Drawing.Size(52, 13);
            this.labelCompressed4Kbps56.TabIndex = 5;
            this.labelCompressed4Kbps56.Text = "56kbps:";
            // 
            // labelCompressed4FilesizeData
            // 
            this.labelCompressed4FilesizeData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(60)))), ((int)(((byte)(52)))));
            this.labelCompressed4FilesizeData.Location = new System.Drawing.Point(60, 6);
            this.labelCompressed4FilesizeData.Name = "labelCompressed4FilesizeData";
            this.labelCompressed4FilesizeData.Size = new System.Drawing.Size(115, 13);
            this.labelCompressed4FilesizeData.TabIndex = 24;
            // 
            // labelCompressed4Filesize
            // 
            this.labelCompressed4Filesize.AutoSize = true;
            this.labelCompressed4Filesize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompressed4Filesize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(118)))), ((int)(((byte)(0)))));
            this.labelCompressed4Filesize.Location = new System.Drawing.Point(6, 6);
            this.labelCompressed4Filesize.Name = "labelCompressed4Filesize";
            this.labelCompressed4Filesize.Size = new System.Drawing.Size(53, 13);
            this.labelCompressed4Filesize.TabIndex = 23;
            this.labelCompressed4Filesize.Text = "Filesize:";
            // 
            // panelBackgroundFrame
            // 
            this.panelBackgroundFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBackgroundFrame.BackColor = System.Drawing.Color.Transparent;
            this.panelBackgroundFrame.Controls.Add(this.splitContainerImages);
            this.panelBackgroundFrame.Location = new System.Drawing.Point(12, 220);
            this.panelBackgroundFrame.Name = "panelBackgroundFrame";
            this.panelBackgroundFrame.Size = new System.Drawing.Size(924, 404);
            this.panelBackgroundFrame.TabIndex = 33;
            this.panelBackgroundFrame.Resize += new System.EventHandler(this.panelBackgroundFrame_Resize);
            // 
            // splitContainerImages
            // 
            this.splitContainerImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerImages.BackColor = System.Drawing.Color.Transparent;
            this.splitContainerImages.Location = new System.Drawing.Point(19, 14);
            this.splitContainerImages.Name = "splitContainerImages";
            // 
            // splitContainerImages.Panel1
            // 
            this.splitContainerImages.Panel1.Controls.Add(this.panelGradientNoFlareOriginal);
            // 
            // splitContainerImages.Panel2
            // 
            this.splitContainerImages.Panel2.Controls.Add(this.panelGradientNoFlareCompressed);
            this.splitContainerImages.Size = new System.Drawing.Size(890, 376);
            this.splitContainerImages.SplitterDistance = 438;
            this.splitContainerImages.TabIndex = 34;
            // 
            // panelGradientNoFlareOriginal
            // 
            this.panelGradientNoFlareOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelGradientNoFlareOriginal.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlareOriginal.Controls.Add(this.originalImageXView);
            this.panelGradientNoFlareOriginal.Location = new System.Drawing.Point(0, 3);
            this.panelGradientNoFlareOriginal.Name = "panelGradientNoFlareOriginal";
            this.panelGradientNoFlareOriginal.Size = new System.Drawing.Size(436, 376);
            this.panelGradientNoFlareOriginal.TabIndex = 33;
            // 
            // originalImageXView
            // 
            this.originalImageXView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.originalImageXView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.originalImageXView.Location = new System.Drawing.Point(12, 12);
            this.originalImageXView.Margin = new System.Windows.Forms.Padding(0);
            this.originalImageXView.Name = "originalImageXView";
            this.originalImageXView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.originalImageXView.Size = new System.Drawing.Size(412, 352);
            this.originalImageXView.TabIndex = 0;
            this.originalImageXView.ScrollEvent += new Accusoft.ImagXpressSdk.ImageXView.ScrollEventHandler(this.originalImageXView_ScrollEvent);
            this.originalImageXView.AutoResizeChanged += new Accusoft.ImagXpressSdk.ImageXView.AutoResizeChangedEventHandler(this.originalImageXView_AutoResizeChanged);
            this.originalImageXView.DragDrop += new System.Windows.Forms.DragEventHandler(this.originalImageXView_DragDrop);
            this.originalImageXView.DragEnter += new System.Windows.Forms.DragEventHandler(this.originalImageXView_DragEnter);
            this.originalImageXView.Resize += new System.EventHandler(this.originalImageXView_Resize);
            this.originalImageXView.ZoomFactorChanged += new Accusoft.ImagXpressSdk.ImageXView.ZoomFactorChangedEventHandler(this.originalImageXView_ZoomFactorChanged);
            // 
            // panelGradientNoFlareCompressed
            // 
            this.panelGradientNoFlareCompressed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelGradientNoFlareCompressed.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlareCompressed.Controls.Add(this.panelCompressedImage);
            this.panelGradientNoFlareCompressed.Location = new System.Drawing.Point(7, 3);
            this.panelGradientNoFlareCompressed.Name = "panelGradientNoFlareCompressed";
            this.panelGradientNoFlareCompressed.Size = new System.Drawing.Size(436, 376);
            this.panelGradientNoFlareCompressed.TabIndex = 33;
            // 
            // panelCompressedImage
            // 
            this.panelCompressedImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCompressedImage.BackColor = System.Drawing.Color.White;
            this.panelCompressedImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCompressedImage.Controls.Add(this.compressed1ImageXView);
            this.panelCompressedImage.Controls.Add(this.compressed4ImageXView);
            this.panelCompressedImage.Controls.Add(this.compressed3ImageXView);
            this.panelCompressedImage.Controls.Add(this.labelCompressed2);
            this.panelCompressedImage.Controls.Add(this.labelCompressed1);
            this.panelCompressedImage.Controls.Add(this.labelCompressed3);
            this.panelCompressedImage.Controls.Add(this.labelCompressed4);
            this.panelCompressedImage.Controls.Add(this.compressed2ImageXView);
            this.panelCompressedImage.Location = new System.Drawing.Point(12, 12);
            this.panelCompressedImage.Name = "panelCompressedImage";
            this.panelCompressedImage.Size = new System.Drawing.Size(412, 352);
            this.panelCompressedImage.TabIndex = 35;
            // 
            // compressed1ImageXView
            // 
            this.compressed1ImageXView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.compressed1ImageXView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.compressed1ImageXView.Location = new System.Drawing.Point(18, 8);
            this.compressed1ImageXView.Name = "compressed1ImageXView";
            this.compressed1ImageXView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.compressed1ImageXView.Size = new System.Drawing.Size(176, 136);
            this.compressed1ImageXView.TabIndex = 8;
            this.compressed1ImageXView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.compressed1ImageXView_MouseClick);
            // 
            // compressed4ImageXView
            // 
            this.compressed4ImageXView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.compressed4ImageXView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.compressed4ImageXView.Location = new System.Drawing.Point(218, 181);
            this.compressed4ImageXView.Name = "compressed4ImageXView";
            this.compressed4ImageXView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.compressed4ImageXView.Size = new System.Drawing.Size(176, 136);
            this.compressed4ImageXView.TabIndex = 11;
            this.compressed4ImageXView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.compressed4ImageXView_MouseClick);
            // 
            // compressed3ImageXView
            // 
            this.compressed3ImageXView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.compressed3ImageXView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.compressed3ImageXView.Location = new System.Drawing.Point(18, 181);
            this.compressed3ImageXView.Name = "compressed3ImageXView";
            this.compressed3ImageXView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.compressed3ImageXView.Size = new System.Drawing.Size(176, 136);
            this.compressed3ImageXView.TabIndex = 10;
            this.compressed3ImageXView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.compressed3ImageXView_MouseClick);
            // 
            // labelCompressed2
            // 
            this.labelCompressed2.Location = new System.Drawing.Point(218, 160);
            this.labelCompressed2.Name = "labelCompressed2";
            this.labelCompressed2.Size = new System.Drawing.Size(172, 16);
            this.labelCompressed2.TabIndex = 13;
            this.labelCompressed2.Text = "compressed2";
            this.labelCompressed2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCompressed1
            // 
            this.labelCompressed1.Location = new System.Drawing.Point(18, 160);
            this.labelCompressed1.Name = "labelCompressed1";
            this.labelCompressed1.Size = new System.Drawing.Size(172, 16);
            this.labelCompressed1.TabIndex = 12;
            this.labelCompressed1.Text = "compressed1";
            this.labelCompressed1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCompressed3
            // 
            this.labelCompressed3.Location = new System.Drawing.Point(18, 333);
            this.labelCompressed3.Name = "labelCompressed3";
            this.labelCompressed3.Size = new System.Drawing.Size(172, 16);
            this.labelCompressed3.TabIndex = 14;
            this.labelCompressed3.Text = "compressed3";
            this.labelCompressed3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCompressed4
            // 
            this.labelCompressed4.Location = new System.Drawing.Point(218, 333);
            this.labelCompressed4.Name = "labelCompressed4";
            this.labelCompressed4.Size = new System.Drawing.Size(172, 16);
            this.labelCompressed4.TabIndex = 15;
            this.labelCompressed4.Text = "compressed4";
            this.labelCompressed4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // compressed2ImageXView
            // 
            this.compressed2ImageXView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.compressed2ImageXView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.compressed2ImageXView.Location = new System.Drawing.Point(218, 8);
            this.compressed2ImageXView.Name = "compressed2ImageXView";
            this.compressed2ImageXView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.compressed2ImageXView.Size = new System.Drawing.Size(176, 136);
            this.compressed2ImageXView.TabIndex = 9;
            this.compressed2ImageXView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.compressed2ImageXView_MouseClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ImagXpressCompressionDemo.Properties.Resources.ap_dark_stripes;
            this.ClientSize = new System.Drawing.Size(944, 664);
            this.Controls.Add(this.panelBackgroundFrameImageInfo);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelLogo);
            this.Controls.Add(this.panelDownload);
            this.Controls.Add(this.panelBackgroundFrame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImagXpress 10 Compression Demo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panelBackgroundFrameImageInfo.ResumeLayout(false);
            this.tabControlImageInfo.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panelBackgroundFrame.ResumeLayout(false);
            this.splitContainerImages.Panel1.ResumeLayout(false);
            this.splitContainerImages.Panel2.ResumeLayout(false);
            this.splitContainerImages.ResumeLayout(false);
            this.panelGradientNoFlareOriginal.ResumeLayout(false);
            this.panelGradientNoFlareCompressed.ResumeLayout(false);
            this.panelCompressedImage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem loadCompressionSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveCompressionSettingsToolStripMenuItem;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private Accusoft.ImagXpressSdk.ImageXView originalImageXView;
        private System.Windows.Forms.Panel panelDownload;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrame;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlareOriginal;
        private System.Windows.Forms.TabControl tabControlImageInfo;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button buttonOriginalSettings1;
        private System.Windows.Forms.Label labelCompressionRatioData;
        private System.Windows.Forms.Label labelCompressionRatio;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.Label labelWidthData;
        private System.Windows.Forms.Label labelBitDepthData;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.Label labelBitDepth;
        private System.Windows.Forms.Label labelHeightData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelT1Data;
        private System.Windows.Forms.Label labelT1;
        private System.Windows.Forms.Label labelKbps384Data;
        private System.Windows.Forms.Label labelKbps384;
        private System.Windows.Forms.Label labelKbps56Data;
        private System.Windows.Forms.Label labelKbps56;
        private System.Windows.Forms.Label labelFilesizeData;
        private System.Windows.Forms.Label labelFilesize;
        private System.Windows.Forms.Label labelFilenameData;
        private System.Windows.Forms.Label labelFilename;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label labelCompressed1CompressionRatioData;
        private System.Windows.Forms.Label labelCompressed1CompressionRatio;
        private System.Windows.Forms.Button buttonSettings1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelCompressed1Width;
        private System.Windows.Forms.Label labelCompressed1WidthData;
        private System.Windows.Forms.Label labelCompressed1BitDepthData;
        private System.Windows.Forms.Label labelCompressed1Height;
        private System.Windows.Forms.Label labelCompressed1BitDepth;
        private System.Windows.Forms.Label labelCompressed1HeightData;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label labelCompressed1T1Data;
        private System.Windows.Forms.Label labelCompressed1Kbps384Data;
        private System.Windows.Forms.Label labelCompressed1T1;
        private System.Windows.Forms.Label labelCompressed1Kbps384;
        private System.Windows.Forms.Label labelCompressed1Kbps56Data;
        private System.Windows.Forms.Label labelCompressed1Kbps56;
        private System.Windows.Forms.Label labelCompressed1FilesizeData;
        private System.Windows.Forms.Label labelCompressed1Filesize;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label labelCompressed2CompressionRatioData;
        private System.Windows.Forms.Label labelCompressed2CompressionRatio;
        private System.Windows.Forms.Button buttonSettings2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label labelCompressed2Width;
        private System.Windows.Forms.Label labelCompressed2WidthData;
        private System.Windows.Forms.Label labelCompressed2BitDepthData;
        private System.Windows.Forms.Label labelCompressed2Height;
        private System.Windows.Forms.Label labelCompressed2BitDepth;
        private System.Windows.Forms.Label labelCompressed2HeightData;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label labelCompressed2T1Data;
        private System.Windows.Forms.Label labelCompressed2Kbps384Data;
        private System.Windows.Forms.Label labelCompressed2T1;
        private System.Windows.Forms.Label labelCompressed2Kbps384;
        private System.Windows.Forms.Label labelCompressed2Kbps56Data;
        private System.Windows.Forms.Label labelCompressed2Kbps56;
        private System.Windows.Forms.Label labelCompressed2FilesizeData;
        private System.Windows.Forms.Label labelCompressed2Filesize;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label labelCompressed3CompressionRatioData;
        private System.Windows.Forms.Label labelCompressed3CompressionRatio;
        private System.Windows.Forms.Button buttonSettings3;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label labelCompressed3Width;
        private System.Windows.Forms.Label labelCompressed3WidthData;
        private System.Windows.Forms.Label labelCompressed3BitDepthData;
        private System.Windows.Forms.Label labelCompressed3Height;
        private System.Windows.Forms.Label labelCompressed3BitDepth;
        private System.Windows.Forms.Label labelCompressed3HeightData;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label labelCompressed3T1Data;
        private System.Windows.Forms.Label labelCompressed3Kbps384Data;
        private System.Windows.Forms.Label labelCompressed3T1;
        private System.Windows.Forms.Label labelCompressed3Kbps384;
        private System.Windows.Forms.Label labelCompressed3Kbps56Data;
        private System.Windows.Forms.Label labelCompressed3Kbps56;
        private System.Windows.Forms.Label labelCompressed3FilesizeData;
        private System.Windows.Forms.Label labelCompressed3Filesize;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label labelCompressed4CompressionRatioData;
        private System.Windows.Forms.Label labelCompressed4CompressionRatio;
        private System.Windows.Forms.Button buttonSettings4;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label labelCompressed4Width;
        private System.Windows.Forms.Label labelCompressed4WidthData;
        private System.Windows.Forms.Label labelCompressed4BitDepthData;
        private System.Windows.Forms.Label labelCompressed4Height;
        private System.Windows.Forms.Label labelCompressed4BitDepth;
        private System.Windows.Forms.Label labelCompressed4HeightData;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label labelCompressed4T1Data;
        private System.Windows.Forms.Label labelCompressed4Kbps384Data;
        private System.Windows.Forms.Label labelCompressed4T1;
        private System.Windows.Forms.Label labelCompressed4Kbps384;
        private System.Windows.Forms.Label labelCompressed4Kbps56Data;
        private System.Windows.Forms.Label labelCompressed4Kbps56;
        private System.Windows.Forms.Label labelCompressed4FilesizeData;
        private System.Windows.Forms.Label labelCompressed4Filesize;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlareCompressed;
        private System.Windows.Forms.Panel panelCompressedImage;
        private Accusoft.ImagXpressSdk.ImageXView compressed1ImageXView;
        private Accusoft.ImagXpressSdk.ImageXView compressed4ImageXView;
        private Accusoft.ImagXpressSdk.ImageXView compressed3ImageXView;
        private System.Windows.Forms.Label labelCompressed2;
        private System.Windows.Forms.Label labelCompressed1;
        private System.Windows.Forms.Label labelCompressed3;
        private System.Windows.Forms.Label labelCompressed4;
        private Accusoft.ImagXpressSdk.ImageXView compressed2ImageXView;
        private System.Windows.Forms.SplitContainer splitContainerImages;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrameImageInfo;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

// Accusoft Pegasus 
using Accusoft.ImagXpressSdk;
using Accusoft.NotateXpressSdk;
using Accusoft.ThumbnailXpressSdk;


namespace ImagXpressViewerDemo
{
    public partial class ViewDemoMain : Form
    {
        private DateTime progressTime;              // keep track of the time it takes to display an image
        private System.String displayedImageName;   // track the currently displayed Image (filename)
        private System.Int32  displayedPageNumber;  // track the currently displayed Page Number (for multipage)
        private Tool lastTool;                      // the last used ImageXpress tool

        /// <summary>
        /// Used for restoring the state of the machine during a reset;
        /// </summary>
        private System.Drawing.Size imageXViewMainOrigSize;
        private System.Drawing.Size thumbFileStripOrigSize;
        private System.Drawing.Size thumbMultiPageOrigSize;
        private System.Drawing.Size formOrigSize;

        private System.Drawing.Point imageXViewMainOrigLocation;
        private System.Drawing.Point thumbFileStripOrigLocation;
        private System.Drawing.Point thumbMultiPageOrigLocation;

        private System.Int32 thumbFileStripOrigCellBorderWidth;
        private System.Int32 thumbMultiPageOrigCellBorderWidth;
        private System.Int32 thumbFileStripOrigCellWidth;
        private System.Int32 thumbMultiPageOrigCellWidth;
        private System.Int32 thumbFileStripOrigCellHeight;
        private System.Int32 thumbMultiPageOrigCellHeight;
        private System.Drawing.Color thumbFileStripOrigSelectBackColor;
        private System.Drawing.Color thumbMultiPageOrigSelectBackColor;
        private System.Drawing.Color thumbFileStripOrigCellBorderColor;
        private System.Drawing.Color thumbMultiPageOrigCellBorderColor;
        private System.Drawing.Color thumbFileStripOrigCellSpacingColor;
        private System.Drawing.Color thumbMultiPageOrigCellSpacingColor;
        private System.Drawing.Color thumbFileStripOrigBackColor;
        private System.Drawing.Color thumbMultiPageOrigBackColor;
        private System.Drawing.Color viewOrigBackColor;
        private bool resizeThumbnailStrip;
        private System.String commonImagePath;
        private bool needToUpdateThumbnailAnnotations;
        private Dictionary<string, int> rotation;
        private ImageXView thumbnailImage;
        private Accusoft.NotateXpressSdk.NotateXpress notateXpressThumbnail;


        public ViewDemoMain()
        {
            InitializeComponent();
            // Call the Accusoft-Pegasus Unlocking Code here
            try
            {
                AccusoftUnlocks.UnlockRuntimes(imagXpress1, thumbnailXpressFilmStrip, thumbnailXpressMultiPage);
            }
            catch
            {
                MessageBox.Show("Invalid Unlock Codes, See the UnlockRuntime calls");
            }
        }

        #region Initialize
        private void ViewDemoMain_Load(object sender, EventArgs e)
        {
            try
            {
                progressTime = DateTime.Now;
                resizeThumbnailStrip = false;
                needToUpdateThumbnailAnnotations = false;
                rotation = new Dictionary<string, int>();

                // Double check the form properties didn't change
                this.Size = new Size(960, 700);
                this.MinimumSize = new Size(960, 700);

                statusStrip1.AutoSize = false;
                toolStripStatusLabel.Text = "Welcome to the ImagXpress10 Demonstration";

                toolStripStatusLabel2.Visible = false;
                toolStripStatusLabel2.Text = "";
                toolStripStatusLabel2.BorderSides = ToolStripStatusLabelBorderSides.All;
                toolStripStatusLabel2.BorderStyle = Border3DStyle.Sunken;

                toolStripStatusLabel3.Visible = false;
                toolStripStatusLabel3.Text = "";
                toolStripStatusLabel3.BorderSides = ToolStripStatusLabelBorderSides.All;
                toolStripStatusLabel3.BorderStyle = Border3DStyle.Sunken;

                toolStripSplitButtonMore.Visible = false;
                toolStripSplitButtonMore.Text = "More ...";

                // Set all of the Accusoft-Pegasus controls startup properties in code.
                imageXViewMain.BorderStyle = ImageXViewBorderStyle.Fixed3D;
                imageXViewMain.BackColor = System.Drawing.Color.FromArgb(224, 224, 224);
                imageXViewMain.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
                imageXViewMain.AutoResize = AutoResizeType.BestFit;
                imageXViewMain.AllowDrop = true;
                imageXViewMain.ScrollBars = ScrollBars.Both;
                imageXViewMain.ToolSet(Tool.Hand, MouseButtons.Left, Keys.None);
                imageXViewMain.AlphaBlend = true;

                // remove dither from the right click
                imageXViewMain.ContextMenu.MenuItems[1].Visible = false;
                // add a new preserve black
                MenuItem menuBlack = new MenuItem();

                menuBlack.Text = "Preserve Black";
                menuBlack.Name = "Black";
                menuBlack.Checked = false;
                imageXViewMain.ContextMenu.MenuItems.Add(4, menuBlack);

                // set up events to sync up context menu changes
                for (int i = 0; i < imageXViewMain.ContextMenu.MenuItems.Count; i++)
                {
                    imageXViewMain.ContextMenu.MenuItems[i].Click += new EventHandler(ContextMenuClickEventHandler);
                }
                imageXViewMain.ContextMenu.MenuItems["Black"].Click += new EventHandler(BlackContextMenuClickEventHandler);

                commonImagePath = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\Common\Images\");
                if (!System.IO.Directory.Exists(commonImagePath))
                    commonImagePath = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images\");

                // get embedded image from resources
                System.Reflection.Assembly thisExe;
                thisExe = System.Reflection.Assembly.GetExecutingAssembly();
                System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.multi.tif");
                imageXViewMain.Image = ImageX.FromStream(imagXpress1, embeddedFile);
                imageXViewMain.Tag = "multi";
                displayedImageName = System.IO.Path.Combine(commonImagePath, "multi.tif");
                displayedPageNumber = 1;
                viewOrigBackColor = imageXViewMain.BackColor;


                processor = new Processor(imagXpress1, imageXViewMain.Image);

                thumbnailXpressFilmStrip.CellSpacingColor = System.Drawing.Color.FromArgb(150, 150, 136);
                thumbnailXpressFilmStrip.BorderStyle = BorderStyle.Fixed3D;
                thumbnailXpressFilmStrip.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom;
                thumbnailXpressFilmStrip.CameraRaw = false;
                thumbnailXpressFilmStrip.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.Single;
                thumbnailXpressFilmStrip.SelectBackColor = System.Drawing.Color.FromArgb(166, 161, 131);
                thumbnailXpressFilmStrip.CellHeight = 110;
                thumbnailXpressFilmStrip.CellWidth = 124;
                thumbnailXpressFilmStrip.ScrollDirection = ScrollDirection.Vertical;
                thumbnailXpressFilmStrip.DescriptorDisplayMethod = DescriptorDisplayMethods.ShortFilename;
                thumbnailXpressFilmStrip.Font = new System.Drawing.Font("Segoe UI", 8);
                thumbnailXpressFilmStrip.ShowImagePlaceholders = true;
                thumbnailXpressFilmStrip.Visible = false;


                thumbnailXpressMultiPage.CellSpacingColor = System.Drawing.Color.FromArgb(150, 150, 136);
                thumbnailXpressMultiPage.BorderStyle = BorderStyle.Fixed3D;
                thumbnailXpressMultiPage.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top;
                thumbnailXpressMultiPage.CameraRaw = false;
                thumbnailXpressMultiPage.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.Single;
                thumbnailXpressMultiPage.SelectBackColor = System.Drawing.Color.FromArgb(166, 161, 131);
                thumbnailXpressMultiPage.CellHeight = 80;
                thumbnailXpressMultiPage.CellWidth = 80;
                thumbnailXpressMultiPage.ScrollDirection = ScrollDirection.Vertical;
                thumbnailXpressMultiPage.EnableAsDragSourceForExternalDragDrop = false;
                thumbnailXpressMultiPage.EnableAsDropTargetForExternalDragDrop = false;
                thumbnailXpressMultiPage.InterComponentThumbnailDragDropEnabled = false;
                thumbnailXpressMultiPage.IntraComponentThumbnailDragDropEnabled = false;
                thumbnailXpressMultiPage.DescriptorDisplayMethod = DescriptorDisplayMethods.IncludePage;
                thumbnailXpressMultiPage.Font = new System.Drawing.Font("Arial", 8);


                // We store these settings after the controls are created to make sure there are no conflicts
                // between design time and run time settings
                imageXViewMainOrigSize = imageXViewMain.Size;
                thumbFileStripOrigSize = thumbnailXpressFilmStrip.Size;
                thumbMultiPageOrigSize = thumbnailXpressMultiPage.Size;
                formOrigSize = this.Size;

                imageXViewMainOrigLocation = imageXViewMain.Location;
                thumbFileStripOrigLocation = thumbnailXpressFilmStrip.Location;
                thumbMultiPageOrigLocation = thumbnailXpressMultiPage.Location;

                thumbFileStripOrigCellBorderWidth = thumbnailXpressFilmStrip.CellBorderWidth;
                thumbMultiPageOrigCellBorderWidth = thumbnailXpressMultiPage.CellBorderWidth;
                thumbFileStripOrigCellWidth = thumbnailXpressFilmStrip.CellWidth;
                thumbMultiPageOrigCellWidth = thumbnailXpressMultiPage.CellWidth;
                thumbFileStripOrigCellHeight = thumbnailXpressFilmStrip.CellHeight;
                thumbMultiPageOrigCellHeight = thumbnailXpressMultiPage.CellHeight;

                thumbFileStripOrigSelectBackColor = thumbnailXpressFilmStrip.SelectBackColor;
                thumbMultiPageOrigSelectBackColor = thumbnailXpressMultiPage.SelectBackColor;
                thumbFileStripOrigCellBorderColor = thumbnailXpressFilmStrip.CellBorderColor;
                thumbMultiPageOrigCellBorderColor = thumbnailXpressMultiPage.CellBorderColor;
                thumbFileStripOrigCellSpacingColor = thumbnailXpressFilmStrip.CellSpacingColor;
                thumbMultiPageOrigCellSpacingColor = thumbnailXpressMultiPage.CellSpacingColor;
                thumbFileStripOrigBackColor = thumbnailXpressFilmStrip.BackColor;
                thumbMultiPageOrigBackColor = thumbnailXpressMultiPage.BackColor;

                loadStartupImages();

                notateXpress1.ClientWindow = imageXViewMain.Handle;
                notateXpress1.LineScaling = LineScaling.ResolutionY;
                notateXpress1.FontScaling = FontScaling.ResolutionY;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "The date is: " + DateTime.Now.ToString();
                System.Drawing.Font fontCurrent = notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont;
                System.Drawing.Font fontNew = new System.Drawing.Font(fontCurrent.Name, 12, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = fontNew;
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.BackStyle = BackStyle.Translucent;
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.FillColor = Color.Yellow;
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.PenWidth = 0;
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.ToolTipText = "Image Annotation";
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;
                // don't auto load/save annotations
                notateXpress1.ImagXpressLoad = false;
                notateXpress1.ImagXpressSave = false;

                // load signature image annotation
                commonImagePath = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\Common\Images\");
                if (!System.IO.Directory.Exists(commonImagePath))
                    commonImagePath = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images\");

                System.IO.Directory.SetCurrentDirectory(commonImagePath);

                String signatureImagePath = System.IO.Path.Combine(commonImagePath, "Signature48.png");
                if (System.IO.File.Exists(signatureImagePath))
                {
                    ImageX imageToolImage = ImageX.FromFile(imagXpress1, signatureImagePath);
                    IntPtr dibHandle = imageToolImage.ToHdib(true);
                    notateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle;

                    //resource cleanup
                    if (imageToolImage != null)
                    {
                        imageToolImage.Dispose();
                        imageToolImage = null;
                    }
                }

                // For storing a copy of the image and annotations
                thumbnailImage = new ImageXView(imagXpress1);
                notateXpressThumbnail = new NotateXpress();
                notateXpressThumbnail.ClientWindow = thumbnailImage.Handle;
                notateXpressThumbnail.LineScaling = LineScaling.ResolutionY;
                notateXpressThumbnail.FontScaling = FontScaling.ResolutionY;
                notateXpressThumbnail.ImagXpressLoad = false;
                notateXpressThumbnail.ImagXpressSave = false;
            }
            catch (ImageXViewException  ex )
            {
                displayStatusText("ImageXView error: " + ex.Message, false);
            }
            catch (ImageXException ex)
            {
                displayStatusText("ImageX error: " + ex.Message, false);
            }
            catch (Exception ex)
            {
                displayStatusText("Error: " + ex.Message, false);
            }
        }

        private void loadStartupImages()
        {

            // Check source tree
            try
            {
                addtoThumbnail(System.IO.Path.Combine(commonImagePath, "multi.tif"));
                addtoThumbnail(System.IO.Path.Combine(commonImagePath, "Japan.jpg"));
                addtoThumbnail(System.IO.Path.Combine(commonImagePath, "benefits.tif"));
                addtoThumbnail(System.IO.Path.Combine(commonImagePath, "Nature1.jpg"));
                addtoThumbnail(System.IO.Path.Combine(commonImagePath, "Nature2.jpg"));
                addtoThumbnail(System.IO.Path.Combine(commonImagePath, "NewOrleans.jpg"));

                addtoThumbnailMultiPage(System.IO.Path.Combine(commonImagePath, "multi.tif"));

                thumbnailXpressFilmStrip.SelectedItems.Clear();
                thumbnailXpressFilmStrip.ScrollItems(ScrollItemsType.ItemIndex, 0);
                thumbnailXpressFilmStrip.SelectedItems.Add(thumbnailXpressFilmStrip.Items[0]);
                displayStatusAllImageInfo();
            }
            catch
            {
                // ignore if it fails
            }
        }

        private void ResetSystem()
        {
            try
            {
                // temp way of cleaning controls.
                imageXViewMain.Image.Dispose();
                thumbnailXpressFilmStrip.Items.Clear();
                rotation.Clear();
                thumbnailXpressMultiPage.Items.Clear();
                thumbnailXpressFilmStrip.SelectedItems.Clear();
                thumbnailXpressMultiPage.SelectedItems.Clear();
                notateXpress1.Layers.Clear();

                System.Reflection.Assembly thisExe;
                thisExe = System.Reflection.Assembly.GetExecutingAssembly();

                // sample code to show list of resources
                //string[] resources = thisExe.GetManifestResourceNames();
                //string list = "";
                //foreach (string resource in resources)
                //    list += resource + "\r\n";
                // MessageBox.Show(list);

                System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.multi.tif");
                displayedImageName = System.IO.Path.Combine(commonImagePath, "multi.tif");
                displayedPageNumber = 1;

                imageXViewMain.Image = ImageX.FromStream(imagXpress1, embeddedFile);
                imageXViewMain.AutoResize = AutoResizeType.BestFit;
                imageXViewMain.Antialias = true;
                imageXViewMain.PreserveBlack = false;
                imageXViewMain.Smoothing = true;
                thumbnailXpressFilmStrip.Visible = false;
                imageXViewMain.AlignHorizontal = AlignHorizontal.Center;
                imageXViewMain.AlignVertical = AlignVertical.Center;
                imageXViewMain.BackColor = viewOrigBackColor;

                processor.Dispose();
                processor = new Processor(imagXpress1, imageXViewMain.Image);

                imageXViewMain.Size = imageXViewMainOrigSize;
                thumbnailXpressFilmStrip.Size = thumbFileStripOrigSize;
                thumbnailXpressMultiPage.Size = thumbMultiPageOrigSize;
                resizeThumbnailStrip = true;
                this.Size = formOrigSize;

                imageXViewMain.Location = imageXViewMainOrigLocation;
                thumbnailXpressFilmStrip.Location = thumbFileStripOrigLocation;
                thumbnailXpressMultiPage.Location = thumbMultiPageOrigLocation;

                thumbnailXpressFilmStrip.CellBorderWidth = thumbFileStripOrigCellBorderWidth;
                thumbnailXpressMultiPage.CellBorderWidth = thumbMultiPageOrigCellBorderWidth;
                thumbnailXpressFilmStrip.CellWidth = thumbFileStripOrigCellWidth;
                thumbnailXpressMultiPage.CellWidth = thumbMultiPageOrigCellWidth;
                thumbnailXpressFilmStrip.CellHeight = thumbFileStripOrigCellHeight;
                thumbnailXpressMultiPage.CellHeight = thumbMultiPageOrigCellHeight;

                thumbnailXpressFilmStrip.SelectBackColor = thumbFileStripOrigSelectBackColor;
                thumbnailXpressMultiPage.SelectBackColor = thumbMultiPageOrigSelectBackColor;
                thumbnailXpressFilmStrip.CellBorderColor = thumbFileStripOrigCellBorderColor;
                thumbnailXpressMultiPage.CellBorderColor = thumbMultiPageOrigCellBorderColor;
                thumbnailXpressFilmStrip.CellSpacingColor = thumbFileStripOrigCellSpacingColor;
                thumbnailXpressMultiPage.CellSpacingColor = thumbMultiPageOrigCellSpacingColor;
                thumbnailXpressFilmStrip.BackColor = thumbFileStripOrigBackColor;
                thumbnailXpressMultiPage.BackColor = thumbMultiPageOrigBackColor;

                cleanUpAnnotationFiles();

                loadStartupImages();
            }
            catch (ImageXViewException ex)
            {
                displayStatusText("ImageXView error: " + ex.Message, false);
            }
            catch (ImageXException ex)
            {
                displayStatusText("ImageX error: " + ex.Message, false);
            }
            catch (Exception ex)
            {
                displayStatusText("Error: " + ex.Message, false);
            }

        }

        #endregion

        #region Open and Close Images

        private void openToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            openThumbnailFile();
        }

        private void openThumbnailFile()
        {
            System.String strDefaultImageFilter = "All Files (*.*)|*.*|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal" +
                                                  "|Windows Device Independent Bitmap(*.DIB)|*.dib" +
                                                  "|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx" +
                                                  "|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls" +
                                                  "|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp" +
                                                  "|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx" +
                                                  "|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic" +
                                                  "|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm" +
                                                  "|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga" +
                                                  "|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2" +
                                                  "|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf" +
                                                  "|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp";

            openFileDialog.FileName = "";
            openFileDialog.Title = "Please select an image";
            openFileDialog.Filter = strDefaultImageFilter;
            openFileDialog.Multiselect = false;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                addtoThumbnail(openFileDialog.FileName);
                loadImagXpress(openFileDialog.FileName);
            }
        }

        private void addtoThumbnail(string thumbName)
        {
            try
            {
                if(System.IO.File.Exists(thumbName))
                {
                    displayStatusText("Load into thumbnail file: " + thumbName.ToLower(), false);

                    // Check if the file is already in the thumbnail strip
                    int i = 0;
                    while (i < thumbnailXpressFilmStrip.Items.Count)
                    {
                        if (System.IO.Path.GetFullPath(thumbnailXpressFilmStrip.Items[i].Filename).ToLower() == System.IO.Path.GetFullPath(thumbName).ToLower())
                        {                
                            // The file is already in the thumbnail strip, so select it but do not add it.
                            thumbnailXpressFilmStrip.SelectedItems.Clear();
                            thumbnailXpressFilmStrip.SelectedItems.Add(thumbnailXpressFilmStrip.Items[i]);
                            thumbnailXpressFilmStrip.EnsureVisible(i);
                            return;
                        }
                        i++;
                    }

                    thumbnailXpressFilmStrip.Items.AddItemsFromFile(thumbName, -1, false);
                    if (thumbnailXpressFilmStrip.Items.Count > 1)
                        thumbnailXpressFilmStrip.Visible = true;
                    thumbnailXpressFilmStrip.SelectedItems.Clear();
                    thumbnailXpressFilmStrip.SelectedItems.Add(thumbnailXpressFilmStrip.Items[thumbnailXpressFilmStrip.Items.Count - 1]);
                    thumbnailXpressFilmStrip.EnsureVisible(thumbnailXpressFilmStrip.Items.Count - 1);
                }
            }
            catch
            {
                displayStatusText("Error: Failure loading into thumbnail file: " + thumbName.ToLower(), false);
            }

        }

        private void addtoThumbnailMultiPage(string thumbName)
        {
            try
            {
                thumbnailXpressMultiPage.Items.Clear();

                int pageCount = imageXViewMain.Image.PageCount;
                if (pageCount <= 1)
                {
                    thumbnailXpressMultiPage.Items.AddItemsFromFile(thumbName, -1, true);
                }
                else
                {
                    // Do not brand annotations on the first page.  The first page will be loaded
                    // and at that time the thumbnail will be updated if annotations exist
                    thumbnailXpressMultiPage.Items.AddItemsFromFile(thumbName, -1, false);

                    // Load the rest of the pages into an ImageX control, load and brand the XML annotations
                    // if they exist, and add the thumbnail image
                    int i = 0;
                    for (i = 1; i < pageCount; i++)
                    {
                        string rotateKey = System.IO.Path.GetFileName(thumbName) + "," + i.ToString();
                        
                        Accusoft.ImagXpressSdk.LoadOptions ixLoadOptions = new Accusoft.ImagXpressSdk.LoadOptions();
                        ixLoadOptions.Rotation = getRotation(rotateKey);
                        thumbnailImage.Image = ImageX.FromFile(imagXpress1, thumbName, i + 1, ixLoadOptions);

                        if (loadAnnotations(thumbName, i + 1, notateXpressThumbnail) == true)
                            notateXpressThumbnail.Layers.Brand(24);

                        notateXpressThumbnail.Layers.Clear();

                        addImageToThumbnailMultiPage(thumbnailImage.Image, i, thumbName);

                        thumbnailImage.Image.Dispose();
                    }
                }
                thumbnailXpressMultiPage.SelectedItems.Clear();
                thumbnailXpressMultiPage.SelectedItems.Add(thumbnailXpressMultiPage.Items[0]);
                
            }
            catch
            {
                displayStatusText("Error: Failure loading into thumbnail file: " + thumbName.ToLower(), false);
            }

        }

        private void addImageToThumbnailMultiPage(ImageX thumbnailImage, System.Int32 insertIndex, System.String thumbName)
        {
            // Save the image to stream
            ThumbnailItem item = thumbnailXpressMultiPage.CreateThumbnailItem(thumbName, insertIndex + 1, ThumbnailType.Image);
            item.UserTag = "";
            MemoryStream thumbnailStream = new MemoryStream();
            Accusoft.ImagXpressSdk.SaveOptions saveOptionsThumbnail = new Accusoft.ImagXpressSdk.SaveOptions();
            saveOptionsThumbnail.Format = ImageXFormat.Jpeg;
            saveOptionsThumbnail.Jpeg.SubSampling = SubSampling.SubSampling411;
            saveOptionsThumbnail.Jpeg.Chrominance = 20;
            saveOptionsThumbnail.Jpeg.Luminance = 20;
            imagXpress1.ProgressEvent -= imagXpress1_ProgressEvent; // Do not display the progress
            thumbnailImage.SaveStream(thumbnailStream, saveOptionsThumbnail);
            imagXpress1.ProgressEvent += new ImagXpress.ProgressEventHandler(imagXpress1_ProgressEvent);


            // Load the image stream into the thumbnail control
            thumbnailStream.Position = 0;
            item.FromStream(thumbnailStream);
            thumbnailXpressMultiPage.Items.InsertItemFromStream(insertIndex, item);

            thumbnailStream.Close();
            thumbnailStream.Dispose();
        }

        private void loadImagXpress(ThumbnailItem thumbLoad)
        {
            if (thumbLoad != null)
            {
                try
                {
                    if ((thumbLoad.Type == ThumbnailType.Image) ||
                        (thumbLoad.Type == ThumbnailType.Cad))
                    {
                        loadImageXpressImage(thumbLoad.Filename, thumbLoad.Page);
                    }
                    else
                    {
                        displayStatusText("File: " + thumbLoad.Filename.ToString().ToLower() + " of type " + thumbLoad.Type.ToString() + " is not valid for ImageXpress to display", false);

                    }
                }
                catch
                {
                    displayStatusText("File: " + thumbLoad.Filename.ToLower() + " is not valid for display", true);
                }
            }
        }

        private void loadImagXpress(System.String filename)
        {
            loadImageXpressImage(filename, 1);
        }

        private void loadImageXpressImage(System.String filename, System.Int32 pageNumber)
        {
            Accusoft.ImagXpressSdk.LoadOptions ixLoadOptions;

            if (filename != null)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    ixLoadOptions = new Accusoft.ImagXpressSdk.LoadOptions();

                    ixLoadOptions.LoadMode = LoadMode.Normal;
                    ixLoadOptions.CameraRawEnabled = false;

                    // Save Annotations before leaving the image
                    saveAnnotations(displayedImageName, displayedPageNumber);
                    notateXpress1.Layers.Clear();

                    displayedImageName = filename;
                    displayedPageNumber = pageNumber;
                    
                    imageXViewMain.Tag = filename.ToLower(); // grab the filename for display during the progess event.
                    imageXViewMain.Tag = System.IO.Path.GetFileNameWithoutExtension(imageXViewMain.Tag.ToString());
                    progressTime = DateTime.Now;

                    string rotateKey = System.IO.Path.GetFileName(filename) + "," + (pageNumber-1).ToString();

                    ixLoadOptions.Rotation = getRotation(rotateKey);

                    imageXViewMain.AutoResize = AutoResizeType.BestFit;
                    imageXViewMain.Image = ImageX.FromFile(imagXpress1, filename, pageNumber, ixLoadOptions);
                    processor = new Processor(imagXpress1, imageXViewMain.Image);

                    displayStatusAllImageInfo();

                    Application.DoEvents();
                    if (thumbnailXpressMultiPage.Items.Count == 0)
                        addtoThumbnailMultiPage(filename);
                    else if ((thumbnailXpressMultiPage.Items.Count > 0) && (thumbnailXpressMultiPage.Items[0].Filename != filename))
                        addtoThumbnailMultiPage(filename);

                    if(loadAnnotations(displayedImageName, displayedPageNumber, notateXpress1) == true)
                        updateThumbnailAnnotations();

                    Cursor = Cursors.Default;
                }
                catch (ImageXViewException /* ex */)
                {
                    displayStatusText("File: " + filename.ToLower() + " is not valid for ImageXView to display", true);
                    Cursor = Cursors.Default;
                }
                catch (ImageXException /* ex */)
                {
                    displayStatusText("File: " + filename.ToLower() + " is not valid for ImageXpress to display", true);
                    Cursor = Cursors.Default;
                }
                catch
                {
                    displayStatusText("File: " + filename.ToLower() + " is not valid for ImageXpress to display", true);
                    Cursor = Cursors.Default;
                }
            }
            else
            {
                displayStatusText("File: " + filename.ToLower() + " is not valid for ImageXpress to display", true);

            }

        }

        private RotateAngle getRotation(string rotateKey)
        {
            if (rotation.ContainsKey(rotateKey))
            {
                System.Int32 rotationValue = (System.Int32)rotation[rotateKey];
                switch (rotationValue)
                {
                    case 0:
                        return RotateAngle.Rotate0;
                    case 90:
                        return RotateAngle.Rotate90;
                    case 180:
                        return RotateAngle.Rotate180;
                    case 270:
                        return RotateAngle.Rotate270;
                }
            }
            return RotateAngle.Rotate0;
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            openThumbnailFile();
        }

        // tracking the time span reduces the flashing on the status bar.
        private void imagXpress1_ProgressEvent(object sender, ProgressEventArgs e)
        {
            TimeSpan duration1;

            duration1 = DateTime.Now - progressTime;
            progressTime = DateTime.Now;

            if (duration1.Milliseconds > 100)
                displayStatusText("File: " + imageXViewMain.Tag + " is " + e.PercentDone.ToString() + "% complete", false);

        }

        private void thumbnailXpressFilmStrip_ItemComplete(object sender, ItemCompleteEventArgs e)
        {
            // if item complete matches IX filename, reset the selected.
            int i = 0;
            Boolean found = false;

            while ((i < (thumbnailXpressFilmStrip.Items.Count)) && (found == false))
            {
                if (thumbnailXpressFilmStrip.Items[i].Filename == displayedImageName)
                {
                    found = true;
                    thumbnailXpressFilmStrip.SelectedItems.Clear();
                    thumbnailXpressFilmStrip.SelectedItems.Add(thumbnailXpressFilmStrip.Items[i]);
                }
                i++;
            }

        }

        private void OpenDirtoolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Open dir
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;

            folderBrowserDialog.Description = "Select a folder with your images";
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                string dirChosen = folderBrowserDialog.SelectedPath;

                thumbnailXpressFilmStrip.Filters.Clear();
                Filter thumbnailFilter = thumbnailXpressFilmStrip.CreateFilter("directory filter");
                thumbnailFilter.IncludeParentDirectory = false;
                thumbnailFilter.IncludeSubDirectory = false;
                thumbnailFilter.MatchAppliesToDirectory = true;
                thumbnailFilter.Type = FilterType.Filename;
                // Only generate thumbnails for IX supported files.

                thumbnailFilter.FilenamePattern = "*.bmp|*.cal|*.dib|*.dca|*.mod|*.dcx|*.gif|*.jp2|*.jls|*.jpg|*.jif|*.ljp|*.pbm|*.pcx|*.pgm|*.pic|*.png|*.ppm|*.tif|*.tiff|*.tga|*.wsq|*.jb2|*.hdp|*.wdp";

                thumbnailXpressFilmStrip.Filters.Add(thumbnailFilter);

                thumbnailXpressFilmStrip.Items.Clear();
                rotation.Clear();

                int newImages = thumbnailXpressFilmStrip.Items.AddItemsFromDirectory(dirChosen + "\\", 0);

                // Select the first item
                if (thumbnailXpressFilmStrip.Items.Count >= 1)
                {
                    thumbnailXpressFilmStrip.SelectedItems.Add(thumbnailXpressFilmStrip.Items[0]);
                    loadImageXpressImage(thumbnailXpressFilmStrip.SelectedItems[0].Filename, 0);
                    thumbnailXpressFilmStrip.Visible = true;
                }
            }
        }


        #endregion

        #region Annotations

        // Returns true if annotations were loaded
        private System.Boolean loadAnnotations(System.String imageFilename, System.Int32 imagePageNumber, NotateXpress notate)
        {
            System.String extendedFilename;
            Accusoft.NotateXpressSdk.LoadOptions loadNotate;

            loadNotate = new Accusoft.NotateXpressSdk.LoadOptions();

            loadNotate.AnnType = AnnotationType.NotateXpressXml;  // Most readable format.
            loadNotate.LoadUserData  = false; // no user data in this demo.

            notate.PageNumber = imagePageNumber;

            extendedFilename = System.IO.Path.GetTempPath() + System.IO.Path.GetFileName(imageFilename) + ".nxxml";

            try
            {
                notate.Layers.FromFile(extendedFilename, loadNotate);
            }
            catch
            {
                // annotations didn't load, or couldn't find file.
                return false;
            }
            return true;

        }

        
        private void saveAnnotations(System.String imageFilename, System.Int32 imagePageNumber)
        {
            System.String extendedFilename;
            Accusoft.NotateXpressSdk.SaveOptions saveNotate;

            saveNotate = new Accusoft.NotateXpressSdk.SaveOptions();

            saveNotate.AnnType = AnnotationType.NotateXpressXml;  // Most readable format.
            saveNotate.SaveUserData = false; // no user data in this demo.

            notateXpress1.PageNumber = imagePageNumber;

            extendedFilename = System.IO.Path.GetTempPath() + System.IO.Path.GetFileName(imageFilename) + ".nxxml";

            try
            {
                notateXpress1.Layers.SaveToFile(extendedFilename, saveNotate);
            }
            catch
            {
                // annotations didn't save, or couldn't write file.
            }

        }


        private void cleanUpAnnotationFiles()
        {
           // Clean out all of the nxxml files in the temp dir.
            try
            {
                DirectoryInfo di= new DirectoryInfo(System.IO.Path.GetTempPath());
                if (di.Exists)
                {
                    FileInfo[] rgFiles2 = di.GetFiles("*.nxxml");
                    foreach (FileInfo fi in rgFiles2)
                    {
                        fi.Delete();
                    }
                }
            }
            catch
            {
                // ignore if it fails
            }

        }
        #endregion

        #region Status and  Errors
        private void thumbnailXpressFilmStrip_Error(object sender, Accusoft.ThumbnailXpressSdk.ErrorEventArgs e)
        {
            // only show errors for a selected thumbnail
            if (thumbnailXpressFilmStrip.SelectedItems.Contains(thumbnailXpressFilmStrip.Items[e.ItemIndex]))
                displayStatusText("Error " + e.Number.ToString() + " in ThumbnailXpress: " + e.Message.ToString(), false);
        }
        private void displayStatusText(System.String statusText, System.Boolean showAlert)
        {

            toolStripStatusLabel.Text = statusText;
            toolStripStatusLabel.BorderStyle = Border3DStyle.Flat;
            toolStripStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.None;
            statusStrip1.Refresh();

            toolStripStatusLabel2.Visible = false;
            toolStripStatusLabel3.Visible = false;
            toolStripSplitButtonMore.Visible = false;
        }

        private void displayStatusAllImageInfo()
        {
            if (imageXViewMain.Image != null)
            {
                toolStripStatusLabel.BorderStyle = Border3DStyle.Sunken;
                toolStripStatusLabel.BorderSides = ToolStripStatusLabelBorderSides.All;

                toolStripStatusLabel.Text = "File: " + imageXViewMain.Tag;
                toolStripStatusLabel2.Text = "Height: " + imageXViewMain.Image.ImageXData.Height;
                toolStripStatusLabel3.Text = "Width: " + imageXViewMain.Image.ImageXData.Width;

                toolStripStatusLabel2.Visible = true;
                toolStripStatusLabel3.Visible = true;
                toolStripSplitButtonMore.Visible = true;
            }
        }
        #endregion

        #region Select, Move, and Drop Images
        private void thumbnailXpressFilmStrip_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(thumbnailXpressFilmStrip.SelectedItems[0].Filename);
            thumbnailXpressFilmStrip.Enabled = false;

            // save off any annotations

            if (thumbnailXpressFilmStrip.SelectedItems.Count > 0)
                loadImagXpress(thumbnailXpressFilmStrip.SelectedItems[0]);
            else
                displayStatusText("", false);
            thumbnailXpressFilmStrip.Enabled = true;

        }

        private void imageXView1_Click(object sender, EventArgs e)
        {
            displayStatusAllImageInfo();
        }


        private void imageXView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;

        }

        private void imageXView1_DragDrop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))  // extra safety
            {
                string[] files;

                try
                {
                    files = (string[])e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop);
                    // add all to thumb then ix
                    //I didn't do a "foreach (string filename in files)", so I can modify the order.
                    for (int i = files.Length - 1; i >= 0; i--)
                        addtoThumbnail(files[i]);

                    loadImagXpress(files[0]);
                }
                catch (Accusoft.ImagXpressSdk.ImagXpressException ex)
                {
                    displayStatusText("Error: " + ex.Message, true);
                }
                catch
                {
                    displayStatusText("Error in ImagXpress drag drop", true);
                }

            }

        }

        private void thumbnailXpressFilmStrip_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))  // extra safety
            {
                try
                {
                    string filename = thumbnailXpressFilmStrip.GetDragFilename((System.IntPtr)e.Data.GetData(DataFormats.FileDrop), 0);

                    loadImagXpress(filename);

                }
                catch
                {
                    displayStatusText("Error in ThumbnailXpress drag drop", true);
                }
            }

        }

        private void thumbnailXpressMultiPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (thumbnailXpressMultiPage.SelectedItems.Count > 0)
                loadImagXpress(thumbnailXpressMultiPage.SelectedItems[0]);
            else
                displayStatusText("", false);

        }

        #endregion



        #region ContextMenu Handling



        // generic for ALL context menu items 
        private void ContextMenuClickEventHandler(object sender, EventArgs e)
        {
            antiAliasToolStripMenuItem.Checked = imageXViewMain.Antialias;
            smoothToolStripMenuItem.Checked = imageXViewMain.Smoothing;
        }

        // specific only for Preserve black context menu
        private void BlackContextMenuClickEventHandler(object sender, EventArgs e)
        {
            // need to manually handle preserve black
            imageXViewMain.PreserveBlack = !imageXViewMain.PreserveBlack;
            imageXViewMain.ContextMenu.MenuItems["Black"].Checked = imageXViewMain.PreserveBlack;
            preserveBlackToolStripMenuItem.Checked = imageXViewMain.PreserveBlack;

        }

        private void smoothingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            smoothToolStripMenuItem.Checked = !smoothToolStripMenuItem.Checked;
            imageXViewMain.Smoothing = smoothToolStripMenuItem.Checked;
        }

        private void antiAliasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            antiAliasToolStripMenuItem.Checked = !antiAliasToolStripMenuItem.Checked;
            imageXViewMain.Antialias = antiAliasToolStripMenuItem.Checked;
        }

        private void preserveBlackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageXViewMain.PreserveBlack = !imageXViewMain.PreserveBlack;
            preserveBlackToolStripMenuItem.Checked = imageXViewMain.PreserveBlack;
            imageXViewMain.ContextMenu.MenuItems["Black"].Checked = imageXViewMain.PreserveBlack;
        }

        #endregion

        #region Toolbar Handling (Application)
        private void rotateLeftToolStripButton_Click(object sender, EventArgs e)
        {
            processor.Rotate(90);
            displayStatusAllImageInfo(); // width and height should be updated

            string rotateKey = System.IO.Path.GetFileName(thumbnailXpressFilmStrip.SelectedItems[0].Filename) + "," + thumbnailXpressMultiPage.Items.IndexOf(thumbnailXpressMultiPage.SelectedItems[0]).ToString();
            int currentRotate = 0;
            if(rotation.ContainsKey(rotateKey))
                currentRotate = (int)rotation[rotateKey];
            currentRotate = (currentRotate + 90) % 360;
            if (rotation.ContainsKey(rotateKey) == true)
                rotation.Remove(rotateKey);
            rotation.Add(rotateKey, currentRotate);

            updateThumbnailAnnotations();
        }

        private void rotateRightToolStripButton_Click(object sender, EventArgs e)
        {
            processor.Rotate(-90);
            displayStatusAllImageInfo(); // width and height should be updated

            string rotateKey = System.IO.Path.GetFileName(thumbnailXpressFilmStrip.SelectedItems[0].Filename) + "," + thumbnailXpressMultiPage.Items.IndexOf(thumbnailXpressMultiPage.SelectedItems[0]).ToString();
            int currentRotate = 0;
            if(rotation.ContainsKey(rotateKey))
                currentRotate = (int)rotation[rotateKey];
            currentRotate = (currentRotate + 270) % 360;
            if (rotation.ContainsKey(rotateKey) == true)
                rotation.Remove(rotateKey);
            rotation.Add(rotateKey, currentRotate);

            updateThumbnailAnnotations();
        }

        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            ResetSystem();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetSystem();
        }

        private void zoomInToolStripButton_Click(object sender, EventArgs e)
        {
            if ((imageXViewMain.AutoResize == AutoResizeType.BestFit) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitWidth) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitHeight) ||
                (imageXViewMain.AutoResize == AutoResizeType.ResizeImage))
            {
                imageXViewMain.AutoResize = AutoResizeType.CropImage;
            }
            if(imageXViewMain.ZoomFactor <= 10)
                imageXViewMain.ZoomFactor = imageXViewMain.ZoomFactor * 1.25;
        }

        private void zoomOutToolStripButton_Click(object sender, EventArgs e)
        {
            if ((imageXViewMain.AutoResize == AutoResizeType.BestFit) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitWidth) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitHeight) ||
                (imageXViewMain.AutoResize == AutoResizeType.ResizeImage))
            {
                imageXViewMain.AutoResize = AutoResizeType.CropImage;
            }
            if(imageXViewMain.Image.ImageXData.Width * imageXViewMain.ZoomFactor >= 10 && imageXViewMain.Image.ImageXData.Height * imageXViewMain.ZoomFactor >= 10)
                imageXViewMain.ZoomFactor = imageXViewMain.ZoomFactor * 0.80;

        }

        private void zoomRectStripButton_Click(object sender, EventArgs e)
        {
            // make sure user doesn't do this twice
            if ((imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.ZoomRect) &&
               (imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.Magnify))
                lastTool = imageXViewMain.ToolGet(MouseButtons.Left, Keys.None);
            imageXViewMain.ToolSet(Tool.ZoomRect, MouseButtons.Left, Keys.None);
        }

        private void imageXViewMain_ToolEvent(object sender, ToolEventArgs e)
        {
            // check if we went from a ZoomRect.  If we did reset to last tool
            if ((e.Tool == Tool.ZoomRect) && (e.Action == ToolAction.End))
            {
                imageXViewMain.ToolSet(lastTool, MouseButtons.Left, Keys.None);
                if (imageXViewMain.ZoomFactor >= 10)
                    imageXViewMain.ZoomFactor = 10;
            }

            if ((e.Tool == Tool.Magnify) && (e.Action == ToolAction.End))
                imageXViewMain.ToolSet(lastTool, MouseButtons.Left, Keys.None);
        }

        private void magnifyToolStripButton_Click(object sender, EventArgs e)
        {
            // make sure user doesn't do this twice
            if ((imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.ZoomRect) &&
               (imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.Magnify))
                lastTool = imageXViewMain.ToolGet(MouseButtons.Left, Keys.None);
            imageXViewMain.ToolSet(Tool.Magnify, MouseButtons.Left, Keys.None);
        }

        private void panToolStripButton_Click(object sender, EventArgs e)
        {
            // hand pan
            imageXViewMain.ToolSet(Tool.Hand, MouseButtons.Left, Keys.None);
        }


        private void bestFitToolStripButton_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.BestFit;
        }

        private void fitWidthToolStripButton_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.FitWidth;
        }

        private void fitHeightToolStripButton_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.FitHeight;
        }

        private void actualSizeToolStripButton_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.CropImage;
            imageXViewMain.ZoomFactor = 1;

        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((imageXViewMain.AutoResize == AutoResizeType.BestFit) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitWidth) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitHeight) ||
                (imageXViewMain.AutoResize == AutoResizeType.ResizeImage))
            {
                imageXViewMain.AutoResize = AutoResizeType.CropImage;
            }
            if (imageXViewMain.ZoomFactor <= 10)
                imageXViewMain.ZoomFactor = imageXViewMain.ZoomFactor * 1.25;

        }

        private void zoomOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((imageXViewMain.AutoResize == AutoResizeType.BestFit) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitWidth) ||
                (imageXViewMain.AutoResize == AutoResizeType.FitHeight) ||
                (imageXViewMain.AutoResize == AutoResizeType.ResizeImage))
            {
                imageXViewMain.AutoResize = AutoResizeType.CropImage;
            }
            if (imageXViewMain.Image.ImageXData.Width * imageXViewMain.ZoomFactor >= 10 && imageXViewMain.Image.ImageXData.Height * imageXViewMain.ZoomFactor >= 10)
                imageXViewMain.ZoomFactor = imageXViewMain.ZoomFactor * 0.80;

        }

        private void zoomToRectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // make sure user doesn't do this twice
            if ((imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.ZoomRect) &&
               (imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.Magnify))
                lastTool = imageXViewMain.ToolGet(MouseButtons.Left, Keys.None);
            imageXViewMain.ToolSet(Tool.ZoomRect, MouseButtons.Left, Keys.None);

        }

        private void magnifyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // make sure user doesn't do this twice
            if ((imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.ZoomRect) &&
                (imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.Magnify))
                lastTool = imageXViewMain.ToolGet(MouseButtons.Left, Keys.None);
            imageXViewMain.ToolSet(Tool.Magnify, MouseButtons.Left, Keys.None);

        }

        private void fitActualSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.CropImage;
            imageXViewMain.ZoomFactor = 1;

        }

        private void fitBestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.BestFit;

        }

        private void fitWidthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.FitWidth;

        }

        private void fitHeightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageXViewMain.AutoResize = AutoResizeType.FitHeight;
        }

        private void rotateRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            processor.Rotate(-90);
            displayStatusAllImageInfo(); // width and height should be updated

            string rotateKey = System.IO.Path.GetFileName(thumbnailXpressFilmStrip.SelectedItems[0].Filename) + "," + thumbnailXpressMultiPage.Items.IndexOf(thumbnailXpressMultiPage.SelectedItems[0]).ToString();
            int currentRotate = 0;
            if (rotation.ContainsKey(rotateKey))
                currentRotate = (int)rotation[rotateKey];
            currentRotate = (currentRotate + 270) % 360;
            if (rotation.ContainsKey(rotateKey) == true)
                rotation.Remove(rotateKey);
            rotation.Add(rotateKey, currentRotate);

            updateThumbnailAnnotations();
        }

        private void rotateLeftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            processor.Rotate(90);
            displayStatusAllImageInfo(); // width and height should be updated

            string rotateKey = System.IO.Path.GetFileName(thumbnailXpressFilmStrip.SelectedItems[0].Filename) + "," + thumbnailXpressMultiPage.Items.IndexOf(thumbnailXpressMultiPage.SelectedItems[0]).ToString();
            int currentRotate = 0;
            if (rotation.ContainsKey(rotateKey))
                currentRotate = (int)rotation[rotateKey];
            currentRotate = (currentRotate + 90) % 360;
            if (rotation.ContainsKey(rotateKey) == true)
                rotation.Remove(rotateKey);
            rotation.Add(rotateKey, currentRotate);

            updateThumbnailAnnotations();
        }
        #endregion

        #region Config Menu
        private void filmStripToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resizeThumbnailStrip = true;

            // Launch config window, passing in the filmstrip
            FormConfigThumb configThumbnail;

            configThumbnail = new FormConfigThumb();
            configThumbnail.loadExistingThumb(thumbnailXpressFilmStrip);
            configThumbnail.ShowDialog();
            configThumbnail.Dispose();
        }

        private void multiPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Launch config window, passing in the filmstrip
            FormConfigThumb configThubmnail;

            configThubmnail = new FormConfigThumb();
            configThubmnail.loadExistingThumb(thumbnailXpressMultiPage);
            // The main image is only passed in to be modified if the multi-page
            // thumbnail strip is being modified, since they both share a panel
            configThubmnail.loadExistingView(imageXViewMain);
            configThubmnail.ShowDialog();
            configThubmnail.Dispose();
        }


        private void viewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Launch config window, passing in the filmstrip
            FormConfigViewer configViewer;

            configViewer = new FormConfigViewer();
            configViewer.loadExistingView(imageXViewMain);
            configViewer.ShowDialog();
            configViewer.Dispose();
        }

        #endregion

        private void ViewDemoMain_KeyDown(object sender, KeyEventArgs e)
        {
            // Delete any selected thumbs on a delete
            if ((e.KeyCode == Keys.Delete) && (thumbnailXpressFilmStrip.SelectedItems.Count >= 1))
            {
                int i = thumbnailXpressFilmStrip.Items.IndexOf(thumbnailXpressFilmStrip.SelectedItems[0]);
                thumbnailXpressFilmStrip.Items.Remove(thumbnailXpressFilmStrip.SelectedItems[0]);

                if (thumbnailXpressFilmStrip.Items.Count > 1)
                {
                    if (thumbnailXpressFilmStrip.Items.Count == i)
                        i--;
                    thumbnailXpressFilmStrip.SelectedItems.Add(thumbnailXpressFilmStrip.Items[i]);
                    loadImagXpress(thumbnailXpressFilmStrip.SelectedItems[0]);
                    thumbnailXpressFilmStrip.EnsureVisible(0);

                }
                else if (thumbnailXpressFilmStrip.Items.Count == 1)
                {
                    thumbnailXpressFilmStrip.Visible = false;
                    loadImagXpress(thumbnailXpressFilmStrip.Items[0]);
                }
            }
            else if ((e.KeyCode == Keys.Delete) && (thumbnailXpressFilmStrip.Items.Count == 1))
            {
                ResetSystem();
            }

        }

        private void aboutImagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void aboutNotateXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.AboutBox();
        }

        private void aboutThumbnailXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            thumbnailXpressFilmStrip.AboutBox();
        }

        private void toolStripSplitButtonMore_ButtonClick(object sender, EventArgs e)
        {
            // Launch config window, passing in the filmstrip
            FormMoreImageInfo moreImageInfo;

            moreImageInfo = new FormMoreImageInfo();
            moreImageInfo.loadExistingView(imageXViewMain);
            moreImageInfo.ShowDialog();
            moreImageInfo.Dispose();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }


        #region Annotations

        private void annotateStart()
        {
            // make sure we have a layer to draw upon.
            if (notateXpress1.Layers.Count == 0)
                notateXpress1.Layers.Add(new Layer());
       }

        private void trackIXTool()
        {
            if (imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) != Tool.None)
                lastTool = imageXViewMain.ToolGet(MouseButtons.Left, Keys.None);
            imageXViewMain.ToolSet(Tool.None, MouseButtons.Left, Keys.None);
        }

        private void resetIXTool()
        {
            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;
            if (imageXViewMain.ToolGet(MouseButtons.Left, Keys.None) == Tool.None)
                imageXViewMain.ToolSet(lastTool, MouseButtons.Left, Keys.None);
        }


        private void RecttoolStripButton_Click(object sender, EventArgs e)
        {
            annotateStart();
            trackIXTool();
            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.RectangleTool;

        }

        private void StamptoolStripButton_Click(object sender, EventArgs e)
        {
            annotateStart();
            trackIXTool();
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "Have a nice day " + Environment.NewLine + DateTime.Now.ToString();

            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.StampTool;

        }

        private void notateXpress1_AnnotationAdded(object sender, AnnotationAddedEventArgs e)
        {
            resetIXTool();

            updateThumbnailAnnotations();
        }

        private void notateXpress1_AnnotationMoved(object sender, AnnotationMovedEventArgs e)
        {
            needToUpdateThumbnailAnnotations = true;
        }

        private void notateXpress1_AnnotationDeleted(object sender, AnnotationDeletedEventArgs e)
        {
            updateThumbnailAnnotations();
        }

        private void notateXpress1_ItemChanged(object sender, Accusoft.NotateXpressSdk.ItemChangedEventArgs e)
        {
            updateThumbnailAnnotations();
        }

        private void imageXViewMain_MouseClick(object sender, MouseEventArgs e)
        {
            if (needToUpdateThumbnailAnnotations == true)
                updateThumbnailAnnotations();
        }

        private void imageXViewMain_MouseLeave(object sender, EventArgs e)
        {
            if (needToUpdateThumbnailAnnotations == true)
                updateThumbnailAnnotations();
        }

        private void updateThumbnailAnnotations()
        {
            needToUpdateThumbnailAnnotations = false;

            if (thumbnailXpressMultiPage.SelectedItems.Count == 1 && notateXpress1.Layers.Count > 0 && notateXpress1.Layers[0].Elements.Count > 0)
            {
                // Remove the item changed event as it interferes when editing a text annotation
                notateXpress1.ItemChanged -= notateXpress1_ItemChanged;

                MemoryStream anns = new MemoryStream();

                // Save the current annotations
                Accusoft.NotateXpressSdk.SaveOptions saveNotate;

                saveNotate = new Accusoft.NotateXpressSdk.SaveOptions();
                try
                {
                    anns = notateXpress1.Layers.SaveToMemoryStream(saveNotate);
                    anns.Position = 0;
                }
                catch
                {
                    // annotations didn't save, or couldn't write file.
                }

                // Create a new thumbnail item to insert at the index of the currently selected thumbnail item
                int index = thumbnailXpressMultiPage.Items.IndexOf(thumbnailXpressMultiPage.SelectedItems[0]);

                // Load the current page into an ImageX control
                thumbnailImage.Image = imageXViewMain.Image.Copy();

                // Load and brand the annotations
                Accusoft.NotateXpressSdk.LoadOptions loadNotate;
                loadNotate = new Accusoft.NotateXpressSdk.LoadOptions();
                try
                {
                    notateXpressThumbnail.Layers.FromMemoryStream(anns, loadNotate);
                    notateXpressThumbnail.Layers.Brand(24);
                }
                catch
                {
                    // annotations didn't load, or couldn't find file.
                }
                notateXpressThumbnail.Layers.Clear();

                anns.Close();

                // Add the branded thumbnail image
                addImageToThumbnailMultiPage(thumbnailImage.Image, index, thumbnailXpressMultiPage.SelectedItems[0].Filename);
                thumbnailXpressMultiPage.SelectedItems.Add(thumbnailXpressMultiPage.Items[index]);

                // Remove the old thumbnail item
                thumbnailXpressMultiPage.Items.RemoveAt(index + 1);

                thumbnailImage.Image.Dispose();
                notateXpress1.ItemChanged += new Accusoft.NotateXpressSdk.NotateXpress.ItemChangedEventHandler(this.notateXpress1_ItemChanged);
            }
        }

        private void textToolStripButton_Click(object sender, EventArgs e)
        {
            annotateStart();
            trackIXTool();
            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.TextTool;
        }

        private void signatureToolStripButton_Click(object sender, EventArgs e)
        {
            annotateStart();
            trackIXTool();
            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.ImageTool;
        }

        private void EdittoolStripButton_Click(object sender, EventArgs e)
        {
            trackIXTool();
            if(notateXpress1.Layers.Count > 0)
                notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;
        }
        #endregion // annotations

        private void ViewDemoMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            cleanUpAnnotationFiles();
        }

        private void pictureBoxDownload_Click(object sender, EventArgs e)
        {
            // Navigate to the URL
            System.Diagnostics.Process.Start("http://www.accusoft.com/imagxpress.htm");
        }

        private void panelBackgroundFrameImage_Resize(object sender, EventArgs e)
        {
            panelBackgroundFrameImage.Invalidate();
        }

        private void panelBackgroundFrameThumbnails_Resize(object sender, EventArgs e)
        {
            panelBackgroundFrameThumbnails.Invalidate();
        }

        private void thumbnailXpressFilmStrip_Resize(object sender, EventArgs e)
        {
            // The thumbnail strip should only need to be resized when the size is changed
            // in the thumbnail configuration dialog.
            if (resizeThumbnailStrip == true)
            {
                // Make sure the thumbnail panels are larger than the thumbnail control
                panelGradient1.Width = thumbnailXpressFilmStrip.Width + 20;
                panelBackgroundFrameThumbnails.Width = thumbnailXpressFilmStrip.Width + 54;
                // Move the image panel to the right so that it does not overlap with the thumbnail panel
                panelBackgroundFrameImage.Location = new Point(panelBackgroundFrameThumbnails.Location.X + panelBackgroundFrameThumbnails.Width + 4, panelBackgroundFrameImage.Location.Y);
                // Temporarily remove the anchoring of the main image panel so that the form resizing does not
                // modify the size of the main image panel
                panelBackgroundFrameImage.Anchor = AnchorStyles.None;
                this.Size = new Size(panelBackgroundFrameImage.Width + panelBackgroundFrameThumbnails.Width + 60, this.Size.Height);
                panelBackgroundFrameImage.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
                resizeThumbnailStrip = false;
            }
        }

    }
}

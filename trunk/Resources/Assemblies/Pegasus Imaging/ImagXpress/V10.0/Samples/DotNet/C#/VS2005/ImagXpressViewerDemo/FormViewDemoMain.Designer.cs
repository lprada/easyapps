﻿namespace ImagXpressViewerDemo
{
    partial class ViewDemoMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                if (notateXpress1 != null)
                {
                    notateXpress1.Dispose();
                    notateXpress1 = null;
                }

                if (imageXViewMain.Image != null)
                {
                    imageXViewMain.Image.Dispose();
                    imageXViewMain.Image = null;
                }

                if (imageXViewMain != null)
                {
                    imageXViewMain.Dispose();
                    imageXViewMain = null;
                }

                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }

                if (thumbnailXpressMultiPage != null)
                {
                    thumbnailXpressMultiPage.Dispose();
                    thumbnailXpressMultiPage = null;
                }

                if (notateXpress1 != null)
                {
                    notateXpress1.Dispose();
                    notateXpress1 = null;
                }
            }

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewDemoMain));
            this.thumbnailXpressFilmStrip = new Accusoft.ThumbnailXpressSdk.ThumbnailXpress();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.processor = new Accusoft.ImagXpressSdk.Processor(this.components);
            this.notateXpress1 = new Accusoft.NotateXpressSdk.NotateXpress(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenDirtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageDisplayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoomToRectangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magnifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fitActualSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fitBestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fitWidthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fitHeightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.rotateRightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateLeftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.antiAliasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smoothToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preserveBlackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filmStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutImagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutNotateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutThumbnailXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripSplitButtonMore = new System.Windows.Forms.ToolStripSplitButton();
            this.thumbnailXpressMultiPage = new Accusoft.ThumbnailXpressSdk.ThumbnailXpress();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.pictureBoxIX = new System.Windows.Forms.PictureBox();
            this.labelImageThumbnails = new System.Windows.Forms.Label();
            this.panelBackgroundFrameImage = new AccusoftCustom.PanelBackgroundFrame();
            this.pictureBoxDownload = new System.Windows.Forms.PictureBox();
            this.panelGradientNoFlare1 = new AccusoftCustom.PanelGradientNoFlare();
            this.imageXViewMain = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.panelBackgroundFrameThumbnails = new AccusoftCustom.PanelBackgroundFrame();
            this.panelGradient1 = new AccusoftCustom.PanelGradient();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.panToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.rotateLeftToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.rotateRightToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.zoomInToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.zoomOutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.zoomRectStripButton = new System.Windows.Forms.ToolStripButton();
            this.magnifyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.actualSizeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.bestFitToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.fitWidthToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.fitHeightToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.textToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.rectToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stampToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.signatureToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.editToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIX)).BeginInit();
            this.panelBackgroundFrameImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDownload)).BeginInit();
            this.panelGradientNoFlare1.SuspendLayout();
            this.panelBackgroundFrameThumbnails.SuspendLayout();
            this.panelGradient1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // thumbnailXpressFilmStrip
            // 
            resources.ApplyResources(this.thumbnailXpressFilmStrip, "thumbnailXpressFilmStrip");
            this.thumbnailXpressFilmStrip.BackColor = System.Drawing.Color.Silver;
            this.thumbnailXpressFilmStrip.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpressFilmStrip.BottomMargin = 0;
            this.thumbnailXpressFilmStrip.CameraRaw = true;
            this.thumbnailXpressFilmStrip.CellBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(161)))), ((int)(((byte)(131)))));
            this.thumbnailXpressFilmStrip.CellBorderWidth = 0;
            this.thumbnailXpressFilmStrip.CellHeight = 32;
            this.thumbnailXpressFilmStrip.CellHorizontalSpacing = 10;
            this.thumbnailXpressFilmStrip.CellSpacingColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.thumbnailXpressFilmStrip.CellVerticalSpacing = 10;
            this.thumbnailXpressFilmStrip.CellWidth = 32;
            this.thumbnailXpressFilmStrip.DblClickDirectoryDrillDown = true;
            this.thumbnailXpressFilmStrip.DescriptorAlignment = ((Accusoft.ThumbnailXpressSdk.DescriptorAlignments)((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter | Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpressFilmStrip.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.Default;
            this.thumbnailXpressFilmStrip.EnableAsDragSourceForExternalDragDrop = true;
            this.thumbnailXpressFilmStrip.EnableAsDropTargetForExternalDragDrop = true;
            this.thumbnailXpressFilmStrip.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon;
            this.thumbnailXpressFilmStrip.FtpPassword = "";
            this.thumbnailXpressFilmStrip.FtpUserName = "";
            this.thumbnailXpressFilmStrip.InterComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpressFilmStrip.IntraComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpressFilmStrip.LeftMargin = 8;
            this.thumbnailXpressFilmStrip.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpressFilmStrip.Name = "thumbnailXpressFilmStrip";
            this.thumbnailXpressFilmStrip.PreserveBlack = false;
            this.thumbnailXpressFilmStrip.ProxyServer = "";
            this.thumbnailXpressFilmStrip.RightMargin = 5;
            this.thumbnailXpressFilmStrip.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical;
            this.thumbnailXpressFilmStrip.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(161)))), ((int)(((byte)(131)))));
            this.thumbnailXpressFilmStrip.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
            this.thumbnailXpressFilmStrip.ShowHourglass = true;
            this.thumbnailXpressFilmStrip.ShowImagePlaceholders = false;
            this.thumbnailXpressFilmStrip.TextBackColor = System.Drawing.Color.Silver;
            this.thumbnailXpressFilmStrip.TopMargin = 5;
            this.thumbnailXpressFilmStrip.Error += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.ErrorEventHandler(this.thumbnailXpressFilmStrip_Error);
            this.thumbnailXpressFilmStrip.DragDrop += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.DragDropEventHandler(this.thumbnailXpressFilmStrip_DragDrop);
            this.thumbnailXpressFilmStrip.Resize += new System.EventHandler(this.thumbnailXpressFilmStrip_Resize);
            this.thumbnailXpressFilmStrip.ItemComplete += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.ItemCompleteEventHandler(this.thumbnailXpressFilmStrip_ItemComplete);
            this.thumbnailXpressFilmStrip.SelectedIndexChanged += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.SelectedIndexChangedEventHandler(this.thumbnailXpressFilmStrip_SelectedIndexChanged);
            // 
            // imagXpress1
            // 
            this.imagXpress1.ProgressEvent += new Accusoft.ImagXpressSdk.ImagXpress.ProgressEventHandler(this.imagXpress1_ProgressEvent);
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            this.notateXpress1.ItemChanged += new Accusoft.NotateXpressSdk.NotateXpress.ItemChangedEventHandler(this.notateXpress1_ItemChanged);
            this.notateXpress1.AnnotationAdded += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
            this.notateXpress1.AnnotationDeleted += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
            this.notateXpress1.AnnotationMoved += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationMovedEventHandler(this.notateXpress1_AnnotationMoved);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.imageDisplayToolStripMenuItem,
            this.configureToolStripMenuItem,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.OpenDirtoolStripMenuItem,
            this.toolStripSeparator1,
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // openToolStripMenuItem
            // 
            resources.ApplyResources(this.openToolStripMenuItem, "openToolStripMenuItem");
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click_1);
            // 
            // OpenDirtoolStripMenuItem
            // 
            resources.ApplyResources(this.OpenDirtoolStripMenuItem, "OpenDirtoolStripMenuItem");
            this.OpenDirtoolStripMenuItem.Name = "OpenDirtoolStripMenuItem";
            this.OpenDirtoolStripMenuItem.Click += new System.EventHandler(this.OpenDirtoolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // newToolStripMenuItem
            // 
            resources.ApplyResources(this.newToolStripMenuItem, "newToolStripMenuItem");
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // imageDisplayToolStripMenuItem
            // 
            this.imageDisplayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zoomInToolStripMenuItem,
            this.zoomOutToolStripMenuItem,
            this.zoomToRectangleToolStripMenuItem,
            this.magnifyToolStripMenuItem,
            this.toolStripSeparator2,
            this.fitActualSizeToolStripMenuItem,
            this.fitBestToolStripMenuItem,
            this.fitWidthToolStripMenuItem,
            this.fitHeightToolStripMenuItem,
            this.toolStripSeparator3,
            this.rotateRightToolStripMenuItem,
            this.rotateLeftToolStripMenuItem,
            this.toolStripSeparator4,
            this.antiAliasToolStripMenuItem,
            this.smoothToolStripMenuItem,
            this.preserveBlackToolStripMenuItem});
            this.imageDisplayToolStripMenuItem.Name = "imageDisplayToolStripMenuItem";
            resources.ApplyResources(this.imageDisplayToolStripMenuItem, "imageDisplayToolStripMenuItem");
            // 
            // zoomInToolStripMenuItem
            // 
            this.zoomInToolStripMenuItem.Name = "zoomInToolStripMenuItem";
            resources.ApplyResources(this.zoomInToolStripMenuItem, "zoomInToolStripMenuItem");
            this.zoomInToolStripMenuItem.Click += new System.EventHandler(this.zoomInToolStripMenuItem_Click);
            // 
            // zoomOutToolStripMenuItem
            // 
            this.zoomOutToolStripMenuItem.Name = "zoomOutToolStripMenuItem";
            resources.ApplyResources(this.zoomOutToolStripMenuItem, "zoomOutToolStripMenuItem");
            this.zoomOutToolStripMenuItem.Click += new System.EventHandler(this.zoomOutToolStripMenuItem_Click);
            // 
            // zoomToRectangleToolStripMenuItem
            // 
            this.zoomToRectangleToolStripMenuItem.Name = "zoomToRectangleToolStripMenuItem";
            resources.ApplyResources(this.zoomToRectangleToolStripMenuItem, "zoomToRectangleToolStripMenuItem");
            this.zoomToRectangleToolStripMenuItem.Click += new System.EventHandler(this.zoomToRectangleToolStripMenuItem_Click);
            // 
            // magnifyToolStripMenuItem
            // 
            this.magnifyToolStripMenuItem.Name = "magnifyToolStripMenuItem";
            resources.ApplyResources(this.magnifyToolStripMenuItem, "magnifyToolStripMenuItem");
            this.magnifyToolStripMenuItem.Click += new System.EventHandler(this.magnifyToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // fitActualSizeToolStripMenuItem
            // 
            this.fitActualSizeToolStripMenuItem.Name = "fitActualSizeToolStripMenuItem";
            resources.ApplyResources(this.fitActualSizeToolStripMenuItem, "fitActualSizeToolStripMenuItem");
            this.fitActualSizeToolStripMenuItem.Click += new System.EventHandler(this.fitActualSizeToolStripMenuItem_Click);
            // 
            // fitBestToolStripMenuItem
            // 
            this.fitBestToolStripMenuItem.Name = "fitBestToolStripMenuItem";
            resources.ApplyResources(this.fitBestToolStripMenuItem, "fitBestToolStripMenuItem");
            this.fitBestToolStripMenuItem.Click += new System.EventHandler(this.fitBestToolStripMenuItem_Click);
            // 
            // fitWidthToolStripMenuItem
            // 
            this.fitWidthToolStripMenuItem.Name = "fitWidthToolStripMenuItem";
            resources.ApplyResources(this.fitWidthToolStripMenuItem, "fitWidthToolStripMenuItem");
            this.fitWidthToolStripMenuItem.Click += new System.EventHandler(this.fitWidthToolStripMenuItem_Click);
            // 
            // fitHeightToolStripMenuItem
            // 
            this.fitHeightToolStripMenuItem.Name = "fitHeightToolStripMenuItem";
            resources.ApplyResources(this.fitHeightToolStripMenuItem, "fitHeightToolStripMenuItem");
            this.fitHeightToolStripMenuItem.Click += new System.EventHandler(this.fitHeightToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // rotateRightToolStripMenuItem
            // 
            this.rotateRightToolStripMenuItem.Name = "rotateRightToolStripMenuItem";
            resources.ApplyResources(this.rotateRightToolStripMenuItem, "rotateRightToolStripMenuItem");
            this.rotateRightToolStripMenuItem.Click += new System.EventHandler(this.rotateRightToolStripMenuItem_Click);
            // 
            // rotateLeftToolStripMenuItem
            // 
            this.rotateLeftToolStripMenuItem.Name = "rotateLeftToolStripMenuItem";
            resources.ApplyResources(this.rotateLeftToolStripMenuItem, "rotateLeftToolStripMenuItem");
            this.rotateLeftToolStripMenuItem.Click += new System.EventHandler(this.rotateLeftToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // antiAliasToolStripMenuItem
            // 
            this.antiAliasToolStripMenuItem.Checked = true;
            this.antiAliasToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.antiAliasToolStripMenuItem.Name = "antiAliasToolStripMenuItem";
            resources.ApplyResources(this.antiAliasToolStripMenuItem, "antiAliasToolStripMenuItem");
            this.antiAliasToolStripMenuItem.Click += new System.EventHandler(this.antiAliasToolStripMenuItem_Click);
            // 
            // smoothToolStripMenuItem
            // 
            this.smoothToolStripMenuItem.Checked = true;
            this.smoothToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.smoothToolStripMenuItem.Name = "smoothToolStripMenuItem";
            resources.ApplyResources(this.smoothToolStripMenuItem, "smoothToolStripMenuItem");
            this.smoothToolStripMenuItem.Click += new System.EventHandler(this.smoothingToolStripMenuItem_Click);
            // 
            // preserveBlackToolStripMenuItem
            // 
            this.preserveBlackToolStripMenuItem.Name = "preserveBlackToolStripMenuItem";
            resources.ApplyResources(this.preserveBlackToolStripMenuItem, "preserveBlackToolStripMenuItem");
            this.preserveBlackToolStripMenuItem.Click += new System.EventHandler(this.preserveBlackToolStripMenuItem_Click);
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filmStripToolStripMenuItem,
            this.multiPageToolStripMenuItem,
            this.viewerToolStripMenuItem});
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            resources.ApplyResources(this.configureToolStripMenuItem, "configureToolStripMenuItem");
            // 
            // filmStripToolStripMenuItem
            // 
            this.filmStripToolStripMenuItem.Name = "filmStripToolStripMenuItem";
            resources.ApplyResources(this.filmStripToolStripMenuItem, "filmStripToolStripMenuItem");
            this.filmStripToolStripMenuItem.Click += new System.EventHandler(this.filmStripToolStripMenuItem_Click);
            // 
            // multiPageToolStripMenuItem
            // 
            this.multiPageToolStripMenuItem.Name = "multiPageToolStripMenuItem";
            resources.ApplyResources(this.multiPageToolStripMenuItem, "multiPageToolStripMenuItem");
            this.multiPageToolStripMenuItem.Click += new System.EventHandler(this.multiPageToolStripMenuItem_Click);
            // 
            // viewerToolStripMenuItem
            // 
            this.viewerToolStripMenuItem.Name = "viewerToolStripMenuItem";
            resources.ApplyResources(this.viewerToolStripMenuItem, "viewerToolStripMenuItem");
            this.viewerToolStripMenuItem.Click += new System.EventHandler(this.viewerToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutImagXpressToolStripMenuItem,
            this.aboutNotateXpressToolStripMenuItem,
            this.aboutThumbnailXpressToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            // 
            // aboutImagXpressToolStripMenuItem
            // 
            this.aboutImagXpressToolStripMenuItem.Name = "aboutImagXpressToolStripMenuItem";
            resources.ApplyResources(this.aboutImagXpressToolStripMenuItem, "aboutImagXpressToolStripMenuItem");
            this.aboutImagXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutImagXpressToolStripMenuItem_Click);
            // 
            // aboutNotateXpressToolStripMenuItem
            // 
            this.aboutNotateXpressToolStripMenuItem.Name = "aboutNotateXpressToolStripMenuItem";
            resources.ApplyResources(this.aboutNotateXpressToolStripMenuItem, "aboutNotateXpressToolStripMenuItem");
            this.aboutNotateXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutNotateXpressToolStripMenuItem_Click);
            // 
            // aboutThumbnailXpressToolStripMenuItem
            // 
            this.aboutThumbnailXpressToolStripMenuItem.Name = "aboutThumbnailXpressToolStripMenuItem";
            resources.ApplyResources(this.aboutThumbnailXpressToolStripMenuItem, "aboutThumbnailXpressToolStripMenuItem");
            this.aboutThumbnailXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutThumbnailXpressToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripSplitButtonMore});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            resources.ApplyResources(this.toolStripStatusLabel, "toolStripStatusLabel");
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            resources.ApplyResources(this.toolStripStatusLabel2, "toolStripStatusLabel2");
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            resources.ApplyResources(this.toolStripStatusLabel3, "toolStripStatusLabel3");
            // 
            // toolStripSplitButtonMore
            // 
            resources.ApplyResources(this.toolStripSplitButtonMore, "toolStripSplitButtonMore");
            this.toolStripSplitButtonMore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButtonMore.Name = "toolStripSplitButtonMore";
            this.toolStripSplitButtonMore.ButtonClick += new System.EventHandler(this.toolStripSplitButtonMore_ButtonClick);
            // 
            // thumbnailXpressMultiPage
            // 
            this.thumbnailXpressMultiPage.BackColor = System.Drawing.Color.Silver;
            this.thumbnailXpressMultiPage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpressMultiPage.BottomMargin = 5;
            this.thumbnailXpressMultiPage.CameraRaw = false;
            this.thumbnailXpressMultiPage.CellBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(161)))), ((int)(((byte)(131)))));
            this.thumbnailXpressMultiPage.CellBorderWidth = 0;
            this.thumbnailXpressMultiPage.CellHeight = 80;
            this.thumbnailXpressMultiPage.CellHorizontalSpacing = 5;
            this.thumbnailXpressMultiPage.CellSpacingColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.thumbnailXpressMultiPage.CellVerticalSpacing = 5;
            this.thumbnailXpressMultiPage.CellWidth = 80;
            this.thumbnailXpressMultiPage.DblClickDirectoryDrillDown = true;
            this.thumbnailXpressMultiPage.DescriptorAlignment = ((Accusoft.ThumbnailXpressSdk.DescriptorAlignments)((Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignCenter | Accusoft.ThumbnailXpressSdk.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpressMultiPage.DescriptorDisplayMethod = Accusoft.ThumbnailXpressSdk.DescriptorDisplayMethods.Default;
            this.thumbnailXpressMultiPage.EnableAsDragSourceForExternalDragDrop = true;
            this.thumbnailXpressMultiPage.EnableAsDropTargetForExternalDragDrop = true;
            this.thumbnailXpressMultiPage.ErrorAction = Accusoft.ThumbnailXpressSdk.ErrorAction.UseErrorIcon;
            this.thumbnailXpressMultiPage.FtpPassword = "";
            this.thumbnailXpressMultiPage.FtpUserName = "";
            this.thumbnailXpressMultiPage.InterComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpressMultiPage.IntraComponentThumbnailDragDropEnabled = true;
            this.thumbnailXpressMultiPage.LeftMargin = 21;
            resources.ApplyResources(this.thumbnailXpressMultiPage, "thumbnailXpressMultiPage");
            this.thumbnailXpressMultiPage.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpressMultiPage.Name = "thumbnailXpressMultiPage";
            this.thumbnailXpressMultiPage.PreserveBlack = false;
            this.thumbnailXpressMultiPage.ProxyServer = "";
            this.thumbnailXpressMultiPage.RightMargin = 5;
            this.thumbnailXpressMultiPage.ScrollDirection = Accusoft.ThumbnailXpressSdk.ScrollDirection.Vertical;
            this.thumbnailXpressMultiPage.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(161)))), ((int)(((byte)(131)))));
            this.thumbnailXpressMultiPage.SelectionMode = Accusoft.ThumbnailXpressSdk.SelectionMode.MultiExtended;
            this.thumbnailXpressMultiPage.ShowHourglass = true;
            this.thumbnailXpressMultiPage.ShowImagePlaceholders = false;
            this.thumbnailXpressMultiPage.TextBackColor = System.Drawing.Color.Silver;
            this.thumbnailXpressMultiPage.TopMargin = 5;
            this.thumbnailXpressMultiPage.SelectedIndexChanged += new Accusoft.ThumbnailXpressSdk.ThumbnailXpressEvents.SelectedIndexChangedEventHandler(this.thumbnailXpressMultiPage_SelectedIndexChanged);
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // pictureBoxIX
            // 
            this.pictureBoxIX.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxIX.BackgroundImage = global::ImagXpressViewerDemo.Properties.Resources.demo_prodtitle_imagxpress;
            resources.ApplyResources(this.pictureBoxIX, "pictureBoxIX");
            this.pictureBoxIX.Name = "pictureBoxIX";
            this.pictureBoxIX.TabStop = false;
            // 
            // labelImageThumbnails
            // 
            resources.ApplyResources(this.labelImageThumbnails, "labelImageThumbnails");
            this.labelImageThumbnails.BackColor = System.Drawing.Color.Transparent;
            this.labelImageThumbnails.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelImageThumbnails.Name = "labelImageThumbnails";
            // 
            // panelBackgroundFrameImage
            // 
            resources.ApplyResources(this.panelBackgroundFrameImage, "panelBackgroundFrameImage");
            this.panelBackgroundFrameImage.Controls.Add(this.pictureBoxDownload);
            this.panelBackgroundFrameImage.Controls.Add(this.pictureBoxIX);
            this.panelBackgroundFrameImage.Controls.Add(this.panelGradientNoFlare1);
            this.panelBackgroundFrameImage.MaximumSize = new System.Drawing.Size(1600, 1600);
            this.panelBackgroundFrameImage.Name = "panelBackgroundFrameImage";
            this.panelBackgroundFrameImage.Resize += new System.EventHandler(this.panelBackgroundFrameImage_Resize);
            // 
            // pictureBoxDownload
            // 
            resources.ApplyResources(this.pictureBoxDownload, "pictureBoxDownload");
            this.pictureBoxDownload.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxDownload.BackgroundImage = global::ImagXpressViewerDemo.Properties.Resources.btn_download_sdk;
            this.pictureBoxDownload.Name = "pictureBoxDownload";
            this.pictureBoxDownload.TabStop = false;
            this.pictureBoxDownload.Click += new System.EventHandler(this.pictureBoxDownload_Click);
            // 
            // panelGradientNoFlare1
            // 
            resources.ApplyResources(this.panelGradientNoFlare1, "panelGradientNoFlare1");
            this.panelGradientNoFlare1.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlare1.Controls.Add(this.imageXViewMain);
            this.panelGradientNoFlare1.Controls.Add(this.thumbnailXpressMultiPage);
            this.panelGradientNoFlare1.MaximumSize = new System.Drawing.Size(1600, 1600);
            this.panelGradientNoFlare1.Name = "panelGradientNoFlare1";
            // 
            // imageXViewMain
            // 
            resources.ApplyResources(this.imageXViewMain, "imageXViewMain");
            this.imageXViewMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.imageXViewMain.Maximum = new System.Drawing.Size(1600, 1600);
            this.imageXViewMain.Name = "imageXViewMain";
            this.imageXViewMain.MouseClick += new System.Windows.Forms.MouseEventHandler(this.imageXViewMain_MouseClick);
            this.imageXViewMain.DragDrop += new System.Windows.Forms.DragEventHandler(this.imageXView1_DragDrop);
            this.imageXViewMain.DragEnter += new System.Windows.Forms.DragEventHandler(this.imageXView1_DragEnter);
            this.imageXViewMain.ToolEvent += new Accusoft.ImagXpressSdk.ImageXView.ToolEventHandler(this.imageXViewMain_ToolEvent);
            this.imageXViewMain.MouseLeave += new System.EventHandler(this.imageXViewMain_MouseLeave);
            this.imageXViewMain.Click += new System.EventHandler(this.imageXView1_Click);
            // 
            // panelBackgroundFrameThumbnails
            // 
            resources.ApplyResources(this.panelBackgroundFrameThumbnails, "panelBackgroundFrameThumbnails");
            this.panelBackgroundFrameThumbnails.Controls.Add(this.panelGradient1);
            this.panelBackgroundFrameThumbnails.MaximumSize = new System.Drawing.Size(600, 1600);
            this.panelBackgroundFrameThumbnails.Name = "panelBackgroundFrameThumbnails";
            this.panelBackgroundFrameThumbnails.Resize += new System.EventHandler(this.panelBackgroundFrameThumbnails_Resize);
            // 
            // panelGradient1
            // 
            resources.ApplyResources(this.panelGradient1, "panelGradient1");
            this.panelGradient1.BackColor = System.Drawing.Color.Transparent;
            this.panelGradient1.Controls.Add(this.labelImageThumbnails);
            this.panelGradient1.MaximumSize = new System.Drawing.Size(600, 1600);
            this.panelGradient1.Name = "panelGradient1";
            // 
            // tableLayoutPanel4
            // 
            resources.ApplyResources(this.tableLayoutPanel4, "tableLayoutPanel4");
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Transparent;
            this.toolStrip1.BackgroundImage = global::ImagXpressViewerDemo.Properties.Resources.toolstrip_background_2;
            this.toolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.panToolStripButton,
            this.toolStripSeparator6,
            this.rotateLeftToolStripButton,
            this.rotateRightToolStripButton,
            this.toolStripSeparator9,
            this.zoomInToolStripButton,
            this.zoomOutToolStripButton,
            this.zoomRectStripButton,
            this.magnifyToolStripButton,
            this.toolStripSeparator7,
            this.actualSizeToolStripButton,
            this.bestFitToolStripButton,
            this.fitWidthToolStripButton,
            this.fitHeightToolStripButton,
            this.toolStripSeparator10,
            this.textToolStripButton,
            this.rectToolStripButton,
            this.stampToolStripButton,
            this.signatureToolStripButton,
            this.editToolStripButton});
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.MaximumSize = new System.Drawing.Size(1600, 67);
            this.toolStrip1.Name = "toolStrip1";
            // 
            // newToolStripButton
            // 
            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_reset;
            resources.ApplyResources(this.newToolStripButton, "newToolStripButton");
            this.newToolStripButton.Margin = new System.Windows.Forms.Padding(0, 0, -1, 12);
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Click += new System.EventHandler(this.newToolStripButton_Click);
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_open;
            resources.ApplyResources(this.openToolStripButton, "openToolStripButton");
            this.openToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 12);
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Click += new System.EventHandler(this.openToolStripButton_Click);
            // 
            // panToolStripButton
            // 
            this.panToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.panToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_hand;
            resources.ApplyResources(this.panToolStripButton, "panToolStripButton");
            this.panToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 12);
            this.panToolStripButton.Name = "panToolStripButton";
            this.panToolStripButton.Click += new System.EventHandler(this.panToolStripButton_Click);
            // 
            // toolStripSeparator6
            // 
            resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            // 
            // rotateLeftToolStripButton
            // 
            this.rotateLeftToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rotateLeftToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_rotate_left;
            resources.ApplyResources(this.rotateLeftToolStripButton, "rotateLeftToolStripButton");
            this.rotateLeftToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.rotateLeftToolStripButton.Name = "rotateLeftToolStripButton";
            this.rotateLeftToolStripButton.Click += new System.EventHandler(this.rotateLeftToolStripButton_Click);
            // 
            // rotateRightToolStripButton
            // 
            this.rotateRightToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rotateRightToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_rotate_right;
            resources.ApplyResources(this.rotateRightToolStripButton, "rotateRightToolStripButton");
            this.rotateRightToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.rotateRightToolStripButton.Name = "rotateRightToolStripButton";
            this.rotateRightToolStripButton.Click += new System.EventHandler(this.rotateRightToolStripButton_Click);
            // 
            // toolStripSeparator9
            // 
            resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            // 
            // zoomInToolStripButton
            // 
            this.zoomInToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomInToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_zoom_in;
            resources.ApplyResources(this.zoomInToolStripButton, "zoomInToolStripButton");
            this.zoomInToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.zoomInToolStripButton.Name = "zoomInToolStripButton";
            this.zoomInToolStripButton.Click += new System.EventHandler(this.zoomInToolStripButton_Click);
            // 
            // zoomOutToolStripButton
            // 
            this.zoomOutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomOutToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_zoom_out;
            resources.ApplyResources(this.zoomOutToolStripButton, "zoomOutToolStripButton");
            this.zoomOutToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.zoomOutToolStripButton.Name = "zoomOutToolStripButton";
            this.zoomOutToolStripButton.Click += new System.EventHandler(this.zoomOutToolStripButton_Click);
            // 
            // zoomRectStripButton
            // 
            this.zoomRectStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomRectStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_zoom_select;
            resources.ApplyResources(this.zoomRectStripButton, "zoomRectStripButton");
            this.zoomRectStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.zoomRectStripButton.Name = "zoomRectStripButton";
            this.zoomRectStripButton.Click += new System.EventHandler(this.zoomRectStripButton_Click);
            // 
            // magnifyToolStripButton
            // 
            this.magnifyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.magnifyToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_magnify;
            resources.ApplyResources(this.magnifyToolStripButton, "magnifyToolStripButton");
            this.magnifyToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.magnifyToolStripButton.Name = "magnifyToolStripButton";
            this.magnifyToolStripButton.Click += new System.EventHandler(this.magnifyToolStripButton_Click);
            // 
            // toolStripSeparator7
            // 
            resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            // 
            // actualSizeToolStripButton
            // 
            this.actualSizeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.actualSizeToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_actual_size;
            resources.ApplyResources(this.actualSizeToolStripButton, "actualSizeToolStripButton");
            this.actualSizeToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.actualSizeToolStripButton.Name = "actualSizeToolStripButton";
            this.actualSizeToolStripButton.Click += new System.EventHandler(this.actualSizeToolStripButton_Click);
            // 
            // bestFitToolStripButton
            // 
            this.bestFitToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bestFitToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_fit_best;
            resources.ApplyResources(this.bestFitToolStripButton, "bestFitToolStripButton");
            this.bestFitToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.bestFitToolStripButton.Name = "bestFitToolStripButton";
            this.bestFitToolStripButton.Click += new System.EventHandler(this.bestFitToolStripButton_Click);
            // 
            // fitWidthToolStripButton
            // 
            this.fitWidthToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fitWidthToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_fit_width;
            resources.ApplyResources(this.fitWidthToolStripButton, "fitWidthToolStripButton");
            this.fitWidthToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.fitWidthToolStripButton.Name = "fitWidthToolStripButton";
            this.fitWidthToolStripButton.Click += new System.EventHandler(this.fitWidthToolStripButton_Click);
            // 
            // fitHeightToolStripButton
            // 
            this.fitHeightToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fitHeightToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_fit_height;
            resources.ApplyResources(this.fitHeightToolStripButton, "fitHeightToolStripButton");
            this.fitHeightToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.fitHeightToolStripButton.Name = "fitHeightToolStripButton";
            this.fitHeightToolStripButton.Click += new System.EventHandler(this.fitHeightToolStripButton_Click);
            // 
            // toolStripSeparator10
            // 
            resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            // 
            // textToolStripButton
            // 
            this.textToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.textToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_ann_text;
            resources.ApplyResources(this.textToolStripButton, "textToolStripButton");
            this.textToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.textToolStripButton.Name = "textToolStripButton";
            this.textToolStripButton.Click += new System.EventHandler(this.textToolStripButton_Click);
            // 
            // rectToolStripButton
            // 
            this.rectToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.rectToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_ann_highlight;
            resources.ApplyResources(this.rectToolStripButton, "rectToolStripButton");
            this.rectToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.rectToolStripButton.Name = "rectToolStripButton";
            this.rectToolStripButton.Click += new System.EventHandler(this.RecttoolStripButton_Click);
            // 
            // stampToolStripButton
            // 
            this.stampToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stampToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_ann_stamp;
            resources.ApplyResources(this.stampToolStripButton, "stampToolStripButton");
            this.stampToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.stampToolStripButton.Name = "stampToolStripButton";
            this.stampToolStripButton.Click += new System.EventHandler(this.StamptoolStripButton_Click);
            // 
            // signatureToolStripButton
            // 
            this.signatureToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.signatureToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_ann_signature;
            resources.ApplyResources(this.signatureToolStripButton, "signatureToolStripButton");
            this.signatureToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 15);
            this.signatureToolStripButton.Name = "signatureToolStripButton";
            this.signatureToolStripButton.Click += new System.EventHandler(this.signatureToolStripButton_Click);
            // 
            // editToolStripButton
            // 
            this.editToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editToolStripButton.Image = global::ImagXpressViewerDemo.Properties.Resources.demo_btn_ann_edit;
            resources.ApplyResources(this.editToolStripButton, "editToolStripButton");
            this.editToolStripButton.Margin = new System.Windows.Forms.Padding(-1, 0, -1, 14);
            this.editToolStripButton.Name = "editToolStripButton";
            this.editToolStripButton.Click += new System.EventHandler(this.EdittoolStripButton_Click);
            // 
            // ViewDemoMain
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(54)))), ((int)(((byte)(45)))));
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.thumbnailXpressFilmStrip);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelBackgroundFrameThumbnails);
            this.Controls.Add(this.panelBackgroundFrameImage);
            this.Controls.Add(this.statusStrip1);
            this.KeyPreview = true;
            this.Name = "ViewDemoMain";
            this.Load += new System.EventHandler(this.ViewDemoMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewDemoMain_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ViewDemoMain_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIX)).EndInit();
            this.panelBackgroundFrameImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDownload)).EndInit();
            this.panelGradientNoFlare1.ResumeLayout(false);
            this.panelBackgroundFrameThumbnails.ResumeLayout(false);
            this.panelGradient1.ResumeLayout(false);
            this.panelGradient1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

}

        #endregion

        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.Processor processor;
        private Accusoft.NotateXpressSdk.NotateXpress notateXpress1;
        private Accusoft.ThumbnailXpressSdk.ThumbnailXpress thumbnailXpressFilmStrip;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutImagXpressToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButtonMore;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private Accusoft.ThumbnailXpressSdk.ThumbnailXpress thumbnailXpressMultiPage;
        private System.Windows.Forms.ToolStripMenuItem OpenDirtoolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ToolStripMenuItem imageDisplayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoomToRectangleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem magnifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem fitActualSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fitBestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fitWidthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fitHeightToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem rotateRightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateLeftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filmStripToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewerToolStripMenuItem;
        private System.Windows.Forms.Label labelImageThumbnails;
        private System.Windows.Forms.PictureBox pictureBoxIX;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrameImage;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrameThumbnails;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlare1;
        private AccusoftCustom.PanelGradient panelGradient1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem antiAliasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smoothToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preserveBlackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutNotateXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutThumbnailXpressToolStripMenuItem;
        private Accusoft.ImagXpressSdk.ImageXView imageXViewMain;
        private System.Windows.Forms.PictureBox pictureBoxDownload;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton panToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton rotateLeftToolStripButton;
        private System.Windows.Forms.ToolStripButton rotateRightToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton zoomInToolStripButton;
        private System.Windows.Forms.ToolStripButton zoomOutToolStripButton;
        private System.Windows.Forms.ToolStripButton zoomRectStripButton;
        private System.Windows.Forms.ToolStripButton magnifyToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton actualSizeToolStripButton;
        private System.Windows.Forms.ToolStripButton bestFitToolStripButton;
        private System.Windows.Forms.ToolStripButton fitWidthToolStripButton;
        private System.Windows.Forms.ToolStripButton fitHeightToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton textToolStripButton;
        private System.Windows.Forms.ToolStripButton rectToolStripButton;
        private System.Windows.Forms.ToolStripButton stampToolStripButton;
        private System.Windows.Forms.ToolStripButton signatureToolStripButton;
        private System.Windows.Forms.ToolStripButton editToolStripButton;
    }
}


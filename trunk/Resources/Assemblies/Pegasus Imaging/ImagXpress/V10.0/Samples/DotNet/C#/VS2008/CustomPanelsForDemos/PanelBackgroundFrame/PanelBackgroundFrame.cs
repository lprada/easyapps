using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//
using System.Drawing.Drawing2D;

namespace AccusoftCustom
{
    public partial class PanelBackgroundFrame : Panel
    {
        public PanelBackgroundFrame()
        {
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint | ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.SupportsTransparentBackColor, true);
            UpdateStyles();
        }

        public override Rectangle DisplayRectangle
        {
            get
            {
                Rectangle BaseRectangle = ClientRectangle;
                BaseRectangle.Inflate(-20, -20);
                return BaseRectangle;
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {

            pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            Pen myPen = new Pen(Color.FromArgb(150, 150, 136));
            myPen.Width = 4;

            RoundRectangle(pe.Graphics, myPen, 5, 5, this.Width - 10, this.Height - 10, 12);



            Pen myPen2 = new Pen(Color.FromArgb(0, 0, 0));
            myPen2.Width = 1;

            RoundRectangle(pe.Graphics, myPen2, 7, 7, this.Width - 14, this.Height - 14, 12);


            // Only do the light-to-dark effect if 32-bit because certain controls do
            // not paint well if the screen is 16-bit.
            if (Screen.PrimaryScreen.BitsPerPixel >= 32)
            {
                Rectangle BaseRectangle = new Rectangle(0, 0, this.Width, this.Height);

                Brush Gradient_Brush =
                    new LinearGradientBrush(
                    BaseRectangle,
                    Color.FromArgb(50, 255, 255, 255), Color.FromArgb(70, 0, 0, 0), 90);


                FillRoundRectangle(pe.Graphics, Gradient_Brush, 8, 8, this.Width - 16, this.Height - 16, 9, 9);
            }
        }


        public void RoundRectangle(Graphics objG, Pen objP, float h, float v, float width, float height, float radius)
        {
            GraphicsPath objGP = new GraphicsPath(); 
            objGP.AddLine(h + radius, v, h + width - (radius * 2), v); 
            objGP.AddArc(h + width - (radius * 2), v, radius * 2, radius * 2, 270, 90); 
            objGP.AddLine(h + width, v + radius, h + width, v + height - (radius * 2)); 
            objGP.AddArc(h + width - (radius * 2), v + height - (radius * 2), radius * 2, radius * 2, 0, 90); // Corner
            objGP.AddLine(h + width - (radius * 2), v + height, h + radius, v + height);
            objGP.AddArc(h, v + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            objGP.AddLine(h, v + height - (radius * 2), h, v + radius);
            objGP.AddArc(h, v, radius * 2, radius * 2, 180, 90);
            objGP.CloseFigure();
            objG.DrawPath(objP, objGP);
            objGP.Dispose();
        }






        #region Public Methods
        public static void FillRoundRectangle(Graphics g, Brush brush, int x, int y, int width, int height, int radiusX, int radiusY)
        {
            FillRoundRectangle(g, brush, (float)x, (float)y, (float)width, (float)height, (float)radiusX, (float)radiusY);
        }
        public static void FillRoundRectangle(Graphics g, Brush brush, float x, float y, float width, float height, float radiusX, float radiusY)
        {
            if (g != null)
            {
                RectangleF rectangle = new RectangleF(x, y, width, height);
                GraphicsPath path = GetRoundedRect(rectangle, radiusX, radiusY);
                g.FillPath(brush, path);
            }
        }
        public static void DrawRoundRectangle(Graphics g, Pen pen, int x, int y, int width, int height, int radiusX, int radiusY)
        {
            DrawRoundRectangle(g, pen, (float)x, (float)y, (float)width, (float)height, (float)radiusX, (float)radiusY);
        }
        public static void DrawRoundRectangle(Graphics g, Pen pen, float x, float y, float width, float height, float radiusX, float radiusY)
        {
            if (g != null)
            {
                RectangleF rectangle = new RectangleF(x, y, width, height); GraphicsPath path = GetRoundedRect(rectangle, radiusX, radiusY);
                g.DrawPath(pen, path);
            }
        }
        #endregion
        #region Private Methods
        private static GraphicsPath GetRoundedRect(RectangleF baseRect, float radiusX, float radiusY)
        {
            // if corner radius is less than or equal to zero,             
            // return the original rectangle
            if (radiusX <= 0.0F || radiusY <= 0.0F)
            {
                GraphicsPath mPath = new GraphicsPath();
                mPath.AddRectangle(baseRect);
                mPath.CloseFigure();
                return mPath;
            }
            // if the corner radius is greater than or equal to
            // half the width, or height (whichever is shorter)
            // then return capsule
            if (radiusX >= (Math.Min(baseRect.Width, baseRect.Height)) / 2.0)
                return GetCapsule(baseRect);
            if (radiusY >= (Math.Min(baseRect.Width, baseRect.Height)) / 2.0)
                return GetCapsule(baseRect);
            // create the arc for the rectangle sides and declare
            // a graphics path object for the drawing
            float diameterX = radiusX * 2.0F;
            float diameterY = radiusY * 2.0F;
            SizeF sizeF = new SizeF(diameterX, diameterY);
            RectangleF arc = new RectangleF(baseRect.Location, sizeF);
            GraphicsPath path = new GraphicsPath();
            // top left arc
            path.AddArc(arc, 180, 90);
            // top right arc
            arc.X = baseRect.Right - diameterX;
            path.AddArc(arc, 270, 90);
            // bottom right arc
            arc.Y = baseRect.Bottom - diameterY;
            path.AddArc(arc, 0, 90);
            // bottom left arc
            arc.X = baseRect.Left;
            path.AddArc(arc, 90, 90);
            path.CloseFigure();
            return path;
        }
        private static GraphicsPath GetCapsule(RectangleF baseRect)
        {
            GraphicsPath path = new GraphicsPath();
            try
            {
                if (baseRect.Width > baseRect.Height)
                {
                    // return horizontal capsule
                    float diameter = baseRect.Height;
                    SizeF sizeF = new SizeF(diameter, diameter);
                    RectangleF arc = new RectangleF(baseRect.Location, sizeF);
                    path.AddArc(arc, 90, 180);
                    arc.X = baseRect.Right - diameter;
                    path.AddArc(arc, 270, 180);
                }
                else if (baseRect.Width < baseRect.Height)
                {
                    // return vertical capsule 
                    float diameter = baseRect.Width;
                    SizeF sizeF = new SizeF(diameter, diameter);
                    RectangleF arc = new RectangleF(baseRect.Location, sizeF);
                    path.AddArc(arc, 180, 180);
                    arc.Y = baseRect.Bottom - diameter;
                    path.AddArc(arc, 0, 180);
                }
                else
                {
                    // return circle 
                    path.AddEllipse(baseRect);
                }
            }
            catch
            {
                path.AddEllipse(baseRect);
            }
            finally
            {
                path.CloseFigure();
            }
            return path;
        }
        #endregion

    }
}

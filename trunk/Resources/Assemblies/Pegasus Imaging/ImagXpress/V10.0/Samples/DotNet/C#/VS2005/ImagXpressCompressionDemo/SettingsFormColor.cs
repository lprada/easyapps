﻿/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accusoft.ImagXpressSdk;

namespace ImagXpressCompressionDemo
{
	public partial class SettingsFormColor : Form
	{
        SaveOptions mySaveOptionsCurrent;
        SaveOptions mySaveOptions1;
        SaveOptions mySaveOptions2;
        SaveOptions mySaveOptions3;
        SaveOptions mySaveOptions4;
        MainForm mainForm;
        bool settingsMatchImage = true;
        bool imageHasBeenModified = false;
        // When the form is first being loaded, update should be disabled
        // because the settings are not being changed at that time.
        bool enableUpdate = false;
        CompressedImage compressedImageCurrent;
        CompressedImage compressedImage1;
        CompressedImage compressedImage2;
        CompressedImage compressedImage3;
        CompressedImage compressedImage4;

        public SettingsFormColor(MainForm theMainForm, CompressedImage theCompressedImage1, CompressedImage theCompressedImage2, CompressedImage theCompressedImage3, CompressedImage theCompressedImage4)
		{
            InitializeComponent();

            // The image has not been modified yet.
            imageHasBeenModified = false;
            settingsMatchImage = true;

            compressedImage1 = theCompressedImage1;
            compressedImage2 = theCompressedImage2;
            compressedImage3 = theCompressedImage3;
            compressedImage4 = theCompressedImage4;

            mainForm = theMainForm;
            mainForm.EnableSettings(false);

            SetAllSaveOptions();

            SelectCompressedImage();
        }

        #region Load Compression Settings

        private void SettingsFormColor_Load(object sender, EventArgs e)
        {
            this.Text = "Compressed Image Settings (Color)";
        }

        private void buttonRestoreDefaults_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("This will reset the color, grayscale, and black and white compression settings for all 4 compressed images to their default values and cannot be undone.  Are you sure you want to restore all defaults?", "Confirm Restore All Defaults", MessageBoxButtons.OKCancel);
            if (res == DialogResult.OK)
            {
                mainForm.SetDefaultCompressionSettings();
                mainForm.ResetCompressedImages();
                mainForm.CompressImages();
                SetAllSaveOptions();
                // Update current compressed image and options
                if (radioButtonCompressedImage1.Checked == true)
                {
                    compressedImageCurrent = compressedImage1;
                    mySaveOptionsCurrent = mySaveOptions1;
                }
                else if (radioButtonCompressedImage2.Checked == true)
                {
                    compressedImageCurrent = compressedImage2;
                    mySaveOptionsCurrent = mySaveOptions2;
                }
                else if (radioButtonCompressedImage3.Checked == true)
                {
                    compressedImageCurrent = compressedImage3;
                    mySaveOptionsCurrent = mySaveOptions3;
                }
                else
                {
                    compressedImageCurrent = compressedImage4;
                    mySaveOptionsCurrent = mySaveOptions4;
                }
                DisplaySettings();
            }
        }

        private void SetAllSaveOptions()
        {
            mySaveOptions1 = new SaveOptions();
            SetSaveOptions(compressedImage1.GetSaveOptionsColor(), mySaveOptions1);
            mySaveOptions2 = new SaveOptions();
            SetSaveOptions(compressedImage2.GetSaveOptionsColor(), mySaveOptions2);
            mySaveOptions3 = new SaveOptions();
            SetSaveOptions(compressedImage3.GetSaveOptionsColor(), mySaveOptions3);
            mySaveOptions4 = new SaveOptions();
            SetSaveOptions(compressedImage4.GetSaveOptionsColor(), mySaveOptions4);
        }

        private void SetSaveOptions(SaveOptions compressedImageSaveOptions, SaveOptions settingsSaveOptions)
        {
            // Copy the save options.
            settingsSaveOptions.Format = compressedImageSaveOptions.Format;
            settingsSaveOptions.Jpeg.Chrominance = compressedImageSaveOptions.Jpeg.Chrominance;
            settingsSaveOptions.Jpeg.Luminance = compressedImageSaveOptions.Jpeg.Luminance;
            settingsSaveOptions.Jpeg.Grayscale = compressedImageSaveOptions.Jpeg.Grayscale;
            settingsSaveOptions.Jpeg.Cosited = compressedImageSaveOptions.Jpeg.Cosited;
            settingsSaveOptions.Jpeg.SubSampling = compressedImageSaveOptions.Jpeg.SubSampling;
            settingsSaveOptions.Jpeg.ColorSpace = compressedImageSaveOptions.Jpeg.ColorSpace;
            settingsSaveOptions.Jpeg.Progressive = compressedImageSaveOptions.Jpeg.Progressive;
            settingsSaveOptions.Tiff.Compression = compressedImageSaveOptions.Tiff.Compression;
            settingsSaveOptions.Exif.ThumbnailSize = compressedImageSaveOptions.Exif.ThumbnailSize;
            settingsSaveOptions.Jp2.CompressSize = compressedImageSaveOptions.Jp2.CompressSize;
            settingsSaveOptions.Jp2.Grayscale = compressedImageSaveOptions.Jp2.Grayscale;
            settingsSaveOptions.Jp2.Order = compressedImageSaveOptions.Jp2.Order;
            settingsSaveOptions.Jp2.PeakSignalToNoiseRatio = compressedImageSaveOptions.Jp2.PeakSignalToNoiseRatio;
            settingsSaveOptions.Jp2.TileSize = compressedImageSaveOptions.Jp2.TileSize;
            settingsSaveOptions.Jp2.Type = compressedImageSaveOptions.Jp2.Type;
            settingsSaveOptions.Gif.Interlaced = compressedImageSaveOptions.Gif.Interlaced;
            settingsSaveOptions.Gif.TransparencyColor = compressedImageSaveOptions.Gif.TransparencyColor;
            settingsSaveOptions.Gif.TransparencyMatch = compressedImageSaveOptions.Gif.TransparencyMatch;
            settingsSaveOptions.Gif.Type = compressedImageSaveOptions.Gif.Type;
            settingsSaveOptions.Png.Interlaced = compressedImageSaveOptions.Png.Interlaced;
            settingsSaveOptions.Png.TransparencyColor = compressedImageSaveOptions.Png.TransparencyColor;
            settingsSaveOptions.Png.TransparencyMatch = compressedImageSaveOptions.Png.TransparencyMatch;
            settingsSaveOptions.Hdp.ChromaSubSampling = compressedImageSaveOptions.Hdp.ChromaSubSampling;
            settingsSaveOptions.Hdp.FrequencyOrder = compressedImageSaveOptions.Hdp.FrequencyOrder;
            settingsSaveOptions.Hdp.Quantization = compressedImageSaveOptions.Hdp.Quantization;
            settingsSaveOptions.Ljp.Method = compressedImageSaveOptions.Ljp.Method;
            settingsSaveOptions.Ljp.Order = compressedImageSaveOptions.Ljp.Order;
            settingsSaveOptions.Ljp.Predictor = compressedImageSaveOptions.Ljp.Predictor;
            settingsSaveOptions.Jls.Interleave = compressedImageSaveOptions.Jls.Interleave;
            settingsSaveOptions.Jls.MaxValue = compressedImageSaveOptions.Jls.MaxValue;
            settingsSaveOptions.Jls.Near = compressedImageSaveOptions.Jls.Near;
            settingsSaveOptions.Jls.Point = compressedImageSaveOptions.Jls.Point;
        }

        private void DisplaySettings()
        {
            enableUpdate = false;

            // Display the save options.

            // Select the tab that corresponds to the save options format.
            switch (mySaveOptionsCurrent.Format)
            {
                case ImageXFormat.Exif:
                    tabControlSettings.SelectedIndex = 0;
                    radioButtonJpeg.Checked = true;
                    radioButtonJpegWrapInExif.Checked = true;
                    break;
                case ImageXFormat.Pdf:
                    tabControlSettings.SelectedIndex = 0;
                    radioButtonJpeg.Checked = true;
                    radioButtonJpegWrapInPdf.Checked = true;
                    break;
                case ImageXFormat.Tiff:
                    tabControlSettings.SelectedIndex = 0;
                    radioButtonJpeg.Checked = true;
                    radioButtonJpegWrapInTiff.Checked = true;
                    break;
                case ImageXFormat.Jpeg:
                    tabControlSettings.SelectedIndex = 0;
                    radioButtonJpeg.Checked = true;
                    radioButtonJpegNoWrap.Checked = true;
                    break;
                case ImageXFormat.Jpeg2000:
                    tabControlSettings.SelectedIndex = 1;
                    radioButtonJpeg2000.Checked = true;
                    break;
                case ImageXFormat.HdPhoto:
                    tabControlSettings.SelectedIndex = 2;
                    radioButtonHdp.Checked = true;
                    break;
                case ImageXFormat.Gif:
                    tabControlSettings.SelectedIndex = 3;
                    radioButtonGif.Checked = true;
                    break;
                case ImageXFormat.Png:
                    tabControlSettings.SelectedIndex = 4;
                    radioButtonPng.Checked = true;
                    break;
                case ImageXFormat.LosslessJpeg:
                    tabControlSettings.SelectedIndex = 5;
                    radioButtonLjp.Checked = true;
                    break;
                case ImageXFormat.JpegLs:
                    tabControlSettings.SelectedIndex = 6;
                    radioButtonJls.Checked = true;
                    break;
                case ImageXFormat.Pcx:
                    tabControlSettings.SelectedIndex = 7;
                    radioButtonPcx.Checked = true;
                    break;
            }

            // Display the JPG save options.
            hScrollBarLuminance.Value = mySaveOptionsCurrent.Jpeg.Luminance;
            hScrollBarChrominance.Value = mySaveOptionsCurrent.Jpeg.Chrominance;
            checkBoxJpegCosited.Checked = mySaveOptionsCurrent.Jpeg.Cosited;
            checkBoxJpegProgressive.Checked = mySaveOptionsCurrent.Jpeg.Progressive;
            switch (mySaveOptionsCurrent.Jpeg.SubSampling)
            {
                case SubSampling.SubSampling111:
                    radioButtonJpegSubSampling111.Checked = true;
                    break;
                case SubSampling.SubSampling211:
                    radioButtonJpegSubSampling211.Checked = true;
                    break;
                case SubSampling.SubSampling211v:
                    radioButtonJpegSubSampling211v.Checked = true;
                    break;
                case SubSampling.SubSampling411:
                    radioButtonJpegSubSampling411.Checked = true;
                    break;
            }
            numericUpDownExifThumbnailSize.Value = mySaveOptionsCurrent.Exif.ThumbnailSize;

            // Display the JP2 save options.
            numericUpDownJp2CompressSize.Value = mySaveOptionsCurrent.Jp2.CompressSize;
            switch (mySaveOptionsCurrent.Jp2.Order)
            {
                case ProgressionOrder.Default:
                    radioButtonJp2OrderDefault.Checked = true;
                    break;
                case ProgressionOrder.ComponentPositionResolutionLayer:
                    radioButtonJp2OrderCprl.Checked = true;
                    break;
                case ProgressionOrder.LayerResolutionComponentPosition:
                    radioButtonJp2OrderLrcp.Checked = true;
                    break;
                case ProgressionOrder.PositionComponentResolutionLayer:
                    radioButtonJp2OrderPcrl.Checked = true;
                    break;
                case ProgressionOrder.ResolutionLayerComponentPosition:
                    radioButtonJp2OrderRlcp.Checked = true;
                    break;
                case ProgressionOrder.ResolutionPositionComponentLayer:
                    radioButtonJp2OrderRpcl.Checked = true;
                    break;
            }
            numericUpDownJp2Peak.Value = (decimal)mySaveOptionsCurrent.Jp2.PeakSignalToNoiseRatio;
            numericUpDownJp2TileWidth.Value = mySaveOptionsCurrent.Jp2.TileSize.Width;
            numericUpDownJp2TileHeight.Value = mySaveOptionsCurrent.Jp2.TileSize.Height;
            switch (mySaveOptionsCurrent.Jp2.Type)
            {
                case Jp2Type.Lossless:
                    radioButtonJp2Lossless.Checked = true;
                    break;
                case Jp2Type.Lossy:
                    radioButtonJp2Lossy.Checked = true;
                    break;
            }

            // Display the HDP save options.
            switch (mySaveOptionsCurrent.Hdp.ChromaSubSampling)
            {
                case HdpSubSampling.ChromaSubSampling400:
                    radioButtonHdpChromaSubSampling400.Checked = true;
                    break;
                case HdpSubSampling.ChromaSubSampling420:
                    radioButtonHdpChromaSubSampling420.Checked = true;
                    break;
                case HdpSubSampling.ChromaSubSampling422:
                    radioButtonHdpChromaSubSampling422.Checked = true;
                    break;
                case HdpSubSampling.ChromaSubSampling444:
                    radioButtonHdpChromaSubSampling444.Checked = true;
                    break;
            }
            if (mySaveOptionsCurrent.Hdp.FrequencyOrder == 0)
                radioButtonHdpOrderSpacial.Checked = true;
            else
                radioButtonHdpOrderFrequency.Checked = true;
            numericUpDownHdpQuantization.Value = mySaveOptionsCurrent.Hdp.Quantization;

            // Display the GIF save options.
            switch (mySaveOptionsCurrent.Gif.Type)
            {
                case GifType.Gif87a:
                    radioButtonGif87a.Checked = true;
                    break;
                case GifType.Gif89a:
                    radioButtonGif89a.Checked = true;
                    break;
            }
            checkBoxGifInterlaced.Checked = mySaveOptionsCurrent.Gif.Interlaced;
            buttonGifTransparencyColor.BackColor = mySaveOptionsCurrent.Gif.TransparencyColor;
            switch (mySaveOptionsCurrent.Gif.TransparencyMatch)
            {
                case TransparencyMatch.None:
                    radioButtonGifNone.Checked = true;
                    break;
                case TransparencyMatch.Closest:
                    radioButtonGifClosest.Checked = true;
                    break;
                case TransparencyMatch.Exact:
                    radioButtonGifExact.Checked = true;
                    break;
            }

            // Display the PNG save options.
            checkBoxPngInterlaced.Checked = mySaveOptionsCurrent.Png.Interlaced;
            buttonPngTransparencyColor.BackColor = mySaveOptionsCurrent.Png.TransparencyColor;
            switch (mySaveOptionsCurrent.Png.TransparencyMatch)
            {
                case TransparencyMatch.None:
                    radioButtonPngNone.Checked = true;
                    break;
                case TransparencyMatch.Closest:
                    radioButtonPngClosest.Checked = true;
                    break;
                case TransparencyMatch.Exact:
                    radioButtonPngExact.Checked = true;
                    break;
            }

            // Display the LJP save options.
            numericUpDownLjpOrder.Value = mySaveOptionsCurrent.Ljp.Order;
            numericUpDownLjpPredictor.Value = mySaveOptionsCurrent.Ljp.Predictor;

            // Display the JLS save options.
            switch (mySaveOptionsCurrent.Jls.Interleave)
            {
                case 0:
                    radioButtonJlsNonInterleaved.Checked = true;
                    break;
                case 1:
                    radioButtonJlsLineInterleaved.Checked = true;
                    break;
                case 2:
                    radioButtonJlsSampleInterleaved.Checked = true;
                    break;
            }
            if (mySaveOptionsCurrent.Jls.MaxValue == 0)
                radioButtonJlsMax0.Checked = true;
            else
                radioButtonJlsMax255.Checked = true;
            numericUpDownJlsNear.Value = mySaveOptionsCurrent.Jls.Near;
            numericUpDownJlsPoint.Value = mySaveOptionsCurrent.Jls.Point;

            enableUpdate = true;
        }

        #endregion

        #region Close

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            // Cancelled.  If the image was modified, revert the
            // image using its original save options.
            if (imageHasBeenModified == true)
            {
                compressedImage1.CompressColorImage();
                compressedImage2.CompressColorImage();
                compressedImage3.CompressColorImage();
                compressedImage4.CompressColorImage();
                mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsColor().Format, mainForm.GetCompressedImage2().GetSaveOptionsColor().Format, mainForm.GetCompressedImage3().GetSaveOptionsColor().Format, mainForm.GetCompressedImage4().GetSaveOptionsColor().Format);
            }
            imageHasBeenModified = false;
            settingsMatchImage = true;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            // Compress the image using the updated settings.
            // Update the main form's settings, but only compress
            // the main form's image if the settings do not match.
            compressedImage1.UpdateSaveOptionsColor(mySaveOptions1);
            compressedImage2.UpdateSaveOptionsColor(mySaveOptions2);
            compressedImage3.UpdateSaveOptionsColor(mySaveOptions3);
            compressedImage4.UpdateSaveOptionsColor(mySaveOptions4);
            if (settingsMatchImage == false)
            {
                mainForm.UpdateCompressedNames(mySaveOptions1.Format, mySaveOptions2.Format, mySaveOptions3.Format, mySaveOptions4.Format);
                compressedImage1.CompressColorImage();
                compressedImage2.CompressColorImage();
                compressedImage3.CompressColorImage();
                compressedImage4.CompressColorImage();
            }
            imageHasBeenModified = false;
            settingsMatchImage = true;
            this.Close();
        }

        private void SettingsFormColor_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Now that the settings form has closed,
            // enable the main form's settings buttons so that
            // this form can be opened again.
            mainForm.EnableSettings(true);
        }

        #endregion

        #region Update Form Image

        private void UpdateImage()
        {
            if (enableUpdate == true)
            {
                if (checkBoxPreview.Checked)
                {
                    compressedImageCurrent.CompressImage(mySaveOptionsCurrent);
                    // Settings were just applied.
                    settingsMatchImage = true;
                    imageHasBeenModified = true;
                }
                else
                {
                    // Settings have been modified, but were not applied to the form image.
                    settingsMatchImage = false;
                }
            }
        }

        private void UpdateImageFormat()
        {
            // When the settings form is first loaded, this is called when
            // the radio button is set, but it is not necessary to update the image
            // at that time because the format did not change.
            if (enableUpdate == true)
            {
                if (checkBoxPreview.Checked)
                {
                    compressedImageCurrent.CompressImage(mySaveOptionsCurrent);
                    switch (compressedImageCurrent.GetWhichCompressedImage())
                    {
                        case 1:
                            mainForm.UpdateCompressedNames(mySaveOptionsCurrent.Format, mainForm.GetCompressedImage2().GetSaveOptionsColor().Format, mainForm.GetCompressedImage3().GetSaveOptionsColor().Format, mainForm.GetCompressedImage4().GetSaveOptionsColor().Format);
                            break;
                        case 2:
                            mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsColor().Format, mySaveOptionsCurrent.Format, mainForm.GetCompressedImage3().GetSaveOptionsColor().Format, mainForm.GetCompressedImage4().GetSaveOptionsColor().Format);
                            break;
                        case 3:
                            mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsColor().Format, mainForm.GetCompressedImage2().GetSaveOptionsColor().Format, mySaveOptionsCurrent.Format, mainForm.GetCompressedImage4().GetSaveOptionsColor().Format);
                            break;
                        case 4:
                            mainForm.UpdateCompressedNames(mainForm.GetCompressedImage1().GetSaveOptionsColor().Format, mainForm.GetCompressedImage2().GetSaveOptionsColor().Format, mainForm.GetCompressedImage3().GetSaveOptionsColor().Format, mySaveOptionsCurrent.Format);
                            break;
                    }
                    // Settings were just applied.
                    settingsMatchImage = true;
                    imageHasBeenModified = true;
                }
                else
                {
                    // Settings have been modified, but were not applied to the form image.
                    settingsMatchImage = false;
                }
            }
        }

        private void checkBoxPreview_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxPreview.Checked)
                UpdateImageFormat();
        }

        #endregion

        #region Modify Compression Format

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControlSettings.SelectedIndex)
            {
                case 0:
                    radioButtonJpeg.Checked = true;
                    break;
                case 1:
                    radioButtonJpeg2000.Checked = true;
                    break;
                case 2:
                    radioButtonHdp.Checked = true;
                    break;
                case 3:
                    radioButtonGif.Checked = true;
                    break;
                case 4:
                    radioButtonPng.Checked = true;
                    break;
                case 5:
                    radioButtonLjp.Checked = true;
                    break;
                case 6:
                    radioButtonJls.Checked = true;
                    break;
                case 7:
                    radioButtonPcx.Checked = true;
                    break;
            }
        }

        private void radioButtonJpeg_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpeg.Checked == true)
            {
                tabControlSettings.SelectedIndex = 0;
                mySaveOptionsCurrent.Format = ImageXFormat.Jpeg;
                UpdateImageFormat();
            }
        }

        private void radioButtonJpeg2000_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpeg2000.Checked == true)
            {
                tabControlSettings.SelectedIndex = 1;
                mySaveOptionsCurrent.Format = ImageXFormat.Jpeg2000;
                UpdateImageFormat();
            }
        }

        private void radioButtonHdp_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdp.Checked == true)
            {
                tabControlSettings.SelectedIndex = 2;
                mySaveOptionsCurrent.Format = ImageXFormat.HdPhoto;
                UpdateImageFormat();
            }
        }

        private void radioButtonGif_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonGif.Checked == true)
            {
                tabControlSettings.SelectedIndex = 3;
                mySaveOptionsCurrent.Format = ImageXFormat.Gif;
                UpdateImageFormat();
            }
        }

        private void radioButtonPng_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPng.Checked == true)
            {
                tabControlSettings.SelectedIndex = 4;
                mySaveOptionsCurrent.Format = ImageXFormat.Png;
                UpdateImageFormat();
            }
        }

        private void radioButtonLjp_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLjp.Checked == true)
            {
                tabControlSettings.SelectedIndex = 5;
                mySaveOptionsCurrent.Format = ImageXFormat.LosslessJpeg;
                UpdateImageFormat();
            }
        }

        private void radioButtonJls_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJls.Checked == true)
            {
                tabControlSettings.SelectedIndex = 6;
                mySaveOptionsCurrent.Format = ImageXFormat.JpegLs;
                UpdateImageFormat();
            }
        }

        private void radioButtonPcx_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPcx.Checked == true)
            {
                tabControlSettings.SelectedIndex = 7;
                mySaveOptionsCurrent.Format = ImageXFormat.Pcx;
                UpdateImageFormat();
            }
        }

        #endregion

        #region Modify Jpeg Compression Settings

        private void hScrollBarLuminance_ValueChanged(object sender, EventArgs e)
        {
            labelLuminanceValue.Text = hScrollBarLuminance.Value.ToString();
            mySaveOptionsCurrent.Jpeg.Luminance = hScrollBarLuminance.Value;
            UpdateImage();
        }

        private void hScrollBarChrominance_ValueChanged(object sender, EventArgs e)
        {
            labelChrominanceValue.Text = hScrollBarChrominance.Value.ToString();
            mySaveOptionsCurrent.Jpeg.Chrominance = hScrollBarChrominance.Value;
            UpdateImage();
        }

        private void checkBoxJpegCosited_CheckedChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jpeg.Cosited = checkBoxJpegCosited.Checked;
            UpdateImage();
        }

        private void checkBoxJpegProgressive_CheckedChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jpeg.Progressive = checkBoxJpegProgressive.Checked;
            UpdateImage();
        }

        private void radioButtonJpegSubSampling111_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegSubSampling111.Checked)
            {
                mySaveOptionsCurrent.Jpeg.SubSampling = SubSampling.SubSampling111;
                UpdateImage();
            }
        }

        private void radioButtonJpegSubSampling211_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegSubSampling211.Checked)
            {
                mySaveOptionsCurrent.Jpeg.SubSampling = SubSampling.SubSampling211;
                UpdateImage();
            }
        }

        private void radioButtonJpegSubSampling211v_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegSubSampling211v.Checked)
            {
                mySaveOptionsCurrent.Jpeg.SubSampling = SubSampling.SubSampling211v;
                UpdateImage();
            }
        }

        private void radioButtonJpegSubSampling411_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegSubSampling411.Checked)
            {
                mySaveOptionsCurrent.Jpeg.SubSampling = SubSampling.SubSampling411;
                UpdateImage();
            }
        }

        private void radioButtonJpegNoWrap_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegNoWrap.Checked)
            {
                mySaveOptionsCurrent.Format = ImageXFormat.Jpeg;
                UpdateImage();
            }
        }

        private void radioButtonJpegWrapInTiff_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegWrapInTiff.Checked)
            {
                mySaveOptionsCurrent.Format = ImageXFormat.Tiff;
                mySaveOptionsCurrent.Tiff.Compression = Compression.Jpeg;
                UpdateImage();
            }
        }

        private void radioButtonJpegWrapInPdf_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegWrapInPdf.Checked)
            {
                mySaveOptionsCurrent.Format = ImageXFormat.Pdf;
                mySaveOptionsCurrent.Pdf.Compression = Compression.Jpeg;
                UpdateImage();
            }
        }

        private void radioButtonJpegWrapInExif_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJpegWrapInExif.Checked)
            {
                checkBoxJpegProgressive.Checked = false;
                checkBoxJpegProgressive.Enabled = false;
                mySaveOptionsCurrent.Format = ImageXFormat.Exif;
                numericUpDownExifThumbnailSize.Enabled = true;
                numericUpDownExifThumbnailSize.Value = mySaveOptionsCurrent.Exif.ThumbnailSize;
                UpdateImage();
            }
            else
            {
                checkBoxJpegProgressive.Enabled = true;
                numericUpDownExifThumbnailSize.Enabled = false;
            }
        }

        private void numericUpDownExifThumbnailSize_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Exif.ThumbnailSize = (int)numericUpDownExifThumbnailSize.Value;
            UpdateImage();
        }

        #endregion

        #region Modify Jp2 Compression Settings

        private void radioButtonJp2Lossless_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2Lossless.Checked)
            {
                numericUpDownJp2CompressSize.Enabled = false;
                numericUpDownJp2Peak.Enabled = false;
                mySaveOptionsCurrent.Jp2.Type = Jp2Type.Lossless;
                UpdateImage();
            }
        }

        private void radioButtonJp2Lossy_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2Lossy.Checked)
            {
                numericUpDownJp2CompressSize.Enabled = true;
                numericUpDownJp2Peak.Enabled = true;
                mySaveOptionsCurrent.Jp2.Type = Jp2Type.Lossy;
                UpdateImage();
            }
        }

        private void numericUpDownJp2CompressSize_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jp2.CompressSize = (int)numericUpDownJp2CompressSize.Value;
            UpdateImage();
        }

        private void radioButtonJp2OrderDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2OrderDefault.Checked)
            {
                mySaveOptionsCurrent.Jp2.Order = ProgressionOrder.Default;
                UpdateImage();
            }
        }

        private void radioButtonJp2OrderCprl_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2OrderCprl.Checked)
            {
                mySaveOptionsCurrent.Jp2.Order = ProgressionOrder.ComponentPositionResolutionLayer;
                UpdateImage();
            }
        }

        private void radioButtonJp2OrderLrcp_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2OrderLrcp.Checked)
            {
                mySaveOptionsCurrent.Jp2.Order = ProgressionOrder.LayerResolutionComponentPosition;
                UpdateImage();
            }
        }

        private void radioButtonJp2OrderPcrl_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2OrderPcrl.Checked)
            {
                mySaveOptionsCurrent.Jp2.Order = ProgressionOrder.PositionComponentResolutionLayer;
                UpdateImage();
            }
        }

        private void radioButtonJp2OrderRlcp_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2OrderRlcp.Checked)
            {
                mySaveOptionsCurrent.Jp2.Order = ProgressionOrder.ResolutionLayerComponentPosition;
                UpdateImage();
            }
        }

        private void radioButtonJp2OrderRpcl_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJp2OrderRpcl.Checked)
            {
                mySaveOptionsCurrent.Jp2.Order = ProgressionOrder.ResolutionPositionComponentLayer;
                UpdateImage();
            }
        }

        private void numericUpDownJp2TileWidth_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jp2.TileSize = new Size((int)numericUpDownJp2TileWidth.Value, mySaveOptionsCurrent.Jp2.TileSize.Height);
            UpdateImage();
        }

        private void numericUpDownJp2TileHeight_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jp2.TileSize = new Size(mySaveOptionsCurrent.Jp2.TileSize.Width, (int)numericUpDownJp2TileHeight.Value);
            UpdateImage();
        }

        private void numericUpDownJp2Peak_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jp2.PeakSignalToNoiseRatio = (double)numericUpDownJp2Peak.Value;
            if (mySaveOptionsCurrent.Jp2.PeakSignalToNoiseRatio == 0)
                numericUpDownJp2CompressSize.Enabled = true;
            else
                numericUpDownJp2CompressSize.Enabled = false;
            UpdateImage();
        }

        #endregion

        #region Modify Hdp Compression Settings

        private void radioButtonHdpChromaSubSampling400_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdpChromaSubSampling400.Checked)
            {
                mySaveOptionsCurrent.Hdp.ChromaSubSampling = HdpSubSampling.ChromaSubSampling400;
                UpdateImage();
            }
        }

        private void radioButtonHdpChromaSubSampling420_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdpChromaSubSampling420.Checked)
            {
                mySaveOptionsCurrent.Hdp.ChromaSubSampling = HdpSubSampling.ChromaSubSampling420;
                UpdateImage();
            }
        }

        private void radioButtonHdpChromaSubSampling422_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdpChromaSubSampling422.Checked)
            {
                mySaveOptionsCurrent.Hdp.ChromaSubSampling = HdpSubSampling.ChromaSubSampling422;
                UpdateImage();
            }
        }

        private void radioButtonHdpChromaSubSampling444_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdpChromaSubSampling444.Checked)
            {
                mySaveOptionsCurrent.Hdp.ChromaSubSampling = HdpSubSampling.ChromaSubSampling444;
                UpdateImage();
            }
        }

        private void radioButtonHdpOrderSpacial_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdpOrderSpacial.Checked)
            {
                mySaveOptionsCurrent.Hdp.FrequencyOrder = 0;
                UpdateImage();
            }
        }

        private void radioButtonHdpOrderFrequency_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonHdpOrderFrequency.Checked)
            {
                mySaveOptionsCurrent.Hdp.FrequencyOrder = 1;
                UpdateImage();
            }
        }

        private void numericUpDownHdpQuantization_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Hdp.Quantization = (int)numericUpDownHdpQuantization.Value;
            UpdateImage();
        }

        #endregion

        #region Modify Gif Compression Settings

        private void radioButtonGifNone_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonGifNone.Checked)
            {
                mySaveOptionsCurrent.Gif.TransparencyMatch = TransparencyMatch.None;
                UpdateImage();
            }
        }

        private void radioButtonGifClosest_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonGifClosest.Checked)
            {
                mySaveOptionsCurrent.Gif.TransparencyMatch = TransparencyMatch.Closest;
                UpdateImage();
            }
        }

        private void radioButtonGifExact_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonGifExact.Checked)
            {
                mySaveOptionsCurrent.Gif.TransparencyMatch = TransparencyMatch.Exact;
                UpdateImage();
            }
        }

        private void radioButtonGif87a_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonGif87a.Checked)
            {
                mySaveOptionsCurrent.Gif.Type = GifType.Gif87a;
                UpdateImage();
            }
        }

        private void radioButtonGif89a_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonGif89a.Checked)
            {
                mySaveOptionsCurrent.Gif.Type = GifType.Gif89a;
                UpdateImage();
            }
        }

        private void checkBoxGifInterlaced_CheckedChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Gif.Interlaced = checkBoxGifInterlaced.Checked;
            UpdateImage();
        }

        private void buttonGifTransparencyColor_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = buttonGifTransparencyColor.BackColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        buttonGifTransparencyColor.BackColor = dlg.Color;

                        mySaveOptionsCurrent.Gif.TransparencyColor = dlg.Color;
                        UpdateImage();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Modify Png Compression Settings

        private void radioButtonPngNone_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPngNone.Checked)
            {
                mySaveOptionsCurrent.Png.TransparencyMatch = TransparencyMatch.None;
                UpdateImage();
            }
        }

        private void radioButtonPngClosest_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPngClosest.Checked)
            {
                mySaveOptionsCurrent.Png.TransparencyMatch = TransparencyMatch.Closest;
                UpdateImage();
            }
        }

        private void radioButtonPngExact_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonPngExact.Checked)
            {
                mySaveOptionsCurrent.Png.TransparencyMatch = TransparencyMatch.Exact;
                UpdateImage();
            }
        }

        private void checkBoxPngInterlaced_CheckedChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Png.Interlaced = checkBoxPngInterlaced.Checked;
            UpdateImage();
        }

        private void buttonPngTransparencyColor_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = buttonPngTransparencyColor.BackColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        buttonPngTransparencyColor.BackColor = dlg.Color;

                        mySaveOptionsCurrent.Png.TransparencyColor = dlg.Color;
                        UpdateImage();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Modify Ljp Compression Settings

        private void numericUpDownLjpOrder_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Ljp.Order = (int)numericUpDownLjpOrder.Value;
            UpdateImage();
        }

        private void numericUpDownLjpPredictor_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Ljp.Predictor = (int)numericUpDownLjpPredictor.Value;
            UpdateImage();
        }

        #endregion

        #region Modify Jls Compression Settings

        private void numericUpDownJlsPoint_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jls.Point = (int)numericUpDownJlsPoint.Value;
            UpdateImage();
        }

        private void numericUpDownJlsNear_ValueChanged(object sender, EventArgs e)
        {
            mySaveOptionsCurrent.Jls.Near = (int)numericUpDownJlsNear.Value;
            UpdateImage();
        }

        private void radioButtonJlsNonInterleaved_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJlsNonInterleaved.Checked)
            {
                mySaveOptionsCurrent.Jls.Interleave = 0;
                UpdateImage();
            }
        }

        private void radioButtonJlsLineInterleaved_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJlsLineInterleaved.Checked)
            {
                mySaveOptionsCurrent.Jls.Interleave = 1;
                UpdateImage();
            }
        }

        private void radioButtonJlsSampleInterleaved_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJlsSampleInterleaved.Checked)
            {
                mySaveOptionsCurrent.Jls.Interleave = 2;
                UpdateImage();
            }
        }

        private void radioButtonJlsMax0_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJlsMax0.Checked)
            {
                mySaveOptionsCurrent.Jls.MaxValue = 0;
                UpdateImage();
            }
        }

        private void radioButtonJlsMax255_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonJlsMax255.Checked)
            {
                mySaveOptionsCurrent.Jls.MaxValue = 255;
                UpdateImage();
            }
        }

        #endregion

        #region Modify Which Compressed Image Settings Apply To

        public void SelectCompressedImage()
        {
            switch (mainForm.GetTabControl().SelectedIndex)
            {
                case 0:
                case 1:
                    radioButtonCompressedImage1.Checked = true;
                    break;
                case 2:
                    radioButtonCompressedImage2.Checked = true;
                    break;
                case 3:
                    radioButtonCompressedImage3.Checked = true;
                    break;
                case 4:
                    radioButtonCompressedImage4.Checked = true;
                    break;
            }
        }

        private void radioButtonCompressedImage1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonCompressedImage1.Checked == true)
            {
                compressedImageCurrent = compressedImage1;
                mySaveOptionsCurrent = mySaveOptions1;
                mainForm.GetTabControl().SelectedIndex = 1;
                DisplaySettings();
            }
        }

        private void radioButtonCompressedImage2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage2.Checked == true)
            {
                compressedImageCurrent = compressedImage2;
                mySaveOptionsCurrent = mySaveOptions2;
                mainForm.GetTabControl().SelectedIndex = 2;
                DisplaySettings();
            }
        }

        private void radioButtonCompressedImage3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage3.Checked == true)
            {
                compressedImageCurrent = compressedImage3;
                mySaveOptionsCurrent = mySaveOptions3;
                mainForm.GetTabControl().SelectedIndex = 3;
                DisplaySettings();
            }
        }

        private void radioButtonCompressedImage4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCompressedImage4.Checked == true)
            {
                compressedImageCurrent = compressedImage4;
                mySaveOptionsCurrent = mySaveOptions4;
                mainForm.GetTabControl().SelectedIndex = 4;
                DisplaySettings();
            }
        }

        #endregion

    }
}

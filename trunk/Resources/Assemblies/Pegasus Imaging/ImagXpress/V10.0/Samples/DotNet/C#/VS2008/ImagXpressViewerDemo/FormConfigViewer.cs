﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// Accusoft Pegasus 
using Accusoft.ImagXpressSdk;
using Accusoft.NotateXpressSdk;
using Accusoft.ThumbnailXpressSdk;

namespace ImagXpressViewerDemo
{
    public partial class FormConfigViewer : Form
    {
        private ImageXView viewToSave;

        public FormConfigViewer()
        {
            InitializeComponent();
            // Call the Accusoft-Pegasus Unlocking Code here
            try
            {
                AccusoftUnlocks.UnlockRuntimes(imagXpressSample, null, null);
            }
            catch
            {
                MessageBox.Show("Invalid Unlock Codes, See the UnlockRuntime calls");
            }
        }

        private void FormConfigViewer_Load(object sender, EventArgs e)
        {
            System.Reflection.Assembly thisExe;
            thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream embeddedFile = thisExe.GetManifestResourceStream("ImagXpressViewerDemo.ShippedImages.NewOrleans.jpg");
            imageXViewSample.Image = ImageX.FromStream(imagXpressSample, embeddedFile);

            comboBoxScrollBars.DataSource = Enum.GetValues(typeof(ScrollBars));
            comboBoxAlignHorizontal.DataSource = Enum.GetValues(typeof(AlignHorizontal));
            comboBoxAlignVertical.DataSource = Enum.GetValues(typeof(AlignVertical));

            comboBoxScrollBars.SelectedIndex = (int)viewToSave.ScrollBars;
            comboBoxAlignHorizontal.SelectedIndex = (int)viewToSave.AlignHorizontal;
            comboBoxAlignVertical.SelectedIndex = (int)viewToSave.AlignVertical;

        }

        public void loadExistingView(ImageXView viewToTweak)
        {
            viewToSave = viewToTweak;

            imageXViewSample.BackColor = viewToSave.BackColor;
            imageXViewSample.BackColor = viewToSave.BackColor;
            imageXViewSample.BorderStyle = viewToSave.BorderStyle;
            imageXViewSample.AutoResize = viewToSave.AutoResize;
            imageXViewSample.AllowDrop = viewToSave.AllowDrop;
            imageXViewSample.AlphaBlend = viewToSave.AlphaBlend;

            imageXViewSample.ScrollBars = viewToSave.ScrollBars;
            imageXViewSample.AlignHorizontal = viewToSave.AlignHorizontal;
            imageXViewSample.AlignVertical = viewToSave.AlignVertical;

            // remove dither from the right click
            imageXViewSample.ContextMenu.MenuItems[1].Visible = false;
            // add a new preserve black
            MenuItem menuBlack = new MenuItem();

            menuBlack.Text = "Preserve Black";
            menuBlack.Name = "Black";
            menuBlack.Checked = false;
            imageXViewSample.ContextMenu.MenuItems.Add(4, menuBlack);

            // set up events to sync up context menu changes
            for (int i = 0; i < imageXViewSample.ContextMenu.MenuItems.Count; i++)
            {
                imageXViewSample.ContextMenu.MenuItems[i].Click += new EventHandler(ContextMenuClickEventHandler);
            }
            imageXViewSample.ContextMenu.MenuItems["Black"].Click += new EventHandler(BlackContextMenuClickEventHandler);

            buttonBackColor.BackColor = viewToSave.BackColor;


        }

        // generic for ALL context menu items 
        private void ContextMenuClickEventHandler(object sender, EventArgs e)
        {

        }

        // specific only for Preserve black context menu
        private void BlackContextMenuClickEventHandler(object sender, EventArgs e)
        {
            // need to manually handle preserve black
            imageXViewSample.PreserveBlack = !imageXViewSample.PreserveBlack;
            imageXViewSample.ContextMenu.MenuItems["Black"].Checked = imageXViewSample.PreserveBlack;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {

            // built in to context
            viewToSave.Antialias = imageXViewSample.Antialias;
            viewToSave.PreserveBlack = imageXViewSample.PreserveBlack;
            viewToSave.Smoothing = imageXViewSample.Smoothing;

            // get all settings back to the proper view control
            viewToSave.BackColor = imageXViewSample.BackColor;
            viewToSave.ScrollBars = imageXViewSample.ScrollBars;
            viewToSave.AlignHorizontal = imageXViewSample.AlignHorizontal;
            viewToSave.AlignVertical = imageXViewSample.AlignVertical;


            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonBackColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = buttonBackColor.BackColor;
            colorDialog.AllowFullOpen = true;
            colorDialog.FullOpen = true;

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                buttonBackColor.BackColor = colorDialog.Color;
                imageXViewSample.BackColor = colorDialog.Color;
            }

        }

        private void comboBoxScrollBars_SelectedValueChanged(object sender, EventArgs e)
        {
            imageXViewSample.ScrollBars = (ScrollBars)comboBoxScrollBars.SelectedValue;
        }

        private void buttonMore_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You can find more information about displaying the images using the ImagXpress SDK");
        }

        private void comboBoxAlignHorizontal_SelectedValueChanged(object sender, EventArgs e)
        {
            imageXViewSample.AlignHorizontal = (AlignHorizontal)comboBoxAlignHorizontal.SelectedValue;

        }

        private void comboBoxAlignVertical_SelectedValueChanged(object sender, EventArgs e)
        {
            imageXViewSample.AlignVertical = (AlignVertical)comboBoxAlignVertical.SelectedValue;

        }

    }
}

﻿namespace ImagXpressViewerDemo
{
    partial class FormMoreImageInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxCompression = new System.Windows.Forms.TextBox();
            this.textBoxFormat = new System.Windows.Forms.TextBox();
            this.labelCompression = new System.Windows.Forms.Label();
            this.labelFormat = new System.Windows.Forms.Label();
            this.textBoxResolutionUnits = new System.Windows.Forms.TextBox();
            this.textBoxResolution = new System.Windows.Forms.TextBox();
            this.textBoxBitsPerPixel = new System.Windows.Forms.TextBox();
            this.textBoxHeight = new System.Windows.Forms.TextBox();
            this.textBoxWidth = new System.Windows.Forms.TextBox();
            this.textBoxShortFileName = new System.Windows.Forms.TextBox();
            this.buttonMore = new System.Windows.Forms.Button();
            this.labelResolutionUnits = new System.Windows.Forms.Label();
            this.labelResolution = new System.Windows.Forms.Label();
            this.labelBitsPerPixel = new System.Windows.Forms.Label();
            this.labelWidth = new System.Windows.Forms.Label();
            this.labelShortFN = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelSelected = new System.Windows.Forms.Label();
            this.panelBackgroundFrame1 = new AccusoftCustom.PanelBackgroundFrame();
            this.panelGradientNoFlare1 = new AccusoftCustom.PanelGradientNoFlare();
            this.panel1.SuspendLayout();
            this.panelBackgroundFrame1.SuspendLayout();
            this.panelGradientNoFlare1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.textBoxCompression);
            this.panel1.Controls.Add(this.textBoxFormat);
            this.panel1.Controls.Add(this.labelCompression);
            this.panel1.Controls.Add(this.labelFormat);
            this.panel1.Controls.Add(this.textBoxResolutionUnits);
            this.panel1.Controls.Add(this.textBoxResolution);
            this.panel1.Controls.Add(this.textBoxBitsPerPixel);
            this.panel1.Controls.Add(this.textBoxHeight);
            this.panel1.Controls.Add(this.textBoxWidth);
            this.panel1.Controls.Add(this.textBoxShortFileName);
            this.panel1.Controls.Add(this.buttonMore);
            this.panel1.Controls.Add(this.labelResolutionUnits);
            this.panel1.Controls.Add(this.labelResolution);
            this.panel1.Controls.Add(this.labelBitsPerPixel);
            this.panel1.Controls.Add(this.labelWidth);
            this.panel1.Controls.Add(this.labelShortFN);
            this.panel1.Controls.Add(this.labelHeight);
            this.panel1.Location = new System.Drawing.Point(16, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(294, 331);
            this.panel1.TabIndex = 10;
            // 
            // textBoxCompression
            // 
            this.textBoxCompression.Location = new System.Drawing.Point(132, 237);
            this.textBoxCompression.Name = "textBoxCompression";
            this.textBoxCompression.ReadOnly = true;
            this.textBoxCompression.Size = new System.Drawing.Size(145, 20);
            this.textBoxCompression.TabIndex = 30;
            // 
            // textBoxFormat
            // 
            this.textBoxFormat.Location = new System.Drawing.Point(132, 206);
            this.textBoxFormat.Name = "textBoxFormat";
            this.textBoxFormat.ReadOnly = true;
            this.textBoxFormat.Size = new System.Drawing.Size(145, 20);
            this.textBoxFormat.TabIndex = 29;
            // 
            // labelCompression
            // 
            this.labelCompression.AutoSize = true;
            this.labelCompression.BackColor = System.Drawing.Color.Transparent;
            this.labelCompression.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelCompression.Location = new System.Drawing.Point(12, 237);
            this.labelCompression.Name = "labelCompression";
            this.labelCompression.Size = new System.Drawing.Size(74, 13);
            this.labelCompression.TabIndex = 26;
            this.labelCompression.Text = "Compression";
            // 
            // labelFormat
            // 
            this.labelFormat.AutoSize = true;
            this.labelFormat.BackColor = System.Drawing.Color.Transparent;
            this.labelFormat.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelFormat.Location = new System.Drawing.Point(12, 208);
            this.labelFormat.Name = "labelFormat";
            this.labelFormat.Size = new System.Drawing.Size(43, 13);
            this.labelFormat.TabIndex = 24;
            this.labelFormat.Text = "Format";
            // 
            // textBoxResolutionUnits
            // 
            this.textBoxResolutionUnits.Location = new System.Drawing.Point(132, 176);
            this.textBoxResolutionUnits.Name = "textBoxResolutionUnits";
            this.textBoxResolutionUnits.ReadOnly = true;
            this.textBoxResolutionUnits.Size = new System.Drawing.Size(145, 20);
            this.textBoxResolutionUnits.TabIndex = 23;
            // 
            // textBoxResolution
            // 
            this.textBoxResolution.Location = new System.Drawing.Point(132, 144);
            this.textBoxResolution.Name = "textBoxResolution";
            this.textBoxResolution.ReadOnly = true;
            this.textBoxResolution.Size = new System.Drawing.Size(145, 20);
            this.textBoxResolution.TabIndex = 22;
            // 
            // textBoxBitsPerPixel
            // 
            this.textBoxBitsPerPixel.Location = new System.Drawing.Point(132, 112);
            this.textBoxBitsPerPixel.Name = "textBoxBitsPerPixel";
            this.textBoxBitsPerPixel.ReadOnly = true;
            this.textBoxBitsPerPixel.Size = new System.Drawing.Size(145, 20);
            this.textBoxBitsPerPixel.TabIndex = 21;
            // 
            // textBoxHeight
            // 
            this.textBoxHeight.Location = new System.Drawing.Point(132, 78);
            this.textBoxHeight.Name = "textBoxHeight";
            this.textBoxHeight.ReadOnly = true;
            this.textBoxHeight.Size = new System.Drawing.Size(145, 20);
            this.textBoxHeight.TabIndex = 20;
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.Location = new System.Drawing.Point(132, 45);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.ReadOnly = true;
            this.textBoxWidth.Size = new System.Drawing.Size(145, 20);
            this.textBoxWidth.TabIndex = 19;
            // 
            // textBoxShortFileName
            // 
            this.textBoxShortFileName.Location = new System.Drawing.Point(132, 14);
            this.textBoxShortFileName.Name = "textBoxShortFileName";
            this.textBoxShortFileName.ReadOnly = true;
            this.textBoxShortFileName.Size = new System.Drawing.Size(145, 20);
            this.textBoxShortFileName.TabIndex = 18;
            // 
            // buttonMore
            // 
            this.buttonMore.Location = new System.Drawing.Point(109, 287);
            this.buttonMore.Name = "buttonMore";
            this.buttonMore.Size = new System.Drawing.Size(75, 23);
            this.buttonMore.TabIndex = 17;
            this.buttonMore.Text = "More";
            this.buttonMore.UseVisualStyleBackColor = true;
            this.buttonMore.Click += new System.EventHandler(this.buttonMore_Click);
            // 
            // labelResolutionUnits
            // 
            this.labelResolutionUnits.AutoSize = true;
            this.labelResolutionUnits.BackColor = System.Drawing.Color.Transparent;
            this.labelResolutionUnits.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelResolutionUnits.Location = new System.Drawing.Point(12, 179);
            this.labelResolutionUnits.Name = "labelResolutionUnits";
            this.labelResolutionUnits.Size = new System.Drawing.Size(93, 13);
            this.labelResolutionUnits.TabIndex = 16;
            this.labelResolutionUnits.Text = "Resolution Units";
            // 
            // labelResolution
            // 
            this.labelResolution.AutoSize = true;
            this.labelResolution.BackColor = System.Drawing.Color.Transparent;
            this.labelResolution.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelResolution.Location = new System.Drawing.Point(12, 144);
            this.labelResolution.Name = "labelResolution";
            this.labelResolution.Size = new System.Drawing.Size(66, 13);
            this.labelResolution.TabIndex = 14;
            this.labelResolution.Text = "Resolution ";
            // 
            // labelBitsPerPixel
            // 
            this.labelBitsPerPixel.AutoSize = true;
            this.labelBitsPerPixel.BackColor = System.Drawing.Color.Transparent;
            this.labelBitsPerPixel.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelBitsPerPixel.Location = new System.Drawing.Point(12, 112);
            this.labelBitsPerPixel.Name = "labelBitsPerPixel";
            this.labelBitsPerPixel.Size = new System.Drawing.Size(71, 13);
            this.labelBitsPerPixel.TabIndex = 11;
            this.labelBitsPerPixel.Text = "Bits Per Pixel";
            // 
            // labelWidth
            // 
            this.labelWidth.AutoSize = true;
            this.labelWidth.BackColor = System.Drawing.Color.Transparent;
            this.labelWidth.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelWidth.Location = new System.Drawing.Point(12, 45);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(39, 13);
            this.labelWidth.TabIndex = 9;
            this.labelWidth.Text = "Width";
            // 
            // labelShortFN
            // 
            this.labelShortFN.AutoSize = true;
            this.labelShortFN.BackColor = System.Drawing.Color.Transparent;
            this.labelShortFN.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelShortFN.Location = new System.Drawing.Point(12, 16);
            this.labelShortFN.Name = "labelShortFN";
            this.labelShortFN.Size = new System.Drawing.Size(88, 13);
            this.labelShortFN.TabIndex = 6;
            this.labelShortFN.Text = "Short File Name";
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.BackColor = System.Drawing.Color.Transparent;
            this.labelHeight.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.labelHeight.Location = new System.Drawing.Point(12, 78);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(42, 13);
            this.labelHeight.TabIndex = 7;
            this.labelHeight.Text = "Height";
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonOK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Location = new System.Drawing.Point(125, 452);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(114, 30);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelSelected
            // 
            this.labelSelected.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSelected.AutoSize = true;
            this.labelSelected.BackColor = System.Drawing.Color.Transparent;
            this.labelSelected.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelected.ForeColor = System.Drawing.Color.White;
            this.labelSelected.Location = new System.Drawing.Point(80, 19);
            this.labelSelected.Name = "labelSelected";
            this.labelSelected.Size = new System.Drawing.Size(201, 18);
            this.labelSelected.TabIndex = 17;
            this.labelSelected.Text = "Selected Image Information";
            // 
            // panelBackgroundFrame1
            // 
            this.panelBackgroundFrame1.Controls.Add(this.labelSelected);
            this.panelBackgroundFrame1.Controls.Add(this.panelGradientNoFlare1);
            this.panelBackgroundFrame1.Location = new System.Drawing.Point(12, 14);
            this.panelBackgroundFrame1.Name = "panelBackgroundFrame1";
            this.panelBackgroundFrame1.Size = new System.Drawing.Size(357, 425);
            this.panelBackgroundFrame1.TabIndex = 18;
            // 
            // panelGradientNoFlare1
            // 
            this.panelGradientNoFlare1.BackColor = System.Drawing.Color.Transparent;
            this.panelGradientNoFlare1.Controls.Add(this.panel1);
            this.panelGradientNoFlare1.Location = new System.Drawing.Point(14, 44);
            this.panelGradientNoFlare1.Name = "panelGradientNoFlare1";
            this.panelGradientNoFlare1.Size = new System.Drawing.Size(329, 364);
            this.panelGradientNoFlare1.TabIndex = 0;
            // 
            // FormMoreImageInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(54)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(383, 494);
            this.Controls.Add(this.panelBackgroundFrame1);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(561, 584);
            this.Name = "FormMoreImageInfo";
            this.Text = "Extended Image Information";
            this.Load += new System.EventHandler(this.FormMoreImageInfo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelBackgroundFrame1.ResumeLayout(false);
            this.panelBackgroundFrame1.PerformLayout();
            this.panelGradientNoFlare1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonMore;
        private System.Windows.Forms.Label labelResolutionUnits;
        private System.Windows.Forms.Label labelResolution;
        private System.Windows.Forms.Label labelBitsPerPixel;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.Label labelShortFN;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.TextBox textBoxShortFileName;
        private System.Windows.Forms.TextBox textBoxHeight;
        private System.Windows.Forms.TextBox textBoxWidth;
        private System.Windows.Forms.TextBox textBoxResolutionUnits;
        private System.Windows.Forms.TextBox textBoxResolution;
        private System.Windows.Forms.TextBox textBoxBitsPerPixel;
        private System.Windows.Forms.TextBox textBoxCompression;
        private System.Windows.Forms.TextBox textBoxFormat;
        private System.Windows.Forms.Label labelCompression;
        private System.Windows.Forms.Label labelFormat;
        private System.Windows.Forms.Label labelSelected;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrame1;
        private AccusoftCustom.PanelGradientNoFlare panelGradientNoFlare1;
    }
}
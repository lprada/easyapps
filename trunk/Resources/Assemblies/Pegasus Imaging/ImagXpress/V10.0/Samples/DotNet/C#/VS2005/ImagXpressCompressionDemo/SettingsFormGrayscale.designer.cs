﻿namespace ImagXpressCompressionDemo
{
	partial class SettingsFormGrayscale
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.checkBoxPreview = new System.Windows.Forms.CheckBox();
            this.buttonRestoreDefaults = new System.Windows.Forms.Button();
            this.groupBoxImageType = new System.Windows.Forms.GroupBox();
            this.radioButtonWsq = new System.Windows.Forms.RadioButton();
            this.radioButtonJls = new System.Windows.Forms.RadioButton();
            this.radioButtonLjp = new System.Windows.Forms.RadioButton();
            this.radioButtonHdp = new System.Windows.Forms.RadioButton();
            this.radioButtonJpeg2000 = new System.Windows.Forms.RadioButton();
            this.radioButtonJpeg = new System.Windows.Forms.RadioButton();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.tabPageJpeg = new System.Windows.Forms.TabPage();
            this.numericUpDownExifThumbnailSize = new System.Windows.Forms.NumericUpDown();
            this.labelExifThumbnailSize = new System.Windows.Forms.Label();
            this.groupBoxJpegWrap = new System.Windows.Forms.GroupBox();
            this.radioButtonJpegWrapInExif = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegNoWrap = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegWrapInPdf = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegWrapInTiff = new System.Windows.Forms.RadioButton();
            this.checkBoxJpegProgressive = new System.Windows.Forms.CheckBox();
            this.groupBoxSubSampling = new System.Windows.Forms.GroupBox();
            this.radioButtonJpegSubSampling411 = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegSubSampling211v = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegSubSampling211 = new System.Windows.Forms.RadioButton();
            this.radioButtonJpegSubSampling111 = new System.Windows.Forms.RadioButton();
            this.checkBoxJpegCosited = new System.Windows.Forms.CheckBox();
            this.labelChrominanceValue = new System.Windows.Forms.Label();
            this.labelLuminanceValue = new System.Windows.Forms.Label();
            this.labelChrominance = new System.Windows.Forms.Label();
            this.labelLuminance = new System.Windows.Forms.Label();
            this.hScrollBarChrominance = new System.Windows.Forms.HScrollBar();
            this.hScrollBarLuminance = new System.Windows.Forms.HScrollBar();
            this.tabPageJpeg2000 = new System.Windows.Forms.TabPage();
            this.labelJp2Peak = new System.Windows.Forms.Label();
            this.numericUpDownJp2Peak = new System.Windows.Forms.NumericUpDown();
            this.groupBoxJp2TileSize = new System.Windows.Forms.GroupBox();
            this.labelJp2TileHeight = new System.Windows.Forms.Label();
            this.labelJp2TileWidth = new System.Windows.Forms.Label();
            this.numericUpDownJp2TileHeight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownJp2TileWidth = new System.Windows.Forms.NumericUpDown();
            this.groupBoxJp2Type = new System.Windows.Forms.GroupBox();
            this.radioButtonJp2Lossy = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2Lossless = new System.Windows.Forms.RadioButton();
            this.numericUpDownJp2CompressSize = new System.Windows.Forms.NumericUpDown();
            this.labelJp2CompressSize = new System.Windows.Forms.Label();
            this.groupBoxJp2Order = new System.Windows.Forms.GroupBox();
            this.radioButtonJp2OrderRpcl = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderRlcp = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderPcrl = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderLrcp = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderCprl = new System.Windows.Forms.RadioButton();
            this.radioButtonJp2OrderDefault = new System.Windows.Forms.RadioButton();
            this.tabPageHdp = new System.Windows.Forms.TabPage();
            this.numericUpDownHdpQuantization = new System.Windows.Forms.NumericUpDown();
            this.labelHdpQuantization = new System.Windows.Forms.Label();
            this.groupBoxHdpOrder = new System.Windows.Forms.GroupBox();
            this.radioButtonHdpOrderFrequency = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpOrderSpacial = new System.Windows.Forms.RadioButton();
            this.groupBoxChromaSubSampling = new System.Windows.Forms.GroupBox();
            this.radioButtonHdpChromaSubSampling444 = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpChromaSubSampling422 = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpChromaSubSampling420 = new System.Windows.Forms.RadioButton();
            this.radioButtonHdpChromaSubSampling400 = new System.Windows.Forms.RadioButton();
            this.tabPageLjp = new System.Windows.Forms.TabPage();
            this.labelLjpPredictor = new System.Windows.Forms.Label();
            this.labelLjpOrder = new System.Windows.Forms.Label();
            this.numericUpDownLjpPredictor = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLjpOrder = new System.Windows.Forms.NumericUpDown();
            this.tabPageJls = new System.Windows.Forms.TabPage();
            this.groupBoxJlsMax = new System.Windows.Forms.GroupBox();
            this.radioButtonJlsMax255 = new System.Windows.Forms.RadioButton();
            this.radioButtonJlsMax0 = new System.Windows.Forms.RadioButton();
            this.labelJlsPoint = new System.Windows.Forms.Label();
            this.labelJlsNear = new System.Windows.Forms.Label();
            this.numericUpDownJlsPoint = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownJlsNear = new System.Windows.Forms.NumericUpDown();
            this.tabPageWsq = new System.Windows.Forms.TabPage();
            this.labelWsqQuantization = new System.Windows.Forms.Label();
            this.numericUpDownWsqQuantization = new System.Windows.Forms.NumericUpDown();
            this.labelWsqWhiteValue = new System.Windows.Forms.Label();
            this.labelWsqBlackValue = new System.Windows.Forms.Label();
            this.labelWsqWhite = new System.Windows.Forms.Label();
            this.labelWsqBlack = new System.Windows.Forms.Label();
            this.hScrollBarWsqWhite = new System.Windows.Forms.HScrollBar();
            this.hScrollBarWsqBlack = new System.Windows.Forms.HScrollBar();
            this.groupBoxCompressedImage = new System.Windows.Forms.GroupBox();
            this.radioButtonCompressedImage4 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage3 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage2 = new System.Windows.Forms.RadioButton();
            this.radioButtonCompressedImage1 = new System.Windows.Forms.RadioButton();
            this.panelBackgroundFrameGrayscale = new AccusoftCustom.PanelBackgroundFrame();
            this.groupBoxSettings.SuspendLayout();
            this.groupBoxImageType.SuspendLayout();
            this.tabControlSettings.SuspendLayout();
            this.tabPageJpeg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExifThumbnailSize)).BeginInit();
            this.groupBoxJpegWrap.SuspendLayout();
            this.groupBoxSubSampling.SuspendLayout();
            this.tabPageJpeg2000.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2Peak)).BeginInit();
            this.groupBoxJp2TileSize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileWidth)).BeginInit();
            this.groupBoxJp2Type.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2CompressSize)).BeginInit();
            this.groupBoxJp2Order.SuspendLayout();
            this.tabPageHdp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHdpQuantization)).BeginInit();
            this.groupBoxHdpOrder.SuspendLayout();
            this.groupBoxChromaSubSampling.SuspendLayout();
            this.tabPageLjp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpPredictor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpOrder)).BeginInit();
            this.tabPageJls.SuspendLayout();
            this.groupBoxJlsMax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsNear)).BeginInit();
            this.tabPageWsq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWsqQuantization)).BeginInit();
            this.groupBoxCompressedImage.SuspendLayout();
            this.panelBackgroundFrameGrayscale.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(178, 340);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(90, 32);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(274, 340);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(90, 32);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.BackColor = System.Drawing.Color.White;
            this.groupBoxSettings.Controls.Add(this.checkBoxPreview);
            this.groupBoxSettings.Controls.Add(this.buttonRestoreDefaults);
            this.groupBoxSettings.Location = new System.Drawing.Point(18, 210);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(136, 88);
            this.groupBoxSettings.TabIndex = 4;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Settings";
            // 
            // checkBoxPreview
            // 
            this.checkBoxPreview.AutoSize = true;
            this.checkBoxPreview.Checked = true;
            this.checkBoxPreview.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPreview.Location = new System.Drawing.Point(35, 23);
            this.checkBoxPreview.Name = "checkBoxPreview";
            this.checkBoxPreview.Size = new System.Drawing.Size(64, 17);
            this.checkBoxPreview.TabIndex = 5;
            this.checkBoxPreview.Text = "Preview";
            this.checkBoxPreview.UseVisualStyleBackColor = true;
            this.checkBoxPreview.CheckedChanged += new System.EventHandler(this.checkBoxPreview_CheckedChanged);
            // 
            // buttonRestoreDefaults
            // 
            this.buttonRestoreDefaults.Location = new System.Drawing.Point(6, 46);
            this.buttonRestoreDefaults.Name = "buttonRestoreDefaults";
            this.buttonRestoreDefaults.Size = new System.Drawing.Size(124, 32);
            this.buttonRestoreDefaults.TabIndex = 4;
            this.buttonRestoreDefaults.Text = "Restore All Defaults";
            this.buttonRestoreDefaults.UseVisualStyleBackColor = true;
            this.buttonRestoreDefaults.Click += new System.EventHandler(this.buttonRestoreDefaults_Click);
            // 
            // groupBoxImageType
            // 
            this.groupBoxImageType.BackColor = System.Drawing.Color.White;
            this.groupBoxImageType.Controls.Add(this.radioButtonWsq);
            this.groupBoxImageType.Controls.Add(this.radioButtonJls);
            this.groupBoxImageType.Controls.Add(this.radioButtonLjp);
            this.groupBoxImageType.Controls.Add(this.radioButtonHdp);
            this.groupBoxImageType.Controls.Add(this.radioButtonJpeg2000);
            this.groupBoxImageType.Controls.Add(this.radioButtonJpeg);
            this.groupBoxImageType.Location = new System.Drawing.Point(18, 69);
            this.groupBoxImageType.Name = "groupBoxImageType";
            this.groupBoxImageType.Size = new System.Drawing.Size(136, 135);
            this.groupBoxImageType.TabIndex = 1;
            this.groupBoxImageType.TabStop = false;
            this.groupBoxImageType.Text = "Image Type";
            // 
            // radioButtonWsq
            // 
            this.radioButtonWsq.AutoSize = true;
            this.radioButtonWsq.Location = new System.Drawing.Point(69, 96);
            this.radioButtonWsq.Name = "radioButtonWsq";
            this.radioButtonWsq.Size = new System.Drawing.Size(51, 17);
            this.radioButtonWsq.TabIndex = 8;
            this.radioButtonWsq.TabStop = true;
            this.radioButtonWsq.Text = "WSQ";
            this.radioButtonWsq.UseVisualStyleBackColor = true;
            this.radioButtonWsq.CheckedChanged += new System.EventHandler(this.radioButtonWsq_CheckedChanged);
            // 
            // radioButtonJls
            // 
            this.radioButtonJls.AutoSize = true;
            this.radioButtonJls.Location = new System.Drawing.Point(11, 96);
            this.radioButtonJls.Name = "radioButtonJls";
            this.radioButtonJls.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJls.TabIndex = 7;
            this.radioButtonJls.TabStop = true;
            this.radioButtonJls.Text = "JLS";
            this.radioButtonJls.UseVisualStyleBackColor = true;
            this.radioButtonJls.CheckedChanged += new System.EventHandler(this.radioButtonJls_CheckedChanged);
            // 
            // radioButtonLjp
            // 
            this.radioButtonLjp.AutoSize = true;
            this.radioButtonLjp.Location = new System.Drawing.Point(69, 64);
            this.radioButtonLjp.Name = "radioButtonLjp";
            this.radioButtonLjp.Size = new System.Drawing.Size(43, 17);
            this.radioButtonLjp.TabIndex = 6;
            this.radioButtonLjp.TabStop = true;
            this.radioButtonLjp.Text = "LJP";
            this.radioButtonLjp.UseVisualStyleBackColor = true;
            this.radioButtonLjp.CheckedChanged += new System.EventHandler(this.radioButtonLjp_CheckedChanged);
            // 
            // radioButtonHdp
            // 
            this.radioButtonHdp.AutoSize = true;
            this.radioButtonHdp.Location = new System.Drawing.Point(11, 64);
            this.radioButtonHdp.Name = "radioButtonHdp";
            this.radioButtonHdp.Size = new System.Drawing.Size(48, 17);
            this.radioButtonHdp.TabIndex = 5;
            this.radioButtonHdp.TabStop = true;
            this.radioButtonHdp.Text = "HDP";
            this.radioButtonHdp.UseVisualStyleBackColor = true;
            this.radioButtonHdp.CheckedChanged += new System.EventHandler(this.radioButtonHdp_CheckedChanged);
            // 
            // radioButtonJpeg2000
            // 
            this.radioButtonJpeg2000.Location = new System.Drawing.Point(69, 19);
            this.radioButtonJpeg2000.Name = "radioButtonJpeg2000";
            this.radioButtonJpeg2000.Size = new System.Drawing.Size(61, 40);
            this.radioButtonJpeg2000.TabIndex = 2;
            this.radioButtonJpeg2000.Text = "JPEG 2000";
            this.radioButtonJpeg2000.UseVisualStyleBackColor = true;
            this.radioButtonJpeg2000.CheckedChanged += new System.EventHandler(this.radioButtonJpeg2000_CheckedChanged);
            // 
            // radioButtonJpeg
            // 
            this.radioButtonJpeg.AutoSize = true;
            this.radioButtonJpeg.Checked = true;
            this.radioButtonJpeg.Location = new System.Drawing.Point(11, 31);
            this.radioButtonJpeg.Name = "radioButtonJpeg";
            this.radioButtonJpeg.Size = new System.Drawing.Size(52, 17);
            this.radioButtonJpeg.TabIndex = 0;
            this.radioButtonJpeg.TabStop = true;
            this.radioButtonJpeg.Text = "JPEG";
            this.radioButtonJpeg.UseVisualStyleBackColor = true;
            this.radioButtonJpeg.CheckedChanged += new System.EventHandler(this.radioButtonJpeg_CheckedChanged);
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Controls.Add(this.tabPageJpeg);
            this.tabControlSettings.Controls.Add(this.tabPageJpeg2000);
            this.tabControlSettings.Controls.Add(this.tabPageHdp);
            this.tabControlSettings.Controls.Add(this.tabPageLjp);
            this.tabControlSettings.Controls.Add(this.tabPageJls);
            this.tabControlSettings.Controls.Add(this.tabPageWsq);
            this.tabControlSettings.ItemSize = new System.Drawing.Size(42, 18);
            this.tabControlSettings.Location = new System.Drawing.Point(156, 23);
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(352, 275);
            this.tabControlSettings.TabIndex = 0;
            this.tabControlSettings.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPageJpeg
            // 
            this.tabPageJpeg.BackColor = System.Drawing.Color.White;
            this.tabPageJpeg.Controls.Add(this.numericUpDownExifThumbnailSize);
            this.tabPageJpeg.Controls.Add(this.labelExifThumbnailSize);
            this.tabPageJpeg.Controls.Add(this.groupBoxJpegWrap);
            this.tabPageJpeg.Controls.Add(this.checkBoxJpegProgressive);
            this.tabPageJpeg.Controls.Add(this.groupBoxSubSampling);
            this.tabPageJpeg.Controls.Add(this.checkBoxJpegCosited);
            this.tabPageJpeg.Controls.Add(this.labelChrominanceValue);
            this.tabPageJpeg.Controls.Add(this.labelLuminanceValue);
            this.tabPageJpeg.Controls.Add(this.labelChrominance);
            this.tabPageJpeg.Controls.Add(this.labelLuminance);
            this.tabPageJpeg.Controls.Add(this.hScrollBarChrominance);
            this.tabPageJpeg.Controls.Add(this.hScrollBarLuminance);
            this.tabPageJpeg.Location = new System.Drawing.Point(4, 22);
            this.tabPageJpeg.Name = "tabPageJpeg";
            this.tabPageJpeg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageJpeg.Size = new System.Drawing.Size(344, 249);
            this.tabPageJpeg.TabIndex = 0;
            this.tabPageJpeg.Text = "JPEG";
            // 
            // numericUpDownExifThumbnailSize
            // 
            this.numericUpDownExifThumbnailSize.Enabled = false;
            this.numericUpDownExifThumbnailSize.Location = new System.Drawing.Point(238, 180);
            this.numericUpDownExifThumbnailSize.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.numericUpDownExifThumbnailSize.Name = "numericUpDownExifThumbnailSize";
            this.numericUpDownExifThumbnailSize.Size = new System.Drawing.Size(61, 20);
            this.numericUpDownExifThumbnailSize.TabIndex = 5;
            this.numericUpDownExifThumbnailSize.ValueChanged += new System.EventHandler(this.numericUpDownExifThumbnailSize_ValueChanged);
            // 
            // labelExifThumbnailSize
            // 
            this.labelExifThumbnailSize.AutoSize = true;
            this.labelExifThumbnailSize.Location = new System.Drawing.Point(235, 164);
            this.labelExifThumbnailSize.Name = "labelExifThumbnailSize";
            this.labelExifThumbnailSize.Size = new System.Drawing.Size(102, 13);
            this.labelExifThumbnailSize.TabIndex = 6;
            this.labelExifThumbnailSize.Text = "Exif Thumbnail Size:";
            // 
            // groupBoxJpegWrap
            // 
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegWrapInExif);
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegNoWrap);
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegWrapInPdf);
            this.groupBoxJpegWrap.Controls.Add(this.radioButtonJpegWrapInTiff);
            this.groupBoxJpegWrap.Location = new System.Drawing.Point(113, 97);
            this.groupBoxJpegWrap.Name = "groupBoxJpegWrap";
            this.groupBoxJpegWrap.Size = new System.Drawing.Size(119, 108);
            this.groupBoxJpegWrap.TabIndex = 18;
            this.groupBoxJpegWrap.TabStop = false;
            this.groupBoxJpegWrap.Text = "Wrap";
            // 
            // radioButtonJpegWrapInExif
            // 
            this.radioButtonJpegWrapInExif.AutoSize = true;
            this.radioButtonJpegWrapInExif.Location = new System.Drawing.Point(9, 78);
            this.radioButtonJpegWrapInExif.Name = "radioButtonJpegWrapInExif";
            this.radioButtonJpegWrapInExif.Size = new System.Drawing.Size(103, 17);
            this.radioButtonJpegWrapInExif.TabIndex = 4;
            this.radioButtonJpegWrapInExif.Text = "Wrap in an EXIF";
            this.radioButtonJpegWrapInExif.UseVisualStyleBackColor = true;
            this.radioButtonJpegWrapInExif.CheckedChanged += new System.EventHandler(this.radioButtonJpegWrapInExif_CheckedChanged);
            // 
            // radioButtonJpegNoWrap
            // 
            this.radioButtonJpegNoWrap.AutoSize = true;
            this.radioButtonJpegNoWrap.Checked = true;
            this.radioButtonJpegNoWrap.Location = new System.Drawing.Point(9, 19);
            this.radioButtonJpegNoWrap.Name = "radioButtonJpegNoWrap";
            this.radioButtonJpegNoWrap.Size = new System.Drawing.Size(83, 17);
            this.radioButtonJpegNoWrap.TabIndex = 3;
            this.radioButtonJpegNoWrap.TabStop = true;
            this.radioButtonJpegNoWrap.Text = "Do not wrap";
            this.radioButtonJpegNoWrap.UseVisualStyleBackColor = true;
            this.radioButtonJpegNoWrap.CheckedChanged += new System.EventHandler(this.radioButtonJpegNoWrap_CheckedChanged);
            // 
            // radioButtonJpegWrapInPdf
            // 
            this.radioButtonJpegWrapInPdf.AutoSize = true;
            this.radioButtonJpegWrapInPdf.Location = new System.Drawing.Point(9, 59);
            this.radioButtonJpegWrapInPdf.Name = "radioButtonJpegWrapInPdf";
            this.radioButtonJpegWrapInPdf.Size = new System.Drawing.Size(95, 17);
            this.radioButtonJpegWrapInPdf.TabIndex = 2;
            this.radioButtonJpegWrapInPdf.Text = "Wrap in a PDF";
            this.radioButtonJpegWrapInPdf.UseVisualStyleBackColor = true;
            this.radioButtonJpegWrapInPdf.CheckedChanged += new System.EventHandler(this.radioButtonJpegWrapInPdf_CheckedChanged);
            // 
            // radioButtonJpegWrapInTiff
            // 
            this.radioButtonJpegWrapInTiff.AutoSize = true;
            this.radioButtonJpegWrapInTiff.Location = new System.Drawing.Point(9, 38);
            this.radioButtonJpegWrapInTiff.Name = "radioButtonJpegWrapInTiff";
            this.radioButtonJpegWrapInTiff.Size = new System.Drawing.Size(96, 17);
            this.radioButtonJpegWrapInTiff.TabIndex = 1;
            this.radioButtonJpegWrapInTiff.Text = "Wrap in a TIFF";
            this.radioButtonJpegWrapInTiff.UseVisualStyleBackColor = true;
            this.radioButtonJpegWrapInTiff.CheckedChanged += new System.EventHandler(this.radioButtonJpegWrapInTiff_CheckedChanged);
            // 
            // checkBoxJpegProgressive
            // 
            this.checkBoxJpegProgressive.AutoSize = true;
            this.checkBoxJpegProgressive.Location = new System.Drawing.Point(238, 139);
            this.checkBoxJpegProgressive.Name = "checkBoxJpegProgressive";
            this.checkBoxJpegProgressive.Size = new System.Drawing.Size(81, 17);
            this.checkBoxJpegProgressive.TabIndex = 17;
            this.checkBoxJpegProgressive.Text = "Progressive";
            this.checkBoxJpegProgressive.UseVisualStyleBackColor = true;
            this.checkBoxJpegProgressive.CheckedChanged += new System.EventHandler(this.checkBoxJpegProgressive_CheckedChanged);
            // 
            // groupBoxSubSampling
            // 
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling411);
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling211v);
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling211);
            this.groupBoxSubSampling.Controls.Add(this.radioButtonJpegSubSampling111);
            this.groupBoxSubSampling.Location = new System.Drawing.Point(17, 97);
            this.groupBoxSubSampling.Name = "groupBoxSubSampling";
            this.groupBoxSubSampling.Size = new System.Drawing.Size(88, 108);
            this.groupBoxSubSampling.TabIndex = 16;
            this.groupBoxSubSampling.TabStop = false;
            this.groupBoxSubSampling.Text = "Sub Sampling";
            // 
            // radioButtonJpegSubSampling411
            // 
            this.radioButtonJpegSubSampling411.AutoSize = true;
            this.radioButtonJpegSubSampling411.Location = new System.Drawing.Point(20, 78);
            this.radioButtonJpegSubSampling411.Name = "radioButtonJpegSubSampling411";
            this.radioButtonJpegSubSampling411.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJpegSubSampling411.TabIndex = 14;
            this.radioButtonJpegSubSampling411.TabStop = true;
            this.radioButtonJpegSubSampling411.Text = "411";
            this.radioButtonJpegSubSampling411.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling411.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling411_CheckedChanged);
            // 
            // radioButtonJpegSubSampling211v
            // 
            this.radioButtonJpegSubSampling211v.AutoSize = true;
            this.radioButtonJpegSubSampling211v.Location = new System.Drawing.Point(20, 59);
            this.radioButtonJpegSubSampling211v.Name = "radioButtonJpegSubSampling211v";
            this.radioButtonJpegSubSampling211v.Size = new System.Drawing.Size(49, 17);
            this.radioButtonJpegSubSampling211v.TabIndex = 13;
            this.radioButtonJpegSubSampling211v.TabStop = true;
            this.radioButtonJpegSubSampling211v.Text = "211v";
            this.radioButtonJpegSubSampling211v.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling211v.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling211v_CheckedChanged);
            // 
            // radioButtonJpegSubSampling211
            // 
            this.radioButtonJpegSubSampling211.AutoSize = true;
            this.radioButtonJpegSubSampling211.Location = new System.Drawing.Point(20, 38);
            this.radioButtonJpegSubSampling211.Name = "radioButtonJpegSubSampling211";
            this.radioButtonJpegSubSampling211.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJpegSubSampling211.TabIndex = 12;
            this.radioButtonJpegSubSampling211.TabStop = true;
            this.radioButtonJpegSubSampling211.Text = "211";
            this.radioButtonJpegSubSampling211.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling211.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling211_CheckedChanged);
            // 
            // radioButtonJpegSubSampling111
            // 
            this.radioButtonJpegSubSampling111.AutoSize = true;
            this.radioButtonJpegSubSampling111.Location = new System.Drawing.Point(20, 19);
            this.radioButtonJpegSubSampling111.Name = "radioButtonJpegSubSampling111";
            this.radioButtonJpegSubSampling111.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJpegSubSampling111.TabIndex = 11;
            this.radioButtonJpegSubSampling111.TabStop = true;
            this.radioButtonJpegSubSampling111.Text = "111";
            this.radioButtonJpegSubSampling111.UseVisualStyleBackColor = true;
            this.radioButtonJpegSubSampling111.CheckedChanged += new System.EventHandler(this.radioButtonJpegSubSampling111_CheckedChanged);
            // 
            // checkBoxJpegCosited
            // 
            this.checkBoxJpegCosited.AutoSize = true;
            this.checkBoxJpegCosited.Location = new System.Drawing.Point(238, 116);
            this.checkBoxJpegCosited.Name = "checkBoxJpegCosited";
            this.checkBoxJpegCosited.Size = new System.Drawing.Size(61, 17);
            this.checkBoxJpegCosited.TabIndex = 7;
            this.checkBoxJpegCosited.Text = "Cosited";
            this.checkBoxJpegCosited.UseVisualStyleBackColor = true;
            this.checkBoxJpegCosited.CheckedChanged += new System.EventHandler(this.checkBoxJpegCosited_CheckedChanged);
            // 
            // labelChrominanceValue
            // 
            this.labelChrominanceValue.AutoSize = true;
            this.labelChrominanceValue.Location = new System.Drawing.Point(294, 71);
            this.labelChrominanceValue.Name = "labelChrominanceValue";
            this.labelChrominanceValue.Size = new System.Drawing.Size(36, 13);
            this.labelChrominanceValue.TabIndex = 6;
            this.labelChrominanceValue.Text = "chrom";
            // 
            // labelLuminanceValue
            // 
            this.labelLuminanceValue.AutoSize = true;
            this.labelLuminanceValue.Location = new System.Drawing.Point(294, 36);
            this.labelLuminanceValue.Name = "labelLuminanceValue";
            this.labelLuminanceValue.Size = new System.Drawing.Size(23, 13);
            this.labelLuminanceValue.TabIndex = 5;
            this.labelLuminanceValue.Text = "lum";
            // 
            // labelChrominance
            // 
            this.labelChrominance.AutoSize = true;
            this.labelChrominance.Location = new System.Drawing.Point(14, 68);
            this.labelChrominance.Name = "labelChrominance";
            this.labelChrominance.Size = new System.Drawing.Size(72, 13);
            this.labelChrominance.TabIndex = 4;
            this.labelChrominance.Text = "Chrominance:";
            // 
            // labelLuminance
            // 
            this.labelLuminance.AutoSize = true;
            this.labelLuminance.Location = new System.Drawing.Point(15, 34);
            this.labelLuminance.Name = "labelLuminance";
            this.labelLuminance.Size = new System.Drawing.Size(62, 13);
            this.labelLuminance.TabIndex = 3;
            this.labelLuminance.Text = "Luminance:";
            // 
            // hScrollBarChrominance
            // 
            this.hScrollBarChrominance.LargeChange = 1;
            this.hScrollBarChrominance.Location = new System.Drawing.Point(89, 68);
            this.hScrollBarChrominance.Maximum = 255;
            this.hScrollBarChrominance.Name = "hScrollBarChrominance";
            this.hScrollBarChrominance.Size = new System.Drawing.Size(192, 19);
            this.hScrollBarChrominance.TabIndex = 2;
            this.hScrollBarChrominance.ValueChanged += new System.EventHandler(this.hScrollBarChrominance_ValueChanged);
            // 
            // hScrollBarLuminance
            // 
            this.hScrollBarLuminance.LargeChange = 1;
            this.hScrollBarLuminance.Location = new System.Drawing.Point(88, 34);
            this.hScrollBarLuminance.Maximum = 255;
            this.hScrollBarLuminance.Name = "hScrollBarLuminance";
            this.hScrollBarLuminance.Size = new System.Drawing.Size(193, 17);
            this.hScrollBarLuminance.TabIndex = 1;
            this.hScrollBarLuminance.ValueChanged += new System.EventHandler(this.hScrollBarLuminance_ValueChanged);
            // 
            // tabPageJpeg2000
            // 
            this.tabPageJpeg2000.BackColor = System.Drawing.Color.White;
            this.tabPageJpeg2000.Controls.Add(this.labelJp2Peak);
            this.tabPageJpeg2000.Controls.Add(this.numericUpDownJp2Peak);
            this.tabPageJpeg2000.Controls.Add(this.groupBoxJp2TileSize);
            this.tabPageJpeg2000.Controls.Add(this.groupBoxJp2Type);
            this.tabPageJpeg2000.Controls.Add(this.numericUpDownJp2CompressSize);
            this.tabPageJpeg2000.Controls.Add(this.labelJp2CompressSize);
            this.tabPageJpeg2000.Controls.Add(this.groupBoxJp2Order);
            this.tabPageJpeg2000.Location = new System.Drawing.Point(4, 22);
            this.tabPageJpeg2000.Name = "tabPageJpeg2000";
            this.tabPageJpeg2000.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageJpeg2000.Size = new System.Drawing.Size(344, 249);
            this.tabPageJpeg2000.TabIndex = 1;
            this.tabPageJpeg2000.Text = "JPEG 2000";
            // 
            // labelJp2Peak
            // 
            this.labelJp2Peak.Location = new System.Drawing.Point(240, 116);
            this.labelJp2Peak.Name = "labelJp2Peak";
            this.labelJp2Peak.Size = new System.Drawing.Size(86, 27);
            this.labelJp2Peak.TabIndex = 11;
            this.labelJp2Peak.Text = "Peak Signal To Noise Ratio:";
            // 
            // numericUpDownJp2Peak
            // 
            this.numericUpDownJp2Peak.DecimalPlaces = 1;
            this.numericUpDownJp2Peak.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownJp2Peak.Location = new System.Drawing.Point(243, 146);
            this.numericUpDownJp2Peak.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownJp2Peak.Name = "numericUpDownJp2Peak";
            this.numericUpDownJp2Peak.Size = new System.Drawing.Size(69, 20);
            this.numericUpDownJp2Peak.TabIndex = 10;
            this.numericUpDownJp2Peak.ValueChanged += new System.EventHandler(this.numericUpDownJp2Peak_ValueChanged);
            // 
            // groupBoxJp2TileSize
            // 
            this.groupBoxJp2TileSize.Controls.Add(this.labelJp2TileHeight);
            this.groupBoxJp2TileSize.Controls.Add(this.labelJp2TileWidth);
            this.groupBoxJp2TileSize.Controls.Add(this.numericUpDownJp2TileHeight);
            this.groupBoxJp2TileSize.Controls.Add(this.numericUpDownJp2TileWidth);
            this.groupBoxJp2TileSize.Location = new System.Drawing.Point(6, 176);
            this.groupBoxJp2TileSize.Name = "groupBoxJp2TileSize";
            this.groupBoxJp2TileSize.Size = new System.Drawing.Size(137, 65);
            this.groupBoxJp2TileSize.TabIndex = 9;
            this.groupBoxJp2TileSize.TabStop = false;
            this.groupBoxJp2TileSize.Text = "Tile Size";
            // 
            // labelJp2TileHeight
            // 
            this.labelJp2TileHeight.AutoSize = true;
            this.labelJp2TileHeight.Location = new System.Drawing.Point(68, 19);
            this.labelJp2TileHeight.Name = "labelJp2TileHeight";
            this.labelJp2TileHeight.Size = new System.Drawing.Size(38, 13);
            this.labelJp2TileHeight.TabIndex = 10;
            this.labelJp2TileHeight.Text = "Height";
            // 
            // labelJp2TileWidth
            // 
            this.labelJp2TileWidth.AutoSize = true;
            this.labelJp2TileWidth.Location = new System.Drawing.Point(5, 19);
            this.labelJp2TileWidth.Name = "labelJp2TileWidth";
            this.labelJp2TileWidth.Size = new System.Drawing.Size(35, 13);
            this.labelJp2TileWidth.TabIndex = 9;
            this.labelJp2TileWidth.Text = "Width";
            // 
            // numericUpDownJp2TileHeight
            // 
            this.numericUpDownJp2TileHeight.Location = new System.Drawing.Point(71, 35);
            this.numericUpDownJp2TileHeight.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownJp2TileHeight.Name = "numericUpDownJp2TileHeight";
            this.numericUpDownJp2TileHeight.Size = new System.Drawing.Size(53, 20);
            this.numericUpDownJp2TileHeight.TabIndex = 8;
            this.numericUpDownJp2TileHeight.ValueChanged += new System.EventHandler(this.numericUpDownJp2TileHeight_ValueChanged);
            // 
            // numericUpDownJp2TileWidth
            // 
            this.numericUpDownJp2TileWidth.Location = new System.Drawing.Point(8, 35);
            this.numericUpDownJp2TileWidth.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownJp2TileWidth.Name = "numericUpDownJp2TileWidth";
            this.numericUpDownJp2TileWidth.Size = new System.Drawing.Size(57, 20);
            this.numericUpDownJp2TileWidth.TabIndex = 6;
            this.numericUpDownJp2TileWidth.ValueChanged += new System.EventHandler(this.numericUpDownJp2TileWidth_ValueChanged);
            // 
            // groupBoxJp2Type
            // 
            this.groupBoxJp2Type.Controls.Add(this.radioButtonJp2Lossy);
            this.groupBoxJp2Type.Controls.Add(this.radioButtonJp2Lossless);
            this.groupBoxJp2Type.Location = new System.Drawing.Point(243, 6);
            this.groupBoxJp2Type.Name = "groupBoxJp2Type";
            this.groupBoxJp2Type.Size = new System.Drawing.Size(85, 65);
            this.groupBoxJp2Type.TabIndex = 5;
            this.groupBoxJp2Type.TabStop = false;
            this.groupBoxJp2Type.Text = "Type";
            // 
            // radioButtonJp2Lossy
            // 
            this.radioButtonJp2Lossy.AutoSize = true;
            this.radioButtonJp2Lossy.Location = new System.Drawing.Point(11, 41);
            this.radioButtonJp2Lossy.Name = "radioButtonJp2Lossy";
            this.radioButtonJp2Lossy.Size = new System.Drawing.Size(52, 17);
            this.radioButtonJp2Lossy.TabIndex = 1;
            this.radioButtonJp2Lossy.TabStop = true;
            this.radioButtonJp2Lossy.Text = "Lossy";
            this.radioButtonJp2Lossy.UseVisualStyleBackColor = true;
            this.radioButtonJp2Lossy.CheckedChanged += new System.EventHandler(this.radioButtonJp2Lossy_CheckedChanged);
            // 
            // radioButtonJp2Lossless
            // 
            this.radioButtonJp2Lossless.AutoSize = true;
            this.radioButtonJp2Lossless.Location = new System.Drawing.Point(11, 18);
            this.radioButtonJp2Lossless.Name = "radioButtonJp2Lossless";
            this.radioButtonJp2Lossless.Size = new System.Drawing.Size(65, 17);
            this.radioButtonJp2Lossless.TabIndex = 0;
            this.radioButtonJp2Lossless.TabStop = true;
            this.radioButtonJp2Lossless.Text = "Lossless";
            this.radioButtonJp2Lossless.UseVisualStyleBackColor = true;
            this.radioButtonJp2Lossless.CheckedChanged += new System.EventHandler(this.radioButtonJp2Lossless_CheckedChanged);
            // 
            // numericUpDownJp2CompressSize
            // 
            this.numericUpDownJp2CompressSize.Location = new System.Drawing.Point(243, 91);
            this.numericUpDownJp2CompressSize.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownJp2CompressSize.Name = "numericUpDownJp2CompressSize";
            this.numericUpDownJp2CompressSize.Size = new System.Drawing.Size(82, 20);
            this.numericUpDownJp2CompressSize.TabIndex = 0;
            this.numericUpDownJp2CompressSize.ValueChanged += new System.EventHandler(this.numericUpDownJp2CompressSize_ValueChanged);
            // 
            // labelJp2CompressSize
            // 
            this.labelJp2CompressSize.AutoSize = true;
            this.labelJp2CompressSize.Location = new System.Drawing.Point(240, 75);
            this.labelJp2CompressSize.Name = "labelJp2CompressSize";
            this.labelJp2CompressSize.Size = new System.Drawing.Size(79, 13);
            this.labelJp2CompressSize.TabIndex = 1;
            this.labelJp2CompressSize.Text = "Compress Size:";
            // 
            // groupBoxJp2Order
            // 
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderRpcl);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderRlcp);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderPcrl);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderLrcp);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderCprl);
            this.groupBoxJp2Order.Controls.Add(this.radioButtonJp2OrderDefault);
            this.groupBoxJp2Order.Location = new System.Drawing.Point(6, 6);
            this.groupBoxJp2Order.Name = "groupBoxJp2Order";
            this.groupBoxJp2Order.Size = new System.Drawing.Size(228, 164);
            this.groupBoxJp2Order.TabIndex = 3;
            this.groupBoxJp2Order.TabStop = false;
            this.groupBoxJp2Order.Text = "Order";
            // 
            // radioButtonJp2OrderRpcl
            // 
            this.radioButtonJp2OrderRpcl.AutoSize = true;
            this.radioButtonJp2OrderRpcl.Location = new System.Drawing.Point(16, 134);
            this.radioButtonJp2OrderRpcl.Name = "radioButtonJp2OrderRpcl";
            this.radioButtonJp2OrderRpcl.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderRpcl.TabIndex = 5;
            this.radioButtonJp2OrderRpcl.TabStop = true;
            this.radioButtonJp2OrderRpcl.Text = "Resolution Position Component Layer";
            this.radioButtonJp2OrderRpcl.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderRpcl.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderRpcl_CheckedChanged);
            // 
            // radioButtonJp2OrderRlcp
            // 
            this.radioButtonJp2OrderRlcp.AutoSize = true;
            this.radioButtonJp2OrderRlcp.Location = new System.Drawing.Point(16, 111);
            this.radioButtonJp2OrderRlcp.Name = "radioButtonJp2OrderRlcp";
            this.radioButtonJp2OrderRlcp.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderRlcp.TabIndex = 4;
            this.radioButtonJp2OrderRlcp.TabStop = true;
            this.radioButtonJp2OrderRlcp.Text = "Resolution Layer Component Position";
            this.radioButtonJp2OrderRlcp.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderRlcp.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderRlcp_CheckedChanged);
            // 
            // radioButtonJp2OrderPcrl
            // 
            this.radioButtonJp2OrderPcrl.AutoSize = true;
            this.radioButtonJp2OrderPcrl.Location = new System.Drawing.Point(16, 88);
            this.radioButtonJp2OrderPcrl.Name = "radioButtonJp2OrderPcrl";
            this.radioButtonJp2OrderPcrl.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderPcrl.TabIndex = 3;
            this.radioButtonJp2OrderPcrl.TabStop = true;
            this.radioButtonJp2OrderPcrl.Text = "Position Component Resolution Layer";
            this.radioButtonJp2OrderPcrl.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderPcrl.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderPcrl_CheckedChanged);
            // 
            // radioButtonJp2OrderLrcp
            // 
            this.radioButtonJp2OrderLrcp.AutoSize = true;
            this.radioButtonJp2OrderLrcp.Location = new System.Drawing.Point(16, 65);
            this.radioButtonJp2OrderLrcp.Name = "radioButtonJp2OrderLrcp";
            this.radioButtonJp2OrderLrcp.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderLrcp.TabIndex = 2;
            this.radioButtonJp2OrderLrcp.TabStop = true;
            this.radioButtonJp2OrderLrcp.Text = "Layer Resolution Component Position";
            this.radioButtonJp2OrderLrcp.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderLrcp.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderLrcp_CheckedChanged);
            // 
            // radioButtonJp2OrderCprl
            // 
            this.radioButtonJp2OrderCprl.AutoSize = true;
            this.radioButtonJp2OrderCprl.Location = new System.Drawing.Point(16, 42);
            this.radioButtonJp2OrderCprl.Name = "radioButtonJp2OrderCprl";
            this.radioButtonJp2OrderCprl.Size = new System.Drawing.Size(201, 17);
            this.radioButtonJp2OrderCprl.TabIndex = 1;
            this.radioButtonJp2OrderCprl.TabStop = true;
            this.radioButtonJp2OrderCprl.Text = "Component Position Resolution Layer";
            this.radioButtonJp2OrderCprl.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderCprl.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderCprl_CheckedChanged);
            // 
            // radioButtonJp2OrderDefault
            // 
            this.radioButtonJp2OrderDefault.AutoSize = true;
            this.radioButtonJp2OrderDefault.Location = new System.Drawing.Point(16, 19);
            this.radioButtonJp2OrderDefault.Name = "radioButtonJp2OrderDefault";
            this.radioButtonJp2OrderDefault.Size = new System.Drawing.Size(59, 17);
            this.radioButtonJp2OrderDefault.TabIndex = 0;
            this.radioButtonJp2OrderDefault.TabStop = true;
            this.radioButtonJp2OrderDefault.Text = "Default";
            this.radioButtonJp2OrderDefault.UseVisualStyleBackColor = true;
            this.radioButtonJp2OrderDefault.CheckedChanged += new System.EventHandler(this.radioButtonJp2OrderDefault_CheckedChanged);
            // 
            // tabPageHdp
            // 
            this.tabPageHdp.BackColor = System.Drawing.Color.White;
            this.tabPageHdp.Controls.Add(this.numericUpDownHdpQuantization);
            this.tabPageHdp.Controls.Add(this.labelHdpQuantization);
            this.tabPageHdp.Controls.Add(this.groupBoxHdpOrder);
            this.tabPageHdp.Controls.Add(this.groupBoxChromaSubSampling);
            this.tabPageHdp.Location = new System.Drawing.Point(4, 22);
            this.tabPageHdp.Name = "tabPageHdp";
            this.tabPageHdp.Size = new System.Drawing.Size(344, 249);
            this.tabPageHdp.TabIndex = 5;
            this.tabPageHdp.Text = "HDP";
            // 
            // numericUpDownHdpQuantization
            // 
            this.numericUpDownHdpQuantization.Location = new System.Drawing.Point(185, 165);
            this.numericUpDownHdpQuantization.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownHdpQuantization.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHdpQuantization.Name = "numericUpDownHdpQuantization";
            this.numericUpDownHdpQuantization.Size = new System.Drawing.Size(77, 20);
            this.numericUpDownHdpQuantization.TabIndex = 9;
            this.numericUpDownHdpQuantization.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHdpQuantization.ValueChanged += new System.EventHandler(this.numericUpDownHdpQuantization_ValueChanged);
            // 
            // labelHdpQuantization
            // 
            this.labelHdpQuantization.AutoSize = true;
            this.labelHdpQuantization.Location = new System.Drawing.Point(182, 149);
            this.labelHdpQuantization.Name = "labelHdpQuantization";
            this.labelHdpQuantization.Size = new System.Drawing.Size(69, 13);
            this.labelHdpQuantization.TabIndex = 7;
            this.labelHdpQuantization.Text = "Quantization:";
            // 
            // groupBoxHdpOrder
            // 
            this.groupBoxHdpOrder.Controls.Add(this.radioButtonHdpOrderFrequency);
            this.groupBoxHdpOrder.Controls.Add(this.radioButtonHdpOrderSpacial);
            this.groupBoxHdpOrder.Location = new System.Drawing.Point(185, 68);
            this.groupBoxHdpOrder.Name = "groupBoxHdpOrder";
            this.groupBoxHdpOrder.Size = new System.Drawing.Size(121, 73);
            this.groupBoxHdpOrder.TabIndex = 1;
            this.groupBoxHdpOrder.TabStop = false;
            this.groupBoxHdpOrder.Text = "Order";
            // 
            // radioButtonHdpOrderFrequency
            // 
            this.radioButtonHdpOrderFrequency.AutoSize = true;
            this.radioButtonHdpOrderFrequency.Location = new System.Drawing.Point(17, 42);
            this.radioButtonHdpOrderFrequency.Name = "radioButtonHdpOrderFrequency";
            this.radioButtonHdpOrderFrequency.Size = new System.Drawing.Size(75, 17);
            this.radioButtonHdpOrderFrequency.TabIndex = 1;
            this.radioButtonHdpOrderFrequency.TabStop = true;
            this.radioButtonHdpOrderFrequency.Text = "Frequency";
            this.radioButtonHdpOrderFrequency.UseVisualStyleBackColor = true;
            this.radioButtonHdpOrderFrequency.CheckedChanged += new System.EventHandler(this.radioButtonHdpOrderFrequency_CheckedChanged);
            // 
            // radioButtonHdpOrderSpacial
            // 
            this.radioButtonHdpOrderSpacial.AutoSize = true;
            this.radioButtonHdpOrderSpacial.Location = new System.Drawing.Point(17, 19);
            this.radioButtonHdpOrderSpacial.Name = "radioButtonHdpOrderSpacial";
            this.radioButtonHdpOrderSpacial.Size = new System.Drawing.Size(60, 17);
            this.radioButtonHdpOrderSpacial.TabIndex = 0;
            this.radioButtonHdpOrderSpacial.TabStop = true;
            this.radioButtonHdpOrderSpacial.Text = "Spacial";
            this.radioButtonHdpOrderSpacial.UseVisualStyleBackColor = true;
            this.radioButtonHdpOrderSpacial.CheckedChanged += new System.EventHandler(this.radioButtonHdpOrderSpacial_CheckedChanged);
            // 
            // groupBoxChromaSubSampling
            // 
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling444);
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling422);
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling420);
            this.groupBoxChromaSubSampling.Controls.Add(this.radioButtonHdpChromaSubSampling400);
            this.groupBoxChromaSubSampling.Location = new System.Drawing.Point(27, 68);
            this.groupBoxChromaSubSampling.Name = "groupBoxChromaSubSampling";
            this.groupBoxChromaSubSampling.Size = new System.Drawing.Size(122, 117);
            this.groupBoxChromaSubSampling.TabIndex = 0;
            this.groupBoxChromaSubSampling.TabStop = false;
            this.groupBoxChromaSubSampling.Text = "Chroma SubSampling";
            // 
            // radioButtonHdpChromaSubSampling444
            // 
            this.radioButtonHdpChromaSubSampling444.AutoSize = true;
            this.radioButtonHdpChromaSubSampling444.Location = new System.Drawing.Point(21, 90);
            this.radioButtonHdpChromaSubSampling444.Name = "radioButtonHdpChromaSubSampling444";
            this.radioButtonHdpChromaSubSampling444.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling444.TabIndex = 3;
            this.radioButtonHdpChromaSubSampling444.TabStop = true;
            this.radioButtonHdpChromaSubSampling444.Text = "444";
            this.radioButtonHdpChromaSubSampling444.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling444.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling444_CheckedChanged);
            // 
            // radioButtonHdpChromaSubSampling422
            // 
            this.radioButtonHdpChromaSubSampling422.AutoSize = true;
            this.radioButtonHdpChromaSubSampling422.Location = new System.Drawing.Point(21, 67);
            this.radioButtonHdpChromaSubSampling422.Name = "radioButtonHdpChromaSubSampling422";
            this.radioButtonHdpChromaSubSampling422.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling422.TabIndex = 2;
            this.radioButtonHdpChromaSubSampling422.TabStop = true;
            this.radioButtonHdpChromaSubSampling422.Text = "422";
            this.radioButtonHdpChromaSubSampling422.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling422.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling422_CheckedChanged);
            // 
            // radioButtonHdpChromaSubSampling420
            // 
            this.radioButtonHdpChromaSubSampling420.AutoSize = true;
            this.radioButtonHdpChromaSubSampling420.Location = new System.Drawing.Point(21, 42);
            this.radioButtonHdpChromaSubSampling420.Name = "radioButtonHdpChromaSubSampling420";
            this.radioButtonHdpChromaSubSampling420.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling420.TabIndex = 1;
            this.radioButtonHdpChromaSubSampling420.TabStop = true;
            this.radioButtonHdpChromaSubSampling420.Text = "420";
            this.radioButtonHdpChromaSubSampling420.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling420.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling420_CheckedChanged);
            // 
            // radioButtonHdpChromaSubSampling400
            // 
            this.radioButtonHdpChromaSubSampling400.AutoSize = true;
            this.radioButtonHdpChromaSubSampling400.Location = new System.Drawing.Point(21, 19);
            this.radioButtonHdpChromaSubSampling400.Name = "radioButtonHdpChromaSubSampling400";
            this.radioButtonHdpChromaSubSampling400.Size = new System.Drawing.Size(43, 17);
            this.radioButtonHdpChromaSubSampling400.TabIndex = 0;
            this.radioButtonHdpChromaSubSampling400.TabStop = true;
            this.radioButtonHdpChromaSubSampling400.Text = "400";
            this.radioButtonHdpChromaSubSampling400.UseVisualStyleBackColor = true;
            this.radioButtonHdpChromaSubSampling400.CheckedChanged += new System.EventHandler(this.radioButtonHdpChromaSubSampling400_CheckedChanged);
            // 
            // tabPageLjp
            // 
            this.tabPageLjp.BackColor = System.Drawing.Color.White;
            this.tabPageLjp.Controls.Add(this.labelLjpPredictor);
            this.tabPageLjp.Controls.Add(this.labelLjpOrder);
            this.tabPageLjp.Controls.Add(this.numericUpDownLjpPredictor);
            this.tabPageLjp.Controls.Add(this.numericUpDownLjpOrder);
            this.tabPageLjp.Location = new System.Drawing.Point(4, 22);
            this.tabPageLjp.Name = "tabPageLjp";
            this.tabPageLjp.Size = new System.Drawing.Size(344, 249);
            this.tabPageLjp.TabIndex = 6;
            this.tabPageLjp.Text = "LJP";
            // 
            // labelLjpPredictor
            // 
            this.labelLjpPredictor.AutoSize = true;
            this.labelLjpPredictor.Location = new System.Drawing.Point(104, 134);
            this.labelLjpPredictor.Name = "labelLjpPredictor";
            this.labelLjpPredictor.Size = new System.Drawing.Size(52, 13);
            this.labelLjpPredictor.TabIndex = 4;
            this.labelLjpPredictor.Text = "Predictor:";
            // 
            // labelLjpOrder
            // 
            this.labelLjpOrder.AutoSize = true;
            this.labelLjpOrder.Location = new System.Drawing.Point(105, 104);
            this.labelLjpOrder.Name = "labelLjpOrder";
            this.labelLjpOrder.Size = new System.Drawing.Size(36, 13);
            this.labelLjpOrder.TabIndex = 3;
            this.labelLjpOrder.Text = "Order:";
            // 
            // numericUpDownLjpPredictor
            // 
            this.numericUpDownLjpPredictor.Location = new System.Drawing.Point(179, 132);
            this.numericUpDownLjpPredictor.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numericUpDownLjpPredictor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpPredictor.Name = "numericUpDownLjpPredictor";
            this.numericUpDownLjpPredictor.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownLjpPredictor.TabIndex = 2;
            this.numericUpDownLjpPredictor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpPredictor.ValueChanged += new System.EventHandler(this.numericUpDownLjpPredictor_ValueChanged);
            // 
            // numericUpDownLjpOrder
            // 
            this.numericUpDownLjpOrder.Location = new System.Drawing.Point(179, 102);
            this.numericUpDownLjpOrder.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownLjpOrder.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpOrder.Name = "numericUpDownLjpOrder";
            this.numericUpDownLjpOrder.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownLjpOrder.TabIndex = 1;
            this.numericUpDownLjpOrder.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownLjpOrder.ValueChanged += new System.EventHandler(this.numericUpDownLjpOrder_ValueChanged);
            // 
            // tabPageJls
            // 
            this.tabPageJls.BackColor = System.Drawing.Color.White;
            this.tabPageJls.Controls.Add(this.groupBoxJlsMax);
            this.tabPageJls.Controls.Add(this.labelJlsPoint);
            this.tabPageJls.Controls.Add(this.labelJlsNear);
            this.tabPageJls.Controls.Add(this.numericUpDownJlsPoint);
            this.tabPageJls.Controls.Add(this.numericUpDownJlsNear);
            this.tabPageJls.Location = new System.Drawing.Point(4, 22);
            this.tabPageJls.Name = "tabPageJls";
            this.tabPageJls.Size = new System.Drawing.Size(344, 249);
            this.tabPageJls.TabIndex = 7;
            this.tabPageJls.Text = "JLS";
            // 
            // groupBoxJlsMax
            // 
            this.groupBoxJlsMax.Controls.Add(this.radioButtonJlsMax255);
            this.groupBoxJlsMax.Controls.Add(this.radioButtonJlsMax0);
            this.groupBoxJlsMax.Location = new System.Drawing.Point(37, 98);
            this.groupBoxJlsMax.Name = "groupBoxJlsMax";
            this.groupBoxJlsMax.Size = new System.Drawing.Size(109, 64);
            this.groupBoxJlsMax.TabIndex = 21;
            this.groupBoxJlsMax.TabStop = false;
            this.groupBoxJlsMax.Text = "Max Value of Near";
            // 
            // radioButtonJlsMax255
            // 
            this.radioButtonJlsMax255.AutoSize = true;
            this.radioButtonJlsMax255.Location = new System.Drawing.Point(15, 40);
            this.radioButtonJlsMax255.Name = "radioButtonJlsMax255";
            this.radioButtonJlsMax255.Size = new System.Drawing.Size(43, 17);
            this.radioButtonJlsMax255.TabIndex = 19;
            this.radioButtonJlsMax255.TabStop = true;
            this.radioButtonJlsMax255.Text = "255";
            this.radioButtonJlsMax255.UseVisualStyleBackColor = true;
            this.radioButtonJlsMax255.CheckedChanged += new System.EventHandler(this.radioButtonJlsMax255_CheckedChanged);
            // 
            // radioButtonJlsMax0
            // 
            this.radioButtonJlsMax0.AutoSize = true;
            this.radioButtonJlsMax0.Location = new System.Drawing.Point(15, 19);
            this.radioButtonJlsMax0.Name = "radioButtonJlsMax0";
            this.radioButtonJlsMax0.Size = new System.Drawing.Size(75, 17);
            this.radioButtonJlsMax0.TabIndex = 18;
            this.radioButtonJlsMax0.TabStop = true;
            this.radioButtonJlsMax0.Text = "0 - Optimal";
            this.radioButtonJlsMax0.UseVisualStyleBackColor = true;
            this.radioButtonJlsMax0.CheckedChanged += new System.EventHandler(this.radioButtonJlsMax0_CheckedChanged);
            // 
            // labelJlsPoint
            // 
            this.labelJlsPoint.AutoSize = true;
            this.labelJlsPoint.Location = new System.Drawing.Point(34, 53);
            this.labelJlsPoint.Name = "labelJlsPoint";
            this.labelJlsPoint.Size = new System.Drawing.Size(92, 13);
            this.labelJlsPoint.TabIndex = 15;
            this.labelJlsPoint.Text = "Precision of Point:";
            // 
            // labelJlsNear
            // 
            this.labelJlsNear.AutoSize = true;
            this.labelJlsNear.Location = new System.Drawing.Point(34, 174);
            this.labelJlsNear.Name = "labelJlsNear";
            this.labelJlsNear.Size = new System.Drawing.Size(230, 13);
            this.labelJlsNear.TabIndex = 14;
            this.labelJlsNear.Text = "Error Tolerance for Near-Lossless Compression:";
            // 
            // numericUpDownJlsPoint
            // 
            this.numericUpDownJlsPoint.Location = new System.Drawing.Point(37, 69);
            this.numericUpDownJlsPoint.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownJlsPoint.Name = "numericUpDownJlsPoint";
            this.numericUpDownJlsPoint.Size = new System.Drawing.Size(89, 20);
            this.numericUpDownJlsPoint.TabIndex = 11;
            this.numericUpDownJlsPoint.ValueChanged += new System.EventHandler(this.numericUpDownJlsPoint_ValueChanged);
            // 
            // numericUpDownJlsNear
            // 
            this.numericUpDownJlsNear.Location = new System.Drawing.Point(37, 190);
            this.numericUpDownJlsNear.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownJlsNear.Name = "numericUpDownJlsNear";
            this.numericUpDownJlsNear.Size = new System.Drawing.Size(89, 20);
            this.numericUpDownJlsNear.TabIndex = 10;
            this.numericUpDownJlsNear.ValueChanged += new System.EventHandler(this.numericUpDownJlsNear_ValueChanged);
            // 
            // tabPageWsq
            // 
            this.tabPageWsq.BackColor = System.Drawing.Color.White;
            this.tabPageWsq.Controls.Add(this.labelWsqQuantization);
            this.tabPageWsq.Controls.Add(this.numericUpDownWsqQuantization);
            this.tabPageWsq.Controls.Add(this.labelWsqWhiteValue);
            this.tabPageWsq.Controls.Add(this.labelWsqBlackValue);
            this.tabPageWsq.Controls.Add(this.labelWsqWhite);
            this.tabPageWsq.Controls.Add(this.labelWsqBlack);
            this.tabPageWsq.Controls.Add(this.hScrollBarWsqWhite);
            this.tabPageWsq.Controls.Add(this.hScrollBarWsqBlack);
            this.tabPageWsq.Location = new System.Drawing.Point(4, 22);
            this.tabPageWsq.Name = "tabPageWsq";
            this.tabPageWsq.Size = new System.Drawing.Size(344, 249);
            this.tabPageWsq.TabIndex = 9;
            this.tabPageWsq.Text = "WSQ";
            // 
            // labelWsqQuantization
            // 
            this.labelWsqQuantization.AutoSize = true;
            this.labelWsqQuantization.Location = new System.Drawing.Point(15, 167);
            this.labelWsqQuantization.Name = "labelWsqQuantization";
            this.labelWsqQuantization.Size = new System.Drawing.Size(69, 13);
            this.labelWsqQuantization.TabIndex = 14;
            this.labelWsqQuantization.Text = "Quantization:";
            // 
            // numericUpDownWsqQuantization
            // 
            this.numericUpDownWsqQuantization.DecimalPlaces = 1;
            this.numericUpDownWsqQuantization.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownWsqQuantization.Location = new System.Drawing.Point(113, 165);
            this.numericUpDownWsqQuantization.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownWsqQuantization.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownWsqQuantization.Name = "numericUpDownWsqQuantization";
            this.numericUpDownWsqQuantization.Size = new System.Drawing.Size(66, 20);
            this.numericUpDownWsqQuantization.TabIndex = 13;
            this.numericUpDownWsqQuantization.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.numericUpDownWsqQuantization.ValueChanged += new System.EventHandler(this.numericUpDownWsqQuantization_ValueChanged);
            // 
            // labelWsqWhiteValue
            // 
            this.labelWsqWhiteValue.AutoSize = true;
            this.labelWsqWhiteValue.Location = new System.Drawing.Point(294, 122);
            this.labelWsqWhiteValue.Name = "labelWsqWhiteValue";
            this.labelWsqWhiteValue.Size = new System.Drawing.Size(32, 13);
            this.labelWsqWhiteValue.TabIndex = 12;
            this.labelWsqWhiteValue.Text = "white";
            // 
            // labelWsqBlackValue
            // 
            this.labelWsqBlackValue.AutoSize = true;
            this.labelWsqBlackValue.Location = new System.Drawing.Point(294, 87);
            this.labelWsqBlackValue.Name = "labelWsqBlackValue";
            this.labelWsqBlackValue.Size = new System.Drawing.Size(33, 13);
            this.labelWsqBlackValue.TabIndex = 11;
            this.labelWsqBlackValue.Text = "black";
            // 
            // labelWsqWhite
            // 
            this.labelWsqWhite.AutoSize = true;
            this.labelWsqWhite.Location = new System.Drawing.Point(14, 119);
            this.labelWsqWhite.Name = "labelWsqWhite";
            this.labelWsqWhite.Size = new System.Drawing.Size(38, 13);
            this.labelWsqWhite.TabIndex = 10;
            this.labelWsqWhite.Text = "White:";
            // 
            // labelWsqBlack
            // 
            this.labelWsqBlack.AutoSize = true;
            this.labelWsqBlack.Location = new System.Drawing.Point(15, 85);
            this.labelWsqBlack.Name = "labelWsqBlack";
            this.labelWsqBlack.Size = new System.Drawing.Size(37, 13);
            this.labelWsqBlack.TabIndex = 9;
            this.labelWsqBlack.Text = "Black:";
            // 
            // hScrollBarWsqWhite
            // 
            this.hScrollBarWsqWhite.LargeChange = 1;
            this.hScrollBarWsqWhite.Location = new System.Drawing.Point(89, 119);
            this.hScrollBarWsqWhite.Maximum = 255;
            this.hScrollBarWsqWhite.Name = "hScrollBarWsqWhite";
            this.hScrollBarWsqWhite.Size = new System.Drawing.Size(192, 19);
            this.hScrollBarWsqWhite.TabIndex = 8;
            this.hScrollBarWsqWhite.ValueChanged += new System.EventHandler(this.hScrollBarWsqWhite_ValueChanged);
            // 
            // hScrollBarWsqBlack
            // 
            this.hScrollBarWsqBlack.LargeChange = 1;
            this.hScrollBarWsqBlack.Location = new System.Drawing.Point(88, 85);
            this.hScrollBarWsqBlack.Maximum = 255;
            this.hScrollBarWsqBlack.Name = "hScrollBarWsqBlack";
            this.hScrollBarWsqBlack.Size = new System.Drawing.Size(193, 17);
            this.hScrollBarWsqBlack.TabIndex = 7;
            this.hScrollBarWsqBlack.ValueChanged += new System.EventHandler(this.hScrollBarWsqBlack_ValueChanged);
            // 
            // groupBoxCompressedImage
            // 
            this.groupBoxCompressedImage.BackColor = System.Drawing.Color.White;
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage4);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage3);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage2);
            this.groupBoxCompressedImage.Controls.Add(this.radioButtonCompressedImage1);
            this.groupBoxCompressedImage.Location = new System.Drawing.Point(18, 23);
            this.groupBoxCompressedImage.Name = "groupBoxCompressedImage";
            this.groupBoxCompressedImage.Size = new System.Drawing.Size(136, 40);
            this.groupBoxCompressedImage.TabIndex = 6;
            this.groupBoxCompressedImage.TabStop = false;
            this.groupBoxCompressedImage.Text = "Compressed Image";
            // 
            // radioButtonCompressedImage4
            // 
            this.radioButtonCompressedImage4.AutoSize = true;
            this.radioButtonCompressedImage4.Location = new System.Drawing.Point(99, 17);
            this.radioButtonCompressedImage4.Name = "radioButtonCompressedImage4";
            this.radioButtonCompressedImage4.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage4.TabIndex = 3;
            this.radioButtonCompressedImage4.TabStop = true;
            this.radioButtonCompressedImage4.Text = "4";
            this.radioButtonCompressedImage4.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage4.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage4_CheckedChanged);
            // 
            // radioButtonCompressedImage3
            // 
            this.radioButtonCompressedImage3.AutoSize = true;
            this.radioButtonCompressedImage3.Location = new System.Drawing.Point(68, 17);
            this.radioButtonCompressedImage3.Name = "radioButtonCompressedImage3";
            this.radioButtonCompressedImage3.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage3.TabIndex = 2;
            this.radioButtonCompressedImage3.TabStop = true;
            this.radioButtonCompressedImage3.Text = "3";
            this.radioButtonCompressedImage3.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage3.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage3_CheckedChanged);
            // 
            // radioButtonCompressedImage2
            // 
            this.radioButtonCompressedImage2.AutoSize = true;
            this.radioButtonCompressedImage2.Location = new System.Drawing.Point(36, 17);
            this.radioButtonCompressedImage2.Name = "radioButtonCompressedImage2";
            this.radioButtonCompressedImage2.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage2.TabIndex = 1;
            this.radioButtonCompressedImage2.TabStop = true;
            this.radioButtonCompressedImage2.Text = "2";
            this.radioButtonCompressedImage2.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage2.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage2_CheckedChanged);
            // 
            // radioButtonCompressedImage1
            // 
            this.radioButtonCompressedImage1.AutoSize = true;
            this.radioButtonCompressedImage1.Location = new System.Drawing.Point(6, 17);
            this.radioButtonCompressedImage1.Name = "radioButtonCompressedImage1";
            this.radioButtonCompressedImage1.Size = new System.Drawing.Size(31, 17);
            this.radioButtonCompressedImage1.TabIndex = 0;
            this.radioButtonCompressedImage1.TabStop = true;
            this.radioButtonCompressedImage1.Text = "1";
            this.radioButtonCompressedImage1.UseVisualStyleBackColor = true;
            this.radioButtonCompressedImage1.CheckedChanged += new System.EventHandler(this.radioButtonCompressedImage1_CheckedChanged);
            // 
            // panelBackgroundFrameGrayscale
            // 
            this.panelBackgroundFrameGrayscale.BackColor = System.Drawing.Color.Transparent;
            this.panelBackgroundFrameGrayscale.Controls.Add(this.groupBoxCompressedImage);
            this.panelBackgroundFrameGrayscale.Controls.Add(this.tabControlSettings);
            this.panelBackgroundFrameGrayscale.Controls.Add(this.groupBoxImageType);
            this.panelBackgroundFrameGrayscale.Controls.Add(this.groupBoxSettings);
            this.panelBackgroundFrameGrayscale.Location = new System.Drawing.Point(12, 12);
            this.panelBackgroundFrameGrayscale.Name = "panelBackgroundFrameGrayscale";
            this.panelBackgroundFrameGrayscale.Size = new System.Drawing.Size(525, 322);
            this.panelBackgroundFrameGrayscale.TabIndex = 7;
            // 
            // SettingsFormGrayscale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ImagXpressCompressionDemo.Properties.Resources.ap_dark_stripes;
            this.ClientSize = new System.Drawing.Size(549, 375);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.panelBackgroundFrameGrayscale);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SettingsFormGrayscale";
            this.Text = "Grayscale Compression Settings";
            this.Load += new System.EventHandler(this.SettingsFormGrayscale_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SettingsFormGrayscale_FormClosed);
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.groupBoxImageType.ResumeLayout(false);
            this.groupBoxImageType.PerformLayout();
            this.tabControlSettings.ResumeLayout(false);
            this.tabPageJpeg.ResumeLayout(false);
            this.tabPageJpeg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExifThumbnailSize)).EndInit();
            this.groupBoxJpegWrap.ResumeLayout(false);
            this.groupBoxJpegWrap.PerformLayout();
            this.groupBoxSubSampling.ResumeLayout(false);
            this.groupBoxSubSampling.PerformLayout();
            this.tabPageJpeg2000.ResumeLayout(false);
            this.tabPageJpeg2000.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2Peak)).EndInit();
            this.groupBoxJp2TileSize.ResumeLayout(false);
            this.groupBoxJp2TileSize.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2TileWidth)).EndInit();
            this.groupBoxJp2Type.ResumeLayout(false);
            this.groupBoxJp2Type.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJp2CompressSize)).EndInit();
            this.groupBoxJp2Order.ResumeLayout(false);
            this.groupBoxJp2Order.PerformLayout();
            this.tabPageHdp.ResumeLayout(false);
            this.tabPageHdp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHdpQuantization)).EndInit();
            this.groupBoxHdpOrder.ResumeLayout(false);
            this.groupBoxHdpOrder.PerformLayout();
            this.groupBoxChromaSubSampling.ResumeLayout(false);
            this.groupBoxChromaSubSampling.PerformLayout();
            this.tabPageLjp.ResumeLayout(false);
            this.tabPageLjp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpPredictor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLjpOrder)).EndInit();
            this.tabPageJls.ResumeLayout(false);
            this.tabPageJls.PerformLayout();
            this.groupBoxJlsMax.ResumeLayout(false);
            this.groupBoxJlsMax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJlsNear)).EndInit();
            this.tabPageWsq.ResumeLayout(false);
            this.tabPageWsq.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWsqQuantization)).EndInit();
            this.groupBoxCompressedImage.ResumeLayout(false);
            this.groupBoxCompressedImage.PerformLayout();
            this.panelBackgroundFrameGrayscale.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.CheckBox checkBoxPreview;
        private System.Windows.Forms.Button buttonRestoreDefaults;
        private System.Windows.Forms.GroupBox groupBoxImageType;
        private System.Windows.Forms.RadioButton radioButtonWsq;
        private System.Windows.Forms.RadioButton radioButtonJls;
        private System.Windows.Forms.RadioButton radioButtonLjp;
        private System.Windows.Forms.RadioButton radioButtonHdp;
        private System.Windows.Forms.RadioButton radioButtonJpeg2000;
        private System.Windows.Forms.RadioButton radioButtonJpeg;
        private System.Windows.Forms.TabControl tabControlSettings;
        private System.Windows.Forms.TabPage tabPageJpeg;
        private System.Windows.Forms.NumericUpDown numericUpDownExifThumbnailSize;
        private System.Windows.Forms.Label labelExifThumbnailSize;
        private System.Windows.Forms.GroupBox groupBoxJpegWrap;
        private System.Windows.Forms.RadioButton radioButtonJpegWrapInExif;
        private System.Windows.Forms.RadioButton radioButtonJpegNoWrap;
        private System.Windows.Forms.RadioButton radioButtonJpegWrapInPdf;
        private System.Windows.Forms.RadioButton radioButtonJpegWrapInTiff;
        private System.Windows.Forms.CheckBox checkBoxJpegProgressive;
        private System.Windows.Forms.GroupBox groupBoxSubSampling;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling411;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling211v;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling211;
        private System.Windows.Forms.RadioButton radioButtonJpegSubSampling111;
        private System.Windows.Forms.CheckBox checkBoxJpegCosited;
        private System.Windows.Forms.Label labelChrominanceValue;
        private System.Windows.Forms.Label labelLuminanceValue;
        private System.Windows.Forms.Label labelChrominance;
        private System.Windows.Forms.Label labelLuminance;
        private System.Windows.Forms.HScrollBar hScrollBarChrominance;
        private System.Windows.Forms.HScrollBar hScrollBarLuminance;
        private System.Windows.Forms.TabPage tabPageJpeg2000;
        private System.Windows.Forms.Label labelJp2Peak;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2Peak;
        private System.Windows.Forms.GroupBox groupBoxJp2TileSize;
        private System.Windows.Forms.Label labelJp2TileHeight;
        private System.Windows.Forms.Label labelJp2TileWidth;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2TileHeight;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2TileWidth;
        private System.Windows.Forms.GroupBox groupBoxJp2Type;
        private System.Windows.Forms.RadioButton radioButtonJp2Lossy;
        private System.Windows.Forms.RadioButton radioButtonJp2Lossless;
        private System.Windows.Forms.NumericUpDown numericUpDownJp2CompressSize;
        private System.Windows.Forms.Label labelJp2CompressSize;
        private System.Windows.Forms.GroupBox groupBoxJp2Order;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderRpcl;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderRlcp;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderPcrl;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderLrcp;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderCprl;
        private System.Windows.Forms.RadioButton radioButtonJp2OrderDefault;
        private System.Windows.Forms.TabPage tabPageHdp;
        private System.Windows.Forms.NumericUpDown numericUpDownHdpQuantization;
        private System.Windows.Forms.Label labelHdpQuantization;
        private System.Windows.Forms.GroupBox groupBoxHdpOrder;
        private System.Windows.Forms.RadioButton radioButtonHdpOrderFrequency;
        private System.Windows.Forms.RadioButton radioButtonHdpOrderSpacial;
        private System.Windows.Forms.GroupBox groupBoxChromaSubSampling;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling444;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling422;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling420;
        private System.Windows.Forms.RadioButton radioButtonHdpChromaSubSampling400;
        private System.Windows.Forms.TabPage tabPageLjp;
        private System.Windows.Forms.Label labelLjpPredictor;
        private System.Windows.Forms.Label labelLjpOrder;
        private System.Windows.Forms.NumericUpDown numericUpDownLjpPredictor;
        private System.Windows.Forms.NumericUpDown numericUpDownLjpOrder;
        private System.Windows.Forms.TabPage tabPageJls;
        private System.Windows.Forms.GroupBox groupBoxJlsMax;
        private System.Windows.Forms.RadioButton radioButtonJlsMax255;
        private System.Windows.Forms.RadioButton radioButtonJlsMax0;
        private System.Windows.Forms.Label labelJlsPoint;
        private System.Windows.Forms.Label labelJlsNear;
        private System.Windows.Forms.NumericUpDown numericUpDownJlsPoint;
        private System.Windows.Forms.NumericUpDown numericUpDownJlsNear;
        private System.Windows.Forms.TabPage tabPageWsq;
        private System.Windows.Forms.Label labelWsqQuantization;
        private System.Windows.Forms.NumericUpDown numericUpDownWsqQuantization;
        private System.Windows.Forms.Label labelWsqWhiteValue;
        private System.Windows.Forms.Label labelWsqBlackValue;
        private System.Windows.Forms.Label labelWsqWhite;
        private System.Windows.Forms.Label labelWsqBlack;
        private System.Windows.Forms.HScrollBar hScrollBarWsqWhite;
        private System.Windows.Forms.HScrollBar hScrollBarWsqBlack;
        private System.Windows.Forms.GroupBox groupBoxCompressedImage;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage4;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage3;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage2;
        private System.Windows.Forms.RadioButton radioButtonCompressedImage1;
        private AccusoftCustom.PanelBackgroundFrame panelBackgroundFrameGrayscale;
	}
}
﻿/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ImagXpressCompressionDemo")]
[assembly: AssemblyDescription("C# ImagXpress Compression Demo")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Accusoft Pegasus")]
[assembly: AssemblyProduct("ImagXpress 10")]
[assembly: AssemblyCopyright("Copyright © 2008-2009 Pegasus Imaging Corporation")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5a874373-fc98-40b1-9d02-68ec6590b814")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("10.0.23.0")]
[assembly: AssemblyFileVersion("10.0.23.0")]

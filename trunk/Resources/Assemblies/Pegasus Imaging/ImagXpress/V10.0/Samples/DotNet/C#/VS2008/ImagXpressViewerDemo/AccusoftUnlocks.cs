﻿using System;
using System.Collections.Generic;
using System.Text;
using Accusoft.ImagXpressSdk;
using Accusoft.ThumbnailXpressSdk;

namespace ImagXpressViewerDemo
{
    /// <summary>
    /// Unlocks the Accusoft component runtimes.
    /// </summary>
    class AccusoftUnlocks
    {
        public static void UnlockRuntimes(ImagXpress ix, ThumbnailXpress tx1, ThumbnailXpress tx2)
        {
            if (tx1 != null)
            {
                //tx1.Licensing.UnlockRuntime(xxxx, xxxx, xxxx, xxxx);
                //tx1.Licensing.UnlockIXRuntime(xxxx, xxxx, xxxx, xxxx);
            }

            if (tx2 != null)
            {
                //tx2.Licensing.UnlockRuntime(xxxx, xxxx, xxxx, xxxx);
                //tx2.Licensing.UnlockIXRuntime(xxxx, xxxx, xxxx, xxxx);
            }

            if (ix != null)
            {
                //ix.Licensing.UnlockRuntime(xxxx,xxxx,xxxx,xxxx);
            }
        }
    }
}

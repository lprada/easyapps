using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accusoft.ImagXpressSdk;
using System.Runtime.InteropServices;
using System.Xml;

namespace ImagXpressCompressionDemo
{
    public partial class MainForm : Form
    {
        private System.String strCurrentImage;
        bool isGrayscale = false;

        CompressedImage compressedImage1;
        CompressedImage compressedImage2;
        CompressedImage compressedImage3;
        CompressedImage compressedImage4;

        ContextMenu cm1;
        ContextMenu cm2;
        ContextMenu cm3;
        ContextMenu cm4;

        SettingsFormColor settingsFormColor;
        SettingsFormBW settingsFormBW;
        SettingsFormGrayscale settingsFormGrayscale;

        private System.String strSettingsFile;
        System.String strDefaultImageFilter = "All Files (*.*)|*.*|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal" +
                                                  "|Windows Device Independent Bitmap(*.DIB)|*.dib" +
                                                  "|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx" +
                                                  "|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls" +
                                                  "|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp" +
                                                  "|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx" +
                                                  "|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic" +
                                                  "|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm" +
                                                  "|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga" +
                                                  "|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2" +
                                                  "|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf" +
                                                  "|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp";

        public MainForm()
        {
            // Call the Accusoft Pegasus Unlocking Code here
            InitializeComponent();
            try
            {
                AccusoftUnlocks.UnlockRuntimes(imagXpress1);
            }
            catch
            {
                MessageBox.Show("Invalid Unlock Codes, See the UnlockRuntime calls");
            }
        }

        #region Form Load and Close

        private void MainForm_Load(object sender, EventArgs e)
        {
            originalImageXView.AutoImageDispose = true;
            compressed1ImageXView.AutoImageDispose = true;
            compressed2ImageXView.AutoImageDispose = true;
            compressed3ImageXView.AutoImageDispose = true;
            compressed4ImageXView.AutoImageDispose = true;

            strSettingsFile = System.IO.Path.GetTempPath() + "ImagXpressCompressionDemoSettings.xml";

            // Set up the compressed image context menus.
            compressed1ImageXView.ContextMenu = null;
            compressed2ImageXView.ContextMenu = null;
            compressed3ImageXView.ContextMenu = null;
            compressed4ImageXView.ContextMenu = null;
            cm1 = new ContextMenu();
            MenuItem menu1Item = new MenuItem("Settings", new EventHandler(buttonSettings1_Click));
            cm1.MenuItems.Add(menu1Item);
            compressed1ImageXView.ContextMenu = cm1;
            cm2 = new ContextMenu();
            MenuItem menu2Item = new MenuItem("Settings", new EventHandler(buttonSettings2_Click));
            cm2.MenuItems.Add(menu2Item);
            compressed2ImageXView.ContextMenu = cm2;
            cm3 = new ContextMenu();
            MenuItem menu3Item = new MenuItem("Settings", new EventHandler(buttonSettings3_Click));
            cm3.MenuItems.Add(menu3Item);
            compressed3ImageXView.ContextMenu = cm3;
            cm4 = new ContextMenu();
            MenuItem menu4Item = new MenuItem("Settings", new EventHandler(buttonSettings4_Click));
            cm4.MenuItems.Add(menu4Item);
            compressed4ImageXView.ContextMenu = cm4;

            originalImageXView.AllowDrop = true;

            // Instantiate the compressed image objects.
            compressedImage1 = new CompressedImage(1, compressed1ImageXView, tabPage1, labelCompressed1FilesizeData, labelCompressed1CompressionRatioData, labelCompressed1WidthData, labelCompressed1HeightData, labelCompressed1BitDepthData, labelCompressed1, labelCompressed1Kbps56Data, labelCompressed1Kbps384Data, labelCompressed1T1Data, this);
            compressedImage2 = new CompressedImage(2, compressed2ImageXView, tabPage2, labelCompressed2FilesizeData, labelCompressed2CompressionRatioData, labelCompressed2WidthData, labelCompressed2HeightData, labelCompressed2BitDepthData, labelCompressed2, labelCompressed2Kbps56Data, labelCompressed2Kbps384Data, labelCompressed2T1Data, this);
            compressedImage3 = new CompressedImage(3, compressed3ImageXView, tabPage3, labelCompressed3FilesizeData, labelCompressed3CompressionRatioData, labelCompressed3WidthData, labelCompressed3HeightData, labelCompressed3BitDepthData, labelCompressed3, labelCompressed3Kbps56Data, labelCompressed3Kbps384Data, labelCompressed3T1Data, this);
            compressedImage4 = new CompressedImage(4, compressed4ImageXView, tabPage4, labelCompressed4FilesizeData, labelCompressed4CompressionRatioData, labelCompressed4WidthData, labelCompressed4HeightData, labelCompressed4BitDepthData, labelCompressed4, labelCompressed4Kbps56Data, labelCompressed4Kbps384Data, labelCompressed4T1Data, this);

            // Load compression settings from XML.
            if (System.IO.File.Exists(strSettingsFile))
                ReadCompressionSettings();
            else // Use default compression settings if no XML settings file is available.
                SetDefaultCompressionSettings();

            // Add progress event.
            imagXpress1.ProgressEvent += new ImagXpress.ProgressEventHandler(imagXpress1_ProgressEvent);

            // Select hand tool.
            originalImageXView.ToolSet(Tool.Hand, MouseButtons.Left, Keys.None);

            // Load an image.
            String strCurrentDir = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\Common\Images\");
            if(!System.IO.Directory.Exists(strCurrentDir))
                strCurrentDir = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images\");
            if (System.IO.Directory.Exists(strCurrentDir))
                System.IO.Directory.SetCurrentDirectory(strCurrentDir);
            strCurrentDir = System.IO.Directory.GetCurrentDirectory();
            strCurrentImage = System.IO.Path.Combine(strCurrentDir, "noalphachannel.tif");
            if(System.IO.File.Exists(strCurrentImage))
                LoadFile();

            if (Screen.PrimaryScreen.Bounds.Width < 1024 || Screen.PrimaryScreen.Bounds.Height < 768)
            {
                MessageBox.Show("The demo requires a minimum screen resolution of 1024x768.");
                Close();
            }

            this.MinimumSize = new Size(1024, 768);
            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width * 3 / 4, Screen.PrimaryScreen.Bounds.Height * 7 / 8);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            WriteCompressionSettings(strSettingsFile);
        }

        #endregion

        #region Status

        void imagXpress1_ProgressEvent(object sender, ProgressEventArgs e)
        {
            toolStripProgressBar1.Value = e.PercentDone;

            // When changes are made to the compression settings using the settings form,
            // an image is loaded using the new compression settings.
            // When the new compressed image has completely loaded, it is necessary to set the zoom factor
            // and scroll position.  The image being loaded will currently have AllowUpdate set to false,
            // so check AllowUpdate to determine which image to scroll.
            if (e.Operation == Operation.LoadImage && e.IsComplete == true)
            {
                DisplayStatusText("File: " +System.IO.Path.GetFileName(strCurrentImage));
                if (compressed1ImageXView.AllowUpdate == false)
                {
                    compressed1ImageXView.ZoomFactor = originalImageXView.ZoomFactor * compressedImage1.GetCompressedToActualRatio();
                    compressed1ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage1.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage1.GetCompressedToActualRatio()));
                }
                else if (compressed2ImageXView.AllowUpdate == false)
                {
                    compressed2ImageXView.ZoomFactor = originalImageXView.ZoomFactor * compressedImage2.GetCompressedToActualRatio();
                    compressed2ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage2.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage2.GetCompressedToActualRatio()));
                }
                else if (compressed3ImageXView.AllowUpdate == false)
                {
                    compressed3ImageXView.ZoomFactor = originalImageXView.ZoomFactor * compressedImage3.GetCompressedToActualRatio();
                    compressed3ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage3.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage3.GetCompressedToActualRatio()));
                }
                else if (compressed4ImageXView.AllowUpdate == false)
                {
                    compressed4ImageXView.ZoomFactor = originalImageXView.ZoomFactor * compressedImage4.GetCompressedToActualRatio();
                    compressed4ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage4.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage4.GetCompressedToActualRatio()));
                }
            }
        }

        public void DisplayStatusText(System.String statusText)
        {
            toolStripStatusLabel1.Text = statusText.Trim();
        }

        #endregion

        #region ImagXpress

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        public ImagXpress GetImagXpress()
        {
            return imagXpress1;
        }

        private void panelDownload_Click(object sender, EventArgs e)
        {
            // Navigate to the URL
            System.Diagnostics.Process.Start("http://www.accusoft.com/imagxpress.htm");
        }

        #endregion

        #region Open Images

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                strCurrentImage = PegasusOpenFile();
                if(strCurrentImage.Length > 0)
                    LoadFile();
            }
            catch (ImagXpressException ex)
            {
                DisplayStatusText(ex.Message);
            }
        }

        private System.String PegasusOpenFile()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Select an Image File";
            dlg.Filter = strDefaultImageFilter;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        private void LoadFile()
        {
            isGrayscale = false;
            ResetCompressedImages();

            // Load the file.  Query and display the file's data.
            if (originalImageXView.Image != null)
                originalImageXView.Image.Dispose();
            originalImageXView.Image = ImageX.FromFile(imagXpress1, strCurrentImage);
            labelFilenameData.Text = System.IO.Path.GetFileName(strCurrentImage);
            labelFilesizeData.Text = originalImageXView.Image.ImageXData.Size.ToString() + " bytes";
            labelCompressionRatioData.Text = ((originalImageXView.Image.ImageXData.Width * originalImageXView.Image.ImageXData.Height * originalImageXView.Image.ImageXData.BitsPerPixel) / originalImageXView.Image.ImageXData.Size).ToString() + " : 1";
            labelWidthData.Text = originalImageXView.Image.ImageXData.Width.ToString();
            labelHeightData.Text = originalImageXView.Image.ImageXData.Height.ToString();
            labelBitDepthData.Text = originalImageXView.Image.ImageXData.BitsPerPixel.ToString();
            labelKbps56Data.Text = ((float)((float)originalImageXView.Image.ImageXData.Size / 57344)).ToString("F3") + " secs";
            labelKbps384Data.Text = ((float)((float)originalImageXView.Image.ImageXData.Size / 393216)).ToString("F3") + " secs";
            labelT1Data.Text = ((float)((float)originalImageXView.Image.ImageXData.Size / 1572864)).ToString("F3") + " secs";

            if ((float)originalImageXView.Image.ImageXData.Width / (float)originalImageXView.Image.ImageXData.Height < (float)originalImageXView.Width / (float)originalImageXView.Height) // .769, .857
                originalImageXView.AutoResize = AutoResizeType.FitWidth;
            else
                originalImageXView.AutoResize = AutoResizeType.FitHeight;

            // Determine if the image is grayscale.
            if (originalImageXView.Image.ImageXData.BitsPerPixel == 8 && originalImageXView.Image.Palette != null)
            {
                isGrayscale = true;
                int i = 0;
                for (i = 0; i < 256; i++)
                {
                    if (originalImageXView.Image.Palette.Entries[i].B != originalImageXView.Image.Palette.Entries[i].G || originalImageXView.Image.Palette.Entries[i].B != originalImageXView.Image.Palette.Entries[i].R)
                    {
                        isGrayscale = false;
                        break;
                    }
                }
            }
            CompressImages();
        }

        private void originalImageXView_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))  // extra safety
            {
                string[] files;

                try
                {
                    files = (string[])e.Data.GetData(System.Windows.Forms.DataFormats.FileDrop);
                    strCurrentImage = files[0];
                    LoadFile();
                }
                catch (ImagXpressException ex)
                {
                    DisplayStatusText("Error: " + ex.Message);
                }
                catch
                {
                    DisplayStatusText("Error in ImagXpress drag drop");
                }
            }
        }

        private void originalImageXView_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        #endregion

        #region Modify Compression Settings

        private void EnableContextMenu(bool enabled)
        {
            if (enabled == true)
            {
                compressed1ImageXView.ContextMenu = cm1;
                compressed2ImageXView.ContextMenu = cm2;
                compressed3ImageXView.ContextMenu = cm3;
                compressed4ImageXView.ContextMenu = cm4;
            }
            else
            {
                compressed1ImageXView.ContextMenu = null;
                compressed2ImageXView.ContextMenu = null;
                compressed3ImageXView.ContextMenu = null;
                compressed4ImageXView.ContextMenu = null;
            }
        }

        public void EnableSettings(bool enabled)
        {
            buttonOriginalSettings1.Enabled = enabled;
            buttonSettings1.Enabled = enabled;
            buttonSettings2.Enabled = enabled;
            buttonSettings3.Enabled = enabled;
            buttonSettings4.Enabled = enabled;
            EnableContextMenu(enabled);
        }

        private void buttonSettings1_Click(object sender, EventArgs e)
        {
            ShowCompressionSettingsDialog(compressedImage1, compressed1ImageXView);
        }

        private void buttonSettings2_Click(object sender, EventArgs e)
        {
            ShowCompressionSettingsDialog(compressedImage2, compressed2ImageXView);
        }

        private void buttonSettings3_Click(object sender, EventArgs e)
        {
            ShowCompressionSettingsDialog(compressedImage3, compressed3ImageXView);
        }

        private void buttonSettings4_Click(object sender, EventArgs e)
        {
            ShowCompressionSettingsDialog(compressedImage4, compressed4ImageXView);
        }

        private void ShowCompressionSettingsDialog(CompressedImage compressedImage, ImageXView compressedImageXView)
        {
            if (originalImageXView.Image != null)
            {
                int xPosition = this.Location.X + panelBackgroundFrame.Location.X + splitContainerImages.Location.X + splitContainerImages.Panel2.Left + panelGradientNoFlareCompressed.Location.X + panelCompressedImage.Location.X + compressedImageXView.Location.X + compressedImageXView.Width + 7;
                int yPosition = this.Location.Y + panelBackgroundFrame.Location.Y + splitContainerImages.Location.Y + splitContainerImages.Panel2.Top + panelGradientNoFlareCompressed.Location.Y + panelCompressedImage.Location.Y + compressedImageXView.Location.Y + 5;

                // Show the Grayscale dialog if the image is grayscale.
                if (isGrayscale == true)
                {
                    settingsFormGrayscale = new SettingsFormGrayscale(this, compressedImage1, compressedImage2, compressedImage3, compressedImage4);
                    settingsFormGrayscale.StartPosition = FormStartPosition.Manual;
                    settingsFormGrayscale.SetDesktopLocation(xPosition, yPosition);
                    settingsFormGrayscale.Show();
                }
                else if (originalImageXView.Image.ImageXData.BitsPerPixel == 1)
                { // Show the BW dialog if the image is black and white.
                    settingsFormBW = new SettingsFormBW(this, compressedImage1, compressedImage2, compressedImage3, compressedImage4);
                    settingsFormBW.StartPosition = FormStartPosition.Manual;
                    settingsFormBW.SetDesktopLocation(xPosition, yPosition);
                    settingsFormBW.Show();
                }
                else // Show the Color dialog if the image is color.
                {
                    settingsFormColor = new SettingsFormColor(this, compressedImage1, compressedImage2, compressedImage3, compressedImage4);
                    settingsFormColor.StartPosition = FormStartPosition.Manual;
                    settingsFormColor.SetDesktopLocation(xPosition, yPosition);
                    settingsFormColor.Show();
                }
            }
        }

        public void SetDefaultCompressionSettings()
        {
            SaveOptions saveOptionsColor1 = new SaveOptions();
            SaveOptions saveOptionsColor2 = new SaveOptions();
            SaveOptions saveOptionsColor3 = new SaveOptions();
            SaveOptions saveOptionsColor4 = new SaveOptions();
            SaveOptions saveOptionsBW1 = new SaveOptions();
            SaveOptions saveOptionsBW2 = new SaveOptions();
            SaveOptions saveOptionsBW3 = new SaveOptions();
            SaveOptions saveOptionsBW4 = new SaveOptions();
            SaveOptions saveOptionsGrayscale1 = new SaveOptions();
            SaveOptions saveOptionsGrayscale2 = new SaveOptions();
            SaveOptions saveOptionsGrayscale3 = new SaveOptions();
            SaveOptions saveOptionsGrayscale4 = new SaveOptions();

            // Color

            saveOptionsColor1.Format = ImageXFormat.Jpeg;
            saveOptionsColor1.Jpeg.Chrominance = 10;
            saveOptionsColor1.Jpeg.Luminance = 10;
            compressedImage1.UpdateSaveOptionsColor(saveOptionsColor1);

            saveOptionsColor2.Format = ImageXFormat.Jpeg;
            saveOptionsColor2.Jpeg.Chrominance = 250;
            saveOptionsColor2.Jpeg.Luminance = 250;
            compressedImage2.UpdateSaveOptionsColor(saveOptionsColor2);

            saveOptionsColor3.Format = ImageXFormat.Jpeg2000;
            saveOptionsColor3.Jp2.TileSize = new Size(256, 256);
            saveOptionsColor3.Jp2.CompressSize = 32 * 1024 * 1024;
            compressedImage3.UpdateSaveOptionsColor(saveOptionsColor3);

            saveOptionsColor4.Format = ImageXFormat.Png;
            compressedImage4.UpdateSaveOptionsColor(saveOptionsColor4);

            // BW

            saveOptionsBW1.Format = ImageXFormat.Tiff;
            saveOptionsBW1.Tiff.Compression = Compression.Group4;
            compressedImage1.UpdateSaveOptionsBW(saveOptionsBW1);

            saveOptionsBW2.Format = ImageXFormat.Pdf;
            saveOptionsBW2.Pdf.Compression = Compression.Jbig2;
            compressedImage2.UpdateSaveOptionsBW(saveOptionsBW2);

            saveOptionsBW3.Format = ImageXFormat.Jbig2;
            compressedImage3.UpdateSaveOptionsBW(saveOptionsBW3);

            saveOptionsBW4.Format = ImageXFormat.Tiff;
            saveOptionsBW4.Tiff.Compression = Compression.Lzw;
            compressedImage4.UpdateSaveOptionsBW(saveOptionsBW4);

            // Grayscale

            saveOptionsGrayscale1.Format = ImageXFormat.Jpeg;
            saveOptionsGrayscale1.Jpeg.Chrominance = 10;
            saveOptionsGrayscale1.Jpeg.Luminance = 10;
            saveOptionsGrayscale1.Jpeg.Grayscale = true;
            saveOptionsGrayscale1.Jp2.Grayscale = true;
            compressedImage1.UpdateSaveOptionsGrayscale(saveOptionsGrayscale1);

            saveOptionsGrayscale2.Format = ImageXFormat.Jpeg;
            saveOptionsGrayscale2.Jpeg.Chrominance = 250;
            saveOptionsGrayscale2.Jpeg.Luminance = 250;
            saveOptionsGrayscale2.Jpeg.Grayscale = true;
            saveOptionsGrayscale2.Jp2.Grayscale = true;
            compressedImage2.UpdateSaveOptionsGrayscale(saveOptionsGrayscale2);

            saveOptionsGrayscale3.Format = ImageXFormat.Jpeg2000;
            saveOptionsGrayscale3.Jp2.TileSize = new Size(256, 256);
            saveOptionsGrayscale3.Jp2.CompressSize = 32 * 1024 * 1024;
            saveOptionsGrayscale3.Jp2.Grayscale = true;
            saveOptionsGrayscale3.Jpeg.Grayscale = true;
            compressedImage3.UpdateSaveOptionsGrayscale(saveOptionsGrayscale3);

            saveOptionsGrayscale4.Format = ImageXFormat.Wsq;
            saveOptionsGrayscale4.Jpeg.Grayscale = true;
            saveOptionsGrayscale4.Jp2.Grayscale = true;
            compressedImage4.UpdateSaveOptionsGrayscale(saveOptionsGrayscale4);
        }

        public CompressedImage GetCompressedImage1()
        {
            return compressedImage1;
        }

        public CompressedImage GetCompressedImage2()
        {
            return compressedImage2;
        }

        public CompressedImage GetCompressedImage3()
        {
            return compressedImage3;
        }

        public CompressedImage GetCompressedImage4()
        {
            return compressedImage4;
        }

        #endregion

        #region Image Selecting, Viewing, and Compressing

        private void SelectCompressedImageOnSettingsForm()
        {
            if (settingsFormColor != null)
                settingsFormColor.SelectCompressedImage();
            else if(settingsFormBW != null)
                settingsFormBW.SelectCompressedImage();
            else if(settingsFormGrayscale != null)
                settingsFormGrayscale.SelectCompressedImage();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelCompressed1.BackColor = Color.White;
            labelCompressed2.BackColor = Color.White;
            labelCompressed3.BackColor = Color.White;
            labelCompressed4.BackColor = Color.White;

            switch (tabControlImageInfo.SelectedIndex)
            {
                case 1:
                    labelCompressed1.BackColor = Color.FromArgb(234, 122, 64);
                    SelectCompressedImageOnSettingsForm();
                    if (ShowingThumbs() == false)
                    {
                        tabControlImageInfo.Enabled = false;
                        if (compressed2ImageXView.Visible == true)
                            compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                        else if (compressed3ImageXView.Visible == true)
                            compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                        else if (compressed4ImageXView.Visible == true)
                            compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                        else
                            break;
                        compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                    }
                    break;
                case 2:
                    labelCompressed2.BackColor = Color.FromArgb(234, 122, 64);
                    SelectCompressedImageOnSettingsForm();
                    if (ShowingThumbs() == false)
                    {
                        tabControlImageInfo.Enabled = false;
                        if (compressed1ImageXView.Visible == true)
                            compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                        else if (compressed3ImageXView.Visible == true)
                            compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                        else if (compressed4ImageXView.Visible == true)
                            compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                        else
                            break;
                        compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                    }
                    break;
                case 3:
                    labelCompressed3.BackColor = Color.FromArgb(234, 122, 64);
                    SelectCompressedImageOnSettingsForm();
                    if (ShowingThumbs() == false)
                    {
                        tabControlImageInfo.Enabled = false;
                        if (compressed1ImageXView.Visible == true)
                            compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                        else if (compressed2ImageXView.Visible == true)
                            compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                        else if (compressed4ImageXView.Visible == true)
                            compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                        else
                            break;
                        compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                    }
                    break;
                case 4:
                    labelCompressed4.BackColor = Color.FromArgb(234, 122, 64);
                    SelectCompressedImageOnSettingsForm();
                    if (ShowingThumbs() == false)
                    {
                        tabControlImageInfo.Enabled = false;
                        if (compressed1ImageXView.Visible == true)
                            compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                        else if (compressed2ImageXView.Visible == true)
                            compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                        else if (compressed3ImageXView.Visible == true)
                            compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                        else
                            break;
                        compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                    }
                    break;
            }
            tabControlImageInfo.Enabled = true;
        }

        private void compressed1ImageXView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
            }
        }

        private void compressed2ImageXView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
            }
        }

        private void compressed3ImageXView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
            }
        }

        private void compressed4ImageXView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
            }
        }

        public void DisplayCompressedImagePanel3DBorder()
        {
            // When the compressed image is shrunk, the compressed image panel
            // should show a 3D border.
            panelCompressedImage.BorderStyle = BorderStyle.Fixed3D;
        }

        public void DisplayCompressedImagePanelNoBorder()
        {
            // When the compressed image is grown, the 3D border should be removed
            // from this panel because the IX control will be grown to fit the panel,
            // and the IX control has its own 3D border.
            panelCompressedImage.BorderStyle = BorderStyle.None;
        }

        private void originalImageXView_ZoomFactorChanged(object sender, ZoomFactorChangedEventArgs e)
        {
            if (BestFitOriginalImage() == false)
            {
                compressed1ImageXView.AutoResize = AutoResizeType.CropImage;
                compressed2ImageXView.AutoResize = AutoResizeType.CropImage;
                compressed3ImageXView.AutoResize = AutoResizeType.CropImage;
                compressed4ImageXView.AutoResize = AutoResizeType.CropImage;

                compressed1ImageXView.ZoomFactor = e.NewValue * compressedImage1.GetCompressedToActualRatio();
                compressed2ImageXView.ZoomFactor = e.NewValue * compressedImage2.GetCompressedToActualRatio();
                compressed3ImageXView.ZoomFactor = e.NewValue * compressedImage3.GetCompressedToActualRatio();
                compressed4ImageXView.ZoomFactor = e.NewValue * compressedImage4.GetCompressedToActualRatio();
            }
        }

        private void originalImageXView_ScrollEvent(object sender, Accusoft.ImagXpressSdk.ScrollEventArgs e)
        {
            compressed1ImageXView.ScrollPosition = new Point((int)(e.NewValue.X * compressedImage1.GetCompressedToActualRatio()), (int)(e.NewValue.Y * compressedImage1.GetCompressedToActualRatio()));
            compressed2ImageXView.ScrollPosition = new Point((int)(e.NewValue.X * compressedImage2.GetCompressedToActualRatio()), (int)(e.NewValue.Y * compressedImage2.GetCompressedToActualRatio()));
            compressed3ImageXView.ScrollPosition = new Point((int)(e.NewValue.X * compressedImage3.GetCompressedToActualRatio()), (int)(e.NewValue.Y * compressedImage3.GetCompressedToActualRatio()));
            compressed4ImageXView.ScrollPosition = new Point((int)(e.NewValue.X * compressedImage4.GetCompressedToActualRatio()), (int)(e.NewValue.Y * compressedImage4.GetCompressedToActualRatio()));

            originalImageXView.Update();
            compressed1ImageXView.Update();
            compressed2ImageXView.Update();
            compressed3ImageXView.Update();
            compressed4ImageXView.Update();
        }

        private void originalImageXView_AutoResizeChanged(object sender, AutoResizeChangedEventArgs e)
        {
            compressed1ImageXView.AutoResize = e.NewValue;
            compressed2ImageXView.AutoResize = e.NewValue;
            compressed3ImageXView.AutoResize = e.NewValue;
            compressed4ImageXView.AutoResize = e.NewValue;

            compressed1ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage1.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage1.GetCompressedToActualRatio()));
            compressed2ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage2.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage2.GetCompressedToActualRatio()));
            compressed3ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage3.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage3.GetCompressedToActualRatio()));
            compressed4ImageXView.ScrollPosition = new Point((int)(originalImageXView.ScrollPosition.X * compressedImage4.GetCompressedToActualRatio()), (int)(originalImageXView.ScrollPosition.Y * compressedImage4.GetCompressedToActualRatio()));
        }

        public bool ShowingThumbs()
        {
            // If more than one of the compressed image controls is visible, then thumbs are being shown.
            if ((compressed1ImageXView.Visible == true) && (compressed2ImageXView.Visible == true))
                return true;
            else
                return false;
        }

        public ImageX GetOriginalImage()
        {
            return originalImageXView.Image;
        }

        public ImageXView GetOriginalImageView()
        {
            return originalImageXView;
        }

        public TabControl GetTabControl()
        {
            return tabControlImageInfo;
        }

        public void CompressImages()
        {
            if (isGrayscale == true) // Grayscale
            {
                compressedImage1.CompressGrayscaleImage();
                compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                compressedImage2.CompressGrayscaleImage();
                compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                compressedImage3.CompressGrayscaleImage();
                compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                compressedImage4.CompressGrayscaleImage();
                compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                UpdateCompressedNames(GetCompressedImage1().GetSaveOptionsGrayscale().Format, GetCompressedImage2().GetSaveOptionsGrayscale().Format, GetCompressedImage3().GetSaveOptionsGrayscale().Format, GetCompressedImage4().GetSaveOptionsGrayscale().Format);
            }
            else if (originalImageXView.Image.ImageXData.BitsPerPixel == 1) // BW
            {
                compressedImage1.CompressBWImage();
                compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                compressedImage2.CompressBWImage();
                compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                compressedImage3.CompressBWImage();
                compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                compressedImage4.CompressBWImage();
                compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                UpdateCompressedNames(GetCompressedImage1().GetSaveOptionsBW().Format, GetCompressedImage2().GetSaveOptionsBW().Format, GetCompressedImage3().GetSaveOptionsBW().Format, GetCompressedImage4().GetSaveOptionsBW().Format);
            }
            else // Color
            {
                compressedImage1.CompressColorImage();
                compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                compressedImage2.CompressColorImage();
                compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                compressedImage3.CompressColorImage();
                compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                compressedImage4.CompressColorImage();
                compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
                UpdateCompressedNames(GetCompressedImage1().GetSaveOptionsColor().Format, GetCompressedImage2().GetSaveOptionsColor().Format, GetCompressedImage3().GetSaveOptionsColor().Format, GetCompressedImage4().GetSaveOptionsColor().Format);
            }
        }

        public void UpdateCompressedNames(ImageXFormat format1, ImageXFormat format2, ImageXFormat format3, ImageXFormat format4)
        {
            // Name the compressed tabs based on the format.
            int count1 = 1; // count of tab 1 duplicate file formats
            int count2 = 1; // count of other tab duplicate file formats
            tabControlImageInfo.TabPages[1].Text = format1.ToString();

            if (format2 == format1)
                tabControlImageInfo.TabPages[2].Text = format1.ToString() + " (" + count1++ + ")";
            else
                tabControlImageInfo.TabPages[2].Text = format2.ToString();

            if (format3 == format1)
                tabControlImageInfo.TabPages[3].Text = format1.ToString() + " (" + count1++ + ")";
            else if (format3 == format2)
                tabControlImageInfo.TabPages[3].Text = format2.ToString() + " (" + count2++ + ")";
            else
                tabControlImageInfo.TabPages[3].Text = format3.ToString();

            if (format4 == format1)
                tabControlImageInfo.TabPages[4].Text = format1.ToString() + " (" + count1++ + ")";
            else if (format4 == format2)
                tabControlImageInfo.TabPages[4].Text = format2.ToString() + " (" + count2++ + ")";
            else if (format4 == format3)
                tabControlImageInfo.TabPages[4].Text = format3.ToString() + " (" + count2++ + ")";
            else
                tabControlImageInfo.TabPages[4].Text = format4.ToString();

            // Name the compressed image labels.
            labelCompressed1.Text = tabControlImageInfo.TabPages[1].Text;
            labelCompressed2.Text = tabControlImageInfo.TabPages[2].Text;
            labelCompressed3.Text = tabControlImageInfo.TabPages[3].Text;
            labelCompressed4.Text = tabControlImageInfo.TabPages[4].Text;
        }

        public void ResetCompressedImages()
        {
            // If one of the compressed images is grown, shrink it.
            if (ShowingThumbs() == false)
            {
                if (compressed1ImageXView.Visible == true)
                    compressedImage1.Select(compressed2ImageXView, compressed3ImageXView, compressed4ImageXView, labelCompressed2, labelCompressed3, labelCompressed4);
                else if (compressed2ImageXView.Visible == true)
                    compressedImage2.Select(compressed1ImageXView, compressed4ImageXView, compressed3ImageXView, labelCompressed1, labelCompressed4, labelCompressed3);
                else if (compressed3ImageXView.Visible == true)
                    compressedImage3.Select(compressed4ImageXView, compressed1ImageXView, compressed2ImageXView, labelCompressed4, labelCompressed1, labelCompressed2);
                else if (compressed4ImageXView.Visible == true)
                    compressedImage4.Select(compressed3ImageXView, compressed2ImageXView, compressed1ImageXView, labelCompressed3, labelCompressed2, labelCompressed1);
            }
            compressed1ImageXView.Visible = false;
            compressed2ImageXView.Visible = false;
            compressed3ImageXView.Visible = false;
            compressed4ImageXView.Visible = false;
            labelCompressed1.Text = "";
            labelCompressed2.Text = "";
            labelCompressed3.Text = "";
            labelCompressed4.Text = "";
        }

        #endregion

        #region Read and Write Compression Settings

        private void loadCompressionSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (originalImageXView.Image != null)
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Title = "Select a Compression Settings File";
                dlg.Filter = "XML (*.XML)|*.xml|All Files (*.*)|*.*";
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    strSettingsFile = dlg.FileName;
                    ReadCompressionSettings();

                    // Compress the image using the newly loaded compression settings.
                    ResetCompressedImages();
                    CompressImages();
                }
                else if (res != DialogResult.Cancel)
                {
                    DisplayStatusText("Error loading compression settings file.");
                }
            }
        }

        private void saveCompressionSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (originalImageXView.Image != null)
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "Save Compression Settings File";
                dlg.Filter = "XML (*.XML)|*.xml|All Files (*.*)|*.*";
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    WriteCompressionSettings(dlg.FileName);
                }
                else if (res != DialogResult.Cancel)
                {
                    DisplayStatusText("Error saving compression settings file.");
                }
            }
        }

        private void ReadCompressionSettings()
        {
            XmlTextReader fileReader = new XmlTextReader(strSettingsFile);
            fileReader.ReadStartElement("Settings");

            fileReader.ReadStartElement("Color");
            compressedImage1.ReadColorCompressionSettings(fileReader);
            compressedImage2.ReadColorCompressionSettings(fileReader);
            compressedImage3.ReadColorCompressionSettings(fileReader);
            compressedImage4.ReadColorCompressionSettings(fileReader);
            fileReader.Read();
            fileReader.MoveToContent();
            fileReader.ReadEndElement();

            fileReader.ReadStartElement("BW");
            compressedImage1.ReadBWCompressionSettings(fileReader);
            compressedImage2.ReadBWCompressionSettings(fileReader);
            compressedImage3.ReadBWCompressionSettings(fileReader);
            compressedImage4.ReadBWCompressionSettings(fileReader);

            fileReader.Read();
            fileReader.MoveToContent();
            fileReader.ReadEndElement();

            fileReader.ReadStartElement("Grayscale");
            compressedImage1.ReadGrayscaleCompressionSettings(fileReader);
            compressedImage2.ReadGrayscaleCompressionSettings(fileReader);
            compressedImage3.ReadGrayscaleCompressionSettings(fileReader);
            compressedImage4.ReadGrayscaleCompressionSettings(fileReader);

            fileReader.Close();
        }

        private void WriteCompressionSettings(String outputFile)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.NewLineOnAttributes = true;
            XmlWriter fileWriter = XmlWriter.Create(outputFile, settings);
            fileWriter.WriteStartElement("Settings");
            fileWriter.WriteStartElement("Color");
            compressedImage1.WriteColorCompressionSettings(fileWriter);
            compressedImage2.WriteColorCompressionSettings(fileWriter);
            compressedImage3.WriteColorCompressionSettings(fileWriter);
            compressedImage4.WriteColorCompressionSettings(fileWriter);
            fileWriter.WriteEndElement();
            fileWriter.WriteStartElement("BW");
            compressedImage1.WriteBWCompressionSettings(fileWriter);
            compressedImage2.WriteBWCompressionSettings(fileWriter);
            compressedImage3.WriteBWCompressionSettings(fileWriter);
            compressedImage4.WriteBWCompressionSettings(fileWriter);
            fileWriter.WriteEndElement();
            fileWriter.WriteStartElement("Grayscale");
            compressedImage1.WriteGrayscaleCompressionSettings(fileWriter);
            compressedImage2.WriteGrayscaleCompressionSettings(fileWriter);
            compressedImage3.WriteGrayscaleCompressionSettings(fileWriter);
            compressedImage4.WriteGrayscaleCompressionSettings(fileWriter);
            fileWriter.WriteEndElement();
            fileWriter.WriteEndElement();
            fileWriter.Flush();
            fileWriter.Close();
        }

        #endregion

        #region Form Resizing

        public Size GetPanelSize()
        {
            return panelGradientNoFlareCompressed.Size;
        }

        public Size GetThumbSize()
        {
            return new Size((int)((float)panelCompressedImage.Size.Width / 2.363636 + 0.5), (int)((float)panelCompressedImage.Size.Height / 2.363636 + 0.5));
        }

        public void ResizeCompressedImages()
        {
            if (originalImageXView.Image != null)
            {
                int xLeft = 0;      // the x position for the left 2 compressed images
                int xRight = 0;     // the x position for the right 2 compressed images
                int yTop = 0;       // the y position for the top 2 compressed images
                int yBottom = 0;    // the y position for the bottom 2 compressed images

                bool showingThumbs = ShowingThumbs();
                Size thumbSize = GetThumbSize();
                labelCompressed1.Width = thumbSize.Width - 2;
                labelCompressed2.Width = thumbSize.Width - 2;
                labelCompressed3.Width = thumbSize.Width - 2;
                labelCompressed4.Width = thumbSize.Width - 2;

                // Only modify the compressed image's location and size if it is shrunk.
                // It is not shrunk in the case that thumbs are not showing and it is visible.

                if (showingThumbs == true || compressed1ImageXView.Visible == false)
                {
                    xLeft = compressed1ImageXView.Location.X;
                    yTop = compressed1ImageXView.Location.Y;
                    compressed1ImageXView.Size = thumbSize;
                }

                if (showingThumbs == true || compressed2ImageXView.Visible == false)
                {
                    compressed2ImageXView.Size = thumbSize;
                    compressed2ImageXView.Location = new Point(panelCompressedImage.Width - compressed2ImageXView.Width - 20, compressed2ImageXView.Location.Y);
                    xRight = compressed2ImageXView.Location.X;
                    yTop = compressed2ImageXView.Location.Y;
                }

                if (showingThumbs == true || compressed3ImageXView.Visible == false)
                {
                    compressed3ImageXView.Size = thumbSize;
                    compressed3ImageXView.Location = new Point(compressed3ImageXView.Location.X, panelCompressedImage.Height - compressed3ImageXView.Height - labelCompressed3.Height - 6);
                    xLeft = compressed3ImageXView.Location.X;
                    yBottom = compressed3ImageXView.Location.Y;
                }

                if (showingThumbs == true || compressed4ImageXView.Visible == false)
                {
                    compressed4ImageXView.Size = thumbSize;
                    compressed4ImageXView.Location = new Point(panelCompressedImage.Width - compressed4ImageXView.Width - 20, panelCompressedImage.Height - compressed4ImageXView.Height - labelCompressed4.Height - 6);
                    xRight = compressed4ImageXView.Location.X;
                    yBottom = compressed4ImageXView.Location.Y;
                }

                // Update the compressed image thumb locations, which are used as the destination
                // location when shrinking a grown compressed image back to thumb size.
                compressedImage1.SetThumbLocation(new Point(xLeft, yTop));
                compressedImage2.SetThumbLocation(new Point(xRight, yTop));
                compressedImage3.SetThumbLocation(new Point(xLeft, yBottom));
                compressedImage4.SetThumbLocation(new Point(xRight, yBottom));
                // Update the compressed image label locations.
                labelCompressed1.Location = new Point(xLeft, yTop + thumbSize.Height + 3);
                labelCompressed2.Location = new Point(xRight, yTop + thumbSize.Height + 3);
                labelCompressed3.Location = new Point(xLeft, yBottom + thumbSize.Height + 3);
                labelCompressed4.Location = new Point(xRight, yBottom + thumbSize.Height + 3);
                // Update the image info panel location.
                panelBackgroundFrameImageInfo.Location = new Point(this.Width - panelBackgroundFrameImageInfo.Width - 24, panelBackgroundFrameImageInfo.Location.Y);
            }
        }

        private void originalImageXView_Resize(object sender, EventArgs e)
        {
            BestFitOriginalImage();
        }

        private bool BestFitOriginalImage()
        {
            if (originalImageXView.Image != null)
            {
                bool widthIsSmaller = originalImageXView.Image.ImageXData.Width * originalImageXView.ZoomFactor < originalImageXView.Width;
                bool heightIsSmaller = originalImageXView.Image.ImageXData.Height * originalImageXView.ZoomFactor < originalImageXView.Height;
                if (widthIsSmaller || heightIsSmaller)
                {
                    float widthToHeightFactorControl = (float)originalImageXView.Width / (float)originalImageXView.Height;
                    float widthToHeightFactorImage = (float)originalImageXView.Image.ImageXData.Width / (float)originalImageXView.Image.ImageXData.Height;
                    if (widthToHeightFactorControl > widthToHeightFactorImage)
                        originalImageXView.AutoResize = AutoResizeType.FitWidth;
                    else
                        originalImageXView.AutoResize = AutoResizeType.FitHeight;
                    return true;
                }
            }
            return false;
        }

        private void panelBackgroundFrame_Resize(object sender, EventArgs e)
        {
            panelBackgroundFrame.Invalidate();
            ResizeCompressedImages();
        }

        #endregion

    }
}
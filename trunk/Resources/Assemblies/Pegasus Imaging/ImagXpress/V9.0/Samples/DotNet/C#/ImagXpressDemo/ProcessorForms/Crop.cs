/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo.ProcessorForms
{
    public partial class CropForm : Form
    {
        public CropForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();
        
        private bool mouseDown;

        public ImageX CurrentImage
        {
            set
            {
                imageXViewCurrent.Image = value;
            }
        }

        public ImageX PreviewImage
        {
            get
            {
                return imageXViewPreview.Image;
            }
        }

        private void ProcessorForm_Load(object sender, EventArgs e)
        {
            try
            {
             
                
                unlock.UnlockControls(imagXpress1);
                
                LBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                WBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                TBox.Maximum = imageXViewCurrent.Image.ImageXData.Height;
                HBox.Maximum = imageXViewCurrent.Image.ImageXData.Height;

                LBox.Value = imageXViewCurrent.Image.ImageXData.Width / 5;
                WBox.Value = imageXViewCurrent.Image.ImageXData.Width * 4 / 5;
                TBox.Value = imageXViewCurrent.Image.ImageXData.Height / 5;
                HBox.Value = imageXViewCurrent.Image.ImageXData.Height * 4 / 5;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            PreviewButton_Click(sender, e);

            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
                
                processor1.Image = imageXViewCurrent.Image.Copy();

                processor1.Crop(new Rectangle((int)LBox.Value, (int)TBox.Value, (int)WBox.Value, (int)HBox.Value));

                if (imageXViewPreview != null)
                {
                    imageXViewPreview.Image = processor1.Image;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void HelpProcessorButton_Click(object sender, EventArgs e)
        {
            Helper help = new Helper();

            help.ShowHelp(this, Text);
        }

        private void imageXViewCurrent_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                mouseDown = true;

                imageXViewCurrent.Rubberband.Stop();
                imageXViewCurrent.Rubberband.Clear();

                imageXViewCurrent.Rubberband.Start(e.Location);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void imageXViewCurrent_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (mouseDown == true)
                {
                    imageXViewCurrent.Rubberband.Update(e.Location);
                }
            }
            catch (Exception) { };
        }

        private void imageXViewCurrent_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                mouseDown = false;

                LBox.Value = imageXViewCurrent.Rubberband.Dimensions.Left;
                TBox.Value = imageXViewCurrent.Rubberband.Dimensions.Top;

                int width = imageXViewCurrent.Rubberband.Dimensions.Width;
                int height = imageXViewCurrent.Rubberband.Dimensions.Height;

                if (width == 0)
                {
                    WBox.Value = 1;
                    LBox.Value = 0;
                }
                else
                {
                    WBox.Value = width;
                }
                if (height == 0)
                {
                    HBox.Value = 1;
                    TBox.Value = 0;
                }
                else
                {
                    HBox.Value = height;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void LBox_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                WBox.Maximum = Math.Abs(imageXViewCurrent.Image.ImageXData.Width - LBox.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void TBox_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                HBox.Maximum = Math.Abs(imageXViewCurrent.Image.ImageXData.Height - TBox.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace HighGray
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private static readonly string []							PegasusRawFileNames = new string [] {"w256_h256_bpp12.raw","w512_h512_bpp12.raw","w512_h512_bpp16.raw"};
		private static readonly int []								PegasusRawWidths = new int [] {256,512,512};
		private static readonly int []								PegasusRawHeights = new int [] {256,512,512};
		private static readonly int []								PegasusRawBitsPer = new int [] {12,12,16};
		private static readonly int []								PegasusRawBytesPer = new int [] {2,2,2};
		private static readonly int []								PegasusRawHighBit = new int [] {0,0,0};
		private static readonly int []								PegasusRawPixelFormat = new int [] {2,2,2};
		private static readonly int []								PegasusRawStride = new int [] {512,1024,1024};
		private int []												TrackBar1Mins = new int [] {-50,1}; //index 0 == standard
		private int []												TrackBar1Maxs = new int [] {50,10}; //index 1 = medical
		private int []												TrackBar2Mins = new int [] {-50,-2000};
		private int []												TrackBar2Maxs = new int [] {50,10};
		private static readonly string []							TrackBar1Label = new string [] {"Brightness","Window Width"};
		private static readonly string []							TrackBar2Label = new string [] {"Contrast","Window Center"};
		private static readonly bool []								SlopeInterceptEnable = new bool [] {false,true};
		private int []												LastTrack1 = new int[] {1,1};
		private int []												LastTrack2 = new int[] {1,1};
		private string												strCurrentDir;
		private string												strCurrentImage;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress		imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView		imageXView1;
		private PegasusImaging.WinForms.ImagXpress9.ImageX          imageX1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuFileOpen;
		private System.Windows.Forms.MenuItem menuFileSaveAs;
		private System.Windows.Forms.MenuItem menuFileHR;
		private System.Windows.Forms.MenuItem menuFileQuit;
		private System.Windows.Forms.MenuItem menuToolbar;
		private System.Windows.Forms.MenuItem menuToolbarShow;
		private System.Windows.Forms.MenuItem menuAbout;
		private System.Windows.Forms.Label labelLastError;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.Label lblGrayMode;
		private System.Windows.Forms.ComboBox comboGrayMode;
		private System.Windows.Forms.Label labelSlope;
		private System.Windows.Forms.TextBox textBoxSlope;
		private System.Windows.Forms.Label labelIntercept;
		private System.Windows.Forms.TextBox textBoxIntercept;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.TrackBar trackBar2;
		private System.Windows.Forms.Label labelTrackBar1;
		private System.Windows.Forms.Label labelTrackBar2;
        private System.Windows.Forms.ListBox listBox1;
        private IContainer components;

		public Form1()
		{

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}

				//***call the Dispose method on the imagXpress1 object and the
				//*** imageXView1 object
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
			}
			
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuFile = new System.Windows.Forms.MenuItem();
            this.menuFileOpen = new System.Windows.Forms.MenuItem();
            this.menuFileSaveAs = new System.Windows.Forms.MenuItem();
            this.menuFileHR = new System.Windows.Forms.MenuItem();
            this.menuFileQuit = new System.Windows.Forms.MenuItem();
            this.menuToolbar = new System.Windows.Forms.MenuItem();
            this.menuToolbarShow = new System.Windows.Forms.MenuItem();
            this.menuAbout = new System.Windows.Forms.MenuItem();
            this.labelLastError = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblGrayMode = new System.Windows.Forms.Label();
            this.comboGrayMode = new System.Windows.Forms.ComboBox();
            this.labelSlope = new System.Windows.Forms.Label();
            this.textBoxSlope = new System.Windows.Forms.TextBox();
            this.labelIntercept = new System.Windows.Forms.Label();
            this.textBoxIntercept = new System.Windows.Forms.TextBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.labelTrackBar1 = new System.Windows.Forms.Label();
            this.labelTrackBar2 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.Location = new System.Drawing.Point(8, 88);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(536, 88);
            this.imageXView1.TabIndex = 0;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuFile,
            this.menuToolbar,
            this.menuAbout});
            // 
            // menuFile
            // 
            this.menuFile.Index = 0;
            this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuFileOpen,
            this.menuFileSaveAs,
            this.menuFileHR,
            this.menuFileQuit});
            this.menuFile.Text = "&File";
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Index = 0;
            this.menuFileOpen.Text = "&Open";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuFileSaveAs
            // 
            this.menuFileSaveAs.Index = 1;
            this.menuFileSaveAs.Text = "&Save As...";
            this.menuFileSaveAs.Click += new System.EventHandler(this.menuFileSaveAs_Click);
            // 
            // menuFileHR
            // 
            this.menuFileHR.Index = 2;
            this.menuFileHR.Text = "-";
            // 
            // menuFileQuit
            // 
            this.menuFileQuit.Index = 3;
            this.menuFileQuit.Text = "&Quit";
            this.menuFileQuit.Click += new System.EventHandler(this.menuFileQuit_Click);
            // 
            // menuToolbar
            // 
            this.menuToolbar.Index = 1;
            this.menuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuToolbarShow});
            this.menuToolbar.Text = "&Toolbar";
            // 
            // menuToolbarShow
            // 
            this.menuToolbarShow.Index = 0;
            this.menuToolbarShow.Text = "&Show";
            this.menuToolbarShow.Click += new System.EventHandler(this.menuToolbarShow_Click);
            // 
            // menuAbout
            // 
            this.menuAbout.Index = 2;
            this.menuAbout.Text = "&About";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // labelLastError
            // 
            this.labelLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLastError.Location = new System.Drawing.Point(550, 86);
            this.labelLastError.Name = "labelLastError";
            this.labelLastError.Size = new System.Drawing.Size(64, 16);
            this.labelLastError.TabIndex = 1;
            this.labelLastError.Text = "Last Error:";
            this.labelLastError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.Location = new System.Drawing.Point(550, 118);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(208, 112);
            this.lblError.TabIndex = 2;
            // 
            // lblGrayMode
            // 
            this.lblGrayMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblGrayMode.Location = new System.Drawing.Point(8, 184);
            this.lblGrayMode.Name = "lblGrayMode";
            this.lblGrayMode.Size = new System.Drawing.Size(64, 16);
            this.lblGrayMode.TabIndex = 4;
            this.lblGrayMode.Text = "Gray Mode:";
            this.lblGrayMode.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // comboGrayMode
            // 
            this.comboGrayMode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboGrayMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGrayMode.Location = new System.Drawing.Point(80, 184);
            this.comboGrayMode.Name = "comboGrayMode";
            this.comboGrayMode.Size = new System.Drawing.Size(136, 21);
            this.comboGrayMode.TabIndex = 5;
            this.comboGrayMode.SelectedIndexChanged += new System.EventHandler(this.comboGrayMode_SelectedIndexChanged);
            // 
            // labelSlope
            // 
            this.labelSlope.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSlope.Location = new System.Drawing.Point(232, 184);
            this.labelSlope.Name = "labelSlope";
            this.labelSlope.Size = new System.Drawing.Size(80, 16);
            this.labelSlope.TabIndex = 6;
            this.labelSlope.Text = "Rescale Slope:";
            // 
            // textBoxSlope
            // 
            this.textBoxSlope.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSlope.Location = new System.Drawing.Point(320, 184);
            this.textBoxSlope.Name = "textBoxSlope";
            this.textBoxSlope.Size = new System.Drawing.Size(64, 21);
            this.textBoxSlope.TabIndex = 7;
            this.textBoxSlope.Text = "1";
            this.textBoxSlope.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxSlope.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxSlope_KeyDown);
            this.textBoxSlope.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxSlope_Validating);
            // 
            // labelIntercept
            // 
            this.labelIntercept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIntercept.Location = new System.Drawing.Point(392, 184);
            this.labelIntercept.Name = "labelIntercept";
            this.labelIntercept.Size = new System.Drawing.Size(96, 16);
            this.labelIntercept.TabIndex = 8;
            this.labelIntercept.Text = "Rescale Intercept:";
            // 
            // textBoxIntercept
            // 
            this.textBoxIntercept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIntercept.Location = new System.Drawing.Point(496, 184);
            this.textBoxIntercept.Name = "textBoxIntercept";
            this.textBoxIntercept.Size = new System.Drawing.Size(48, 21);
            this.textBoxIntercept.TabIndex = 9;
            this.textBoxIntercept.Text = "0";
            this.textBoxIntercept.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxIntercept.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxIntercept_KeyDown);
            this.textBoxIntercept.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxIntercept_Validating);
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.Location = new System.Drawing.Point(8, 216);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(536, 45);
            this.trackBar1.TabIndex = 10;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // trackBar2
            // 
            this.trackBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar2.Location = new System.Drawing.Point(8, 288);
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(536, 45);
            this.trackBar2.TabIndex = 11;
            this.trackBar2.ValueChanged += new System.EventHandler(this.trackBar2_ValueChanged);
            // 
            // labelTrackBar1
            // 
            this.labelTrackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTrackBar1.Location = new System.Drawing.Point(8, 258);
            this.labelTrackBar1.Name = "labelTrackBar1";
            this.labelTrackBar1.Size = new System.Drawing.Size(544, 16);
            this.labelTrackBar1.TabIndex = 12;
            this.labelTrackBar1.Text = "Window Width";
            this.labelTrackBar1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTrackBar2
            // 
            this.labelTrackBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTrackBar2.Location = new System.Drawing.Point(16, 344);
            this.labelTrackBar2.Name = "labelTrackBar2";
            this.labelTrackBar2.Size = new System.Drawing.Size(528, 16);
            this.labelTrackBar2.TabIndex = 13;
            this.labelTrackBar2.Text = "Window Center";
            this.labelTrackBar2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Using grayscale images (choose from the .raw files in the default directory)."});
            this.listBox1.Location = new System.Drawing.Point(8, 8);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(776, 69);
            this.listBox1.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(792, 377);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.labelTrackBar2);
            this.Controls.Add(this.labelTrackBar1);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.textBoxIntercept);
            this.Controls.Add(this.labelIntercept);
            this.Controls.Add(this.textBoxSlope);
            this.Controls.Add(this.labelSlope);
            this.Controls.Add(this.comboGrayMode);
            this.Controls.Add(this.lblGrayMode);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.labelLastError);
            this.Controls.Add(this.imageXView1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "High Grayscale";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void menuAbout_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
			
		}
		private void Form1_Load(object sender, System.EventArgs e)
		{
            Application.EnableVisualStyles();

			// **The UnlockRuntime function must be called to distribute the runtime**
			// imagXpress1.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)

			strCurrentDir = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..\..\..\..\..\..\..\Common\Images\");

			comboGrayMode.Items.Add("Standard");
			comboGrayMode.Items.Add("Medical");
			comboGrayMode.SelectedIndex = 0;
		}

		private void comboGrayMode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//enable/disable the slope intercept stuff
			labelSlope.Enabled = SlopeInterceptEnable[comboGrayMode.SelectedIndex];
			textBoxSlope.Enabled = SlopeInterceptEnable[comboGrayMode.SelectedIndex];

			labelIntercept.Enabled = SlopeInterceptEnable[comboGrayMode.SelectedIndex];
			textBoxIntercept.Enabled = SlopeInterceptEnable[comboGrayMode.SelectedIndex];

			//change the ranges of the trackbars
			trackBar1.Minimum = TrackBar1Mins[comboGrayMode.SelectedIndex];
			trackBar1.Maximum = TrackBar1Maxs[comboGrayMode.SelectedIndex];

			trackBar2.Minimum = TrackBar2Mins[comboGrayMode.SelectedIndex];
			trackBar2.Maximum = TrackBar2Maxs[comboGrayMode.SelectedIndex];

			//change the labels of the trackbars
			labelTrackBar1.Text = TrackBar1Label[comboGrayMode.SelectedIndex] + ": " + trackBar1.Value.ToString(cultNumber);
			labelTrackBar2.Text = TrackBar2Label[comboGrayMode.SelectedIndex] + ": " + trackBar2.Value.ToString(cultNumber);

			//change the gray mode
			imageXView1.GrayMode = (PegasusImaging.WinForms.ImagXpress9.GrayMode)comboGrayMode.SelectedIndex;

			//Set the trackbar values (to the values last set in that mode)
			trackBar1.Value = LastTrack1[comboGrayMode.SelectedIndex];
			trackBar2.Value = LastTrack2[comboGrayMode.SelectedIndex];
		}

		private void trackBar1_ValueChanged(object sender, System.EventArgs e)
		{
			labelTrackBar1.Text = TrackBar1Label[comboGrayMode.SelectedIndex] + ": " + trackBar1.Value.ToString();
			if (comboGrayMode.SelectedIndex == (int)PegasusImaging.WinForms.ImagXpress9.GrayMode.Standard)
			{
				imageXView1.Brightness = trackBar1.Value;
			} 
			else 
			{
                imageXView1.Medical.WindowWidth = (double)trackBar1.Value;			
			}
			//update the last value array for tracking...
			LastTrack1[comboGrayMode.SelectedIndex] = trackBar1.Value;
		}

		private void trackBar2_ValueChanged(object sender, System.EventArgs e)
		{
			labelTrackBar2.Text = TrackBar2Label[comboGrayMode.SelectedIndex] + ": " + trackBar2.Value.ToString(cultNumber);
			if (comboGrayMode.SelectedIndex == (int)PegasusImaging.WinForms.ImagXpress9.GrayMode.Standard)
			{
				imageXView1.Contrast = trackBar2.Value;
			} 
			else 
			{
				imageXView1.Medical.WindowCenter = (double)trackBar2.Value;			
			}	
			//update the last value array for tracking...
			LastTrack2[comboGrayMode.SelectedIndex] = trackBar2.Value;
		}

		private void textBoxSlope_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//need to check that the text is a double, and a valid one...
			//if it is not a valid double, or an exception is thrown, then the event should be cancelled
			double daNum;
			try 
			{
				daNum = Convert.ToDouble(textBoxSlope.Text);
			}
			catch
			{		
				lblError.Text = "Error: Could not convert the slope to a number.";
				e.Cancel = true;
				textBoxSlope.Focus();
				return;
			}
			try 
			{
				imageXView1.Medical.RescaleSlope = daNum;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
			{
				PegasusError(eX,lblError);
				e.Cancel = true;
				textBoxSlope.Focus();
				return;
			}
		}

		private void textBoxIntercept_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			//need to check that the text is a double, and a valid one...
			//if it is not a valid double, or an exception is thrown, then the event should be cancelled
			double daNum;
			try 
			{
				daNum = Convert.ToDouble(textBoxSlope.Text);
			}
			catch
			{		
				lblError.Text = "Error: Could not convert the slope to a number.";
				e.Cancel = true;
				textBoxIntercept.Focus();
				return;
			}
			try 
			{
				imageXView1.Medical.RescaleIntercept = daNum;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
			{
				PegasusError(eX,lblError);
				e.Cancel = true;
				textBoxIntercept.Focus();
				return;
			}
		}

		private void menuToolbarShow_Click(object sender, System.EventArgs e)
		{
			menuToolbarShow.Text = (imageXView1.Toolbar.Activated)? "&Show":"&Hide";
			try
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
			{
				PegasusError(eX,lblError);
			}
		}

		private void menuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private bool IsPegasusSampleRawMedicalFile(System.String daFileName)
		{
			for (int i = 0; i < PegasusRawFileNames.Length; i++)
			{
				if (PegasusRawFileNames[i] == daFileName)
					return true;
			}
			return false;
		}
		private int GetIndexOfPegasusSampleRawMedicalFile(System.String daFileName)
		{
			for (int i = 0; i < PegasusRawFileNames.Length; i++)
			{
				if (PegasusRawFileNames[i] == daFileName)
					return i;
			}
			return -1;
		}

		private void SetMedicalTrackBarRange(int daBPP)
		{
			TrackBar1Maxs[1] = ((int)Math.Pow(2,daBPP));
			TrackBar2Maxs[1] = ((int)Math.Pow(2,daBPP));
		}

		private void menuFileOpen_Click(object sender, System.EventArgs e)
		{
			System.String strTmp = PegasusOpenFile("Medical Raw Image Files (*.raw)|*.raw|Lossless JPEG Files (*.ljp)|*.ljp| All Files(*.*)|*.*");
			if (strTmp.Length != 0)
			{
				if (strTmp.EndsWith(".ljp"))
				{
					try 
					{
						strCurrentImage = strTmp;
						imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strTmp);
						lblError.Text = "";
					} 
					catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
					{
						PegasusError(eX,lblError);
					}
				} 
				else 
				{
					
						strCurrentImage = strTmp;
						PegasusImaging.WinForms.ImagXpress9.LoadOptions lo = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
						lo.LoadMode = PegasusImaging.WinForms.ImagXpress9.LoadMode.Raw;
					

						// check to see if it is one of our shipping raw images that we know the info for
						if (IsPegasusSampleRawMedicalFile(System.IO.Path.GetFileName(strCurrentImage)))
						{
							try 
							{
								int index = GetIndexOfPegasusSampleRawMedicalFile(System.IO.Path.GetFileName(strCurrentImage));
								lo.Raw.BitsPerPixel = PegasusRawBitsPer[index];
								lo.Raw.BytesPerPixel = PegasusRawBytesPer[index];
								lo.Raw.Height = PegasusRawHeights[index];
								lo.Raw.Width = PegasusRawWidths[index];
								lo.Raw.HighBitIndex = PegasusRawHighBit[index];
								lo.Raw.Stride = PegasusRawStride[index];
								lo.Raw.PixelFormat = ((PegasusImaging.WinForms.ImagXpress9.ImageXPixelFormat)PegasusRawPixelFormat[index]);

								SetMedicalTrackBarRange(lo.Raw.BitsPerPixel);

								imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1,strTmp, lo);
							
								// clear out the error in case there was an error from a previous operation
								lblError.Text = "";
							}
							catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
							{
								PegasusError(eX,lblError);
							}

						} 
						else 
						{

							
								LoadRawSettings RawSettings = new LoadRawSettings(strTmp);
								if (RawSettings.ShowDialog(this) == DialogResult.OK) 
								{
									try
									{
										lo.Raw.BitsPerPixel = RawSettings.GetRawBitsPerPixel();
										lo.Raw.BytesPerPixel = RawSettings.GetRawBytesPerPixel();
										lo.Raw.Height = RawSettings.GetRawHeight();
										lo.Raw.Width = RawSettings.GetRawWidth();
										lo.Raw.HighBitIndex = RawSettings.GetRawHighBitIndex();
										lo.Raw.Stride = RawSettings.GetRawStride();
										lo.Raw.PixelFormat = RawSettings.GetRawPixelFormat();
										lo.ImageOffset = RawSettings.GetRawOffset();
							
										SetMedicalTrackBarRange(lo.Raw.BitsPerPixel);

										imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strTmp, lo);
								
										// clear out the error in case there was an error from a previous operation
										lblError.Text = "";
									
									}
									catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
									{
										PegasusError(eX,lblError);
									}
									catch (System.Exception eX)
									{
										PegasusError(eX,lblError);
									}
						
								} 
								else 
								{
									lblError.Text = "Loading of the image cancelled.";
									return;
								}
						
						}
				}
					
			}
		}

		private void textBoxSlope_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if ((e.KeyCode == System.Windows.Forms.Keys.Enter) || (e.KeyCode == System.Windows.Forms.Keys.Return))
			{
				e.Handled = true;
				textBoxIntercept.Focus();
			} 
			else 
			{
				e.Handled = false;
			}
			
		}

		private void textBoxIntercept_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if ((e.KeyCode == System.Windows.Forms.Keys.Enter) || (e.KeyCode == System.Windows.Forms.Keys.Return))
			{
				e.Handled = true;
				trackBar1.Focus();
			} 
			else 
			{
				e.Handled = false;
			}
		}

		private void menuFileSaveAs_Click(object sender, System.EventArgs e)
		{
			try
			{
				SaveOptionsSettings SaveOptionsSettings = new SaveOptionsSettings();
				if (SaveOptionsSettings.ShowDialog(this) == DialogResult.OK) 
				{
					imageX1 = imageXView1.Image;
					imageX1.Save(SaveOptionsSettings.GetSaveFileName(),SaveOptionsSettings.GetSaveOptions());
					
				}
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX)
			{
				PegasusError(eX,lblError);
			}
		}
	}
}

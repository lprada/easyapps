/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo
{
    public partial class RawForm : Form
    {
        public RawForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();

        private ByteViewer bv;

        private string fileName;
        
        private ImageXPixelFormat formatChosen;

        private int widthChosen, heightChosen, stride, highBitIndex, bitsPerPixel, bytesPerPixel, offset;

        private LoadOptions lo;

        public string FileName
        {
            set
            {
                fileName = value;
            }
        }

        public ImageXPixelFormat FormatChosen
        {
            get
            {
                return formatChosen;
            }
            set
            {
                formatChosen = value;
            }
        }

        public int Offset
        {
            get
            {
                return (int)offsetBox.Value;
            }
            set
            {
                offset = value;
            }
        }

        public int WidthChosen
        {
            get
            {
                return (int)WidthBox.Value;
            }
            set
            {
                widthChosen = value;
            }
        }

        public int HeightChosen
        {
            get
            {
                return (int)HeightBox.Value;
            }
            set
            {
                heightChosen = value;
            }
        }

        public int Stride
        {
            get
            {
                return (int)StrideBox.Value;
            }
            set
            {
                stride = value;
            }
        }

        public int HighBitIndex
        {
            get
            {
                return (int)HighBox.Value;
            }
            set
            {
                highBitIndex = value;
            }
        }

        public int BitsPerPixel
        {
            get
            {
                return (int)BitsBox.Value;
            }
            set
            {
                bitsPerPixel = value;
            }
        }

        public int BytesPerPixel
        {
            get
            {
                return (int)BytesBox.Value;
            }
            set
            {
                bytesPerPixel = value;
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void RawForm_Load(object sender, EventArgs e)
        {
            try
            {


                unlock.UnlockControls(imagXpress1);

                
                bv = new ByteViewer();                

                bv.Location = new Point(DescriptionListBox.Location.X, offsetLabel.Location.Y + 30);
                bv.Height = Height - bv.Location.Y - (Height - OKButton.Location.Y);
                bv.SetDisplayMode(System.ComponentModel.Design.DisplayMode.Hexdump);
                
                Controls.Add(bv);
                
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    fs.Position = 0;

                    byte[] data = new byte[fs.Length];

                    fs.Read(data, 0, data.Length);

                    bv.SetBytes(data);
                }

                lo = new LoadOptions();

                switch ((int)formatChosen)
                {
                    case 1:
                        {
                            FormatComboBox.SelectedIndex = 0;
                            break;
                        }
                    case 2:
                        {
                            FormatComboBox.SelectedIndex = 1;
                            break;
                        }
                    case 4:
                        {
                            FormatComboBox.SelectedIndex = 2;
                            break;
                        }
                    case 8:
                        {
                            FormatComboBox.SelectedIndex = 3;
                            break;
                        }
                    case 0x10:
                        {
                            FormatComboBox.SelectedIndex = 4;
                            break;
                        }
                    case 0x20:
                        {
                            FormatComboBox.SelectedIndex = 5;
                            break;
                        }
                    case 0x40:
                        {
                            FormatComboBox.SelectedIndex = 6;
                            break;
                        }
                    case 0x80:
                        {
                            FormatComboBox.SelectedIndex = 7;
                            break;
                        }
                    case 0x100:
                        {
                            FormatComboBox.SelectedIndex = 8;
                            break;
                        }
                    case 0x400:
                        {
                            FormatComboBox.SelectedIndex = 9;
                            break;
                        }
                    case 0x800:
                        {
                            FormatComboBox.SelectedIndex = 10;
                            break;
                        }
                    case 0x1000:
                        {
                            FormatComboBox.SelectedIndex = 11;
                            break;
                        }
                    case 0x10000:
                        {
                            FormatComboBox.SelectedIndex = 12;
                            break;
                        }
                    case 0x20000:
                        {
                            FormatComboBox.SelectedIndex = 13;
                            break;
                        }
                    case 0x100000:
                        {
                            FormatComboBox.SelectedIndex = 14;
                            break;
                        }
                    case 0x200000:
                        {
                            FormatComboBox.SelectedIndex = 15;
                            break;
                        }
                    case 0x400000:
                        {
                            FormatComboBox.SelectedIndex = 16;
                            break;
                        }
                }

                BitsBox.Value = bitsPerPixel;
                BytesBox.Value = bytesPerPixel;
                StrideBox.Value = stride;
                WidthBox.Value = widthChosen;
                HeightBox.Value = heightChosen;
                HighBox.Value = highBitIndex;
                offsetBox.Value = offset;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadFileButton_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateRawImage()
        {
            try
            {
                lo.Raw.BitsPerPixel = (int)BitsBox.Value;
                lo.Raw.BytesPerPixel = (int)BytesBox.Value;
                lo.Raw.Height = (int)HeightBox.Value;
                lo.Raw.Width = (int)WidthBox.Value;
                lo.Raw.Stride = (int)StrideBox.Value;
                lo.Raw.HighBitIndex = (int)HighBox.Value;
                lo.Raw.PixelFormat = formatChosen;
                lo.ImageOffset = (int)offsetBox.Value;
                lo.LoadMode = LoadMode.Raw;

                if (imageXView1 != null)
                {
                    if (imageXView1.Image != null)
                    {
                        imageXView1.Image.Dispose();
                        imageXView1.Image = null;
                    }
                }

                imageXView1.Image = ImageX.FromFile(imagXpress1, fileName, lo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormatComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (FormatComboBox.SelectedIndex)
                {
                    case 0:
                        {
                            formatChosen = ImageXPixelFormat.Dib;
                            break;
                        }
                    case 1:
                        {
                            formatChosen = ImageXPixelFormat.Raw;
                            break;
                        }
                    case 2:
                        {
                            formatChosen = ImageXPixelFormat.BigEndian;
                            break;
                        }
                    case 3:
                        {
                            formatChosen = ImageXPixelFormat.TopDown;
                            break;
                        }
                    case 4:
                        {
                            formatChosen = ImageXPixelFormat.Signed;
                            break;
                        }
                    case 5:
                        {
                            formatChosen = ImageXPixelFormat.Gray;
                            break;
                        }
                    case 6:
                        {
                            formatChosen = ImageXPixelFormat.HighGray;
                            break;
                        }
                    case 7:
                        {
                            formatChosen = ImageXPixelFormat.Alpha;
                            break;
                        }
                    case 8:
                        {
                            formatChosen = ImageXPixelFormat.Rgb;
                            break;
                        }
                    case 9:
                        {
                            formatChosen = ImageXPixelFormat.Rgb555;
                            break;
                        }
                    case 10:
                        {
                            formatChosen = ImageXPixelFormat.Rgb565;
                            break;
                        }
                    case 11:
                        {
                            formatChosen = ImageXPixelFormat.Argb;
                            break;
                        }
                    case 12:
                        {
                            formatChosen = ImageXPixelFormat.ColorMapped;
                            break;
                        }
                    case 13:
                        {
                            formatChosen = ImageXPixelFormat.Cmyk;
                            break;
                        }
                    case 14:
                        {
                            formatChosen = ImageXPixelFormat.Compressed;
                            break;
                        }
                    case 15:
                        {
                            formatChosen = ImageXPixelFormat.NonInterleaved;
                            break;
                        }
                    case 16:
                        {
                            formatChosen = ImageXPixelFormat.NoDibPad;
                            break;
                        }
                }

                UpdateRawImage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void WidthBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }

        private void HeightBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }

        private void StrideBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }

        private void BytesBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }

        private void HighBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }

        private void BitsBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }

        private void offsetBox_ValueChanged(object sender, EventArgs e)
        {
            UpdateRawImage();
        }
    }
}
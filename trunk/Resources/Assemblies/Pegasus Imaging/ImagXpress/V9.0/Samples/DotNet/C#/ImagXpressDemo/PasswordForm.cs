/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ImagXpressDemo
{
    public partial class PasswordForm : Form
    {
        public PasswordForm()
        {
            InitializeComponent();
        }

        private string secureFileType;

        public string SecureFileType
        {
            set
            {
                secureFileType = value;
            }
        }

        public string Password
        {
            get
            {
                return passwordTextBox.Text;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();

            DialogResult = DialogResult.Cancel;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            Close();

            DialogResult = DialogResult.OK;
        }

        private void PasswordForm_Load(object sender, EventArgs e)
        {
            try
            {
                Helper helper = new Helper();

                string iconsPath = helper.GetPath()[3];

                if (Icon != null)
                {
                    Icon.Dispose();
                    Icon = null;
                }

                if (secureFileType == "PDF")
                {
                    Icon = new Icon(iconsPath + "PdfXpress.ico");
                }
                else
                {
                    Icon = new Icon(iconsPath + "ImagXpress.ico");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo.ProcessorForms
{
    public partial class RemoveDustForm : Form
    {
        public RemoveDustForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();

        private bool mouseDown;

        public ImageX CurrentImage
        {
            set
            {
                imageXViewCurrent.Image = value;
            }
        }

        public ImageX PreviewImage
        {
            get
            {
                return imageXViewPreview.Image;
            }
        }

        private void ProcessorForm_Load(object sender, EventArgs e)
        {
            try
            {
             
                
                unlock.UnlockControls(imagXpress1);


                comboBoxDefectType.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            PreviewButton_Click(sender, e);

            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
                
                processor1.Image = imageXViewCurrent.Image.Copy();

                processor1.SetArea(new RectangleF((float)imageXViewCurrent.Rubberband.Dimensions.Left, 
                    (float)imageXViewCurrent.Rubberband.Dimensions.Top, (float)imageXViewCurrent.Rubberband.Dimensions.Width, 
                    (float)imageXViewCurrent.Rubberband.Dimensions.Height));
                processor1.EnableArea = true;

                processor1.RemoveDust((int)thresholdBox.Value, (int)filterBox.Value, (int)smoothBox.Value, 
                    (DefectType)comboBoxDefectType.SelectedIndex);
                
                if (imageXViewPreview != null)
                {
                    imageXViewPreview.Image = processor1.Image;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void imageXViewCurrent_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                mouseDown = true;

                imageXViewCurrent.Rubberband.Stop();
                imageXViewCurrent.Rubberband.Clear();

                imageXViewCurrent.Rubberband.Start(e.Location);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void imageXViewCurrent_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (mouseDown == true)
                {
                    imageXViewCurrent.Rubberband.Update(e.Location);
                }
            }
            catch (Exception) { };
        }

        private void imageXViewCurrent_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void HelpProcessorButton_Click(object sender, EventArgs e)
        {
            Helper help = new Helper();

            help.ShowHelp(this, Text);
        }
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace ImagXpressDemo
{
    public partial class PaletteForm : Form
    {
        public PaletteForm()
        {
            InitializeComponent();
        }

        private Color[] entries;

        private ColorPalette colorPalette;
        
        private Button[] colorButtons;

        private ColorDialog colorDialog;

        public ColorPalette palette
        {
            get
            {
                return colorPalette;
            }
            set
            {
                colorPalette = value;
            }
        }
        
        private void PaletteForm_Load(object sender, EventArgs e)
        {
            try
            {
                colorLabel.Text = "No color chosen.";

                colorDialog = new ColorDialog();

                entries = colorPalette.Entries;

                //initialize array of Buttons
                colorButtons = new Button[entries.Length];

                int height = 2;
                int j = 0;
                int spacer = 2;

                for (int i = 0; i < entries.Length; i++)
                {
                    //create new button, set size, set flatstyle
                    colorButtons[i] = new Button();
                    colorButtons[i].Width = 25;
                    colorButtons[i].Height = 25;
                    colorButtons[i].FlatStyle = FlatStyle.Flat;

                    //set backcolor to be palette color entry
                    colorButtons[i].BackColor = entries[i];

                    if (j == 16)
                    {
                        height += colorButtons[i].Height + spacer;
                        j = 0;
                    }

                    colorButtons[i].Location = new Point(j * (colorButtons[i].Width + spacer) + spacer, height);

                    j += 1;

                    //add button
                    this.Controls.Add(colorButtons[i]);

                    //add event handlers for button
                    colorButtons[i].MouseMove += new MouseEventHandler(button_MouseMove);
                    colorButtons[i].MouseClick += new MouseEventHandler(button_Click);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                for (int i = 0; i < colorButtons.Length; i++)
                {
                    if (((Button)sender).BackColor == colorButtons[i].BackColor)
                    {
                        //update label with color
                        colorLabel.Text = "{R: " + colorButtons[i].BackColor.R.ToString() + " G: " + colorButtons[i].BackColor.G.ToString()
                            + " B: " + colorButtons[i].BackColor.B.ToString() + "}";

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_Click(object sender, MouseEventArgs e)
        {
            try
            {
                for (int i = 0; i < colorButtons.Length; i++)
                {
                    if (((Button)sender).BackColor == colorButtons[i].BackColor)
                    {
                        //set the Color chosen of the ColorDialog
                        colorDialog.Color = colorButtons[i].BackColor;

                        if (colorDialog.ShowDialog() == DialogResult.OK)
                        {
                            //update the button's backColor
                            colorButtons[i].BackColor = colorDialog.Color;

                            //update the Color array with the Color chosen
                            entries[i] = colorDialog.Color;
                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }
    }
}
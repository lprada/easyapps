/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class SaveOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveOptionsForm));
            this.RenderLabel = new System.Windows.Forms.Label();
            this.renderIntentComboBox = new System.Windows.Forms.ComboBox();
            this.DCXGroupBox = new System.Windows.Forms.GroupBox();
            this.DCXMultiCheckBox = new System.Windows.Forms.CheckBox();
            this.EXIFGroupBox = new System.Windows.Forms.GroupBox();
            this.ThumbLabel = new System.Windows.Forms.Label();
            this.ThumbComboBox = new System.Windows.Forms.ComboBox();
            this.GIFGroupBox = new System.Windows.Forms.GroupBox();
            this.transparencyColorButton = new System.Windows.Forms.Button();
            this.backColorLabel = new System.Windows.Forms.Label();
            this.MatchLabel = new System.Windows.Forms.Label();
            this.MatchGifComboBox = new System.Windows.Forms.ComboBox();
            this.GIFLabel = new System.Windows.Forms.Label();
            this.GIFComboBox = new System.Windows.Forms.ComboBox();
            this.InterlacedGIFCheckBox = new System.Windows.Forms.CheckBox();
            this.HDPGroupBox = new System.Windows.Forms.GroupBox();
            this.QuantizationLabel = new System.Windows.Forms.Label();
            this.FrequencyLabel = new System.Windows.Forms.Label();
            this.QuantizationHDPBox = new System.Windows.Forms.NumericUpDown();
            this.FrequencyBox = new System.Windows.Forms.NumericUpDown();
            this.ChromaLabel = new System.Windows.Forms.Label();
            this.ChromaComboBox = new System.Windows.Forms.ComboBox();
            this.ICONGroupBox = new System.Windows.Forms.GroupBox();
            this.ICONMultiCheckBox = new System.Windows.Forms.CheckBox();
            this.InternetGroupBox = new System.Windows.Forms.GroupBox();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.InternetPasswordTextBox = new System.Windows.Forms.TextBox();
            this.DomainLabel = new System.Windows.Forms.Label();
            this.DomainTextBox = new System.Windows.Forms.TextBox();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.FTPLabel = new System.Windows.Forms.Label();
            this.UserNameTextBox = new System.Windows.Forms.TextBox();
            this.FTPComboBox = new System.Windows.Forms.ComboBox();
            this.JBIGGroupBox = new System.Windows.Forms.GroupBox();
            this.LooseLabel = new System.Windows.Forms.Label();
            this.InvertCheckBox = new System.Windows.Forms.CheckBox();
            this.looseBox = new System.Windows.Forms.NumericUpDown();
            this.FileOrgLabel = new System.Windows.Forms.Label();
            this.FileOrgComboBox = new System.Windows.Forms.ComboBox();
            this.EncodeLabel = new System.Windows.Forms.Label();
            this.EncodeComboBox = new System.Windows.Forms.ComboBox();
            this.JLSGroupBox = new System.Windows.Forms.GroupBox();
            this.NearLabel = new System.Windows.Forms.Label();
            this.NearBox = new System.Windows.Forms.NumericUpDown();
            this.PointLabel = new System.Windows.Forms.Label();
            this.PointBox = new System.Windows.Forms.NumericUpDown();
            this.MaxLabel = new System.Windows.Forms.Label();
            this.MaxBox = new System.Windows.Forms.NumericUpDown();
            this.InterleaveLabel = new System.Windows.Forms.Label();
            this.InterleaveBox = new System.Windows.Forms.NumericUpDown();
            this.OKButton = new System.Windows.Forms.Button();
            this.JP2GroupBox = new System.Windows.Forms.GroupBox();
            this.JP2TypeLabel = new System.Windows.Forms.Label();
            this.JP2ComboBox = new System.Windows.Forms.ComboBox();
            this.TileHLabel = new System.Windows.Forms.Label();
            this.SNRLabel = new System.Windows.Forms.Label();
            this.TileHBox = new System.Windows.Forms.NumericUpDown();
            this.TileWLabel = new System.Windows.Forms.Label();
            this.TileWBox = new System.Windows.Forms.NumericUpDown();
            this.SNRBox = new System.Windows.Forms.NumericUpDown();
            this.OrderLabel = new System.Windows.Forms.Label();
            this.OrderComboBox = new System.Windows.Forms.ComboBox();
            this.GrayscaleJP2CheckBox = new System.Windows.Forms.CheckBox();
            this.CompressLabel = new System.Windows.Forms.Label();
            this.CompressBox = new System.Windows.Forms.NumericUpDown();
            this.JPGGroupBox = new System.Windows.Forms.GroupBox();
            this.ProgressiveCheckBox = new System.Windows.Forms.CheckBox();
            this.SubSamplingLabel = new System.Windows.Forms.Label();
            this.JPGSubComboBox = new System.Windows.Forms.ComboBox();
            this.LuminanceLabel = new System.Windows.Forms.Label();
            this.LuminanceBox = new System.Windows.Forms.NumericUpDown();
            this.GrayscaleJPGCheckBox = new System.Windows.Forms.CheckBox();
            this.CositedCheckBox = new System.Windows.Forms.CheckBox();
            this.ColorSpaceLabel = new System.Windows.Forms.Label();
            this.ColorSpaceJPGComboBox = new System.Windows.Forms.ComboBox();
            this.ChrominanceLabel = new System.Windows.Forms.Label();
            this.ChromBox = new System.Windows.Forms.NumericUpDown();
            this.LibraryLabel = new System.Windows.Forms.Label();
            this.LibraryBox = new System.Windows.Forms.NumericUpDown();
            this.LJPGroupBox = new System.Windows.Forms.GroupBox();
            this.comboBoxMethod = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPredictor = new System.Windows.Forms.Label();
            this.predictorBox = new System.Windows.Forms.NumericUpDown();
            this.labelOrder = new System.Windows.Forms.Label();
            this.LJPTypeLabel = new System.Windows.Forms.Label();
            this.LJPComboBox = new System.Windows.Forms.ComboBox();
            this.orderBox = new System.Windows.Forms.NumericUpDown();
            this.PICGroupBox = new System.Windows.Forms.GroupBox();
            this.PicLabel = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.PNGGroupBox = new System.Windows.Forms.GroupBox();
            this.transpColorButton = new System.Windows.Forms.Button();
            this.TranspColorLabel = new System.Windows.Forms.Label();
            this.MatchPNGLabel = new System.Windows.Forms.Label();
            this.PngMatchComboBox = new System.Windows.Forms.ComboBox();
            this.InterlacedPNGCheckBox = new System.Windows.Forms.CheckBox();
            this.PortableLabel = new System.Windows.Forms.Label();
            this.PortableComboBox = new System.Windows.Forms.ComboBox();
            this.targetButton = new System.Windows.Forms.Button();
            this.UseEmbedCheckBox = new System.Windows.Forms.CheckBox();
            this.WSQGroupBox = new System.Windows.Forms.GroupBox();
            this.QuantizationWSQLabel = new System.Windows.Forms.Label();
            this.QuantizationWSQBox = new System.Windows.Forms.NumericUpDown();
            this.WhiteLabel = new System.Windows.Forms.Label();
            this.WhiteBox = new System.Windows.Forms.NumericUpDown();
            this.BlackLabel = new System.Windows.Forms.Label();
            this.BlackBox = new System.Windows.Forms.NumericUpDown();
            this.TIFFGroupBox = new System.Windows.Forms.GroupBox();
            this.FastCheckBox = new System.Windows.Forms.CheckBox();
            this.CompressionLabel = new System.Windows.Forms.Label();
            this.CompressionComboBox = new System.Windows.Forms.ComboBox();
            this.RowsLabel = new System.Windows.Forms.Label();
            this.RowsBox = new System.Windows.Forms.NumericUpDown();
            this.MultiTiffCheckBox = new System.Windows.Forms.CheckBox();
            this.ColorspaceTIFFLabel = new System.Windows.Forms.Label();
            this.ColorSpaceTIFFcomboBox = new System.Windows.Forms.ComboBox();
            this.ByteOrderLabel = new System.Windows.Forms.Label();
            this.ByteOrderComboBox = new System.Windows.Forms.ComboBox();
            this.bufferCheckBox = new System.Windows.Forms.CheckBox();
            this.PortableGroupBox = new System.Windows.Forms.GroupBox();
            this.PDFGroupBox = new System.Windows.Forms.GroupBox();
            this.swapCheckBox = new System.Windows.Forms.CheckBox();
            this.PDFCompressionLabel = new System.Windows.Forms.Label();
            this.PDFCompressionComboBox = new System.Windows.Forms.ComboBox();
            this.PDFMultiCheckBox = new System.Windows.Forms.CheckBox();
            this.PDFColorSpaceLabel = new System.Windows.Forms.Label();
            this.PDFColorSpaceComboBox = new System.Windows.Forms.ComboBox();
            this.MetaCheckBox = new System.Windows.Forms.CheckBox();
            this.DCXGroupBox.SuspendLayout();
            this.EXIFGroupBox.SuspendLayout();
            this.GIFGroupBox.SuspendLayout();
            this.HDPGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuantizationHDPBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyBox)).BeginInit();
            this.ICONGroupBox.SuspendLayout();
            this.InternetGroupBox.SuspendLayout();
            this.JBIGGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.looseBox)).BeginInit();
            this.JLSGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NearBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InterleaveBox)).BeginInit();
            this.JP2GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TileHBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TileWBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNRBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompressBox)).BeginInit();
            this.JPGGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuminanceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChromBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LibraryBox)).BeginInit();
            this.LJPGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.predictorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBox)).BeginInit();
            this.PICGroupBox.SuspendLayout();
            this.PNGGroupBox.SuspendLayout();
            this.WSQGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuantizationWSQBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlackBox)).BeginInit();
            this.TIFFGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RowsBox)).BeginInit();
            this.PortableGroupBox.SuspendLayout();
            this.PDFGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // RenderLabel
            // 
            this.RenderLabel.AutoSize = true;
            this.RenderLabel.Location = new System.Drawing.Point(295, 19);
            this.RenderLabel.Name = "RenderLabel";
            this.RenderLabel.Size = new System.Drawing.Size(102, 13);
            this.RenderLabel.TabIndex = 3;
            this.RenderLabel.Text = "Color Render Intent:";
            // 
            // renderIntentComboBox
            // 
            this.renderIntentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.renderIntentComboBox.FormattingEnabled = true;
            this.renderIntentComboBox.Items.AddRange(new object[] {
            "Images",
            "Business",
            "Graphics",
            "AbsoluteColorMetric"});
            this.renderIntentComboBox.Location = new System.Drawing.Point(403, 16);
            this.renderIntentComboBox.Name = "renderIntentComboBox";
            this.renderIntentComboBox.Size = new System.Drawing.Size(102, 21);
            this.renderIntentComboBox.TabIndex = 4;
            // 
            // DCXGroupBox
            // 
            this.DCXGroupBox.Controls.Add(this.DCXMultiCheckBox);
            this.DCXGroupBox.Location = new System.Drawing.Point(584, 68);
            this.DCXGroupBox.Name = "DCXGroupBox";
            this.DCXGroupBox.Size = new System.Drawing.Size(156, 36);
            this.DCXGroupBox.TabIndex = 10;
            this.DCXGroupBox.TabStop = false;
            this.DCXGroupBox.Text = "DCX";
            // 
            // DCXMultiCheckBox
            // 
            this.DCXMultiCheckBox.AutoSize = true;
            this.DCXMultiCheckBox.Location = new System.Drawing.Point(46, 14);
            this.DCXMultiCheckBox.Name = "DCXMultiCheckBox";
            this.DCXMultiCheckBox.Size = new System.Drawing.Size(73, 17);
            this.DCXMultiCheckBox.TabIndex = 0;
            this.DCXMultiCheckBox.Text = "MultiPage";
            this.DCXMultiCheckBox.UseVisualStyleBackColor = true;
            // 
            // EXIFGroupBox
            // 
            this.EXIFGroupBox.Controls.Add(this.ThumbLabel);
            this.EXIFGroupBox.Controls.Add(this.ThumbComboBox);
            this.EXIFGroupBox.Location = new System.Drawing.Point(749, 338);
            this.EXIFGroupBox.Name = "EXIFGroupBox";
            this.EXIFGroupBox.Size = new System.Drawing.Size(187, 55);
            this.EXIFGroupBox.TabIndex = 21;
            this.EXIFGroupBox.TabStop = false;
            this.EXIFGroupBox.Text = "EXIF";
            // 
            // ThumbLabel
            // 
            this.ThumbLabel.Location = new System.Drawing.Point(6, 25);
            this.ThumbLabel.Name = "ThumbLabel";
            this.ThumbLabel.Size = new System.Drawing.Size(67, 27);
            this.ThumbLabel.TabIndex = 0;
            this.ThumbLabel.Text = "Thumbnail Size:";
            // 
            // ThumbComboBox
            // 
            this.ThumbComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ThumbComboBox.FormattingEnabled = true;
            this.ThumbComboBox.Items.AddRange(new object[] {
            "No thumbnail",
            "1/4",
            "1/16",
            "1/64",
            "1/256"});
            this.ThumbComboBox.Location = new System.Drawing.Point(79, 25);
            this.ThumbComboBox.Name = "ThumbComboBox";
            this.ThumbComboBox.Size = new System.Drawing.Size(93, 21);
            this.ThumbComboBox.TabIndex = 1;
            // 
            // GIFGroupBox
            // 
            this.GIFGroupBox.Controls.Add(this.transparencyColorButton);
            this.GIFGroupBox.Controls.Add(this.backColorLabel);
            this.GIFGroupBox.Controls.Add(this.MatchLabel);
            this.GIFGroupBox.Controls.Add(this.MatchGifComboBox);
            this.GIFGroupBox.Controls.Add(this.GIFLabel);
            this.GIFGroupBox.Controls.Add(this.GIFComboBox);
            this.GIFGroupBox.Controls.Add(this.InterlacedGIFCheckBox);
            this.GIFGroupBox.Location = new System.Drawing.Point(749, 200);
            this.GIFGroupBox.Name = "GIFGroupBox";
            this.GIFGroupBox.Size = new System.Drawing.Size(207, 131);
            this.GIFGroupBox.TabIndex = 17;
            this.GIFGroupBox.TabStop = false;
            this.GIFGroupBox.Text = "GIF";
            // 
            // transparencyColorButton
            // 
            this.transparencyColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.transparencyColorButton.Location = new System.Drawing.Point(120, 74);
            this.transparencyColorButton.Name = "transparencyColorButton";
            this.transparencyColorButton.Size = new System.Drawing.Size(28, 23);
            this.transparencyColorButton.TabIndex = 5;
            this.transparencyColorButton.UseVisualStyleBackColor = true;
            this.transparencyColorButton.Click += new System.EventHandler(this.transparencyColorButton_Click);
            // 
            // backColorLabel
            // 
            this.backColorLabel.AutoSize = true;
            this.backColorLabel.Location = new System.Drawing.Point(15, 79);
            this.backColorLabel.Name = "backColorLabel";
            this.backColorLabel.Size = new System.Drawing.Size(102, 13);
            this.backColorLabel.TabIndex = 4;
            this.backColorLabel.Text = "Transparency Color:";
            // 
            // MatchLabel
            // 
            this.MatchLabel.AutoSize = true;
            this.MatchLabel.Location = new System.Drawing.Point(12, 48);
            this.MatchLabel.Name = "MatchLabel";
            this.MatchLabel.Size = new System.Drawing.Size(108, 13);
            this.MatchLabel.TabIndex = 2;
            this.MatchLabel.Text = "Transparency Match:";
            // 
            // MatchGifComboBox
            // 
            this.MatchGifComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MatchGifComboBox.FormattingEnabled = true;
            this.MatchGifComboBox.Items.AddRange(new object[] {
            "None",
            "Closest",
            "Exact"});
            this.MatchGifComboBox.Location = new System.Drawing.Point(120, 45);
            this.MatchGifComboBox.Name = "MatchGifComboBox";
            this.MatchGifComboBox.Size = new System.Drawing.Size(73, 21);
            this.MatchGifComboBox.TabIndex = 3;
            // 
            // GIFLabel
            // 
            this.GIFLabel.AutoSize = true;
            this.GIFLabel.Location = new System.Drawing.Point(83, 21);
            this.GIFLabel.Name = "GIFLabel";
            this.GIFLabel.Size = new System.Drawing.Size(34, 13);
            this.GIFLabel.TabIndex = 0;
            this.GIFLabel.Text = "Type:";
            // 
            // GIFComboBox
            // 
            this.GIFComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GIFComboBox.FormattingEnabled = true;
            this.GIFComboBox.Items.AddRange(new object[] {
            "Gif89a",
            "Gif87a"});
            this.GIFComboBox.Location = new System.Drawing.Point(120, 18);
            this.GIFComboBox.Name = "GIFComboBox";
            this.GIFComboBox.Size = new System.Drawing.Size(73, 21);
            this.GIFComboBox.TabIndex = 1;
            // 
            // InterlacedGIFCheckBox
            // 
            this.InterlacedGIFCheckBox.AutoSize = true;
            this.InterlacedGIFCheckBox.Location = new System.Drawing.Point(75, 108);
            this.InterlacedGIFCheckBox.Name = "InterlacedGIFCheckBox";
            this.InterlacedGIFCheckBox.Size = new System.Drawing.Size(73, 17);
            this.InterlacedGIFCheckBox.TabIndex = 6;
            this.InterlacedGIFCheckBox.Text = "Interlaced";
            this.InterlacedGIFCheckBox.UseVisualStyleBackColor = true;
            // 
            // HDPGroupBox
            // 
            this.HDPGroupBox.Controls.Add(this.QuantizationLabel);
            this.HDPGroupBox.Controls.Add(this.FrequencyLabel);
            this.HDPGroupBox.Controls.Add(this.QuantizationHDPBox);
            this.HDPGroupBox.Controls.Add(this.FrequencyBox);
            this.HDPGroupBox.Controls.Add(this.ChromaLabel);
            this.HDPGroupBox.Controls.Add(this.ChromaComboBox);
            this.HDPGroupBox.Location = new System.Drawing.Point(219, 68);
            this.HDPGroupBox.Name = "HDPGroupBox";
            this.HDPGroupBox.Size = new System.Drawing.Size(324, 73);
            this.HDPGroupBox.TabIndex = 9;
            this.HDPGroupBox.TabStop = false;
            this.HDPGroupBox.Text = "HD Photo";
            // 
            // QuantizationLabel
            // 
            this.QuantizationLabel.AutoSize = true;
            this.QuantizationLabel.Location = new System.Drawing.Point(176, 48);
            this.QuantizationLabel.Name = "QuantizationLabel";
            this.QuantizationLabel.Size = new System.Drawing.Size(69, 13);
            this.QuantizationLabel.TabIndex = 4;
            this.QuantizationLabel.Text = "Quantization:";
            // 
            // FrequencyLabel
            // 
            this.FrequencyLabel.AutoSize = true;
            this.FrequencyLabel.Location = new System.Drawing.Point(6, 51);
            this.FrequencyLabel.Name = "FrequencyLabel";
            this.FrequencyLabel.Size = new System.Drawing.Size(89, 13);
            this.FrequencyLabel.TabIndex = 2;
            this.FrequencyLabel.Text = "Frequency Order:";
            // 
            // QuantizationHDPBox
            // 
            this.QuantizationHDPBox.Location = new System.Drawing.Point(249, 46);
            this.QuantizationHDPBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.QuantizationHDPBox.Name = "QuantizationHDPBox";
            this.QuantizationHDPBox.Size = new System.Drawing.Size(69, 20);
            this.QuantizationHDPBox.TabIndex = 5;
            this.QuantizationHDPBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // FrequencyBox
            // 
            this.FrequencyBox.Location = new System.Drawing.Point(101, 44);
            this.FrequencyBox.Name = "FrequencyBox";
            this.FrequencyBox.Size = new System.Drawing.Size(69, 20);
            this.FrequencyBox.TabIndex = 3;
            // 
            // ChromaLabel
            // 
            this.ChromaLabel.Location = new System.Drawing.Point(9, 17);
            this.ChromaLabel.Name = "ChromaLabel";
            this.ChromaLabel.Size = new System.Drawing.Size(86, 34);
            this.ChromaLabel.TabIndex = 0;
            this.ChromaLabel.Text = "Chroma Subsampling:";
            // 
            // ChromaComboBox
            // 
            this.ChromaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ChromaComboBox.FormattingEnabled = true;
            this.ChromaComboBox.Items.AddRange(new object[] {
            "No Color (4:0:0)",
            "2 to 1 Vertical Subsampling (4:2:0)",
            "2 to 1 Horizontal Subsampling (4:2:2)",
            "No Subsampling (4:4:4)"});
            this.ChromaComboBox.Location = new System.Drawing.Point(101, 14);
            this.ChromaComboBox.Name = "ChromaComboBox";
            this.ChromaComboBox.Size = new System.Drawing.Size(209, 21);
            this.ChromaComboBox.TabIndex = 1;
            // 
            // ICONGroupBox
            // 
            this.ICONGroupBox.Controls.Add(this.ICONMultiCheckBox);
            this.ICONGroupBox.Location = new System.Drawing.Point(584, 109);
            this.ICONGroupBox.Name = "ICONGroupBox";
            this.ICONGroupBox.Size = new System.Drawing.Size(156, 37);
            this.ICONGroupBox.TabIndex = 11;
            this.ICONGroupBox.TabStop = false;
            this.ICONGroupBox.Text = "ICON";
            // 
            // ICONMultiCheckBox
            // 
            this.ICONMultiCheckBox.AutoSize = true;
            this.ICONMultiCheckBox.Location = new System.Drawing.Point(46, 13);
            this.ICONMultiCheckBox.Name = "ICONMultiCheckBox";
            this.ICONMultiCheckBox.Size = new System.Drawing.Size(73, 17);
            this.ICONMultiCheckBox.TabIndex = 0;
            this.ICONMultiCheckBox.Text = "MultiPage";
            this.ICONMultiCheckBox.UseVisualStyleBackColor = true;
            // 
            // InternetGroupBox
            // 
            this.InternetGroupBox.Controls.Add(this.PasswordLabel);
            this.InternetGroupBox.Controls.Add(this.InternetPasswordTextBox);
            this.InternetGroupBox.Controls.Add(this.DomainLabel);
            this.InternetGroupBox.Controls.Add(this.DomainTextBox);
            this.InternetGroupBox.Controls.Add(this.UserNameLabel);
            this.InternetGroupBox.Controls.Add(this.FTPLabel);
            this.InternetGroupBox.Controls.Add(this.UserNameTextBox);
            this.InternetGroupBox.Controls.Add(this.FTPComboBox);
            this.InternetGroupBox.Location = new System.Drawing.Point(584, 165);
            this.InternetGroupBox.Name = "InternetGroupBox";
            this.InternetGroupBox.Size = new System.Drawing.Size(156, 137);
            this.InternetGroupBox.TabIndex = 15;
            this.InternetGroupBox.TabStop = false;
            this.InternetGroupBox.Text = "Internet";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(1, 83);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(56, 13);
            this.PasswordLabel.TabIndex = 4;
            this.PasswordLabel.Text = "Password:";
            // 
            // InternetPasswordTextBox
            // 
            this.InternetPasswordTextBox.Location = new System.Drawing.Point(74, 80);
            this.InternetPasswordTextBox.Name = "InternetPasswordTextBox";
            this.InternetPasswordTextBox.Size = new System.Drawing.Size(71, 20);
            this.InternetPasswordTextBox.TabIndex = 5;
            // 
            // DomainLabel
            // 
            this.DomainLabel.AutoSize = true;
            this.DomainLabel.Location = new System.Drawing.Point(1, 106);
            this.DomainLabel.Name = "DomainLabel";
            this.DomainLabel.Size = new System.Drawing.Size(46, 13);
            this.DomainLabel.TabIndex = 6;
            this.DomainLabel.Text = "Domain:";
            // 
            // DomainTextBox
            // 
            this.DomainTextBox.Location = new System.Drawing.Point(74, 106);
            this.DomainTextBox.Name = "DomainTextBox";
            this.DomainTextBox.Size = new System.Drawing.Size(71, 20);
            this.DomainTextBox.TabIndex = 7;
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Location = new System.Drawing.Point(1, 57);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(63, 13);
            this.UserNameLabel.TabIndex = 2;
            this.UserNameLabel.Text = "User Name:";
            // 
            // FTPLabel
            // 
            this.FTPLabel.AutoSize = true;
            this.FTPLabel.Location = new System.Drawing.Point(1, 23);
            this.FTPLabel.Name = "FTPLabel";
            this.FTPLabel.Size = new System.Drawing.Size(60, 13);
            this.FTPLabel.TabIndex = 0;
            this.FTPLabel.Text = "FTP Mode:";
            // 
            // UserNameTextBox
            // 
            this.UserNameTextBox.Location = new System.Drawing.Point(74, 54);
            this.UserNameTextBox.Name = "UserNameTextBox";
            this.UserNameTextBox.Size = new System.Drawing.Size(71, 20);
            this.UserNameTextBox.TabIndex = 3;
            // 
            // FTPComboBox
            // 
            this.FTPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FTPComboBox.FormattingEnabled = true;
            this.FTPComboBox.Items.AddRange(new object[] {
            "Active",
            "Passive"});
            this.FTPComboBox.Location = new System.Drawing.Point(74, 19);
            this.FTPComboBox.Name = "FTPComboBox";
            this.FTPComboBox.Size = new System.Drawing.Size(71, 21);
            this.FTPComboBox.TabIndex = 1;
            // 
            // JBIGGroupBox
            // 
            this.JBIGGroupBox.Controls.Add(this.LooseLabel);
            this.JBIGGroupBox.Controls.Add(this.InvertCheckBox);
            this.JBIGGroupBox.Controls.Add(this.looseBox);
            this.JBIGGroupBox.Controls.Add(this.FileOrgLabel);
            this.JBIGGroupBox.Controls.Add(this.FileOrgComboBox);
            this.JBIGGroupBox.Controls.Add(this.EncodeLabel);
            this.JBIGGroupBox.Controls.Add(this.EncodeComboBox);
            this.JBIGGroupBox.Location = new System.Drawing.Point(219, 147);
            this.JBIGGroupBox.Name = "JBIGGroupBox";
            this.JBIGGroupBox.Size = new System.Drawing.Size(359, 98);
            this.JBIGGroupBox.TabIndex = 14;
            this.JBIGGroupBox.TabStop = false;
            this.JBIGGroupBox.Text = "JBIG2";
            // 
            // LooseLabel
            // 
            this.LooseLabel.AutoSize = true;
            this.LooseLabel.Location = new System.Drawing.Point(19, 76);
            this.LooseLabel.Name = "LooseLabel";
            this.LooseLabel.Size = new System.Drawing.Size(124, 13);
            this.LooseLabel.TabIndex = 4;
            this.LooseLabel.Text = "Looseness Compression:";
            // 
            // InvertCheckBox
            // 
            this.InvertCheckBox.AutoSize = true;
            this.InvertCheckBox.Location = new System.Drawing.Point(251, 75);
            this.InvertCheckBox.Name = "InvertCheckBox";
            this.InvertCheckBox.Size = new System.Drawing.Size(102, 17);
            this.InvertCheckBox.TabIndex = 6;
            this.InvertCheckBox.Text = "Inverted Region";
            this.InvertCheckBox.UseVisualStyleBackColor = true;
            // 
            // looseBox
            // 
            this.looseBox.Location = new System.Drawing.Point(158, 74);
            this.looseBox.Name = "looseBox";
            this.looseBox.Size = new System.Drawing.Size(69, 20);
            this.looseBox.TabIndex = 5;
            // 
            // FileOrgLabel
            // 
            this.FileOrgLabel.AutoSize = true;
            this.FileOrgLabel.Location = new System.Drawing.Point(64, 47);
            this.FileOrgLabel.Name = "FileOrgLabel";
            this.FileOrgLabel.Size = new System.Drawing.Size(88, 13);
            this.FileOrgLabel.TabIndex = 2;
            this.FileOrgLabel.Text = "File Organization:";
            // 
            // FileOrgComboBox
            // 
            this.FileOrgComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FileOrgComboBox.FormattingEnabled = true;
            this.FileOrgComboBox.Items.AddRange(new object[] {
            "Default",
            "Sequential",
            "RandomAccess"});
            this.FileOrgComboBox.Location = new System.Drawing.Point(158, 43);
            this.FileOrgComboBox.Name = "FileOrgComboBox";
            this.FileOrgComboBox.Size = new System.Drawing.Size(195, 21);
            this.FileOrgComboBox.TabIndex = 3;
            // 
            // EncodeLabel
            // 
            this.EncodeLabel.AutoSize = true;
            this.EncodeLabel.Location = new System.Drawing.Point(12, 19);
            this.EncodeLabel.Name = "EncodeLabel";
            this.EncodeLabel.Size = new System.Drawing.Size(140, 13);
            this.EncodeLabel.TabIndex = 0;
            this.EncodeLabel.Text = "Encode Mode Compression:";
            // 
            // EncodeComboBox
            // 
            this.EncodeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EncodeComboBox.FormattingEnabled = true;
            this.EncodeComboBox.Items.AddRange(new object[] {
            "AutoDetect",
            "Lossless Generic MQ",
            "Lossless Generic Mmr",
            "Lossless Text Mq",
            "Lossless Text Mmr",
            "Lossless Text SpmMq",
            "Lossless Text SpmMmr",
            "Lossy Text Mq",
            "Lossy Text Mmr",
            "Lossy Halftone Mq",
            "Lossy Halftone Mmr"});
            this.EncodeComboBox.Location = new System.Drawing.Point(158, 16);
            this.EncodeComboBox.Name = "EncodeComboBox";
            this.EncodeComboBox.Size = new System.Drawing.Size(195, 21);
            this.EncodeComboBox.TabIndex = 1;
            // 
            // JLSGroupBox
            // 
            this.JLSGroupBox.Controls.Add(this.NearLabel);
            this.JLSGroupBox.Controls.Add(this.NearBox);
            this.JLSGroupBox.Controls.Add(this.PointLabel);
            this.JLSGroupBox.Controls.Add(this.PointBox);
            this.JLSGroupBox.Controls.Add(this.MaxLabel);
            this.JLSGroupBox.Controls.Add(this.MaxBox);
            this.JLSGroupBox.Controls.Add(this.InterleaveLabel);
            this.JLSGroupBox.Controls.Add(this.InterleaveBox);
            this.JLSGroupBox.Location = new System.Drawing.Point(8, 388);
            this.JLSGroupBox.Name = "JLSGroupBox";
            this.JLSGroupBox.Size = new System.Drawing.Size(205, 137);
            this.JLSGroupBox.TabIndex = 23;
            this.JLSGroupBox.TabStop = false;
            this.JLSGroupBox.Text = "JLS";
            // 
            // NearLabel
            // 
            this.NearLabel.Location = new System.Drawing.Point(23, 66);
            this.NearLabel.Name = "NearLabel";
            this.NearLabel.Size = new System.Drawing.Size(92, 39);
            this.NearLabel.TabIndex = 4;
            this.NearLabel.Text = "Error Tolerance for Near-Lossless Compression:";
            // 
            // NearBox
            // 
            this.NearBox.Location = new System.Drawing.Point(121, 76);
            this.NearBox.Name = "NearBox";
            this.NearBox.Size = new System.Drawing.Size(56, 20);
            this.NearBox.TabIndex = 5;
            // 
            // PointLabel
            // 
            this.PointLabel.AutoSize = true;
            this.PointLabel.Location = new System.Drawing.Point(15, 118);
            this.PointLabel.Name = "PointLabel";
            this.PointLabel.Size = new System.Drawing.Size(92, 13);
            this.PointLabel.TabIndex = 6;
            this.PointLabel.Text = "Precision of Point:";
            // 
            // PointBox
            // 
            this.PointBox.Location = new System.Drawing.Point(121, 111);
            this.PointBox.Name = "PointBox";
            this.PointBox.Size = new System.Drawing.Size(56, 20);
            this.PointBox.TabIndex = 7;
            // 
            // MaxLabel
            // 
            this.MaxLabel.AutoSize = true;
            this.MaxLabel.Location = new System.Drawing.Point(9, 42);
            this.MaxLabel.Name = "MaxLabel";
            this.MaxLabel.Size = new System.Drawing.Size(98, 13);
            this.MaxLabel.TabIndex = 2;
            this.MaxLabel.Text = "Max Value of Near:";
            // 
            // MaxBox
            // 
            this.MaxBox.Location = new System.Drawing.Point(121, 40);
            this.MaxBox.Name = "MaxBox";
            this.MaxBox.Size = new System.Drawing.Size(56, 20);
            this.MaxBox.TabIndex = 3;
            // 
            // InterleaveLabel
            // 
            this.InterleaveLabel.AutoSize = true;
            this.InterleaveLabel.Location = new System.Drawing.Point(47, 16);
            this.InterleaveLabel.Name = "InterleaveLabel";
            this.InterleaveLabel.Size = new System.Drawing.Size(57, 13);
            this.InterleaveLabel.TabIndex = 0;
            this.InterleaveLabel.Text = "Interleave:";
            // 
            // InterleaveBox
            // 
            this.InterleaveBox.Location = new System.Drawing.Point(121, 14);
            this.InterleaveBox.Name = "InterleaveBox";
            this.InterleaveBox.Size = new System.Drawing.Size(56, 20);
            this.InterleaveBox.TabIndex = 1;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(422, 543);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(107, 29);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // JP2GroupBox
            // 
            this.JP2GroupBox.Controls.Add(this.JP2TypeLabel);
            this.JP2GroupBox.Controls.Add(this.JP2ComboBox);
            this.JP2GroupBox.Controls.Add(this.TileHLabel);
            this.JP2GroupBox.Controls.Add(this.SNRLabel);
            this.JP2GroupBox.Controls.Add(this.TileHBox);
            this.JP2GroupBox.Controls.Add(this.TileWLabel);
            this.JP2GroupBox.Controls.Add(this.TileWBox);
            this.JP2GroupBox.Controls.Add(this.SNRBox);
            this.JP2GroupBox.Controls.Add(this.OrderLabel);
            this.JP2GroupBox.Controls.Add(this.OrderComboBox);
            this.JP2GroupBox.Controls.Add(this.GrayscaleJP2CheckBox);
            this.JP2GroupBox.Controls.Add(this.CompressLabel);
            this.JP2GroupBox.Controls.Add(this.CompressBox);
            this.JP2GroupBox.Location = new System.Drawing.Point(219, 250);
            this.JP2GroupBox.Name = "JP2GroupBox";
            this.JP2GroupBox.Size = new System.Drawing.Size(334, 195);
            this.JP2GroupBox.TabIndex = 19;
            this.JP2GroupBox.TabStop = false;
            this.JP2GroupBox.Text = "JPEG2000";
            // 
            // JP2TypeLabel
            // 
            this.JP2TypeLabel.AutoSize = true;
            this.JP2TypeLabel.Location = new System.Drawing.Point(49, 167);
            this.JP2TypeLabel.Name = "JP2TypeLabel";
            this.JP2TypeLabel.Size = new System.Drawing.Size(34, 13);
            this.JP2TypeLabel.TabIndex = 11;
            this.JP2TypeLabel.Text = "Type:";
            // 
            // JP2ComboBox
            // 
            this.JP2ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.JP2ComboBox.FormattingEnabled = true;
            this.JP2ComboBox.Items.AddRange(new object[] {
            "Lossy",
            "Lossless"});
            this.JP2ComboBox.Location = new System.Drawing.Point(117, 164);
            this.JP2ComboBox.Name = "JP2ComboBox";
            this.JP2ComboBox.Size = new System.Drawing.Size(174, 21);
            this.JP2ComboBox.TabIndex = 12;
            // 
            // TileHLabel
            // 
            this.TileHLabel.AutoSize = true;
            this.TileHLabel.Location = new System.Drawing.Point(43, 138);
            this.TileHLabel.Name = "TileHLabel";
            this.TileHLabel.Size = new System.Drawing.Size(61, 13);
            this.TileHLabel.TabIndex = 9;
            this.TileHLabel.Text = "Tile Height:";
            // 
            // SNRLabel
            // 
            this.SNRLabel.AutoSize = true;
            this.SNRLabel.Location = new System.Drawing.Point(45, 88);
            this.SNRLabel.Name = "SNRLabel";
            this.SNRLabel.Size = new System.Drawing.Size(61, 13);
            this.SNRLabel.TabIndex = 5;
            this.SNRLabel.Text = "Peak SNR:";
            // 
            // TileHBox
            // 
            this.TileHBox.Location = new System.Drawing.Point(117, 138);
            this.TileHBox.Name = "TileHBox";
            this.TileHBox.Size = new System.Drawing.Size(67, 20);
            this.TileHBox.TabIndex = 10;
            // 
            // TileWLabel
            // 
            this.TileWLabel.AutoSize = true;
            this.TileWLabel.Location = new System.Drawing.Point(43, 112);
            this.TileWLabel.Name = "TileWLabel";
            this.TileWLabel.Size = new System.Drawing.Size(58, 13);
            this.TileWLabel.TabIndex = 7;
            this.TileWLabel.Text = "Tile Width:";
            // 
            // TileWBox
            // 
            this.TileWBox.Location = new System.Drawing.Point(117, 112);
            this.TileWBox.Name = "TileWBox";
            this.TileWBox.Size = new System.Drawing.Size(67, 20);
            this.TileWBox.TabIndex = 8;
            // 
            // SNRBox
            // 
            this.SNRBox.Location = new System.Drawing.Point(117, 86);
            this.SNRBox.Name = "SNRBox";
            this.SNRBox.Size = new System.Drawing.Size(69, 20);
            this.SNRBox.TabIndex = 6;
            // 
            // OrderLabel
            // 
            this.OrderLabel.AutoSize = true;
            this.OrderLabel.Location = new System.Drawing.Point(12, 59);
            this.OrderLabel.Name = "OrderLabel";
            this.OrderLabel.Size = new System.Drawing.Size(94, 13);
            this.OrderLabel.TabIndex = 3;
            this.OrderLabel.Text = "Progression Order:";
            // 
            // OrderComboBox
            // 
            this.OrderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OrderComboBox.FormattingEnabled = true;
            this.OrderComboBox.Items.AddRange(new object[] {
            "Default",
            "Layer, Resolution, Component, Position",
            "Resolution, Layer, Component, Position",
            "Resolution, Position, Component, Layer",
            "Position, Component, Resolution, Layer",
            "Component, Position, Resolution, Layer"});
            this.OrderComboBox.Location = new System.Drawing.Point(117, 56);
            this.OrderComboBox.Name = "OrderComboBox";
            this.OrderComboBox.Size = new System.Drawing.Size(211, 21);
            this.OrderComboBox.TabIndex = 4;
            // 
            // GrayscaleJP2CheckBox
            // 
            this.GrayscaleJP2CheckBox.AutoSize = true;
            this.GrayscaleJP2CheckBox.Location = new System.Drawing.Point(204, 26);
            this.GrayscaleJP2CheckBox.Name = "GrayscaleJP2CheckBox";
            this.GrayscaleJP2CheckBox.Size = new System.Drawing.Size(73, 17);
            this.GrayscaleJP2CheckBox.TabIndex = 2;
            this.GrayscaleJP2CheckBox.Text = "Grayscale";
            this.GrayscaleJP2CheckBox.UseVisualStyleBackColor = true;
            // 
            // CompressLabel
            // 
            this.CompressLabel.AutoSize = true;
            this.CompressLabel.Location = new System.Drawing.Point(22, 26);
            this.CompressLabel.Name = "CompressLabel";
            this.CompressLabel.Size = new System.Drawing.Size(79, 13);
            this.CompressLabel.TabIndex = 0;
            this.CompressLabel.Text = "Compress Size:";
            // 
            // CompressBox
            // 
            this.CompressBox.Location = new System.Drawing.Point(117, 24);
            this.CompressBox.Name = "CompressBox";
            this.CompressBox.Size = new System.Drawing.Size(69, 20);
            this.CompressBox.TabIndex = 1;
            // 
            // JPGGroupBox
            // 
            this.JPGGroupBox.Controls.Add(this.ProgressiveCheckBox);
            this.JPGGroupBox.Controls.Add(this.SubSamplingLabel);
            this.JPGGroupBox.Controls.Add(this.JPGSubComboBox);
            this.JPGGroupBox.Controls.Add(this.LuminanceLabel);
            this.JPGGroupBox.Controls.Add(this.LuminanceBox);
            this.JPGGroupBox.Controls.Add(this.GrayscaleJPGCheckBox);
            this.JPGGroupBox.Controls.Add(this.CositedCheckBox);
            this.JPGGroupBox.Controls.Add(this.ColorSpaceLabel);
            this.JPGGroupBox.Controls.Add(this.ColorSpaceJPGComboBox);
            this.JPGGroupBox.Controls.Add(this.ChrominanceLabel);
            this.JPGGroupBox.Controls.Add(this.ChromBox);
            this.JPGGroupBox.Location = new System.Drawing.Point(562, 451);
            this.JPGGroupBox.Name = "JPGGroupBox";
            this.JPGGroupBox.Size = new System.Drawing.Size(346, 117);
            this.JPGGroupBox.TabIndex = 25;
            this.JPGGroupBox.TabStop = false;
            this.JPGGroupBox.Text = "JPEG";
            // 
            // ProgressiveCheckBox
            // 
            this.ProgressiveCheckBox.AutoSize = true;
            this.ProgressiveCheckBox.Location = new System.Drawing.Point(95, 67);
            this.ProgressiveCheckBox.Name = "ProgressiveCheckBox";
            this.ProgressiveCheckBox.Size = new System.Drawing.Size(81, 17);
            this.ProgressiveCheckBox.TabIndex = 7;
            this.ProgressiveCheckBox.Text = "Progressive";
            this.ProgressiveCheckBox.UseVisualStyleBackColor = true;
            // 
            // SubSamplingLabel
            // 
            this.SubSamplingLabel.AutoSize = true;
            this.SubSamplingLabel.Location = new System.Drawing.Point(12, 92);
            this.SubSamplingLabel.Name = "SubSamplingLabel";
            this.SubSamplingLabel.Size = new System.Drawing.Size(70, 13);
            this.SubSamplingLabel.TabIndex = 9;
            this.SubSamplingLabel.Text = "Subsampling:";
            // 
            // JPGSubComboBox
            // 
            this.JPGSubComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.JPGSubComboBox.FormattingEnabled = true;
            this.JPGSubComboBox.Items.AddRange(new object[] {
            "No subsampling (1:1:1)",
            "2 to 1 Horizontal Subsampling (2:1:1)",
            "2 to 1 Horizontal and Vertical Subsampling (4:1:1)",
            "2 to 1 Vertical Subsampling (2:1:1)"});
            this.JPGSubComboBox.Location = new System.Drawing.Point(88, 90);
            this.JPGSubComboBox.Name = "JPGSubComboBox";
            this.JPGSubComboBox.Size = new System.Drawing.Size(252, 21);
            this.JPGSubComboBox.TabIndex = 10;
            // 
            // LuminanceLabel
            // 
            this.LuminanceLabel.AutoSize = true;
            this.LuminanceLabel.Location = new System.Drawing.Point(182, 16);
            this.LuminanceLabel.Name = "LuminanceLabel";
            this.LuminanceLabel.Size = new System.Drawing.Size(62, 13);
            this.LuminanceLabel.TabIndex = 2;
            this.LuminanceLabel.Text = "Luminance:";
            // 
            // LuminanceBox
            // 
            this.LuminanceBox.Location = new System.Drawing.Point(247, 14);
            this.LuminanceBox.Name = "LuminanceBox";
            this.LuminanceBox.Size = new System.Drawing.Size(69, 20);
            this.LuminanceBox.TabIndex = 3;
            // 
            // GrayscaleJPGCheckBox
            // 
            this.GrayscaleJPGCheckBox.AutoSize = true;
            this.GrayscaleJPGCheckBox.Location = new System.Drawing.Point(183, 67);
            this.GrayscaleJPGCheckBox.Name = "GrayscaleJPGCheckBox";
            this.GrayscaleJPGCheckBox.Size = new System.Drawing.Size(73, 17);
            this.GrayscaleJPGCheckBox.TabIndex = 8;
            this.GrayscaleJPGCheckBox.Text = "Grayscale";
            this.GrayscaleJPGCheckBox.UseVisualStyleBackColor = true;
            // 
            // CositedCheckBox
            // 
            this.CositedCheckBox.AutoSize = true;
            this.CositedCheckBox.Location = new System.Drawing.Point(18, 67);
            this.CositedCheckBox.Name = "CositedCheckBox";
            this.CositedCheckBox.Size = new System.Drawing.Size(61, 17);
            this.CositedCheckBox.TabIndex = 6;
            this.CositedCheckBox.Text = "Cosited";
            this.CositedCheckBox.UseVisualStyleBackColor = true;
            // 
            // ColorSpaceLabel
            // 
            this.ColorSpaceLabel.AutoSize = true;
            this.ColorSpaceLabel.Location = new System.Drawing.Point(11, 43);
            this.ColorSpaceLabel.Name = "ColorSpaceLabel";
            this.ColorSpaceLabel.Size = new System.Drawing.Size(68, 13);
            this.ColorSpaceLabel.TabIndex = 4;
            this.ColorSpaceLabel.Text = "Color Space:";
            // 
            // ColorSpaceJPGComboBox
            // 
            this.ColorSpaceJPGComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ColorSpaceJPGComboBox.FormattingEnabled = true;
            this.ColorSpaceJPGComboBox.Items.AddRange(new object[] {
            "RGB",
            "CMYK"});
            this.ColorSpaceJPGComboBox.Location = new System.Drawing.Point(102, 40);
            this.ColorSpaceJPGComboBox.Name = "ColorSpaceJPGComboBox";
            this.ColorSpaceJPGComboBox.Size = new System.Drawing.Size(121, 21);
            this.ColorSpaceJPGComboBox.TabIndex = 5;
            this.ColorSpaceJPGComboBox.SelectedIndexChanged += new System.EventHandler(this.ColorSpaceJPGComboBox_SelectedIndexChanged);
            // 
            // ChrominanceLabel
            // 
            this.ChrominanceLabel.AutoSize = true;
            this.ChrominanceLabel.Location = new System.Drawing.Point(7, 16);
            this.ChrominanceLabel.Name = "ChrominanceLabel";
            this.ChrominanceLabel.Size = new System.Drawing.Size(72, 13);
            this.ChrominanceLabel.TabIndex = 0;
            this.ChrominanceLabel.Text = "Chrominance:";
            // 
            // ChromBox
            // 
            this.ChromBox.Location = new System.Drawing.Point(102, 14);
            this.ChromBox.Name = "ChromBox";
            this.ChromBox.Size = new System.Drawing.Size(69, 20);
            this.ChromBox.TabIndex = 1;
            // 
            // LibraryLabel
            // 
            this.LibraryLabel.AutoSize = true;
            this.LibraryLabel.Location = new System.Drawing.Point(32, 19);
            this.LibraryLabel.Name = "LibraryLabel";
            this.LibraryLabel.Size = new System.Drawing.Size(175, 13);
            this.LibraryLabel.TabIndex = 1;
            this.LibraryLabel.Text = "Library Number of Threads Allowed:";
            // 
            // LibraryBox
            // 
            this.LibraryBox.Location = new System.Drawing.Point(216, 15);
            this.LibraryBox.Name = "LibraryBox";
            this.LibraryBox.Size = new System.Drawing.Size(67, 20);
            this.LibraryBox.TabIndex = 2;
            // 
            // LJPGroupBox
            // 
            this.LJPGroupBox.Controls.Add(this.comboBoxMethod);
            this.LJPGroupBox.Controls.Add(this.label2);
            this.LJPGroupBox.Controls.Add(this.labelPredictor);
            this.LJPGroupBox.Controls.Add(this.predictorBox);
            this.LJPGroupBox.Controls.Add(this.labelOrder);
            this.LJPGroupBox.Controls.Add(this.LJPTypeLabel);
            this.LJPGroupBox.Controls.Add(this.LJPComboBox);
            this.LJPGroupBox.Controls.Add(this.orderBox);
            this.LJPGroupBox.Location = new System.Drawing.Point(750, 68);
            this.LJPGroupBox.Name = "LJPGroupBox";
            this.LJPGroupBox.Size = new System.Drawing.Size(174, 126);
            this.LJPGroupBox.TabIndex = 13;
            this.LJPGroupBox.TabStop = false;
            this.LJPGroupBox.Text = "LJP";
            // 
            // comboBoxMethod
            // 
            this.comboBoxMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMethod.FormattingEnabled = true;
            this.comboBoxMethod.Items.AddRange(new object[] {
            "Jpeg",
            "Loco",
            "Ppmd"});
            this.comboBoxMethod.Location = new System.Drawing.Point(61, 102);
            this.comboBoxMethod.Name = "comboBoxMethod";
            this.comboBoxMethod.Size = new System.Drawing.Size(101, 21);
            this.comboBoxMethod.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Method:";
            // 
            // labelPredictor
            // 
            this.labelPredictor.AutoSize = true;
            this.labelPredictor.Location = new System.Drawing.Point(35, 79);
            this.labelPredictor.Name = "labelPredictor";
            this.labelPredictor.Size = new System.Drawing.Size(52, 13);
            this.labelPredictor.TabIndex = 13;
            this.labelPredictor.Text = "Predictor:";
            // 
            // predictorBox
            // 
            this.predictorBox.Location = new System.Drawing.Point(93, 77);
            this.predictorBox.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.predictorBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.predictorBox.Name = "predictorBox";
            this.predictorBox.Size = new System.Drawing.Size(69, 20);
            this.predictorBox.TabIndex = 14;
            this.predictorBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelOrder
            // 
            this.labelOrder.AutoSize = true;
            this.labelOrder.Location = new System.Drawing.Point(51, 53);
            this.labelOrder.Name = "labelOrder";
            this.labelOrder.Size = new System.Drawing.Size(36, 13);
            this.labelOrder.TabIndex = 11;
            this.labelOrder.Text = "Order:";
            // 
            // LJPTypeLabel
            // 
            this.LJPTypeLabel.AutoSize = true;
            this.LJPTypeLabel.Location = new System.Drawing.Point(21, 27);
            this.LJPTypeLabel.Name = "LJPTypeLabel";
            this.LJPTypeLabel.Size = new System.Drawing.Size(34, 13);
            this.LJPTypeLabel.TabIndex = 0;
            this.LJPTypeLabel.Text = "Type:";
            // 
            // LJPComboBox
            // 
            this.LJPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LJPComboBox.FormattingEnabled = true;
            this.LJPComboBox.Items.AddRange(new object[] {
            "RGB",
            "YCbCr"});
            this.LJPComboBox.Location = new System.Drawing.Point(61, 24);
            this.LJPComboBox.Name = "LJPComboBox";
            this.LJPComboBox.Size = new System.Drawing.Size(101, 21);
            this.LJPComboBox.TabIndex = 1;
            // 
            // orderBox
            // 
            this.orderBox.Location = new System.Drawing.Point(93, 51);
            this.orderBox.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.orderBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.orderBox.Name = "orderBox";
            this.orderBox.Size = new System.Drawing.Size(69, 20);
            this.orderBox.TabIndex = 12;
            this.orderBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // PICGroupBox
            // 
            this.PICGroupBox.Controls.Add(this.PicLabel);
            this.PICGroupBox.Controls.Add(this.PasswordTextBox);
            this.PICGroupBox.Location = new System.Drawing.Point(749, 394);
            this.PICGroupBox.Name = "PICGroupBox";
            this.PICGroupBox.Size = new System.Drawing.Size(187, 46);
            this.PICGroupBox.TabIndex = 22;
            this.PICGroupBox.TabStop = false;
            this.PICGroupBox.Text = "PIC";
            // 
            // PicLabel
            // 
            this.PicLabel.AutoSize = true;
            this.PicLabel.Location = new System.Drawing.Point(17, 22);
            this.PicLabel.Name = "PicLabel";
            this.PicLabel.Size = new System.Drawing.Size(56, 13);
            this.PicLabel.TabIndex = 0;
            this.PicLabel.Text = "Password:";
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(75, 19);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(100, 20);
            this.PasswordTextBox.TabIndex = 1;
            // 
            // PNGGroupBox
            // 
            this.PNGGroupBox.Controls.Add(this.transpColorButton);
            this.PNGGroupBox.Controls.Add(this.TranspColorLabel);
            this.PNGGroupBox.Controls.Add(this.MatchPNGLabel);
            this.PNGGroupBox.Controls.Add(this.PngMatchComboBox);
            this.PNGGroupBox.Controls.Add(this.InterlacedPNGCheckBox);
            this.PNGGroupBox.Location = new System.Drawing.Point(219, 451);
            this.PNGGroupBox.Name = "PNGGroupBox";
            this.PNGGroupBox.Size = new System.Drawing.Size(334, 74);
            this.PNGGroupBox.TabIndex = 24;
            this.PNGGroupBox.TabStop = false;
            this.PNGGroupBox.Text = "PNG";
            // 
            // transpColorButton
            // 
            this.transpColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.transpColorButton.Location = new System.Drawing.Point(297, 45);
            this.transpColorButton.Name = "transpColorButton";
            this.transpColorButton.Size = new System.Drawing.Size(28, 23);
            this.transpColorButton.TabIndex = 4;
            this.transpColorButton.UseVisualStyleBackColor = true;
            this.transpColorButton.Click += new System.EventHandler(this.transpColorButton_Click);
            // 
            // TranspColorLabel
            // 
            this.TranspColorLabel.AutoSize = true;
            this.TranspColorLabel.Location = new System.Drawing.Point(189, 50);
            this.TranspColorLabel.Name = "TranspColorLabel";
            this.TranspColorLabel.Size = new System.Drawing.Size(102, 13);
            this.TranspColorLabel.TabIndex = 3;
            this.TranspColorLabel.Text = "Transparency Color:";
            // 
            // MatchPNGLabel
            // 
            this.MatchPNGLabel.Location = new System.Drawing.Point(4, 35);
            this.MatchPNGLabel.Name = "MatchPNGLabel";
            this.MatchPNGLabel.Size = new System.Drawing.Size(87, 31);
            this.MatchPNGLabel.TabIndex = 1;
            this.MatchPNGLabel.Text = "Transparency Match:";
            // 
            // PngMatchComboBox
            // 
            this.PngMatchComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PngMatchComboBox.FormattingEnabled = true;
            this.PngMatchComboBox.Items.AddRange(new object[] {
            "None",
            "Closest",
            "Exact"});
            this.PngMatchComboBox.Location = new System.Drawing.Point(96, 41);
            this.PngMatchComboBox.Name = "PngMatchComboBox";
            this.PngMatchComboBox.Size = new System.Drawing.Size(81, 21);
            this.PngMatchComboBox.TabIndex = 2;
            // 
            // InterlacedPNGCheckBox
            // 
            this.InterlacedPNGCheckBox.AutoSize = true;
            this.InterlacedPNGCheckBox.Location = new System.Drawing.Point(139, 16);
            this.InterlacedPNGCheckBox.Name = "InterlacedPNGCheckBox";
            this.InterlacedPNGCheckBox.Size = new System.Drawing.Size(73, 17);
            this.InterlacedPNGCheckBox.TabIndex = 0;
            this.InterlacedPNGCheckBox.Text = "Interlaced";
            this.InterlacedPNGCheckBox.UseVisualStyleBackColor = true;
            // 
            // PortableLabel
            // 
            this.PortableLabel.AutoSize = true;
            this.PortableLabel.Location = new System.Drawing.Point(8, 22);
            this.PortableLabel.Name = "PortableLabel";
            this.PortableLabel.Size = new System.Drawing.Size(49, 13);
            this.PortableLabel.TabIndex = 0;
            this.PortableLabel.Text = "Portable:";
            // 
            // PortableComboBox
            // 
            this.PortableComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PortableComboBox.FormattingEnabled = true;
            this.PortableComboBox.Items.AddRange(new object[] {
            "Binary",
            "Ascii"});
            this.PortableComboBox.Location = new System.Drawing.Point(63, 15);
            this.PortableComboBox.Name = "PortableComboBox";
            this.PortableComboBox.Size = new System.Drawing.Size(93, 21);
            this.PortableComboBox.TabIndex = 1;
            // 
            // targetButton
            // 
            this.targetButton.Location = new System.Drawing.Point(700, 16);
            this.targetButton.Name = "targetButton";
            this.targetButton.Size = new System.Drawing.Size(125, 27);
            this.targetButton.TabIndex = 7;
            this.targetButton.Text = "Target Profile Name";
            this.targetButton.UseVisualStyleBackColor = true;
            this.targetButton.Click += new System.EventHandler(this.targetButton_Click);
            // 
            // UseEmbedCheckBox
            // 
            this.UseEmbedCheckBox.AutoSize = true;
            this.UseEmbedCheckBox.Location = new System.Drawing.Point(32, 44);
            this.UseEmbedCheckBox.Name = "UseEmbedCheckBox";
            this.UseEmbedCheckBox.Size = new System.Drawing.Size(191, 17);
            this.UseEmbedCheckBox.TabIndex = 5;
            this.UseEmbedCheckBox.Text = "Use Embedded Color Management";
            this.UseEmbedCheckBox.UseVisualStyleBackColor = true;
            // 
            // WSQGroupBox
            // 
            this.WSQGroupBox.Controls.Add(this.QuantizationWSQLabel);
            this.WSQGroupBox.Controls.Add(this.QuantizationWSQBox);
            this.WSQGroupBox.Controls.Add(this.WhiteLabel);
            this.WSQGroupBox.Controls.Add(this.WhiteBox);
            this.WSQGroupBox.Controls.Add(this.BlackLabel);
            this.WSQGroupBox.Controls.Add(this.BlackBox);
            this.WSQGroupBox.Location = new System.Drawing.Point(559, 303);
            this.WSQGroupBox.Name = "WSQGroupBox";
            this.WSQGroupBox.Size = new System.Drawing.Size(184, 100);
            this.WSQGroupBox.TabIndex = 20;
            this.WSQGroupBox.TabStop = false;
            this.WSQGroupBox.Text = "WSQ";
            // 
            // QuantizationWSQLabel
            // 
            this.QuantizationWSQLabel.AutoSize = true;
            this.QuantizationWSQLabel.Location = new System.Drawing.Point(15, 77);
            this.QuantizationWSQLabel.Name = "QuantizationWSQLabel";
            this.QuantizationWSQLabel.Size = new System.Drawing.Size(69, 13);
            this.QuantizationWSQLabel.TabIndex = 4;
            this.QuantizationWSQLabel.Text = "Quantization:";
            // 
            // QuantizationWSQBox
            // 
            this.QuantizationWSQBox.Location = new System.Drawing.Point(90, 75);
            this.QuantizationWSQBox.Name = "QuantizationWSQBox";
            this.QuantizationWSQBox.Size = new System.Drawing.Size(69, 20);
            this.QuantizationWSQBox.TabIndex = 5;
            // 
            // WhiteLabel
            // 
            this.WhiteLabel.AutoSize = true;
            this.WhiteLabel.Location = new System.Drawing.Point(15, 51);
            this.WhiteLabel.Name = "WhiteLabel";
            this.WhiteLabel.Size = new System.Drawing.Size(38, 13);
            this.WhiteLabel.TabIndex = 2;
            this.WhiteLabel.Text = "White:";
            // 
            // WhiteBox
            // 
            this.WhiteBox.Location = new System.Drawing.Point(90, 49);
            this.WhiteBox.Name = "WhiteBox";
            this.WhiteBox.Size = new System.Drawing.Size(69, 20);
            this.WhiteBox.TabIndex = 3;
            // 
            // BlackLabel
            // 
            this.BlackLabel.AutoSize = true;
            this.BlackLabel.Location = new System.Drawing.Point(16, 25);
            this.BlackLabel.Name = "BlackLabel";
            this.BlackLabel.Size = new System.Drawing.Size(37, 13);
            this.BlackLabel.TabIndex = 0;
            this.BlackLabel.Text = "Black:";
            // 
            // BlackBox
            // 
            this.BlackBox.Location = new System.Drawing.Point(90, 23);
            this.BlackBox.Name = "BlackBox";
            this.BlackBox.Size = new System.Drawing.Size(69, 20);
            this.BlackBox.TabIndex = 1;
            // 
            // TIFFGroupBox
            // 
            this.TIFFGroupBox.Controls.Add(this.FastCheckBox);
            this.TIFFGroupBox.Controls.Add(this.CompressionLabel);
            this.TIFFGroupBox.Controls.Add(this.CompressionComboBox);
            this.TIFFGroupBox.Controls.Add(this.RowsLabel);
            this.TIFFGroupBox.Controls.Add(this.RowsBox);
            this.TIFFGroupBox.Controls.Add(this.MultiTiffCheckBox);
            this.TIFFGroupBox.Controls.Add(this.ColorspaceTIFFLabel);
            this.TIFFGroupBox.Controls.Add(this.ColorSpaceTIFFcomboBox);
            this.TIFFGroupBox.Controls.Add(this.ByteOrderLabel);
            this.TIFFGroupBox.Controls.Add(this.ByteOrderComboBox);
            this.TIFFGroupBox.Location = new System.Drawing.Point(8, 68);
            this.TIFFGroupBox.Name = "TIFFGroupBox";
            this.TIFFGroupBox.Size = new System.Drawing.Size(205, 177);
            this.TIFFGroupBox.TabIndex = 8;
            this.TIFFGroupBox.TabStop = false;
            this.TIFFGroupBox.Text = "TIFF";
            // 
            // FastCheckBox
            // 
            this.FastCheckBox.Location = new System.Drawing.Point(91, 126);
            this.FastCheckBox.Name = "FastCheckBox";
            this.FastCheckBox.Size = new System.Drawing.Size(108, 46);
            this.FastCheckBox.TabIndex = 11;
            this.FastCheckBox.Text = "Fast TIFF Write (MultiPage only) ";
            this.FastCheckBox.UseVisualStyleBackColor = true;
            // 
            // CompressionLabel
            // 
            this.CompressionLabel.AutoSize = true;
            this.CompressionLabel.Location = new System.Drawing.Point(10, 71);
            this.CompressionLabel.Name = "CompressionLabel";
            this.CompressionLabel.Size = new System.Drawing.Size(70, 13);
            this.CompressionLabel.TabIndex = 4;
            this.CompressionLabel.Text = "Compression:";
            // 
            // CompressionComboBox
            // 
            this.CompressionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CompressionComboBox.FormattingEnabled = true;
            this.CompressionComboBox.Items.AddRange(new object[] {
            "No Compression",
            "Rle",
            "Lzw",
            "PackBits",
            "Defalte",
            "Group3 1D",
            "Group3 2D",
            "Group4",
            "Jpeg",
            "Jpeg7"});
            this.CompressionComboBox.Location = new System.Drawing.Point(83, 68);
            this.CompressionComboBox.Name = "CompressionComboBox";
            this.CompressionComboBox.Size = new System.Drawing.Size(107, 21);
            this.CompressionComboBox.TabIndex = 5;
            // 
            // RowsLabel
            // 
            this.RowsLabel.AutoSize = true;
            this.RowsLabel.Location = new System.Drawing.Point(21, 98);
            this.RowsLabel.Name = "RowsLabel";
            this.RowsLabel.Size = new System.Drawing.Size(80, 13);
            this.RowsLabel.TabIndex = 6;
            this.RowsLabel.Text = "Rows Per Strip:";
            // 
            // RowsBox
            // 
            this.RowsBox.Location = new System.Drawing.Point(121, 96);
            this.RowsBox.Name = "RowsBox";
            this.RowsBox.Size = new System.Drawing.Size(69, 20);
            this.RowsBox.TabIndex = 7;
            // 
            // MultiTiffCheckBox
            // 
            this.MultiTiffCheckBox.AutoSize = true;
            this.MultiTiffCheckBox.Checked = true;
            this.MultiTiffCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MultiTiffCheckBox.Location = new System.Drawing.Point(5, 141);
            this.MultiTiffCheckBox.Name = "MultiTiffCheckBox";
            this.MultiTiffCheckBox.Size = new System.Drawing.Size(73, 17);
            this.MultiTiffCheckBox.TabIndex = 10;
            this.MultiTiffCheckBox.Text = "MultiPage";
            this.MultiTiffCheckBox.UseVisualStyleBackColor = true;
            this.MultiTiffCheckBox.CheckedChanged += new System.EventHandler(this.MultiTiffCheckBox_CheckedChanged);
            // 
            // ColorspaceTIFFLabel
            // 
            this.ColorspaceTIFFLabel.AutoSize = true;
            this.ColorspaceTIFFLabel.Location = new System.Drawing.Point(10, 44);
            this.ColorspaceTIFFLabel.Name = "ColorspaceTIFFLabel";
            this.ColorspaceTIFFLabel.Size = new System.Drawing.Size(68, 13);
            this.ColorspaceTIFFLabel.TabIndex = 2;
            this.ColorspaceTIFFLabel.Text = "Color Space:";
            // 
            // ColorSpaceTIFFcomboBox
            // 
            this.ColorSpaceTIFFcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ColorSpaceTIFFcomboBox.FormattingEnabled = true;
            this.ColorSpaceTIFFcomboBox.Items.AddRange(new object[] {
            "RGB",
            "CMYK"});
            this.ColorSpaceTIFFcomboBox.Location = new System.Drawing.Point(84, 41);
            this.ColorSpaceTIFFcomboBox.Name = "ColorSpaceTIFFcomboBox";
            this.ColorSpaceTIFFcomboBox.Size = new System.Drawing.Size(106, 21);
            this.ColorSpaceTIFFcomboBox.TabIndex = 3;
            this.ColorSpaceTIFFcomboBox.SelectedIndexChanged += new System.EventHandler(this.ColorSpaceTIFFcomboBox_SelectedIndexChanged);
            // 
            // ByteOrderLabel
            // 
            this.ByteOrderLabel.AutoSize = true;
            this.ByteOrderLabel.Location = new System.Drawing.Point(6, 17);
            this.ByteOrderLabel.Name = "ByteOrderLabel";
            this.ByteOrderLabel.Size = new System.Drawing.Size(60, 13);
            this.ByteOrderLabel.TabIndex = 0;
            this.ByteOrderLabel.Text = "Byte Order:";
            // 
            // ByteOrderComboBox
            // 
            this.ByteOrderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ByteOrderComboBox.FormattingEnabled = true;
            this.ByteOrderComboBox.Items.AddRange(new object[] {
            "Default",
            "Intel",
            "Motorola"});
            this.ByteOrderComboBox.Location = new System.Drawing.Point(84, 14);
            this.ByteOrderComboBox.Name = "ByteOrderComboBox";
            this.ByteOrderComboBox.Size = new System.Drawing.Size(106, 21);
            this.ByteOrderComboBox.TabIndex = 1;
            // 
            // bufferCheckBox
            // 
            this.bufferCheckBox.AutoSize = true;
            this.bufferCheckBox.Location = new System.Drawing.Point(235, 43);
            this.bufferCheckBox.Name = "bufferCheckBox";
            this.bufferCheckBox.Size = new System.Drawing.Size(77, 17);
            this.bufferCheckBox.TabIndex = 6;
            this.bufferCheckBox.Text = "Buffer FTP";
            this.bufferCheckBox.UseVisualStyleBackColor = true;
            // 
            // PortableGroupBox
            // 
            this.PortableGroupBox.Controls.Add(this.PortableLabel);
            this.PortableGroupBox.Controls.Add(this.PortableComboBox);
            this.PortableGroupBox.Location = new System.Drawing.Point(560, 405);
            this.PortableGroupBox.Name = "PortableGroupBox";
            this.PortableGroupBox.Size = new System.Drawing.Size(178, 43);
            this.PortableGroupBox.TabIndex = 12;
            this.PortableGroupBox.TabStop = false;
            this.PortableGroupBox.Text = "Portable";
            // 
            // PDFGroupBox
            // 
            this.PDFGroupBox.Controls.Add(this.swapCheckBox);
            this.PDFGroupBox.Controls.Add(this.PDFCompressionLabel);
            this.PDFGroupBox.Controls.Add(this.PDFCompressionComboBox);
            this.PDFGroupBox.Controls.Add(this.PDFMultiCheckBox);
            this.PDFGroupBox.Controls.Add(this.PDFColorSpaceLabel);
            this.PDFGroupBox.Controls.Add(this.PDFColorSpaceComboBox);
            this.PDFGroupBox.Location = new System.Drawing.Point(8, 257);
            this.PDFGroupBox.Name = "PDFGroupBox";
            this.PDFGroupBox.Size = new System.Drawing.Size(205, 125);
            this.PDFGroupBox.TabIndex = 18;
            this.PDFGroupBox.TabStop = false;
            this.PDFGroupBox.Text = "PDF";
            // 
            // swapCheckBox
            // 
            this.swapCheckBox.AutoSize = true;
            this.swapCheckBox.Location = new System.Drawing.Point(42, 101);
            this.swapCheckBox.Name = "swapCheckBox";
            this.swapCheckBox.Size = new System.Drawing.Size(135, 17);
            this.swapCheckBox.TabIndex = 5;
            this.swapCheckBox.Text = "Swap Black and White";
            this.swapCheckBox.UseVisualStyleBackColor = true;
            // 
            // PDFCompressionLabel
            // 
            this.PDFCompressionLabel.AutoSize = true;
            this.PDFCompressionLabel.Location = new System.Drawing.Point(5, 49);
            this.PDFCompressionLabel.Name = "PDFCompressionLabel";
            this.PDFCompressionLabel.Size = new System.Drawing.Size(70, 13);
            this.PDFCompressionLabel.TabIndex = 2;
            this.PDFCompressionLabel.Text = "Compression:";
            // 
            // PDFCompressionComboBox
            // 
            this.PDFCompressionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PDFCompressionComboBox.FormattingEnabled = true;
            this.PDFCompressionComboBox.Items.AddRange(new object[] {
            "No Compression",
            "Group3 1D",
            "Group3 2D",
            "Group4",
            "Jpeg",
            "Jbig2",
            "Jpeg2000"});
            this.PDFCompressionComboBox.Location = new System.Drawing.Point(78, 46);
            this.PDFCompressionComboBox.Name = "PDFCompressionComboBox";
            this.PDFCompressionComboBox.Size = new System.Drawing.Size(117, 21);
            this.PDFCompressionComboBox.TabIndex = 3;
            // 
            // PDFMultiCheckBox
            // 
            this.PDFMultiCheckBox.AutoSize = true;
            this.PDFMultiCheckBox.Checked = true;
            this.PDFMultiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PDFMultiCheckBox.Location = new System.Drawing.Point(42, 78);
            this.PDFMultiCheckBox.Name = "PDFMultiCheckBox";
            this.PDFMultiCheckBox.Size = new System.Drawing.Size(73, 17);
            this.PDFMultiCheckBox.TabIndex = 4;
            this.PDFMultiCheckBox.Text = "MultiPage";
            this.PDFMultiCheckBox.UseVisualStyleBackColor = true;
            // 
            // PDFColorSpaceLabel
            // 
            this.PDFColorSpaceLabel.AutoSize = true;
            this.PDFColorSpaceLabel.Location = new System.Drawing.Point(5, 22);
            this.PDFColorSpaceLabel.Name = "PDFColorSpaceLabel";
            this.PDFColorSpaceLabel.Size = new System.Drawing.Size(68, 13);
            this.PDFColorSpaceLabel.TabIndex = 0;
            this.PDFColorSpaceLabel.Text = "Color Space:";
            // 
            // PDFColorSpaceComboBox
            // 
            this.PDFColorSpaceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PDFColorSpaceComboBox.FormattingEnabled = true;
            this.PDFColorSpaceComboBox.Items.AddRange(new object[] {
            "RGB",
            "CMYK"});
            this.PDFColorSpaceComboBox.Location = new System.Drawing.Point(79, 19);
            this.PDFColorSpaceComboBox.Name = "PDFColorSpaceComboBox";
            this.PDFColorSpaceComboBox.Size = new System.Drawing.Size(116, 21);
            this.PDFColorSpaceComboBox.TabIndex = 1;
            this.PDFColorSpaceComboBox.SelectedIndexChanged += new System.EventHandler(this.PDFColorSpaceComboBox_SelectedIndexChanged);
            // 
            // MetaCheckBox
            // 
            this.MetaCheckBox.AutoSize = true;
            this.MetaCheckBox.Checked = true;
            this.MetaCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MetaCheckBox.Location = new System.Drawing.Point(559, 19);
            this.MetaCheckBox.Name = "MetaCheckBox";
            this.MetaCheckBox.Size = new System.Drawing.Size(115, 17);
            this.MetaCheckBox.TabIndex = 26;
            this.MetaCheckBox.Text = "Save All MetaData";
            this.MetaCheckBox.UseVisualStyleBackColor = true;
            // 
            // SaveOptionsForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 607);
            this.ControlBox = false;
            this.Controls.Add(this.MetaCheckBox);
            this.Controls.Add(this.PDFGroupBox);
            this.Controls.Add(this.PortableGroupBox);
            this.Controls.Add(this.GIFGroupBox);
            this.Controls.Add(this.bufferCheckBox);
            this.Controls.Add(this.TIFFGroupBox);
            this.Controls.Add(this.WSQGroupBox);
            this.Controls.Add(this.UseEmbedCheckBox);
            this.Controls.Add(this.JPGGroupBox);
            this.Controls.Add(this.targetButton);
            this.Controls.Add(this.PNGGroupBox);
            this.Controls.Add(this.LJPGroupBox);
            this.Controls.Add(this.PICGroupBox);
            this.Controls.Add(this.LibraryLabel);
            this.Controls.Add(this.LibraryBox);
            this.Controls.Add(this.EXIFGroupBox);
            this.Controls.Add(this.JP2GroupBox);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.JLSGroupBox);
            this.Controls.Add(this.JBIGGroupBox);
            this.Controls.Add(this.InternetGroupBox);
            this.Controls.Add(this.ICONGroupBox);
            this.Controls.Add(this.HDPGroupBox);
            this.Controls.Add(this.DCXGroupBox);
            this.Controls.Add(this.RenderLabel);
            this.Controls.Add(this.renderIntentComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SaveOptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Save Options";
            this.Load += new System.EventHandler(this.SaveOptionsForm_Load);
            this.DCXGroupBox.ResumeLayout(false);
            this.DCXGroupBox.PerformLayout();
            this.EXIFGroupBox.ResumeLayout(false);
            this.GIFGroupBox.ResumeLayout(false);
            this.GIFGroupBox.PerformLayout();
            this.HDPGroupBox.ResumeLayout(false);
            this.HDPGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuantizationHDPBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyBox)).EndInit();
            this.ICONGroupBox.ResumeLayout(false);
            this.ICONGroupBox.PerformLayout();
            this.InternetGroupBox.ResumeLayout(false);
            this.InternetGroupBox.PerformLayout();
            this.JBIGGroupBox.ResumeLayout(false);
            this.JBIGGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.looseBox)).EndInit();
            this.JLSGroupBox.ResumeLayout(false);
            this.JLSGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NearBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PointBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InterleaveBox)).EndInit();
            this.JP2GroupBox.ResumeLayout(false);
            this.JP2GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TileHBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TileWBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SNRBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CompressBox)).EndInit();
            this.JPGGroupBox.ResumeLayout(false);
            this.JPGGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LuminanceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChromBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LibraryBox)).EndInit();
            this.LJPGroupBox.ResumeLayout(false);
            this.LJPGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.predictorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBox)).EndInit();
            this.PICGroupBox.ResumeLayout(false);
            this.PICGroupBox.PerformLayout();
            this.PNGGroupBox.ResumeLayout(false);
            this.PNGGroupBox.PerformLayout();
            this.WSQGroupBox.ResumeLayout(false);
            this.WSQGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuantizationWSQBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlackBox)).EndInit();
            this.TIFFGroupBox.ResumeLayout(false);
            this.TIFFGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RowsBox)).EndInit();
            this.PortableGroupBox.ResumeLayout(false);
            this.PortableGroupBox.PerformLayout();
            this.PDFGroupBox.ResumeLayout(false);
            this.PDFGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label RenderLabel;
        private System.Windows.Forms.ComboBox renderIntentComboBox;
        private System.Windows.Forms.GroupBox DCXGroupBox;
        private System.Windows.Forms.CheckBox DCXMultiCheckBox;
        private System.Windows.Forms.GroupBox EXIFGroupBox;
        private System.Windows.Forms.Label ThumbLabel;
        private System.Windows.Forms.ComboBox ThumbComboBox;
        private System.Windows.Forms.GroupBox GIFGroupBox;
        private System.Windows.Forms.CheckBox InterlacedGIFCheckBox;
        private System.Windows.Forms.Label MatchLabel;
        private System.Windows.Forms.ComboBox MatchGifComboBox;
        private System.Windows.Forms.Label GIFLabel;
        private System.Windows.Forms.ComboBox GIFComboBox;
        private System.Windows.Forms.Button transparencyColorButton;
        private System.Windows.Forms.Label backColorLabel;
        private System.Windows.Forms.GroupBox HDPGroupBox;
        private System.Windows.Forms.Label QuantizationLabel;
        private System.Windows.Forms.Label FrequencyLabel;
        private System.Windows.Forms.NumericUpDown QuantizationHDPBox;
        private System.Windows.Forms.NumericUpDown FrequencyBox;
        private System.Windows.Forms.Label ChromaLabel;
        private System.Windows.Forms.ComboBox ChromaComboBox;
        private System.Windows.Forms.GroupBox ICONGroupBox;
        private System.Windows.Forms.CheckBox ICONMultiCheckBox;
        private System.Windows.Forms.GroupBox InternetGroupBox;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.TextBox InternetPasswordTextBox;
        private System.Windows.Forms.Label DomainLabel;
        private System.Windows.Forms.TextBox DomainTextBox;
        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.Label FTPLabel;
        private System.Windows.Forms.TextBox UserNameTextBox;
        private System.Windows.Forms.ComboBox FTPComboBox;
        private System.Windows.Forms.GroupBox JBIGGroupBox;
        private System.Windows.Forms.Label EncodeLabel;
        private System.Windows.Forms.ComboBox EncodeComboBox;
        private System.Windows.Forms.Label LooseLabel;
        private System.Windows.Forms.CheckBox InvertCheckBox;
        private System.Windows.Forms.NumericUpDown looseBox;
        private System.Windows.Forms.Label FileOrgLabel;
        private System.Windows.Forms.ComboBox FileOrgComboBox;
        private System.Windows.Forms.GroupBox JLSGroupBox;
        private System.Windows.Forms.Label NearLabel;
        private System.Windows.Forms.NumericUpDown NearBox;
        private System.Windows.Forms.Label PointLabel;
        private System.Windows.Forms.NumericUpDown PointBox;
        private System.Windows.Forms.Label MaxLabel;
        private System.Windows.Forms.NumericUpDown MaxBox;
        private System.Windows.Forms.Label InterleaveLabel;
        private System.Windows.Forms.NumericUpDown InterleaveBox;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.GroupBox JP2GroupBox;
        private System.Windows.Forms.Label CompressLabel;
        private System.Windows.Forms.NumericUpDown CompressBox;
        private System.Windows.Forms.Label OrderLabel;
        private System.Windows.Forms.ComboBox OrderComboBox;
        private System.Windows.Forms.CheckBox GrayscaleJP2CheckBox;
        private System.Windows.Forms.Label SNRLabel;
        private System.Windows.Forms.NumericUpDown SNRBox;
        private System.Windows.Forms.Label TileHLabel;
        private System.Windows.Forms.NumericUpDown TileHBox;
        private System.Windows.Forms.Label TileWLabel;
        private System.Windows.Forms.NumericUpDown TileWBox;
        private System.Windows.Forms.Label JP2TypeLabel;
        private System.Windows.Forms.ComboBox JP2ComboBox;
        private System.Windows.Forms.GroupBox JPGGroupBox;
        private System.Windows.Forms.Label ColorSpaceLabel;
        private System.Windows.Forms.ComboBox ColorSpaceJPGComboBox;
        private System.Windows.Forms.Label ChrominanceLabel;
        private System.Windows.Forms.NumericUpDown ChromBox;
        private System.Windows.Forms.CheckBox GrayscaleJPGCheckBox;
        private System.Windows.Forms.CheckBox CositedCheckBox;
        private System.Windows.Forms.Label LuminanceLabel;
        private System.Windows.Forms.NumericUpDown LuminanceBox;
        private System.Windows.Forms.Label SubSamplingLabel;
        private System.Windows.Forms.ComboBox JPGSubComboBox;
        private System.Windows.Forms.CheckBox ProgressiveCheckBox;
        private System.Windows.Forms.Label LibraryLabel;
        private System.Windows.Forms.NumericUpDown LibraryBox;
        private System.Windows.Forms.GroupBox LJPGroupBox;
        private System.Windows.Forms.Label LJPTypeLabel;
        private System.Windows.Forms.ComboBox LJPComboBox;
        private System.Windows.Forms.GroupBox PICGroupBox;
        private System.Windows.Forms.Label PicLabel;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.GroupBox PNGGroupBox;
        private System.Windows.Forms.Button transpColorButton;
        private System.Windows.Forms.Label TranspColorLabel;
        private System.Windows.Forms.Label MatchPNGLabel;
        private System.Windows.Forms.ComboBox PngMatchComboBox;
        private System.Windows.Forms.CheckBox InterlacedPNGCheckBox;
        private System.Windows.Forms.Label PortableLabel;
        private System.Windows.Forms.ComboBox PortableComboBox;
        private System.Windows.Forms.Button targetButton;
        private System.Windows.Forms.CheckBox UseEmbedCheckBox;
        private System.Windows.Forms.GroupBox WSQGroupBox;
        private System.Windows.Forms.Label QuantizationWSQLabel;
        private System.Windows.Forms.NumericUpDown QuantizationWSQBox;
        private System.Windows.Forms.Label WhiteLabel;
        private System.Windows.Forms.NumericUpDown WhiteBox;
        private System.Windows.Forms.Label BlackLabel;
        private System.Windows.Forms.NumericUpDown BlackBox;
        private System.Windows.Forms.GroupBox TIFFGroupBox;
        private System.Windows.Forms.Label ColorspaceTIFFLabel;
        private System.Windows.Forms.ComboBox ColorSpaceTIFFcomboBox;
        private System.Windows.Forms.Label ByteOrderLabel;
        private System.Windows.Forms.ComboBox ByteOrderComboBox;
        private System.Windows.Forms.Label RowsLabel;
        private System.Windows.Forms.NumericUpDown RowsBox;
        private System.Windows.Forms.CheckBox MultiTiffCheckBox;
        private System.Windows.Forms.Label CompressionLabel;
        private System.Windows.Forms.ComboBox CompressionComboBox;
        private System.Windows.Forms.CheckBox bufferCheckBox;
        private System.Windows.Forms.GroupBox PortableGroupBox;
        private System.Windows.Forms.GroupBox PDFGroupBox;
        private System.Windows.Forms.CheckBox swapCheckBox;
        private System.Windows.Forms.Label PDFCompressionLabel;
        private System.Windows.Forms.ComboBox PDFCompressionComboBox;
        private System.Windows.Forms.CheckBox PDFMultiCheckBox;
        private System.Windows.Forms.Label PDFColorSpaceLabel;
        private System.Windows.Forms.ComboBox PDFColorSpaceComboBox;
        private System.Windows.Forms.CheckBox MetaCheckBox;
        private System.Windows.Forms.CheckBox FastCheckBox;
        private System.Windows.Forms.Label labelOrder;
        private System.Windows.Forms.NumericUpDown orderBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPredictor;
        private System.Windows.Forms.NumericUpDown predictorBox;
        private System.Windows.Forms.ComboBox comboBoxMethod;
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.PrintPro4;

namespace ImagXpressDemo
{
    public partial class PrintForm : Form
    {
        Unlocks unlock = new Unlocks();

        Helper helper = new Helper();

        private List<ImageX> imagePages;

        private Printer targetPrinter;        

        public PrintForm()
        {
            InitializeComponent();

            helper.CenterScreen(this);
        }

        public ImageX[] Pages
        {
            get
            {
                return imagePages.ToArray();
            }
            set
            {
                imagePages = new List<ImageX>(value);
            }
        }

        private void PrintForm_Load(object sender, EventArgs e)
        {


            unlock.UnlockControls(imagXpress);
            unlock.UnlockControls(printPro);
 

            if ((imagePages == null) || (imagePages.Count < 1))
            {
                MessageBox.Show("No pages to print!", "Error");
                DialogResult = DialogResult.Cancel;
            }
            targetPrinter = Printer.SelectPrinter(printPro, 1, imagePages.Count, 1, imagePages.Count, PrintRangeAvailable.All | PrintRangeAvailable.Pages);
            if (targetPrinter == null)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }

            targetPrinter.PrintUnits = PrintingUnit.Inch;

            foreach (Paper aPaper in targetPrinter.Papers)
            {
                int index = comboBoxPaper.Items.Add(aPaper);
                if (aPaper.Id == targetPrinter.Paper.Id)
                    comboBoxPaper.SelectedIndex = index;
            }
            if (targetPrinter.PaperBins.Count < 1)
            {
                comboBoxPaperBin.Enabled = false;
                comboBoxPaperBin.Items.Add("None");
                comboBoxPaperBin.SelectedIndex = 0;
            }
            foreach (PaperBin aPaperBin in targetPrinter.PaperBins)
            {
                int index = comboBoxPaperBin.Items.Add(aPaperBin);
                if (aPaperBin.Id == targetPrinter.PaperBin.Id)
                    comboBoxPaperBin.SelectedIndex = index;
            }
            comboBoxPrintQuality.SelectedIndex = 0;
            if (targetPrinter.PrintQuality.Dpi() < 0)
            {
                comboBoxPrintQuality.SelectedIndex = (-1 * targetPrinter.PrintQuality.Dpi()) - 1;
            }
            else
            {
                checkBoxDpi.Checked = true;
                numericUpDownPrintQualityDPI.Value = targetPrinter.PrintQuality.Dpi();
            }
            ToBox.Maximum = imagePages.Count;
            FromBox.Maximum = imagePages.Count;
            ToBox.Value= (decimal) targetPrinter.RangeTo;
            FromBox.Value = (decimal)targetPrinter.RangeFrom;
            numericUpDownCopies.Value = targetPrinter.Copies;
            numericUpDownPreviewPage.Maximum = imagePages.Count;
            labelPrinterNameText.Text = targetPrinter.Name;
            labelPrinterPortText.Text = targetPrinter.PortName;
            labelPrinterDriverText.Text = targetPrinter.DriverName;
            comboBoxOrientation.SelectedIndex = targetPrinter.Orientation == PegasusImaging.WinForms.PrintPro4.Orientation.Landscape ? 1 : 0;
            radioButtonColor.Checked = targetPrinter.ColorMode == PrintColorMode.Color;
            radioButtonBlackAndWhite.Checked = !radioButtonColor.Checked;
            comboBoxRotation.SelectedIndex = 0;
            comboBoxPreviewScale.SelectedIndex = 0;
            XBox.Value = 0;
            YBox.Value = 0;
            WBox.Value = (decimal)targetPrinter.PaperWidth;
            HBox.Value = (decimal)targetPrinter.PaperHeight;

            UpdatePrintPreviewImage();
        }

        private void PrintPage(PrintJob printJob, ImageX image)
        {            
            using (ImageX pageCopy = image.Copy())
            {
                using (Processor prc = new Processor(imagXpress, pageCopy))
                {
                    if (comboBoxRotation.SelectedIndex != 0)
                    {
                        double rotationAngle = 0.0;
                        switch (comboBoxRotation.SelectedIndex)
                        {
                            case 1: rotationAngle = 90.0; break;
                            case 2: rotationAngle = 180.0; break;
                            case 3: rotationAngle = 270.0; break;
                        }
                        prc.Rotate(rotationAngle);
                    }
                    if (checkBoxMirror.Checked)
                        prc.Mirror();
                }
                using (Bitmap imageBitmap = pageCopy.ToBitmap(false))
                {
                    float x, y, width, height;
                    if (checkBoxFitToPage.Checked)
                    {
                        x = printJob.LeftMargin;
                        y = printJob.TopMargin;
                        width = printJob.PaperWidth;
                        height = printJob.PaperHeight;
                    }
                    else
                    {
                        x = (float)((float)XBox.Value + printJob.LeftMargin);
                        y = (float)((float)YBox.Value + printJob.TopMargin);
                        width = (float)WBox.Value;
                        height = (float)HBox.Value;
                    }                    
                    printJob.PrintImage(imageBitmap, x, y, width, height, checkBoxKeepAspectRatio.Checked);
                }
            }
        }

        private void buttonSetupPrinter_Click(object sender, EventArgs e)
        {
            if (targetPrinter != null)
                targetPrinter.SetupDialog();
            numericUpDownCopies.Value = targetPrinter.Copies;
            radioButtonBlackAndWhite.Checked = targetPrinter.ColorMode == PrintColorMode.Monochrome;
            radioButtonColor.Checked = targetPrinter.ColorMode == PrintColorMode.Color;
            comboBoxOrientation.SelectedIndex = ((Int32)targetPrinter.Orientation) - 1;
            foreach(Paper aPaper in comboBoxPaper.Items)
            {
                if (aPaper.Id == targetPrinter.Paper.Id)
                    comboBoxPaper.SelectedItem = aPaper;
            }
            foreach (PaperBin aPaperBin in comboBoxPaperBin.Items)
            {
                if (aPaperBin.Id == targetPrinter.PaperBin.Id)
                    comboBoxPaperBin.SelectedItem = aPaperBin;
            }

            UpdatePrintPreviewImage();
        }

        private void UpdatePrintPreviewImage()
        {
            PrintPreviewScale previewScale = (PrintPreviewScale)(comboBoxPreviewScale.SelectedIndex + 1);
            targetPrinter.Orientation = (PegasusImaging.WinForms.PrintPro4.Orientation)(comboBoxOrientation.SelectedIndex + 1);

            using (PrintPreviewJob previewJob = new PrintPreviewJob(targetPrinter, previewScale, true))
            {
                PrintPage(previewJob, imagePages[(Int32)numericUpDownPreviewPage.Value - 1]);
                previewJob.Finish();
                using (Bitmap previewBitmap = previewJob.GetPrintPreviewBitmap(1))
                {
                    imageXView.Image = ImageX.FromBitmap(imagXpress, previewBitmap);
                }
            }
        }
            
        private void buttonPreviewUpdate_Click(object sender, EventArgs e)
        {
            UpdatePrintPreviewImage();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            PrintDocument();
            DialogResult = DialogResult.OK;
        }

        private void PrintDocument()
        {
            targetPrinter.Orientation = (PegasusImaging.WinForms.PrintPro4.Orientation)(comboBoxOrientation.SelectedIndex + 1);
            if (comboBoxPaper.SelectedIndex >= 0)
                targetPrinter.Paper = comboBoxPaper.SelectedItem as Paper;
            if (comboBoxPaperBin.SelectedIndex >= 0 && comboBoxPaperBin.Enabled == true)
                targetPrinter.PaperBin = comboBoxPaperBin.SelectedItem as PaperBin;
            targetPrinter.RangeFrom = (int)FromBox.Value;
            targetPrinter.RangeTo = (int)ToBox.Value;


            using (PrintJob printJob = new PrintJob(targetPrinter, true))
            {
                bool isFirstPage = true;
                if (targetPrinter.RangeSelection == PrintRangeSelection.Pages)
                {
                    for (int pageIndex = targetPrinter.RangeFrom - 1; pageIndex < targetPrinter.RangeTo; pageIndex++)
                    {
                        if (isFirstPage == false)
                        {
                            printJob.NewPage();
                            isFirstPage = false;
                        }
                        PrintPage(printJob, imagePages[pageIndex]);
                    }
                }
                else
                {
                    foreach (ImageX anImage in imagePages)
                    {
                        if (isFirstPage == false)
                        {
                            printJob.NewPage();
                            isFirstPage = false;
                        }
                        PrintPage(printJob, anImage);
                    }
                }
                printJob.Finish();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }    

        private void radioButtonBlackAndWhite_CheckedChanged(object sender, EventArgs e)
        {
            targetPrinter.ColorMode = PrintColorMode.Monochrome;
        }

        private void radioButtonColor_CheckedChanged(object sender, EventArgs e)
        {
            targetPrinter.ColorMode = PrintColorMode.Color;
        }

        private void checkBoxFitToPage_CheckedChanged(object sender, EventArgs e)
        {
            XBox.Enabled = WBox.Enabled = YBox.Enabled = HBox.Enabled = !checkBoxFitToPage.Checked;
        }

        private void numericUpDownCopies_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDownCopies.Value <= targetPrinter.MaxCopies)
                targetPrinter.Copies = (Int32)numericUpDownCopies.Value;
            else
                numericUpDownCopies.Value = targetPrinter.MaxCopies;
        }

        private void checkBoxDpi_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxPrintQuality.Enabled = !(numericUpDownPrintQualityDPI.Enabled = checkBoxDpi.Checked);
        }

        private void FromBox_ValueChanged(object sender, EventArgs e)
        {
            if (FromBox.Value > ToBox.Value)
                FromBox.Value = ToBox.Value;
        }

        private void ToBox_ValueChanged(object sender, EventArgs e)
        {
            if (ToBox.Value < FromBox.Value)
                ToBox.Value = FromBox.Value;
        }

        private void XBox_ValueChanged(object sender, EventArgs e)
        {
            if (WBox.Value + XBox.Value > (decimal)targetPrinter.PaperWidth)
            {
                XBox.Value = Math.Max((decimal)targetPrinter.PaperWidth - WBox.Value, 0);
            }
        }

        private void YBox_ValueChanged(object sender, EventArgs e)
        {
            if (HBox.Value + YBox.Value > (decimal)targetPrinter.PaperHeight)
            {
                YBox.Value = Math.Max((decimal)targetPrinter.PaperHeight - HBox.Value, 0);
            }
        }

        private void WBox_ValueChanged(object sender, EventArgs e)
        {
            if (WBox.Value + XBox.Value > (decimal)targetPrinter.PaperWidth)
            {
                WBox.Value = Math.Max((decimal)targetPrinter.PaperWidth - XBox.Value, 0) ;
            }
        }

        private void HBox_ValueChanged(object sender, EventArgs e)
        {
            if (HBox.Value + YBox.Value > (decimal)targetPrinter.PaperHeight)
            {
                HBox.Value = Math.Max((decimal)targetPrinter.PaperHeight - YBox.Value, 0);
            }
        }

        private void comboBoxPaper_SelectedIndexChanged(object sender, EventArgs e)
        {
            targetPrinter.Paper = comboBoxPaper.SelectedItem as Paper;
            XBox.Value = 0;
            YBox.Value = 0;
            WBox.Value = (decimal)targetPrinter.PaperWidth;
            HBox.Value = (decimal)targetPrinter.PaperHeight;
        }

        private void comboBoxPaperBin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxPaperBin.Enabled == true)
                targetPrinter.PaperBin = comboBoxPaperBin.SelectedItem as PaperBin;
        }

        private void numericUpDownPrintQualityDPI_ValueChanged(object sender, EventArgs e)
        {
            targetPrinter.RequestedDpi = (int)numericUpDownPrintQualityDPI.Value;
        }

        private void comboBoxPrintQuality_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxPrintQuality.SelectedIndex)
            {
                case 0:
                    targetPrinter.PrintQuality = PrintQuality.Draft();
                    break;
                case 1:
                    targetPrinter.PrintQuality = PrintQuality.Low();
                    break;
                case 2:
                    targetPrinter.PrintQuality = PrintQuality.Medium();
                    break;
                case 3:
                    targetPrinter.PrintQuality = PrintQuality.High();
                    break;
            }
        }

        private void comboBoxOrientation_SelectedIndexChanged(object sender, EventArgs e)
        {
            targetPrinter.Orientation = (PegasusImaging.WinForms.PrintPro4.Orientation)(comboBoxOrientation.SelectedIndex + 1);
            XBox.Value = 0;
            YBox.Value = 0;
            WBox.Value = (decimal)targetPrinter.PaperWidth;
            HBox.Value = (decimal)targetPrinter.PaperHeight;
        }        
    }
}

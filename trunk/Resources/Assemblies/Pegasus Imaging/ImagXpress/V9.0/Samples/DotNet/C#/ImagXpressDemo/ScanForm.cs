/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.TwainPro5;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo
{
    public partial class ScanForm : Form
    {
        Unlocks unlock = new Unlocks();
        
        Helper helper = new Helper();

        public ScanForm()
        {            
            InitializeComponent();

            helper.CenterScreen(this);
        }

        private TwainDevice twainDevice;

        private Capability capability;
        private CapabilityContainer capcontainer;

        private ColorDialog foreColorDialog;
        private FontDialog fontDialog;

        private List<ImageX> ScannedImages;

        private int currentPage;

        private string filenameSavedAs;

        public ImageX[] scannedImages
        {
            get
            {
                return ScannedImages.ToArray();
            }
        }

        public string FilenameSavedAs
        {
            get
            {
                return filenameSavedAs;
            }
        }

        private int imageCount;

        #region ScanForm Event Handlers

        private void ScanForm_Load(object sender, EventArgs e)
        {
            try
            {
                

                unlock.UnlockControls(imagXpress1);
                unlock.UnlockControls(twainPro1);


                ScannedImages = new List<ImageX>();

                foreColorDialog = new ColorDialog();
                fontDialog = new FontDialog();

                twainDevice = new TwainDevice(twainPro1);

                twainDevice.Scanned += new ScannedEventHandler(twainScanned);

                foreach (Capability cap in Enum.GetValues(typeof(Capability)))
                {
                    comboBoxCapabilites.Items.Add(cap.ToString());
                }
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        #region TwainDevice Event Handlers

        private void twainScanned(object sender, ScannedEventArgs e)
        {
            try
            {
                using (Bitmap bp = e.ScannedImage.ToBitmap())
                {
                    //resource cleanup
                    if (imageXView1 != null)
                    {
                        if (imageXView1.Image != null)
                        {
                            imageXView1.Image.Dispose();
                            imageXView1.Image = null;
                        }
                    }

                    imageXView1.Image = ImageX.FromBitmap(imagXpress1, bp);                    
                }

                ScannedImages.Add(imageXView1.Image.Copy());

                imageCount++;

                if (imageCount > 1)
                {
                    previousButton.Enabled = true;
                }

                nextButton.Enabled = false;

                countLabel.Text = "of " + imageCount.ToString();

                pageTextBox.Text = imageCount.ToString();
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Exception Handlers

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void TwainExceptionHandler(TwainException ex, string methodName)
        {
            MessageBox.Show("Error #: " + ex.ErrorNumber.ToString() + " - " + ex.ConditionDescription
                 + " - Condition Code #: " + ex.ConditionCode.ToString(), "Error from " + methodName,
                 MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

        private void sourceButton_Click(object sender, EventArgs e)
        {
            try
            {
                twainDevice.CloseSession();

                try
                {
                    twainDevice.SelectSource();

                    twainDevice.OpenSession();

                    groupBoxLayout.Enabled = true;
                    groupBoxCaption.Enabled = true;
                    groupBoxCaps.Enabled = true;

                    scanButton.Enabled = true;
                    UICheckBox.Enabled = true;

                    comboBoxCapabilites.SelectedIndex = 0;

                    UpdateUI();

                    twainDevice.ShowUserInterface = false;
                }
                catch (OperationCancelledException)
                {
                    //expected exception if Cancel button is pressed
                }
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex,MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void UpdateUI()
        {
            comboBoxHAlign.SelectedIndex = (int)twainDevice.Caption.HorizontalAlignment;
            comboBoxVAlign.SelectedIndex = (int)twainDevice.Caption.VerticalAlignment;
            textBoxCaption.Text = twainDevice.Caption.Text;
            textBoxCapLeft.Text = twainDevice.Caption.Area.Left.ToString();
            textBoxCapTop.Text = twainDevice.Caption.Area.Top.ToString();
            textBoxCapWidth.Text = twainDevice.Caption.Area.Width.ToString();
            textBoxCapHeight.Text = twainDevice.Caption.Area.Height.ToString();
            fontButton.Text = twainDevice.Caption.Font.Name + ", " + twainDevice.Caption.Font.Style + ", "
                    + twainDevice.Caption.Font.Size;
            foreColorButton.BackColor = twainDevice.Caption.ForeColor;
            checkBoxClipCaption.Checked = twainDevice.Caption.Clip;
            checkBoxShadowText.Checked = twainDevice.Caption.ShadowText;

            textBoxIB.Text = twainDevice.ImageLayout.Bottom.ToString();
            textBoxIL.Text = twainDevice.ImageLayout.Left.ToString();
            textBoxIR.Text = twainDevice.ImageLayout.Right.ToString();
            textBoxIT.Text = twainDevice.ImageLayout.Top.ToString();
        }

        private void UICheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                twainDevice.ShowUserInterface = UICheckBox.Checked;
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #region ScanForm Event Handlers

        private void foreColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (foreColorDialog.ShowDialog() == DialogResult.OK)
                {
                    foreColorButton.BackColor = foreColorDialog.Color;
                }
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void fontButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (fontDialog.ShowDialog() == DialogResult.OK)
                {
                    fontButton.Text = fontDialog.Font.Name + ", " + fontDialog.Font.Style + ", "
                        + fontDialog.Font.Size;
                }
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex,MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void buttonLayoutUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                twainDevice.ImageLayout = new RectangleF(Single.Parse(textBoxIL.Text), Single.Parse(textBoxIT.Text),
                    Math.Abs(Single.Parse(textBoxIR.Text) - Single.Parse(textBoxIL.Text)),
                    Math.Abs(Single.Parse(textBoxIT.Text) - Single.Parse(textBoxIB.Text)));
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex,MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void updateCaptionButton_Click(object sender, EventArgs e)
        {
            try
            {
                twainDevice.Caption.Clip = checkBoxClipCaption.Checked;
                twainDevice.Caption.ShadowText = checkBoxShadowText.Checked;
                twainDevice.Caption.Area = new Rectangle(Int32.Parse(textBoxCapLeft.Text), Int32.Parse(textBoxCapTop.Text),
                    Int32.Parse(textBoxCapWidth.Text), Int32.Parse(textBoxCapHeight.Text));
                twainDevice.Caption.HorizontalAlignment =
                   (PegasusImaging.WinForms.TwainPro5.HorizontalAlignment)comboBoxHAlign.SelectedIndex;
                twainDevice.Caption.VerticalAlignment =
                    (PegasusImaging.WinForms.TwainPro5.VerticalAlignment)comboBoxVAlign.SelectedIndex;
                twainDevice.Caption.Font = fontDialog.Font;
                twainDevice.Caption.ForeColor = foreColorDialog.Color;
                twainDevice.Caption.Text = textBoxCaption.Text;
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex,MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void comboBoxCaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateCapabilityButton.Enabled = true;

                //clear capabilities listbox
                listBoxCaps.Items.Clear();

                //clear current capability textbox and enable it
                textBoxCurrent.Text = "";
                textBoxCurrent.Enabled = true;

                labelStep.Text = "";
                labelMax.Text = "";
                labelMin.Text = "";
                labelUnits.Text = "";
                labelDefault.Text = "";

                foreach (Capability cap in Enum.GetValues(typeof(Capability)))
                {
                    if (cap.ToString() == (string)comboBoxCapabilites.Items[comboBoxCapabilites.SelectedIndex])
                    {
                        capability = cap;
                        break;
                    }
                }

                //known read-only capabilities
                if (capability == Capability.CapDeviceOnline || capability == Capability.CapDuplex ||
                    capability == Capability.CapFeederLoaded || capability == Capability.CapPaperDetectable ||
                    capability == Capability.IcapPhysicalWidth || capability == Capability.IcapPhysicalHeight ||
                    capability == Capability.CapUIControllable || capability == Capability.IcapXNativeResolution ||
                    capability == Capability.IcapYNativeResolution)
                {
                    updateCapabilityButton.Enabled = false;
                    textBoxCurrent.Enabled = false;

                    listBoxCaps.Items.Add("Read-only capabilities can't be set.");
                }

                if (twainDevice.IsCapabilitySupported(Capability.IcapUnits) == true)
                {
                    //if capability has known units then display them for current units chosen
                    if (capability == Capability.IcapPhysicalWidth || capability == Capability.IcapPhysicalHeight
                        || capability == Capability.IcapXNativeResolution || capability == Capability.IcapYNativeResolution
                        || capability == Capability.IcapXResolution || capability == Capability.IcapYResolution)
                    {
                        capcontainer = twainDevice.GetCapability(Capability.IcapUnits);

                        string description = "";

                        if (capcontainer is CapabilityContainerOneValueFloat)
                        {
                            CapabilityContainerOneValueFloat myCap = new CapabilityContainerOneValueFloat(Capability.IcapUnits);

                            description = twainDevice.GetCapabilityConstantDescription(Capability.IcapUnits, (int)myCap.Value);

                            if (description == "" || description == null)
                            {
                                labelUnits.Text = myCap.Value.ToString();
                            }
                            else
                            {
                                labelUnits.Text = myCap.Value.ToString() + " - " + description;
                            }
                        }
                        else if (capcontainer is CapabilityContainerEnum)
                        {
                            CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(Capability.IcapUnits);

                            float fResult = ((CapabilityContainerOneValueFloat)myCap.CurrentValue).Value;

                            description = twainDevice.GetCapabilityConstantDescription(Capability.IcapUnits, (int)fResult);

                            if (description == "" || description == null)
                            {
                                labelUnits.Text = fResult.ToString();
                            }
                            else
                            {
                                labelUnits.Text = fResult.ToString() + " - " + description;
                            }
                        }
                    }
                }

                CapabilityUpdate(capability);
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void CapabilityUpdate(Capability cap)
        {
            if (twainDevice.IsCapabilitySupported(cap) == false)
            {
                listBoxCaps.Items.Add("The Capability " + cap.ToString() + " is not supported.");

                textBoxCurrent.Enabled = false;

                updateCapabilityButton.Enabled = false;
            }
            else
            {
                capcontainer = twainDevice.GetCapability(cap);

                // What data type is the Cap?  We have to check the data type
                // to know which properties are valid

                if (capcontainer is CapabilityContainerOneValueFloat)
                {
                    // Type OneValuteFloat returns a float
                    CapabilityContainerOneValueFloat myCap = (CapabilityContainerOneValueFloat)twainDevice.GetCapability(cap);

                    //Value is a float so convert to string
                    textBoxCurrent.Text = myCap.Value.ToString();
                }
                else if (capcontainer is CapabilityContainerOneValueString)
                {
                    //Type OneValueString returns a string
                    CapabilityContainerOneValueString myCap = (CapabilityContainerOneValueString)twainDevice.GetCapability(cap);

                    //value is a string
                    textBoxCurrent.Text = myCap.Value;
                }
                else if (capcontainer is CapabilityContainerEnum)
                {   // Type Enumeration returns a list of legal values as well as current and
                    // default values.  GetCapabilityDescription() can be used to find out what the constants mean
                    CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(cap);

                    textBoxCurrent.Text = ((CapabilityContainerOneValueFloat)myCap.CurrentValue).Value.ToString();

                    //cast DefaultValue as a OneValueFloat and then get the Value as a float
                    float defaultValue = ((CapabilityContainerOneValueFloat)myCap.DefaultValue).Value;

                    string defaultString = "";

                    try
                    {
                        //get the constant description of the capability for the default value
                        defaultString = twainDevice.GetCapabilityConstantDescription(cap, (int)defaultValue);
                    }
                    catch { };

                    if (defaultString == null || defaultString == "")
                    {
                        labelDefault.Text = defaultValue.ToString();
                    }
                    else
                    {
                        labelDefault.Text = defaultValue.ToString() + " - " + defaultString;
                    }

                    string strResult = "";
                    float fResult = 0;

                    for (int i = 0; i < myCap.Values.Count; i++)
                    {
                        //cast Values[i] as a OneValueFloat and then get the Value as a float
                        fResult = ((CapabilityContainerOneValueFloat)myCap.Values[i]).Value;

                        try
                        {
                            //get the constant description of the capability for that Value
                            strResult = twainDevice.GetCapabilityConstantDescription(cap, (int)fResult);
                        }
                        catch { };

                        if (strResult == null || strResult == "")
                        {
                            listBoxCaps.Items.Add(fResult.ToString());
                        }
                        else
                        {
                            listBoxCaps.Items.Add(fResult.ToString() + " - " + strResult);
                        }
                    }
                }
                else if (capcontainer is CapabilityContainerArray)
                {
                    // Type Array returns a list of values, but no current or default values
                    // This is a less common type that many sources don't use
                    CapabilityContainerArray myCap = (CapabilityContainerArray)twainDevice.GetCapability(cap);

                    float fResult = 0;

                    string strResult = "";

                    for (int i = 0; i < myCap.Values.Count; i++)
                    {
                        //cast Values[i] as a OneValueFloat and then get the Value as a float
                        fResult = ((CapabilityContainerOneValueFloat)myCap.Values[i]).Value;

                        try
                        {
                            //get the constant description of the capability for that Value
                            strResult = twainDevice.GetCapabilityConstantDescription(cap, (int)fResult);
                        }
                        catch { };

                        if (strResult == null || strResult == "")
                        {
                            listBoxCaps.Items.Add(fResult.ToString());
                        }
                        else
                        {
                            listBoxCaps.Items.Add(fResult.ToString() + " - " + strResult);
                        }
                    }
                }
                else if (capcontainer is CapabilityContainerRange)
                {
                    //Type Range returns a range of values as well as current and default values
                    CapabilityContainerRange myCap = (CapabilityContainerRange)twainDevice.GetCapability(cap);

                    //Value is a float so convert to a string
                    textBoxCurrent.Text = myCap.Value.ToString();

                    //Value is a float so convert to a string
                    labelDefault.Text = myCap.Default.ToString();

                    labelStep.Text = myCap.Step.ToString();

                    //Value is a float so convert to a string
                    labelMin.Text = myCap.Minimum.ToString(); ;

                    //Value is a float so convert to a string
                    labelMax.Text = myCap.Maximum.ToString();
                }
            }
        }

        private void updateCapabilityButton_Click(object sender, EventArgs e)
        {
            try
            {
                CapabilityContainerOneValue myCap;
                if (capcontainer is CapabilityContainerOneValue)
                {
                    myCap = (CapabilityContainerOneValue)capcontainer;
                }
                else if (capcontainer is CapabilityContainerEnum)
                {
                    myCap = ((CapabilityContainerEnum)capcontainer).CurrentValue;
                }
                else
                {
                    myCap = new CapabilityContainerOneValueFloat(capability);
                }

                //set the capability
                if (myCap is CapabilityContainerOneValueFloat)
                {
                    ((CapabilityContainerOneValueFloat)myCap).Value = Single.Parse(textBoxCurrent.Text);
                }
                else if (myCap is CapabilityContainerOneValueString)
                {
                    ((CapabilityContainerOneValueString)myCap).Value = textBoxCurrent.Text;
                }
                twainDevice.SetCapability(myCap);

                comboBoxCaps_SelectedIndexChanged(sender, e);
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex , MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        private void scanButton_Click(object sender, EventArgs e)
        {
            try
            {
                twainDevice.StartSession();

                OKButton.Enabled = true;

                twainDevice.CloseSession();
                twainDevice.OpenSession();
            }
            catch (TwainException ex)
            {
                TwainExceptionHandler(ex,MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.Title = "Save the Image File";
                dlg.InitialDirectory = Application.StartupPath;
                dlg.Filter = helper.SaveImageFilter;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    filenameSavedAs = dlg.FileName;

                    using (SaveOptionsForm saveForm = new SaveOptionsForm())
                    {
                        helper.CenterScreen(saveForm);

                        saveForm.ImageSaveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();

                        helper.IdentifySaveOptionUI(dlg.FilterIndex, saveForm, saveForm.ImageSaveOptions);

                        saveForm.ShowDialog();

                        PegasusImaging.WinForms.ImagXpress9.SaveOptions saveOptions = saveForm.ImageSaveOptions;

                        if ((saveOptions.Tiff.MultiPage == true && saveOptions.Format == ImageXFormat.Tiff) || (saveOptions.Dcx.MultiPage == true && saveOptions.Format == ImageXFormat.Dcx)
                            || (saveOptions.Pdf.MultiPage == true && saveOptions.Format == ImageXFormat.Pdf) || (saveOptions.Icon.MultiPage == true && saveOptions.Format == ImageXFormat.Ico))
                        {
                            foreach (ImageX img in ScannedImages)
                            {
                                img.Save(dlg.FileName, saveOptions);
                            }
                        }
                        else
                        {
                            imageXView1.Image.Save(dlg.FileName);
                        }
                    }

                    DialogResult = DialogResult.OK;

                    Close();
                }
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            previousButton.Enabled = true;

            currentPage = Int32.Parse(pageTextBox.Text);

            currentPage++;

            pageTextBox.Text = currentPage.ToString();

            Paging();

            if (currentPage == imageCount)
            {
                nextButton.Enabled = false;
            }
        }

        private void previousButton_Click(object sender, EventArgs e)
        {
            nextButton.Enabled = true;

            currentPage = Int32.Parse(pageTextBox.Text);

            currentPage--;

            pageTextBox.Text = currentPage.ToString();

            Paging();

            if (currentPage == 1)
            {
                previousButton.Enabled = false;
            }
        }

        private void Paging()
        {
            try
            {
                //resource cleanup
                if (imageXView1 != null)
                {
                    if (imageXView1.Image != null)
                    {
                        imageXView1.Image.Dispose();
                        imageXView1.Image = null;
                    }
                }

                imageXView1.Image = ScannedImages[currentPage - 1].Copy();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}

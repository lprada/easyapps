'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Imports PegasusImaging.WinForms.ImagXpress9

Public Class MainForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (ImageXView1.Image Is Nothing) Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents DescriptionListBox As System.Windows.Forms.ListBox
    Friend WithEvents PrintButton1 As System.Windows.Forms.Button
    Friend WithEvents PrintButton2 As System.Windows.Forms.Button
    Friend WithEvents PrintButton3 As System.Windows.Forms.Button
    Friend WithEvents ErrorLabel2 As System.Windows.Forms.Label
    Friend WithEvents ErrorLabel As System.Windows.Forms.Label
    Friend WithEvents MainMenu As System.Windows.Forms.MainMenu
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents ToolbarMenu As System.Windows.Forms.MenuItem
    Friend WithEvents AboutMenu As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ShowMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents OpenMenuItem As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.DescriptionListBox = New System.Windows.Forms.ListBox
        Me.PrintButton1 = New System.Windows.Forms.Button
        Me.PrintButton2 = New System.Windows.Forms.Button
        Me.PrintButton3 = New System.Windows.Forms.Button
        Me.ErrorLabel2 = New System.Windows.Forms.Label
        Me.ErrorLabel = New System.Windows.Forms.Label
        Me.MainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.FileMenu = New System.Windows.Forms.MenuItem
        Me.OpenMenuItem = New System.Windows.Forms.MenuItem
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem
        Me.ToolbarMenu = New System.Windows.Forms.MenuItem
        Me.ShowMenuItem = New System.Windows.Forms.MenuItem
        Me.AboutMenu = New System.Windows.Forms.MenuItem
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(8, 72)
        Me.ImageXView1.MouseWheelCapture = False
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(384, 304)
        Me.ImageXView1.TabIndex = 0
        '
        'DescriptionListBox
        '
        Me.DescriptionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionListBox.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1) Printing an image with ImagXpress."})
        Me.DescriptionListBox.Location = New System.Drawing.Point(8, 8)
        Me.DescriptionListBox.Name = "DescriptionListBox"
        Me.DescriptionListBox.Size = New System.Drawing.Size(808, 43)
        Me.DescriptionListBox.TabIndex = 1
        '
        'PrintButton1
        '
        Me.PrintButton1.Location = New System.Drawing.Point(120, 432)
        Me.PrintButton1.Name = "PrintButton1"
        Me.PrintButton1.Size = New System.Drawing.Size(192, 32)
        Me.PrintButton1.TabIndex = 2
        Me.PrintButton1.Text = "Print Image Centered on Page"
        '
        'PrintButton2
        '
        Me.PrintButton2.Location = New System.Drawing.Point(120, 480)
        Me.PrintButton2.Name = "PrintButton2"
        Me.PrintButton2.Size = New System.Drawing.Size(192, 32)
        Me.PrintButton2.TabIndex = 3
        Me.PrintButton2.Text = "Print 2 Images Centered on Page"
        '
        'PrintButton3
        '
        Me.PrintButton3.Location = New System.Drawing.Point(120, 528)
        Me.PrintButton3.Name = "PrintButton3"
        Me.PrintButton3.Size = New System.Drawing.Size(192, 32)
        Me.PrintButton3.TabIndex = 4
        Me.PrintButton3.Text = "Print Image Fit to Page"
        '
        'ErrorLabel2
        '
        Me.ErrorLabel2.Location = New System.Drawing.Point(405, 515)
        Me.ErrorLabel2.Name = "ErrorLabel2"
        Me.ErrorLabel2.Size = New System.Drawing.Size(88, 16)
        Me.ErrorLabel2.TabIndex = 5
        Me.ErrorLabel2.Text = "Last Error:"
        '
        'ErrorLabel
        '
        Me.ErrorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ErrorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ErrorLabel.Location = New System.Drawing.Point(408, 549)
        Me.ErrorLabel.Name = "ErrorLabel"
        Me.ErrorLabel.Size = New System.Drawing.Size(400, 59)
        Me.ErrorLabel.TabIndex = 6
        '
        'MainMenu
        '
        Me.MainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.ToolbarMenu, Me.AboutMenu})
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.OpenMenuItem, Me.ExitMenuItem})
        Me.FileMenu.Text = "&File"
        '
        'OpenMenuItem
        '
        Me.OpenMenuItem.Index = 0
        Me.OpenMenuItem.Text = "&Open"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 1
        Me.ExitMenuItem.Text = "E&xit"
        '
        'ToolbarMenu
        '
        Me.ToolbarMenu.Index = 1
        Me.ToolbarMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ShowMenuItem})
        Me.ToolbarMenu.Text = "&Toolbar"
        '
        'ShowMenuItem
        '
        Me.ShowMenuItem.Index = 0
        Me.ShowMenuItem.Text = "&Show"
        '
        'AboutMenu
        '
        Me.AboutMenu.Index = 2
        Me.AboutMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ImagXpressMenuItem})
        Me.AboutMenu.Text = "&About"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 0
        Me.ImagXpressMenuItem.Text = "Imag&Xpress"
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(826, 619)
        Me.Controls.Add(Me.ErrorLabel)
        Me.Controls.Add(Me.ErrorLabel2)
        Me.Controls.Add(Me.PrintButton3)
        Me.Controls.Add(Me.PrintButton2)
        Me.Controls.Add(Me.PrintButton1)
        Me.Controls.Add(Me.DescriptionListBox)
        Me.Controls.Add(Me.ImageXView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Print"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"

    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\Common\Images\"
    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "\..\..\..\..\..\..\..\Common\Images")
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.gif;*.hdp;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wdp;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*" & _
 ".dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.h" & _
 "dp;*.wdp|All Files (*.*)|*.*"

    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Dim ButtonOne As Boolean
    Dim ButtonTwo As Boolean


    Private Sub PrintButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton2.Click
        ButtonTwo = True
        HelperPrint()
    End Sub


    Private Sub PrintButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton1.Click
        ButtonOne = True
        HelperPrint()
    End Sub

    Private Sub OpenMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenMenuItem.Click
        Dim imgFilename As String = PegasusOpenFile()

        If Not (imgFilename = "") Then
            Try
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, imgFilename)
            Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
                PegasusError(ex, ErrorLabel)
            End Try
        End If
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub ShowMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowMenuItem.Click
        If (ShowMenuItem.Checked = False) Then
            ImageXView1.Toolbar.Activated = True
            ShowMenuItem.Checked = True
        Else
            ImageXView1.Toolbar.Activated = False
            ShowMenuItem.Checked = False
        End If
    End Sub

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Application.EnableVisualStyles()

        '**The UnlockRuntime function must be called to distribute the runtime**
        'imagXpress1.Licensing.UnlockRuntime(12345,12345,12345,12345)
        Try
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, Application.StartupPath + "..\..\..\..\..\..\..\..\Common\Images\vermont.jpg")
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            ErrorLabel.Text = ex.Message
        End Try

    End Sub

    Private Sub HelperPrint()

        If Not (ImageXView1.Image Is Nothing) Then
            ImageXView1.Image.Dispose()
            ImageXView1.Image = Nothing
        End If

        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, Application.StartupPath + "..\..\..\..\..\..\..\..\Common\Images\vermont.jpg")

        Dim width As System.Int32 = ImageXView1.Image.ImageXData.Width
        Dim height As System.Int32 = ImageXView1.Image.ImageXData.Height

        ImageXView1.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch

        Dim resx As Double = Math.Abs(ImageXView1.Image.ImageXData.Resolution.Dimensions.Width)
        Dim resy As Double = Math.Abs(ImageXView1.Image.ImageXData.Resolution.Dimensions.Height)

        Using img As ImageX = New ImageX(ImagXpress1, CInt(8.5 * resx), CInt(11 * resy), 24, Color.White)

            ImageXView1.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch

            Dim g As System.Drawing.Graphics = img.GetGraphics()

            g.PageUnit = GraphicsUnit.Inch

            ImageXView1.Image.Resolution.Units = GraphicsUnit.Inch

            If ButtonOne = True Then

                ImageXView1.Print(g, New RectangleF(Convert.ToSingle(8.5 - width / resx) / 2, Convert.ToSingle(11 - height / resy) / 2, Convert.ToSingle(width / resx), Convert.ToSingle(height / resy)))

            ElseIf ButtonTwo = True Then

                ImageXView1.Print(g, New RectangleF(Convert.ToSingle(8.5 - width / resx) / 2, Convert.ToSingle(11 - 2 * height / resy), Convert.ToSingle(width / resx), Convert.ToSingle(height / resy)))

                ImageXView1.Print(g, New RectangleF(Convert.ToSingle(8.5 - width / resx) / 2, Convert.ToSingle(height / resy), Convert.ToSingle(width / resx), Convert.ToSingle(height / resy)))

            Else

                ImageXView1.Print(g, New RectangleF(0, 0, Convert.ToSingle(8.5), Convert.ToSingle(10.5)))
            End If

            ButtonOne = False
            ButtonTwo = False

            img.ReleaseGraphics()

            If Not (ImageXView1.Image Is Nothing) Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If

            ImageXView1.Image = img.Copy()
        End Using
    End Sub

    Private Sub PrintButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton3.Click
        HelperPrint()
    End Sub
End Class

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports PegasusImaging.WinForms.ImagXpress9
Namespace AlphaChannels

	Public Class Alpha
	Inherits System.Windows.Forms.Form
		Private imagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
		Private imageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
		Private ixproc1 As PegasusImaging.WinForms.ImagXpress9.Processor
        Friend WithEvents menuFile As System.Windows.Forms.MainMenu
        Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
        Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
        Friend WithEvents menuItem1 As System.Windows.Forms.MenuItem
        Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
        Friend WithEvents mnuToolBar As System.Windows.Forms.MenuItem
        Friend WithEvents mnuShow As System.Windows.Forms.MenuItem
		Private lblError As System.Windows.Forms.Label

		Private strCurrentImage As System.String
		Private strCurrentImage1 As System.String
		Private strCurrentImage2 As System.String
		Private strCurrentImage3 As System.String
        Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
        Friend WithEvents cmdAddAlpha As System.Windows.Forms.Button
        Friend WithEvents mnuAlpha As System.Windows.Forms.MenuItem
		Private lstdesc As System.Windows.Forms.ListBox
        Friend WithEvents cmbAlpaha As System.Windows.Forms.ComboBox
		Private imageXView2 As PegasusImaging.WinForms.ImagXpress9.ImageXView
		Private lblAlpha As System.Windows.Forms.Label
		Private lblsource As System.Windows.Forms.Label
        Private labelLastError As System.Windows.Forms.Label
        Private components As System.ComponentModel.IContainer

        Public Sub New()

            InitializeComponent()
        End Sub

        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (imagXpress1 Is Nothing) Then
                    imagXpress1.Dispose()
                    imagXpress1 = Nothing
                End If
                If Not (imageXView1 Is Nothing) Then
                    imageXView1.Dispose()
                    imageXView1 = Nothing
                End If
                If Not (imageXView2 Is Nothing) Then
                    imageXView2.Dispose()
                    imageXView2 = Nothing
                End If
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.imagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
            Me.imageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
            Me.menuFile = New System.Windows.Forms.MainMenu(Me.components)
            Me.mnuFile = New System.Windows.Forms.MenuItem
            Me.mnuOpen = New System.Windows.Forms.MenuItem
            Me.mnuAlpha = New System.Windows.Forms.MenuItem
            Me.menuItem1 = New System.Windows.Forms.MenuItem
            Me.mnuQuit = New System.Windows.Forms.MenuItem
            Me.mnuToolBar = New System.Windows.Forms.MenuItem
            Me.mnuShow = New System.Windows.Forms.MenuItem
            Me.mnuAbout = New System.Windows.Forms.MenuItem
            Me.lblError = New System.Windows.Forms.Label
            Me.cmdAddAlpha = New System.Windows.Forms.Button
            Me.lstdesc = New System.Windows.Forms.ListBox
            Me.cmbAlpaha = New System.Windows.Forms.ComboBox
            Me.imageXView2 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
            Me.lblAlpha = New System.Windows.Forms.Label
            Me.lblsource = New System.Windows.Forms.Label
            Me.labelLastError = New System.Windows.Forms.Label
            Me.SuspendLayout()
            '
            'imageXView1
            '
            Me.imageXView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit
            Me.imageXView1.AutoScroll = True
            Me.imageXView1.BackColor = System.Drawing.SystemColors.ActiveBorder
            Me.imageXView1.Location = New System.Drawing.Point(176, 176)
            Me.imageXView1.MouseWheelCapture = False
            Me.imageXView1.Name = "imageXView1"
            Me.imageXView1.Size = New System.Drawing.Size(384, 328)
            Me.imageXView1.TabIndex = 0
            '
            'menuFile
            '
            Me.menuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolBar, Me.mnuAbout})
            '
            'mnuFile
            '
            Me.mnuFile.Index = 0
            Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.mnuAlpha, Me.menuItem1, Me.mnuQuit})
            Me.mnuFile.Text = "&File"
            '
            'mnuOpen
            '
            Me.mnuOpen.Index = 0
            Me.mnuOpen.Text = "&Open Source Image"
            '
            'mnuAlpha
            '
            Me.mnuAlpha.Index = 1
            Me.mnuAlpha.Text = "&Open Alpha Channel Image"
            '
            'menuItem1
            '
            Me.menuItem1.Index = 2
            Me.menuItem1.Text = "-"
            '
            'mnuQuit
            '
            Me.mnuQuit.Index = 3
            Me.mnuQuit.Text = "&Quit"
            '
            'mnuToolBar
            '
            Me.mnuToolBar.Index = 1
            Me.mnuToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuShow})
            Me.mnuToolBar.Text = "&ToolBar"
            '
            'mnuShow
            '
            Me.mnuShow.Index = 0
            Me.mnuShow.Text = "&Show"
            '
            'mnuAbout
            '
            Me.mnuAbout.Index = 2
            Me.mnuAbout.Text = "&About"
            '
            'lblError
            '
            Me.lblError.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblError.Location = New System.Drawing.Point(264, 96)
            Me.lblError.Name = "lblError"
            Me.lblError.Size = New System.Drawing.Size(480, 56)
            Me.lblError.TabIndex = 1
            '
            'cmdAddAlpha
            '
            Me.cmdAddAlpha.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.cmdAddAlpha.Location = New System.Drawing.Point(8, 464)
            Me.cmdAddAlpha.Name = "cmdAddAlpha"
            Me.cmdAddAlpha.Size = New System.Drawing.Size(160, 40)
            Me.cmdAddAlpha.TabIndex = 3
            Me.cmdAddAlpha.Text = "Blend Alpha Channel Image With Source Image"
            '
            'lstdesc
            '
            Me.lstdesc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lstdesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lstdesc.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Merging an image with an alpha channel to a source image and displaying the com" & _
                            "bined image. ", "", "You can select an Alpha Channel image in the dropdown list or load your own sourc" & _
                            "e image and/or alpha channel image", "in the sample."})
            Me.lstdesc.Location = New System.Drawing.Point(16, 16)
            Me.lstdesc.Name = "lstdesc"
            Me.lstdesc.Size = New System.Drawing.Size(736, 69)
            Me.lstdesc.TabIndex = 4
            '
            'cmbAlpaha
            '
            Me.cmbAlpaha.Items.AddRange(New Object() {"AlphaImage1", "AlphaImage2", "AlpahaImage3"})
            Me.cmbAlpaha.Location = New System.Drawing.Point(16, 96)
            Me.cmbAlpaha.Name = "cmbAlpaha"
            Me.cmbAlpaha.Size = New System.Drawing.Size(144, 21)
            Me.cmbAlpaha.TabIndex = 5
            '
            'imageXView2
            '
            Me.imageXView2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit
            Me.imageXView2.Location = New System.Drawing.Point(16, 176)
            Me.imageXView2.MouseWheelCapture = False
            Me.imageXView2.Name = "imageXView2"
            Me.imageXView2.Size = New System.Drawing.Size(144, 280)
            Me.imageXView2.TabIndex = 6
            '
            'lblAlpha
            '
            Me.lblAlpha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblAlpha.Location = New System.Drawing.Point(16, 152)
            Me.lblAlpha.Name = "lblAlpha"
            Me.lblAlpha.Size = New System.Drawing.Size(144, 16)
            Me.lblAlpha.TabIndex = 7
            Me.lblAlpha.Text = "Alpha Channel Image"
            Me.lblAlpha.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            '
            'lblsource
            '
            Me.lblsource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblsource.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lblsource.Location = New System.Drawing.Point(176, 152)
            Me.lblsource.Name = "lblsource"
            Me.lblsource.Size = New System.Drawing.Size(384, 16)
            Me.lblsource.TabIndex = 8
            Me.lblsource.Text = "Source Image"
            Me.lblsource.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            '
            'labelLastError
            '
            Me.labelLastError.Location = New System.Drawing.Point(184, 96)
            Me.labelLastError.Name = "labelLastError"
            Me.labelLastError.Size = New System.Drawing.Size(72, 16)
            Me.labelLastError.TabIndex = 9
            Me.labelLastError.Text = "Last Error:"
            Me.labelLastError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'Alpha
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(760, 513)
            Me.Controls.Add(Me.labelLastError)
            Me.Controls.Add(Me.lblsource)
            Me.Controls.Add(Me.lblAlpha)
            Me.Controls.Add(Me.imageXView2)
            Me.Controls.Add(Me.cmbAlpaha)
            Me.Controls.Add(Me.lstdesc)
            Me.Controls.Add(Me.cmdAddAlpha)
            Me.Controls.Add(Me.lblError)
            Me.Controls.Add(Me.imageXView1)
            Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Menu = Me.menuFile
            Me.Name = "Alpha"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Alpha Channel Processing"
            Me.ResumeLayout(False)

        End Sub

        <STAThread()> _
        Shared Sub Main()
            Application.Run(New Alpha())
        End Sub

        Private Sub mnuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click
            Dim strLoadResult As System.String = PegasusOpenFile()
            If Not (strLoadResult.Length = 0) Then
                strCurrentImage = strLoadResult
            End If

            'clear out the error before the next image load
            lblError.Text = ""

            Try
                Dim oldImage As PegasusImaging.WinForms.ImagXpress9.ImageX = imageXView1.Image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage)
				ixproc1.Image = imageXView1.Image
				If Not (oldImage Is Nothing) Then
					oldImage.Dispose()
					oldImage = Nothing
				End If
			Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
				PegasusError(ex, lblError)
			End Try
		End Sub

		Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
			Application.Exit()
		End Sub

		Private Sub mnuShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShow.Click
			Me.mnuShow.Text = Microsoft.VisualBasic.IIf((imageXView1.Toolbar.Activated), "&Show", "&Hide")
			Try
				imageXView1.Toolbar.Activated = Not imageXView1.Toolbar.Activated
			Catch eX As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
				PegasusError(eX, lblError)
			End Try
		End Sub
		
#Region "Pegasus Imaging Sample Application Standard Functions"

   '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo    
        Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\Common\Images\"

        Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "\..\..\..\..\..\..\..\..\Common\Images")

    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.gif;*.hdp;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wdp;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*" & _
	".dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.h" & _
	"dp;*.wdp|All Files (*.*)|*.*"

    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

	Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
		Dim iTmp As System.Int32
		Try
			iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
		Catch ex As System.NullReferenceException
			PegasusError(ex, lblError)
			textTextBox.Text = scrScroll.Value.ToString(cultNumber)
			Return
		Catch ex As System.Exception
			PegasusError(ex, lblError)
			textTextBox.Text = scrScroll.Value.ToString(cultNumber)
			Return
		End Try
		If (iTmp < scrScroll.Maximum) AndAlso (iTmp > scrScroll.Minimum) Then
			scrScroll.Value = iTmp
		Else
			iTmp = scrScroll.Value
		End If
		textTextBox.Text = iTmp.ToString(cultNumber)
	End Sub

#End Region
		
		Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

			'***Must call Unlock runtime to distribute app
            'ImagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

            strCurrentImage = System.IO.Path.Combine(strCurrentDir, "alpha1.jpg")
			ixproc1 = New PegasusImaging.WinForms.ImagXpress9.Processor(imagXpress1)
            strCurrentImage1 = System.IO.Path.Combine(strCurrentDir, "alpha1.tif")
            strCurrentImage2 = System.IO.Path.Combine(strCurrentDir, "alpha2.tif")
            strCurrentImage3 = System.IO.Path.Combine(strCurrentDir, "alpha3.tif")
			cmbAlpaha.SelectedIndex = 0
			imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage1)
			If System.IO.File.Exists(strCurrentImage) Then
				Try
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage)
					ixproc1.Image = imageXView1.Image
				Catch eX As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
					PegasusError(eX, lblError)
				End Try
			End If
		End Sub

		Private Sub ReloadImage()
			Try
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage)

			Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
				PegasusError(ex, lblError)
			End Try
		End Sub

		Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
			imagXpress1.AboutBox()
		End Sub

		Private Sub cmdAddAlpha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddAlpha.Click

			Try
				Dim alphaImage As PegasusImaging.WinForms.ImagXpress9.ImageX = imageXView2.Image.Copy
				ixproc1.Merge(alphaImage, MergeSize.Crop, MergeStyle.AlphaForeGroundOverBackGround, False, Color.Blue, 90, 90)
				If Not (alphaImage Is Nothing) Then
					alphaImage.Dispose()
					alphaImage = Nothing
				End If

			Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
				PegasusError(ex, lblError)
			End Try
		End Sub

		Private Sub mnuAlpha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAlpha.Click
			Dim strLoadResult As System.String = PegasusOpenFile()
			If Not (strLoadResult.Length = 0) Then
				strCurrentImage1 = strLoadResult
			End If


			'clear out the error before the next image load
			lblError.Text = ""

			Try

				Dim oldImage As PegasusImaging.WinForms.ImagXpress9.ImageX = imageXView2.Image
				imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage1)
				If Not (oldImage Is Nothing) Then
					oldImage.Dispose()
					oldImage = Nothing
				End If
			Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
				PegasusError(ex, lblError)
			End Try
		End Sub

		Private Sub cmbAlpaha_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAlpaha.SelectedIndexChanged

			'clear out the error before the next image load
			lblError.Text = ""

			Select Case cmbAlpaha.SelectedIndex

				Case 0
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage1)
					' break
				Case 1
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage2)
					' break
				Case 2
					imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage3)
					' break
			End Select
		End Sub

		Private Sub cmdReload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
			If System.IO.File.Exists(strCurrentImage) Then
				Try
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage)
				Catch eX As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
					PegasusError(eX, lblError)
				End Try
			End If
		End Sub

       
     
    End Class
End Namespace


/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo.ProcessorForms
{
    public partial class PolynomialWarpForm : Form
    {
        public PolynomialWarpForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();

        public ImageX CurrentImage
        {
            set
            {
                imageXViewCurrent.Image = value;
            }
        }

        public ImageX PreviewImage
        {
            get
            {
                return imageXViewPreview.Image;
            }
        }

        private void ProcessorForm_Load(object sender, EventArgs e)
        {
            try
            {
             
                
                unlock.UnlockControls(imagXpress1);


                ULBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                ULBox2.Maximum = imageXViewCurrent.Image.ImageXData.Height;
                URBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                URBox2.Maximum = imageXViewCurrent.Image.ImageXData.Height;
                LRBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                LRBox2.Maximum = imageXViewCurrent.Image.ImageXData.Height;
                LLBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                LLBox2.Maximum = imageXViewCurrent.Image.ImageXData.Height;

                ULBox.Value = imageXViewCurrent.Image.ImageXData.Width / 6;
                ULBox2.Value = imageXViewCurrent.Image.ImageXData.Height / 6;
                URBox.Value = imageXViewCurrent.Image.ImageXData.Width * 3 / 4;
                URBox2.Value = ULBox2.Value - imageXViewCurrent.Image.ImageXData.Height/8;
                LLBox.Value = ULBox.Value - imageXViewCurrent.Image.ImageXData.Height/9;
                LLBox2.Value = imageXViewCurrent.Image.ImageXData.Height * 3 / 4;
                LRBox.Value = URBox.Value - imageXViewCurrent.Image.ImageXData.Width/10;
                LRBox2.Value = LLBox2.Value + imageXViewCurrent.Image.ImageXData.Height /10;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            PreviewButton_Click(sender, e);

            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
                
                processor1.Image = imageXViewCurrent.Image.Copy();

                processor1.PolynomialWarp(new Point((int)ULBox.Value, (int)ULBox2.Value),
                    new Point((int)URBox.Value, (int)URBox2.Value),
                    new Point((int)LRBox.Value, (int)LRBox2.Value),
                    new Point((int)LLBox.Value, (int)LLBox2.Value));

                if (imageXViewPreview != null)
                {
                    imageXViewPreview.Image = processor1.Image;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void HelpProcessorButton_Click(object sender, EventArgs e)
        {
            Helper help = new Helper();

            help.ShowHelp(this, Text);
        }

        private void buttonBackgroundColor_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = buttonBackgroundColor.BackColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        processor1.BackgroundColor = dlg.Color;
                        buttonBackgroundColor.BackColor = dlg.Color;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
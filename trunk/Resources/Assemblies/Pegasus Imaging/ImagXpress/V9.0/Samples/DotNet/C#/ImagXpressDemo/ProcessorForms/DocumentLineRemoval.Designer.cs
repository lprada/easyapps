/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class DocumentLineRemovalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentLineRemovalForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.GapBox = new System.Windows.Forms.NumericUpDown();
            this.ThickBox = new System.Windows.Forms.NumericUpDown();
            this.AspectBox = new System.Windows.Forms.NumericUpDown();
            this.RepairBox = new System.Windows.Forms.NumericUpDown();
            this.LengthBox = new System.Windows.Forms.NumericUpDown();
            this.labelLinesFound = new System.Windows.Forms.Label();
            this.labelMaximumCharacterRepairSize = new System.Windows.Forms.Label();
            this.labelMaximumGap = new System.Windows.Forms.Label();
            this.labelMinimumAspectRatio = new System.Windows.Forms.Label();
            this.labelMaximumThickness = new System.Windows.Forms.Label();
            this.labelMinimumLength = new System.Windows.Forms.Label();
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.labelModifyResult = new System.Windows.Forms.Label();
            this.labelModified = new System.Windows.Forms.Label();
            this.labelCount = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GapBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThickBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AspectBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepairBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(400, 535);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(634, 535);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(516, 535);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // GapBox
            // 
            this.GapBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.GapBox.Location = new System.Drawing.Point(302, 448);
            this.GapBox.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.GapBox.Name = "GapBox";
            this.GapBox.Size = new System.Drawing.Size(44, 20);
            this.GapBox.TabIndex = 26;
            this.GapBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ThickBox
            // 
            this.ThickBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThickBox.Location = new System.Drawing.Point(151, 448);
            this.ThickBox.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.ThickBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ThickBox.Name = "ThickBox";
            this.ThickBox.Size = new System.Drawing.Size(48, 20);
            this.ThickBox.TabIndex = 25;
            this.ThickBox.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // AspectBox
            // 
            this.AspectBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AspectBox.Location = new System.Drawing.Point(160, 473);
            this.AspectBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.AspectBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.AspectBox.Name = "AspectBox";
            this.AspectBox.Size = new System.Drawing.Size(67, 20);
            this.AspectBox.TabIndex = 24;
            this.AspectBox.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // RepairBox
            // 
            this.RepairBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RepairBox.Location = new System.Drawing.Point(198, 499);
            this.RepairBox.Name = "RepairBox";
            this.RepairBox.Size = new System.Drawing.Size(67, 20);
            this.RepairBox.TabIndex = 23;
            this.RepairBox.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // LengthBox
            // 
            this.LengthBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LengthBox.Location = new System.Drawing.Point(132, 422);
            this.LengthBox.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.LengthBox.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.LengthBox.Name = "LengthBox";
            this.LengthBox.Size = new System.Drawing.Size(67, 20);
            this.LengthBox.TabIndex = 22;
            this.LengthBox.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // labelLinesFound
            // 
            this.labelLinesFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelLinesFound.AutoSize = true;
            this.labelLinesFound.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLinesFound.Location = new System.Drawing.Point(291, 522);
            this.labelLinesFound.Name = "labelLinesFound";
            this.labelLinesFound.Size = new System.Drawing.Size(14, 13);
            this.labelLinesFound.TabIndex = 21;
            this.labelLinesFound.Text = "0";
            this.labelLinesFound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMaximumCharacterRepairSize
            // 
            this.labelMaximumCharacterRepairSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMaximumCharacterRepairSize.AutoSize = true;
            this.labelMaximumCharacterRepairSize.Location = new System.Drawing.Point(39, 502);
            this.labelMaximumCharacterRepairSize.Name = "labelMaximumCharacterRepairSize";
            this.labelMaximumCharacterRepairSize.Size = new System.Drawing.Size(160, 13);
            this.labelMaximumCharacterRepairSize.TabIndex = 20;
            this.labelMaximumCharacterRepairSize.Text = "Maximum Character Repair Size:";
            // 
            // labelMaximumGap
            // 
            this.labelMaximumGap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMaximumGap.AutoSize = true;
            this.labelMaximumGap.Location = new System.Drawing.Point(219, 450);
            this.labelMaximumGap.Name = "labelMaximumGap";
            this.labelMaximumGap.Size = new System.Drawing.Size(77, 13);
            this.labelMaximumGap.TabIndex = 19;
            this.labelMaximumGap.Text = "Maximum Gap:";
            // 
            // labelMinimumAspectRatio
            // 
            this.labelMinimumAspectRatio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMinimumAspectRatio.AutoSize = true;
            this.labelMinimumAspectRatio.Location = new System.Drawing.Point(39, 476);
            this.labelMinimumAspectRatio.Name = "labelMinimumAspectRatio";
            this.labelMinimumAspectRatio.Size = new System.Drawing.Size(115, 13);
            this.labelMinimumAspectRatio.TabIndex = 18;
            this.labelMinimumAspectRatio.Text = "Minimum Aspect Ratio:";
            // 
            // labelMaximumThickness
            // 
            this.labelMaximumThickness.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMaximumThickness.AutoSize = true;
            this.labelMaximumThickness.Location = new System.Drawing.Point(39, 450);
            this.labelMaximumThickness.Name = "labelMaximumThickness";
            this.labelMaximumThickness.Size = new System.Drawing.Size(106, 13);
            this.labelMaximumThickness.TabIndex = 17;
            this.labelMaximumThickness.Text = "Maximum Thickness:";
            // 
            // labelMinimumLength
            // 
            this.labelMinimumLength.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMinimumLength.AutoSize = true;
            this.labelMinimumLength.Location = new System.Drawing.Point(39, 424);
            this.labelMinimumLength.Name = "labelMinimumLength";
            this.labelMinimumLength.Size = new System.Drawing.Size(87, 13);
            this.labelMinimumLength.TabIndex = 16;
            this.labelMinimumLength.Text = "Minimum Length:";
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(11, 542);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 27;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // labelModifyResult
            // 
            this.labelModifyResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelModifyResult.AutoSize = true;
            this.labelModifyResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifyResult.Location = new System.Drawing.Point(291, 547);
            this.labelModifyResult.Name = "labelModifyResult";
            this.labelModifyResult.Size = new System.Drawing.Size(37, 13);
            this.labelModifyResult.TabIndex = 58;
            this.labelModifyResult.Text = "False";
            this.labelModifyResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelModified
            // 
            this.labelModified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelModified.AutoSize = true;
            this.labelModified.Location = new System.Drawing.Point(158, 547);
            this.labelModified.Name = "labelModified";
            this.labelModified.Size = new System.Drawing.Size(103, 13);
            this.labelModified.TabIndex = 57;
            this.labelModified.Text = "Image was modified:";
            // 
            // labelCount
            // 
            this.labelCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCount.AutoSize = true;
            this.labelCount.Location = new System.Drawing.Point(158, 522);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(127, 13);
            this.labelCount.TabIndex = 59;
            this.labelCount.Text = "Count of Lines Removed:";
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(14, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 61;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(14, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 60;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // DocumentLineRemovalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 570);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.labelCount);
            this.Controls.Add(this.labelModifyResult);
            this.Controls.Add(this.labelModified);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.GapBox);
            this.Controls.Add(this.ThickBox);
            this.Controls.Add(this.AspectBox);
            this.Controls.Add(this.RepairBox);
            this.Controls.Add(this.LengthBox);
            this.Controls.Add(this.labelLinesFound);
            this.Controls.Add(this.labelMaximumCharacterRepairSize);
            this.Controls.Add(this.labelMaximumGap);
            this.Controls.Add(this.labelMinimumAspectRatio);
            this.Controls.Add(this.labelMaximumThickness);
            this.Controls.Add(this.labelMinimumLength);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DocumentLineRemovalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "DocumentLineRemoval";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GapBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThickBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AspectBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepairBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.NumericUpDown GapBox;
        private System.Windows.Forms.NumericUpDown ThickBox;
        private System.Windows.Forms.NumericUpDown AspectBox;
        private System.Windows.Forms.NumericUpDown RepairBox;
        private System.Windows.Forms.NumericUpDown LengthBox;
        private System.Windows.Forms.Label labelLinesFound;
        private System.Windows.Forms.Label labelMaximumCharacterRepairSize;
        private System.Windows.Forms.Label labelMaximumGap;
        private System.Windows.Forms.Label labelMinimumAspectRatio;
        private System.Windows.Forms.Label labelMaximumThickness;
        private System.Windows.Forms.Label labelMinimumLength;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.Label labelModifyResult;
        private System.Windows.Forms.Label labelModified;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress9;

namespace Print
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private System.Windows.Forms.ListBox DescriptionListBox;
		private System.Windows.Forms.Label ErrorLabel2;
		private System.Windows.Forms.Label ErrorLabel;
		private System.Windows.Forms.MainMenu MainMenu;
		private System.Windows.Forms.Button PrintButton1;
		private System.Windows.Forms.MenuItem FileMenu;
		private System.Windows.Forms.MenuItem OpenMenuItem;
		private System.Windows.Forms.MenuItem ExitMenuItem;
		private System.Windows.Forms.MenuItem ToolbarMenu;
		private System.Windows.Forms.MenuItem ShowMenuItem;
		private System.Windows.Forms.MenuItem AboutMenu;
		private System.Windows.Forms.MenuItem ImagXpressMenuItem;
		private System.Windows.Forms.Button PrintButton2;
        private System.Windows.Forms.Button PrintButton3;

		private bool ButtonOne;
        private bool ButtonTwo;
        private IContainer components;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (imageXView1.Image != null)
				{
					imageXView1.Image.Dispose();
					imageXView1.Image = null;
				}
				if (imageXView1 != null)
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.DescriptionListBox = new System.Windows.Forms.ListBox();
            this.ErrorLabel2 = new System.Windows.Forms.Label();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.OpenMenuItem = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.ToolbarMenu = new System.Windows.Forms.MenuItem();
            this.ShowMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.PrintButton1 = new System.Windows.Forms.Button();
            this.PrintButton2 = new System.Windows.Forms.Button();
            this.PrintButton3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(8, 64);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(336, 344);
            this.imageXView1.TabIndex = 0;
            // 
            // DescriptionListBox
            // 
            this.DescriptionListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionListBox.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1) Printing an image with ImagXpress."});
            this.DescriptionListBox.Location = new System.Drawing.Point(8, 8);
            this.DescriptionListBox.Name = "DescriptionListBox";
            this.DescriptionListBox.Size = new System.Drawing.Size(680, 43);
            this.DescriptionListBox.TabIndex = 1;
            // 
            // ErrorLabel2
            // 
            this.ErrorLabel2.Location = new System.Drawing.Point(357, 465);
            this.ErrorLabel2.Name = "ErrorLabel2";
            this.ErrorLabel2.Size = new System.Drawing.Size(80, 16);
            this.ErrorLabel2.TabIndex = 4;
            this.ErrorLabel2.Text = "Last Error:";
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorLabel.Location = new System.Drawing.Point(357, 502);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(328, 49);
            this.ErrorLabel.TabIndex = 5;
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.ToolbarMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.OpenMenuItem,
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // OpenMenuItem
            // 
            this.OpenMenuItem.Index = 0;
            this.OpenMenuItem.Text = "&Open";
            this.OpenMenuItem.Click += new System.EventHandler(this.OpenMenuItem_Click);
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 1;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // ToolbarMenu
            // 
            this.ToolbarMenu.Index = 1;
            this.ToolbarMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ShowMenuItem});
            this.ToolbarMenu.Text = "&Toolbar";
            // 
            // ShowMenuItem
            // 
            this.ShowMenuItem.Index = 0;
            this.ShowMenuItem.Text = "&Show";
            this.ShowMenuItem.Click += new System.EventHandler(this.ShowMenuItem_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 2;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ImagXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 0;
            this.ImagXpressMenuItem.Text = "Imag&Xpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // PrintButton1
            // 
            this.PrintButton1.Location = new System.Drawing.Point(80, 432);
            this.PrintButton1.Name = "PrintButton1";
            this.PrintButton1.Size = new System.Drawing.Size(200, 32);
            this.PrintButton1.TabIndex = 6;
            this.PrintButton1.Text = "Print Image Centered on Page";
            this.PrintButton1.Click += new System.EventHandler(this.PrintButton1_Click);
            // 
            // PrintButton2
            // 
            this.PrintButton2.Location = new System.Drawing.Point(80, 480);
            this.PrintButton2.Name = "PrintButton2";
            this.PrintButton2.Size = new System.Drawing.Size(200, 32);
            this.PrintButton2.TabIndex = 7;
            this.PrintButton2.Text = "Print 2 Images Centered on Page";
            this.PrintButton2.Click += new System.EventHandler(this.PrintButton2_Click);
            // 
            // PrintButton3
            // 
            this.PrintButton3.Location = new System.Drawing.Point(80, 528);
            this.PrintButton3.Name = "PrintButton3";
            this.PrintButton3.Size = new System.Drawing.Size(200, 32);
            this.PrintButton3.TabIndex = 8;
            this.PrintButton3.Text = "Print Image Fit to Page";
            this.PrintButton3.Click += new System.EventHandler(this.PrintButton3_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(696, 579);
            this.Controls.Add(this.PrintButton3);
            this.Controls.Add(this.PrintButton2);
            this.Controls.Add(this.PrintButton1);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.ErrorLabel2);
            this.Controls.Add(this.DescriptionListBox);
            this.Controls.Add(this.imageXView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		

		private void MainForm_Load(object sender, System.EventArgs e)
		{


			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.Licensing.UnlockRuntime(12345,12345,12345,12345);

            Application.EnableVisualStyles();

			try
			{
                imageXView1.Image = ImageX.FromFile(imagXpress1, Application.StartupPath + 
                        "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg");
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
			{
				ErrorLabel.Text = ex.Message;
			}
		}

		private void ImagXpressMenuItem_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void ExitMenuItem_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void ShowMenuItem_Click(object sender, System.EventArgs e)
		{
			if (ShowMenuItem.Checked == false)
			{
				imageXView1.Toolbar.Activated = true;
				ShowMenuItem.Checked = true;
			}
			else
			{
				imageXView1.Toolbar.Activated = false;
				ShowMenuItem.Checked = false;
			}
		}

		private void OpenMenuItem_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.InitialDirectory = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images";
			dlg.Filter = "All files(*.*)|*.*";
			dlg.FilterIndex = 0;
			dlg.Title = "Open Image";

			dlg.ShowDialog();

			if (dlg.FileName != "")
			{
				try
				{
                    imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, dlg.FileName);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
				{
					ErrorLabel.Text = ex.Message;
				}
			}
		}

		private void PrintButton1_Click(object sender, System.EventArgs e)
		{
            try
            {
                ButtonOne = true;
                HelperPrint();
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }
		}

        private void HelperPrint()
        {
            if (imageXView1.Image != null)
            {
                imageXView1.Image.Dispose();
                imageXView1.Image = null;
            }

            imageXView1.Image = ImageX.FromFile(imagXpress1, Application.StartupPath +
                        "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg");

            int width = imageXView1.Image.ImageXData.Width;
            int height = imageXView1.Image.ImageXData.Height;

            imageXView1.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch;

            double resx = Math.Round(imageXView1.Image.ImageXData.Resolution.Dimensions.Width);
            double resy = Math.Round(imageXView1.Image.ImageXData.Resolution.Dimensions.Height);

            using (ImageX img = new ImageX(imagXpress1, (int)(8.5 * resx), (int)(11 * resy), 24, Color.White))
            {
                using (Graphics g = img.GetGraphics())
                {
                    g.PageUnit = GraphicsUnit.Inch;

                    imageXView1.Image.Resolution.Units = GraphicsUnit.Inch;

                    if (ButtonOne == true)
                    {
                        imageXView1.Print(g, new RectangleF(
                             (float)(8.5 - width / resx) / 2,
                             (float)(11 - height / resy) / 2,
                             (float)(width / resx), (float)(height / resy)));
                    }
                    else if (ButtonTwo == true)
                    {
                        imageXView1.Print(g, new RectangleF((float)(8.5 - width / resx) / 2,
                            (float)(11 - 2 * height / resy),
                            (float)(width / resx),
                            (float)(height / resy)));
                        imageXView1.Print(g, new RectangleF((float)(8.5 - width / resx) / 2,
                            (float)(height / resy),
                            (float)(width / resx),
                            (float)(height / resy)));
                    }
                    else
                    {
                        imageXView1.Print(g, new RectangleF((float).25,(float).25, (float)8.25, (float)10.5));
                    }

                    ButtonOne = false;
                    ButtonTwo = false;

                    img.ReleaseGraphics();
                }

                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }

                imageXView1.Image = img.Copy();
            }
        }

		private void PrintButton2_Click(object sender, System.EventArgs e)
		{
            try
            {
                ButtonTwo = true;

                HelperPrint();
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }
		}

		private void PrintButton3_Click(object sender, System.EventArgs e)
		{
            try
            {
                HelperPrint();
            }
            catch (Exception ex)
            {
                ErrorLabel.Text = ex.Message;
            }
		}
	}
}

using System;
using PegasusImaging.WinForms.ImagXpress9;

namespace TIFFTags
{
	/// <summary>
	/// Summary description for TagInfoClass.
	/// </summary>
	public class TagInfo
	{
		private bool myWriteable = false;
		private int  myTagNumber = 0;
		private int  myItemCount; //-1 == N == dependant on another tag
		private string myTagName = "";
		private string myTagDescription = "";
		private PegasusImaging.WinForms.ImagXpress9.TagTypes	myTagType;
		
		/// <summary>
		/// The constructor for the TagInfo class. It assumes all values are correct
		/// </summary>
		/// <param name="name">Tag name. Example: ImageWidth</param>
		/// <param name="number">Tag number. Example: 256</param>
		/// <param name="count">Tag Item Count. Use -1 for N(Dependant on another value)</param>
		/// <param name="type">Tag Data Type. Example: SHORT</param>
		/// <param name="writeable">Is it valid to change/write this tag?</param>
		private TagInfo(string name, int number, int count, TagTypes type, bool writeable)
		{
			myWriteable = writeable;
			myTagType = type;
			myItemCount = count;
			myTagNumber = number;
			myTagName = name;		
		}

		/// <summary>
		/// The constructor for the TagInfo class. It assumes all values are correct
		/// </summary>
		/// <param name="name">Tag name. Example: ImageWidth</param>
		/// <param name="number">Tag number. Example: 256</param>
		/// <param name="count">Tag Item Count. Use -1 for N(Dependant on another value)</param>
		/// <param name="type">Tag Data Type. Example: SHORT</param>
		/// <param name="writeable">Is it valid to change/write this tag?</param>
		/// <param name="description">Description of the tag</param>
		private TagInfo(string name, int number, int count, TagTypes type, bool writeable, string description)
		{
			myWriteable = writeable;
			myTagType = type;
			myItemCount = count;
			myTagNumber = number;
			myTagName = name;
			myTagDescription = description;			
		}

		#region properties
		public int ItemCount
		{
			get
			{
				return myItemCount;
			}
		}

		public int Number
		{
			get
			{
				return myTagNumber;
			}
		}

		public string Name
		{
			get
			{
				return myTagName;
			}
		}

		public bool Writeable
		{
			get
			{
				return myWriteable;
			}
		}	

		public PegasusImaging.WinForms.ImagXpress9.TagTypes Type
		{
			get
			{
				return myTagType;
			}
		}

		public string Description
		{
			get
			{
				return myTagDescription;
			}
		}
		#endregion

		#region public tag information finding functions
		/// <summary>
		/// Grabs information about a tag based on its number
		/// </summary>
		/// <param name="TagNumber">The tag number</param>
		/// <returns>TagInfo describing the tag, or null if the tag isn't found</returns>
		public static TagInfo GetTag(int TagNumber)
		{
			foreach (TagInfo theTag in myTagListing)
			{
				if (theTag.Number == TagNumber)
					return theTag;
			}
			return null;
		}
		
		/// <summary>
		/// Grabs information about a tag based on its name
		/// </summary>
		/// <param name="TagName">The tag name</param>
		/// <returns>TagInfo describing the tag, or null if the tag is not found</returns>
		public static TagInfo GetTag(string TagName)
		{
			foreach (TagInfo theTag in myTagListing)
			{
				if (theTag.Name.Equals(TagName))
					return theTag;
			}
			return null;
		}
		#endregion

		#region static tiff tag list

		private static TagInfo[] myTagListing = {
			new TagInfo("NewSubfileType",				0x00fe,	1,	TagTypes.Long,		false,	"A general indication of the kind of data contained in this subfile. Replaces the old SubfileType field, due to limitations in the definition of that field. NewSubfileType is mainly useful when there are multiple subfiles in a single TIFF file."),
			new TagInfo("SubfileType",					0x00ff,	1,	TagTypes.Short,		false,	"A general indication of the kind of data contained in this subfile. The specification defines these values: 1 = full-resolution image data; 2 = reduced-resolution image data; 3 = a single page of a multi-page image (see the PageNumber field description)."),
			new TagInfo("ImageWidth",					0x0100, 1,  TagTypes.Short,		false,	"Image Width"),
			new TagInfo("ImageLength",					0x0101, 1,  TagTypes.Short,		false,	"Image Height"),
			new TagInfo("BitsPerSample",				0x0102, 3,  TagTypes.Short,		false,	"Number of Bits Per Component"),
			new TagInfo("Compression",					0x0103, 1,  TagTypes.Short,		false,	"Compression scheme used on the image data."),
			new TagInfo("PhotometricInterpretation",	0x0106, 1,  TagTypes.Short,		false,	"The color space of the image data."),
			new TagInfo("Threshholding",				0x0107, 1,  TagTypes.Short,		false,	"For black and white TIFF files that represent shades of gray, the technique used to convert from gray to black and white pixels."),        
			new TagInfo("CellWidth",					0x0108, 1,  TagTypes.Short,		false,	"The width of the dithering or halftoning matrix used to create a dithered or halftoned bilevel file."),        
			new TagInfo("CellLength",					0x0109, 1,  TagTypes.Short,		false,	"The length of the dithering or halftoning matrix used to create a dithered or halftoned bilevel file."),        
			new TagInfo("FillOrder",					0x010a,-1,  TagTypes.Short,		false,	"The logical order of bits within a byte."),
			new TagInfo("DocumentName",					0x010d,-1,  TagTypes.Ascii,		true,	"The name of the document from which this image was scanned."),
			new TagInfo("ImageDescription",				0x010e,-1,  TagTypes.Ascii,		true,	"Title"),
			new TagInfo("Make",							0x010f,-1,  TagTypes.Ascii,		true,	"Equipment Make"),
			new TagInfo("Model",						0x0110,-1,  TagTypes.Ascii,		true,	"Camera Model"),
			new TagInfo("StripOffsets",					0x0111,-1,  TagTypes.Short,		false,	"Image Data Location"),
			new TagInfo("Orientation",					0x0112, 1,  TagTypes.Short,		false,	"Image Orientation"),
			new TagInfo("SamplesPerPixel",				0x0115, 1,  TagTypes.Short,		false,	"Number of Components"),
			new TagInfo("RowsPerStrip",					0x0116, 1,  TagTypes.Short,		false,	"Number of Rows Per Strip"),
			new TagInfo("StripByteCounts",				0x0117,-1,  TagTypes.Short,		false,	"Bytes per Compressed Strip"),
			new TagInfo("MinSampleValue",				0x0118,-1,  TagTypes.Short,		false,	"The minimum component value used."),
			new TagInfo("MaxSampleValue",				0x0119,-1,  TagTypes.Short,		false,	"The maximum component value used."),
			new TagInfo("XResolution",					0x011a, 1,  TagTypes.Rational,  true,	"Horizontal Resolution"),
			new TagInfo("YResolution",					0x011b, 1,  TagTypes.Rational,  true,	"Vertical Resolution"),
			new TagInfo("PlanarConfiguration",			0x011c, 1,  TagTypes.Short,		false,	"Image Data Arrangement"),
			new TagInfo("ResolutionUnit",				0x0128, 1,  TagTypes.Short,		false,	"Resolution Unit"),
			new TagInfo("TransferFunction",				0x012d,-1,  TagTypes.Short,		false,	"Transfer Function"),
			new TagInfo("Software",						0x0131,-1,  TagTypes.Ascii,		true,	"Camera Software"),
			new TagInfo("DateTime",						0x0132, 20, TagTypes.Ascii,		true,	"Image Creation Date"),
			new TagInfo("Artist",						0x013b,-1,  TagTypes.Ascii,		true,	"Photographer"),
			new TagInfo("WhitePoint",					0x013e, 2,  TagTypes.Rational,	false,	"White Point Chromaticity"),
			new TagInfo("PrimaryChromaticities",		0x013f, 6,  TagTypes.Rational,	false,	"Chromaticities of Primary Colors"),
			new TagInfo("SubIFDs",						0x014A,-1,	TagTypes.Long,		false,	"Offset to child IFDs. Each value is an offset (from the beginning of the TIFF file, as always) to a child IFD. Child images provide extra information for the parent image - such as a subsampled version of the parent image."),
			new TagInfo("TransferRange",				0x0156,-1,  TagTypes.Short,		false,	"Expands the range of the TransferFunction."),
			new TagInfo("JPEGProc",						0x0200,-1,  TagTypes.Short,		false,	"Old-style JPEG compression field. TechNote2 invalidates this part of the specification."),
			new TagInfo("JPEGInterchangeFormat",		0x0201, 1,  TagTypes.Long,		false,	"Offset to JPEG SOI"),
			new TagInfo("JPEGInterchangeFormatLength",	0x0202, 1,  TagTypes.Long,		false,	"Bytes of JPEG Data"),
			new TagInfo("YCbCrCoefficients",			0x0211, 3,  TagTypes.Rational,	false,	"Color Space Xform Matrix Coeff's"),
			new TagInfo("YCbCrSubSampling",				0x0212, 2,  TagTypes.Short,		false,	"Chrominance Comp Samp Ratio"),
			new TagInfo("YCbCrPositioning",				0x0213, 1,  TagTypes.Short,		false,	"Chrominance Comp Positioning"),
			new TagInfo("ReferenceBlackWhite",			0x0214, 6,  TagTypes.Rational,	false,	"Black and White Ref Point Values"),
			new TagInfo("CFARepeatPatternDim",			0x828d,-1,  TagTypes.Undefine,	false,	""),
			new TagInfo("CFAPattern",					0x828e,-1,  TagTypes.Undefine,	false,	""),
			new TagInfo("BatteryLevel",					0x828f,-1,  TagTypes.Undefine,	false,	""),
			new TagInfo("Copyright",					0x8298,-1,  TagTypes.Ascii,		true,	"Copyright"),
			new TagInfo("ExposureTime",					0x829a,	1,	TagTypes.Rational,	true,	"Exposure time, given in seconds."),
			new TagInfo("FNumber",						0x829d,	1,	TagTypes.Rational,	false,	"The F number."),
			new TagInfo("IPTCmetadata",					0x83bb,-1,  TagTypes.Byte,		false,	"IPTC (International Press Telecommunications Council) metadata."),
			new TagInfo("Photoshop",					0x8649,-1,	TagTypes.Byte,		false,	"Collection of Photoshop 'Image Resource Blocks'."),
			new TagInfo("ExifOffset",					0x8769, 1,  TagTypes.Long,		false,	"Exif IFD Pointer"),
			new TagInfo("InterColorProfile",			0x8773,-1,  TagTypes.Undefine,	false,	"ICC profile data."),
			new TagInfo("ExposureProgram",				0x8822,	1,	TagTypes.Short,		false,	"The class of the program used by the camera to set exposure when the picture is taken."),
			new TagInfo("SpectralSensitivity",			0x8824,-1,  TagTypes.Ascii,		false,	"Indicates the spectral sensitivity of each channel of the camera used. The tag value is an ASCII string compatible with the standard developed by the ASTM Technical committee."),
			new TagInfo("GPSInfo",						0x8825, 1,  TagTypes.Long,		false,	"GPS Info IFD Pointer"),
			new TagInfo("ISOSpeedRatings",				0x8827,-1,  TagTypes.Short,		false,	"Indicates the ISO Speed and ISO Latitude of the camera or input device as specified in ISO 12232."),
			new TagInfo("OECF",							0x8828,-1,  TagTypes.Undefine,	false,	"Indicates the Opto-Electric Conversion Function (OECF) specified in ISO 14524. OECF is the relationship between the camera optical input and the image values."),
			new TagInfo("ExifVersion",					0x9000, 4,  TagTypes.Undefine,	false,	"The version of the supported Exif standard. Nonexistence of this field is taken to mean nonconformance to the Exif standard. All four bytes should be interpreted as ASCII values. The first two bytes encode the upper part of the standard version, the next two bytes encode the lower part. For example, the byte sequence 48, 50, 50, 48, is the equivalent of the ASCII value \"0220\", and denotes version 2.20."),
			new TagInfo("DateTimeOriginal",				0x9003, 20, TagTypes.Ascii,		true,	"The date and time when the original image data was generated. For a digital still camera, this is the date and time the picture was taken or recorded. The format is \"YYYY:MM:DD HH:MM:SS\" with time shown in 24-hour format, and the date and time separated by one blank character (hex 20). When the date and time are unknown, all the character spaces except colons (\":\") may be filled with blank characters, or else the Interoperability field may be filled with blank characters. The character string length is 20 bytes including NULL for termination. When the field is left blank, it is treated as unknown."),
			new TagInfo("DateTimeDigitized",			0x9004, 20, TagTypes.Ascii,		true,	"The date and time when the image was stored as digital data. If, for example, an image was captured by a digital still camera, and at the same time the file was recorded, then the DateTimeOriginal and DateTimeDigitized will have the same contents. The format is \"YYYY:MM:DD HH:MM:SS\" with time shown in 24-hour format, and the date and time separated by one blank character (hex 20). When the date and time are unknown, all the character spaces except colons (\":\") may be filled with blank characters, or else the Interoperability field may be filled with blank characters. The character string length is 20 bytes including NULL for termination. When the field is left blank, it is treated as unknown."),
			new TagInfo("ComponentsConfiguration",		0x9101, 4,  TagTypes.Undefine,	false,	"Specific to compressed data; specifies the channels and complements PhotometricInterpretation. Information specific to compressed data. The channels of each component are arranged in order from the 1st component to the 4th. For uncompressed data the data arrangement is given in the PhotometricInterpretation tag. However, since PhotometricInterpretation can only express the order of Y,Cb and Cr, this tag is provided for cases when compressed data uses components other than Y, Cb, and Cr and to enable support of other sequences."),
			new TagInfo("CompressedBitsPerPixel",		0x9102, 1,  TagTypes.Rational,	false,	"Specific to compressed data; states the compressed bits per pixel. The compression mode used for a compressed image is indicated in unit bits per pixel."),
			new TagInfo("ShutterSpeedValue",			0x9201,	1,	TagTypes.Srational,	false,	"Shutter speed. The unit is the APEX (Additive System of Photographic Exposure) setting."),
			new TagInfo("ApertureValue",				0x9202,	1,	TagTypes.Rational,	false,	"The lens aperture. The unit is the APEX (Additive System of Photographic Exposure) setting."),
			new TagInfo("BrightnessValue",				0x9203,	1,	TagTypes.Srational,	true,	"The value of brightness. The unit is the APEX (Additive System of Photographic Exposure) setting. Ordinarily it is given in the range of -99.99 to 99.99. Note that if the numerator of the recorded value is hex FFFFFFFF, Unknown shall be indicated."),
			new TagInfo("ExposureBiasValue",			0x9204, 1,	TagTypes.Srational,	true,	"The exposure bias. The unit is the APEX (Additive System of Photographic Exposure) setting. Ordinarily it is given in the range of -99.99 to 99.99."),
			new TagInfo("MaxApertureValue",				0x9205,	1,	TagTypes.Rational,	false,	"The smallest F number of the lens. The unit is the APEX (Additive System of Photographic Exposure) setting. Ordinarily it is given in the range of 00.00 to 99.99, but it is not limited to this range."),
			new TagInfo("SubjectDistance",				0x9206,	1,	TagTypes.Rational,	true,	"The distance to the subject, given in meters. Note that if the numerator of the recorded value is hex FFFFFFFF, Infinity shall be indicated; and if the numerator is 0, Distance unknown shall be indicated."),
			new TagInfo("MeteringMode",					0x9207, 1,  TagTypes.Short,		false,	"The metering mode. The specification defines these values: 0 = Unknown; 1 = Average; 2 = CenterWeightedAverage; 3 = Spot; 4 = MultiSpot; 5 = Pattern; 6 = Partial; 255 = other"),
			new TagInfo("LightSource",					0x9208, 1,  TagTypes.Short,		false,	"The kind of light source."),
			new TagInfo("Flash",						0x9209, 1,  TagTypes.Short,		false,	"Indicates the status of flash when the image was shot. Bit 0 indicates the flash firing status, bits 1 and 2 indicate the flash return status, bits 3 and 4 indicate the flash mode, bit 5 indicates whether the flash function is present, and bit 6 indicates \"red eye\" mode."),
			new TagInfo("FocalLength",					0x920a, 1,  TagTypes.Rational,	true,	"The actual focal length of the lens, in mm. Conversion is not made to the focal length of a 35 mm film camera."),
			new TagInfo("SubjectArea",					0x9214,-1,  TagTypes.Short,		false,	"Indicates the location and area of the main subject in the overall scene."),
			new TagInfo("MakerNote",					0x927c,-1,  TagTypes.Undefine,	false,	"Manufacturer specific information. A tag for manufacturers of Exif writers to record any desired information. The contents are up to the manufacturer, but this tag should not be used for any other than its intended purpose."),
			new TagInfo("UserComment",					0x9286,-1,  TagTypes.Undefine,	false,	"Keywords or comments on the image; complements ImageDescription."),
			new TagInfo("SubsecTime",					0x9290,-1,  TagTypes.Ascii,		true,	"A tag used to record fractions of seconds for the DateTime tag."),
			new TagInfo("SubsecTimeOrginal",			0x9291,-1,  TagTypes.Ascii,		true,	"A tag used to record fractions of seconds for the DateTimeOriginal tag."),
			new TagInfo("SubsecTimeDigitized",			0x9292,-1,  TagTypes.Ascii,		true,	"A tag used to record fractions of seconds for the DateTimeDigitized tag."),
			new TagInfo("FlashPixVersion",				0xa000, 4,  TagTypes.Undefine,	false,	"The Flashpix format version supported by a FPXR file."),
			new TagInfo("ColorSpace",					0xa001, 1,  TagTypes.Short,		false,	"The color space information tag is always recorded as the color space specifier. Normally sRGB (=1) is used to define the color space based on the PC monitor conditions and environment. If a color space other than sRGB is used, Uncalibrated (=65535) is set. Image data recorded as Uncalibrated can be treated as sRGB when it is converted to Flashpix."),
			new TagInfo("PixelXDimension",				0xa002, 1,  TagTypes.Short,		false,	"Specific to compressed data; the valid width of the meaningful image."),
			new TagInfo("PixelYDimension",				0xa003, 1,  TagTypes.Short,		false,	"Specific to compressed data; the valid height of the meaningful image."),
			new TagInfo("RelatedSoundFile",				0xa004, 13, TagTypes.Ascii,		true,	"Used to record the name of an audio file related to the image data. This tag is used to record the name of an audio file related to the image data. The only relational information recorded here is the Exif audio file name and extension (an ASCII string consisting of 8 characters + '.' + 3 characters). The path is not recorded."),
			new TagInfo("InteroperabilityOffset",		0xa005, 1,  TagTypes.Long,		false,	"Interoperability IFD Pointer"),
			new TagInfo("FlashEnergy",					0xa20b, 1,  TagTypes.Rational,	false,	"Indicates the strobe energy at the time the image is captured, as measured in Beam Candle Power Seconds"),
			new TagInfo("SpatialFrequencyResponse",		0xa20c,-1,  TagTypes.Undefine,	false,	"Records the camera or input device spatial frequency table and SFR values in the direction of image width, image height, and diagonal direction, as specified in ISO 12233."),
			new TagInfo("FocalPlaneXResolution",		0xa20e, 1,  TagTypes.Rational,	false,	"Indicates the number of pixels in the image width (X) direction per FocalPlaneResolutionUnit on the camera focal plane."),
			new TagInfo("FocalPlaneYResolution",		0xa20f, 1,  TagTypes.Rational,	false,	"Indicates the number of pixels in the image height (Y) direction per FocalPlaneResolutionUnit on the camera focal plane."),
			new TagInfo("FocalPlaneResolutionUnit",		0xa210, 1,  TagTypes.Short,		false,	"Indicates the unit for measuring FocalPlaneXResolution and FocalPlaneYResolution."),
			new TagInfo("SubjectLocation",				0xa214, 2,  TagTypes.Short,		false,	"Indicates the location of the main subject in the scene. The value of this tag represents the pixel at the center of the main subject relative to the left edge, prior to rotation processing as per the Rotation tag. The first value indicates the X column number and second indicates the Y row number. When a camera records the main subject location, it is recommended that the SubjectArea tag be used instead of this tag."),
			new TagInfo("ExposureIndex",				0xa215,	1,	TagTypes.Rational,	true,	"Indicates the exposure index selected on the camera or input device at the time the image is captured."),
			new TagInfo("SensingMethod",				0xa217, 1,  TagTypes.Short,		false,	"ndicates the image sensor type on the camera or input device. The specification defines these values: 1 = Not defined; 2 = One-chip color area sensor; 3 = Two-chip color area sensor; 4 = Three-chip color area sensor; 5 = Color sequential area sensor; 7 = Trilinear sensor; 8 = Color sequential linear sensor."),
			new TagInfo("FileSource",					0xa300, 1,  TagTypes.Undefine,	false,	"Indicates the image source. If a DSC (Digital Still Camera) recorded the image, this tag will always be set to 3, indicating that the image was recorded on a DSC."),
			new TagInfo("SceneType",					0xa301, 1,  TagTypes.Undefine,	false,	"Indicates the type of scene. If a DSC recorded the image, this tag value shall always be set to 1, indicating that the image was directly photographed."),
			new TagInfo("CFAPattern",					0xa302,-1,  TagTypes.Undefine,	false,	"Indicates the color filter array (CFA) geometric pattern of the image sensor when a one-chip color area sensor is used."),
			new TagInfo("CustomRendered",				0xa401, 1,  TagTypes.Short,		false,	"Indicates the use of special processing on image data, such as rendering geared to output. When special processing is performed, the reader is expected to disable or minimize any further processing."),
			new TagInfo("ExposureMode",					0xa402,	1,	TagTypes.Short,		false,	"Indicates the exposure mode set when the image was shot. In auto-bracketing mode, the camera shoots a series of frames of the same scene at different exposure settings."),
			new TagInfo("WhiteBalance",					0xa403, 1,  TagTypes.Short,		false,	"Indicates the white balance mode set when the image was shot."),
			new TagInfo("DigitalZoomRatio",				0xa404, 1,  TagTypes.Rational,	false,	"Indicates the digital zoom ratio when the image was shot. If the numerator of the recorded value is 0, this indicates that digital zoom was not used."),
			new TagInfo("FocalLengthIn35mmFilm",		0xa405, 1,	TagTypes.Short,		true,	"Indicates the equivalent focal length assuming a 35mm film camera, in mm. A value of 0 means the focal length is unknown. Note that this tag differs from the FocalLength tag."),
			new TagInfo("SceneCaptureType",				0xa406, 1,  TagTypes.Short,		false,	"Indicates the type of scene that was shot. It can also be used to record the mode in which the image was shot. Note that this differs from the SceneType tag."),
			new TagInfo("GainControl",					0xa407, 1,  TagTypes.Short,		false,	"Indicates the degree of overall image gain adjustment."),
			new TagInfo("Contrast",						0xa408, 1,  TagTypes.Short,		false,	"Indicates the direction of contrast processing applied by the camera when the image was shot."),
			new TagInfo("Saturation",					0xa409, 1,  TagTypes.Short,		false,	"Indicates the direction of saturation processing applied by the camera when the image was shot."),
			new TagInfo("Sharpness",					0xa40a, 1,  TagTypes.Short,		false,	"Indicates the direction of sharpness processing applied by the camera when the image was shot."),
			new TagInfo("DeviceSettingDescr",			0xa40b,-1,  TagTypes.Undefine,	false,	"This tag indicates information on the picture-taking conditions of a particular camera model."),
			new TagInfo("SubjectDistRange",				0xa40c, 1,  TagTypes.Short,		false,	"Indicates the distance to the subject."),
			new TagInfo("ImageUniqueID",				0xa420, 33, TagTypes.Ascii,		true,	"Unique Image ID (hexadecimal notation 128bit fixed length)"),
			new TagInfo("GammaCoefficient",				0xa500, 1,  TagTypes.Rational,	false,	"Gamma Coefficient"),
			new TagInfo("Unknown",						0xffff,-1,  TagTypes.Undefine,	false,	"No such tag")};

		#endregion

	}
}

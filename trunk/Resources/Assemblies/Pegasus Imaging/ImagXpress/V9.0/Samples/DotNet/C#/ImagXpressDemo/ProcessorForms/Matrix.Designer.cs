/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class MatrixForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MatrixForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.Box22 = new System.Windows.Forms.NumericUpDown();
            this.Box21 = new System.Windows.Forms.NumericUpDown();
            this.Box20 = new System.Windows.Forms.NumericUpDown();
            this.Box12 = new System.Windows.Forms.NumericUpDown();
            this.Box11 = new System.Windows.Forms.NumericUpDown();
            this.Box10 = new System.Windows.Forms.NumericUpDown();
            this.Box02 = new System.Windows.Forms.NumericUpDown();
            this.Box01 = new System.Windows.Forms.NumericUpDown();
            this.Box00 = new System.Windows.Forms.NumericUpDown();
            this.IntensityBox = new System.Windows.Forms.NumericUpDown();
            this.DivisorBox = new System.Windows.Forms.NumericUpDown();
            this.labelIntensity = new System.Windows.Forms.Label();
            this.labelDivisor = new System.Windows.Forms.Label();
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.MatrixLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Box22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntensityBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DivisorBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(400, 523);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(634, 523);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(516, 523);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // Box22
            // 
            this.Box22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box22.Location = new System.Drawing.Point(284, 469);
            this.Box22.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box22.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box22.Name = "Box22";
            this.Box22.Size = new System.Drawing.Size(71, 20);
            this.Box22.TabIndex = 34;
            this.Box22.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // Box21
            // 
            this.Box21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box21.Location = new System.Drawing.Point(202, 469);
            this.Box21.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box21.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box21.Name = "Box21";
            this.Box21.Size = new System.Drawing.Size(71, 20);
            this.Box21.TabIndex = 33;
            this.Box21.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // Box20
            // 
            this.Box20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box20.Location = new System.Drawing.Point(125, 469);
            this.Box20.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box20.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box20.Name = "Box20";
            this.Box20.Size = new System.Drawing.Size(71, 20);
            this.Box20.TabIndex = 32;
            this.Box20.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Box12
            // 
            this.Box12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box12.Location = new System.Drawing.Point(284, 443);
            this.Box12.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box12.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box12.Name = "Box12";
            this.Box12.Size = new System.Drawing.Size(71, 20);
            this.Box12.TabIndex = 31;
            this.Box12.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // Box11
            // 
            this.Box11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box11.Location = new System.Drawing.Point(202, 443);
            this.Box11.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box11.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box11.Name = "Box11";
            this.Box11.Size = new System.Drawing.Size(71, 20);
            this.Box11.TabIndex = 30;
            this.Box11.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // Box10
            // 
            this.Box10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box10.Location = new System.Drawing.Point(125, 443);
            this.Box10.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box10.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box10.Name = "Box10";
            this.Box10.Size = new System.Drawing.Size(71, 20);
            this.Box10.TabIndex = 29;
            this.Box10.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Box02
            // 
            this.Box02.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box02.Location = new System.Drawing.Point(284, 417);
            this.Box02.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box02.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box02.Name = "Box02";
            this.Box02.Size = new System.Drawing.Size(71, 20);
            this.Box02.TabIndex = 28;
            this.Box02.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // Box01
            // 
            this.Box01.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box01.Location = new System.Drawing.Point(202, 417);
            this.Box01.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box01.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box01.Name = "Box01";
            this.Box01.Size = new System.Drawing.Size(71, 20);
            this.Box01.TabIndex = 27;
            this.Box01.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // Box00
            // 
            this.Box00.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Box00.Location = new System.Drawing.Point(125, 417);
            this.Box00.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Box00.Minimum = new decimal(new int[] {
            32767,
            0,
            0,
            -2147483648});
            this.Box00.Name = "Box00";
            this.Box00.Size = new System.Drawing.Size(71, 20);
            this.Box00.TabIndex = 26;
            this.Box00.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // IntensityBox
            // 
            this.IntensityBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IntensityBox.Location = new System.Drawing.Point(300, 512);
            this.IntensityBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.IntensityBox.Name = "IntensityBox";
            this.IntensityBox.Size = new System.Drawing.Size(55, 20);
            this.IntensityBox.TabIndex = 25;
            this.IntensityBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // DivisorBox
            // 
            this.DivisorBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DivisorBox.Location = new System.Drawing.Point(172, 512);
            this.DivisorBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.DivisorBox.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.DivisorBox.Name = "DivisorBox";
            this.DivisorBox.Size = new System.Drawing.Size(55, 20);
            this.DivisorBox.TabIndex = 24;
            this.DivisorBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // labelIntensity
            // 
            this.labelIntensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelIntensity.AutoSize = true;
            this.labelIntensity.Location = new System.Drawing.Point(245, 514);
            this.labelIntensity.Name = "labelIntensity";
            this.labelIntensity.Size = new System.Drawing.Size(49, 13);
            this.labelIntensity.TabIndex = 23;
            this.labelIntensity.Text = "Intensity:";
            // 
            // labelDivisor
            // 
            this.labelDivisor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDivisor.AutoSize = true;
            this.labelDivisor.Location = new System.Drawing.Point(124, 514);
            this.labelDivisor.Name = "labelDivisor";
            this.labelDivisor.Size = new System.Drawing.Size(42, 13);
            this.labelDivisor.TabIndex = 22;
            this.labelDivisor.Text = "Divisor:";
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(11, 523);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 35;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // MatrixLabel
            // 
            this.MatrixLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MatrixLabel.AutoSize = true;
            this.MatrixLabel.Location = new System.Drawing.Point(53, 424);
            this.MatrixLabel.Name = "MatrixLabel";
            this.MatrixLabel.Size = new System.Drawing.Size(58, 13);
            this.MatrixLabel.TabIndex = 36;
            this.MatrixLabel.Text = "3x3 Matrix:";
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(14, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 49;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(14, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 48;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // MatrixForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 558);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.MatrixLabel);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.Box22);
            this.Controls.Add(this.Box21);
            this.Controls.Add(this.Box20);
            this.Controls.Add(this.Box12);
            this.Controls.Add(this.Box11);
            this.Controls.Add(this.Box10);
            this.Controls.Add(this.Box02);
            this.Controls.Add(this.Box01);
            this.Controls.Add(this.Box00);
            this.Controls.Add(this.IntensityBox);
            this.Controls.Add(this.DivisorBox);
            this.Controls.Add(this.labelIntensity);
            this.Controls.Add(this.labelDivisor);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MatrixForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Matrix";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Box22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Box00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IntensityBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DivisorBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.NumericUpDown Box22;
        private System.Windows.Forms.NumericUpDown Box21;
        private System.Windows.Forms.NumericUpDown Box20;
        private System.Windows.Forms.NumericUpDown Box12;
        private System.Windows.Forms.NumericUpDown Box11;
        private System.Windows.Forms.NumericUpDown Box10;
        private System.Windows.Forms.NumericUpDown Box02;
        private System.Windows.Forms.NumericUpDown Box01;
        private System.Windows.Forms.NumericUpDown Box00;
        private System.Windows.Forms.NumericUpDown IntensityBox;
        private System.Windows.Forms.NumericUpDown DivisorBox;
        private System.Windows.Forms.Label labelIntensity;
        private System.Windows.Forms.Label labelDivisor;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.Label MatrixLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Rubberbanding
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Rubberbanding : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView2;
	
		private System.Windows.Forms.Label lblLoadStatus;
		private System.Windows.Forms.ListBox lstStatus;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem4;

		private String strCurrentDir;
		private String imgSourceFileName; 
		private String imgDestFileName;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuFileOpenSource;
		private System.Windows.Forms.MenuItem mnuFileOpenDest;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout; 

		private System.Drawing.Point pointBegin;
		private System.Drawing.Point pointEnd;
		private bool				 banding;
        PegasusImaging.WinForms.ImagXpress9.ImageX tempcopy;
        private IContainer components;

		public Rubberbanding()
		{

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				//Dispose of the ImagXpress,ImageXView and tempcopy ImageX objects
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}

				if (imageXView1 != null)
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}

				if (imageXView2 != null)
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}

				if (tempcopy != null)
				{
					tempcopy.Dispose();
					tempcopy = null;
				}
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXView2 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.lblLoadStatus = new System.Windows.Forms.Label();
            this.lstStatus = new System.Windows.Forms.ListBox();
            this.lstInfo = new System.Windows.Forms.ListBox();
            this.lblLastError = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.mnuFile = new System.Windows.Forms.MenuItem();
            this.mnuFileOpenSource = new System.Windows.Forms.MenuItem();
            this.mnuFileOpenDest = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuFileQuit = new System.Windows.Forms.MenuItem();
            this.mnuToolbar = new System.Windows.Forms.MenuItem();
            this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // imagXpress1
            // 
            this.imagXpress1.ProgressEvent += new PegasusImaging.WinForms.ImagXpress9.ImagXpress.ProgressEventHandler(this.ProgressEventHandler);
            this.imagXpress1.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress9.ImagXpress.ImageStatusEventHandler(this.ImageStatusEventHandler);
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.imageXView1.Location = new System.Drawing.Point(8, 72);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(280, 252);
            this.imageXView1.TabIndex = 0;
            this.imageXView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageXView1_MouseUp);
            this.imageXView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageXView1_MouseDown);
            this.imageXView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageXView1_MouseMove);
            // 
            // imageXView2
            // 
            this.imageXView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView2.Location = new System.Drawing.Point(296, 72);
            this.imageXView2.MouseWheelCapture = false;
            this.imageXView2.Name = "imageXView2";
            this.imageXView2.Size = new System.Drawing.Size(280, 252);
            this.imageXView2.TabIndex = 1;
            // 
            // lblLoadStatus
            // 
            this.lblLoadStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoadStatus.Location = new System.Drawing.Point(584, 72);
            this.lblLoadStatus.Name = "lblLoadStatus";
            this.lblLoadStatus.Size = new System.Drawing.Size(104, 24);
            this.lblLoadStatus.TabIndex = 2;
            this.lblLoadStatus.Text = "Load Status:";
            // 
            // lstStatus
            // 
            this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstStatus.Location = new System.Drawing.Point(584, 96);
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.Size = new System.Drawing.Size(232, 134);
            this.lstStatus.TabIndex = 3;
            // 
            // lstInfo
            // 
            this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstInfo.Items.AddRange(new object[] {
            "This sample demonstrates using the Rubberband property of the ImageXView class to" +
                "",
            "draw a rubberband on the Left ImageXView control and then using the Mouse events " +
                "to copy",
            "the selected portion to the Right ImagXView control"});
            this.lstInfo.Location = new System.Drawing.Point(8, 8);
            this.lstInfo.Name = "lstInfo";
            this.lstInfo.Size = new System.Drawing.Size(808, 56);
            this.lstInfo.TabIndex = 4;
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(584, 244);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(120, 16);
            this.lblLastError.TabIndex = 5;
            this.lblLastError.Text = "Last Error:";
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.Location = new System.Drawing.Point(584, 268);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(232, 56);
            this.lblError.TabIndex = 6;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFile,
            this.mnuToolbar,
            this.mnuAbout});
            // 
            // mnuFile
            // 
            this.mnuFile.Index = 0;
            this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileOpenSource,
            this.mnuFileOpenDest,
            this.menuItem4,
            this.mnuFileQuit});
            this.mnuFile.Text = "&File";
            // 
            // mnuFileOpenSource
            // 
            this.mnuFileOpenSource.Index = 0;
            this.mnuFileOpenSource.Text = "&Open Source Image";
            this.mnuFileOpenSource.Click += new System.EventHandler(this.mnuFileOpenSource_Click);
            // 
            // mnuFileOpenDest
            // 
            this.mnuFileOpenDest.Index = 1;
            this.mnuFileOpenDest.Text = "Open &Destination Image";
            this.mnuFileOpenDest.Click += new System.EventHandler(this.mnuFileOpenDest_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "-";
            // 
            // mnuFileQuit
            // 
            this.mnuFileQuit.Index = 3;
            this.mnuFileQuit.Text = "&Quit";
            this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
            // 
            // mnuToolbar
            // 
            this.mnuToolbar.Index = 1;
            this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuToolbarShow});
            this.mnuToolbar.Text = "&Toolbar";
            // 
            // mnuToolbarShow
            // 
            this.mnuToolbarShow.Index = 0;
            this.mnuToolbarShow.Text = "&Show";
            this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 2;
            this.mnuAbout.Text = "&About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // Rubberbanding
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(824, 329);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lstInfo);
            this.Controls.Add(this.lstStatus);
            this.Controls.Add(this.lblLoadStatus);
            this.Controls.Add(this.imageXView2);
            this.Controls.Add(this.imageXView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "Rubberbanding";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rubberbanding C# Sample";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

		}
		#endregion

		
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		string GetFileName(System.String FullName) 
		{
			return (FullName.Substring(FullName.LastIndexOf("\\")+1,FullName.Length - FullName.LastIndexOf("\\") - 1));
		}
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{

			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}
	
		#endregion


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Rubberbanding());
		}


		private void imageXView1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{

			try
			{
				pointEnd.X = e.X;
				pointEnd.Y = e.Y;
				imageXView1.Rubberband.Update(pointEnd);
				banding = false;
				//obtain the rubberbanded area via the Copy
				tempcopy  = imageXView1.Rubberband.Copy();
				//Create a processor object
				PegasusImaging.WinForms.ImagXpress9.Processor prc = new PegasusImaging.WinForms.ImagXpress9.Processor(imagXpress1, imageXView2.Image);
				//Set the Area to copy the rubberbanded area to
				prc.SetArea(imageXView1.Rubberband.Dimensions);
				//Merge the selected portion to a new ImagXView control
				if(tempcopy != null)
				{
					prc.Merge(ref tempcopy, PegasusImaging.WinForms.ImagXpress9.MergeSize.Crop,PegasusImaging.WinForms.ImagXpress9.MergeStyle.Normal,false,System.Drawing.Color.Black,0,0);
				}
				}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
		}

		private void imageXView1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (banding)
			{
				pointEnd.X = e.X;
				pointEnd.Y = e.Y;
			}
			imageXView1.Rubberband.Update(pointEnd);
		
		}

		private void imageXView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			pointBegin.X = e.X;
			pointBegin.Y = e.Y;
			banding = true;
			imageXView1.Rubberband.Enabled = true;
			imageXView1.Rubberband.Start(pointBegin,false);

		}

		private void mnuFileOpenDest_Click(object sender, System.EventArgs e)
		{
			try
			{
				string theNewFile = PegasusOpenFile();
				if (theNewFile.Length != 0)
				{

					//clear out any error before the next operation
					lblError.Text = "";

					imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, theNewFile);
					imgDestFileName = theNewFile;
				}

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
		}

		private void mnuFileOpenSource_Click(object sender, System.EventArgs e)
		{
			try
			{
				string theNewFile = PegasusOpenFile();
				if (theNewFile.Length != 0)
				{

					//clear out any error before the next operation
					lblError.Text = "";


					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, theNewFile);
					imgSourceFileName = theNewFile;
				}
			
			}
			catch(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
			{	
				PegasusError(ex,lblError);
			}
		}
		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			this.mnuToolbarShow.Text = (imageXView1.Toolbar.Activated)? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
				imageXView2.Toolbar.Activated = !imageXView2.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			try 
			{
			imagXpress1.AboutBox();
	
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			
			// **The UnlockRuntime function must be called to distribute the runtime**
			// imagXpress1.Licensing.UnlockRuntime(12345, 12345, 12345, 12345);
			
			//Set the default directory to the common images directory
			strCurrentDir = System.IO.Path.Combine(System.Environment.CurrentDirectory,strCommonImagesDirectory);

			System.String strCurrentdir = System.IO.Directory.GetCurrentDirectory ().ToString();

			imgSourceFileName = System.IO.Path.Combine (strCurrentdir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\Images\window.jpg");
			imgDestFileName = System.IO.Path.Combine (strCurrentdir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\Images\vermont.jpg");

			try 
			{
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, imgSourceFileName);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			try 
			{
				imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, imgDestFileName);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}

			pointBegin = new System.Drawing.Point();
			pointEnd = new System.Drawing.Point();
			banding = false;
		}
	
		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress9.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString());

			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress9.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString() + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes.ToString() + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}
	}
}

/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class MetaDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (imageXView1 != null)
                {
                    if (imageXView1.Image != null)
                    {
                        imageXView1.Image.Dispose();
                        imageXView1.Image = null;
                    }

                    imageXView1.Dispose();
                    imageXView1 = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch { }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MetaDataForm));
            this.listViewTags = new System.Windows.Forms.ListView();
            this.DescriptionColumn = new System.Windows.Forms.ColumnHeader();
            this.NumberColumn = new System.Windows.Forms.ColumnHeader();
            this.TypeColumn = new System.Windows.Forms.ColumnHeader();
            this.CountColumn = new System.Windows.Forms.ColumnHeader();
            this.DataColumn = new System.Windows.Forms.ColumnHeader();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.listViewThumbnail = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.OKButton = new System.Windows.Forms.Button();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.previousButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.pageTextBox = new System.Windows.Forms.TextBox();
            this.pageLabel = new System.Windows.Forms.Label();
            this.countLabel = new System.Windows.Forms.Label();
            this.thumbDescriptionLabel = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.TabRoll = new System.Windows.Forms.TabControl();
            this.TagsTab = new System.Windows.Forms.TabPage();
            this.ThumbnailTab = new System.Windows.Forms.TabPage();
            this.CommentTab = new System.Windows.Forms.TabPage();
            this.IPTCTab = new System.Windows.Forms.TabPage();
            this.listViewIPTC = new System.Windows.Forms.ListView();
            this.descriptionIPTCColumn = new System.Windows.Forms.ColumnHeader();
            this.dataTypeColumn = new System.Windows.Forms.ColumnHeader();
            this.dataSetColumn = new System.Windows.Forms.ColumnHeader();
            this.dataValueColumn = new System.Windows.Forms.ColumnHeader();
            this.calsTab = new System.Windows.Forms.TabPage();
            this.listViewCals = new System.Windows.Forms.ListView();
            this.calsNameColumn = new System.Windows.Forms.ColumnHeader();
            this.calsDataColumn = new System.Windows.Forms.ColumnHeader();
            this.TabRoll.SuspendLayout();
            this.TagsTab.SuspendLayout();
            this.ThumbnailTab.SuspendLayout();
            this.CommentTab.SuspendLayout();
            this.IPTCTab.SuspendLayout();
            this.calsTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewTags
            // 
            this.listViewTags.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.DescriptionColumn,
            this.NumberColumn,
            this.TypeColumn,
            this.CountColumn,
            this.DataColumn});
            this.listViewTags.FullRowSelect = true;
            this.listViewTags.Location = new System.Drawing.Point(3, 45);
            this.listViewTags.Name = "listViewTags";
            this.listViewTags.Size = new System.Drawing.Size(720, 590);
            this.listViewTags.TabIndex = 1;
            this.listViewTags.UseCompatibleStateImageBehavior = false;
            this.listViewTags.View = System.Windows.Forms.View.Details;
            // 
            // DescriptionColumn
            // 
            this.DescriptionColumn.Text = "Description";
            this.DescriptionColumn.Width = 160;
            // 
            // NumberColumn
            // 
            this.NumberColumn.Text = "Number";
            // 
            // TypeColumn
            // 
            this.TypeColumn.Text = "Type";
            // 
            // CountColumn
            // 
            this.CountColumn.Text = "Count";
            // 
            // DataColumn
            // 
            this.DataColumn.Text = "Data";
            this.DataColumn.Width = 500;
            // 
            // listViewThumbnail
            // 
            this.listViewThumbnail.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listViewThumbnail.FullRowSelect = true;
            this.listViewThumbnail.Location = new System.Drawing.Point(9, 6);
            this.listViewThumbnail.Name = "listViewThumbnail";
            this.listViewThumbnail.Size = new System.Drawing.Size(717, 290);
            this.listViewThumbnail.TabIndex = 2;
            this.listViewThumbnail.UseCompatibleStateImageBehavior = false;
            this.listViewThumbnail.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Description";
            this.columnHeader1.Width = 160;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Number";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Type";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Count";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Data";
            this.columnHeader5.Width = 500;
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(207, 357);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(328, 271);
            this.imageXView1.TabIndex = 3;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(319, 695);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // commentTextBox
            // 
            this.commentTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentTextBox.Location = new System.Drawing.Point(3, 3);
            this.commentTextBox.Multiline = true;
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.commentTextBox.Size = new System.Drawing.Size(726, 645);
            this.commentTextBox.TabIndex = 7;
            // 
            // previousButton
            // 
            this.previousButton.Enabled = false;
            this.previousButton.Location = new System.Drawing.Point(211, 16);
            this.previousButton.Name = "previousButton";
            this.previousButton.Size = new System.Drawing.Size(108, 23);
            this.previousButton.TabIndex = 10;
            this.previousButton.Text = "Previous page";
            this.previousButton.UseVisualStyleBackColor = true;
            this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Enabled = false;
            this.nextButton.Location = new System.Drawing.Point(325, 16);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(108, 23);
            this.nextButton.TabIndex = 11;
            this.nextButton.Text = "Next Page";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // pageTextBox
            // 
            this.pageTextBox.Location = new System.Drawing.Point(484, 19);
            this.pageTextBox.Name = "pageTextBox";
            this.pageTextBox.ReadOnly = true;
            this.pageTextBox.Size = new System.Drawing.Size(46, 20);
            this.pageTextBox.TabIndex = 12;
            this.pageTextBox.Text = "1";
            // 
            // pageLabel
            // 
            this.pageLabel.AutoSize = true;
            this.pageLabel.Location = new System.Drawing.Point(443, 22);
            this.pageLabel.Name = "pageLabel";
            this.pageLabel.Size = new System.Drawing.Size(35, 13);
            this.pageLabel.TabIndex = 13;
            this.pageLabel.Text = "Page:";
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Location = new System.Drawing.Point(536, 22);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(25, 13);
            this.countLabel.TabIndex = 14;
            this.countLabel.Text = "of 1";
            // 
            // thumbDescriptionLabel
            // 
            this.thumbDescriptionLabel.AutoSize = true;
            this.thumbDescriptionLabel.Location = new System.Drawing.Point(177, 318);
            this.thumbDescriptionLabel.Name = "thumbDescriptionLabel";
            this.thumbDescriptionLabel.Size = new System.Drawing.Size(0, 13);
            this.thumbDescriptionLabel.TabIndex = 15;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(607, 695);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(122, 23);
            this.saveButton.TabIndex = 16;
            this.saveButton.Text = "Save Info to Clipboard";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // TabRoll
            // 
            this.TabRoll.Controls.Add(this.TagsTab);
            this.TabRoll.Controls.Add(this.ThumbnailTab);
            this.TabRoll.Controls.Add(this.CommentTab);
            this.TabRoll.Controls.Add(this.IPTCTab);
            this.TabRoll.Controls.Add(this.calsTab);
            this.TabRoll.Location = new System.Drawing.Point(8, 12);
            this.TabRoll.Name = "TabRoll";
            this.TabRoll.SelectedIndex = 0;
            this.TabRoll.Size = new System.Drawing.Size(740, 677);
            this.TabRoll.TabIndex = 17;
            // 
            // TagsTab
            // 
            this.TagsTab.Controls.Add(this.previousButton);
            this.TagsTab.Controls.Add(this.listViewTags);
            this.TagsTab.Controls.Add(this.countLabel);
            this.TagsTab.Controls.Add(this.nextButton);
            this.TagsTab.Controls.Add(this.pageLabel);
            this.TagsTab.Controls.Add(this.pageTextBox);
            this.TagsTab.Location = new System.Drawing.Point(4, 22);
            this.TagsTab.Name = "TagsTab";
            this.TagsTab.Padding = new System.Windows.Forms.Padding(3);
            this.TagsTab.Size = new System.Drawing.Size(732, 651);
            this.TagsTab.TabIndex = 0;
            this.TagsTab.Text = "TIFF/EXIF Tags";
            this.TagsTab.UseVisualStyleBackColor = true;
            // 
            // ThumbnailTab
            // 
            this.ThumbnailTab.Controls.Add(this.listViewThumbnail);
            this.ThumbnailTab.Controls.Add(this.thumbDescriptionLabel);
            this.ThumbnailTab.Controls.Add(this.imageXView1);
            this.ThumbnailTab.Location = new System.Drawing.Point(4, 22);
            this.ThumbnailTab.Name = "ThumbnailTab";
            this.ThumbnailTab.Padding = new System.Windows.Forms.Padding(3);
            this.ThumbnailTab.Size = new System.Drawing.Size(732, 651);
            this.ThumbnailTab.TabIndex = 1;
            this.ThumbnailTab.Text = "Thumbnail";
            this.ThumbnailTab.UseVisualStyleBackColor = true;
            // 
            // CommentTab
            // 
            this.CommentTab.Controls.Add(this.commentTextBox);
            this.CommentTab.Location = new System.Drawing.Point(4, 22);
            this.CommentTab.Name = "CommentTab";
            this.CommentTab.Padding = new System.Windows.Forms.Padding(3);
            this.CommentTab.Size = new System.Drawing.Size(732, 651);
            this.CommentTab.TabIndex = 2;
            this.CommentTab.Text = "JPEG Comment";
            this.CommentTab.UseVisualStyleBackColor = true;
            // 
            // IPTCTab
            // 
            this.IPTCTab.Controls.Add(this.listViewIPTC);
            this.IPTCTab.Location = new System.Drawing.Point(4, 22);
            this.IPTCTab.Name = "IPTCTab";
            this.IPTCTab.Padding = new System.Windows.Forms.Padding(3);
            this.IPTCTab.Size = new System.Drawing.Size(732, 651);
            this.IPTCTab.TabIndex = 3;
            this.IPTCTab.Text = "IPTC";
            this.IPTCTab.UseVisualStyleBackColor = true;
            // 
            // listViewIPTC
            // 
            this.listViewIPTC.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.descriptionIPTCColumn,
            this.dataTypeColumn,
            this.dataSetColumn,
            this.dataValueColumn});
            this.listViewIPTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewIPTC.FullRowSelect = true;
            this.listViewIPTC.Location = new System.Drawing.Point(3, 3);
            this.listViewIPTC.Name = "listViewIPTC";
            this.listViewIPTC.Size = new System.Drawing.Size(726, 645);
            this.listViewIPTC.TabIndex = 0;
            this.listViewIPTC.UseCompatibleStateImageBehavior = false;
            this.listViewIPTC.View = System.Windows.Forms.View.Details;
            // 
            // descriptionIPTCColumn
            // 
            this.descriptionIPTCColumn.Text = "Description";
            this.descriptionIPTCColumn.Width = 184;
            // 
            // dataTypeColumn
            // 
            this.dataTypeColumn.Text = "Data Set Type";
            this.dataTypeColumn.Width = 106;
            // 
            // dataSetColumn
            // 
            this.dataSetColumn.Text = "Data Set";
            this.dataSetColumn.Width = 77;
            // 
            // dataValueColumn
            // 
            this.dataValueColumn.Text = "Data";
            this.dataValueColumn.Width = 354;
            // 
            // calsTab
            // 
            this.calsTab.Controls.Add(this.listViewCals);
            this.calsTab.Location = new System.Drawing.Point(4, 22);
            this.calsTab.Name = "calsTab";
            this.calsTab.Padding = new System.Windows.Forms.Padding(3);
            this.calsTab.Size = new System.Drawing.Size(732, 651);
            this.calsTab.TabIndex = 4;
            this.calsTab.Text = "CALS";
            this.calsTab.UseVisualStyleBackColor = true;
            // 
            // listViewCals
            // 
            this.listViewCals.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.calsNameColumn,
            this.calsDataColumn});
            this.listViewCals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewCals.FullRowSelect = true;
            this.listViewCals.Location = new System.Drawing.Point(3, 3);
            this.listViewCals.Name = "listViewCals";
            this.listViewCals.Size = new System.Drawing.Size(726, 645);
            this.listViewCals.TabIndex = 1;
            this.listViewCals.UseCompatibleStateImageBehavior = false;
            this.listViewCals.View = System.Windows.Forms.View.Details;
            // 
            // calsNameColumn
            // 
            this.calsNameColumn.Text = "ID";
            this.calsNameColumn.Width = 134;
            // 
            // calsDataColumn
            // 
            this.calsDataColumn.Text = "Data";
            this.calsDataColumn.Width = 580;
            // 
            // MetaDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 730);
            this.Controls.Add(this.TabRoll);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MetaDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Meta Data";
            this.Load += new System.EventHandler(this.MetaDataForm_Load);
            this.TabRoll.ResumeLayout(false);
            this.TagsTab.ResumeLayout(false);
            this.TagsTab.PerformLayout();
            this.ThumbnailTab.ResumeLayout(false);
            this.ThumbnailTab.PerformLayout();
            this.CommentTab.ResumeLayout(false);
            this.CommentTab.PerformLayout();
            this.IPTCTab.ResumeLayout(false);
            this.calsTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewTags;
        private System.Windows.Forms.ColumnHeader DescriptionColumn;
        private System.Windows.Forms.ColumnHeader NumberColumn;
        private System.Windows.Forms.ColumnHeader TypeColumn;
        private System.Windows.Forms.ColumnHeader CountColumn;
        private System.Windows.Forms.ColumnHeader DataColumn;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.ListView listViewThumbnail;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.Button previousButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.TextBox pageTextBox;
        private System.Windows.Forms.Label pageLabel;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.Label thumbDescriptionLabel;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TabControl TabRoll;
        private System.Windows.Forms.TabPage TagsTab;
        private System.Windows.Forms.TabPage ThumbnailTab;
        private System.Windows.Forms.TabPage CommentTab;
        private System.Windows.Forms.TabPage IPTCTab;
        private System.Windows.Forms.ListView listViewIPTC;
        private System.Windows.Forms.ColumnHeader dataTypeColumn;
        private System.Windows.Forms.ColumnHeader dataSetColumn;
        private System.Windows.Forms.ColumnHeader dataValueColumn;
        private System.Windows.Forms.ColumnHeader descriptionIPTCColumn;
        private System.Windows.Forms.TabPage calsTab;
        private System.Windows.Forms.ListView listViewCals;
        private System.Windows.Forms.ColumnHeader calsNameColumn;
        private System.Windows.Forms.ColumnHeader calsDataColumn;
    }
}
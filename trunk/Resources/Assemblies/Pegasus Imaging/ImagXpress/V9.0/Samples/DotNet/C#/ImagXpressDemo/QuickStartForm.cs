/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace ImagXpressDemo
{
    public partial class QuickStartForm : Form
    {
        public QuickStartForm()
        {
            InitializeComponent();
        }

        Helper helper = new Helper();

        public bool ShowHelp
        {
            get
            {
                return startCheckBox.Checked;
            }
            set
            {
                startCheckBox.Checked = value;
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if (startCheckBox.Checked == true)
            {
                using (TextWriter tw = new StreamWriter(helper.quickStartFile))
                {
                    tw.WriteLine(helper.gettingStartedKey);
                }
            }
            else
            {
                if (File.Exists(helper.quickStartFile) == true)
                {
                    File.Delete(helper.quickStartFile);
                }
            }

            DialogResult = DialogResult.OK;

            Close();
        }
    }
}
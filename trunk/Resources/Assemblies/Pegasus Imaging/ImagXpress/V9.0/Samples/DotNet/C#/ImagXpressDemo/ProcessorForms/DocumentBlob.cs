/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo.ProcessorForms
{
    public partial class DocumentBlobForm : Form
    {
        public DocumentBlobForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();

        public ImageX CurrentImage
        {
            set
            {
                imageXViewCurrent.Image = value;
            }
        }

        public ImageX PreviewImage
        {
            get
            {
                return imageXViewPreview.Image;
            }
        }

        private void ProcessorForm_Load(object sender, EventArgs e)
        {
            try
            {
             
                
                unlock.UnlockControls(imagXpress1);


                LBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                WBox.Maximum = imageXViewCurrent.Image.ImageXData.Width;
                TBox.Maximum = imageXViewCurrent.Image.ImageXData.Height;
                HBox.Maximum = imageXViewCurrent.Image.ImageXData.Height;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            PreviewButton_Click(sender, e);

            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
                
                processor1.Image = imageXViewCurrent.Image.Copy();

                processor1.DocumentBlobRemoval(new Rectangle((int)LBox.Value, (int)TBox.Value, (int)WBox.Value, 
                    (int)HBox.Value), (int)MinBox.Value, (int)MaxBox.Value, (short)DensityBox.Value);

                labelBlobsFound.Text = processor1.CountOfObjectsFound.ToString();
                labelModifyResult.Text = processor1.ImageWasModified.ToString();

                if (imageXViewPreview != null)
                {
                    imageXViewPreview.Image = processor1.Image;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void HelpProcessorButton_Click(object sender, EventArgs e)
        {
            Helper help = new Helper();

            help.ShowHelp(this, Text);
        }

        private void MinBox_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (MinBox.Value > MaxBox.Value)
                {
                    MinBox.Value = MaxBox.Value - 1;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void MaxBox_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (MaxBox.Value < MinBox.Value)
                {
                    MaxBox.Value = MinBox.Value + 1;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
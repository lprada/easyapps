/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class ScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (foreColorDialog != null)
                {
                    foreColorDialog.Dispose();
                    foreColorDialog = null;
                }
                if (fontDialog != null)
                {
                    fontDialog.Dispose();
                    fontDialog = null;
                }
                if (imageXView1 != null)
                {
                    if (imageXView1.Image != null)
                    {
                        imageXView1.Image.Dispose();
                        imageXView1.Image = null;
                    }

                    imageXView1.Dispose();
                    imageXView1 = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (twainDevice != null)
                {
                    twainDevice.CloseSession();
                    twainDevice.Dispose();
                    twainDevice = null;
                }
                if (twainPro1 != null)
                {
                    twainPro1.Dispose();
                    twainPro1 = null;
                }
                if (ScannedImages != null)
                {
                    for (int i = 0; i < ScannedImages.Count; i++)
                    {
                        if (ScannedImages[i] != null)
                        {
                            ScannedImages[i].Dispose();
                            ScannedImages[i] = null;
                        }
                    }
                }
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
            }
            catch {};

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScanForm));
            this.sourceButton = new System.Windows.Forms.Button();
            this.scanButton = new System.Windows.Forms.Button();
            this.CancelScanButton = new System.Windows.Forms.Button();
            this.UICheckBox = new System.Windows.Forms.CheckBox();
            this.groupBoxCaption = new System.Windows.Forms.GroupBox();
            this.updateCaptionButton = new System.Windows.Forms.Button();
            this.foreColorButton = new System.Windows.Forms.Button();
            this.comboBoxVAlign = new System.Windows.Forms.ComboBox();
            this.comboBoxHAlign = new System.Windows.Forms.ComboBox();
            this.ForeColorLabel = new System.Windows.Forms.Label();
            this.textBoxCaption = new System.Windows.Forms.TextBox();
            this.textBoxCapHeight = new System.Windows.Forms.TextBox();
            this.fontButton = new System.Windows.Forms.Button();
            this.textBoxCapWidth = new System.Windows.Forms.TextBox();
            this.FontLabel = new System.Windows.Forms.Label();
            this.textBoxCapTop = new System.Windows.Forms.TextBox();
            this.textBoxCapLeft = new System.Windows.Forms.TextBox();
            this.labelVAlign = new System.Windows.Forms.Label();
            this.labelHAlign = new System.Windows.Forms.Label();
            this.labelCaption = new System.Windows.Forms.Label();
            this.labelCapHeight = new System.Windows.Forms.Label();
            this.labelCapWidth = new System.Windows.Forms.Label();
            this.labelCapTop = new System.Windows.Forms.Label();
            this.labelCapLeft = new System.Windows.Forms.Label();
            this.checkBoxShadowText = new System.Windows.Forms.CheckBox();
            this.checkBoxClipCaption = new System.Windows.Forms.CheckBox();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.groupBoxLayout = new System.Windows.Forms.GroupBox();
            this.textBoxIB = new System.Windows.Forms.TextBox();
            this.textBoxIR = new System.Windows.Forms.TextBox();
            this.textBoxIT = new System.Windows.Forms.TextBox();
            this.textBoxIL = new System.Windows.Forms.TextBox();
            this.buttonLayoutUpdate = new System.Windows.Forms.Button();
            this.bottomLabel = new System.Windows.Forms.Label();
            this.rightLabel = new System.Windows.Forms.Label();
            this.topLabel = new System.Windows.Forms.Label();
            this.leftLabel = new System.Windows.Forms.Label();
            this.groupBoxCaps = new System.Windows.Forms.GroupBox();
            this.stepLabel = new System.Windows.Forms.Label();
            this.labelStep = new System.Windows.Forms.Label();
            this.MaxValueLabel = new System.Windows.Forms.Label();
            this.MinValueLabel = new System.Windows.Forms.Label();
            this.DefaultValueLabel = new System.Windows.Forms.Label();
            this.UnitsValueLabel = new System.Windows.Forms.Label();
            this.ValuesLabel = new System.Windows.Forms.Label();
            this.updateCapabilityButton = new System.Windows.Forms.Button();
            this.labelUnits = new System.Windows.Forms.Label();
            this.labelDefault = new System.Windows.Forms.Label();
            this.labelMax = new System.Windows.Forms.Label();
            this.labelMin = new System.Windows.Forms.Label();
            this.textBoxCurrent = new System.Windows.Forms.TextBox();
            this.labelCurrent = new System.Windows.Forms.Label();
            this.listBoxCaps = new System.Windows.Forms.ListBox();
            this.comboBoxCapabilites = new System.Windows.Forms.ComboBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.countLabel = new System.Windows.Forms.Label();
            this.pageLabel = new System.Windows.Forms.Label();
            this.pageTextBox = new System.Windows.Forms.TextBox();
            this.nextButton = new System.Windows.Forms.Button();
            this.previousButton = new System.Windows.Forms.Button();
            this.twainPro1 = new PegasusImaging.WinForms.TwainPro5.TwainPro(this.components);
            this.groupBoxCaption.SuspendLayout();
            this.groupBoxLayout.SuspendLayout();
            this.groupBoxCaps.SuspendLayout();
            this.SuspendLayout();
            // 
            // sourceButton
            // 
            this.sourceButton.Location = new System.Drawing.Point(84, 12);
            this.sourceButton.Name = "sourceButton";
            this.sourceButton.Size = new System.Drawing.Size(110, 23);
            this.sourceButton.TabIndex = 0;
            this.sourceButton.Text = "Select Scanner";
            this.sourceButton.UseVisualStyleBackColor = true;
            this.sourceButton.Click += new System.EventHandler(this.sourceButton_Click);
            // 
            // scanButton
            // 
            this.scanButton.Enabled = false;
            this.scanButton.Location = new System.Drawing.Point(636, 659);
            this.scanButton.Name = "scanButton";
            this.scanButton.Size = new System.Drawing.Size(97, 23);
            this.scanButton.TabIndex = 1;
            this.scanButton.Text = "Acquire Image";
            this.scanButton.UseVisualStyleBackColor = true;
            this.scanButton.Click += new System.EventHandler(this.scanButton_Click);
            // 
            // CancelScanButton
            // 
            this.CancelScanButton.Location = new System.Drawing.Point(739, 659);
            this.CancelScanButton.Name = "CancelScanButton";
            this.CancelScanButton.Size = new System.Drawing.Size(97, 23);
            this.CancelScanButton.TabIndex = 2;
            this.CancelScanButton.Text = "Cancel";
            this.CancelScanButton.UseVisualStyleBackColor = true;
            this.CancelScanButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // UICheckBox
            // 
            this.UICheckBox.AutoSize = true;
            this.UICheckBox.Enabled = false;
            this.UICheckBox.Location = new System.Drawing.Point(84, 42);
            this.UICheckBox.Name = "UICheckBox";
            this.UICheckBox.Size = new System.Drawing.Size(123, 17);
            this.UICheckBox.TabIndex = 3;
            this.UICheckBox.Text = "Show User Interface";
            this.UICheckBox.UseVisualStyleBackColor = true;
            this.UICheckBox.CheckedChanged += new System.EventHandler(this.UICheckBox_CheckedChanged);
            // 
            // groupBoxCaption
            // 
            this.groupBoxCaption.Controls.Add(this.updateCaptionButton);
            this.groupBoxCaption.Controls.Add(this.foreColorButton);
            this.groupBoxCaption.Controls.Add(this.comboBoxVAlign);
            this.groupBoxCaption.Controls.Add(this.comboBoxHAlign);
            this.groupBoxCaption.Controls.Add(this.ForeColorLabel);
            this.groupBoxCaption.Controls.Add(this.textBoxCaption);
            this.groupBoxCaption.Controls.Add(this.textBoxCapHeight);
            this.groupBoxCaption.Controls.Add(this.fontButton);
            this.groupBoxCaption.Controls.Add(this.textBoxCapWidth);
            this.groupBoxCaption.Controls.Add(this.FontLabel);
            this.groupBoxCaption.Controls.Add(this.textBoxCapTop);
            this.groupBoxCaption.Controls.Add(this.textBoxCapLeft);
            this.groupBoxCaption.Controls.Add(this.labelVAlign);
            this.groupBoxCaption.Controls.Add(this.labelHAlign);
            this.groupBoxCaption.Controls.Add(this.labelCaption);
            this.groupBoxCaption.Controls.Add(this.labelCapHeight);
            this.groupBoxCaption.Controls.Add(this.labelCapWidth);
            this.groupBoxCaption.Controls.Add(this.labelCapTop);
            this.groupBoxCaption.Controls.Add(this.labelCapLeft);
            this.groupBoxCaption.Controls.Add(this.checkBoxShadowText);
            this.groupBoxCaption.Controls.Add(this.checkBoxClipCaption);
            this.groupBoxCaption.Enabled = false;
            this.groupBoxCaption.Location = new System.Drawing.Point(12, 169);
            this.groupBoxCaption.Name = "groupBoxCaption";
            this.groupBoxCaption.Size = new System.Drawing.Size(261, 220);
            this.groupBoxCaption.TabIndex = 4;
            this.groupBoxCaption.TabStop = false;
            this.groupBoxCaption.Text = "Caption";
            // 
            // updateCaptionButton
            // 
            this.updateCaptionButton.Location = new System.Drawing.Point(173, 190);
            this.updateCaptionButton.Name = "updateCaptionButton";
            this.updateCaptionButton.Size = new System.Drawing.Size(79, 25);
            this.updateCaptionButton.TabIndex = 35;
            this.updateCaptionButton.Text = "Update";
            this.updateCaptionButton.Click += new System.EventHandler(this.updateCaptionButton_Click);
            // 
            // foreColorButton
            // 
            this.foreColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.foreColorButton.Location = new System.Drawing.Point(72, 191);
            this.foreColorButton.Name = "foreColorButton";
            this.foreColorButton.Size = new System.Drawing.Size(28, 23);
            this.foreColorButton.TabIndex = 33;
            this.foreColorButton.UseVisualStyleBackColor = true;
            this.foreColorButton.Click += new System.EventHandler(this.foreColorButton_Click);
            // 
            // comboBoxVAlign
            // 
            this.comboBoxVAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVAlign.Items.AddRange(new object[] {
            "Top",
            "Bottom",
            "Center"});
            this.comboBoxVAlign.Location = new System.Drawing.Point(128, 108);
            this.comboBoxVAlign.Name = "comboBoxVAlign";
            this.comboBoxVAlign.Size = new System.Drawing.Size(80, 21);
            this.comboBoxVAlign.TabIndex = 15;
            // 
            // comboBoxHAlign
            // 
            this.comboBoxHAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHAlign.Items.AddRange(new object[] {
            "Left",
            "Right",
            "Center"});
            this.comboBoxHAlign.Location = new System.Drawing.Point(128, 60);
            this.comboBoxHAlign.Name = "comboBoxHAlign";
            this.comboBoxHAlign.Size = new System.Drawing.Size(80, 21);
            this.comboBoxHAlign.TabIndex = 14;
            // 
            // ForeColorLabel
            // 
            this.ForeColorLabel.AutoSize = true;
            this.ForeColorLabel.Location = new System.Drawing.Point(19, 196);
            this.ForeColorLabel.Name = "ForeColorLabel";
            this.ForeColorLabel.Size = new System.Drawing.Size(55, 13);
            this.ForeColorLabel.TabIndex = 34;
            this.ForeColorLabel.Text = "ForeColor:";
            // 
            // textBoxCaption
            // 
            this.textBoxCaption.Location = new System.Drawing.Point(72, 135);
            this.textBoxCaption.Name = "textBoxCaption";
            this.textBoxCaption.Size = new System.Drawing.Size(136, 20);
            this.textBoxCaption.TabIndex = 13;
            // 
            // textBoxCapHeight
            // 
            this.textBoxCapHeight.Location = new System.Drawing.Point(72, 109);
            this.textBoxCapHeight.Name = "textBoxCapHeight";
            this.textBoxCapHeight.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapHeight.TabIndex = 12;
            this.textBoxCapHeight.Text = "0";
            this.textBoxCapHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fontButton
            // 
            this.fontButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fontButton.Location = new System.Drawing.Point(72, 161);
            this.fontButton.Name = "fontButton";
            this.fontButton.Size = new System.Drawing.Size(180, 25);
            this.fontButton.TabIndex = 32;
            this.fontButton.UseVisualStyleBackColor = true;
            this.fontButton.Click += new System.EventHandler(this.fontButton_Click);
            // 
            // textBoxCapWidth
            // 
            this.textBoxCapWidth.Location = new System.Drawing.Point(72, 85);
            this.textBoxCapWidth.Name = "textBoxCapWidth";
            this.textBoxCapWidth.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapWidth.TabIndex = 11;
            this.textBoxCapWidth.Text = "0";
            this.textBoxCapWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FontLabel
            // 
            this.FontLabel.AutoSize = true;
            this.FontLabel.Location = new System.Drawing.Point(19, 166);
            this.FontLabel.Name = "FontLabel";
            this.FontLabel.Size = new System.Drawing.Size(31, 13);
            this.FontLabel.TabIndex = 31;
            this.FontLabel.Text = "Font:";
            // 
            // textBoxCapTop
            // 
            this.textBoxCapTop.Location = new System.Drawing.Point(72, 61);
            this.textBoxCapTop.Name = "textBoxCapTop";
            this.textBoxCapTop.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapTop.TabIndex = 10;
            this.textBoxCapTop.Text = "0";
            this.textBoxCapTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapLeft
            // 
            this.textBoxCapLeft.Location = new System.Drawing.Point(72, 37);
            this.textBoxCapLeft.Name = "textBoxCapLeft";
            this.textBoxCapLeft.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapLeft.TabIndex = 9;
            this.textBoxCapLeft.Text = "0";
            this.textBoxCapLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelVAlign
            // 
            this.labelVAlign.Location = new System.Drawing.Point(128, 92);
            this.labelVAlign.Name = "labelVAlign";
            this.labelVAlign.Size = new System.Drawing.Size(80, 16);
            this.labelVAlign.TabIndex = 8;
            this.labelVAlign.Text = "Vertical Align";
            // 
            // labelHAlign
            // 
            this.labelHAlign.Location = new System.Drawing.Point(128, 44);
            this.labelHAlign.Name = "labelHAlign";
            this.labelHAlign.Size = new System.Drawing.Size(92, 16);
            this.labelHAlign.TabIndex = 7;
            this.labelHAlign.Text = "Horizontal Align";
            // 
            // labelCaption
            // 
            this.labelCaption.Location = new System.Drawing.Point(19, 138);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(48, 16);
            this.labelCaption.TabIndex = 6;
            this.labelCaption.Text = "Caption:";
            // 
            // labelCapHeight
            // 
            this.labelCapHeight.Location = new System.Drawing.Point(19, 113);
            this.labelCapHeight.Name = "labelCapHeight";
            this.labelCapHeight.Size = new System.Drawing.Size(40, 16);
            this.labelCapHeight.TabIndex = 5;
            this.labelCapHeight.Text = "Height:";
            // 
            // labelCapWidth
            // 
            this.labelCapWidth.Location = new System.Drawing.Point(19, 89);
            this.labelCapWidth.Name = "labelCapWidth";
            this.labelCapWidth.Size = new System.Drawing.Size(40, 16);
            this.labelCapWidth.TabIndex = 4;
            this.labelCapWidth.Text = "Width:";
            // 
            // labelCapTop
            // 
            this.labelCapTop.Location = new System.Drawing.Point(19, 65);
            this.labelCapTop.Name = "labelCapTop";
            this.labelCapTop.Size = new System.Drawing.Size(32, 16);
            this.labelCapTop.TabIndex = 3;
            this.labelCapTop.Text = "Top:";
            // 
            // labelCapLeft
            // 
            this.labelCapLeft.Location = new System.Drawing.Point(19, 41);
            this.labelCapLeft.Name = "labelCapLeft";
            this.labelCapLeft.Size = new System.Drawing.Size(32, 16);
            this.labelCapLeft.TabIndex = 2;
            this.labelCapLeft.Text = "Left:";
            // 
            // checkBoxShadowText
            // 
            this.checkBoxShadowText.Location = new System.Drawing.Point(124, 16);
            this.checkBoxShadowText.Name = "checkBoxShadowText";
            this.checkBoxShadowText.Size = new System.Drawing.Size(96, 16);
            this.checkBoxShadowText.TabIndex = 1;
            this.checkBoxShadowText.Text = "Shadow Text";
            // 
            // checkBoxClipCaption
            // 
            this.checkBoxClipCaption.Location = new System.Drawing.Point(22, 16);
            this.checkBoxClipCaption.Name = "checkBoxClipCaption";
            this.checkBoxClipCaption.Size = new System.Drawing.Size(88, 16);
            this.checkBoxClipCaption.TabIndex = 0;
            this.checkBoxClipCaption.Text = "Clip Caption";
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(279, 42);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(557, 606);
            this.imageXView1.TabIndex = 5;
            // 
            // groupBoxLayout
            // 
            this.groupBoxLayout.Controls.Add(this.textBoxIB);
            this.groupBoxLayout.Controls.Add(this.textBoxIR);
            this.groupBoxLayout.Controls.Add(this.textBoxIT);
            this.groupBoxLayout.Controls.Add(this.textBoxIL);
            this.groupBoxLayout.Controls.Add(this.buttonLayoutUpdate);
            this.groupBoxLayout.Controls.Add(this.bottomLabel);
            this.groupBoxLayout.Controls.Add(this.rightLabel);
            this.groupBoxLayout.Controls.Add(this.topLabel);
            this.groupBoxLayout.Controls.Add(this.leftLabel);
            this.groupBoxLayout.Enabled = false;
            this.groupBoxLayout.Location = new System.Drawing.Point(12, 56);
            this.groupBoxLayout.Name = "groupBoxLayout";
            this.groupBoxLayout.Size = new System.Drawing.Size(261, 107);
            this.groupBoxLayout.TabIndex = 6;
            this.groupBoxLayout.TabStop = false;
            this.groupBoxLayout.Text = "Layout";
            // 
            // textBoxIB
            // 
            this.textBoxIB.Location = new System.Drawing.Point(186, 49);
            this.textBoxIB.Name = "textBoxIB";
            this.textBoxIB.Size = new System.Drawing.Size(66, 20);
            this.textBoxIB.TabIndex = 8;
            // 
            // textBoxIR
            // 
            this.textBoxIR.Location = new System.Drawing.Point(49, 49);
            this.textBoxIR.Name = "textBoxIR";
            this.textBoxIR.Size = new System.Drawing.Size(60, 20);
            this.textBoxIR.TabIndex = 7;
            // 
            // textBoxIT
            // 
            this.textBoxIT.Location = new System.Drawing.Point(186, 23);
            this.textBoxIT.Name = "textBoxIT";
            this.textBoxIT.Size = new System.Drawing.Size(66, 20);
            this.textBoxIT.TabIndex = 6;
            // 
            // textBoxIL
            // 
            this.textBoxIL.Location = new System.Drawing.Point(50, 23);
            this.textBoxIL.Name = "textBoxIL";
            this.textBoxIL.Size = new System.Drawing.Size(60, 20);
            this.textBoxIL.TabIndex = 5;
            // 
            // buttonLayoutUpdate
            // 
            this.buttonLayoutUpdate.Location = new System.Drawing.Point(89, 76);
            this.buttonLayoutUpdate.Name = "buttonLayoutUpdate";
            this.buttonLayoutUpdate.Size = new System.Drawing.Size(79, 25);
            this.buttonLayoutUpdate.TabIndex = 4;
            this.buttonLayoutUpdate.Text = "Update";
            this.buttonLayoutUpdate.Click += new System.EventHandler(this.buttonLayoutUpdate_Click);
            // 
            // bottomLabel
            // 
            this.bottomLabel.Location = new System.Drawing.Point(141, 52);
            this.bottomLabel.Name = "bottomLabel";
            this.bottomLabel.Size = new System.Drawing.Size(42, 16);
            this.bottomLabel.TabIndex = 3;
            this.bottomLabel.Text = "Bottom:";
            // 
            // rightLabel
            // 
            this.rightLabel.Location = new System.Drawing.Point(6, 53);
            this.rightLabel.Name = "rightLabel";
            this.rightLabel.Size = new System.Drawing.Size(35, 16);
            this.rightLabel.TabIndex = 2;
            this.rightLabel.Text = "Right:";
            // 
            // topLabel
            // 
            this.topLabel.Location = new System.Drawing.Point(141, 27);
            this.topLabel.Name = "topLabel";
            this.topLabel.Size = new System.Drawing.Size(39, 16);
            this.topLabel.TabIndex = 1;
            this.topLabel.Text = "Top:";
            // 
            // leftLabel
            // 
            this.leftLabel.Location = new System.Drawing.Point(6, 26);
            this.leftLabel.Name = "leftLabel";
            this.leftLabel.Size = new System.Drawing.Size(38, 16);
            this.leftLabel.TabIndex = 0;
            this.leftLabel.Text = "Left:";
            // 
            // groupBoxCaps
            // 
            this.groupBoxCaps.Controls.Add(this.stepLabel);
            this.groupBoxCaps.Controls.Add(this.labelStep);
            this.groupBoxCaps.Controls.Add(this.MaxValueLabel);
            this.groupBoxCaps.Controls.Add(this.MinValueLabel);
            this.groupBoxCaps.Controls.Add(this.DefaultValueLabel);
            this.groupBoxCaps.Controls.Add(this.UnitsValueLabel);
            this.groupBoxCaps.Controls.Add(this.ValuesLabel);
            this.groupBoxCaps.Controls.Add(this.updateCapabilityButton);
            this.groupBoxCaps.Controls.Add(this.labelUnits);
            this.groupBoxCaps.Controls.Add(this.labelDefault);
            this.groupBoxCaps.Controls.Add(this.labelMax);
            this.groupBoxCaps.Controls.Add(this.labelMin);
            this.groupBoxCaps.Controls.Add(this.textBoxCurrent);
            this.groupBoxCaps.Controls.Add(this.labelCurrent);
            this.groupBoxCaps.Controls.Add(this.listBoxCaps);
            this.groupBoxCaps.Controls.Add(this.comboBoxCapabilites);
            this.groupBoxCaps.Enabled = false;
            this.groupBoxCaps.Location = new System.Drawing.Point(12, 395);
            this.groupBoxCaps.Name = "groupBoxCaps";
            this.groupBoxCaps.Size = new System.Drawing.Size(261, 302);
            this.groupBoxCaps.TabIndex = 7;
            this.groupBoxCaps.TabStop = false;
            this.groupBoxCaps.Text = "Capabilities";
            // 
            // stepLabel
            // 
            this.stepLabel.AutoSize = true;
            this.stepLabel.Location = new System.Drawing.Point(6, 190);
            this.stepLabel.Name = "stepLabel";
            this.stepLabel.Size = new System.Drawing.Size(62, 13);
            this.stepLabel.TabIndex = 16;
            this.stepLabel.Text = "Step Value:";
            // 
            // labelStep
            // 
            this.labelStep.AutoSize = true;
            this.labelStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStep.Location = new System.Drawing.Point(100, 190);
            this.labelStep.Name = "labelStep";
            this.labelStep.Size = new System.Drawing.Size(0, 13);
            this.labelStep.TabIndex = 15;
            // 
            // MaxValueLabel
            // 
            this.MaxValueLabel.AutoSize = true;
            this.MaxValueLabel.Location = new System.Drawing.Point(6, 276);
            this.MaxValueLabel.Name = "MaxValueLabel";
            this.MaxValueLabel.Size = new System.Drawing.Size(84, 13);
            this.MaxValueLabel.TabIndex = 14;
            this.MaxValueLabel.Text = "Maximum Value:";
            // 
            // MinValueLabel
            // 
            this.MinValueLabel.AutoSize = true;
            this.MinValueLabel.Location = new System.Drawing.Point(6, 246);
            this.MinValueLabel.Name = "MinValueLabel";
            this.MinValueLabel.Size = new System.Drawing.Size(81, 13);
            this.MinValueLabel.TabIndex = 13;
            this.MinValueLabel.Text = "Minimum Value:";
            // 
            // DefaultValueLabel
            // 
            this.DefaultValueLabel.AutoSize = true;
            this.DefaultValueLabel.Location = new System.Drawing.Point(6, 216);
            this.DefaultValueLabel.Name = "DefaultValueLabel";
            this.DefaultValueLabel.Size = new System.Drawing.Size(74, 13);
            this.DefaultValueLabel.TabIndex = 12;
            this.DefaultValueLabel.Text = "Default Value:";
            // 
            // UnitsValueLabel
            // 
            this.UnitsValueLabel.AutoSize = true;
            this.UnitsValueLabel.Location = new System.Drawing.Point(6, 166);
            this.UnitsValueLabel.Name = "UnitsValueLabel";
            this.UnitsValueLabel.Size = new System.Drawing.Size(90, 13);
            this.UnitsValueLabel.TabIndex = 11;
            this.UnitsValueLabel.Text = "Units of Measure:";
            // 
            // ValuesLabel
            // 
            this.ValuesLabel.AutoSize = true;
            this.ValuesLabel.Location = new System.Drawing.Point(6, 43);
            this.ValuesLabel.Name = "ValuesLabel";
            this.ValuesLabel.Size = new System.Drawing.Size(71, 13);
            this.ValuesLabel.TabIndex = 10;
            this.ValuesLabel.Text = "Legal Values:";
            // 
            // updateCapabilityButton
            // 
            this.updateCapabilityButton.Enabled = false;
            this.updateCapabilityButton.Location = new System.Drawing.Point(174, 134);
            this.updateCapabilityButton.Name = "updateCapabilityButton";
            this.updateCapabilityButton.Size = new System.Drawing.Size(79, 25);
            this.updateCapabilityButton.TabIndex = 9;
            this.updateCapabilityButton.Text = "Update";
            this.updateCapabilityButton.Click += new System.EventHandler(this.updateCapabilityButton_Click);
            // 
            // labelUnits
            // 
            this.labelUnits.AutoSize = true;
            this.labelUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnits.Location = new System.Drawing.Point(100, 166);
            this.labelUnits.Name = "labelUnits";
            this.labelUnits.Size = new System.Drawing.Size(0, 13);
            this.labelUnits.TabIndex = 8;
            // 
            // labelDefault
            // 
            this.labelDefault.AutoSize = true;
            this.labelDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDefault.Location = new System.Drawing.Point(100, 216);
            this.labelDefault.Name = "labelDefault";
            this.labelDefault.Size = new System.Drawing.Size(0, 13);
            this.labelDefault.TabIndex = 1;
            // 
            // labelMax
            // 
            this.labelMax.AutoSize = true;
            this.labelMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMax.Location = new System.Drawing.Point(100, 276);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(0, 13);
            this.labelMax.TabIndex = 7;
            // 
            // labelMin
            // 
            this.labelMin.AutoSize = true;
            this.labelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMin.Location = new System.Drawing.Point(100, 246);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(0, 13);
            this.labelMin.TabIndex = 6;
            // 
            // textBoxCurrent
            // 
            this.textBoxCurrent.Enabled = false;
            this.textBoxCurrent.Location = new System.Drawing.Point(60, 134);
            this.textBoxCurrent.Name = "textBoxCurrent";
            this.textBoxCurrent.Size = new System.Drawing.Size(108, 20);
            this.textBoxCurrent.TabIndex = 4;
            // 
            // labelCurrent
            // 
            this.labelCurrent.Location = new System.Drawing.Point(6, 137);
            this.labelCurrent.Name = "labelCurrent";
            this.labelCurrent.Size = new System.Drawing.Size(48, 16);
            this.labelCurrent.TabIndex = 3;
            this.labelCurrent.Text = "Current:";
            // 
            // listBoxCaps
            // 
            this.listBoxCaps.HorizontalScrollbar = true;
            this.listBoxCaps.Location = new System.Drawing.Point(6, 59);
            this.listBoxCaps.Name = "listBoxCaps";
            this.listBoxCaps.Size = new System.Drawing.Size(246, 69);
            this.listBoxCaps.TabIndex = 2;
            // 
            // comboBoxCapabilites
            // 
            this.comboBoxCapabilites.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCapabilites.Location = new System.Drawing.Point(6, 19);
            this.comboBoxCapabilites.Name = "comboBoxCapabilites";
            this.comboBoxCapabilites.Size = new System.Drawing.Size(249, 21);
            this.comboBoxCapabilites.Sorted = true;
            this.comboBoxCapabilites.TabIndex = 0;
            this.comboBoxCapabilites.SelectedIndexChanged += new System.EventHandler(this.comboBoxCaps_SelectedIndexChanged);
            // 
            // OKButton
            // 
            this.OKButton.Enabled = false;
            this.OKButton.Location = new System.Drawing.Point(533, 659);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(97, 23);
            this.OKButton.TabIndex = 8;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // countLabel
            // 
            this.countLabel.AutoSize = true;
            this.countLabel.Location = new System.Drawing.Point(726, 17);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(0, 13);
            this.countLabel.TabIndex = 19;
            // 
            // pageLabel
            // 
            this.pageLabel.AutoSize = true;
            this.pageLabel.Location = new System.Drawing.Point(633, 17);
            this.pageLabel.Name = "pageLabel";
            this.pageLabel.Size = new System.Drawing.Size(39, 13);
            this.pageLabel.TabIndex = 18;
            this.pageLabel.Text = "Image:";
            // 
            // pageTextBox
            // 
            this.pageTextBox.Location = new System.Drawing.Point(674, 14);
            this.pageTextBox.Name = "pageTextBox";
            this.pageTextBox.ReadOnly = true;
            this.pageTextBox.Size = new System.Drawing.Size(46, 20);
            this.pageTextBox.TabIndex = 17;
            // 
            // nextButton
            // 
            this.nextButton.Enabled = false;
            this.nextButton.Location = new System.Drawing.Point(515, 11);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(108, 23);
            this.nextButton.TabIndex = 16;
            this.nextButton.Text = "Next Image";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // previousButton
            // 
            this.previousButton.Enabled = false;
            this.previousButton.Location = new System.Drawing.Point(401, 11);
            this.previousButton.Name = "previousButton";
            this.previousButton.Size = new System.Drawing.Size(108, 23);
            this.previousButton.TabIndex = 15;
            this.previousButton.Text = "Previous Image";
            this.previousButton.UseVisualStyleBackColor = true;
            this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // ScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 709);
            this.Controls.Add(this.countLabel);
            this.Controls.Add(this.pageLabel);
            this.Controls.Add(this.pageTextBox);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.previousButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.groupBoxCaps);
            this.Controls.Add(this.groupBoxLayout);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.groupBoxCaption);
            this.Controls.Add(this.UICheckBox);
            this.Controls.Add(this.CancelScanButton);
            this.Controls.Add(this.scanButton);
            this.Controls.Add(this.sourceButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ScanForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "TWAIN Scan";
            this.Load += new System.EventHandler(this.ScanForm_Load);
            this.groupBoxCaption.ResumeLayout(false);
            this.groupBoxCaption.PerformLayout();
            this.groupBoxLayout.ResumeLayout(false);
            this.groupBoxLayout.PerformLayout();
            this.groupBoxCaps.ResumeLayout(false);
            this.groupBoxCaps.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sourceButton;
        private System.Windows.Forms.Button scanButton;
        private System.Windows.Forms.Button CancelScanButton;
        private System.Windows.Forms.CheckBox UICheckBox;
        private System.Windows.Forms.GroupBox groupBoxCaption;
        private System.Windows.Forms.ComboBox comboBoxVAlign;
        private System.Windows.Forms.ComboBox comboBoxHAlign;
        private System.Windows.Forms.TextBox textBoxCaption;
        private System.Windows.Forms.TextBox textBoxCapHeight;
        private System.Windows.Forms.TextBox textBoxCapWidth;
        private System.Windows.Forms.TextBox textBoxCapTop;
        private System.Windows.Forms.TextBox textBoxCapLeft;
        private System.Windows.Forms.Label labelVAlign;
        private System.Windows.Forms.Label labelHAlign;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Label labelCapHeight;
        private System.Windows.Forms.Label labelCapWidth;
        private System.Windows.Forms.Label labelCapTop;
        private System.Windows.Forms.Label labelCapLeft;
        private System.Windows.Forms.CheckBox checkBoxShadowText;
        private System.Windows.Forms.CheckBox checkBoxClipCaption;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private System.Windows.Forms.GroupBox groupBoxLayout;
        private System.Windows.Forms.TextBox textBoxIB;
        private System.Windows.Forms.TextBox textBoxIR;
        private System.Windows.Forms.TextBox textBoxIT;
        private System.Windows.Forms.TextBox textBoxIL;
        private System.Windows.Forms.Button buttonLayoutUpdate;
        private System.Windows.Forms.Label bottomLabel;
        private System.Windows.Forms.Label rightLabel;
        private System.Windows.Forms.Label topLabel;
        private System.Windows.Forms.Label leftLabel;
        private System.Windows.Forms.GroupBox groupBoxCaps;
        private System.Windows.Forms.Label labelUnits;
        private System.Windows.Forms.Label labelDefault;
        private System.Windows.Forms.Label labelMax;
        private System.Windows.Forms.Label labelMin;
        private System.Windows.Forms.TextBox textBoxCurrent;
        private System.Windows.Forms.Label labelCurrent;
        private System.Windows.Forms.ListBox listBoxCaps;
        private System.Windows.Forms.ComboBox comboBoxCapabilites;
        private System.Windows.Forms.Button foreColorButton;
        private System.Windows.Forms.Label ForeColorLabel;
        private System.Windows.Forms.Button fontButton;
        private System.Windows.Forms.Label FontLabel;
        private System.Windows.Forms.Button updateCaptionButton;
        private System.Windows.Forms.Button updateCapabilityButton;
        private System.Windows.Forms.Label ValuesLabel;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Label MaxValueLabel;
        private System.Windows.Forms.Label MinValueLabel;
        private System.Windows.Forms.Label DefaultValueLabel;
        private System.Windows.Forms.Label UnitsValueLabel;
        private System.Windows.Forms.Label stepLabel;
        private System.Windows.Forms.Label labelStep;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.Label pageLabel;
        private System.Windows.Forms.TextBox pageTextBox;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button previousButton;
        private PegasusImaging.WinForms.TwainPro5.TwainPro twainPro1;
    }
}
'****************************************************************'
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress9
Imports System.Windows.Forms

Public Class DocumentImagingandCleanUp
    Inherits System.Windows.Forms.Form

    Private imgFileName As String

    Private imagX1 As PegasusImaging.WinForms.ImagXpress9.ImageX
    Private ixProcessor1 As PegasusImaging.WinForms.ImagXpress9.Processor
    Private cPadColor As System.Drawing.Color
    Private iDeskewPadCol As Integer
    Private iShearPadCol As Integer

    'File I/O Variables
    
    Dim strimageFile As System.String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            ' Don't forget to dispose IX
            '
            If Not (ImagXpress1 Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (ImageXView2 Is Nothing) Then

                ImageXView2.Dispose()
                ImageXView2 = Nothing
            End If
            If Not (ixProcessor1 Is Nothing) Then

                ixProcessor1.Dispose()
                ixProcessor1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents ImageXView2 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileQuit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents docBorderCrop As System.Windows.Forms.TabPage
    Friend WithEvents docZoomSmooth As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocBorderCrop As System.Windows.Forms.GroupBox
    Friend WithEvents grbxDocZoomSmooth As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBorderCrop As System.Windows.Forms.Button
    Friend WithEvents docDeskew As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocDeskew As System.Windows.Forms.GroupBox
    Friend WithEvents docDespeckle As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocDespeckle As System.Windows.Forms.GroupBox
    Friend WithEvents docDilate As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocDilate As System.Windows.Forms.GroupBox
    Friend WithEvents docErode As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocErode As System.Windows.Forms.GroupBox
    Friend WithEvents docLineRemoval As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocLineRemoval As System.Windows.Forms.GroupBox
    Friend WithEvents docShear As System.Windows.Forms.TabPage
    Friend WithEvents grbxDocShear As System.Windows.Forms.GroupBox
    Friend WithEvents lblMinAngle As System.Windows.Forms.Label
    Friend WithEvents hscrMinAngle As System.Windows.Forms.HScrollBar
    Friend WithEvents lblMinAngleVal As System.Windows.Forms.Label
    Friend WithEvents lblMinConfidence As System.Windows.Forms.Label
    Friend WithEvents hscrMinConfidence As System.Windows.Forms.HScrollBar
    Friend WithEvents lblMinConfidenceVal As System.Windows.Forms.Label
    Friend WithEvents lblPadColor As System.Windows.Forms.Label
    Friend WithEvents lblQuality As System.Windows.Forms.Label
    Friend WithEvents lblQualityVal As System.Windows.Forms.Label
    Friend WithEvents chkMaintOrigSize As System.Windows.Forms.CheckBox
    Friend WithEvents cmdDeskew As System.Windows.Forms.Button
    Friend WithEvents hscrQuality As System.Windows.Forms.HScrollBar
    Friend WithEvents rbDeskewWhite As System.Windows.Forms.RadioButton
    Friend WithEvents rbDeskewBlack As System.Windows.Forms.RadioButton
    Friend WithEvents lblSpeckWidth As System.Windows.Forms.Label
    Friend WithEvents hscrDespSpeckWidth As System.Windows.Forms.HScrollBar
    Friend WithEvents lblDespSpeckWVal As System.Windows.Forms.Label
    Friend WithEvents lblDespSpeckHeight As System.Windows.Forms.Label
    Friend WithEvents hscrDespSpeckHeight As System.Windows.Forms.HScrollBar
    Friend WithEvents lblDespSpeckHVal As System.Windows.Forms.Label
    Friend WithEvents cmdDespeckle As System.Windows.Forms.Button
    Friend WithEvents lblDilateAmount As System.Windows.Forms.Label
    Friend WithEvents hscrDilateAmount As System.Windows.Forms.HScrollBar
    Friend WithEvents lblDilateAmountVal As System.Windows.Forms.Label
    Friend WithEvents lblDilateDirection As System.Windows.Forms.Label
    Friend WithEvents cboDilateDirection As System.Windows.Forms.ComboBox
    Friend WithEvents cmdDilate As System.Windows.Forms.Button
    Friend WithEvents lblErodeAmount As System.Windows.Forms.Label
    Friend WithEvents hscrErodeAmount As System.Windows.Forms.HScrollBar
    Friend WithEvents lblErodeAmountVal As System.Windows.Forms.Label
    Friend WithEvents lblErodeDirection As System.Windows.Forms.Label
    Friend WithEvents cboErodeDirection As System.Windows.Forms.ComboBox
    Friend WithEvents cmdErode As System.Windows.Forms.Button
    Friend WithEvents lblShearAngle As System.Windows.Forms.Label
    Friend WithEvents hscrShearAngle As System.Windows.Forms.HScrollBar
    Friend WithEvents lblShearAngleVal As System.Windows.Forms.Label
    Friend WithEvents lblShearPadColor As System.Windows.Forms.Label
    Friend WithEvents rbShearWhite As System.Windows.Forms.RadioButton
    Friend WithEvents rbShearBlack As System.Windows.Forms.RadioButton
    Friend WithEvents lblShearType As System.Windows.Forms.Label
    Friend WithEvents cboShearType As System.Windows.Forms.ComboBox
    Friend WithEvents cmdShear As System.Windows.Forms.Button
    Friend WithEvents lblMinLength As System.Windows.Forms.Label
    Friend WithEvents hscrMinLength As System.Windows.Forms.HScrollBar
    Friend WithEvents lblMinLengthVal As System.Windows.Forms.Label
    Friend WithEvents lblMaxThickness As System.Windows.Forms.Label
    Friend WithEvents lblMaxThicknessVal As System.Windows.Forms.Label
    Friend WithEvents hscrMaxThickness As System.Windows.Forms.HScrollBar
    Friend WithEvents lblMinAspRatio As System.Windows.Forms.Label
    Friend WithEvents hscrMinAspRatio As System.Windows.Forms.HScrollBar
    Friend WithEvents lblMaxGap As System.Windows.Forms.Label
    Friend WithEvents lblMaxGapVal As System.Windows.Forms.Label
    Friend WithEvents lblMaxCharRepSize As System.Windows.Forms.Label
    Friend WithEvents lblMaxCharRepSizeVal As System.Windows.Forms.Label
    Friend WithEvents lblMinAspRatioVal As System.Windows.Forms.Label
    Friend WithEvents hscrMaxGap As System.Windows.Forms.HScrollBar
    Friend WithEvents hscrlMaxCharRepSize As System.Windows.Forms.HScrollBar
    Friend WithEvents cmdLineRemoval As System.Windows.Forms.Button
    Friend WithEvents cmdZoomSmooth As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.ImageXView2 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuFileOpen = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.mnuFileQuit = New System.Windows.Forms.MenuItem
        Me.mnuToolbar = New System.Windows.Forms.MenuItem
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem
        Me.mnuAbout = New System.Windows.Forms.MenuItem
        Me.lblError = New System.Windows.Forms.Label
        Me.lblLastError = New System.Windows.Forms.Label
        Me.lstInfo = New System.Windows.Forms.ListBox
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.docBorderCrop = New System.Windows.Forms.TabPage
        Me.grbxDocBorderCrop = New System.Windows.Forms.GroupBox
        Me.cmdBorderCrop = New System.Windows.Forms.Button
        Me.docLineRemoval = New System.Windows.Forms.TabPage
        Me.grbxDocLineRemoval = New System.Windows.Forms.GroupBox
        Me.cmdLineRemoval = New System.Windows.Forms.Button
        Me.hscrlMaxCharRepSize = New System.Windows.Forms.HScrollBar
        Me.lblMaxCharRepSizeVal = New System.Windows.Forms.Label
        Me.lblMaxCharRepSize = New System.Windows.Forms.Label
        Me.hscrMaxGap = New System.Windows.Forms.HScrollBar
        Me.lblMaxGapVal = New System.Windows.Forms.Label
        Me.lblMaxGap = New System.Windows.Forms.Label
        Me.hscrMinAspRatio = New System.Windows.Forms.HScrollBar
        Me.lblMinAspRatioVal = New System.Windows.Forms.Label
        Me.lblMinAspRatio = New System.Windows.Forms.Label
        Me.hscrMaxThickness = New System.Windows.Forms.HScrollBar
        Me.lblMaxThicknessVal = New System.Windows.Forms.Label
        Me.lblMaxThickness = New System.Windows.Forms.Label
        Me.lblMinLengthVal = New System.Windows.Forms.Label
        Me.hscrMinLength = New System.Windows.Forms.HScrollBar
        Me.lblMinLength = New System.Windows.Forms.Label
        Me.docDeskew = New System.Windows.Forms.TabPage
        Me.grbxDocDeskew = New System.Windows.Forms.GroupBox
        Me.rbDeskewBlack = New System.Windows.Forms.RadioButton
        Me.rbDeskewWhite = New System.Windows.Forms.RadioButton
        Me.cmdDeskew = New System.Windows.Forms.Button
        Me.chkMaintOrigSize = New System.Windows.Forms.CheckBox
        Me.lblQualityVal = New System.Windows.Forms.Label
        Me.hscrQuality = New System.Windows.Forms.HScrollBar
        Me.lblQuality = New System.Windows.Forms.Label
        Me.lblPadColor = New System.Windows.Forms.Label
        Me.lblMinConfidenceVal = New System.Windows.Forms.Label
        Me.hscrMinConfidence = New System.Windows.Forms.HScrollBar
        Me.lblMinConfidence = New System.Windows.Forms.Label
        Me.lblMinAngleVal = New System.Windows.Forms.Label
        Me.hscrMinAngle = New System.Windows.Forms.HScrollBar
        Me.lblMinAngle = New System.Windows.Forms.Label
        Me.docShear = New System.Windows.Forms.TabPage
        Me.grbxDocShear = New System.Windows.Forms.GroupBox
        Me.cmdShear = New System.Windows.Forms.Button
        Me.cboShearType = New System.Windows.Forms.ComboBox
        Me.lblShearType = New System.Windows.Forms.Label
        Me.rbShearBlack = New System.Windows.Forms.RadioButton
        Me.rbShearWhite = New System.Windows.Forms.RadioButton
        Me.lblShearPadColor = New System.Windows.Forms.Label
        Me.lblShearAngleVal = New System.Windows.Forms.Label
        Me.hscrShearAngle = New System.Windows.Forms.HScrollBar
        Me.lblShearAngle = New System.Windows.Forms.Label
        Me.docErode = New System.Windows.Forms.TabPage
        Me.grbxDocErode = New System.Windows.Forms.GroupBox
        Me.cmdErode = New System.Windows.Forms.Button
        Me.cboErodeDirection = New System.Windows.Forms.ComboBox
        Me.lblErodeDirection = New System.Windows.Forms.Label
        Me.lblErodeAmountVal = New System.Windows.Forms.Label
        Me.hscrErodeAmount = New System.Windows.Forms.HScrollBar
        Me.lblErodeAmount = New System.Windows.Forms.Label
        Me.docDilate = New System.Windows.Forms.TabPage
        Me.grbxDocDilate = New System.Windows.Forms.GroupBox
        Me.cmdDilate = New System.Windows.Forms.Button
        Me.cboDilateDirection = New System.Windows.Forms.ComboBox
        Me.lblDilateDirection = New System.Windows.Forms.Label
        Me.lblDilateAmountVal = New System.Windows.Forms.Label
        Me.hscrDilateAmount = New System.Windows.Forms.HScrollBar
        Me.lblDilateAmount = New System.Windows.Forms.Label
        Me.docDespeckle = New System.Windows.Forms.TabPage
        Me.grbxDocDespeckle = New System.Windows.Forms.GroupBox
        Me.cmdDespeckle = New System.Windows.Forms.Button
        Me.lblDespSpeckHVal = New System.Windows.Forms.Label
        Me.hscrDespSpeckHeight = New System.Windows.Forms.HScrollBar
        Me.lblDespSpeckHeight = New System.Windows.Forms.Label
        Me.lblDespSpeckWVal = New System.Windows.Forms.Label
        Me.hscrDespSpeckWidth = New System.Windows.Forms.HScrollBar
        Me.lblSpeckWidth = New System.Windows.Forms.Label
        Me.docZoomSmooth = New System.Windows.Forms.TabPage
        Me.grbxDocZoomSmooth = New System.Windows.Forms.GroupBox
        Me.cmdZoomSmooth = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.docBorderCrop.SuspendLayout()
        Me.grbxDocBorderCrop.SuspendLayout()
        Me.docLineRemoval.SuspendLayout()
        Me.grbxDocLineRemoval.SuspendLayout()
        Me.docDeskew.SuspendLayout()
        Me.grbxDocDeskew.SuspendLayout()
        Me.docShear.SuspendLayout()
        Me.grbxDocShear.SuspendLayout()
        Me.docErode.SuspendLayout()
        Me.grbxDocErode.SuspendLayout()
        Me.docDilate.SuspendLayout()
        Me.grbxDocDilate.SuspendLayout()
        Me.docDespeckle.SuspendLayout()
        Me.grbxDocDespeckle.SuspendLayout()
        Me.docZoomSmooth.SuspendLayout()
        Me.grbxDocZoomSmooth.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(16, 88)
        Me.ImageXView1.MouseWheelCapture = False
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(440, 287)
        Me.ImageXView1.TabIndex = 0
        '
        'ImageXView2
        '
        Me.ImageXView2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ImageXView2.AutoScroll = True
        Me.ImageXView2.Location = New System.Drawing.Point(470, 88)
        Me.ImageXView2.MouseWheelCapture = False
        Me.ImageXView2.Name = "ImageXView2"
        Me.ImageXView2.Size = New System.Drawing.Size(424, 284)
        Me.ImageXView2.TabIndex = 1
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolbar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileOpen, Me.MenuItem2, Me.mnuFileQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Index = 0
        Me.mnuFileOpen.Text = "&Open"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'mnuFileQuit
        '
        Me.mnuFileQuit.Index = 2
        Me.mnuFileQuit.Text = "&Quit"
        '
        'mnuToolbar
        '
        Me.mnuToolbar.Index = 1
        Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolbar.Text = "&Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(679, 572)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(191, 96)
        Me.lblError.TabIndex = 26
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLastError.Location = New System.Drawing.Point(679, 540)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(167, 24)
        Me.lblLastError.TabIndex = 25
        Me.lblLastError.Text = "Last Error:"
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "", "1)Using the DocumentBorderCrop, DocumentDeskew, DocumentDespeckle, DocumentDilate" & _
                        ", DocumentErode, DocumentLineRemoval,", "   DocumentShear and DocumentSmoothZoom methods."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(873, 30)
        Me.lstInfo.TabIndex = 29
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.docBorderCrop)
        Me.TabControl1.Controls.Add(Me.docLineRemoval)
        Me.TabControl1.Controls.Add(Me.docDeskew)
        Me.TabControl1.Controls.Add(Me.docShear)
        Me.TabControl1.Controls.Add(Me.docErode)
        Me.TabControl1.Controls.Add(Me.docDilate)
        Me.TabControl1.Controls.Add(Me.docDespeckle)
        Me.TabControl1.Controls.Add(Me.docZoomSmooth)
        Me.TabControl1.Location = New System.Drawing.Point(24, 391)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(640, 264)
        Me.TabControl1.TabIndex = 30
        '
        'docBorderCrop
        '
        Me.docBorderCrop.Controls.Add(Me.grbxDocBorderCrop)
        Me.docBorderCrop.Location = New System.Drawing.Point(4, 22)
        Me.docBorderCrop.Name = "docBorderCrop"
        Me.docBorderCrop.Size = New System.Drawing.Size(632, 238)
        Me.docBorderCrop.TabIndex = 0
        Me.docBorderCrop.Text = "Doc Border Crop"
        '
        'grbxDocBorderCrop
        '
        Me.grbxDocBorderCrop.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocBorderCrop.Controls.Add(Me.cmdBorderCrop)
        Me.grbxDocBorderCrop.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocBorderCrop.Name = "grbxDocBorderCrop"
        Me.grbxDocBorderCrop.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocBorderCrop.TabIndex = 0
        Me.grbxDocBorderCrop.TabStop = False
        Me.grbxDocBorderCrop.Text = "Document Border Crop"
        '
        'cmdBorderCrop
        '
        Me.cmdBorderCrop.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdBorderCrop.Location = New System.Drawing.Point(162, 166)
        Me.cmdBorderCrop.Name = "cmdBorderCrop"
        Me.cmdBorderCrop.Size = New System.Drawing.Size(282, 40)
        Me.cmdBorderCrop.TabIndex = 0
        Me.cmdBorderCrop.Text = "Document Border Crop"
        '
        'docLineRemoval
        '
        Me.docLineRemoval.Controls.Add(Me.grbxDocLineRemoval)
        Me.docLineRemoval.Location = New System.Drawing.Point(4, 22)
        Me.docLineRemoval.Name = "docLineRemoval"
        Me.docLineRemoval.Size = New System.Drawing.Size(632, 238)
        Me.docLineRemoval.TabIndex = 2
        Me.docLineRemoval.Text = "Doc Line Removal"
        '
        'grbxDocLineRemoval
        '
        Me.grbxDocLineRemoval.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocLineRemoval.Controls.Add(Me.cmdLineRemoval)
        Me.grbxDocLineRemoval.Controls.Add(Me.hscrlMaxCharRepSize)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMaxCharRepSizeVal)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMaxCharRepSize)
        Me.grbxDocLineRemoval.Controls.Add(Me.hscrMaxGap)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMaxGapVal)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMaxGap)
        Me.grbxDocLineRemoval.Controls.Add(Me.hscrMinAspRatio)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMinAspRatioVal)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMinAspRatio)
        Me.grbxDocLineRemoval.Controls.Add(Me.hscrMaxThickness)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMaxThicknessVal)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMaxThickness)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMinLengthVal)
        Me.grbxDocLineRemoval.Controls.Add(Me.hscrMinLength)
        Me.grbxDocLineRemoval.Controls.Add(Me.lblMinLength)
        Me.grbxDocLineRemoval.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocLineRemoval.Name = "grbxDocLineRemoval"
        Me.grbxDocLineRemoval.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocLineRemoval.TabIndex = 0
        Me.grbxDocLineRemoval.TabStop = False
        Me.grbxDocLineRemoval.Text = "Document Line Removal"
        '
        'cmdLineRemoval
        '
        Me.cmdLineRemoval.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdLineRemoval.Location = New System.Drawing.Point(224, 192)
        Me.cmdLineRemoval.Name = "cmdLineRemoval"
        Me.cmdLineRemoval.Size = New System.Drawing.Size(200, 24)
        Me.cmdLineRemoval.TabIndex = 15
        Me.cmdLineRemoval.Text = "Document Line Removal"
        '
        'hscrlMaxCharRepSize
        '
        Me.hscrlMaxCharRepSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.hscrlMaxCharRepSize.Location = New System.Drawing.Point(152, 160)
        Me.hscrlMaxCharRepSize.Name = "hscrlMaxCharRepSize"
        Me.hscrlMaxCharRepSize.Size = New System.Drawing.Size(368, 16)
        Me.hscrlMaxCharRepSize.TabIndex = 14
        Me.hscrlMaxCharRepSize.Value = 20
        '
        'lblMaxCharRepSizeVal
        '
        Me.lblMaxCharRepSizeVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxCharRepSizeVal.Location = New System.Drawing.Point(536, 160)
        Me.lblMaxCharRepSizeVal.Name = "lblMaxCharRepSizeVal"
        Me.lblMaxCharRepSizeVal.Size = New System.Drawing.Size(72, 16)
        Me.lblMaxCharRepSizeVal.TabIndex = 13
        '
        'lblMaxCharRepSize
        '
        Me.lblMaxCharRepSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxCharRepSize.Location = New System.Drawing.Point(8, 160)
        Me.lblMaxCharRepSize.Name = "lblMaxCharRepSize"
        Me.lblMaxCharRepSize.Size = New System.Drawing.Size(144, 16)
        Me.lblMaxCharRepSize.TabIndex = 12
        Me.lblMaxCharRepSize.Text = "Max Character Repair Size:"
        '
        'hscrMaxGap
        '
        Me.hscrMaxGap.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.hscrMaxGap.Location = New System.Drawing.Point(152, 128)
        Me.hscrMaxGap.Maximum = 20
        Me.hscrMaxGap.Name = "hscrMaxGap"
        Me.hscrMaxGap.Size = New System.Drawing.Size(368, 16)
        Me.hscrMaxGap.TabIndex = 11
        Me.hscrMaxGap.Value = 1
        '
        'lblMaxGapVal
        '
        Me.lblMaxGapVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxGapVal.Location = New System.Drawing.Point(536, 128)
        Me.lblMaxGapVal.Name = "lblMaxGapVal"
        Me.lblMaxGapVal.Size = New System.Drawing.Size(64, 16)
        Me.lblMaxGapVal.TabIndex = 10
        '
        'lblMaxGap
        '
        Me.lblMaxGap.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxGap.Location = New System.Drawing.Point(8, 128)
        Me.lblMaxGap.Name = "lblMaxGap"
        Me.lblMaxGap.Size = New System.Drawing.Size(120, 16)
        Me.lblMaxGap.TabIndex = 9
        Me.lblMaxGap.Text = "Max Gap:"
        '
        'hscrMinAspRatio
        '
        Me.hscrMinAspRatio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.hscrMinAspRatio.Location = New System.Drawing.Point(152, 96)
        Me.hscrMinAspRatio.Maximum = 1000
        Me.hscrMinAspRatio.Minimum = 1
        Me.hscrMinAspRatio.Name = "hscrMinAspRatio"
        Me.hscrMinAspRatio.Size = New System.Drawing.Size(368, 16)
        Me.hscrMinAspRatio.TabIndex = 8
        Me.hscrMinAspRatio.Value = 10
        '
        'lblMinAspRatioVal
        '
        Me.lblMinAspRatioVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMinAspRatioVal.Location = New System.Drawing.Point(536, 96)
        Me.lblMinAspRatioVal.Name = "lblMinAspRatioVal"
        Me.lblMinAspRatioVal.Size = New System.Drawing.Size(64, 16)
        Me.lblMinAspRatioVal.TabIndex = 7
        '
        'lblMinAspRatio
        '
        Me.lblMinAspRatio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMinAspRatio.Location = New System.Drawing.Point(8, 96)
        Me.lblMinAspRatio.Name = "lblMinAspRatio"
        Me.lblMinAspRatio.Size = New System.Drawing.Size(128, 24)
        Me.lblMinAspRatio.TabIndex = 6
        Me.lblMinAspRatio.Text = "Min Aspect Ratio:"
        '
        'hscrMaxThickness
        '
        Me.hscrMaxThickness.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.hscrMaxThickness.Location = New System.Drawing.Point(152, 64)
        Me.hscrMaxThickness.Name = "hscrMaxThickness"
        Me.hscrMaxThickness.Size = New System.Drawing.Size(368, 16)
        Me.hscrMaxThickness.TabIndex = 5
        Me.hscrMaxThickness.Value = 20
        '
        'lblMaxThicknessVal
        '
        Me.lblMaxThicknessVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxThicknessVal.Location = New System.Drawing.Point(536, 64)
        Me.lblMaxThicknessVal.Name = "lblMaxThicknessVal"
        Me.lblMaxThicknessVal.Size = New System.Drawing.Size(56, 16)
        Me.lblMaxThicknessVal.TabIndex = 4
        '
        'lblMaxThickness
        '
        Me.lblMaxThickness.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMaxThickness.Location = New System.Drawing.Point(8, 64)
        Me.lblMaxThickness.Name = "lblMaxThickness"
        Me.lblMaxThickness.Size = New System.Drawing.Size(128, 16)
        Me.lblMaxThickness.TabIndex = 3
        Me.lblMaxThickness.Text = "Max Thickness:"
        '
        'lblMinLengthVal
        '
        Me.lblMinLengthVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMinLengthVal.Location = New System.Drawing.Point(536, 24)
        Me.lblMinLengthVal.Name = "lblMinLengthVal"
        Me.lblMinLengthVal.Size = New System.Drawing.Size(56, 16)
        Me.lblMinLengthVal.TabIndex = 2
        '
        'hscrMinLength
        '
        Me.hscrMinLength.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.hscrMinLength.Location = New System.Drawing.Point(152, 24)
        Me.hscrMinLength.Maximum = 20000
        Me.hscrMinLength.Minimum = 10
        Me.hscrMinLength.Name = "hscrMinLength"
        Me.hscrMinLength.Size = New System.Drawing.Size(368, 16)
        Me.hscrMinLength.TabIndex = 1
        Me.hscrMinLength.Value = 50
        '
        'lblMinLength
        '
        Me.lblMinLength.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblMinLength.Location = New System.Drawing.Point(8, 24)
        Me.lblMinLength.Name = "lblMinLength"
        Me.lblMinLength.Size = New System.Drawing.Size(120, 16)
        Me.lblMinLength.TabIndex = 0
        Me.lblMinLength.Text = "Minimum Length:"
        '
        'docDeskew
        '
        Me.docDeskew.Controls.Add(Me.grbxDocDeskew)
        Me.docDeskew.Location = New System.Drawing.Point(4, 22)
        Me.docDeskew.Name = "docDeskew"
        Me.docDeskew.Size = New System.Drawing.Size(632, 238)
        Me.docDeskew.TabIndex = 6
        Me.docDeskew.Text = "Doc Deskew"
        '
        'grbxDocDeskew
        '
        Me.grbxDocDeskew.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocDeskew.Controls.Add(Me.rbDeskewBlack)
        Me.grbxDocDeskew.Controls.Add(Me.rbDeskewWhite)
        Me.grbxDocDeskew.Controls.Add(Me.cmdDeskew)
        Me.grbxDocDeskew.Controls.Add(Me.chkMaintOrigSize)
        Me.grbxDocDeskew.Controls.Add(Me.lblQualityVal)
        Me.grbxDocDeskew.Controls.Add(Me.hscrQuality)
        Me.grbxDocDeskew.Controls.Add(Me.lblQuality)
        Me.grbxDocDeskew.Controls.Add(Me.lblPadColor)
        Me.grbxDocDeskew.Controls.Add(Me.lblMinConfidenceVal)
        Me.grbxDocDeskew.Controls.Add(Me.hscrMinConfidence)
        Me.grbxDocDeskew.Controls.Add(Me.lblMinConfidence)
        Me.grbxDocDeskew.Controls.Add(Me.lblMinAngleVal)
        Me.grbxDocDeskew.Controls.Add(Me.hscrMinAngle)
        Me.grbxDocDeskew.Controls.Add(Me.lblMinAngle)
        Me.grbxDocDeskew.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocDeskew.Name = "grbxDocDeskew"
        Me.grbxDocDeskew.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocDeskew.TabIndex = 0
        Me.grbxDocDeskew.TabStop = False
        Me.grbxDocDeskew.Text = "Document Deskew"
        '
        'rbDeskewBlack
        '
        Me.rbDeskewBlack.Location = New System.Drawing.Point(304, 96)
        Me.rbDeskewBlack.Name = "rbDeskewBlack"
        Me.rbDeskewBlack.Size = New System.Drawing.Size(96, 24)
        Me.rbDeskewBlack.TabIndex = 15
        Me.rbDeskewBlack.Text = "Black"
        '
        'rbDeskewWhite
        '
        Me.rbDeskewWhite.Checked = True
        Me.rbDeskewWhite.Location = New System.Drawing.Point(184, 96)
        Me.rbDeskewWhite.Name = "rbDeskewWhite"
        Me.rbDeskewWhite.Size = New System.Drawing.Size(96, 24)
        Me.rbDeskewWhite.TabIndex = 14
        Me.rbDeskewWhite.TabStop = True
        Me.rbDeskewWhite.Text = "White"
        '
        'cmdDeskew
        '
        Me.cmdDeskew.Location = New System.Drawing.Point(232, 184)
        Me.cmdDeskew.Name = "cmdDeskew"
        Me.cmdDeskew.Size = New System.Drawing.Size(160, 32)
        Me.cmdDeskew.TabIndex = 13
        Me.cmdDeskew.Text = "Document Deskew"
        '
        'chkMaintOrigSize
        '
        Me.chkMaintOrigSize.Location = New System.Drawing.Point(16, 184)
        Me.chkMaintOrigSize.Name = "chkMaintOrigSize"
        Me.chkMaintOrigSize.Size = New System.Drawing.Size(136, 24)
        Me.chkMaintOrigSize.TabIndex = 12
        Me.chkMaintOrigSize.Text = "Maintain Original Size"
        '
        'lblQualityVal
        '
        Me.lblQualityVal.Location = New System.Drawing.Point(544, 136)
        Me.lblQualityVal.Name = "lblQualityVal"
        Me.lblQualityVal.Size = New System.Drawing.Size(48, 24)
        Me.lblQualityVal.TabIndex = 11
        '
        'hscrQuality
        '
        Me.hscrQuality.Location = New System.Drawing.Point(176, 136)
        Me.hscrQuality.Name = "hscrQuality"
        Me.hscrQuality.Size = New System.Drawing.Size(344, 16)
        Me.hscrQuality.TabIndex = 10
        Me.hscrQuality.Value = 80
        '
        'lblQuality
        '
        Me.lblQuality.Location = New System.Drawing.Point(16, 136)
        Me.lblQuality.Name = "lblQuality"
        Me.lblQuality.Size = New System.Drawing.Size(96, 24)
        Me.lblQuality.TabIndex = 9
        Me.lblQuality.Text = "Quality:"
        '
        'lblPadColor
        '
        Me.lblPadColor.Location = New System.Drawing.Point(16, 96)
        Me.lblPadColor.Name = "lblPadColor"
        Me.lblPadColor.Size = New System.Drawing.Size(104, 24)
        Me.lblPadColor.TabIndex = 6
        Me.lblPadColor.Text = "Pad Color:"
        '
        'lblMinConfidenceVal
        '
        Me.lblMinConfidenceVal.Location = New System.Drawing.Point(544, 64)
        Me.lblMinConfidenceVal.Name = "lblMinConfidenceVal"
        Me.lblMinConfidenceVal.Size = New System.Drawing.Size(56, 24)
        Me.lblMinConfidenceVal.TabIndex = 5
        '
        'hscrMinConfidence
        '
        Me.hscrMinConfidence.Location = New System.Drawing.Point(176, 64)
        Me.hscrMinConfidence.Name = "hscrMinConfidence"
        Me.hscrMinConfidence.Size = New System.Drawing.Size(344, 16)
        Me.hscrMinConfidence.TabIndex = 4
        Me.hscrMinConfidence.Value = 50
        '
        'lblMinConfidence
        '
        Me.lblMinConfidence.Location = New System.Drawing.Point(16, 64)
        Me.lblMinConfidence.Name = "lblMinConfidence"
        Me.lblMinConfidence.Size = New System.Drawing.Size(120, 24)
        Me.lblMinConfidence.TabIndex = 3
        Me.lblMinConfidence.Text = "Minimum Confidence:"
        '
        'lblMinAngleVal
        '
        Me.lblMinAngleVal.Location = New System.Drawing.Point(544, 32)
        Me.lblMinAngleVal.Name = "lblMinAngleVal"
        Me.lblMinAngleVal.Size = New System.Drawing.Size(48, 16)
        Me.lblMinAngleVal.TabIndex = 2
        '
        'hscrMinAngle
        '
        Me.hscrMinAngle.Location = New System.Drawing.Point(176, 24)
        Me.hscrMinAngle.Maximum = 500
        Me.hscrMinAngle.Name = "hscrMinAngle"
        Me.hscrMinAngle.Size = New System.Drawing.Size(344, 16)
        Me.hscrMinAngle.TabIndex = 1
        Me.hscrMinAngle.Value = 20
        '
        'lblMinAngle
        '
        Me.lblMinAngle.Location = New System.Drawing.Point(16, 24)
        Me.lblMinAngle.Name = "lblMinAngle"
        Me.lblMinAngle.Size = New System.Drawing.Size(112, 24)
        Me.lblMinAngle.TabIndex = 0
        Me.lblMinAngle.Text = "Minimum Angle:"
        '
        'docShear
        '
        Me.docShear.Controls.Add(Me.grbxDocShear)
        Me.docShear.Location = New System.Drawing.Point(4, 22)
        Me.docShear.Name = "docShear"
        Me.docShear.Size = New System.Drawing.Size(632, 238)
        Me.docShear.TabIndex = 3
        Me.docShear.Text = "Doc Shear"
        '
        'grbxDocShear
        '
        Me.grbxDocShear.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocShear.Controls.Add(Me.cmdShear)
        Me.grbxDocShear.Controls.Add(Me.cboShearType)
        Me.grbxDocShear.Controls.Add(Me.lblShearType)
        Me.grbxDocShear.Controls.Add(Me.rbShearBlack)
        Me.grbxDocShear.Controls.Add(Me.rbShearWhite)
        Me.grbxDocShear.Controls.Add(Me.lblShearPadColor)
        Me.grbxDocShear.Controls.Add(Me.lblShearAngleVal)
        Me.grbxDocShear.Controls.Add(Me.hscrShearAngle)
        Me.grbxDocShear.Controls.Add(Me.lblShearAngle)
        Me.grbxDocShear.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocShear.Name = "grbxDocShear"
        Me.grbxDocShear.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocShear.TabIndex = 0
        Me.grbxDocShear.TabStop = False
        Me.grbxDocShear.Text = "Document Shear"
        '
        'cmdShear
        '
        Me.cmdShear.Location = New System.Drawing.Point(200, 176)
        Me.cmdShear.Name = "cmdShear"
        Me.cmdShear.Size = New System.Drawing.Size(200, 32)
        Me.cmdShear.TabIndex = 8
        Me.cmdShear.Text = "Document Shear"
        '
        'cboShearType
        '
        Me.cboShearType.Items.AddRange(New Object() {"DocShearVertical", "DocShearHorizontal"})
        Me.cboShearType.Location = New System.Drawing.Point(152, 128)
        Me.cboShearType.Name = "cboShearType"
        Me.cboShearType.Size = New System.Drawing.Size(232, 21)
        Me.cboShearType.TabIndex = 7
        '
        'lblShearType
        '
        Me.lblShearType.Location = New System.Drawing.Point(24, 128)
        Me.lblShearType.Name = "lblShearType"
        Me.lblShearType.Size = New System.Drawing.Size(120, 24)
        Me.lblShearType.TabIndex = 6
        Me.lblShearType.Text = "Shear Type:"
        '
        'rbShearBlack
        '
        Me.rbShearBlack.Location = New System.Drawing.Point(304, 72)
        Me.rbShearBlack.Name = "rbShearBlack"
        Me.rbShearBlack.Size = New System.Drawing.Size(128, 24)
        Me.rbShearBlack.TabIndex = 5
        Me.rbShearBlack.Text = "Black"
        '
        'rbShearWhite
        '
        Me.rbShearWhite.Checked = True
        Me.rbShearWhite.Location = New System.Drawing.Point(168, 72)
        Me.rbShearWhite.Name = "rbShearWhite"
        Me.rbShearWhite.Size = New System.Drawing.Size(112, 24)
        Me.rbShearWhite.TabIndex = 4
        Me.rbShearWhite.TabStop = True
        Me.rbShearWhite.Text = "White"
        '
        'lblShearPadColor
        '
        Me.lblShearPadColor.Location = New System.Drawing.Point(24, 72)
        Me.lblShearPadColor.Name = "lblShearPadColor"
        Me.lblShearPadColor.Size = New System.Drawing.Size(120, 24)
        Me.lblShearPadColor.TabIndex = 3
        Me.lblShearPadColor.Text = "Pad Color:"
        '
        'lblShearAngleVal
        '
        Me.lblShearAngleVal.Location = New System.Drawing.Point(536, 32)
        Me.lblShearAngleVal.Name = "lblShearAngleVal"
        Me.lblShearAngleVal.Size = New System.Drawing.Size(56, 16)
        Me.lblShearAngleVal.TabIndex = 2
        '
        'hscrShearAngle
        '
        Me.hscrShearAngle.Location = New System.Drawing.Point(152, 32)
        Me.hscrShearAngle.Maximum = 70
        Me.hscrShearAngle.Minimum = -70
        Me.hscrShearAngle.Name = "hscrShearAngle"
        Me.hscrShearAngle.Size = New System.Drawing.Size(368, 16)
        Me.hscrShearAngle.TabIndex = 1
        '
        'lblShearAngle
        '
        Me.lblShearAngle.Location = New System.Drawing.Point(24, 32)
        Me.lblShearAngle.Name = "lblShearAngle"
        Me.lblShearAngle.Size = New System.Drawing.Size(104, 24)
        Me.lblShearAngle.TabIndex = 0
        Me.lblShearAngle.Text = "Shear Angle:"
        '
        'docErode
        '
        Me.docErode.Controls.Add(Me.grbxDocErode)
        Me.docErode.Location = New System.Drawing.Point(4, 22)
        Me.docErode.Name = "docErode"
        Me.docErode.Size = New System.Drawing.Size(632, 238)
        Me.docErode.TabIndex = 1
        Me.docErode.Text = "Doc Erode"
        '
        'grbxDocErode
        '
        Me.grbxDocErode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocErode.Controls.Add(Me.cmdErode)
        Me.grbxDocErode.Controls.Add(Me.cboErodeDirection)
        Me.grbxDocErode.Controls.Add(Me.lblErodeDirection)
        Me.grbxDocErode.Controls.Add(Me.lblErodeAmountVal)
        Me.grbxDocErode.Controls.Add(Me.hscrErodeAmount)
        Me.grbxDocErode.Controls.Add(Me.lblErodeAmount)
        Me.grbxDocErode.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocErode.Name = "grbxDocErode"
        Me.grbxDocErode.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocErode.TabIndex = 0
        Me.grbxDocErode.TabStop = False
        Me.grbxDocErode.Text = "Document Erode"
        '
        'cmdErode
        '
        Me.cmdErode.Location = New System.Drawing.Point(240, 176)
        Me.cmdErode.Name = "cmdErode"
        Me.cmdErode.Size = New System.Drawing.Size(144, 32)
        Me.cmdErode.TabIndex = 5
        Me.cmdErode.Text = "Document Erode"
        '
        'cboErodeDirection
        '
        Me.cboErodeDirection.Items.AddRange(New Object() {"EnhancementDirectionAll", "EnhancementDirectionLeft", "EnhancementDirectionRight", "EnhancementDirectionUp", "EnhancementDirectionDown", "EnhancementDirectionLeftUp", "EnhancementDirectionLeftDown", "EnhancementDirectionRightUp", "EnhancementDirectionRightDown"})
        Me.cboErodeDirection.Location = New System.Drawing.Point(192, 112)
        Me.cboErodeDirection.Name = "cboErodeDirection"
        Me.cboErodeDirection.Size = New System.Drawing.Size(216, 21)
        Me.cboErodeDirection.TabIndex = 4
        '
        'lblErodeDirection
        '
        Me.lblErodeDirection.Location = New System.Drawing.Point(24, 112)
        Me.lblErodeDirection.Name = "lblErodeDirection"
        Me.lblErodeDirection.Size = New System.Drawing.Size(128, 24)
        Me.lblErodeDirection.TabIndex = 3
        Me.lblErodeDirection.Text = "Direction:"
        '
        'lblErodeAmountVal
        '
        Me.lblErodeAmountVal.Location = New System.Drawing.Point(536, 40)
        Me.lblErodeAmountVal.Name = "lblErodeAmountVal"
        Me.lblErodeAmountVal.Size = New System.Drawing.Size(64, 24)
        Me.lblErodeAmountVal.TabIndex = 2
        '
        'hscrErodeAmount
        '
        Me.hscrErodeAmount.Location = New System.Drawing.Point(192, 40)
        Me.hscrErodeAmount.Maximum = 500
        Me.hscrErodeAmount.Minimum = 1
        Me.hscrErodeAmount.Name = "hscrErodeAmount"
        Me.hscrErodeAmount.Size = New System.Drawing.Size(320, 16)
        Me.hscrErodeAmount.TabIndex = 1
        Me.hscrErodeAmount.Value = 1
        '
        'lblErodeAmount
        '
        Me.lblErodeAmount.Location = New System.Drawing.Point(24, 40)
        Me.lblErodeAmount.Name = "lblErodeAmount"
        Me.lblErodeAmount.Size = New System.Drawing.Size(120, 24)
        Me.lblErodeAmount.TabIndex = 0
        Me.lblErodeAmount.Text = "Erode Amount:"
        '
        'docDilate
        '
        Me.docDilate.Controls.Add(Me.grbxDocDilate)
        Me.docDilate.Location = New System.Drawing.Point(4, 22)
        Me.docDilate.Name = "docDilate"
        Me.docDilate.Size = New System.Drawing.Size(632, 238)
        Me.docDilate.TabIndex = 4
        Me.docDilate.Text = "Doc Dilate"
        '
        'grbxDocDilate
        '
        Me.grbxDocDilate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocDilate.Controls.Add(Me.cmdDilate)
        Me.grbxDocDilate.Controls.Add(Me.cboDilateDirection)
        Me.grbxDocDilate.Controls.Add(Me.lblDilateDirection)
        Me.grbxDocDilate.Controls.Add(Me.lblDilateAmountVal)
        Me.grbxDocDilate.Controls.Add(Me.hscrDilateAmount)
        Me.grbxDocDilate.Controls.Add(Me.lblDilateAmount)
        Me.grbxDocDilate.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocDilate.Name = "grbxDocDilate"
        Me.grbxDocDilate.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocDilate.TabIndex = 0
        Me.grbxDocDilate.TabStop = False
        Me.grbxDocDilate.Text = "Document Dilate"
        '
        'cmdDilate
        '
        Me.cmdDilate.Location = New System.Drawing.Point(232, 176)
        Me.cmdDilate.Name = "cmdDilate"
        Me.cmdDilate.Size = New System.Drawing.Size(152, 32)
        Me.cmdDilate.TabIndex = 5
        Me.cmdDilate.Text = "Document Dilate"
        '
        'cboDilateDirection
        '
        Me.cboDilateDirection.Items.AddRange(New Object() {"EnhancementDirectionAll", "EnhancementDirectionLeft", "EnhancementDirectionRight", "EnhancementDirectionUp", "EnhancementDirectionDown", "EnhancementDirectionLeftUp", "EnhancementDirectionLeftDown", "EnhancementDirectionRightUp", "EnhancementDirectionRightDown"})
        Me.cboDilateDirection.Location = New System.Drawing.Point(184, 112)
        Me.cboDilateDirection.Name = "cboDilateDirection"
        Me.cboDilateDirection.Size = New System.Drawing.Size(224, 21)
        Me.cboDilateDirection.TabIndex = 4
        '
        'lblDilateDirection
        '
        Me.lblDilateDirection.Location = New System.Drawing.Point(24, 112)
        Me.lblDilateDirection.Name = "lblDilateDirection"
        Me.lblDilateDirection.Size = New System.Drawing.Size(128, 16)
        Me.lblDilateDirection.TabIndex = 3
        Me.lblDilateDirection.Text = "Direction:"
        '
        'lblDilateAmountVal
        '
        Me.lblDilateAmountVal.Location = New System.Drawing.Point(536, 40)
        Me.lblDilateAmountVal.Name = "lblDilateAmountVal"
        Me.lblDilateAmountVal.Size = New System.Drawing.Size(64, 24)
        Me.lblDilateAmountVal.TabIndex = 2
        '
        'hscrDilateAmount
        '
        Me.hscrDilateAmount.Location = New System.Drawing.Point(184, 40)
        Me.hscrDilateAmount.Maximum = 500
        Me.hscrDilateAmount.Minimum = 1
        Me.hscrDilateAmount.Name = "hscrDilateAmount"
        Me.hscrDilateAmount.Size = New System.Drawing.Size(336, 16)
        Me.hscrDilateAmount.TabIndex = 1
        Me.hscrDilateAmount.Value = 1
        '
        'lblDilateAmount
        '
        Me.lblDilateAmount.Location = New System.Drawing.Point(24, 40)
        Me.lblDilateAmount.Name = "lblDilateAmount"
        Me.lblDilateAmount.Size = New System.Drawing.Size(136, 16)
        Me.lblDilateAmount.TabIndex = 0
        Me.lblDilateAmount.Text = "Dilate Amount:"
        '
        'docDespeckle
        '
        Me.docDespeckle.Controls.Add(Me.grbxDocDespeckle)
        Me.docDespeckle.Location = New System.Drawing.Point(4, 22)
        Me.docDespeckle.Name = "docDespeckle"
        Me.docDespeckle.Size = New System.Drawing.Size(632, 238)
        Me.docDespeckle.TabIndex = 5
        Me.docDespeckle.Text = "Doc Despeckle"
        '
        'grbxDocDespeckle
        '
        Me.grbxDocDespeckle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocDespeckle.Controls.Add(Me.cmdDespeckle)
        Me.grbxDocDespeckle.Controls.Add(Me.lblDespSpeckHVal)
        Me.grbxDocDespeckle.Controls.Add(Me.hscrDespSpeckHeight)
        Me.grbxDocDespeckle.Controls.Add(Me.lblDespSpeckHeight)
        Me.grbxDocDespeckle.Controls.Add(Me.lblDespSpeckWVal)
        Me.grbxDocDespeckle.Controls.Add(Me.hscrDespSpeckWidth)
        Me.grbxDocDespeckle.Controls.Add(Me.lblSpeckWidth)
        Me.grbxDocDespeckle.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocDespeckle.Name = "grbxDocDespeckle"
        Me.grbxDocDespeckle.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocDespeckle.TabIndex = 0
        Me.grbxDocDespeckle.TabStop = False
        Me.grbxDocDespeckle.Text = "Document Despeckle"
        '
        'cmdDespeckle
        '
        Me.cmdDespeckle.Location = New System.Drawing.Point(240, 168)
        Me.cmdDespeckle.Name = "cmdDespeckle"
        Me.cmdDespeckle.Size = New System.Drawing.Size(136, 40)
        Me.cmdDespeckle.TabIndex = 6
        Me.cmdDespeckle.Text = "Document Despeckle"
        '
        'lblDespSpeckHVal
        '
        Me.lblDespSpeckHVal.Location = New System.Drawing.Point(528, 96)
        Me.lblDespSpeckHVal.Name = "lblDespSpeckHVal"
        Me.lblDespSpeckHVal.Size = New System.Drawing.Size(56, 24)
        Me.lblDespSpeckHVal.TabIndex = 5
        '
        'hscrDespSpeckHeight
        '
        Me.hscrDespSpeckHeight.Location = New System.Drawing.Point(192, 96)
        Me.hscrDespSpeckHeight.Minimum = 1
        Me.hscrDespSpeckHeight.Name = "hscrDespSpeckHeight"
        Me.hscrDespSpeckHeight.Size = New System.Drawing.Size(312, 16)
        Me.hscrDespSpeckHeight.TabIndex = 4
        Me.hscrDespSpeckHeight.Value = 2
        '
        'lblDespSpeckHeight
        '
        Me.lblDespSpeckHeight.Location = New System.Drawing.Point(24, 96)
        Me.lblDespSpeckHeight.Name = "lblDespSpeckHeight"
        Me.lblDespSpeckHeight.Size = New System.Drawing.Size(128, 32)
        Me.lblDespSpeckHeight.TabIndex = 3
        Me.lblDespSpeckHeight.Text = "Speck Height:"
        '
        'lblDespSpeckWVal
        '
        Me.lblDespSpeckWVal.Location = New System.Drawing.Point(528, 40)
        Me.lblDespSpeckWVal.Name = "lblDespSpeckWVal"
        Me.lblDespSpeckWVal.Size = New System.Drawing.Size(64, 24)
        Me.lblDespSpeckWVal.TabIndex = 2
        '
        'hscrDespSpeckWidth
        '
        Me.hscrDespSpeckWidth.Location = New System.Drawing.Point(192, 40)
        Me.hscrDespSpeckWidth.Minimum = 1
        Me.hscrDespSpeckWidth.Name = "hscrDespSpeckWidth"
        Me.hscrDespSpeckWidth.Size = New System.Drawing.Size(312, 16)
        Me.hscrDespSpeckWidth.TabIndex = 1
        Me.hscrDespSpeckWidth.Value = 2
        '
        'lblSpeckWidth
        '
        Me.lblSpeckWidth.Location = New System.Drawing.Point(24, 40)
        Me.lblSpeckWidth.Name = "lblSpeckWidth"
        Me.lblSpeckWidth.Size = New System.Drawing.Size(136, 24)
        Me.lblSpeckWidth.TabIndex = 0
        Me.lblSpeckWidth.Text = "Speck Width:"
        '
        'docZoomSmooth
        '
        Me.docZoomSmooth.Controls.Add(Me.grbxDocZoomSmooth)
        Me.docZoomSmooth.Location = New System.Drawing.Point(4, 22)
        Me.docZoomSmooth.Name = "docZoomSmooth"
        Me.docZoomSmooth.Size = New System.Drawing.Size(632, 238)
        Me.docZoomSmooth.TabIndex = 7
        Me.docZoomSmooth.Text = "Doc ZoomSmooth"
        '
        'grbxDocZoomSmooth
        '
        Me.grbxDocZoomSmooth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbxDocZoomSmooth.Controls.Add(Me.cmdZoomSmooth)
        Me.grbxDocZoomSmooth.Location = New System.Drawing.Point(8, 8)
        Me.grbxDocZoomSmooth.Name = "grbxDocZoomSmooth"
        Me.grbxDocZoomSmooth.Size = New System.Drawing.Size(616, 223)
        Me.grbxDocZoomSmooth.TabIndex = 0
        Me.grbxDocZoomSmooth.TabStop = False
        Me.grbxDocZoomSmooth.Text = "Document ZoomSmooth"
        '
        'cmdZoomSmooth
        '
        Me.cmdZoomSmooth.Location = New System.Drawing.Point(208, 136)
        Me.cmdZoomSmooth.Name = "cmdZoomSmooth"
        Me.cmdZoomSmooth.Size = New System.Drawing.Size(184, 40)
        Me.cmdZoomSmooth.TabIndex = 0
        Me.cmdZoomSmooth.Text = "Document ZoomSmooth"
        '
        'DocumentImagingandCleanUp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(912, 665)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.lblLastError)
        Me.Controls.Add(Me.ImageXView2)
        Me.Controls.Add(Me.ImageXView1)
        Me.Menu = Me.MainMenu1
        Me.Name = "DocumentImagingandCleanUp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DocumentImagingandImageCleanUp"
        Me.TabControl1.ResumeLayout(False)
        Me.docBorderCrop.ResumeLayout(False)
        Me.grbxDocBorderCrop.ResumeLayout(False)
        Me.docLineRemoval.ResumeLayout(False)
        Me.grbxDocLineRemoval.ResumeLayout(False)
        Me.docDeskew.ResumeLayout(False)
        Me.grbxDocDeskew.ResumeLayout(False)
        Me.docShear.ResumeLayout(False)
        Me.grbxDocShear.ResumeLayout(False)
        Me.docErode.ResumeLayout(False)
        Me.grbxDocErode.ResumeLayout(False)
        Me.docDilate.ResumeLayout(False)
        Me.grbxDocDilate.ResumeLayout(False)
        Me.docDespeckle.ResumeLayout(False)
        Me.grbxDocDespeckle.ResumeLayout(False)
        Me.docZoomSmooth.ResumeLayout(False)
        Me.grbxDocZoomSmooth.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument

#Region "Pegasus Imaging Sample Application Standard Functions"

    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\Common\Images\"

    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "\..\..\..\..\..\..\..\Common\Images")
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.gif;*.hdp;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wdp;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*" & _
 ".dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.h" & _
 "dp;*.wdp|All Files (*.*)|*.*"

    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region


    Private Sub cmdBorderCrop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorderCrop.Click

        Try
            ixProcessor1.Image = imagX1.Copy()
            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentBorderCrop()
            ImageXView2.Image = ixProcessor1.Image()
            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)

            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub DocumentImagingandCleanUp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)

            strimageFile = System.IO.Path.Combine(strCurrentDir, "barcode.pcx")

            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strimageFile)
            ImageXView1.ZoomToFit(ZoomToFitType.FitBest)
            imagX1 = New PegasusImaging.WinForms.ImagXpress9.ImageX(ImagXpress1)
            imagX1 = ImageXView1.Image
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try
        Try


        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try


        cPadColor = New System.Drawing.Color()
        ixProcessor1 = New PegasusImaging.WinForms.ImagXpress9.Processor(ImagXpress1)


        lblMinAngleVal.Text = hscrMinAngle.Value * 0.01
        lblMinConfidenceVal.Text = hscrMinConfidence.Value
        lblQualityVal.Text = hscrQuality.Value
        lblDespSpeckWVal.Text = hscrDespSpeckWidth.Value
        lblDespSpeckHVal.Text = hscrDespSpeckHeight.Value
        lblDilateAmountVal.Text = hscrDilateAmount.Value
        lblErodeAmountVal.Text = hscrErodeAmount.Value
        lblShearAngleVal.Text = hscrShearAngle.Value
        lblMinLengthVal.Text = hscrMinLength.Value
        lblMaxThicknessVal.Text = hscrMaxThickness.Value
        lblMinAspRatioVal.Text = hscrMinAspRatio.Value
        lblMaxGapVal.Text = hscrMaxGap.Value
        lblMaxCharRepSizeVal.Text = hscrlMaxCharRepSize.Value

        cboDilateDirection.SelectedIndex = 0
        cboErodeDirection.SelectedIndex = 0
        cboShearType.SelectedIndex = 0


        cPadColor = Color.FromKnownColor(KnownColor.White)
        iDeskewPadCol = cPadColor.ToArgb()
        iShearPadCol = cPadColor.ToArgb()

    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then
            Try
                strimageFile = strtemp
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strimageFile)
                ImageXView1.ZoomToFit(ZoomToFitType.FitBest)

                If Not imagX1 Is Nothing Then
                    imagX1.Dispose()
                    imagX1 = ImageXView1.Image
                End If
            Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
                PegasusError(ex, lblError)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblError)
            End Try
        End If

    End Sub

    Private Sub cmdDeskew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeskew.Click
        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentDeskew(hscrMinAngle.Value * 0.01, hscrMinConfidence.Value, iDeskewPadCol, _
                                        chkMaintOrigSize.Checked, hscrQuality.Value)
            ImageXView2.Image = ixProcessor1.Image
            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)

            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub



    Private Sub cmdDespeckle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDespeckle.Click

        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentDespeckle(hscrDespSpeckWidth.Value, hscrDespSpeckHeight.Height)
            ImageXView2.Image = ixProcessor1.Image

            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub


    Private Sub cmdDilate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDilate.Click

        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentDilate(hscrDilateAmount.Value, cboDilateDirection.SelectedIndex)
            ImageXView2.Image = ixProcessor1.Image

            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub



    Private Sub cmdErode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdErode.Click

        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentErode(hscrErodeAmount.Value, cboErodeDirection.SelectedIndex)
            ImageXView2.Image = ixProcessor1.Image

            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub



    Private Sub cmdShear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShear.Click

        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentShear(hscrShearAngle.Value, iShearPadCol, cboShearType.SelectedIndex)
            ImageXView2.Image = ixProcessor1.Image

            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub cmdLineRemoval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLineRemoval.Click
        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentLineRemoval(hscrMinLength.Value, hscrMaxThickness.Value, hscrMinAspRatio.Value, _
                                            hscrMaxGap.Value, hscrlMaxCharRepSize.Value)
            ImageXView2.Image = ixProcessor1.Image

            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub

    Private Sub cmdZoomSmooth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdZoomSmooth.Click

        Try

            ixProcessor1.Image = imagX1.Copy

            If imagX1.ImageXData.BitsPerPixel > 1 Then
                MessageBox.Show("Image is " + imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")
                ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone)

            End If

            ixProcessor1.DocumentZoomSmooth()
            ImageXView2.Image = ixProcessor1.Image

            ImageXView2.ZoomToFit(ZoomToFitType.FitBest)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub

    Private Sub hscrMinLength_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMinLength.Scroll
        lblMinLengthVal.Text = hscrMinLength.Value
    End Sub

    Private Sub hscrMaxThickness_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMaxThickness.Scroll
        lblMaxThicknessVal.Text = hscrMaxThickness.Value

    End Sub

    Private Sub hscrMinAspRatio_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMinAspRatio.Scroll
        lblMinAspRatioVal.Text = hscrMinAspRatio.Value
    End Sub


    Private Sub hscrMaxGap_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMaxGap.Scroll
        lblMaxGapVal.Text = hscrMaxGap.Value
    End Sub

    Private Sub hscrlMaxCharRepSize_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrlMaxCharRepSize.Scroll
        lblMaxCharRepSizeVal.Text = hscrlMaxCharRepSize.Value
    End Sub

    Private Sub hscrMinAngle_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMinAngle.Scroll
        lblMinAngleVal.Text = hscrMinAngle.Value * 0.01
    End Sub

    Private Sub hscrMinConfidence_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrMinConfidence.Scroll
        lblMinConfidenceVal.Text = hscrMinConfidence.Value
    End Sub


    Private Sub hscrQuality_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrQuality.Scroll
        lblQualityVal.Text = hscrQuality.Value
    End Sub

    Private Sub rbDeskewWhite_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbDeskewWhite.CheckedChanged
        If rbDeskewWhite.Checked = True Then

            cPadColor = Color.FromKnownColor(KnownColor.White)
            iDeskewPadCol = cPadColor.ToArgb()
        End If
    End Sub

    Private Sub rbDeskewBlack_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbDeskewBlack.CheckedChanged
        If rbDeskewBlack.Checked = True Then

            cPadColor = Color.FromKnownColor(KnownColor.Black)
            iDeskewPadCol = cPadColor.ToArgb()
        End If
    End Sub

    Private Sub hscrDespSpeckWidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrDespSpeckWidth.Scroll
        lblDespSpeckWVal.Text = hscrDespSpeckWidth.Value
    End Sub

    Private Sub hscrDespSpeckHeight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrDespSpeckHeight.Scroll
        lblDespSpeckHVal.Text = hscrDespSpeckHeight.Value
    End Sub

    Private Sub hscrShearAngle_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrShearAngle.Scroll

        lblShearAngleVal.Text = hscrShearAngle.Value

    End Sub

    Private Sub rbShearWhite_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbShearWhite.CheckedChanged
        If rbShearWhite.Checked = True Then

            cPadColor = Color.FromKnownColor(KnownColor.White)
            iShearPadCol = cPadColor.ToArgb()
        End If
    End Sub

    Private Sub rbShearBlack_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbShearBlack.CheckedChanged
        If rbShearBlack.Checked = True Then

            cPadColor = Color.FromKnownColor(KnownColor.Black)
            iShearPadCol = cPadColor.ToArgb()
        End If
    End Sub

    Private Sub hscrDilateAmount_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrDilateAmount.Scroll
        lblDilateAmountVal.Text = hscrDilateAmount.Value
    End Sub

    Private Sub hscrErodeAmount_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrErodeAmount.Scroll
        lblErodeAmountVal.Text = hscrErodeAmount.Value
    End Sub


    Private Sub mnuFileQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileQuit.Click
        Application.Exit()
    End Sub


    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click

        If ImageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False
            ImageXView2.Toolbar.Activated = False

        Else
            mnuToolbarShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True
            ImageXView2.Toolbar.Activated = True
        End If
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        Try
            ImagXpress1.AboutBox()
        Catch eX As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(eX, lblError)
        End Try
    End Sub

End Class

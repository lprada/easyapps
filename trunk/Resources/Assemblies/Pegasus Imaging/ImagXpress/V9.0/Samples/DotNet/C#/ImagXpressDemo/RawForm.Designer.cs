/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class RawForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (bv != null)
            {
                bv.Dispose();
                bv = null;
            }
            if (imageXView1 != null)
            {
                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }

                imageXView1.Dispose();
                imageXView1 = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RawForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.OKButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.WidthBox = new System.Windows.Forms.NumericUpDown();
            this.HeightBox = new System.Windows.Forms.NumericUpDown();
            this.WidthLabel = new System.Windows.Forms.Label();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.FormatComboBox = new System.Windows.Forms.ComboBox();
            this.FormatLabel = new System.Windows.Forms.Label();
            this.StrideLabel = new System.Windows.Forms.Label();
            this.StrideBox = new System.Windows.Forms.NumericUpDown();
            this.BitsLabel = new System.Windows.Forms.Label();
            this.BitsBox = new System.Windows.Forms.NumericUpDown();
            this.BytesLabel = new System.Windows.Forms.Label();
            this.BytesBox = new System.Windows.Forms.NumericUpDown();
            this.HighBitLabel = new System.Windows.Forms.Label();
            this.HighBox = new System.Windows.Forms.NumericUpDown();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.offsetLabel = new System.Windows.Forms.Label();
            this.offsetBox = new System.Windows.Forms.NumericUpDown();
            this.DescriptionListBox = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.WidthBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeightBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrideBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BitsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BytesBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.offsetBox)).BeginInit();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(402, 684);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(109, 23);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CButton
            // 
            this.CButton.Location = new System.Drawing.Point(533, 684);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(109, 23);
            this.CButton.TabIndex = 5;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // WidthBox
            // 
            this.WidthBox.Location = new System.Drawing.Point(69, 96);
            this.WidthBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.WidthBox.Name = "WidthBox";
            this.WidthBox.Size = new System.Drawing.Size(66, 20);
            this.WidthBox.TabIndex = 6;
            this.WidthBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.WidthBox.ValueChanged += new System.EventHandler(this.WidthBox_ValueChanged);
            // 
            // HeightBox
            // 
            this.HeightBox.Location = new System.Drawing.Point(69, 125);
            this.HeightBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.HeightBox.Name = "HeightBox";
            this.HeightBox.Size = new System.Drawing.Size(66, 20);
            this.HeightBox.TabIndex = 7;
            this.HeightBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.HeightBox.ValueChanged += new System.EventHandler(this.HeightBox_ValueChanged);
            // 
            // WidthLabel
            // 
            this.WidthLabel.AutoSize = true;
            this.WidthLabel.Location = new System.Drawing.Point(25, 98);
            this.WidthLabel.Name = "WidthLabel";
            this.WidthLabel.Size = new System.Drawing.Size(38, 13);
            this.WidthLabel.TabIndex = 8;
            this.WidthLabel.Text = "Width:";
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Location = new System.Drawing.Point(26, 127);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(41, 13);
            this.HeightLabel.TabIndex = 9;
            this.HeightLabel.Text = "Height:";
            // 
            // FormatComboBox
            // 
            this.FormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FormatComboBox.FormattingEnabled = true;
            this.FormatComboBox.Items.AddRange(new object[] {
            "Dib",
            "Raw",
            "Big Endian",
            "Top Down",
            "Signed",
            "Gray",
            "High Gray",
            "Alpha",
            "Rgb",
            "Rgb555",
            "Rgb565",
            "Argb",
            "ColorMapped",
            "Cmyk",
            "Compressed",
            "NonInterleaved",
            "NoDibPad"});
            this.FormatComboBox.Location = new System.Drawing.Point(226, 94);
            this.FormatComboBox.Name = "FormatComboBox";
            this.FormatComboBox.Size = new System.Drawing.Size(159, 21);
            this.FormatComboBox.TabIndex = 10;
            this.FormatComboBox.SelectedIndexChanged += new System.EventHandler(this.FormatComboBox_SelectedIndexChanged);
            // 
            // FormatLabel
            // 
            this.FormatLabel.AutoSize = true;
            this.FormatLabel.Location = new System.Drawing.Point(153, 97);
            this.FormatLabel.Name = "FormatLabel";
            this.FormatLabel.Size = new System.Drawing.Size(67, 13);
            this.FormatLabel.TabIndex = 11;
            this.FormatLabel.Text = "Pixel Format:";
            // 
            // StrideLabel
            // 
            this.StrideLabel.AutoSize = true;
            this.StrideLabel.Location = new System.Drawing.Point(25, 153);
            this.StrideLabel.Name = "StrideLabel";
            this.StrideLabel.Size = new System.Drawing.Size(37, 13);
            this.StrideLabel.TabIndex = 13;
            this.StrideLabel.Text = "Stride:";
            // 
            // StrideBox
            // 
            this.StrideBox.Location = new System.Drawing.Point(69, 151);
            this.StrideBox.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.StrideBox.Name = "StrideBox";
            this.StrideBox.Size = new System.Drawing.Size(67, 20);
            this.StrideBox.TabIndex = 12;
            this.StrideBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.StrideBox.ValueChanged += new System.EventHandler(this.StrideBox_ValueChanged);
            // 
            // BitsLabel
            // 
            this.BitsLabel.AutoSize = true;
            this.BitsLabel.Location = new System.Drawing.Point(153, 180);
            this.BitsLabel.Name = "BitsLabel";
            this.BitsLabel.Size = new System.Drawing.Size(71, 13);
            this.BitsLabel.TabIndex = 15;
            this.BitsLabel.Text = "Bits Per Pixel:";
            // 
            // BitsBox
            // 
            this.BitsBox.Location = new System.Drawing.Point(239, 180);
            this.BitsBox.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.BitsBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.BitsBox.Name = "BitsBox";
            this.BitsBox.Size = new System.Drawing.Size(38, 20);
            this.BitsBox.TabIndex = 14;
            this.BitsBox.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.BitsBox.ValueChanged += new System.EventHandler(this.BitsBox_ValueChanged);
            // 
            // BytesLabel
            // 
            this.BytesLabel.AutoSize = true;
            this.BytesLabel.Location = new System.Drawing.Point(153, 129);
            this.BytesLabel.Name = "BytesLabel";
            this.BytesLabel.Size = new System.Drawing.Size(80, 13);
            this.BytesLabel.TabIndex = 17;
            this.BytesLabel.Text = "Bytes Per Pixel:";
            // 
            // BytesBox
            // 
            this.BytesBox.Location = new System.Drawing.Point(239, 124);
            this.BytesBox.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.BytesBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.BytesBox.Name = "BytesBox";
            this.BytesBox.Size = new System.Drawing.Size(38, 20);
            this.BytesBox.TabIndex = 16;
            this.BytesBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.BytesBox.ValueChanged += new System.EventHandler(this.BytesBox_ValueChanged);
            // 
            // HighBitLabel
            // 
            this.HighBitLabel.AutoSize = true;
            this.HighBitLabel.Location = new System.Drawing.Point(153, 153);
            this.HighBitLabel.Name = "HighBitLabel";
            this.HighBitLabel.Size = new System.Drawing.Size(76, 13);
            this.HighBitLabel.TabIndex = 19;
            this.HighBitLabel.Text = "High Bit Index:";
            // 
            // HighBox
            // 
            this.HighBox.Location = new System.Drawing.Point(238, 151);
            this.HighBox.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.HighBox.Name = "HighBox";
            this.HighBox.Size = new System.Drawing.Size(38, 20);
            this.HighBox.TabIndex = 18;
            this.HighBox.ValueChanged += new System.EventHandler(this.HighBox_ValueChanged);
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(430, 11);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(212, 190);
            this.imageXView1.TabIndex = 20;
            // 
            // offsetLabel
            // 
            this.offsetLabel.AutoSize = true;
            this.offsetLabel.Location = new System.Drawing.Point(5, 183);
            this.offsetLabel.Name = "offsetLabel";
            this.offsetLabel.Size = new System.Drawing.Size(57, 13);
            this.offsetLabel.TabIndex = 23;
            this.offsetLabel.Text = "File Offset:";
            // 
            // offsetBox
            // 
            this.offsetBox.Location = new System.Drawing.Point(70, 181);
            this.offsetBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.offsetBox.Name = "offsetBox";
            this.offsetBox.Size = new System.Drawing.Size(66, 20);
            this.offsetBox.TabIndex = 22;
            this.offsetBox.ValueChanged += new System.EventHandler(this.offsetBox_ValueChanged);
            // 
            // DescriptionListBox
            // 
            this.DescriptionListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionListBox.FormattingEnabled = true;
            this.DescriptionListBox.Items.AddRange(new object[] {
            "To load a RAW image (not the same as Camera RAW), you must",
            "specify image width, height, stride (width times number of bytes",
            "per pixel), file offset, pixel format, bytes per pixel,  high bit index,",
            "& bits per pixel."});
            this.DescriptionListBox.Location = new System.Drawing.Point(12, 11);
            this.DescriptionListBox.Name = "DescriptionListBox";
            this.DescriptionListBox.Size = new System.Drawing.Size(393, 69);
            this.DescriptionListBox.TabIndex = 24;
            // 
            // RawForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 719);
            this.Controls.Add(this.DescriptionListBox);
            this.Controls.Add(this.offsetLabel);
            this.Controls.Add(this.offsetBox);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.HighBitLabel);
            this.Controls.Add(this.HighBox);
            this.Controls.Add(this.BytesLabel);
            this.Controls.Add(this.BytesBox);
            this.Controls.Add(this.BitsLabel);
            this.Controls.Add(this.BitsBox);
            this.Controls.Add(this.StrideLabel);
            this.Controls.Add(this.StrideBox);
            this.Controls.Add(this.FormatLabel);
            this.Controls.Add(this.FormatComboBox);
            this.Controls.Add(this.HeightLabel);
            this.Controls.Add(this.WidthLabel);
            this.Controls.Add(this.HeightBox);
            this.Controls.Add(this.WidthBox);
            this.Controls.Add(this.CButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RawForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Raw";
            this.Load += new System.EventHandler(this.RawForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.WidthBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeightBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StrideBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BitsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BytesBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.offsetBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.NumericUpDown WidthBox;
        private System.Windows.Forms.NumericUpDown HeightBox;
        private System.Windows.Forms.Label WidthLabel;
        private System.Windows.Forms.Label HeightLabel;
        private System.Windows.Forms.ComboBox FormatComboBox;
        private System.Windows.Forms.Label FormatLabel;
        private System.Windows.Forms.Label StrideLabel;
        private System.Windows.Forms.NumericUpDown StrideBox;
        private System.Windows.Forms.Label BitsLabel;
        private System.Windows.Forms.NumericUpDown BitsBox;
        private System.Windows.Forms.Label BytesLabel;
        private System.Windows.Forms.NumericUpDown BytesBox;
        private System.Windows.Forms.Label HighBitLabel;
        private System.Windows.Forms.NumericUpDown HighBox;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private System.Windows.Forms.Label offsetLabel;
        private System.Windows.Forms.NumericUpDown offsetBox;
        private System.Windows.Forms.ListBox DescriptionListBox;
    }
}
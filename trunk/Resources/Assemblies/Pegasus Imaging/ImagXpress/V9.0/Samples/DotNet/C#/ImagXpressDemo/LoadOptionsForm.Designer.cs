/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class LoadOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadOptionsForm));
            this.CameraRawCheckBox = new System.Windows.Forms.CheckBox();
            this.renderIntentComboBox = new System.Windows.Forms.ComboBox();
            this.CropXBox = new System.Windows.Forms.NumericUpDown();
            this.CropYBox = new System.Windows.Forms.NumericUpDown();
            this.CropWBox = new System.Windows.Forms.NumericUpDown();
            this.CropHBox = new System.Windows.Forms.NumericUpDown();
            this.CropXLabel = new System.Windows.Forms.Label();
            this.CropYLabel = new System.Windows.Forms.Label();
            this.CropWLabel = new System.Windows.Forms.Label();
            this.CropHLabel = new System.Windows.Forms.Label();
            this.OffsetTextBox = new System.Windows.Forms.TextBox();
            this.OffsetLabel = new System.Windows.Forms.Label();
            this.FTPComboBox = new System.Windows.Forms.ComboBox();
            this.FTPLabel = new System.Windows.Forms.Label();
            this.LoadAlphaCheckBox = new System.Windows.Forms.CheckBox();
            this.LoadLabel = new System.Windows.Forms.Label();
            this.LoadComboBox = new System.Windows.Forms.ComboBox();
            this.MaintainCheckBox = new System.Windows.Forms.CheckBox();
            this.ConvertCheckBox = new System.Windows.Forms.CheckBox();
            this.ResYBox = new System.Windows.Forms.NumericUpDown();
            this.ResXBox = new System.Windows.Forms.NumericUpDown();
            this.UnitsLabel = new System.Windows.Forms.Label();
            this.UnitsComboBox = new System.Windows.Forms.ComboBox();
            this.ResYLabel = new System.Windows.Forms.Label();
            this.ResXLabel = new System.Windows.Forms.Label();
            this.MetaGroupBox = new System.Windows.Forms.GroupBox();
            this.InternetGroupBox = new System.Windows.Forms.GroupBox();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.InternetPasswordTextBox = new System.Windows.Forms.TextBox();
            this.DomainLabel = new System.Windows.Forms.Label();
            this.DomainTextBox = new System.Windows.Forms.TextBox();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.UserNameTextBox = new System.Windows.Forms.TextBox();
            this.PicLabel = new System.Windows.Forms.Label();
            this.PasswordTextBox = new System.Windows.Forms.TextBox();
            this.PicGroupBox = new System.Windows.Forms.GroupBox();
            this.JpegGroupBox = new System.Windows.Forms.GroupBox();
            this.EnhancedCheckBox = new System.Windows.Forms.CheckBox();
            this.CositedCheckBox = new System.Windows.Forms.CheckBox();
            this.PreserveCheckBox = new System.Windows.Forms.CheckBox();
            this.RenderLabel = new System.Windows.Forms.Label();
            this.TIFFGroupBox = new System.Windows.Forms.GroupBox();
            this.IFDLabel = new System.Windows.Forms.Label();
            this.SpecialCheckBox = new System.Windows.Forms.CheckBox();
            this.IFDOffsetTextBox = new System.Windows.Forms.TextBox();
            this.ThumbComboBox = new System.Windows.Forms.ComboBox();
            this.ThumbLabel = new System.Windows.Forms.Label();
            this.ResizeYLabel = new System.Windows.Forms.Label();
            this.ResizeXLabel = new System.Windows.Forms.Label();
            this.ResizeYBox = new System.Windows.Forms.NumericUpDown();
            this.ResizeXBox = new System.Windows.Forms.NumericUpDown();
            this.RotationLabel = new System.Windows.Forms.Label();
            this.RotationComboBox = new System.Windows.Forms.ComboBox();
            this.ScaleCheckBox = new System.Windows.Forms.CheckBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.UseEmbedCheckBox = new System.Windows.Forms.CheckBox();
            this.sourceButton = new System.Windows.Forms.Button();
            this.Jbig2GroupBox = new System.Windows.Forms.GroupBox();
            this.SwapCheckBox = new System.Windows.Forms.CheckBox();
            this.HdpGroupBox = new System.Windows.Forms.GroupBox();
            this.postComboBox = new System.Windows.Forms.ComboBox();
            this.PostLabel = new System.Windows.Forms.Label();
            this.LibraryLabel = new System.Windows.Forms.Label();
            this.LibraryBox = new System.Windows.Forms.NumericUpDown();
            this.ResizeCheckBox = new System.Windows.Forms.CheckBox();
            this.CadGroupBox = new System.Windows.Forms.GroupBox();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.WidthLabel = new System.Windows.Forms.Label();
            this.HeightBox = new System.Windows.Forms.NumericUpDown();
            this.WidthBox = new System.Windows.Forms.NumericUpDown();
            this.PaperYLabel = new System.Windows.Forms.Label();
            this.CadResLabel = new System.Windows.Forms.Label();
            this.PaperXLabel = new System.Windows.Forms.Label();
            this.PaperBitLabel = new System.Windows.Forms.Label();
            this.PaperYBox = new System.Windows.Forms.NumericUpDown();
            this.CadUnitsComboBox = new System.Windows.Forms.ComboBox();
            this.PaperXBox = new System.Windows.Forms.NumericUpDown();
            this.PaperBox = new System.Windows.Forms.NumericUpDown();
            this.LayoutLabel = new System.Windows.Forms.Label();
            this.LayoutBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.CropXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropWBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropHBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResXBox)).BeginInit();
            this.MetaGroupBox.SuspendLayout();
            this.InternetGroupBox.SuspendLayout();
            this.PicGroupBox.SuspendLayout();
            this.JpegGroupBox.SuspendLayout();
            this.TIFFGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResizeYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResizeXBox)).BeginInit();
            this.Jbig2GroupBox.SuspendLayout();
            this.HdpGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LibraryBox)).BeginInit();
            this.CadGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeightBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaperYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaperXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaperBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CameraRawCheckBox
            // 
            this.CameraRawCheckBox.AutoSize = true;
            this.CameraRawCheckBox.Location = new System.Drawing.Point(15, 13);
            this.CameraRawCheckBox.Name = "CameraRawCheckBox";
            this.CameraRawCheckBox.Size = new System.Drawing.Size(129, 17);
            this.CameraRawCheckBox.TabIndex = 0;
            this.CameraRawCheckBox.Text = "Camera Raw Enabled";
            this.CameraRawCheckBox.UseVisualStyleBackColor = true;
            // 
            // renderIntentComboBox
            // 
            this.renderIntentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.renderIntentComboBox.FormattingEnabled = true;
            this.renderIntentComboBox.Items.AddRange(new object[] {
            "Images",
            "Business",
            "Graphics",
            "AbsoluteColorMetric"});
            this.renderIntentComboBox.Location = new System.Drawing.Point(114, 68);
            this.renderIntentComboBox.Name = "renderIntentComboBox";
            this.renderIntentComboBox.Size = new System.Drawing.Size(121, 21);
            this.renderIntentComboBox.TabIndex = 8;
            // 
            // CropXBox
            // 
            this.CropXBox.Location = new System.Drawing.Point(337, 69);
            this.CropXBox.Name = "CropXBox";
            this.CropXBox.Size = new System.Drawing.Size(67, 20);
            this.CropXBox.TabIndex = 10;
            // 
            // CropYBox
            // 
            this.CropYBox.Location = new System.Drawing.Point(337, 95);
            this.CropYBox.Name = "CropYBox";
            this.CropYBox.Size = new System.Drawing.Size(67, 20);
            this.CropYBox.TabIndex = 14;
            // 
            // CropWBox
            // 
            this.CropWBox.Location = new System.Drawing.Point(337, 121);
            this.CropWBox.Name = "CropWBox";
            this.CropWBox.Size = new System.Drawing.Size(67, 20);
            this.CropWBox.TabIndex = 18;
            // 
            // CropHBox
            // 
            this.CropHBox.Location = new System.Drawing.Point(337, 147);
            this.CropHBox.Name = "CropHBox";
            this.CropHBox.Size = new System.Drawing.Size(67, 20);
            this.CropHBox.TabIndex = 20;
            // 
            // CropXLabel
            // 
            this.CropXLabel.AutoSize = true;
            this.CropXLabel.Location = new System.Drawing.Point(289, 71);
            this.CropXLabel.Name = "CropXLabel";
            this.CropXLabel.Size = new System.Drawing.Size(42, 13);
            this.CropXLabel.TabIndex = 9;
            this.CropXLabel.Text = "Crop X:";
            // 
            // CropYLabel
            // 
            this.CropYLabel.AutoSize = true;
            this.CropYLabel.Location = new System.Drawing.Point(289, 97);
            this.CropYLabel.Name = "CropYLabel";
            this.CropYLabel.Size = new System.Drawing.Size(42, 13);
            this.CropYLabel.TabIndex = 13;
            this.CropYLabel.Text = "Crop Y:";
            // 
            // CropWLabel
            // 
            this.CropWLabel.AutoSize = true;
            this.CropWLabel.Location = new System.Drawing.Point(289, 124);
            this.CropWLabel.Name = "CropWLabel";
            this.CropWLabel.Size = new System.Drawing.Size(46, 13);
            this.CropWLabel.TabIndex = 17;
            this.CropWLabel.Text = "Crop W:";
            // 
            // CropHLabel
            // 
            this.CropHLabel.AutoSize = true;
            this.CropHLabel.Location = new System.Drawing.Point(288, 150);
            this.CropHLabel.Name = "CropHLabel";
            this.CropHLabel.Size = new System.Drawing.Size(43, 13);
            this.CropHLabel.TabIndex = 19;
            this.CropHLabel.Text = "Crop H:";
            // 
            // OffsetTextBox
            // 
            this.OffsetTextBox.Location = new System.Drawing.Point(115, 241);
            this.OffsetTextBox.Name = "OffsetTextBox";
            this.OffsetTextBox.Size = new System.Drawing.Size(121, 20);
            this.OffsetTextBox.TabIndex = 30;
            this.OffsetTextBox.Text = "0";
            // 
            // OffsetLabel
            // 
            this.OffsetLabel.AutoSize = true;
            this.OffsetLabel.Location = new System.Drawing.Point(39, 244);
            this.OffsetLabel.Name = "OffsetLabel";
            this.OffsetLabel.Size = new System.Drawing.Size(70, 13);
            this.OffsetLabel.TabIndex = 29;
            this.OffsetLabel.Text = "Image Offset:";
            // 
            // FTPComboBox
            // 
            this.FTPComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FTPComboBox.FormattingEnabled = true;
            this.FTPComboBox.Items.AddRange(new object[] {
            "Active",
            "Passive"});
            this.FTPComboBox.Location = new System.Drawing.Point(86, 22);
            this.FTPComboBox.Name = "FTPComboBox";
            this.FTPComboBox.Size = new System.Drawing.Size(121, 21);
            this.FTPComboBox.TabIndex = 1;
            // 
            // FTPLabel
            // 
            this.FTPLabel.AutoSize = true;
            this.FTPLabel.Location = new System.Drawing.Point(16, 25);
            this.FTPLabel.Name = "FTPLabel";
            this.FTPLabel.Size = new System.Drawing.Size(60, 13);
            this.FTPLabel.TabIndex = 0;
            this.FTPLabel.Text = "FTP Mode:";
            // 
            // LoadAlphaCheckBox
            // 
            this.LoadAlphaCheckBox.AutoSize = true;
            this.LoadAlphaCheckBox.Location = new System.Drawing.Point(150, 13);
            this.LoadAlphaCheckBox.Name = "LoadAlphaCheckBox";
            this.LoadAlphaCheckBox.Size = new System.Drawing.Size(122, 17);
            this.LoadAlphaCheckBox.TabIndex = 1;
            this.LoadAlphaCheckBox.Text = "Load Alpha Channel";
            this.LoadAlphaCheckBox.UseVisualStyleBackColor = true;
            // 
            // LoadLabel
            // 
            this.LoadLabel.AutoSize = true;
            this.LoadLabel.Location = new System.Drawing.Point(45, 106);
            this.LoadLabel.Name = "LoadLabel";
            this.LoadLabel.Size = new System.Drawing.Size(64, 13);
            this.LoadLabel.TabIndex = 11;
            this.LoadLabel.Text = "Load Mode:";
            // 
            // LoadComboBox
            // 
            this.LoadComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LoadComboBox.FormattingEnabled = true;
            this.LoadComboBox.Items.AddRange(new object[] {
            "Normal",
            "Raw"});
            this.LoadComboBox.Location = new System.Drawing.Point(114, 103);
            this.LoadComboBox.Name = "LoadComboBox";
            this.LoadComboBox.Size = new System.Drawing.Size(121, 21);
            this.LoadComboBox.TabIndex = 12;
            // 
            // MaintainCheckBox
            // 
            this.MaintainCheckBox.AutoSize = true;
            this.MaintainCheckBox.Location = new System.Drawing.Point(490, 13);
            this.MaintainCheckBox.Name = "MaintainCheckBox";
            this.MaintainCheckBox.Size = new System.Drawing.Size(130, 17);
            this.MaintainCheckBox.TabIndex = 3;
            this.MaintainCheckBox.Text = "Maintain Aspect Ratio";
            this.MaintainCheckBox.UseVisualStyleBackColor = true;
            // 
            // ConvertCheckBox
            // 
            this.ConvertCheckBox.AutoSize = true;
            this.ConvertCheckBox.Location = new System.Drawing.Point(75, 17);
            this.ConvertCheckBox.Name = "ConvertCheckBox";
            this.ConvertCheckBox.Size = new System.Drawing.Size(98, 17);
            this.ConvertCheckBox.TabIndex = 0;
            this.ConvertCheckBox.Text = "Convert To Dib";
            this.ConvertCheckBox.UseVisualStyleBackColor = true;
            // 
            // ResYBox
            // 
            this.ResYBox.Location = new System.Drawing.Point(106, 67);
            this.ResYBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ResYBox.Name = "ResYBox";
            this.ResYBox.Size = new System.Drawing.Size(67, 20);
            this.ResYBox.TabIndex = 4;
            // 
            // ResXBox
            // 
            this.ResXBox.Location = new System.Drawing.Point(106, 41);
            this.ResXBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ResXBox.Name = "ResXBox";
            this.ResXBox.Size = new System.Drawing.Size(67, 20);
            this.ResXBox.TabIndex = 2;
            // 
            // UnitsLabel
            // 
            this.UnitsLabel.AutoSize = true;
            this.UnitsLabel.Location = new System.Drawing.Point(9, 98);
            this.UnitsLabel.Name = "UnitsLabel";
            this.UnitsLabel.Size = new System.Drawing.Size(34, 13);
            this.UnitsLabel.TabIndex = 5;
            this.UnitsLabel.Text = "Units:";
            // 
            // UnitsComboBox
            // 
            this.UnitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UnitsComboBox.FormattingEnabled = true;
            this.UnitsComboBox.Items.AddRange(new object[] {
            "World",
            "Display",
            "Pixel",
            "Point",
            "Inch",
            "Document",
            "Millimeter"});
            this.UnitsComboBox.Location = new System.Drawing.Point(52, 95);
            this.UnitsComboBox.Name = "UnitsComboBox";
            this.UnitsComboBox.Size = new System.Drawing.Size(121, 21);
            this.UnitsComboBox.TabIndex = 6;
            // 
            // ResYLabel
            // 
            this.ResYLabel.AutoSize = true;
            this.ResYLabel.Location = new System.Drawing.Point(25, 72);
            this.ResYLabel.Name = "ResYLabel";
            this.ResYLabel.Size = new System.Drawing.Size(70, 13);
            this.ResYLabel.TabIndex = 3;
            this.ResYLabel.Text = "Resolution Y:";
            // 
            // ResXLabel
            // 
            this.ResXLabel.AutoSize = true;
            this.ResXLabel.Location = new System.Drawing.Point(25, 46);
            this.ResXLabel.Name = "ResXLabel";
            this.ResXLabel.Size = new System.Drawing.Size(70, 13);
            this.ResXLabel.TabIndex = 1;
            this.ResXLabel.Text = "Resolution X:";
            // 
            // MetaGroupBox
            // 
            this.MetaGroupBox.Controls.Add(this.ResYLabel);
            this.MetaGroupBox.Controls.Add(this.ResXLabel);
            this.MetaGroupBox.Controls.Add(this.UnitsLabel);
            this.MetaGroupBox.Controls.Add(this.UnitsComboBox);
            this.MetaGroupBox.Controls.Add(this.ResYBox);
            this.MetaGroupBox.Controls.Add(this.ResXBox);
            this.MetaGroupBox.Controls.Add(this.ConvertCheckBox);
            this.MetaGroupBox.Location = new System.Drawing.Point(438, 269);
            this.MetaGroupBox.Name = "MetaGroupBox";
            this.MetaGroupBox.Size = new System.Drawing.Size(215, 126);
            this.MetaGroupBox.TabIndex = 35;
            this.MetaGroupBox.TabStop = false;
            this.MetaGroupBox.Text = "Metafile";
            // 
            // InternetGroupBox
            // 
            this.InternetGroupBox.Controls.Add(this.PasswordLabel);
            this.InternetGroupBox.Controls.Add(this.InternetPasswordTextBox);
            this.InternetGroupBox.Controls.Add(this.DomainLabel);
            this.InternetGroupBox.Controls.Add(this.DomainTextBox);
            this.InternetGroupBox.Controls.Add(this.UserNameLabel);
            this.InternetGroupBox.Controls.Add(this.FTPLabel);
            this.InternetGroupBox.Controls.Add(this.UserNameTextBox);
            this.InternetGroupBox.Controls.Add(this.FTPComboBox);
            this.InternetGroupBox.Location = new System.Drawing.Point(213, 267);
            this.InternetGroupBox.Name = "InternetGroupBox";
            this.InternetGroupBox.Size = new System.Drawing.Size(219, 137);
            this.InternetGroupBox.TabIndex = 34;
            this.InternetGroupBox.TabStop = false;
            this.InternetGroupBox.Text = "Internet";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Location = new System.Drawing.Point(16, 85);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(56, 13);
            this.PasswordLabel.TabIndex = 4;
            this.PasswordLabel.Text = "Password:";
            // 
            // InternetPasswordTextBox
            // 
            this.InternetPasswordTextBox.Location = new System.Drawing.Point(89, 82);
            this.InternetPasswordTextBox.Name = "InternetPasswordTextBox";
            this.InternetPasswordTextBox.Size = new System.Drawing.Size(118, 20);
            this.InternetPasswordTextBox.TabIndex = 5;
            // 
            // DomainLabel
            // 
            this.DomainLabel.AutoSize = true;
            this.DomainLabel.Location = new System.Drawing.Point(16, 108);
            this.DomainLabel.Name = "DomainLabel";
            this.DomainLabel.Size = new System.Drawing.Size(46, 13);
            this.DomainLabel.TabIndex = 6;
            this.DomainLabel.Text = "Domain:";
            // 
            // DomainTextBox
            // 
            this.DomainTextBox.Location = new System.Drawing.Point(89, 108);
            this.DomainTextBox.Name = "DomainTextBox";
            this.DomainTextBox.Size = new System.Drawing.Size(118, 20);
            this.DomainTextBox.TabIndex = 7;
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Location = new System.Drawing.Point(16, 59);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(63, 13);
            this.UserNameLabel.TabIndex = 2;
            this.UserNameLabel.Text = "User Name:";
            // 
            // UserNameTextBox
            // 
            this.UserNameTextBox.Location = new System.Drawing.Point(89, 56);
            this.UserNameTextBox.Name = "UserNameTextBox";
            this.UserNameTextBox.Size = new System.Drawing.Size(118, 20);
            this.UserNameTextBox.TabIndex = 3;
            // 
            // PicLabel
            // 
            this.PicLabel.AutoSize = true;
            this.PicLabel.Location = new System.Drawing.Point(15, 33);
            this.PicLabel.Name = "PicLabel";
            this.PicLabel.Size = new System.Drawing.Size(56, 13);
            this.PicLabel.TabIndex = 0;
            this.PicLabel.Text = "Password:";
            // 
            // PasswordTextBox
            // 
            this.PasswordTextBox.Location = new System.Drawing.Point(88, 30);
            this.PasswordTextBox.Name = "PasswordTextBox";
            this.PasswordTextBox.Size = new System.Drawing.Size(100, 20);
            this.PasswordTextBox.TabIndex = 1;
            // 
            // PicGroupBox
            // 
            this.PicGroupBox.Controls.Add(this.PicLabel);
            this.PicGroupBox.Controls.Add(this.PasswordTextBox);
            this.PicGroupBox.Location = new System.Drawing.Point(213, 413);
            this.PicGroupBox.Name = "PicGroupBox";
            this.PicGroupBox.Size = new System.Drawing.Size(248, 67);
            this.PicGroupBox.TabIndex = 39;
            this.PicGroupBox.TabStop = false;
            this.PicGroupBox.Text = "PIC";
            // 
            // JpegGroupBox
            // 
            this.JpegGroupBox.Controls.Add(this.EnhancedCheckBox);
            this.JpegGroupBox.Controls.Add(this.CositedCheckBox);
            this.JpegGroupBox.Location = new System.Drawing.Point(17, 413);
            this.JpegGroupBox.Name = "JpegGroupBox";
            this.JpegGroupBox.Size = new System.Drawing.Size(190, 67);
            this.JpegGroupBox.TabIndex = 38;
            this.JpegGroupBox.TabStop = false;
            this.JpegGroupBox.Text = "JPEG";
            // 
            // EnhancedCheckBox
            // 
            this.EnhancedCheckBox.AutoSize = true;
            this.EnhancedCheckBox.Location = new System.Drawing.Point(13, 42);
            this.EnhancedCheckBox.Name = "EnhancedCheckBox";
            this.EnhancedCheckBox.Size = new System.Drawing.Size(75, 17);
            this.EnhancedCheckBox.TabIndex = 1;
            this.EnhancedCheckBox.Text = "Enhanced";
            this.EnhancedCheckBox.UseVisualStyleBackColor = true;
            // 
            // CositedCheckBox
            // 
            this.CositedCheckBox.AutoSize = true;
            this.CositedCheckBox.Location = new System.Drawing.Point(13, 19);
            this.CositedCheckBox.Name = "CositedCheckBox";
            this.CositedCheckBox.Size = new System.Drawing.Size(61, 17);
            this.CositedCheckBox.TabIndex = 0;
            this.CositedCheckBox.Text = "Cosited";
            this.CositedCheckBox.UseVisualStyleBackColor = true;
            // 
            // PreserveCheckBox
            // 
            this.PreserveCheckBox.AutoSize = true;
            this.PreserveCheckBox.Location = new System.Drawing.Point(15, 36);
            this.PreserveCheckBox.Name = "PreserveCheckBox";
            this.PreserveCheckBox.Size = new System.Drawing.Size(98, 17);
            this.PreserveCheckBox.TabIndex = 4;
            this.PreserveCheckBox.Text = "Preserve Black";
            this.PreserveCheckBox.UseVisualStyleBackColor = true;
            // 
            // RenderLabel
            // 
            this.RenderLabel.AutoSize = true;
            this.RenderLabel.Location = new System.Drawing.Point(6, 71);
            this.RenderLabel.Name = "RenderLabel";
            this.RenderLabel.Size = new System.Drawing.Size(102, 13);
            this.RenderLabel.TabIndex = 7;
            this.RenderLabel.Text = "Color Render Intent:";
            // 
            // TIFFGroupBox
            // 
            this.TIFFGroupBox.Controls.Add(this.IFDLabel);
            this.TIFFGroupBox.Controls.Add(this.SpecialCheckBox);
            this.TIFFGroupBox.Controls.Add(this.IFDOffsetTextBox);
            this.TIFFGroupBox.Location = new System.Drawing.Point(466, 413);
            this.TIFFGroupBox.Name = "TIFFGroupBox";
            this.TIFFGroupBox.Size = new System.Drawing.Size(187, 67);
            this.TIFFGroupBox.TabIndex = 40;
            this.TIFFGroupBox.TabStop = false;
            this.TIFFGroupBox.Text = "TIFF";
            // 
            // IFDLabel
            // 
            this.IFDLabel.AutoSize = true;
            this.IFDLabel.Location = new System.Drawing.Point(9, 44);
            this.IFDLabel.Name = "IFDLabel";
            this.IFDLabel.Size = new System.Drawing.Size(58, 13);
            this.IFDLabel.TabIndex = 1;
            this.IFDLabel.Text = "IFD Offset:";
            // 
            // SpecialCheckBox
            // 
            this.SpecialCheckBox.AutoSize = true;
            this.SpecialCheckBox.Location = new System.Drawing.Point(13, 19);
            this.SpecialCheckBox.Name = "SpecialCheckBox";
            this.SpecialCheckBox.Size = new System.Drawing.Size(106, 17);
            this.SpecialCheckBox.TabIndex = 0;
            this.SpecialCheckBox.Text = "Special Handling";
            this.SpecialCheckBox.UseVisualStyleBackColor = true;
            // 
            // IFDOffsetTextBox
            // 
            this.IFDOffsetTextBox.Location = new System.Drawing.Point(73, 42);
            this.IFDOffsetTextBox.Name = "IFDOffsetTextBox";
            this.IFDOffsetTextBox.Size = new System.Drawing.Size(108, 20);
            this.IFDOffsetTextBox.TabIndex = 2;
            this.IFDOffsetTextBox.Text = "0";
            // 
            // ThumbComboBox
            // 
            this.ThumbComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ThumbComboBox.FormattingEnabled = true;
            this.ThumbComboBox.Items.AddRange(new object[] {
            "No thumbnail",
            "1/4",
            "1/16",
            "1/64"});
            this.ThumbComboBox.Location = new System.Drawing.Point(115, 174);
            this.ThumbComboBox.Name = "ThumbComboBox";
            this.ThumbComboBox.Size = new System.Drawing.Size(121, 21);
            this.ThumbComboBox.TabIndex = 22;
            // 
            // ThumbLabel
            // 
            this.ThumbLabel.Location = new System.Drawing.Point(20, 172);
            this.ThumbLabel.Name = "ThumbLabel";
            this.ThumbLabel.Size = new System.Drawing.Size(85, 30);
            this.ThumbLabel.TabIndex = 21;
            this.ThumbLabel.Text = "JPEG/PIC Thumbnail Size:";
            // 
            // ResizeYLabel
            // 
            this.ResizeYLabel.AutoSize = true;
            this.ResizeYLabel.Location = new System.Drawing.Point(279, 208);
            this.ResizeYLabel.Name = "ResizeYLabel";
            this.ResizeYLabel.Size = new System.Drawing.Size(52, 13);
            this.ResizeYLabel.TabIndex = 27;
            this.ResizeYLabel.Text = "Resize Y:";
            // 
            // ResizeXLabel
            // 
            this.ResizeXLabel.AutoSize = true;
            this.ResizeXLabel.Location = new System.Drawing.Point(279, 182);
            this.ResizeXLabel.Name = "ResizeXLabel";
            this.ResizeXLabel.Size = new System.Drawing.Size(52, 13);
            this.ResizeXLabel.TabIndex = 23;
            this.ResizeXLabel.Text = "Resize X:";
            // 
            // ResizeYBox
            // 
            this.ResizeYBox.Location = new System.Drawing.Point(337, 203);
            this.ResizeYBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ResizeYBox.Name = "ResizeYBox";
            this.ResizeYBox.Size = new System.Drawing.Size(67, 20);
            this.ResizeYBox.TabIndex = 28;
            // 
            // ResizeXBox
            // 
            this.ResizeXBox.Location = new System.Drawing.Point(337, 177);
            this.ResizeXBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ResizeXBox.Name = "ResizeXBox";
            this.ResizeXBox.Size = new System.Drawing.Size(67, 20);
            this.ResizeXBox.TabIndex = 24;
            // 
            // RotationLabel
            // 
            this.RotationLabel.AutoSize = true;
            this.RotationLabel.Location = new System.Drawing.Point(52, 140);
            this.RotationLabel.Name = "RotationLabel";
            this.RotationLabel.Size = new System.Drawing.Size(50, 13);
            this.RotationLabel.TabIndex = 15;
            this.RotationLabel.Text = "Rotation:";
            // 
            // RotationComboBox
            // 
            this.RotationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RotationComboBox.FormattingEnabled = true;
            this.RotationComboBox.Items.AddRange(new object[] {
            "0",
            "90",
            "180",
            "270",
            "Flip",
            "Mirror"});
            this.RotationComboBox.Location = new System.Drawing.Point(114, 137);
            this.RotationComboBox.Name = "RotationComboBox";
            this.RotationComboBox.Size = new System.Drawing.Size(121, 21);
            this.RotationComboBox.TabIndex = 16;
            // 
            // ScaleCheckBox
            // 
            this.ScaleCheckBox.AutoSize = true;
            this.ScaleCheckBox.Location = new System.Drawing.Point(150, 36);
            this.ScaleCheckBox.Name = "ScaleCheckBox";
            this.ScaleCheckBox.Size = new System.Drawing.Size(90, 17);
            this.ScaleCheckBox.TabIndex = 5;
            this.ScaleCheckBox.Text = "Scale to Gray";
            this.ScaleCheckBox.UseVisualStyleBackColor = true;
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(291, 499);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(107, 29);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // UseEmbedCheckBox
            // 
            this.UseEmbedCheckBox.AutoSize = true;
            this.UseEmbedCheckBox.Location = new System.Drawing.Point(292, 35);
            this.UseEmbedCheckBox.Name = "UseEmbedCheckBox";
            this.UseEmbedCheckBox.Size = new System.Drawing.Size(191, 17);
            this.UseEmbedCheckBox.TabIndex = 6;
            this.UseEmbedCheckBox.Text = "Use Embedded Color Management";
            this.UseEmbedCheckBox.UseVisualStyleBackColor = true;
            // 
            // sourceButton
            // 
            this.sourceButton.Location = new System.Drawing.Point(282, 239);
            this.sourceButton.Name = "sourceButton";
            this.sourceButton.Size = new System.Drawing.Size(125, 23);
            this.sourceButton.TabIndex = 31;
            this.sourceButton.Text = "Source Profile Name";
            this.sourceButton.UseVisualStyleBackColor = true;
            this.sourceButton.Click += new System.EventHandler(this.sourceButton_Click);
            // 
            // Jbig2GroupBox
            // 
            this.Jbig2GroupBox.Controls.Add(this.SwapCheckBox);
            this.Jbig2GroupBox.Location = new System.Drawing.Point(17, 340);
            this.Jbig2GroupBox.Name = "Jbig2GroupBox";
            this.Jbig2GroupBox.Size = new System.Drawing.Size(190, 67);
            this.Jbig2GroupBox.TabIndex = 36;
            this.Jbig2GroupBox.TabStop = false;
            this.Jbig2GroupBox.Text = "JBIG2";
            // 
            // SwapCheckBox
            // 
            this.SwapCheckBox.AutoSize = true;
            this.SwapCheckBox.Location = new System.Drawing.Point(28, 31);
            this.SwapCheckBox.Name = "SwapCheckBox";
            this.SwapCheckBox.Size = new System.Drawing.Size(135, 17);
            this.SwapCheckBox.TabIndex = 0;
            this.SwapCheckBox.Text = "Swap Black and White";
            this.SwapCheckBox.UseVisualStyleBackColor = true;
            // 
            // HdpGroupBox
            // 
            this.HdpGroupBox.Controls.Add(this.postComboBox);
            this.HdpGroupBox.Controls.Add(this.PostLabel);
            this.HdpGroupBox.Location = new System.Drawing.Point(15, 267);
            this.HdpGroupBox.Name = "HdpGroupBox";
            this.HdpGroupBox.Size = new System.Drawing.Size(192, 67);
            this.HdpGroupBox.TabIndex = 33;
            this.HdpGroupBox.TabStop = false;
            this.HdpGroupBox.Text = "HD Photo";
            // 
            // postComboBox
            // 
            this.postComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.postComboBox.FormattingEnabled = true;
            this.postComboBox.Items.AddRange(new object[] {
            "None",
            "Light",
            "Medium",
            "Strong",
            "Very Strong"});
            this.postComboBox.Location = new System.Drawing.Point(71, 25);
            this.postComboBox.Name = "postComboBox";
            this.postComboBox.Size = new System.Drawing.Size(106, 21);
            this.postComboBox.TabIndex = 1;
            // 
            // PostLabel
            // 
            this.PostLabel.Location = new System.Drawing.Point(6, 20);
            this.PostLabel.Name = "PostLabel";
            this.PostLabel.Size = new System.Drawing.Size(70, 41);
            this.PostLabel.TabIndex = 0;
            this.PostLabel.Text = "Post Processing Filter:";
            // 
            // LibraryLabel
            // 
            this.LibraryLabel.AutoSize = true;
            this.LibraryLabel.Location = new System.Drawing.Point(6, 207);
            this.LibraryLabel.Name = "LibraryLabel";
            this.LibraryLabel.Size = new System.Drawing.Size(175, 13);
            this.LibraryLabel.TabIndex = 25;
            this.LibraryLabel.Text = "Library Number of Threads Allowed:";
            // 
            // LibraryBox
            // 
            this.LibraryBox.Location = new System.Drawing.Point(187, 203);
            this.LibraryBox.Name = "LibraryBox";
            this.LibraryBox.Size = new System.Drawing.Size(67, 20);
            this.LibraryBox.TabIndex = 26;
            // 
            // ResizeCheckBox
            // 
            this.ResizeCheckBox.AutoSize = true;
            this.ResizeCheckBox.Location = new System.Drawing.Point(292, 12);
            this.ResizeCheckBox.Name = "ResizeCheckBox";
            this.ResizeCheckBox.Size = new System.Drawing.Size(127, 17);
            this.ResizeCheckBox.TabIndex = 2;
            this.ResizeCheckBox.Text = "Load Resize Antialias";
            this.ResizeCheckBox.UseVisualStyleBackColor = true;
            // 
            // CadGroupBox
            // 
            this.CadGroupBox.Controls.Add(this.HeightLabel);
            this.CadGroupBox.Controls.Add(this.WidthLabel);
            this.CadGroupBox.Controls.Add(this.HeightBox);
            this.CadGroupBox.Controls.Add(this.WidthBox);
            this.CadGroupBox.Controls.Add(this.PaperYLabel);
            this.CadGroupBox.Controls.Add(this.CadResLabel);
            this.CadGroupBox.Controls.Add(this.PaperXLabel);
            this.CadGroupBox.Controls.Add(this.PaperBitLabel);
            this.CadGroupBox.Controls.Add(this.PaperYBox);
            this.CadGroupBox.Controls.Add(this.CadUnitsComboBox);
            this.CadGroupBox.Controls.Add(this.PaperXBox);
            this.CadGroupBox.Controls.Add(this.PaperBox);
            this.CadGroupBox.Controls.Add(this.LayoutLabel);
            this.CadGroupBox.Controls.Add(this.LayoutBox);
            this.CadGroupBox.Location = new System.Drawing.Point(425, 58);
            this.CadGroupBox.Name = "CadGroupBox";
            this.CadGroupBox.Size = new System.Drawing.Size(258, 206);
            this.CadGroupBox.TabIndex = 32;
            this.CadGroupBox.TabStop = false;
            this.CadGroupBox.Text = "CAD";
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Location = new System.Drawing.Point(52, 180);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(72, 13);
            this.HeightLabel.TabIndex = 12;
            this.HeightLabel.Text = "Paper Height:";
            // 
            // WidthLabel
            // 
            this.WidthLabel.AutoSize = true;
            this.WidthLabel.Location = new System.Drawing.Point(52, 157);
            this.WidthLabel.Name = "WidthLabel";
            this.WidthLabel.Size = new System.Drawing.Size(69, 13);
            this.WidthLabel.TabIndex = 10;
            this.WidthLabel.Text = "Paper Width:";
            // 
            // HeightBox
            // 
            this.HeightBox.Location = new System.Drawing.Point(130, 176);
            this.HeightBox.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.HeightBox.Name = "HeightBox";
            this.HeightBox.Size = new System.Drawing.Size(67, 20);
            this.HeightBox.TabIndex = 13;
            this.HeightBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // WidthBox
            // 
            this.WidthBox.Location = new System.Drawing.Point(130, 150);
            this.WidthBox.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.WidthBox.Name = "WidthBox";
            this.WidthBox.Size = new System.Drawing.Size(67, 20);
            this.WidthBox.TabIndex = 11;
            this.WidthBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // PaperYLabel
            // 
            this.PaperYLabel.AutoSize = true;
            this.PaperYLabel.Location = new System.Drawing.Point(20, 99);
            this.PaperYLabel.Name = "PaperYLabel";
            this.PaperYLabel.Size = new System.Drawing.Size(101, 13);
            this.PaperYLabel.TabIndex = 6;
            this.PaperYLabel.Text = "Paper Resolution Y:";
            // 
            // CadResLabel
            // 
            this.CadResLabel.AutoSize = true;
            this.CadResLabel.Location = new System.Drawing.Point(6, 127);
            this.CadResLabel.Name = "CadResLabel";
            this.CadResLabel.Size = new System.Drawing.Size(118, 13);
            this.CadResLabel.TabIndex = 8;
            this.CadResLabel.Text = "Paper Resolution Units:";
            // 
            // PaperXLabel
            // 
            this.PaperXLabel.AutoSize = true;
            this.PaperXLabel.Location = new System.Drawing.Point(20, 73);
            this.PaperXLabel.Name = "PaperXLabel";
            this.PaperXLabel.Size = new System.Drawing.Size(101, 13);
            this.PaperXLabel.TabIndex = 4;
            this.PaperXLabel.Text = "Paper Resolution X:";
            // 
            // PaperBitLabel
            // 
            this.PaperBitLabel.AutoSize = true;
            this.PaperBitLabel.Location = new System.Drawing.Point(36, 47);
            this.PaperBitLabel.Name = "PaperBitLabel";
            this.PaperBitLabel.Size = new System.Drawing.Size(85, 13);
            this.PaperBitLabel.TabIndex = 2;
            this.PaperBitLabel.Text = "Paper Bit Depth:";
            // 
            // PaperYBox
            // 
            this.PaperYBox.Location = new System.Drawing.Point(130, 97);
            this.PaperYBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.PaperYBox.Name = "PaperYBox";
            this.PaperYBox.Size = new System.Drawing.Size(67, 20);
            this.PaperYBox.TabIndex = 7;
            this.PaperYBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // CadUnitsComboBox
            // 
            this.CadUnitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CadUnitsComboBox.FormattingEnabled = true;
            this.CadUnitsComboBox.Items.AddRange(new object[] {
            "Not available",
            "None",
            "Inch",
            "Centimeter"});
            this.CadUnitsComboBox.Location = new System.Drawing.Point(130, 123);
            this.CadUnitsComboBox.Name = "CadUnitsComboBox";
            this.CadUnitsComboBox.Size = new System.Drawing.Size(99, 21);
            this.CadUnitsComboBox.TabIndex = 9;
            // 
            // PaperXBox
            // 
            this.PaperXBox.Location = new System.Drawing.Point(130, 71);
            this.PaperXBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.PaperXBox.Name = "PaperXBox";
            this.PaperXBox.Size = new System.Drawing.Size(67, 20);
            this.PaperXBox.TabIndex = 5;
            this.PaperXBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // PaperBox
            // 
            this.PaperBox.Location = new System.Drawing.Point(130, 45);
            this.PaperBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PaperBox.Name = "PaperBox";
            this.PaperBox.Size = new System.Drawing.Size(67, 20);
            this.PaperBox.TabIndex = 3;
            this.PaperBox.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // LayoutLabel
            // 
            this.LayoutLabel.AutoSize = true;
            this.LayoutLabel.Location = new System.Drawing.Point(25, 21);
            this.LayoutLabel.Name = "LayoutLabel";
            this.LayoutLabel.Size = new System.Drawing.Size(96, 13);
            this.LayoutLabel.TabIndex = 0;
            this.LayoutLabel.Text = "Layout To Render:";
            // 
            // LayoutBox
            // 
            this.LayoutBox.Location = new System.Drawing.Point(130, 19);
            this.LayoutBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.LayoutBox.Name = "LayoutBox";
            this.LayoutBox.Size = new System.Drawing.Size(67, 20);
            this.LayoutBox.TabIndex = 1;
            // 
            // LoadOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 563);
            this.ControlBox = false;
            this.Controls.Add(this.CadGroupBox);
            this.Controls.Add(this.ResizeCheckBox);
            this.Controls.Add(this.LibraryLabel);
            this.Controls.Add(this.LibraryBox);
            this.Controls.Add(this.HdpGroupBox);
            this.Controls.Add(this.Jbig2GroupBox);
            this.Controls.Add(this.sourceButton);
            this.Controls.Add(this.UseEmbedCheckBox);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.ScaleCheckBox);
            this.Controls.Add(this.RotationLabel);
            this.Controls.Add(this.RotationComboBox);
            this.Controls.Add(this.ResizeYLabel);
            this.Controls.Add(this.ResizeXLabel);
            this.Controls.Add(this.ResizeYBox);
            this.Controls.Add(this.ResizeXBox);
            this.Controls.Add(this.ThumbLabel);
            this.Controls.Add(this.TIFFGroupBox);
            this.Controls.Add(this.RenderLabel);
            this.Controls.Add(this.ThumbComboBox);
            this.Controls.Add(this.PreserveCheckBox);
            this.Controls.Add(this.JpegGroupBox);
            this.Controls.Add(this.PicGroupBox);
            this.Controls.Add(this.InternetGroupBox);
            this.Controls.Add(this.MetaGroupBox);
            this.Controls.Add(this.MaintainCheckBox);
            this.Controls.Add(this.LoadLabel);
            this.Controls.Add(this.LoadComboBox);
            this.Controls.Add(this.LoadAlphaCheckBox);
            this.Controls.Add(this.OffsetLabel);
            this.Controls.Add(this.OffsetTextBox);
            this.Controls.Add(this.CropHLabel);
            this.Controls.Add(this.CropWLabel);
            this.Controls.Add(this.CropYLabel);
            this.Controls.Add(this.CropXLabel);
            this.Controls.Add(this.CropHBox);
            this.Controls.Add(this.CropWBox);
            this.Controls.Add(this.CropYBox);
            this.Controls.Add(this.CropXBox);
            this.Controls.Add(this.renderIntentComboBox);
            this.Controls.Add(this.CameraRawCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LoadOptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Load Options";
            this.Load += new System.EventHandler(this.LoadOptionsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.CropXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropWBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CropHBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResXBox)).EndInit();
            this.MetaGroupBox.ResumeLayout(false);
            this.MetaGroupBox.PerformLayout();
            this.InternetGroupBox.ResumeLayout(false);
            this.InternetGroupBox.PerformLayout();
            this.PicGroupBox.ResumeLayout(false);
            this.PicGroupBox.PerformLayout();
            this.JpegGroupBox.ResumeLayout(false);
            this.JpegGroupBox.PerformLayout();
            this.TIFFGroupBox.ResumeLayout(false);
            this.TIFFGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResizeYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResizeXBox)).EndInit();
            this.Jbig2GroupBox.ResumeLayout(false);
            this.Jbig2GroupBox.PerformLayout();
            this.HdpGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LibraryBox)).EndInit();
            this.CadGroupBox.ResumeLayout(false);
            this.CadGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HeightBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaperYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaperXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaperBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox CameraRawCheckBox;
        private System.Windows.Forms.ComboBox renderIntentComboBox;
        private System.Windows.Forms.NumericUpDown CropXBox;
        private System.Windows.Forms.NumericUpDown CropYBox;
        private System.Windows.Forms.NumericUpDown CropWBox;
        private System.Windows.Forms.NumericUpDown CropHBox;
        private System.Windows.Forms.Label CropXLabel;
        private System.Windows.Forms.Label CropYLabel;
        private System.Windows.Forms.Label CropWLabel;
        private System.Windows.Forms.Label CropHLabel;
        private System.Windows.Forms.TextBox OffsetTextBox;
        private System.Windows.Forms.Label OffsetLabel;
        private System.Windows.Forms.ComboBox FTPComboBox;
        private System.Windows.Forms.Label FTPLabel;
        private System.Windows.Forms.CheckBox LoadAlphaCheckBox;
        private System.Windows.Forms.Label LoadLabel;
        private System.Windows.Forms.ComboBox LoadComboBox;
        private System.Windows.Forms.CheckBox MaintainCheckBox;
        private System.Windows.Forms.CheckBox ConvertCheckBox;
        private System.Windows.Forms.NumericUpDown ResYBox;
        private System.Windows.Forms.NumericUpDown ResXBox;
        private System.Windows.Forms.Label UnitsLabel;
        private System.Windows.Forms.ComboBox UnitsComboBox;
        private System.Windows.Forms.Label ResYLabel;
        private System.Windows.Forms.Label ResXLabel;
        private System.Windows.Forms.GroupBox MetaGroupBox;
        private System.Windows.Forms.GroupBox InternetGroupBox;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.TextBox InternetPasswordTextBox;
        private System.Windows.Forms.Label DomainLabel;
        private System.Windows.Forms.TextBox DomainTextBox;
        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.TextBox UserNameTextBox;
        private System.Windows.Forms.Label PicLabel;
        private System.Windows.Forms.TextBox PasswordTextBox;
        private System.Windows.Forms.GroupBox PicGroupBox;
        private System.Windows.Forms.GroupBox JpegGroupBox;
        private System.Windows.Forms.CheckBox EnhancedCheckBox;
        private System.Windows.Forms.CheckBox CositedCheckBox;
        private System.Windows.Forms.CheckBox PreserveCheckBox;
        private System.Windows.Forms.Label RenderLabel;
        private System.Windows.Forms.GroupBox TIFFGroupBox;
        private System.Windows.Forms.CheckBox SpecialCheckBox;
        private System.Windows.Forms.Label IFDLabel;
        private System.Windows.Forms.TextBox IFDOffsetTextBox;
        private System.Windows.Forms.ComboBox ThumbComboBox;
        private System.Windows.Forms.Label ThumbLabel;
        private System.Windows.Forms.Label ResizeYLabel;
        private System.Windows.Forms.Label ResizeXLabel;
        private System.Windows.Forms.NumericUpDown ResizeYBox;
        private System.Windows.Forms.NumericUpDown ResizeXBox;
        private System.Windows.Forms.Label RotationLabel;
        private System.Windows.Forms.ComboBox RotationComboBox;
        private System.Windows.Forms.CheckBox ScaleCheckBox;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.CheckBox UseEmbedCheckBox;
        private System.Windows.Forms.Button sourceButton;
        private System.Windows.Forms.GroupBox Jbig2GroupBox;
        private System.Windows.Forms.CheckBox SwapCheckBox;
        private System.Windows.Forms.GroupBox HdpGroupBox;
        private System.Windows.Forms.Label PostLabel;
        private System.Windows.Forms.Label LibraryLabel;
        private System.Windows.Forms.NumericUpDown LibraryBox;
        private System.Windows.Forms.CheckBox ResizeCheckBox;
        private System.Windows.Forms.GroupBox CadGroupBox;
        private System.Windows.Forms.Label LayoutLabel;
        private System.Windows.Forms.NumericUpDown LayoutBox;
        private System.Windows.Forms.Label PaperYLabel;
        private System.Windows.Forms.Label CadResLabel;
        private System.Windows.Forms.Label PaperXLabel;
        private System.Windows.Forms.Label PaperBitLabel;
        private System.Windows.Forms.NumericUpDown PaperYBox;
        private System.Windows.Forms.ComboBox CadUnitsComboBox;
        private System.Windows.Forms.NumericUpDown PaperXBox;
        private System.Windows.Forms.NumericUpDown PaperBox;
        private System.Windows.Forms.Label HeightLabel;
        private System.Windows.Forms.Label WidthLabel;
        private System.Windows.Forms.NumericUpDown HeightBox;
        private System.Windows.Forms.NumericUpDown WidthBox;
        private System.Windows.Forms.ComboBox postComboBox;
    }
}
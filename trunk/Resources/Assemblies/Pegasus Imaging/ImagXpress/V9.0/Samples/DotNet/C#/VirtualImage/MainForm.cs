/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;
using System.Runtime.InteropServices;

namespace Virtual_Image
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
       
        private int height, width, srcX, srcY, srcWidth, srcHeight;
        private LoadOptions lo;
        private ImageXData data;

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        public static extern IntPtr DeleteObject(IntPtr hDc);

        private void MainForm_Load(object sender, EventArgs e)
        {
           

                //***Must call the UnlockRuntime method to unlock the control
                //***The unlock codes below are for illustration purposes only, they won't work!
                //imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);


        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            try
            {
                data = ImageX.QueryFile(imagXpress1, Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\autolevel.jpg");
                width = data.Width;
                height = data.Height;

                imageXView1.Image = new ImageX(imagXpress1, width, height, data.BitsPerPixel, System.Drawing.Color.White, new Resolution(data.Resolution.Dimensions.Width, data.Resolution.Dimensions.Height, data.Resolution.Units), true);
            }
            catch (ImagXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void imageXView1_PaintImage(object sender, PaintImageEventArgs e)
        {
            try
            {
                lo = new LoadOptions();

                srcX = (int)(e.LogicalRectangle.Left / e.ZoomFactor);
                srcY = (int)(e.LogicalRectangle.Top / e.ZoomFactor);
                srcWidth = (int)(e.ViewRectangle.Width / e.ZoomFactor);
                srcHeight = (int)(e.ViewRectangle.Height / e.ZoomFactor);

                if (srcWidth > width)
                {
                    srcWidth = width;
                }

                if (srcHeight > height)
                {
                    srcHeight = height;
                }

                if (srcX + srcWidth > width)
                {
                    srcX = srcX - ((srcX + srcWidth) - width);
                }

                if (srcY + srcHeight > height)
                {
                    srcY = srcY - ((srcY + srcHeight) - height);
                }

                lo.CropRectangle = new Rectangle(srcX, srcY, srcWidth, srcHeight);

                imageXView2.ZoomFactor = e.ZoomFactor;
                imageXView2.Image = ImageX.FromFile(imagXpress1, Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\autolevel.jpg", lo);

                IntPtr hBitmap = imageXView2.Image.ToHbitmap(true);

                e.Graphics.DrawImage(Image.FromHbitmap(hBitmap), 0, 0, e.ViewRectangle.Width, e.ViewRectangle.Height);

                DeleteObject(hBitmap);
                
                imageXView2.Image.Dispose();
            }
            catch (ImagXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
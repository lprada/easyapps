/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class BinarizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BinarizeForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.AngleBox = new System.Windows.Forms.NumericUpDown();
            this.HighBox = new System.Windows.Forms.NumericUpDown();
            this.PitchBox = new System.Windows.Forms.NumericUpDown();
            this.ConBox = new System.Windows.Forms.NumericUpDown();
            this.EccBox = new System.Windows.Forms.NumericUpDown();
            this.LowBox = new System.Windows.Forms.NumericUpDown();
            this.labelLocalContrastEnhancement = new System.Windows.Forms.Label();
            this.labelGridPitch = new System.Windows.Forms.Label();
            this.labelHighThreshold = new System.Windows.Forms.Label();
            this.labelLowThreshold = new System.Windows.Forms.Label();
            this.comboBoxBlur = new System.Windows.Forms.ComboBox();
            this.labelBlur = new System.Windows.Forms.Label();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.BinzarizeLabel = new System.Windows.Forms.Label();
            this.AngleLabel = new System.Windows.Forms.Label();
            this.EccLabel = new System.Windows.Forms.Label();
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AngleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PitchBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EccBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LowBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(410, 543);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(644, 543);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(526, 543);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // AngleBox
            // 
            this.AngleBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AngleBox.Enabled = false;
            this.AngleBox.Location = new System.Drawing.Point(203, 490);
            this.AngleBox.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.AngleBox.Name = "AngleBox";
            this.AngleBox.Size = new System.Drawing.Size(61, 20);
            this.AngleBox.TabIndex = 33;
            // 
            // HighBox
            // 
            this.HighBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HighBox.Location = new System.Drawing.Point(364, 461);
            this.HighBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.HighBox.Name = "HighBox";
            this.HighBox.Size = new System.Drawing.Size(61, 20);
            this.HighBox.TabIndex = 32;
            this.HighBox.Value = new decimal(new int[] {
            190,
            0,
            0,
            0});
            this.HighBox.ValueChanged += new System.EventHandler(this.HighBox_ValueChanged);
            // 
            // PitchBox
            // 
            this.PitchBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PitchBox.Enabled = false;
            this.PitchBox.Location = new System.Drawing.Point(364, 489);
            this.PitchBox.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.PitchBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PitchBox.Name = "PitchBox";
            this.PitchBox.Size = new System.Drawing.Size(61, 20);
            this.PitchBox.TabIndex = 31;
            this.PitchBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ConBox
            // 
            this.ConBox.Location = new System.Drawing.Point(273, 546);
            this.ConBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ConBox.Name = "ConBox";
            this.ConBox.Size = new System.Drawing.Size(61, 20);
            this.ConBox.TabIndex = 30;
            // 
            // EccBox
            // 
            this.EccBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EccBox.Enabled = false;
            this.EccBox.Location = new System.Drawing.Point(203, 516);
            this.EccBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.EccBox.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.EccBox.Name = "EccBox";
            this.EccBox.Size = new System.Drawing.Size(61, 20);
            this.EccBox.TabIndex = 29;
            // 
            // LowBox
            // 
            this.LowBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LowBox.Location = new System.Drawing.Point(203, 462);
            this.LowBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.LowBox.Name = "LowBox";
            this.LowBox.Size = new System.Drawing.Size(61, 20);
            this.LowBox.TabIndex = 28;
            this.LowBox.Value = new decimal(new int[] {
            170,
            0,
            0,
            0});
            this.LowBox.ValueChanged += new System.EventHandler(this.LowBox_ValueChanged);
            // 
            // labelLocalContrastEnhancement
            // 
            this.labelLocalContrastEnhancement.AutoSize = true;
            this.labelLocalContrastEnhancement.Location = new System.Drawing.Point(120, 548);
            this.labelLocalContrastEnhancement.Name = "labelLocalContrastEnhancement";
            this.labelLocalContrastEnhancement.Size = new System.Drawing.Size(147, 13);
            this.labelLocalContrastEnhancement.TabIndex = 27;
            this.labelLocalContrastEnhancement.Text = "Local Contrast Enhancement:";
            // 
            // labelGridPitch
            // 
            this.labelGridPitch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelGridPitch.AutoSize = true;
            this.labelGridPitch.Location = new System.Drawing.Point(276, 489);
            this.labelGridPitch.Name = "labelGridPitch";
            this.labelGridPitch.Size = new System.Drawing.Size(56, 13);
            this.labelGridPitch.TabIndex = 26;
            this.labelGridPitch.Text = "Grid Pitch:";
            // 
            // labelHighThreshold
            // 
            this.labelHighThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelHighThreshold.AutoSize = true;
            this.labelHighThreshold.Location = new System.Drawing.Point(276, 463);
            this.labelHighThreshold.Name = "labelHighThreshold";
            this.labelHighThreshold.Size = new System.Drawing.Size(82, 13);
            this.labelHighThreshold.TabIndex = 25;
            this.labelHighThreshold.Text = "High Threshold:";
            // 
            // labelLowThreshold
            // 
            this.labelLowThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelLowThreshold.AutoSize = true;
            this.labelLowThreshold.Location = new System.Drawing.Point(117, 464);
            this.labelLowThreshold.Name = "labelLowThreshold";
            this.labelLowThreshold.Size = new System.Drawing.Size(80, 13);
            this.labelLowThreshold.TabIndex = 24;
            this.labelLowThreshold.Text = "Low Threshold:";
            // 
            // comboBoxBlur
            // 
            this.comboBoxBlur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxBlur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBlur.FormattingEnabled = true;
            this.comboBoxBlur.Items.AddRange(new object[] {
            "No Blur",
            "Gaussian",
            "Smart"});
            this.comboBoxBlur.Location = new System.Drawing.Point(306, 434);
            this.comboBoxBlur.Name = "comboBoxBlur";
            this.comboBoxBlur.Size = new System.Drawing.Size(119, 21);
            this.comboBoxBlur.TabIndex = 23;
            // 
            // labelBlur
            // 
            this.labelBlur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBlur.AutoSize = true;
            this.labelBlur.Location = new System.Drawing.Point(272, 437);
            this.labelBlur.Name = "labelBlur";
            this.labelBlur.Size = new System.Drawing.Size(28, 13);
            this.labelBlur.TabIndex = 22;
            this.labelBlur.Text = "Blur:";
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Items.AddRange(new object[] {
            "Quick Text",
            "Photo Halftone",
            "Gray"});
            this.comboBoxMode.Location = new System.Drawing.Point(120, 434);
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.Size = new System.Drawing.Size(119, 21);
            this.comboBoxMode.TabIndex = 21;
            this.comboBoxMode.SelectedIndexChanged += new System.EventHandler(this.comboBoxMode_SelectedIndexChanged);
            // 
            // BinzarizeLabel
            // 
            this.BinzarizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BinzarizeLabel.AutoSize = true;
            this.BinzarizeLabel.Location = new System.Drawing.Point(31, 437);
            this.BinzarizeLabel.Name = "BinzarizeLabel";
            this.BinzarizeLabel.Size = new System.Drawing.Size(77, 13);
            this.BinzarizeLabel.TabIndex = 34;
            this.BinzarizeLabel.Text = "Binarize Mode:";
            // 
            // AngleLabel
            // 
            this.AngleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AngleLabel.AutoSize = true;
            this.AngleLabel.Location = new System.Drawing.Point(117, 490);
            this.AngleLabel.Name = "AngleLabel";
            this.AngleLabel.Size = new System.Drawing.Size(59, 13);
            this.AngleLabel.TabIndex = 35;
            this.AngleLabel.Text = "Grid Angle:";
            // 
            // EccLabel
            // 
            this.EccLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EccLabel.AutoSize = true;
            this.EccLabel.Location = new System.Drawing.Point(117, 518);
            this.EccLabel.Name = "EccLabel";
            this.EccLabel.Size = new System.Drawing.Size(65, 13);
            this.EccLabel.TabIndex = 36;
            this.EccLabel.Text = "Eccentricity:";
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(7, 539);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 37;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 41;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 381);
            this.tableLayoutPanel.TabIndex = 40;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 375);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 375);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // BinarizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 576);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.EccLabel);
            this.Controls.Add(this.AngleLabel);
            this.Controls.Add(this.BinzarizeLabel);
            this.Controls.Add(this.AngleBox);
            this.Controls.Add(this.HighBox);
            this.Controls.Add(this.PitchBox);
            this.Controls.Add(this.ConBox);
            this.Controls.Add(this.EccBox);
            this.Controls.Add(this.LowBox);
            this.Controls.Add(this.labelLocalContrastEnhancement);
            this.Controls.Add(this.labelGridPitch);
            this.Controls.Add(this.labelHighThreshold);
            this.Controls.Add(this.labelLowThreshold);
            this.Controls.Add(this.comboBoxBlur);
            this.Controls.Add(this.labelBlur);
            this.Controls.Add(this.comboBoxMode);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BinarizeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Binarize";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AngleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PitchBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EccBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LowBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.NumericUpDown AngleBox;
        private System.Windows.Forms.NumericUpDown HighBox;
        private System.Windows.Forms.NumericUpDown PitchBox;
        private System.Windows.Forms.NumericUpDown ConBox;
        private System.Windows.Forms.NumericUpDown EccBox;
        private System.Windows.Forms.NumericUpDown LowBox;
        private System.Windows.Forms.Label labelLocalContrastEnhancement;
        private System.Windows.Forms.Label labelGridPitch;
        private System.Windows.Forms.Label labelHighThreshold;
        private System.Windows.Forms.Label labelLowThreshold;
        private System.Windows.Forms.ComboBox comboBoxBlur;
        private System.Windows.Forms.Label labelBlur;
        private System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.Label BinzarizeLabel;
        private System.Windows.Forms.Label AngleLabel;
        private System.Windows.Forms.Label EccLabel;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
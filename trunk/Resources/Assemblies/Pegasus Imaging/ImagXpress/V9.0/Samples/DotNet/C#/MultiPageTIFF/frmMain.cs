/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices; // DllImport

[StructLayout(LayoutKind.Sequential)]
public struct RECT 
{
	public int left;
	public int top;
	public int right;
	public int bottom;
}

public class Win32 
{
	[DllImport("User32.Dll")]
	public static extern void SetWindowText(int hWnd, String s);
	[DllImport("User32.Dll")]
	public static extern bool MoveWindow(int hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
	[DllImport("User32.Dll")]
	public static extern bool GetWindowRect(int hWnd, ref RECT rc);
}



namespace MultiPageTiff
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		//private PegasusImaging.WinForms.ImagXpress9.ImageX ixTiffFile;
		private System.Windows.Forms.Button cmdMake;
		private System.Windows.Forms.Button cmdRemove;
		private System.Windows.Forms.Button cmdInsert;
		private System.Windows.Forms.Button cmdCompact;
		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.Label lblMore;
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem mnuFileFile;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.String strImageFile1;
		private System.String strImageFile2;
		private System.String strImageFile3;
		private System.String strMPFile;
		private System.String strTmpFile;
		private System.String strCurrentDir ;
		private System.Windows.Forms.RichTextBox rtfInfo;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Label lblError;
		private PegasusImaging.WinForms.ImagXpress9.ImageX imgTmp;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView2;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
				//***call the Dispose method on the imagXpress1 object and the imageXView1 object
			//***call the Dispose method on the imagXpress1 object and the
			//*** imageXView1 object
			if (!(imagXpress1 == null)) 
			{
				imagXpress1.Dispose();
				imagXpress1 = null;
			}
			if (!(imageXView1 == null)) 
			{
				imageXView1.Dispose();
				imageXView1 = null;
			}
			if (!(imageXView2 == null)) 
			{
				imageXView2.Dispose();
				imageXView2 = null;
			}
			if (!(imageXView3 == null)) 
			{
				imageXView3.Dispose();
				imageXView3 = null;
			}
			
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdMake = new System.Windows.Forms.Button();
			this.cmdRemove = new System.Windows.Forms.Button();
			this.cmdInsert = new System.Windows.Forms.Button();
			this.cmdCompact = new System.Windows.Forms.Button();
			this.lblStatus = new System.Windows.Forms.Label();
			this.lblMore = new System.Windows.Forms.Label();
			this.mnuFile = new System.Windows.Forms.MainMenu();
			this.mnuFileFile = new System.Windows.Forms.MenuItem();
			this.mnuFileQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.rtfInfo = new System.Windows.Forms.RichTextBox();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
			this.imageXView3 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
			this.SuspendLayout();
			// 
			// cmdMake
			// 
			this.cmdMake.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdMake.Location = new System.Drawing.Point(360, 144);
			this.cmdMake.Name = "cmdMake";
			this.cmdMake.Size = new System.Drawing.Size(120, 24);
			this.cmdMake.TabIndex = 4;
			this.cmdMake.Text = "Make MP.TIF";
			this.cmdMake.Click += new System.EventHandler(this.cmdMake_Click);
			// 
			// cmdRemove
			// 
			this.cmdRemove.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdRemove.Location = new System.Drawing.Point(360, 176);
			this.cmdRemove.Name = "cmdRemove";
			this.cmdRemove.Size = new System.Drawing.Size(120, 24);
			this.cmdRemove.TabIndex = 5;
			this.cmdRemove.Text = "Remove Page 2";
			this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
			// 
			// cmdInsert
			// 
			this.cmdInsert.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdInsert.Location = new System.Drawing.Point(360, 208);
			this.cmdInsert.Name = "cmdInsert";
			this.cmdInsert.Size = new System.Drawing.Size(120, 24);
			this.cmdInsert.TabIndex = 6;
			this.cmdInsert.Text = "Insert Page 2";
			this.cmdInsert.Click += new System.EventHandler(this.cmdInsert_Click);
			// 
			// cmdCompact
			// 
			this.cmdCompact.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdCompact.Location = new System.Drawing.Point(360, 240);
			this.cmdCompact.Name = "cmdCompact";
			this.cmdCompact.Size = new System.Drawing.Size(120, 24);
			this.cmdCompact.TabIndex = 7;
			this.cmdCompact.Text = "Compact";
			this.cmdCompact.Click += new System.EventHandler(this.cmdCompact_Click);
			// 
			// lblStatus
			// 
			this.lblStatus.Location = new System.Drawing.Point(16, 144);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(200, 40);
			this.lblStatus.TabIndex = 8;
			// 
			// lblMore
			// 
			this.lblMore.Location = new System.Drawing.Point(224, 152);
			this.lblMore.Name = "lblMore";
			this.lblMore.Size = new System.Drawing.Size(120, 24);
			this.lblMore.TabIndex = 9;
			this.lblMore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuFileFile,
																					this.mnuToolbar,
																					this.mnuAbout});
			// 
			// mnuFileFile
			// 
			this.mnuFileFile.Index = 0;
			this.mnuFileFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuFileQuit});
			this.mnuFileFile.Text = "&File";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 0;
			this.mnuFileQuit.Text = "&Quit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "&Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "&Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// rtfInfo
			// 
			this.rtfInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.rtfInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.rtfInfo.Location = new System.Drawing.Point(16, 8);
			this.rtfInfo.Name = "rtfInfo";
			this.rtfInfo.ReadOnly = true;
			this.rtfInfo.Size = new System.Drawing.Size(464, 128);
			this.rtfInfo.TabIndex = 10;
			this.rtfInfo.Text = @"This example demonstrates operations that ImagXpress can perform on Multiple page Tiff files.
1. Creating  multiple page Tiff files
2. Displaying individual pages
3. Obtaining the number of Pages
4. Deleting a page
5. Inserting a page
6. Compacting unused pages from a file";
			// 
			// lblLastError
			// 
			this.lblLastError.Location = new System.Drawing.Point(16, 192);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(80, 72);
			this.lblLastError.TabIndex = 14;
			this.lblLastError.Text = "Last Error:";
			// 
			// lblError
			// 
			this.lblError.Location = new System.Drawing.Point(96, 192);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(248, 72);
			this.lblError.TabIndex = 15;
			// 
			// imageXView1
			// 
			this.imageXView1.Location = new System.Drawing.Point(16, 280);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(128, 152);
			this.imageXView1.TabIndex = 16;
			// 
			// imageXView2
			// 
			this.imageXView2.Location = new System.Drawing.Point(192, 280);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(128, 152);
			this.imageXView2.TabIndex = 17;
			// 
			// imageXView3
			// 
			this.imageXView3.Location = new System.Drawing.Point(352, 280);
			this.imageXView3.Name = "imageXView3";
			this.imageXView3.Size = new System.Drawing.Size(128, 152);
			this.imageXView3.TabIndex = 18;
			// 
			// frmMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(496, 465);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.imageXView3,
																		  this.imageXView2,
																		  this.imageXView1,
																		  this.lblError,
																		  this.lblLastError,
																		  this.rtfInfo,
																		  this.lblMore,
																		  this.lblStatus,
																		  this.cmdCompact,
																		  this.cmdInsert,
																		  this.cmdRemove,
																		  this.cmdMake});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Menu = this.mnuFile;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MultiPage TIFF";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmMain());
		}

		private void ReloadViews() 
		{
				
			//Load the MP TIFF here
			imgTmp = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strMPFile);
			System.Int32 nNbrPages = imgTmp.PageCount;
			
			// Show the first 5 pages
			for (System.Int32 i = 1; i <= 3; i++)
			{
				if( i <= nNbrPages)
				{

					switch(i)
					{

						case 1:
						{

							imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strMPFile);
							imageXView1.Image.Page = i;
							break;
						}
		
						case 2:
						{

							imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strMPFile);
							imageXView2.Image.Page = 2;
							break;
						}

						case 3:
						{
							imageXView3.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strMPFile);
							imageXView3.Image.Page = 3;
							break;
						}
					
					}

				}
				else
				{
					switch (i)
					{
						case 1:
						{
							imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, "");
							break;
						}

						case 2:
						{
							imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, "");
							break;
						}

						case 3:
						{
							imageXView3.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, "");
							break;
						}

					}
				}
			}
			
			lblStatus.Text = "File Size: " + imageXView1.Image.ImageXData.Size.ToString(cultNumber) + " bytes.\n" + "Pages: " + imageXView1.Image.PageCount;
					
			if ((imageXView1.Image.Page != 0))
			{
				if (imageXView1.Image.PageCount > 3)
				{
					lblMore.Text = "More...";
				} 
				else 
				{
					lblMore.Text = "";
				}
			}
			else
			{
				lblMore.Text = "";
			}
		}
			
		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void cmdMake_Click(object sender, System.EventArgs e)
		{
			
			if (System.IO.File.Exists(strMPFile) )
			{
				System.IO.File.Delete(strMPFile);
			}
			try 
			{

				//clear out any error before next operation
				lblError.Text = "";

				imgTmp = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile1);
				PegasusImaging.WinForms.ImagXpress9.SaveOptions soOpts = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
				try 
				{
					soOpts.Format = PegasusImaging.WinForms.ImagXpress9.ImageXFormat.Tiff;
					soOpts.Tiff.Compression = PegasusImaging.WinForms.ImagXpress9.Compression.NoCompression;
					soOpts.Tiff.MultiPage = true;
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
				imgTmp.Save(strMPFile,soOpts);
				try 
				{
					PegasusImaging.WinForms.ImagXpress9.ImageX.InsertPage(imagXpress1, strImageFile2, strMPFile, 2);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
				try 
				{
					PegasusImaging.WinForms.ImagXpress9.ImageX.InsertPage(imagXpress1, strImageFile3, strMPFile, 3);    
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
				return;
			}			
			ReloadViews();
		}
		private void frmMain_Load(object sender, System.EventArgs e)
		{

			//***Must call the UnlockRuntime method here***
			//imagXpress1.License.UnlockRuntime(12345,12345,12345);
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ();
			strImageFile1 = System.IO.Path.Combine (strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\page1.tif");
			strImageFile2 = System.IO.Path.Combine (strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\page2.tif");
			strImageFile3 = System.IO.Path.Combine (strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\page3.tif");
			strMPFile = System.IO.Path.Combine (strCurrentDir, @"..\\..\\mp.tif");
			strTmpFile = System.IO.Path.Combine (strCurrentDir, @"..\\..\\compact.tif");
			
		}

		private void cmdRemove_Click(object sender, System.EventArgs e)
		{
			try 
			{
				//clear out any error before next operation
				lblError.Text = "";

				PegasusImaging.WinForms.ImagXpress9.ImageX.DeletePage(imagXpress1, strMPFile, 2);
				
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImageXException ex)
			{
				PegasusError(ex,lblError);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			ReloadViews();
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
		System.IntPtr hwndTB;
			RECT controlRect = new RECT();
			RECT toolbarRect = new RECT();
			this.mnuToolbarShow.Text = (imageXView1.Toolbar.Activated)? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			hwndTB = imageXView1.Toolbar.Hwnd;
			Win32.GetWindowRect(imageXView1.Handle.ToInt32(),ref controlRect);
			Win32.GetWindowRect(hwndTB.ToInt32(),ref toolbarRect);
			Win32.SetWindowText(hwndTB.ToInt32(),"Toolbar 1");
			Win32.MoveWindow(hwndTB.ToInt32(),controlRect.left+8,controlRect.bottom + 12,toolbarRect.right - toolbarRect.left,toolbarRect.bottom - toolbarRect.top,true);
			try 
			{
				imageXView2.Toolbar.Activated = !imageXView2.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			hwndTB = imageXView2.Toolbar.Hwnd;
			Win32.GetWindowRect(imageXView2.Handle.ToInt32(),ref controlRect);
			Win32.GetWindowRect(hwndTB.ToInt32(),ref toolbarRect);
			Win32.SetWindowText(hwndTB.ToInt32(),"Toolbar 2");
			Win32.MoveWindow(hwndTB.ToInt32(),controlRect.left+8,controlRect.bottom + 12,toolbarRect.right - toolbarRect.left,toolbarRect.bottom - toolbarRect.top,true);
			try 
			{
				imageXView3.Toolbar.Activated = !imageXView3.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			hwndTB = imageXView3.Toolbar.Hwnd;
			Win32.GetWindowRect(imageXView3.Handle.ToInt32(),ref controlRect);
			Win32.GetWindowRect(hwndTB.ToInt32(),ref toolbarRect);
			Win32.SetWindowText(hwndTB.ToInt32(),"Toolbar 3");
			Win32.MoveWindow(hwndTB.ToInt32(),controlRect.left+8,controlRect.bottom + 12,toolbarRect.right - toolbarRect.left,toolbarRect.bottom - toolbarRect.top,true);

			
			
		}

		private void cmdInsert_Click(object sender, System.EventArgs e)
		{
			try 
			{
				//clear out any error before next operation
				lblError.Text = "";

				PegasusImaging.WinForms.ImagXpress9.ImageX.InsertPage(imagXpress1, strImageFile2, strMPFile, 2);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			ReloadViews();
		}

		private void cmdCompact_Click(object sender, System.EventArgs e)
		{
			try 
			{
				//clear out any error before next operation
				lblError.Text = "";

				PegasusImaging.WinForms.ImagXpress9.ImageX.CompactFile(imagXpress1, strMPFile, strTmpFile);
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);;
			}
			if (System.IO.File.Exists(strTmpFile)) 
			{
				System.IO.File.Delete(strMPFile);
				System.IO.File.Move(strTmpFile,strMPFile);
			}
			ReloadViews();
		}
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion
	}
}

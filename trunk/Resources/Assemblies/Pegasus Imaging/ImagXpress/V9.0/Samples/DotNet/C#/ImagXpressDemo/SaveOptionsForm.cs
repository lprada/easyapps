/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo
{
    public partial class SaveOptionsForm : Form
    {
        Unlocks unlock = new Unlocks();

        Helper helper = new Helper();

        public SaveOptionsForm()
        {
            InitializeComponent();
        }

        private SaveOptions so;

        private Color transparentColorGif, transparencyColorPng;

        public SaveOptions ImageSaveOptions
        {
            get
            {
                return so;
            }
            set
            {
                so = value;
            }
        }

        public bool FastTiffWrite
        {
            get
            {
                return FastCheckBox.Checked;
            }
        }

        public bool MetaDataSave
        {
            get
            {
                return MetaCheckBox.Checked;
            }
        }

        public Color TiffBackgroundColor
        {
            set
            {
                TIFFGroupBox.BackColor = value;
            }
        }

        public Color PdfBackgroundColor
        {
            set
            {
                PDFGroupBox.BackColor = value;
            }
        }

        public Color JpgBackgroundColor
        {
            set
            {
                JPGGroupBox.BackColor = value;
            }
        }

        public Color ExifBackgroundColor
        {
            set
            {
                EXIFGroupBox.BackColor = value;
            }
        }

        public Color LjpBackgroundColor
        {
            set
            {
                LJPGroupBox.BackColor = value;
            }
        }

        public Color JlsBackgroundColor
        {
            set
            {
                JLSGroupBox.BackColor = value;
            }
        }

        public Color PicBackgroundColor
        {
            set
            {
                PICGroupBox.BackColor = value;
            }
        }

        public Color GifBackgroundColor
        {
            set
            {
                GIFGroupBox.BackColor = value;
            }
        }

        public Color PngBackgroundColor
        {
            set
            {
                PNGGroupBox.BackColor = value;
            }
        }

        public Color DcxBackgroundColor
        {
            set
            {
                DCXGroupBox.BackColor = value;
            }
        }

        public Color Jp2BackgroundColor
        {
            set
            {
                JP2GroupBox.BackColor = value;
            }
        }

        public Color IconBackgroundColor
        {
            set
            {
                ICONGroupBox.BackColor = value;
            }
        }

        public Color PortableBackgroundColor
        {
            set
            {
                PortableGroupBox.BackColor = value;
            }
        }

        public Color WsqBackgroundColor
        {
            set
            {
                WSQGroupBox.BackColor = value;
            }
        }

        public Color JbigBackgroundColor
        {
            set
            {
                JBIGGroupBox.BackColor = value;
            }
        }

        public Color HdpBackgroundColor
        {
            set
            {
                HDPGroupBox.BackColor = value;
            }
        }

        private void SaveOptionsForm_Load(object sender, EventArgs e)
        {
            try
            {
                transparentColorGif = new Color();

                PortableComboBox.SelectedIndex = 0;
                renderIntentComboBox.SelectedIndex = 0;
                ByteOrderComboBox.SelectedIndex = 0;
                ColorSpaceTIFFcomboBox.SelectedIndex = 0;
                CompressionComboBox.SelectedIndex = 0;
                GIFComboBox.SelectedIndex = 0;
                MatchGifComboBox.SelectedIndex = 0;
                ChromaComboBox.SelectedIndex = 0;
                FTPComboBox.SelectedIndex = 0;
                EncodeComboBox.SelectedIndex = 0;
                FileOrgComboBox.SelectedIndex = 0;
                ColorSpaceJPGComboBox.SelectedIndex = 0;
                JPGSubComboBox.SelectedIndex = 0;
                OrderComboBox.SelectedIndex = 0;
                JP2ComboBox.SelectedIndex = 0;
                LJPComboBox.SelectedIndex = 0;
                PngMatchComboBox.SelectedIndex = 0;
                ThumbComboBox.SelectedIndex = 0;
                PDFColorSpaceComboBox.SelectedIndex = 0;
                PDFCompressionComboBox.SelectedIndex = 0;
                comboBoxMethod.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                so.BufferFtp = bufferCheckBox.Checked;

                so.ColorRenderIntent = (RenderIntent) renderIntentComboBox.SelectedIndex;

                so.Dcx.MultiPage = DCXMultiCheckBox.Checked;

                so.Exif.ThumbnailSize = ThumbComboBox.SelectedIndex;

                so.Gif.Interlaced = InterlacedGIFCheckBox.Checked;
                so.Gif.TransparencyColor = transparentColorGif;
                so.Gif.TransparencyMatch = (TransparencyMatch)MatchGifComboBox.SelectedIndex;
                so.Gif.Type = (GifType)GIFComboBox.SelectedIndex;

                so.Hdp.ChromaSubSampling = (HdpSubSampling) ChromaComboBox.SelectedIndex;
                so.Hdp.FrequencyOrder = (int)FrequencyBox.Value;
                so.Hdp.Quantization = (int) QuantizationHDPBox.Value;

                so.Icon.MultiPage = ICONMultiCheckBox.Checked;

                so.Internet.FtpMode = (FtpMode)FTPComboBox.SelectedIndex;
                so.Internet.NetworkCredentials = new System.Net.NetworkCredential(UserNameTextBox.Text, InternetPasswordTextBox.Text,
                        DomainTextBox.Text);

                so.Jbig2.EncodeModeCompression = (Jbig2EncodeModeCompression) EncodeComboBox.SelectedIndex;
                so.Jbig2.FileOrganization = (Jbig2FileOrganization)FileOrgComboBox.SelectedIndex;
                so.Jbig2.InvertedRegion = InvertCheckBox.Checked;
                so.Jbig2.LoosenessCompression = (int)looseBox.Value;

                so.Jls.Interleave = (int)InterleaveBox.Value;
                so.Jls.MaxValue = (int)MaxBox.Value;
                so.Jls.Near = (int)NearBox.Value;
                so.Jls.Point = (int)PointBox.Value;

                so.Jp2.CompressSize = (int)CompressBox.Value;
                so.Jp2.Grayscale = GrayscaleJP2CheckBox.Checked;
                so.Jp2.Order = (ProgressionOrder) OrderComboBox.SelectedIndex;
                so.Jp2.PeakSignalToNoiseRatio = (double)SNRBox.Value;
                so.Jp2.TileSize = new Size((int)TileWBox.Value, (int)TileHBox.Value);
                so.Jp2.Type = (Jp2Type)JP2ComboBox.SelectedIndex;

                so.Jpeg.Chrominance = (int)ChromBox.Value;
                so.Jpeg.ColorSpace = (ColorSpace) ColorSpaceJPGComboBox.SelectedIndex;
                so.Jpeg.Cosited = CositedCheckBox.Checked;
                so.Jpeg.Grayscale = GrayscaleJPGCheckBox.Checked;
                so.Jpeg.Luminance = (int)LuminanceBox.Value;
                so.Jpeg.Progressive = ProgressiveCheckBox.Checked;
                so.Jpeg.SubSampling = (SubSampling)JPGSubComboBox.SelectedIndex;

                so.LibraryNumberOfThreadsAllowed = (int)LibraryBox.Value;

                so.Ljp.Type = (LjpType)LJPComboBox.SelectedIndex;
                so.Ljp.Method = (LjpMethod)comboBoxMethod.SelectedIndex;
                so.Ljp.Order = (int)orderBox.Value;
                so.Ljp.Predictor = (int)predictorBox.Value;

                so.Pic.Password = PasswordTextBox.Text;

                so.Pdf.ColorSpace = (ColorSpace) PDFColorSpaceComboBox.SelectedIndex;

                if (PDFColorSpaceComboBox.SelectedIndex == 0)
                {
                    switch (PDFCompressionComboBox.SelectedIndex)
                    {
                        case 0:
                            {
                                so.Pdf.Compression = Compression.NoCompression;
                                break;
                            }
                        case 1:
                            {
                                so.Pdf.Compression = Compression.Group3Fax1d;
                                break;
                            }
                        case 2:
                            {
                                so.Pdf.Compression = Compression.Group3Fax2d;
                                break;
                            }
                        case 3:
                            {
                                so.Pdf.Compression = Compression.Group4;
                                break;
                            }
                        case 4:
                            {
                                so.Pdf.Compression = Compression.Jpeg;
                                break;
                            }
                        case 5:
                            {
                                so.Pdf.Compression = Compression.Jbig2;
                                break;
                            }
                        case 6:
                            {
                                so.Pdf.Compression = Compression.Jpeg2000;
                                break;
                            }
                    }
                }
                else
                {
                    so.Pdf.Compression = Compression.Jpeg;
                }
                
                so.Pdf.MultiPage = PDFMultiCheckBox.Checked;
                so.Pdf.SwapBlackAndWhite = swapCheckBox.Checked;

                so.Png.Interlaced = InterlacedPNGCheckBox.Checked;
                so.Png.TransparencyColor = transparencyColorPng;
                so.Png.TransparencyMatch = (TransparencyMatch)PngMatchComboBox.SelectedIndex;

                so.Portable.Type = (PortableType)PortableComboBox.SelectedIndex;

                so.Tiff.ByteOrder = (ByteOrder) ByteOrderComboBox.SelectedIndex;
                so.Tiff.ColorSpace = (ColorSpace)ColorSpaceTIFFcomboBox.SelectedIndex;
                so.Tiff.Compression = (Compression)CompressionComboBox.SelectedIndex;
                so.Tiff.MultiPage = MultiTiffCheckBox.Checked;
                so.Tiff.RowsPerStrip = (int)RowsBox.Value;

                if (FastCheckBox.Checked == true)
                {
                    so.Tiff.UseIFDOffset = true;
                    so.Tiff.IFDOffset = 0;
                }

                so.UseEmbeddedColorManagement = UseEmbedCheckBox.Checked;

                so.Wsq.Black = (int)BlackBox.Value;
                so.Wsq.White = (int)WhiteBox.Value;
                so.Wsq.Quantization = (double)QuantizationWSQBox.Value;

                DialogResult = DialogResult.OK;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void transparencyColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = transparencyColorButton.BackColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        transparencyColorButton.BackColor = dlg.Color;

                        transparentColorGif = dlg.Color;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void transpColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = transpColorButton.BackColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        transpColorButton.BackColor = dlg.Color;

                        transparencyColorPng = dlg.Color;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void targetButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.InitialDirectory = helper.GetPath()[0];
                    openDialog.Filter = "All Supported Profiles (*.ICM, *.ICC)|*.icm;*.icc|All Files (*.*)|*.*";
                    openDialog.FilterIndex = 0;
                    openDialog.Title = "Open a Target Profile";

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        so.TargetProfileName = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void MultiTiffCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (MultiTiffCheckBox.Checked == false)
            {
                FastCheckBox.Checked = false;
                FastCheckBox.Enabled = false;
            }
            else
            {
                FastCheckBox.Enabled = true;
            }
        }

        private void ColorSpaceTIFFcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CompressionComboBox.Items.Clear();

                if (ColorSpaceTIFFcomboBox.SelectedIndex == 1)
                {
                    UseEmbedCheckBox.Checked = true;

                    if (so.TargetProfileName == null)
                    {
                        MessageBox.Show("Please choose a Target Profile first to save to CMYK.",
                            "CMYK", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    CompressionComboBox.Items.Add("No Compression");
                }
                else
                {
                    CompressionComboBox.Items.Add("No Compression");
                    CompressionComboBox.Items.Add("Rle");
                    CompressionComboBox.Items.Add("Lzw");
                    CompressionComboBox.Items.Add("PackBits");
                    CompressionComboBox.Items.Add("Defalte");
                    CompressionComboBox.Items.Add("Group3 1D");
                    CompressionComboBox.Items.Add("Group3 2D");
                    CompressionComboBox.Items.Add("Group4");
                    CompressionComboBox.Items.Add("Jpeg");
                    CompressionComboBox.Items.Add("Jpeg7");
                }

                CompressionComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PDFColorSpaceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PDFCompressionComboBox.Items.Clear();

                if (PDFColorSpaceComboBox.SelectedIndex == 1)
                {
                    UseEmbedCheckBox.Checked = true;

                    if (so.TargetProfileName == null)
                    {
                        MessageBox.Show("Please choose a Target Profile first to save to CMYK.",
                            "CMYK", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    PDFCompressionComboBox.Items.Add("Jpeg");
                }
                else
                {
                    PDFCompressionComboBox.Items.Add("No Compression");
                    PDFCompressionComboBox.Items.Add("Group3 1D");
                    PDFCompressionComboBox.Items.Add("Group3 2D");
                    PDFCompressionComboBox.Items.Add("Group4");
                    PDFCompressionComboBox.Items.Add("Jpeg");
                    PDFCompressionComboBox.Items.Add("Jbig2");
                    PDFCompressionComboBox.Items.Add("Jpeg2000");
                }

                PDFCompressionComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ColorSpaceJPGComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ColorSpaceJPGComboBox.SelectedIndex == 1)
                {
                    UseEmbedCheckBox.Checked = true;

                    ProgressiveCheckBox.Checked = false;

                    if (so.TargetProfileName == null)
                    {
                        MessageBox.Show("Please choose a Target Profile first to save to CMYK.",
                            "CMYK", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
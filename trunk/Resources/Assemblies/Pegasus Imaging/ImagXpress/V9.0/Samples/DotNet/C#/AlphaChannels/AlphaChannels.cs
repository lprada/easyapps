/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress9;

namespace Alpha_Channels
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
        private System.Windows.Forms.Label AlphaLabel1;
		private System.Windows.Forms.Label SourceLabel;
		private System.Windows.Forms.ListBox DescriptionListBox;
        private System.Windows.Forms.Button BlendButton;
		private System.Windows.Forms.ComboBox AlphaComboBox;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView2;
		private System.Windows.Forms.MenuItem FileMenu;
		private System.Windows.Forms.MenuItem OpenSourceMenuItem;
		private System.Windows.Forms.MenuItem OpenAlphaMenuItem;
		private System.Windows.Forms.MenuItem ExitMenuItem;
		private System.Windows.Forms.MenuItem ToolbarMenu;
		private System.Windows.Forms.MenuItem ShowMenuItem;
		private System.Windows.Forms.MenuItem AboutMenu;
		private System.Windows.Forms.MenuItem ImagXpressMenuItem;
		private System.Windows.Forms.MenuItem FileMenuSeparator;
		private System.Windows.Forms.MainMenu MainMenu;

		private PegasusImaging.WinForms.ImagXpress9.Processor ixproc1;

        private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripStatusLabel statusLabel;
        private IContainer components;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (imageXView1.Image != null)
				{
					imageXView1.Image.Dispose();
					imageXView1.Image = null;
				}
				if (imageXView2.Image != null)
				{
					imageXView2.Image.Dispose();
					imageXView2.Image = null;
				}
				if (imageXView1 != null)
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (imageXView2 != null)
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.AlphaLabel1 = new System.Windows.Forms.Label();
            this.SourceLabel = new System.Windows.Forms.Label();
            this.DescriptionListBox = new System.Windows.Forms.ListBox();
            this.BlendButton = new System.Windows.Forms.Button();
            this.AlphaComboBox = new System.Windows.Forms.ComboBox();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXView2 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.OpenSourceMenuItem = new System.Windows.Forms.MenuItem();
            this.OpenAlphaMenuItem = new System.Windows.Forms.MenuItem();
            this.FileMenuSeparator = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.ToolbarMenu = new System.Windows.Forms.MenuItem();
            this.ShowMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AlphaLabel1
            // 
            this.AlphaLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AlphaLabel1.Location = new System.Drawing.Point(71, 152);
            this.AlphaLabel1.Name = "AlphaLabel1";
            this.AlphaLabel1.Size = new System.Drawing.Size(120, 16);
            this.AlphaLabel1.TabIndex = 0;
            this.AlphaLabel1.Text = "Alpha Channel Image";
            this.AlphaLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SourceLabel
            // 
            this.SourceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SourceLabel.Location = new System.Drawing.Point(430, 152);
            this.SourceLabel.Name = "SourceLabel";
            this.SourceLabel.Size = new System.Drawing.Size(88, 16);
            this.SourceLabel.TabIndex = 4;
            this.SourceLabel.Text = "Source Image";
            this.SourceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DescriptionListBox
            // 
            this.DescriptionListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionListBox.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Merging an image with an alpha channel to a source image and displaying the com" +
                "bined image. ",
            "",
            "You can select an Alpha Channel image in the dropdown list or load your own sourc" +
                "e image and/or alpha channel image",
            "in the sample."});
            this.DescriptionListBox.Location = new System.Drawing.Point(8, 8);
            this.DescriptionListBox.Name = "DescriptionListBox";
            this.DescriptionListBox.Size = new System.Drawing.Size(692, 69);
            this.DescriptionListBox.TabIndex = 5;
            // 
            // BlendButton
            // 
            this.BlendButton.Location = new System.Drawing.Point(263, 104);
            this.BlendButton.Name = "BlendButton";
            this.BlendButton.Size = new System.Drawing.Size(160, 32);
            this.BlendButton.TabIndex = 6;
            this.BlendButton.Text = "Blend Alpha Channel Image with Source Image";
            this.BlendButton.Click += new System.EventHandler(this.BlendButton_Click);
            // 
            // AlphaComboBox
            // 
            this.AlphaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AlphaComboBox.Items.AddRange(new object[] {
            "AlphaImage1",
            "AlphaImage2",
            "AlpahaImage3"});
            this.AlphaComboBox.Location = new System.Drawing.Point(23, 104);
            this.AlphaComboBox.Name = "AlphaComboBox";
            this.AlphaComboBox.Size = new System.Drawing.Size(144, 21);
            this.AlphaComboBox.TabIndex = 8;
            this.AlphaComboBox.SelectedIndexChanged += new System.EventHandler(this.AlphaComboBox_SelectedIndexChanged);
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.Location = new System.Drawing.Point(263, 171);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(423, 386);
            this.imageXView1.TabIndex = 9;
            // 
            // imageXView2
            // 
            this.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView2.Location = new System.Drawing.Point(23, 171);
            this.imageXView2.MouseWheelCapture = false;
            this.imageXView2.Name = "imageXView2";
            this.imageXView2.Size = new System.Drawing.Size(216, 386);
            this.imageXView2.TabIndex = 10;
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.ToolbarMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.OpenSourceMenuItem,
            this.OpenAlphaMenuItem,
            this.FileMenuSeparator,
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // OpenSourceMenuItem
            // 
            this.OpenSourceMenuItem.Index = 0;
            this.OpenSourceMenuItem.Text = "&Open Source Image";
            this.OpenSourceMenuItem.Click += new System.EventHandler(this.OpenSourceMenuItem_Click);
            // 
            // OpenAlphaMenuItem
            // 
            this.OpenAlphaMenuItem.Index = 1;
            this.OpenAlphaMenuItem.Text = "&Open Alpha Channel Image";
            this.OpenAlphaMenuItem.Click += new System.EventHandler(this.OpenAlphaMenuItem_Click);
            // 
            // FileMenuSeparator
            // 
            this.FileMenuSeparator.Index = 2;
            this.FileMenuSeparator.Text = "-";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 3;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // ToolbarMenu
            // 
            this.ToolbarMenu.Index = 1;
            this.ToolbarMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ShowMenuItem});
            this.ToolbarMenu.Text = "&Toolbar";
            // 
            // ShowMenuItem
            // 
            this.ShowMenuItem.Index = 0;
            this.ShowMenuItem.Text = "&Show";
            this.ShowMenuItem.Click += new System.EventHandler(this.ShowMenuItem_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 2;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ImagXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 0;
            this.ImagXpressMenuItem.Text = "Imag&Xpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 564);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(706, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(20, 17);
            this.statusLabel.Text = "Ok";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(706, 586);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.imageXView2);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.AlphaComboBox);
            this.Controls.Add(this.BlendButton);
            this.Controls.Add(this.DescriptionListBox);
            this.Controls.Add(this.SourceLabel);
            this.Controls.Add(this.AlphaLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alpha Channels";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private void ImagXpressMenuItem_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void ExitMenuItem_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
        
        private void ResetStatus( )
        {
            statusLabel.Text = "Ok";
        }

        private void SetStatus(ImagXpressException ex)
        {
            SetStatus(ex.Message + "\n" + ex.Source + "\n" + "Error Number: " + ex.Number.ToString());
        }

        private void SetStatus(string status)
        {
            statusLabel.Text = status;
        }

		private void ShowMenuItem_Click(object sender, System.EventArgs e)
		{
			if (ShowMenuItem.Checked == false)
			{
				imageXView1.Toolbar.Activated = true;
				ShowMenuItem.Checked = true;
			}
			else
			{
				imageXView1.Toolbar.Activated = false;
				ShowMenuItem.Checked = false;
			}
		}

		private void MainForm_Load(object sender, System.EventArgs e)
		{
            Application.EnableVisualStyles();
            
            //***Must call UnlockRuntime to unlock control
			//imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

			ixproc1 = new PegasusImaging.WinForms.ImagXpress9.Processor(imagXpress1);
			AlphaComboBox.SelectedIndex = 0;
			imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1,Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\Alpha1.tif");

			try
			{
                imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\Alpha1.jpg");
				ixproc1.Image = imageXView1.Image;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
			{
                SetStatus( ex );              
			}	 
		}

		private void BlendButton_Click(object sender, System.EventArgs e)
		{
			try
			{
                ResetStatus();
				PegasusImaging.WinForms.ImagXpress9.ImageX alphaImage = imageXView2.Image.Copy();

                // Set an arbitrary location to merge the image into
                PointF pt = GetAlphaImageLocation( );
                SizeF size = new SizeF( alphaImage.ImageXData.Width, alphaImage.ImageXData.Height );
                ixproc1.SetArea(new RectangleF(pt, size));
                ixproc1.EnableArea = true;

				ixproc1.Merge(ref alphaImage, PegasusImaging.WinForms.ImagXpress9.MergeSize.Crop, PegasusImaging.WinForms.ImagXpress9.MergeStyle.AlphaForeGroundOverBackGround, false, System.Drawing.Color.Blue, 90, 90);
				if (alphaImage != null)
				{
					alphaImage.Dispose();
					alphaImage = null;
				}
                SetStatus("Image successfully blended!");
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
			{
				SetStatus( ex );
			}
		}

        private PointF GetAlphaImageLocation( )
        {
            PointF pt = PointF.Empty;
            switch (AlphaComboBox.SelectedIndex)
            {
                case 0:
                    pt = new PointF( 100, 50 );
                    break;
                case 1:
                    pt = new PointF( 0, 25 );
                    break;
                case 2:
                    pt = new PointF( 500, 200 );
                    break;
            }
            return pt;
        }

		private void AlphaComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            ResetStatus();

            try
            {
                switch (AlphaComboBox.SelectedIndex)
                {
                    case 0:
                        {
                            imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\Alpha1.tif");
                            break;
                        }
                    case 1:
                        {
                            imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\Alpha2.tif");
                            break;
                        }
                    case 2:
                        {
                            imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\Alpha3.tif");
                            break;
                        }
                }
            }
            catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
            {
                SetStatus( ex );
            }
		}

		private void OpenSourceMenuItem_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.FilterIndex = 0;
			dlg.InitialDirectory = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images";
			dlg.Title = "Open Source Image";
			dlg.Filter = "All Files (*.*)|*.*";
			
			dlg.ShowDialog();
			
			if (dlg.FileName != "")
			{
				try
				{
                    imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, dlg.FileName);
					ixproc1.Image = imageXView1.Image;
                    ResetStatus();
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
				{
                    SetStatus(ex);
				}
			}
		}

		private void OpenAlphaMenuItem_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.FilterIndex = 0;
			dlg.InitialDirectory = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images";
			dlg.Title = "Open Alpha Image";
			dlg.Filter = "All Files (*.*)|*.*";
			
			dlg.ShowDialog();
			
			if (dlg.FileName != "")
			{
				try
				{
                    imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, dlg.FileName);
                    ResetStatus();
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
				{
                    SetStatus( ex );
				}
			}
		}
	}
}

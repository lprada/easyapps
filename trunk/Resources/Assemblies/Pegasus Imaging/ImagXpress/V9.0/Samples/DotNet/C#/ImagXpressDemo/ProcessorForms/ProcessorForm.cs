/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo.ProcessorForms
{
    public partial class ProcessorForm : Form
    {
        public ProcessorForm(string name)
        {
            InitializeComponent();

            Text = name;
        }

        Unlocks unlock = new Unlocks();

        public ImageX CurrentImage
        {
            set
            {
                imageXViewCurrent.Image = value;
            }
        }

        public ImageX PreviewImage
        {
            get
            {
                return imageXViewPreview.Image;
            }
        }

        private void ProcessorForm_Load(object sender, EventArgs e)
        {
            try
            {
             
                
                unlock.UnlockControls(imagXpress1);

                DoProcessorAction();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            DoProcessorAction();

            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void DoProcessorAction()
        {
            try
            {
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
                
                processor1.Image = imageXViewCurrent.Image.Copy();

                switch (Text)
                {
                    case "Equalize":
                        {
                            processor1.Equalize();
                            break;
                        }
                    case "Mirror":
                        {
                            processor1.Mirror();
                            break;
                        }
                    case "AutoContrast":
                        {
                            processor1.AutoContrast();
                            break;
                        }
                    case "Emboss":
                        {
                            processor1.Emboss();
                            break;
                        }
                    case "Despeckle":
                        {
                            processor1.Despeckle();
                            break;
                        }
                    case "Dilate":
                        {
                            processor1.Dilate();
                            break;
                        }
                    case "Erode":
                        {
                            processor1.Erode();
                            break;
                        }
                    case "Negate":
                        {
                            processor1.Negate();
                            break;
                        }
                    case "DocumentBorderCrop":
                        {
                            processor1.DocumentBorderCrop();
                            break;
                        }
                    case "DocumentZoomSmooth":
                        {
                            processor1.DocumentZoomSmooth();
                            break;
                        }
                    case "Diffuse":
                        {
                            processor1.Diffuse();
                            break;
                        }
                    case "Blur":
                        {
                            processor1.Blur();
                            break;
                        }
                    case "Median":
                        {
                            processor1.Median();
                            break;
                        }
                    case "AutoColorBalance":
                        {
                            processor1.AutoColorBalance();
                            break;
                        }
                    case "AutoLightness":
                        {
                            processor1.AutoLightness();
                            break;
                        }
                    case "Outline":
                        {
                            processor1.Outline();
                            break;
                        }
                    case "Flip":
                        {
                            processor1.Flip();
                            break;
                        }
                    case "AutoLevel":
                        {
                            processor1.AutoLevel();
                            break;
                        }
                }

                if (imageXViewPreview != null)
                {
                    imageXViewPreview.Image = processor1.Image;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            imageXViewPreview.Image = imageXViewCurrent.Image;
        }

        private void HelpButton_Click(object sender, EventArgs e)
        {
            Helper help = new Helper();

            help.ShowHelp(this, Text);
        }
    }
}
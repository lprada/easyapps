/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (twainPro1 != null)
            {
                twainPro1.Dispose();
                twainPro1 = null;
            }
            if (printPro1 != null)
            {
                printPro1.Dispose();
                printPro1 = null;
            }
            if (notateXpress1 != null)
            {
                notateXpress1.ClientDisconnect();

                notateXpress1.Dispose();
                notateXpress1 = null;
            }
            if (pdfXpress1 != null)
            {
                if (pdfXpress1.Documents != null)
                {
                    for (int i = 0; i < pdfXpress1.Documents.Count; i++)
                    {
                        if (pdfXpress1.Documents[i] != null)
                        {
                            pdfXpress1.Documents[i].Dispose();
                        }
                    }
                    pdfXpress1.Documents.Dispose();
                }

                pdfXpress1.Dispose();
                pdfXpress1 = null;
            }
            if (thumbChosen != null)
            {
                thumbChosen.Dispose();
                thumbChosen = null;
            }
            if (thumbnailXpressPaging != null)
            {
                thumbnailXpressPaging.Dispose();
                thumbnailXpressPaging = null;
            }
            if (thumbnailXpressDrillDown != null)
            {
                thumbnailXpressDrillDown.Dispose();
                thumbnailXpressDrillDown = null;
            }
            if (dataForCad != null)
            {
                dataForCad.Dispose();
                dataForCad = null;
            }
            if (imageXView != null)
            {
                if (imageXView.Image != null)
                {
                    imageXView.Image.Dispose();
                    imageXView.Image = null;
                }

                imageXView.Dispose();
                imageXView = null;
            }
            if (imageXViewHistogram != null)
            {
                if (imageXViewHistogram.Image != null)
                {
                    imageXViewHistogram.Image.Dispose();
                    imageXViewHistogram.Image = null;
                }

                imageXViewHistogram.Dispose();
                imageXViewHistogram = null;
            }
            if (redPen != null)
            {
                redPen.Dispose();
                redPen = null;
            }
            if (greenPen != null)
            {
                greenPen.Dispose();
                greenPen = null;
            }
            if (bluePen != null)
            {
                bluePen.Dispose();
                bluePen = null;
            }
            if (blackPen != null)
            {
                blackPen.Dispose();
                blackPen = null;
            }
            if (imageToolImage != null)
            {
                imageToolImage.Dispose();
                imageToolImage = null;
            }
            if (PrintImages != null)
            {
                for (int i = 0; i < PrintImages.Length; i++)
                {
                    if (PrintImages[i] != null)
                    {
                        PrintImages[i].Dispose();
                        PrintImages[i] = null;
                    }
                }
            }
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (folderBrowserDialog != null)
            {
                folderBrowserDialog.Dispose();
                folderBrowserDialog = null;
            }
            if (borderColorDialog != null)
            {
                borderColorDialog.Dispose();
                borderColorDialog = null;
            }
            if (spacingColorDialog != null)
            {
                spacingColorDialog.Dispose();
                spacingColorDialog = null;
            }
            if (backColorDialog != null)
            {
                backColorDialog.Dispose();
                backColorDialog = null;
            }
            if (textBackColorDialog != null)
            {
                textBackColorDialog.Dispose();
                textBackColorDialog = null;
            }
            if (selectBackColorDialog != null)
            {
                selectBackColorDialog.Dispose();
                selectBackColorDialog = null;
            }
            if (fontDialog != null)
            {
                fontDialog.Dispose();
                fontDialog = null;
            }
            if (font != null)
            {
                font.Dispose();
                font = null;
            }
            if (redImage != null)
            {
                redImage.Dispose();
                redImage = null;
            }
            if (greenImage != null)
            {
                greenImage.Dispose();
                greenImage = null;
            }
            if (blueImage != null)
            {
                blueImage.Dispose();
                blueImage = null;
            }
            if (alphaImage != null)
            {
                alphaImage.Dispose();
                alphaImage = null;
            }
            if (hueImage != null)
            {
                hueImage.Dispose();
                hueImage = null;
            }
            if (saturationImage != null)
            {
                saturationImage.Dispose();
                saturationImage = null;
            }
            if (luminanceImage != null)
            {
                luminanceImage.Dispose();
                luminanceImage = null;
            }
            if (cyanImage != null)
            {
                cyanImage.Dispose();
                cyanImage = null;
            }
            if (magentaImage != null)
            {
                magentaImage.Dispose();
                magentaImage = null;
            }
            if (yellowImage != null)
            {
                yellowImage.Dispose();
                yellowImage = null;
            }
            if (blackImage != null)
            {
                blackImage.Dispose();
                blackImage = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (cadLayerCheckBoxes != null)
            {
                for (int i = 0; i < cadLayerCheckBoxes.Length; i++)
                {
                    if (cadLayerCheckBoxes[i] != null)
                    {
                        cadLayerCheckBoxes[i].Dispose();
                        cadLayerCheckBoxes[i] = null;
                    }
                }
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileWithLoadOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directoryOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rawFileOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentCleanupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.binarizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blobRemovalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blobRemoval1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blobRemoval2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borderDetectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borderCrop1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borderCrop2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deskewToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deskew1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deskew2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deskew3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.despeckleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.despeckle1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.despeckle2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.despeckle3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dilateToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.erodeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.multiPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLines1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeLines2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smoothZoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tagsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.photoEnhancementToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alphaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alpha1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alpha2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alpha3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.colorBalanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoBalance1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoBalance2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoLevel1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoLevel2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contrastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoContrastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoLightnessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cropToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eXIFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orientationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redEyeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redEye1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redEye2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redEye3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redEye4ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDustToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDust1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDust2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scratchedToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpen1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpen2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effects1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effects2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effects3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scanImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveasNoOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.withSaveOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentCleanupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binarizeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.blobRemovalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.borderCropToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deskewToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.despeckleToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dilateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.erodeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lineRemovalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.negateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.shearToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.smoothZoomToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.photoEnhancementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alphaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.flattenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reduceToAlphaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brightnessToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.brightnessToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gammaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cleanupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deskewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.despeckleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dilateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.erodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorBalanceAutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.levelAutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorBalanceManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorModifyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adjustHSLToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.adjustRGBToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDepthToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceColorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contrastToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.autoToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.equalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lightnessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lightnessManualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cropToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cropToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cropBroderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orientationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mirrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.redEyeRemovalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDustToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeScratchesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resizeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpenToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.softenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.unsharpenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersAndEffectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blurToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonizeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.diffuseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.embossToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.matrixToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.medianToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mosaicToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.motionBlurToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.negateToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.noiseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outlineToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.parabolicToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.perspectiveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pinchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.polynomialWarpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.posterizeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rippleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.solarizeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.swirlToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.twistToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gettingStartedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pdfXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printProToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thumbnailXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twainProToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Toolbar = new System.Windows.Forms.ToolStrip();
            this.OpenButton = new System.Windows.Forms.ToolStripSplitButton();
            this.fileToolStripDropDownMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileWithLoadOptionsDropDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rawFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownSampleImages = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBinarize = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBlob = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBlob1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBlob2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBorder = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBorder1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownBorder2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDeskew = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDeskew1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDeskew2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDeskew3 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDespeckle = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDespeckle1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDespeckle2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDespeckle3 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDilate = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownErode = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownMulti = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownNegate = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownLineRemoval = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownLineRemoval1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownLineRemoval2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownShear = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownSmooth = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownTags = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownPhoto = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownAlpha = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownAlpha1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownAlpha2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownAlpha3 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownColorBalance = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownColorBalance1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownColorBalance2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownColorBalance3 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownColorBalance4 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownContrast = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownContrast1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownContrast2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownCrop = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownExif = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownOrientation = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownOrientation1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownOrientation2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownRedEye = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownRedEye1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownRedEye2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownRedEye3 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownRedEye4 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDust = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDust1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownDust2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownScratch = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownSharpen = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownSharpen1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownSharpen2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownEffects = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownEffects1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownEffects2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dropDownEffects3 = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseButton = new System.Windows.Forms.ToolStripButton();
            this.SaveButton = new System.Windows.Forms.ToolStripButton();
            this.PrintButton = new System.Windows.Forms.ToolStripButton();
            this.ScanButton = new System.Windows.Forms.ToolStripButton();
            this.CutButton = new System.Windows.Forms.ToolStripButton();
            this.CopyButton = new System.Windows.Forms.ToolStripButton();
            this.PasteButton = new System.Windows.Forms.ToolStripButton();
            this.RefreshButton = new System.Windows.Forms.ToolStripButton();
            this.ViewingSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.ViewingLabel = new System.Windows.Forms.ToolStripLabel();
            this.ZoomFitButton = new System.Windows.Forms.ToolStripButton();
            this.Zoom1to1Button = new System.Windows.Forms.ToolStripButton();
            this.ZoomWidthButton = new System.Windows.Forms.ToolStripButton();
            this.ZoomHeightButton = new System.Windows.Forms.ToolStripButton();
            this.RotateLeftButton = new System.Windows.Forms.ToolStripButton();
            this.RotateRightButton = new System.Windows.Forms.ToolStripButton();
            this.PointerIXButton = new System.Windows.Forms.ToolStripButton();
            this.ZoomInButton = new System.Windows.Forms.ToolStripButton();
            this.ZoomOutButton = new System.Windows.Forms.ToolStripButton();
            this.ZoomRectangleButton = new System.Windows.Forms.ToolStripButton();
            this.HandButton = new System.Windows.Forms.ToolStripButton();
            this.AreaButton = new System.Windows.Forms.ToolStripButton();
            this.MagnifierButton = new System.Windows.Forms.ToolStripButton();
            this.AnnotationSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.AnnotationsToolStrip = new System.Windows.Forms.ToolStripLabel();
            this.PointerNXButton = new System.Windows.Forms.ToolStripButton();
            this.NoteButton = new System.Windows.Forms.ToolStripButton();
            this.TextButton = new System.Windows.Forms.ToolStripButton();
            this.RectangleButton = new System.Windows.Forms.ToolStripButton();
            this.PolygonButton = new System.Windows.Forms.ToolStripButton();
            this.PolylineButton = new System.Windows.Forms.ToolStripButton();
            this.LineButton = new System.Windows.Forms.ToolStripButton();
            this.FreehandButton = new System.Windows.Forms.ToolStripButton();
            this.StampButton = new System.Windows.Forms.ToolStripButton();
            this.EllipseButton = new System.Windows.Forms.ToolStripButton();
            this.RulerButton = new System.Windows.Forms.ToolStripButton();
            this.ImageButton = new System.Windows.Forms.ToolStripButton();
            this.HighlightButton = new System.Windows.Forms.ToolStripButton();
            this.ButtonButton = new System.Windows.Forms.ToolStripButton();
            this.BrandButton = new System.Windows.Forms.ToolStripButton();
            this.PagingSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.PageLabel = new System.Windows.Forms.ToolStripLabel();
            this.PageBox = new System.Windows.Forms.ToolStripTextBox();
            this.PageCountLabel = new System.Windows.Forms.ToolStripLabel();
            this.PreviousButton = new System.Windows.Forms.ToolStripButton();
            this.NextButton = new System.Windows.Forms.ToolStripButton();
            this.HelpSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.HelpInfoButton = new System.Windows.Forms.ToolStripButton();
            this.Statusbar = new System.Windows.Forms.StatusStrip();
            this.MousePositionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.PanelThumbnail = new System.Windows.Forms.Panel();
            this.PanelSplitThumbnailDrillDown = new System.Windows.Forms.Panel();
            this.thumbnailXpressDrillDown = new PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress();
            this.PanelBottomThumb = new System.Windows.Forms.Panel();
            this.thumbnailTextBox = new System.Windows.Forms.TextBox();
            this.ClearThumbButton = new System.Windows.Forms.Button();
            this.MiddleSplitter = new System.Windows.Forms.Splitter();
            this.PanelProperties = new System.Windows.Forms.Panel();
            this.TabRoll = new System.Windows.Forms.TabControl();
            this.InfoTab = new System.Windows.Forms.TabPage();
            this.panelImageInfoGrid = new System.Windows.Forms.Panel();
            this.dataGridImageInfo = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelMetaData = new System.Windows.Forms.Panel();
            this.metaDataButton = new System.Windows.Forms.Button();
            this.ViewTab = new System.Windows.Forms.TabPage();
            this.targetButton = new System.Windows.Forms.Button();
            this.monitorButton = new System.Windows.Forms.Button();
            this.printerProfileButton = new System.Windows.Forms.Button();
            this.PaletteFileButton = new System.Windows.Forms.Button();
            this.zoomOutLabel = new System.Windows.Forms.Label();
            this.zoomOutNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.zoomInLabel = new System.Windows.Forms.Label();
            this.zoomInNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.magHeightLabel = new System.Windows.Forms.Label();
            this.magHeightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.magnificationLabel = new System.Windows.Forms.Label();
            this.magFactorNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.magWidthLabel = new System.Windows.Forms.Label();
            this.magWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.backColorButton = new System.Windows.Forms.Button();
            this.backColorLabel = new System.Windows.Forms.Label();
            this.aspectXLabel = new System.Windows.Forms.Label();
            this.aspectXNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.aspectYLabel = new System.Windows.Forms.Label();
            this.aspectYNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.alphaCheckBox = new System.Windows.Forms.CheckBox();
            this.RenderLabel = new System.Windows.Forms.Label();
            this.renderComboBox = new System.Windows.Forms.ComboBox();
            this.DisplayLabel = new System.Windows.Forms.Label();
            this.displayComboBox = new System.Windows.Forms.ComboBox();
            this.proofCheckBox = new System.Windows.Forms.CheckBox();
            this.ICMCheckBox = new System.Windows.Forms.CheckBox();
            this.PaletteLabel = new System.Windows.Forms.Label();
            this.paletteComboBox = new System.Windows.Forms.ComboBox();
            this.progressiveCheckBox = new System.Windows.Forms.CheckBox();
            this.interceptLabel = new System.Windows.Forms.Label();
            this.SlopeLabel = new System.Windows.Forms.Label();
            this.interceptNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.slopeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.grayComboBox = new System.Windows.Forms.ComboBox();
            this.VerticalComboBox = new System.Windows.Forms.ComboBox();
            this.HorzComboBox = new System.Windows.Forms.ComboBox();
            this.HorzLabel = new System.Windows.Forms.Label();
            this.VerticalLabel = new System.Windows.Forms.Label();
            this.ZoomLabel = new System.Windows.Forms.Label();
            this.zoomNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.smoothCheckBox = new System.Windows.Forms.CheckBox();
            this.preserveCheckBox = new System.Windows.Forms.CheckBox();
            this.ditherCheckBox = new System.Windows.Forms.CheckBox();
            this.antiCheckBox = new System.Windows.Forms.CheckBox();
            this.GrayLabel = new System.Windows.Forms.Label();
            this.ResetButton = new System.Windows.Forms.Button();
            this.BrightValueLabel = new System.Windows.Forms.Label();
            this.ConValueLabel = new System.Windows.Forms.Label();
            this.ContrastLabel = new System.Windows.Forms.Label();
            this.BrightnessLabel = new System.Windows.Forms.Label();
            this.ConBar = new System.Windows.Forms.TrackBar();
            this.BrightBar = new System.Windows.Forms.TrackBar();
            this.ThumbTab = new System.Windows.Forms.TabPage();
            this.threadHungNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.threadHungLabel = new System.Windows.Forms.Label();
            this.threadStartNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.threadStartLabel = new System.Windows.Forms.Label();
            this.hourGlassCheckBox = new System.Windows.Forms.CheckBox();
            this.ErrorActionLabel = new System.Windows.Forms.Label();
            this.errorComboBox = new System.Windows.Forms.ComboBox();
            this.threadCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ThreadCountLabel = new System.Windows.Forms.Label();
            this.preserveBlackCheckBox = new System.Windows.Forms.CheckBox();
            this.selectBackColorButton = new System.Windows.Forms.Button();
            this.BackColorSelectLabel = new System.Windows.Forms.Label();
            this.cameraRawCheckBox = new System.Windows.Forms.CheckBox();
            this.scrollLabel = new System.Windows.Forms.Label();
            this.scrollDirectionComboBox = new System.Windows.Forms.ComboBox();
            this.bitDepthLabel = new System.Windows.Forms.Label();
            this.bitDepthComboBox = new System.Windows.Forms.ComboBox();
            this.textBackColorButton = new System.Windows.Forms.Button();
            this.TextBackColorLabel = new System.Windows.Forms.Label();
            this.placeHoldersCheckBox = new System.Windows.Forms.CheckBox();
            this.fontButton = new System.Windows.Forms.Button();
            this.FontLabel = new System.Windows.Forms.Label();
            this.ThumbnailLabel = new System.Windows.Forms.Label();
            this.thumbComboBox = new System.Windows.Forms.ComboBox();
            this.DescriptorDisplayLabel = new System.Windows.Forms.Label();
            this.descriptorDisplayComboBox = new System.Windows.Forms.ComboBox();
            this.HozAlignLabel = new System.Windows.Forms.Label();
            this.descriptorHorzComboBox = new System.Windows.Forms.ComboBox();
            this.DescriptorVLabel = new System.Windows.Forms.Label();
            this.descriptorVertComboBox = new System.Windows.Forms.ComboBox();
            this.VerticalSpacingLabel = new System.Windows.Forms.Label();
            this.vertSpacingNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.CellHorzLabel = new System.Windows.Forms.Label();
            this.horzSpacingNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.borderButton = new System.Windows.Forms.Button();
            this.BoderColorLabel = new System.Windows.Forms.Label();
            this.SpacingButton = new System.Windows.Forms.Button();
            this.SpacingColorLabel = new System.Windows.Forms.Label();
            this.borderWidthLabel = new System.Windows.Forms.Label();
            this.borderWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.heightLabel = new System.Windows.Forms.Label();
            this.cellHeightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.widthLabel = new System.Windows.Forms.Label();
            this.cellWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.boderThumbLabel = new System.Windows.Forms.Label();
            this.ColorTab = new System.Windows.Forms.TabPage();
            this.PaletteButtonLabel = new System.Windows.Forms.Label();
            this.paletteButton = new System.Windows.Forms.Button();
            this.resetComponentsButton = new System.Windows.Forms.Button();
            this.componentsLabel = new System.Windows.Forms.Label();
            this.cmykGroupBox = new System.Windows.Forms.GroupBox();
            this.blackRadioButton = new System.Windows.Forms.RadioButton();
            this.yellowRadioButton = new System.Windows.Forms.RadioButton();
            this.magentaRadioButton = new System.Windows.Forms.RadioButton();
            this.cyanRadioButton = new System.Windows.Forms.RadioButton();
            this.hslGroupBox = new System.Windows.Forms.GroupBox();
            this.luminanceRadioButton = new System.Windows.Forms.RadioButton();
            this.saturationRadioButton = new System.Windows.Forms.RadioButton();
            this.hueRadioButton = new System.Windows.Forms.RadioButton();
            this.rgbaGroupBox = new System.Windows.Forms.GroupBox();
            this.alphaRadioButton = new System.Windows.Forms.RadioButton();
            this.blueRadioButton = new System.Windows.Forms.RadioButton();
            this.greenRadioButton = new System.Windows.Forms.RadioButton();
            this.redRadioButton = new System.Windows.Forms.RadioButton();
            this.histogramLabel = new System.Windows.Forms.Label();
            this.comboBoxHistogram = new System.Windows.Forms.ComboBox();
            this.imageXViewHistogram = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.LayersCadTab = new System.Windows.Forms.TabPage();
            this.SelectButton = new System.Windows.Forms.Button();
            this.LayersLabel = new System.Windows.Forms.Label();
            this.ThumbnailSplitter = new System.Windows.Forms.Splitter();
            this.PanelPaging = new System.Windows.Forms.Panel();
            this.thumbnailXpressPaging = new PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress();
            this.PanelView = new System.Windows.Forms.Panel();
            this.imageXView = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.SplitterView = new System.Windows.Forms.Splitter();
            this.notateXpress1 = new PegasusImaging.WinForms.NotateXpress9.NotateXpress(this.components);
            this.pdfXpress1 = new PegasusImaging.WinForms.PdfXpress2.PdfXpress();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.twainPro1 = new PegasusImaging.WinForms.TwainPro5.TwainPro(this.components);
            this.printPro1 = new PegasusImaging.WinForms.PrintPro4.PrintPro(this.components);
            this.MainMenu.SuspendLayout();
            this.Toolbar.SuspendLayout();
            this.Statusbar.SuspendLayout();
            this.PanelThumbnail.SuspendLayout();
            this.PanelSplitThumbnailDrillDown.SuspendLayout();
            this.PanelBottomThumb.SuspendLayout();
            this.PanelProperties.SuspendLayout();
            this.TabRoll.SuspendLayout();
            this.InfoTab.SuspendLayout();
            this.panelImageInfoGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridImageInfo)).BeginInit();
            this.panelMetaData.SuspendLayout();
            this.ViewTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomOutNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomInNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magHeightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magFactorNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.magWidthNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectXNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectYNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interceptNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slopeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BrightBar)).BeginInit();
            this.ThumbTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threadHungNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threadStartNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threadCountNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vertSpacingNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horzSpacingNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderWidthNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellHeightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellWidthNumericUpDown)).BeginInit();
            this.ColorTab.SuspendLayout();
            this.cmykGroupBox.SuspendLayout();
            this.hslGroupBox.SuspendLayout();
            this.rgbaGroupBox.SuspendLayout();
            this.LayersCadTab.SuspendLayout();
            this.PanelPaging.SuspendLayout();
            this.PanelView.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.BackColor = System.Drawing.SystemColors.Control;
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.documentCleanupToolStripMenuItem,
            this.photoEnhancementToolStripMenuItem,
            this.filtersAndEffectsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(1169, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.printImageToolStripMenuItem,
            this.scanImageToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.loadAnnotationsToolStripMenuItem,
            this.saveAnnotationsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileOpenToolStripMenuItem,
            this.fileWithLoadOptionsToolStripMenuItem,
            this.directoryOpenToolStripMenuItem,
            this.rawFileOpenToolStripMenuItem,
            this.sampleImagesToolStripMenuItem});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.openToolStripMenuItem.Text = "&Open";
            // 
            // fileOpenToolStripMenuItem
            // 
            this.fileOpenToolStripMenuItem.Name = "fileOpenToolStripMenuItem";
            this.fileOpenToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.fileOpenToolStripMenuItem.Text = "&File";
            this.fileOpenToolStripMenuItem.Click += new System.EventHandler(this.fileOpenToolStripMenuItem_Click);
            // 
            // fileWithLoadOptionsToolStripMenuItem
            // 
            this.fileWithLoadOptionsToolStripMenuItem.Name = "fileWithLoadOptionsToolStripMenuItem";
            this.fileWithLoadOptionsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.fileWithLoadOptionsToolStripMenuItem.Text = "File With Load Options";
            this.fileWithLoadOptionsToolStripMenuItem.Click += new System.EventHandler(this.fileWithLoadOptionsToolStripMenuItem_Click);
            // 
            // directoryOpenToolStripMenuItem
            // 
            this.directoryOpenToolStripMenuItem.Name = "directoryOpenToolStripMenuItem";
            this.directoryOpenToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.directoryOpenToolStripMenuItem.Text = "&Directory";
            this.directoryOpenToolStripMenuItem.Click += new System.EventHandler(this.directoryOpenToolStripMenuItem_Click);
            // 
            // rawFileOpenToolStripMenuItem
            // 
            this.rawFileOpenToolStripMenuItem.Name = "rawFileOpenToolStripMenuItem";
            this.rawFileOpenToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.rawFileOpenToolStripMenuItem.Text = "&Raw File";
            this.rawFileOpenToolStripMenuItem.Click += new System.EventHandler(this.rawFileOpenToolStripMenuItem_Click);
            // 
            // sampleImagesToolStripMenuItem
            // 
            this.sampleImagesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.documentCleanupToolStripMenuItem1,
            this.photoEnhancementToolStripMenuItem1,
            this.effectsToolStripMenuItem});
            this.sampleImagesToolStripMenuItem.Name = "sampleImagesToolStripMenuItem";
            this.sampleImagesToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.sampleImagesToolStripMenuItem.Text = "&Sample Images";
            // 
            // documentCleanupToolStripMenuItem1
            // 
            this.documentCleanupToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.binarizeToolStripMenuItem,
            this.blobRemovalToolStripMenuItem,
            this.borderDetectToolStripMenuItem,
            this.deskewToolStripMenuItem1,
            this.despeckleToolStripMenuItem1,
            this.dilateToolStripMenuItem2,
            this.erodeToolStripMenuItem2,
            this.multiPageToolStripMenuItem,
            this.negateToolStripMenuItem,
            this.removeLinesToolStripMenuItem,
            this.shearToolStripMenuItem,
            this.smoothZoomToolStripMenuItem,
            this.tagsToolStripMenuItem1});
            this.documentCleanupToolStripMenuItem1.Name = "documentCleanupToolStripMenuItem1";
            this.documentCleanupToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.documentCleanupToolStripMenuItem1.Text = "Document Cleanup";
            // 
            // binarizeToolStripMenuItem
            // 
            this.binarizeToolStripMenuItem.Name = "binarizeToolStripMenuItem";
            this.binarizeToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.binarizeToolStripMenuItem.Text = "Binarize";
            this.binarizeToolStripMenuItem.Click += new System.EventHandler(this.binarizeToolStripMenuItem1_Click);
            // 
            // blobRemovalToolStripMenuItem
            // 
            this.blobRemovalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blobRemoval1ToolStripMenuItem,
            this.blobRemoval2ToolStripMenuItem});
            this.blobRemovalToolStripMenuItem.Name = "blobRemovalToolStripMenuItem";
            this.blobRemovalToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.blobRemovalToolStripMenuItem.Text = "Blob Removal";
            // 
            // blobRemoval1ToolStripMenuItem
            // 
            this.blobRemoval1ToolStripMenuItem.Name = "blobRemoval1ToolStripMenuItem";
            this.blobRemoval1ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.blobRemoval1ToolStripMenuItem.Text = "Blob Removal 1";
            this.blobRemoval1ToolStripMenuItem.Click += new System.EventHandler(this.blobRemoval1ToolStripMenuItem_Click);
            // 
            // blobRemoval2ToolStripMenuItem
            // 
            this.blobRemoval2ToolStripMenuItem.Name = "blobRemoval2ToolStripMenuItem";
            this.blobRemoval2ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.blobRemoval2ToolStripMenuItem.Text = "Blob Removal 2";
            this.blobRemoval2ToolStripMenuItem.Click += new System.EventHandler(this.blobRemoval2ToolStripMenuItem_Click);
            // 
            // borderDetectToolStripMenuItem
            // 
            this.borderDetectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.borderCrop1ToolStripMenuItem,
            this.borderCrop2ToolStripMenuItem});
            this.borderDetectToolStripMenuItem.Name = "borderDetectToolStripMenuItem";
            this.borderDetectToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.borderDetectToolStripMenuItem.Text = "Border Crop";
            // 
            // borderCrop1ToolStripMenuItem
            // 
            this.borderCrop1ToolStripMenuItem.Name = "borderCrop1ToolStripMenuItem";
            this.borderCrop1ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.borderCrop1ToolStripMenuItem.Text = "Border Crop 1";
            this.borderCrop1ToolStripMenuItem.Click += new System.EventHandler(this.borderCrop1ToolStripMenuItem_Click);
            // 
            // borderCrop2ToolStripMenuItem
            // 
            this.borderCrop2ToolStripMenuItem.Name = "borderCrop2ToolStripMenuItem";
            this.borderCrop2ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.borderCrop2ToolStripMenuItem.Text = "Border Crop 2";
            this.borderCrop2ToolStripMenuItem.Click += new System.EventHandler(this.borderCrop2ToolStripMenuItem_Click);
            // 
            // deskewToolStripMenuItem1
            // 
            this.deskewToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deskew1ToolStripMenuItem,
            this.deskew2ToolStripMenuItem,
            this.deskew3ToolStripMenuItem});
            this.deskewToolStripMenuItem1.Name = "deskewToolStripMenuItem1";
            this.deskewToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.deskewToolStripMenuItem1.Text = "Deskew";
            // 
            // deskew1ToolStripMenuItem
            // 
            this.deskew1ToolStripMenuItem.Name = "deskew1ToolStripMenuItem";
            this.deskew1ToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.deskew1ToolStripMenuItem.Text = "Deskew 1";
            this.deskew1ToolStripMenuItem.Click += new System.EventHandler(this.deskew1ToolStripMenuItem_Click);
            // 
            // deskew2ToolStripMenuItem
            // 
            this.deskew2ToolStripMenuItem.Name = "deskew2ToolStripMenuItem";
            this.deskew2ToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.deskew2ToolStripMenuItem.Text = "Deskew 2";
            this.deskew2ToolStripMenuItem.Click += new System.EventHandler(this.deskew2ToolStripMenuItem_Click);
            // 
            // deskew3ToolStripMenuItem
            // 
            this.deskew3ToolStripMenuItem.Name = "deskew3ToolStripMenuItem";
            this.deskew3ToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.deskew3ToolStripMenuItem.Text = "Deskew 3";
            this.deskew3ToolStripMenuItem.Click += new System.EventHandler(this.deskew3ToolStripMenuItem_Click);
            // 
            // despeckleToolStripMenuItem1
            // 
            this.despeckleToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.despeckle1ToolStripMenuItem,
            this.despeckle2ToolStripMenuItem,
            this.despeckle3ToolStripMenuItem});
            this.despeckleToolStripMenuItem1.Name = "despeckleToolStripMenuItem1";
            this.despeckleToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.despeckleToolStripMenuItem1.Text = "Despeckle";
            // 
            // despeckle1ToolStripMenuItem
            // 
            this.despeckle1ToolStripMenuItem.Name = "despeckle1ToolStripMenuItem";
            this.despeckle1ToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.despeckle1ToolStripMenuItem.Text = "Despeckle 1";
            this.despeckle1ToolStripMenuItem.Click += new System.EventHandler(this.despeckle1ToolStripMenuItem_Click);
            // 
            // despeckle2ToolStripMenuItem
            // 
            this.despeckle2ToolStripMenuItem.Name = "despeckle2ToolStripMenuItem";
            this.despeckle2ToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.despeckle2ToolStripMenuItem.Text = "Despeckle 2";
            this.despeckle2ToolStripMenuItem.Click += new System.EventHandler(this.despeckle2ToolStripMenuItem_Click);
            // 
            // despeckle3ToolStripMenuItem
            // 
            this.despeckle3ToolStripMenuItem.Name = "despeckle3ToolStripMenuItem";
            this.despeckle3ToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.despeckle3ToolStripMenuItem.Text = "Despeckle 3";
            this.despeckle3ToolStripMenuItem.Click += new System.EventHandler(this.despeckle3ToolStripMenuItem_Click);
            // 
            // dilateToolStripMenuItem2
            // 
            this.dilateToolStripMenuItem2.Name = "dilateToolStripMenuItem2";
            this.dilateToolStripMenuItem2.Size = new System.Drawing.Size(149, 22);
            this.dilateToolStripMenuItem2.Text = "Dilate";
            this.dilateToolStripMenuItem2.Click += new System.EventHandler(this.dilateToolStripMenuItem2_Click);
            // 
            // erodeToolStripMenuItem2
            // 
            this.erodeToolStripMenuItem2.Name = "erodeToolStripMenuItem2";
            this.erodeToolStripMenuItem2.Size = new System.Drawing.Size(149, 22);
            this.erodeToolStripMenuItem2.Text = "Erode";
            this.erodeToolStripMenuItem2.Click += new System.EventHandler(this.erodeToolStripMenuItem2_Click);
            // 
            // multiPageToolStripMenuItem
            // 
            this.multiPageToolStripMenuItem.Name = "multiPageToolStripMenuItem";
            this.multiPageToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.multiPageToolStripMenuItem.Text = "MultiPage";
            this.multiPageToolStripMenuItem.Click += new System.EventHandler(this.multiPageToolStripMenuItem_Click);
            // 
            // negateToolStripMenuItem
            // 
            this.negateToolStripMenuItem.Name = "negateToolStripMenuItem";
            this.negateToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.negateToolStripMenuItem.Text = "Negate";
            this.negateToolStripMenuItem.Click += new System.EventHandler(this.negateToolStripMenuItem_Click_1);
            // 
            // removeLinesToolStripMenuItem
            // 
            this.removeLinesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeLines1ToolStripMenuItem,
            this.removeLines2ToolStripMenuItem});
            this.removeLinesToolStripMenuItem.Name = "removeLinesToolStripMenuItem";
            this.removeLinesToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.removeLinesToolStripMenuItem.Text = "Line Removal";
            // 
            // removeLines1ToolStripMenuItem
            // 
            this.removeLines1ToolStripMenuItem.Name = "removeLines1ToolStripMenuItem";
            this.removeLines1ToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.removeLines1ToolStripMenuItem.Text = "Remove Lines 1";
            this.removeLines1ToolStripMenuItem.Click += new System.EventHandler(this.removeLines1ToolStripMenuItem_Click);
            // 
            // removeLines2ToolStripMenuItem
            // 
            this.removeLines2ToolStripMenuItem.Name = "removeLines2ToolStripMenuItem";
            this.removeLines2ToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.removeLines2ToolStripMenuItem.Text = "Remove Lines 2";
            this.removeLines2ToolStripMenuItem.Click += new System.EventHandler(this.removeLines2ToolStripMenuItem_Click);
            // 
            // shearToolStripMenuItem
            // 
            this.shearToolStripMenuItem.Name = "shearToolStripMenuItem";
            this.shearToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.shearToolStripMenuItem.Text = "Shear";
            this.shearToolStripMenuItem.Click += new System.EventHandler(this.shearToolStripMenuItem_Click_1);
            // 
            // smoothZoomToolStripMenuItem
            // 
            this.smoothZoomToolStripMenuItem.Name = "smoothZoomToolStripMenuItem";
            this.smoothZoomToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.smoothZoomToolStripMenuItem.Text = "Smooth && Zoom";
            this.smoothZoomToolStripMenuItem.Click += new System.EventHandler(this.smoothZoomToolStripMenuItem_Click_1);
            // 
            // tagsToolStripMenuItem1
            // 
            this.tagsToolStripMenuItem1.Name = "tagsToolStripMenuItem1";
            this.tagsToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.tagsToolStripMenuItem1.Text = "Tags";
            this.tagsToolStripMenuItem1.Click += new System.EventHandler(this.tagsToolStripMenuItem1_Click);
            // 
            // photoEnhancementToolStripMenuItem1
            // 
            this.photoEnhancementToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alphaToolStripMenuItem,
            this.colorBalanceToolStripMenuItem1,
            this.contrastToolStripMenuItem,
            this.cropToolStripMenuItem,
            this.eXIFToolStripMenuItem,
            this.orientationToolStripMenuItem1,
            this.redEyeToolStripMenuItem,
            this.removeDustToolStripMenuItem,
            this.scratchedToolStripMenuItem1,
            this.sharpenToolStripMenuItem});
            this.photoEnhancementToolStripMenuItem1.Name = "photoEnhancementToolStripMenuItem1";
            this.photoEnhancementToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.photoEnhancementToolStripMenuItem1.Text = "Photo Enhancement";
            // 
            // alphaToolStripMenuItem
            // 
            this.alphaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alpha1ToolStripMenuItem1,
            this.alpha2ToolStripMenuItem1,
            this.alpha3ToolStripMenuItem1});
            this.alphaToolStripMenuItem.Name = "alphaToolStripMenuItem";
            this.alphaToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.alphaToolStripMenuItem.Text = "Alpha";
            // 
            // alpha1ToolStripMenuItem1
            // 
            this.alpha1ToolStripMenuItem1.Name = "alpha1ToolStripMenuItem1";
            this.alpha1ToolStripMenuItem1.Size = new System.Drawing.Size(110, 22);
            this.alpha1ToolStripMenuItem1.Text = "Alpha 1";
            this.alpha1ToolStripMenuItem1.Click += new System.EventHandler(this.alpha1ToolStripMenuItem1_Click);
            // 
            // alpha2ToolStripMenuItem1
            // 
            this.alpha2ToolStripMenuItem1.Name = "alpha2ToolStripMenuItem1";
            this.alpha2ToolStripMenuItem1.Size = new System.Drawing.Size(110, 22);
            this.alpha2ToolStripMenuItem1.Text = "Alpha 2";
            this.alpha2ToolStripMenuItem1.Click += new System.EventHandler(this.alpha2ToolStripMenuItem1_Click);
            // 
            // alpha3ToolStripMenuItem1
            // 
            this.alpha3ToolStripMenuItem1.Name = "alpha3ToolStripMenuItem1";
            this.alpha3ToolStripMenuItem1.Size = new System.Drawing.Size(110, 22);
            this.alpha3ToolStripMenuItem1.Text = "Alpha 3";
            this.alpha3ToolStripMenuItem1.Click += new System.EventHandler(this.alpha3ToolStripMenuItem1_Click);
            // 
            // colorBalanceToolStripMenuItem1
            // 
            this.colorBalanceToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoBalance1ToolStripMenuItem,
            this.autoBalance2ToolStripMenuItem,
            this.autoLevel1ToolStripMenuItem,
            this.autoLevel2ToolStripMenuItem});
            this.colorBalanceToolStripMenuItem1.Name = "colorBalanceToolStripMenuItem1";
            this.colorBalanceToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.colorBalanceToolStripMenuItem1.Text = "Color Balance";
            // 
            // autoBalance1ToolStripMenuItem
            // 
            this.autoBalance1ToolStripMenuItem.Name = "autoBalance1ToolStripMenuItem";
            this.autoBalance1ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.autoBalance1ToolStripMenuItem.Text = "Auto Balance 1";
            this.autoBalance1ToolStripMenuItem.Click += new System.EventHandler(this.autoBalance1ToolStripMenuItem_Click);
            // 
            // autoBalance2ToolStripMenuItem
            // 
            this.autoBalance2ToolStripMenuItem.Name = "autoBalance2ToolStripMenuItem";
            this.autoBalance2ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.autoBalance2ToolStripMenuItem.Text = "Auto Balance 2";
            this.autoBalance2ToolStripMenuItem.Click += new System.EventHandler(this.autoBalance2ToolStripMenuItem_Click);
            // 
            // autoLevel1ToolStripMenuItem
            // 
            this.autoLevel1ToolStripMenuItem.Name = "autoLevel1ToolStripMenuItem";
            this.autoLevel1ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.autoLevel1ToolStripMenuItem.Text = "Auto Level 1";
            this.autoLevel1ToolStripMenuItem.Click += new System.EventHandler(this.autoLevel2ToolStripMenuItem_Click);
            // 
            // autoLevel2ToolStripMenuItem
            // 
            this.autoLevel2ToolStripMenuItem.Name = "autoLevel2ToolStripMenuItem";
            this.autoLevel2ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.autoLevel2ToolStripMenuItem.Text = "Auto Level 2";
            this.autoLevel2ToolStripMenuItem.Click += new System.EventHandler(this.autoLevel3ToolStripMenuItem_Click);
            // 
            // contrastToolStripMenuItem
            // 
            this.contrastToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoContrastToolStripMenuItem,
            this.autoLightnessToolStripMenuItem});
            this.contrastToolStripMenuItem.Name = "contrastToolStripMenuItem";
            this.contrastToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.contrastToolStripMenuItem.Text = "Contrast";
            // 
            // autoContrastToolStripMenuItem
            // 
            this.autoContrastToolStripMenuItem.Name = "autoContrastToolStripMenuItem";
            this.autoContrastToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.autoContrastToolStripMenuItem.Text = "Auto Contrast";
            this.autoContrastToolStripMenuItem.Click += new System.EventHandler(this.autoContrastToolStripMenuItem_Click_1);
            // 
            // autoLightnessToolStripMenuItem
            // 
            this.autoLightnessToolStripMenuItem.Name = "autoLightnessToolStripMenuItem";
            this.autoLightnessToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.autoLightnessToolStripMenuItem.Text = "Auto Lightness";
            this.autoLightnessToolStripMenuItem.Click += new System.EventHandler(this.autoLightness2ToolStripMenuItem_Click);
            // 
            // cropToolStripMenuItem
            // 
            this.cropToolStripMenuItem.Name = "cropToolStripMenuItem";
            this.cropToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cropToolStripMenuItem.Text = "Crop";
            this.cropToolStripMenuItem.Click += new System.EventHandler(this.cropToolStripMenuItem_Click_1);
            // 
            // eXIFToolStripMenuItem
            // 
            this.eXIFToolStripMenuItem.Name = "eXIFToolStripMenuItem";
            this.eXIFToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.eXIFToolStripMenuItem.Text = "EXIF";
            this.eXIFToolStripMenuItem.Click += new System.EventHandler(this.eXIFToolStripMenuItem_Click);
            // 
            // orientationToolStripMenuItem1
            // 
            this.orientationToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rotate1ToolStripMenuItem,
            this.rotate2ToolStripMenuItem});
            this.orientationToolStripMenuItem1.Name = "orientationToolStripMenuItem1";
            this.orientationToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.orientationToolStripMenuItem1.Text = "Orientation";
            // 
            // rotate1ToolStripMenuItem
            // 
            this.rotate1ToolStripMenuItem.Name = "rotate1ToolStripMenuItem";
            this.rotate1ToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.rotate1ToolStripMenuItem.Text = "Rotate 1";
            this.rotate1ToolStripMenuItem.Click += new System.EventHandler(this.rotate1ToolStripMenuItem_Click);
            // 
            // rotate2ToolStripMenuItem
            // 
            this.rotate2ToolStripMenuItem.Name = "rotate2ToolStripMenuItem";
            this.rotate2ToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.rotate2ToolStripMenuItem.Text = "Rotate 2";
            this.rotate2ToolStripMenuItem.Click += new System.EventHandler(this.rotate2ToolStripMenuItem_Click);
            // 
            // redEyeToolStripMenuItem
            // 
            this.redEyeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redEye1ToolStripMenuItem1,
            this.redEye2ToolStripMenuItem1,
            this.redEye3ToolStripMenuItem1,
            this.redEye4ToolStripMenuItem1});
            this.redEyeToolStripMenuItem.Name = "redEyeToolStripMenuItem";
            this.redEyeToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.redEyeToolStripMenuItem.Text = "Red Eye Removal";
            // 
            // redEye1ToolStripMenuItem1
            // 
            this.redEye1ToolStripMenuItem1.Name = "redEye1ToolStripMenuItem1";
            this.redEye1ToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.redEye1ToolStripMenuItem1.Text = "Remove Red Eye 1";
            this.redEye1ToolStripMenuItem1.Click += new System.EventHandler(this.redEye1ToolStripMenuItem1_Click);
            // 
            // redEye2ToolStripMenuItem1
            // 
            this.redEye2ToolStripMenuItem1.Name = "redEye2ToolStripMenuItem1";
            this.redEye2ToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.redEye2ToolStripMenuItem1.Text = "Remove Red Eye 2";
            this.redEye2ToolStripMenuItem1.Click += new System.EventHandler(this.redEye2ToolStripMenuItem1_Click);
            // 
            // redEye3ToolStripMenuItem1
            // 
            this.redEye3ToolStripMenuItem1.Name = "redEye3ToolStripMenuItem1";
            this.redEye3ToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.redEye3ToolStripMenuItem1.Text = "Remove Red Eye 3";
            this.redEye3ToolStripMenuItem1.Click += new System.EventHandler(this.redEye3ToolStripMenuItem1_Click);
            // 
            // redEye4ToolStripMenuItem1
            // 
            this.redEye4ToolStripMenuItem1.Name = "redEye4ToolStripMenuItem1";
            this.redEye4ToolStripMenuItem1.Size = new System.Drawing.Size(165, 22);
            this.redEye4ToolStripMenuItem1.Text = "Remove Red Eye 4";
            this.redEye4ToolStripMenuItem1.Click += new System.EventHandler(this.redEye4ToolStripMenuItem1_Click);
            // 
            // removeDustToolStripMenuItem
            // 
            this.removeDustToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeDust1ToolStripMenuItem,
            this.removeDust2ToolStripMenuItem});
            this.removeDustToolStripMenuItem.Name = "removeDustToolStripMenuItem";
            this.removeDustToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.removeDustToolStripMenuItem.Text = "Remove Dust";
            // 
            // removeDust1ToolStripMenuItem
            // 
            this.removeDust1ToolStripMenuItem.Name = "removeDust1ToolStripMenuItem";
            this.removeDust1ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.removeDust1ToolStripMenuItem.Text = "Remove Dust 1";
            this.removeDust1ToolStripMenuItem.Click += new System.EventHandler(this.removeDust1ToolStripMenuItem_Click);
            // 
            // removeDust2ToolStripMenuItem
            // 
            this.removeDust2ToolStripMenuItem.Name = "removeDust2ToolStripMenuItem";
            this.removeDust2ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.removeDust2ToolStripMenuItem.Text = "Remove Dust 2";
            this.removeDust2ToolStripMenuItem.Click += new System.EventHandler(this.removeDust2ToolStripMenuItem_Click);
            // 
            // scratchedToolStripMenuItem1
            // 
            this.scratchedToolStripMenuItem1.Name = "scratchedToolStripMenuItem1";
            this.scratchedToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.scratchedToolStripMenuItem1.Text = "Remove Scracthes";
            this.scratchedToolStripMenuItem1.Click += new System.EventHandler(this.scratchedToolStripMenuItem1_Click);
            // 
            // sharpenToolStripMenuItem
            // 
            this.sharpenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sharpen1ToolStripMenuItem,
            this.sharpen2ToolStripMenuItem});
            this.sharpenToolStripMenuItem.Name = "sharpenToolStripMenuItem";
            this.sharpenToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.sharpenToolStripMenuItem.Text = "Sharpen";
            // 
            // sharpen1ToolStripMenuItem
            // 
            this.sharpen1ToolStripMenuItem.Name = "sharpen1ToolStripMenuItem";
            this.sharpen1ToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.sharpen1ToolStripMenuItem.Text = "Sharpen 1";
            this.sharpen1ToolStripMenuItem.Click += new System.EventHandler(this.sharpen1ToolStripMenuItem_Click);
            // 
            // sharpen2ToolStripMenuItem
            // 
            this.sharpen2ToolStripMenuItem.Name = "sharpen2ToolStripMenuItem";
            this.sharpen2ToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.sharpen2ToolStripMenuItem.Text = "Sharpen 2";
            this.sharpen2ToolStripMenuItem.Click += new System.EventHandler(this.sharpen2ToolStripMenuItem_Click);
            // 
            // effectsToolStripMenuItem
            // 
            this.effectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.effects1ToolStripMenuItem,
            this.effects2ToolStripMenuItem,
            this.effects3ToolStripMenuItem});
            this.effectsToolStripMenuItem.Name = "effectsToolStripMenuItem";
            this.effectsToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.effectsToolStripMenuItem.Text = "Effects";
            // 
            // effects1ToolStripMenuItem
            // 
            this.effects1ToolStripMenuItem.Name = "effects1ToolStripMenuItem";
            this.effects1ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.effects1ToolStripMenuItem.Text = "Effects 1";
            this.effects1ToolStripMenuItem.Click += new System.EventHandler(this.effects1ToolStripMenuItem_Click);
            // 
            // effects2ToolStripMenuItem
            // 
            this.effects2ToolStripMenuItem.Name = "effects2ToolStripMenuItem";
            this.effects2ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.effects2ToolStripMenuItem.Text = "Effects 2";
            this.effects2ToolStripMenuItem.Click += new System.EventHandler(this.effects2ToolStripMenuItem_Click);
            // 
            // effects3ToolStripMenuItem
            // 
            this.effects3ToolStripMenuItem.Name = "effects3ToolStripMenuItem";
            this.effects3ToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.effects3ToolStripMenuItem.Text = "Effects 3";
            this.effects3ToolStripMenuItem.Click += new System.EventHandler(this.effects3ToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Enabled = false;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // printImageToolStripMenuItem
            // 
            this.printImageToolStripMenuItem.Enabled = false;
            this.printImageToolStripMenuItem.Name = "printImageToolStripMenuItem";
            this.printImageToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.printImageToolStripMenuItem.Text = "&Print Image";
            this.printImageToolStripMenuItem.Click += new System.EventHandler(this.printImageToolStripMenuItem_Click);
            // 
            // scanImageToolStripMenuItem
            // 
            this.scanImageToolStripMenuItem.Name = "scanImageToolStripMenuItem";
            this.scanImageToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.scanImageToolStripMenuItem.Text = "&Scan Image";
            this.scanImageToolStripMenuItem.Click += new System.EventHandler(this.scanImageToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveasNoOptionsToolStripMenuItem,
            this.withSaveOptionsToolStripMenuItem});
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.saveAsToolStripMenuItem.Text = "S&ave as";
            // 
            // saveasNoOptionsToolStripMenuItem
            // 
            this.saveasNoOptionsToolStripMenuItem.Name = "saveasNoOptionsToolStripMenuItem";
            this.saveasNoOptionsToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.saveasNoOptionsToolStripMenuItem.Text = "File";
            this.saveasNoOptionsToolStripMenuItem.Click += new System.EventHandler(this.saveasNoOptionsToolStripMenuItem_Click);
            // 
            // withSaveOptionsToolStripMenuItem
            // 
            this.withSaveOptionsToolStripMenuItem.Name = "withSaveOptionsToolStripMenuItem";
            this.withSaveOptionsToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.withSaveOptionsToolStripMenuItem.Text = "File With Save Options";
            this.withSaveOptionsToolStripMenuItem.Click += new System.EventHandler(this.withSaveOptionsToolStripMenuItem_Click);
            // 
            // loadAnnotationsToolStripMenuItem
            // 
            this.loadAnnotationsToolStripMenuItem.Enabled = false;
            this.loadAnnotationsToolStripMenuItem.Name = "loadAnnotationsToolStripMenuItem";
            this.loadAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.loadAnnotationsToolStripMenuItem.Text = "Load Annotations";
            this.loadAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.loadAnnotationsToolStripMenuItem_Click);
            // 
            // saveAnnotationsToolStripMenuItem
            // 
            this.saveAnnotationsToolStripMenuItem.Enabled = false;
            this.saveAnnotationsToolStripMenuItem.Name = "saveAnnotationsToolStripMenuItem";
            this.saveAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.saveAnnotationsToolStripMenuItem.Text = "Save Annotations";
            this.saveAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.saveAnnotationsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // documentCleanupToolStripMenuItem
            // 
            this.documentCleanupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.binarizeToolStripMenuItem2,
            this.blobRemovalToolStripMenuItem1,
            this.borderCropToolStripMenuItem1,
            this.deskewToolStripMenuItem2,
            this.despeckleToolStripMenuItem2,
            this.dilateToolStripMenuItem1,
            this.erodeToolStripMenuItem1,
            this.lineRemovalToolStripMenuItem1,
            this.negateToolStripMenuItem1,
            this.shearToolStripMenuItem1,
            this.smoothZoomToolStripMenuItem1});
            this.documentCleanupToolStripMenuItem.Enabled = false;
            this.documentCleanupToolStripMenuItem.Name = "documentCleanupToolStripMenuItem";
            this.documentCleanupToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.documentCleanupToolStripMenuItem.Text = "&Document Cleanup";
            // 
            // binarizeToolStripMenuItem2
            // 
            this.binarizeToolStripMenuItem2.Name = "binarizeToolStripMenuItem2";
            this.binarizeToolStripMenuItem2.Size = new System.Drawing.Size(149, 22);
            this.binarizeToolStripMenuItem2.Text = "&Binarize";
            this.binarizeToolStripMenuItem2.Click += new System.EventHandler(this.binarizeToolStripMenuItem_Click);
            // 
            // blobRemovalToolStripMenuItem1
            // 
            this.blobRemovalToolStripMenuItem1.Name = "blobRemovalToolStripMenuItem1";
            this.blobRemovalToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.blobRemovalToolStripMenuItem1.Text = "Blob &Removal";
            this.blobRemovalToolStripMenuItem1.Click += new System.EventHandler(this.blobRemovalToolStripMenuItem_Click);
            // 
            // borderCropToolStripMenuItem1
            // 
            this.borderCropToolStripMenuItem1.Name = "borderCropToolStripMenuItem1";
            this.borderCropToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.borderCropToolStripMenuItem1.Text = "Border &Crop";
            this.borderCropToolStripMenuItem1.Click += new System.EventHandler(this.borderCropToolStripMenuItem_Click);
            // 
            // deskewToolStripMenuItem2
            // 
            this.deskewToolStripMenuItem2.Name = "deskewToolStripMenuItem2";
            this.deskewToolStripMenuItem2.Size = new System.Drawing.Size(149, 22);
            this.deskewToolStripMenuItem2.Text = "&Deskew";
            this.deskewToolStripMenuItem2.Click += new System.EventHandler(this.deskewToolStripMenuItem_Click);
            // 
            // despeckleToolStripMenuItem2
            // 
            this.despeckleToolStripMenuItem2.Name = "despeckleToolStripMenuItem2";
            this.despeckleToolStripMenuItem2.Size = new System.Drawing.Size(149, 22);
            this.despeckleToolStripMenuItem2.Text = "D&especkle";
            this.despeckleToolStripMenuItem2.Click += new System.EventHandler(this.despeckleToolStripMenuItem_Click);
            // 
            // dilateToolStripMenuItem1
            // 
            this.dilateToolStripMenuItem1.Name = "dilateToolStripMenuItem1";
            this.dilateToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.dilateToolStripMenuItem1.Text = "D&ilate";
            this.dilateToolStripMenuItem1.Click += new System.EventHandler(this.dilateToolStripMenuItem_Click);
            // 
            // erodeToolStripMenuItem1
            // 
            this.erodeToolStripMenuItem1.Name = "erodeToolStripMenuItem1";
            this.erodeToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.erodeToolStripMenuItem1.Text = "Er&ode";
            this.erodeToolStripMenuItem1.Click += new System.EventHandler(this.erodeToolStripMenuItem_Click);
            // 
            // lineRemovalToolStripMenuItem1
            // 
            this.lineRemovalToolStripMenuItem1.Name = "lineRemovalToolStripMenuItem1";
            this.lineRemovalToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.lineRemovalToolStripMenuItem1.Text = "&Line Removal";
            this.lineRemovalToolStripMenuItem1.Click += new System.EventHandler(this.lineRemovalToolStripMenuItem_Click);
            // 
            // negateToolStripMenuItem1
            // 
            this.negateToolStripMenuItem1.Name = "negateToolStripMenuItem1";
            this.negateToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.negateToolStripMenuItem1.Text = "&Negate";
            this.negateToolStripMenuItem1.Click += new System.EventHandler(this.negateToolStripMenuItem_Click);
            // 
            // shearToolStripMenuItem1
            // 
            this.shearToolStripMenuItem1.Name = "shearToolStripMenuItem1";
            this.shearToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.shearToolStripMenuItem1.Text = "&Shear";
            this.shearToolStripMenuItem1.Click += new System.EventHandler(this.shearToolStripMenuItem_Click);
            // 
            // smoothZoomToolStripMenuItem1
            // 
            this.smoothZoomToolStripMenuItem1.Name = "smoothZoomToolStripMenuItem1";
            this.smoothZoomToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.smoothZoomToolStripMenuItem1.Text = "Smooth && &Zoom";
            this.smoothZoomToolStripMenuItem1.Click += new System.EventHandler(this.smoothZoomToolStripMenuItem_Click);
            // 
            // photoEnhancementToolStripMenuItem
            // 
            this.photoEnhancementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alphaToolStripMenuItem1,
            this.brightnessToolStripMenuItem1,
            this.cleanupToolStripMenuItem,
            this.colorBalanceToolStripMenuItem,
            this.colorModifyToolStripMenuItem,
            this.contrastToolStripMenuItem1,
            this.cropToolStripMenuItem1,
            this.orientationToolStripMenuItem,
            this.redEyeRemovalToolStripMenuItem,
            this.removeDustToolStripMenuItem1,
            this.removeScratchesToolStripMenuItem1,
            this.resizeToolStripMenuItem2,
            this.sharpenToolStripMenuItem1});
            this.photoEnhancementToolStripMenuItem.Enabled = false;
            this.photoEnhancementToolStripMenuItem.Name = "photoEnhancementToolStripMenuItem";
            this.photoEnhancementToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.photoEnhancementToolStripMenuItem.Text = "&Photo Enhancement";
            // 
            // alphaToolStripMenuItem1
            // 
            this.alphaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.flattenToolStripMenuItem,
            this.mergeToolStripMenuItem1,
            this.reduceToAlphaToolStripMenuItem});
            this.alphaToolStripMenuItem1.Name = "alphaToolStripMenuItem1";
            this.alphaToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.alphaToolStripMenuItem1.Text = "&Alpha";
            // 
            // flattenToolStripMenuItem
            // 
            this.flattenToolStripMenuItem.Name = "flattenToolStripMenuItem";
            this.flattenToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.flattenToolStripMenuItem.Text = "&Flatten";
            this.flattenToolStripMenuItem.Click += new System.EventHandler(this.alphaFlattenToolStripMenuItem_Click);
            // 
            // mergeToolStripMenuItem1
            // 
            this.mergeToolStripMenuItem1.Name = "mergeToolStripMenuItem1";
            this.mergeToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.mergeToolStripMenuItem1.Text = "&Merge";
            this.mergeToolStripMenuItem1.Click += new System.EventHandler(this.alphaMergeToolStripMenuItem_Click);
            // 
            // reduceToAlphaToolStripMenuItem
            // 
            this.reduceToAlphaToolStripMenuItem.Name = "reduceToAlphaToolStripMenuItem";
            this.reduceToAlphaToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.reduceToAlphaToolStripMenuItem.Text = "&Reduce to Alpha";
            this.reduceToAlphaToolStripMenuItem.Click += new System.EventHandler(this.alphaReduceToAlphaToolStripMenuItem_Click);
            // 
            // brightnessToolStripMenuItem1
            // 
            this.brightnessToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.brightnessToolStripMenuItem2,
            this.gammaToolStripMenuItem1});
            this.brightnessToolStripMenuItem1.Name = "brightnessToolStripMenuItem1";
            this.brightnessToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.brightnessToolStripMenuItem1.Text = "&Brightness";
            // 
            // brightnessToolStripMenuItem2
            // 
            this.brightnessToolStripMenuItem2.Name = "brightnessToolStripMenuItem2";
            this.brightnessToolStripMenuItem2.Size = new System.Drawing.Size(124, 22);
            this.brightnessToolStripMenuItem2.Text = "&Brightness";
            this.brightnessToolStripMenuItem2.Click += new System.EventHandler(this.brightnessToolStripMenuItem_Click);
            // 
            // gammaToolStripMenuItem1
            // 
            this.gammaToolStripMenuItem1.Name = "gammaToolStripMenuItem1";
            this.gammaToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.gammaToolStripMenuItem1.Text = "&Gamma";
            this.gammaToolStripMenuItem1.Click += new System.EventHandler(this.gammaToolStripMenuItem_Click);
            // 
            // cleanupToolStripMenuItem
            // 
            this.cleanupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deskewToolStripMenuItem,
            this.despeckleToolStripMenuItem,
            this.dilateToolStripMenuItem,
            this.erodeToolStripMenuItem});
            this.cleanupToolStripMenuItem.Name = "cleanupToolStripMenuItem";
            this.cleanupToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cleanupToolStripMenuItem.Text = "&Clarify";
            // 
            // deskewToolStripMenuItem
            // 
            this.deskewToolStripMenuItem.Name = "deskewToolStripMenuItem";
            this.deskewToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.deskewToolStripMenuItem.Text = "D&eskew";
            this.deskewToolStripMenuItem.Click += new System.EventHandler(this.deskewColorToolStripMenuItem_Click);
            // 
            // despeckleToolStripMenuItem
            // 
            this.despeckleToolStripMenuItem.Name = "despeckleToolStripMenuItem";
            this.despeckleToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.despeckleToolStripMenuItem.Text = "De&speckle";
            this.despeckleToolStripMenuItem.Click += new System.EventHandler(this.despeckleColorToolStripMenuItem_Click);
            // 
            // dilateToolStripMenuItem
            // 
            this.dilateToolStripMenuItem.Name = "dilateToolStripMenuItem";
            this.dilateToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.dilateToolStripMenuItem.Text = "&Dilate";
            this.dilateToolStripMenuItem.Click += new System.EventHandler(this.dilateColorToolStripMenuItem_Click);
            // 
            // erodeToolStripMenuItem
            // 
            this.erodeToolStripMenuItem.Name = "erodeToolStripMenuItem";
            this.erodeToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.erodeToolStripMenuItem.Text = "&Erode";
            this.erodeToolStripMenuItem.Click += new System.EventHandler(this.erodeColorToolStripMenuItem_Click);
            // 
            // colorBalanceToolStripMenuItem
            // 
            this.colorBalanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.colorBalanceAutoToolStripMenuItem,
            this.levelAutoToolStripMenuItem,
            this.colorBalanceManualToolStripMenuItem});
            this.colorBalanceToolStripMenuItem.Name = "colorBalanceToolStripMenuItem";
            this.colorBalanceToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.colorBalanceToolStripMenuItem.Text = "&Color Balance";
            // 
            // colorBalanceAutoToolStripMenuItem
            // 
            this.colorBalanceAutoToolStripMenuItem.Name = "colorBalanceAutoToolStripMenuItem";
            this.colorBalanceAutoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.colorBalanceAutoToolStripMenuItem.Text = "Color Balance (&Auto)";
            this.colorBalanceAutoToolStripMenuItem.Click += new System.EventHandler(this.autoColorBalanceToolStripMenuItem_Click);
            // 
            // levelAutoToolStripMenuItem
            // 
            this.levelAutoToolStripMenuItem.Name = "levelAutoToolStripMenuItem";
            this.levelAutoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.levelAutoToolStripMenuItem.Text = "&Level (Auto)";
            this.levelAutoToolStripMenuItem.Click += new System.EventHandler(this.autoLevelToolStripMenuItem_Click);
            // 
            // colorBalanceManualToolStripMenuItem
            // 
            this.colorBalanceManualToolStripMenuItem.Name = "colorBalanceManualToolStripMenuItem";
            this.colorBalanceManualToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.colorBalanceManualToolStripMenuItem.Text = "Color Balance (&Manual)";
            this.colorBalanceManualToolStripMenuItem.Click += new System.EventHandler(this.adjustColorBalanceToolStripMenuItem_Click);
            // 
            // colorModifyToolStripMenuItem
            // 
            this.colorModifyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adjustHSLToolStripMenuItem1,
            this.adjustRGBToolStripMenuItem1,
            this.colorDepthToolStripMenuItem1,
            this.replaceColorsToolStripMenuItem1});
            this.colorModifyToolStripMenuItem.Name = "colorModifyToolStripMenuItem";
            this.colorModifyToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.colorModifyToolStripMenuItem.Text = "Color &Modify";
            // 
            // adjustHSLToolStripMenuItem1
            // 
            this.adjustHSLToolStripMenuItem1.Name = "adjustHSLToolStripMenuItem1";
            this.adjustHSLToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.adjustHSLToolStripMenuItem1.Text = "&Adjust HSL";
            this.adjustHSLToolStripMenuItem1.Click += new System.EventHandler(this.adjustHSLToolStripMenuItem_Click);
            // 
            // adjustRGBToolStripMenuItem1
            // 
            this.adjustRGBToolStripMenuItem1.Name = "adjustRGBToolStripMenuItem1";
            this.adjustRGBToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.adjustRGBToolStripMenuItem1.Text = "Adjust &RGB";
            this.adjustRGBToolStripMenuItem1.Click += new System.EventHandler(this.adjustRGBToolStripMenuItem_Click);
            // 
            // colorDepthToolStripMenuItem1
            // 
            this.colorDepthToolStripMenuItem1.Name = "colorDepthToolStripMenuItem1";
            this.colorDepthToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.colorDepthToolStripMenuItem1.Text = "Color &Depth";
            this.colorDepthToolStripMenuItem1.Click += new System.EventHandler(this.colorDepthToolStripMenuItem_Click);
            // 
            // replaceColorsToolStripMenuItem1
            // 
            this.replaceColorsToolStripMenuItem1.Name = "replaceColorsToolStripMenuItem1";
            this.replaceColorsToolStripMenuItem1.Size = new System.Drawing.Size(145, 22);
            this.replaceColorsToolStripMenuItem1.Text = "&Replace Colors";
            this.replaceColorsToolStripMenuItem1.Click += new System.EventHandler(this.replaceColorsToolStripMenuItem_Click);
            // 
            // contrastToolStripMenuItem1
            // 
            this.contrastToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoToolStripMenuItem2,
            this.manualToolStripMenuItem2,
            this.equalizeToolStripMenuItem,
            this.lightnessToolStripMenuItem,
            this.lightnessManualToolStripMenuItem});
            this.contrastToolStripMenuItem1.Name = "contrastToolStripMenuItem1";
            this.contrastToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.contrastToolStripMenuItem1.Text = "Co&ntrast";
            // 
            // autoToolStripMenuItem2
            // 
            this.autoToolStripMenuItem2.Name = "autoToolStripMenuItem2";
            this.autoToolStripMenuItem2.Size = new System.Drawing.Size(164, 22);
            this.autoToolStripMenuItem2.Text = "&Contrast (Auto)";
            this.autoToolStripMenuItem2.Click += new System.EventHandler(this.autoContrastToolStripMenuItem_Click);
            // 
            // manualToolStripMenuItem2
            // 
            this.manualToolStripMenuItem2.Name = "manualToolStripMenuItem2";
            this.manualToolStripMenuItem2.Size = new System.Drawing.Size(164, 22);
            this.manualToolStripMenuItem2.Text = "C&ontrast (Manual)";
            this.manualToolStripMenuItem2.Click += new System.EventHandler(this.contrastToolStripMenuItem_Click);
            // 
            // equalizeToolStripMenuItem
            // 
            this.equalizeToolStripMenuItem.Name = "equalizeToolStripMenuItem";
            this.equalizeToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.equalizeToolStripMenuItem.Text = "&Equalize";
            this.equalizeToolStripMenuItem.Click += new System.EventHandler(this.equalizeToolStripMenuItem_Click);
            // 
            // lightnessToolStripMenuItem
            // 
            this.lightnessToolStripMenuItem.Name = "lightnessToolStripMenuItem";
            this.lightnessToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.lightnessToolStripMenuItem.Text = "Lightness (&Auto)";
            this.lightnessToolStripMenuItem.Click += new System.EventHandler(this.autoLightnessToolStripMenuItem_Click);
            // 
            // lightnessManualToolStripMenuItem
            // 
            this.lightnessManualToolStripMenuItem.Name = "lightnessManualToolStripMenuItem";
            this.lightnessManualToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.lightnessManualToolStripMenuItem.Text = "Lightness (&Manual)";
            this.lightnessManualToolStripMenuItem.Click += new System.EventHandler(this.adjustLightnessToolStripMenuItem_Click);
            // 
            // cropToolStripMenuItem1
            // 
            this.cropToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cropToolStripMenuItem2,
            this.manualToolStripMenuItem1,
            this.cropBroderToolStripMenuItem});
            this.cropToolStripMenuItem1.Name = "cropToolStripMenuItem1";
            this.cropToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.cropToolStripMenuItem1.Text = "C&rop";
            // 
            // cropToolStripMenuItem2
            // 
            this.cropToolStripMenuItem2.Name = "cropToolStripMenuItem2";
            this.cropToolStripMenuItem2.Size = new System.Drawing.Size(142, 22);
            this.cropToolStripMenuItem2.Text = "Crop (&Auto)";
            this.cropToolStripMenuItem2.Click += new System.EventHandler(this.autoCropToolStripMenuItem_Click);
            // 
            // manualToolStripMenuItem1
            // 
            this.manualToolStripMenuItem1.Name = "manualToolStripMenuItem1";
            this.manualToolStripMenuItem1.Size = new System.Drawing.Size(142, 22);
            this.manualToolStripMenuItem1.Text = "Crop (&Manual)";
            this.manualToolStripMenuItem1.Click += new System.EventHandler(this.cropToolStripMenuItem_Click);
            // 
            // cropBroderToolStripMenuItem
            // 
            this.cropBroderToolStripMenuItem.Name = "cropBroderToolStripMenuItem";
            this.cropBroderToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.cropBroderToolStripMenuItem.Text = "Crop&Border";
            this.cropBroderToolStripMenuItem.Click += new System.EventHandler(this.cropBorderToolStripMenuItem_Click);
            // 
            // orientationToolStripMenuItem
            // 
            this.orientationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.flipToolStripMenuItem,
            this.mirrorToolStripMenuItem,
            this.rotateToolStripMenuItem1});
            this.orientationToolStripMenuItem.Name = "orientationToolStripMenuItem";
            this.orientationToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.orientationToolStripMenuItem.Text = "&Orientation";
            // 
            // flipToolStripMenuItem
            // 
            this.flipToolStripMenuItem.Name = "flipToolStripMenuItem";
            this.flipToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.flipToolStripMenuItem.Text = "Flip";
            this.flipToolStripMenuItem.Click += new System.EventHandler(this.flipToolStripMenuItem_Click);
            // 
            // mirrorToolStripMenuItem
            // 
            this.mirrorToolStripMenuItem.Name = "mirrorToolStripMenuItem";
            this.mirrorToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.mirrorToolStripMenuItem.Text = "Mirror";
            this.mirrorToolStripMenuItem.Click += new System.EventHandler(this.mirrorToolStripMenuItem_Click);
            // 
            // rotateToolStripMenuItem1
            // 
            this.rotateToolStripMenuItem1.Name = "rotateToolStripMenuItem1";
            this.rotateToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.rotateToolStripMenuItem1.Text = "&Rotate";
            this.rotateToolStripMenuItem1.Click += new System.EventHandler(this.rotateToolStripMenuItem_Click);
            // 
            // redEyeRemovalToolStripMenuItem
            // 
            this.redEyeRemovalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoToolStripMenuItem,
            this.manualToolStripMenuItem});
            this.redEyeRemovalToolStripMenuItem.Name = "redEyeRemovalToolStripMenuItem";
            this.redEyeRemovalToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.redEyeRemovalToolStripMenuItem.Text = "&Red Eye Removal";
            // 
            // autoToolStripMenuItem
            // 
            this.autoToolStripMenuItem.Name = "autoToolStripMenuItem";
            this.autoToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.autoToolStripMenuItem.Text = "&Auto";
            this.autoToolStripMenuItem.Click += new System.EventHandler(this.autoRemoveRedEyeToolStripMenuItem_Click);
            // 
            // manualToolStripMenuItem
            // 
            this.manualToolStripMenuItem.Name = "manualToolStripMenuItem";
            this.manualToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.manualToolStripMenuItem.Text = "&Manual";
            this.manualToolStripMenuItem.Click += new System.EventHandler(this.redEyeToolStripMenuItem_Click);
            // 
            // removeDustToolStripMenuItem1
            // 
            this.removeDustToolStripMenuItem1.Name = "removeDustToolStripMenuItem1";
            this.removeDustToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.removeDustToolStripMenuItem1.Text = "Remove &Dust";
            this.removeDustToolStripMenuItem1.Click += new System.EventHandler(this.removeDustToolStripMenuItem_Click);
            // 
            // removeScratchesToolStripMenuItem1
            // 
            this.removeScratchesToolStripMenuItem1.Name = "removeScratchesToolStripMenuItem1";
            this.removeScratchesToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.removeScratchesToolStripMenuItem1.Text = "Remove &Scratches";
            this.removeScratchesToolStripMenuItem1.Click += new System.EventHandler(this.removeScratchesToolStripMenuItem_Click);
            // 
            // resizeToolStripMenuItem2
            // 
            this.resizeToolStripMenuItem2.Name = "resizeToolStripMenuItem2";
            this.resizeToolStripMenuItem2.Size = new System.Drawing.Size(163, 22);
            this.resizeToolStripMenuItem2.Text = "Resi&ze";
            this.resizeToolStripMenuItem2.Click += new System.EventHandler(this.resizeToolStripMenuItem_Click);
            // 
            // sharpenToolStripMenuItem1
            // 
            this.sharpenToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sharpenToolStripMenuItem2,
            this.softenToolStripMenuItem1,
            this.unsharpenToolStripMenuItem1});
            this.sharpenToolStripMenuItem1.Name = "sharpenToolStripMenuItem1";
            this.sharpenToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.sharpenToolStripMenuItem1.Text = "S&harpen";
            // 
            // sharpenToolStripMenuItem2
            // 
            this.sharpenToolStripMenuItem2.Name = "sharpenToolStripMenuItem2";
            this.sharpenToolStripMenuItem2.Size = new System.Drawing.Size(126, 22);
            this.sharpenToolStripMenuItem2.Text = "&Sharpen";
            this.sharpenToolStripMenuItem2.Click += new System.EventHandler(this.sharpenToolStripMenuItem_Click);
            // 
            // softenToolStripMenuItem1
            // 
            this.softenToolStripMenuItem1.Name = "softenToolStripMenuItem1";
            this.softenToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.softenToolStripMenuItem1.Text = "S&often";
            this.softenToolStripMenuItem1.Click += new System.EventHandler(this.softenToolStripMenuItem_Click);
            // 
            // unsharpenToolStripMenuItem1
            // 
            this.unsharpenToolStripMenuItem1.Name = "unsharpenToolStripMenuItem1";
            this.unsharpenToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.unsharpenToolStripMenuItem1.Text = "&Unsharpen";
            this.unsharpenToolStripMenuItem1.Click += new System.EventHandler(this.unsharpenToolStripMenuItem_Click);
            // 
            // filtersAndEffectsToolStripMenuItem
            // 
            this.filtersAndEffectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blenToolStripMenuItem,
            this.blurToolStripMenuItem1,
            this.buttonizeToolStripMenuItem1,
            this.diffuseToolStripMenuItem1,
            this.embossToolStripMenuItem1,
            this.matrixToolStripMenuItem1,
            this.medianToolStripMenuItem1,
            this.mosaicToolStripMenuItem1,
            this.motionBlurToolStripMenuItem1,
            this.negateToolStripMenuItem2,
            this.noiseToolStripMenuItem1,
            this.outlineToolStripMenuItem1,
            this.parabolicToolStripMenuItem1,
            this.perspectiveToolStripMenuItem1,
            this.pinchToolStripMenuItem1,
            this.polynomialWarpToolStripMenuItem1,
            this.posterizeToolStripMenuItem1,
            this.rippleToolStripMenuItem1,
            this.solarizeToolStripMenuItem1,
            this.swirlToolStripMenuItem1,
            this.tileToolStripMenuItem1,
            this.twistToolStripMenuItem1});
            this.filtersAndEffectsToolStripMenuItem.Enabled = false;
            this.filtersAndEffectsToolStripMenuItem.Name = "filtersAndEffectsToolStripMenuItem";
            this.filtersAndEffectsToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.filtersAndEffectsToolStripMenuItem.Text = "&Effects";
            // 
            // blenToolStripMenuItem
            // 
            this.blenToolStripMenuItem.Name = "blenToolStripMenuItem";
            this.blenToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.blenToolStripMenuItem.Text = "&Blend";
            this.blenToolStripMenuItem.Click += new System.EventHandler(this.blendToolStripMenuItem_Click);
            // 
            // blurToolStripMenuItem1
            // 
            this.blurToolStripMenuItem1.Name = "blurToolStripMenuItem1";
            this.blurToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.blurToolStripMenuItem1.Text = "Blu&r";
            this.blurToolStripMenuItem1.Click += new System.EventHandler(this.blurToolStripMenuItem_Click);
            // 
            // buttonizeToolStripMenuItem1
            // 
            this.buttonizeToolStripMenuItem1.Name = "buttonizeToolStripMenuItem1";
            this.buttonizeToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.buttonizeToolStripMenuItem1.Text = "B&uttonize";
            this.buttonizeToolStripMenuItem1.Click += new System.EventHandler(this.buttonizeToolStripMenuItem_Click);
            // 
            // diffuseToolStripMenuItem1
            // 
            this.diffuseToolStripMenuItem1.Name = "diffuseToolStripMenuItem1";
            this.diffuseToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.diffuseToolStripMenuItem1.Text = "&Diffuse";
            this.diffuseToolStripMenuItem1.Click += new System.EventHandler(this.diffuseToolStripMenuItem_Click);
            // 
            // embossToolStripMenuItem1
            // 
            this.embossToolStripMenuItem1.Name = "embossToolStripMenuItem1";
            this.embossToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.embossToolStripMenuItem1.Text = "&Emboss";
            this.embossToolStripMenuItem1.Click += new System.EventHandler(this.embossToolStripMenuItem_Click);
            // 
            // matrixToolStripMenuItem1
            // 
            this.matrixToolStripMenuItem1.Name = "matrixToolStripMenuItem1";
            this.matrixToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.matrixToolStripMenuItem1.Text = "&Matrix";
            this.matrixToolStripMenuItem1.Click += new System.EventHandler(this.matrixToolStripMenuItem_Click);
            // 
            // medianToolStripMenuItem1
            // 
            this.medianToolStripMenuItem1.Name = "medianToolStripMenuItem1";
            this.medianToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.medianToolStripMenuItem1.Text = "Med&ian";
            this.medianToolStripMenuItem1.Click += new System.EventHandler(this.medianToolStripMenuItem_Click);
            // 
            // mosaicToolStripMenuItem1
            // 
            this.mosaicToolStripMenuItem1.Name = "mosaicToolStripMenuItem1";
            this.mosaicToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.mosaicToolStripMenuItem1.Text = "M&osaic";
            this.mosaicToolStripMenuItem1.Click += new System.EventHandler(this.mosaicToolStripMenuItem_Click);
            // 
            // motionBlurToolStripMenuItem1
            // 
            this.motionBlurToolStripMenuItem1.Name = "motionBlurToolStripMenuItem1";
            this.motionBlurToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.motionBlurToolStripMenuItem1.Text = "Mo&tion Blur";
            this.motionBlurToolStripMenuItem1.Click += new System.EventHandler(this.motionBlurToolStripMenuItem_Click);
            // 
            // negateToolStripMenuItem2
            // 
            this.negateToolStripMenuItem2.Name = "negateToolStripMenuItem2";
            this.negateToolStripMenuItem2.Size = new System.Drawing.Size(153, 22);
            this.negateToolStripMenuItem2.Text = "&Negate";
            this.negateToolStripMenuItem2.Click += new System.EventHandler(this.negateToolStripMenuItem_Click);
            // 
            // noiseToolStripMenuItem1
            // 
            this.noiseToolStripMenuItem1.Name = "noiseToolStripMenuItem1";
            this.noiseToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.noiseToolStripMenuItem1.Text = "N&oise";
            this.noiseToolStripMenuItem1.Click += new System.EventHandler(this.noiseToolStripMenuItem_Click);
            // 
            // outlineToolStripMenuItem1
            // 
            this.outlineToolStripMenuItem1.Name = "outlineToolStripMenuItem1";
            this.outlineToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.outlineToolStripMenuItem1.Text = "Out&line";
            this.outlineToolStripMenuItem1.Click += new System.EventHandler(this.outlineToolStripMenuItem_Click);
            // 
            // parabolicToolStripMenuItem1
            // 
            this.parabolicToolStripMenuItem1.Name = "parabolicToolStripMenuItem1";
            this.parabolicToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.parabolicToolStripMenuItem1.Text = "&Parabolic";
            this.parabolicToolStripMenuItem1.Click += new System.EventHandler(this.parabolicToolStripMenuItem_Click);
            // 
            // perspectiveToolStripMenuItem1
            // 
            this.perspectiveToolStripMenuItem1.Name = "perspectiveToolStripMenuItem1";
            this.perspectiveToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.perspectiveToolStripMenuItem1.Text = "P&erspective";
            this.perspectiveToolStripMenuItem1.Click += new System.EventHandler(this.perspectiveToolStripMenuItem_Click);
            // 
            // pinchToolStripMenuItem1
            // 
            this.pinchToolStripMenuItem1.Name = "pinchToolStripMenuItem1";
            this.pinchToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.pinchToolStripMenuItem1.Text = "P&inch";
            this.pinchToolStripMenuItem1.Click += new System.EventHandler(this.pinchToolStripMenuItem_Click);
            // 
            // polynomialWarpToolStripMenuItem1
            // 
            this.polynomialWarpToolStripMenuItem1.Name = "polynomialWarpToolStripMenuItem1";
            this.polynomialWarpToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.polynomialWarpToolStripMenuItem1.Text = "P&olynomial Warp";
            this.polynomialWarpToolStripMenuItem1.Click += new System.EventHandler(this.polynomialWarpToolStripMenuItem_Click);
            // 
            // posterizeToolStripMenuItem1
            // 
            this.posterizeToolStripMenuItem1.Name = "posterizeToolStripMenuItem1";
            this.posterizeToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.posterizeToolStripMenuItem1.Text = "Posteri&ze";
            this.posterizeToolStripMenuItem1.Click += new System.EventHandler(this.posterizeToolStripMenuItem_Click);
            // 
            // rippleToolStripMenuItem1
            // 
            this.rippleToolStripMenuItem1.Name = "rippleToolStripMenuItem1";
            this.rippleToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.rippleToolStripMenuItem1.Text = "&Ripple";
            this.rippleToolStripMenuItem1.Click += new System.EventHandler(this.rippleToolStripMenuItem_Click);
            // 
            // solarizeToolStripMenuItem1
            // 
            this.solarizeToolStripMenuItem1.Name = "solarizeToolStripMenuItem1";
            this.solarizeToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.solarizeToolStripMenuItem1.Text = "&Solarize";
            this.solarizeToolStripMenuItem1.Click += new System.EventHandler(this.solarizeToolStripMenuItem_Click);
            // 
            // swirlToolStripMenuItem1
            // 
            this.swirlToolStripMenuItem1.Name = "swirlToolStripMenuItem1";
            this.swirlToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.swirlToolStripMenuItem1.Text = "S&wirl";
            this.swirlToolStripMenuItem1.Click += new System.EventHandler(this.swirlToolStripMenuItem_Click);
            // 
            // tileToolStripMenuItem1
            // 
            this.tileToolStripMenuItem1.Name = "tileToolStripMenuItem1";
            this.tileToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.tileToolStripMenuItem1.Text = "&Tile";
            this.tileToolStripMenuItem1.Click += new System.EventHandler(this.tileToolStripMenuItem_Click);
            // 
            // twistToolStripMenuItem1
            // 
            this.twistToolStripMenuItem1.Name = "twistToolStripMenuItem1";
            this.twistToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.twistToolStripMenuItem1.Text = "T&wist";
            this.twistToolStripMenuItem1.Click += new System.EventHandler(this.twistToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gettingStartedToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.imagXpressToolStripMenuItem,
            this.notateXpressToolStripMenuItem,
            this.pdfXpressToolStripMenuItem,
            this.printProToolStripMenuItem,
            this.thumbnailXpressToolStripMenuItem,
            this.twainProToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // gettingStartedToolStripMenuItem
            // 
            this.gettingStartedToolStripMenuItem.Name = "gettingStartedToolStripMenuItem";
            this.gettingStartedToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.gettingStartedToolStripMenuItem.Text = "&Quick Start";
            this.gettingStartedToolStripMenuItem.Click += new System.EventHandler(this.gettingStartedToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.imagXpressToolStripMenuItem.Text = "Imag&Xpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click);
            // 
            // notateXpressToolStripMenuItem
            // 
            this.notateXpressToolStripMenuItem.Name = "notateXpressToolStripMenuItem";
            this.notateXpressToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.notateXpressToolStripMenuItem.Text = "&NotateXpress";
            this.notateXpressToolStripMenuItem.Click += new System.EventHandler(this.notateXpressToolStripMenuItem_Click);
            // 
            // pdfXpressToolStripMenuItem
            // 
            this.pdfXpressToolStripMenuItem.Name = "pdfXpressToolStripMenuItem";
            this.pdfXpressToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.pdfXpressToolStripMenuItem.Text = "PD&FXpress";
            this.pdfXpressToolStripMenuItem.Click += new System.EventHandler(this.pdfXpressToolStripMenuItem_Click);
            // 
            // printProToolStripMenuItem
            // 
            this.printProToolStripMenuItem.Name = "printProToolStripMenuItem";
            this.printProToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.printProToolStripMenuItem.Text = "&PrintPro";
            this.printProToolStripMenuItem.Click += new System.EventHandler(this.printProToolStripMenuItem_Click);
            // 
            // thumbnailXpressToolStripMenuItem
            // 
            this.thumbnailXpressToolStripMenuItem.Name = "thumbnailXpressToolStripMenuItem";
            this.thumbnailXpressToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.thumbnailXpressToolStripMenuItem.Text = "&ThumbnailXpress";
            this.thumbnailXpressToolStripMenuItem.Click += new System.EventHandler(this.thumbnailXpressToolStripMenuItem_Click);
            // 
            // twainProToolStripMenuItem
            // 
            this.twainProToolStripMenuItem.Name = "twainProToolStripMenuItem";
            this.twainProToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.twainProToolStripMenuItem.Text = "T&wainPro";
            this.twainProToolStripMenuItem.Click += new System.EventHandler(this.twainProToolStripMenuItem_Click);
            // 
            // Toolbar
            // 
            this.Toolbar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Toolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenButton,
            this.CloseButton,
            this.SaveButton,
            this.PrintButton,
            this.ScanButton,
            this.CutButton,
            this.CopyButton,
            this.PasteButton,
            this.RefreshButton,
            this.ViewingSeparator,
            this.ViewingLabel,
            this.ZoomFitButton,
            this.Zoom1to1Button,
            this.ZoomWidthButton,
            this.ZoomHeightButton,
            this.RotateLeftButton,
            this.RotateRightButton,
            this.PointerIXButton,
            this.ZoomInButton,
            this.ZoomOutButton,
            this.ZoomRectangleButton,
            this.HandButton,
            this.AreaButton,
            this.MagnifierButton,
            this.AnnotationSeparator,
            this.AnnotationsToolStrip,
            this.PointerNXButton,
            this.NoteButton,
            this.TextButton,
            this.RectangleButton,
            this.PolygonButton,
            this.PolylineButton,
            this.LineButton,
            this.FreehandButton,
            this.StampButton,
            this.EllipseButton,
            this.RulerButton,
            this.ImageButton,
            this.HighlightButton,
            this.ButtonButton,
            this.BrandButton,
            this.PagingSeparator,
            this.PageLabel,
            this.PageBox,
            this.PageCountLabel,
            this.PreviousButton,
            this.NextButton,
            this.HelpSeparator,
            this.HelpInfoButton});
            this.Toolbar.Location = new System.Drawing.Point(0, 24);
            this.Toolbar.Name = "Toolbar";
            this.Toolbar.Size = new System.Drawing.Size(1169, 25);
            this.Toolbar.TabIndex = 1;
            this.Toolbar.Text = "toolStrip1";
            // 
            // OpenButton
            // 
            this.OpenButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OpenButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripDropDownMenuItem,
            this.fileWithLoadOptionsDropDownToolStripMenuItem,
            this.directoryToolStripMenuItem,
            this.rawFileToolStripMenuItem,
            this.dropDownSampleImages});
            this.OpenButton.Image = ((System.Drawing.Image)(resources.GetObject("OpenButton.Image")));
            this.OpenButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(32, 22);
            this.OpenButton.Text = "Open an Image";
            this.OpenButton.ButtonClick += new System.EventHandler(this.OpenButton_ButtonClick);
            // 
            // fileToolStripDropDownMenuItem
            // 
            this.fileToolStripDropDownMenuItem.Name = "fileToolStripDropDownMenuItem";
            this.fileToolStripDropDownMenuItem.Size = new System.Drawing.Size(179, 22);
            this.fileToolStripDropDownMenuItem.Text = "File";
            this.fileToolStripDropDownMenuItem.Click += new System.EventHandler(this.fileToolStripDropDownMenuItem_Click);
            // 
            // fileWithLoadOptionsDropDownToolStripMenuItem
            // 
            this.fileWithLoadOptionsDropDownToolStripMenuItem.Name = "fileWithLoadOptionsDropDownToolStripMenuItem";
            this.fileWithLoadOptionsDropDownToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.fileWithLoadOptionsDropDownToolStripMenuItem.Text = "File with Load Options";
            this.fileWithLoadOptionsDropDownToolStripMenuItem.Click += new System.EventHandler(this.fileWithLoadOptionsDropDownToolStripMenuItem_Click);
            // 
            // directoryToolStripMenuItem
            // 
            this.directoryToolStripMenuItem.Name = "directoryToolStripMenuItem";
            this.directoryToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.directoryToolStripMenuItem.Text = "Directory";
            this.directoryToolStripMenuItem.Click += new System.EventHandler(this.directoryToolStripMenuItem_Click);
            // 
            // rawFileToolStripMenuItem
            // 
            this.rawFileToolStripMenuItem.Name = "rawFileToolStripMenuItem";
            this.rawFileToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.rawFileToolStripMenuItem.Text = "Raw File";
            this.rawFileToolStripMenuItem.Click += new System.EventHandler(this.rawFileToolStripMenuItem_Click);
            // 
            // dropDownSampleImages
            // 
            this.dropDownSampleImages.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownDocument,
            this.dropDownPhoto,
            this.dropDownEffects});
            this.dropDownSampleImages.Name = "dropDownSampleImages";
            this.dropDownSampleImages.Size = new System.Drawing.Size(179, 22);
            this.dropDownSampleImages.Text = "&Sample Images";
            // 
            // dropDownDocument
            // 
            this.dropDownDocument.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownBinarize,
            this.dropDownBlob,
            this.dropDownBorder,
            this.dropDownDeskew,
            this.dropDownDespeckle,
            this.dropDownDilate,
            this.dropDownErode,
            this.dropDownMulti,
            this.dropDownNegate,
            this.dropDownLineRemoval,
            this.dropDownShear,
            this.dropDownSmooth,
            this.dropDownTags});
            this.dropDownDocument.Name = "dropDownDocument";
            this.dropDownDocument.Size = new System.Drawing.Size(170, 22);
            this.dropDownDocument.Text = "Document Cleanup";
            // 
            // dropDownBinarize
            // 
            this.dropDownBinarize.Name = "dropDownBinarize";
            this.dropDownBinarize.Size = new System.Drawing.Size(149, 22);
            this.dropDownBinarize.Text = "Binarize";
            this.dropDownBinarize.Click += new System.EventHandler(this.dropDownBinarize_Click);
            // 
            // dropDownBlob
            // 
            this.dropDownBlob.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownBlob1,
            this.dropDownBlob2});
            this.dropDownBlob.Name = "dropDownBlob";
            this.dropDownBlob.Size = new System.Drawing.Size(149, 22);
            this.dropDownBlob.Text = "Blob Removal";
            // 
            // dropDownBlob1
            // 
            this.dropDownBlob1.Name = "dropDownBlob1";
            this.dropDownBlob1.Size = new System.Drawing.Size(147, 22);
            this.dropDownBlob1.Text = "Blob Removal 1";
            this.dropDownBlob1.Click += new System.EventHandler(this.dropDownBlob1_Click);
            // 
            // dropDownBlob2
            // 
            this.dropDownBlob2.Name = "dropDownBlob2";
            this.dropDownBlob2.Size = new System.Drawing.Size(147, 22);
            this.dropDownBlob2.Text = "Blob Removal 2";
            this.dropDownBlob2.Click += new System.EventHandler(this.dropDownBlob2_Click);
            // 
            // dropDownBorder
            // 
            this.dropDownBorder.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownBorder1,
            this.dropDownBorder2});
            this.dropDownBorder.Name = "dropDownBorder";
            this.dropDownBorder.Size = new System.Drawing.Size(149, 22);
            this.dropDownBorder.Text = "Border Crop";
            // 
            // dropDownBorder1
            // 
            this.dropDownBorder1.Name = "dropDownBorder1";
            this.dropDownBorder1.Size = new System.Drawing.Size(141, 22);
            this.dropDownBorder1.Text = "Border Crop 1";
            this.dropDownBorder1.Click += new System.EventHandler(this.dropDownBorder1_Click);
            // 
            // dropDownBorder2
            // 
            this.dropDownBorder2.Name = "dropDownBorder2";
            this.dropDownBorder2.Size = new System.Drawing.Size(141, 22);
            this.dropDownBorder2.Text = "Border Crop 2";
            this.dropDownBorder2.Click += new System.EventHandler(this.dropDownBorder2_Click);
            // 
            // dropDownDeskew
            // 
            this.dropDownDeskew.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownDeskew1,
            this.dropDownDeskew2,
            this.dropDownDeskew3});
            this.dropDownDeskew.Name = "dropDownDeskew";
            this.dropDownDeskew.Size = new System.Drawing.Size(149, 22);
            this.dropDownDeskew.Text = "Deskew";
            // 
            // dropDownDeskew1
            // 
            this.dropDownDeskew1.Name = "dropDownDeskew1";
            this.dropDownDeskew1.Size = new System.Drawing.Size(120, 22);
            this.dropDownDeskew1.Text = "Deskew 1";
            this.dropDownDeskew1.Click += new System.EventHandler(this.dropDownDeskew1_Click);
            // 
            // dropDownDeskew2
            // 
            this.dropDownDeskew2.Name = "dropDownDeskew2";
            this.dropDownDeskew2.Size = new System.Drawing.Size(120, 22);
            this.dropDownDeskew2.Text = "Deskew 2";
            this.dropDownDeskew2.Click += new System.EventHandler(this.dropDownDeskew2_Click);
            // 
            // dropDownDeskew3
            // 
            this.dropDownDeskew3.Name = "dropDownDeskew3";
            this.dropDownDeskew3.Size = new System.Drawing.Size(120, 22);
            this.dropDownDeskew3.Text = "Deskew 3";
            this.dropDownDeskew3.Click += new System.EventHandler(this.dropDownDeskew3_Click);
            // 
            // dropDownDespeckle
            // 
            this.dropDownDespeckle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownDespeckle1,
            this.dropDownDespeckle2,
            this.dropDownDespeckle3});
            this.dropDownDespeckle.Name = "dropDownDespeckle";
            this.dropDownDespeckle.Size = new System.Drawing.Size(149, 22);
            this.dropDownDespeckle.Text = "Despeckle";
            // 
            // dropDownDespeckle1
            // 
            this.dropDownDespeckle1.Name = "dropDownDespeckle1";
            this.dropDownDespeckle1.Size = new System.Drawing.Size(131, 22);
            this.dropDownDespeckle1.Text = "Despeckle 1";
            this.dropDownDespeckle1.Click += new System.EventHandler(this.dropDownDespeckle1_Click);
            // 
            // dropDownDespeckle2
            // 
            this.dropDownDespeckle2.Name = "dropDownDespeckle2";
            this.dropDownDespeckle2.Size = new System.Drawing.Size(131, 22);
            this.dropDownDespeckle2.Text = "Despeckle 2";
            this.dropDownDespeckle2.Click += new System.EventHandler(this.dropDownDespeckle2_Click);
            // 
            // dropDownDespeckle3
            // 
            this.dropDownDespeckle3.Name = "dropDownDespeckle3";
            this.dropDownDespeckle3.Size = new System.Drawing.Size(131, 22);
            this.dropDownDespeckle3.Text = "Despeckle 3";
            this.dropDownDespeckle3.Click += new System.EventHandler(this.dropDownDespeckle3_Click);
            // 
            // dropDownDilate
            // 
            this.dropDownDilate.Name = "dropDownDilate";
            this.dropDownDilate.Size = new System.Drawing.Size(149, 22);
            this.dropDownDilate.Text = "Dilate";
            this.dropDownDilate.Click += new System.EventHandler(this.dropDownDilate_Click);
            // 
            // dropDownErode
            // 
            this.dropDownErode.Name = "dropDownErode";
            this.dropDownErode.Size = new System.Drawing.Size(149, 22);
            this.dropDownErode.Text = "Erode";
            this.dropDownErode.Click += new System.EventHandler(this.dropDownErode_Click);
            // 
            // dropDownMulti
            // 
            this.dropDownMulti.Name = "dropDownMulti";
            this.dropDownMulti.Size = new System.Drawing.Size(149, 22);
            this.dropDownMulti.Text = "MultiPage";
            this.dropDownMulti.Click += new System.EventHandler(this.dropDownMulti_Click);
            // 
            // dropDownNegate
            // 
            this.dropDownNegate.Name = "dropDownNegate";
            this.dropDownNegate.Size = new System.Drawing.Size(149, 22);
            this.dropDownNegate.Text = "Negate";
            this.dropDownNegate.Click += new System.EventHandler(this.dropDownNegate_Click);
            // 
            // dropDownLineRemoval
            // 
            this.dropDownLineRemoval.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownLineRemoval1,
            this.dropDownLineRemoval2});
            this.dropDownLineRemoval.Name = "dropDownLineRemoval";
            this.dropDownLineRemoval.Size = new System.Drawing.Size(149, 22);
            this.dropDownLineRemoval.Text = "Line Removal";
            // 
            // dropDownLineRemoval1
            // 
            this.dropDownLineRemoval1.Name = "dropDownLineRemoval1";
            this.dropDownLineRemoval1.Size = new System.Drawing.Size(149, 22);
            this.dropDownLineRemoval1.Text = "Remove Lines 1";
            this.dropDownLineRemoval1.Click += new System.EventHandler(this.dropDownLineRemoval1_Click);
            // 
            // dropDownLineRemoval2
            // 
            this.dropDownLineRemoval2.Name = "dropDownLineRemoval2";
            this.dropDownLineRemoval2.Size = new System.Drawing.Size(149, 22);
            this.dropDownLineRemoval2.Text = "Remove Lines 2";
            this.dropDownLineRemoval2.Click += new System.EventHandler(this.dropDownLineRemoval2_Click);
            // 
            // dropDownShear
            // 
            this.dropDownShear.Name = "dropDownShear";
            this.dropDownShear.Size = new System.Drawing.Size(149, 22);
            this.dropDownShear.Text = "Shear";
            this.dropDownShear.Click += new System.EventHandler(this.dropDownShear_Click);
            // 
            // dropDownSmooth
            // 
            this.dropDownSmooth.Name = "dropDownSmooth";
            this.dropDownSmooth.Size = new System.Drawing.Size(149, 22);
            this.dropDownSmooth.Text = "Smooth && Zoom";
            this.dropDownSmooth.Click += new System.EventHandler(this.dropDownSmooth_Click);
            // 
            // dropDownTags
            // 
            this.dropDownTags.Name = "dropDownTags";
            this.dropDownTags.Size = new System.Drawing.Size(149, 22);
            this.dropDownTags.Text = "Tags";
            this.dropDownTags.Click += new System.EventHandler(this.dropDownTags_Click);
            // 
            // dropDownPhoto
            // 
            this.dropDownPhoto.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownAlpha,
            this.dropDownColorBalance,
            this.dropDownContrast,
            this.dropDownCrop,
            this.dropDownExif,
            this.dropDownOrientation,
            this.dropDownRedEye,
            this.dropDownDust,
            this.dropDownScratch,
            this.dropDownSharpen});
            this.dropDownPhoto.Name = "dropDownPhoto";
            this.dropDownPhoto.Size = new System.Drawing.Size(170, 22);
            this.dropDownPhoto.Text = "Photo Enhancement";
            // 
            // dropDownAlpha
            // 
            this.dropDownAlpha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownAlpha1,
            this.dropDownAlpha2,
            this.dropDownAlpha3});
            this.dropDownAlpha.Name = "dropDownAlpha";
            this.dropDownAlpha.Size = new System.Drawing.Size(163, 22);
            this.dropDownAlpha.Text = "Alpha";
            // 
            // dropDownAlpha1
            // 
            this.dropDownAlpha1.Name = "dropDownAlpha1";
            this.dropDownAlpha1.Size = new System.Drawing.Size(110, 22);
            this.dropDownAlpha1.Text = "Alpha 1";
            this.dropDownAlpha1.Click += new System.EventHandler(this.dropDownAlpha1_Click);
            // 
            // dropDownAlpha2
            // 
            this.dropDownAlpha2.Name = "dropDownAlpha2";
            this.dropDownAlpha2.Size = new System.Drawing.Size(110, 22);
            this.dropDownAlpha2.Text = "Alpha 2";
            this.dropDownAlpha2.Click += new System.EventHandler(this.dropDownAlpha2_Click);
            // 
            // dropDownAlpha3
            // 
            this.dropDownAlpha3.Name = "dropDownAlpha3";
            this.dropDownAlpha3.Size = new System.Drawing.Size(110, 22);
            this.dropDownAlpha3.Text = "Alpha 3";
            this.dropDownAlpha3.Click += new System.EventHandler(this.dropDownAlpha3_Click);
            // 
            // dropDownColorBalance
            // 
            this.dropDownColorBalance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownColorBalance1,
            this.dropDownColorBalance2,
            this.dropDownColorBalance3,
            this.dropDownColorBalance4});
            this.dropDownColorBalance.Name = "dropDownColorBalance";
            this.dropDownColorBalance.Size = new System.Drawing.Size(163, 22);
            this.dropDownColorBalance.Text = "Color Balance";
            // 
            // dropDownColorBalance1
            // 
            this.dropDownColorBalance1.Name = "dropDownColorBalance1";
            this.dropDownColorBalance1.Size = new System.Drawing.Size(146, 22);
            this.dropDownColorBalance1.Text = "Auto Balance 1";
            this.dropDownColorBalance1.Click += new System.EventHandler(this.dropDownColorBalance1_Click);
            // 
            // dropDownColorBalance2
            // 
            this.dropDownColorBalance2.Name = "dropDownColorBalance2";
            this.dropDownColorBalance2.Size = new System.Drawing.Size(146, 22);
            this.dropDownColorBalance2.Text = "Auto Balance 2";
            this.dropDownColorBalance2.Click += new System.EventHandler(this.dropDownColorBalance2_Click);
            // 
            // dropDownColorBalance3
            // 
            this.dropDownColorBalance3.Name = "dropDownColorBalance3";
            this.dropDownColorBalance3.Size = new System.Drawing.Size(146, 22);
            this.dropDownColorBalance3.Text = "Auto Level 1";
            this.dropDownColorBalance3.Click += new System.EventHandler(this.dropDownColorBalance3_Click);
            // 
            // dropDownColorBalance4
            // 
            this.dropDownColorBalance4.Name = "dropDownColorBalance4";
            this.dropDownColorBalance4.Size = new System.Drawing.Size(146, 22);
            this.dropDownColorBalance4.Text = "Auto Level 2";
            this.dropDownColorBalance4.Click += new System.EventHandler(this.dropDownColorBalance4_Click);
            // 
            // dropDownContrast
            // 
            this.dropDownContrast.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownContrast1,
            this.dropDownContrast2});
            this.dropDownContrast.Name = "dropDownContrast";
            this.dropDownContrast.Size = new System.Drawing.Size(163, 22);
            this.dropDownContrast.Text = "Contrast";
            // 
            // dropDownContrast1
            // 
            this.dropDownContrast1.Name = "dropDownContrast1";
            this.dropDownContrast1.Size = new System.Drawing.Size(145, 22);
            this.dropDownContrast1.Text = "Auto Contrast";
            this.dropDownContrast1.Click += new System.EventHandler(this.dropDownContrast1_Click);
            // 
            // dropDownContrast2
            // 
            this.dropDownContrast2.Name = "dropDownContrast2";
            this.dropDownContrast2.Size = new System.Drawing.Size(145, 22);
            this.dropDownContrast2.Text = "Auto Lightness";
            this.dropDownContrast2.Click += new System.EventHandler(this.dropDownContrast2_Click);
            // 
            // dropDownCrop
            // 
            this.dropDownCrop.Name = "dropDownCrop";
            this.dropDownCrop.Size = new System.Drawing.Size(163, 22);
            this.dropDownCrop.Text = "Crop";
            this.dropDownCrop.Click += new System.EventHandler(this.dropDownCrop_Click);
            // 
            // dropDownExif
            // 
            this.dropDownExif.Name = "dropDownExif";
            this.dropDownExif.Size = new System.Drawing.Size(163, 22);
            this.dropDownExif.Text = "EXIF";
            this.dropDownExif.Click += new System.EventHandler(this.dropDownExif_Click);
            // 
            // dropDownOrientation
            // 
            this.dropDownOrientation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownOrientation1,
            this.dropDownOrientation2});
            this.dropDownOrientation.Name = "dropDownOrientation";
            this.dropDownOrientation.Size = new System.Drawing.Size(163, 22);
            this.dropDownOrientation.Text = "Orientation";
            // 
            // dropDownOrientation1
            // 
            this.dropDownOrientation1.Name = "dropDownOrientation1";
            this.dropDownOrientation1.Size = new System.Drawing.Size(116, 22);
            this.dropDownOrientation1.Text = "Rotate 1";
            this.dropDownOrientation1.Click += new System.EventHandler(this.dropDownOrientation1_Click);
            // 
            // dropDownOrientation2
            // 
            this.dropDownOrientation2.Name = "dropDownOrientation2";
            this.dropDownOrientation2.Size = new System.Drawing.Size(116, 22);
            this.dropDownOrientation2.Text = "Rotate 2";
            this.dropDownOrientation2.Click += new System.EventHandler(this.dropDownOrientation2_Click);
            // 
            // dropDownRedEye
            // 
            this.dropDownRedEye.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownRedEye1,
            this.dropDownRedEye2,
            this.dropDownRedEye3,
            this.dropDownRedEye4});
            this.dropDownRedEye.Name = "dropDownRedEye";
            this.dropDownRedEye.Size = new System.Drawing.Size(163, 22);
            this.dropDownRedEye.Text = "Red Eye Removal";
            // 
            // dropDownRedEye1
            // 
            this.dropDownRedEye1.Name = "dropDownRedEye1";
            this.dropDownRedEye1.Size = new System.Drawing.Size(165, 22);
            this.dropDownRedEye1.Text = "Remove Red Eye 1";
            this.dropDownRedEye1.Click += new System.EventHandler(this.dropDownRedEye1_Click);
            // 
            // dropDownRedEye2
            // 
            this.dropDownRedEye2.Name = "dropDownRedEye2";
            this.dropDownRedEye2.Size = new System.Drawing.Size(165, 22);
            this.dropDownRedEye2.Text = "Remove Red Eye 2";
            this.dropDownRedEye2.Click += new System.EventHandler(this.dropDownRedEye2_Click);
            // 
            // dropDownRedEye3
            // 
            this.dropDownRedEye3.Name = "dropDownRedEye3";
            this.dropDownRedEye3.Size = new System.Drawing.Size(165, 22);
            this.dropDownRedEye3.Text = "Remove Red Eye 3";
            this.dropDownRedEye3.Click += new System.EventHandler(this.dropDownRedEye3_Click);
            // 
            // dropDownRedEye4
            // 
            this.dropDownRedEye4.Name = "dropDownRedEye4";
            this.dropDownRedEye4.Size = new System.Drawing.Size(165, 22);
            this.dropDownRedEye4.Text = "Remove Red Eye 4";
            this.dropDownRedEye4.Click += new System.EventHandler(this.dropDownRedEye4_Click);
            // 
            // dropDownDust
            // 
            this.dropDownDust.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownDust1,
            this.dropDownDust2});
            this.dropDownDust.Name = "dropDownDust";
            this.dropDownDust.Size = new System.Drawing.Size(163, 22);
            this.dropDownDust.Text = "Remove Dust";
            // 
            // dropDownDust1
            // 
            this.dropDownDust1.Name = "dropDownDust1";
            this.dropDownDust1.Size = new System.Drawing.Size(147, 22);
            this.dropDownDust1.Text = "Remove Dust 1";
            this.dropDownDust1.Click += new System.EventHandler(this.dropDownDust1_Click);
            // 
            // dropDownDust2
            // 
            this.dropDownDust2.Name = "dropDownDust2";
            this.dropDownDust2.Size = new System.Drawing.Size(147, 22);
            this.dropDownDust2.Text = "Remove Dust 2";
            this.dropDownDust2.Click += new System.EventHandler(this.dropDownDust2_Click);
            // 
            // dropDownScratch
            // 
            this.dropDownScratch.Name = "dropDownScratch";
            this.dropDownScratch.Size = new System.Drawing.Size(163, 22);
            this.dropDownScratch.Text = "Remove Scracthes";
            this.dropDownScratch.Click += new System.EventHandler(this.dropDownScratch_Click);
            // 
            // dropDownSharpen
            // 
            this.dropDownSharpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownSharpen1,
            this.dropDownSharpen2});
            this.dropDownSharpen.Name = "dropDownSharpen";
            this.dropDownSharpen.Size = new System.Drawing.Size(163, 22);
            this.dropDownSharpen.Text = "Sharpen";
            // 
            // dropDownSharpen1
            // 
            this.dropDownSharpen1.Name = "dropDownSharpen1";
            this.dropDownSharpen1.Size = new System.Drawing.Size(123, 22);
            this.dropDownSharpen1.Text = "Sharpen 1";
            this.dropDownSharpen1.Click += new System.EventHandler(this.dropDownSharpen1_Click);
            // 
            // dropDownSharpen2
            // 
            this.dropDownSharpen2.Name = "dropDownSharpen2";
            this.dropDownSharpen2.Size = new System.Drawing.Size(123, 22);
            this.dropDownSharpen2.Text = "Sharpen 2";
            this.dropDownSharpen2.Click += new System.EventHandler(this.dropDownSharpen2_Click);
            // 
            // dropDownEffects
            // 
            this.dropDownEffects.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dropDownEffects1,
            this.dropDownEffects2,
            this.dropDownEffects3});
            this.dropDownEffects.Name = "dropDownEffects";
            this.dropDownEffects.Size = new System.Drawing.Size(170, 22);
            this.dropDownEffects.Text = "Effects";
            // 
            // dropDownEffects1
            // 
            this.dropDownEffects1.Name = "dropDownEffects1";
            this.dropDownEffects1.Size = new System.Drawing.Size(117, 22);
            this.dropDownEffects1.Text = "Effects 1";
            this.dropDownEffects1.Click += new System.EventHandler(this.dropDownEffects1_Click);
            // 
            // dropDownEffects2
            // 
            this.dropDownEffects2.Name = "dropDownEffects2";
            this.dropDownEffects2.Size = new System.Drawing.Size(117, 22);
            this.dropDownEffects2.Text = "Effects 2";
            this.dropDownEffects2.Click += new System.EventHandler(this.dropDownEffects2_Click);
            // 
            // dropDownEffects3
            // 
            this.dropDownEffects3.Name = "dropDownEffects3";
            this.dropDownEffects3.Size = new System.Drawing.Size(117, 22);
            this.dropDownEffects3.Text = "Effects 3";
            this.dropDownEffects3.Click += new System.EventHandler(this.dropDownEffects3_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CloseButton.Enabled = false;
            this.CloseButton.Image = ((System.Drawing.Image)(resources.GetObject("CloseButton.Image")));
            this.CloseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(23, 22);
            this.CloseButton.Text = "Close Image";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveButton.Enabled = false;
            this.SaveButton.Image = ((System.Drawing.Image)(resources.GetObject("SaveButton.Image")));
            this.SaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(23, 22);
            this.SaveButton.Text = "Save the Image";
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // PrintButton
            // 
            this.PrintButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrintButton.Enabled = false;
            this.PrintButton.Image = ((System.Drawing.Image)(resources.GetObject("PrintButton.Image")));
            this.PrintButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(23, 22);
            this.PrintButton.Text = "toolStripButton1";
            this.PrintButton.ToolTipText = "Print Image";
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // ScanButton
            // 
            this.ScanButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ScanButton.Image = ((System.Drawing.Image)(resources.GetObject("ScanButton.Image")));
            this.ScanButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Size = new System.Drawing.Size(23, 22);
            this.ScanButton.Text = "toolStripButton2";
            this.ScanButton.ToolTipText = "Scan Image";
            this.ScanButton.Click += new System.EventHandler(this.ScanButton_Click);
            // 
            // CutButton
            // 
            this.CutButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CutButton.Enabled = false;
            this.CutButton.Image = ((System.Drawing.Image)(resources.GetObject("CutButton.Image")));
            this.CutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CutButton.Name = "CutButton";
            this.CutButton.Size = new System.Drawing.Size(23, 22);
            this.CutButton.Text = "Cut Image";
            this.CutButton.Click += new System.EventHandler(this.CutButton_Click);
            // 
            // CopyButton
            // 
            this.CopyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CopyButton.Enabled = false;
            this.CopyButton.Image = ((System.Drawing.Image)(resources.GetObject("CopyButton.Image")));
            this.CopyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(23, 22);
            this.CopyButton.Text = "Copy Image";
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // PasteButton
            // 
            this.PasteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PasteButton.Enabled = false;
            this.PasteButton.Image = ((System.Drawing.Image)(resources.GetObject("PasteButton.Image")));
            this.PasteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PasteButton.Name = "PasteButton";
            this.PasteButton.Size = new System.Drawing.Size(23, 22);
            this.PasteButton.Text = "Paste";
            this.PasteButton.Click += new System.EventHandler(this.PasteButton_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RefreshButton.Enabled = false;
            this.RefreshButton.Image = ((System.Drawing.Image)(resources.GetObject("RefreshButton.Image")));
            this.RefreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(23, 22);
            this.RefreshButton.Text = "Refresh the Image";
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // ViewingSeparator
            // 
            this.ViewingSeparator.Name = "ViewingSeparator";
            this.ViewingSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // ViewingLabel
            // 
            this.ViewingLabel.Name = "ViewingLabel";
            this.ViewingLabel.Size = new System.Drawing.Size(43, 22);
            this.ViewingLabel.Text = "Viewing";
            // 
            // ZoomFitButton
            // 
            this.ZoomFitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoomFitButton.Enabled = false;
            this.ZoomFitButton.Image = ((System.Drawing.Image)(resources.GetObject("ZoomFitButton.Image")));
            this.ZoomFitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ZoomFitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomFitButton.Name = "ZoomFitButton";
            this.ZoomFitButton.Size = new System.Drawing.Size(23, 22);
            this.ZoomFitButton.Text = "Zoom to Best Fit";
            this.ZoomFitButton.Click += new System.EventHandler(this.ZoomFitButton_Click);
            // 
            // Zoom1to1Button
            // 
            this.Zoom1to1Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Zoom1to1Button.Enabled = false;
            this.Zoom1to1Button.Image = ((System.Drawing.Image)(resources.GetObject("Zoom1to1Button.Image")));
            this.Zoom1to1Button.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Zoom1to1Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Zoom1to1Button.Name = "Zoom1to1Button";
            this.Zoom1to1Button.Size = new System.Drawing.Size(23, 22);
            this.Zoom1to1Button.Text = "Zoom to 1 to 1 Ratio";
            this.Zoom1to1Button.Click += new System.EventHandler(this.Zoom1to1Button_Click);
            // 
            // ZoomWidthButton
            // 
            this.ZoomWidthButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoomWidthButton.Enabled = false;
            this.ZoomWidthButton.Image = ((System.Drawing.Image)(resources.GetObject("ZoomWidthButton.Image")));
            this.ZoomWidthButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ZoomWidthButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomWidthButton.Name = "ZoomWidthButton";
            this.ZoomWidthButton.Size = new System.Drawing.Size(23, 22);
            this.ZoomWidthButton.Text = "Zoom To Width";
            this.ZoomWidthButton.Click += new System.EventHandler(this.ZoomWidthButton_Click);
            // 
            // ZoomHeightButton
            // 
            this.ZoomHeightButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoomHeightButton.Enabled = false;
            this.ZoomHeightButton.Image = ((System.Drawing.Image)(resources.GetObject("ZoomHeightButton.Image")));
            this.ZoomHeightButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ZoomHeightButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomHeightButton.Name = "ZoomHeightButton";
            this.ZoomHeightButton.Size = new System.Drawing.Size(23, 22);
            this.ZoomHeightButton.Text = "Zoom To Height";
            this.ZoomHeightButton.Click += new System.EventHandler(this.ZoomHeightButton_Click);
            // 
            // RotateLeftButton
            // 
            this.RotateLeftButton.CheckOnClick = true;
            this.RotateLeftButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RotateLeftButton.Enabled = false;
            this.RotateLeftButton.Image = ((System.Drawing.Image)(resources.GetObject("RotateLeftButton.Image")));
            this.RotateLeftButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.RotateLeftButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RotateLeftButton.Name = "RotateLeftButton";
            this.RotateLeftButton.Size = new System.Drawing.Size(23, 22);
            this.RotateLeftButton.Text = "Rotate Left";
            this.RotateLeftButton.Click += new System.EventHandler(this.rotateLeftButton_Click);
            // 
            // RotateRightButton
            // 
            this.RotateRightButton.CheckOnClick = true;
            this.RotateRightButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RotateRightButton.Enabled = false;
            this.RotateRightButton.Image = ((System.Drawing.Image)(resources.GetObject("RotateRightButton.Image")));
            this.RotateRightButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.RotateRightButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RotateRightButton.Name = "RotateRightButton";
            this.RotateRightButton.Size = new System.Drawing.Size(23, 22);
            this.RotateRightButton.Text = "Rotate Right";
            this.RotateRightButton.Click += new System.EventHandler(this.rotateRightButton_Click);
            // 
            // PointerIXButton
            // 
            this.PointerIXButton.Checked = true;
            this.PointerIXButton.CheckOnClick = true;
            this.PointerIXButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PointerIXButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PointerIXButton.Enabled = false;
            this.PointerIXButton.Image = ((System.Drawing.Image)(resources.GetObject("PointerIXButton.Image")));
            this.PointerIXButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PointerIXButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.PointerIXButton.Name = "PointerIXButton";
            this.PointerIXButton.Size = new System.Drawing.Size(23, 22);
            this.PointerIXButton.Text = "Pointer";
            this.PointerIXButton.Click += new System.EventHandler(this.PointerIXButton_Click);
            // 
            // ZoomInButton
            // 
            this.ZoomInButton.CheckOnClick = true;
            this.ZoomInButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoomInButton.Enabled = false;
            this.ZoomInButton.Image = ((System.Drawing.Image)(resources.GetObject("ZoomInButton.Image")));
            this.ZoomInButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ZoomInButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.ZoomInButton.Name = "ZoomInButton";
            this.ZoomInButton.Size = new System.Drawing.Size(23, 22);
            this.ZoomInButton.Text = "Zoom In";
            this.ZoomInButton.Click += new System.EventHandler(this.ZoomInButton_Click);
            // 
            // ZoomOutButton
            // 
            this.ZoomOutButton.CheckOnClick = true;
            this.ZoomOutButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoomOutButton.Enabled = false;
            this.ZoomOutButton.Image = ((System.Drawing.Image)(resources.GetObject("ZoomOutButton.Image")));
            this.ZoomOutButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ZoomOutButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.ZoomOutButton.Name = "ZoomOutButton";
            this.ZoomOutButton.Size = new System.Drawing.Size(23, 22);
            this.ZoomOutButton.Text = "Zoom Out";
            this.ZoomOutButton.Click += new System.EventHandler(this.ZoomOutButton_Click);
            // 
            // ZoomRectangleButton
            // 
            this.ZoomRectangleButton.CheckOnClick = true;
            this.ZoomRectangleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ZoomRectangleButton.Enabled = false;
            this.ZoomRectangleButton.Image = ((System.Drawing.Image)(resources.GetObject("ZoomRectangleButton.Image")));
            this.ZoomRectangleButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ZoomRectangleButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.ZoomRectangleButton.Name = "ZoomRectangleButton";
            this.ZoomRectangleButton.Size = new System.Drawing.Size(23, 22);
            this.ZoomRectangleButton.Text = "Zoom to Rectangle";
            this.ZoomRectangleButton.Click += new System.EventHandler(this.ZoomRectangleButton_Click);
            // 
            // HandButton
            // 
            this.HandButton.CheckOnClick = true;
            this.HandButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HandButton.Enabled = false;
            this.HandButton.Image = ((System.Drawing.Image)(resources.GetObject("HandButton.Image")));
            this.HandButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.HandButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.HandButton.Name = "HandButton";
            this.HandButton.Size = new System.Drawing.Size(23, 22);
            this.HandButton.Text = "Pan Image";
            this.HandButton.Click += new System.EventHandler(this.HandButton_Click);
            // 
            // AreaButton
            // 
            this.AreaButton.CheckOnClick = true;
            this.AreaButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AreaButton.Enabled = false;
            this.AreaButton.Image = ((System.Drawing.Image)(resources.GetObject("AreaButton.Image")));
            this.AreaButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.AreaButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.AreaButton.Name = "AreaButton";
            this.AreaButton.Size = new System.Drawing.Size(23, 22);
            this.AreaButton.Text = "Select Area";
            this.AreaButton.Click += new System.EventHandler(this.AreaButton_Click);
            // 
            // MagnifierButton
            // 
            this.MagnifierButton.CheckOnClick = true;
            this.MagnifierButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MagnifierButton.Enabled = false;
            this.MagnifierButton.Image = ((System.Drawing.Image)(resources.GetObject("MagnifierButton.Image")));
            this.MagnifierButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MagnifierButton.ImageTransparentColor = System.Drawing.Color.Silver;
            this.MagnifierButton.Name = "MagnifierButton";
            this.MagnifierButton.Size = new System.Drawing.Size(23, 22);
            this.MagnifierButton.Text = "Magnify Image";
            this.MagnifierButton.Click += new System.EventHandler(this.MagnifierButton_Click);
            // 
            // AnnotationSeparator
            // 
            this.AnnotationSeparator.Name = "AnnotationSeparator";
            this.AnnotationSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // AnnotationsToolStrip
            // 
            this.AnnotationsToolStrip.Name = "AnnotationsToolStrip";
            this.AnnotationsToolStrip.Size = new System.Drawing.Size(65, 22);
            this.AnnotationsToolStrip.Text = "Annotations";
            // 
            // PointerNXButton
            // 
            this.PointerNXButton.Checked = true;
            this.PointerNXButton.CheckOnClick = true;
            this.PointerNXButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PointerNXButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PointerNXButton.Enabled = false;
            this.PointerNXButton.Image = ((System.Drawing.Image)(resources.GetObject("PointerNXButton.Image")));
            this.PointerNXButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PointerNXButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PointerNXButton.Name = "PointerNXButton";
            this.PointerNXButton.Size = new System.Drawing.Size(23, 22);
            this.PointerNXButton.Text = "Select Annotation";
            this.PointerNXButton.Click += new System.EventHandler(this.PointerNXButton_Click);
            // 
            // NoteButton
            // 
            this.NoteButton.CheckOnClick = true;
            this.NoteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NoteButton.Enabled = false;
            this.NoteButton.Image = ((System.Drawing.Image)(resources.GetObject("NoteButton.Image")));
            this.NoteButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.NoteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NoteButton.Name = "NoteButton";
            this.NoteButton.Size = new System.Drawing.Size(23, 22);
            this.NoteButton.Text = "Note Annotation";
            this.NoteButton.Click += new System.EventHandler(this.NoteButton_Click);
            // 
            // TextButton
            // 
            this.TextButton.CheckOnClick = true;
            this.TextButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TextButton.Enabled = false;
            this.TextButton.Image = ((System.Drawing.Image)(resources.GetObject("TextButton.Image")));
            this.TextButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TextButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TextButton.Name = "TextButton";
            this.TextButton.Size = new System.Drawing.Size(23, 22);
            this.TextButton.Text = "Text Annotation";
            this.TextButton.Click += new System.EventHandler(this.TextButton_Click);
            // 
            // RectangleButton
            // 
            this.RectangleButton.CheckOnClick = true;
            this.RectangleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RectangleButton.Enabled = false;
            this.RectangleButton.Image = ((System.Drawing.Image)(resources.GetObject("RectangleButton.Image")));
            this.RectangleButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.RectangleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RectangleButton.Name = "RectangleButton";
            this.RectangleButton.Size = new System.Drawing.Size(23, 22);
            this.RectangleButton.Text = "Rectangle Annotation";
            this.RectangleButton.Click += new System.EventHandler(this.RectangleButton_Click);
            // 
            // PolygonButton
            // 
            this.PolygonButton.CheckOnClick = true;
            this.PolygonButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PolygonButton.Enabled = false;
            this.PolygonButton.Image = ((System.Drawing.Image)(resources.GetObject("PolygonButton.Image")));
            this.PolygonButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PolygonButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PolygonButton.Name = "PolygonButton";
            this.PolygonButton.Size = new System.Drawing.Size(23, 22);
            this.PolygonButton.Text = "Polygon Annotation";
            this.PolygonButton.Click += new System.EventHandler(this.PolygonButton_Click);
            // 
            // PolylineButton
            // 
            this.PolylineButton.CheckOnClick = true;
            this.PolylineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PolylineButton.Enabled = false;
            this.PolylineButton.Image = ((System.Drawing.Image)(resources.GetObject("PolylineButton.Image")));
            this.PolylineButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.PolylineButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PolylineButton.Name = "PolylineButton";
            this.PolylineButton.Size = new System.Drawing.Size(23, 22);
            this.PolylineButton.Text = "Polyline Annotation";
            this.PolylineButton.Click += new System.EventHandler(this.PolylineButton_Click);
            // 
            // LineButton
            // 
            this.LineButton.CheckOnClick = true;
            this.LineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LineButton.Enabled = false;
            this.LineButton.Image = ((System.Drawing.Image)(resources.GetObject("LineButton.Image")));
            this.LineButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.LineButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LineButton.Name = "LineButton";
            this.LineButton.Size = new System.Drawing.Size(23, 22);
            this.LineButton.Text = "Line Annotation";
            this.LineButton.Click += new System.EventHandler(this.LineButton_Click);
            // 
            // FreehandButton
            // 
            this.FreehandButton.CheckOnClick = true;
            this.FreehandButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.FreehandButton.Enabled = false;
            this.FreehandButton.Image = ((System.Drawing.Image)(resources.GetObject("FreehandButton.Image")));
            this.FreehandButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.FreehandButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FreehandButton.Name = "FreehandButton";
            this.FreehandButton.Size = new System.Drawing.Size(23, 22);
            this.FreehandButton.Text = "Freehand Annotation";
            this.FreehandButton.Click += new System.EventHandler(this.FreehandButton_Click);
            // 
            // StampButton
            // 
            this.StampButton.CheckOnClick = true;
            this.StampButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StampButton.Enabled = false;
            this.StampButton.Image = ((System.Drawing.Image)(resources.GetObject("StampButton.Image")));
            this.StampButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.StampButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StampButton.Name = "StampButton";
            this.StampButton.Size = new System.Drawing.Size(23, 22);
            this.StampButton.Text = "Stamp Annotation";
            this.StampButton.Click += new System.EventHandler(this.StampButton_Click);
            // 
            // EllipseButton
            // 
            this.EllipseButton.CheckOnClick = true;
            this.EllipseButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EllipseButton.Enabled = false;
            this.EllipseButton.Image = ((System.Drawing.Image)(resources.GetObject("EllipseButton.Image")));
            this.EllipseButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.EllipseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EllipseButton.Name = "EllipseButton";
            this.EllipseButton.Size = new System.Drawing.Size(23, 22);
            this.EllipseButton.Text = "Ellipse Annotation";
            this.EllipseButton.Click += new System.EventHandler(this.EllipseButton_Click);
            // 
            // RulerButton
            // 
            this.RulerButton.CheckOnClick = true;
            this.RulerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RulerButton.Enabled = false;
            this.RulerButton.Image = ((System.Drawing.Image)(resources.GetObject("RulerButton.Image")));
            this.RulerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.RulerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RulerButton.Name = "RulerButton";
            this.RulerButton.Size = new System.Drawing.Size(23, 22);
            this.RulerButton.Text = "Ruler Annotation";
            this.RulerButton.Click += new System.EventHandler(this.RulerButton_Click);
            // 
            // ImageButton
            // 
            this.ImageButton.CheckOnClick = true;
            this.ImageButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ImageButton.Enabled = false;
            this.ImageButton.Image = ((System.Drawing.Image)(resources.GetObject("ImageButton.Image")));
            this.ImageButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ImageButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ImageButton.Name = "ImageButton";
            this.ImageButton.Size = new System.Drawing.Size(23, 22);
            this.ImageButton.Text = "Image Annotation";
            this.ImageButton.Click += new System.EventHandler(this.ImageButton_Click);
            // 
            // HighlightButton
            // 
            this.HighlightButton.CheckOnClick = true;
            this.HighlightButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HighlightButton.Enabled = false;
            this.HighlightButton.Image = ((System.Drawing.Image)(resources.GetObject("HighlightButton.Image")));
            this.HighlightButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.HighlightButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HighlightButton.Name = "HighlightButton";
            this.HighlightButton.Size = new System.Drawing.Size(23, 22);
            this.HighlightButton.Text = "Highlight Annotation";
            this.HighlightButton.Click += new System.EventHandler(this.HighlightButton_Click);
            // 
            // ButtonButton
            // 
            this.ButtonButton.CheckOnClick = true;
            this.ButtonButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ButtonButton.Enabled = false;
            this.ButtonButton.Image = ((System.Drawing.Image)(resources.GetObject("ButtonButton.Image")));
            this.ButtonButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ButtonButton.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(159)))), ((int)(((byte)(170)))));
            this.ButtonButton.Name = "ButtonButton";
            this.ButtonButton.Size = new System.Drawing.Size(23, 22);
            this.ButtonButton.Text = "Button Annotation";
            this.ButtonButton.Click += new System.EventHandler(this.ButtonButton_Click);
            // 
            // BrandButton
            // 
            this.BrandButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BrandButton.Enabled = false;
            this.BrandButton.Image = ((System.Drawing.Image)(resources.GetObject("BrandButton.Image")));
            this.BrandButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.BrandButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BrandButton.Name = "BrandButton";
            this.BrandButton.Size = new System.Drawing.Size(23, 22);
            this.BrandButton.Text = "Brand Annotations into Image";
            this.BrandButton.Click += new System.EventHandler(this.BrandButton_Click);
            // 
            // PagingSeparator
            // 
            this.PagingSeparator.Name = "PagingSeparator";
            this.PagingSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // PageLabel
            // 
            this.PageLabel.Name = "PageLabel";
            this.PageLabel.Size = new System.Drawing.Size(31, 22);
            this.PageLabel.Text = "Page";
            // 
            // PageBox
            // 
            this.PageBox.Enabled = false;
            this.PageBox.Name = "PageBox";
            this.PageBox.Size = new System.Drawing.Size(25, 25);
            this.PageBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PageBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PageBox_KeyPress);
            // 
            // PageCountLabel
            // 
            this.PageCountLabel.Name = "PageCountLabel";
            this.PageCountLabel.Size = new System.Drawing.Size(0, 22);
            // 
            // PreviousButton
            // 
            this.PreviousButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PreviousButton.Enabled = false;
            this.PreviousButton.Image = ((System.Drawing.Image)(resources.GetObject("PreviousButton.Image")));
            this.PreviousButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PreviousButton.Name = "PreviousButton";
            this.PreviousButton.Size = new System.Drawing.Size(23, 22);
            this.PreviousButton.Text = "Previous Page";
            this.PreviousButton.Click += new System.EventHandler(this.PreviousButton_Click);
            // 
            // NextButton
            // 
            this.NextButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NextButton.Enabled = false;
            this.NextButton.Image = ((System.Drawing.Image)(resources.GetObject("NextButton.Image")));
            this.NextButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(23, 22);
            this.NextButton.Text = "Next Page";
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // HelpSeparator
            // 
            this.HelpSeparator.Name = "HelpSeparator";
            this.HelpSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // HelpInfoButton
            // 
            this.HelpInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.HelpInfoButton.Image = ((System.Drawing.Image)(resources.GetObject("HelpInfoButton.Image")));
            this.HelpInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HelpInfoButton.Name = "HelpInfoButton";
            this.HelpInfoButton.Size = new System.Drawing.Size(23, 22);
            this.HelpInfoButton.Text = "Help";
            this.HelpInfoButton.Click += new System.EventHandler(this.HelpInfoButton_Click);
            // 
            // Statusbar
            // 
            this.Statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MousePositionLabel});
            this.Statusbar.Location = new System.Drawing.Point(0, 660);
            this.Statusbar.Name = "Statusbar";
            this.Statusbar.Size = new System.Drawing.Size(1169, 22);
            this.Statusbar.TabIndex = 3;
            this.Statusbar.Text = "statusStrip1";
            // 
            // MousePositionLabel
            // 
            this.MousePositionLabel.Name = "MousePositionLabel";
            this.MousePositionLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // PanelThumbnail
            // 
            this.PanelThumbnail.Controls.Add(this.PanelSplitThumbnailDrillDown);
            this.PanelThumbnail.Controls.Add(this.MiddleSplitter);
            this.PanelThumbnail.Controls.Add(this.PanelProperties);
            this.PanelThumbnail.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelThumbnail.Location = new System.Drawing.Point(0, 49);
            this.PanelThumbnail.Name = "PanelThumbnail";
            this.PanelThumbnail.Size = new System.Drawing.Size(354, 611);
            this.PanelThumbnail.TabIndex = 4;
            // 
            // PanelSplitThumbnailDrillDown
            // 
            this.PanelSplitThumbnailDrillDown.Controls.Add(this.thumbnailXpressDrillDown);
            this.PanelSplitThumbnailDrillDown.Controls.Add(this.PanelBottomThumb);
            this.PanelSplitThumbnailDrillDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelSplitThumbnailDrillDown.Location = new System.Drawing.Point(0, 0);
            this.PanelSplitThumbnailDrillDown.Name = "PanelSplitThumbnailDrillDown";
            this.PanelSplitThumbnailDrillDown.Size = new System.Drawing.Size(354, 309);
            this.PanelSplitThumbnailDrillDown.TabIndex = 4;
            // 
            // thumbnailXpressDrillDown
            // 
            this.thumbnailXpressDrillDown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpressDrillDown.BottomMargin = 5;
            this.thumbnailXpressDrillDown.CameraRaw = false;
            this.thumbnailXpressDrillDown.CellBorderColor = System.Drawing.Color.Black;
            this.thumbnailXpressDrillDown.CellBorderWidth = 1;
            this.thumbnailXpressDrillDown.CellHeight = 80;
            this.thumbnailXpressDrillDown.CellHorizontalSpacing = 5;
            this.thumbnailXpressDrillDown.CellSpacingColor = System.Drawing.Color.White;
            this.thumbnailXpressDrillDown.CellVerticalSpacing = 5;
            this.thumbnailXpressDrillDown.CellWidth = 80;
            this.thumbnailXpressDrillDown.DblClickDirectoryDrillDown = true;
            this.thumbnailXpressDrillDown.DescriptorAlignment = ((PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments)((PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignLeft | PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpressDrillDown.DescriptorDisplayMethod = PegasusImaging.WinForms.ThumbnailXpress2.DescriptorDisplayMethods.Default;
            this.thumbnailXpressDrillDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumbnailXpressDrillDown.EnableAsDragSourceForExternalDragDrop = false;
            this.thumbnailXpressDrillDown.EnableAsDropTargetForExternalDragDrop = false;
            this.thumbnailXpressDrillDown.ErrorAction = PegasusImaging.WinForms.ThumbnailXpress2.ErrorAction.UseErrorIcon;
            this.thumbnailXpressDrillDown.FtpPassword = "";
            this.thumbnailXpressDrillDown.FtpUserName = "";
            this.thumbnailXpressDrillDown.InterComponentThumbnailDragDropEnabled = false;
            this.thumbnailXpressDrillDown.IntraComponentThumbnailDragDropEnabled = false;
            this.thumbnailXpressDrillDown.LeftMargin = 5;
            this.thumbnailXpressDrillDown.Location = new System.Drawing.Point(0, 0);
            this.thumbnailXpressDrillDown.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpressDrillDown.Name = "thumbnailXpressDrillDown";
            this.thumbnailXpressDrillDown.PreserveBlack = false;
            this.thumbnailXpressDrillDown.ProxyServer = "";
            this.thumbnailXpressDrillDown.RightMargin = 5;
            this.thumbnailXpressDrillDown.ScrollDirection = PegasusImaging.WinForms.ThumbnailXpress2.ScrollDirection.Vertical;
            this.thumbnailXpressDrillDown.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.thumbnailXpressDrillDown.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.Single;
            this.thumbnailXpressDrillDown.ShowHourglass = true;
            this.thumbnailXpressDrillDown.ShowImagePlaceholders = false;
            this.thumbnailXpressDrillDown.Size = new System.Drawing.Size(354, 262);
            this.thumbnailXpressDrillDown.TabIndex = 0;
            this.thumbnailXpressDrillDown.TextBackColor = System.Drawing.Color.White;
            this.thumbnailXpressDrillDown.TopMargin = 5;
            this.thumbnailXpressDrillDown.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.thumbnailXpressDrillDown_MouseDoubleClick);
            this.thumbnailXpressDrillDown.MouseClick += new System.Windows.Forms.MouseEventHandler(this.thumbnailXpressDrillDown_MouseClick);
            // 
            // PanelBottomThumb
            // 
            this.PanelBottomThumb.Controls.Add(this.thumbnailTextBox);
            this.PanelBottomThumb.Controls.Add(this.ClearThumbButton);
            this.PanelBottomThumb.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBottomThumb.Location = new System.Drawing.Point(0, 262);
            this.PanelBottomThumb.Name = "PanelBottomThumb";
            this.PanelBottomThumb.Size = new System.Drawing.Size(354, 47);
            this.PanelBottomThumb.TabIndex = 6;
            // 
            // thumbnailTextBox
            // 
            this.thumbnailTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumbnailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thumbnailTextBox.Location = new System.Drawing.Point(0, 0);
            this.thumbnailTextBox.Name = "thumbnailTextBox";
            this.thumbnailTextBox.ReadOnly = true;
            this.thumbnailTextBox.Size = new System.Drawing.Size(354, 20);
            this.thumbnailTextBox.TabIndex = 0;
            this.thumbnailTextBox.Text = "Recent Images";
            // 
            // ClearThumbButton
            // 
            this.ClearThumbButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ClearThumbButton.Location = new System.Drawing.Point(0, 20);
            this.ClearThumbButton.Name = "ClearThumbButton";
            this.ClearThumbButton.Size = new System.Drawing.Size(354, 27);
            this.ClearThumbButton.TabIndex = 1;
            this.ClearThumbButton.Text = "Clear All Thumbnails && Image";
            this.ClearThumbButton.UseVisualStyleBackColor = true;
            this.ClearThumbButton.Click += new System.EventHandler(this.ClearThumbButton_Click);
            // 
            // MiddleSplitter
            // 
            this.MiddleSplitter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MiddleSplitter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MiddleSplitter.Location = new System.Drawing.Point(0, 309);
            this.MiddleSplitter.Name = "MiddleSplitter";
            this.MiddleSplitter.Size = new System.Drawing.Size(354, 10);
            this.MiddleSplitter.TabIndex = 0;
            this.MiddleSplitter.TabStop = false;
            // 
            // PanelProperties
            // 
            this.PanelProperties.Controls.Add(this.TabRoll);
            this.PanelProperties.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelProperties.Location = new System.Drawing.Point(0, 319);
            this.PanelProperties.Name = "PanelProperties";
            this.PanelProperties.Size = new System.Drawing.Size(354, 292);
            this.PanelProperties.TabIndex = 1;
            // 
            // TabRoll
            // 
            this.TabRoll.Controls.Add(this.InfoTab);
            this.TabRoll.Controls.Add(this.ViewTab);
            this.TabRoll.Controls.Add(this.ThumbTab);
            this.TabRoll.Controls.Add(this.ColorTab);
            this.TabRoll.Controls.Add(this.LayersCadTab);
            this.TabRoll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabRoll.Location = new System.Drawing.Point(0, 0);
            this.TabRoll.Name = "TabRoll";
            this.TabRoll.SelectedIndex = 0;
            this.TabRoll.Size = new System.Drawing.Size(354, 292);
            this.TabRoll.TabIndex = 0;
            this.TabRoll.Click += new System.EventHandler(this.TabRoll_Click);
            // 
            // InfoTab
            // 
            this.InfoTab.Controls.Add(this.panelImageInfoGrid);
            this.InfoTab.Controls.Add(this.panelMetaData);
            this.InfoTab.Location = new System.Drawing.Point(4, 22);
            this.InfoTab.Name = "InfoTab";
            this.InfoTab.Padding = new System.Windows.Forms.Padding(3);
            this.InfoTab.Size = new System.Drawing.Size(346, 266);
            this.InfoTab.TabIndex = 0;
            this.InfoTab.Text = "Image Info";
            this.InfoTab.UseVisualStyleBackColor = true;
            // 
            // panelImageInfoGrid
            // 
            this.panelImageInfoGrid.Controls.Add(this.dataGridImageInfo);
            this.panelImageInfoGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelImageInfoGrid.Location = new System.Drawing.Point(3, 3);
            this.panelImageInfoGrid.Name = "panelImageInfoGrid";
            this.panelImageInfoGrid.Size = new System.Drawing.Size(340, 228);
            this.panelImageInfoGrid.TabIndex = 1;
            // 
            // dataGridImageInfo
            // 
            this.dataGridImageInfo.AllowUserToAddRows = false;
            this.dataGridImageInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridImageInfo.ColumnHeadersVisible = false;
            this.dataGridImageInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dataGridImageInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridImageInfo.Location = new System.Drawing.Point(0, 0);
            this.dataGridImageInfo.Name = "dataGridImageInfo";
            this.dataGridImageInfo.ReadOnly = true;
            this.dataGridImageInfo.Size = new System.Drawing.Size(340, 228);
            this.dataGridImageInfo.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 5;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.HeaderText = "";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 5;
            // 
            // panelMetaData
            // 
            this.panelMetaData.Controls.Add(this.metaDataButton);
            this.panelMetaData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelMetaData.Location = new System.Drawing.Point(3, 231);
            this.panelMetaData.Name = "panelMetaData";
            this.panelMetaData.Size = new System.Drawing.Size(340, 32);
            this.panelMetaData.TabIndex = 0;
            // 
            // metaDataButton
            // 
            this.metaDataButton.Enabled = false;
            this.metaDataButton.Location = new System.Drawing.Point(122, 6);
            this.metaDataButton.Name = "metaDataButton";
            this.metaDataButton.Size = new System.Drawing.Size(75, 23);
            this.metaDataButton.TabIndex = 0;
            this.metaDataButton.Text = "MetaData";
            this.metaDataButton.UseVisualStyleBackColor = true;
            this.metaDataButton.Click += new System.EventHandler(this.metaDataButton_Click);
            // 
            // ViewTab
            // 
            this.ViewTab.AutoScroll = true;
            this.ViewTab.Controls.Add(this.targetButton);
            this.ViewTab.Controls.Add(this.monitorButton);
            this.ViewTab.Controls.Add(this.printerProfileButton);
            this.ViewTab.Controls.Add(this.PaletteFileButton);
            this.ViewTab.Controls.Add(this.zoomOutLabel);
            this.ViewTab.Controls.Add(this.zoomOutNumericUpDown);
            this.ViewTab.Controls.Add(this.zoomInLabel);
            this.ViewTab.Controls.Add(this.zoomInNumericUpDown);
            this.ViewTab.Controls.Add(this.magHeightLabel);
            this.ViewTab.Controls.Add(this.magHeightNumericUpDown);
            this.ViewTab.Controls.Add(this.magnificationLabel);
            this.ViewTab.Controls.Add(this.magFactorNumericUpDown);
            this.ViewTab.Controls.Add(this.magWidthLabel);
            this.ViewTab.Controls.Add(this.magWidthNumericUpDown);
            this.ViewTab.Controls.Add(this.backColorButton);
            this.ViewTab.Controls.Add(this.backColorLabel);
            this.ViewTab.Controls.Add(this.aspectXLabel);
            this.ViewTab.Controls.Add(this.aspectXNumericUpDown);
            this.ViewTab.Controls.Add(this.aspectYLabel);
            this.ViewTab.Controls.Add(this.aspectYNumericUpDown);
            this.ViewTab.Controls.Add(this.alphaCheckBox);
            this.ViewTab.Controls.Add(this.RenderLabel);
            this.ViewTab.Controls.Add(this.renderComboBox);
            this.ViewTab.Controls.Add(this.DisplayLabel);
            this.ViewTab.Controls.Add(this.displayComboBox);
            this.ViewTab.Controls.Add(this.proofCheckBox);
            this.ViewTab.Controls.Add(this.ICMCheckBox);
            this.ViewTab.Controls.Add(this.PaletteLabel);
            this.ViewTab.Controls.Add(this.paletteComboBox);
            this.ViewTab.Controls.Add(this.progressiveCheckBox);
            this.ViewTab.Controls.Add(this.interceptLabel);
            this.ViewTab.Controls.Add(this.SlopeLabel);
            this.ViewTab.Controls.Add(this.interceptNumericUpDown);
            this.ViewTab.Controls.Add(this.slopeNumericUpDown);
            this.ViewTab.Controls.Add(this.grayComboBox);
            this.ViewTab.Controls.Add(this.VerticalComboBox);
            this.ViewTab.Controls.Add(this.HorzComboBox);
            this.ViewTab.Controls.Add(this.HorzLabel);
            this.ViewTab.Controls.Add(this.VerticalLabel);
            this.ViewTab.Controls.Add(this.ZoomLabel);
            this.ViewTab.Controls.Add(this.zoomNumericUpDown);
            this.ViewTab.Controls.Add(this.smoothCheckBox);
            this.ViewTab.Controls.Add(this.preserveCheckBox);
            this.ViewTab.Controls.Add(this.ditherCheckBox);
            this.ViewTab.Controls.Add(this.antiCheckBox);
            this.ViewTab.Controls.Add(this.GrayLabel);
            this.ViewTab.Controls.Add(this.ResetButton);
            this.ViewTab.Controls.Add(this.BrightValueLabel);
            this.ViewTab.Controls.Add(this.ConValueLabel);
            this.ViewTab.Controls.Add(this.ContrastLabel);
            this.ViewTab.Controls.Add(this.BrightnessLabel);
            this.ViewTab.Controls.Add(this.ConBar);
            this.ViewTab.Controls.Add(this.BrightBar);
            this.ViewTab.Location = new System.Drawing.Point(4, 22);
            this.ViewTab.Name = "ViewTab";
            this.ViewTab.Size = new System.Drawing.Size(346, 266);
            this.ViewTab.TabIndex = 1;
            this.ViewTab.Text = "View";
            this.ViewTab.UseVisualStyleBackColor = true;
            // 
            // targetButton
            // 
            this.targetButton.Location = new System.Drawing.Point(140, 642);
            this.targetButton.Name = "targetButton";
            this.targetButton.Size = new System.Drawing.Size(125, 23);
            this.targetButton.TabIndex = 58;
            this.targetButton.Text = "Target Profile Name";
            this.targetButton.UseVisualStyleBackColor = true;
            this.targetButton.Click += new System.EventHandler(this.targetButton_Click);
            // 
            // monitorButton
            // 
            this.monitorButton.Location = new System.Drawing.Point(140, 613);
            this.monitorButton.Name = "monitorButton";
            this.monitorButton.Size = new System.Drawing.Size(125, 23);
            this.monitorButton.TabIndex = 57;
            this.monitorButton.Text = "Monitor Profile Name";
            this.monitorButton.UseVisualStyleBackColor = true;
            this.monitorButton.Click += new System.EventHandler(this.monitorButton_Click);
            // 
            // printerProfileButton
            // 
            this.printerProfileButton.Location = new System.Drawing.Point(4, 642);
            this.printerProfileButton.Name = "printerProfileButton";
            this.printerProfileButton.Size = new System.Drawing.Size(125, 23);
            this.printerProfileButton.TabIndex = 56;
            this.printerProfileButton.Text = "Printer Profile Name";
            this.printerProfileButton.UseVisualStyleBackColor = true;
            this.printerProfileButton.Click += new System.EventHandler(this.printerProfileButton_Click);
            // 
            // PaletteFileButton
            // 
            this.PaletteFileButton.Location = new System.Drawing.Point(5, 613);
            this.PaletteFileButton.Name = "PaletteFileButton";
            this.PaletteFileButton.Size = new System.Drawing.Size(125, 23);
            this.PaletteFileButton.TabIndex = 55;
            this.PaletteFileButton.Text = "Palette  FileName";
            this.PaletteFileButton.UseVisualStyleBackColor = true;
            this.PaletteFileButton.Click += new System.EventHandler(this.PaletteFileButton_Click);
            // 
            // zoomOutLabel
            // 
            this.zoomOutLabel.AutoSize = true;
            this.zoomOutLabel.Location = new System.Drawing.Point(5, 589);
            this.zoomOutLabel.Name = "zoomOutLabel";
            this.zoomOutLabel.Size = new System.Drawing.Size(114, 13);
            this.zoomOutLabel.TabIndex = 54;
            this.zoomOutLabel.Text = "Zoom Out Tool Factor:";
            // 
            // zoomOutNumericUpDown
            // 
            this.zoomOutNumericUpDown.Location = new System.Drawing.Point(157, 587);
            this.zoomOutNumericUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.zoomOutNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.zoomOutNumericUpDown.Name = "zoomOutNumericUpDown";
            this.zoomOutNumericUpDown.Size = new System.Drawing.Size(58, 20);
            this.zoomOutNumericUpDown.TabIndex = 53;
            this.zoomOutNumericUpDown.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.zoomOutNumericUpDown.ValueChanged += new System.EventHandler(this.zoomOutNumericUpDown_ValueChanged);
            // 
            // zoomInLabel
            // 
            this.zoomInLabel.AutoSize = true;
            this.zoomInLabel.Location = new System.Drawing.Point(6, 562);
            this.zoomInLabel.Name = "zoomInLabel";
            this.zoomInLabel.Size = new System.Drawing.Size(106, 13);
            this.zoomInLabel.TabIndex = 52;
            this.zoomInLabel.Text = "Zoom In Tool Factor:";
            // 
            // zoomInNumericUpDown
            // 
            this.zoomInNumericUpDown.Location = new System.Drawing.Point(157, 560);
            this.zoomInNumericUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.zoomInNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.zoomInNumericUpDown.Name = "zoomInNumericUpDown";
            this.zoomInNumericUpDown.Size = new System.Drawing.Size(58, 20);
            this.zoomInNumericUpDown.TabIndex = 51;
            this.zoomInNumericUpDown.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.zoomInNumericUpDown.ValueChanged += new System.EventHandler(this.zoomInNumericUpDown_ValueChanged);
            // 
            // magHeightLabel
            // 
            this.magHeightLabel.AutoSize = true;
            this.magHeightLabel.Location = new System.Drawing.Point(8, 511);
            this.magHeightLabel.Name = "magHeightLabel";
            this.magHeightLabel.Size = new System.Drawing.Size(111, 13);
            this.magHeightLabel.TabIndex = 50;
            this.magHeightLabel.Text = "Magnifier Tool Height:";
            // 
            // magHeightNumericUpDown
            // 
            this.magHeightNumericUpDown.Location = new System.Drawing.Point(157, 509);
            this.magHeightNumericUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.magHeightNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.magHeightNumericUpDown.Name = "magHeightNumericUpDown";
            this.magHeightNumericUpDown.Size = new System.Drawing.Size(58, 20);
            this.magHeightNumericUpDown.TabIndex = 49;
            this.magHeightNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.magHeightNumericUpDown.ValueChanged += new System.EventHandler(this.magHeightNumericUpDown_ValueChanged);
            // 
            // magnificationLabel
            // 
            this.magnificationLabel.AutoSize = true;
            this.magnificationLabel.Location = new System.Drawing.Point(6, 536);
            this.magnificationLabel.Name = "magnificationLabel";
            this.magnificationLabel.Size = new System.Drawing.Size(174, 13);
            this.magnificationLabel.TabIndex = 48;
            this.magnificationLabel.Text = "Magnifier Tool Magnfication Factor:";
            // 
            // magFactorNumericUpDown
            // 
            this.magFactorNumericUpDown.Location = new System.Drawing.Point(186, 534);
            this.magFactorNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.magFactorNumericUpDown.Name = "magFactorNumericUpDown";
            this.magFactorNumericUpDown.Size = new System.Drawing.Size(58, 20);
            this.magFactorNumericUpDown.TabIndex = 47;
            this.magFactorNumericUpDown.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.magFactorNumericUpDown.ValueChanged += new System.EventHandler(this.magFactorNumericUpDown_ValueChanged);
            // 
            // magWidthLabel
            // 
            this.magWidthLabel.AutoSize = true;
            this.magWidthLabel.Location = new System.Drawing.Point(8, 486);
            this.magWidthLabel.Name = "magWidthLabel";
            this.magWidthLabel.Size = new System.Drawing.Size(108, 13);
            this.magWidthLabel.TabIndex = 46;
            this.magWidthLabel.Text = "Magnifier Tool Width:";
            // 
            // magWidthNumericUpDown
            // 
            this.magWidthNumericUpDown.Location = new System.Drawing.Point(157, 482);
            this.magWidthNumericUpDown.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.magWidthNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.magWidthNumericUpDown.Name = "magWidthNumericUpDown";
            this.magWidthNumericUpDown.Size = new System.Drawing.Size(58, 20);
            this.magWidthNumericUpDown.TabIndex = 45;
            this.magWidthNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.magWidthNumericUpDown.ValueChanged += new System.EventHandler(this.magWidthNumericUpDown_ValueChanged);
            // 
            // backColorButton
            // 
            this.backColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backColorButton.Location = new System.Drawing.Point(77, 132);
            this.backColorButton.Name = "backColorButton";
            this.backColorButton.Size = new System.Drawing.Size(28, 23);
            this.backColorButton.TabIndex = 43;
            this.backColorButton.UseVisualStyleBackColor = true;
            this.backColorButton.Click += new System.EventHandler(this.backColorButton_Click);
            // 
            // backColorLabel
            // 
            this.backColorLabel.AutoSize = true;
            this.backColorLabel.Location = new System.Drawing.Point(9, 137);
            this.backColorLabel.Name = "backColorLabel";
            this.backColorLabel.Size = new System.Drawing.Size(62, 13);
            this.backColorLabel.TabIndex = 44;
            this.backColorLabel.Text = "Back Color:";
            // 
            // aspectXLabel
            // 
            this.aspectXLabel.AutoSize = true;
            this.aspectXLabel.Location = new System.Drawing.Point(174, 107);
            this.aspectXLabel.Name = "aspectXLabel";
            this.aspectXLabel.Size = new System.Drawing.Size(53, 13);
            this.aspectXLabel.TabIndex = 42;
            this.aspectXLabel.Text = "Aspect X:";
            // 
            // aspectXNumericUpDown
            // 
            this.aspectXNumericUpDown.Location = new System.Drawing.Point(233, 103);
            this.aspectXNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.aspectXNumericUpDown.Name = "aspectXNumericUpDown";
            this.aspectXNumericUpDown.Size = new System.Drawing.Size(62, 20);
            this.aspectXNumericUpDown.TabIndex = 41;
            this.aspectXNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.aspectXNumericUpDown.ValueChanged += new System.EventHandler(this.aspectXNumericUpDown_ValueChanged);
            // 
            // aspectYLabel
            // 
            this.aspectYLabel.AutoSize = true;
            this.aspectYLabel.Location = new System.Drawing.Point(173, 130);
            this.aspectYLabel.Name = "aspectYLabel";
            this.aspectYLabel.Size = new System.Drawing.Size(53, 13);
            this.aspectYLabel.TabIndex = 40;
            this.aspectYLabel.Text = "Aspect Y:";
            // 
            // aspectYNumericUpDown
            // 
            this.aspectYNumericUpDown.Location = new System.Drawing.Point(233, 129);
            this.aspectYNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.aspectYNumericUpDown.Name = "aspectYNumericUpDown";
            this.aspectYNumericUpDown.Size = new System.Drawing.Size(62, 20);
            this.aspectYNumericUpDown.TabIndex = 39;
            this.aspectYNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.aspectYNumericUpDown.ValueChanged += new System.EventHandler(this.aspectYNumericUpDown_ValueChanged);
            // 
            // alphaCheckBox
            // 
            this.alphaCheckBox.AutoSize = true;
            this.alphaCheckBox.Location = new System.Drawing.Point(196, 13);
            this.alphaCheckBox.Name = "alphaCheckBox";
            this.alphaCheckBox.Size = new System.Drawing.Size(83, 17);
            this.alphaCheckBox.TabIndex = 38;
            this.alphaCheckBox.Text = "Alpha Blend";
            this.alphaCheckBox.UseVisualStyleBackColor = true;
            this.alphaCheckBox.CheckedChanged += new System.EventHandler(this.alphaCheckBox_CheckedChanged);
            // 
            // RenderLabel
            // 
            this.RenderLabel.AutoSize = true;
            this.RenderLabel.Location = new System.Drawing.Point(7, 452);
            this.RenderLabel.Name = "RenderLabel";
            this.RenderLabel.Size = new System.Drawing.Size(102, 13);
            this.RenderLabel.TabIndex = 35;
            this.RenderLabel.Text = "Color Render Intent:";
            // 
            // renderComboBox
            // 
            this.renderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.renderComboBox.FormattingEnabled = true;
            this.renderComboBox.Items.AddRange(new object[] {
            "Images",
            "Business",
            "Graphics",
            "AbsoluteColormetric"});
            this.renderComboBox.Location = new System.Drawing.Point(114, 449);
            this.renderComboBox.Name = "renderComboBox";
            this.renderComboBox.Size = new System.Drawing.Size(112, 21);
            this.renderComboBox.TabIndex = 34;
            this.renderComboBox.SelectedIndexChanged += new System.EventHandler(this.renderComboBox_SelectedIndexChanged);
            // 
            // DisplayLabel
            // 
            this.DisplayLabel.AutoSize = true;
            this.DisplayLabel.Location = new System.Drawing.Point(7, 425);
            this.DisplayLabel.Name = "DisplayLabel";
            this.DisplayLabel.Size = new System.Drawing.Size(74, 13);
            this.DisplayLabel.TabIndex = 33;
            this.DisplayLabel.Text = "Display Mode:";
            // 
            // displayComboBox
            // 
            this.displayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.displayComboBox.FormattingEnabled = true;
            this.displayComboBox.Items.AddRange(new object[] {
            "True Color",
            "Palette Color",
            "Vga Color"});
            this.displayComboBox.Location = new System.Drawing.Point(114, 422);
            this.displayComboBox.Name = "displayComboBox";
            this.displayComboBox.Size = new System.Drawing.Size(112, 21);
            this.displayComboBox.TabIndex = 32;
            this.displayComboBox.SelectedIndexChanged += new System.EventHandler(this.displayComboBox_SelectedIndexChanged);
            // 
            // proofCheckBox
            // 
            this.proofCheckBox.AutoSize = true;
            this.proofCheckBox.Location = new System.Drawing.Point(120, 59);
            this.proofCheckBox.Name = "proofCheckBox";
            this.proofCheckBox.Size = new System.Drawing.Size(87, 17);
            this.proofCheckBox.TabIndex = 31;
            this.proofCheckBox.Text = "ICM Proofing";
            this.proofCheckBox.UseVisualStyleBackColor = true;
            // 
            // ICMCheckBox
            // 
            this.ICMCheckBox.AutoSize = true;
            this.ICMCheckBox.Location = new System.Drawing.Point(7, 82);
            this.ICMCheckBox.Name = "ICMCheckBox";
            this.ICMCheckBox.Size = new System.Drawing.Size(45, 17);
            this.ICMCheckBox.TabIndex = 30;
            this.ICMCheckBox.Text = "ICM";
            this.ICMCheckBox.UseVisualStyleBackColor = true;
            // 
            // PaletteLabel
            // 
            this.PaletteLabel.AutoSize = true;
            this.PaletteLabel.Location = new System.Drawing.Point(7, 398);
            this.PaletteLabel.Name = "PaletteLabel";
            this.PaletteLabel.Size = new System.Drawing.Size(70, 13);
            this.PaletteLabel.TabIndex = 29;
            this.PaletteLabel.Text = "Palette Type:";
            // 
            // paletteComboBox
            // 
            this.paletteComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.paletteComboBox.Enabled = false;
            this.paletteComboBox.FormattingEnabled = true;
            this.paletteComboBox.Items.AddRange(new object[] {
            "Optimized",
            "Fixed",
            "Gray",
            "PalFile",
            "UserDefined"});
            this.paletteComboBox.Location = new System.Drawing.Point(114, 395);
            this.paletteComboBox.Name = "paletteComboBox";
            this.paletteComboBox.Size = new System.Drawing.Size(112, 21);
            this.paletteComboBox.TabIndex = 28;
            this.paletteComboBox.SelectedIndexChanged += new System.EventHandler(this.paletteComboBox_SelectedIndexChanged);
            // 
            // progressiveCheckBox
            // 
            this.progressiveCheckBox.AutoSize = true;
            this.progressiveCheckBox.Location = new System.Drawing.Point(7, 59);
            this.progressiveCheckBox.Name = "progressiveCheckBox";
            this.progressiveCheckBox.Size = new System.Drawing.Size(81, 17);
            this.progressiveCheckBox.TabIndex = 27;
            this.progressiveCheckBox.Text = "Progressive";
            this.progressiveCheckBox.UseVisualStyleBackColor = true;
            this.progressiveCheckBox.CheckedChanged += new System.EventHandler(this.progressiveCheckBox_CheckedChanged);
            // 
            // interceptLabel
            // 
            this.interceptLabel.AutoSize = true;
            this.interceptLabel.Location = new System.Drawing.Point(122, 184);
            this.interceptLabel.Name = "interceptLabel";
            this.interceptLabel.Size = new System.Drawing.Size(94, 13);
            this.interceptLabel.TabIndex = 26;
            this.interceptLabel.Text = "Rescale Intercept:";
            // 
            // SlopeLabel
            // 
            this.SlopeLabel.AutoSize = true;
            this.SlopeLabel.Location = new System.Drawing.Point(137, 158);
            this.SlopeLabel.Name = "SlopeLabel";
            this.SlopeLabel.Size = new System.Drawing.Size(79, 13);
            this.SlopeLabel.TabIndex = 25;
            this.SlopeLabel.Text = "Rescale Slope:";
            // 
            // interceptNumericUpDown
            // 
            this.interceptNumericUpDown.Enabled = false;
            this.interceptNumericUpDown.Location = new System.Drawing.Point(224, 182);
            this.interceptNumericUpDown.Name = "interceptNumericUpDown";
            this.interceptNumericUpDown.Size = new System.Drawing.Size(56, 20);
            this.interceptNumericUpDown.TabIndex = 24;
            this.interceptNumericUpDown.ValueChanged += new System.EventHandler(this.interceptNumericUpDown_ValueChanged);
            // 
            // slopeNumericUpDown
            // 
            this.slopeNumericUpDown.Enabled = false;
            this.slopeNumericUpDown.Location = new System.Drawing.Point(224, 156);
            this.slopeNumericUpDown.Name = "slopeNumericUpDown";
            this.slopeNumericUpDown.Size = new System.Drawing.Size(56, 20);
            this.slopeNumericUpDown.TabIndex = 23;
            this.slopeNumericUpDown.ValueChanged += new System.EventHandler(this.slopeNumericUpDown_ValueChanged);
            // 
            // grayComboBox
            // 
            this.grayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.grayComboBox.FormattingEnabled = true;
            this.grayComboBox.Items.AddRange(new object[] {
            "Standard",
            "Medical"});
            this.grayComboBox.Location = new System.Drawing.Point(10, 179);
            this.grayComboBox.Name = "grayComboBox";
            this.grayComboBox.Size = new System.Drawing.Size(78, 21);
            this.grayComboBox.TabIndex = 22;
            this.grayComboBox.SelectedIndexChanged += new System.EventHandler(this.grayComboBox_SelectedIndexChanged);
            // 
            // VerticalComboBox
            // 
            this.VerticalComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VerticalComboBox.FormattingEnabled = true;
            this.VerticalComboBox.Items.AddRange(new object[] {
            "Top",
            "Center",
            "Bottom"});
            this.VerticalComboBox.Location = new System.Drawing.Point(114, 341);
            this.VerticalComboBox.Name = "VerticalComboBox";
            this.VerticalComboBox.Size = new System.Drawing.Size(112, 21);
            this.VerticalComboBox.TabIndex = 21;
            this.VerticalComboBox.SelectedIndexChanged += new System.EventHandler(this.VerticalComboBox_SelectedIndexChanged);
            // 
            // HorzComboBox
            // 
            this.HorzComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HorzComboBox.FormattingEnabled = true;
            this.HorzComboBox.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.HorzComboBox.Location = new System.Drawing.Point(114, 368);
            this.HorzComboBox.Name = "HorzComboBox";
            this.HorzComboBox.Size = new System.Drawing.Size(112, 21);
            this.HorzComboBox.TabIndex = 20;
            this.HorzComboBox.SelectedIndexChanged += new System.EventHandler(this.HorzComboBox_SelectedIndexChanged);
            // 
            // HorzLabel
            // 
            this.HorzLabel.AutoSize = true;
            this.HorzLabel.Location = new System.Drawing.Point(5, 371);
            this.HorzLabel.Name = "HorzLabel";
            this.HorzLabel.Size = new System.Drawing.Size(106, 13);
            this.HorzLabel.TabIndex = 19;
            this.HorzLabel.Text = "Horizontal Alignment:";
            // 
            // VerticalLabel
            // 
            this.VerticalLabel.AutoSize = true;
            this.VerticalLabel.Location = new System.Drawing.Point(5, 344);
            this.VerticalLabel.Name = "VerticalLabel";
            this.VerticalLabel.Size = new System.Drawing.Size(94, 13);
            this.VerticalLabel.TabIndex = 17;
            this.VerticalLabel.Text = "Vertical Alignment:";
            // 
            // ZoomLabel
            // 
            this.ZoomLabel.AutoSize = true;
            this.ZoomLabel.Location = new System.Drawing.Point(8, 109);
            this.ZoomLabel.Name = "ZoomLabel";
            this.ZoomLabel.Size = new System.Drawing.Size(70, 13);
            this.ZoomLabel.TabIndex = 15;
            this.ZoomLabel.Text = "Zoom Factor:";
            // 
            // zoomNumericUpDown
            // 
            this.zoomNumericUpDown.DecimalPlaces = 1;
            this.zoomNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.zoomNumericUpDown.Location = new System.Drawing.Point(86, 107);
            this.zoomNumericUpDown.Name = "zoomNumericUpDown";
            this.zoomNumericUpDown.Size = new System.Drawing.Size(58, 20);
            this.zoomNumericUpDown.TabIndex = 14;
            this.zoomNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.zoomNumericUpDown.ValueChanged += new System.EventHandler(this.zoomNumericUpDown_ValueChanged);
            // 
            // smoothCheckBox
            // 
            this.smoothCheckBox.AutoSize = true;
            this.smoothCheckBox.Checked = true;
            this.smoothCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.smoothCheckBox.Location = new System.Drawing.Point(7, 36);
            this.smoothCheckBox.Name = "smoothCheckBox";
            this.smoothCheckBox.Size = new System.Drawing.Size(76, 17);
            this.smoothCheckBox.TabIndex = 13;
            this.smoothCheckBox.Text = "Smoothing";
            this.smoothCheckBox.UseVisualStyleBackColor = true;
            this.smoothCheckBox.CheckedChanged += new System.EventHandler(this.smoothCheckBox_CheckedChanged);
            // 
            // preserveCheckBox
            // 
            this.preserveCheckBox.AutoSize = true;
            this.preserveCheckBox.Location = new System.Drawing.Point(120, 36);
            this.preserveCheckBox.Name = "preserveCheckBox";
            this.preserveCheckBox.Size = new System.Drawing.Size(98, 17);
            this.preserveCheckBox.TabIndex = 12;
            this.preserveCheckBox.Text = "Preserve Black";
            this.preserveCheckBox.UseVisualStyleBackColor = true;
            this.preserveCheckBox.CheckedChanged += new System.EventHandler(this.preserveCheckBox_CheckedChanged);
            // 
            // ditherCheckBox
            // 
            this.ditherCheckBox.AutoSize = true;
            this.ditherCheckBox.Checked = true;
            this.ditherCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ditherCheckBox.Location = new System.Drawing.Point(120, 13);
            this.ditherCheckBox.Name = "ditherCheckBox";
            this.ditherCheckBox.Size = new System.Drawing.Size(68, 17);
            this.ditherCheckBox.TabIndex = 11;
            this.ditherCheckBox.Text = "Dithering";
            this.ditherCheckBox.UseVisualStyleBackColor = true;
            this.ditherCheckBox.CheckedChanged += new System.EventHandler(this.ditherCheckBox_CheckedChanged);
            // 
            // antiCheckBox
            // 
            this.antiCheckBox.AutoSize = true;
            this.antiCheckBox.Checked = true;
            this.antiCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.antiCheckBox.Location = new System.Drawing.Point(7, 13);
            this.antiCheckBox.Name = "antiCheckBox";
            this.antiCheckBox.Size = new System.Drawing.Size(114, 17);
            this.antiCheckBox.TabIndex = 10;
            this.antiCheckBox.Text = "Anti-Aliasing Effect";
            this.antiCheckBox.UseVisualStyleBackColor = true;
            this.antiCheckBox.CheckedChanged += new System.EventHandler(this.antiCheckBox_CheckedChanged);
            // 
            // GrayLabel
            // 
            this.GrayLabel.AutoSize = true;
            this.GrayLabel.Location = new System.Drawing.Point(9, 163);
            this.GrayLabel.Name = "GrayLabel";
            this.GrayLabel.Size = new System.Drawing.Size(59, 13);
            this.GrayLabel.TabIndex = 8;
            this.GrayLabel.Text = "GrayMode:";
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(261, 256);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(60, 20);
            this.ResetButton.TabIndex = 6;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // BrightValueLabel
            // 
            this.BrightValueLabel.Location = new System.Drawing.Point(173, 256);
            this.BrightValueLabel.Name = "BrightValueLabel";
            this.BrightValueLabel.Size = new System.Drawing.Size(66, 13);
            this.BrightValueLabel.TabIndex = 5;
            this.BrightValueLabel.Text = "0";
            // 
            // ConValueLabel
            // 
            this.ConValueLabel.Location = new System.Drawing.Point(173, 320);
            this.ConValueLabel.Name = "ConValueLabel";
            this.ConValueLabel.Size = new System.Drawing.Size(66, 13);
            this.ConValueLabel.TabIndex = 4;
            this.ConValueLabel.Text = "0";
            // 
            // ContrastLabel
            // 
            this.ContrastLabel.AutoSize = true;
            this.ContrastLabel.Location = new System.Drawing.Point(80, 320);
            this.ContrastLabel.Name = "ContrastLabel";
            this.ContrastLabel.Size = new System.Drawing.Size(49, 13);
            this.ContrastLabel.TabIndex = 3;
            this.ContrastLabel.Text = "Contrast:";
            // 
            // BrightnessLabel
            // 
            this.BrightnessLabel.AutoSize = true;
            this.BrightnessLabel.Location = new System.Drawing.Point(80, 256);
            this.BrightnessLabel.Name = "BrightnessLabel";
            this.BrightnessLabel.Size = new System.Drawing.Size(59, 13);
            this.BrightnessLabel.TabIndex = 2;
            this.BrightnessLabel.Text = "Brightness:";
            // 
            // ConBar
            // 
            this.ConBar.Location = new System.Drawing.Point(6, 272);
            this.ConBar.Maximum = 100;
            this.ConBar.Minimum = -100;
            this.ConBar.Name = "ConBar";
            this.ConBar.Size = new System.Drawing.Size(273, 45);
            this.ConBar.TabIndex = 1;
            this.ConBar.Scroll += new System.EventHandler(this.ConBar_Scroll);
            // 
            // BrightBar
            // 
            this.BrightBar.Location = new System.Drawing.Point(6, 208);
            this.BrightBar.Maximum = 100;
            this.BrightBar.Minimum = -100;
            this.BrightBar.Name = "BrightBar";
            this.BrightBar.Size = new System.Drawing.Size(273, 45);
            this.BrightBar.TabIndex = 0;
            this.BrightBar.Scroll += new System.EventHandler(this.BrightBar_Scroll);
            // 
            // ThumbTab
            // 
            this.ThumbTab.AutoScroll = true;
            this.ThumbTab.Controls.Add(this.threadHungNumericUpDown);
            this.ThumbTab.Controls.Add(this.threadHungLabel);
            this.ThumbTab.Controls.Add(this.threadStartNumericUpDown);
            this.ThumbTab.Controls.Add(this.threadStartLabel);
            this.ThumbTab.Controls.Add(this.hourGlassCheckBox);
            this.ThumbTab.Controls.Add(this.ErrorActionLabel);
            this.ThumbTab.Controls.Add(this.errorComboBox);
            this.ThumbTab.Controls.Add(this.threadCountNumericUpDown);
            this.ThumbTab.Controls.Add(this.ThreadCountLabel);
            this.ThumbTab.Controls.Add(this.preserveBlackCheckBox);
            this.ThumbTab.Controls.Add(this.selectBackColorButton);
            this.ThumbTab.Controls.Add(this.BackColorSelectLabel);
            this.ThumbTab.Controls.Add(this.cameraRawCheckBox);
            this.ThumbTab.Controls.Add(this.scrollLabel);
            this.ThumbTab.Controls.Add(this.scrollDirectionComboBox);
            this.ThumbTab.Controls.Add(this.bitDepthLabel);
            this.ThumbTab.Controls.Add(this.bitDepthComboBox);
            this.ThumbTab.Controls.Add(this.textBackColorButton);
            this.ThumbTab.Controls.Add(this.TextBackColorLabel);
            this.ThumbTab.Controls.Add(this.placeHoldersCheckBox);
            this.ThumbTab.Controls.Add(this.fontButton);
            this.ThumbTab.Controls.Add(this.FontLabel);
            this.ThumbTab.Controls.Add(this.ThumbnailLabel);
            this.ThumbTab.Controls.Add(this.thumbComboBox);
            this.ThumbTab.Controls.Add(this.DescriptorDisplayLabel);
            this.ThumbTab.Controls.Add(this.descriptorDisplayComboBox);
            this.ThumbTab.Controls.Add(this.HozAlignLabel);
            this.ThumbTab.Controls.Add(this.descriptorHorzComboBox);
            this.ThumbTab.Controls.Add(this.DescriptorVLabel);
            this.ThumbTab.Controls.Add(this.descriptorVertComboBox);
            this.ThumbTab.Controls.Add(this.VerticalSpacingLabel);
            this.ThumbTab.Controls.Add(this.vertSpacingNumericUpDown);
            this.ThumbTab.Controls.Add(this.CellHorzLabel);
            this.ThumbTab.Controls.Add(this.horzSpacingNumericUpDown);
            this.ThumbTab.Controls.Add(this.borderButton);
            this.ThumbTab.Controls.Add(this.BoderColorLabel);
            this.ThumbTab.Controls.Add(this.SpacingButton);
            this.ThumbTab.Controls.Add(this.SpacingColorLabel);
            this.ThumbTab.Controls.Add(this.borderWidthLabel);
            this.ThumbTab.Controls.Add(this.borderWidthNumericUpDown);
            this.ThumbTab.Controls.Add(this.heightLabel);
            this.ThumbTab.Controls.Add(this.cellHeightNumericUpDown);
            this.ThumbTab.Controls.Add(this.widthLabel);
            this.ThumbTab.Controls.Add(this.cellWidthNumericUpDown);
            this.ThumbTab.Controls.Add(this.boderThumbLabel);
            this.ThumbTab.Location = new System.Drawing.Point(4, 22);
            this.ThumbTab.Name = "ThumbTab";
            this.ThumbTab.Padding = new System.Windows.Forms.Padding(3);
            this.ThumbTab.Size = new System.Drawing.Size(346, 266);
            this.ThumbTab.TabIndex = 3;
            this.ThumbTab.Text = "Thumbnail";
            this.ThumbTab.UseVisualStyleBackColor = true;
            // 
            // threadHungNumericUpDown
            // 
            this.threadHungNumericUpDown.Location = new System.Drawing.Point(139, 359);
            this.threadHungNumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.threadHungNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.threadHungNumericUpDown.Name = "threadHungNumericUpDown";
            this.threadHungNumericUpDown.Size = new System.Drawing.Size(79, 20);
            this.threadHungNumericUpDown.TabIndex = 49;
            this.threadHungNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.threadHungNumericUpDown.ValueChanged += new System.EventHandler(this.threadHungNumericUpDown_ValueChanged);
            // 
            // threadHungLabel
            // 
            this.threadHungLabel.AutoSize = true;
            this.threadHungLabel.Location = new System.Drawing.Point(10, 361);
            this.threadHungLabel.Name = "threadHungLabel";
            this.threadHungLabel.Size = new System.Drawing.Size(120, 13);
            this.threadHungLabel.TabIndex = 48;
            this.threadHungLabel.Text = "Thread Hung Theshold:";
            // 
            // threadStartNumericUpDown
            // 
            this.threadStartNumericUpDown.Location = new System.Drawing.Point(139, 333);
            this.threadStartNumericUpDown.Maximum = new decimal(new int[] {
            4500,
            0,
            0,
            0});
            this.threadStartNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.threadStartNumericUpDown.Name = "threadStartNumericUpDown";
            this.threadStartNumericUpDown.Size = new System.Drawing.Size(79, 20);
            this.threadStartNumericUpDown.TabIndex = 47;
            this.threadStartNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.threadStartNumericUpDown.ValueChanged += new System.EventHandler(this.threadStartNumericUpDown_ValueChanged);
            // 
            // threadStartLabel
            // 
            this.threadStartLabel.AutoSize = true;
            this.threadStartLabel.Location = new System.Drawing.Point(11, 335);
            this.threadStartLabel.Name = "threadStartLabel";
            this.threadStartLabel.Size = new System.Drawing.Size(116, 13);
            this.threadStartLabel.TabIndex = 46;
            this.threadStartLabel.Text = "Thread Start Theshold:";
            // 
            // hourGlassCheckBox
            // 
            this.hourGlassCheckBox.AutoSize = true;
            this.hourGlassCheckBox.Checked = true;
            this.hourGlassCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.hourGlassCheckBox.Location = new System.Drawing.Point(129, 412);
            this.hourGlassCheckBox.Name = "hourGlassCheckBox";
            this.hourGlassCheckBox.Size = new System.Drawing.Size(103, 17);
            this.hourGlassCheckBox.TabIndex = 45;
            this.hourGlassCheckBox.Text = "Show Hourglass";
            this.hourGlassCheckBox.UseVisualStyleBackColor = true;
            this.hourGlassCheckBox.CheckedChanged += new System.EventHandler(this.hourGlassCheckBox_CheckedChanged);
            // 
            // ErrorActionLabel
            // 
            this.ErrorActionLabel.AutoSize = true;
            this.ErrorActionLabel.Location = new System.Drawing.Point(10, 497);
            this.ErrorActionLabel.Name = "ErrorActionLabel";
            this.ErrorActionLabel.Size = new System.Drawing.Size(65, 13);
            this.ErrorActionLabel.TabIndex = 44;
            this.ErrorActionLabel.Text = "Error Action:";
            // 
            // errorComboBox
            // 
            this.errorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.errorComboBox.FormattingEnabled = true;
            this.errorComboBox.Items.AddRange(new object[] {
            "User Error Icon",
            "Abort"});
            this.errorComboBox.Location = new System.Drawing.Point(173, 497);
            this.errorComboBox.Name = "errorComboBox";
            this.errorComboBox.Size = new System.Drawing.Size(117, 21);
            this.errorComboBox.TabIndex = 43;
            this.errorComboBox.SelectedIndexChanged += new System.EventHandler(this.errorComboBox_SelectedIndexChanged);
            // 
            // threadCountNumericUpDown
            // 
            this.threadCountNumericUpDown.Location = new System.Drawing.Point(139, 307);
            this.threadCountNumericUpDown.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.threadCountNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.threadCountNumericUpDown.Name = "threadCountNumericUpDown";
            this.threadCountNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.threadCountNumericUpDown.TabIndex = 42;
            this.threadCountNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.threadCountNumericUpDown.ValueChanged += new System.EventHandler(this.threadCountNumericUpDown_ValueChanged);
            // 
            // ThreadCountLabel
            // 
            this.ThreadCountLabel.AutoSize = true;
            this.ThreadCountLabel.Location = new System.Drawing.Point(11, 309);
            this.ThreadCountLabel.Name = "ThreadCountLabel";
            this.ThreadCountLabel.Size = new System.Drawing.Size(122, 13);
            this.ThreadCountLabel.TabIndex = 41;
            this.ThreadCountLabel.Text = "Maximum Thread Count:";
            // 
            // preserveBlackCheckBox
            // 
            this.preserveBlackCheckBox.AutoSize = true;
            this.preserveBlackCheckBox.Location = new System.Drawing.Point(15, 413);
            this.preserveBlackCheckBox.Name = "preserveBlackCheckBox";
            this.preserveBlackCheckBox.Size = new System.Drawing.Size(98, 17);
            this.preserveBlackCheckBox.TabIndex = 39;
            this.preserveBlackCheckBox.Text = "Preserve Black";
            this.preserveBlackCheckBox.UseVisualStyleBackColor = true;
            this.preserveBlackCheckBox.CheckedChanged += new System.EventHandler(this.preserveBlackCheckBox_CheckedChanged);
            // 
            // selectBackColorButton
            // 
            this.selectBackColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectBackColorButton.Location = new System.Drawing.Point(305, 134);
            this.selectBackColorButton.Name = "selectBackColorButton";
            this.selectBackColorButton.Size = new System.Drawing.Size(28, 23);
            this.selectBackColorButton.TabIndex = 37;
            this.selectBackColorButton.UseVisualStyleBackColor = true;
            this.selectBackColorButton.Click += new System.EventHandler(this.selectBackColorButton_Click);
            // 
            // BackColorSelectLabel
            // 
            this.BackColorSelectLabel.AutoSize = true;
            this.BackColorSelectLabel.Location = new System.Drawing.Point(189, 139);
            this.BackColorSelectLabel.Name = "BackColorSelectLabel";
            this.BackColorSelectLabel.Size = new System.Drawing.Size(95, 13);
            this.BackColorSelectLabel.TabIndex = 38;
            this.BackColorSelectLabel.Text = "Select Back Color:";
            // 
            // cameraRawCheckBox
            // 
            this.cameraRawCheckBox.AutoSize = true;
            this.cameraRawCheckBox.Location = new System.Drawing.Point(15, 390);
            this.cameraRawCheckBox.Name = "cameraRawCheckBox";
            this.cameraRawCheckBox.Size = new System.Drawing.Size(87, 17);
            this.cameraRawCheckBox.TabIndex = 36;
            this.cameraRawCheckBox.Text = "Camera Raw";
            this.cameraRawCheckBox.UseVisualStyleBackColor = true;
            this.cameraRawCheckBox.CheckedChanged += new System.EventHandler(this.cameraRawCheckBox_CheckedChanged);
            // 
            // scrollLabel
            // 
            this.scrollLabel.AutoSize = true;
            this.scrollLabel.Location = new System.Drawing.Point(12, 438);
            this.scrollLabel.Name = "scrollLabel";
            this.scrollLabel.Size = new System.Drawing.Size(81, 13);
            this.scrollLabel.TabIndex = 34;
            this.scrollLabel.Text = "Scroll Direction:";
            // 
            // scrollDirectionComboBox
            // 
            this.scrollDirectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scrollDirectionComboBox.FormattingEnabled = true;
            this.scrollDirectionComboBox.Items.AddRange(new object[] {
            "Horizontal",
            "Vertical"});
            this.scrollDirectionComboBox.Location = new System.Drawing.Point(173, 435);
            this.scrollDirectionComboBox.Name = "scrollDirectionComboBox";
            this.scrollDirectionComboBox.Size = new System.Drawing.Size(117, 21);
            this.scrollDirectionComboBox.TabIndex = 33;
            this.scrollDirectionComboBox.SelectedIndexChanged += new System.EventHandler(this.scrollDirectionComboBox_SelectedIndexChanged);
            // 
            // bitDepthLabel
            // 
            this.bitDepthLabel.AutoSize = true;
            this.bitDepthLabel.Location = new System.Drawing.Point(11, 469);
            this.bitDepthLabel.Name = "bitDepthLabel";
            this.bitDepthLabel.Size = new System.Drawing.Size(153, 13);
            this.bitDepthLabel.TabIndex = 32;
            this.bitDepthLabel.Text = "Maximum Thumbnail Bit Depth:";
            // 
            // bitDepthComboBox
            // 
            this.bitDepthComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bitDepthComboBox.FormattingEnabled = true;
            this.bitDepthComboBox.Items.AddRange(new object[] {
            "24",
            "8"});
            this.bitDepthComboBox.Location = new System.Drawing.Point(173, 466);
            this.bitDepthComboBox.Name = "bitDepthComboBox";
            this.bitDepthComboBox.Size = new System.Drawing.Size(117, 21);
            this.bitDepthComboBox.TabIndex = 31;
            this.bitDepthComboBox.SelectedIndexChanged += new System.EventHandler(this.bitDepthComboBox_SelectedIndexChanged);
            // 
            // textBackColorButton
            // 
            this.textBackColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.textBackColorButton.Location = new System.Drawing.Point(127, 273);
            this.textBackColorButton.Name = "textBackColorButton";
            this.textBackColorButton.Size = new System.Drawing.Size(28, 23);
            this.textBackColorButton.TabIndex = 29;
            this.textBackColorButton.UseVisualStyleBackColor = true;
            this.textBackColorButton.Click += new System.EventHandler(this.textBackColorButton_Click);
            // 
            // TextBackColorLabel
            // 
            this.TextBackColorLabel.AutoSize = true;
            this.TextBackColorLabel.Location = new System.Drawing.Point(10, 278);
            this.TextBackColorLabel.Name = "TextBackColorLabel";
            this.TextBackColorLabel.Size = new System.Drawing.Size(113, 13);
            this.TextBackColorLabel.TabIndex = 30;
            this.TextBackColorLabel.Text = "Descriptor Back Color:";
            // 
            // placeHoldersCheckBox
            // 
            this.placeHoldersCheckBox.AutoSize = true;
            this.placeHoldersCheckBox.Location = new System.Drawing.Point(129, 390);
            this.placeHoldersCheckBox.Name = "placeHoldersCheckBox";
            this.placeHoldersCheckBox.Size = new System.Drawing.Size(149, 17);
            this.placeHoldersCheckBox.TabIndex = 7;
            this.placeHoldersCheckBox.Text = "Show Image Placeholders";
            this.placeHoldersCheckBox.UseVisualStyleBackColor = true;
            this.placeHoldersCheckBox.CheckedChanged += new System.EventHandler(this.placeHoldersCheckBox_CheckedChanged);
            // 
            // fontButton
            // 
            this.fontButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fontButton.Location = new System.Drawing.Point(100, 244);
            this.fontButton.Name = "fontButton";
            this.fontButton.Size = new System.Drawing.Size(192, 23);
            this.fontButton.TabIndex = 28;
            this.fontButton.UseVisualStyleBackColor = true;
            this.fontButton.Click += new System.EventHandler(this.fontButton_Click);
            // 
            // FontLabel
            // 
            this.FontLabel.AutoSize = true;
            this.FontLabel.Location = new System.Drawing.Point(12, 249);
            this.FontLabel.Name = "FontLabel";
            this.FontLabel.Size = new System.Drawing.Size(82, 13);
            this.FontLabel.TabIndex = 27;
            this.FontLabel.Text = "Descriptor Font:";
            // 
            // ThumbnailLabel
            // 
            this.ThumbnailLabel.AutoSize = true;
            this.ThumbnailLabel.Location = new System.Drawing.Point(8, 9);
            this.ThumbnailLabel.Name = "ThumbnailLabel";
            this.ThumbnailLabel.Size = new System.Drawing.Size(127, 13);
            this.ThumbnailLabel.TabIndex = 26;
            this.ThumbnailLabel.Text = "ThumbnailXpress Control:";
            // 
            // thumbComboBox
            // 
            this.thumbComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.thumbComboBox.FormattingEnabled = true;
            this.thumbComboBox.Items.AddRange(new object[] {
            "Recent Images Thumbnail",
            "Paging Thumbnail"});
            this.thumbComboBox.Location = new System.Drawing.Point(141, 6);
            this.thumbComboBox.Name = "thumbComboBox";
            this.thumbComboBox.Size = new System.Drawing.Size(182, 21);
            this.thumbComboBox.TabIndex = 25;
            this.thumbComboBox.SelectedIndexChanged += new System.EventHandler(this.thumbComboBox_SelectedIndexChanged);
            // 
            // DescriptorDisplayLabel
            // 
            this.DescriptorDisplayLabel.AutoSize = true;
            this.DescriptorDisplayLabel.Location = new System.Drawing.Point(12, 219);
            this.DescriptorDisplayLabel.Name = "DescriptorDisplayLabel";
            this.DescriptorDisplayLabel.Size = new System.Drawing.Size(134, 13);
            this.DescriptorDisplayLabel.TabIndex = 24;
            this.DescriptorDisplayLabel.Text = "Descriptor Display Method:";
            // 
            // descriptorDisplayComboBox
            // 
            this.descriptorDisplayComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.descriptorDisplayComboBox.FormattingEnabled = true;
            this.descriptorDisplayComboBox.Items.AddRange(new object[] {
            "None",
            "Default",
            "LongFilename",
            "ShortFilename",
            "IncludePage",
            "Custom"});
            this.descriptorDisplayComboBox.Location = new System.Drawing.Point(175, 217);
            this.descriptorDisplayComboBox.Name = "descriptorDisplayComboBox";
            this.descriptorDisplayComboBox.Size = new System.Drawing.Size(117, 21);
            this.descriptorDisplayComboBox.TabIndex = 23;
            this.descriptorDisplayComboBox.SelectedIndexChanged += new System.EventHandler(this.descriptorDisplayComboBox_SelectedIndexChanged);
            // 
            // HozAlignLabel
            // 
            this.HozAlignLabel.AutoSize = true;
            this.HozAlignLabel.Location = new System.Drawing.Point(12, 192);
            this.HozAlignLabel.Name = "HozAlignLabel";
            this.HozAlignLabel.Size = new System.Drawing.Size(157, 13);
            this.HozAlignLabel.TabIndex = 22;
            this.HozAlignLabel.Text = "Descriptor Horizontal Alignment:";
            // 
            // descriptorHorzComboBox
            // 
            this.descriptorHorzComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.descriptorHorzComboBox.FormattingEnabled = true;
            this.descriptorHorzComboBox.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.descriptorHorzComboBox.Location = new System.Drawing.Point(175, 190);
            this.descriptorHorzComboBox.Name = "descriptorHorzComboBox";
            this.descriptorHorzComboBox.Size = new System.Drawing.Size(117, 21);
            this.descriptorHorzComboBox.TabIndex = 21;
            this.descriptorHorzComboBox.SelectedIndexChanged += new System.EventHandler(this.descriptorHorzComboBox_SelectedIndexChanged);
            // 
            // DescriptorVLabel
            // 
            this.DescriptorVLabel.AutoSize = true;
            this.DescriptorVLabel.Location = new System.Drawing.Point(12, 165);
            this.DescriptorVLabel.Name = "DescriptorVLabel";
            this.DescriptorVLabel.Size = new System.Drawing.Size(145, 13);
            this.DescriptorVLabel.TabIndex = 20;
            this.DescriptorVLabel.Text = "Descriptor Vertical Alignment:";
            // 
            // descriptorVertComboBox
            // 
            this.descriptorVertComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.descriptorVertComboBox.FormattingEnabled = true;
            this.descriptorVertComboBox.Items.AddRange(new object[] {
            "Top",
            "Center",
            "Bottom"});
            this.descriptorVertComboBox.Location = new System.Drawing.Point(175, 162);
            this.descriptorVertComboBox.Name = "descriptorVertComboBox";
            this.descriptorVertComboBox.Size = new System.Drawing.Size(117, 21);
            this.descriptorVertComboBox.TabIndex = 7;
            this.descriptorVertComboBox.SelectedIndexChanged += new System.EventHandler(this.descriptorHorzComboBox_SelectedIndexChanged);
            // 
            // VerticalSpacingLabel
            // 
            this.VerticalSpacingLabel.AutoSize = true;
            this.VerticalSpacingLabel.Location = new System.Drawing.Point(11, 129);
            this.VerticalSpacingLabel.Name = "VerticalSpacingLabel";
            this.VerticalSpacingLabel.Size = new System.Drawing.Size(107, 13);
            this.VerticalSpacingLabel.TabIndex = 19;
            this.VerticalSpacingLabel.Text = "Cell Vertical Spacing:";
            // 
            // vertSpacingNumericUpDown
            // 
            this.vertSpacingNumericUpDown.Location = new System.Drawing.Point(136, 127);
            this.vertSpacingNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.vertSpacingNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.vertSpacingNumericUpDown.Name = "vertSpacingNumericUpDown";
            this.vertSpacingNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.vertSpacingNumericUpDown.TabIndex = 18;
            this.vertSpacingNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.vertSpacingNumericUpDown.ValueChanged += new System.EventHandler(this.vertSpacingNumericUpDown_ValueChanged);
            // 
            // CellHorzLabel
            // 
            this.CellHorzLabel.AutoSize = true;
            this.CellHorzLabel.Location = new System.Drawing.Point(11, 103);
            this.CellHorzLabel.Name = "CellHorzLabel";
            this.CellHorzLabel.Size = new System.Drawing.Size(119, 13);
            this.CellHorzLabel.TabIndex = 17;
            this.CellHorzLabel.Text = "Cell Horizontal Spacing:";
            // 
            // horzSpacingNumericUpDown
            // 
            this.horzSpacingNumericUpDown.Location = new System.Drawing.Point(136, 101);
            this.horzSpacingNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.horzSpacingNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.horzSpacingNumericUpDown.Name = "horzSpacingNumericUpDown";
            this.horzSpacingNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.horzSpacingNumericUpDown.TabIndex = 16;
            this.horzSpacingNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.horzSpacingNumericUpDown.ValueChanged += new System.EventHandler(this.horzSpacingNumericUpDown_ValueChanged);
            // 
            // borderButton
            // 
            this.borderButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.borderButton.Location = new System.Drawing.Point(286, 45);
            this.borderButton.Name = "borderButton";
            this.borderButton.Size = new System.Drawing.Size(28, 23);
            this.borderButton.TabIndex = 14;
            this.borderButton.UseVisualStyleBackColor = true;
            this.borderButton.Click += new System.EventHandler(this.borderButton_Click);
            // 
            // BoderColorLabel
            // 
            this.BoderColorLabel.AutoSize = true;
            this.BoderColorLabel.Location = new System.Drawing.Point(188, 50);
            this.BoderColorLabel.Name = "BoderColorLabel";
            this.BoderColorLabel.Size = new System.Drawing.Size(88, 13);
            this.BoderColorLabel.TabIndex = 15;
            this.BoderColorLabel.Text = "Cell Border Color:";
            // 
            // SpacingButton
            // 
            this.SpacingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SpacingButton.Location = new System.Drawing.Point(305, 103);
            this.SpacingButton.Name = "SpacingButton";
            this.SpacingButton.Size = new System.Drawing.Size(28, 23);
            this.SpacingButton.TabIndex = 10;
            this.SpacingButton.UseVisualStyleBackColor = true;
            this.SpacingButton.Click += new System.EventHandler(this.SpacingButton_Click);
            // 
            // SpacingColorLabel
            // 
            this.SpacingColorLabel.AutoSize = true;
            this.SpacingColorLabel.Location = new System.Drawing.Point(188, 108);
            this.SpacingColorLabel.Name = "SpacingColorLabel";
            this.SpacingColorLabel.Size = new System.Drawing.Size(96, 13);
            this.SpacingColorLabel.TabIndex = 13;
            this.SpacingColorLabel.Text = "Cell Spacing Color:";
            // 
            // borderWidthLabel
            // 
            this.borderWidthLabel.AutoSize = true;
            this.borderWidthLabel.Location = new System.Drawing.Point(188, 76);
            this.borderWidthLabel.Name = "borderWidthLabel";
            this.borderWidthLabel.Size = new System.Drawing.Size(92, 13);
            this.borderWidthLabel.TabIndex = 12;
            this.borderWidthLabel.Text = "Cell Border Width:";
            // 
            // borderWidthNumericUpDown
            // 
            this.borderWidthNumericUpDown.Location = new System.Drawing.Point(286, 74);
            this.borderWidthNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.borderWidthNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.borderWidthNumericUpDown.Name = "borderWidthNumericUpDown";
            this.borderWidthNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.borderWidthNumericUpDown.TabIndex = 11;
            this.borderWidthNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.borderWidthNumericUpDown.ValueChanged += new System.EventHandler(this.borderWidthNumericUpDown_ValueChanged);
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Location = new System.Drawing.Point(11, 76);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(61, 13);
            this.heightLabel.TabIndex = 10;
            this.heightLabel.Text = "Cell Height:";
            // 
            // cellHeightNumericUpDown
            // 
            this.cellHeightNumericUpDown.Location = new System.Drawing.Point(136, 76);
            this.cellHeightNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.cellHeightNumericUpDown.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.cellHeightNumericUpDown.Name = "cellHeightNumericUpDown";
            this.cellHeightNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.cellHeightNumericUpDown.TabIndex = 9;
            this.cellHeightNumericUpDown.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.cellHeightNumericUpDown.ValueChanged += new System.EventHandler(this.cellHeightNumericUpDown_ValueChanged);
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Location = new System.Drawing.Point(11, 50);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(58, 13);
            this.widthLabel.TabIndex = 8;
            this.widthLabel.Text = "Cell Width:";
            // 
            // cellWidthNumericUpDown
            // 
            this.cellWidthNumericUpDown.Location = new System.Drawing.Point(136, 48);
            this.cellWidthNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.cellWidthNumericUpDown.Minimum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.cellWidthNumericUpDown.Name = "cellWidthNumericUpDown";
            this.cellWidthNumericUpDown.Size = new System.Drawing.Size(44, 20);
            this.cellWidthNumericUpDown.TabIndex = 7;
            this.cellWidthNumericUpDown.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.cellWidthNumericUpDown.ValueChanged += new System.EventHandler(this.cellWidthNumericUpDown_ValueChanged);
            // 
            // boderThumbLabel
            // 
            this.boderThumbLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boderThumbLabel.Location = new System.Drawing.Point(3, 3);
            this.boderThumbLabel.Name = "boderThumbLabel";
            this.boderThumbLabel.Size = new System.Drawing.Size(334, 29);
            this.boderThumbLabel.TabIndex = 35;
            // 
            // ColorTab
            // 
            this.ColorTab.AutoScroll = true;
            this.ColorTab.Controls.Add(this.PaletteButtonLabel);
            this.ColorTab.Controls.Add(this.paletteButton);
            this.ColorTab.Controls.Add(this.resetComponentsButton);
            this.ColorTab.Controls.Add(this.componentsLabel);
            this.ColorTab.Controls.Add(this.cmykGroupBox);
            this.ColorTab.Controls.Add(this.hslGroupBox);
            this.ColorTab.Controls.Add(this.rgbaGroupBox);
            this.ColorTab.Controls.Add(this.histogramLabel);
            this.ColorTab.Controls.Add(this.comboBoxHistogram);
            this.ColorTab.Controls.Add(this.imageXViewHistogram);
            this.ColorTab.Location = new System.Drawing.Point(4, 22);
            this.ColorTab.Name = "ColorTab";
            this.ColorTab.Padding = new System.Windows.Forms.Padding(3);
            this.ColorTab.Size = new System.Drawing.Size(346, 266);
            this.ColorTab.TabIndex = 4;
            this.ColorTab.Text = "Color";
            this.ColorTab.UseVisualStyleBackColor = true;
            // 
            // PaletteButtonLabel
            // 
            this.PaletteButtonLabel.AutoSize = true;
            this.PaletteButtonLabel.Location = new System.Drawing.Point(212, 258);
            this.PaletteButtonLabel.Name = "PaletteButtonLabel";
            this.PaletteButtonLabel.Size = new System.Drawing.Size(88, 13);
            this.PaletteButtonLabel.TabIndex = 57;
            this.PaletteButtonLabel.Text = "(8-bit or less only)";
            // 
            // paletteButton
            // 
            this.paletteButton.Enabled = false;
            this.paletteButton.Location = new System.Drawing.Point(131, 253);
            this.paletteButton.Name = "paletteButton";
            this.paletteButton.Size = new System.Drawing.Size(75, 23);
            this.paletteButton.TabIndex = 56;
            this.paletteButton.Text = "Palette";
            this.paletteButton.UseVisualStyleBackColor = true;
            this.paletteButton.Click += new System.EventHandler(this.paletteButton_Click);
            // 
            // resetComponentsButton
            // 
            this.resetComponentsButton.Enabled = false;
            this.resetComponentsButton.Location = new System.Drawing.Point(182, 413);
            this.resetComponentsButton.Name = "resetComponentsButton";
            this.resetComponentsButton.Size = new System.Drawing.Size(133, 23);
            this.resetComponentsButton.TabIndex = 10;
            this.resetComponentsButton.Text = "Reset Image to Original";
            this.resetComponentsButton.UseVisualStyleBackColor = true;
            this.resetComponentsButton.Click += new System.EventHandler(this.resetComponentsButton_Click);
            // 
            // componentsLabel
            // 
            this.componentsLabel.AutoSize = true;
            this.componentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.componentsLabel.Location = new System.Drawing.Point(104, 288);
            this.componentsLabel.Name = "componentsLabel";
            this.componentsLabel.Size = new System.Drawing.Size(123, 13);
            this.componentsLabel.TabIndex = 9;
            this.componentsLabel.Text = "Color Channel Selection:";
            // 
            // cmykGroupBox
            // 
            this.cmykGroupBox.Controls.Add(this.blackRadioButton);
            this.cmykGroupBox.Controls.Add(this.yellowRadioButton);
            this.cmykGroupBox.Controls.Add(this.magentaRadioButton);
            this.cmykGroupBox.Controls.Add(this.cyanRadioButton);
            this.cmykGroupBox.Location = new System.Drawing.Point(24, 425);
            this.cmykGroupBox.Name = "cmykGroupBox";
            this.cmykGroupBox.Size = new System.Drawing.Size(133, 109);
            this.cmykGroupBox.TabIndex = 8;
            this.cmykGroupBox.TabStop = false;
            this.cmykGroupBox.Text = "CMYK";
            // 
            // blackRadioButton
            // 
            this.blackRadioButton.AutoSize = true;
            this.blackRadioButton.Enabled = false;
            this.blackRadioButton.Location = new System.Drawing.Point(20, 88);
            this.blackRadioButton.Name = "blackRadioButton";
            this.blackRadioButton.Size = new System.Drawing.Size(52, 17);
            this.blackRadioButton.TabIndex = 5;
            this.blackRadioButton.TabStop = true;
            this.blackRadioButton.Text = "Black";
            this.blackRadioButton.UseVisualStyleBackColor = true;
            this.blackRadioButton.Click += new System.EventHandler(this.blackRadioButton_Click);
            // 
            // yellowRadioButton
            // 
            this.yellowRadioButton.AutoSize = true;
            this.yellowRadioButton.Enabled = false;
            this.yellowRadioButton.Location = new System.Drawing.Point(20, 65);
            this.yellowRadioButton.Name = "yellowRadioButton";
            this.yellowRadioButton.Size = new System.Drawing.Size(56, 17);
            this.yellowRadioButton.TabIndex = 4;
            this.yellowRadioButton.TabStop = true;
            this.yellowRadioButton.Text = "Yellow";
            this.yellowRadioButton.UseVisualStyleBackColor = true;
            this.yellowRadioButton.Click += new System.EventHandler(this.yellowRadioButton_Click);
            // 
            // magentaRadioButton
            // 
            this.magentaRadioButton.AutoSize = true;
            this.magentaRadioButton.Enabled = false;
            this.magentaRadioButton.Location = new System.Drawing.Point(20, 42);
            this.magentaRadioButton.Name = "magentaRadioButton";
            this.magentaRadioButton.Size = new System.Drawing.Size(67, 17);
            this.magentaRadioButton.TabIndex = 3;
            this.magentaRadioButton.TabStop = true;
            this.magentaRadioButton.Text = "Magenta";
            this.magentaRadioButton.UseVisualStyleBackColor = true;
            this.magentaRadioButton.Click += new System.EventHandler(this.magentaRadioButton_Click);
            // 
            // cyanRadioButton
            // 
            this.cyanRadioButton.AutoSize = true;
            this.cyanRadioButton.Enabled = false;
            this.cyanRadioButton.Location = new System.Drawing.Point(20, 19);
            this.cyanRadioButton.Name = "cyanRadioButton";
            this.cyanRadioButton.Size = new System.Drawing.Size(49, 17);
            this.cyanRadioButton.TabIndex = 2;
            this.cyanRadioButton.TabStop = true;
            this.cyanRadioButton.Text = "Cyan";
            this.cyanRadioButton.UseVisualStyleBackColor = true;
            this.cyanRadioButton.Click += new System.EventHandler(this.cyanRadioButton_Click);
            // 
            // hslGroupBox
            // 
            this.hslGroupBox.Controls.Add(this.luminanceRadioButton);
            this.hslGroupBox.Controls.Add(this.saturationRadioButton);
            this.hslGroupBox.Controls.Add(this.hueRadioButton);
            this.hslGroupBox.Location = new System.Drawing.Point(182, 304);
            this.hslGroupBox.Name = "hslGroupBox";
            this.hslGroupBox.Size = new System.Drawing.Size(133, 90);
            this.hslGroupBox.TabIndex = 7;
            this.hslGroupBox.TabStop = false;
            this.hslGroupBox.Text = "HSL";
            // 
            // luminanceRadioButton
            // 
            this.luminanceRadioButton.AutoSize = true;
            this.luminanceRadioButton.Enabled = false;
            this.luminanceRadioButton.Location = new System.Drawing.Point(20, 65);
            this.luminanceRadioButton.Name = "luminanceRadioButton";
            this.luminanceRadioButton.Size = new System.Drawing.Size(77, 17);
            this.luminanceRadioButton.TabIndex = 4;
            this.luminanceRadioButton.TabStop = true;
            this.luminanceRadioButton.Text = "Luminance";
            this.luminanceRadioButton.UseVisualStyleBackColor = true;
            this.luminanceRadioButton.Click += new System.EventHandler(this.luminanceRadioButton_Click);
            // 
            // saturationRadioButton
            // 
            this.saturationRadioButton.AutoSize = true;
            this.saturationRadioButton.Enabled = false;
            this.saturationRadioButton.Location = new System.Drawing.Point(20, 42);
            this.saturationRadioButton.Name = "saturationRadioButton";
            this.saturationRadioButton.Size = new System.Drawing.Size(73, 17);
            this.saturationRadioButton.TabIndex = 3;
            this.saturationRadioButton.TabStop = true;
            this.saturationRadioButton.Text = "Saturation";
            this.saturationRadioButton.UseVisualStyleBackColor = true;
            this.saturationRadioButton.Click += new System.EventHandler(this.saturationRadioButton_Click);
            // 
            // hueRadioButton
            // 
            this.hueRadioButton.AutoSize = true;
            this.hueRadioButton.Enabled = false;
            this.hueRadioButton.Location = new System.Drawing.Point(20, 19);
            this.hueRadioButton.Name = "hueRadioButton";
            this.hueRadioButton.Size = new System.Drawing.Size(45, 17);
            this.hueRadioButton.TabIndex = 2;
            this.hueRadioButton.TabStop = true;
            this.hueRadioButton.Text = "Hue";
            this.hueRadioButton.UseVisualStyleBackColor = true;
            this.hueRadioButton.Click += new System.EventHandler(this.hueRadioButton_Click);
            // 
            // rgbaGroupBox
            // 
            this.rgbaGroupBox.Controls.Add(this.alphaRadioButton);
            this.rgbaGroupBox.Controls.Add(this.blueRadioButton);
            this.rgbaGroupBox.Controls.Add(this.greenRadioButton);
            this.rgbaGroupBox.Controls.Add(this.redRadioButton);
            this.rgbaGroupBox.Location = new System.Drawing.Point(24, 304);
            this.rgbaGroupBox.Name = "rgbaGroupBox";
            this.rgbaGroupBox.Size = new System.Drawing.Size(133, 115);
            this.rgbaGroupBox.TabIndex = 6;
            this.rgbaGroupBox.TabStop = false;
            this.rgbaGroupBox.Text = "RGBA";
            // 
            // alphaRadioButton
            // 
            this.alphaRadioButton.AutoSize = true;
            this.alphaRadioButton.Enabled = false;
            this.alphaRadioButton.Location = new System.Drawing.Point(20, 88);
            this.alphaRadioButton.Name = "alphaRadioButton";
            this.alphaRadioButton.Size = new System.Drawing.Size(52, 17);
            this.alphaRadioButton.TabIndex = 5;
            this.alphaRadioButton.TabStop = true;
            this.alphaRadioButton.Text = "Alpha";
            this.alphaRadioButton.UseVisualStyleBackColor = true;
            this.alphaRadioButton.Click += new System.EventHandler(this.alphaRadioButton_Click);
            // 
            // blueRadioButton
            // 
            this.blueRadioButton.AutoSize = true;
            this.blueRadioButton.Enabled = false;
            this.blueRadioButton.Location = new System.Drawing.Point(20, 65);
            this.blueRadioButton.Name = "blueRadioButton";
            this.blueRadioButton.Size = new System.Drawing.Size(46, 17);
            this.blueRadioButton.TabIndex = 4;
            this.blueRadioButton.TabStop = true;
            this.blueRadioButton.Text = "Blue";
            this.blueRadioButton.UseVisualStyleBackColor = true;
            this.blueRadioButton.Click += new System.EventHandler(this.blueRadioButton_Click);
            // 
            // greenRadioButton
            // 
            this.greenRadioButton.AutoSize = true;
            this.greenRadioButton.Enabled = false;
            this.greenRadioButton.Location = new System.Drawing.Point(20, 42);
            this.greenRadioButton.Name = "greenRadioButton";
            this.greenRadioButton.Size = new System.Drawing.Size(54, 17);
            this.greenRadioButton.TabIndex = 3;
            this.greenRadioButton.TabStop = true;
            this.greenRadioButton.Text = "Green";
            this.greenRadioButton.UseVisualStyleBackColor = true;
            this.greenRadioButton.Click += new System.EventHandler(this.greenRadioButton_Click);
            // 
            // redRadioButton
            // 
            this.redRadioButton.AutoSize = true;
            this.redRadioButton.Enabled = false;
            this.redRadioButton.Location = new System.Drawing.Point(20, 19);
            this.redRadioButton.Name = "redRadioButton";
            this.redRadioButton.Size = new System.Drawing.Size(45, 17);
            this.redRadioButton.TabIndex = 2;
            this.redRadioButton.TabStop = true;
            this.redRadioButton.Text = "Red";
            this.redRadioButton.UseVisualStyleBackColor = true;
            this.redRadioButton.Click += new System.EventHandler(this.redRadioButton_Click);
            // 
            // histogramLabel
            // 
            this.histogramLabel.AutoSize = true;
            this.histogramLabel.Location = new System.Drawing.Point(84, 9);
            this.histogramLabel.Name = "histogramLabel";
            this.histogramLabel.Size = new System.Drawing.Size(57, 13);
            this.histogramLabel.TabIndex = 1;
            this.histogramLabel.Text = "Histogram:";
            // 
            // comboBoxHistogram
            // 
            this.comboBoxHistogram.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHistogram.Enabled = false;
            this.comboBoxHistogram.FormattingEnabled = true;
            this.comboBoxHistogram.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "RGB"});
            this.comboBoxHistogram.Location = new System.Drawing.Point(147, 6);
            this.comboBoxHistogram.Name = "comboBoxHistogram";
            this.comboBoxHistogram.Size = new System.Drawing.Size(79, 21);
            this.comboBoxHistogram.TabIndex = 0;
            this.comboBoxHistogram.SelectedIndexChanged += new System.EventHandler(this.comboBoxHistogram_SelectedIndexChanged);
            // 
            // imageXViewHistogram
            // 
            this.imageXViewHistogram.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewHistogram.Location = new System.Drawing.Point(6, 33);
            this.imageXViewHistogram.MouseWheelCapture = false;
            this.imageXViewHistogram.Name = "imageXViewHistogram";
            this.imageXViewHistogram.Size = new System.Drawing.Size(319, 214);
            this.imageXViewHistogram.TabIndex = 0;
            // 
            // LayersCadTab
            // 
            this.LayersCadTab.AutoScroll = true;
            this.LayersCadTab.Controls.Add(this.SelectButton);
            this.LayersCadTab.Controls.Add(this.LayersLabel);
            this.LayersCadTab.Location = new System.Drawing.Point(4, 22);
            this.LayersCadTab.Name = "LayersCadTab";
            this.LayersCadTab.Padding = new System.Windows.Forms.Padding(3);
            this.LayersCadTab.Size = new System.Drawing.Size(346, 266);
            this.LayersCadTab.TabIndex = 5;
            this.LayersCadTab.Text = "CAD";
            this.LayersCadTab.UseVisualStyleBackColor = true;
            // 
            // SelectButton
            // 
            this.SelectButton.Location = new System.Drawing.Point(151, 13);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(75, 23);
            this.SelectButton.TabIndex = 1;
            this.SelectButton.Text = "Select All";
            this.SelectButton.UseVisualStyleBackColor = true;
            this.SelectButton.Click += new System.EventHandler(this.SelectButton_Click);
            // 
            // LayersLabel
            // 
            this.LayersLabel.AutoSize = true;
            this.LayersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LayersLabel.Location = new System.Drawing.Point(61, 16);
            this.LayersLabel.Name = "LayersLabel";
            this.LayersLabel.Size = new System.Drawing.Size(59, 16);
            this.LayersLabel.TabIndex = 0;
            this.LayersLabel.Text = "Layers:";
            // 
            // ThumbnailSplitter
            // 
            this.ThumbnailSplitter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ThumbnailSplitter.Location = new System.Drawing.Point(354, 49);
            this.ThumbnailSplitter.Name = "ThumbnailSplitter";
            this.ThumbnailSplitter.Size = new System.Drawing.Size(10, 611);
            this.ThumbnailSplitter.TabIndex = 5;
            this.ThumbnailSplitter.TabStop = false;
            // 
            // PanelPaging
            // 
            this.PanelPaging.Controls.Add(this.thumbnailXpressPaging);
            this.PanelPaging.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelPaging.Location = new System.Drawing.Point(364, 49);
            this.PanelPaging.Name = "PanelPaging";
            this.PanelPaging.Size = new System.Drawing.Size(138, 611);
            this.PanelPaging.TabIndex = 8;
            // 
            // thumbnailXpressPaging
            // 
            this.thumbnailXpressPaging.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thumbnailXpressPaging.BottomMargin = 5;
            this.thumbnailXpressPaging.CameraRaw = false;
            this.thumbnailXpressPaging.CellBorderColor = System.Drawing.Color.Black;
            this.thumbnailXpressPaging.CellBorderWidth = 1;
            this.thumbnailXpressPaging.CellHeight = 80;
            this.thumbnailXpressPaging.CellHorizontalSpacing = 5;
            this.thumbnailXpressPaging.CellSpacingColor = System.Drawing.Color.White;
            this.thumbnailXpressPaging.CellVerticalSpacing = 5;
            this.thumbnailXpressPaging.CellWidth = 100;
            this.thumbnailXpressPaging.DblClickDirectoryDrillDown = true;
            this.thumbnailXpressPaging.DescriptorAlignment = ((PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments)((PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignLeft | PegasusImaging.WinForms.ThumbnailXpress2.DescriptorAlignments.AlignBottom)));
            this.thumbnailXpressPaging.DescriptorDisplayMethod = PegasusImaging.WinForms.ThumbnailXpress2.DescriptorDisplayMethods.Default;
            this.thumbnailXpressPaging.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumbnailXpressPaging.EnableAsDragSourceForExternalDragDrop = false;
            this.thumbnailXpressPaging.EnableAsDropTargetForExternalDragDrop = false;
            this.thumbnailXpressPaging.ErrorAction = PegasusImaging.WinForms.ThumbnailXpress2.ErrorAction.UseErrorIcon;
            this.thumbnailXpressPaging.FtpPassword = "";
            this.thumbnailXpressPaging.FtpUserName = "";
            this.thumbnailXpressPaging.InterComponentThumbnailDragDropEnabled = false;
            this.thumbnailXpressPaging.IntraComponentThumbnailDragDropEnabled = false;
            this.thumbnailXpressPaging.LeftMargin = 5;
            this.thumbnailXpressPaging.Location = new System.Drawing.Point(0, 0);
            this.thumbnailXpressPaging.MaximumThumbnailBitDepth = 24;
            this.thumbnailXpressPaging.Name = "thumbnailXpressPaging";
            this.thumbnailXpressPaging.PreserveBlack = false;
            this.thumbnailXpressPaging.ProxyServer = "";
            this.thumbnailXpressPaging.RightMargin = 5;
            this.thumbnailXpressPaging.ScrollDirection = PegasusImaging.WinForms.ThumbnailXpress2.ScrollDirection.Vertical;
            this.thumbnailXpressPaging.SelectBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.thumbnailXpressPaging.SelectionMode = PegasusImaging.WinForms.ThumbnailXpress2.SelectionMode.Single;
            this.thumbnailXpressPaging.ShowHourglass = true;
            this.thumbnailXpressPaging.ShowImagePlaceholders = false;
            this.thumbnailXpressPaging.Size = new System.Drawing.Size(138, 611);
            this.thumbnailXpressPaging.TabIndex = 0;
            this.thumbnailXpressPaging.TextBackColor = System.Drawing.Color.White;
            this.thumbnailXpressPaging.TopMargin = 5;
            this.thumbnailXpressPaging.MouseDown += new System.Windows.Forms.MouseEventHandler(this.thumbnailXpressPaging_MouseDown);
            // 
            // PanelView
            // 
            this.PanelView.Controls.Add(this.imageXView);
            this.PanelView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelView.Location = new System.Drawing.Point(502, 49);
            this.PanelView.Name = "PanelView";
            this.PanelView.Size = new System.Drawing.Size(667, 611);
            this.PanelView.TabIndex = 0;
            // 
            // imageXView
            // 
            this.imageXView.AllowDrop = true;
            this.imageXView.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView.AutoScroll = true;
            this.imageXView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageXView.Location = new System.Drawing.Point(0, 0);
            this.imageXView.MouseWheelCapture = false;
            this.imageXView.Name = "imageXView";
            this.imageXView.Size = new System.Drawing.Size(667, 611);
            this.imageXView.TabIndex = 0;
            this.imageXView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageXView_MouseDown);
            this.imageXView.DragEnter += new System.Windows.Forms.DragEventHandler(this.imageXView_DragEnter);
            this.imageXView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageXView_MouseMove);
            this.imageXView.ZoomFactorChanged += new PegasusImaging.WinForms.ImagXpress9.ImageXView.ZoomFactorChangedEventHandler(this.imageXView_ZoomFactorChanged);
            this.imageXView.ToolEvent += new PegasusImaging.WinForms.ImagXpress9.ImageXView.ToolEventHandler(this.imageXView_ToolEvent);
            this.imageXView.MouseLeave += new System.EventHandler(this.imageXView_MouseLeave);
            // 
            // SplitterView
            // 
            this.SplitterView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitterView.Location = new System.Drawing.Point(502, 49);
            this.SplitterView.Name = "SplitterView";
            this.SplitterView.Size = new System.Drawing.Size(10, 611);
            this.SplitterView.TabIndex = 2;
            this.SplitterView.TabStop = false;
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = PegasusImaging.WinForms.NotateXpress9.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            this.notateXpress1.AnnotationAdded += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
            this.notateXpress1.AnnotationDeleted += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
            this.notateXpress1.AnnotationMoved += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationMovedEventHandler(this.notateXpress1_AnnotationMoved);
            // 
            // pdfXpress1
            // 
            this.pdfXpress1.Debug = false;
            this.pdfXpress1.DebugLogFile = "C:\\Documents and Settings\\jargento.JPG.COM\\Local Settings\\Temp\\PDFXpress2.log";
            this.pdfXpress1.ErrorLevel = PegasusImaging.WinForms.PdfXpress2.ErrorLevel.Production;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 682);
            this.Controls.Add(this.SplitterView);
            this.Controls.Add(this.PanelView);
            this.Controls.Add(this.PanelPaging);
            this.Controls.Add(this.ThumbnailSplitter);
            this.Controls.Add(this.PanelThumbnail);
            this.Controls.Add(this.Statusbar);
            this.Controls.Add(this.Toolbar);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ImagXpress 9 Demo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.Toolbar.ResumeLayout(false);
            this.Toolbar.PerformLayout();
            this.Statusbar.ResumeLayout(false);
            this.Statusbar.PerformLayout();
            this.PanelThumbnail.ResumeLayout(false);
            this.PanelSplitThumbnailDrillDown.ResumeLayout(false);
            this.PanelBottomThumb.ResumeLayout(false);
            this.PanelBottomThumb.PerformLayout();
            this.PanelProperties.ResumeLayout(false);
            this.TabRoll.ResumeLayout(false);
            this.InfoTab.ResumeLayout(false);
            this.panelImageInfoGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridImageInfo)).EndInit();
            this.panelMetaData.ResumeLayout(false);
            this.ViewTab.ResumeLayout(false);
            this.ViewTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomOutNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomInNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magHeightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magFactorNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.magWidthNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectXNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspectYNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interceptNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slopeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zoomNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BrightBar)).EndInit();
            this.ThumbTab.ResumeLayout(false);
            this.ThumbTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.threadHungNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threadStartNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threadCountNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vertSpacingNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horzSpacingNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderWidthNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellHeightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellWidthNumericUpDown)).EndInit();
            this.ColorTab.ResumeLayout(false);
            this.ColorTab.PerformLayout();
            this.cmykGroupBox.ResumeLayout(false);
            this.cmykGroupBox.PerformLayout();
            this.hslGroupBox.ResumeLayout(false);
            this.hslGroupBox.PerformLayout();
            this.rgbaGroupBox.ResumeLayout(false);
            this.rgbaGroupBox.PerformLayout();
            this.LayersCadTab.ResumeLayout(false);
            this.LayersCadTab.PerformLayout();
            this.PanelPaging.ResumeLayout(false);
            this.PanelView.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStrip Toolbar;
        private System.Windows.Forms.StatusStrip Statusbar;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton SaveButton;
        private System.Windows.Forms.ToolStripButton NoteButton;
        private System.Windows.Forms.ToolStripButton StampButton;
        private System.Windows.Forms.ToolStripButton TextButton;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton ZoomInButton;
        private System.Windows.Forms.ToolStripButton ZoomRectangleButton;
        private System.Windows.Forms.ToolStripButton ZoomHeightButton;
        private System.Windows.Forms.ToolStripButton ZoomOutButton;
        private System.Windows.Forms.ToolStripSeparator AnnotationSeparator;
        private System.Windows.Forms.ToolStripButton FreehandButton;
        private System.Windows.Forms.ToolStripButton HighlightButton;
        private System.Windows.Forms.ToolStripButton LineButton;
        private System.Windows.Forms.ToolStripButton PolygonButton;
        private System.Windows.Forms.ToolStripButton PolylineButton;
        private System.Windows.Forms.ToolStripButton RectangleButton;
        private System.Windows.Forms.ToolStripSeparator PagingSeparator;
        private System.Windows.Forms.ToolStripButton RefreshButton;
        private System.Windows.Forms.ToolStripSeparator ViewingSeparator;
        private System.Windows.Forms.ToolStripButton ZoomFitButton;
        private System.Windows.Forms.ToolStripButton ZoomWidthButton;
        private System.Windows.Forms.ToolStripButton MagnifierButton;
        private System.Windows.Forms.ToolStripButton HelpInfoButton;
        private System.Windows.Forms.ToolStripLabel AnnotationsToolStrip;
        private System.Windows.Forms.ToolStripButton HandButton;
        private System.Windows.Forms.ToolStripButton PreviousButton;
        private System.Windows.Forms.ToolStripButton NextButton;
        private System.Windows.Forms.ToolStripSeparator HelpSeparator;
        private System.Windows.Forms.ToolStripLabel PageLabel;
        private System.Windows.Forms.ToolStripTextBox PageBox;
        private System.Windows.Forms.ToolStripLabel PageCountLabel;
        private System.Windows.Forms.ToolStripButton PointerNXButton;
        private System.Windows.Forms.ToolStripButton EllipseButton;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printProToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thumbnailXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem twainProToolStripMenuItem;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Panel PanelThumbnail;
        private System.Windows.Forms.Splitter ThumbnailSplitter;
        private System.Windows.Forms.Panel PanelProperties;
        private System.Windows.Forms.Splitter MiddleSplitter;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel MousePositionLabel;
        private System.Windows.Forms.Panel PanelPaging;
        private System.Windows.Forms.Panel PanelView;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView;
        private PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress thumbnailXpressDrillDown;
        private PegasusImaging.WinForms.ThumbnailXpress2.ThumbnailXpress thumbnailXpressPaging;
        private System.Windows.Forms.Splitter SplitterView;
        private System.Windows.Forms.ToolStripButton PointerIXButton;
        private System.Windows.Forms.ToolStripButton AreaButton;
        private System.Windows.Forms.ToolStripButton CutButton;
        private System.Windows.Forms.ToolStripButton CopyButton;
        private System.Windows.Forms.ToolStripButton PasteButton;
        private System.Windows.Forms.ToolStripMenuItem fileOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem directoryOpenToolStripMenuItem;
        private PegasusImaging.WinForms.NotateXpress9.NotateXpress notateXpress1;
        private System.Windows.Forms.ToolStripButton CloseButton;
        private System.Windows.Forms.ToolStripMenuItem pdfXpressToolStripMenuItem;
        private PegasusImaging.WinForms.PdfXpress2.PdfXpress pdfXpress1;
        private System.Windows.Forms.ToolStripButton PrintButton;
        private System.Windows.Forms.ToolStripMenuItem printImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scanImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton Zoom1to1Button;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Panel PanelSplitThumbnailDrillDown;
        private System.Windows.Forms.TextBox thumbnailTextBox;
        private System.Windows.Forms.Panel PanelBottomThumb;
        private System.Windows.Forms.Button ClearThumbButton;
        private System.Windows.Forms.ToolStripButton BrandButton;
        private System.Windows.Forms.ToolStripButton RulerButton;
        private System.Windows.Forms.ToolStripButton ImageButton;
        private System.Windows.Forms.ToolStripButton ButtonButton;
        private System.Windows.Forms.ToolStripMenuItem sampleImagesToolStripMenuItem;
        private System.Windows.Forms.TabControl TabRoll;
        private System.Windows.Forms.TabPage InfoTab;
        private System.Windows.Forms.Panel panelImageInfoGrid;
        private System.Windows.Forms.DataGridView dataGridImageInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Panel panelMetaData;
        private System.Windows.Forms.Button metaDataButton;
        private System.Windows.Forms.TabPage ViewTab;
        private System.Windows.Forms.Label zoomOutLabel;
        private System.Windows.Forms.NumericUpDown zoomOutNumericUpDown;
        private System.Windows.Forms.Label zoomInLabel;
        private System.Windows.Forms.NumericUpDown zoomInNumericUpDown;
        private System.Windows.Forms.Label magHeightLabel;
        private System.Windows.Forms.NumericUpDown magHeightNumericUpDown;
        private System.Windows.Forms.Label magnificationLabel;
        private System.Windows.Forms.NumericUpDown magFactorNumericUpDown;
        private System.Windows.Forms.Label magWidthLabel;
        private System.Windows.Forms.NumericUpDown magWidthNumericUpDown;
        private System.Windows.Forms.Button backColorButton;
        private System.Windows.Forms.Label backColorLabel;
        private System.Windows.Forms.Label aspectXLabel;
        private System.Windows.Forms.NumericUpDown aspectXNumericUpDown;
        private System.Windows.Forms.Label aspectYLabel;
        private System.Windows.Forms.NumericUpDown aspectYNumericUpDown;
        private System.Windows.Forms.CheckBox alphaCheckBox;
        private System.Windows.Forms.Label RenderLabel;
        private System.Windows.Forms.ComboBox renderComboBox;
        private System.Windows.Forms.Label DisplayLabel;
        private System.Windows.Forms.ComboBox displayComboBox;
        private System.Windows.Forms.CheckBox proofCheckBox;
        private System.Windows.Forms.CheckBox ICMCheckBox;
        private System.Windows.Forms.Label PaletteLabel;
        private System.Windows.Forms.ComboBox paletteComboBox;
        private System.Windows.Forms.CheckBox progressiveCheckBox;
        private System.Windows.Forms.Label interceptLabel;
        private System.Windows.Forms.Label SlopeLabel;
        private System.Windows.Forms.NumericUpDown interceptNumericUpDown;
        private System.Windows.Forms.NumericUpDown slopeNumericUpDown;
        private System.Windows.Forms.ComboBox grayComboBox;
        private System.Windows.Forms.ComboBox VerticalComboBox;
        private System.Windows.Forms.ComboBox HorzComboBox;
        private System.Windows.Forms.Label HorzLabel;
        private System.Windows.Forms.Label VerticalLabel;
        private System.Windows.Forms.Label ZoomLabel;
        private System.Windows.Forms.NumericUpDown zoomNumericUpDown;
        private System.Windows.Forms.CheckBox smoothCheckBox;
        private System.Windows.Forms.CheckBox preserveCheckBox;
        private System.Windows.Forms.CheckBox ditherCheckBox;
        private System.Windows.Forms.CheckBox antiCheckBox;
        private System.Windows.Forms.Label GrayLabel;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Label BrightValueLabel;
        private System.Windows.Forms.Label ConValueLabel;
        private System.Windows.Forms.Label ContrastLabel;
        private System.Windows.Forms.Label BrightnessLabel;
        private System.Windows.Forms.TrackBar ConBar;
        private System.Windows.Forms.TrackBar BrightBar;
        private System.Windows.Forms.TabPage ThumbTab;
        private System.Windows.Forms.NumericUpDown threadHungNumericUpDown;
        private System.Windows.Forms.Label threadHungLabel;
        private System.Windows.Forms.NumericUpDown threadStartNumericUpDown;
        private System.Windows.Forms.Label threadStartLabel;
        private System.Windows.Forms.CheckBox hourGlassCheckBox;
        private System.Windows.Forms.Label ErrorActionLabel;
        private System.Windows.Forms.ComboBox errorComboBox;
        private System.Windows.Forms.NumericUpDown threadCountNumericUpDown;
        private System.Windows.Forms.Label ThreadCountLabel;
        private System.Windows.Forms.CheckBox preserveBlackCheckBox;
        private System.Windows.Forms.Button selectBackColorButton;
        private System.Windows.Forms.Label BackColorSelectLabel;
        private System.Windows.Forms.CheckBox cameraRawCheckBox;
        private System.Windows.Forms.Label scrollLabel;
        private System.Windows.Forms.ComboBox scrollDirectionComboBox;
        private System.Windows.Forms.Label bitDepthLabel;
        private System.Windows.Forms.ComboBox bitDepthComboBox;
        private System.Windows.Forms.Button textBackColorButton;
        private System.Windows.Forms.Label TextBackColorLabel;
        private System.Windows.Forms.CheckBox placeHoldersCheckBox;
        private System.Windows.Forms.Button fontButton;
        private System.Windows.Forms.Label FontLabel;
        private System.Windows.Forms.Label ThumbnailLabel;
        private System.Windows.Forms.ComboBox thumbComboBox;
        private System.Windows.Forms.Label DescriptorDisplayLabel;
        private System.Windows.Forms.ComboBox descriptorDisplayComboBox;
        private System.Windows.Forms.Label HozAlignLabel;
        private System.Windows.Forms.ComboBox descriptorHorzComboBox;
        private System.Windows.Forms.Label DescriptorVLabel;
        private System.Windows.Forms.ComboBox descriptorVertComboBox;
        private System.Windows.Forms.Label VerticalSpacingLabel;
        private System.Windows.Forms.NumericUpDown vertSpacingNumericUpDown;
        private System.Windows.Forms.Label CellHorzLabel;
        private System.Windows.Forms.NumericUpDown horzSpacingNumericUpDown;
        private System.Windows.Forms.Button borderButton;
        private System.Windows.Forms.Label BoderColorLabel;
        private System.Windows.Forms.Button SpacingButton;
        private System.Windows.Forms.Label SpacingColorLabel;
        private System.Windows.Forms.Label borderWidthLabel;
        private System.Windows.Forms.NumericUpDown borderWidthNumericUpDown;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.NumericUpDown cellHeightNumericUpDown;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.NumericUpDown cellWidthNumericUpDown;
        private System.Windows.Forms.Label boderThumbLabel;
        private System.Windows.Forms.TabPage ColorTab;
        private System.Windows.Forms.Button resetComponentsButton;
        private System.Windows.Forms.Label componentsLabel;
        private System.Windows.Forms.GroupBox cmykGroupBox;
        private System.Windows.Forms.RadioButton blackRadioButton;
        private System.Windows.Forms.RadioButton yellowRadioButton;
        private System.Windows.Forms.RadioButton magentaRadioButton;
        private System.Windows.Forms.RadioButton cyanRadioButton;
        private System.Windows.Forms.GroupBox hslGroupBox;
        private System.Windows.Forms.RadioButton luminanceRadioButton;
        private System.Windows.Forms.RadioButton saturationRadioButton;
        private System.Windows.Forms.RadioButton hueRadioButton;
        private System.Windows.Forms.GroupBox rgbaGroupBox;
        private System.Windows.Forms.RadioButton alphaRadioButton;
        private System.Windows.Forms.RadioButton blueRadioButton;
        private System.Windows.Forms.RadioButton greenRadioButton;
        private System.Windows.Forms.RadioButton redRadioButton;
        private System.Windows.Forms.Label histogramLabel;
        private System.Windows.Forms.ComboBox comboBoxHistogram;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewHistogram;
        private System.Windows.Forms.ToolStripMenuItem rawFileOpenToolStripMenuItem;
        private System.Windows.Forms.TabPage LayersCadTab;
        private System.Windows.Forms.Label LayersLabel;
        private System.Windows.Forms.Button paletteButton;
        private System.Windows.Forms.Button PaletteFileButton;
        private System.Windows.Forms.Button printerProfileButton;
        private System.Windows.Forms.Button targetButton;
        private System.Windows.Forms.Button monitorButton;
        private System.Windows.Forms.ToolStripSplitButton OpenButton;
        private System.Windows.Forms.ToolStripMenuItem fileWithLoadOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripDropDownMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileWithLoadOptionsDropDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem directoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveasNoOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem withSaveOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rawFileToolStripMenuItem;
        private PegasusImaging.WinForms.TwainPro5.TwainPro twainPro1;
        private PegasusImaging.WinForms.PrintPro4.PrintPro printPro1;
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.ToolStripButton ScanButton;
        private System.Windows.Forms.ToolStripButton RotateLeftButton;
        private System.Windows.Forms.ToolStripButton RotateRightButton;
        private System.Windows.Forms.ToolStripMenuItem documentCleanupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem photoEnhancementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filtersAndEffectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem binarizeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem blobRemovalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem borderCropToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deskewToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem despeckleToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem dilateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem erodeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem lineRemovalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem negateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem shearToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem smoothZoomToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alphaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem brightnessToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem colorBalanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorModifyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contrastToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cropToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem orientationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redEyeRemovalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeDustToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removeScratchesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resizeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sharpenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sharpenToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem softenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem unsharpenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem autoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cropBroderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flattenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reduceToAlphaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brightnessToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gammaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem colorBalanceAutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem levelAutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorBalanceManualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adjustHSLToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem adjustRGBToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem colorDepthToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem replaceColorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem autoToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem equalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lightnessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lightnessManualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blurToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem buttonizeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem diffuseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem embossToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem matrixToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem medianToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mosaicToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem motionBlurToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem negateToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem noiseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outlineToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem parabolicToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem perspectiveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pinchToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem polynomialWarpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem posterizeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rippleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem solarizeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem swirlToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem twistToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem flipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mirrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cropToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cleanupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deskewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem despeckleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dilateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem erodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentCleanupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem blobRemovalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borderDetectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deskewToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem despeckleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem photoEnhancementToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alphaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alpha1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alpha2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alpha3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redEyeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redEye1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redEye2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redEye3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem redEye4ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem scratchedToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removeDustToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blobRemoval1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blobRemoval2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borderCrop1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borderCrop2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deskew1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deskew2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deskew3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem despeckle1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem despeckle2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem despeckle3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dilateToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem erodeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem shearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smoothZoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLines1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeLines2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effects1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effects2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effects3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cropToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orientationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rotate1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeDust1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeDust2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpen1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpen2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel ViewingLabel;
        private System.Windows.Forms.ToolStripMenuItem tagsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem binarizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eXIFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorBalanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem autoLevel1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoLevel2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoBalance1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoBalance2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contrastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoLightnessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoContrastToolStripMenuItem;
        private System.Windows.Forms.Label PaletteButtonLabel;
        private System.Windows.Forms.ToolStripMenuItem gettingStartedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dropDownSampleImages;
        private System.Windows.Forms.ToolStripMenuItem dropDownDocument;
        private System.Windows.Forms.ToolStripMenuItem dropDownBinarize;
        private System.Windows.Forms.ToolStripMenuItem dropDownBlob;
        private System.Windows.Forms.ToolStripMenuItem dropDownBlob1;
        private System.Windows.Forms.ToolStripMenuItem dropDownBlob2;
        private System.Windows.Forms.ToolStripMenuItem dropDownBorder;
        private System.Windows.Forms.ToolStripMenuItem dropDownBorder1;
        private System.Windows.Forms.ToolStripMenuItem dropDownBorder2;
        private System.Windows.Forms.ToolStripMenuItem dropDownDeskew;
        private System.Windows.Forms.ToolStripMenuItem dropDownDeskew1;
        private System.Windows.Forms.ToolStripMenuItem dropDownDeskew2;
        private System.Windows.Forms.ToolStripMenuItem dropDownDeskew3;
        private System.Windows.Forms.ToolStripMenuItem dropDownDespeckle;
        private System.Windows.Forms.ToolStripMenuItem dropDownDespeckle1;
        private System.Windows.Forms.ToolStripMenuItem dropDownDespeckle2;
        private System.Windows.Forms.ToolStripMenuItem dropDownDespeckle3;
        private System.Windows.Forms.ToolStripMenuItem dropDownDilate;
        private System.Windows.Forms.ToolStripMenuItem dropDownErode;
        private System.Windows.Forms.ToolStripMenuItem dropDownMulti;
        private System.Windows.Forms.ToolStripMenuItem dropDownNegate;
        private System.Windows.Forms.ToolStripMenuItem dropDownLineRemoval;
        private System.Windows.Forms.ToolStripMenuItem dropDownLineRemoval1;
        private System.Windows.Forms.ToolStripMenuItem dropDownLineRemoval2;
        private System.Windows.Forms.ToolStripMenuItem dropDownShear;
        private System.Windows.Forms.ToolStripMenuItem dropDownSmooth;
        private System.Windows.Forms.ToolStripMenuItem dropDownTags;
        private System.Windows.Forms.ToolStripMenuItem dropDownPhoto;
        private System.Windows.Forms.ToolStripMenuItem dropDownAlpha;
        private System.Windows.Forms.ToolStripMenuItem dropDownAlpha1;
        private System.Windows.Forms.ToolStripMenuItem dropDownAlpha2;
        private System.Windows.Forms.ToolStripMenuItem dropDownAlpha3;
        private System.Windows.Forms.ToolStripMenuItem dropDownColorBalance;
        private System.Windows.Forms.ToolStripMenuItem dropDownColorBalance1;
        private System.Windows.Forms.ToolStripMenuItem dropDownColorBalance2;
        private System.Windows.Forms.ToolStripMenuItem dropDownColorBalance3;
        private System.Windows.Forms.ToolStripMenuItem dropDownColorBalance4;
        private System.Windows.Forms.ToolStripMenuItem dropDownContrast;
        private System.Windows.Forms.ToolStripMenuItem dropDownContrast1;
        private System.Windows.Forms.ToolStripMenuItem dropDownContrast2;
        private System.Windows.Forms.ToolStripMenuItem dropDownCrop;
        private System.Windows.Forms.ToolStripMenuItem dropDownExif;
        private System.Windows.Forms.ToolStripMenuItem dropDownOrientation;
        private System.Windows.Forms.ToolStripMenuItem dropDownOrientation1;
        private System.Windows.Forms.ToolStripMenuItem dropDownOrientation2;
        private System.Windows.Forms.ToolStripMenuItem dropDownRedEye;
        private System.Windows.Forms.ToolStripMenuItem dropDownRedEye1;
        private System.Windows.Forms.ToolStripMenuItem dropDownRedEye2;
        private System.Windows.Forms.ToolStripMenuItem dropDownRedEye3;
        private System.Windows.Forms.ToolStripMenuItem dropDownRedEye4;
        private System.Windows.Forms.ToolStripMenuItem dropDownDust;
        private System.Windows.Forms.ToolStripMenuItem dropDownDust1;
        private System.Windows.Forms.ToolStripMenuItem dropDownDust2;
        private System.Windows.Forms.ToolStripMenuItem dropDownScratch;
        private System.Windows.Forms.ToolStripMenuItem dropDownSharpen;
        private System.Windows.Forms.ToolStripMenuItem dropDownSharpen1;
        private System.Windows.Forms.ToolStripMenuItem dropDownSharpen2;
        private System.Windows.Forms.ToolStripMenuItem dropDownEffects;
        private System.Windows.Forms.ToolStripMenuItem dropDownEffects1;
        private System.Windows.Forms.ToolStripMenuItem dropDownEffects2;
        private System.Windows.Forms.ToolStripMenuItem dropDownEffects3;
    }
}


//****************************************************************'
//* Copyright 2008- Pegasus Imaging Corporation, Tampa Florida. *'
//* This sample code is provided to Pegasus licensees "as is"    *'
//* with no restrictions on use or modification. No warranty for *'
//* use of this sample code is provided by Pegasus.              *'
//****************************************************************'

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using PegasusImaging.WinForms.ImagXpress9;

namespace JPGCompression
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmCompression : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageX imagX1;
		private PegasusImaging.WinForms.ImagXpress9.SaveOptions soSaveOptions;
		private PegasusImaging.WinForms.ImagXpress9.LoadOptions loLoadOptions;
		

		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.HScrollBar hsChrom;
		private System.Windows.Forms.HScrollBar hsLumin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rb111;
		private System.Windows.Forms.RadioButton rb211;
		private System.Windows.Forms.RadioButton rb411;
		private System.Windows.Forms.RadioButton rb211v;
		private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblShowCR;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Label lblError;

		private String strCurrentDir;
		private String 	strImageFile; 
			
		private long lCompressedSize;
		private long lFileSize;
        private double dCompressionRatio;
        private IContainer components;



		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		
		private System.Windows.Forms.Label lblChrom;
		private System.Windows.Forms.Label lblLumin;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuQuit;
		
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		string GetFileName(System.String FullName) 
		{
			return (FullName.Substring(FullName.LastIndexOf("\\")+1,FullName.Length - FullName.LastIndexOf("\\") - 1));
		}
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}
	
		#endregion

		public frmCompression()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				
				//  Don't forget to dispose IX
				// 
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}

				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuFileOpen = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuQuit = new System.Windows.Forms.MenuItem();
            this.mnuToolbar = new System.Windows.Forms.MenuItem();
            this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.lstInfo = new System.Windows.Forms.ListBox();
            this.hsChrom = new System.Windows.Forms.HScrollBar();
            this.hsLumin = new System.Windows.Forms.HScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb211v = new System.Windows.Forms.RadioButton();
            this.rb411 = new System.Windows.Forms.RadioButton();
            this.rb211 = new System.Windows.Forms.RadioButton();
            this.rb111 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.lblShowCR = new System.Windows.Forms.Label();
            this.lblLastError = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblChrom = new System.Windows.Forms.Label();
            this.lblLumin = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(8, 88);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(424, 280);
            this.imageXView1.TabIndex = 0;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.mnuToolbar,
            this.mnuAbout});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileOpen,
            this.menuItem3,
            this.mnuQuit});
            this.menuItem1.Text = "File";
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.Index = 0;
            this.mnuFileOpen.Text = "Open";
            this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = 2;
            this.mnuQuit.Text = "Quit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // mnuToolbar
            // 
            this.mnuToolbar.Index = 1;
            this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuToolbarShow});
            this.mnuToolbar.Text = "Toolbar";
            // 
            // mnuToolbarShow
            // 
            this.mnuToolbarShow.Index = 0;
            this.mnuToolbarShow.Text = "Show";
            this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 2;
            this.mnuAbout.Text = "About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // lstInfo
            // 
            this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstInfo.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Varying the JPEG quality settings using the SaveJPEGChromFactor, SaveJPEGLumFac" +
                "tor ",
            " and SaveJPEGSubSampling properties.",
            "2)Saving an image to a Memory Stream."});
            this.lstInfo.Location = new System.Drawing.Point(8, 8);
            this.lstInfo.Name = "lstInfo";
            this.lstInfo.Size = new System.Drawing.Size(680, 69);
            this.lstInfo.TabIndex = 1;
            // 
            // hsChrom
            // 
            this.hsChrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.hsChrom.Location = new System.Drawing.Point(72, 384);
            this.hsChrom.Maximum = 255;
            this.hsChrom.Name = "hsChrom";
            this.hsChrom.Size = new System.Drawing.Size(344, 16);
            this.hsChrom.TabIndex = 2;
            this.hsChrom.Value = 10;
            this.hsChrom.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsChrom_Scroll);
            // 
            // hsLumin
            // 
            this.hsLumin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.hsLumin.Location = new System.Drawing.Point(72, 440);
            this.hsLumin.Maximum = 255;
            this.hsLumin.Name = "hsLumin";
            this.hsLumin.Size = new System.Drawing.Size(344, 16);
            this.hsLumin.TabIndex = 3;
            this.hsLumin.Value = 1;
            this.hsLumin.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsLumin_Scroll);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Location = new System.Drawing.Point(24, 384);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 40);
            this.label1.TabIndex = 4;
            this.label1.Text = "Chrom.";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Location = new System.Drawing.Point(24, 440);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 40);
            this.label2.TabIndex = 5;
            this.label2.Text = "Lum.";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rb211v);
            this.groupBox1.Controls.Add(this.rb411);
            this.groupBox1.Controls.Add(this.rb211);
            this.groupBox1.Controls.Add(this.rb111);
            this.groupBox1.Location = new System.Drawing.Point(16, 496);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 48);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sub Sampling";
            // 
            // rb211v
            // 
            this.rb211v.Location = new System.Drawing.Point(288, 16);
            this.rb211v.Name = "rb211v";
            this.rb211v.Size = new System.Drawing.Size(96, 24);
            this.rb211v.TabIndex = 3;
            this.rb211v.Text = "2:1:1 v";
            this.rb211v.CheckedChanged += new System.EventHandler(this.rb211v_CheckedChanged);
            // 
            // rb411
            // 
            this.rb411.Location = new System.Drawing.Point(184, 16);
            this.rb411.Name = "rb411";
            this.rb411.Size = new System.Drawing.Size(64, 24);
            this.rb411.TabIndex = 2;
            this.rb411.Text = "4:1:1";
            this.rb411.CheckedChanged += new System.EventHandler(this.rb411_CheckedChanged);
            // 
            // rb211
            // 
            this.rb211.Location = new System.Drawing.Point(112, 16);
            this.rb211.Name = "rb211";
            this.rb211.Size = new System.Drawing.Size(56, 24);
            this.rb211.TabIndex = 1;
            this.rb211.Text = "2:1:1";
            this.rb211.CheckedChanged += new System.EventHandler(this.rb211_CheckedChanged);
            // 
            // rb111
            // 
            this.rb111.Location = new System.Drawing.Point(24, 16);
            this.rb111.Name = "rb111";
            this.rb111.Size = new System.Drawing.Size(64, 24);
            this.rb111.TabIndex = 0;
            this.rb111.Text = "1:1:1";
            this.rb111.CheckedChanged += new System.EventHandler(this.rb111_CheckedChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(16, 552);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 40);
            this.label3.TabIndex = 7;
            this.label3.Text = "Compression Ratio:";
            // 
            // lblShowCR
            // 
            this.lblShowCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblShowCR.Location = new System.Drawing.Point(136, 552);
            this.lblShowCR.Name = "lblShowCR";
            this.lblShowCR.Size = new System.Drawing.Size(104, 40);
            this.lblShowCR.TabIndex = 8;
            this.lblShowCR.Text = "lblShowCR";
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(453, 96);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(184, 32);
            this.lblLastError.TabIndex = 11;
            this.lblLastError.Text = "Last Error:";
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.Location = new System.Drawing.Point(453, 144);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(200, 120);
            this.lblError.TabIndex = 12;
            // 
            // lblChrom
            // 
            this.lblChrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblChrom.Location = new System.Drawing.Point(432, 384);
            this.lblChrom.Name = "lblChrom";
            this.lblChrom.Size = new System.Drawing.Size(40, 32);
            this.lblChrom.TabIndex = 13;
            // 
            // lblLumin
            // 
            this.lblLumin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLumin.Location = new System.Drawing.Point(432, 440);
            this.lblLumin.Name = "lblLumin";
            this.lblLumin.Size = new System.Drawing.Size(40, 24);
            this.lblLumin.TabIndex = 14;
            // 
            // frmCompression
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(696, 617);
            this.Controls.Add(this.lblLumin);
            this.Controls.Add(this.lblChrom);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lblShowCR);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hsLumin);
            this.Controls.Add(this.hsChrom);
            this.Controls.Add(this.lstInfo);
            this.Controls.Add(this.imageXView1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "frmCompression";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImagXpress JPG Compression";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmCompression());
		}

	 
		private void Form1_Load(object sender, System.EventArgs e)
		{
			try 
			{

				// **The UnlockRuntime function must be called to distribute the runtime**
				// imagXpress1.Licensing.UnlockRuntime(12345, 12345, 12345, 12345)
                imageXView1.ScrollBars = ScrollBars.None;

				//Create a new load options object so we can recieve events from the images we load
				loLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
				
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}

			imagX1 = new PegasusImaging.WinForms.ImagXpress9.ImageX(imagXpress1);
		
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString (cultNumber);
			strImageFile = System.IO.Path.Combine (strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\window.jpg");
			strCurrentDir = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\");


			soSaveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();

			lblChrom.Text =  soSaveOptions.Jpeg.Chrominance.ToString(cultNumber);
			lblLumin.Text = soSaveOptions.Jpeg.Luminance.ToString(cultNumber);
			hsChrom.Value = soSaveOptions.Jpeg.Chrominance;
			hsLumin.Value =  soSaveOptions.Jpeg.Luminance;

			rb411.Checked = true;
			
			LoadFile();
					
		}
		private void UpdateImage()
		{
				
			soSaveOptions.Jpeg.Chrominance = hsChrom.Value;
			soSaveOptions.Jpeg.Luminance = hsLumin.Value ;

			soSaveOptions.Format = PegasusImaging.WinForms.ImagXpress9.ImageXFormat.Jpeg; 

			lblChrom.Text =  soSaveOptions.Jpeg.Chrominance.ToString(cultNumber);
			lblLumin.Text = soSaveOptions.Jpeg.Luminance.ToString(cultNumber);
		
			System.IO.MemoryStream fsStream = new System.IO.MemoryStream(); 

			try
			{
				//save the file to memory
				
				imagX1.SaveStream(fsStream,soSaveOptions);

				
				fsStream.Flush();
				fsStream.Position = 0;

				
				
				//load the stream
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromStream(imagXpress1, fsStream);

				
				//Compute the compression ratio
				lCompressedSize = fsStream.Length;

				lFileSize = imagX1.ImageXData.Width;
				lFileSize = lFileSize * imagX1.ImageXData.Height * 3;

				if(lCompressedSize > 0)
				{
					dCompressionRatio = (double)lFileSize/(double)lCompressedSize;
					lblShowCR.Text =  dCompressionRatio.ToString(cultNumber);
				}
				else
				{
					lblShowCR.Text = "N/A";
				}
			}
			catch(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			
			fsStream.Close();
		}

		private void LoadFile()
		{
			try 
			{
				imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFile, loLoadOptions);

				//clear out the error in case there was an error from a previous operation
				lblError.Text = "";		
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			catch(System.IO.IOException ex)
			{
				PegasusError(ex,lblError);
			}
			UpdateImage();
			
		}

		private void rb111_CheckedChanged(object sender, System.EventArgs e)
		{
			soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress9.SubSampling.SubSampling111;
			LoadFile();
		
		}

		private void rb211_CheckedChanged(object sender, System.EventArgs e)
		{
			soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress9.SubSampling.SubSampling211;
			LoadFile();
		}

		private void rb411_CheckedChanged(object sender, System.EventArgs e)
		{
			soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress9.SubSampling.SubSampling411;
			LoadFile();
		}

		private void rb211v_CheckedChanged(object sender, System.EventArgs e)
		{
			soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress9.SubSampling.SubSampling211v;
			LoadFile();
		}

		private void hsChrom_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			LoadFile();
		}

		private void hsLumin_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			LoadFile();
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			
				strImageFile = PegasusOpenFile();
			LoadFile();
				

		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			this.mnuToolbarShow.Text = (imageXView1.Toolbar.Activated) ? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		
		
	}
}

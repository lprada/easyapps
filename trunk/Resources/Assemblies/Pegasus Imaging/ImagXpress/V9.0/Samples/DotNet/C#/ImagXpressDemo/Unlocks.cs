/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;

namespace ImagXpressDemo
{
    class Unlocks
    {
        public void UnlockControls(Component component)
        {
            Helper helper = new Helper();

            //helper.UnlockControls(component);
        }
    }
}

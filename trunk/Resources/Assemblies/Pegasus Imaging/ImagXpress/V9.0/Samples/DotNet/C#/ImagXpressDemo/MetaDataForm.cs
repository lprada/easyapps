/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo
{
    public partial class MetaDataForm : Form
    {
        public MetaDataForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();

        private int pageCount, pageNumber;
        private string filename;
        
        //tag class to handle EXIF and TIF tags
        Tags tagClass = new Tags();

        private LoadOptions lo = new LoadOptions();

        public int PageCount
        {
            set
            {
                pageCount = value;
            }
        }

        public string FileName
        {
            set
            {
                filename = value;
            }
        }

        public string commentText
        {
            set
            {
                commentTextBox.Text = value;
            }
        }

        private void MetaDataForm_Load(object sender, EventArgs e)
        {


            unlock.UnlockControls(imagXpress1);


            //set the paging
            if (pageCount > 1)
            {
                nextButton.Enabled = true;
            }

            countLabel.Text = "of " + pageCount.ToString();

            pageNumber = 1;

            //load the tags
            GetTags(pageNumber);
        }

        private void IPTCTags(ImageXIptcCollection tags)
        {
            for (int i = 0; i < tags.Count; i++)
            {
                listViewIPTC.Items.Add(new ListViewItem(new string[] {tagClass.IPTCHelper(tags[i].DataSetType, 
                    tags[i].DataSet), tags[i].DataSetType.ToString(), tags[i].DataSet.ToString(), tags[i].DataFieldString}));
            }
        }

        private void GetTags(int pageNumber)
        {
            try
            {
                listViewThumbnail.Items.Clear();
                listViewIPTC.Items.Clear();
                listViewCals.Items.Clear();
               
                //retrieve the tags for the image opened at the page specified
                using (ImageX tagsImageX = ImageX.ReadAllMetaData(imagXpress1, filename, pageNumber))
                {
                    if (tagsImageX.IptcDataSet != null)
                    {
                        IPTCTags(tagsImageX.IptcDataSet);
                    }
                    else
                    {
                        listViewIPTC.Items.Add("No tags found");
                    }

                    //use the Tag class to show the tags in the ListView
                    if (tagsImageX.Tags != null)
                    {
                        tagClass.ShowTags(tagsImageX.Tags, listViewTags, listViewThumbnail);
                    }
                    else
                    {
                        listViewTags.Items.Add("No tags found");
                    }

                    if (tagsImageX.CalTags != null)
                    {
                        foreach (ImageCalsTag tag in tagsImageX.CalTags)
                        {
                            listViewCals.Items.Add(new ListViewItem(new string[] { tag.CalsId, tag.CalsData }));
                        }
                    }
                    else
                    {
                        listViewCals.Items.Add("No tags found");
                    }
                }

                //get the thumbnail image offset
                lo.ImageOffset = (int)tagClass.ThumbnailOffset;

                if (lo.ImageOffset != 0)
                {
                    //resource cleanup
                    if (imageXView1.Image != null)
                        imageXView1.Image.Dispose();

                    //load the thumbnail image
                    imageXView1.Image = ImageX.FromFile(imagXpress1, filename, lo);

                    //set to DPI
                    imageXView1.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch;

                    //show the thumbnail properties
                    thumbDescriptionLabel.Text = "Width: " + imageXView1.Image.ImageXData.Width.ToString() +
                        ", Height: " + imageXView1.Image.ImageXData.Height.ToString() + ", Format: " + imageXView1.Image.ImageXData.Format.ToString()
                        + ", Resolution: " + imageXView1.Image.ImageXData.Resolution.Dimensions.ToString() + " DPI";
                }
                else
                {                    
                    listViewThumbnail.Items.Add("No thumbnail found");
                }
            }
            catch (ImageXException)
            {
                //if no tags found, clear the ListView, and inform user no tags were found
                listViewTags.Items.Clear();
                listViewTags.Items.Add("No Tags Found");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void nextButton_Click(object sender, EventArgs e)
        { 
            //set the paging
            previousButton.Enabled = true;

            pageNumber += 1;

            if (pageNumber == pageCount)
            {
                nextButton.Enabled = false;
            }

            pageTextBox.Text = pageNumber.ToString();

            //load the tags
            GetTags(pageNumber);
        }

        private void previousButton_Click(object sender, EventArgs e)
        { 
            //set the paging
            nextButton.Enabled = true;

            pageNumber -= 1;

            if (pageNumber == 1)
            {
                previousButton.Enabled = false;
            }

            pageTextBox.Text = pageNumber.ToString();

            //load the tags
            GetTags(pageNumber);
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                //collect the tag information and copy to the clipboard

                StringBuilder sb = new StringBuilder();

                sb.Append("TIFF/EXIF tags");
                sb.AppendLine();

                for (int i = 0; i < listViewTags.Items.Count; i++)
                {
                    for (int j = 0; j < listViewTags.Items[i].SubItems.Count; j++)
                    {
                        string newStr = listViewTags.Items[i].SubItems[j].Text.Replace('\0', '.');

                        sb.Append(newStr + " ");
                    }

                    sb.AppendLine();
                }

                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("Thumbnail:");

                for (int i = 0; i < listViewThumbnail.Items.Count; i++)
                {
                    for (int j = 0; j < listViewThumbnail.Items[i].SubItems.Count; j++)
                    {
                        string newStr = listViewThumbnail.Items[i].SubItems[j].Text.Replace('\0', '.');

                        sb.Append(newStr + " ");
                    }

                    sb.AppendLine();
                }

                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("JPEG Comments:");

                sb.Append(commentTextBox.Text);

                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("IPTC Tags:");

                for (int i = 0; i < listViewIPTC.Items.Count; i++)
                {
                    for (int j = 0; j < listViewIPTC.Items[i].SubItems.Count; j++)
                    {
                        string newStr = listViewIPTC.Items[i].SubItems[j].Text.Replace('\0', '.');

                        sb.Append(newStr + " ");
                    }

                    sb.AppendLine();
                }

                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("CALS Tags:");

                for (int i = 0; i < listViewCals.Items.Count; i++)
                {
                    for (int j = 0; j < listViewCals.Items[i].SubItems.Count; j++)
                    {
                        string newStr = listViewCals.Items[i].SubItems[j].Text.Replace('\0', '.');

                        sb.Append(newStr + " ");
                    }

                    sb.AppendLine();
                }

                Clipboard.SetText(sb.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class RemoveDustForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveDustForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.smoothBox = new System.Windows.Forms.NumericUpDown();
            this.filterBox = new System.Windows.Forms.NumericUpDown();
            this.thresholdBox = new System.Windows.Forms.NumericUpDown();
            this.labelSmoothingAdjustment = new System.Windows.Forms.Label();
            this.labelFilterAdjustment = new System.Windows.Forms.Label();
            this.labelThreshold = new System.Windows.Forms.Label();
            this.comboBoxDefectType = new System.Windows.Forms.ComboBox();
            this.labelDefectType = new System.Windows.Forms.Label();
            this.listBoxDescription = new System.Windows.Forms.ListBox();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.smoothBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(403, 531);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(637, 531);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(519, 531);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(12, 531);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 36;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // smoothBox
            // 
            this.smoothBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.smoothBox.Location = new System.Drawing.Point(328, 500);
            this.smoothBox.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.smoothBox.Name = "smoothBox";
            this.smoothBox.Size = new System.Drawing.Size(51, 20);
            this.smoothBox.TabIndex = 44;
            // 
            // filterBox
            // 
            this.filterBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.filterBox.Location = new System.Drawing.Point(142, 498);
            this.filterBox.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.filterBox.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            -2147483648});
            this.filterBox.Name = "filterBox";
            this.filterBox.Size = new System.Drawing.Size(51, 20);
            this.filterBox.TabIndex = 43;
            this.filterBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // thresholdBox
            // 
            this.thresholdBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.thresholdBox.Location = new System.Drawing.Point(142, 471);
            this.thresholdBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.thresholdBox.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.thresholdBox.Name = "thresholdBox";
            this.thresholdBox.Size = new System.Drawing.Size(51, 20);
            this.thresholdBox.TabIndex = 42;
            this.thresholdBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // labelSmoothingAdjustment
            // 
            this.labelSmoothingAdjustment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSmoothingAdjustment.AutoSize = true;
            this.labelSmoothingAdjustment.Location = new System.Drawing.Point(207, 502);
            this.labelSmoothingAdjustment.Name = "labelSmoothingAdjustment";
            this.labelSmoothingAdjustment.Size = new System.Drawing.Size(115, 13);
            this.labelSmoothingAdjustment.TabIndex = 41;
            this.labelSmoothingAdjustment.Text = "Smoothing Adjustment:";
            // 
            // labelFilterAdjustment
            // 
            this.labelFilterAdjustment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFilterAdjustment.AutoSize = true;
            this.labelFilterAdjustment.Location = new System.Drawing.Point(21, 500);
            this.labelFilterAdjustment.Name = "labelFilterAdjustment";
            this.labelFilterAdjustment.Size = new System.Drawing.Size(87, 13);
            this.labelFilterAdjustment.TabIndex = 40;
            this.labelFilterAdjustment.Text = "Filter Adjustment:";
            // 
            // labelThreshold
            // 
            this.labelThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelThreshold.AutoSize = true;
            this.labelThreshold.Location = new System.Drawing.Point(21, 474);
            this.labelThreshold.Name = "labelThreshold";
            this.labelThreshold.Size = new System.Drawing.Size(57, 13);
            this.labelThreshold.TabIndex = 39;
            this.labelThreshold.Text = "Threshold:";
            // 
            // comboBoxDefectType
            // 
            this.comboBoxDefectType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxDefectType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDefectType.FormattingEnabled = true;
            this.comboBoxDefectType.Items.AddRange(new object[] {
            "Light Defect",
            "Dark Defect"});
            this.comboBoxDefectType.Location = new System.Drawing.Point(282, 471);
            this.comboBoxDefectType.Name = "comboBoxDefectType";
            this.comboBoxDefectType.Size = new System.Drawing.Size(117, 21);
            this.comboBoxDefectType.TabIndex = 38;
            // 
            // labelDefectType
            // 
            this.labelDefectType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDefectType.AutoSize = true;
            this.labelDefectType.Location = new System.Drawing.Point(207, 474);
            this.labelDefectType.Name = "labelDefectType";
            this.labelDefectType.Size = new System.Drawing.Size(69, 13);
            this.labelDefectType.TabIndex = 37;
            this.labelDefectType.Text = "Defect Type:";
            // 
            // listBoxDescription
            // 
            this.listBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxDescription.FormattingEnabled = true;
            this.listBoxDescription.Items.AddRange(new object[] {
            "Please use the mouse on the Current Image ",
            "to select an Area to Remove Dust."});
            this.listBoxDescription.Location = new System.Drawing.Point(12, 418);
            this.listBoxDescription.Name = "listBoxDescription";
            this.listBoxDescription.Size = new System.Drawing.Size(259, 43);
            this.listBoxDescription.TabIndex = 45;
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 51;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 365);
            this.tableLayoutPanel.TabIndex = 50;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 359);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 359);
            this.imageXViewCurrent.TabIndex = 0;
            this.imageXViewCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageXViewCurrent_MouseUp);
            this.imageXViewCurrent.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageXViewCurrent_MouseDown);
            this.imageXViewCurrent.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageXViewCurrent_MouseMove);
            // 
            // RemoveDustForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 564);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.listBoxDescription);
            this.Controls.Add(this.smoothBox);
            this.Controls.Add(this.filterBox);
            this.Controls.Add(this.thresholdBox);
            this.Controls.Add(this.labelSmoothingAdjustment);
            this.Controls.Add(this.labelFilterAdjustment);
            this.Controls.Add(this.labelThreshold);
            this.Controls.Add(this.comboBoxDefectType);
            this.Controls.Add(this.labelDefectType);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RemoveDustForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "RemoveDust";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.smoothBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.NumericUpDown smoothBox;
        private System.Windows.Forms.NumericUpDown filterBox;
        private System.Windows.Forms.NumericUpDown thresholdBox;
        private System.Windows.Forms.Label labelSmoothingAdjustment;
        private System.Windows.Forms.Label labelFilterAdjustment;
        private System.Windows.Forms.Label labelThreshold;
        private System.Windows.Forms.ComboBox comboBoxDefectType;
        private System.Windows.Forms.Label labelDefectType;
        private System.Windows.Forms.ListBox listBoxDescription;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.NotateXpress9;
using PegasusImaging.WinForms.PdfXpress2;
using PegasusImaging.WinForms.ThumbnailXpress2;
using PegasusImaging.WinForms.TwainPro5;

namespace ImagXpressDemo
{
    public partial class MainForm : Form
    {
        Unlocks unlock = new Unlocks();
        
        Helper helper = new Helper();

        public MainForm()
        {
            InitializeComponent();

            helper.CenterScreen(this);
        }

        #region Win32 API Functions

        [DllImport("user32.dll")]
        private static extern IntPtr LoadCursorFromFile(string lpFileName);

        [DllImport("kernel32.dll")]
        static extern UIntPtr GlobalSize(IntPtr hMem);

        #endregion

        #region Sample Images

        //common images path
        string commonImagePath;

        //paths of sample files, installed with SDK
        private string Alpha1ImagePath, Alpha2ImagePath, Alpha3ImagePath, BinarizeImagePath, ExifImagePath, MultiImagePath, 
            RedEye1ImagePath, RedEye2ImagePath, RedEye3ImagePath, RedEye4ImagePath, Dust1ImagePath, Dust2ImagePath,
            Sharpen1ImagePath, Sharpen2ImagePath, TagsImagePath, AutoBalance1ImagePath, AutoBalance2ImagePath, AutoLevel1ImagePath, 
            AutoLevel2ImagePath, AutoLevel3ImagePath, AutoLightness1ImagePath, AutoLightness2ImagePath, CropImagePath, 
            AutoContrastImagePath, Rotate1ImagePath, Rotate2ImagePath, ScratchedImagePath, Effects1ImagePath, Effects2ImagePath,
            Effects3ImagePath, Blob1ImagePath, Blob2ImagePath, Border1ImagePath, Border2ImagePath,
            Deskew1ImagePath, Deskew2ImagePath, Deskew3ImagePath, Despeckle1ImagePath, Despeckle2ImagePath, Despeckle3ImagePath,
            DilateImagePath, ErodeImagePath, NegateImagePath, Lines1ImagePath, Lines2ImagePath, ShearImagePath, SmoothImagePath;

        //flag indicating whether a sample image is loaded or not
        private bool SampleImageLoad;

        #endregion

        #region UI Fields

        //boolean to identify if form is being closed
        private bool formClosing;

        //array of toolbar buttons
        private ToolStripButton[] AnnotateToolbar, ViewToolbar;

        private ToolStripButton[] toolbarButtons;
        private ToolStripMenuItem[] menuItems;

        //colors to be used with ColorDialogs
        private Color borderColor, spacingColor, backColor, textBackColor, selectBackColor;

        private Font font;

        //array of radio buttons for Color channel selection
        private RadioButton[] radioRGBAButtons, radioHSLButtons, radioCMYKButtons;

        //pens used for Histogram drawing
        private Pen redPen = new Pen(Color.Red);
        private Pen greenPen = new Pen(Color.Green);
        private Pen bluePen = new Pen(Color.Blue);
        private Pen blackPen = new Pen(Color.Black);

        #endregion

        #region PDFXpress Fields
        
        //PDFXpress render options
        private RenderOptions ro;
        
        //PDF page number
        private int PDFPageNumber;
        
        //indicates whether a PDF was chosen or not
        private bool PDFOpened;

        private string fontPath;
        private string cmapPath;
        private string iconPath;       

        #endregion

        #region Dialog Fields

        private string directorySaved;

        //index to store filter type chosen from FileDialogs
        private int openFilterIndex, saveFilterIndex;

        //string that stores filename of image currently being viewed
        private string filenameOpened;

        //string that stores a copy of the file currently being viewed, used in the case that the viewed file is being overwritten
        private string copyOfOpenFile;

        //string that stores the directory of the current image file opened
        private string directoryOpened;

        //save file dialog result from MessageBox shown whenever recent file changes have occurred and the user
        //is trying to load a new image
        private DialogResult saveChangesResult;

        //color dialogs
        private ColorDialog borderColorDialog, spacingColorDialog, backColorDialog, textBackColorDialog, selectBackColorDialog;

        //font dialog
        private FontDialog fontDialog;

        #endregion

        #region ThumbnailXpress Fields

        //location where mouse is over ThumbnailXpressDrillDown control
        private Point mouseOverPointThumbnailDrillDown;

        //thumbnail item for ThumbnailXpressDrillDown control
        private ThumbnailItem mouseOverItemThumbnailDrillDown;

        //list to keep track of recent thumbnails & whether they're images or directories for ThumbnailXpressDrillDown
        private ArrayList recentImages, recentImagesIdentify;

        //width of ThumbnailXpressPaging control
        private int thumbWidth;

        //parent directory of directory drilled down into by ThumbnailXpressDrillDown
        private string parentDirectory;

        //filter for ThumbnailXpresDrillDown
        private Filter filterThumbnails;

        //horizontal vertical alignment of ThumbnailXpress variables
        private DescriptorAlignments alignH, alignV;

        //display method of descriptor of ThumbnailXpress control variable
        private DescriptorDisplayMethods descriptorDisplay;

        //ThumbnailXpress chosen by comboBox selection
        private ThumbnailXpress thumbChosen;

        private string thumbnailFilterPattern = @"(*.jpg)|(*.bmp)|(*.gif)|(*.cal)|(*.dib)|(*.dca)|(*.mod)|(*.modca)|(*.dcx)|(*.gif)|(*.ico)|(*.jp2)|(*.jls)|(*.jpeg)|(*.jif)|(*.ljp)|(*.pbm)|(*.pcx)|(*.pdf)|(*.tpdf)|(*.pgm)|(*.pic)|(*.png)|(*.ppm)|(*.tif)|(*.tiff)|(*.tga)|(*.wmf)|(*.wsq)|(*.jb2)|(*.dwg)|(*.dwf)|(*.dxf)|(*.hdp)|(*.wdp)";

        #endregion

        #region ImagXpress Fields

        private bool metaOff;

        //indicates whether or not Fast Tiff write is used
        private bool fastTiff;

        //IFD offset for fast Tiff write
        private int fastTiffOffset;

        //ArrayList to store images when editing occurs
        private ArrayList imagesOpened, indexImagesOpened;

        //page number and page count for non-pdf images
        private int pageNumber, pageCount;

        //flag indicating whether or not current image has been changed in any way
        private bool editFlag;

        //points of selection rectangle drawn by user
        private Point startPointSelection, endPointSelection;
        private PointF mouseUpdatePoint;

        //processor of ImagXpress for image processing operations
        private Processor processor1;

        //indicates whether CAD image is opened or not        
        private bool cadOpened;

        private bool cadInitial;

        //ImageXData associated with CAD image
        private ImageXData dataForCad;

        //indicates whether or not metafile is opened
        private bool metaOpened;
        
        //ImageX associated with ImageTool annotation of NotateXpress
        private ImageX imageToolImage;

        //array of ImageX's for PrintForm
        private ImageX[] PrintImages;

        //keeps track of loadoptions used
        private ArrayList recentLoadOptions;
        
        //flag indicating whether or not image is opened via the open file dialog
        private bool initialImageLoad;

        //array of CheckBoxes for CAD layers
        private CheckBox[] cadLayerCheckBoxes;

        //global loadOptions variable for ImagXpress
        private PegasusImaging.WinForms.ImagXpress9.LoadOptions loadOptions;

        //image path for ImageTool annotation of NotateXpress
        private string PathImageToolImage;

        //arrays for RGB intensity data for histogram
        private int[] redData = new int[256];
        private int[] greenData = new int[256];
        private int[] blueData = new int[256];

        //bitmaps for color channel selection
        private Bitmap redImage, greenImage, blueImage, alphaImage;
        private Bitmap hueImage, saturationImage, luminanceImage;
        private Bitmap cyanImage, magentaImage, yellowImage, blackImage;

        private ImageXFormat formatView;
        private int heightView, widthView, bppView, sizeView;
        private SizeF resolutionView;
        private Compression compressionView;

        private string jpegComments;

        private bool imageRefresh;

        private int rawWidth = 100, rawHeight = 100, rawStride = 100;
        private int rawHighBit = 0, rawOffset = 0;
        private int rawBits = 1, rawBytes = 1;
        private ImageXPixelFormat rawChosen = ImageXPixelFormat.Dib;

        #endregion

        #region NotateXpress Fields

        //current AnnotationTool selected
        private AnnotationTool currentTool;

        //layer of NotateXpress for annotations
        private Layer layer;

        //ArrayList to store layerCollection data
        private ArrayList layerData;

        //global load and save options for NotateXpress
        private PegasusImaging.WinForms.NotateXpress9.SaveOptions annotateSaveOptions;
        private PegasusImaging.WinForms.NotateXpress9.LoadOptions annotateLoadOptions;

        //boolean to identify if annotations are updating or not
        private bool annotationUpdating;

        #endregion

        #region Startup Helper Methods

        private void QuickStartHelper()
        {
            QuickStartForm startForm = new QuickStartForm();

            helper.CenterScreen(startForm);

            startForm.Show();
        }

        #endregion

        #region Main Form Event Handlers

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (File.Exists(helper.quickStartFile) == true)
            {
                using (TextReader tw = new StreamReader(helper.quickStartFile))
                {
                    if (tw.ReadLine() != helper.gettingStartedKey)
                    {
                        QuickStartHelper();
                    }
                }
            }
            else
            {
                QuickStartHelper();
            }
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                //display the splash form to the user for 5 seconds unless it's clicked first
                using (SplashForm splsh = new SplashForm())
                {
                    helper.CenterScreen(splsh);

                    splsh.ShowDialog();
                }

                //must use to create thumbnails of PDF's, because PDFXpress can open virtually any PDF
                thumbnailXpressPaging.Licensing.AccessPdfXpress2();
                thumbnailXpressDrillDown.Licensing.AccessPdfXpress2();

                Unlocks unlock = new Unlocks();

                unlock.UnlockControls(imagXpress1);
                unlock.UnlockControls(thumbnailXpressDrillDown);
                unlock.UnlockControls(thumbnailXpressPaging);
                unlock.UnlockControls(pdfXpress1);
                unlock.UnlockControls(printPro1);
                unlock.UnlockControls(twainPro1);

                DataGridViewPopulate();

                InitializeControls();

                thumbnailXpressDrillDown.Font = fontDialog.Font;
                thumbnailXpressPaging.Font = fontDialog.Font;                
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //identify that the main form is closing
            formClosing = true;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            ZoomImage();
        }

        #endregion

        #region Initialize Methods

        private void InitializeControls()
        {
            string[] commonPaths = helper.GetPath();

            commonImagePath = commonPaths[0];
            fontPath = commonPaths[1];
            cmapPath = commonPaths[2];
            iconPath = commonPaths[3];

            PathImageToolImage = commonImagePath + "shack8.tif";

            Alpha1ImagePath = commonImagePath + "Alpha1.tif";
            Alpha2ImagePath = commonImagePath + "Alpha2.tif";
            Alpha3ImagePath = commonImagePath + "Alpha3.tif";
            AutoLevel1ImagePath = commonImagePath + "autoLevel.jpg";
            AutoBalance1ImagePath = commonImagePath + "Auto_Balance1.JPG";
            BinarizeImagePath = commonImagePath + "Binarize.jpg";
            ExifImagePath = commonImagePath + "EXIF.jpg";
            MultiImagePath = commonImagePath + "multi.tif";
            RedEye1ImagePath = commonImagePath + "RedEye1.jpg";
            RedEye2ImagePath = commonImagePath + "RedEye2.jpg";
            RedEye3ImagePath = commonImagePath + "RedEye3.jpg";
            RedEye4ImagePath = commonImagePath + "RedEye4.jpg";
            ScratchedImagePath = commonImagePath + "scratched.jpg";
            Dust1ImagePath = commonImagePath + "Dust.jpg";
            Dust2ImagePath = commonImagePath + "Dust1.jpg";
            Sharpen1ImagePath = commonImagePath + "Sharpen1.jpg";
            Sharpen2ImagePath = commonImagePath + "Sharpen2.jpg";
            TagsImagePath = commonImagePath + "1040filled1.tif";
            Effects1ImagePath = commonImagePath + "Effects1.jpg";
            Effects2ImagePath = commonImagePath + "Effects2.jpg";
            Effects3ImagePath = commonImagePath + "Effects3.jpg";
            AutoBalance2ImagePath = commonImagePath + "AutoBalance2.jpg";
            AutoLevel2ImagePath = commonImagePath + "AutoLevel1.jpg";
            AutoLevel3ImagePath = commonImagePath + "AutoLevel2.jpg";
            AutoLightness1ImagePath = commonImagePath + "AutoLightness1.jpg";
            AutoLightness2ImagePath = commonImagePath + "AutoLightness2.jpg";
            CropImagePath = commonImagePath + "crop.jpg";
            AutoContrastImagePath = commonImagePath + "AutoContrast.jpg";
            Rotate1ImagePath = commonImagePath + "Rotate1.jpg";
            Rotate2ImagePath = commonImagePath + "Rotate2.jpg";
            Blob1ImagePath = commonImagePath + "blob1.tif";
            Blob2ImagePath = commonImagePath + "blob2.tif";
            Border1ImagePath = commonImagePath + "Border1.tif";
            Border2ImagePath = commonImagePath + "Border2.tif";
            Deskew1ImagePath = commonImagePath + "Deskew.tif";
            Deskew2ImagePath = commonImagePath + "Deskew2.tif";
            Deskew3ImagePath = commonImagePath + "Deskew3.tif";
            Despeckle1ImagePath = commonImagePath + "Despeckle.tif";
            Despeckle2ImagePath = commonImagePath + "Despeckle2.tif";
            Despeckle3ImagePath = commonImagePath + "Despeckle3.tif";
            DilateImagePath = commonImagePath + "Dilate.tif";
            ErodeImagePath = commonImagePath + "Erode.tif";
            SmoothImagePath = commonImagePath + "Document.tif";
            ShearImagePath = commonImagePath + "Shear.tif";
            NegateImagePath = commonImagePath + "Negate.tif";
            Lines1ImagePath = commonImagePath + "Lines.tif";
            Lines2ImagePath = commonImagePath + "Lines.bmp";

            #region ThumbnailXpress Initialize

            //initialize lists for keeping track of thumbnails and whether they're images or folders
            recentImages = new ArrayList();
            recentImagesIdentify = new ArrayList();

            //keep track of width of ThumbnailXpress Paging control
            thumbWidth = PanelPaging.Width;

            //initialize lists for keeping track of images opened when they're edited
            imagesOpened = new ArrayList();
            indexImagesOpened = new ArrayList();

            //Main Form shouldn't have ThumbnailXpress Paging control visible when first loaded
            SinglePageImage();
            PageBox.Text = "";
            PageCountLabel.Text = "";

            //want to Filter thumbnail items so only ImagXpress readable image files are visible besides folders
            filterThumbnails = thumbnailXpressDrillDown.CreateFilter("DrillDownFilter");
            filterThumbnails.FilenamePattern = thumbnailFilterPattern;

            thumbnailXpressDrillDown.Filters.Add(filterThumbnails);

            #endregion

            #region ImagXpress Initialize

            for (int i =0 ;i < imageXView.ContextMenu.MenuItems.Count; i++)
            {
                imageXView.ContextMenu.MenuItems[i].Click += new EventHandler(ContextMenuClickEventHandler);
            }

            //initialize list for keeping track of loadOptions used
            recentLoadOptions = new ArrayList();

            //initialize ImagXpress Processor
            processor1 = new Processor(imagXpress1);

            //erase default right click context menu
            imageXViewHistogram.ContextMenu = null;

            #endregion

            #region PDFXpress Initialize

            //must call Initialize() method first for PDFXpress
            if (Directory.Exists(fontPath) == true && Directory.Exists(cmapPath) == true)
            {
                pdfXpress1.Initialize(fontPath, cmapPath);
            }
            else
            {
                pdfXpress1.Initialize();
            }

            //set render options for PDF, 300DPI
            ro = new RenderOptions();
            ro.ProduceDibSection = false;
            ro.ResolutionX = 300;
            ro.ResolutionY = 300;

            #endregion

            #region NotateXpress Initialize

            AnnotationDefaults();

            //hook NotateXpress to ImagXpress, this must be done first or annotations can not be added
            notateXpress1.ClientWindow = imageXView.Handle;

            //default annotation tool is PointerTool
            currentTool = AnnotationTool.PointerTool;

            layerData = new ArrayList();
            annotateLoadOptions = new PegasusImaging.WinForms.NotateXpress9.LoadOptions();
            annotateSaveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();

            #endregion

            #region Dialog Initialize

            //initialize FolderBrowser Dialog and OpenFileDialog to default to Common\Images directory
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

            if (Directory.Exists(commonImagePath) == true)
            {
                folderBrowserDialog.SelectedPath = Path.GetFullPath(commonImagePath);
            }
            else
            {
                folderBrowserDialog.SelectedPath = Application.StartupPath;
            }

            //initialize Color dialogs
            borderColorDialog = new ColorDialog();
            spacingColorDialog = new ColorDialog();
            backColorDialog = new ColorDialog();
            fontDialog = new FontDialog();
            textBackColorDialog = new ColorDialog();
            selectBackColorDialog = new ColorDialog();

            #endregion

            #region UI Initialize

            //create array of Toolbar buttons
            AnnotateToolbar = new ToolStripButton[] { TextButton, NoteButton, RectangleButton,
                                 PolygonButton, PolylineButton, LineButton, FreehandButton,
                                 StampButton, EllipseButton, HighlightButton, PointerNXButton,
                                 ImageButton, RulerButton, ButtonButton};

            ViewToolbar = new ToolStripButton[]{AreaButton, ZoomOutButton, RotateLeftButton, RotateRightButton, MagnifierButton, ZoomRectangleButton, 
                                  HandButton, PointerIXButton, ZoomInButton};

            toolbarButtons = new ToolStripButton[]{CloseButton, SaveButton, CutButton, CopyButton, PasteButton, RefreshButton,
                                   SaveButton, PrintButton, Zoom1to1Button, ZoomFitButton, ZoomHeightButton, ZoomWidthButton, 
                                   ZoomInButton,ZoomOutButton,RotateLeftButton, RotateRightButton, ZoomRectangleButton, PointerIXButton, HandButton, AreaButton, MagnifierButton,
                                   RectangleButton, PointerNXButton, TextButton, LineButton, PolygonButton, PolylineButton, HighlightButton,
                                   StampButton, EllipseButton, FreehandButton, NoteButton, BrandButton, ImageButton, RulerButton, 
                                   ButtonButton};

            menuItems = new ToolStripMenuItem[]{closeToolStripMenuItem, saveAsToolStripMenuItem, saveToolStripMenuItem, 
                                   documentCleanupToolStripMenuItem, photoEnhancementToolStripMenuItem,filtersAndEffectsToolStripMenuItem,
                                   printImageToolStripMenuItem, saveAnnotationsToolStripMenuItem, loadAnnotationsToolStripMenuItem};
             
            //create arrays of RadioButtons
            radioRGBAButtons = new RadioButton[] { redRadioButton, greenRadioButton, blueRadioButton, alphaRadioButton };
            radioHSLButtons = new RadioButton[] { hueRadioButton, saturationRadioButton, luminanceRadioButton };
            radioCMYKButtons = new RadioButton[] { cyanRadioButton, magentaRadioButton, yellowRadioButton, blackRadioButton };

            //initialize Selected item of ComboBoxes
            grayComboBox.SelectedIndex = 0;
            VerticalComboBox.SelectedIndex = 1;
            HorzComboBox.SelectedIndex = 1;
            paletteComboBox.SelectedIndex = 0;
            displayComboBox.SelectedIndex = 0;
            renderComboBox.SelectedIndex = 0;
            thumbComboBox.SelectedIndex = 0;
            descriptorHorzComboBox.SelectedIndex = 0;//left justified
            descriptorVertComboBox.SelectedIndex = 2;
            descriptorDisplayComboBox.SelectedIndex = 1;
            bitDepthComboBox.SelectedIndex = 0;
            scrollDirectionComboBox.SelectedIndex = 1;
            errorComboBox.SelectedIndex = 0;

            backColor = imageXView.BackColor;
            backColorButton.BackColor = backColor;
            backColorDialog.CustomColors = new int[] { GetRGBColor(backColorButton.BackColor) };

            threadHungNumericUpDown.Value = 9000;

            comboBoxHistogram.SelectedIndex = 0;

            //remove CAD tab on startup
            TabRoll.TabPages.RemoveAt(4);
          
            #endregion
        }

        #endregion

        #region Open/Close Image & Paging Methods

        private void ImageLoadIntoView()
        {
            imageXView.AllowUpdate = false;

            if (PDFOpened == true)
            {
                //PDF documents are read by PDFXpress and are rendered as images for display
                using (Bitmap bp = pdfXpress1.Documents[0].RenderPageToBitmap(PDFPageNumber, ro))
                {
                    //resource cleanup
                    if (imageXView != null)
                    {
                        if (imageXView.Image != null)
                        {
                            imageXView.Image.Dispose();
                            imageXView.Image = null;
                        }
                    }

                    imageXView.Image = ImageX.FromBitmap(imagXpress1, bp);
                    imageXView.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch;
                    imageXView.Image.ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX,(float) ro.ResolutionY);
                }
            }
            else
            {
                if (initialImageLoad == false)
                {
                    for (int i = 0; i < recentImages.Count; i++)
                    {
                        if ((string)recentImages[i] == filenameOpened && imageRefresh == true)
                        {
                            if (i < recentLoadOptions.Count - 1)
                            {
                                loadOptions = (PegasusImaging.WinForms.ImagXpress9.LoadOptions)recentLoadOptions[i];

                                break;
                            }
                            else
                            {
                                loadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();

                                recentLoadOptions.Add(loadOptions);

                                break;
                            }
                        }
                    }
                }

                if (cadOpened == true)
                {
                    //set CadData for loadOptions
                    loadOptions.Cad.CadData = dataForCad;

                    //need to set layout to render, analogous to page number of multi-page image file
                    loadOptions.Cad.LayoutToRender = pageNumber - 1;

                    if (cadInitial == true)
                    {
                        //resource cleanup
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = ImageX.CadFileOpen(imagXpress1, filenameOpened, loadOptions);
                        cadInitial = false;
                    }

                    imageXView.Image.CadFileView(loadOptions);

                    //if file is opened via file dialog and was opened once before and the loadOptions have changed
                    //compared to then then update them in the ArrayList
                    for (int i = 0; i < recentImages.Count; i++)
                    {
                        if ((string)recentImages[i] == filenameOpened)
                        {
                            if (loadOptions != (PegasusImaging.WinForms.ImagXpress9.LoadOptions)recentLoadOptions[i])
                            {
                                recentLoadOptions[i] = loadOptions;

                                ThumbnailMemoryUpdate(i, thumbnailXpressDrillDown, (string)recentImages[i],
                                    (PegasusImaging.WinForms.ImagXpress9.LoadOptions)recentLoadOptions[i]);
                            }

                            break;
                        }
                    }
                }
                else
                {
                    if (metaOpened == true)
                    {
                        //convert metafiles to a bitmap so things like UniqueColorCount() will work
                        loadOptions.Metafile.ConvertToDib = true;
                    }

                    //resource cleanup
                    if (imageXView != null)
                    {
                        if (imageXView.Image != null)
                        {
                            imageXView.Image.Dispose();
                            imageXView.Image = null;
                        }
                    }

                    try
                    {
                        //if the image is not opened via the open file dialog, then use FromFile()
                        imageXView.Image = ImageX.FromFile(imagXpress1, filenameOpened, pageNumber, loadOptions);
                    }
                    catch (ImageXException ex)
                    {
                        //1015 corresponds to an invalid password
                        if (ex.Number == 1015)
                        {
                            using (PasswordForm passForm = new PasswordForm())
                            {
                                helper.CenterScreen(passForm);

                                passForm.SecureFileType = "PIC";

                                if (passForm.ShowDialog() == DialogResult.OK)
                                {
                                    loadOptions.Pic.Password = passForm.Password;

                                    try
                                    {
                                        imageXView.Image = ImageX.FromFile(imagXpress1, filenameOpened, pageNumber, loadOptions);
                                    }
                                    catch (ImageXException ex2)
                                    {
                                        if (ex2.Number == 1015)
                                        {
                                            MessageBox.Show("The password you entered is not correct.", "Error", 
                                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                                            ImageUpdate();
                                        }
                                        else
                                        {
                                            //otherwise throw the exception
                                            throw ex2;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //otherwise throw the exception
                            throw ex;
                        }
                    }

                    if (imageXView.Image != null)
                    {
                        if (initialImageLoad == true && metaOpened == true)
                        {
                            //if file is opened via file dialog and was opened once before and the loadOptions have changed
                            //compared to then then update them in the ArrayList
                            for (int i = 0; i < recentImages.Count; i++)
                            {
                                if ((string)recentImages[i] == filenameOpened)
                                {
                                    if (loadOptions != (PegasusImaging.WinForms.ImagXpress9.LoadOptions)recentLoadOptions[i])
                                    {
                                        recentLoadOptions[i] = loadOptions;

                                        ThumbnailMemoryUpdate(i, thumbnailXpressDrillDown, (string)recentImages[i],
                                            (PegasusImaging.WinForms.ImagXpress9.LoadOptions)recentLoadOptions[i]);
                                    }

                                    break;
                                }
                            }
                        }
                    }
                }
            }

            imageXView.AllowUpdate = true;
        }

        private void PagingUpdateUI(int page)
        {
            try
            {
                if (pageCount == 1)
                {
                    SinglePageImage();
                }
                else
                {
                    if (PDFOpened == true)
                    {
                        page = PDFPageNumber + 1;
                    }

                    thumbnailXpressPaging.SelectedItems.Clear();
                    //select the current thumbnail
                    thumbnailXpressPaging.SelectedItems.Add(thumbnailXpressPaging.Items[page - 1]);
                    
                    //scroll thumbnail selected into view
                    thumbnailXpressPaging.EnsureVisible(page - 1);

                    //update UI to reflect multi-page image file
                    PageBox.Enabled = true;
                    PageCountLabel.Text = "of " + pageCount.ToString();

                    if (page == pageCount)
                    {
                        NextButton.Enabled = false;
                        PreviousButton.Enabled = true;
                    }
                    else if (page == 1)
                    {
                        PreviousButton.Enabled = false;
                        NextButton.Enabled = true;
                    }
                    else
                    {
                        PreviousButton.Enabled = true;
                        NextButton.Enabled = true;
                    }
                }

                if (imageXView.Image != null)
                    ImageInfoTabsUpdate();

                ResetImageSelection();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void PagingUpdate()
        {
            //if a multi-page image file was opened add thumbnails for pages
            if (pageCount > 1)
            {
                thumbnailXpressPaging.Items.AddItemsFromFile(filenameOpened, 0, true);

                //show ThumbnailXpress paging control, because image is multi-page
                PanelPaging.Width = thumbWidth;
                SplitterView.Visible = true;
            }
            else
            {
                SinglePageImage();
            }

            //reset Annotation layers, add new layers for new image
            ResetNotate(pageCount);
        }

        private void CloseImage()
        {
            try
            {
                //disable radio buttons
                redRadioButton.Enabled = false;
                greenRadioButton.Enabled = false;
                blueRadioButton.Enabled = false;
                alphaRadioButton.Enabled = false;
                hueRadioButton.Enabled = false;
                saturationRadioButton.Enabled = false;
                luminanceRadioButton.Enabled = false;
                cyanRadioButton.Enabled = false;
                magentaRadioButton.Enabled = false;
                yellowRadioButton.Enabled = false;
                blackRadioButton.Enabled = false;

                resetComponentsButton.Enabled = false;
                
                if (pdfXpress1.Documents != null)
                {
                    for (int i = 0; i < pdfXpress1.Documents.Count; i++)
                    {
                        if (pdfXpress1.Documents[i] != null)
                            pdfXpress1.Documents[i].Dispose();
                    }

                    pdfXpress1.Documents.Clear();
                }

                imageXView.AllowUpdate = false;
               
                //resource cleanup, image closed so dispose of ImageX object
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        //close the cad file
                        if (cadOpened == true)                        
                            imageXView.Image.CadFileClose();

                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                }

                imageXView.AllowUpdate = true;

                //remove CAD tab
                ResetCadTab();

                //reset toolbar
                ToolbarMenu(false);

                //reset UI for single-page image
                SinglePageImage();

                //reset image info
                ImageInfoClear();

                //Don't need to retrieve recent items during close unless navigated to directory
                if (thumbnailTextBox.Text != "Recent Images")
                    RetrieveRecentItems();
                
                //remove selected thumbnail from ThumbnailXpresDrillDown, remove it from Recent Images
                //ArrayLists too
                for (int i = 0; i < thumbnailXpressDrillDown.Items.Count ; i++)
                {
                    if (filenameOpened == thumbnailXpressDrillDown.Items[i].Filename)
                    {
                        if (i < recentImages.Count)
                        {
                            recentImages.RemoveAt(i);
                            recentImagesIdentify.RemoveAt(i);
                        }
                        break;
                    }
                }

                if (thumbnailXpressDrillDown.SelectedItems.Count > 0)
                    thumbnailXpressDrillDown.Items.Remove(thumbnailXpressDrillDown.SelectedItems[0]);

                //since nothing is in ImageXView, we don't want a thumbnail selected
                thumbnailXpressDrillDown.SelectedItems.Clear();

                //clear ImageX's from memory
                ImageMemoryReset();

                //clear annotation layers
                ResetNotate();

                //reset color channel selection
                ClearColorRadioButtons();

                //resource cleanup
                if (imageXViewHistogram.Image != null)
                {
                    imageXViewHistogram.Image.Dispose();
                    imageXViewHistogram.Image = null;
                }

                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                if (redImage != null)
                {
                    redImage.Dispose();
                    redImage = null;
                }

                if (greenImage != null)
                {
                    greenImage.Dispose();
                    greenImage = null;
                }

                if (blueImage != null)
                {
                    blueImage.Dispose();
                    blueImage = null;
                }

                if (alphaImage != null)
                {
                    alphaImage.Dispose();
                    alphaImage = null;
                }

                if (hueImage != null)
                {
                    hueImage.Dispose();
                    hueImage = null;
                }

                if (saturationImage != null)
                {
                    saturationImage.Dispose();
                    saturationImage = null;
                }

                if (luminanceImage != null)
                {
                    luminanceImage.Dispose();
                    luminanceImage = null;
                }

                if (cyanImage != null)
                {
                    cyanImage.Dispose();
                    cyanImage = null;
                }

                if (magentaImage != null)
                {
                    magentaImage.Dispose();
                    magentaImage = null;
                }

                if (yellowImage != null)
                {
                    yellowImage.Dispose();
                    yellowImage = null;
                }

                if (blackImage != null)
                {
                    blackImage.Dispose();
                    blackImage = null;
                }

                comboBoxHistogram.Enabled = false;

                paletteButton.Enabled = false;

                metaDataButton.Enabled = false;

                PageBox.Text = "";
                PageCountLabel.Text = "";

                //ImageXView no longer has an image, so reset editFlag
                editFlag = false;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Paging(int page)
        {
            try
            {
                //image paged to should be reflected by thumbnail as well
                thumbnailXpressPaging.SelectedItems.Clear();

                try
                {
                    //turn painting of ImageXView off temporarily so control doesn't go to gray
                    imageXView.AllowUpdate = false;

                    //if the editFlag is true, want to update from memory if ImageX is already in memory,
                    //otherwise always load from disk or ImageUpdateViewMemoryOrFile() assigns new ImageX object to ImageXView.Image...
                    //so either way need to dispose of ImageXView.Image first
                    if (editFlag == true)
                    {
                        ImageUpdateViewMemoryOrFile();
                    }
                    else
                    {
                        ImageLoadIntoView();
                    }

                    if (imageXView.Image != null)
                    {
                        //set global variables for image properties
                        widthView = imageXView.Image.ImageXData.Width;
                        heightView = imageXView.Image.ImageXData.Height;
                        bppView = imageXView.Image.ImageXData.BitsPerPixel;
                        compressionView = imageXView.Image.ImageXData.Compression;
                        formatView = imageXView.Image.ImageXData.Format;
                        sizeView = imageXView.Image.ImageXData.Size;

                        if (bppView == 32)
                        {
                            alphaToolStripMenuItem1.Enabled = true;
                        }
                        else
                        {
                            alphaToolStripMenuItem1.Enabled = false;
                        }

                        //want to display DPI
                        imageXView.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch;

                        resolutionView = imageXView.Image.ImageXData.Resolution.Dimensions;

                        if (PDFOpened == false)
                        {
                            //update annotations layer because of paging
                            UpdateNotate(page);
                        }
                        else
                        {
                            UpdateNotate(page + 1);
                        }

                        //in case layers are reset by mistake
                        if (notateXpress1.Layers.Count == 0)
                        {
                            ResetNotate(pageCount);
                        }

                        //change zoom depending on image size
                        ZoomImage();

                        PagingUpdateUI(page);
                    }

                    //turn ImageXView painting back on now that image update is complete
                    imageXView.AllowUpdate = true;
                }
                catch (Exception ex)
                {
                    ErrorHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ImageUpdate()
        {
            CadLayer cadLayer = null;

            try
            {
                imageRefresh = true;

                metaDataButton.Enabled = true;
              
                //clear thumbnails
                thumbnailXpressPaging.Items.Clear();

                //if the file chosen is a pdf, then use PDFXpress to render it as a bitmap
                //this is because ImagXpress can only open a small subset of PDF's, image-only PDF's
                //while PDFXpress is designed to open all PDF's

                //test if file chosen has .PDF extension or not
                PDFOpened = filenameOpened.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase)
                    || filenameOpened.EndsWith(".tpdf", StringComparison.OrdinalIgnoreCase);
                cadOpened = filenameOpened.EndsWith(".dwg", StringComparison.OrdinalIgnoreCase)
                    || filenameOpened.EndsWith(".dxf", StringComparison.OrdinalIgnoreCase)
                    || filenameOpened.EndsWith(".dwf", StringComparison.OrdinalIgnoreCase);
                metaOpened = filenameOpened.EndsWith(".wmf", StringComparison.OrdinalIgnoreCase)
                    || filenameOpened.EndsWith(".emf", StringComparison.OrdinalIgnoreCase);

                //before image is opened, reset current Page Number, Page Count and PageBox UI
                PageBox.Text = "1";
                pageNumber = 1;
                pageCount = 1;

                //clear any ImageX's stored in memory previously because now opening new image
                ImageMemoryReset();

                ResetCadTab();

                if (pdfXpress1.Documents != null)
                {
                    for (int i = 0; i < pdfXpress1.Documents.Count; i++)
                    {
                        if (pdfXpress1.Documents[i] != null)
                            pdfXpress1.Documents[i].Dispose();
                    }

                    pdfXpress1.Documents.Clear();
                }

                if (PDFOpened == true)
                {
                    //want to make sure OpenFile() isn't used to open PDF's because it doesn't use
                    //PDFXpress2
                    initialImageLoad = false;

                    //if pdf is chosen add it to DocumentCollection
                    pdfXpress1.Documents.Clear();

                    try
                    {
                        pdfXpress1.Documents.Add(filenameOpened);
                    }
                    catch (PegasusImaging.WinForms.PdfXpress2.InvalidPasswordException ex)
                    {
                        //39 corresponds to an invalid password
                        if (ex.Number == 39)
                        {
                            using (PasswordForm passForm = new PasswordForm())
                            {
                                helper.CenterScreen(passForm);

                                passForm.SecureFileType = "PDF";

                                if (passForm.ShowDialog() == DialogResult.OK)
                                {
                                    try
                                    {
                                        pdfXpress1.Documents.Add(filenameOpened, passForm.Password);
                                    }
                                    catch (PegasusImaging.WinForms.PdfXpress2.InvalidPasswordException ex2)
                                    {
                                        //39 corresponds to an invalid password
                                        if (ex2.Number == 39)
                                        {
                                            MessageBox.Show("The password you entered is not correct.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                                            ImageUpdate();
                                        }
                                        else
                                        {
                                            //otherwise throw the exception
                                            throw ex2;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //otherwise throw the exception
                            throw ex;
                        }
                    }

                    if (pdfXpress1.Documents.Count > 0)
                    {
                        pageCount = pdfXpress1.Documents[0].PageCount;
                        PDFPageNumber = 0;

                        PagingUpdate();

                        Paging(PDFPageNumber);
                    }
                }
                else
                {
                    if (loadOptions == null)
                    {
                        //no load options are set so no need to update thumbnail via memory
                        initialImageLoad = false;

                        loadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
                    }

                    //set the current page number to 1 because file is being initially opened, set page count
                    pageNumber = 1;

                    if (cadOpened == true)
                    {
                        cadInitial = true;

                        //add CAD tab
                        if (TabRoll.TabPages.Count == 4)
                            TabRoll.TabPages.Insert(4, LayersCadTab);

                        //resource cleanup
                        if (dataForCad != null)
                        {
                            dataForCad.Dispose();
                            dataForCad = null;
                        }

                        //retrieve ImageXData
                        dataForCad = ImageX.QueryFile(imagXpress1, filenameOpened, 1, loadOptions);

                        //set CadData for loadOptions
                        loadOptions.Cad.CadData = dataForCad;

                        int layoutCount, layerCount;
                        CadFileType cadType;
                        CadVersion cadVersion;

                        //retrieve Cad information
                        dataForCad.QueryCadFormat(out cadType, out cadVersion, out layerCount, out layoutCount);

                        string[] cadLayerNames = new string[layerCount];
                        bool[] cadLayerChecked = new bool[layerCount];
                        int[] cadIndices = new int[layerCount];

                        //get an array of CAD layer names, layerOn states, and indices
                        for (int i = 0; i < layerCount; i++)
                        {
                            //resource cleanup
                            if (cadLayer != null)
                            {
                                cadLayer.Dispose();
                                cadLayer = null;
                            }

                            cadLayer = dataForCad.Layer(i);

                            cadLayerNames[i] = cadLayer.LayerName;
                            cadLayerChecked[i] = cadLayer.LayerOn;
                            cadIndices[i] = i;
                        }

                        //upadte the CAD tab with the layer information
                        CadLayersUIUpdate(cadLayerNames, cadLayerChecked, cadIndices);

                        //set layout count, analogous to page count
                        pageCount = layoutCount;

                        //set the cadOpened flag to true, because a CAD image was opened
                        cadOpened = true;

                        loadOptions.Cad.LayoutToRender = 0;
                    }
                    else
                    {
                        //if file loaded is CameraRaw, need to set CamerawRawEnabled to true in the LoadOptions
                        PegasusImaging.WinForms.ImagXpress9.LoadOptions queryLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
                        queryLoadOptions.CameraRawEnabled = true;

                        using (ImageXData queryImg = ImageX.QueryFile(imagXpress1, filenameOpened, 1, queryLoadOptions))
                        {
                            if (queryImg.Format == ImageXFormat.CameraRaw)
                            {
                                loadOptions.CameraRawEnabled = true;
                                thumbnailXpressDrillDown.CameraRaw = true;
                                cameraRawCheckBox.Checked = true;
                            }
                            else
                            {
                                loadOptions.CameraRawEnabled = false;
                                thumbnailXpressDrillDown.CameraRaw = false;
                                cameraRawCheckBox.Checked = false;
                            }
                        }

                        //want to retieve pageCount of non-pdf, non-cad images without having to open file
                        pageCount = ImageX.NumPages(imagXpress1, filenameOpened);
                    }

                    PagingUpdate();

                    Paging(pageNumber);
                }

                grayComboBox.SelectedIndex = 0;

                if (imageXView.Image != null)
                {
                    if (notateXpress1.Layers.Count > 0)
                        notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;

                    ResetToolbars(false);

                    UpdateAnnotationSize();
                }

                ImageMemoryReset();

                imageRefresh = false;
            }
            finally
            {
                if (cadLayer != null)
                {
                    cadLayer.Dispose();
                    cadLayer = null;
                }
            }
        }

        private void ImageLoad()
        {
            try
            {
                initialImageLoad = true;

                filenameOpened = Path.GetFullPath(filenameOpened);

                //a new image was opened, so reset the editFlag
                editFlag = false;

                ImageUpdate();

                if (imageXView.Image != null)
                {
                    ToolbarMenu(true);

                    ResetImageSelection();

                    //if an image is opened via the OpenFileDialog, want to reset ThumbnailXpress DrillDown control
                    //to be Recent Images
                    if (thumbnailTextBox.Text != "Recent Images")
                    {
                        RetrieveRecentItems();
                    }

                    //check to make sure file opened doesn't already have thumbnail, if it does then don't add a new one,
                    //otherwise add a new thumbnail to the end and select it
                    if (CheckForRepeatsInThumbnailDrillDown(filenameOpened, 0) == false)
                    {
                        if (initialImageLoad == true)
                        {
                            ThumbnailMemoryUpdate(-1, thumbnailXpressDrillDown, filenameOpened, loadOptions);
                        }
                        else
                        {
                            thumbnailXpressDrillDown.Items.AddItemsFromFile(filenameOpened, -1, false);
                        }

                        thumbnailXpressDrillDown.SelectedItems.Clear();
                        thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[thumbnailXpressDrillDown.Items.Count - 1]);

                        //scroll thumbnail selected into view
                        thumbnailXpressDrillDown.EnsureVisible(thumbnailXpressDrillDown.Items.Count - 1);
                    }

                    initialImageLoad = false;
                }
            }
            catch (ImageXException ex)
            {
                ErrorHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ThumbnailMemoryUpdate(int index, ThumbnailXpress thumbControl, string filename, 
            PegasusImaging.WinForms.ImagXpress9.LoadOptions localLoadOptions)
        {
            ImageX img = null;

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    //want to save current image out with compression to save thumbnailing time
                    PegasusImaging.WinForms.ImagXpress9.SaveOptions saveOptionsThumbnail = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();

                    if (bppView == 1)
                    {
                        saveOptionsThumbnail.Format = ImageXFormat.Png;
                    }
                    else
                    {
                        saveOptionsThumbnail.Format = ImageXFormat.Jpeg;
                        saveOptionsThumbnail.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress9.SubSampling.SubSampling411;
                        saveOptionsThumbnail.Jpeg.Chrominance = 20;
                        saveOptionsThumbnail.Jpeg.Luminance = 20;
                    }

                    //resource cleanup
                    if (processor1 != null)
                    {
                        if (processor1.Image != null)
                        {
                            processor1.Image.Dispose();
                            processor1.Image = null;
                        }
                    }

                    if (thumbControl == thumbnailXpressDrillDown)
                    {
                        if (filename.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase) == true 
                            || filename.EndsWith(".tpdf", StringComparison.OrdinalIgnoreCase) == true)
                        {
                            pdfXpress1.Documents.Add(filename, "");

                            using (Bitmap bp = pdfXpress1.Documents[1].RenderPageToBitmap(0, ro))
                            {
                                img = ImageX.FromBitmap(imagXpress1, bp);
                                img.ImageXData.Resolution.Units = GraphicsUnit.Inch;
                                img.ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);
                            }
                        }
                        else
                        {
                            img = ImageX.FromFile(imagXpress1, filename, localLoadOptions);
                        }
                        
                        processor1.Image = img.Copy();

                        if (img.ImageXData.BitsPerPixel >= 9 && img.ImageXData.BitsPerPixel <= 16)
                        {
                            Medical medical = new Medical();
                            
                            processor1.ApplyGrayscaleTransform(medical);
                        }
                    }
                    else
                    {
                        processor1.Image = imageXView.Image.Copy();
                    }

                    //resize image to be smaller to update thumbnail faster
                    if (processor1.Image.ImageXData.Width > 2000 || processor1.Image.ImageXData.Height > 2000)
                    {
                        processor1.Resize(new Size(processor1.Image.ImageXData.Width / 5, processor1.Image.ImageXData.Height / 5),
                                            ResizeType.Fast);
                    }
                    else
                    {
                        processor1.Resize(new Size(processor1.Image.ImageXData.Width / 2, processor1.Image.ImageXData.Height / 2),
                                            ResizeType.Fast);
                    }
                    
                    
                    //save current Image to a MemoryStream with the above SaveOptions                        
                    processor1.Image.SaveStream(ms, saveOptionsThumbnail);

                    ms.Position = 0;

                    //create a new ThumbnailItem from the stream
                    ThumbnailItem itemFromStream = thumbnailXpressPaging.CreateThumbnailItem("", 1, ThumbnailType.Image);
                    itemFromStream.UserTag = "";
                    itemFromStream.FromStream(ms);

                    //set the Descriptor, Page, & Filename of the thumbnail
                    itemFromStream.Filename = filename;

                    if (thumbControl == thumbnailXpressPaging)
                    {
                        itemFromStream.Descriptor = Path.GetFileName(filename) + ", Page" + thumbControl.Items[index - 1].Page.ToString();
                        itemFromStream.Page = thumbControl.Items[index - 1].Page;

                        //remove the old thumbnail and insert the new one in its place and select it
                        thumbControl.Items.RemoveAt(index - 1);
                        thumbControl.Items.InsertItemFromStream(index - 1, itemFromStream);
                        thumbControl.SelectedItems.Clear();
                        thumbControl.SelectedItems.Add(thumbControl.Items[index - 1]);
                    }
                    else
                    {
                        itemFromStream.Descriptor = Path.GetFileName(filename);

                        if (index != -1)
                        {
                            if (thumbControl.Items.Count > index)
                                thumbControl.Items.RemoveAt(index);

                            thumbControl.Items.InsertItemFromStream(index, itemFromStream);
                        }
                        else
                        {
                            thumbControl.Items.AddItemFromStream(itemFromStream);
                        }
                    }
                }
            }
            finally
            {
                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }               
                if (img != null)
                {
                    img.Dispose();
                    img = null;
                }
            }
        }

        private void OpenFileDialog(bool useOptions)
        {            
            //check if image has been edited in anyway whatsoever before continuing
            if (CheckForImageChanges() == false)
            {
                if (SampleImageLoad == true)
                {
                    ImageLoad();
                }
                else
                {
                    using (OpenFileDialog dlg = new OpenFileDialog())
                    {
                        //reset filter index to last chosen filter index for open file dialog
                        if (filenameOpened != null)
                            dlg.FileName = filenameOpened;

                        dlg.FilterIndex = openFilterIndex;
                        dlg.Title = "Open an Image File";

                        if (directoryOpened == null || Directory.Exists(directoryOpened) == false)
                        {
                            if (Directory.Exists(commonImagePath) == true)
                            {
                                dlg.InitialDirectory = commonImagePath;
                            }
                            else
                            {
                                dlg.InitialDirectory = Application.StartupPath;
                            }
                        }
                        else
                        {
                            dlg.InitialDirectory = directoryOpened;
                        }

                        dlg.Filter = helper.OpenImageFilter;

                        if (dlg.ShowDialog() == DialogResult.OK)
                        {
                            //set global load options based on LoadOptions set in OpenFileDialog
                            if (useOptions == true)
                            {
                                using (LoadOptionsForm loadForm = new LoadOptionsForm())
                                {
                                    helper.CenterScreen(loadForm);

                                    using (ImageXData data = ImageX.QueryFile(imagXpress1, dlg.FileName))
                                    {
                                        loadForm.CropXMax = (decimal)data.Width;
                                        loadForm.CropYMax = (decimal)data.Height;
                                    }

                                    helper.IdentifyLoadOptionUI(dlg.FilterIndex, loadForm);

                                    if (loadForm.ShowDialog() == DialogResult.OK)
                                    {
                                        loadOptions = loadForm.ImageLoadOptions;
                                    }
                                }
                            }
                            else
                            {
                                loadOptions = null;
                            }

                            //remember the last filter index and file chosen by the user
                            openFilterIndex = dlg.FilterIndex;
                            filenameOpened = dlg.FileName;

                            directoryOpened = dlg.InitialDirectory;

                            ImageLoad();
                        }
                    }
                }
            }
        }

        #endregion

        #region Annotation Default Settings

        //Annotation toolbar uses the Annotation tools of the NotateXpress toolbar
        //here we're setting the default attributes of these tools to better show off the tools' versitility
        private void AnnotationDefaults()
        {
            try
            {
                //button tool default settings
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.AutoSize = PegasusImaging.WinForms.NotateXpress9.AutoSize.ToContents;
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.Text = "Button";
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.TextColor = Color.Black;
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.TextFont = new Font(FontFamily.GenericMonospace, 24, FontStyle.Bold);
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.ToolTipText = "Button Annotation";

                //ruler tool default settings
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.BackStyle = BackStyle.Opaque;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.MeasurementUnit = MeasurementUnit.Inches;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.PenColor = Color.FromArgb(96, 48, 0);
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.PenFillStyle = PenFillStyle.Opaque;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.PenStyle = PenStyle.Solid;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.PenWidth = 9;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.ShowAbbreviation = false;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.ShowGauge = true;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.TextColor = Color.Black;
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.ToolTipText = "Ruler Annotation";

                //image tool default settings
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.BackStyle = BackStyle.Opaque;
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.PenColor = Color.Black;
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.ToolTipText = "Image Annotation";

                if (Directory.Exists(PathImageToolImage) == true)
                {
                    imageToolImage = ImageX.FromFile(imagXpress1, PathImageToolImage);
                    IntPtr dibHandle = imageToolImage.ToHdib(true);

                    notateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle;

                    //resource cleanup
                    if (imageToolImage != null)
                    {
                        imageToolImage.Dispose();
                        imageToolImage = null;
                    }

                    //resource cleanup
                    Marshal.FreeHGlobal(dibHandle);
                }
                
                //stamp tool default settings
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.AutoSize = PegasusImaging.WinForms.NotateXpress9.AutoSize.ToContents;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.BackColor = Color.Beige;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.BackStyle = BackStyle.Opaque;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.PenColor = Color.Black;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.PenWidth = 5;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "Stamp by NotateXpress 9.0";
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextColor = Color.Maroon;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = new Font(FontFamily.GenericMonospace, 24, FontStyle.Bold);
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextOrientation = TextOrientation.Degree0;
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.ToolTipText = "Stamp Annotation";

                //rectangle tool default settings
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.BackStyle = BackStyle.Translucent;
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.FillColor = Color.FromArgb(212, 223, 255);
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.PenColor = Color.FromArgb(85, 127, 170);
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.RectangleToolbarDefaults.ToolTipText = "Rectangle Annotation";

                //note tool default settings
                notateXpress1.ToolbarDefaults.NoteToolbarDefaults.BackColor = Color.Yellow;
                notateXpress1.ToolbarDefaults.NoteToolbarDefaults.TextColor = Color.Black;
                notateXpress1.ToolbarDefaults.NoteToolbarDefaults.TextFont = new Font(FontFamily.GenericMonospace, 23, FontStyle.Bold);
                notateXpress1.ToolbarDefaults.NoteToolbarDefaults.TextJustification = TextJustification.Left;
                notateXpress1.ToolbarDefaults.NoteToolbarDefaults.ToolTipText = "Note Annotation";

                //ellipse tool default settings
                notateXpress1.ToolbarDefaults.EllipseToolbarDefaults.BackStyle = BackStyle.Translucent;
                notateXpress1.ToolbarDefaults.EllipseToolbarDefaults.FillColor = Color.FromArgb(212, 223, 255);
                notateXpress1.ToolbarDefaults.EllipseToolbarDefaults.PenColor = Color.FromArgb(85, 127, 170);
                notateXpress1.ToolbarDefaults.EllipseToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.EllipseToolbarDefaults.ToolTipText = "Ellipse Annotation";

                //text tool default settings
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.BackStyle = BackStyle.Opaque;
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.PenColor = Color.Black;
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.BackColor = Color.Cornsilk;
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextColor = Color.Black;
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = new Font(FontFamily.GenericMonospace, 23, FontStyle.Bold);
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextJustification = TextJustification.Left;
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.ToolTipText = "Text Annotation";
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.AutoSize = PegasusImaging.WinForms.NotateXpress9.AutoSize.ToContents;

                //line tool default settings
                notateXpress1.ToolbarDefaults.LineToolbarDefaults.PenColor = Color.DarkBlue;
                notateXpress1.ToolbarDefaults.LineToolbarDefaults.PenFillStyle = PenFillStyle.Opaque;
                notateXpress1.ToolbarDefaults.LineToolbarDefaults.PenStyle = PenStyle.Solid;
                notateXpress1.ToolbarDefaults.LineToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.LineToolbarDefaults.ToolTipText = "Line Annotation";

                //polyline tool default settings
                notateXpress1.ToolbarDefaults.PolyLineToolbarDefaults.PenColor = Color.Blue;
                notateXpress1.ToolbarDefaults.PolyLineToolbarDefaults.PenFillStyle = PenFillStyle.Opaque;
                notateXpress1.ToolbarDefaults.PolyLineToolbarDefaults.PenStyle = PenStyle.Solid;
                notateXpress1.ToolbarDefaults.PolyLineToolbarDefaults.PenWidth = 2;
                notateXpress1.ToolbarDefaults.PolyLineToolbarDefaults.ToolTipText = "Polyline Annotation";

                //polygon tool default settings
                notateXpress1.ToolbarDefaults.PolygonToolbarDefaults.BackStyle = BackStyle.Translucent;
                notateXpress1.ToolbarDefaults.PolygonToolbarDefaults.FillColor = Color.MintCream;
                notateXpress1.ToolbarDefaults.PolygonToolbarDefaults.PenColor = Color.Navy;
                notateXpress1.ToolbarDefaults.PolygonToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.PolygonToolbarDefaults.ToolTipText = "Polygon Annotation";

                //freehand tool default settings
                notateXpress1.ToolbarDefaults.FreehandToolbarDefaults.PenColor = Color.DarkSeaGreen;
                notateXpress1.ToolbarDefaults.FreehandToolbarDefaults.PenFillStyle = PenFillStyle.Opaque;
                notateXpress1.ToolbarDefaults.FreehandToolbarDefaults.PenStyle = PenStyle.Solid;
                notateXpress1.ToolbarDefaults.FreehandToolbarDefaults.PenWidth = 7;
                notateXpress1.ToolbarDefaults.FreehandToolbarDefaults.ToolTipText = "Freehand Annotation";

                //highlight tool default settings
                notateXpress1.ToolbarDefaults.BlockHighlightToolbarDefaults.FillColor = Color.Yellow;
                notateXpress1.ToolbarDefaults.BlockHighlightToolbarDefaults.ToolTipText = "Highlight Annotation";
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Methods to Update UI

        //if image is smaller then screen, then start at 1:1 ratio, otherwise start at BestFit
        private void ZoomImage()
        {
            try
            {
                imageXView.AllowUpdate = false;

                if ((imageXView.Height > heightView) && (imageXView.Width > widthView))
                {
                    //must change AutoResize property to CropImage which will allow ZoomFactor setting to have an
                    //effect
                    imageXView.AutoResize = AutoResizeType.CropImage;
                    imageXView.ZoomFactor = 1.0;
                }
                else
                {
                    imageXView.ZoomToFit(ZoomToFitType.FitBest);
                }

                imageXView.AllowUpdate = true;
            }
            catch(Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }


        private void ResetToolbars(bool annotate)
        {
            if (annotate == true)
            {
                //reset Viewing Toolbar to ImagXpress Pointer tool and set the tool to the pointer
                ToggleToolbar(PointerIXButton.Name, ViewToolbar);

                imageXView.ToolSet(Tool.None, MouseButtons.Left, Keys.None);
            }
            else
            {
                //uncheck the other Annotate toolbar buttons except for the Pointer
                ToggleToolbar(PointerNXButton.Name, AnnotateToolbar);

                if (notateXpress1.Layers.Count > 0)
                    notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;
            }
        }

        private void ViewToolbarUpdate(string toolName, Tool tool)
        {
            try
            {
                //force only the NotateXpress Pointer Tool to be chosen
                ResetToolbars(false);

                //toggle the View Toolbar button selected
                ToggleToolbar(toolName, ViewToolbar);

                //Select the ImagXpress toolbar's tool chosen
                imageXView.ToolSet(tool, MouseButtons.Left, Keys.None);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ZoomToolUpdate(ZoomToFitType zoom)
        {
            try
            {
                //imageXView object's AutoResize is set to BestFit at startup, we must
                //set to a different enumeration that allows ZoomToFit() to have an effect
                imageXView.AutoResize = AutoResizeType.CropImage;

                //Zoom to BestFit
                imageXView.ZoomToFit(zoom);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void AnnotateToolbarUpdate(string annotateName, AnnotationTool tool)
        {
            try
            {
                ToggleToolbar(annotateName, AnnotateToolbar);

                //set the current layer's AnnotationTool
                notateXpress1.Layers[0].Toolbar.Selected = tool;

                currentTool = tool;

                ResetToolbars(true);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        //call this method when an image is loaded or not to turn on or off the toolbar and menubars
        private void ToolbarMenu(bool onOff)
        {
            for (int i = 0; i < toolbarButtons.Length; i++)
            {
                toolbarButtons[i].Enabled = onOff;
            }

            for (int i = 0; i < menuItems.Length; i++)
            {
                menuItems[i].Enabled = onOff;
            }
        }

        private void ResetImageSelection()
        {
            startPointSelection = new Point(0, 0);
            endPointSelection = new Point(0, 0);
            //identify to user whole image will be copied or cut
            CopyButton.ToolTipText = "Copy Image";
            CutButton.ToolTipText = "Cut Image";
        }

        private void DataGridViewPopulate()
        {
            //populate DataGridView programatically

            string[] rows = new string[] { "File Name", "Width (pixels)", "Height (pixels)", "Bits Per Pixel", "Resolution (pixels per unit)",
                                           "Resolution Units","Format","Compression","Compressed File Size (bytes)",
                                           "Uncompressed File Size (bytes)", "Compression Ratio","Unique Color Count"};

            for (int i = 0; i < rows.Length; i++)
            {
                dataGridImageInfo.Rows.Add(rows[i]);
            }
        }

        private void metaDataButton_Click(object sender, EventArgs e)
        {
            using (MetaDataForm metaForm = new MetaDataForm())
            {
                helper.CenterScreen(metaForm);

                metaForm.PageCount = pageCount;
                metaForm.FileName = filenameOpened;
                metaForm.commentText = GetJPEGComments();

                metaForm.ShowDialog();
            }
        }

        private void ImageInfoTabsUpdate()
        {
            IntPtr dib = new IntPtr(0);

            try
            {
                if (imageXView.Image != null)
                {
                    switch (TabRoll.SelectedIndex)
                    {
                        case 0:
                            {
                                dataGridImageInfo.Rows[0].Cells[1].Value = filenameOpened;
                                dataGridImageInfo.Rows[1].Cells[1].Value = widthView;
                                dataGridImageInfo.Rows[2].Cells[1].Value = heightView;
                                dataGridImageInfo.Rows[3].Cells[1].Value = bppView;

                                if (cadOpened == true)
                                {
                                    SizeF cadSize = new SizeF((float)loadOptions.Cad.PaperResolutionX, (float)loadOptions.Cad.PaperResolutionY);
                                    dataGridImageInfo.Rows[4].Cells[1].Value = cadSize;
                                }
                                else if (PDFOpened == true)
                                {
                                    SizeF pdfSize = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);
                                    dataGridImageInfo.Rows[4].Cells[1].Value = pdfSize;
                                }
                                else
                                {
                                    dataGridImageInfo.Rows[4].Cells[1].Value = resolutionView;
                                }

                                dataGridImageInfo.Rows[5].Cells[1].Value = "Inch";

                                //if PDF is opened can't rely on data.Format property
                                if (PDFOpened == true)
                                {
                                    dataGridImageInfo.Rows[6].Cells[1].Value = "PDF";
                                }
                                else
                                {
                                    dataGridImageInfo.Rows[6].Cells[1].Value = formatView;
                                }

                                dataGridImageInfo.Rows[7].Cells[1].Value = compressionView;
                                dataGridImageInfo.Rows[8].Cells[1].Value = sizeView;

                                try
                                {
                                    //resource cleanup
                                    if (processor1 != null)
                                    {
                                        if (processor1.Image != null)
                                        {
                                            processor1.Image.Dispose();
                                            processor1.Image = null;
                                        }
                                    }

                                    processor1.Image = imageXView.Image.Copy();
                                   
                                    if (bppView >= 9 && bppView <= 16)
                                    {
                                        Medical medical = new Medical();

                                        processor1.ApplyGrayscaleTransform(medical);

                                        dib = processor1.Image.ToHdib(false);
                                    }
                                    else
                                    {
                                        dib = imageXView.Image.ToHdib(false);
                                    }
                                    
                                    //use WinApi to get uncompressed file size
                                    UInt32 uncompressedSize = GlobalSize(dib).ToUInt32();

                                    dataGridImageInfo.Rows[9].Cells[1].Value = uncompressedSize;

                                    //compression ratio = uncompressed file size divided by the compressed file size
                                    float ratio = (float)uncompressedSize / sizeView;

                                    dataGridImageInfo.Rows[10].Cells[1].Value = ratio.ToString("F2") + " : 1";

                                    try
                                    {
                                        //retrieve unique color count
                                        dataGridImageInfo.Rows[11].Cells[1].Value = processor1.UniqueColors();
                                    }
                                    catch (ProcessorException)
                                    {
                                        dataGridImageInfo.Rows[11].Cells[1].Value = "Unknown";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ErrorHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
                                }

                                dataGridImageInfo.AutoResizeColumn(1);

                                break;
                            }
                        case 1:
                            {
                                //ImageXView needs to be updated first to get the correct aspect ratio
                                imageXView.Update();

                                aspectXNumericUpDown.Value = imageXView.AspectX;
                                aspectYNumericUpDown.Value = imageXView.AspectY;

                                break;
                            }
                        case 3:
                            {
                                //update histogram info, reset Color channel selection
                                ColorTabUpdate();
                                break;
                            }
                    }
                }

                //to prevent problem of datgrid row getting stuck
                dataGridImageInfo.Refresh();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {               
                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }

                //resource cleanup
                Marshal.FreeHGlobal(dib);
            }
        }

        private void ColorTabUpdate()
        {
            if (bppView <= 8)
                paletteButton.Enabled = true;
            else
                paletteButton.Enabled = false;

            //resource cleanup
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }
            }
            
            processor1.Image = imageXView.Image.Copy();

            redData = new int[256];
            greenData = new int[256];
            blueData = new int[256];

            //only draw RGB histogram for 24 or 32-bit images
            if (bppView == 24 || bppView == 32)
            {
                //enable combobox
                comboBoxHistogram.Enabled = true;

                //retrieve RGB intensity arrays
                processor1.RGBColorCount(out redData, out greenData, out blueData);
                
                //reset combobox
                comboBoxHistogram.SelectedIndex = 0;

                //draw red histogram
                DrawHistogram(redData, null, null, redPen, null, null);
            }
            else
            {
                //resource cleanup
                if (imageXViewHistogram != null)
                {
                    if (imageXViewHistogram.Image != null)
                    {
                        imageXViewHistogram.Image.Dispose();
                        imageXViewHistogram.Image = null;
                    }
                }

                //disable combobox
                comboBoxHistogram.Enabled = false;
            }

            //resource cleanup
            if (redImage != null)
            {
                redImage.Dispose();
                redImage = null;
            }

            if (greenImage != null)
            {
                greenImage.Dispose();
                greenImage = null;
            }

            if (blueImage != null)
            {
                blueImage.Dispose();
                blueImage = null;
            }

            if (alphaImage != null)
            {
                alphaImage.Dispose();
                alphaImage = null;
            }

            if (hueImage != null)
            {
                hueImage.Dispose();
                hueImage = null;
            }

            if (saturationImage != null)
            {
                saturationImage.Dispose();
                saturationImage = null;
            }

            if (luminanceImage != null)
            {
                luminanceImage.Dispose();
                luminanceImage = null;
            }

            if (cyanImage != null)
            {
                cyanImage.Dispose();
                cyanImage = null;
            }

            if (magentaImage != null)
            {
                magentaImage.Dispose();
                magentaImage = null;
            }

            if (yellowImage != null)
            {
                yellowImage.Dispose();
                yellowImage = null;
            }

            if (blackImage != null)
            {
                blackImage.Dispose();
                blackImage = null;
            }

            ClearColorRadioButtons();

            //disable alpha channel if image doesn't have alpha channel
            if (bppView != 32)
            {
                alphaRadioButton.Enabled = false;
            }
            else
            {
                alphaRadioButton.Enabled = true;
            }

            //for non-24bpp images RGBA, CMY, HS really won't show anything
            if (bppView == 24 || bppView == 32)
            {
                redRadioButton.Enabled = true;
                greenRadioButton.Enabled = true;
                blueRadioButton.Enabled = true;
                cyanRadioButton.Enabled = true;
                magentaRadioButton.Enabled = true;
                yellowRadioButton.Enabled = true;
                blackRadioButton.Enabled = true;
                hueRadioButton.Enabled = true;
                saturationRadioButton.Enabled = true;
                luminanceRadioButton.Enabled = true;                
            }
            else
            {
                redRadioButton.Enabled = false;
                greenRadioButton.Enabled = false;
                blueRadioButton.Enabled = false;
                cyanRadioButton.Enabled = false;
                magentaRadioButton.Enabled = false;
                yellowRadioButton.Enabled = false;
                hueRadioButton.Enabled = false;
                saturationRadioButton.Enabled = false;
                blackRadioButton.Enabled = true;
                luminanceRadioButton.Enabled = true;
            }

            resetComponentsButton.Enabled = true;
        }

        private void ImageInfoClear()
        {
            //clear image info items from DataGrid
            for (int i = 0; i < dataGridImageInfo.Rows.Count; i++)
                dataGridImageInfo.Rows[i].Cells[1].Value = "";

            dataGridImageInfo.AutoResizeColumn(1);
        }

        private void SinglePageImage()
        {
            //single page image so reset paging UI
            PageBox.Text = "1";
            PageBox.Enabled = false;
            NextButton.Enabled = false;
            PreviousButton.Enabled = false;
            PageCountLabel.Text = "of 1";

            //hide thumbnailXpress paging control and splitter for single-page images
            PanelPaging.Width = 0;
            SplitterView.Visible = false;

            //clear thumbnails for paging
            thumbnailXpressPaging.Items.Clear();
        }

        //method to toggletoolbar buttons on and off so only one can be selected at a time   
        //this treats the buttons of the toolbar as if they were in a radio group
        private void ToggleToolbar(string buttonChosen, ToolStripButton[] toolStrip)
        {
            for (int i = 0; i < toolStrip.Length; i++)
            {
                if (buttonChosen == toolStrip[i].Name)
                {
                    toolStrip[i].Checked = true;
                }
                else
                {
                    toolStrip[i].Checked = false;
                }
            }
        }

        private void RefreshImage()
        {
            //check for recent image changes before proceeding
            if (CheckForImageChanges() == false)
            {
                //if refresh button is pressed, act as if image was being opened through
                //the open file dialog, reset image, paging, thumbnails, annotations
                ImageUpdate();

                ResetImageSelection();

                //reset edit flag
                editFlag = false;
            }
        }

        #endregion

        #region ImageXView Event Handlers

        private void ContextMenuClickEventHandler(object sender, EventArgs e)
        {
            try
            {
                antiCheckBox.Checked = imageXView.Antialias;
                ditherCheckBox.Checked = imageXView.Dithered;
                smoothCheckBox.Checked = imageXView.Smoothing;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void imageXView_ZoomFactorChanged(object sender, ZoomFactorChangedEventArgs e)
        {
            try
            {
                //whenever the ZoomFactor changes, update the NumericUpDown corresponding to it on the View tab
                zoomNumericUpDown.Value = (decimal)imageXView.ZoomFactor;
            }
            catch (Exception ex)
            {
                //if exception occurs don't want to pop up message box because it will fire every time mouse is moved
                //and user will be forced to shut down
                MousePositionLabel.Text = ex.Message;
            }
        }

        private void imageXView_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                //as long as image is loaded, update statusbar with location of mouse cursor, translate from view
                //to pixel coordinates
                if (imageXView.Image != null)
                {
                    mouseUpdatePoint = imageXView.Translate(e.Location, TranslateType.ViewToPixel);

                    //if outside bounds of visible image, only want to report positive coordinates
                    if (mouseUpdatePoint.X > -1 && mouseUpdatePoint.Y > -1)
                    {
                        string color = "Color: {R: " + imageXView.PixelColor.R.ToString() + " G: " + imageXView.PixelColor.G.ToString() 
                                       + " B: " + imageXView.PixelColor.B.ToString() + "}";

                        MousePositionLabel.Text = mouseUpdatePoint.ToString() + " " + color;
                    }
                }
            }
            catch (Exception ex)
            {
                //if exception occurs don't want to pop up message box because it will fire every time mouse is moved
                //and user will be forced to shut down
                MousePositionLabel.Text = ex.Message;
            }
        }

        private void imageXView_MouseLeave(object sender, EventArgs e)
        {
            //no longer hovering over ImageXView, so don't output location anymore
            MousePositionLabel.Text = "";
        }

        private void imageXView_MouseDown(object sender, MouseEventArgs e)
        {
            //reset selection rectangle points, because Select Tool is no longer being used
            //when this event handler fires
            ResetImageSelection();
        }

        private void imageXView_ToolEvent(object sender, ToolEventArgs e)
        {
            if (e.Tool == Tool.Select)
            {
                if (e.Action == ToolAction.Begin)
                {
                    startPointSelection = e.Location;
                }
                if (e.Action == ToolAction.End)
                {
                    //if select tool begins and ends at the same point, then no selection was drawn and we should reset the start
                    //point and end point of the user's selection
                    if (startPointSelection == e.Location)
                    {
                        ResetImageSelection();
                    }
                    else
                    {
                        endPointSelection = e.Location;
                        //identify that copy/cut will not affect whole image, but only selection
                        CopyButton.ToolTipText = "Copy Selection";
                        CutButton.ToolTipText = "Cut Selection";
                    }
                }
            }
        }

        private void imageXView_DragEnter(object sender, DragEventArgs e)
        {
            try
            {
                SampleImageLoad = true;

                //get filename of file dragged into control
                string[] fileDropped = (string[])e.Data.GetData(DataFormats.FileDrop);

                filenameOpened = fileDropped[0];

                OpenFileDialog(false);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region File/Edit Toolbar Event Handlers


        private void dropDownAlpha1_Click(object sender, EventArgs e)
        {
            alpha1ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownAlpha2_Click(object sender, EventArgs e)
        {
            alpha2ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownAlpha3_Click(object sender, EventArgs e)
        {
            alpha3ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownColorBalance1_Click(object sender, EventArgs e)
        {
            autoBalance1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownColorBalance2_Click(object sender, EventArgs e)
        {
            autoBalance2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownColorBalance3_Click(object sender, EventArgs e)
        {
            autoLevel2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownColorBalance4_Click(object sender, EventArgs e)
        {
            autoLevel3ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownContrast1_Click(object sender, EventArgs e)
        {
            autoContrastToolStripMenuItem_Click_1(sender, e);
        }

        private void dropDownContrast2_Click(object sender, EventArgs e)
        {
            autoLightness2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownCrop_Click(object sender, EventArgs e)
        {
            cropToolStripMenuItem_Click_1(sender, e);
        }

        private void dropDownExif_Click(object sender, EventArgs e)
        {
            eXIFToolStripMenuItem_Click(sender, e);
        }

        private void dropDownOrientation1_Click(object sender, EventArgs e)
        {
            rotate1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownOrientation2_Click(object sender, EventArgs e)
        {
            rotate2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownRedEye1_Click(object sender, EventArgs e)
        {
            redEye1ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownRedEye2_Click(object sender, EventArgs e)
        {
            redEye2ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownRedEye3_Click(object sender, EventArgs e)
        {
            redEye3ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownRedEye4_Click(object sender, EventArgs e)
        {
            redEye4ToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownDust1_Click(object sender, EventArgs e)
        {
            removeDust1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDust2_Click(object sender, EventArgs e)
        {
            removeDust2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownScratch_Click(object sender, EventArgs e)
        {
            scratchedToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownSharpen1_Click(object sender, EventArgs e)
        {
            sharpen1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownSharpen2_Click(object sender, EventArgs e)
        {
            sharpen2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownEffects1_Click(object sender, EventArgs e)
        {
            effects1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownEffects2_Click(object sender, EventArgs e)
        {
            effects2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownEffects3_Click(object sender, EventArgs e)
        {
            effects3ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownBinarize_Click(object sender, EventArgs e)
        {
            binarizeToolStripMenuItem1_Click(sender, e);
        }

        private void dropDownBlob1_Click(object sender, EventArgs e)
        {
            blobRemoval1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownBlob2_Click(object sender, EventArgs e)
        {
            blobRemoval2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownBorder1_Click(object sender, EventArgs e)
        {
            borderCrop1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownBorder2_Click(object sender, EventArgs e)
        {
            borderCrop2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDeskew1_Click(object sender, EventArgs e)
        {
            deskew1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDeskew2_Click(object sender, EventArgs e)
        {
            deskew2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDeskew3_Click(object sender, EventArgs e)
        {
            deskew3ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDespeckle1_Click(object sender, EventArgs e)
        {
            despeckle1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDespeckle2_Click(object sender, EventArgs e)
        {
            despeckle2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDespeckle3_Click(object sender, EventArgs e)
        {
            despeckle3ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownDilate_Click(object sender, EventArgs e)
        {
            dilateToolStripMenuItem2_Click(sender, e);
        }

        private void dropDownErode_Click(object sender, EventArgs e)
        {
            erodeToolStripMenuItem2_Click(sender, e);
        }

        private void dropDownMulti_Click(object sender, EventArgs e)
        {
            multiPageToolStripMenuItem_Click(sender, e);
        }

        private void dropDownNegate_Click(object sender, EventArgs e)
        {
            negateToolStripMenuItem_Click_1(sender, e);
        }

        private void dropDownLineRemoval1_Click(object sender, EventArgs e)
        {
            removeLines1ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownLineRemoval2_Click(object sender, EventArgs e)
        {
            removeLines2ToolStripMenuItem_Click(sender, e);
        }

        private void dropDownShear_Click(object sender, EventArgs e)
        {
            shearToolStripMenuItem_Click_1(sender, e);
        }

        private void dropDownSmooth_Click(object sender, EventArgs e)
        {
            smoothZoomToolStripMenuItem_Click_1(sender, e);
        }

        private void dropDownTags_Click(object sender, EventArgs e)
        {
            tagsToolStripMenuItem1_Click(sender, e);
        }

        private void fileToolStripDropDownMenuItem_Click(object sender, EventArgs e)
        {
            fileOpenToolStripMenuItem_Click(sender, e);
        }

        private void fileWithLoadOptionsDropDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileWithLoadOptionsToolStripMenuItem_Click(sender, e);
        }

        private void OpenButton_ButtonClick(object sender, EventArgs e)
        {
            fileToolStripDropDownMenuItem_Click(sender, e);
        }   

        private void CloseButton_Click(object sender, EventArgs e)
        {
            //check if image has been edited in anyway whatsoever before continuing
            if (CheckForImageChanges() == false)
            {
                CloseImage();
            }
        }

        private void directoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            directoryOpenToolStripMenuItem_Click(sender, e);
        }

        private void rawFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rawFileOpenToolStripMenuItem_Click(sender, e);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            saveToolStripMenuItem_Click(sender, e);
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            printImageToolStripMenuItem_Click(sender, e);
        }

        private void ScanButton_Click(object sender, EventArgs e)
        {
            try
            {
                //check if image has been edited in anyway whatsoever before continuing
                if (CheckForImageChanges() == false)
                {
                    using (ScanForm scanForm = new ScanForm())
                    {
                        helper.CenterScreen(scanForm);

                        if (scanForm.ShowDialog() == DialogResult.OK)
                        {
                            ImageMemoryReset();

                            editFlag = false;

                            filenameOpened = scanForm.FilenameSavedAs;

                            SampleImageLoad = true;

                            OpenFileDialog(false);

                            ToolbarMenu(true);

                            ResetImageSelection();

                            //if an image is opened via the OpenFileDialog, want to reset ThumbnailXpress DrillDown control
                            //to be Recent Images
                            if (thumbnailTextBox.Text != "Recent Images")
                            {
                                RetrieveRecentItems();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshImage();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void CutButton_Click(object sender, EventArgs e)
        {
            try
            {
                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }

                processor1.Image = imageXView.Image.Copy();

                //if nothing selected, copy whole image to clipboard
                if (startPointSelection == new Point(0, 0) && endPointSelection == new Point(0, 0))
                {
                    imageXView.CopyToClipboard();

                    //create a white image of the same size as the currently loaded image
                    ImageX blankImage = new ImageX(imagXpress1, widthView, heightView, bppView, Color.White);

                    //merge current image with white image to create empty image, effectively cutting the whole image
                    processor1.Merge(ref blankImage, MergeSize.Crop, MergeStyle.Normal, false, Color.Transparent, 100, 0);

                    //resource cleanup, must dispose of ImageX blankImge created above
                    if (blankImage != null)
                    {
                        blankImage.Dispose();
                        blankImage = null;
                    }
                }
                else
                {
                    //rubberband selected region
                    imageXView.Rubberband.Start(startPointSelection);
                    imageXView.Rubberband.Update(endPointSelection);

                    //pass rubberbanded image to new ImageXView
                    ImageXView imageRubberbanded = new ImageXView(imagXpress1);

                    imageRubberbanded.Image = imageXView.Rubberband.Copy();

                    //create a white image of size of the selection by the user
                    ImageX blankImage = new ImageX(imagXpress1, imageXView.Rubberband.Dimensions.Width,
                        imageXView.Rubberband.Dimensions.Height, bppView, Color.White);

                    //tell processor to only act on this area
                    processor1.SetArea(imageXView.Rubberband.Dimensions);

                    //clear the rubberband from display
                    imageXView.Rubberband.Stop();

                    //copy the image selected to the clipboard
                    imageRubberbanded.CopyToClipboard();

                    //merge current image with white image to cut out portion of image selected by user
                    processor1.Merge(ref blankImage, MergeSize.Crop, MergeStyle.Normal, false, Color.Transparent, 100, 0);

                    //tell processor to act on whole image in future
                    processor1.SetArea(new RectangleF(0, 0, 0, 0));

                    //resource cleanup, must dispose of ImageX blankImage created above
                    if (blankImage != null)
                    {
                        blankImage.Dispose();
                        blankImage = null;
                    }

                    //resource cleanup, must dispose of ImageXView imageRubberbanded created above and dispose of its
                    //Image object
                    if (imageRubberbanded != null)
                    {
                        if (imageRubberbanded.Image != null)
                        {
                            imageRubberbanded.Image.Dispose();
                            imageRubberbanded.Image = null;
                        }

                        imageRubberbanded.Dispose();
                        imageRubberbanded = null;
                    }

                    //resource cleanup
                    imageXView.Rubberband.Clear();
                }

                //resource cleanup
                if (imageXView.Image != null)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                }

                imageXView.Image = processor1.Image.Copy();

                //a cut changes the image in some way, so turn on editFlag
                editFlag = true;

                //add the ImageX to memory so edits such as cut can be retrieved on the fly
                ImageMemoryAdd();

                ImageUpdateViewMemoryOrFile();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
            }
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            try
            {
                //if nothing selected, copy whole image to clipboard
                if (startPointSelection == new Point(0, 0) && endPointSelection == new Point(0, 0))
                {
                    imageXView.CopyToClipboard();
                }
                else
                {
                    //rubberband selected region
                    imageXView.Rubberband.Start(startPointSelection);
                    imageXView.Rubberband.Update(endPointSelection);

                    //pass rubberbanded image to new ImageXView
                    ImageXView imageRubberbanded = new ImageXView(imagXpress1);

                    imageRubberbanded.Image = imageXView.Rubberband.Copy();

                    imageXView.Rubberband.Stop();

                    //copy the image selected to the clipboard
                    imageRubberbanded.CopyToClipboard();

                    //resource cleanup, dispose of ImageXView imageRubberbanded created above, dispose of its Image object too
                    if (imageRubberbanded != null)
                    {
                        if (imageRubberbanded.Image != null)
                        {
                            imageRubberbanded.Image.Dispose();
                            imageRubberbanded.Image = null;
                        }

                        imageRubberbanded.Dispose();
                        imageRubberbanded = null;
                    }

                    //resource cleanup
                    imageXView.Rubberband.Clear();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HelpInfoButton_Click(object sender, EventArgs e)
        {
            try
            {
                helper.ShowHelp(this, "");
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void PasteButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageXView.PasteFromClipboard();

                //clear the annotations on the current page
                layerData.RemoveAt(pageNumber - 1);
                notateXpress1.Layers.Clear();
                notateXpress1.Layers.Add(new Layer());
                layerData.Insert(pageNumber - 1, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));

                //paste changes the current image, so turn on editFlag
                editFlag = true;

                ResetImageSelection();

                //add ImageX to memory so edits such as paste can be retreived on the fly
                ImageMemoryAdd();

                if (PDFOpened == true)
                {
                    Paging(PDFPageNumber);
                }
                else
                {
                    Paging(pageNumber);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Viewing Toolbar Event Handlers

        private void Zoom1to1Button_Click(object sender, EventArgs e)
        {
            try
            {
                //imageXView object's AutoResize is set to BestFit at startup, in order to zoom to 1:1 ratio we must
                //set to a different enumeration that allows ZoomFactor setting to have an effect
                imageXView.AutoResize = AutoResizeType.CropImage;
                imageXView.ZoomFactor = 1.0;

                zoomNumericUpDown.Value = (decimal)1.0;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ZoomHeightButton_Click(object sender, EventArgs e)
        {
            ZoomToolUpdate(ZoomToFitType.FitHeight);
        }

        private void ZoomWidthButton_Click(object sender, EventArgs e)
        {
            ZoomToolUpdate(ZoomToFitType.FitWidth);
        }

        private void ZoomFitButton_Click(object sender, EventArgs e)
        {
            ZoomToolUpdate(ZoomToFitType.FitBest);
        }

        private void AreaButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(AreaButton.Name, Tool.Select);
        }

        private void HandButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(HandButton.Name, Tool.Hand);
        }

        private void PointerIXButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(PointerIXButton.Name, Tool.None);
        }

        private void MagnifierButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(MagnifierButton.Name, Tool.Magnify);
        }

        private void ZoomRectangleButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(ZoomRectangleButton.Name, Tool.ZoomRect);
        }

        private void ZoomInButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(ZoomInButton.Name, Tool.ZoomIn);
        }

        private void ZoomOutButton_Click(object sender, EventArgs e)
        {
            ViewToolbarUpdate(ZoomOutButton.Name, Tool.ZoomOut);
        }

        private void RotateHelper(double angle)
        {
            using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
            {
                proc.Rotate(angle);

                //resource cleanup
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                }

                imageXView.Image = proc.Image;

                ImageProcesssingHelper();
            }
        }

        private void rotateLeftButton_Click(object sender, EventArgs e)
        {
            try
            {
                RotateHelper(90.0);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void rotateRightButton_Click(object sender, EventArgs e)
        {
            try
            {
                RotateHelper(270.0);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Annotate Toolbar Event Handlers

        private void PointerNXButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(PointerNXButton.Name, AnnotationTool.PointerTool);
        }

        private void NoteButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(NoteButton.Name, AnnotationTool.NoteTool);
        }

        private void TextButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(TextButton.Name, AnnotationTool.TextTool);
        }

        private void RectangleButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(RectangleButton.Name, AnnotationTool.RectangleTool);
        }

        private void ButtonButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(ButtonButton.Name, AnnotationTool.ButtonTool);
        }

        private void ImageButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(ImageButton.Name, AnnotationTool.ImageTool);
        }

        private void RulerButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(RulerButton.Name, AnnotationTool.RulerTool);
        }

        private void PolygonButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(PolygonButton.Name, AnnotationTool.PolygonTool);
        }

        private void PolylineButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(PolylineButton.Name, AnnotationTool.PolyLineTool);
        }

        private void LineButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(LineButton.Name, AnnotationTool.LineTool);
        }

        private void FreehandButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(FreehandButton.Name, AnnotationTool.FreehandTool);
        }

        private void StampButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(StampButton.Name, AnnotationTool.StampTool);
        }

        private void EllipseButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(EllipseButton.Name, AnnotationTool.EllipseTool);
        }

        private void HighlightButton_Click(object sender, EventArgs e)
        {
            AnnotateToolbarUpdate(HighlightButton.Name, AnnotationTool.BlockHighlightTool);
        }

        #endregion

        #region Paging Toolbar Event Handlers

        private void PageBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if enter key is pressed, enter IF statement
            if (e.KeyChar == (char)Keys.Enter)
            {
                try
                {
                    Int32 pageChosen = Convert.ToInt32(PageBox.Text);

                    //if pdf is chosen must base paging on different values then ImagXpress uses
                    if (PDFOpened == true)
                    {
                        //make sure page # entered is valid (within page range)
                        if ((pageChosen > 0) && (pageChosen <= pageCount))
                        {
                            PDFPageNumber = pageChosen - 1;

                            Paging(PDFPageNumber);
                        }
                        else
                        {
                            MessageBox.Show("Page number is not valid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            PageBox.Text = ((int)PDFPageNumber + 1).ToString();
                        }
                    }
                    else
                    {
                        //make sure page # entered is valid (within page range)
                        if (pageChosen > 0 && pageChosen <= pageCount)
                        {
                            pageNumber = pageChosen;

                            Paging(pageNumber);
                        }
                        else
                        {
                            MessageBox.Show("Page number is not valid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            PageBox.Text = imageXView.Image.Page.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //make sure page number is entered in correct format (no letters allowed as input for example)
                    MessageBox.Show(ex.Message, "Error from " + MethodBase.GetCurrentMethod().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                    if (PDFOpened == true)
                    {
                        PageBox.Text = ((int)PDFPageNumber + 1).ToString();
                    }
                    else
                    {
                        PageBox.Text = imageXView.Image.Page.ToString();
                    }
                }
            }
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            //increment the page by 1, update the page number displayed
            try
            {
                //if pdf is chosen, must base paging on different system then ImagXpress uses
                if (PDFOpened == true)
                {
                    PDFPageNumber += 1;
                    PageBox.Text = ((int)(PDFPageNumber + 1)).ToString();

                    Paging(PDFPageNumber);
                }
                else
                {
                    pageNumber += 1;

                    PageBox.Text = pageNumber.ToString();

                    Paging(pageNumber);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void PreviousButton_Click(object sender, EventArgs e)
        {
            //decrement the page by 1, update the page number displayed
            try
            {
                //if pdf is chosen, must base paging on different system then ImagXpress uses
                if (PDFOpened == true)
                {
                    PDFPageNumber -= 1;
                    PageBox.Text = ((int)(PDFPageNumber + 1)).ToString();

                    Paging(PDFPageNumber);
                }
                else
                {
                    pageNumber -= 1;

                    PageBox.Text = pageNumber.ToString();

                    Paging(pageNumber);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Menubar Event Handlers

        private void rawFileOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //check if image has been edited in anyway whatsoever before continuing
                if (CheckForImageChanges() == false)
                {
                    //reset SampleImageLoad flag
                    SampleImageLoad = false;

                    using (OpenFileDialog dlg = new OpenFileDialog())
                    {
                        //reset filter index to last chosen filter index for open file dialog
                        if (filenameOpened != null)
                            dlg.FileName = filenameOpened;

                        dlg.FilterIndex = openFilterIndex;
                        dlg.Title = "Open an Image File";

                        if (directoryOpened == null || Directory.Exists(directoryOpened) == false)
                        {
                            if (Directory.Exists(commonImagePath) == true)
                            {
                                dlg.InitialDirectory = commonImagePath;
                            }
                            else
                            {
                                dlg.InitialDirectory = Application.StartupPath;
                            }
                        }
                        else
                        {
                            dlg.InitialDirectory = directoryOpened;
                        }

                        dlg.Filter = helper.OpenImageFilter;

                        if (dlg.ShowDialog() == DialogResult.OK)
                        {
                            using (RawForm rawForm = new RawForm())
                            {
                                helper.CenterScreen(rawForm);

                                rawForm.FileName = dlg.FileName;

                                rawForm.WidthChosen = rawWidth;
                                rawForm.HeightChosen = rawHeight;
                                rawForm.Stride = rawStride;
                                rawForm.BitsPerPixel = rawBits;
                                rawForm.BytesPerPixel = rawBytes;
                                rawForm.HighBitIndex = rawHighBit;
                                rawForm.FormatChosen = rawChosen;
                                rawForm.Offset = rawOffset;

                                if (rawForm.ShowDialog() == DialogResult.OK)
                                {
                                    rawWidth = rawForm.WidthChosen;
                                    rawHeight = rawForm.HeightChosen;
                                    rawStride = rawForm.Stride;
                                    rawBits = rawForm.BitsPerPixel;
                                    rawBytes = rawForm.BytesPerPixel;
                                    rawHighBit = rawForm.HighBitIndex;
                                    rawChosen = rawForm.FormatChosen;
                                    rawOffset = rawForm.Offset;

                                    PegasusImaging.WinForms.ImagXpress9.LoadOptions rawLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();

                                    rawLoadOptions.Raw.BitsPerPixel = rawBits;
                                    rawLoadOptions.Raw.BytesPerPixel = rawBytes;
                                    rawLoadOptions.Raw.Height = rawHeight;
                                    rawLoadOptions.Raw.HighBitIndex = rawHighBit;
                                    rawLoadOptions.Raw.PixelFormat = rawChosen;
                                    rawLoadOptions.Raw.Stride = rawStride;
                                    rawLoadOptions.Raw.Width = rawWidth;

                                    rawLoadOptions.LoadMode = LoadMode.Raw;

                                    //remember the last filter index and file chosen by the user
                                    openFilterIndex = 0;
                                    filenameOpened = dlg.FileName;

                                    filenameOpened = Path.GetFullPath(filenameOpened);

                                    //a new image was opened, so reset the editFlag
                                    editFlag = false;

                                    //set global load options based on LoadOptions set in OpenFileDialog
                                    loadOptions = rawLoadOptions;

                                    pageNumber = 1;

                                    pageCount = 1;

                                    PagingUpdate();

                                    Paging(pageNumber);

                                    if (imageXView.Image != null)
                                    {
                                        loadOptions.LoadMode = LoadMode.Normal;

                                        grayComboBox.SelectedIndex = 0;

                                        if (notateXpress1.Layers.Count > 0)
                                            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;

                                        ResetToolbars(false);

                                        UpdateAnnotationSize();

                                        ToolbarMenu(true);

                                        ResetImageSelection();

                                        //if an image is opened via the OpenFiileDialog, want to reset ThumbnailXpress DrillDown control
                                        //to be Recent Images
                                        if (thumbnailTextBox.Text != "Recent Images")
                                        {
                                            RetrieveRecentItems();
                                        }

                                        //check to make sure file opened doesn't already have thumbnail, if it does then don't add a new one,
                                        //otherwise add a new thumbnail to the end and select it
                                        if (CheckForRepeatsInThumbnailDrillDown(filenameOpened, 0) == false)
                                        {
                                            if (initialImageLoad == true)
                                            {
                                                ThumbnailMemoryUpdate(-1, thumbnailXpressDrillDown, filenameOpened, loadOptions);
                                            }
                                            else
                                            {
                                                thumbnailXpressDrillDown.Items.AddItemsFromFile(filenameOpened, -1, false);
                                            }

                                            thumbnailXpressDrillDown.SelectedItems.Clear();
                                            thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[thumbnailXpressDrillDown.Items.Count - 1]);

                                            //scroll thumbnail selected into view
                                            thumbnailXpressDrillDown.EnsureVisible(thumbnailXpressDrillDown.Items.Count - 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (ImageXException ex)
            {
                ErrorHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void directoryOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //check if image has been edited in anyway whatsoever before continuing
                if (CheckForImageChanges() == false)
                {
                    if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                    {
                        string dirChosen = folderBrowserDialog.SelectedPath;

                        //if the directory was already added to the Recent Images as thumbnail, don't add it again
                        CheckForRepeatsInThumbnailDrillDown(dirChosen, 1);

                        //clear the current thumbnails, then add thumbnails for the directory chosen
                        thumbnailXpressDrillDown.Items.Clear();
                        thumbnailXpressDrillDown.Items.AddItemsFromDirectory(dirChosen + "\\", 0);

                        //store the parent directory of the directory chosen
                        parentDirectory = Path.GetDirectoryName(dirChosen);

                        //update the textbox with the directory chosen so the user knows when they drill up or down via thumbnail
                        //where they're located
                        thumbnailTextBox.Text = dirChosen;

                        //give the user the option to navigate up or to return to recent images
                        PseudoFoldersForThumbnailDrillDown();

                        //reset editFlag
                        editFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseButton_Click(sender, e);
        }

        private void fileWithLoadOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SampleImageLoad = false;

            OpenFileDialog(true);
        }

        private void fileOpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //reset SampleImageLoad flag
            SampleImageLoad = false;

            OpenFileDialog(false);
        }

        private void withSaveOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs(true);
        }

        private void saveasNoOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs(false);
        }

        private void scanImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScanButton_Click(sender, e);
        }

        private void printImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (PrintForm printForm = new PrintForm())
                {
                    //initialize array of ImageX's to be size of number of pages in image opened
                    PrintImages = new ImageX[pageCount];

                    if (editFlag == false)
                    {
                        if (PDFOpened == false)
                        {
                            //if the image is in its original form then create the array using ImageX's
                            //created by calling ImageX.FromFile()
                            for (int i = 0; i < PrintImages.Length; i++)
                            {
                                PrintImages[i] = ImageX.FromFile(imagXpress1, filenameOpened, i + 1, loadOptions);
                            }
                        }
                        else
                        {
                            //if the image is in its original form and it's a PDF then create the array of ImageX's
                            //by calling RenderPageToBitmap() and then ImageX.FromBitmap()
                            for (int i = 0; i < PrintImages.Length; i++)
                            {
                                using (Bitmap bp = pdfXpress1.Documents[0].RenderPageToBitmap(i, ro))
                                {
                                    PrintImages[i] = ImageX.FromBitmap(imagXpress1, bp);
                                    PrintImages[i].ImageXData.Resolution.Units = GraphicsUnit.Inch;
                                    PrintImages[i].ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (PDFOpened == false)
                        {
                            //if the image has been edited, then pages in their orginal form should use ImageX.FromFile()
                            //to create the ImageX and pages that have been edited should use a copy of their ImageX
                            //already stored in memory in the ArrayList imagesOpened
                            for (int i = 0; i < PrintImages.Length; i++)
                            {
                                for (int j = 0; j < indexImagesOpened.Count; j++)
                                {
                                    if ((int)indexImagesOpened[j] == i + 1)
                                    {
                                        PrintImages[i] = ((ImageX)imagesOpened[j]).Copy();

                                        break;
                                    }
                                }

                                if (PrintImages[i] == null)
                                {
                                    PrintImages[i] = ImageX.FromFile(imagXpress1, filenameOpened, i + 1, loadOptions);
                                }
                            }
                        }
                        else
                        {
                            //if the image has been edited, then pages in their original form and is a PDF then we should use
                            //RenderPageToBitmap() and then ImageX.FromBitmap() to create the ImageX's and pages that have been
                            //edited should use a copy of their ImageX already stored in memory in imagesOpened
                            for (int i = 0; i < PrintImages.Length; i++)
                            {
                                for (int j = 0; j < indexImagesOpened.Count; j++)
                                {
                                    if ((int)indexImagesOpened[j] == i)
                                    {
                                        PrintImages[i] = ((ImageX)imagesOpened[j]).Copy();

                                        break;
                                    }
                                }

                                if (PrintImages[i] == null)
                                {
                                    //if the image is a PDF then create the ImageX
                                    //by calling RenderPageToBitmap() and then ImageX.FromBitmap()
                                    using (Bitmap bp = pdfXpress1.Documents[0].RenderPageToBitmap(i, ro))
                                    {
                                        PrintImages[i] = ImageX.FromBitmap(imagXpress1, bp);
                                        PrintImages[i].ImageXData.Resolution.Units = GraphicsUnit.Inch;
                                        PrintImages[i].ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);                                
                                    }
                                }
                            }
                        }
                    }

                    //set the Pages to be the array of ImageX's created above
                    printForm.Pages = PrintImages;

                    //show the PrintForm
                    printForm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                //must explicitly dispose of the array of ImageX's created above
                for (int i = 0; i < PrintImages.Length; i++)
                {
                    //resource cleanup
                    if (PrintImages[i] != null)
                    {
                        PrintImages[i].Dispose();
                        PrintImages[i] = null;
                    }
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void saveAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //temporarily remove the Annotation events
                notateXpress1.AnnotationAdded -= new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
                notateXpress1.AnnotationDeleted -= new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);

                using (SaveFileDialog saveAnnDlg = new SaveFileDialog())
                {
                    saveAnnDlg.Title = "Save Annotation File";
                    saveAnnDlg.Filter = "NotateXpress XML (.NXXML)|*.nxxml|All Files (*.*)|*.*";
                    if (saveAnnDlg.ShowDialog() == DialogResult.OK)
                    {                        
                        //store the current AnnotationTool
                        AnnotationTool toolSelected = notateXpress1.Layers[0].Toolbar.Selected;

                        PegasusImaging.WinForms.NotateXpress9.SaveOptions annSaveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();
                        annSaveOptions.AnnType = AnnotationType.NotateXpressXml;
                        PegasusImaging.WinForms.NotateXpress9.LoadOptions annLoadOptions = new PegasusImaging.WinForms.NotateXpress9.LoadOptions();

                        //only loop through and save pages that are currently in memory
                        //as pages with annotations should be saved in memory
                        int j = 0;
                        for (int i = 0; i < pageCount; i++)
                        {
                            if (pageCount > 1)
                            {
                                if (indexImagesOpened.Contains(i + 1))
                                {
                                    if (imageXView != null)
                                    {
                                        if (imageXView.Image != null)
                                        {
                                            imageXView.Image.Dispose();
                                            imageXView.Image = null;
                                        }
                                    }

                                    imageXView.Image = ((ImageX)imagesOpened[j]).Copy();
                                    imageXView.Update();
                                    j++;
                                }
                                notateXpress1.Layers.Clear();
                                notateXpress1.Layers.Add(new Layer());
                                notateXpress1.Layers.FromByteArray((Byte[])layerData[(int)i], annLoadOptions);
                                notateXpress1.PageNumber = i + 1;
                            }
                            notateXpress1.Layers.SaveToFile(saveAnnDlg.FileName, annSaveOptions);
                            notateXpress1.PageNumber = 1;
                        }

                        //reload current page and annotations if they are not currently loaded
                        if (pageNumber != pageCount)
                        {
                            Paging(pageNumber);
                        }

                        notateXpress1.Layers[0].Toolbar.Selected = toolSelected;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                //restore annotation events
                notateXpress1.AnnotationAdded += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
                notateXpress1.AnnotationDeleted += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
            }
        }

        private void loadAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //temporarily remove the Annotation events
                notateXpress1.AnnotationAdded -= new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
                notateXpress1.AnnotationDeleted -= new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);

                using (OpenFileDialog loadAnnDlg = new OpenFileDialog())
                {
                    loadAnnDlg.Title = "Load Annotation File";
                    loadAnnDlg.Filter = "NotateXpress XML (.NXXML)|*.nxxml|All Files (*.*)|*.*";
                    loadAnnDlg.FilterIndex = 0;

                    if (loadAnnDlg.ShowDialog() == DialogResult.OK)
                    {                        
                        notateXpress1.Layers.Clear();
                        PegasusImaging.WinForms.NotateXpress9.LoadOptions annLoadOptions = new PegasusImaging.WinForms.NotateXpress9.LoadOptions();
                        annLoadOptions.AnnType = AnnotationType.NotateXpressXml;
                        PegasusImaging.WinForms.NotateXpress9.SaveOptions annSaveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();

                        int i = 0;
                        for (i = 0; i < pageCount; i++)
                        {
                            //if the annotations are not on the current page, load the page.
                            if (pageCount > 1)
                            {
                                if (imageXView != null)
                                {
                                    if (imageXView.Image != null)
                                    {
                                        imageXView.Image.Dispose();
                                        imageXView.Image = null;
                                    }
                                }

                                if (PDFOpened == true)
                                {
                                    using (Bitmap bp = pdfXpress1.Documents[0].RenderPageToBitmap(i, ro))
                                    {
                                        imageXView.Image = ImageX.FromBitmap(imagXpress1, bp);
                                        imageXView.Image.ImageXData.Resolution.Units = GraphicsUnit.Inch;
                                        imageXView.Image.ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);
                                    }
                                }
                                else
                                {
                                    imageXView.Image = ImageX.FromFile(imagXpress1, filenameOpened, i + 1);
                                }
                            }
                            notateXpress1.PageNumber = i + 1;
                            notateXpress1.Layers.Clear();
                            notateXpress1.Layers.Add(new Layer());
                            try
                            {
                                notateXpress1.Layers.FromFile(loadAnnDlg.FileName, annLoadOptions);
                                notateXpress1.PageNumber = 1;
                                layerData[i] = notateXpress1.Layers.SaveToByteArray(annSaveOptions);
                            }
                            catch (Exception)
                            {
                                //this page does not have any layers
                            }
                        }

                        //reload current page if it is not currently loaded
                        if (pageNumber != pageCount)
                            Paging(pageNumber);

                        notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                //restore annotation events
                notateXpress1.AnnotationAdded += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
                notateXpress1.AnnotationDeleted += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpInfoButton_Click(sender, e);
        }

        private void gettingStartedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {                
                using (QuickStartForm startForm = new QuickStartForm())
                {
                    helper.CenterScreen(startForm);

                    if (File.Exists(helper.quickStartFile) == true)
                    {
                        using (TextReader tw = new StreamReader(helper.quickStartFile))
                        {
                            if (tw.ReadLine() == helper.gettingStartedKey)
                            {
                                startForm.ShowHelp = true;
                            }
                        }
                    }

                    startForm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void thumbnailXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            thumbnailXpressDrillDown.AboutBox();
        }

        private void pdfXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pdfXpress1.AboutBox();
        }

        private void notateXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.AboutBox();
        }

        private void twainProToolStripMenuItem_Click(object sender, EventArgs e)
        {
            twainPro1.AboutBox();
        }

        private void printProToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printPro1.AboutBox();
        }

        #endregion

        #region ThumbnailXpressPaging Event Handlers

        private void thumbnailXpressPaging_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                //identifty the ThumbnailItem chosen by the user
                Point mouseOverThumbnail = e.Location;

                ThumbnailItem item = null;
                try
                {
                    item = thumbnailXpressPaging.GetItemFromPoint(mouseOverThumbnail);
                }
                catch { }

                if (item != null)
                {
                    ResetImageSelection();

                    //if it's an actual ThumbnailItem then do paging based on PDF or not
                    if (item.Filename.EndsWith(".pdf", StringComparison.OrdinalIgnoreCase) || item.Filename.EndsWith(".tpdf", StringComparison.OrdinalIgnoreCase) == true)
                    {
                        PageBox.Text = item.Page.ToString();

                        PDFPageNumber = item.Page - 1;

                        Paging(PDFPageNumber);
                    }
                    else
                    {
                        pageNumber = item.Page;

                        PageBox.Text = pageNumber.ToString();

                        Paging(pageNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region ThumbnailXpressDrillDown Event Handlers

        private void thumbnailXpressDrillDown_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (mouseOverItemThumbnailDrillDown != null)
                {
                    if (mouseOverItemThumbnailDrillDown.UserTag != null)
                    {
                        //if the user double clicked the Recent Images folder, then display the Recent Images
                        if (mouseOverItemThumbnailDrillDown.UserTag.ToString() == "...Recent Images...")
                        {
                            RetrieveRecentItems();
                        }
                    }
                    //if the user double clicked the navigate up folder, enter below ELSE IF statement
                    else if (mouseOverItemThumbnailDrillDown.Filename == "...")
                    {
                        if (parentDirectory != null)
                        {
                            //update textbox with new location navigated to
                            thumbnailTextBox.Text = parentDirectory;

                            //clear the thumbnails, add new thumbnails for the directory navigated to
                            thumbnailXpressDrillDown.Items.Clear();
                            thumbnailXpressDrillDown.Items.AddItemsFromDirectory(parentDirectory + "\\", 0);

                            //get the parent directory of the current directory
                            parentDirectory = Path.GetDirectoryName(parentDirectory);

                            //add thumbnails to navigate up or return to Recent Images
                            PseudoFoldersForThumbnailDrillDown();
                        }
                    }
                    else
                    {
                        //if the user drills down into a folder, execute the below IF statement
                        if (mouseOverItemThumbnailDrillDown.Type == ThumbnailType.ParentDirectory
                            || mouseOverItemThumbnailDrillDown.Type == ThumbnailType.SubDirectory)
                        {
                            //update the textbox with the current directory
                            thumbnailTextBox.Text = mouseOverItemThumbnailDrillDown.Filename;

                            //get the parent directory of the directory drilled into
                            parentDirectory = Path.GetDirectoryName(mouseOverItemThumbnailDrillDown.Filename);

                            //add thumbnails to navigate up or return to Recent Images
                            PseudoFoldersForThumbnailDrillDown();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void thumbnailXpressDrillDown_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                //identifty the ThumbnailItem chosen by the user
                mouseOverPointThumbnailDrillDown = e.Location;

                try
                {
                    mouseOverItemThumbnailDrillDown = thumbnailXpressDrillDown.GetItemFromPoint(mouseOverPointThumbnailDrillDown);
                }
                catch { }

                if (mouseOverItemThumbnailDrillDown != null)
                {
                    //if the thumbnail is an image, update the ImageXView with the image
                    if (mouseOverItemThumbnailDrillDown.Type == ThumbnailType.Image
                        || mouseOverItemThumbnailDrillDown.Type == ThumbnailType.Cad
                        || mouseOverItemThumbnailDrillDown.Type == ThumbnailType.Pdf)
                    {
                        //check if image has changed in any way before proceeding
                        if (CheckForImageChanges() == false)
                        {
                            ToolbarMenu(true);

                            ResetImageSelection();

                            filenameOpened = mouseOverItemThumbnailDrillDown.Filename;

                            try
                            {
                                //image was updated because thumbnail was selected from ThumbnailXpress
                                //DrillDown control, so must reset editFlag
                                editFlag = false;

                                ImageUpdate();

                                //don't add a new thumbnail if it's already in the collection
                                CheckForRepeatsInThumbnailDrillDown(filenameOpened, 0);
                            }
                            catch (ImageXException ex)
                            {
                                ErrorHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
                            }
                            catch (AccessViolationException ex)
                            {
                                ErrorHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        else
                        {
                            //if image has been changed in some way, and user selects Cancel from MessageBox
                            //prompted for file saving, then we act as if nothing changed, but the ThumbnailDrillDown
                            //control already changed what thumbnail was selected automatically so we need to reset it
                            RestoreThumbnailDrillDownSelectedItems();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region ThumbnailXpressDrillDown Methods

        private void RestoreThumbnailDrillDownSelectedItems()
        {
            thumbnailXpressDrillDown.SelectedItems.Clear();

            //if one of the thumbnails in ThumbnailXpressDrillDown corresponds to the current image in the ImageXView,
            //then select the thumbnail
            for (int i = 0; i < thumbnailXpressDrillDown.Items.Count; i++)
            {
                if (filenameOpened == thumbnailXpressDrillDown.Items[i].Filename)
                {
                    thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[i]);

                    //scroll thumbnail selected into view
                    thumbnailXpressDrillDown.EnsureVisible(i);

                    break;
                }
            }
        }

        private bool CheckForRepeatsInThumbnailDrillDown(string filename, int fileOrDir)
        {
            try
            {
                if (recentImages.Count > 0)
                {
                    for (int i = 0; i < recentImages.Count; i++)
                    {
                        //if a repeat is found return true, also if in Recent Images directory then need to reset
                        //thumbnail selected in ThumbnailXpressDrillDown control
                        if (recentImages[i].ToString() == filename && thumbnailTextBox.Text == "Recent Images")
                        {
                            thumbnailXpressDrillDown.SelectedItems.Clear();
                            thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[i]);

                            //scroll thumbnail selected into view
                            thumbnailXpressDrillDown.EnsureVisible(i);

                            return true;
                        }
                        //repeat found so return true
                        if (recentImages[i].ToString() == filename)
                        {
                            return true;
                        }
                    }
                }

                recentImages.Add(filename);
                recentImagesIdentify.Add(fileOrDir);
                recentLoadOptions.Add(loadOptions);

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error from " + MethodBase.GetCurrentMethod().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                return false;
            }
        }

        private void ClearThumbButton_Click(object sender, EventArgs e)
        {
            try
            {
                //check if image has been edited in anyway whatsoever before continuing
                if (CheckForImageChanges() == false)
                {
                    CloseImage();

                    //clear all thumbnails from ThumbnailXpressDrillDown control, and reset Arraylists,
                    //default to Recent Images directory
                    thumbnailXpressDrillDown.Items.Clear();
                    recentImagesIdentify.Clear();
                    recentImages.Clear();
                    recentLoadOptions.Clear();
                    thumbnailTextBox.Text = "Recent Images";
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void PseudoFoldersForThumbnailDrillDown()
        {
            //show a pseudo-folder as the first thumbnail which can be used to navigate up a directory
            ThumbnailItem itemParent = thumbnailXpressDrillDown.CreateThumbnailItem("...", 0, ThumbnailType.ParentDirectory);
            thumbnailXpressDrillDown.Items.Insert(0, itemParent);

            //show a pseudo-folder as the second thumbnail which can be used to return to Recent Images
            ThumbnailItem itemRecentImages = thumbnailXpressDrillDown.CreateThumbnailItem("..", 0, ThumbnailType.ParentDirectory);
            thumbnailXpressDrillDown.Items.Insert(1, itemRecentImages);
            thumbnailXpressDrillDown.Items[1].Descriptor = "...Recent Images...";
            thumbnailXpressDrillDown.Items[1].UserTag = "...Recent Images...";
        }

        private void RetrieveRecentItems()
        {
            try
            {
                //reset TextBox and clear current thumbnails
                thumbnailTextBox.Text = "Recent Images";
                thumbnailXpressDrillDown.Items.Clear();

                //iterate through the Recent Images, if it's an image add a thubmanil for the image, if it's a directory
                //add a thumbnail for the directory
                //this method retrieves the Recent Images to their original order from before a user loaded a directory
                for (int i = 0; i < recentImages.Count; i++)
                {
                    if ((int)recentImagesIdentify[i] == 0)
                    {
                        ThumbnailMemoryUpdate(i, thumbnailXpressDrillDown, (string)recentImages[i],
                            (PegasusImaging.WinForms.ImagXpress9.LoadOptions)recentLoadOptions[i]);
                    }
                    else
                    {
                        thumbnailXpressDrillDown.Items.AddItemsFromDirectory(recentImages[i].ToString(), -1);
                    }
                }

                //select thumbnail of currently opened image
                RestoreThumbnailDrillDownSelectedItems();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Tags Method

        private string GetJPEGComments()
        {
            try
            {
                if (imageXView.Image != null)
                {
                    //if there are JPEG comments, then reset the textbox and append them
                    if (imageXView.Image.JpegCommentCount > 0)
                    {
                        jpegComments = "";

                        for (int i = 1; i <= imageXView.Image.JpegCommentCount; i++)
                        {
                            jpegComments += imageXView.Image.ReturnJpegComment(i) + "\n";
                        }
                    }
                    else
                    {
                        jpegComments = "No JPEG Commments Found";
                    }
                }
            }
            catch (ImageXException)
            {
                jpegComments = "No JPEG Commments Found";
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }

            return jpegComments;
        }

        #endregion

        #region Save Methods/Original Image Edited Methods

        private int CheckIfImageExistsInMemory(int page)
        {
            //if the page number exists in the ArrayList this means the image represent this page is already in memory
            //so we return the index of the ArrayList where it was found, or return -1 representing that it wasn't found
            for (int i = 0; i < indexImagesOpened.Count; i++)
            {
                if ((int)indexImagesOpened[i] == page)
                {
                    return i;
                }
            }

            return -1;
        }

        private void SaveHelper(int index, string fileName, PegasusImaging.WinForms.ImagXpress9.SaveOptions so, bool metaData)
        {
            if (PDFOpened == true)
            {
                //if a PDF was opened, then we want to take the PDF document and render the page specified by the index
                //to a bitmap using the global RenderOptions
                using (Bitmap bp = pdfXpress1.Documents[0].RenderPageToBitmap(index, ro))
                {
                    //We pass this bitmap to an ImageX object
                    using (ImageX img = ImageX.FromBitmap(imagXpress1, bp))
                    {
                        //we manually set the resolution
                        img.ImageXData.Resolution.Units = GraphicsUnit.Inch;
                        img.ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);

                        SaveValidCompressionHelper(so, img);

                        //save the rendered pdf with the SaveOptions specified
                        img.Save(fileName, so);
                    }
                }
            }
            else
            {
                //we read the image in from the file at the specified index (pageNumber)
                using (ImageX img = ImageX.FromFile(imagXpress1, filenameOpened, index))
                {
                    if (fastTiff == true)
                    {
                        so.Tiff.UseIFDOffset = true;
                        so.Tiff.IFDOffset = fastTiffOffset;
                    }

                    SaveValidCompressionHelper(so, img);
                    
                    //save the image with the SaveOptions specified
                    img.Save(fileName, so);

                    if (fastTiff == true)
                        fastTiffOffset = so.Tiff.IFDOffset;

                    if (metaData == true && (so.Format == ImageXFormat.Jpeg || so.Format == ImageXFormat.Exif
                                        || so.Format == ImageXFormat.Tiff || so.Format == ImageXFormat.Cals))
                    {
                        SaveMetaDataHelper(fileName, index);
                    }
                }
            }
        }

        //this is an overload of the above SaveHelper that doesn't take a SaveOptions argument
        //we simply save without specifying SaveOptions
        private void SaveHelper(int index, string fileName)
        {
            if (PDFOpened == true)
            {
                using (Bitmap bp = pdfXpress1.Documents[0].RenderPageToBitmap(index, ro))
                {
                    using (ImageX img = ImageX.FromBitmap(imagXpress1, bp))
                    {
                        img.ImageXData.Resolution.Units = GraphicsUnit.Inch;
                        img.ImageXData.Resolution.Dimensions = new SizeF((float)ro.ResolutionX, (float)ro.ResolutionY);

                        img.Save(fileName);
                    }
                }
            }
            else
            {
                using (ImageX img = ImageX.FromFile(imagXpress1, filenameOpened, index))
                {
                    img.Save(fileName);
                }
            }
        }

        private void SaveMultiPageHelper(string fileName, PegasusImaging.WinForms.ImagXpress9.SaveOptions so, bool metaData)
        {
            int initial, upperBound;

            //we set where to start and end our loop based on if it's an image or PDF
            if (PDFOpened == true)
            {
                initial = 0;
                upperBound = pageCount;
            }
            else
            {
                initial = 1;
                upperBound = pageCount + 1;
            }

            if (so != null)
            {
                if (so.Tiff.UseIFDOffset == true)
                {
                    fastTiff = true;
                }
            }

            for (int i = initial; i < upperBound; i++)
            {
                if (i == initial)
                {
                    //delete the file if it already exists
                    if (File.Exists(fileName))
                    {
                        //if the input file is the same as the output file,
                        //make a temp copy of the file in order to later load
                        //page 2, 3, etc.
                        if (fileName == filenameOpened)
                        {
                            copyOfOpenFile = System.IO.Path.GetTempPath() + filenameOpened.Substring(filenameOpened.LastIndexOf('\\')+1);
                            File.Copy(filenameOpened, copyOfOpenFile, true);
                            filenameOpened = copyOfOpenFile;
                        }
                        File.Delete(fileName);
                    }
                }

                //if the image has been edited in some way, check to see if the ImageX for the current page is already
                if (editFlag == true)
                {
                    int alreadyExisting;

                    alreadyExisting = CheckIfImageExistsInMemory(i);

                    if (alreadyExisting == -1)
                    {
                        //if the image isn't in memory call our SaveHelper routine with or without the SaveOptions
                        //based on what the user chose to save the image
                        if (so != null)
                        {
                            SaveHelper(i, fileName, so, metaData);
                        }
                        else
                        {
                            SaveHelper(i, fileName);
                        }
                    }
                    else
                    {
                        SaveValidCompressionHelper(so, ((ImageX)imagesOpened[alreadyExisting]));

                        //if the image is already in memory, we can save it directly with the SaveOptions specified
                        ((ImageX)imagesOpened[alreadyExisting]).Save(fileName, so);
                    }
                }
                else
                {
                    //if the image isn't in memory call our SaveHelper routine with or without the SaveOptions
                    //based on what the user chose to save the image
                    if (so != null)
                    {
                        SaveHelper(i, fileName, so, metaData);
                    }
                    else
                    {
                        SaveHelper(i, fileName);
                    }
                }
            }

            if (copyOfOpenFile != null)
            {
                //if a temp copy of the file was made, delete it
                if (copyOfOpenFile.Length > 0)
                    File.Delete(copyOfOpenFile);
            }

            filenameOpened = fileName;
        }

        private void SaveValidCompressionHelper(PegasusImaging.WinForms.ImagXpress9.SaveOptions saveChosenOptions, 
                                                ImageX img)
        {
            //Rle is 1-bit compression for TIF, but ImagXpress doesn't automatically downsample to
            //1-bit so we must manually do this
            //Jpeg or Jpeg7 is 8 or 24-bit compression for TIF, but ImagXpress doesn't automatically
            //upsample so we must manually do this
            if (saveChosenOptions.Tiff.Compression == Compression.Rle &&
                img.ImageXData.BitsPerPixel != 1)
            {
                using (Processor proc = new Processor(imagXpress1, img))
                {
                    proc.ColorDepth(1, 0, 0);
                }

                metaOff = true;
            }
            else if ((saveChosenOptions.Tiff.Compression == Compression.Jpeg 
                || saveChosenOptions.Tiff.Compression == Compression.Jpeg7) &&
                (img.ImageXData.BitsPerPixel != 8 || img.ImageXData.BitsPerPixel != 24))
            {
                using (Processor proc = new Processor(imagXpress1, img))
                {
                    proc.ColorDepth(24, 0, 0);
                }

                metaOff = true;
            }

            //if the resolution of the input image is so small that the PDF created will be beyond 200x200 inches then
            //PDFXpress won't be able to render it so need to force a larger resolution on the image first, will default
            //to a PDF of size 8.5x11 inches
            if (saveChosenOptions.Format == ImageXFormat.Pdf)
            {
                img.Resolution.Units = GraphicsUnit.Inch;

                if ((img.ImageXData.Width / img.Resolution.Dimensions.Width) > 200
                    || (img.ImageXData.Height / img.Resolution.Dimensions.Height) > 200)
                {
                    img.Resolution.Dimensions = new SizeF((float)(img.ImageXData.Width / 8.5), img.ImageXData.Height / 11);
                }
            }
        }

        private void SaveMetaDataHelper(string fileName, int index)
        {
            using (ImageX data = new ImageX(imagXpress1))
            {
                using (ImageX dataToCopy = ImageX.ReadAllMetaData(imagXpress1, filenameOpened, index))
                {
                    data.Tags = dataToCopy.Tags;
                    data.IptcDataSet = dataToCopy.IptcDataSet;
                    data.CalTags = dataToCopy.CalTags;

                    // If there is meta data, save it.  Only certain file formats support meta data.
                    if((!(data.Tags == null && data.IptcDataSet == null  && data.CalTags == null)) && fastTiff == false
                        && metaOff == false)
                        ImageX.SaveAllMetaData(imagXpress1, data, fileName, index);
                }
            }
        }

        private void SaveAs(bool useOptions)
        {
            try
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    //reset fast Tiff write
                    fastTiff = false;
                    fastTiffOffset = 0;

                    metaOff = false;

                    //save file dialog defaults to initial directory of image currently opened and filename
                    //of image opened
                    //remove the file extension
                    int pos = filenameOpened.LastIndexOf('.');
                    if (pos > 0)
                        dlg.FileName = filenameOpened.Substring(0, pos);
                    else
                        dlg.FileName = filenameOpened;

                    if (Directory.Exists(directorySaved) == true)
                    {
                        dlg.InitialDirectory = directorySaved;
                    }
                    else
                    {
                        dlg.InitialDirectory = Path.GetDirectoryName(filenameOpened);
                    }

                    dlg.InitialDirectory = Application.StartupPath;

                    dlg.Title = "Save the Image File";

                    dlg.Filter = helper.SaveImageFilter;

                    dlg.FilterIndex = helper.GetSaveAsIndex(imageXView.Image.ImageXData.Format);

                    saveChangesResult = dlg.ShowDialog();

                    if (saveChangesResult == DialogResult.OK)
                    {
                        saveFilterIndex = dlg.FilterIndex;

                        directorySaved = Path.GetDirectoryName(dlg.FileName);

                        //if SaveOptions are specified enter the IF statement
                        if (useOptions == true)
                        {
                            using (SaveOptionsForm saveForm = new SaveOptionsForm())
                            {
                                helper.CenterScreen(saveForm);

                                saveForm.ImageSaveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();

                                helper.IdentifySaveOptionUI(dlg.FilterIndex, saveForm, saveForm.ImageSaveOptions);

                                saveForm.ShowDialog();

                                PegasusImaging.WinForms.ImagXpress9.SaveOptions saveOptions = saveForm.ImageSaveOptions;

                                //if the image is multipage we want to save all of the pages with SaveOptions
                                if (pageCount > 1 && ((saveOptions.Tiff.MultiPage == true && saveOptions.Format == ImageXFormat.Tiff) || (saveOptions.Dcx.MultiPage == true && saveOptions.Format == ImageXFormat.Dcx)
                                    || (saveOptions.Pdf.MultiPage == true && saveOptions.Format == ImageXFormat.Pdf) || (saveOptions.Icon.MultiPage == true && saveOptions.Format == ImageXFormat.Ico)))
                                {
                                    SaveMultiPageHelper(dlg.FileName, saveOptions, saveForm.MetaDataSave);
                                }
                                else
                                {
                                    SaveValidCompressionHelper(saveOptions, imageXView.Image);
                                    
                                    //save single-page image to file with saveOptions
                                    imageXView.Image.Save(dlg.FileName, saveOptions);

                                    if (saveForm.MetaDataSave == true && 
                                        (saveOptions.Format == ImageXFormat.Jpeg || saveOptions.Format == ImageXFormat.Exif
                                        || saveOptions.Format == ImageXFormat.Tiff || saveOptions.Format == ImageXFormat.Cals))
                                    {
                                        SaveMetaDataHelper(dlg.FileName, 1);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //if the input image is multipage and the output file type supports multipage,
                            //we want to save all of the pages
                            if (pageCount > 1 && (dlg.FilterIndex == 2 || dlg.FilterIndex == 3 || dlg.FilterIndex == 15 || dlg.FilterIndex == 20))
                            {
                                PegasusImaging.WinForms.ImagXpress9.SaveOptions saveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
                                saveOptions.Tiff.MultiPage = true;
                                saveOptions.Dcx.MultiPage = true;
                                saveOptions.Icon.MultiPage = true;
                                saveOptions.Pdf.MultiPage = true;
                                SaveMultiPageHelper(dlg.FileName, saveOptions, false);
                            }
                            else
                            {
                                //save single-page image to file without saveOptions
                                imageXView.Image.Save(dlg.FileName);
                            }
                        }

                        //reset edit flag
                        editFlag = false;

                        //when image is saved, update old thumbnail of image being saved with new thumbnail 
                        //representing with save as filename
                        //if image being saved already exists in Recent Images, need to update that thumbnail as well
                        bool alreadyExists = false;

                        for (int i = 0; i < thumbnailXpressDrillDown.Items.Count; i++)
                        {
                            if (thumbnailXpressDrillDown.Items[i].Filename == dlg.FileName)
                            {
                                thumbnailXpressDrillDown.Items.RemoveAt(i);

                                thumbnailXpressDrillDown.Items.AddItemsFromFile(dlg.FileName, i, false);

                                thumbnailXpressDrillDown.SelectedItems.Clear();
                                thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[i]);

                                alreadyExists = true;

                                break;
                            }
                        }

                        if (alreadyExists == false)
                        {
                            thumbnailXpressDrillDown.Items.AddItemsFromFile(dlg.FileName, -1, false);

                            thumbnailXpressDrillDown.SelectedItems.Clear();
                            thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[thumbnailXpressDrillDown.Items.Count - 1]);

                            recentImages.Add(dlg.FileName);
                            recentImagesIdentify.Add(0);
                        }

                        filenameOpened = dlg.FileName;

                        RefreshImage();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Save()
        {
            try
            {
                //if the image is multipage we want to save all of the pages
                if (pageCount > 1)
                {
                    PegasusImaging.WinForms.ImagXpress9.SaveOptions saveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
                    saveOptions.Tiff.MultiPage = true;
                    saveOptions.Dcx.MultiPage = true;
                    saveOptions.Icon.MultiPage = true;
                    saveOptions.Pdf.MultiPage = true;
                    SaveMultiPageHelper(filenameOpened, saveOptions, false);
                }
                else
                {
                    //save single-page image to file without saveOptions
                    imageXView.Image.Save(filenameOpened);
                }

                //reset edit flag
                editFlag = false;

                //update thumbnail and select it
                for (int i = 0; i < thumbnailXpressDrillDown.Items.Count; i++)
                {
                    if (filenameOpened == thumbnailXpressDrillDown.Items[i].Filename)
                    {
                        thumbnailXpressDrillDown.Items.RemoveAt(i);
                        thumbnailXpressDrillDown.Items.AddItemsFromFile(filenameOpened, i, false);
                        thumbnailXpressDrillDown.SelectedItems.Clear();
                        thumbnailXpressDrillDown.SelectedItems.Add(thumbnailXpressDrillDown.Items[i]);

                        break;
                    }
                }

                RefreshImage();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool ImageEdited()
        {
            DialogResult savePrompt = MessageBox.Show("Do you want to save your changes to the image "
                                      + Path.GetFullPath(filenameOpened) + "?", "File Save Warning",
                                      MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            //if the user selects Cancel, return true
            if (savePrompt == DialogResult.Cancel)
                return true;
            //if the user selects Yes, then prompt user with Save As File Dialog
            if (savePrompt == DialogResult.Yes)
            {
                SaveAs(false);

                //if user doesn't end up saving, then return true
                if (saveChangesResult == DialogResult.Cancel)
                {
                    return true;
                }

                //user has saved their changes to the image, so reset the editFlag
                editFlag = false;

                //if the user ends up saving return false
                return false;
            }
            //if the user selects No, return false
            return false;
        }

        //if the image has been edited in any way, run ImageEdited()
        private bool CheckForImageChanges()
        {
            if (editFlag == true)
            {
                return ImageEdited();
            }

            return false;
        }

        #endregion

        #region NotateXpress Event Handlers

        //if an annotation is added, turn on the editFlag
        private void notateXpress1_AnnotationAdded(object sender, AnnotationAddedEventArgs e)
        {
            AnnotationUpdate();
            AnnotateToolbarUpdate(PointerNXButton.Name, AnnotationTool.PointerTool);
        }

        //if an annotation is deleted, turn on the editFlag
        private void notateXpress1_AnnotationDeleted(object sender, AnnotationDeletedEventArgs e)
        {
            AnnotationUpdate();
        }

        //if an annotation is moved, turn on the editFlag
        private void notateXpress1_AnnotationMoved(object sender, AnnotationMovedEventArgs e)
        {           
            AnnotationUpdate();
        }

        #endregion

        #region NotateXpress Methods

        private void AnnotationUpdate()
        {
            try
            {
                if (formClosing == false && imageRefresh == false && annotationUpdating == false)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.AllowUpdate = false;

                        if (pageCount > 1)
                        {
                            //add the ImageX to memory so edits such as cut can be retrieved on the fly
                            ImageMemoryAdd();

                            ImageUpdateViewMemoryOrFile();

                            if (PDFOpened == false)
                            {
                                layerData.RemoveAt(pageNumber - 1);
                                layerData.Insert(pageNumber - 1, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                            }
                            else
                            {
                                layerData.RemoveAt(PDFPageNumber);
                                layerData.Insert(PDFPageNumber, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                            }
                        }

                        imageXView.AllowUpdate = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void UpdateTextAnnotationSize(string text, ref Font fontUsed, float size)
        {
            try
            {
                using (Bitmap b = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                {
                    using (Graphics g = Graphics.FromImage(b))
                    {
                        SizeF stringSize;
                        float currentSize;

                        //measure size of stamp text
                        stringSize = g.MeasureString(text, fontUsed);

                        if (imageXView.Image != null)
                        {
                            //if the stamp text size is bigger then half of the current image we want to resize it so it will fit
                            if (((int)stringSize.Width > widthView / 2)
                                && ((int)stringSize.Height > heightView / 2))
                            {
                                do
                                {
                                    currentSize = size;

                                    if (currentSize < 2)
                                        break;

                                    fontUsed = new Font(FontFamily.GenericMonospace, currentSize / 2, FontStyle.Bold);

                                    stringSize = g.MeasureString(text, fontUsed);

                                    size = fontUsed.SizeInPoints;

                                } while ((((int)stringSize.Width > widthView / 2)
                                        && ((int)stringSize.Height > heightView / 2)));
                            }
                            else if ((((int)stringSize.Width < widthView / 12)
                                        && ((int)stringSize.Height < heightView / 12)))
                            {
                                do
                                {
                                    currentSize = size;

                                    fontUsed = new Font(FontFamily.GenericMonospace, (float)(currentSize * 1.25), FontStyle.Bold);

                                    stringSize = g.MeasureString(text, fontUsed);

                                    size = fontUsed.SizeInPoints;
                                }
                                while ((((int)stringSize.Width < widthView / 12)
                                        && ((int)stringSize.Height < heightView / 12)));
                            }
                        }
                    }
                }             
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        //ImageTool and StampTool annotations appear at a preset size, depending on the image size loaded we will want to
        //control this so the annotation fits
        private void UpdateAnnotationSize()
        {
            Font annotationFont = null;

            try
            {
                //reset Stamp Text size
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = new Font(FontFamily.GenericMonospace, 23, FontStyle.Bold);

                annotationFont = notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont;
                float annotationSize = notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont.SizeInPoints;

                UpdateTextAnnotationSize("Stamp by NotateXpress 9.0", ref annotationFont, annotationSize);
                notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = annotationFont;

                annotationFont = notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.TextFont;
                annotationSize = notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.TextFont.SizeInPoints;
                UpdateTextAnnotationSize("Button", ref annotationFont, annotationSize);
                notateXpress1.ToolbarDefaults.ButtonToolbarDefaults.TextFont = annotationFont;

                annotationFont = notateXpress1.ToolbarDefaults.RulerToolbarDefaults.TextFont;
                annotationSize = notateXpress1.ToolbarDefaults.RulerToolbarDefaults.TextFont.SizeInPoints;
                UpdateTextAnnotationSize("1.00 inches", ref annotationFont, annotationSize);
                notateXpress1.ToolbarDefaults.RulerToolbarDefaults.TextFont = annotationFont;

                annotationFont = notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont;
                annotationSize = notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont.SizeInPoints;
                UpdateTextAnnotationSize("text", ref annotationFont, annotationSize);
                notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = annotationFont;

                try
                {
                    //resource cleanup
                    if (imageToolImage != null)
                    {
                        imageToolImage.Dispose();
                        imageToolImage = null;
                    }

                    imageToolImage = ImageX.FromFile(imagXpress1, PathImageToolImage);

                    if (imageToolImage != null)
                    {
                        PegasusImaging.WinForms.ImagXpress9.LoadOptions lo = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();

                        int width = imageToolImage.ImageXData.Width;
                        int height = imageToolImage.ImageXData.Height;

                        if (widthView != 0 && heightView != 0)
                        {
                            //resize the ImageAnnotation's Image loaded so it will fit within the ImageXView.Image
                            if ((width * 2 > widthView / 4) || (height * 2 > heightView / 4))
                            {
                                do
                                {
                                    lo.Resize = new Size(widthView / 2, height / 2);

                                    //resouce cleanup
                                    if (imageToolImage != null)
                                    {
                                        imageToolImage.Dispose();
                                        imageToolImage = null;
                                    }

                                    imageToolImage = ImageX.FromFile(imagXpress1, PathImageToolImage, lo);

                                    width = imageToolImage.ImageXData.Width;
                                    height = imageToolImage.ImageXData.Height;
                                }
                                while ((width > widthView / 2) || (height > heightView / 2));
                            }
                        }

                        IntPtr dibHandle = imageToolImage.ToHdib(true);

                        //set the new ImageTool's dibhandle
                        notateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle;

                        //resource cleanup
                        if (imageToolImage != null)
                        {
                            imageToolImage.Dispose();
                            imageToolImage = null;
                        }

                        //resource cleanup
                        Marshal.FreeHGlobal(dibHandle);
                    }
                }
                catch { }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                if (annotationFont != null)
                {
                    annotationFont.Dispose();
                    annotationFont = null;
                }
            }
        }

        //need to update the image in memory & have the current image being viewed reflect that
        private void Brand(int page)
        {
            try
            {
                for (int i = 0; i < indexImagesOpened.Count; i++)
                {
                    if ((int)indexImagesOpened[i] == page)
                    {
                        //resource cleanup, must dispose of ImageX first before assigning new ImageX
                        //to it using ImageX.Copy()
                        if (((ImageX)imagesOpened[i]) != null)
                        {
                            ((ImageX)imagesOpened[i]).Dispose();
                            imagesOpened[i] = null;
                        }

                        imagesOpened[i] = imageXView.Image.Copy();

                        break;
                    }
                }

                Paging(page);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BrandButton_Click(object sender, EventArgs e)
        {
            try
            {
                editFlag = true;

                //brand annotations on current image at 24-bit
                notateXpress1.Layers.Brand(24);

                //clear off annotations in current layer because now they've been branded in and we don't
                //need a duplicate layer of selectable annotations
                notateXpress1.Layers[0].Elements.Clear();

                if (PDFOpened == true)
                {
                    Brand(PDFPageNumber);
                }
                else
                {
                    Brand(pageNumber);
                }

                //update the image in memory because we've edited it from its original form
                ImageMemoryAdd();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        //reset all of the NotateXpress layers and elements
        private void ResetNotate()
        {
            if (notateXpress1.Layers != null)
            {
                for (int i = 0; i < notateXpress1.Layers.Count; i++)
                {
                    if (notateXpress1.Layers[i].Elements != null)
                    {
                        notateXpress1.Layers[i].Elements.Dispose();
                    }
                }
                notateXpress1.Layers.Clear();
                notateXpress1.Layers.Dispose();
            }

            layerData.Clear();
        }

        //reset NotateXpress, then add a new layer for each page of the image
        private void ResetNotate(int pageCnt)
        {
            try
            {
                ResetNotate();

                for (int i = 0; i < pageCnt; i++)
                {
                    layer = new Layer();
                    
                    notateXpress1.Layers.Clear();

                    notateXpress1.Layers.Add(layer);

                    layerData.Add(notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                }

                //select the current layer, make it visible and select the current AnnotationTool
                notateXpress1.Layers[0].Select();
                notateXpress1.Layers[0].Visible = true;
                notateXpress1.Layers[0].Toolbar.Selected = currentTool;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void UpdateNotate(int page)
        {
            try
            {
                notateXpress1.AnnotationAdded -= new NotateXpress.AnnotationAddedEventHandler(notateXpress1_AnnotationAdded);

                if (notateXpress1.Layers.Count > 0)
                {
                    //store the current AnnotationTool
                    AnnotationTool toolSelected = notateXpress1.Layers[0].Toolbar.Selected;

                    notateXpress1.Layers.Clear();

                    annotationUpdating = true;

                    notateXpress1.Layers.FromByteArray((byte[])layerData[page - 1], annotateLoadOptions);

                    //select the layer, make it visible and update the current AnnotationTool
                    notateXpress1.Layers[0].Select();
                    notateXpress1.Layers[0].Visible = true;
                    notateXpress1.Layers[0].Toolbar.Selected = toolSelected;

                    if (pageCount > 1)
                    {
                        annotationUpdating = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                notateXpress1.AnnotationAdded += new NotateXpress.AnnotationAddedEventHandler(notateXpress1_AnnotationAdded);
            }
        }

        #endregion

        #region ImageX's stored in memory, only when image is edited

        //if the image already exits in memory, update the ImageXView with it
        private bool ImageAlreadyPresentInMemoryUpdateView(int page)
        {
            for (int i = 0; i < indexImagesOpened.Count; i++)
            {
                if ((int)indexImagesOpened[i] == page)
                {
                    //resource cleanup
                    if (imageXView != null)
                    {
                        if (imageXView.Image != null)
                        {                            
                            imageXView.Image.Dispose();
                            imageXView.Image = null;
                        }
                    }

                    imageXView.Image = ((ImageX)imagesOpened[i]).Copy();

                    return true;
                }
            }

            return false;
        }

        //if the page already exists in memory, update the ImageXView from Memory, if not update the ImageXView
        //using FromFile() and add the page to memory for next time
        private void ImageUpdateViewMemoryOrFile()
        {
            bool alreadyExists = false;

            if (PDFOpened == true)
            {
                alreadyExists = ImageAlreadyPresentInMemoryUpdateView(PDFPageNumber);
            }
            else
            {
                alreadyExists = ImageAlreadyPresentInMemoryUpdateView(pageNumber);
            }

            if (alreadyExists == false)
            {
                ImageLoadIntoView();
            }
        }

        //remove ImageX from memory
        private void ImageAlreadyPresentInMemoryRemoveIt(int page)
        {
            try
            {
                for (int i = 0; i < indexImagesOpened.Count; i++)
                {
                    if ((int)indexImagesOpened[i] == page)
                    {
                        indexImagesOpened.RemoveAt(i);

                        //resource cleanup, must dispose of ImageX
                        if (((ImageX)imagesOpened[i]) != null)
                        {
                            ((ImageX)imagesOpened[i]).Dispose();
                            imagesOpened[i] = null;
                        }

                        imagesOpened.RemoveAt(i);

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        //add ImageX to memory
        private void ImageMemoryAdd()
        {
            try
            {
                int page = 0;

                if (PDFOpened == true)
                {
                    page = PDFPageNumber;

                    ImageAlreadyPresentInMemoryRemoveIt(page);
                    indexImagesOpened.Add(page);

                    page += 1;
                }
                else
                {
                    page = pageNumber;

                    ImageAlreadyPresentInMemoryRemoveIt(page);
                    indexImagesOpened.Add(page);
                }

                if (imageXView.Image != null)
                {
                    imagesOpened.Add(imageXView.Image.Copy());
                    
                    //only update thumbnails from memory for multipage images because single-page images won't have a thumbnail
                    if (pageCount > 1)
                    {
                        notateXpress1.Layers.Brand(24);

                        ThumbnailMemoryUpdate(page, thumbnailXpressPaging, filenameOpened, loadOptions);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        //clear the ImageX's stored in memory and clear the ArrayLists associated with them
        private void ImageMemoryReset()
        {
            try
            {
                //resource cleanup, dipose of ImageX's
                foreach (ImageX img in imagesOpened)
                {
                    if (img != null)
                    {
                        img.Dispose();
                    }
                }

                //reset ArrayLists
                imagesOpened.Clear();
                indexImagesOpened.Clear();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Adjust Menu Event Handlers

        private void flipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Flip");
        }

        private void ImageProcessing(string method)
        {
            try
            {
                if (imageXView.Image.ImageXData.BitsPerPixel != 24 &&                     
                    (method == "AutoColorBalance" || method == "AutoLightness"))
                {
                    MessageBox.Show(@"Method is only for 24-bit images, the image you're trying to process is not 24-bit.", "24-bit Method Only",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    using (ProcessorForms.ProcessorForm processForm = new ProcessorForms.ProcessorForm(method))
                    {
                        if ((imageXView.Image.ImageXData.BitsPerPixel != 1) &&
                            (method == "DocumentBorderCrop" || method == "DocumentZoomSmooth"))
                        {
                            MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);

                            using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                            {
                                proc.ColorDepth(1, 0, 0);
                                processForm.CurrentImage = proc.Image.Copy();
                            }
                        }
                        else
                        {
                            processForm.CurrentImage = imageXView.Image.Copy();
                        }

                        helper.CenterScreen(processForm);

                        if (processForm.ShowDialog() == DialogResult.OK && processForm.PreviewImage != null)
                        {
                            imageXView.AllowUpdate = false;

                            //resource cleanup, need to dispose of current ImageXView.Image object first
                            if (imageXView != null)
                            {
                                if (imageXView.Image != null)
                                {
                                    imageXView.Image.Dispose();
                                    imageXView.Image = null;
                                }
                            }

                            imageXView.Image = processForm.PreviewImage;

                            //image has been edited from its original form, so set the edit flag to true
                            editFlag = true;

                            //add the ImageX to memory so edits such as cut can be retrieved on the fly
                            ImageMemoryAdd();

                            //change zoom depending on image size
                            ZoomImage();

                            ResetImageSelection();

                            ImageInfoTabsUpdate();

                            if (pageCount > 1)
                            {
                                if (PDFOpened == true)
                                {
                                    layerData.RemoveAt(PDFPageNumber);
                                    layerData.Insert(PDFPageNumber, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                                }
                                else
                                {
                                    layerData.RemoveAt(pageNumber - 1);
                                    layerData.Insert(pageNumber - 1, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                                }
                            }

                            ImageUpdateViewMemoryOrFile();

                            imageXView.AllowUpdate = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void mirrorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Mirror");
        }       

        private void rotateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.RotateForm frm = new ProcessorForms.RotateForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Alpha Menu Event Handlers

        private void alphaReduceToAlphaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.AlphaReduceForm frm = new ProcessorForms.AlphaReduceForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void alphaMergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.MergeForm frm = new ProcessorForms.MergeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void alphaFlattenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.AlphaFlattenForm frm = new ProcessorForms.AlphaFlattenForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Appearance Event Handlers

        private void ImageProcesssingHelper()
        {
            //image has been edited from its original form, so set the edit flag to true
            editFlag = true;

            //add the ImageX to memory so edits such as cut can be retrieved on the fly
            ImageMemoryAdd();

            //change zoom depending on image size
            ZoomImage();

            ResetImageSelection();

            ImageInfoTabsUpdate();

            if (pageCount > 1)
            {
                if (PDFOpened == true)
                {
                    layerData.RemoveAt(PDFPageNumber);
                    layerData.Insert(PDFPageNumber, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                }
                else
                {
                    layerData.RemoveAt(pageNumber - 1);
                    layerData.Insert(pageNumber - 1, notateXpress1.Layers.SaveToByteArray(annotateSaveOptions));
                }
            }

            ImageUpdateViewMemoryOrFile();

            imageXView.AllowUpdate = true;
        }

        private void adjustColorBalanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (imageXView.Image.ImageXData.BitsPerPixel != 24)
                {
                    MessageBox.Show(@"Method is only for 24-bit images, the image you're trying to process is not 24-bit.", "24-bit Method Only",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    using (ProcessorForms.AdjustColorBalanceForm frm = new ProcessorForms.AdjustColorBalanceForm())
                    {
                        frm.CurrentImage = imageXView.Image.Copy();

                        helper.CenterScreen(frm);

                        if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                        {
                            imageXView.AllowUpdate = false;

                            //resource cleanup, need to dispose of current ImageXView.Image object first
                            if (imageXView != null)
                            {
                                if (imageXView.Image != null)
                                {
                                    imageXView.Image.Dispose();
                                    imageXView.Image = null;
                                }
                            }

                            imageXView.Image = frm.PreviewImage;

                            ImageProcesssingHelper();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void adjustHSLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.AdjustHSLForm frm = new ProcessorForms.AdjustHSLForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void adjustRGBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.AdjustRGBForm frm = new ProcessorForms.AdjustRGBForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void autoContrastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("AutoContrast");
        }

        private void brightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.BrightnessForm frm = new ProcessorForms.BrightnessForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void contrastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.ContrastForm frm = new ProcessorForms.ContrastForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void embossToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Emboss");
        }

        private void gammaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.GammaForm frm = new ProcessorForms.GammaForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void parabolicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.ParabolicForm frm = new ProcessorForms.ParabolicForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void posterizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.PosterizeForm frm = new ProcessorForms.PosterizeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void solarizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.SolarizeForm frm = new ProcessorForms.SolarizeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Cleanup Menu Event Handlers

        private void deskewColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DeskewForm frm = new ProcessorForms.DeskewForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void despeckleColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Despeckle");
        }

        private void dilateColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Dilate");
        }

        private void erodeColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Erode");
        }

        #endregion

        #region Color Menu Event Handlers

        private void binarizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.BinarizeForm frm = new ProcessorForms.BinarizeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void colorDepthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.ColorDepthForm frm = new ProcessorForms.ColorDepthForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void negateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Negate");
        }

        private void replaceColorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.ReplaceColorsForm frm = new ProcessorForms.ReplaceColorsForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Document Editing Menu Event Handlers

        private void borderCropToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("DocumentBorderCrop");
        }

        private void blobRemovalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentBlobForm frm = new ProcessorForms.DocumentBlobForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void deskewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentDeskewForm frm = new ProcessorForms.DocumentDeskewForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void despeckleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentDespeckleForm frm = new ProcessorForms.DocumentDespeckleForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void dilateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentDilateForm frm = new ProcessorForms.DocumentDilateForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void erodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentErodeForm frm = new ProcessorForms.DocumentErodeForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void lineRemovalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentLineRemovalForm frm = new ProcessorForms.DocumentLineRemovalForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void shearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.DocumentShearForm frm = new ProcessorForms.DocumentShearForm())
                {
                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {
                        MessageBox.Show(@"Method is only for 1-bit images, the image you're trying to process is not 1-bit so it will automatically be downsampled to 1-bit on the Image Processing Form.", "1-bit Method Only",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        using (Processor proc = new Processor(imagXpress1, imageXView.Image.Copy()))
                        {
                            proc.ColorDepth(1, 0, 0);
                            frm.CurrentImage = proc.Image.Copy();
                        }
                    }
                    else
                    {
                        frm.CurrentImage = imageXView.Image.Copy();
                    }

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void smoothZoomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("DocumentZoomSmooth");
        }

        #endregion

        #region Filters Menu Event Handlers

        private void blendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.BlendForm frm = new ProcessorForms.BlendForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void blurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Blur");
        }

        private void diffuseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Diffuse");
        }

        private void motionBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.MotionBlurForm frm = new ProcessorForms.MotionBlurForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void matrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.MatrixForm frm = new ProcessorForms.MatrixForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void medianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Median");
        }

        private void sharpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.SharpenForm frm = new ProcessorForms.SharpenForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void softenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.SoftenForm frm = new ProcessorForms.SoftenForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void unsharpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.UnsharpenForm frm = new ProcessorForms.UnsharpenForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Photo Editing Menu Event Handlers

        private void adjustLightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (imageXView.Image.ImageXData.BitsPerPixel != 24)
                {
                    MessageBox.Show(@"Method is only for 24-bit images, the image you're trying to process is not 24-bit.", "24-bit Method Only",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    using (ProcessorForms.AdjustLightnessForm frm = new ProcessorForms.AdjustLightnessForm())
                    {
                        frm.CurrentImage = imageXView.Image.Copy();

                        helper.CenterScreen(frm);

                        if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                        {
                            imageXView.AllowUpdate = false;

                            //resource cleanup, need to dispose of current ImageXView.Image object first
                            if (imageXView != null)
                            {
                                if (imageXView.Image != null)
                                {
                                    imageXView.Image.Dispose();
                                    imageXView.Image = null;
                                }
                            }

                            imageXView.Image = frm.PreviewImage;

                            ImageProcesssingHelper();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void autoColorBalanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("AutoColorBalance");
        }

        private void autoLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("AutoLevel");
        }

        private void autoLightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("AutoLightness");
        }

        private void equalizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Equalize");
        }  

        private void autoRemoveRedEyeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.AutoRemoveRedEyeForm frm = new ProcessorForms.AutoRemoveRedEyeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void removeDustToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.RemoveDustForm frm = new ProcessorForms.RemoveDustForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void redEyeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.RemoveRedEyeForm frm = new ProcessorForms.RemoveRedEyeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void removeScratchesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.RemoveScratchesForm frm = new ProcessorForms.RemoveScratchesForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Sizing Menu Event Handlers

        private void autoCropToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.AutoCropForm frm = new ProcessorForms.AutoCropForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void cropToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.CropForm frm = new ProcessorForms.CropForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void cropBorderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.CropBorderForm frm = new ProcessorForms.CropBorderForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void resizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.ResizeForm frm = new ProcessorForms.ResizeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Special Effects Menu Event Handlers

        private void buttonizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.ButtonizeForm frm = new ProcessorForms.ButtonizeForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void mosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.MosaicForm frm = new ProcessorForms.MosaicForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void noiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.NoiseForm frm = new ProcessorForms.NoiseForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void outlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageProcessing("Outline");
        }

        private void perspectiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.PerspectiveForm frm = new ProcessorForms.PerspectiveForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void pinchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.PinchForm frm = new ProcessorForms.PinchForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void polynomialWarpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.PolynomialWarpForm frm = new ProcessorForms.PolynomialWarpForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void rippleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.RippleForm frm = new ProcessorForms.RippleForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void swirlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.SwirlForm frm = new ProcessorForms.SwirlForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void tileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.TileForm frm = new ProcessorForms.TileForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void twistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ProcessorForms.TwistForm frm = new ProcessorForms.TwistForm())
                {
                    frm.CurrentImage = imageXView.Image.Copy();

                    helper.CenterScreen(frm);

                    if (frm.ShowDialog() == DialogResult.OK && frm.PreviewImage != null)
                    {
                        imageXView.AllowUpdate = false;

                        //resource cleanup, need to dispose of current ImageXView.Image object first
                        if (imageXView != null)
                        {
                            if (imageXView.Image != null)
                            {
                                imageXView.Image.Dispose();
                                imageXView.Image = null;
                            }
                        }

                        imageXView.Image = frm.PreviewImage;

                        ImageProcesssingHelper();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region CAD Tab Methods

        private void CadLayersUIUpdate(string[] layerNames, bool[] layerChecked, int[] layerIndex)
        {
            try
            {
                //resource cleanup
                if (cadLayerCheckBoxes != null)
                {
                    for (int i = 0; i < cadLayerCheckBoxes.Length; i++)
                    {
                        if (cadLayerCheckBoxes[i] != null)
                        {
                            cadLayerCheckBoxes[i].Dispose();
                            cadLayerCheckBoxes[i] = null;
                        }
                    }
                }

                //get the y position of the label for the layers
                int yLocation = LayersLabel.Location.Y;

                //initialize the CheckBox array
                cadLayerCheckBoxes = new CheckBox[layerNames.Length];

                for (int i = 0; i < layerNames.Length; i++)
                {
                    //create a new checkbox, autosize to its contents, set the text, set the checked state
                    //also store the layer # in the Tag property
                    cadLayerCheckBoxes[i] = new CheckBox();

                    cadLayerCheckBoxes[i].AutoSize = true;
                    cadLayerCheckBoxes[i].Text = layerNames[i];
                    cadLayerCheckBoxes[i].Tag = layerIndex[i];
                    cadLayerCheckBoxes[i].Checked = layerChecked[i];

                    yLocation += cadLayerCheckBoxes[i].Height;

                    cadLayerCheckBoxes[i].Location = new Point(LayersLabel.Location.X, yLocation);

                    //add the checkbox
                    TabRoll.TabPages[4].Controls.Add(cadLayerCheckBoxes[i]);

                    //add an event handler for the checkbox
                    cadLayerCheckBoxes[i].CheckedChanged += new EventHandler(CadLayerCheckBoxes_CheckedChanged);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ResetCadTab()
        {
            try
            {
                //remove the CAD tab
                if (TabRoll.TabPages.Count == 5)
                    TabRoll.TabPages.RemoveAt(4);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region CAD Tab Event Handlers & Methods

        private void CadLayerCheckBoxes_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //toggle the Cad layer
                loadOptions.Cad.CadData.Layer((int)((CheckBox)sender).Tag).LayerOn = ((CheckBox)sender).Checked;

                Paging(pageNumber);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void CadLayerUIHelper(bool onOff)
        {
            for (int i = 0; i < cadLayerCheckBoxes.Length; i++)
            {
                cadLayerCheckBoxes[i].Checked = onOff;
                loadOptions.Cad.CadData.Layer(i).LayerOn = onOff;
            }
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            if (SelectButton.Text == "Select All")
            {
                CadLayerUIHelper(true);

                SelectButton.Text = "Deselect All";
            }
            else
            {
                CadLayerUIHelper(false);

                SelectButton.Text = "Select All";
            }
        }

        #endregion

        #region View Tab Event Handlers

        private void BrightBar_Scroll(object sender, EventArgs e)
        {
            try
            {
                if (grayComboBox.SelectedIndex == 0)
                {
                    imageXView.Brightness = BrightBar.Value;
                }
                else
                {
                    imageXView.Medical.WindowWidth = BrightBar.Value;
                }

                BrightValueLabel.Text = BrightBar.Value.ToString();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ConBar_Scroll(object sender, EventArgs e)
        {
            try
            {
                if (grayComboBox.SelectedIndex == 0)
                {
                    imageXView.Contrast = ConBar.Value;
                }
                else
                {
                    imageXView.Medical.WindowCenter = ConBar.Value;
                }

                ConValueLabel.Text = ConBar.Value.ToString();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            try
            {
                ConBar.Value = 0;
                BrightBar.Value = 0;
                ConValueLabel.Text = "0";
                BrightValueLabel.Text = "0";
                imageXView.Contrast = 0;
                imageXView.Brightness = 0;
                imageXView.Medical.WindowCenter = 0;
                imageXView.Medical.WindowWidth = 0;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void monitorButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.InitialDirectory = commonImagePath;
                    openDialog.Filter = "All Supported Profiles (*.ICM, *.ICC)|*.icm;*.icc|All Files (*.*)|*.*";
                    openDialog.FilterIndex = 0;
                    openDialog.Title = "Open a Monitor Profile";

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        imageXView.MonitorProfileName = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void printerProfileButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.InitialDirectory = commonImagePath;
                    openDialog.Filter = "All Supported Profiles (*.ICM, *.ICC)|*.icm;*.icc|All Files (*.*)|*.*";
                    openDialog.FilterIndex = 0;
                    openDialog.Title = "Open a Printer Profile";

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        imageXView.PrinterProfileName = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void targetButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.InitialDirectory = commonImagePath;
                    openDialog.Filter = "All Supported Profiles (*.ICM, *.ICC)|*.icm;*.icc|All Files (*.*)|*.*";
                    openDialog.FilterIndex = 0;
                    openDialog.Title = "Open a Target Profile";

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        imageXView.TargetProfileName = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void PaletteFileButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.InitialDirectory = commonImagePath;
                    openDialog.Filter = "PAL (*.PAL)|*.PAL|All Files (*.*)|*.*";
                    openDialog.FilterIndex = 0;
                    openDialog.Title = "Open a Palette File";

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        imageXView.PaletteFilename = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void antiCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.Antialias = antiCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void smoothCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.Smoothing = smoothCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void alphaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.AlphaBlend = alphaCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ditherCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.Dithered = ditherCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void progressiveCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.Progressive = progressiveCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void preserveCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.PreserveBlack = preserveCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void slopeNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.Medical.RescaleSlope = (double)slopeNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void interceptNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.Medical.RescaleIntercept = (double)interceptNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void VerticalComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.AlignVertical = (AlignVertical)VerticalComboBox.SelectedIndex;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HorzComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.AlignHorizontal = (AlignHorizontal)HorzComboBox.SelectedIndex;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void grayComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (grayComboBox.SelectedIndex == 1)
                {
                    //medical viewing only applies to grayscale images
                    if (bppView <= 16 && bppView >= 8)
                    {
                        ContrastLabel.Text = "Window Center:";
                        BrightnessLabel.Text = "Window Width:";

                        imageXView.GrayMode = GrayMode.Medical;

                        imageXView.Medical.ValuesOfInterestLookupTable.Enabled = false;
                        imageXView.Medical.ModalityLookupTable.Enabled = false;

                        slopeNumericUpDown.Enabled = true;
                        interceptNumericUpDown.Enabled = true;

                        ConBar.Maximum = (int)Math.Pow(2, bppView);
                        ConBar.Minimum = 0;
                        BrightBar.Maximum = (int)Math.Pow(2, bppView);
                        BrightBar.Minimum = 0;

                        ConBar.Value = (int)imageXView.Medical.WindowCenter;
                        BrightBar.Value = (int)imageXView.Medical.WindowWidth;
                        ConValueLabel.Text = imageXView.Medical.WindowCenter.ToString();
                        BrightValueLabel.Text = imageXView.Medical.WindowWidth.ToString();
                    }
                    else
                    {
                        grayComboBox.SelectedIndex = 0;

                        MessageBox.Show("This feature only applies to grayscale images.", "Image is not Grayscale", MessageBoxButtons.OK, MessageBoxIcon.Error);            
                    }
                }
                else
                {
                    ContrastLabel.Text = "Contrast:";
                    BrightnessLabel.Text = "Brightness:";

                    imageXView.GrayMode = GrayMode.Standard;

                    slopeNumericUpDown.Enabled = false;
                    interceptNumericUpDown.Enabled = false;

                    ConBar.Minimum = -100;
                    ConBar.Maximum = 100;
                    BrightBar.Minimum = -100;
                    BrightBar.Maximum = 100;

                    ConBar.Value = imageXView.Contrast;
                    BrightBar.Value = imageXView.Brightness;
                    ConValueLabel.Text = imageXView.Contrast.ToString();
                    BrightValueLabel.Text = imageXView.Brightness.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void paletteComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.PaletteType = (PaletteType)paletteComboBox.SelectedIndex;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void displayComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.DisplayMode = (DisplayMode)displayComboBox.SelectedIndex;

                if (displayComboBox.SelectedIndex == 1)
                {
                    paletteComboBox.Enabled = true;
                }
                else
                {
                    paletteComboBox.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void renderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.ColorRenderIntent = (RenderIntent)renderComboBox.SelectedIndex;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void aspectXNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //ImageXView needs to be updated first to get the correct aspect ratio
                imageXView.Update();
                imageXView.AspectX = (int)aspectXNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void aspectYNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //ImageXView needs to be updated first to get the correct aspect ratio
                imageXView.Update();
                imageXView.AspectY = (int)aspectYNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void zoomNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.AutoResize = AutoResizeType.CropImage;
                imageXView.ZoomFactor = (double)zoomNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void backColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                backColorDialog.Color = backColor;

                if (backColorDialog.ShowDialog() == DialogResult.OK)
                {
                    imageXView.BackColor = backColorDialog.Color;

                    backColor = backColorDialog.Color;

                    backColorButton.BackColor = backColor;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void magWidthNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.ToolSetAttribute(Tool.Magnify, ToolAttribute.MagWidth, (int)magWidthNumericUpDown.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void magHeightNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.ToolSetAttribute(Tool.Magnify, ToolAttribute.MagHeight, (int)magHeightNumericUpDown.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void magFactorNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.ToolSetAttribute(Tool.Magnify, ToolAttribute.MagFactor, (int)magFactorNumericUpDown.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void zoomInNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.ToolSetAttribute(Tool.ZoomIn, ToolAttribute.ZoomInFactor, (int)zoomInNumericUpDown.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void zoomOutNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                imageXView.ToolSetAttribute(Tool.ZoomOut, ToolAttribute.ZoomOutFactor, (int)zoomOutNumericUpDown.Value);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region TabRoll Event Handlers

        private void TabRoll_Click(object sender, EventArgs e)
        {
            if (TabRoll.SelectedIndex == 0)
            {
                dataGridImageInfo.AutoResizeColumn(1);
            }

            ImageInfoTabsUpdate();
        }

        #endregion

        #region Thumbnail Tab Event Handlers

        private void thumbComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (thumbComboBox.SelectedIndex == 0)
                {
                    thumbChosen = thumbnailXpressDrillDown;
                }
                else
                {
                    thumbChosen = thumbnailXpressPaging;
                }

                UpdateThumbnailUI();
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void cellWidthNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.CellWidth = (int)cellWidthNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void cellHeightNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.CellHeight = (int)cellHeightNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void borderWidthNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.CellBorderWidth = (int)borderWidthNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SpacingButton_Click(object sender, EventArgs e)
        {
            try
            {
                spacingColorDialog.Color = spacingColor;

                if (spacingColorDialog.ShowDialog() == DialogResult.OK)
                {
                    thumbChosen.CellSpacingColor = spacingColorDialog.Color;

                    spacingColor = spacingColorDialog.Color;

                    SpacingButton.BackColor = spacingColor;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void borderButton_Click(object sender, EventArgs e)
        {
            try
            {
                borderColorDialog.Color = borderColor;

                if (borderColorDialog.ShowDialog() == DialogResult.OK)
                {
                    thumbChosen.CellBorderColor = borderColorDialog.Color;

                    borderColor = borderColorDialog.Color;

                    borderButton.BackColor = borderColor;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void horzSpacingNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.CellHorizontalSpacing = (int)horzSpacingNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void vertSpacingNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.CellVerticalSpacing = (int)vertSpacingNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void descriptorHorzComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (descriptorVertComboBox.SelectedIndex)
                {
                    case 0:
                        {
                            alignV = DescriptorAlignments.AlignTop;
                            break;
                        }
                    case 1:
                        {
                            alignV = DescriptorAlignments.AlignVCenter;
                            break;
                        }
                    case 2:
                        {
                            alignV = DescriptorAlignments.AlignBottom;
                            break;
                        }
                    default:
                        {
                            alignV = DescriptorAlignments.AlignBottom;
                            break;
                        }
                }

                //if Vertical Alignment is Center, can't set Horizontal Alignment
                if (alignV == DescriptorAlignments.AlignVCenter)
                {
                    descriptorHorzComboBox.Enabled = false;
                }
                else
                {
                    descriptorHorzComboBox.Enabled = true;
                }

                if (alignV == DescriptorAlignments.AlignTop || alignV == DescriptorAlignments.AlignBottom)
                {
                    switch (descriptorHorzComboBox.SelectedIndex)
                    {
                        case 0:
                            {
                                alignH = DescriptorAlignments.AlignLeft;
                                break;
                            }
                        case 1:
                            {
                                alignH = DescriptorAlignments.AlignCenter;
                                break;
                            }
                        case 2:
                            {
                                alignH = DescriptorAlignments.AlignRight;
                                break;
                            }
                        default:
                            {
                                alignH = DescriptorAlignments.AlignCenter;
                                break;
                            }
                    }

                    //set both Vertical and Horizontal Alignment
                    thumbChosen.DescriptorAlignment = (DescriptorAlignments)(alignH | alignV);
                }
                else
                {
                    //if Vertical Alignment is Center, can't set Horizontal Alignment
                    thumbChosen.DescriptorAlignment = alignV;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void descriptorDisplayComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (descriptorDisplayComboBox.SelectedIndex)
                {
                    case 0:
                        {
                            descriptorDisplay = DescriptorDisplayMethods.None;
                            break;
                        }
                    case 1:
                        {
                            descriptorDisplay = DescriptorDisplayMethods.Default;
                            break;
                        }
                    case 2:
                        {
                            descriptorDisplay = DescriptorDisplayMethods.LongFilename;
                            break;
                        }
                    case 3:
                        {
                            descriptorDisplay = DescriptorDisplayMethods.ShortFilename;
                            break;
                        }
                    case 4:
                        {
                            descriptorDisplay = DescriptorDisplayMethods.IncludePage;
                            break;
                        }
                    case 5:
                        {
                            descriptorDisplay = DescriptorDisplayMethods.Custom;
                            break;
                        }
                }

                thumbChosen.DescriptorDisplayMethod = descriptorDisplay;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void fontButton_Click(object sender, EventArgs e)
        {
            try
            {
                fontDialog.Font = font;

                if (fontDialog.ShowDialog() == DialogResult.OK)
                {
                    thumbChosen.Font = fontDialog.Font;

                    font = thumbChosen.Font;

                    fontButton.Text = font.Name + ", " + thumbChosen.Font.Style + ", " + thumbChosen.Font.Size;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void textBackColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                textBackColorDialog.Color = textBackColor;

                if (textBackColorDialog.ShowDialog() == DialogResult.OK)
                {
                    thumbChosen.TextBackColor = textBackColorDialog.Color;

                    textBackColor = thumbChosen.TextBackColor;

                    textBackColorButton.BackColor = textBackColor;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void placeHoldersCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.ShowImagePlaceholders = placeHoldersCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bitDepthComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (bitDepthComboBox.SelectedIndex == 0)
                {
                    thumbChosen.MaximumThumbnailBitDepth = 24;
                }
                else
                {
                    thumbChosen.MaximumThumbnailBitDepth = 8;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void scrollDirectionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.ScrollDirection = (ScrollDirection)scrollDirectionComboBox.SelectedIndex;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void cameraRawCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.CameraRaw = cameraRawCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void preserveBlackCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.PreserveBlack = preserveBlackCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void selectBackColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                selectBackColorDialog.Color = selectBackColor;

                if (selectBackColorDialog.ShowDialog() == DialogResult.OK)
                {
                    thumbChosen.SelectBackColor = selectBackColorDialog.Color;

                    selectBackColor = thumbChosen.SelectBackColor;

                    selectBackColorButton.BackColor = selectBackColor;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void threadStartNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.ThreadStartThreshold = (int)threadStartNumericUpDown.Value;

                threadHungNumericUpDown.Minimum = 2 * threadStartNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void threadHungNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.ThreadHungThreshold = (int)threadHungNumericUpDown.Value;

                threadStartNumericUpDown.Maximum = threadHungNumericUpDown.Value / 2;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void threadCountNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.MaximumThreadCount = (int)threadCountNumericUpDown.Value;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void errorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.ErrorAction = (ErrorAction)errorComboBox.SelectedIndex;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void hourGlassCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                thumbChosen.ShowHourglass = hourGlassCheckBox.Checked;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Thumbnail Tab Methods

        private void UpdateThumbnailUI()
        {
            cellWidthNumericUpDown.Value = thumbChosen.CellWidth;
            cellHeightNumericUpDown.Value = thumbChosen.CellHeight;
            SpacingButton.BackColor = thumbChosen.CellSpacingColor;
            spacingColor = SpacingButton.BackColor;
            vertSpacingNumericUpDown.Value = thumbChosen.CellVerticalSpacing;
            horzSpacingNumericUpDown.Value = thumbChosen.CellHorizontalSpacing;
            borderWidthNumericUpDown.Value = thumbChosen.CellBorderWidth;
            borderButton.BackColor = thumbChosen.CellBorderColor;
            borderColor = borderButton.BackColor;

            switch ((int)thumbChosen.DescriptorDisplayMethod)
            {
                case 0:
                    {
                        descriptorDisplayComboBox.SelectedIndex = 0;
                        break;
                    }
                case 1:
                    {
                        descriptorDisplayComboBox.SelectedIndex = 1;
                        break;
                    }
                case 2:
                    {
                        descriptorDisplayComboBox.SelectedIndex = 2;
                        break;
                    }
                case 4:
                    {
                        descriptorDisplayComboBox.SelectedIndex = 3;
                        break;
                    }
                case 8:
                    {
                        descriptorDisplayComboBox.SelectedIndex = 4;
                        break;
                    }
                case 16:
                    {
                        descriptorDisplayComboBox.SelectedIndex = 5;
                        break;
                    }
            }

            if ((DescriptorAlignments.AlignBottom & thumbChosen.DescriptorAlignment) == DescriptorAlignments.AlignBottom)
            {
                descriptorVertComboBox.SelectedIndex = 2;
            }
            else if (thumbChosen.DescriptorAlignment == DescriptorAlignments.AlignVCenter)
            {
                descriptorVertComboBox.SelectedIndex = 1;
            }
            else
            {
                descriptorVertComboBox.SelectedIndex = 0;
            }

            if ((DescriptorAlignments.AlignLeft & thumbChosen.DescriptorAlignment) == DescriptorAlignments.AlignLeft)
            {
                descriptorHorzComboBox.SelectedIndex = 0;
            }
            else if ((DescriptorAlignments.AlignCenter & thumbChosen.DescriptorAlignment) == DescriptorAlignments.AlignCenter)
            {
                descriptorHorzComboBox.SelectedIndex = 1;
            }
            else
            {
                descriptorHorzComboBox.SelectedIndex = 2;
            }

            fontButton.Text = thumbChosen.Font.Name + ", " + thumbChosen.Font.Style + ", " + thumbChosen.Font.Size;
            font = thumbChosen.Font;

            textBackColorButton.BackColor = thumbChosen.TextBackColor;
            textBackColor = textBackColorButton.BackColor;

            placeHoldersCheckBox.Checked = thumbChosen.ShowImagePlaceholders;

            if (thumbChosen.MaximumThumbnailBitDepth == 24)
            {
                bitDepthComboBox.SelectedIndex = 0;
            }
            else
            {
                bitDepthComboBox.SelectedIndex = 1;
            }

            scrollDirectionComboBox.SelectedIndex = (int)thumbChosen.ScrollDirection;

            cameraRawCheckBox.Checked = thumbChosen.CameraRaw;

            preserveBlackCheckBox.Checked = thumbChosen.PreserveBlack;

            selectBackColorButton.BackColor = thumbChosen.SelectBackColor;
            selectBackColor = selectBackColorButton.BackColor;

            threadCountNumericUpDown.Value = thumbChosen.MaximumThreadCount;

            errorComboBox.SelectedIndex = (int)thumbChosen.ErrorAction;

            hourGlassCheckBox.Checked = thumbChosen.ShowHourglass;

            threadStartNumericUpDown.Value = thumbChosen.ThreadStartThreshold;
            threadHungNumericUpDown.Value = thumbChosen.ThreadHungThreshold;

            threadHungNumericUpDown.Minimum = 2 * threadStartNumericUpDown.Value;
            threadStartNumericUpDown.Maximum = threadHungNumericUpDown.Value / 2;
        }

        #endregion

        #region Color Helper Function

        private int GetRGBColor(Color color)
        {
            return ((color.B << 16) | (color.G << 8) | color.R);
        }

        #endregion

        #region Error Handler Methods

        private void ExceptionHandler(string error, string methodName)
        {            
            //want to enable painting of ImageXView when this point is reached
            imageXView.AllowUpdate = true;
            imageXViewHistogram.AllowUpdate = true;

            if (loadOptions != null)
                loadOptions.LoadMode = LoadMode.Normal;
            
            annotationUpdating = false;

            editFlag = false;

            MessageBox.Show("An error occured in the Demo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //when an image opened can't be opened for some reason and an exception is issued, we want to close
        //the image to reset the UI and remove its thumbnail and filename from the RecentImages list
        private void ErrorHandler(string error, string methodName)
        {
            editFlag = false;

            imageXView.AllowUpdate = true;
            imageXViewHistogram.AllowUpdate = true;

            if (loadOptions != null)
                loadOptions.LoadMode = LoadMode.Normal;

            for (int i = 0; i < recentImages.Count; i++)
            {
                if (recentImages[i].ToString() == filenameOpened)
                {
                    recentImages.RemoveAt(i);
                    recentImagesIdentify.RemoveAt(i);

                    break;
                }
            }

            CloseImage();            

            MessageBox.Show("An error occured in the Demo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

        #region Sample Image Loading Helper Method

        private void sampleImage(string sampleImagePath)
        {
            //check if image has been edited in anyway whatsoever before continuing
            if (CheckForImageChanges() == false)
            {
                editFlag = false;

                SampleImageLoad = true;

                if (File.Exists(sampleImagePath) == true)
                {
                    filenameOpened = sampleImagePath;

                    OpenFileDialog(false);
                }
                else
                {
                    MessageBox.Show("Sample image could not be found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion

        #region Sample Images Menu Event Handlers

        private void blobRemoval1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Blob1ImagePath);
        }

        private void blobRemoval2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Blob2ImagePath);
        }

        private void borderCrop1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Border1ImagePath);
        }

        private void borderCrop2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Border2ImagePath);
        }

        private void deskew1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Deskew1ImagePath);
        }

        private void deskew2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Deskew2ImagePath);
        }

        private void deskew3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Deskew3ImagePath);
        }

        private void despeckle1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Despeckle1ImagePath);
        }

        private void despeckle2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Despeckle2ImagePath);
        }

        private void despeckle3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Despeckle3ImagePath);
        }

        private void dilateToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            sampleImage(DilateImagePath);
        }

        private void erodeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            sampleImage(ErodeImagePath);
        }

        private void negateToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            sampleImage(NegateImagePath);
        }

        private void removeLines1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Lines1ImagePath);
        }

        private void removeLines2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Lines2ImagePath);
        }

        private void shearToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            sampleImage(ShearImagePath);
        }

        private void smoothZoomToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            sampleImage(SmoothImagePath);
        }

        private void effects1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Effects1ImagePath);
        }

        private void effects2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Effects2ImagePath);
        }

        private void effects3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Effects3ImagePath);
        }

        private void alpha1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(Alpha1ImagePath);
        }

        private void alpha2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(Alpha2ImagePath);
        }

        private void alpha3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(Alpha3ImagePath);
        }

        private void autoBalance1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(AutoBalance1ImagePath);
        }

        private void autoBalance2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(AutoBalance2ImagePath);
        }

        private void autoContrastToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            sampleImage(AutoContrastImagePath);
        }

        private void autoLevel2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(AutoLevel2ImagePath);
        }

        private void autoLevel3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(AutoLevel3ImagePath);
        }

        private void autoLightness2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(AutoLightness2ImagePath);
        }

        private void cropToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            sampleImage(CropImagePath);
        }

        private void rotate1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Rotate1ImagePath);
        }

        private void rotate2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Rotate2ImagePath);
        }

        private void redEye1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(RedEye1ImagePath);
        }

        private void redEye2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(RedEye2ImagePath);
        }

        private void redEye3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(RedEye3ImagePath);
        }

        private void redEye4ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(RedEye4ImagePath);
        }

        private void scratchedToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(ScratchedImagePath);
        }

        private void removeDust1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Dust1ImagePath);
        }

        private void removeDust2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Dust2ImagePath);
        }

        private void sharpen1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Sharpen1ImagePath);
        }

        private void sharpen2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(Sharpen2ImagePath);
        }

        private void eXIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(ExifImagePath);
        }

        private void multiPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sampleImage(MultiImagePath);
        }

        private void binarizeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(BinarizeImagePath);
        }

        private void tagsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            sampleImage(TagsImagePath);
        }

        #endregion

        #region Color Tab Event Handlers

        private void paletteButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (PaletteForm paletteForm = new PaletteForm())
                {
                    helper.CenterScreen(paletteForm);

                    paletteForm.palette = imageXView.Image.Palette;

                    if (paletteForm.ShowDialog() == DialogResult.OK)
                    {
                        imageXView.Image.Palette = paletteForm.palette;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void comboBoxHistogram_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (imageXView.Image != null)
            {
                switch (comboBoxHistogram.SelectedIndex)
                {
                    case 0:
                        {
                            DrawHistogram(redData, null, null, redPen, null, null);
                            break;
                        }
                    case 1:
                        {
                            DrawHistogram(null, greenData, null, null, greenPen, null);
                            break;
                        }
                    case 2:
                        {
                            DrawHistogram(null, null, blueData, null, null, bluePen);
                            break;
                        }
                    case 3:
                        {
                            DrawHistogram(redData, greenData, blueData, blackPen, blackPen, blackPen);
                            break;
                        }
                }
            }
        }

        private void blackRadioButton_Click(object sender, EventArgs e)
        {
            CMYKComponent();
            ColorComponent(blackImage);
            UpdateRadioColorButtons("CMYK");
        }

        private void yellowRadioButton_Click(object sender, EventArgs e)
        {
            CMYKComponent();
            ColorComponent(yellowImage);
            UpdateRadioColorButtons("CMYK");
        }

        private void magentaRadioButton_Click(object sender, EventArgs e)
        {
            CMYKComponent();
            ColorComponent(magentaImage);
            UpdateRadioColorButtons("CMYK");
        }

        private void cyanRadioButton_Click(object sender, EventArgs e)
        {
            CMYKComponent();
            ColorComponent(cyanImage);
            UpdateRadioColorButtons("CMYK");
        }

        private void luminanceRadioButton_Click(object sender, EventArgs e)
        {
            HSLComponent();
            ColorComponent(luminanceImage);
            UpdateRadioColorButtons("HSL");
        }

        private void saturationRadioButton_Click(object sender, EventArgs e)
        {
            HSLComponent();
            ColorComponent(saturationImage);
            UpdateRadioColorButtons("HSL");
        }

        private void hueRadioButton_Click(object sender, EventArgs e)
        {
            HSLComponent();
            ColorComponent(hueImage);
            UpdateRadioColorButtons("HSL");
        }

        private void alphaRadioButton_Click(object sender, EventArgs e)
        {
            RGBComponent();
            ColorComponent(alphaImage);
            UpdateRadioColorButtons("RGBA");
        }

        private void blueRadioButton_Click(object sender, EventArgs e)
        {
            RGBComponent();
            ColorComponent(blueImage);
            UpdateRadioColorButtons("RGBA");
        }

        private void greenRadioButton_Click(object sender, EventArgs e)
        {
            RGBComponent();
            ColorComponent(greenImage);
            UpdateRadioColorButtons("RGBA");
        }

        private void redRadioButton_Click(object sender, EventArgs e)
        {
            RGBComponent();
            ColorComponent(redImage);
            UpdateRadioColorButtons("RGBA");
        }

        private void resetComponentsButton_Click(object sender, EventArgs e)
        {
            ClearColorRadioButtons();

            ImageUpdate();

            ResetImageSelection();
            
            editFlag = false;
        }

        #endregion

        #region Color Tab Methods

        private void RGBComponent()
        {
            try
            {
                //resource cleanup
                if (redImage != null)
                {
                    redImage.Dispose();
                    redImage = null;
                }

                if (greenImage != null)
                {
                    greenImage.Dispose();
                    greenImage = null;
                }

                if (blueImage != null)
                {
                    blueImage.Dispose();
                    blueImage = null;
                }

                if (alphaImage != null)
                {
                    alphaImage.Dispose();
                    alphaImage = null;
                }

                //want to disable painting of ImageXView to prevent flickering
                imageXView.AllowUpdate = false;

                //resource cleanup, ImageUpdateViewMemoryOrFile() or ImageLoadIntoView() will assign the imageXView.Image object
                //a new ImageX that's created so we must dispose of the ImageX object first
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                }
     
                if (editFlag == true)
                {
                    ImageUpdateViewMemoryOrFile();
                }
                else
                {
                    ImageLoadIntoView();
                }

                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }

                processor1.Image = imageXView.Image;

                processor1.ColorSeparation(SeparationType.Rgba, out redImage, out greenImage, out blueImage, out alphaImage);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void HSLComponent()
        {
            Bitmap placeholder = null;

            try
            {
                //resource cleanup
                if (hueImage != null)
                {
                    hueImage.Dispose();
                    hueImage = null;
                }

                if (saturationImage != null)
                {
                    saturationImage.Dispose();
                    saturationImage = null;
                }

                if (luminanceImage != null)
                {
                    luminanceImage.Dispose();
                    luminanceImage = null;
                }

                //want to disable painting of ImageXView to prevent flickering
                imageXView.AllowUpdate = false;

                //resource cleanup, ImageUpdateViewMemoryOrFile() or ImageLoadIntoView() will assign the imageXView.Image object
                //a new ImageX that's created so we must dispose of the ImageX object first
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                }

                if (editFlag == true)
                {
                    ImageUpdateViewMemoryOrFile();
                }
                else
                {
                    ImageLoadIntoView();
                }

                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }

                processor1.Image = imageXView.Image;

                processor1.ColorSeparation(SeparationType.Hsl, out hueImage, out saturationImage, out luminanceImage, out placeholder);                
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
            finally
            {
                //resource cleanup
                if (placeholder != null)
                {
                    placeholder.Dispose();
                    placeholder = null;
                }
            }
        }

        private void CMYKComponent()
        {
            try
            {

                if (cyanImage != null)
                {
                    cyanImage.Dispose();
                    cyanImage = null;
                }

                if (magentaImage != null)
                {
                    magentaImage.Dispose();
                    magentaImage = null;
                }

                if (yellowImage != null)
                {
                    yellowImage.Dispose();
                    yellowImage = null;
                }

                if (blackImage != null)
                {
                    blackImage.Dispose();
                    blackImage = null;
                }

                //want to disable painting of ImageXView to prevent flickering
                imageXView.AllowUpdate = false;

                //resource cleanup, ImageUpdateViewMemoryOrFile() or ImageLoadIntoView() will assign the imageXView.Image object
                //a new ImageX that's created so we must dispose of the ImageX object first
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                }

                if (editFlag == true)
                {
                    ImageUpdateViewMemoryOrFile();
                }
                else
                {
                    ImageLoadIntoView();
                }

                //resource cleanup
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }

                processor1.Image = imageXView.Image;

                processor1.ColorSeparation(SeparationType.Cmyk, out cyanImage, out magentaImage, out yellowImage, out blackImage);
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ColorComponent(Bitmap colorImage)
        {
            try
            {
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        //resource cleanup, need to dispose because below ImageX.FromBitmap() call creates a new ImageX object
                        imageXView.Image.Dispose();
                        imageXView.Image = null;

                        imageXView.Image = ImageX.FromBitmap(imagXpress1, colorImage);

                        //resource cleanup
                        if (colorImage != null)
                        {
                            colorImage.Dispose();
                            colorImage = null;
                        }

                        //want to allow painting of ImageXView to resume
                        imageXView.AllowUpdate = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void UpdateRadioColorButtons(string radioGroupName)
        {
            //want to allow only 1 groupbox to be selected at once
            if (radioGroupName == "RGBA")
            {
                foreach (RadioButton radioButton in radioHSLButtons)
                {
                    radioButton.Checked = false;
                }
                foreach (RadioButton radioButton in radioCMYKButtons)
                {
                    radioButton.Checked = false;
                }
            }
            else if (radioGroupName == "HSL")
            {
                foreach (RadioButton radioButton in radioRGBAButtons)
                {
                    radioButton.Checked = false;
                }
                foreach (RadioButton radioButton in radioCMYKButtons)
                {
                    radioButton.Checked = false;
                }
            }
            else
            {
                foreach (RadioButton radioButton in radioRGBAButtons)
                {
                    radioButton.Checked = false;
                }
                foreach (RadioButton radioButton in radioHSLButtons)
                {
                    radioButton.Checked = false;
                }
            }            
        }

        private void ClearColorRadioButtons()
        {
            foreach (RadioButton radioButton in radioRGBAButtons)
            {
                radioButton.Checked = false;
            }
            foreach (RadioButton radioButton in radioHSLButtons)
            {
                radioButton.Checked = false;
            }
            foreach (RadioButton radioButton in radioCMYKButtons)
            {
                radioButton.Checked = false;
            }
        }

        private void RGBPenListBuilder(ArrayList dataList, int[] data, ArrayList penList, Pen pen)
        {
            if (data != null)
                dataList.Add(data);

            if (pen != null)
                penList.Add(pen);
        }

        private void DrawHistogram(int[] data1, int[] data2, int[] data3, Pen pen1, Pen pen2, Pen pen3)
        {
            try
            {
                //want to disable painting of ImageXView to more quickly update the view
                imageXViewHistogram.AllowUpdate = false;

                //resource cleanup, because creating a new ImageX object below
                if (imageXViewHistogram != null)
                {
                    if (imageXViewHistogram.Image != null)
                    {
                        imageXViewHistogram.Image.Dispose();
                        imageXViewHistogram.Image = null;
                    }
                }

                //create a color blank image for the histogram
                imageXViewHistogram.Image = new ImageX(imagXpress1, 260, 260, 24, Color.White);

                using (Graphics graphics = imageXViewHistogram.Image.GetGraphics())
                {
                    ArrayList dataList = new ArrayList();
                    ArrayList penList = new ArrayList();

                    RGBPenListBuilder(dataList, data1, penList, pen1);
                    RGBPenListBuilder(dataList, data2, penList, pen2);
                    RGBPenListBuilder(dataList, data3, penList, pen3);

                    int[] data;
                    Pen pen = null;

                    for (int j = 0; j < dataList.Count; j++)
                    {
                        data = (int[])dataList[j];

                        pen = (Pen)penList[j];

                        int max = 0;

                        //find the maximum
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i] > max)
                            {
                                max = data[i];
                            }
                        }

                        int heightHistogram = imageXViewHistogram.Image.ImageXData.Height;
                        int height = heightHistogram - 20;

                        //set the ratio so whatever the intensity of RGB it will fit in the ImageX created above nicely
                        int ratio = max / height;

                        //want to prevent divide by 0
                        if (ratio == 0)
                        {
                            ratio = 1;
                        }

                        //want to make sure histogram fits in bounds of ImageXView
                        if (height - max / ratio < 0)
                        {
                            do
                            {
                                ratio += 1;
                            }
                            while (height - max / ratio < 0);
                        }

                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i] > height)
                                data[i] = data[i] / ratio;

                            graphics.DrawLine(pen, new Point(i, heightHistogram), new Point(i, heightHistogram - data[i]));
                        }
                    }

                    imageXViewHistogram.Image.ReleaseGraphics();

                    //want to allow painting of ImageXView to resume
                    imageXViewHistogram.AllowUpdate = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion
    }
}

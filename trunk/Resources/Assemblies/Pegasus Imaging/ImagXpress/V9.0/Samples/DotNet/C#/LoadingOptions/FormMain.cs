/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace LoadingOptions
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox grpCrop;
		private System.Windows.Forms.GroupBox grpResize;
		private System.Windows.Forms.GroupBox grpRotate;
		private System.Windows.Forms.CheckBox chkCrop;
		private System.Windows.Forms.CheckBox chkResize;
		private System.Windows.Forms.CheckBox chkRotate;
		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.Label lblCropXVal;
		private System.Windows.Forms.Label lblCropYVal;
		private System.Windows.Forms.Label lblCropWidthVal;
		private System.Windows.Forms.Label lblCropHeightVal;
		private System.Windows.Forms.Label lblCropX;
		private System.Windows.Forms.Label lblCropY;
		private System.Windows.Forms.Label lblCropWidth;
		private System.Windows.Forms.Label lblCropHeight;
		private System.Windows.Forms.CheckBox chkResizeAspect;
		private System.Windows.Forms.Label lblResizeWidth;
		private System.Windows.Forms.Label lblResizeHeight;
		private System.Windows.Forms.Label lblRotateSettings;
		private System.String strCurrentImage;
		private PegasusImaging.WinForms.ImagXpress9.LoadOptions loLoadOptions;
		private System.Windows.Forms.HScrollBar scrCropX;
		private System.Windows.Forms.HScrollBar scrCropY;
		private System.Windows.Forms.HScrollBar scrCropWidth;
		private System.Windows.Forms.HScrollBar scrCropHeight;
		private System.Windows.Forms.HScrollBar scrResizeWidth;
		private System.Windows.Forms.HScrollBar scrResizeHeight;
		private System.Windows.Forms.ComboBox cmbRotate;
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem mnuFileFile;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem mnuSpacer;
		private System.Windows.Forms.MenuItem mnuFileExit;
		private System.Windows.Forms.RichTextBox lblInfo;
		private System.Windows.Forms.TextBox txtResizeWidthVal;
		private System.Windows.Forms.TextBox txtResizeHeightVal;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private System.Windows.Forms.ListBox lstStatus;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			
			//***call the Dispose method on the imagXpress1 object and the
			//*** imageXView1 object
			if (!(imagXpress1 == null)) 
			{
				imagXpress1.Dispose();
				imagXpress1 = null;
			}
			if (!(imageXView1 == null)) 
			{
				imageXView1.Dispose();
				imageXView1 = null;
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grpCrop = new System.Windows.Forms.GroupBox();
			this.lblCropHeight = new System.Windows.Forms.Label();
			this.lblCropWidth = new System.Windows.Forms.Label();
			this.lblCropY = new System.Windows.Forms.Label();
			this.lblCropX = new System.Windows.Forms.Label();
			this.lblCropHeightVal = new System.Windows.Forms.Label();
			this.lblCropWidthVal = new System.Windows.Forms.Label();
			this.lblCropYVal = new System.Windows.Forms.Label();
			this.lblCropXVal = new System.Windows.Forms.Label();
			this.scrCropHeight = new System.Windows.Forms.HScrollBar();
			this.scrCropWidth = new System.Windows.Forms.HScrollBar();
			this.scrCropY = new System.Windows.Forms.HScrollBar();
			this.scrCropX = new System.Windows.Forms.HScrollBar();
			this.chkCrop = new System.Windows.Forms.CheckBox();
			this.grpResize = new System.Windows.Forms.GroupBox();
			this.txtResizeHeightVal = new System.Windows.Forms.TextBox();
			this.txtResizeWidthVal = new System.Windows.Forms.TextBox();
			this.lblResizeHeight = new System.Windows.Forms.Label();
			this.lblResizeWidth = new System.Windows.Forms.Label();
			this.chkResizeAspect = new System.Windows.Forms.CheckBox();
			this.scrResizeHeight = new System.Windows.Forms.HScrollBar();
			this.scrResizeWidth = new System.Windows.Forms.HScrollBar();
			this.chkResize = new System.Windows.Forms.CheckBox();
			this.grpRotate = new System.Windows.Forms.GroupBox();
			this.lblRotateSettings = new System.Windows.Forms.Label();
			this.cmbRotate = new System.Windows.Forms.ComboBox();
			this.chkRotate = new System.Windows.Forms.CheckBox();
			this.lblStatus = new System.Windows.Forms.Label();
			this.mnuFile = new System.Windows.Forms.MainMenu();
			this.mnuFileFile = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.mnuSpacer = new System.Windows.Forms.MenuItem();
			this.mnuFileExit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.lblInfo = new System.Windows.Forms.RichTextBox();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.grpCrop.SuspendLayout();
			this.grpResize.SuspendLayout();
			this.grpRotate.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpCrop
			// 
			this.grpCrop.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.grpCrop.Controls.AddRange(new System.Windows.Forms.Control[] {
																				  this.lblCropHeight,
																				  this.lblCropWidth,
																				  this.lblCropY,
																				  this.lblCropX,
																				  this.lblCropHeightVal,
																				  this.lblCropWidthVal,
																				  this.lblCropYVal,
																				  this.lblCropXVal,
																				  this.scrCropHeight,
																				  this.scrCropWidth,
																				  this.scrCropY,
																				  this.scrCropX,
																				  this.chkCrop});
			this.grpCrop.Location = new System.Drawing.Point(16, 360);
			this.grpCrop.Name = "grpCrop";
			this.grpCrop.Size = new System.Drawing.Size(200, 128);
			this.grpCrop.TabIndex = 2;
			this.grpCrop.TabStop = false;
			this.grpCrop.Text = "Load Crop";
			// 
			// lblCropHeight
			// 
			this.lblCropHeight.Location = new System.Drawing.Point(8, 96);
			this.lblCropHeight.Name = "lblCropHeight";
			this.lblCropHeight.Size = new System.Drawing.Size(48, 16);
			this.lblCropHeight.TabIndex = 12;
			this.lblCropHeight.Text = "Height";
			this.lblCropHeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCropWidth
			// 
			this.lblCropWidth.Location = new System.Drawing.Point(8, 80);
			this.lblCropWidth.Name = "lblCropWidth";
			this.lblCropWidth.Size = new System.Drawing.Size(48, 16);
			this.lblCropWidth.TabIndex = 11;
			this.lblCropWidth.Text = "Width";
			this.lblCropWidth.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCropY
			// 
			this.lblCropY.Location = new System.Drawing.Point(8, 64);
			this.lblCropY.Name = "lblCropY";
			this.lblCropY.Size = new System.Drawing.Size(48, 16);
			this.lblCropY.TabIndex = 10;
			this.lblCropY.Text = "Y";
			this.lblCropY.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCropX
			// 
			this.lblCropX.Location = new System.Drawing.Point(8, 48);
			this.lblCropX.Name = "lblCropX";
			this.lblCropX.Size = new System.Drawing.Size(48, 16);
			this.lblCropX.TabIndex = 9;
			this.lblCropX.Text = "X";
			this.lblCropX.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCropHeightVal
			// 
			this.lblCropHeightVal.Location = new System.Drawing.Point(160, 96);
			this.lblCropHeightVal.Name = "lblCropHeightVal";
			this.lblCropHeightVal.Size = new System.Drawing.Size(32, 16);
			this.lblCropHeightVal.TabIndex = 8;
			this.lblCropHeightVal.Text = "0";
			this.lblCropHeightVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCropWidthVal
			// 
			this.lblCropWidthVal.Location = new System.Drawing.Point(160, 80);
			this.lblCropWidthVal.Name = "lblCropWidthVal";
			this.lblCropWidthVal.Size = new System.Drawing.Size(32, 16);
			this.lblCropWidthVal.TabIndex = 7;
			this.lblCropWidthVal.Text = "0";
			this.lblCropWidthVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCropYVal
			// 
			this.lblCropYVal.Location = new System.Drawing.Point(160, 64);
			this.lblCropYVal.Name = "lblCropYVal";
			this.lblCropYVal.Size = new System.Drawing.Size(32, 16);
			this.lblCropYVal.TabIndex = 6;
			this.lblCropYVal.Text = "0";
			this.lblCropYVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCropXVal
			// 
			this.lblCropXVal.Location = new System.Drawing.Point(160, 48);
			this.lblCropXVal.Name = "lblCropXVal";
			this.lblCropXVal.Size = new System.Drawing.Size(32, 16);
			this.lblCropXVal.TabIndex = 5;
			this.lblCropXVal.Text = "0";
			this.lblCropXVal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// scrCropHeight
			// 
			this.scrCropHeight.LargeChange = 1;
			this.scrCropHeight.Location = new System.Drawing.Point(56, 96);
			this.scrCropHeight.Maximum = 1200;
			this.scrCropHeight.Name = "scrCropHeight";
			this.scrCropHeight.Size = new System.Drawing.Size(104, 16);
			this.scrCropHeight.TabIndex = 4;
			this.scrCropHeight.ValueChanged += new System.EventHandler(this.scrCropHeight_Changed);
			// 
			// scrCropWidth
			// 
			this.scrCropWidth.LargeChange = 1;
			this.scrCropWidth.Location = new System.Drawing.Point(56, 80);
			this.scrCropWidth.Maximum = 1600;
			this.scrCropWidth.Name = "scrCropWidth";
			this.scrCropWidth.Size = new System.Drawing.Size(104, 16);
			this.scrCropWidth.TabIndex = 3;
			this.scrCropWidth.ValueChanged += new System.EventHandler(this.scrCropWidth_Changed);
			// 
			// scrCropY
			// 
			this.scrCropY.LargeChange = 1;
			this.scrCropY.Location = new System.Drawing.Point(56, 64);
			this.scrCropY.Maximum = 1200;
			this.scrCropY.Name = "scrCropY";
			this.scrCropY.Size = new System.Drawing.Size(104, 16);
			this.scrCropY.TabIndex = 2;
			this.scrCropY.ValueChanged += new System.EventHandler(this.scrCropY_Changed);
			// 
			// scrCropX
			// 
			this.scrCropX.LargeChange = 1;
			this.scrCropX.Location = new System.Drawing.Point(56, 48);
			this.scrCropX.Maximum = 1600;
			this.scrCropX.Name = "scrCropX";
			this.scrCropX.Size = new System.Drawing.Size(104, 16);
			this.scrCropX.TabIndex = 1;
			this.scrCropX.ValueChanged += new System.EventHandler(this.scrCropX_Changed);
			// 
			// chkCrop
			// 
			this.chkCrop.Location = new System.Drawing.Point(16, 24);
			this.chkCrop.Name = "chkCrop";
			this.chkCrop.Size = new System.Drawing.Size(168, 16);
			this.chkCrop.TabIndex = 0;
			this.chkCrop.Text = "Crop Image on Load";
			this.chkCrop.CheckedChanged += new System.EventHandler(this.chkCrop_CheckedChanged);
			// 
			// grpResize
			// 
			this.grpResize.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.grpResize.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.txtResizeHeightVal,
																					this.txtResizeWidthVal,
																					this.lblResizeHeight,
																					this.lblResizeWidth,
																					this.chkResizeAspect,
																					this.scrResizeHeight,
																					this.scrResizeWidth,
																					this.chkResize});
			this.grpResize.Location = new System.Drawing.Point(224, 360);
			this.grpResize.Name = "grpResize";
			this.grpResize.Size = new System.Drawing.Size(200, 128);
			this.grpResize.TabIndex = 3;
			this.grpResize.TabStop = false;
			this.grpResize.Text = "Load Resize";
			// 
			// txtResizeHeightVal
			// 
			this.txtResizeHeightVal.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.txtResizeHeightVal.Location = new System.Drawing.Point(160, 72);
			this.txtResizeHeightVal.Name = "txtResizeHeightVal";
			this.txtResizeHeightVal.Size = new System.Drawing.Size(32, 20);
			this.txtResizeHeightVal.TabIndex = 7;
			this.txtResizeHeightVal.Text = "1";
			this.txtResizeHeightVal.TextChanged += new System.EventHandler(this.txtResizeHeightVal_TextChanged);
			// 
			// txtResizeWidthVal
			// 
			this.txtResizeWidthVal.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.txtResizeWidthVal.Location = new System.Drawing.Point(160, 48);
			this.txtResizeWidthVal.Name = "txtResizeWidthVal";
			this.txtResizeWidthVal.Size = new System.Drawing.Size(32, 20);
			this.txtResizeWidthVal.TabIndex = 6;
			this.txtResizeWidthVal.Text = "1";
			this.txtResizeWidthVal.TextChanged += new System.EventHandler(this.txtResizeWidthVal_TextChanged);
			// 
			// lblResizeHeight
			// 
			this.lblResizeHeight.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblResizeHeight.Location = new System.Drawing.Point(8, 72);
			this.lblResizeHeight.Name = "lblResizeHeight";
			this.lblResizeHeight.Size = new System.Drawing.Size(40, 16);
			this.lblResizeHeight.TabIndex = 5;
			this.lblResizeHeight.Text = "Height";
			this.lblResizeHeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblResizeWidth
			// 
			this.lblResizeWidth.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.lblResizeWidth.Location = new System.Drawing.Point(8, 48);
			this.lblResizeWidth.Name = "lblResizeWidth";
			this.lblResizeWidth.Size = new System.Drawing.Size(40, 16);
			this.lblResizeWidth.TabIndex = 4;
			this.lblResizeWidth.Text = "Width";
			this.lblResizeWidth.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// chkResizeAspect
			// 
			this.chkResizeAspect.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.chkResizeAspect.Location = new System.Drawing.Point(32, 96);
			this.chkResizeAspect.Name = "chkResizeAspect";
			this.chkResizeAspect.Size = new System.Drawing.Size(136, 16);
			this.chkResizeAspect.TabIndex = 3;
			this.chkResizeAspect.Text = "Preserve Aspect Ratio";
			// 
			// scrResizeHeight
			// 
			this.scrResizeHeight.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.scrResizeHeight.LargeChange = 1;
			this.scrResizeHeight.Location = new System.Drawing.Point(48, 72);
			this.scrResizeHeight.Maximum = 1200;
			this.scrResizeHeight.Minimum = 1;
			this.scrResizeHeight.Name = "scrResizeHeight";
			this.scrResizeHeight.Size = new System.Drawing.Size(112, 16);
			this.scrResizeHeight.TabIndex = 2;
			this.scrResizeHeight.Value = 1;
			this.scrResizeHeight.ValueChanged += new System.EventHandler(this.scrResizeHeight_Changed);
			// 
			// scrResizeWidth
			// 
			this.scrResizeWidth.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.scrResizeWidth.LargeChange = 1;
			this.scrResizeWidth.Location = new System.Drawing.Point(48, 48);
			this.scrResizeWidth.Maximum = 1600;
			this.scrResizeWidth.Minimum = 1;
			this.scrResizeWidth.Name = "scrResizeWidth";
			this.scrResizeWidth.Size = new System.Drawing.Size(112, 16);
			this.scrResizeWidth.TabIndex = 1;
			this.scrResizeWidth.Value = 1;
			this.scrResizeWidth.ValueChanged += new System.EventHandler(this.scrResizeWidth_Changed);
			// 
			// chkResize
			// 
			this.chkResize.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.chkResize.Location = new System.Drawing.Point(16, 24);
			this.chkResize.Name = "chkResize";
			this.chkResize.Size = new System.Drawing.Size(168, 16);
			this.chkResize.TabIndex = 0;
			this.chkResize.Text = "Resize Image on Load";
			this.chkResize.CheckedChanged += new System.EventHandler(this.chkResize_CheckedChanged);
			// 
			// grpRotate
			// 
			this.grpRotate.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.grpRotate.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.lblRotateSettings,
																					this.cmbRotate,
																					this.chkRotate});
			this.grpRotate.Location = new System.Drawing.Point(432, 360);
			this.grpRotate.Name = "grpRotate";
			this.grpRotate.Size = new System.Drawing.Size(176, 128);
			this.grpRotate.TabIndex = 4;
			this.grpRotate.TabStop = false;
			this.grpRotate.Text = "Load Rotate";
			// 
			// lblRotateSettings
			// 
			this.lblRotateSettings.Location = new System.Drawing.Point(16, 64);
			this.lblRotateSettings.Name = "lblRotateSettings";
			this.lblRotateSettings.Size = new System.Drawing.Size(88, 16);
			this.lblRotateSettings.TabIndex = 2;
			this.lblRotateSettings.Text = "Rotate Settings";
			// 
			// cmbRotate
			// 
			this.cmbRotate.Items.AddRange(new object[] {
														   "None",
														   "90 Degrees",
														   "180 Degrees",
														   "270 Degrees",
														   "Flip",
														   "Mirror"});
			this.cmbRotate.Location = new System.Drawing.Point(40, 80);
			this.cmbRotate.Name = "cmbRotate";
			this.cmbRotate.Size = new System.Drawing.Size(120, 21);
			this.cmbRotate.TabIndex = 1;
			// 
			// chkRotate
			// 
			this.chkRotate.Location = new System.Drawing.Point(16, 24);
			this.chkRotate.Name = "chkRotate";
			this.chkRotate.Size = new System.Drawing.Size(144, 16);
			this.chkRotate.TabIndex = 0;
			this.chkRotate.Text = "Rotate Image on Load";
			this.chkRotate.CheckedChanged += new System.EventHandler(this.chkRotate_CheckedChanged);
			// 
			// lblStatus
			// 
			this.lblStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblStatus.Location = new System.Drawing.Point(432, 88);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(96, 16);
			this.lblStatus.TabIndex = 6;
			this.lblStatus.Text = "Load Status:";
			// 
			// mnuFile
			// 
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuFileFile,
																					this.mnuToolbar,
																					this.menuItem1});
			// 
			// mnuFileFile
			// 
			this.mnuFileFile.Index = 0;
			this.mnuFileFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuFileOpen,
																						this.mnuSpacer,
																						this.mnuFileExit});
			this.mnuFileFile.Text = "File";
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Index = 0;
			this.mnuFileOpen.Text = "Open Image";
			this.mnuFileOpen.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// mnuSpacer
			// 
			this.mnuSpacer.Index = 1;
			this.mnuSpacer.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 2;
			this.menuItem1.Text = "About";
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// lblInfo
			// 
			this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInfo.Location = new System.Drawing.Point(16, 8);
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.ReadOnly = true;
			this.lblInfo.Size = new System.Drawing.Size(592, 64);
			this.lblInfo.TabIndex = 8;
			this.lblInfo.Text = "This example demonstrates the following:\n1) Using the LoadOptions object.\nSelect " +
				"any of the 3 loading options by clicking the box next to the appropriate option," +
				"\nthen click the Load Image button";
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblLastError.Location = new System.Drawing.Point(432, 248);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(176, 24);
			this.lblLastError.TabIndex = 9;
			this.lblLastError.Text = "Last Error:";
			// 
			// lblError
			// 
			this.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblError.Location = new System.Drawing.Point(432, 280);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(176, 64);
			this.lblError.TabIndex = 10;
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.AutoScroll = true;
			this.imageXView1.Location = new System.Drawing.Point(16, 80);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(408, 272);
			this.imageXView1.TabIndex = 12;
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(432, 104);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(184, 134);
			this.lstStatus.TabIndex = 13;
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(624, 502);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstStatus,
																		  this.imageXView1,
																		  this.lblError,
																		  this.lblLastError,
																		  this.lblInfo,
																		  this.lblStatus,
																		  this.grpRotate,
																		  this.grpResize,
																		  this.grpCrop});
			this.MaximizeBox = false;
			this.Menu = this.mnuFile;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Loading Options";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.grpCrop.ResumeLayout(false);
			this.grpResize.ResumeLayout(false);
			this.grpRotate.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{		
	
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

			//Create a new load options object so we can recieve events from the images we load
			loLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
			
			//this is where events are assigned. This happens before the file gets loaded.
			
			PegasusImaging.WinForms.ImagXpress9.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress9.ImageX.ProgressEventHandler( this.ProgressEventHandler );
			PegasusImaging.WinForms.ImagXpress9.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress9.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );

		
			//here we set the current directory and image so that the file open dialog box works well
			strCurrentImage = System.IO.Path.Combine (strCurrentDir, "window.jpg");

			//we open a default image to view
			imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage, loLoadOptions);

			//Now we reset our options controls to enabled or disabled.
			chkCrop_CheckedChanged(sender,e);
			chkResize_CheckedChanged(sender,e);
			chkRotate_CheckedChanged(sender,e);
			cmbRotate.SelectedIndex = 0;

			

		}

		private void chkCrop_CheckedChanged(object sender, System.EventArgs e)
		{
			foreach (System.Windows.Forms.Control cControl in grpCrop.Controls) 
			{
				if (cControl.Name != "chkCrop") 
				{
					cControl.Enabled = chkCrop.Checked;
				}
			}
		}

		private void chkResize_CheckedChanged(object sender, System.EventArgs e)
		{
			foreach (System.Windows.Forms.Control cControl in grpResize.Controls) 
			{
				if (cControl.Name != "chkResize") 
				{
					cControl.Enabled = chkResize.Checked;
				}
			}
		}

		private void chkRotate_CheckedChanged(object sender, System.EventArgs e)
		{
			foreach (System.Windows.Forms.Control cControl in grpRotate.Controls) 
			{
				if (cControl.Name != "chkRotate") 
				{
					cControl.Enabled = chkRotate.Checked;
				}
			}
		}


		private void DoCropOpts(bool bSelected,System.Int32 iWidth, System.Int32 iHeight)
		{
			if (bSelected)
			{
				if (scrCropX.Value > iWidth) 
				{
					scrCropX.Value = iWidth-1;
				}
				if (scrCropY.Value > iHeight) 
				{
					scrCropY.Value = iHeight-1;
				}
				if (scrCropWidth.Value > (iWidth - scrCropX.Value)) 
				{
					scrCropWidth.Value = iWidth - scrCropX.Value;
				}
				if (scrCropHeight.Value > (iHeight - scrCropY.Value)) 
				{
					scrCropHeight.Value = iHeight - scrCropY.Value;
				}
				try 
				{
					loLoadOptions.CropRectangle = new System.Drawing.Rectangle(scrCropX.Value,scrCropY.Value,scrCropWidth.Value,scrCropHeight.Value);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException e) 
				{
					PegasusError(e,lblError);
				}
			} 
			else 
			{
				try 
				{
					loLoadOptions.CropRectangle =new System.Drawing.Rectangle(0,0,iWidth,iHeight);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException e) 
				{
					PegasusError(e,lblError);
				}
			}
		}
		private void DoResizeOpts(bool bSelected)
		{
			if (bSelected)
			{
				try 
				{
					loLoadOptions.Resize = new System.Drawing.Size(scrResizeWidth.Value,scrResizeHeight.Value);
					loLoadOptions.MaintainAspectRatio = chkResizeAspect.Checked;
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException e) 
				{
					PegasusError(e,lblError);
				}
			} 
			else 
			{
				try 
				{
					loLoadOptions.Resize = new System.Drawing.Size();
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException e) 
				{
					PegasusError(e,lblError);
				}
			}
		}
		private void DoRotateOpts(bool bSelected)
		{
			if (bSelected)
			{
				try 
				{
					loLoadOptions.Rotation = (PegasusImaging.WinForms.ImagXpress9.RotateAngle)cmbRotate.SelectedIndex;
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException e) 
				{
					PegasusError(e,lblError);
				}
			}
			else
			{
				try
				{
					loLoadOptions.Rotation = 0;
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException e) 
				{
					PegasusError(e,lblError);
				}
			}
		}
		
		private void scrCropX_Changed(object sender, System.EventArgs e)
		{
			lblCropXVal.Text = scrCropX.Value.ToString(cultNumber);
		}

		private void scrCropY_Changed(object sender, System.EventArgs e)
		{
			lblCropYVal.Text = scrCropY.Value.ToString(cultNumber);
		}

		private void scrCropWidth_Changed(object sender, System.EventArgs e)
		{
			lblCropWidthVal.Text = scrCropWidth.Value.ToString(cultNumber);
		}

		private void scrCropHeight_Changed(object sender, System.EventArgs e)
		{
			lblCropHeightVal.Text = scrCropHeight.Value.ToString(cultNumber);
		}

		private void scrResizeWidth_Changed(object sender, System.EventArgs e)
		{
			txtResizeWidthVal.Text = scrResizeWidth.Value.ToString(cultNumber);
		}

		private void scrResizeHeight_Changed(object sender, System.EventArgs e)
		{
			txtResizeHeightVal.Text = scrResizeHeight.Value.ToString(cultNumber);
		}

		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress9.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress9.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		
		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			//First we grab the filename of the image we want to open
			System.String strLoadResult = PegasusOpenFile();

			//And we check to make sure the file is valid
			if (strLoadResult.Length != 0)
			{
				//If it is valid, we set our internal image filename equal to it
				strCurrentImage = strLoadResult;

				//And we also set the current directory equal to the file's directory
				strCurrentDir = System.IO.Path.GetDirectoryName(strLoadResult);

				//then we create an image object, but we don't open a file yet
				PegasusImaging.WinForms.ImagXpress9.ImageX iImage;
								
				try 
				{
					//now we load the image using our internal filename
					iImage = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage);
					//and we check that the crop options are not out of range for the image we just opened
					DoCropOpts(chkCrop.Checked,iImage.ImageXData.Width,iImage.ImageXData.Height);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
				{
					PegasusError(ex,lblError);
				}
				catch (System.IO.IOException ex)
				{
						PegasusError(ex,lblError);	
				}
				try 
				{
					//we do the same for the resize options
					DoResizeOpts(chkResize.Checked);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
				{
					PegasusError(ex,lblError);
				}
				try 
				{
					//And the rotate options
					DoRotateOpts(chkRotate.Checked);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
				{
					PegasusError(ex,lblError);
				}
				try 
				{
					//it's all looking good, so we load our image into an ImageX object
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage, loLoadOptions);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
				{
					PegasusError(ex,lblError);
				}

				catch (System.IO.IOException ex) 
				{
					PegasusError(ex,lblError);
				}
			} 
			else 
			{
				System.Windows.Forms.MessageBox.Show(strNoImageError);
			}
		}

		private void txtResizeWidthVal_TextChanged(object sender, System.EventArgs e)
		{
			System.Int32 iTmp;
			//if the txt is valid, change the slider
			try 
			{
				iTmp = Convert.ToInt32(txtResizeWidthVal.Text,cultNumber);
			} 
			catch (System.FormatException) 
			{
				txtResizeWidthVal.Text = scrResizeWidth.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrResizeWidth.Maximum) && (iTmp > scrResizeWidth.Minimum))
			{
				scrResizeWidth.Value = iTmp;
			} 
			else 
			{
				iTmp = scrResizeWidth.Value;
			}
			txtResizeWidthVal.Text = iTmp.ToString(cultNumber);
		}

		private void txtResizeHeightVal_TextChanged(object sender, System.EventArgs e)
		{
			System.Int32 iTmp;
			//if the txt is valid, change the slider
			try 
			{
				iTmp = Convert.ToInt32(txtResizeHeightVal.Text,cultNumber);
			} 
			catch (System.FormatException) 
			{
				txtResizeHeightVal.Text = scrResizeHeight.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrResizeHeight.Maximum) && (iTmp > scrResizeHeight.Minimum))
			{
				scrResizeHeight.Value = iTmp;
			} 
			else 
			{
				iTmp = scrResizeHeight.Value;
			}
			txtResizeHeightVal.Text = iTmp.ToString(cultNumber);
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();	
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			this.mnuToolbarShow.Text = (imageXView1.Toolbar.Activated) ? "Show":"Hide";
			imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
		}

		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private System.String strCurrentDir = @"..\..\..\..\..\..\..\..\Common\Images\";
		private const System.String strNoImageError = "You must select an image file to open.";
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox, System.Windows.Forms.Label ErrorLabel)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,ErrorLabel);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.FormatException ex)
			{
				PegasusError(ex,ErrorLabel);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion

	}
}

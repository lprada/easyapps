﻿/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly..
[assembly: AssemblyTitle("Pegasus Imaging ImagXpress v9.0 Demo")]
[assembly: AssemblyDescription("http://www.PegasusImaging.com")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Pegasus Imaging Corporation")]
[assembly: AssemblyProduct("Pegasus Imaging ImagXpress v9.0")]
[assembly: AssemblyCopyright("Copyright © 2008 Pegasus Imaging Corporation")]
[assembly: AssemblyTrademark("ImagXpress™ is a registered trademark of Pegasus Imaging Corporation")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("51c4a13d-e608-4ce8-8aa7-2701e71c3d27")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("9.0.25.0")]
[assembly: AssemblyFileVersion("9.0.25.0")]

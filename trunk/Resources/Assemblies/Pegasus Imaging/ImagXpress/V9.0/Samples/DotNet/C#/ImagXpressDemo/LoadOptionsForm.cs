/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo
{
    public partial class LoadOptionsForm : Form
    {

        Unlocks unlock = new Unlocks();

        Helper helper = new Helper();

        public LoadOptionsForm()
        {
            InitializeComponent();
        }

        private LoadOptions lo;

        public LoadOptions ImageLoadOptions
        {
            get
            {
                return lo;
            }
            set
            {
                lo = value;
            }
        }

        public decimal CropXMax
        {
            set
            {
                CropXBox.Maximum = value;
                CropWBox.Maximum = value;
            }
        }

        public decimal CropYMax
        {
            set
            {
                CropYBox.Maximum = value;
                CropHBox.Maximum = value;
            }
        }

        public Color JpegBackgroundColor
        {
            set
            {
                JpegGroupBox.BackColor = value;
            }
        }

        public Color TiffBackgroundColor
        {
            set
            {
                TIFFGroupBox.BackColor = value;
            }
        }

        public Color PicBackgroundColor
        {
            set
            {
                PicGroupBox.BackColor = value;
            }
        }

        public Color MetaBackgroundColor
        {
            set
            {
                MetaGroupBox.BackColor = value;
            }
        }

        public Color Jbig2BackgroundColor
        {
            set
            {
                Jbig2GroupBox.BackColor = value;
            }
        }

        public Color HdpBackgroundColor
        {
            set
            {
                HdpGroupBox.BackColor = value;
            }
        }

        public Color CadBackgroundColor
        {
            set
            {
                CadGroupBox.BackColor = value;
            }
        }

        private void LoadOptionsForm_Load(object sender, EventArgs e)
        {
            try
            {
                lo = new LoadOptions();

                renderIntentComboBox.SelectedIndex = 0;
                LoadComboBox.SelectedIndex = 0;
                RotationComboBox.SelectedIndex = 0;
                ThumbComboBox.SelectedIndex = 0;
                CadUnitsComboBox.SelectedIndex = 2;
                UnitsComboBox.SelectedIndex = 0;
                FTPComboBox.SelectedIndex = 0;
                postComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                lo.Cad.LayoutToRender = (int)LayoutBox.Value;
                lo.Cad.PaperBitDepth = (int)PaperBox.Value;
                lo.Cad.PaperHeight = (double)HeightBox.Value;
                lo.Cad.PaperWidth = (double)WidthBox.Value;
                lo.Cad.PaperResolutionUnits = (ResolutionUnits)CadUnitsComboBox.SelectedIndex;
                lo.Cad.PaperResolutionX = (double)PaperXBox.Value;
                lo.Cad.PaperResolutionY = (double)PaperYBox.Value;

                lo.CameraRawEnabled = CameraRawCheckBox.Checked;

                lo.ColorRenderIntent = (RenderIntent)renderIntentComboBox.SelectedIndex;

                lo.CropRectangle = new Rectangle((int)CropXBox.Value, (int)CropYBox.Value, (int)CropWBox.Value, (int)CropHBox.Value);

                lo.HdPhoto.PostProcessingFilter = (HdpPostProcessFilter)postComboBox.SelectedIndex;

                lo.ImageOffset = Int32.Parse(OffsetTextBox.Text);

                lo.Internet.FtpMode = (FtpMode)FTPComboBox.SelectedIndex;
                lo.Internet.NetworkCredentials = new System.Net.NetworkCredential(UserNameTextBox.Text, InternetPasswordTextBox.Text,
                        DomainTextBox.Text);

                lo.Jbig2.SwapBlackAndWhite = SwapCheckBox.Checked;

                lo.Jpeg.Cosited = CositedCheckBox.Checked;
                lo.Jpeg.Enhanced = EnhancedCheckBox.Checked;

                lo.LibraryNumberOfThreadsAllowed = (int)LibraryBox.Value;

                lo.LoadAlphaChannel = LoadAlphaCheckBox.Checked;

                lo.LoadMode = (LoadMode)LoadComboBox.SelectedIndex;

                lo.LoadResizeAntiAlias = ResizeCheckBox.Checked;

                lo.MaintainAspectRatio = MaintainCheckBox.Checked;

                lo.Metafile.ConvertToDib = ConvertCheckBox.Checked;
                lo.Metafile.Resolution.Units = (GraphicsUnit) UnitsComboBox.SelectedIndex;

                lo.Metafile.Resolution.Dimensions = new SizeF((float)ResXBox.Value, (float)ResYBox.Value);

                lo.Pic.Password = PasswordTextBox.Text;

                lo.PreserveBlack = PreserveCheckBox.Checked;

                lo.Resize = new Size((int)ResizeXBox.Value, (int)ResizeYBox.Value);

                lo.Rotation = (RotateAngle)RotationComboBox.SelectedIndex;

                lo.ScaleToGray = ScaleCheckBox.Checked;

                lo.ThumbnailSize = (ThumbnailSize)ThumbComboBox.SelectedIndex;

                lo.Tiff.IFDOffset = Int32.Parse(IFDOffsetTextBox.Text);
                lo.Tiff.SpecialHandling = SpecialCheckBox.Checked;

                lo.UseEmbeddedColorManagement = UseEmbedCheckBox.Checked;

                DialogResult = DialogResult.OK;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sourceButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.InitialDirectory = helper.GetPath()[0];
                    openDialog.Filter = "All Supported Profiles (*.ICM, *.ICC)|*.icm;*.icc|All Files (*.*)|*.*";
                    openDialog.FilterIndex = 0;
                    openDialog.Title = "Open a Source Profile";

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        lo.SourceProfileName = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }
    }
}
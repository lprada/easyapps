namespace Virtual_Image
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (imageXView1.Image != null)
            {
                imageXView1.Image.Dispose();
                imageXView1.Image = null;
            }
            if (imageXView1 != null)
            {
                imageXView1.Dispose();
                imageXView1 = null;
            }
            if (imageXView2.Image != null)
            {
                imageXView2.Image.Dispose();
                imageXView2.Image = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXView2 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.DescriptionListBox = new System.Windows.Forms.ListBox();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LoadButton = new System.Windows.Forms.Button();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(82, 89);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(399, 380);
            this.imageXView1.TabIndex = 0;
            this.imageXView1.PaintImage += new PegasusImaging.WinForms.ImagXpress9.ImageXView.PaintImageEventHandler(this.imageXView1_PaintImage);
            // 
            // imageXView2
            // 
            this.imageXView2.Location = new System.Drawing.Point(512, 180);
            this.imageXView2.Name = "imageXView2";
            this.imageXView2.Size = new System.Drawing.Size(35, 23);
            this.imageXView2.TabIndex = 1;
            this.imageXView2.Visible = false;
            // 
            // DescriptionListBox
            // 
            this.DescriptionListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionListBox.FormattingEnabled = true;
            this.DescriptionListBox.Items.AddRange(new object[] {
            "This example shows how to load a virtual image using the PaintImage event of Imag" +
                "Xpress."});
            this.DescriptionListBox.Location = new System.Drawing.Point(12, 27);
            this.DescriptionListBox.Name = "DescriptionListBox";
            this.DescriptionListBox.Size = new System.Drawing.Size(550, 56);
            this.DescriptionListBox.TabIndex = 0;
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(574, 24);
            this.MainMenu.TabIndex = 2;
            this.MainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imagXpressToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.imagXpressToolStripMenuItem.Text = "Imag&Xpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(228, 475);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(112, 37);
            this.LoadButton.TabIndex = 0;
            this.LoadButton.Text = "Load Virtual Image";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 517);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.DescriptionListBox);
            this.Controls.Add(this.imageXView2);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.MainMenu;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Virtual Image";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView2;
        private System.Windows.Forms.ListBox DescriptionListBox;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagXpressToolStripMenuItem;
        private System.Windows.Forms.Button LoadButton;
    }
}


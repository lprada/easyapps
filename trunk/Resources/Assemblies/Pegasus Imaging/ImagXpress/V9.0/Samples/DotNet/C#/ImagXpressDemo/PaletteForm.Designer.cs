/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class PaletteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (colorButtons != null)
            {
                for (int i = 0; i < colorButtons.Length; i++)
                {
                    if (colorButtons[i] != null)
                    {
                        colorButtons[i].Dispose();
                        colorButtons[i] = null;
                    }
                }
            }
            if (colorDialog != null)
            {
                colorDialog.Dispose();
                colorDialog = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaletteForm));
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelPaletteButton = new System.Windows.Forms.Button();
            this.colorLabel = new System.Windows.Forms.Label();
            this.instructionsLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(96, 494);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelPaletteButton
            // 
            this.CancelPaletteButton.Location = new System.Drawing.Point(269, 494);
            this.CancelPaletteButton.Name = "CancelPaletteButton";
            this.CancelPaletteButton.Size = new System.Drawing.Size(75, 23);
            this.CancelPaletteButton.TabIndex = 1;
            this.CancelPaletteButton.Text = "Cancel";
            this.CancelPaletteButton.UseVisualStyleBackColor = true;
            this.CancelPaletteButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // colorLabel
            // 
            this.colorLabel.AutoSize = true;
            this.colorLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.colorLabel.Location = new System.Drawing.Point(179, 435);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(2, 15);
            this.colorLabel.TabIndex = 2;
            // 
            // instructionsLabel
            // 
            this.instructionsLabel.AutoSize = true;
            this.instructionsLabel.Location = new System.Drawing.Point(27, 468);
            this.instructionsLabel.Name = "instructionsLabel";
            this.instructionsLabel.Size = new System.Drawing.Size(349, 13);
            this.instructionsLabel.TabIndex = 3;
            this.instructionsLabel.Text = "To change a palette color click the current color and choose a new one.";
            // 
            // PaletteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 529);
            this.Controls.Add(this.instructionsLabel);
            this.Controls.Add(this.colorLabel);
            this.Controls.Add(this.CancelPaletteButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PaletteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Color Palette";
            this.Load += new System.EventHandler(this.PaletteForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelPaletteButton;
        private System.Windows.Forms.Label colorLabel;
        private System.Windows.Forms.Label instructionsLabel;
    }
}
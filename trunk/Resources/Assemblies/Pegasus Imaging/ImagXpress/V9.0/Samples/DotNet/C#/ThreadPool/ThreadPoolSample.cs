/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;

namespace ThreadPoolSample
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class ThreadPoolSample : System.Windows.Forms.Form
	{
		private const int MAX_THREADS = 50;
		private System.Windows.Forms.Button button1;
		private int CurrentThreadCount;
		private ManualResetEvent[] finishEvents= new ManualResetEvent[MAX_THREADS];
		private IXWorkerThread[] myWorkerThreads= new IXWorkerThread[MAX_THREADS];
		private System.Random myRand;
		private System.Timers.Timer finishWatcherStartTimer;
		private bool isRunning;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ListView listView1;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox lblInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuToolbarShow;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblError;
		private System.Collections.ArrayList LargeImageCollection;

		public ThreadPoolSample()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			//ManualResetEvent[] finishEvents 
			//IXWorkerThread[] myWorkerThreads 
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			isRunning = false;
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}

				//***call the Dispose method on the imagXpress1 object and the
				//*** imageXView1 object
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.button1 = new System.Windows.Forms.Button();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.listView1 = new System.Windows.Forms.ListView();
			this.label1 = new System.Windows.Forms.Label();
			this.lblInfo = new System.Windows.Forms.RichTextBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuToolbarShow = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.label2 = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.button1.Location = new System.Drawing.Point(472, 456);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(248, 24);
			this.button1.TabIndex = 0;
			this.button1.Text = "Generate 5 more images.";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
			this.imageXView1.Location = new System.Drawing.Point(8, 96);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(456, 352);
			this.imageXView1.TabIndex = 3;
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(48, 48);
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// listView1
			// 
			this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.listView1.LargeImageList = this.imageList1;
			this.listView1.Location = new System.Drawing.Point(472, 96);
			this.listView1.MultiSelect = false;
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(248, 352);
			this.listView1.SmallImageList = this.imageList1;
			this.listView1.TabIndex = 4;
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 456);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(456, 24);
			this.label1.TabIndex = 5;
			this.label1.Text = "Thread Count: ";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblInfo
			// 
			this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblInfo.Location = new System.Drawing.Point(8, 8);
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.Size = new System.Drawing.Size(712, 64);
			this.lblInfo.TabIndex = 6;
			this.lblInfo.Text = "This example demonstrates the following:\n1) Using a thread pool.\n2) Processing im" +
				"age data inside of a thread.\nPress \"Generate 5 more images\" to generate 5 more i" +
				"mages, you can enlarge the view of the images by clicking on them.";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem3,
																					  this.menuItem5});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2});
			this.menuItem1.Text = "&File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "&Quit";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuToolbarShow});
			this.menuItem3.Text = "&Toolbar";
			// 
			// menuToolbarShow
			// 
			this.menuToolbarShow.Index = 0;
			this.menuToolbarShow.Text = "&Show";
			this.menuToolbarShow.Click += new System.EventHandler(this.menuToolbarShow_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 2;
			this.menuItem5.Text = "&About";
			this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 7;
			this.label2.Text = "Last Error:";
			// 
			// lblError
			// 
			this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblError.Location = new System.Drawing.Point(72, 80);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(648, 16);
			this.lblError.TabIndex = 8;
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(728, 489);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblError,
																		  this.label2,
																		  this.lblInfo,
																		  this.label1,
																		  this.listView1,
																		  this.imageXView1,
																		  this.button1});
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Menu = this.mainMenu1;
			this.Name = "FormMain";
			this.Text = "Thread Pool Sample";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ThreadPoolSample());
		}

        private delegate void SafeContextListViewItemAdder(System.Windows.Forms.ListViewItem theItem);
        private delegate void SafeContextLabelTextChanger(System.String theString);
        private delegate void SafeContextImageListAdder(System.Drawing.Image theImage);

        private void RealImageListAdder(System.Drawing.Image theImage)
        {
            imageList1.Images.Add(theImage);
        }

        private void RealLabelTextChanger(System.String theString)
        {
            label1.Text = theString;
        }

        private void RealListViewItemAdder(System.Windows.Forms.ListViewItem theItem)
        {
            listView1.Items.Add(theItem);
        }

		private void FinishWatcher()
		{
			while (isRunning)
			{
				int finisher = WaitHandle.WaitAny(finishEvents,50,false);
				Application.DoEvents();
				if (finisher != WaitHandle.WaitTimeout)
				{         
                    //imageList1 is tied to listView1, so check if listView1 needs invoking
                    if (listView1.InvokeRequired)
                    {
                        object[] theArgs = { myWorkerThreads[finisher].GetResult() };
                        listView1.Invoke(new SafeContextImageListAdder(this.RealImageListAdder), theArgs);
                    }
                    else
                    {
                        imageList1.Images.Add(myWorkerThreads[finisher].GetResult());
                    }
					LargeImageCollection.Add(myWorkerThreads[finisher].GetResult());

                    //this next call can be cross-thread
                    if (listView1.InvokeRequired)
                    {
                        object[] theArgs = { new System.Windows.Forms.ListViewItem(finisher.ToString(), imageList1.Images.Count - 1) };
                        listView1.Invoke(new SafeContextListViewItemAdder(this.RealListViewItemAdder), theArgs);
                    }
                    else
                    {
                        listView1.Items.Add(new System.Windows.Forms.ListViewItem(finisher.ToString(), imageList1.Images.Count - 1));
                    }
					System.Threading.Thread.Sleep(50);
					Application.DoEvents();
					finishEvents[finisher] = new ManualResetEvent(false);
					myWorkerThreads[finisher] = null;
					CurrentThreadCount -= 1;
					UpdateThreadCountDisplay();
					Application.DoEvents();
				}				
			}
		}

		private int GetFirstAvailableIndex()
		{
			for (int i = 0; i < MAX_THREADS; i++)
			{
				if (myWorkerThreads[i] == null)
					return i;
			}
			return -1; //no more threads available
		}
		
		private void button1_Click(object sender, System.EventArgs e)
		{		
			const int numToRun = 5;
			for (int i = 0; i < numToRun; i++)
			{
				int x = GetFirstAvailableIndex();
				if (x != -1)
				{
					finishEvents[x] = new ManualResetEvent(false);
					IXWorkerThread wt = new IXWorkerThread(myRand.Next(),finishEvents[x], imagXpress1);
					myWorkerThreads[x] = wt;
					ThreadPool.QueueUserWorkItem(new WaitCallback(wt.ThreadPoolCallback), x);				
					CurrentThreadCount += 1;
				}				
			}			
			UpdateThreadCountDisplay();
		}
		private void UpdateThreadCountDisplay()
		{
            if (label1.InvokeRequired)
            {
                object[] theArgs = { "Thread Count: " + CurrentThreadCount.ToString() };
                label1.Invoke(new SafeContextLabelTextChanger(this.RealLabelTextChanger), theArgs);
            }
            else
            {
                label1.Text = "Thread Count: " + CurrentThreadCount.ToString();
            }
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.Licensing.UnlockRuntime(12345,12345,12345,12345);

			LargeImageCollection = new System.Collections.ArrayList();
			myRand = new System.Random();
			isRunning = true;

			for (int x = 0; x < MAX_THREADS; x++)
			{
				finishEvents[x] = new ManualResetEvent(false);
			}

			finishWatcherStartTimer = new System.Timers.Timer();
			finishWatcherStartTimer.Elapsed += new System.Timers.ElapsedEventHandler(FinishStarter);
			finishWatcherStartTimer.Interval = 500;
			finishWatcherStartTimer.Enabled = true;			
		}
		public void FinishStarter(object source, System.Timers.ElapsedEventArgs e)
		{
			finishWatcherStartTimer.Enabled = false;
			finishWatcherStartTimer.Stop();
			finishWatcherStartTimer.Dispose();
			FinishWatcher();			
		}

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{	
			if (listView1.SelectedIndices.Count > 0)
			{
				int daImageIndex = listView1.SelectedIndices[0];
				System.Drawing.Bitmap daImageBitmap = ((System.Drawing.Bitmap)LargeImageCollection[daImageIndex]);
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHbitmap(imagXpress1, daImageBitmap.GetHbitmap());
			}
		}

		private void menuItem5_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void menuToolbarShow_Click(object sender, System.EventArgs e)
		{
			this.menuToolbarShow.Text = (imageXView1.Toolbar.Activated)?"Show":"Hide";
			imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
	}
}

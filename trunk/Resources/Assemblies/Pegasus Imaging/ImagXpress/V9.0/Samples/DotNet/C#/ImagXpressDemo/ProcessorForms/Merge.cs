/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo.ProcessorForms
{
    public partial class MergeForm : Form
    {
        public MergeForm()
        {
            InitializeComponent();
        }

        Unlocks unlock = new Unlocks();

        Helper helper = new Helper();

        private ImageX sourceImage;

        public ImageX CurrentImage
        {
            set
            {
                imageXViewCurrent.Image = value;
            }
        }

        public ImageX PreviewImage
        {
            get
            {
                return imageXViewPreview.Image;
            }
        }

        private string Alpha1ImagePath, Alpha2ImagePath, Alpha3ImagePath;
        private string AlphaBrushImagePath, BallImagePath, commonImagePath;

        private void ProcessorForm_Load(object sender, EventArgs e)
        {
            try
            {
             
                
                unlock.UnlockControls(imagXpress1);


                commonImagePath = helper.GetPath()[0];   

                Alpha1ImagePath = commonImagePath + "Alpha1.tif";
                Alpha2ImagePath = commonImagePath + "Alpha2.tif";
                Alpha3ImagePath = commonImagePath + "Alpha3.tif";
                AlphaBrushImagePath = commonImagePath + "alphabush_small.tif";
                BallImagePath = commonImagePath + "Ball1.bmp";

                comboBoxSizing.SelectedIndex = 0;
                comboBoxStyle.SelectedIndex = 0;
                comboBoxSourceImage.SelectedIndex = 0;             
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            PreviewButton_Click(sender, e);

            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (processor1 != null)
                {
                    if (processor1.Image != null)
                    {
                        processor1.Image.Dispose();
                        processor1.Image = null;
                    }
                }
                
                processor1.Image = imageXViewCurrent.Image.Copy();

                processor1.Merge(ref sourceImage, (MergeSize)comboBoxSizing.SelectedIndex, (MergeStyle)comboBoxStyle.SelectedIndex, 
                    checkBoxTransparency.Checked, buttonTransparentColor.BackColor, (int)HighBox.Value, (int)LowBox.Value);

                if (imageXViewPreview != null)
                {
                    imageXViewPreview.Image = processor1.Image;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ExceptionHandler(string error, string methodName)
        {
            MessageBox.Show(error, "Error from " + methodName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ShadeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Helper help = new Helper();

            help.ShowHelp(this, Text);
        }

        private void buttonTransparentColor_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = buttonTransparentColor.BackColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        buttonTransparentColor.BackColor = dlg.Color;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }

        private void comboBoxSourceImage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (sourceImage != null)
                {
                    sourceImage.Dispose();
                    sourceImage = null;
                }

                switch (comboBoxSourceImage.SelectedIndex)
                {
                    case 0:
                        {
                            sourceImage = ImageX.FromFile(imagXpress1, Alpha1ImagePath);
                            break;
                        }
                    case 1:
                        {
                            sourceImage = ImageX.FromFile(imagXpress1, Alpha2ImagePath);
                            break;
                        }
                    case 2:
                        {
                            sourceImage = ImageX.FromFile(imagXpress1, Alpha3ImagePath);
                            break;
                        }
                    case 3:
                        {
                            sourceImage = ImageX.FromFile(imagXpress1, AlphaBrushImagePath);
                            break;
                        }
                    case 4:
                        {
                            sourceImage = ImageX.FromFile(imagXpress1, BallImagePath);
                            break;
                        }
                    case 5:
                        {
                            using (OpenFileDialog dlg = new OpenFileDialog())
                            {
                                dlg.Title = "Open an Image File";
                                dlg.Filter = helper.OpenImageFilter;

                                if (dlg.ShowDialog() == DialogResult.OK)
                                {
                                    sourceImage = ImageX.FromFile(imagXpress1, dlg.FileName);

                                    customLabel.Text = Path.GetFullPath(dlg.FileName);
                                }
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex.Message, MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Globalization;

namespace ViewingOptions
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Double dZoomFactor;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private System.Windows.Forms.GroupBox grpZooming;
		private System.Windows.Forms.CheckBox chkZoom;
		private System.Windows.Forms.Button cmdZoomIn;
		private System.Windows.Forms.Button cmdZoomOut;
		private System.Windows.Forms.GroupBox grpVAlign;
		private System.Windows.Forms.GroupBox grpHAlign;
		private System.Windows.Forms.GroupBox grpDisplayOpts;
		private System.Windows.Forms.CheckBox chkAA;
		private System.Windows.Forms.CheckBox chkSmoothing;
		private System.Windows.Forms.CheckBox chkDithering;
		private System.Windows.Forms.GroupBox grpBrightandCont;
		private System.Windows.Forms.TrackBar tbBrightness;
		private System.Windows.Forms.Button cmdReset;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cmbHAlign;
		private System.Windows.Forms.ComboBox cmbVAlign;
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem mnuFileFile;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.String strCurrentImage;
		private PegasusImaging.WinForms.ImagXpress9.LoadOptions loLoadOptions;
		private System.Windows.Forms.TrackBar tbContrast;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.Label lblErrorTell;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.Label lblLoadStatus;
		private System.Windows.Forms.ListBox lstStatus;
		private System.Windows.Forms.ListBox listBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}

				//***call the Dispose method on the imagXpress1 object and the
				//*** imageXView1 object
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(imagXpress1);
			this.grpZooming = new System.Windows.Forms.GroupBox();
			this.cmdZoomOut = new System.Windows.Forms.Button();
			this.cmdZoomIn = new System.Windows.Forms.Button();
			this.chkZoom = new System.Windows.Forms.CheckBox();
			this.grpVAlign = new System.Windows.Forms.GroupBox();
			this.cmbVAlign = new System.Windows.Forms.ComboBox();
			this.grpHAlign = new System.Windows.Forms.GroupBox();
			this.cmbHAlign = new System.Windows.Forms.ComboBox();
			this.grpDisplayOpts = new System.Windows.Forms.GroupBox();
			this.chkDithering = new System.Windows.Forms.CheckBox();
			this.chkSmoothing = new System.Windows.Forms.CheckBox();
			this.chkAA = new System.Windows.Forms.CheckBox();
			this.grpBrightandCont = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cmdReset = new System.Windows.Forms.Button();
			this.tbContrast = new System.Windows.Forms.TrackBar();
			this.tbBrightness = new System.Windows.Forms.TrackBar();
			this.mnuFile = new System.Windows.Forms.MainMenu();
			this.mnuFileFile = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mnuFileQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.lblErrorTell = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.grpZooming.SuspendLayout();
			this.grpVAlign.SuspendLayout();
			this.grpHAlign.SuspendLayout();
			this.grpDisplayOpts.SuspendLayout();
			this.grpBrightandCont.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbContrast)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbBrightness)).BeginInit();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.Location = new System.Drawing.Point(16, 136);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(464, 304);
			this.imageXView1.TabIndex = 1;
			// 
			// grpZooming
			// 
			this.grpZooming.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.grpZooming.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.cmdZoomOut,
																					 this.cmdZoomIn,
																					 this.chkZoom});
			this.grpZooming.Location = new System.Drawing.Point(488, 128);
			this.grpZooming.Name = "grpZooming";
			this.grpZooming.Size = new System.Drawing.Size(160, 80);
			this.grpZooming.TabIndex = 2;
			this.grpZooming.TabStop = false;
			this.grpZooming.Text = "Zooming";
			// 
			// cmdZoomOut
			// 
			this.cmdZoomOut.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom);
			this.cmdZoomOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdZoomOut.Location = new System.Drawing.Point(28, 49);
			this.cmdZoomOut.Name = "cmdZoomOut";
			this.cmdZoomOut.Size = new System.Drawing.Size(40, 23);
			this.cmdZoomOut.TabIndex = 2;
			this.cmdZoomOut.Text = "-";
			this.cmdZoomOut.Click += new System.EventHandler(this.cmdZoomOut_Click);
			// 
			// cmdZoomIn
			// 
			this.cmdZoomIn.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdZoomIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdZoomIn.Location = new System.Drawing.Point(96, 49);
			this.cmdZoomIn.Name = "cmdZoomIn";
			this.cmdZoomIn.Size = new System.Drawing.Size(40, 23);
			this.cmdZoomIn.TabIndex = 1;
			this.cmdZoomIn.Text = "+";
			this.cmdZoomIn.Click += new System.EventHandler(this.cmdZoomIn_Click);
			// 
			// chkZoom
			// 
			this.chkZoom.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.chkZoom.Location = new System.Drawing.Point(32, 21);
			this.chkZoom.Name = "chkZoom";
			this.chkZoom.Size = new System.Drawing.Size(112, 16);
			this.chkZoom.TabIndex = 0;
			this.chkZoom.Text = "Enable Zooming";
			this.chkZoom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkZoom.CheckedChanged += new System.EventHandler(this.chkZoom_CheckedChanged);
			// 
			// grpVAlign
			// 
			this.grpVAlign.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.grpVAlign.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.cmbVAlign});
			this.grpVAlign.Location = new System.Drawing.Point(488, 216);
			this.grpVAlign.Name = "grpVAlign";
			this.grpVAlign.Size = new System.Drawing.Size(160, 48);
			this.grpVAlign.TabIndex = 3;
			this.grpVAlign.TabStop = false;
			this.grpVAlign.Text = "Vertical Alignment";
			// 
			// cmbVAlign
			// 
			this.cmbVAlign.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.cmbVAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbVAlign.Items.AddRange(new object[] {
														   "Top",
														   "Center",
														   "Bottom"});
			this.cmbVAlign.Location = new System.Drawing.Point(16, 17);
			this.cmbVAlign.Name = "cmbVAlign";
			this.cmbVAlign.Size = new System.Drawing.Size(136, 21);
			this.cmbVAlign.TabIndex = 1;
			this.cmbVAlign.SelectedIndexChanged += new System.EventHandler(this.cmbVAlign_SelectedIndexChanged);
			// 
			// grpHAlign
			// 
			this.grpHAlign.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.grpHAlign.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.cmbHAlign});
			this.grpHAlign.Location = new System.Drawing.Point(488, 272);
			this.grpHAlign.Name = "grpHAlign";
			this.grpHAlign.Size = new System.Drawing.Size(160, 48);
			this.grpHAlign.TabIndex = 4;
			this.grpHAlign.TabStop = false;
			this.grpHAlign.Text = "Horizontal Alignment";
			// 
			// cmbHAlign
			// 
			this.cmbHAlign.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.cmbHAlign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbHAlign.Items.AddRange(new object[] {
														   "Left",
														   "Middle",
														   "Right"});
			this.cmbHAlign.Location = new System.Drawing.Point(16, 17);
			this.cmbHAlign.Name = "cmbHAlign";
			this.cmbHAlign.Size = new System.Drawing.Size(136, 21);
			this.cmbHAlign.TabIndex = 0;
			this.cmbHAlign.SelectedIndexChanged += new System.EventHandler(this.cmbHAlign_SelectedIndexChanged);
			// 
			// grpDisplayOpts
			// 
			this.grpDisplayOpts.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.grpDisplayOpts.Controls.AddRange(new System.Windows.Forms.Control[] {
																						 this.chkDithering,
																						 this.chkSmoothing,
																						 this.chkAA});
			this.grpDisplayOpts.Location = new System.Drawing.Point(488, 320);
			this.grpDisplayOpts.Name = "grpDisplayOpts";
			this.grpDisplayOpts.Size = new System.Drawing.Size(160, 96);
			this.grpDisplayOpts.TabIndex = 5;
			this.grpDisplayOpts.TabStop = false;
			this.grpDisplayOpts.Text = "Display Options";
			// 
			// chkDithering
			// 
			this.chkDithering.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.chkDithering.Checked = true;
			this.chkDithering.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkDithering.Location = new System.Drawing.Point(16, 73);
			this.chkDithering.Name = "chkDithering";
			this.chkDithering.Size = new System.Drawing.Size(136, 16);
			this.chkDithering.TabIndex = 2;
			this.chkDithering.Text = "Dithering";
			this.chkDithering.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkDithering.CheckedChanged += new System.EventHandler(this.chkDithering_CheckedChanged);
			// 
			// chkSmoothing
			// 
			this.chkSmoothing.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.chkSmoothing.Checked = true;
			this.chkSmoothing.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkSmoothing.Location = new System.Drawing.Point(16, 49);
			this.chkSmoothing.Name = "chkSmoothing";
			this.chkSmoothing.Size = new System.Drawing.Size(136, 16);
			this.chkSmoothing.TabIndex = 1;
			this.chkSmoothing.Text = "Smoothing (Color)";
			this.chkSmoothing.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkSmoothing.CheckedChanged += new System.EventHandler(this.chkSmoothing_CheckedChanged);
			// 
			// chkAA
			// 
			this.chkAA.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.chkAA.Checked = true;
			this.chkAA.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chkAA.Location = new System.Drawing.Point(16, 25);
			this.chkAA.Name = "chkAA";
			this.chkAA.Size = new System.Drawing.Size(136, 16);
			this.chkAA.TabIndex = 0;
			this.chkAA.Text = "Anti-Aliasing (B&&W)";
			this.chkAA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkAA.CheckedChanged += new System.EventHandler(this.chkAA_CheckedChanged);
			// 
			// grpBrightandCont
			// 
			this.grpBrightandCont.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.grpBrightandCont.Controls.AddRange(new System.Windows.Forms.Control[] {
																						   this.label3,
																						   this.label2,
																						   this.cmdReset,
																						   this.tbContrast,
																						   this.tbBrightness});
			this.grpBrightandCont.Location = new System.Drawing.Point(16, 448);
			this.grpBrightandCont.Name = "grpBrightandCont";
			this.grpBrightandCont.Size = new System.Drawing.Size(464, 104);
			this.grpBrightandCont.TabIndex = 6;
			this.grpBrightandCont.TabStop = false;
			this.grpBrightandCont.Text = "Brightness and Contrast";
			// 
			// label3
			// 
			this.label3.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label3.Location = new System.Drawing.Point(264, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(184, 24);
			this.label3.TabIndex = 4;
			this.label3.Text = "Contrast Adjustment";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label2.Location = new System.Drawing.Point(16, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(184, 24);
			this.label2.TabIndex = 3;
			this.label2.Text = "Brightness Adjustment";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cmdReset
			// 
			this.cmdReset.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmdReset.Location = new System.Drawing.Point(208, 48);
			this.cmdReset.Name = "cmdReset";
			this.cmdReset.Size = new System.Drawing.Size(48, 24);
			this.cmdReset.TabIndex = 2;
			this.cmdReset.Text = "Reset";
			this.cmdReset.Click += new System.EventHandler(this.cmdReset_Click);
			// 
			// tbContrast
			// 
			this.tbContrast.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.tbContrast.LargeChange = 1;
			this.tbContrast.Location = new System.Drawing.Point(264, 56);
			this.tbContrast.Maximum = 100;
			this.tbContrast.Minimum = -100;
			this.tbContrast.Name = "tbContrast";
			this.tbContrast.Size = new System.Drawing.Size(184, 45);
			this.tbContrast.TabIndex = 1;
			this.tbContrast.Scroll += new System.EventHandler(this.tbContrast_Scroll);
			// 
			// tbBrightness
			// 
			this.tbBrightness.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.tbBrightness.LargeChange = 1;
			this.tbBrightness.Location = new System.Drawing.Point(16, 56);
			this.tbBrightness.Maximum = 100;
			this.tbBrightness.Minimum = -100;
			this.tbBrightness.Name = "tbBrightness";
			this.tbBrightness.Size = new System.Drawing.Size(184, 45);
			this.tbBrightness.TabIndex = 0;
			this.tbBrightness.Scroll += new System.EventHandler(this.tbBrightness_Scroll);
			// 
			// mnuFile
			// 
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuFileFile,
																					this.mnuToolbar,
																					this.mnuAbout});
			// 
			// mnuFileFile
			// 
			this.mnuFileFile.Index = 0;
			this.mnuFileFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuFileOpen,
																						this.menuItem3,
																						this.mnuFileQuit});
			this.mnuFileFile.Text = "&File";
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Index = 0;
			this.mnuFileOpen.Text = "&Open";
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "-";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 2;
			this.mnuFileQuit.Text = "&Quit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "&Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "&Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// lblErrorTell
			// 
			this.lblErrorTell.Location = new System.Drawing.Point(24, 72);
			this.lblErrorTell.Name = "lblErrorTell";
			this.lblErrorTell.Size = new System.Drawing.Size(64, 32);
			this.lblErrorTell.TabIndex = 8;
			this.lblErrorTell.Text = "Last Error:";
			// 
			// lblError
			// 
			this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblError.Location = new System.Drawing.Point(96, 72);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(552, 56);
			this.lblError.TabIndex = 9;
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(488, 424);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(72, 16);
			this.lblLoadStatus.TabIndex = 10;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(488, 448);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(160, 95);
			this.lstStatus.TabIndex = 11;
			// 
			// listBox1
			// 
			this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.listBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.listBox1.Items.AddRange(new object[] {
														  "This sample demonstrates the following functionality:",
														  "1)Using the the many different View control properties. ",
														  "",
														  "All of these options change the way the image is viewed, but they do not change t" +
														  "he image data underneath."});
			this.listBox1.Location = new System.Drawing.Point(16, 8);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(632, 56);
			this.listBox1.TabIndex = 12;
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(664, 585);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.listBox1,
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.lblError,
																		  this.lblErrorTell,
																		  this.grpBrightandCont,
																		  this.grpDisplayOpts,
																		  this.grpVAlign,
																		  this.grpZooming,
																		  this.imageXView1,
																		  this.grpHAlign});
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Menu = this.mnuFile;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Viewing Options";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.grpZooming.ResumeLayout(false);
			this.grpVAlign.ResumeLayout(false);
			this.grpHAlign.ResumeLayout(false);
			this.grpDisplayOpts.ResumeLayout(false);
			this.grpBrightandCont.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tbContrast)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbBrightness)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
			
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);			
			
			//Create a new load options object so we can recieve events from the images we load
			loLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
			
			//this is where events are assigned. This happens before the file gets loaded.
			PegasusImaging.WinForms.ImagXpress9.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress9.ImageX.ProgressEventHandler( this.ProgressEventHandler );
			PegasusImaging.WinForms.ImagXpress9.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress9.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );

			cmbHAlign.SelectedIndex = 1;
			cmbVAlign.SelectedIndex = 1;
			cmdZoomIn.Enabled = false;
			cmdZoomOut.Enabled = false;

			strCurrentImage = System.IO.Path.Combine (strCurrentDir, "benefits.tif");
			
			if (System.IO.File.Exists(strCurrentImage))
			{
				try 
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage, loLoadOptions);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}
			try 
			{
				imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress9.ZoomToFitType.FitBest);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}

			dZoomFactor = Convert.ToDouble(imageXView1.ZoomFactor);
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress9.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress9.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		
		private void chkZoom_CheckedChanged(object sender, System.EventArgs e)
		{
			cmdZoomIn.Enabled = chkZoom.Checked;
			cmdZoomOut.Enabled = chkZoom.Checked;
			if (!chkZoom.Checked) 
			{
				try 
				{
					imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress9.ZoomToFitType.FitBest);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			} 
			else 
			{
				try 
				{
					imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}
		}

		private void cmdZoomIn_Click(object sender, System.EventArgs e)
		{
			dZoomFactor += 0.005;
			try 
			{
				imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void cmdZoomOut_Click(object sender, System.EventArgs e)
		{
			dZoomFactor -= 0.005;
			try 
			{
				imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void cmbVAlign_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.AlignVertical = ((PegasusImaging.WinForms.ImagXpress9.AlignVertical)cmbVAlign.SelectedIndex);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void cmbHAlign_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.AlignHorizontal = ((PegasusImaging.WinForms.ImagXpress9.AlignHorizontal)cmbHAlign.SelectedIndex);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void chkAA_CheckedChanged(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.Antialias = chkAA.Checked;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void chkSmoothing_CheckedChanged(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.Smoothing = chkSmoothing.Checked;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void chkDithering_CheckedChanged(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.Dithered = chkDithering.Checked;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void tbBrightness_Scroll(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.Brightness = tbBrightness.Value;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void tbContrast_Scroll(object sender, System.EventArgs e)
		{
			try 
			{
				imageXView1.Contrast = tbContrast.Value;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void cmdReset_Click(object sender, System.EventArgs e)
		{
			tbBrightness.Value = 0;
			tbContrast.Value = 0;
			try 
			{
				imageXView1.Brightness = 0;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
			try 
			{
				imageXView1.Contrast = 0;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			System.String sTmp = PegasusOpenFile();
			if (sTmp.Length != 0)
			{
				strCurrentImage = sTmp;
				try 
				{
					//clear out error before next image load
					lblError.Text = "";

					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage, loLoadOptions);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
				try 
				{
					imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress9.ZoomToFitType.FitBest);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			this.mnuToolbarShow.Text = (imageXView1.Toolbar.Activated)? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}
		
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
		private System.String strCurrentDir = @"..\..\..\..\..\..\..\..\Common\Images\";
		private const System.String strNoImageError = "You must select an image file to open.";
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion
	}
}

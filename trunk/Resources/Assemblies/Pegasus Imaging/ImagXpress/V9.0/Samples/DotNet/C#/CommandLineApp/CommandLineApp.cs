/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using PegasusImaging.WinForms.ImagXpress9;


namespace CommandLineApp
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	/// 

	class CommandLineApp
	{

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			//
			// TODO: Add code to start application here
			//

			UnlockIXandProcessImg UnlockIXandProcessImg = new UnlockIXandProcessImg();
					
		}
	}

	public class UnlockIXandProcessImg
	{
		private PegasusImaging.WinForms.ImagXpress9.ImageX imagX1;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.Processor imagProcessor;
		private string sInputFileName;
		private string sOutputFileName;
		private PegasusImaging.WinForms.ImagXpress9.SaveOptions soSaveOptions;
		string strCurrentDir = System.IO.Directory.GetCurrentDirectory();

		public UnlockIXandProcessImg()
		{

			try 
			{
				imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress();
				
				// use your ImagXpress 9.0 unlock codes here    
				//imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);
				System.Console.WriteLine("ImagXpress 9.0 successfully unlocked.");
				
				imagProcessor = new PegasusImaging.WinForms.ImagXpress9.Processor(imagXpress1);
				soSaveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
				soSaveOptions.Format = ImageXFormat.Tiff;
				soSaveOptions.Tiff.Compression = Compression.Group4;
				sInputFileName = System.IO.Path.Combine(strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\Benefits.tif");
				sOutputFileName = (strCurrentDir + "\\BenefitsRotated.tif");

				imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, sInputFileName);
				imagProcessor.Image = imagX1;
				imagProcessor.Rotate(180);
				imagX1 = imagProcessor.Image;
				imagX1.Save(sOutputFileName, soSaveOptions);
				
				Dispose();
				System.Console.WriteLine(("Rotated TIFF saved to file " + sOutputFileName));
				System.Console.ReadLine();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				Dispose();
				System.Console.WriteLine(ex.Message);
				System.Console.WriteLine(ex.Source);
				System.Console.ReadLine();
			}
			catch (System.Exception ex) 
			{
				Dispose();
				System.Console.WriteLine(ex.Message);
				System.Console.WriteLine(ex.Source);
				System.Console.ReadLine();
			}
		}

		// Don't forget to Dispose ImagXpress
		void Dispose() 
		{
			if (!(imagXpress1 == null)) 
			{
				imagXpress1.Dispose();
				imagXpress1 = null;
			}
			if (!(imagProcessor == null)) 
			{
				imagProcessor.Dispose();
				imagProcessor = null;
			}
			if (!(imagX1 == null)) 
			{
				imagX1.Dispose();
				imagX1 = null;
			}
		}

	}
}

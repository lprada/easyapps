/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class RemoveScratchesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveScratchesForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.AdjBox = new System.Windows.Forms.NumericUpDown();
            this.SmoothBox = new System.Windows.Forms.NumericUpDown();
            this.ThresholdBox = new System.Windows.Forms.NumericUpDown();
            this.WidthBox = new System.Windows.Forms.NumericUpDown();
            this.EndYBox = new System.Windows.Forms.NumericUpDown();
            this.StartYBox = new System.Windows.Forms.NumericUpDown();
            this.EndXBox = new System.Windows.Forms.NumericUpDown();
            this.StartXBox = new System.Windows.Forms.NumericUpDown();
            this.labelSmoothing = new System.Windows.Forms.Label();
            this.labelAdjustment = new System.Windows.Forms.Label();
            this.labelThreshold = new System.Windows.Forms.Label();
            this.labelWidth = new System.Windows.Forms.Label();
            this.comboBoxScratchType = new System.Windows.Forms.ComboBox();
            this.labelScratchType = new System.Windows.Forms.Label();
            this.labelEndingPointSeperator = new System.Windows.Forms.Label();
            this.labelStartingPointSeperator = new System.Windows.Forms.Label();
            this.labelEndingPoint = new System.Windows.Forms.Label();
            this.labelStartingPoint = new System.Windows.Forms.Label();
            this.listBoxDescription = new System.Windows.Forms.ListBox();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AdjBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmoothBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThresholdBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartXBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(401, 549);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(635, 549);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(517, 549);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(10, 549);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 36;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            // 
            // AdjBox
            // 
            this.AdjBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AdjBox.Location = new System.Drawing.Point(410, 498);
            this.AdjBox.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.AdjBox.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            -2147483648});
            this.AdjBox.Name = "AdjBox";
            this.AdjBox.Size = new System.Drawing.Size(51, 20);
            this.AdjBox.TabIndex = 54;
            this.AdjBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // SmoothBox
            // 
            this.SmoothBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SmoothBox.Location = new System.Drawing.Point(527, 498);
            this.SmoothBox.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            -2147483648});
            this.SmoothBox.Name = "SmoothBox";
            this.SmoothBox.Size = new System.Drawing.Size(51, 20);
            this.SmoothBox.TabIndex = 53;
            this.SmoothBox.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // ThresholdBox
            // 
            this.ThresholdBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ThresholdBox.Location = new System.Drawing.Point(527, 473);
            this.ThresholdBox.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ThresholdBox.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.ThresholdBox.Name = "ThresholdBox";
            this.ThresholdBox.Size = new System.Drawing.Size(51, 20);
            this.ThresholdBox.TabIndex = 52;
            this.ThresholdBox.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // WidthBox
            // 
            this.WidthBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.WidthBox.Location = new System.Drawing.Point(410, 473);
            this.WidthBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WidthBox.Name = "WidthBox";
            this.WidthBox.Size = new System.Drawing.Size(51, 20);
            this.WidthBox.TabIndex = 51;
            this.WidthBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // EndYBox
            // 
            this.EndYBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EndYBox.Location = new System.Drawing.Point(270, 498);
            this.EndYBox.Name = "EndYBox";
            this.EndYBox.Size = new System.Drawing.Size(51, 20);
            this.EndYBox.TabIndex = 50;
            // 
            // StartYBox
            // 
            this.StartYBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StartYBox.Location = new System.Drawing.Point(270, 473);
            this.StartYBox.Name = "StartYBox";
            this.StartYBox.Size = new System.Drawing.Size(51, 20);
            this.StartYBox.TabIndex = 49;
            // 
            // EndXBox
            // 
            this.EndXBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EndXBox.Location = new System.Drawing.Point(188, 498);
            this.EndXBox.Name = "EndXBox";
            this.EndXBox.Size = new System.Drawing.Size(51, 20);
            this.EndXBox.TabIndex = 48;
            // 
            // StartXBox
            // 
            this.StartXBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StartXBox.Location = new System.Drawing.Point(188, 473);
            this.StartXBox.Name = "StartXBox";
            this.StartXBox.Size = new System.Drawing.Size(51, 20);
            this.StartXBox.TabIndex = 47;
            // 
            // labelSmoothing
            // 
            this.labelSmoothing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSmoothing.AutoSize = true;
            this.labelSmoothing.Location = new System.Drawing.Point(467, 505);
            this.labelSmoothing.Name = "labelSmoothing";
            this.labelSmoothing.Size = new System.Drawing.Size(60, 13);
            this.labelSmoothing.TabIndex = 46;
            this.labelSmoothing.Text = "Smoothing:";
            // 
            // labelAdjustment
            // 
            this.labelAdjustment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelAdjustment.AutoSize = true;
            this.labelAdjustment.Location = new System.Drawing.Point(342, 501);
            this.labelAdjustment.Name = "labelAdjustment";
            this.labelAdjustment.Size = new System.Drawing.Size(62, 13);
            this.labelAdjustment.TabIndex = 45;
            this.labelAdjustment.Text = "Adjustment:";
            // 
            // labelThreshold
            // 
            this.labelThreshold.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelThreshold.AutoSize = true;
            this.labelThreshold.Location = new System.Drawing.Point(467, 475);
            this.labelThreshold.Name = "labelThreshold";
            this.labelThreshold.Size = new System.Drawing.Size(57, 13);
            this.labelThreshold.TabIndex = 44;
            this.labelThreshold.Text = "Threshold:";
            // 
            // labelWidth
            // 
            this.labelWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelWidth.AutoSize = true;
            this.labelWidth.Location = new System.Drawing.Point(342, 475);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(38, 13);
            this.labelWidth.TabIndex = 43;
            this.labelWidth.Text = "Width:";
            // 
            // comboBoxScratchType
            // 
            this.comboBoxScratchType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxScratchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScratchType.FormattingEnabled = true;
            this.comboBoxScratchType.Items.AddRange(new object[] {
            "Light Defect",
            "Dark Defect"});
            this.comboBoxScratchType.Location = new System.Drawing.Point(208, 534);
            this.comboBoxScratchType.Name = "comboBoxScratchType";
            this.comboBoxScratchType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxScratchType.TabIndex = 42;
            // 
            // labelScratchType
            // 
            this.labelScratchType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelScratchType.AutoSize = true;
            this.labelScratchType.Location = new System.Drawing.Point(128, 537);
            this.labelScratchType.Name = "labelScratchType";
            this.labelScratchType.Size = new System.Drawing.Size(74, 13);
            this.labelScratchType.TabIndex = 41;
            this.labelScratchType.Text = "Scratch Type:";
            // 
            // labelEndingPointSeperator
            // 
            this.labelEndingPointSeperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelEndingPointSeperator.AutoSize = true;
            this.labelEndingPointSeperator.Location = new System.Drawing.Point(252, 501);
            this.labelEndingPointSeperator.Name = "labelEndingPointSeperator";
            this.labelEndingPointSeperator.Size = new System.Drawing.Size(10, 13);
            this.labelEndingPointSeperator.TabIndex = 40;
            this.labelEndingPointSeperator.Text = ",";
            // 
            // labelStartingPointSeperator
            // 
            this.labelStartingPointSeperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStartingPointSeperator.AutoSize = true;
            this.labelStartingPointSeperator.Location = new System.Drawing.Point(252, 475);
            this.labelStartingPointSeperator.Name = "labelStartingPointSeperator";
            this.labelStartingPointSeperator.Size = new System.Drawing.Size(10, 13);
            this.labelStartingPointSeperator.TabIndex = 39;
            this.labelStartingPointSeperator.Text = ",";
            // 
            // labelEndingPoint
            // 
            this.labelEndingPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelEndingPoint.AutoSize = true;
            this.labelEndingPoint.Location = new System.Drawing.Point(108, 501);
            this.labelEndingPoint.Name = "labelEndingPoint";
            this.labelEndingPoint.Size = new System.Drawing.Size(70, 13);
            this.labelEndingPoint.TabIndex = 38;
            this.labelEndingPoint.Text = "Ending Point:";
            // 
            // labelStartingPoint
            // 
            this.labelStartingPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStartingPoint.AutoSize = true;
            this.labelStartingPoint.Location = new System.Drawing.Point(108, 475);
            this.labelStartingPoint.Name = "labelStartingPoint";
            this.labelStartingPoint.Size = new System.Drawing.Size(73, 13);
            this.labelStartingPoint.TabIndex = 37;
            this.labelStartingPoint.Text = "Starting Point:";
            // 
            // listBoxDescription
            // 
            this.listBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxDescription.FormattingEnabled = true;
            this.listBoxDescription.Items.AddRange(new object[] {
            "Please use the mouse on the Current Image ",
            "to select an Area to Remove Scratches."});
            this.listBoxDescription.Location = new System.Drawing.Point(10, 424);
            this.listBoxDescription.Name = "listBoxDescription";
            this.listBoxDescription.Size = new System.Drawing.Size(259, 43);
            this.listBoxDescription.TabIndex = 55;
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 57;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 56;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            this.imageXViewCurrent.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageXViewCurrent_MouseUp);
            this.imageXViewCurrent.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageXViewCurrent_MouseDown);
            this.imageXViewCurrent.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageXViewCurrent_MouseMove);
            // 
            // RemoveScratchesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 583);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.listBoxDescription);
            this.Controls.Add(this.AdjBox);
            this.Controls.Add(this.SmoothBox);
            this.Controls.Add(this.ThresholdBox);
            this.Controls.Add(this.WidthBox);
            this.Controls.Add(this.EndYBox);
            this.Controls.Add(this.StartYBox);
            this.Controls.Add(this.EndXBox);
            this.Controls.Add(this.StartXBox);
            this.Controls.Add(this.labelSmoothing);
            this.Controls.Add(this.labelAdjustment);
            this.Controls.Add(this.labelThreshold);
            this.Controls.Add(this.labelWidth);
            this.Controls.Add(this.comboBoxScratchType);
            this.Controls.Add(this.labelScratchType);
            this.Controls.Add(this.labelEndingPointSeperator);
            this.Controls.Add(this.labelStartingPointSeperator);
            this.Controls.Add(this.labelEndingPoint);
            this.Controls.Add(this.labelStartingPoint);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RemoveScratchesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "RemoveScratches";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AdjBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SmoothBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThresholdBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WidthBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartXBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.NumericUpDown AdjBox;
        private System.Windows.Forms.NumericUpDown SmoothBox;
        private System.Windows.Forms.NumericUpDown ThresholdBox;
        private System.Windows.Forms.NumericUpDown WidthBox;
        private System.Windows.Forms.NumericUpDown EndYBox;
        private System.Windows.Forms.NumericUpDown StartYBox;
        private System.Windows.Forms.NumericUpDown EndXBox;
        private System.Windows.Forms.NumericUpDown StartXBox;
        private System.Windows.Forms.Label labelSmoothing;
        private System.Windows.Forms.Label labelAdjustment;
        private System.Windows.Forms.Label labelThreshold;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.ComboBox comboBoxScratchType;
        private System.Windows.Forms.Label labelScratchType;
        private System.Windows.Forms.Label labelEndingPointSeperator;
        private System.Windows.Forms.Label labelStartingPointSeperator;
        private System.Windows.Forms.Label labelEndingPoint;
        private System.Windows.Forms.Label labelStartingPoint;
        private System.Windows.Forms.ListBox listBoxDescription;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
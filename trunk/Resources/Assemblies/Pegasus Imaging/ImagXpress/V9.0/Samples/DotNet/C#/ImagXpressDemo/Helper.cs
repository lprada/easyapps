/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.NotateXpress9;
using PegasusImaging.WinForms.PdfXpress2;
using PegasusImaging.WinForms.PrintPro4;
using PegasusImaging.WinForms.ThumbnailXpress2;
using PegasusImaging.WinForms.TwainPro5;

namespace ImagXpressDemo
{
    class Helper
    {
        public string gettingStartedFile = "startup.txt";
        public string quickStartFile = "quickstart.txt";
        public string gettingStartedKey = "NO";

        public string OpenImageFilter = @"All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.emf;*.gif;*.hdp;*.ica;*.ico;*.jb2;*.jbig2;*.jp2;*.jls;*.jpg;*.jif;*.jpeg;*.ljp;*.pbm;*.pcx;*.pdf;*.pgm;*.pic;*.png;*.ppm;*.raw;*.tiff;*.tif;*.tga;*.wbmp;*.wmf;*.wpg;*.wsq;*.wdp;|Windows Bitmap (*.BMP)|*.bmp|Tagged Image Format (*.TIFF, *.TIF)|*.tif;*.tiff|Portable Document Format (*.PDF, *.TPDF)|*.pdf;*.tpdf|JFIF Compliant JPEG (*.JPG, *.JIF, *.JPEG)|*.jpg;*.jif;*.jpeg|EXIF JPG (.*JPG)|*.jpg|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Computer Aided Acquisition and Logistics Support Raster Format (*.CAL)|*.cal|Lossless JPEG (*.LJP)|*.ljp|JPEG LS (*.JLS)|*.jls|Pegasus Imaging Corporation or Enhanced (*.PIC, *.EPIC)|*.pic;*.epic|CompuServe Graphics Interchange Format (*.GIF)|*.gif|Portable Network Graphics (*.PNG)|*.png|Truevision TARGA (*.TGA)|*.tga|ZSoft PaintBrush (*.PCX)|*.pcx|ZSoft Multiple Page (*.DCX)|*.dcx|Windows Device Independent Bitmap (*.DIB)|*.dib|WordPerfect Graphic (*.WPG)|*.wpg|Windows Metafile Format (*.WMF)|*.wmf|Enhanced MetaFile Foramt (*.EMF)|*.emf|JPEG 2000 (*.JP2)|*.jp2|Wireless Bitmap (*.WBMP)|*.wbmp|Independent Computing Architecture or IOCA (*.ICA)|*.ica|ICON (*.ICO)|*.ico|Portable Pixmap (*.PPM)|*.ppm|Portable Graymap (*.PGM)|*.pgm|Portable Bitmap (*.PBM)|*.pbm|Wavelet Scalar Quantization Fingerprint File (*.WSQ)|*.wsq|RAW (*.RAW)|*.RAW|JBIG2 File (*.JB2)|*.jb2|Drawing (*.DWG)|*.dwg|Drawing Exchange Format (*.DXF)|*.dxf|Design Web Format (*.DWF)|*.dwf|HD Photo/JPEG XR (*.WDP, *.HDP)|*.wdp;*.hdp|All Files (*.*)|*.*";

        public string SaveImageFilter = @"Windows Bitmap (*.BMP)|*.bmp|Tagged Image Format (*.TIFF, *.TIF)|*.tif;*.tiff|Portable Document Format (*.PDF, *.TPDF)|*.pdf;*.tpdf|JFIF Compliant JPEG (*.JPG, *.JIF, *.JPEG)|*.jpg;*.jif;*.jpeg|EXIF JPG (.*JPG)|*.jpg|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Computer Aided Acquisition and Logistics Support Raster Format (*.CAL)|*.cal|Lossless JPEG (*.LJP)|*.ljp|JPEG LS (*.JLS)|*.jls|Pegasus Imaging Corporation or Enhanced (*.PIC, *.EPIC)|*.pic;*.epic|CompuServe Graphics Interchange Format (*.GIF)|*.gif|Portable Network Graphics (*.PNG)|*.png|Truevision TARGA (*.TGA)|*.tga|ZSoft PaintBrush (*.PCX)|*.pcx|ZSoft Multiple Page (*.DCX)|*.dcx|Windows Device Independent Bitmap (*.DIB)|*.dib|WordPerfect Graphic (*.WPG)|*.wpg|JPEG 2000 (*.JP2)|*.jp2|Wireless Bitmap (*.WBMP)|*.wbmp|ICON (*.ICO)|*.ico|Portable Pixmap (*.PPM)|*.ppm|Portable Graymap (*.PGM)|*.pgm|Portable Bitmap (*.PBM)|*.pbm|Wavelet Scalar Quantization Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|HD Photo/JPEG XR (*.WDP, *.HDP)|*.wdp;*.hdp";

        string commonImagePath, fontPath, cmapPath, iconsPath;

        const int imagXpressRuntime1 = 1234;
        const int imagXpressRuntime2 = 1234;
        const int imagXpressRuntime3 = 1234;
        const int imagXpressRuntime4 = 1234;
        const int thumbnailXpressRuntime1 = 1234;
        const int thumbnailXpressRuntime2 = 1234;
        const int thumbnailXpressRuntime3 = 1234;
        const int thumbnailXpressRuntime4 = 1234;
        const int pdfXpressRuntime1 = 1234;
        const int pdfXpressRuntime2 = 1234;
        const int pdfXpressRuntime3 = 1234;
        const int pdfXpressRuntime4 = 1234;
        const int twainProRuntime1 = 1234;
        const int twainProRuntime2 = 1234;
        const int twainProRuntime3 = 1234;
        const int twainProRuntime4 = 1234;
        const int printProRuntime1 = 1234;
        const int printProRuntime2 = 1234;
        const int printProRuntime3 = 1234;
        const int printProRuntime4 = 1234;

        //******************************************************************************************************
        //You must call the UnlockRuntime() method to unlock the control.
        //The 1234's are not actual unlock codes and are for illustrative purposes only.
        //Runtime licensing (aka unlock codes) must be obtained in order to unlock the control on distribution,
        //if you don't have them you will need to contact Sales (sales@jpg.com) to purchase them.
        //******************************************************************************************************

        public void UnlockControls(Component component)
        {
            if (component is ImagXpress)
            {
                ((ImagXpress)component).Licensing.UnlockRuntime(imagXpressRuntime1, imagXpressRuntime2,
                                                                imagXpressRuntime3, imagXpressRuntime4);
            }
            else if (component is ThumbnailXpress)
            {
                ((ThumbnailXpress)component).Licensing.UnlockIXRuntime(imagXpressRuntime1, imagXpressRuntime2,
                                                                       imagXpressRuntime3, imagXpressRuntime4);
                ((ThumbnailXpress)component).Licensing.UnlockPdfXpressRuntime(pdfXpressRuntime1, pdfXpressRuntime2,
                                                                              pdfXpressRuntime3, pdfXpressRuntime4);
                ((ThumbnailXpress)component).Licensing.UnlockRuntime(thumbnailXpressRuntime1, thumbnailXpressRuntime2,
                                                                     thumbnailXpressRuntime3, thumbnailXpressRuntime4);
            }
            else if (component is PdfXpress)
            {
                ((PdfXpress)component).Licensing.UnlockRuntime(pdfXpressRuntime1, pdfXpressRuntime2,
                                                               pdfXpressRuntime3, pdfXpressRuntime4);
            }
            else if (component is TwainPro)
            {
                ((TwainPro)component).Licensing.UnlockRuntime(twainProRuntime1, twainProRuntime2, twainProRuntime3, twainProRuntime4);
            }
            else if (component is PrintPro)
            {
                ((PrintPro)component).Licensing.UnlockRuntime(printProRuntime1, printProRuntime2, printProRuntime3, printProRuntime4);
            }
        }

        public void ShowHelp(Control control, string Keyword)
        {
            if (Keyword.Length > 0)
            {
                Help.ShowHelp(control, "PegasusImaging.WinForms.ImagXpress9.chm",
                     "PegasusImaging.WinForms.ImagXpress9~PegasusImaging.WinForms.ImagXpress9.Processor~" + Keyword + ".html");
            }
            else
            {
                Help.ShowHelp(control, "PegasusImaging.WinForms.ImagXpress9.chm");
            }
        }

        public void IdentifyLoadOptionUI(int index, LoadOptionsForm loadForm)
        {
            Color backgroundColor = Color.LightBlue;

            switch (index)
            {
                case 3:
                    {
                        loadForm.TiffBackgroundColor = backgroundColor;
                        break;
                    }
                case 5:
                    {
                        loadForm.JpegBackgroundColor = backgroundColor;
                        break;
                    }
                case 11:
                    {
                        loadForm.PicBackgroundColor = backgroundColor;
                        break;
                    }
                case 19:
                case 20:
                    {
                        loadForm.MetaBackgroundColor = backgroundColor;
                        break;
                    }
                case 29:
                    {
                        loadForm.Jbig2BackgroundColor = backgroundColor;
                        break;
                    }
                case 30:
                case 31:
                case 32:
                    {
                        loadForm.CadBackgroundColor = backgroundColor;
                        break;
                    }
                case 33:
                    {
                        loadForm.HdpBackgroundColor = backgroundColor;
                        break;
                    }
            }
        }

        public void IdentifySaveOptionUI(int index, SaveOptionsForm saveForm, PegasusImaging.WinForms.ImagXpress9.SaveOptions so)
        {
            Color backgroundColor = Color.LightBlue;

            switch (index)
            {
                case 1:
                    {
                        so.Format = ImageXFormat.Bmp;
                        break;
                    }
                case 2:
                    {
                        so.Format = ImageXFormat.Tiff;
                        saveForm.TiffBackgroundColor = backgroundColor;
                        break;
                    }
                case 3:
                    {
                        so.Format = ImageXFormat.Pdf;
                        saveForm.PdfBackgroundColor = backgroundColor;
                        break;
                    }
                case 4:
                    {
                        so.Format = ImageXFormat.Jpeg;
                        saveForm.JpgBackgroundColor = backgroundColor;
                        break;
                    }
                case 5:
                    {
                        so.Format = ImageXFormat.Exif;
                        saveForm.ExifBackgroundColor = backgroundColor;
                        break;
                    }
                case 6:
                    {
                        so.Format = ImageXFormat.Modca;
                        break;
                    }
                case 7:
                    {
                        so.Format = ImageXFormat.Cals;
                        break;
                    }
                case 8:
                    {
                        so.Format = ImageXFormat.LosslessJpeg;
                        saveForm.LjpBackgroundColor = backgroundColor;
                        break;
                    }
                case 9:
                    {
                        so.Format = ImageXFormat.JpegLs;
                        saveForm.JlsBackgroundColor = backgroundColor;
                        break;
                    }
                case 10:
                    {
                        so.Format = ImageXFormat.Pic;
                        saveForm.PicBackgroundColor = backgroundColor;
                        break;
                    }
                case 11:
                    {
                        so.Format = ImageXFormat.Gif;
                        saveForm.GifBackgroundColor = backgroundColor;
                        break;
                    }
                case 12:
                    {
                        so.Format = ImageXFormat.Png;
                        saveForm.PngBackgroundColor = backgroundColor;
                        break;
                    }
                case 13:
                    {
                        so.Format = ImageXFormat.Tga;
                        break;
                    }
                case 14:
                    {
                        so.Format = ImageXFormat.Pcx;
                        break;
                    }
                case 15:
                    {
                        so.Format = ImageXFormat.Dcx;
                        saveForm.DcxBackgroundColor = backgroundColor;
                        break;
                    }
                case 16:
                    {
                        so.Format = ImageXFormat.Dib;
                        break;
                    }
                case 17:
                    {
                        so.Format = ImageXFormat.Wpg;
                        break;
                    }
                case 18:
                    {
                        so.Format = ImageXFormat.Jpeg2000;
                        saveForm.Jp2BackgroundColor = backgroundColor;
                        break;
                    }
                case 19:
                    {
                        so.Format = ImageXFormat.WirelessBmp;
                        break;
                    }
                case 20:
                    {
                        so.Format = ImageXFormat.Ico;
                        saveForm.IconBackgroundColor = backgroundColor;
                        break;
                    }
                case 21:
                    {
                        so.Format = ImageXFormat.Ppm;
                        saveForm.PortableBackgroundColor = backgroundColor;
                        break;
                    }
                case 22:
                    {
                        so.Format = ImageXFormat.Pgm;
                        saveForm.PortableBackgroundColor = backgroundColor;
                        break;
                    }
                case 23:
                    {
                        so.Format = ImageXFormat.Pbm;
                        saveForm.PortableBackgroundColor = backgroundColor;
                        break;
                    }
                case 24:
                    {
                        so.Format = ImageXFormat.Wsq;
                        saveForm.WsqBackgroundColor = backgroundColor;
                        break;
                    }
                case 25:
                    {
                        so.Format = ImageXFormat.Jbig2;
                        saveForm.JbigBackgroundColor = backgroundColor;
                        break;
                    }
                case 26:
                    {
                        so.Format = ImageXFormat.HdPhoto;
                        saveForm.HdpBackgroundColor = backgroundColor;
                        break;
                    }
            }
        }

        public string[] GetPath()
        {
            //get index of last "\\"
            int startIndex = Application.StartupPath.LastIndexOf("\\");

            string startPath = "";

            //identify the parent directory of the exe
            if (startIndex != -1)
                startPath = Application.StartupPath.Substring(startIndex + 1);

            //if it's the licensed demo or not set the paths accordingly
            if (startPath == "ImagXpressLicensedDemo")
            {
                commonImagePath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

                fontPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v2.0\\Support\\Font\\";
                cmapPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v2.0\\Support\\CMap\\";

                iconsPath = Application.StartupPath + "..\\..\\Images\\";
            }
            else
            {
                commonImagePath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

                fontPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v2.0\\Support\\Font\\";
                cmapPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v2.0\\Support\\CMap\\";

                iconsPath = Application.StartupPath + "..\\..\\..\\Images\\";
            }

            return new string[] { commonImagePath, fontPath, cmapPath , iconsPath};
        }

        public void CenterScreen(Form form)
        {
            //want to keep main form on primary screen
            Screen screen = Screen.PrimaryScreen;

            form.StartPosition = FormStartPosition.Manual;

            //equivalent StartPosition of CenterScreen
            form.Location = new Point((screen.WorkingArea.Width - form.Width) / 2, (screen.WorkingArea.Height - form.Height) / 2);
        }

        public int GetSaveAsIndex(ImageXFormat fileFormat)
        {
            switch (fileFormat)
            {
                case ImageXFormat.Bmp:
                    {
                        return 1;
                    }
                case ImageXFormat.Tiff:
                    {
                        return 2;
                    }
                case ImageXFormat.Pdf:
                    {
                        return 3;
                    }
                case ImageXFormat.Jpeg:
                    {
                        return 4;
                    }
                case ImageXFormat.Exif:
                    {
                        return 5;
                    }
                case ImageXFormat.Modca:
                    {
                        return 6;
                    }
                case ImageXFormat.Cals:
                    {
                        return 7;
                    }
                case ImageXFormat.LosslessJpeg:
                    {
                        return 8;
                    }
                case ImageXFormat.JpegLs:
                    {
                        return 9;
                    }
                case ImageXFormat.Pic:
                    {
                        return 10;
                    }
                case ImageXFormat.Gif:
                    {
                        return 11;
                    }
                case ImageXFormat.Png:
                    {
                        return 12;
                    }
                case ImageXFormat.Tga:
                    {
                        return 13;
                    }
                case ImageXFormat.Pcx:
                    {
                        return 14;
                    }
                case ImageXFormat.Dcx:
                    {
                        return 15;
                    }
                case ImageXFormat.Dib:
                    {
                        return 16;
                    }
                case ImageXFormat.Wpg:
                    {
                        return 17;
                    }
                case ImageXFormat.Jpeg2000:
                    {
                        return 18;
                    }
                case ImageXFormat.WirelessBmp:
                    {
                        return 19;
                    }
                case ImageXFormat.Ico:
                    {
                        return 20;
                    }
                case ImageXFormat.Ppm:
                    {
                        return 21;
                    }
                case ImageXFormat.Pgm:
                    {
                        return 22;
                    }
                case ImageXFormat.Pbm:
                    {
                        return 23;
                    }
                case ImageXFormat.Ioca:
                    {
                        return 24;
                    }
                case ImageXFormat.Wsq:
                    {
                        return 25;
                    }
                case ImageXFormat.Jbig2:
                    {
                        return 26;
                    }
                case ImageXFormat.HdPhoto:
                    {
                        return 27;
                    }
                default:
                    {
                        return 1;
                    }
            }
        }
    }
}

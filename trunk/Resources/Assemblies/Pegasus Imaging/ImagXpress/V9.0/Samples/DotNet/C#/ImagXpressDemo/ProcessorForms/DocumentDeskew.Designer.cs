/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class DocumentDeskewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentDeskewForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.AngleBox = new System.Windows.Forms.NumericUpDown();
            this.ConfBox = new System.Windows.Forms.NumericUpDown();
            this.MaintainCheckBox = new System.Windows.Forms.CheckBox();
            this.QualityBox = new System.Windows.Forms.NumericUpDown();
            this.Variationlabel = new System.Windows.Forms.Label();
            this.ConfidenceLabel = new System.Windows.Forms.Label();
            this.RotationLabel = new System.Windows.Forms.Label();
            this.AngleLabel = new System.Windows.Forms.Label();
            this.ConfLabel = new System.Windows.Forms.Label();
            this.PadLabel = new System.Windows.Forms.Label();
            this.QualityLabel = new System.Windows.Forms.Label();
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.PadComboBox = new System.Windows.Forms.ComboBox();
            this.labelRotation = new System.Windows.Forms.Label();
            this.labelConfidence = new System.Windows.Forms.Label();
            this.labelVariation = new System.Windows.Forms.Label();
            this.labelModifyResult = new System.Windows.Forms.Label();
            this.labelModified = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.AngleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualityBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(396, 561);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(634, 561);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(516, 561);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // AngleBox
            // 
            this.AngleBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AngleBox.DecimalPlaces = 1;
            this.AngleBox.Location = new System.Drawing.Point(128, 418);
            this.AngleBox.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.AngleBox.Name = "AngleBox";
            this.AngleBox.Size = new System.Drawing.Size(72, 20);
            this.AngleBox.TabIndex = 3;
            this.AngleBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            65536});
            // 
            // ConfBox
            // 
            this.ConfBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ConfBox.Location = new System.Drawing.Point(128, 444);
            this.ConfBox.Name = "ConfBox";
            this.ConfBox.Size = new System.Drawing.Size(72, 20);
            this.ConfBox.TabIndex = 4;
            this.ConfBox.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // MaintainCheckBox
            // 
            this.MaintainCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MaintainCheckBox.AutoSize = true;
            this.MaintainCheckBox.Location = new System.Drawing.Point(221, 420);
            this.MaintainCheckBox.Name = "MaintainCheckBox";
            this.MaintainCheckBox.Size = new System.Drawing.Size(127, 17);
            this.MaintainCheckBox.TabIndex = 6;
            this.MaintainCheckBox.Text = "Maintain Original Size";
            this.MaintainCheckBox.UseVisualStyleBackColor = true;
            // 
            // QualityBox
            // 
            this.QualityBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.QualityBox.Location = new System.Drawing.Point(67, 481);
            this.QualityBox.Name = "QualityBox";
            this.QualityBox.Size = new System.Drawing.Size(72, 20);
            this.QualityBox.TabIndex = 7;
            this.QualityBox.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // Variationlabel
            // 
            this.Variationlabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Variationlabel.AutoSize = true;
            this.Variationlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Variationlabel.Location = new System.Drawing.Point(303, 527);
            this.Variationlabel.Name = "Variationlabel";
            this.Variationlabel.Size = new System.Drawing.Size(14, 13);
            this.Variationlabel.TabIndex = 8;
            this.Variationlabel.Text = "0";
            // 
            // ConfidenceLabel
            // 
            this.ConfidenceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ConfidenceLabel.AutoSize = true;
            this.ConfidenceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfidenceLabel.Location = new System.Drawing.Point(303, 505);
            this.ConfidenceLabel.Name = "ConfidenceLabel";
            this.ConfidenceLabel.Size = new System.Drawing.Size(14, 13);
            this.ConfidenceLabel.TabIndex = 9;
            this.ConfidenceLabel.Text = "0";
            // 
            // RotationLabel
            // 
            this.RotationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.RotationLabel.AutoSize = true;
            this.RotationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RotationLabel.Location = new System.Drawing.Point(303, 483);
            this.RotationLabel.Name = "RotationLabel";
            this.RotationLabel.Size = new System.Drawing.Size(14, 13);
            this.RotationLabel.TabIndex = 10;
            this.RotationLabel.Text = "0";
            // 
            // AngleLabel
            // 
            this.AngleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AngleLabel.AutoSize = true;
            this.AngleLabel.Location = new System.Drawing.Point(12, 420);
            this.AngleLabel.Name = "AngleLabel";
            this.AngleLabel.Size = new System.Drawing.Size(81, 13);
            this.AngleLabel.TabIndex = 11;
            this.AngleLabel.Text = "Minimum Angle:";
            // 
            // ConfLabel
            // 
            this.ConfLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ConfLabel.AutoSize = true;
            this.ConfLabel.Location = new System.Drawing.Point(13, 446);
            this.ConfLabel.Name = "ConfLabel";
            this.ConfLabel.Size = new System.Drawing.Size(108, 13);
            this.ConfLabel.TabIndex = 12;
            this.ConfLabel.Text = "Minimum Confidence:";
            // 
            // PadLabel
            // 
            this.PadLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PadLabel.AutoSize = true;
            this.PadLabel.Location = new System.Drawing.Point(218, 451);
            this.PadLabel.Name = "PadLabel";
            this.PadLabel.Size = new System.Drawing.Size(56, 13);
            this.PadLabel.TabIndex = 13;
            this.PadLabel.Text = "Pad Color:";
            // 
            // QualityLabel
            // 
            this.QualityLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.QualityLabel.AutoSize = true;
            this.QualityLabel.Location = new System.Drawing.Point(13, 483);
            this.QualityLabel.Name = "QualityLabel";
            this.QualityLabel.Size = new System.Drawing.Size(39, 13);
            this.QualityLabel.TabIndex = 14;
            this.QualityLabel.Text = "Quality";
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(12, 561);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 15;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // PadComboBox
            // 
            this.PadComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PadComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PadComboBox.FormattingEnabled = true;
            this.PadComboBox.Items.AddRange(new object[] {
            "Black",
            "White"});
            this.PadComboBox.Location = new System.Drawing.Point(280, 448);
            this.PadComboBox.Name = "PadComboBox";
            this.PadComboBox.Size = new System.Drawing.Size(81, 21);
            this.PadComboBox.TabIndex = 16;
            // 
            // labelRotation
            // 
            this.labelRotation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRotation.AutoSize = true;
            this.labelRotation.Location = new System.Drawing.Point(194, 483);
            this.labelRotation.Name = "labelRotation";
            this.labelRotation.Size = new System.Drawing.Size(80, 13);
            this.labelRotation.TabIndex = 17;
            this.labelRotation.Text = "Rotation Angle:";
            // 
            // labelConfidence
            // 
            this.labelConfidence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelConfidence.AutoSize = true;
            this.labelConfidence.Location = new System.Drawing.Point(194, 505);
            this.labelConfidence.Name = "labelConfidence";
            this.labelConfidence.Size = new System.Drawing.Size(64, 13);
            this.labelConfidence.TabIndex = 18;
            this.labelConfidence.Text = "Confidence:";
            // 
            // labelVariation
            // 
            this.labelVariation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelVariation.AutoSize = true;
            this.labelVariation.Location = new System.Drawing.Point(194, 527);
            this.labelVariation.Name = "labelVariation";
            this.labelVariation.Size = new System.Drawing.Size(51, 13);
            this.labelVariation.TabIndex = 19;
            this.labelVariation.Text = "Variation:";
            // 
            // labelModifyResult
            // 
            this.labelModifyResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelModifyResult.AutoSize = true;
            this.labelModifyResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifyResult.Location = new System.Drawing.Point(303, 551);
            this.labelModifyResult.Name = "labelModifyResult";
            this.labelModifyResult.Size = new System.Drawing.Size(37, 13);
            this.labelModifyResult.TabIndex = 52;
            this.labelModifyResult.Text = "False";
            this.labelModifyResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelModified
            // 
            this.labelModified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelModified.AutoSize = true;
            this.labelModified.Location = new System.Drawing.Point(194, 551);
            this.labelModified.Name = "labelModified";
            this.labelModified.Size = new System.Drawing.Size(103, 13);
            this.labelModified.TabIndex = 51;
            this.labelModified.Text = "Image was modified:";
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 54;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 53;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // DocumentDeskewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 596);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.labelModifyResult);
            this.Controls.Add(this.labelModified);
            this.Controls.Add(this.labelVariation);
            this.Controls.Add(this.labelConfidence);
            this.Controls.Add(this.labelRotation);
            this.Controls.Add(this.PadComboBox);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.QualityLabel);
            this.Controls.Add(this.PadLabel);
            this.Controls.Add(this.ConfLabel);
            this.Controls.Add(this.AngleLabel);
            this.Controls.Add(this.RotationLabel);
            this.Controls.Add(this.ConfidenceLabel);
            this.Controls.Add(this.Variationlabel);
            this.Controls.Add(this.QualityBox);
            this.Controls.Add(this.MaintainCheckBox);
            this.Controls.Add(this.ConfBox);
            this.Controls.Add(this.AngleBox);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DocumentDeskewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "DocumentDeskew";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AngleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConfBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualityBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.NumericUpDown AngleBox;
        private System.Windows.Forms.NumericUpDown ConfBox;
        private System.Windows.Forms.CheckBox MaintainCheckBox;
        private System.Windows.Forms.NumericUpDown QualityBox;
        private System.Windows.Forms.Label Variationlabel;
        private System.Windows.Forms.Label ConfidenceLabel;
        private System.Windows.Forms.Label RotationLabel;
        private System.Windows.Forms.Label AngleLabel;
        private System.Windows.Forms.Label ConfLabel;
        private System.Windows.Forms.Label PadLabel;
        private System.Windows.Forms.Label QualityLabel;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.ComboBox PadComboBox;
        private System.Windows.Forms.Label labelRotation;
        private System.Windows.Forms.Label labelConfidence;
        private System.Windows.Forms.Label labelVariation;
        private System.Windows.Forms.Label labelModifyResult;
        private System.Windows.Forms.Label labelModified;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class SplashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
            this.DemoInfo = new System.Windows.Forms.Label();
            this.cmdOK = new System.Windows.Forms.Button();
            this.lblDisclaimer = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblPlatform = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblURL = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.BottomGroupBox = new System.Windows.Forms.GroupBox();
            this.PDFLabel = new System.Windows.Forms.Label();
            this.PrintProLabel = new System.Windows.Forms.Label();
            this.TwainProLabel = new System.Windows.Forms.Label();
            this.ThumbLabel = new System.Windows.Forms.Label();
            this.NotateLabel = new System.Windows.Forms.Label();
            this.ImagXpressLabel = new System.Windows.Forms.Label();
            this.CSharpLabel = new System.Windows.Forms.Label();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.BottomGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // DemoInfo
            // 
            this.DemoInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DemoInfo.Location = new System.Drawing.Point(85, 65);
            this.DemoInfo.Name = "DemoInfo";
            this.DemoInfo.Size = new System.Drawing.Size(584, 61);
            this.DemoInfo.TabIndex = 18;
            this.DemoInfo.Text = resources.GetString("DemoInfo.Text");
            this.DemoInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = System.Drawing.SystemColors.Control;
            this.cmdOK.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOK.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdOK.Location = new System.Drawing.Point(620, 397);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdOK.Size = new System.Drawing.Size(89, 25);
            this.cmdOK.TabIndex = 5;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // lblDisclaimer
            // 
            this.lblDisclaimer.BackColor = System.Drawing.SystemColors.Control;
            this.lblDisclaimer.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDisclaimer.Font = new System.Drawing.Font("Arial", 9.6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisclaimer.ForeColor = System.Drawing.Color.Blue;
            this.lblDisclaimer.Location = new System.Drawing.Point(115, 168);
            this.lblDisclaimer.Name = "lblDisclaimer";
            this.lblDisclaimer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDisclaimer.Size = new System.Drawing.Size(513, 30);
            this.lblDisclaimer.TabIndex = 17;
            this.lblDisclaimer.Text = "Pegasus Imaging Components Used by the ImagXpress 9 Demo Application: ";
            this.lblDisclaimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductName
            // 
            this.lblProductName.BackColor = System.Drawing.SystemColors.Control;
            this.lblProductName.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblProductName.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblProductName.Location = new System.Drawing.Point(113, 24);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblProductName.Size = new System.Drawing.Size(481, 29);
            this.lblProductName.TabIndex = 16;
            this.lblProductName.Tag = "Product";
            this.lblProductName.Text = "ImagXpress 9 Demo";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPlatform
            // 
            this.lblPlatform.AccessibleDescription = "0";
            this.lblPlatform.AutoSize = true;
            this.lblPlatform.BackColor = System.Drawing.SystemColors.Control;
            this.lblPlatform.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblPlatform.Font = new System.Drawing.Font("Arial", 15.6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlatform.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPlatform.Location = new System.Drawing.Point(317, 315);
            this.lblPlatform.Name = "lblPlatform";
            this.lblPlatform.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblPlatform.Size = new System.Drawing.Size(113, 24);
            this.lblPlatform.TabIndex = 15;
            this.lblPlatform.Tag = "Platform";
            this.lblPlatform.Text = "For Win32";
            this.lblPlatform.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblVersion
            // 
            this.lblVersion.AccessibleDescription = "0";
            this.lblVersion.BackColor = System.Drawing.SystemColors.Control;
            this.lblVersion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblVersion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblVersion.Location = new System.Drawing.Point(300, 350);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblVersion.Size = new System.Drawing.Size(152, 19);
            this.lblVersion.TabIndex = 14;
            this.lblVersion.Tag = "Version";
            this.lblVersion.Text = "Demo Version 9";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblURL
            // 
            this.lblURL.AccessibleDescription = "0";
            this.lblURL.AutoSize = true;
            this.lblURL.BackColor = System.Drawing.SystemColors.Control;
            this.lblURL.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblURL.Font = new System.Drawing.Font("Arial", 8.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblURL.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblURL.Location = new System.Drawing.Point(294, 402);
            this.lblURL.Name = "lblURL";
            this.lblURL.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblURL.Size = new System.Drawing.Size(158, 15);
            this.lblURL.TabIndex = 6;
            this.lblURL.Tag = "Warning";
            this.lblURL.Text = "www.pegasusimaging.com";
            this.lblURL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Logo
            // 
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(2, -3);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(728, 104);
            this.Logo.TabIndex = 22;
            this.Logo.TabStop = false;
            // 
            // lblCompany
            // 
            this.lblCompany.AccessibleDescription = "0";
            this.lblCompany.BackColor = System.Drawing.SystemColors.Control;
            this.lblCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCompany.Font = new System.Drawing.Font("Arial", 8.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCompany.Location = new System.Drawing.Point(285, 385);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCompany.Size = new System.Drawing.Size(177, 17);
            this.lblCompany.TabIndex = 4;
            this.lblCompany.Tag = "Company";
            this.lblCompany.Text = "Pegasus Imaging Corp.";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCopyright
            // 
            this.lblCopyright.AccessibleDescription = "0";
            this.lblCopyright.BackColor = System.Drawing.SystemColors.Control;
            this.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCopyright.Font = new System.Drawing.Font("Arial", 8.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCopyright.Location = new System.Drawing.Point(285, 369);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCopyright.Size = new System.Drawing.Size(177, 17);
            this.lblCopyright.TabIndex = 3;
            this.lblCopyright.Tag = "Copyright";
            this.lblCopyright.Text = "Copyright 2008";
            this.lblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BottomGroupBox
            // 
            this.BottomGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.BottomGroupBox.Controls.Add(this.PDFLabel);
            this.BottomGroupBox.Controls.Add(this.PrintProLabel);
            this.BottomGroupBox.Controls.Add(this.TwainProLabel);
            this.BottomGroupBox.Controls.Add(this.ThumbLabel);
            this.BottomGroupBox.Controls.Add(this.NotateLabel);
            this.BottomGroupBox.Controls.Add(this.ImagXpressLabel);
            this.BottomGroupBox.Controls.Add(this.CSharpLabel);
            this.BottomGroupBox.Controls.Add(this.DemoInfo);
            this.BottomGroupBox.Controls.Add(this.cmdOK);
            this.BottomGroupBox.Controls.Add(this.lblDisclaimer);
            this.BottomGroupBox.Controls.Add(this.lblProductName);
            this.BottomGroupBox.Controls.Add(this.lblPlatform);
            this.BottomGroupBox.Controls.Add(this.lblVersion);
            this.BottomGroupBox.Controls.Add(this.lblURL);
            this.BottomGroupBox.Controls.Add(this.lblCompany);
            this.BottomGroupBox.Controls.Add(this.lblCopyright);
            this.BottomGroupBox.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BottomGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BottomGroupBox.Location = new System.Drawing.Point(2, 96);
            this.BottomGroupBox.Name = "BottomGroupBox";
            this.BottomGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BottomGroupBox.Size = new System.Drawing.Size(728, 432);
            this.BottomGroupBox.TabIndex = 21;
            this.BottomGroupBox.TabStop = false;
            // 
            // PDFLabel
            // 
            this.PDFLabel.BackColor = System.Drawing.SystemColors.Control;
            this.PDFLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.PDFLabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PDFLabel.ForeColor = System.Drawing.Color.Blue;
            this.PDFLabel.Location = new System.Drawing.Point(323, 288);
            this.PDFLabel.Name = "PDFLabel";
            this.PDFLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PDFLabel.Size = new System.Drawing.Size(100, 16);
            this.PDFLabel.TabIndex = 25;
            this.PDFLabel.Text = "PDFXpress v2";
            this.PDFLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PrintProLabel
            // 
            this.PrintProLabel.BackColor = System.Drawing.SystemColors.Control;
            this.PrintProLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.PrintProLabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintProLabel.ForeColor = System.Drawing.Color.Blue;
            this.PrintProLabel.Location = new System.Drawing.Point(329, 270);
            this.PrintProLabel.Name = "PrintProLabel";
            this.PrintProLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PrintProLabel.Size = new System.Drawing.Size(90, 16);
            this.PrintProLabel.TabIndex = 24;
            this.PrintProLabel.Text = "PrintPro v4";
            this.PrintProLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TwainProLabel
            // 
            this.TwainProLabel.BackColor = System.Drawing.SystemColors.Control;
            this.TwainProLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.TwainProLabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TwainProLabel.ForeColor = System.Drawing.Color.Blue;
            this.TwainProLabel.Location = new System.Drawing.Point(323, 252);
            this.TwainProLabel.Name = "TwainProLabel";
            this.TwainProLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TwainProLabel.Size = new System.Drawing.Size(100, 16);
            this.TwainProLabel.TabIndex = 23;
            this.TwainProLabel.Text = "TwainPro v5";
            this.TwainProLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ThumbLabel
            // 
            this.ThumbLabel.BackColor = System.Drawing.SystemColors.Control;
            this.ThumbLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.ThumbLabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ThumbLabel.ForeColor = System.Drawing.Color.Blue;
            this.ThumbLabel.Location = new System.Drawing.Point(301, 236);
            this.ThumbLabel.Name = "ThumbLabel";
            this.ThumbLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ThumbLabel.Size = new System.Drawing.Size(145, 16);
            this.ThumbLabel.TabIndex = 22;
            this.ThumbLabel.Text = "ThumbnailXpress v2";
            this.ThumbLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NotateLabel
            // 
            this.NotateLabel.BackColor = System.Drawing.SystemColors.Control;
            this.NotateLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.NotateLabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotateLabel.ForeColor = System.Drawing.Color.Blue;
            this.NotateLabel.Location = new System.Drawing.Point(318, 220);
            this.NotateLabel.Name = "NotateLabel";
            this.NotateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.NotateLabel.Size = new System.Drawing.Size(118, 16);
            this.NotateLabel.TabIndex = 21;
            this.NotateLabel.Text = "NotateXpress v9";
            this.NotateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ImagXpressLabel
            // 
            this.ImagXpressLabel.BackColor = System.Drawing.SystemColors.Control;
            this.ImagXpressLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.ImagXpressLabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImagXpressLabel.ForeColor = System.Drawing.Color.Blue;
            this.ImagXpressLabel.Location = new System.Drawing.Point(323, 198);
            this.ImagXpressLabel.Name = "ImagXpressLabel";
            this.ImagXpressLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ImagXpressLabel.Size = new System.Drawing.Size(101, 22);
            this.ImagXpressLabel.TabIndex = 20;
            this.ImagXpressLabel.Text = "ImagXpress v9";
            this.ImagXpressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CSharpLabel
            // 
            this.CSharpLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CSharpLabel.Location = new System.Drawing.Point(151, 126);
            this.CSharpLabel.Name = "CSharpLabel";
            this.CSharpLabel.Size = new System.Drawing.Size(430, 42);
            this.CSharpLabel.TabIndex = 19;
            this.CSharpLabel.Text = "The ImagXpress 9 Demo application C# source code is provided in the ImagXpress 9 " +
                ".NET SDK installation.";
            this.CSharpLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SplashForm
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 524);
            this.ControlBox = false;
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.BottomGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ImagXpress 9 Demo";
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.BottomGroupBox.ResumeLayout(false);
            this.BottomGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label DemoInfo;
        public System.Windows.Forms.Button cmdOK;
        public System.Windows.Forms.Label lblDisclaimer;
        public System.Windows.Forms.Label lblProductName;
        public System.Windows.Forms.Label lblPlatform;
        public System.Windows.Forms.Label lblVersion;
        public System.Windows.Forms.Label lblURL;
        private System.Windows.Forms.PictureBox Logo;
        public System.Windows.Forms.Label lblCompany;
        public System.Windows.Forms.Label lblCopyright;
        public System.Windows.Forms.GroupBox BottomGroupBox;
        private System.Windows.Forms.Label CSharpLabel;
        public System.Windows.Forms.ToolTip ToolTip1;
        public System.Windows.Forms.Label PDFLabel;
        public System.Windows.Forms.Label PrintProLabel;
        public System.Windows.Forms.Label TwainProLabel;
        public System.Windows.Forms.Label ThumbLabel;
        public System.Windows.Forms.Label NotateLabel;
        public System.Windows.Forms.Label ImagXpressLabel;
    }
}
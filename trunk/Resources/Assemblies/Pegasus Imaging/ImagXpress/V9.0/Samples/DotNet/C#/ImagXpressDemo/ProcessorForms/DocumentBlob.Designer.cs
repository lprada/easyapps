/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class DocumentBlobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentBlobForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.WBox = new System.Windows.Forms.NumericUpDown();
            this.LBox = new System.Windows.Forms.NumericUpDown();
            this.DensityBox = new System.Windows.Forms.NumericUpDown();
            this.MaxBox = new System.Windows.Forms.NumericUpDown();
            this.MinBox = new System.Windows.Forms.NumericUpDown();
            this.HBox = new System.Windows.Forms.NumericUpDown();
            this.TBox = new System.Windows.Forms.NumericUpDown();
            this.labelBlobsFound = new System.Windows.Forms.Label();
            this.labelMinimumDensity = new System.Windows.Forms.Label();
            this.labelMaximumPixelCount = new System.Windows.Forms.Label();
            this.labelMinimumPixelCount = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.labelWidth = new System.Windows.Forms.Label();
            this.labelLeft = new System.Windows.Forms.Label();
            this.labelTop = new System.Windows.Forms.Label();
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.labelBlob = new System.Windows.Forms.Label();
            this.labelBlobFound = new System.Windows.Forms.Label();
            this.labelModified = new System.Windows.Forms.Label();
            this.labelModifyResult = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.WBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DensityBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(397, 523);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(631, 523);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(513, 523);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // WBox
            // 
            this.WBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.WBox.Location = new System.Drawing.Point(113, 461);
            this.WBox.Name = "WBox";
            this.WBox.Size = new System.Drawing.Size(65, 20);
            this.WBox.TabIndex = 45;
            // 
            // LBox
            // 
            this.LBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LBox.Location = new System.Drawing.Point(113, 435);
            this.LBox.Name = "LBox";
            this.LBox.Size = new System.Drawing.Size(65, 20);
            this.LBox.TabIndex = 44;
            // 
            // DensityBox
            // 
            this.DensityBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DensityBox.Location = new System.Drawing.Point(428, 468);
            this.DensityBox.Name = "DensityBox";
            this.DensityBox.Size = new System.Drawing.Size(65, 20);
            this.DensityBox.TabIndex = 43;
            this.DensityBox.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // MaxBox
            // 
            this.MaxBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MaxBox.Location = new System.Drawing.Point(428, 443);
            this.MaxBox.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.MaxBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaxBox.Name = "MaxBox";
            this.MaxBox.Size = new System.Drawing.Size(65, 20);
            this.MaxBox.TabIndex = 42;
            this.MaxBox.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.MaxBox.ValueChanged += new System.EventHandler(this.MaxBox_ValueChanged);
            // 
            // MinBox
            // 
            this.MinBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.MinBox.Location = new System.Drawing.Point(428, 419);
            this.MinBox.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.MinBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MinBox.Name = "MinBox";
            this.MinBox.Size = new System.Drawing.Size(65, 20);
            this.MinBox.TabIndex = 41;
            this.MinBox.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.MinBox.ValueChanged += new System.EventHandler(this.MinBox_ValueChanged);
            // 
            // HBox
            // 
            this.HBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HBox.Location = new System.Drawing.Point(231, 464);
            this.HBox.Name = "HBox";
            this.HBox.Size = new System.Drawing.Size(65, 20);
            this.HBox.TabIndex = 40;
            // 
            // TBox
            // 
            this.TBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TBox.Location = new System.Drawing.Point(229, 435);
            this.TBox.Name = "TBox";
            this.TBox.Size = new System.Drawing.Size(65, 20);
            this.TBox.TabIndex = 39;
            // 
            // labelBlobsFound
            // 
            this.labelBlobsFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBlobsFound.AutoSize = true;
            this.labelBlobsFound.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBlobsFound.Location = new System.Drawing.Point(199, 505);
            this.labelBlobsFound.Name = "labelBlobsFound";
            this.labelBlobsFound.Size = new System.Drawing.Size(14, 13);
            this.labelBlobsFound.TabIndex = 38;
            this.labelBlobsFound.Text = "0";
            this.labelBlobsFound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMinimumDensity
            // 
            this.labelMinimumDensity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMinimumDensity.AutoSize = true;
            this.labelMinimumDensity.Location = new System.Drawing.Point(312, 471);
            this.labelMinimumDensity.Name = "labelMinimumDensity";
            this.labelMinimumDensity.Size = new System.Drawing.Size(89, 13);
            this.labelMinimumDensity.TabIndex = 37;
            this.labelMinimumDensity.Text = "Minimum Density:";
            // 
            // labelMaximumPixelCount
            // 
            this.labelMaximumPixelCount.AutoSize = true;
            this.labelMaximumPixelCount.Location = new System.Drawing.Point(312, 445);
            this.labelMaximumPixelCount.Name = "labelMaximumPixelCount";
            this.labelMaximumPixelCount.Size = new System.Drawing.Size(110, 13);
            this.labelMaximumPixelCount.TabIndex = 36;
            this.labelMaximumPixelCount.Text = "Maximum Pixel Count:";
            // 
            // labelMinimumPixelCount
            // 
            this.labelMinimumPixelCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelMinimumPixelCount.AutoSize = true;
            this.labelMinimumPixelCount.Location = new System.Drawing.Point(312, 419);
            this.labelMinimumPixelCount.Name = "labelMinimumPixelCount";
            this.labelMinimumPixelCount.Size = new System.Drawing.Size(107, 13);
            this.labelMinimumPixelCount.TabIndex = 35;
            this.labelMinimumPixelCount.Text = "Minimum Pixel Count:";
            // 
            // labelHeight
            // 
            this.labelHeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelHeight.AutoSize = true;
            this.labelHeight.Location = new System.Drawing.Point(184, 464);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(41, 13);
            this.labelHeight.TabIndex = 34;
            this.labelHeight.Text = "Height:";
            // 
            // labelWidth
            // 
            this.labelWidth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelWidth.AutoSize = true;
            this.labelWidth.Location = new System.Drawing.Point(71, 464);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(38, 13);
            this.labelWidth.TabIndex = 33;
            this.labelWidth.Text = "Width:";
            // 
            // labelLeft
            // 
            this.labelLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelLeft.AutoSize = true;
            this.labelLeft.Location = new System.Drawing.Point(71, 438);
            this.labelLeft.Name = "labelLeft";
            this.labelLeft.Size = new System.Drawing.Size(28, 13);
            this.labelLeft.TabIndex = 32;
            this.labelLeft.Text = "Left:";
            // 
            // labelTop
            // 
            this.labelTop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTop.AutoSize = true;
            this.labelTop.Location = new System.Drawing.Point(184, 438);
            this.labelTop.Name = "labelTop";
            this.labelTop.Size = new System.Drawing.Size(29, 13);
            this.labelTop.TabIndex = 31;
            this.labelTop.Text = "Top:";
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(8, 517);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 46;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // labelBlob
            // 
            this.labelBlob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBlob.AutoSize = true;
            this.labelBlob.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBlob.Location = new System.Drawing.Point(12, 419);
            this.labelBlob.Name = "labelBlob";
            this.labelBlob.Size = new System.Drawing.Size(166, 13);
            this.labelBlob.TabIndex = 47;
            this.labelBlob.Text = "Rectangle to Remove Blobs from:";
            // 
            // labelBlobFound
            // 
            this.labelBlobFound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBlobFound.AutoSize = true;
            this.labelBlobFound.Location = new System.Drawing.Point(129, 505);
            this.labelBlobFound.Name = "labelBlobFound";
            this.labelBlobFound.Size = new System.Drawing.Size(66, 13);
            this.labelBlobFound.TabIndex = 48;
            this.labelBlobFound.Text = "Blobs found:";
            // 
            // labelModified
            // 
            this.labelModified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelModified.AutoSize = true;
            this.labelModified.Location = new System.Drawing.Point(129, 527);
            this.labelModified.Name = "labelModified";
            this.labelModified.Size = new System.Drawing.Size(103, 13);
            this.labelModified.TabIndex = 49;
            this.labelModified.Text = "Image was modified:";
            // 
            // labelModifyResult
            // 
            this.labelModifyResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelModifyResult.AutoSize = true;
            this.labelModifyResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModifyResult.Location = new System.Drawing.Point(238, 527);
            this.labelModifyResult.Name = "labelModifyResult";
            this.labelModifyResult.Size = new System.Drawing.Size(37, 13);
            this.labelModifyResult.TabIndex = 50;
            this.labelModifyResult.Text = "False";
            this.labelModifyResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(8, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 52;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(8, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 51;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // DocumentBlobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 552);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.labelModifyResult);
            this.Controls.Add(this.labelModified);
            this.Controls.Add(this.labelBlobFound);
            this.Controls.Add(this.labelBlob);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.WBox);
            this.Controls.Add(this.LBox);
            this.Controls.Add(this.DensityBox);
            this.Controls.Add(this.MaxBox);
            this.Controls.Add(this.MinBox);
            this.Controls.Add(this.HBox);
            this.Controls.Add(this.TBox);
            this.Controls.Add(this.labelBlobsFound);
            this.Controls.Add(this.labelMinimumDensity);
            this.Controls.Add(this.labelMaximumPixelCount);
            this.Controls.Add(this.labelMinimumPixelCount);
            this.Controls.Add(this.labelHeight);
            this.Controls.Add(this.labelWidth);
            this.Controls.Add(this.labelLeft);
            this.Controls.Add(this.labelTop);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DocumentBlobForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "DocumentBlobRemoval";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.WBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DensityBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.NumericUpDown WBox;
        private System.Windows.Forms.NumericUpDown LBox;
        private System.Windows.Forms.NumericUpDown DensityBox;
        private System.Windows.Forms.NumericUpDown MaxBox;
        private System.Windows.Forms.NumericUpDown MinBox;
        private System.Windows.Forms.NumericUpDown HBox;
        private System.Windows.Forms.NumericUpDown TBox;
        private System.Windows.Forms.Label labelBlobsFound;
        private System.Windows.Forms.Label labelMinimumDensity;
        private System.Windows.Forms.Label labelMaximumPixelCount;
        private System.Windows.Forms.Label labelMinimumPixelCount;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.Label labelLeft;
        private System.Windows.Forms.Label labelTop;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.Label labelBlob;
        private System.Windows.Forms.Label labelBlobFound;
        private System.Windows.Forms.Label labelModified;
        private System.Windows.Forms.Label labelModifyResult;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
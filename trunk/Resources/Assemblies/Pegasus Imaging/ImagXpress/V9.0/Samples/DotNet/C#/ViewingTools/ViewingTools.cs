/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace ViewingTools
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuFile;
		private System.Windows.Forms.MenuItem menuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuFileExit;
		private System.Windows.Forms.MenuItem menuToolbar;
		private System.Windows.Forms.MenuItem menuAbout;
		private System.Windows.Forms.MenuItem menuToolbarShow;
		private System.Windows.Forms.ComboBox comboViewingTool;

		private System.String strCurrentImage;
        private System.Windows.Forms.Label labelViewingTool;
		internal System.Windows.Forms.Label lblLastError;
        internal System.Windows.Forms.Label lblerror;
        private IContainer components;

		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				// Don't forget to dispose IX
				//
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (imageXView1 != null)
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.comboViewingTool = new System.Windows.Forms.ComboBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuFile = new System.Windows.Forms.MenuItem();
            this.menuFileOpen = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuFileExit = new System.Windows.Forms.MenuItem();
            this.menuToolbar = new System.Windows.Forms.MenuItem();
            this.menuToolbarShow = new System.Windows.Forms.MenuItem();
            this.menuAbout = new System.Windows.Forms.MenuItem();
            this.labelViewingTool = new System.Windows.Forms.Label();
            this.lblLastError = new System.Windows.Forms.Label();
            this.lblerror = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(16, 8);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(648, 64);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "This example demonstrates the following:\n1) Using tools without the toolbar.\nSele" +
                "ct the tool you wish to use from the combo box.";
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.Location = new System.Drawing.Point(16, 80);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(424, 380);
            this.imageXView1.TabIndex = 1;
            // 
            // comboViewingTool
            // 
            this.comboViewingTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboViewingTool.Items.AddRange(new object[] {
            "None",
            "Zoom In",
            "Zoom Out",
            "Zoom Rect",
            "Hand Tool (Pan)",
            "Magnifier",
            "Select"});
            this.comboViewingTool.Location = new System.Drawing.Point(96, 468);
            this.comboViewingTool.Name = "comboViewingTool";
            this.comboViewingTool.Size = new System.Drawing.Size(568, 21);
            this.comboViewingTool.TabIndex = 5;
            this.comboViewingTool.Text = "comboBox1";
            this.comboViewingTool.SelectedIndexChanged += new System.EventHandler(this.comboViewingTool_SelectedIndexChanged);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuFile,
            this.menuToolbar,
            this.menuAbout});
            // 
            // menuFile
            // 
            this.menuFile.Index = 0;
            this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuFileOpen,
            this.menuItem3,
            this.menuFileExit});
            this.menuFile.Text = "&File";
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Index = 0;
            this.menuFileOpen.Text = "&Open Image";
            this.menuFileOpen.Click += new System.EventHandler(this.menuFileOpen_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "-";
            // 
            // menuFileExit
            // 
            this.menuFileExit.Index = 2;
            this.menuFileExit.Text = "&Exit";
            this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
            // 
            // menuToolbar
            // 
            this.menuToolbar.Index = 1;
            this.menuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuToolbarShow});
            this.menuToolbar.Text = "&Toolbar";
            // 
            // menuToolbarShow
            // 
            this.menuToolbarShow.Index = 0;
            this.menuToolbarShow.Text = "&Show";
            this.menuToolbarShow.Click += new System.EventHandler(this.menuToolbarShow_Click);
            // 
            // menuAbout
            // 
            this.menuAbout.Index = 2;
            this.menuAbout.Text = "&About";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // labelViewingTool
            // 
            this.labelViewingTool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelViewingTool.Location = new System.Drawing.Point(16, 468);
            this.labelViewingTool.Name = "labelViewingTool";
            this.labelViewingTool.Size = new System.Drawing.Size(72, 16);
            this.labelViewingTool.TabIndex = 7;
            this.labelViewingTool.Text = "Viewing Tool:";
            this.labelViewingTool.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(448, 84);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(136, 32);
            this.lblLastError.TabIndex = 22;
            this.lblLastError.Text = "Last Error:";
            // 
            // lblerror
            // 
            this.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblerror.Location = new System.Drawing.Point(448, 124);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(208, 88);
            this.lblerror.TabIndex = 21;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(672, 505);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.labelViewingTool);
            this.Controls.Add(this.comboViewingTool);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.richTextBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Viewing Tools";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void menuFileExit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
		
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private System.String strCurrentDir = @"..\..\..\..\..\..\..\..\Common\Images\";
		private const System.String strNoImageError = "You must select an image file to open.";
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox, System.Windows.Forms.Label ErrorLabel)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,ErrorLabel);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.FormatException ex)
			{
				PegasusError(ex,ErrorLabel);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion


		private void FormMain_Load(object sender, System.EventArgs e)
		{
			try 
			{			
			
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.Licensing.UnlockRuntime(12345,12345,12345,12345);
			
			//here we set the current directory and image so that the file open dialog box works well
			strCurrentImage = System.IO.Path.Combine (strCurrentDir, "window.jpg");

			//we open a default image to view
			imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage);

			//set the default tool to none
			comboViewingTool.SelectedIndex = 0;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}

		}

		private void comboViewingTool_SelectedIndexChanged(object sender, System.EventArgs e)
		{			
			imageXView1.ToolSet((PegasusImaging.WinForms.ImagXpress9.Tool)comboViewingTool.SelectedIndex,System.Windows.Forms.MouseButtons.Left,System.Windows.Forms.Keys.None);
			
		}

	    private void menuToolbarShow_Click(object sender, System.EventArgs e)
		{
			this.menuToolbarShow.Text = (imageXView1.Toolbar.Activated)?"&Show":"&Hide";
			imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
		}

		private void menuFileOpen_Click(object sender, System.EventArgs e)
		{
			//First we grab the filename of the image we want to open
			System.String strLoadResult = PegasusOpenFile();

			//And we check to make sure the file is valid
			if (strLoadResult.Length != 0)
			{
				//If it is valid, we set our internal image filename equal to it
				strCurrentImage = strLoadResult;

				//And we also set the current directory equal to the file's directory
				strCurrentDir = System.IO.Path.GetDirectoryName(strLoadResult);

				try 
				{
					//Clear out the error before the next image load
					lblerror.Text = "";

					//it's all looking good, so we load our image into an ImageX object
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage);
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
				{
					PegasusError(ex,lblerror);
				}
			} 
			else 
			{
				System.Windows.Forms.MessageBox.Show(strNoImageError);
			}
		}

		private void menuAbout_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

	}
}

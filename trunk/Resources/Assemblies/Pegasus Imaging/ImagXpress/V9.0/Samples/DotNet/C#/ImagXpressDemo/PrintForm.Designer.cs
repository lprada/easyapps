/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo
{
    partial class PrintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (targetPrinter != null)
            {
                targetPrinter.Dispose();
                targetPrinter = null;
            }
            if (printPro != null)
            {
                printPro.Dispose();
                printPro = null;
            }
            if (imagePages != null)
            {
                for (int i = 0; i < imagePages.Count; i++)
                {
                    if (imagePages[i] != null)
                    {
                        imagePages[i].Dispose();
                        imagePages[i] = null;
                    }
                }
            }
            if (imageXView != null)
            {
                if (imageXView.Image != null)
                {
                    imageXView.Image.Dispose();
                    imageXView.Image = null;
                }

                imageXView.Dispose();
                imageXView = null;
            }
            if (imagXpress != null)
            {
                imagXpress.Dispose();
                imagXpress = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintForm));
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.imagXpress = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.labelOrientation = new System.Windows.Forms.Label();
            this.comboBoxOrientation = new System.Windows.Forms.ComboBox();
            this.buttonSetupPrinter = new System.Windows.Forms.Button();
            this.groupBoxFormat = new System.Windows.Forms.GroupBox();
            this.comboBoxPrintQuality = new System.Windows.Forms.ComboBox();
            this.numericUpDownPrintQualityDPI = new System.Windows.Forms.NumericUpDown();
            this.checkBoxDpi = new System.Windows.Forms.CheckBox();
            this.labelQuality = new System.Windows.Forms.Label();
            this.radioButtonBlackAndWhite = new System.Windows.Forms.RadioButton();
            this.radioButtonColor = new System.Windows.Forms.RadioButton();
            this.groupBoxImagePosition = new System.Windows.Forms.GroupBox();
            this.HBox = new System.Windows.Forms.NumericUpDown();
            this.WBox = new System.Windows.Forms.NumericUpDown();
            this.YBox = new System.Windows.Forms.NumericUpDown();
            this.XBox = new System.Windows.Forms.NumericUpDown();
            this.checkBoxKeepAspectRatio = new System.Windows.Forms.CheckBox();
            this.checkBoxFitToPage = new System.Windows.Forms.CheckBox();
            this.labelPositionHeight = new System.Windows.Forms.Label();
            this.labelPositionY = new System.Windows.Forms.Label();
            this.labelPositionWidth = new System.Windows.Forms.Label();
            this.labelPositionX = new System.Windows.Forms.Label();
            this.groupBoxPreview = new System.Windows.Forms.GroupBox();
            this.comboBoxPreviewScale = new System.Windows.Forms.ComboBox();
            this.labelPreviewScale = new System.Windows.Forms.Label();
            this.labelPreviewPage = new System.Windows.Forms.Label();
            this.numericUpDownPreviewPage = new System.Windows.Forms.NumericUpDown();
            this.buttonPreviewUpdate = new System.Windows.Forms.Button();
            this.groupBoxPrinter = new System.Windows.Forms.GroupBox();
            this.ToBox = new System.Windows.Forms.NumericUpDown();
            this.FromBox = new System.Windows.Forms.NumericUpDown();
            this.labelPagesTo = new System.Windows.Forms.Label();
            this.labelPages = new System.Windows.Forms.Label();
            this.comboBoxPaperBin = new System.Windows.Forms.ComboBox();
            this.comboBoxPaper = new System.Windows.Forms.ComboBox();
            this.labelPaperBin = new System.Windows.Forms.Label();
            this.labelPaper = new System.Windows.Forms.Label();
            this.labelCopies = new System.Windows.Forms.Label();
            this.numericUpDownCopies = new System.Windows.Forms.NumericUpDown();
            this.labelPrinterPortText = new System.Windows.Forms.Label();
            this.labelPrinterPort = new System.Windows.Forms.Label();
            this.labelPrinterDriverText = new System.Windows.Forms.Label();
            this.labelPrinterDriver = new System.Windows.Forms.Label();
            this.labelPrinterNameText = new System.Windows.Forms.Label();
            this.labelPrinterName = new System.Windows.Forms.Label();
            this.groupBoxImageOptions = new System.Windows.Forms.GroupBox();
            this.checkBoxMirror = new System.Windows.Forms.CheckBox();
            this.comboBoxRotation = new System.Windows.Forms.ComboBox();
            this.labelRotation = new System.Windows.Forms.Label();
            this.printPro = new PegasusImaging.WinForms.PrintPro4.PrintPro(this.components);
            this.groupBoxFormat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrintQualityDPI)).BeginInit();
            this.groupBoxImagePosition.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XBox)).BeginInit();
            this.groupBoxPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPreviewPage)).BeginInit();
            this.groupBoxPrinter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCopies)).BeginInit();
            this.groupBoxImageOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(621, 521);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(72, 32);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrint.Location = new System.Drawing.Point(512, 521);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(72, 32);
            this.buttonPrint.TabIndex = 1;
            this.buttonPrint.Text = "&Print";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // imageXView
            // 
            this.imageXView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView.Location = new System.Drawing.Point(260, 12);
            this.imageXView.MouseWheelCapture = false;
            this.imageXView.Name = "imageXView";
            this.imageXView.Size = new System.Drawing.Size(433, 457);
            this.imageXView.TabIndex = 3;
            // 
            // labelOrientation
            // 
            this.labelOrientation.AutoSize = true;
            this.labelOrientation.Location = new System.Drawing.Point(6, 22);
            this.labelOrientation.Name = "labelOrientation";
            this.labelOrientation.Size = new System.Drawing.Size(61, 13);
            this.labelOrientation.TabIndex = 6;
            this.labelOrientation.Text = "Orientation:";
            // 
            // comboBoxOrientation
            // 
            this.comboBoxOrientation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOrientation.FormattingEnabled = true;
            this.comboBoxOrientation.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
            this.comboBoxOrientation.Location = new System.Drawing.Point(73, 19);
            this.comboBoxOrientation.Name = "comboBoxOrientation";
            this.comboBoxOrientation.Size = new System.Drawing.Size(154, 21);
            this.comboBoxOrientation.TabIndex = 7;
            this.comboBoxOrientation.SelectedIndexChanged += new System.EventHandler(this.comboBoxOrientation_SelectedIndexChanged);
            // 
            // buttonSetupPrinter
            // 
            this.buttonSetupPrinter.Location = new System.Drawing.Point(145, 162);
            this.buttonSetupPrinter.Name = "buttonSetupPrinter";
            this.buttonSetupPrinter.Size = new System.Drawing.Size(82, 24);
            this.buttonSetupPrinter.TabIndex = 8;
            this.buttonSetupPrinter.Text = "Setup";
            this.buttonSetupPrinter.UseVisualStyleBackColor = true;
            this.buttonSetupPrinter.Click += new System.EventHandler(this.buttonSetupPrinter_Click);
            // 
            // groupBoxFormat
            // 
            this.groupBoxFormat.Controls.Add(this.comboBoxPrintQuality);
            this.groupBoxFormat.Controls.Add(this.numericUpDownPrintQualityDPI);
            this.groupBoxFormat.Controls.Add(this.checkBoxDpi);
            this.groupBoxFormat.Controls.Add(this.labelQuality);
            this.groupBoxFormat.Controls.Add(this.radioButtonBlackAndWhite);
            this.groupBoxFormat.Controls.Add(this.radioButtonColor);
            this.groupBoxFormat.Controls.Add(this.labelOrientation);
            this.groupBoxFormat.Controls.Add(this.comboBoxOrientation);
            this.groupBoxFormat.Location = new System.Drawing.Point(12, 214);
            this.groupBoxFormat.Name = "groupBoxFormat";
            this.groupBoxFormat.Size = new System.Drawing.Size(233, 100);
            this.groupBoxFormat.TabIndex = 13;
            this.groupBoxFormat.TabStop = false;
            this.groupBoxFormat.Text = "Format";
            // 
            // comboBoxPrintQuality
            // 
            this.comboBoxPrintQuality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrintQuality.FormattingEnabled = true;
            this.comboBoxPrintQuality.Items.AddRange(new object[] {
            "Draft",
            "Low",
            "Medium",
            "High"});
            this.comboBoxPrintQuality.Location = new System.Drawing.Point(56, 69);
            this.comboBoxPrintQuality.Name = "comboBoxPrintQuality";
            this.comboBoxPrintQuality.Size = new System.Drawing.Size(57, 21);
            this.comboBoxPrintQuality.TabIndex = 16;
            this.comboBoxPrintQuality.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrintQuality_SelectedIndexChanged);
            // 
            // numericUpDownPrintQualityDPI
            // 
            this.numericUpDownPrintQualityDPI.Enabled = false;
            this.numericUpDownPrintQualityDPI.Location = new System.Drawing.Point(172, 70);
            this.numericUpDownPrintQualityDPI.Maximum = new decimal(new int[] {
            6400,
            0,
            0,
            0});
            this.numericUpDownPrintQualityDPI.Minimum = new decimal(new int[] {
            72,
            0,
            0,
            0});
            this.numericUpDownPrintQualityDPI.Name = "numericUpDownPrintQualityDPI";
            this.numericUpDownPrintQualityDPI.Size = new System.Drawing.Size(55, 20);
            this.numericUpDownPrintQualityDPI.TabIndex = 18;
            this.numericUpDownPrintQualityDPI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownPrintQualityDPI.Value = new decimal(new int[] {
            72,
            0,
            0,
            0});
            this.numericUpDownPrintQualityDPI.ValueChanged += new System.EventHandler(this.numericUpDownPrintQualityDPI_ValueChanged);
            // 
            // checkBoxDpi
            // 
            this.checkBoxDpi.AutoSize = true;
            this.checkBoxDpi.Location = new System.Drawing.Point(119, 71);
            this.checkBoxDpi.Name = "checkBoxDpi";
            this.checkBoxDpi.Size = new System.Drawing.Size(47, 17);
            this.checkBoxDpi.TabIndex = 17;
            this.checkBoxDpi.Text = "DPI:";
            this.checkBoxDpi.UseVisualStyleBackColor = true;
            this.checkBoxDpi.CheckedChanged += new System.EventHandler(this.checkBoxDpi_CheckedChanged);
            // 
            // labelQuality
            // 
            this.labelQuality.AutoSize = true;
            this.labelQuality.Location = new System.Drawing.Point(8, 72);
            this.labelQuality.Name = "labelQuality";
            this.labelQuality.Size = new System.Drawing.Size(42, 13);
            this.labelQuality.TabIndex = 15;
            this.labelQuality.Text = "Quality:";
            // 
            // radioButtonBlackAndWhite
            // 
            this.radioButtonBlackAndWhite.AutoSize = true;
            this.radioButtonBlackAndWhite.Location = new System.Drawing.Point(61, 46);
            this.radioButtonBlackAndWhite.Name = "radioButtonBlackAndWhite";
            this.radioButtonBlackAndWhite.Size = new System.Drawing.Size(104, 17);
            this.radioButtonBlackAndWhite.TabIndex = 14;
            this.radioButtonBlackAndWhite.Text = "Black and White";
            this.radioButtonBlackAndWhite.UseVisualStyleBackColor = true;
            this.radioButtonBlackAndWhite.CheckedChanged += new System.EventHandler(this.radioButtonBlackAndWhite_CheckedChanged);
            // 
            // radioButtonColor
            // 
            this.radioButtonColor.AutoSize = true;
            this.radioButtonColor.Checked = true;
            this.radioButtonColor.Location = new System.Drawing.Point(6, 46);
            this.radioButtonColor.Name = "radioButtonColor";
            this.radioButtonColor.Size = new System.Drawing.Size(49, 17);
            this.radioButtonColor.TabIndex = 13;
            this.radioButtonColor.TabStop = true;
            this.radioButtonColor.Text = "Color";
            this.radioButtonColor.UseVisualStyleBackColor = true;
            this.radioButtonColor.CheckedChanged += new System.EventHandler(this.radioButtonColor_CheckedChanged);
            // 
            // groupBoxImagePosition
            // 
            this.groupBoxImagePosition.Controls.Add(this.HBox);
            this.groupBoxImagePosition.Controls.Add(this.WBox);
            this.groupBoxImagePosition.Controls.Add(this.YBox);
            this.groupBoxImagePosition.Controls.Add(this.XBox);
            this.groupBoxImagePosition.Controls.Add(this.checkBoxKeepAspectRatio);
            this.groupBoxImagePosition.Controls.Add(this.checkBoxFitToPage);
            this.groupBoxImagePosition.Controls.Add(this.labelPositionHeight);
            this.groupBoxImagePosition.Controls.Add(this.labelPositionY);
            this.groupBoxImagePosition.Controls.Add(this.labelPositionWidth);
            this.groupBoxImagePosition.Controls.Add(this.labelPositionX);
            this.groupBoxImagePosition.Location = new System.Drawing.Point(12, 320);
            this.groupBoxImagePosition.Name = "groupBoxImagePosition";
            this.groupBoxImagePosition.Size = new System.Drawing.Size(233, 95);
            this.groupBoxImagePosition.TabIndex = 14;
            this.groupBoxImagePosition.TabStop = false;
            this.groupBoxImagePosition.Text = "Image Position";
            // 
            // HBox
            // 
            this.HBox.DecimalPlaces = 1;
            this.HBox.Enabled = false;
            this.HBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.HBox.Location = new System.Drawing.Point(148, 45);
            this.HBox.Name = "HBox";
            this.HBox.Size = new System.Drawing.Size(55, 20);
            this.HBox.TabIndex = 27;
            this.HBox.ValueChanged += new System.EventHandler(this.HBox_ValueChanged);
            // 
            // WBox
            // 
            this.WBox.DecimalPlaces = 1;
            this.WBox.Enabled = false;
            this.WBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.WBox.Location = new System.Drawing.Point(148, 19);
            this.WBox.Name = "WBox";
            this.WBox.Size = new System.Drawing.Size(55, 20);
            this.WBox.TabIndex = 26;
            this.WBox.ValueChanged += new System.EventHandler(this.WBox_ValueChanged);
            // 
            // YBox
            // 
            this.YBox.DecimalPlaces = 1;
            this.YBox.Enabled = false;
            this.YBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.YBox.Location = new System.Drawing.Point(30, 46);
            this.YBox.Name = "YBox";
            this.YBox.Size = new System.Drawing.Size(55, 20);
            this.YBox.TabIndex = 25;
            this.YBox.ValueChanged += new System.EventHandler(this.YBox_ValueChanged);
            // 
            // XBox
            // 
            this.XBox.DecimalPlaces = 1;
            this.XBox.Enabled = false;
            this.XBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.XBox.Location = new System.Drawing.Point(30, 19);
            this.XBox.Name = "XBox";
            this.XBox.Size = new System.Drawing.Size(55, 20);
            this.XBox.TabIndex = 24;
            this.XBox.ValueChanged += new System.EventHandler(this.XBox_ValueChanged);
            // 
            // checkBoxKeepAspectRatio
            // 
            this.checkBoxKeepAspectRatio.AutoSize = true;
            this.checkBoxKeepAspectRatio.Checked = true;
            this.checkBoxKeepAspectRatio.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxKeepAspectRatio.Location = new System.Drawing.Point(112, 71);
            this.checkBoxKeepAspectRatio.Name = "checkBoxKeepAspectRatio";
            this.checkBoxKeepAspectRatio.Size = new System.Drawing.Size(115, 17);
            this.checkBoxKeepAspectRatio.TabIndex = 7;
            this.checkBoxKeepAspectRatio.Text = "Keep Aspect Ratio";
            this.checkBoxKeepAspectRatio.UseVisualStyleBackColor = true;
            // 
            // checkBoxFitToPage
            // 
            this.checkBoxFitToPage.AutoSize = true;
            this.checkBoxFitToPage.Checked = true;
            this.checkBoxFitToPage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFitToPage.Location = new System.Drawing.Point(6, 71);
            this.checkBoxFitToPage.Name = "checkBoxFitToPage";
            this.checkBoxFitToPage.Size = new System.Drawing.Size(77, 17);
            this.checkBoxFitToPage.TabIndex = 8;
            this.checkBoxFitToPage.Text = "Fit to Page";
            this.checkBoxFitToPage.UseVisualStyleBackColor = true;
            this.checkBoxFitToPage.CheckedChanged += new System.EventHandler(this.checkBoxFitToPage_CheckedChanged);
            // 
            // labelPositionHeight
            // 
            this.labelPositionHeight.AutoSize = true;
            this.labelPositionHeight.Location = new System.Drawing.Point(104, 48);
            this.labelPositionHeight.Name = "labelPositionHeight";
            this.labelPositionHeight.Size = new System.Drawing.Size(41, 13);
            this.labelPositionHeight.TabIndex = 6;
            this.labelPositionHeight.Text = "Height:";
            // 
            // labelPositionY
            // 
            this.labelPositionY.AutoSize = true;
            this.labelPositionY.Location = new System.Drawing.Point(6, 48);
            this.labelPositionY.Name = "labelPositionY";
            this.labelPositionY.Size = new System.Drawing.Size(17, 13);
            this.labelPositionY.TabIndex = 4;
            this.labelPositionY.Text = "Y:";
            // 
            // labelPositionWidth
            // 
            this.labelPositionWidth.AutoSize = true;
            this.labelPositionWidth.Location = new System.Drawing.Point(104, 22);
            this.labelPositionWidth.Name = "labelPositionWidth";
            this.labelPositionWidth.Size = new System.Drawing.Size(38, 13);
            this.labelPositionWidth.TabIndex = 2;
            this.labelPositionWidth.Text = "Width:";
            // 
            // labelPositionX
            // 
            this.labelPositionX.AutoSize = true;
            this.labelPositionX.Location = new System.Drawing.Point(6, 22);
            this.labelPositionX.Name = "labelPositionX";
            this.labelPositionX.Size = new System.Drawing.Size(17, 13);
            this.labelPositionX.TabIndex = 0;
            this.labelPositionX.Text = "X:";
            // 
            // groupBoxPreview
            // 
            this.groupBoxPreview.Controls.Add(this.comboBoxPreviewScale);
            this.groupBoxPreview.Controls.Add(this.labelPreviewScale);
            this.groupBoxPreview.Controls.Add(this.labelPreviewPage);
            this.groupBoxPreview.Controls.Add(this.numericUpDownPreviewPage);
            this.groupBoxPreview.Controls.Add(this.buttonPreviewUpdate);
            this.groupBoxPreview.Location = new System.Drawing.Point(12, 475);
            this.groupBoxPreview.Name = "groupBoxPreview";
            this.groupBoxPreview.Size = new System.Drawing.Size(317, 82);
            this.groupBoxPreview.TabIndex = 15;
            this.groupBoxPreview.TabStop = false;
            this.groupBoxPreview.Text = "Preview";
            // 
            // comboBoxPreviewScale
            // 
            this.comboBoxPreviewScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPreviewScale.DropDownWidth = 242;
            this.comboBoxPreviewScale.FormattingEnabled = true;
            this.comboBoxPreviewScale.Items.AddRange(new object[] {
            "1:1 - Same size as the printed image",
            "1:2 - One half the size of the printed image",
            "1:3 - One third the size of the printed image",
            "1:4 - One fourth the size of the printed image",
            "1:5 - One fifth the size of the printed image",
            "1:6 - One sixth the size of the printed image",
            "1:7 - One seventh the size of the printed image",
            "1:8 - One eighth the size of the printed image",
            "1:9 - One ninth the size of the printed image",
            "1:10 - One tenth the size of the printed image"});
            this.comboBoxPreviewScale.Location = new System.Drawing.Point(50, 22);
            this.comboBoxPreviewScale.Name = "comboBoxPreviewScale";
            this.comboBoxPreviewScale.Size = new System.Drawing.Size(262, 21);
            this.comboBoxPreviewScale.TabIndex = 4;
            // 
            // labelPreviewScale
            // 
            this.labelPreviewScale.AutoSize = true;
            this.labelPreviewScale.Location = new System.Drawing.Point(6, 22);
            this.labelPreviewScale.Name = "labelPreviewScale";
            this.labelPreviewScale.Size = new System.Drawing.Size(37, 13);
            this.labelPreviewScale.TabIndex = 3;
            this.labelPreviewScale.Text = "Scale:";
            // 
            // labelPreviewPage
            // 
            this.labelPreviewPage.AutoSize = true;
            this.labelPreviewPage.Location = new System.Drawing.Point(8, 57);
            this.labelPreviewPage.Name = "labelPreviewPage";
            this.labelPreviewPage.Size = new System.Drawing.Size(35, 13);
            this.labelPreviewPage.TabIndex = 2;
            this.labelPreviewPage.Text = "Page:";
            // 
            // numericUpDownPreviewPage
            // 
            this.numericUpDownPreviewPage.Location = new System.Drawing.Point(49, 55);
            this.numericUpDownPreviewPage.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPreviewPage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPreviewPage.Name = "numericUpDownPreviewPage";
            this.numericUpDownPreviewPage.Size = new System.Drawing.Size(68, 20);
            this.numericUpDownPreviewPage.TabIndex = 1;
            this.numericUpDownPreviewPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownPreviewPage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonPreviewUpdate
            // 
            this.buttonPreviewUpdate.Location = new System.Drawing.Point(246, 49);
            this.buttonPreviewUpdate.Name = "buttonPreviewUpdate";
            this.buttonPreviewUpdate.Size = new System.Drawing.Size(65, 26);
            this.buttonPreviewUpdate.TabIndex = 0;
            this.buttonPreviewUpdate.Text = "Update";
            this.buttonPreviewUpdate.UseVisualStyleBackColor = true;
            this.buttonPreviewUpdate.Click += new System.EventHandler(this.buttonPreviewUpdate_Click);
            // 
            // groupBoxPrinter
            // 
            this.groupBoxPrinter.Controls.Add(this.ToBox);
            this.groupBoxPrinter.Controls.Add(this.FromBox);
            this.groupBoxPrinter.Controls.Add(this.labelPagesTo);
            this.groupBoxPrinter.Controls.Add(this.labelPages);
            this.groupBoxPrinter.Controls.Add(this.comboBoxPaperBin);
            this.groupBoxPrinter.Controls.Add(this.comboBoxPaper);
            this.groupBoxPrinter.Controls.Add(this.labelPaperBin);
            this.groupBoxPrinter.Controls.Add(this.labelPaper);
            this.groupBoxPrinter.Controls.Add(this.labelCopies);
            this.groupBoxPrinter.Controls.Add(this.numericUpDownCopies);
            this.groupBoxPrinter.Controls.Add(this.labelPrinterPortText);
            this.groupBoxPrinter.Controls.Add(this.labelPrinterPort);
            this.groupBoxPrinter.Controls.Add(this.labelPrinterDriverText);
            this.groupBoxPrinter.Controls.Add(this.labelPrinterDriver);
            this.groupBoxPrinter.Controls.Add(this.labelPrinterNameText);
            this.groupBoxPrinter.Controls.Add(this.labelPrinterName);
            this.groupBoxPrinter.Controls.Add(this.buttonSetupPrinter);
            this.groupBoxPrinter.Location = new System.Drawing.Point(12, 12);
            this.groupBoxPrinter.Name = "groupBoxPrinter";
            this.groupBoxPrinter.Size = new System.Drawing.Size(233, 196);
            this.groupBoxPrinter.TabIndex = 16;
            this.groupBoxPrinter.TabStop = false;
            this.groupBoxPrinter.Text = "Printer";
            // 
            // ToBox
            // 
            this.ToBox.Location = new System.Drawing.Point(145, 79);
            this.ToBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ToBox.Name = "ToBox";
            this.ToBox.Size = new System.Drawing.Size(55, 20);
            this.ToBox.TabIndex = 29;
            this.ToBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ToBox.ValueChanged += new System.EventHandler(this.ToBox_ValueChanged);
            // 
            // FromBox
            // 
            this.FromBox.Location = new System.Drawing.Point(49, 79);
            this.FromBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FromBox.Name = "FromBox";
            this.FromBox.Size = new System.Drawing.Size(55, 20);
            this.FromBox.TabIndex = 28;
            this.FromBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FromBox.ValueChanged += new System.EventHandler(this.FromBox_ValueChanged);
            // 
            // labelPagesTo
            // 
            this.labelPagesTo.AutoSize = true;
            this.labelPagesTo.Location = new System.Drawing.Point(119, 81);
            this.labelPagesTo.Name = "labelPagesTo";
            this.labelPagesTo.Size = new System.Drawing.Size(23, 13);
            this.labelPagesTo.TabIndex = 23;
            this.labelPagesTo.Text = "To:";
            // 
            // labelPages
            // 
            this.labelPages.AutoSize = true;
            this.labelPages.Location = new System.Drawing.Point(6, 81);
            this.labelPages.Name = "labelPages";
            this.labelPages.Size = new System.Drawing.Size(40, 13);
            this.labelPages.TabIndex = 21;
            this.labelPages.Text = "Pages:";
            // 
            // comboBoxPaperBin
            // 
            this.comboBoxPaperBin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPaperBin.FormattingEnabled = true;
            this.comboBoxPaperBin.Location = new System.Drawing.Point(68, 135);
            this.comboBoxPaperBin.Name = "comboBoxPaperBin";
            this.comboBoxPaperBin.Size = new System.Drawing.Size(159, 21);
            this.comboBoxPaperBin.TabIndex = 20;
            this.comboBoxPaperBin.SelectedIndexChanged += new System.EventHandler(this.comboBoxPaperBin_SelectedIndexChanged);
            // 
            // comboBoxPaper
            // 
            this.comboBoxPaper.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPaper.FormattingEnabled = true;
            this.comboBoxPaper.Location = new System.Drawing.Point(50, 108);
            this.comboBoxPaper.Name = "comboBoxPaper";
            this.comboBoxPaper.Size = new System.Drawing.Size(177, 21);
            this.comboBoxPaper.TabIndex = 19;
            this.comboBoxPaper.SelectedIndexChanged += new System.EventHandler(this.comboBoxPaper_SelectedIndexChanged);
            // 
            // labelPaperBin
            // 
            this.labelPaperBin.AutoSize = true;
            this.labelPaperBin.Location = new System.Drawing.Point(6, 138);
            this.labelPaperBin.Name = "labelPaperBin";
            this.labelPaperBin.Size = new System.Drawing.Size(56, 13);
            this.labelPaperBin.TabIndex = 18;
            this.labelPaperBin.Text = "Paper Bin:";
            // 
            // labelPaper
            // 
            this.labelPaper.AutoSize = true;
            this.labelPaper.Location = new System.Drawing.Point(6, 111);
            this.labelPaper.Name = "labelPaper";
            this.labelPaper.Size = new System.Drawing.Size(38, 13);
            this.labelPaper.TabIndex = 17;
            this.labelPaper.Text = "Paper:";
            // 
            // labelCopies
            // 
            this.labelCopies.AutoSize = true;
            this.labelCopies.Location = new System.Drawing.Point(6, 168);
            this.labelCopies.Name = "labelCopies";
            this.labelCopies.Size = new System.Drawing.Size(42, 13);
            this.labelCopies.TabIndex = 16;
            this.labelCopies.Text = "Copies:";
            // 
            // numericUpDownCopies
            // 
            this.numericUpDownCopies.Location = new System.Drawing.Point(54, 166);
            this.numericUpDownCopies.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDownCopies.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCopies.Name = "numericUpDownCopies";
            this.numericUpDownCopies.Size = new System.Drawing.Size(85, 20);
            this.numericUpDownCopies.TabIndex = 15;
            this.numericUpDownCopies.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownCopies.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCopies.ValueChanged += new System.EventHandler(this.numericUpDownCopies_ValueChanged);
            // 
            // labelPrinterPortText
            // 
            this.labelPrinterPortText.AutoSize = true;
            this.labelPrinterPortText.Location = new System.Drawing.Point(54, 62);
            this.labelPrinterPortText.Name = "labelPrinterPortText";
            this.labelPrinterPortText.Size = new System.Drawing.Size(31, 13);
            this.labelPrinterPortText.TabIndex = 14;
            this.labelPrinterPortText.Text = "none";
            // 
            // labelPrinterPort
            // 
            this.labelPrinterPort.Location = new System.Drawing.Point(6, 58);
            this.labelPrinterPort.Name = "labelPrinterPort";
            this.labelPrinterPort.Size = new System.Drawing.Size(42, 21);
            this.labelPrinterPort.TabIndex = 13;
            this.labelPrinterPort.Text = "Port:";
            this.labelPrinterPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrinterDriverText
            // 
            this.labelPrinterDriverText.AutoSize = true;
            this.labelPrinterDriverText.Location = new System.Drawing.Point(54, 41);
            this.labelPrinterDriverText.Name = "labelPrinterDriverText";
            this.labelPrinterDriverText.Size = new System.Drawing.Size(31, 13);
            this.labelPrinterDriverText.TabIndex = 12;
            this.labelPrinterDriverText.Text = "none";
            // 
            // labelPrinterDriver
            // 
            this.labelPrinterDriver.Location = new System.Drawing.Point(6, 37);
            this.labelPrinterDriver.Name = "labelPrinterDriver";
            this.labelPrinterDriver.Size = new System.Drawing.Size(42, 21);
            this.labelPrinterDriver.TabIndex = 11;
            this.labelPrinterDriver.Text = "Driver:";
            this.labelPrinterDriver.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelPrinterNameText
            // 
            this.labelPrinterNameText.AutoSize = true;
            this.labelPrinterNameText.Location = new System.Drawing.Point(54, 20);
            this.labelPrinterNameText.Name = "labelPrinterNameText";
            this.labelPrinterNameText.Size = new System.Drawing.Size(31, 13);
            this.labelPrinterNameText.TabIndex = 10;
            this.labelPrinterNameText.Text = "none";
            // 
            // labelPrinterName
            // 
            this.labelPrinterName.Location = new System.Drawing.Point(6, 16);
            this.labelPrinterName.Name = "labelPrinterName";
            this.labelPrinterName.Size = new System.Drawing.Size(42, 21);
            this.labelPrinterName.TabIndex = 9;
            this.labelPrinterName.Text = "Name:";
            this.labelPrinterName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBoxImageOptions
            // 
            this.groupBoxImageOptions.Controls.Add(this.checkBoxMirror);
            this.groupBoxImageOptions.Controls.Add(this.comboBoxRotation);
            this.groupBoxImageOptions.Controls.Add(this.labelRotation);
            this.groupBoxImageOptions.Location = new System.Drawing.Point(12, 421);
            this.groupBoxImageOptions.Name = "groupBoxImageOptions";
            this.groupBoxImageOptions.Size = new System.Drawing.Size(233, 48);
            this.groupBoxImageOptions.TabIndex = 17;
            this.groupBoxImageOptions.TabStop = false;
            this.groupBoxImageOptions.Text = "Image Preprocessing Options";
            // 
            // checkBoxMirror
            // 
            this.checkBoxMirror.AutoSize = true;
            this.checkBoxMirror.Location = new System.Drawing.Point(175, 21);
            this.checkBoxMirror.Name = "checkBoxMirror";
            this.checkBoxMirror.Size = new System.Drawing.Size(52, 17);
            this.checkBoxMirror.TabIndex = 2;
            this.checkBoxMirror.Text = "Mirror";
            this.checkBoxMirror.UseVisualStyleBackColor = true;
            // 
            // comboBoxRotation
            // 
            this.comboBoxRotation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRotation.FormattingEnabled = true;
            this.comboBoxRotation.Items.AddRange(new object[] {
            "None",
            "90 Degrees",
            "180 Degrees",
            "270 Degrees"});
            this.comboBoxRotation.Location = new System.Drawing.Point(62, 19);
            this.comboBoxRotation.Name = "comboBoxRotation";
            this.comboBoxRotation.Size = new System.Drawing.Size(107, 21);
            this.comboBoxRotation.TabIndex = 1;
            // 
            // labelRotation
            // 
            this.labelRotation.AutoSize = true;
            this.labelRotation.Location = new System.Drawing.Point(6, 22);
            this.labelRotation.Name = "labelRotation";
            this.labelRotation.Size = new System.Drawing.Size(50, 13);
            this.labelRotation.TabIndex = 0;
            this.labelRotation.Text = "Rotation:";
            // 
            // PrintForm
            // 
            this.AcceptButton = this.buttonPrint;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(705, 567);
            this.Controls.Add(this.groupBoxImageOptions);
            this.Controls.Add(this.groupBoxPrinter);
            this.Controls.Add(this.groupBoxPreview);
            this.Controls.Add(this.groupBoxImagePosition);
            this.Controls.Add(this.groupBoxFormat);
            this.Controls.Add(this.imageXView);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.buttonCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrintForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print";
            this.Load += new System.EventHandler(this.PrintForm_Load);
            this.groupBoxFormat.ResumeLayout(false);
            this.groupBoxFormat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrintQualityDPI)).EndInit();
            this.groupBoxImagePosition.ResumeLayout(false);
            this.groupBoxImagePosition.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XBox)).EndInit();
            this.groupBoxPreview.ResumeLayout(false);
            this.groupBoxPreview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPreviewPage)).EndInit();
            this.groupBoxPrinter.ResumeLayout(false);
            this.groupBoxPrinter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCopies)).EndInit();
            this.groupBoxImageOptions.ResumeLayout(false);
            this.groupBoxImageOptions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonPrint;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView;
        private System.Windows.Forms.Label labelOrientation;
        private System.Windows.Forms.ComboBox comboBoxOrientation;
        private System.Windows.Forms.Button buttonSetupPrinter;
        private System.Windows.Forms.GroupBox groupBoxFormat;
        private System.Windows.Forms.GroupBox groupBoxImagePosition;
        private System.Windows.Forms.Label labelPositionHeight;
        private System.Windows.Forms.Label labelPositionY;
        private System.Windows.Forms.Label labelPositionWidth;
        private System.Windows.Forms.Label labelPositionX;
        private System.Windows.Forms.GroupBox groupBoxPreview;
        private System.Windows.Forms.Label labelPreviewPage;
        private System.Windows.Forms.NumericUpDown numericUpDownPreviewPage;
        private System.Windows.Forms.Button buttonPreviewUpdate;
        private System.Windows.Forms.GroupBox groupBoxPrinter;
        private System.Windows.Forms.GroupBox groupBoxImageOptions;
        private System.Windows.Forms.ComboBox comboBoxRotation;
        private System.Windows.Forms.Label labelRotation;
        private System.Windows.Forms.CheckBox checkBoxMirror;
        private System.Windows.Forms.Label labelPrinterPortText;
        private System.Windows.Forms.Label labelPrinterPort;
        private System.Windows.Forms.Label labelPrinterDriverText;
        private System.Windows.Forms.Label labelPrinterDriver;
        private System.Windows.Forms.Label labelPrinterNameText;
        private System.Windows.Forms.Label labelPrinterName;
        private System.Windows.Forms.RadioButton radioButtonBlackAndWhite;
        private System.Windows.Forms.RadioButton radioButtonColor;
        private System.Windows.Forms.CheckBox checkBoxKeepAspectRatio;
        private System.Windows.Forms.ComboBox comboBoxPreviewScale;
        private System.Windows.Forms.Label labelPreviewScale;
        private System.Windows.Forms.CheckBox checkBoxFitToPage;
        private System.Windows.Forms.NumericUpDown numericUpDownCopies;
        private System.Windows.Forms.Label labelCopies;
        private System.Windows.Forms.ComboBox comboBoxPaperBin;
        private System.Windows.Forms.ComboBox comboBoxPaper;
        private System.Windows.Forms.Label labelPaperBin;
        private System.Windows.Forms.Label labelPaper;
        private System.Windows.Forms.Label labelPagesTo;
        private System.Windows.Forms.Label labelPages;
        private System.Windows.Forms.NumericUpDown numericUpDownPrintQualityDPI;
        private System.Windows.Forms.CheckBox checkBoxDpi;
        private System.Windows.Forms.ComboBox comboBoxPrintQuality;
        private System.Windows.Forms.Label labelQuality;
        private PegasusImaging.WinForms.PrintPro4.PrintPro printPro;
        private System.Windows.Forms.NumericUpDown HBox;
        private System.Windows.Forms.NumericUpDown WBox;
        private System.Windows.Forms.NumericUpDown YBox;
        private System.Windows.Forms.NumericUpDown XBox;
        private System.Windows.Forms.NumericUpDown ToBox;
        private System.Windows.Forms.NumericUpDown FromBox;
    }
}
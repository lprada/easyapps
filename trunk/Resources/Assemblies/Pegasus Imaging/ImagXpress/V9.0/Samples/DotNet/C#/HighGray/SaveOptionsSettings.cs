/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace HighGray
{
	/// <summary>
	/// Summary description for SaveOptions.
	/// </summary>
	public class SaveOptionsSettings : System.Windows.Forms.Form
	{

		private PegasusImaging.WinForms.ImagXpress9.SaveOptions SaveOptions;
		private String sFilename; 
		
		private System.Windows.Forms.GroupBox grpSaveOptions;
		private System.Windows.Forms.GroupBox gbJPEG2000Types;
		private System.Windows.Forms.RadioButton rb2000Lossy;
		private System.Windows.Forms.RadioButton rb2000GrayScale;
		private System.Windows.Forms.RadioButton rb2000Lossless;
		private System.Windows.Forms.RadioButton rbJPEG;
		private System.Windows.Forms.RadioButton rbJPEGLS;
		private System.Windows.Forms.RadioButton rbJPEG2000;
		private System.Windows.Forms.RadioButton rbLosslessJPG;
		private System.Windows.Forms.Button cmdSave;
		private System.Windows.Forms.Button cmdClose;
		private System.Windows.Forms.RadioButton rbRAW;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SaveOptionsSettings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			SaveOptions = new SaveOptions();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grpSaveOptions = new System.Windows.Forms.GroupBox();
			this.cmdSave = new System.Windows.Forms.Button();
			this.rbLosslessJPG = new System.Windows.Forms.RadioButton();
			this.rbJPEG2000 = new System.Windows.Forms.RadioButton();
			this.rbJPEGLS = new System.Windows.Forms.RadioButton();
			this.rbJPEG = new System.Windows.Forms.RadioButton();
			this.gbJPEG2000Types = new System.Windows.Forms.GroupBox();
			this.rb2000Lossless = new System.Windows.Forms.RadioButton();
			this.rb2000GrayScale = new System.Windows.Forms.RadioButton();
			this.rb2000Lossy = new System.Windows.Forms.RadioButton();
			this.cmdClose = new System.Windows.Forms.Button();
			this.rbRAW = new System.Windows.Forms.RadioButton();
			this.grpSaveOptions.SuspendLayout();
			this.gbJPEG2000Types.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpSaveOptions
			// 
			this.grpSaveOptions.Controls.AddRange(new System.Windows.Forms.Control[] {
																						 this.rbRAW,
																						 this.cmdSave,
																						 this.rbLosslessJPG,
																						 this.rbJPEG2000,
																						 this.rbJPEGLS,
																						 this.rbJPEG,
																						 this.gbJPEG2000Types});
			this.grpSaveOptions.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.grpSaveOptions.Location = new System.Drawing.Point(16, 24);
			this.grpSaveOptions.Name = "grpSaveOptions";
			this.grpSaveOptions.Size = new System.Drawing.Size(464, 352);
			this.grpSaveOptions.TabIndex = 0;
			this.grpSaveOptions.TabStop = false;
			this.grpSaveOptions.Text = "Saving Options";
			// 
			// cmdSave
			// 
			this.cmdSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmdSave.Location = new System.Drawing.Point(160, 312);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(120, 32);
			this.cmdSave.TabIndex = 6;
			this.cmdSave.Text = "Save Image";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// rbLosslessJPG
			// 
			this.rbLosslessJPG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.rbLosslessJPG.Location = new System.Drawing.Point(16, 224);
			this.rbLosslessJPG.Name = "rbLosslessJPG";
			this.rbLosslessJPG.Size = new System.Drawing.Size(152, 32);
			this.rbLosslessJPG.TabIndex = 4;
			this.rbLosslessJPG.Text = "Lossless JPG";
			this.rbLosslessJPG.CheckedChanged += new System.EventHandler(this.rbLosslessJPG_CheckedChanged);
			// 
			// rbJPEG2000
			// 
			this.rbJPEG2000.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.rbJPEG2000.Location = new System.Drawing.Point(16, 176);
			this.rbJPEG2000.Name = "rbJPEG2000";
			this.rbJPEG2000.Size = new System.Drawing.Size(152, 32);
			this.rbJPEG2000.TabIndex = 3;
			this.rbJPEG2000.Text = "JPEG 2000";
			this.rbJPEG2000.CheckedChanged += new System.EventHandler(this.rbJPEG2000_CheckedChanged);
			// 
			// rbJPEGLS
			// 
			this.rbJPEGLS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.rbJPEGLS.Location = new System.Drawing.Point(16, 120);
			this.rbJPEGLS.Name = "rbJPEGLS";
			this.rbJPEGLS.Size = new System.Drawing.Size(152, 32);
			this.rbJPEGLS.TabIndex = 2;
			this.rbJPEGLS.Text = "JPEG-LS";
			this.rbJPEGLS.CheckedChanged += new System.EventHandler(this.rbJPEGLS_CheckedChanged);
			// 
			// rbJPEG
			// 
			this.rbJPEG.Checked = true;
			this.rbJPEG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.rbJPEG.Location = new System.Drawing.Point(16, 56);
			this.rbJPEG.Name = "rbJPEG";
			this.rbJPEG.Size = new System.Drawing.Size(152, 32);
			this.rbJPEG.TabIndex = 1;
			this.rbJPEG.TabStop = true;
			this.rbJPEG.Text = "JPEG";
			this.rbJPEG.CheckedChanged += new System.EventHandler(this.rbJPEG_CheckedChanged);
			// 
			// gbJPEG2000Types
			// 
			this.gbJPEG2000Types.Controls.AddRange(new System.Windows.Forms.Control[] {
																						  this.rb2000Lossless,
																						  this.rb2000GrayScale,
																						  this.rb2000Lossy});
			this.gbJPEG2000Types.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.gbJPEG2000Types.Location = new System.Drawing.Point(280, 128);
			this.gbJPEG2000Types.Name = "gbJPEG2000Types";
			this.gbJPEG2000Types.Size = new System.Drawing.Size(168, 144);
			this.gbJPEG2000Types.TabIndex = 0;
			this.gbJPEG2000Types.TabStop = false;
			this.gbJPEG2000Types.Text = "JPEG 2000 Types";
			// 
			// rb2000Lossless
			// 
			this.rb2000Lossless.Location = new System.Drawing.Point(16, 112);
			this.rb2000Lossless.Name = "rb2000Lossless";
			this.rb2000Lossless.Size = new System.Drawing.Size(136, 24);
			this.rb2000Lossless.TabIndex = 2;
			this.rb2000Lossless.Text = "JPEG 2000 Lossless";
			// 
			// rb2000GrayScale
			// 
			this.rb2000GrayScale.Location = new System.Drawing.Point(16, 72);
			this.rb2000GrayScale.Name = "rb2000GrayScale";
			this.rb2000GrayScale.Size = new System.Drawing.Size(136, 32);
			this.rb2000GrayScale.TabIndex = 1;
			this.rb2000GrayScale.Text = "JPEG 2000 GrayScale";
			// 
			// rb2000Lossy
			// 
			this.rb2000Lossy.Checked = true;
			this.rb2000Lossy.Location = new System.Drawing.Point(16, 32);
			this.rb2000Lossy.Name = "rb2000Lossy";
			this.rb2000Lossy.Size = new System.Drawing.Size(144, 24);
			this.rb2000Lossy.TabIndex = 0;
			this.rb2000Lossy.TabStop = true;
			this.rb2000Lossy.Text = "JPEG 2000 Lossy";
			// 
			// cmdClose
			// 
			this.cmdClose.Location = new System.Drawing.Point(344, 376);
			this.cmdClose.Name = "cmdClose";
			this.cmdClose.Size = new System.Drawing.Size(88, 32);
			this.cmdClose.TabIndex = 1;
			this.cmdClose.Text = "Close";
			this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
			// 
			// rbRAW
			// 
			this.rbRAW.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.rbRAW.Location = new System.Drawing.Point(16, 272);
			this.rbRAW.Name = "rbRAW";
			this.rbRAW.Size = new System.Drawing.Size(152, 32);
			this.rbRAW.TabIndex = 7;
			this.rbRAW.Text = "Raw";
			this.rbRAW.CheckedChanged += new System.EventHandler(this.rbRAW_CheckedChanged);
			// 
			// SaveOptionsSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(496, 414);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdClose,
																		  this.grpSaveOptions});
			this.Name = "SaveOptionsSettings";
			this.Text = "SaveOptionsSettings";
			this.Load += new System.EventHandler(this.SaveOptionsSettings_Load);
			this.grpSaveOptions.ResumeLayout(false);
			this.gbJPEG2000Types.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SaveOptionsSettings_Load(object sender, System.EventArgs e)
		{
			gbJPEG2000Types.Visible = false;
		}

		private void rbJPEG_CheckedChanged(object sender, System.EventArgs e)
		{
			gbJPEG2000Types.Visible = false;
		}

		private void rbJPEGLS_CheckedChanged(object sender, System.EventArgs e)
		{
			gbJPEG2000Types.Visible = false;
		}

		private void rbJPEG2000_CheckedChanged(object sender, System.EventArgs e)
		{
			gbJPEG2000Types.Visible = rbJPEG2000.Checked;
		}
		
		private void rbLosslessJPG_CheckedChanged(object sender, System.EventArgs e)
		{
			gbJPEG2000Types.Visible = false;
		}

		private void rbRAW_CheckedChanged(object sender, System.EventArgs e)
		{
			gbJPEG2000Types.Visible = false;
		}
		
		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (rbJPEG.Checked)
			{
				SaveOptions.Format = ImageXFormat.Jpeg;
				sFilename = Application.StartupPath + "\\jpeg.jpg";		
			}	

			else if(rbJPEGLS.Checked)
			 {
				SaveOptions.Format = ImageXFormat.JpegLs;
				sFilename = Application.StartupPath + "\\jlsnear.jls";	
			 }

			else if(rbLosslessJPG.Checked)
			{
				//lossless JPEG
				sFilename = Application.StartupPath + "\\ljpRGB.ljp";

				SaveOptions.Format = ImageXFormat.LosslessJpeg;
				SaveOptions.Ljp.Type = LjpType.Rgb;
				
			}

			else if(rbRAW.Checked)
			{
				
				//RAW
				SaveOptions.Format = ImageXFormat.Raw;
				sFilename = Application.StartupPath + "\\raw.raw";
				
			}
			else if(rb2000Lossy.Checked && rbJPEG2000.Checked)
			{
				// JPEG 2000 lossy
				sFilename = Application.StartupPath + "\\jp2lossy.jp2";

				SaveOptions.Format = ImageXFormat.Jpeg2000;
				SaveOptions.Jp2.Order = ProgressionOrder.Default;
				SaveOptions.Jp2.Type = Jp2Type.Lossy;

				
	
			}
			else if(rb2000GrayScale.Checked && rbJPEG2000.Checked)
			{
				//JPEG 2000 GrayScale
				sFilename = Application.StartupPath + "\\jp2lossygray.jp2";
				
				SaveOptions.Format = ImageXFormat.Jpeg2000;
				SaveOptions.Jp2.Grayscale = true;
				SaveOptions.Jp2.Type = Jp2Type.Lossless;
	
			}

			else if(rb2000Lossless.Checked && rbJPEG2000.Checked)
			{
				//lossless JPEG2000
				sFilename = Application.StartupPath + "\\jp2lossless.jp2";
				
				SaveOptions.Format = ImageXFormat.Jpeg2000;
				SaveOptions.Jp2.Order = ProgressionOrder.Default;
				SaveOptions.Jp2.Type = Jp2Type.Lossless;
	
			}
						
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		public string GetSaveFileName()
		{
			
			return sFilename;
		}	

		public PegasusImaging.WinForms.ImagXpress9.SaveOptions GetSaveOptions()
		{
			
			return SaveOptions;
		}

		private void cmdClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		

			
	}
}

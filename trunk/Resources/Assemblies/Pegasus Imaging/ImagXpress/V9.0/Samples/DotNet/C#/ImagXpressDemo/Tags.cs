/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace ImagXpressDemo
{
    class Tags
    {
        private long thumbnailOffset;

        public long ThumbnailOffset
        {
            get
            {
                return thumbnailOffset;
            }
        }

        //enumeration for tag values in hex
        public enum ExifTagNumber : int
        {
            ProcessingSoftware = 0xb,
            NewSubFileType = 0xfe,
            SubFileType = 0xff,
            ImageWidth = 0x100,
            ImageLength = 0x101,
            BitsPerSample = 0x102,
            Compression = 0x103,
            PhotometricInterpretation = 0x106,
            Thresholding = 0x107,
            CellWidth = 0x108,
            CellLength = 0x109,
            FillOrder = 0x10a,
            DocumentName = 0x10d,
            ImageDescription = 0x10e,
            Make = 0x10f,
            Model = 0x110,
            StripOffsets = 0x111,
            Orientation = 0x112,
            SamplesPerPixel = 0x115,
            RowsPerStrip = 0x116,
            StripByteCounts = 0x117,
            MinSampleValue = 0x118,
            MaxSampleValue = 0x119,
            XResolution = 0x11a,
            YResolution = 0x11b,
            PlanarConfiguration = 0x11c,
            PageName = 0x11d,
            XPosition = 0x11e,
            YPosition = 0x11f,
            FreeOffsets = 0x120,
            FreeByteCounts = 0x121,
            GrayResponseUnit = 0x122,
            GrayResponseCurve = 0x123,
            T4Options = 0x124,
            T6Options = 0x125,
            ResolutionUnit = 0x128,
            PageNumber = 0x129,
            ColorResponseUnit = 0x12c,
            TransferFunction = 0x12d,
            Software = 0x131,
            DateTime = 0x132,
            Artist = 0x13b,
            HostComputer = 0x13c,
            Predictor = 0x13d,
            WhitePoint = 0x13e,
            PrimaryChromaticities = 0x13f,
            ColorMap = 0x140,
            HalftoneHints = 0x141,
            TileWidth = 0x142,
            TileLength = 0x143,
            TileOffsets = 0x144,
            TileByteCounts = 0x145,
            BadFaxLines = 0x146,
            CleanFaxData = 0x147,
            ConsecutiveBadFaxLines = 0x148,
            SubIFDs = 0x14A,
            InkSet = 0x14c,
            InkNames = 0x14d,
            NumberOfInks = 0x14e,
            DotRange = 0x150,
            TargetPrinter = 0x151,
            ExtraSamples = 0x152,
            SampleFormat = 0x153,
            SMinSampleValue = 0x154,
            SMaxSampleValue = 0x155,
            TransferRange = 0x156,
            ClipPath = 0x157,
            XClipPathUnits = 0x158,
            YClipPathUnits = 0x159,
            Indexed = 0x15A,
            JPEGTables = 0x15B,
            OPIProxy = 0x15f,
            GlobalParametersIFD = 0x190,
            ProfileType = 0x191,
            FaxProfile = 0x192,
            CodingMethods = 0x193,
            VersionYear = 0x194,
            ModeNumber = 0x195,
            Decode = 0x1b1,
            DefaultImageColor = 0x1b2,
            JpegProc = 0x200,
            JpegInterchangeFormat = 0x201,
            JpegInterchangeFormatLength = 0x202,
            JpegRestartInterval = 0x203,
            JpegLosslessPredictors = 0x205,
            JpegPointTransforms = 0x206,
            JpegQTables = 0x207,
            JpegDCTables = 0x208,
            JpegACTables = 0x209,
            YCbCrCoefficients = 0x211,
            YCbCrSubSampling = 0x212,
            YCbCrPositioning = 0x213,
            ReferenceBlackWhite = 0x214,
            StripRowCounts = 0x22f,
            XMP = 0x2BC,
            RelatedImageFormat = 0x1000,
            RelatedImageWidth = 0x1001,
            RelatedImageHeight = 0x1002,
            Rating = 0x4746,
            RatingPercent = 0x4749,
            ImageID = 0x800D,
            WangAnnotation = 0x80A4,
            Matteing = 0x80E3,
            DataType = 0x80E4,
            ImageDepth = 0x80E5,
            TileDepth = 0x80E6,
            Model2 = 0x827D,
            CFARepeatPatterDim = 0x828D,
            CFAPattern2 = 0x828E,
            BatteryLevel = 0x828F,
            KodakIFD = 0x8290,
            Copyright = 0x8298,
            ExposureTime = 0x829A,
            FNumber = 0x829D,
            MDFileTag = 0x82A5,
            MDScalePixel = 0x82A6,
            MDColorTable = 0x82A7,
            MDLabName = 0x82A8,
            MDSampleInfo = 0x82A9,
            MDPrepDate = 0x82AA,
            MDPrepTime = 0x82AB,
            MDFileUnits = 0x82AC,
            PixelScale = 0x830E,
            IPTC_NAA = 0x83BB,
            IntergraphPacketData = 0x847E,
            IntergraphFlagRegisters = 0x847F,
            IntegraphMatrix = 0x8480,
            ModelTiePoint = 0x8482,
            Site = 0x84E0,
            ColorSequence = 0x84E1,
            IT8Header = 0x84E2,
            RasterPadding = 0x84E3,
            BitsPerRunLength = 0x84E4,
            BitsPerExtendedRunLength = 0x84E5,
            ColorTable = 0x84E6,
            ImageColorIndicator = 0x84E7,
            BackgroundColorIndicator = 0x84E8,
            ImageColorValue = 0x84E9,
            BackgroundColorvalue = 0x84EA,
            PixelIntensityRange = 0x84EB,
            TransparencyIndicator = 0x84EC,
            ColorCharacterization = 0x84ED,
            HCUsage = 0x84EE,
            SEMInfo = 0x8546,
            AFCP_IPTC = 0x8568,
            ModelTransform = 0x85D8,
            WB_GRGBLevels = 0x8602,
            LeafData = 0x8606,
            PhotoShop = 0x8649,
            ExifIFD = 0x8769,
            ICCProfile = 0x8773,
            ImageLayer = 0x87AC,
            GeoTiffDirectory = 0x87AF,
            GeoTiffDoubleParams = 0x87B0,
            GeoTiffAsciiParams = 0x87B1,
            ExposureProg = 0x8822,
            SpectralSense = 0x8824,
            GPSInfo = 0x8825,
            ISOSpeed = 0x8827,
            OECF = 0x8828,
            Interlace = 0x8829,
            TimeZoneOffset = 0x882A,
            SelfTimerMode = 0x882B,
            FaxRecvParams = 0x885C,
            FaxSubAddress = 0x885D,
            FaxRecvTime = 0x885E,
            LeafSubIFD = 0x888E,
            ExifVer = 0x9000,
            DTOrig = 0x9003,
            DTDigitized = 0x9004,
            CompConfig = 0x9101,
            CompBPP = 0x9102,
            ShutterSpeed = 0x9201,
            Aperture = 0x9202,
            Brightness = 0x9203,
            ExposureBias = 0x9204,
            MaxAperture = 0x9205,
            SubjectDist = 0x9206,
            MeteringMode = 0x9207,
            LightSource = 0x9208,
            Flash = 0x9209,
            FocalLength = 0x920A,
            FlashEnergy = 0x920B,
            SpatialFrequencyResponse = 0x920C,
            Noise = 0x920D,
            FocalXRes = 0x920E,
            FocalYRes = 0x920F,
            FocalResUnit = 0x9210,
            ImageNumber = 0x9211,
            SecurityClassification = 0x9212,
            ImageHistory = 0x9213,
            SubjectLocation = 0x9214,
            ExposureIndex = 0x9215,
            Tiff_EPStandardID = 0x9216,
            SensingMethod = 0x9217,
            StoNits = 0x923F,
            MakerNote = 0x927C,
            UserComment = 0x9286,
            DTSubsec = 0x9290,
            DTOrigSS = 0x9291,
            DTDigSS = 0x9292,
            ImageSourceData = 0x935C,
            XPTitle = 0x9C9B,
            XPComment = 0x9C9C,
            XPAuthor = 0x9C9D,
            XPKeywords = 0x9C9E,
            XPSubject = 0x9C9F,
            FPXVer = 0xA000,
            ColorSpace = 0xA001,
            ExifPixXDim = 0xA002,
            ExifPixYDim = 0xA003,
            RelatedWav = 0xA004,
            Interop = 0xA005,
            FlashEnergy2 = 0xA20B,
            SpatialFR = 0xA20C,
            Noise2 = 0xA20D,
            FocalPlaneXResolution = 0xA20E,
            FocalPlaneYResolution = 0xA20F,
            FocalPlaneResolutionUnits = 0xA210,
            ImageNumber2 = 0xA211,
            SecurityClassification2 = 0xA212,
            ImageHistory2 = 0xA213,
            SubjectLocation2 = 0xA214,
            ExposureIndex2 = 0xA215,
            Tiff_EPStandardID2 = 0xA216,
            SensingMethod2 = 0xA217,
            FileSource = 0xA300,
            SceneType = 0xA301,
            CfaPattern = 0xA302,
            CustomRendered = 0xA401,
            ExposureMode = 0xA402,
            WhiteBalance = 0xA403,
            DigitalZoomRatio = 0xA404,
            FocalLengthIn35MMFormat = 0xA405,
            SceneCaptureType = 0xA406,
            GainControl = 0xA407,
            Contrast = 0xA408,
            Saturation = 0xA409,
            Sharpness = 0xA40A,
            DeviceSettingDescription = 0xA40B,
            SubjectDistanceRange = 0xA40C,
            ImageUniqueID = 0xA420,
            GDALMetaData = 0xA480,
            GDALNoData = 0xA481,
            Gamma = 0xA500,
            PixelFormat = 0xBC01,
            Transformation = 0xBC02,
            Uncompressed = 0xBC03,
            ImageType = 0xBC04,
            ImageWidth2 = 0xBC80,
            ImageHeight = 0xBC81,
            WidthResolution = 0xBC82,
            HeightResolution = 0xBC83,
            ImageOffset = 0xBCC0,
            ImageByteCount = 0xBCC1,
            AlphaOffset = 0xBCC2,
            AlphaByteCount = 0xBCC3,
            ImageDataDiscard = 0xBCC4,
            AlphaDataDiscard = 0xBCC5,
            OceScanJobDesc = 0xC427,
            OceApplicationSelector = 0xC428,
            OceIDNumber = 0xC429,
            OceImageLogic = 0x42A,
            Annotations = 0xC44F,
            PrintIM = 0xC4A5,
            DNGVersion = 0xC612,
            DNGBackwardVersion = 0xC613,
            UniqueCameraModel = 0xC614,
            LocalizedCameraModel = 0xC615,
            CFAPlaneColor = 0xC616,
            CFALayout = 0xC617,
            Linearizationtable = 0xC618,
            BlackLevelRepatDim = 0xC619,
            BlackLevel = 0xC61A,
            BlackLevelDeltaH = 0xC61B,
            BlackLevelDeltaV = 0xC61C,
            WhiteLevel = 0xC61D,
            DefaultScale = 0xC61E,
            DefaulCropOrigin = 0xC61F,
            DefaultCropSize = 0xC6120,
            ColorMatrix1 = 0xC621,
            ColorMatrix2 = 0xC622,
            CameraCalibration1 = 0xC623,
            CameraCalibration2 = 0xC624,
            ReductionMatrix1 = 0xC625,
            ReductionMatrix2 = 0xC626,
            AnalogBlance = 0xC627,
            AsShotNeutral = 0xC628,
            AsShotWhiteXY = 0xC629,
            BaselineExposure = 0xC62A,
            BaselineNoise = 0xC62B,
            BaselineSharpness = 0xC62C,
            BayerGreenSplit = 0xC62D,
            LinearResponseLimit = 0xC62E,
            CameraSerialNumber = 0xC62F,
            DNGLensInfo = 0xC630,
            ChromaBlurRadius = 0xC631,
            AntiAliasStrength = 0xC632,
            ShadowScale = 0xC633,
            SR2Private = 0xC634,
            MakerNoteSafety = 0xC635,
            RawImageSegmentation = 0xC640,
            CalibrationIlluminant1 = 0xC65A,
            CalibrationIlluminant2 = 0xC65B,
            BestQualityScale = 0xC65C,
            RawUniqueID = 0xC65D,
            AliasLayerMetaData = 0xC660,
            OriginalRawFileName = 0xC68B,
            OriginalRawFileData = 0xC68C,
            ActiveArea = 0xC68D,
            MaskedAreas = 0xC68E,
            AsShotICCProfile = 0xC68F,
            AsShotPreProfileMatrix = 0xC690,
            CurrentICCProfile = 0xC691,
            CurrentPreProfileMatrix = 0xC692,
            Padding = 0xEA1C,
            OffsetSchema = 0xEA1D,
            OwnerName = 0xFDE8,
            SerialNumber = 0xFDE9,
            Lens = 0xFDEA,
            RawFile = 0xFE4C,
            Converter = 0xFE4D,
            WhiteBalance2 = 0xFE4E,
            Exposure = 0xFE51,
            Shadows = 0xFE52,
            Brightness2 = 0xFE53,
            Contrast2 = 0xFE54,
            Saturation2 = 0xFE55,
            Sharpness2 = 0xFE56,
            Smoothness = 0xFE57,
            MoireFilter = 0xFE58,
        };

        ExifTagNumber[] expectedValuesForTags = new ExifTagNumber[]{
                       ExifTagNumber.PhotometricInterpretation, ExifTagNumber.Compression,
                       ExifTagNumber.Orientation, ExifTagNumber.PlanarConfiguration,
                       ExifTagNumber.ResolutionUnit, ExifTagNumber.PageNumber,
                       ExifTagNumber.Thresholding, ExifTagNumber.FillOrder,
                       ExifTagNumber.SubFileType, ExifTagNumber.NewSubFileType,
                       ExifTagNumber.GrayResponseUnit, ExifTagNumber.ExtraSamples,
                       ExifTagNumber.T6Options, ExifTagNumber.T4Options,
                       ExifTagNumber.Predictor,
                       ExifTagNumber.CleanFaxData, ExifTagNumber.InkSet,
                       ExifTagNumber.SampleFormat, ExifTagNumber.YCbCrPositioning,
                       ExifTagNumber.ExposureProg, ExifTagNumber.MeteringMode,
                       ExifTagNumber.LightSource, ExifTagNumber.Flash,
                       ExifTagNumber.ColorSpace, ExifTagNumber.FileSource,
                       ExifTagNumber.SceneType, ExifTagNumber.CustomRendered,
                       ExifTagNumber.WhiteBalance, ExifTagNumber.WhiteBalance2,
                       ExifTagNumber.SceneCaptureType, ExifTagNumber.GainControl,
                       ExifTagNumber.Contrast,ExifTagNumber.Contrast2,
                       ExifTagNumber.Saturation, ExifTagNumber.Saturation2,
                       ExifTagNumber.Sharpness, ExifTagNumber.Sharpness2,
                       ExifTagNumber.SubjectDistanceRange, ExifTagNumber.ExposureMode,
                       ExifTagNumber.YCbCrSubSampling, ExifTagNumber.ImageDataDiscard,
                       ExifTagNumber.AlphaDataDiscard, ExifTagNumber.Indexed,
                       ExifTagNumber.ProfileType, ExifTagNumber.FaxProfile,
                       ExifTagNumber.CodingMethods, ExifTagNumber.JpegProc,
                       ExifTagNumber.FocalPlaneResolutionUnits, ExifTagNumber.FocalLengthIn35MMFormat,
                       ExifTagNumber.SecurityClassification, ExifTagNumber.SecurityClassification2,
                       ExifTagNumber.SensingMethod, ExifTagNumber.SensingMethod2,
                       ExifTagNumber.MakerNoteSafety, ExifTagNumber.ImageType,
                       ExifTagNumber.Uncompressed, ExifTagNumber.Transformation,
                       ExifTagNumber.PixelFormat, ExifTagNumber.CFALayout,
                       ExifTagNumber.CalibrationIlluminant1,ExifTagNumber.CalibrationIlluminant2,
                       ExifTagNumber.CompConfig};
        
         //strings for display of tags
         string string_ProcessingSoftware = ("ProcessingSoftware");
         string string_NewSubFileType = ("NewSubFileType");
         string string_SubFileType = ("SubFileType");
         string string_ImageWidth = ("ImageWidth");
         string string_ImageLength = ("ImageLength/Height");
         string string_BitsPerSample = ("BitsPerSample");
         string string_Compression = ("Compression");
         string string_PhotometricInterpretation = ("PhotometricInterpretation");
         string stringThresholding = ("Thresholding");
         string string_CellWidth = ("CellWidth");
         string string_CellLength = ("CellLength/Height");
         string string_FillOrder = ("FillOrder");
         string string_DocumentName = ("DocumentName");
         string string_ImageDescription = ("ImageDescription");
         string string_Make = ("Make");
         string string_Model = ("Model");
         string string_StripOffsets = ("StripOffsets");
         string string_Orientation = ("Orientation");
         string string_SamplesPerPixel = ("SamplesPerPixel");
         string string_RowsPerStrip = ("RowsPerStrip");
         string string_StripByteCounts = ("StripByteCounts");
         string string_MinSampleValue = ("MinSampleValue");
         string string_MaxSampleValue = ("MaxSampleValue");
         string string_XResolution = ("XResolution");
         string string_YResolution = ("YResolution");
         string string_PlanarConfiguration = ("PlanarConfiguration");
         string string_PageName = ("PageName");
         string string_XPosition = ("XPosition");
         string string_YPosition = ("YPosition");
         string string_FreeOffsets = ("FreeOffsets");
         string string_FreeByteCounts = ("FreeByteCounts");
         string string_GrayResponseUnit = ("GrayResponseUnit");
         string string_GrayResponseCurve = ("GrayResponseCurve");
         string string4Options = ("T4Options/Group3options");
         string string6Options = ("T6Options/Group4Options");
         string string_ResolutionUnit = ("ResolutionUnit");
         string string_PageNumber = ("PageNumber");
         string string_ColorResponseUnit = ("ColorResponseUnit");
         string stringTransferFunction = ("TransferFunction");
         string string_Software = ("Software");
         string string_DateTime = ("DateTime");
         string string_Artist = ("Artist");
         string string_HostComputer = ("HostComputer");
         string string_Predictor = ("Predictor");
         string string_WhitePoint = ("WhitePoint");
         string string_PrimaryChromaticities = ("PrimaryChromaticities");
         string string_ColorMap = ("ColorMap");
         string string_HalftoneHints = ("HalftoneHints");      
         string stringFileWidth = ("TileWidth");
         string stringFileLength = ("TileLength/Height");
         string stringFileOffsets = ("TileOffsets");
         string stringFileByteCounts = ("TileByteCounts");
         string string_BadFaxLines = ("BadFaxLines");
         string string_CleanFaxData = ("CleanFaxData");
         string string_ConsecutiveBadFaxLines = ("ConsecutiveBadFaxLines");
         string string_SubIFDs = ("SubIFDs"); 
         string string_InkSet = ("InkSet");
         string string_InkNames = ("InkNames");
         string string_NumberOfInks = ("NumberOfInks");
         string string_DotRange = ("DotRange");
         string stringTargetPrinter = ("TargetPrinter");
         string string_ExtraSamples = ("ExtraSamples");
         string string_SampleFormat = ("SampleFormat");
         string string_SMinSampleValue = ("SMinSampleValue");
         string string_SMaxSampleValue = ("SMaxSampleValue");     
         string stringTransferRange = ("TransferRange");
         string string_ClipPath = ("ClipPath");
         string string_XClipPathUnits = ("XClipPathUnits");
         string string_YClipPathUnits = ("YClipPathUnits");
         string string_Indexed = ("Indexed");
         string string_JPEGTables = ("JPEGTables");
         string string_OPIProxy = ("OPIProxy");
         string string_GlobalParametersIFD = ("GlobalParametersIFD");
         string string_ProfileType = ("ProfileType");
         string string_FaxProfile = ("FaxProfile");
         string string_CodingMethods = ("CodingMethods");
         string string_VersionYear = ("VersionYear");
         string string_ModeNumber = ("ModeNumber");
         string string_Decode = ("Decode");
         string string_DefaultImageColor = ("DefaultImageColor");
         string string_jpegProc = ("jpegProc");
         string string_jpegInterchangeFormat = ("jpegInterchangeFormat");
         string string_jpegInterchangeFormatLength = ("jpegInterchangeFormatLength");
         string string_jpegRestartInterval = ("jpegRestartInterval");
         string string_jpegLosslessPredictors = ("jpegLosslessPredictors");        
         string string_jpegPointTransforms = ("jpegPointTransforms");
         string string_jpegQTables = ("jpegQTables");
         string string_jpegDCTTables = ("jpegDCTTables");
         string string_jpegACTables = ("jpegACTables");
         string string_YCbCrCoefficients = ("YCbCrCoefficients");
         string string_YCbCrSubSampling = ("YCbCrSubSampling");
         string string_YCbCrPositioning = ("YCbCrPositioning");
         string string_ReferenceBlackWhite = ("ReferenceBlackWhite");
         string string_StripRowCounts = ("StripRowCounts");
         string string_XMP = ("XMP");
         string string_RelatedImageFileFormat = ("RelatedImageFileFormat");
         string string_RelatedImageWidth = ("RelatedImageWidth");
         string string_RelatedImageHeight = ("RelatedImageHeight");
         string string_Rating = ("Rating");
         string string_RatingPercent = ("RatingPercent");
         string string_ImageID = ("ImageID");
         string string_WangAnnotations = ("Wang Annotations");
         string string_Matteing = ("Matteing");
         string string_DataType = ("DatType");
         string string_ImageDepth = ("ImageDepth");
         string string_TileDepth = ("TileDepth");
         string string_CFARepeatPatternDim = ("CFARepeatPatterDim");
         string string_CFAPattern2 = ("CFAPattern2");
         string string_BatteryLevel = ("BatteryLevel");
         string string_KodakIFD = ("KodakIFD");
         string string_Copyright = ("Copyright");
         string string_ExposureTime = ("ExposureTime");
         string string_FNumber = ("FNumber");
         string string_MDFileTag = ("MDFileTag");
         string string_MDScalePixel = ("MDScalePixel");
         string string_MDColorTable = ("MDColorTable");
        string string_MDLabName = ("MDLabName");
        string string_MDSampleInfo = ("MDSampleInfo");
        string string_MDPrepDate = ("MDPrepDate");
        string string_MDPrepTime = ("MDPrepTime");
        string string_MDFileUnits = ("MDFileUnits");
        string string_PixelScale = ("PixelScale");
        string string_IPTC_NAA = ("IPTC-NAA");
        string string_IntergraphPacketData = ("IntergraphPacketData");
        string string_IntergraphFlagRegisters = ("IntergraphFlagRegisters");
        string string_IntergraphMatrix = ("IntergraphMatrix");
        string string_ModelTiePoint = ("ModelTiePoint");
        string string_Site = ("Site");
        string string_ColorSequence = ("Color Sequence");
        string string_IT8Header = ("IT8Header");
        string string_RasterPadding = ("RasterPadding");
        string string_BitsPerRunLength = ("BitsPerRunLength");
        string string_BitsPerExtendedRunLength = ("BitsPerExtendedRunLength");
        string string_ColorTable = ("ColorTable");
        string string_ImageColorIndicator = ("ImageColorIndicator");
        string string_BackgroundColorIndicator = ("BackgroundColorIndicator"); 	 
        string string_ImageColorValue = ("ImageColorValue");
        string string_BackgroundColorValue = ("BackgroundColorValue"); 	 
        string string_PixelIntensityRange = ("PixelIntensityRange");
        string string_TransparencyIndicator = ("TransparencyIndicator");
        string string_ColorCharacterization = ("ColorCharacterization");
        string string_HCUsage = (" HCUsage");
        string string_SEMInfo = ("SEMInfo");
        string string_AFCP_IPTC = ("AFCP_IPTC");
        string string_ModelTransform = ("ModelTransform");
        string string_WB_GRGBLevels = ("WB_GRGBLevels");
        string string_LeafData = ("LeafData");
        string string_PhotoshopSettings = ("PhotoshopSettings");
        string string_ICC_Profile = ("ICC_Profile");
        string string_ImageLayer = ("ImageLayer");
        string string_GeoTiffDirectory = ("GeoTiffDirectory");
        string string_GeoTiffDoubleParams = ("GeoTiffDoubleParams");
        string string_GeoTiffAsciiParams = ("GeoTiffAsciiParams");
        string string_ExposureProgram = ("ExposureProgram");
        string string_SpectralSensitivity = ("SpectralSensitivity");
        string string_GPSInfo = ("GPSInfo/IFD");
        string string_ExifIFD = ("ExifIFD/Offset");
        string string_ISOSpeed = ("ISOSpeedRatings");
        string string_Opto_ElectricConvFactor = ("Opto-ElectricConvFactor");
        string string_Interlace = ("Interlace");
        string string_TimeZoneOffset = ("TimeZoneOffset");
        string string_SelfTimerMode = ("SelfTimerMode");
        string string_FaxRecvParams = ("FaxRecvParams");
        string string_FaxSubAddress = ("FaxSubAddress");
        string string_FaxRecvTime = ("FaxRecTime");
        string string_LeafSubIFD = ("LeafSubIFD");
        string string_ExifVer = ("ExifVersion");
        string string_DTOrig = ("DateTimeOriginal");
        string string_DateTimeDigitized = ("DateTimeDigitized");
        string string_CompConfig = ("ComponentsConfiguration");
        string string_CompBPP = ("CompressedBitsPerPixel");
        string string_ShutterSpeed = ("ShutterSpeed");
        string string_Aperture = ("Aperture");
        string string_Brightness = ("Brightness");
        string string_ExposureCompensation = ("ExposureCompensation/Bias");
        string string_MaxAperture = ("MaxApertureValue");
        string string_SubjectDist = ("SubjectDistance");
        string string_MeteringMode = ("MeteringMode");
        string string_LightSource = ("LightSource");
        string string_Flash = ("Flash");
        string string_FocalLength = ("FocalLength");
        string string_FlashEnergy = ("FlashEnergy");
        string string_SpatialFR = ("SpatialFrequencyResponse");
        string string_Noise = ("Noise");
        string string_FocalXRes = ("FocalPlaneXResolution");
        string string_FocalYRes = ("FocalPlaneYResolution");
        string string_FocalResUnit = ("FocalResolutionUnit");
        string string_ImageNumber = ("ImageNumber");
        string string_SecurityClassification = ("SecurityClassification");
        string string_ImageHistory = ("ImageHistory");
        string string_SubjectLoc = ("SubjectLocation");
        string string_ExposureIndex = ("ExposureIndex");
        string string_Tiff_EPStandardID = ("Tiff-EPStandardID");
        string string_SensingMethod = ("SensingMethod");
        string string_StoNits = ("StoNits");
        string string_MakerNote = ("MakerNote");
        string string_UserComment = ("UserComment");
        string string_DTSubsec = ("DateTimeSubseconds");
        string string_DTOrigSS = ("DateTimeOrigSubseconds");
        string string_DTDigSS = ("DateTimeDigitizedSubseconds");
        string string_ImageSourceData = ("ImageSourceData");
        string string_XPTitle = ("XPTitle");
        string string_XPComment = ("XPComment");
        string string_XPAuthor = ("XPAuthor");
        string string_XPKeywords = ("Keywords");
        string string_XPSubject = ("XPSubject");
        string string_FPXVer = ("FlashPixVersion");
        string string_ColorSpace = ("ColorSpace");
        string string_ExifImageWidth = ("ExifImageWidth");
        string string_ExifImageHeight = ("ExifImageHeight");
        string string_RelatedWav = ("RelatedSoundFile");
        string string_Interop = ("InteroperabilityIFD/Offset");
        string string_FocalXResolution = ("FocalXResolution");
        string string_FocalYResolution = ("FocalYResolution");
        string string_FocalResolutionUnit = ("FocalResolutionUnit");
        string string_FileSource = ("FileSource");
        string string_SceneType = ("SceneType");
        string string_CfaPattern = ("CfaPattern");
        string string_CustomRendered = ("CustomRendered");
        string string_ExposureMode = ("ExposureMode");
        string string_WhiteBalance = ("WhiteBalance");
        string string_DigitalZoomRatio = ("DigitalZoomRatio");
        string string_FocalLengthIn35MMFormat = ("FocalLengthIn35MMFormat");
        string string_SceneCaptureType = ("SceneCaptureType");
        string string_GainControl = ("GainControl");
        string string_Contrast = ("Contrast");
        string string_Saturation = ("Saturation");
        string string_Sharpness = ("Sharpness");
        string string_DeviceSettingDescription = ("DeviceSettingDescription");
        string string_SubjectDistanceRange = ("SubjectDistanceRange");
        string string_ImageUniqueID = ("ImageUniqueID");
        string string_GDALMetaData = ("GDALMetaData");
        string string_GDALNoData = ("GDALNoData");
        string string_Gamma = ("Gamma");
        string string_PixelFormat = ("PixelFormat");
        string string_Transformation = ("Transformation");
        string string_Uncompressed = ("Uncomprssed");
        string string_ImageType = ("ImageType");
        string string_ImageHeight = ("ImageHeight");
        string string_WidthResolution = ("WidthResolution");
        string string_HeightResolution = ("HeightResolution");
        string string_ImageOffset =("ImageOffset");
        string string_ImageByteCount =("ImageByteCount");
        string string_AlphaOffset = ("AlphaOffset");
        string string_AlphaByteCount = ("AlphaByteCount");
        string string_ImageDataDiscard = ("ImageDataDiscard");
        string string_AlphaDataDiscard = ("AlphaDataDiscard");
        string string_OceScanJobDesc = ("OceScanJobDesc");
        string string_OceApplicationSelector = ("OceApplicationSelector");
        string string_OceIDNumber = ("OceIDNumber");
        string string_OceImageLogic = ("OceImageLogic");        
        string string_Annotations = ("Annotations");
        string string_PrintIM = ("PrintIM");
        string string_DNGVersion =("DNGVersion");
        string string_DNGBackwardVersion = ("DNGBackwardVersion");
        string string_UniqueCameraModel = ("UniqueCameraModel");
        string string_LocalizedCameraModel = ("LocalizedCameraModel");
        string string_CFAPlaneColor =("CFAPlaneColor");
        string string_CFALayout = ("CFALayout");
        string string_Linearizationtable = ("Linearizationtable");
        string string_BlackLevelRepatDim = ("BlackLevelRepatDim");
        string string_BlackLevel = ("BlackLevel");
        string string_BlackLevelDeltaH = ("BlackLevelDeltaH");
        string string_BlackLevelDeltaV = ("BlackLevelDeltaV");
        string string_WhiteLevel = ("WhiteLevel");
        string string_DefaultScale = ("DefaultScale");
        string string_DefaulCropOrigin = ("DefaulCropOrigin");
        string string_DefaultCropSize = ("DefaultCropSize");
        string string_ColorMatrix1 = ("ColorMatrix1");
        string string_ColorMatrix2 = ("ColorMatrix2");
        string string_CameraCalibration1 = ("CameraCalibration1");
        string string_CameraCalibration2 = ("CameraCalibration2");
        string string_ReductionMatrix1 = ("ReductionMatrix1");
        string string_ReductionMatrix2 = ("ReductionMatrix2");
        string string_AnalogBlance = ("AnalogBlance");
        string string_AsShotNeutral = ("AsShotNeutral");
        string string_AsShotWhiteXY = ("AsShotWhiteXY");
        string string_BaselineExposure =("BaselineExposure");
        string string_BaselineNoise = ("BaselineNoise");
        string string_BaselineSharpness = ("BaselineSharpness");
        string string_BayerGreenSplit = ("BayerGreenSplit");
        string string_LinearResponseLimit = ("LinearResponseLimit");
        string string_CameraSerialNumber = ("CameraSerialNumber");
        string string_DNGLensInfo = ("DNGLensInfo");
        string string_ChromaBlurRadius = ("ChromaBlurRadius");
        string string_AntiAliasStrength = ("AntiAliasStrength");
        string string_ShadowScale = ("ShadowScale");
        string string_SR2Private = ("SR2Private");
        string string_MakerNoteSafety = ("MakerNoteSafety");
        string string_RawImageSegmentation = ("CalibrationIlluminant1");
        string string_CalibrationIlluminant1 = ("CalibrationIlluminant1");
        string string_CalibrationIlluminant2 = ("CalibrationIlluminant2");
        string string_BestQualityScale = ("BestQualityScale");
        string string_RawUniqueID = ("RawUniqueID");
        string string_AliasLayerMetaData = ("AliasLayerMetaData");
        string string_OriginalRawFileName =("OriginalRawFileName");
        string string_OriginalRawFileData = ("OriginalRawFileData");
        string string_ActiveArea = ("ActiveArea");
        string string_MaskedAreas = ("MaskedAreas");                
        string string_AsShotICCProfile = ("ICCProfile");
        string string_AsShotPreProfileMatrix = ("AsShotPreProfileMatrix");
        string string_CurrentICCProfile = ("CurrentICCProfile");
        string string_CurrentPreProfileMatrix =("CurrentPreProfileMatrix");
        string string_Padding =("Padding");
        string string_OffsetSchema =("OffsetSchema");
        string string_OwnerName =("OwnerName");
        string string_SerialNumber = ("SerialNumber");
        string string_Lens = ("Lens");
        string string_RawFile = ("RawFile");
        string string_Converter = ("Converter");
        string string_Exposure = ("Exposure");
        string string_Shadows = ("Shadows");
        string string_Smoothness = ("Smoothness");
        string string_MoireFilter = ("MoireFilter");
        string string_Default = ("Unknown");

        string[] interOpIndexIndex = new string[] { "R03", "R98", "THM" };
        string[] interOpIndexDisplay = new string[] { "DCF option file", "DCF basic file", "DCF thumbnail file" };

        int[] newSubFileTypeIndex = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        string[] newSubFileTypeDisplay = new string[] { "Full Resolution Image", "Reduced-resolution Image", 
            "Single Page of a Multi-Page Image", "Single Page of a Multi-Image Reduced-Resolution Image", "Transparency Mask", 
            "Transparency Mask of Reduced-Resolution Image", "Transparency Mask of Multi-Page Image", 
            "Transparency Mask of Reduced-Resolution Multi-Page Image" };

        int[] subFileTypeIndex = new int[] { 1, 2, 3 };
        string[] subFileTypeDisplay = new string[] {"Full Resolution image", "Reduced-Resolution Image", 
            "Single Page of a Multi-Page Image" };

        int[] compresssionIndex = new int[]{1,2,3,4,5,6,7,8,9,10,99,262,32766,32767,32769,32771,32773,32809,32867,32895,32896,
            32897,32898,32908,32909,32946,32947,34661,34676,34677,34712,34713,65000,65535};
        string[] compressionDisplay = new string[]{"No Compression", "CCITT modified Huffman RLE", 
            "CCITT Group 3 fax encoding", "CCITT Group 4 fax encoding", "LZW", "JPEG old-style", "JPEG", "Adobe Deflate", 
            "JBIG B/W", "JBIG Color", "JPEG", "Kodak262", "Next", "Sony ARW Compressed", "Epson ERF Compressed", "CCIRLEW",
        "Packbits", "Thunderscan", "Kodak KDC Compressed", "IT8CTPAD", "IT8LW", "IT8MP", "IT8BL", "PixarFilm", "PixarLog",
        "Deflate", "DCS", "JBIG", "SGILog", "SGILog24", "JPEG 2000", "Nikon NEF Compressed", "Kodak DCR Compressed",
        "Pentax PEF Compressed"};

        int[] photometericInterpretationIndex = new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 32803, 32844, 32845, 34892 };
        string[] photometericInterpretationDisplay = new string[] {"WhiteIsZero", "BlackIsZero", "RGB", "Palette color", 
            "Transparency Mask", "CMYK", "YCbCr", "CIE L*a*b*", "ICC L*a*b*", "ITU L*a*b*", "Color Filter Array",
         "Pixar LogL", "Pixar LogLuv", "Linear Raw"};

        int[] thresholdingIndex = new int[] { 1, 2, 3 };
        string[] thresholdingDisplay = new string[] { "Bilevel", "HalfTone", "ErrorDiffuse" };

        int[] orientationIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        string[] orientationDisplay = new string[] {"(0,0) is TopLeft", "(0,0) is TopRight", "(0,0) is BottomRight", 
            "(0,0) is BottomLeft", "(0,0) is LeftTop", "(0,0) is RightTop", "(0,0) is RightBottom", "(0,0) is LeftBottom"};

        int[] fillOrderIndex = new int[] { 1, 2 };
        string[] fillOrderDisplay = new string[] { "MSB2LSB", "LSB2MSB" };

        int[] planarConfigurationIndex = new int[] { 1, 2 };
        string[] planarConfigurationDisplay = new string[] { "Chunky", "Planar" };

        int[] t4OptionsIndex = new int[] { 0, 1, 2, 4};
        string[] t4OptionsDisplay = new string[] { "1DEncoding", "2DEncoding", "Uncompressed", "Fillbits" };

        int[] t6OptionsIndex = new int[] { 0, 2 };
        string[] t6OptionsDisplay = new string[] { "Compressed", "Uncompressed" };

        int[] resolutionUnitIndex = new int[] { 1, 2, 3 };
        string[] resolutionUnitDisplay = {"None", "Inch", "Centimeter" };

        int[] grayResponseUnitIndex = new int[] { 1, 2, 3, 4, 5 };
        string[] grayResponseUnitDisplay = { "Tenths", "Hundredths", "Thousandths", "Ten-Thousandths", "Hundred-Thousdandths" };

        int[] predictorIndex = new int[] { 1, 2, 3 };
        string[] predictorDisplay = new string[] { "None", "Horizontal Differencing" , "Floating Point"};

        int[] cleanFaxDataIndex = new int[] { 0, 1, 2 };
        string[] cleanFaxDataDisplay = new string[] { "Clean", "Regenerated", "Unclean" };

        int[] inkSetIndex = new int[] { 1, 2 };
        string[] inkSetDisplay = new string[] { "CMYK", "Not CMYK" };

        int[] indexedIndex = new int[] { 0, 1 };
        string[] indexedDisplay = new string[] { "Not indexed", "Indexed" };

        int[] profileTypeIndex = new int[] { 0, 1 };
        string[] profileTypeDisplay = new string[] { "Unspecified", "Group 3 Fax" };

        int[] faxProfileIndex = new int[] { 0, 1, 2, 3, 4, 5, 6 };
        string[] faxProfileDisplay = new string[]{"Unknown", "Minimal B&W Lossless, Profile S", "Extended B&W Lossless, Profile F",
            "Lossless JBIG B&W, Profile J", "Lossy color and grayscale, Profile C", "Lossless color and grayscale, Profile L",
            "Mixed raster content, Profile M"};

        int[] codingMethodsIndex = new int[] { 0, 1, 2, 3, 4, 5, 6 };
        string[] codingMethodsDisplay = new string[]{"Unspecified compression", "Modified Huffman", "Modified read",
            "Modified MR", "JBIG", "Baseline JPG", "JBIG color"};

        int[] jpegProcIndex = new int[] { 1, 14 };
        string[] jpegProcDisplay = new string[] { "Baseline", "Lossless" };

        int[] focalPlaneResolutionUnitIndex = new int[] { 1, 2, 3, 4, 5 };
        string[] focalPlaneResolutionUnitDisplay = new string[] { "None", "Inches", "cm", "mm", "um" };

        string[] securityClassificationIndex = new string[] { "C", "R", "S", "T", "U" };
        string[] securityClassificationDisplay = new string[] {"Confidential", "Restricted", "Secret","Top Secret",
            "Unclassified"};

        int[] sensingMethodIndex = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        string[] sensingMethodDisplay = new string[] {"Monochrome area", "One-chip color area", "Two-chip color area",
            "Three-chip color area", "Color sequential area", "Monochrome linear", "Trilinear", "Color sequential linear"};

        int[] makerNoteSafetyIndex = new int[] { 0, 1 };
        string[] makerNoteSafetyDisplay = new string[] { "Unsafe", "Safe" };

        int[] imageTypeIndex = new int[] { 0, 1 };
        string[] imageTypeDisplay = new string[] { "Preview", "Page" };

        int[] uncompressedIndex = new int[] { 0, 1 };
        string[] uncompressedDisplay = new string[] { "No", "Yes" };

        int[] transformationIndex = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        string[] transformationDisplay = new string[]{"(0,0) is TopLeft","(0,0) is BottomLeft","(0,0) is TopRight",
            "(0,0) is BottomRight",  "(0,0) is RightTop","(0,0) is RightBottom","(0,0) is LeftTop",  "(0,0) is LeftBottom"};

        int[] imageDataDiscardIndex = new int[] { 0, 1, 2, 3 };
        string[] imageDataDiscardDisplay = new string[]{"Full Resolution", "Flexbits discarded", "Highpass frequency data discarded",
            "Highpass and Lowpass frequency data discarded"};

        string[] pageNumberDisplay = { "Page ", " of " };

        int[] extraSamplesIndex = new int[] { 0, 1, 2 };
        string[] extraSamplesDisplay = { "Unspecified", "Associated Alpha", "Unassociated Alpha" };

        int[] yCbCrPositioningIndex = new int[] { 1, 2 };
        string[] yCbCrPositioningDisplay = new string[] { "Centered", "Cosited" };

        int[] exposureProgramIndex = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        string[] exposureProgramDisplay = new string[]{"Not Defined", "Manual", "Program AE/Normal",
            "Aperture-priority AE", "Shutter Speed priority AE", "Creative (Slow speed)", 
            "Action (High Speed)", "Portrait", "Landscape"};

        int[] meteringModeIndex = new int[] { 0, 1, 2, 3, 4, 5, 6, 255 };
        string[] meterindgModeDisplay = new string[]{"Unknown", "Average", "Center-weighted average",
            "Spot", "Multi-spot", "Multi-segment", "Partial", "Other"};

        int[] lightSourceIndex = new int[] { 0, 1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 255 };
        string[] lightSourceDisplay = new string[]{"Unknown", "Daylight"," Fluorescent", "Tungsten" ,"Flash",
            "Fine Weather","Cloudy","Shade","Daylight Fluorescent","Day White Fluorescent","Cool White Fluorescent",
            "White Fluorescent","Standard Light A","Standard Light B","Standard Light C","D55","D65","D75","D50",
            "ISO Studio Tungsten","Other"};

        int[] flashIndex = new int[]{0,1,5,7,8,9,0xd,0xf,0x10,0x14,0x18,0x19,0x1d,0x1f,0x20,0x30,0x41,0x45,
            0x47,0x49,0x4d,0x4f,0x50,0x58,0x59,0x5d,0x5f};

        string[] flashDisplay = new string[]{"No Flash", "Fired", "Fired, Return not detected", "Fired, Return detected",
            "On, Did not fire", "On", "On, Return not detected", "On, Return detected", "Off", "Off, Did not fire, Return not detected",
            "Auto, Did not fire", "Auto, Fired", "Auto, Fired, Return not detected", "Auto, Fired, Return detected",
            "No flash function", "Off, No flash function", "Fired, Red-eye reduction", "Fired, Red-eye reduction, Return not detected",
            "Fired, Red-eye reduction, Return detected", "On, Red-eye reduction", "On, Red-eye reduction, Return not detected",
            "On, Red-eye reduction, Return detected", "Off, Red-eye reduction", "Auto, Did not fire, Red-eye reduction",
            "Auto, Fired, Red-eye reduction", "Auto, Fired, Red-eye reduction, Return not detected", "Auto, Fired, Red-eye reduction, Return detected"};

        int[] colorSpaceIndex = new int[] { 1, 2, 65535 };
        string[] colorSpaceDisplay = new string[] { "sRGB", "Adobe RGB", "Uncalibrated" };

        int[] fileSourceIndex = new int[] { 1, 2, 3 };
        string[] fileSourceDisplay = new string[] { "Film Scanner", "Reflection Print Scanner", "Digital Camera" };

        int[] sceneTypeIndex = new int[] { 1 };
        string[] sceneTypeDisplay = new string[] { "Directly photographed" };

        int[] customRenderedIndex = new int[] { 0, 1 };
        string[] customRenderedDisplay = new string[] { "Normal", "Custom" };

        int[] exposureModeIndex = new int[] { 0, 1, 2 };
        string[] exposureModeDisplay = new string[] { "Auto", "Manual", "Auto Bracket" };

        int[] whiteBalanceIndex = new int[] { 0, 1 };
        string[] whiteBalanceDisplay = new string[] { "Auto", "Manual" };

        int[] sceneCaptureTypeIndex = new int[] { 0, 1, 2, 3 };
        string[] sceneCaptureTypeDisplay = new string[] { "Standard", "Lanscape", "Portrait", "Night" };

        int[] gainControlIndex = new int[] { 0, 1, 2, 3, 4 };
        string[] gainControlDisplay = new string[] { "None", "Low gain up", "High gain up", "Low gain down", "High gain down" };

        int[] contrastIndex = new int[] { 0, 1, 2 };
        string[] contrastDisplay = new string[] { "Normal", "Low", "High" };

        int[] saturationIndex = new int[] { 0, 1, 2 };
        string[] saturationDisplay = new string[] { "Normal", "Low", "High" };

        int[] sharpnessIndex = new int[] { 0, 1, 2 };
        string[] sharpnessDisplay = new string[] { "Normal", "Soft", "Hard" };

        int[] subjectDistanceRangeIndex = new int[] { 0, 1, 2, 3 };
        string[] subjectDistanceRangeDisplay = new string[] { "Unknown", "Macro", "Close", "Distant" };

        string[] yCbCrSubsamplingIndex = new string[] { "1 1", "1 2", "2 1", "2 2", "4 1", "4 2" };
        string[] yCbCrSubsamplingDisplay = new string[] { "4:4:4", "4:4:0", "4:2:2", "4:2:0", "4:1:1", "4:1:0" };

        int[] pixelFormatIndex = new int[] { 0x5,0x8,0x9,0xa,0xb,0xc,0xd,0xe,0xf,0x10,0x11,0x12,0x13,0x15,0x16,0x17,
        0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0x2b,0x2c,0x2d,
        0x2e,0x2f,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3d,0x3e,0x3f};

        string[] pixelFormatDisplay = new string[] {"Black & White","8-bit Gray","16-bit BGR555","16-bit BGR565","16-bit Gray",
            "24-bit BGR","24-bit RGB","32-bit BGR","32-bit BGRA","32-bit PBGRA","32-bit Gray Float","48-bit RGB Fixed Point",
            "32-bit BGR101010","48-bit RGB","64-bit RGBA","64-bit PRGBA","96-bit RGB Fixed Point","128-bit RGBA Float",
            "128-bit PRGBA Float","128-bit RGB Float","32-bit CMYK","64-bit RGBA Fixed Point","128-bit RGBA Fixed Point",
            "64-bit CMYK","24-bit 3 Channels","32-bit 4 Channels","40-bit 5 Channels","48-bit 6 Channels","56-bit 7 Channels",
            "64-bit 8 Channels","48-bit 3 Channels","64-bit 4 Channels","80-","bit 5 Channels","96-bit 6 Channels","112-bit 7 Channels",
            "128-bit 8 Channels","40-bit CMYK Alpha","80-bit CMYK Alpha","32-bit 3 Channels Alpha","40-bit 4 Channels Alpha",
            "48-bit 5 Channels Alpha","56-bit 6 Channels Alpha","64-bit 7 Channels Alpha","72-bit 8 Channels Alpha","64-bit 3 Channels Alpha",
            "80-bit 4 Channels Alpha","96-bit 5 Channels Alpha","112-bit 6 Channels Alpha","128-bit 7 Channels Alpha",
            "144-bit 8 Channels Alpha","64-bit RGBA Half","48-bit RGB Half","32-bit RGBE","16-bit Gray Half","32-bit Gray Fixed Point"};

        int[] cFALayoutIndex = new int[] { 1, 2, 3, 4, 5 };
        string[] cFALayoutDisplay = new string[]{"Rectangular", "Even columns offset down 1/2 row","Even columns offset up 1/2 row",
            "Even rows offset right 1/2 column", "Even rows offset left 1/2 column"};

        int[] componentsConfigurationIndex = new int[] { 1, 2, 3, 4, 5, 6 };
        string[] componentsConfigurationDisplay = new string[] { "Y", "Cb", "Cr", "R", "G", "B" };

        int[] sampleFormatIndex = new int[] {1, 2, 3, 4, 5, 6 };
        string[] sampleFormatDisplay = new string[] {"Unsigned Integer", "Two's Complement Signed Integer", 
            "IEEE Floating Point", "Undefined", "Complex Integer", "Complex IEEE Floating Point"};

        private string tagDescription;

        public void ShowTags(ImageXTagCollection tags, ListView listView, ListView listThumbnail)
        {
            ImageXTag tag;
            
            //clear ListView
            listView.Items.Clear();
            
            byte oldTagLevel = 0;

            //identify level 0 of tags
            listView.Items.Add(new ListViewItem(new string[] { "***Property Level " + "0", "-----------------------------", "-----------------------------", "-----------------------------", "-----------------------------" }));

            //reset strings
            tagDescription = "";
            string tagNumber = "";
            string tagType = "";
            string tagCount = "";
            string tagData = "";

            StringBuilder sb;

            if (tags != null)
            {
                //loop through all tags found
                for (int i = 0; i < tags.Count; i++)
                {
                    tagDescription = "";
                    tagNumber = "";
                    tagType = "";
                    tagData = "";

                    tag = tags[i];

                    //indicate new tag level
                    if (tag.TagLevel != oldTagLevel)
                    {
                        if (tag.TagLevel == 1)
                        {
                            listThumbnail.Items.Add(new ListViewItem(new string[] { "***Property Level " + tag.TagLevel.ToString(), "-----------------------------", "-----------------------------", "-----------------------------", "-----------------------------" }));
                        }
                        else
                        {
                            listView.Items.Add(new ListViewItem(new string[] { "***Property Level " + tag.TagLevel.ToString(), "-----------------------------", "-----------------------------", "-----------------------------", "-----------------------------" }));                        
                        }

                        oldTagLevel = tag.TagLevel;
                    }

                    tagNumber = tag.TagNumber.ToString();
                    tagType = tag.TagType.ToString();
                    tagCount = tag.TagElementsCount.ToString();

                    //create tag data based on tag type
                    switch (tag.TagType)
                    {
                        case TagTypes.Byte:
                        case TagTypes.Undefine:
                            try
                            {
                                byte[] byteData = tag.GetTagBytes();

                                sb = new StringBuilder(byteData.Length);

                                for (int j = 0; j < byteData.Length; j++)
                                {
                                    sb.Append(byteData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Ascii:
                            try
                            {
                                byte[] asciiData = tag.GetTagBytes();

                                tagData = Encoding.ASCII.GetString(asciiData);
                            }
                            catch { }
                            break;
                        case TagTypes.Sbyte:
                            try
                            {
                                short[] shortData = tag.GetTagSBytes();

                                sb = new StringBuilder(shortData.Length);

                                for (int j = 0; j < shortData.Length; j++)
                                {
                                    sb.Append(shortData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Short:
                            try
                            {
                                int[] shortData = tag.GetTagUInt16();

                                sb = new StringBuilder(shortData.Length);

                                for (int j = 0; j < shortData.Length; j++)
                                {
                                    sb.Append(shortData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Sshort:
                            try
                            {
                                short[] shortData = tag.GetTagInt16();

                                sb = new StringBuilder(shortData.Length);

                                for (int j = 0; j < shortData.Length; j++)
                                {
                                    sb.Append(shortData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Long:
                            try
                            {
                                long[] longData = tag.GetTagUInt32();

                                sb = new StringBuilder(longData.Length);

                                for (int j = 0; j < longData.Length; j++)
                                {
                                    sb.Append(longData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Slong:
                            try
                            {
                                int[] longData = tag.GetTagInt32();

                                sb = new StringBuilder(longData.Length);

                                for (int j = 0; j < longData.Length; j++)
                                {
                                    sb.Append(longData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Rational:
                            try
                            {
                                long[] rationalData = tag.GetTagRational();

                                sb = new StringBuilder(rationalData.Length);

                                for (int j = 0; j < rationalData.Length; j += 2)
                                {
                                    sb.Append(rationalData[j].ToString() + "/" + rationalData[j + 1].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Srational:
                            try
                            {
                                int[] rationalData = tag.GetTagSRational();

                                sb = new StringBuilder(rationalData.Length);

                                for (int j = 0; j < rationalData.Length; j += 2)
                                {
                                    sb.Append(rationalData[j].ToString() + "/" + rationalData[j + 1].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;
                        case TagTypes.Float:
                            try
                            {
                                float[] floatData = tag.GetTagFloat();

                                sb = new StringBuilder(floatData.Length);

                                for (int j = 0; j < floatData.Length; j++)
                                {
                                    sb.Append(floatData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;

                        case TagTypes.Double:
                            try
                            {
                                double[] doubleData = tag.GetTagDouble();

                                sb = new StringBuilder(doubleData.Length);

                                for (int j = 0; j < doubleData.Length; j++)
                                {
                                    sb.Append(doubleData[j].ToString() + " ");
                                }

                                tagData = sb.ToString();
                            }
                            catch { }
                            break;

                        default: break;

                    }

                    string zero = "0";

                    //get the tag description
                    tagDescription = GetDescription(Int32.Parse(tagNumber), zero)[0];

                    int tagNum = Int32.Parse(tagNumber);

                    foreach (ExifTagNumber ExifTagNum in expectedValuesForTags)
                    {
                        if ((int)ExifTagNum == tagNum)
                            tagData = GetDescription(Int32.Parse(tagNumber), tagData)[1];
                    }

                    try
                    {
                        //add tag info to ListView
                        if (tag.TagLevel == 1)
                        {
                            if (Convert.ToInt32(tagNumber) == 513)
                            {
                                thumbnailOffset = Convert.ToInt64(tagData);
                            }

                            listThumbnail.Items.Add(new ListViewItem(new string[] { tagDescription, tagNumber, tagType, tagCount, tagData }));
                        }
                        else
                        {
                            listView.Items.Add(new ListViewItem(new string[] { tagDescription, tagNumber, tagType, tagCount, tagData }));
                        }
                    }
                    catch { }
                }
            }
        }

        private string KnownTagValueUndefined(int[] index, string[] display, string data)
        {
            try
            {
                string returnString = "";

                for (int i = 0; i < data.Length; i++)
                {
                    for (int j = 0; j < index.Length; j++)
                    {
                        try
                        {
                            if (Int32.Parse(data[i].ToString()) == index[j])
                            {
                                returnString += display[j];
                            }
                        }
                        catch (FormatException) { }
                    }
                }

                return returnString + " | Value: " + data;
            }
            catch
            {
                return "Unknown" + " | Value: " + data;
            }
        }

        private string KnownTagValue(int[] index, string[] display, string data)
        {
            try
            {
                for (int i = 0; i < index.Length; i++)
                {
                    if (Convert.ToInt32(data) == index[i])
                    {
                        return display[i] + " | Value: " + data;
                    }
                }

                return data;
            }
            catch
            {
                return "Unknown" + " | Value: " + data;
            }
        }

        private string KnownTagValue(string[] index, string[] display, string data)
        {
            try
            {
                for (int i = 0; i < index.Length; i++)
                {
                    if (data.Trim() == index[i])
                    {
                        return display[i] + " | Value: " + data;
                    }
                }

                return data;
            }
            catch
            {
                return "Unknown" + " | Value: " + data;
            }
        }

        private string[] GetDescription(int NumberOfTag, string DataString)
        {
            switch (NumberOfTag)
            {
                case(int)ExifTagNumber.ProcessingSoftware:
                    return new string[] { string_ProcessingSoftware };

                case (int)ExifTagNumber.ExifPixYDim:
                    return new string[] { string_ExifImageHeight};

                case (int)ExifTagNumber.Artist:
                    return new string[] { string_Artist };

                case (int)ExifTagNumber.BitsPerSample:
                    return new string[] { string_BitsPerSample };

                case (int)ExifTagNumber.CellLength:
                    return new string[] { string_CellLength };

                case (int)ExifTagNumber.CellWidth:
                    return new string[] { string_CellWidth };

                case (int)ExifTagNumber.ColorMap:
                    return new string[] { string_ColorMap };

                case (int)ExifTagNumber.Compression:
                    return new string[] { string_Compression, KnownTagValue(compresssionIndex, compressionDisplay, DataString) };

                case (int)ExifTagNumber.Copyright:
                    return new string[] { string_Copyright };

                case (int)ExifTagNumber.DateTime:
                    return new string[] { string_DateTime };

                case (int)ExifTagNumber.DocumentName:
                    return new string[] { string_DocumentName };

                case (int)ExifTagNumber.DotRange:
                    return new string[] { string_DotRange };

                case (int)ExifTagNumber.Aperture:
                    return new string[] { string_Aperture };

                case (int)ExifTagNumber.Brightness:
                case(int)ExifTagNumber.Brightness2:
                    return new string[] { string_Brightness };

                case (int)ExifTagNumber.CfaPattern:
                    return new string[] { string_CfaPattern };

                case (int)ExifTagNumber.ColorSpace:
                    return new string[] { string_ColorSpace, KnownTagValue(colorSpaceIndex, colorSpaceDisplay, DataString) };

                case (int)ExifTagNumber.CompBPP:
                    return new string[] { string_CompBPP };

                case (int)ExifTagNumber.CompConfig:
                    return new string[] { string_CompConfig, KnownTagValueUndefined(componentsConfigurationIndex, componentsConfigurationDisplay, DataString)};

                case (int)ExifTagNumber.DTDigitized:
                    return new string[] { string_DateTimeDigitized };

                case (int)ExifTagNumber.DTDigSS:
                    return new string[] { string_DTDigSS };

                case (int)ExifTagNumber.DTOrig:
                    return new string[] { string_DTOrig };

                case (int)ExifTagNumber.DTOrigSS:
                    return new string[] { string_DTOrigSS };

                case (int)ExifTagNumber.DTSubsec:
                    return new string[] { string_DTSubsec };

                case (int)ExifTagNumber.ExposureIndex:
                case(int)ExifTagNumber.ExposureIndex2:
                    return new string[] { string_ExposureIndex };

                case (int)ExifTagNumber.ExposureProg:
                    return new string[] { string_ExposureProgram, KnownTagValue(exposureProgramIndex, exposureProgramDisplay, DataString) };

                case (int)ExifTagNumber.ExposureTime:
                    return new string[] { string_ExposureTime };

                case (int)ExifTagNumber.FileSource:
                    return new string[] { string_FileSource, KnownTagValue(fileSourceIndex, fileSourceDisplay, DataString) };

                case (int)ExifTagNumber.Flash:
                    return new string[] { string_Flash, KnownTagValue(flashIndex, flashDisplay, DataString) };

                case (int)ExifTagNumber.FlashEnergy:
                case(int)ExifTagNumber.FlashEnergy2:
                    return new string[] { string_FlashEnergy };

                case (int)ExifTagNumber.FNumber:
                    return new string[] { string_FNumber };

                case (int)ExifTagNumber.FocalLength:
                    return new string[] { string_FocalLength };

                case (int)ExifTagNumber.FocalResUnit:
                    return new string[] { string_FocalResUnit };

                case (int)ExifTagNumber.FocalXRes:
                    return new string[] { string_FocalXRes };

                case (int)ExifTagNumber.FocalYRes:
                    return new string[] { string_FocalYRes };

                case (int)ExifTagNumber.FPXVer:
                    return new string[] { string_FPXVer };

                case (int)ExifTagNumber.ImageHistory:
                case(int)ExifTagNumber.ImageHistory2:
                    return new string[] { string_ImageHistory };

                case (int)ExifTagNumber.ImageNumber:
                case(int)ExifTagNumber.ImageNumber2:
                    return new string[] { string_ImageNumber };

                case (int)ExifTagNumber.Interlace:
                    return new string[] { string_Interlace };

                case (int)ExifTagNumber.Interop:
                    return new string[] { string_Interop };

                case (int)ExifTagNumber.ISOSpeed:
                    return new string[] { string_ISOSpeed };

                case (int)ExifTagNumber.LightSource:
                    return new string[] { string_LightSource, KnownTagValue(lightSourceIndex, lightSourceDisplay, DataString) };

                case (int)ExifTagNumber.MakerNote:
                    return new string[] { string_MakerNote };

                case (int)ExifTagNumber.MaxAperture:
                    return new string[] { string_MaxAperture };

                case (int)ExifTagNumber.MeteringMode:
                    return new string[] { string_MeteringMode, KnownTagValue(meteringModeIndex, meterindgModeDisplay, DataString) };

                case (int)ExifTagNumber.Noise:
                case(int)ExifTagNumber.Noise2:
                    return new string[] { string_Noise };

                case (int)ExifTagNumber.OECF:
                    return new string[] { string_Opto_ElectricConvFactor };

                case (int)ExifTagNumber.ExifPixXDim:
                    return new string[] { string_ExifImageWidth };

                case (int)ExifTagNumber.RelatedWav:
                    return new string[] { string_RelatedWav };

                case (int)ExifTagNumber.SceneType:
                    return new string[] { string_SceneType, KnownTagValue(sceneTypeIndex, sceneTypeDisplay, DataString) };

                case (int)ExifTagNumber.SecurityClassification:
                case(int)ExifTagNumber.SecurityClassification2:
                    return new string[] { string_SecurityClassification, KnownTagValue(securityClassificationIndex, securityClassificationDisplay, DataString) };

                case (int)ExifTagNumber.SelfTimerMode:
                    return new string[] { string_SelfTimerMode };

                case (int)ExifTagNumber.SensingMethod:
                case(int)ExifTagNumber.SensingMethod2:
                    return new string[] { string_SensingMethod , KnownTagValue(sensingMethodIndex, sensingMethodDisplay, DataString)};

                case (int)ExifTagNumber.ShutterSpeed:
                    return new string[] { string_ShutterSpeed };

                case (int)ExifTagNumber.SpatialFR:
                    return new string[] { string_SpatialFR };

                case (int)ExifTagNumber.SpectralSense:
                    return new string[] { string_SpectralSensitivity };

                case (int)ExifTagNumber.SubjectDist:
                    return new string[] { string_SubjectDist };

                case (int)ExifTagNumber.SubjectLocation:
                case(int)ExifTagNumber.SubjectLocation2:
                    return new string[] { string_SubjectLoc };

                case (int)ExifTagNumber.Tiff_EPStandardID:
                case(int)ExifTagNumber.Tiff_EPStandardID2:
                    return new string[] { string_Tiff_EPStandardID };

                case (int)ExifTagNumber.TimeZoneOffset:
                    return new string[] { string_TimeZoneOffset };

                case (int)ExifTagNumber.UserComment:
                    return new string[] { string_UserComment };

                case (int)ExifTagNumber.ExifVer:
                    return new string[] { string_ExifVer };

                case (int)ExifTagNumber.ExtraSamples:
                    return new string[] { string_ExtraSamples, KnownTagValue(extraSamplesIndex, extraSamplesDisplay, DataString) };

                case (int)ExifTagNumber.FillOrder:
                    return new string[] { string_FillOrder, KnownTagValue(fillOrderIndex, fillOrderDisplay, DataString) };

                case (int)ExifTagNumber.FreeByteCounts:
                    return new string[] { string_FreeByteCounts };

                case (int)ExifTagNumber.FreeOffsets:
                    return new string[] { string_FreeOffsets };

                case (int)ExifTagNumber.GrayResponseCurve:
                    return new string[] { string_GrayResponseCurve};

                case (int)ExifTagNumber.GrayResponseUnit:
                    return new string[] { string_GrayResponseUnit, KnownTagValue(grayResponseUnitIndex, grayResponseUnitDisplay, DataString) };

                case (int)ExifTagNumber.HalftoneHints:
                    return new string[] { string_HalftoneHints };

                case (int)ExifTagNumber.HostComputer:
                    return new string[] { string_HostComputer };

                case (int)ExifTagNumber.ImageDescription:
                    return new string[] { string_ImageDescription };

                case (int)ExifTagNumber.ImageLength:
                    return new string[] { string_ImageLength };

                case (int)ExifTagNumber.ImageWidth:
                case(int)ExifTagNumber.ImageWidth2:
                    return new string[] { string_ImageWidth };

                case (int)ExifTagNumber.InkNames:
                    return new string[] { string_InkNames };

                case (int)ExifTagNumber.InkSet:
                    return new string[] { string_InkSet, KnownTagValue(inkSetIndex, inkSetDisplay, DataString) };

                case (int)ExifTagNumber.JpegACTables:
                    return new string[] { string_jpegACTables };

                case (int)ExifTagNumber.JpegDCTables:
                    return new string[] { string_jpegDCTTables };

                case (int)ExifTagNumber.JpegInterchangeFormat:
                    return new string[] { string_jpegInterchangeFormat };

                case (int)ExifTagNumber.JpegInterchangeFormatLength:
                    return new string[] { string_jpegInterchangeFormatLength };

                case (int)ExifTagNumber.JpegLosslessPredictors:
                    return new string[] { string_jpegLosslessPredictors };

                case (int)ExifTagNumber.JpegPointTransforms:
                    return new string[] { string_jpegPointTransforms };

                case (int)ExifTagNumber.JpegProc:
                    return new string[] { string_jpegProc, KnownTagValue(jpegProcIndex, jpegProcDisplay, DataString) };

                case (int)ExifTagNumber.JpegQTables:
                    return new string[] { string_jpegQTables };

                case (int)ExifTagNumber.JpegRestartInterval:
                    return new string[] { string_jpegRestartInterval };

                case (int)ExifTagNumber.Make:
                    return new string[] { string_Make };

                case (int)ExifTagNumber.MaxSampleValue:
                    return new string[] { string_MaxSampleValue };

                case (int)ExifTagNumber.MinSampleValue:
                    return new string[] { string_MinSampleValue };

                case (int)ExifTagNumber.Model:
                case(int)ExifTagNumber.Model2:
                    return new string[] { string_Model };

                case (int)ExifTagNumber.NewSubFileType:
                    return new string[] { string_NewSubFileType, KnownTagValue(newSubFileTypeIndex, newSubFileTypeDisplay, DataString) };

                case (int)ExifTagNumber.NumberOfInks:
                    return new string[] { string_NumberOfInks };

                case (int)ExifTagNumber.Orientation:
                    return new string[] { string_Orientation, KnownTagValue(orientationIndex, orientationDisplay, DataString) };

                case (int)ExifTagNumber.PageName:
                    return new string[] { string_PageName };

                case (int)ExifTagNumber.PageNumber:
                    if (tagDescription == "")
                    {
                        return new string[] { string_PageNumber, "" };
                    }
                    else
                    {

                        int firstSpace = DataString.IndexOf(" ", 0);
                        int secondSpace = DataString.IndexOf(" ", firstSpace + 1);

                        return new string[] { string_PageNumber, pageNumberDisplay[0] 
                                      + DataString.Substring(0, firstSpace) + pageNumberDisplay[1] 
                                      + DataString.Substring(firstSpace + 1, DataString.Length - secondSpace) };
                    }

                case (int)ExifTagNumber.PhotometricInterpretation:
                    return new string[] { string_PhotometricInterpretation, KnownTagValue(photometericInterpretationIndex, photometericInterpretationDisplay, DataString) };

                case (int)ExifTagNumber.PlanarConfiguration:
                    return new string[] { string_PlanarConfiguration, KnownTagValue(planarConfigurationIndex, planarConfigurationDisplay, DataString) };

                case (int)ExifTagNumber.Predictor:
                    return new string[] { string_Predictor, KnownTagValue(predictorIndex, predictorDisplay, DataString) };

                case (int)ExifTagNumber.PrimaryChromaticities:
                    return new string[] { string_PrimaryChromaticities };

                case (int)ExifTagNumber.ReferenceBlackWhite:
                    return new string[] { string_ReferenceBlackWhite };

                case (int)ExifTagNumber.ResolutionUnit:
                    return new string[] { string_ResolutionUnit, KnownTagValue(resolutionUnitIndex, resolutionUnitDisplay, DataString) };

                case (int)ExifTagNumber.RowsPerStrip:
                    return new string[] { string_RowsPerStrip };

                case (int)ExifTagNumber.SampleFormat:
                    return new string[] { string_SampleFormat, KnownTagValueUndefined(sampleFormatIndex, sampleFormatDisplay, DataString)};

                case (int)ExifTagNumber.SamplesPerPixel:
                    return new string[] { string_SamplesPerPixel };

                case (int)ExifTagNumber.SMaxSampleValue:
                    return new string[] { string_SMaxSampleValue };

                case (int)ExifTagNumber.SMinSampleValue:
                    return new string[] { string_SMinSampleValue };

                case (int)ExifTagNumber.Software:
                    return new string[] { string_Software };

                case (int)ExifTagNumber.StripByteCounts:
                    return new string[] { string_StripByteCounts };

                case (int)ExifTagNumber.StripOffsets:
                    return new string[] { string_StripOffsets };

                case (int)ExifTagNumber.SubFileType:
                    return new string[] { string_SubFileType,KnownTagValue(subFileTypeIndex, subFileTypeDisplay, DataString) };

                case (int)ExifTagNumber.T4Options:
                    return new string[] { string4Options, KnownTagValue(t4OptionsIndex, t4OptionsDisplay, DataString) };

                case (int)ExifTagNumber.T6Options:
                    return new string[] { string6Options, KnownTagValue(t6OptionsIndex, t6OptionsDisplay, DataString)};

                case (int)ExifTagNumber.TargetPrinter:
                    return new string[] { stringTargetPrinter };

                case (int)ExifTagNumber.Thresholding:
                    return new string[] { stringThresholding, KnownTagValue(thresholdingIndex, thresholdingDisplay, DataString) };

                case (int)ExifTagNumber.TileByteCounts:
                    return new string[] { stringFileByteCounts };

                case (int)ExifTagNumber.TileLength:
                    return new string[] { stringFileLength };

                case (int)ExifTagNumber.TileOffsets:
                    return new string[] { stringFileOffsets };

                case (int)ExifTagNumber.TileWidth:
                    return new string[] { stringFileWidth };

                case (int)ExifTagNumber.TransferFunction:
                    return new string[] { stringTransferFunction };

                case (int)ExifTagNumber.TransferRange:
                    return new string[] { stringTransferRange };

                case (int)ExifTagNumber.WhitePoint:
                    return new string[] { string_WhitePoint };

                case (int)ExifTagNumber.XPosition:
                    return new string[] { string_XPosition };

                case (int)ExifTagNumber.XResolution:
                    return new string[] { string_XResolution };

                case (int)ExifTagNumber.YCbCrCoefficients:
                    return new string[] { string_YCbCrCoefficients };

                case (int)ExifTagNumber.YCbCrPositioning:
                    return new string[] { string_YCbCrPositioning, KnownTagValue(yCbCrPositioningIndex, yCbCrPositioningDisplay, DataString) };

                case (int)ExifTagNumber.YCbCrSubSampling:
                    return new string[] { string_YCbCrSubSampling, KnownTagValue(yCbCrSubsamplingIndex, yCbCrSubsamplingDisplay, DataString) };

                case (int)ExifTagNumber.YPosition:
                    return new string[] { string_YPosition };

                case (int)ExifTagNumber.YResolution:
                    return new string[] { string_YResolution };
                
                case (int)ExifTagNumber.WangAnnotation:
                    return new string[] { string_WangAnnotations };

                case (int)ExifTagNumber.JPEGTables:
                    return new string[] { string_JPEGTables };

                case (int)ExifTagNumber.SceneCaptureType:
                    return new string[] { string_SceneCaptureType, KnownTagValue(sceneCaptureTypeIndex, sceneCaptureTypeDisplay, DataString) };

                case (int)ExifTagNumber.CustomRendered:
                    return new string[] { string_CustomRendered, KnownTagValue(customRenderedIndex, customRenderedDisplay, DataString) };
                
                case (int)ExifTagNumber.SubIFDs:
                    return new string[] {string_SubIFDs};

                case (int)ExifTagNumber.WhiteBalance:
                case(int)ExifTagNumber.WhiteBalance2:
                    return new string[] { string_WhiteBalance , KnownTagValue(whiteBalanceIndex, whiteBalanceDisplay, DataString)};

                case (int)ExifTagNumber.ExposureMode:
                    return new string[] { string_ExposureMode,KnownTagValue(exposureModeIndex, exposureModeDisplay, DataString) };
                case(int)ExifTagNumber.PhotoShop:
                    return new string[] {string_PhotoshopSettings};
                
                case (int)ExifTagNumber.XMP:
                    return new string[] {string_XMP};

                case (int)ExifTagNumber.BadFaxLines:
                    return new string[] { string_BadFaxLines };

                case (int)ExifTagNumber.CleanFaxData:
                    return new string[] { string_CleanFaxData, KnownTagValue(cleanFaxDataIndex, cleanFaxDataDisplay, DataString) };

                case (int)ExifTagNumber.ConsecutiveBadFaxLines:
                    return new string[] { string_ConsecutiveBadFaxLines };
                
                case (int)ExifTagNumber.ColorResponseUnit:
                    return new string[] { string_ColorResponseUnit };

                case (int)ExifTagNumber.ClipPath :
                    return new string[] { string_ClipPath };

                case (int)ExifTagNumber.XClipPathUnits:
                    return new string[] { string_XClipPathUnits };

                case (int)ExifTagNumber.YClipPathUnits:
                    return new string[] { string_YClipPathUnits };

                case (int)ExifTagNumber.Indexed:
                    return new string[] { string_Indexed, KnownTagValue(indexedIndex, indexedDisplay, DataString) };

                case (int)ExifTagNumber.OPIProxy:
                    return new string[] { string_OPIProxy };

                case (int)ExifTagNumber.GlobalParametersIFD:
                    return new string[] { string_GlobalParametersIFD };

                case (int)ExifTagNumber.ProfileType:
                    return new string[] { string_ProfileType, KnownTagValue(profileTypeIndex, profileTypeDisplay, DataString) };

                case (int)ExifTagNumber.FaxProfile:
                    return new string[] { string_FaxProfile, KnownTagValue(faxProfileIndex, faxProfileDisplay, DataString) };

                case (int)ExifTagNumber.CodingMethods:
                    return new string[] { string_CodingMethods, KnownTagValue(codingMethodsIndex, codingMethodsDisplay, DataString) };

                case (int)ExifTagNumber.VersionYear:
                    return new string[] { string_VersionYear};

                case (int)ExifTagNumber.ModeNumber:
                    return new string[] { string_ModeNumber };

                case (int)ExifTagNumber.Decode:
                    return new string[] { string_Decode };

                case (int)ExifTagNumber.DefaultImageColor:
                    return new string[] { string_DefaultImageColor };

                case (int)ExifTagNumber.StripRowCounts:
                    return new string[] { string_StripRowCounts };

                case (int)ExifTagNumber.RelatedImageFormat:
                    return new string[] { string_RelatedImageFileFormat };
                
                case (int)ExifTagNumber.RelatedImageWidth:
                    return new string[] { string_RelatedImageWidth };

                case (int)ExifTagNumber.RelatedImageHeight:
                    return new string[] { string_RelatedImageHeight };

                case (int)ExifTagNumber.Rating:
                    return new string[] { string_Rating };

                case (int)ExifTagNumber.RatingPercent:
                    return new string[] { string_RatingPercent };

                case (int)ExifTagNumber.ImageID:
                    return new string[] { string_ImageID };

                case (int)ExifTagNumber.Matteing:
                    return new string[] { string_Matteing };

                case (int)ExifTagNumber.DataType:
                    return new string[] { string_DataType };

                case (int)ExifTagNumber.ImageDepth:
                    return new string[] { string_ImageDepth };

                case (int)ExifTagNumber.TileDepth:
                    return new string[] { string_TileDepth };

                case (int)ExifTagNumber.CFARepeatPatterDim:
                    return new string[] { string_CFARepeatPatternDim };
                
                case (int)ExifTagNumber.CFAPattern2:
                    return new string[] { string_CFAPattern2 };

                case (int)ExifTagNumber.BatteryLevel:
                    return new string[] { string_BatteryLevel };

                case (int)ExifTagNumber.MDScalePixel:
                    return new string[] { string_MDScalePixel };

                case (int)ExifTagNumber.KodakIFD:
                    return new string[] { string_KodakIFD };

                case(int)ExifTagNumber.MDFileTag:
                    return new string[] { string_MDFileTag };

                case (int)ExifTagNumber.MDColorTable :
                    return new string[] { string_MDColorTable };

                case (int)ExifTagNumber.MDLabName:
                    return new string[] { string_MDLabName };

                case(int)ExifTagNumber.MDSampleInfo:
                    return new string[] { string_MDSampleInfo };

                case(int)ExifTagNumber.MDPrepDate:
                    return new string[] { string_MDPrepDate };

                case(int)ExifTagNumber.MDPrepTime:
                    return new string[] { string_MDPrepTime };

                case(int)ExifTagNumber.MDFileUnits:
                    return new string[] { string_MDFileUnits };

                case(int)ExifTagNumber.PixelScale:
                    return new string[] { string_PixelScale };

                case(int)ExifTagNumber.IPTC_NAA:
                    return new string[] { string_IPTC_NAA };

                case (int)ExifTagNumber.IntergraphPacketData:
                    return new string[] { string_IntergraphPacketData };

                case(int)ExifTagNumber.IntergraphFlagRegisters:
                    return new string[] { string_IntergraphFlagRegisters };

                case(int)ExifTagNumber.IntegraphMatrix:
                    return new string[] { string_IntergraphMatrix };

                case(int)ExifTagNumber.ModelTiePoint:
                    return new string[] { string_ModelTiePoint };

                case(int)ExifTagNumber.Site:
                    return new string[] { string_Site };

                case(int)ExifTagNumber.ColorSequence:
                    return new string[] { string_ColorSequence };

                case(int)ExifTagNumber.IT8Header:
                    return new string[] { string_IT8Header };

                case(int)ExifTagNumber.RasterPadding:
                    return new string[] { string_RasterPadding };

                case(int)ExifTagNumber.BitsPerRunLength:
                    return new string[] { string_BitsPerRunLength };

                case(int)ExifTagNumber.BitsPerExtendedRunLength:
                    return new string[] { string_BitsPerExtendedRunLength };

                case(int)ExifTagNumber.ColorTable:
                    return new string[] { string_ColorTable };

                case(int)ExifTagNumber.ImageColorIndicator:
                    return new string[] { string_ImageColorIndicator };

                case(int)ExifTagNumber.BackgroundColorIndicator:
                    return new string[] { string_BackgroundColorIndicator };

                case(int)ExifTagNumber.ImageColorValue:
                    return new string[] { string_ImageColorValue };

                case (int)ExifTagNumber.BackgroundColorvalue:
                    return new string[] { string_BackgroundColorValue };

                case(int)ExifTagNumber.PixelIntensityRange:
                    return new string[] { string_PixelIntensityRange };

                case(int)ExifTagNumber.TransparencyIndicator:
                    return new string[] { string_TransparencyIndicator };

                case(int)ExifTagNumber.ColorCharacterization:
                    return new string[] { string_ColorCharacterization };

                case(int)ExifTagNumber.HCUsage:
                    return new string[] { string_HCUsage };

                case(int)ExifTagNumber.SEMInfo:
                    return new string[] { string_SEMInfo };

                case(int)ExifTagNumber.AFCP_IPTC:
                    return new string[] { string_AFCP_IPTC };

                case(int)ExifTagNumber.ModelTransform:
                    return new string[] { string_ModelTransform };

                case(int)ExifTagNumber.WB_GRGBLevels:
                    return new string[] { string_WB_GRGBLevels };

                case(int)ExifTagNumber.LeafData:
                    return new string[] { string_LeafData };

                case(int)ExifTagNumber.ExifIFD:
                    return new string[] { string_ExifIFD };
                    
                case(int)ExifTagNumber.ICCProfile:
                    return new string[] { string_ICC_Profile };

                case(int)ExifTagNumber.ImageLayer:
                    return new string[] { string_ImageLayer };

                case(int)ExifTagNumber.GeoTiffDirectory:
                    return new string[] { string_GeoTiffDirectory };

                case(int)ExifTagNumber.GeoTiffDoubleParams:
                    return new string[] { string_GeoTiffDoubleParams };

                case(int)ExifTagNumber.GeoTiffAsciiParams:
                    return new string[] { string_GeoTiffAsciiParams };

                case(int)ExifTagNumber.GPSInfo:
                    return new string[] { string_GPSInfo };

                case(int)ExifTagNumber.FaxRecvParams:
                    return new string[] { string_FaxRecvParams };

                case(int)ExifTagNumber.FaxSubAddress:
                    return new string[] { string_FaxSubAddress };

                case(int)ExifTagNumber.FaxRecvTime:
                    return new string[] { string_FaxRecvTime };

                case(int)ExifTagNumber.LeafSubIFD:
                    return new string[] { string_LeafSubIFD };

                case(int)ExifTagNumber.ExposureBias:
                    return new string[] { string_ExposureCompensation };

                case(int)ExifTagNumber.StoNits:
                    return new string[] { string_StoNits };

                case(int)ExifTagNumber.ImageSourceData:
                    return new string[] { string_ImageSourceData };

                case(int)ExifTagNumber.XPTitle:
                    return new string[] { string_XPTitle };

                case(int)ExifTagNumber.XPComment:
                    return new string[] { string_XPComment };

                case(int)ExifTagNumber.XPAuthor:
                    return new string[] { string_XPAuthor };

                case(int)ExifTagNumber.XPKeywords:
                    return new string[] { string_XPKeywords };

                case(int)ExifTagNumber.XPSubject:
                    return new string[] { string_XPSubject };

                case(int)ExifTagNumber.SpatialFrequencyResponse:
                    return new string[] { string_SpatialFR };

                case(int)ExifTagNumber.FocalPlaneXResolution:
                    return new string[] { string_FocalXResolution };

                case(int)ExifTagNumber.FocalPlaneYResolution:
                    return new string[] { string_FocalYResolution };

                case(int)ExifTagNumber.FocalPlaneResolutionUnits:
                    return new string[] { string_FocalResolutionUnit, KnownTagValue(focalPlaneResolutionUnitIndex, focalPlaneResolutionUnitDisplay, DataString) };

                case(int)ExifTagNumber.DigitalZoomRatio:
                    return new string[] { string_DigitalZoomRatio };

                case(int)ExifTagNumber.FocalLengthIn35MMFormat:
                    return new string[] { string_FocalLengthIn35MMFormat, DataString + " mm" };

                case(int)ExifTagNumber.GainControl:
                    return new string[] { string_GainControl, KnownTagValue(gainControlIndex, gainControlDisplay, DataString) };

                case(int)ExifTagNumber.Contrast:
                case(int)ExifTagNumber.Contrast2:
                    return new string[] { string_Contrast, KnownTagValue(contrastIndex, contrastDisplay, DataString) };

                case(int)ExifTagNumber.Saturation:
                case(int)ExifTagNumber.Saturation2:
                    return new string[] { string_Saturation, KnownTagValue(saturationIndex, saturationDisplay, DataString) };

                case(int)ExifTagNumber.Sharpness:
                case(int)ExifTagNumber.Sharpness2:
                    return new string[] { string_Sharpness, KnownTagValue(sharpnessIndex, sharpnessDisplay, DataString) };

                case (int)ExifTagNumber.DeviceSettingDescription:
                    return new string[] { string_DeviceSettingDescription };

                case(int)ExifTagNumber.SubjectDistanceRange:
                    return new string[] { string_SubjectDistanceRange, KnownTagValue(subjectDistanceRangeIndex, subjectDistanceRangeDisplay, DataString) };

                case(int)ExifTagNumber.ImageUniqueID:
                    return new string[] { string_ImageUniqueID };

                case(int)ExifTagNumber.GDALMetaData:
                    return new string[] { string_GDALMetaData };

                case(int)ExifTagNumber.GDALNoData:
                    return new string[] { string_GDALNoData };

                case(int)ExifTagNumber.Gamma:
                    return new string[] { string_Gamma };

                case(int)ExifTagNumber.PixelFormat:
                    return new string[] { string_PixelFormat,KnownTagValue(pixelFormatIndex, pixelFormatDisplay, DataString) };

                case(int)ExifTagNumber.Transformation:
                    return new string[] { string_Transformation, KnownTagValue(transformationIndex, transformationDisplay, DataString) };

                case(int)ExifTagNumber.Uncompressed:
                    return new string[] { string_Uncompressed, KnownTagValue(uncompressedIndex, uncompressedDisplay, DataString) };

                case(int)ExifTagNumber.ImageType:
                    return new string[] { string_ImageType, KnownTagValue(makerNoteSafetyIndex, makerNoteSafetyDisplay, DataString) };

                case(int)ExifTagNumber.ImageHeight:
                    return new string[] { string_ImageHeight };

                case(int)ExifTagNumber.WidthResolution:
                    return new string[] { string_WidthResolution };

                case(int)ExifTagNumber.HeightResolution:
                    return new string[] { string_HeightResolution };

                case(int)ExifTagNumber.ImageOffset:
                    return new string[] { string_ImageOffset };

                case(int)ExifTagNumber.ImageByteCount:
                    return new string[] { string_ImageByteCount };

                case(int)ExifTagNumber.AlphaOffset:
                    return new string[] { string_AlphaOffset };

                case(int)ExifTagNumber.AlphaByteCount:
                    return new string[] { string_AlphaByteCount };

                case(int)ExifTagNumber.ImageDataDiscard:
                    return new string[] { string_ImageDataDiscard, KnownTagValue(imageDataDiscardIndex, imageDataDiscardDisplay, DataString) };

                case(int)ExifTagNumber.AlphaDataDiscard:
                    return new string[] { string_AlphaDataDiscard, KnownTagValue(imageDataDiscardIndex, imageDataDiscardDisplay, DataString) };

                case(int)ExifTagNumber.OceScanJobDesc:
                    return new string[] { string_OceScanJobDesc };

                case(int)ExifTagNumber.OceIDNumber:
                    return new string[] { string_OceIDNumber };

                case(int)ExifTagNumber.OceApplicationSelector:
                    return new string[] { string_OceApplicationSelector };

                case(int)ExifTagNumber.OceImageLogic:
                    return new string[] { string_OceImageLogic };

                case(int)ExifTagNumber.Annotations:
                    return new string[] { string_Annotations };

                case(int)ExifTagNumber.PrintIM:
                    return new string[] { string_PrintIM };

                case(int)ExifTagNumber.DNGVersion:
                    return new string[] { string_DNGVersion };
                    
                case(int)ExifTagNumber.DNGBackwardVersion:
                    return new string[] { string_DNGBackwardVersion };

                case(int)ExifTagNumber.UniqueCameraModel:
                    return new string[] { string_UniqueCameraModel };

                case(int)ExifTagNumber.LocalizedCameraModel:
                    return new string[] { string_LocalizedCameraModel };

                case(int)ExifTagNumber.CFAPlaneColor:
                    return new string[] { string_CFAPlaneColor };

                case(int)ExifTagNumber.CFALayout:
                    return new string[] { string_CFALayout, KnownTagValue(cFALayoutIndex, cFALayoutDisplay, DataString) };

                case(int)ExifTagNumber.Linearizationtable:
                    return new string[] { string_Linearizationtable };

                case(int)ExifTagNumber.BlackLevelRepatDim:
                    return new string[] { string_BlackLevelRepatDim };
                    
                case(int)ExifTagNumber.BlackLevel:
                    return new string[] { string_BlackLevel };

                case(int)ExifTagNumber.BlackLevelDeltaH:
                    return new string[] { string_BlackLevelDeltaH };
                
                case (int)ExifTagNumber.BlackLevelDeltaV:
                    return new string[] { string_BlackLevelDeltaV };

                case(int)ExifTagNumber.WhiteLevel:
                    return new string[] { string_WhiteLevel };
                
                case (int)ExifTagNumber.DefaultScale:
                    return new string[] { string_DefaultScale };

                case(int)ExifTagNumber.DefaulCropOrigin:
                    return new string[] { string_DefaulCropOrigin };

                case(int)ExifTagNumber.DefaultCropSize:
                    return new string[] { string_DefaultCropSize };

                case(int)ExifTagNumber.ColorMatrix1:
                    return new string[] { string_ColorMatrix1 };

                case (int)ExifTagNumber.ColorMatrix2:
                    return new string[] { string_ColorMatrix2 };

                case(int)ExifTagNumber.CameraCalibration1:
                    return new string[] { string_CameraCalibration1, KnownTagValue(lightSourceIndex, lightSourceDisplay, DataString)};

                case (int)ExifTagNumber.CameraCalibration2:
                    return new string[] { string_CameraCalibration2,KnownTagValue(lightSourceIndex, lightSourceDisplay, DataString) };

                case(int)ExifTagNumber.ReductionMatrix1:
                    return new string[] { string_ReductionMatrix1 };

                case (int)ExifTagNumber.ReductionMatrix2:
                    return new string[] { string_ReductionMatrix2 };

                case(int)ExifTagNumber.AnalogBlance:
                    return new string[] { string_AnalogBlance };

                case(int)ExifTagNumber.AsShotNeutral:
                    return new string[] { string_AsShotNeutral };

                case(int)ExifTagNumber.AsShotWhiteXY:
                    return new string[] { string_AsShotWhiteXY };

                case(int)ExifTagNumber.BaselineExposure:
                    return new string[]{string_BaselineExposure};

                case (int)ExifTagNumber.BaselineNoise:
                    return new string[] { string_BaselineNoise };

                case (int)ExifTagNumber.BaselineSharpness:
                    return new string[] { string_BaselineSharpness };

                case(int)ExifTagNumber.BayerGreenSplit:
                    return new string[] { string_BayerGreenSplit };

                case(int)ExifTagNumber.LinearResponseLimit:
                    return new string[] { string_LinearResponseLimit };

                case(int)ExifTagNumber.CameraSerialNumber:
                    return new string[] { string_CameraSerialNumber };

                case(int)ExifTagNumber.DNGLensInfo:
                    return new string[] { string_DNGLensInfo };

                case(int)ExifTagNumber.ChromaBlurRadius:
                    return new string[] { string_ChromaBlurRadius };

                case(int)ExifTagNumber.AntiAliasStrength:
                    return new string[] { string_AntiAliasStrength };

                case(int)ExifTagNumber.ShadowScale:
                    return new string[] { string_ShadowScale };

                case(int)ExifTagNumber.SR2Private:
                    return new string[] { string_SR2Private };

                case(int)ExifTagNumber.MakerNoteSafety:
                    return new string[] { string_MakerNoteSafety, KnownTagValue(makerNoteSafetyIndex, makerNoteSafetyDisplay, DataString) };

                case(int)ExifTagNumber.RawImageSegmentation:
                    return new string[] { string_RawImageSegmentation };

                case(int)ExifTagNumber.CalibrationIlluminant1:
                    return new string[] { string_CalibrationIlluminant1 };

                case (int)ExifTagNumber.CalibrationIlluminant2:
                    return new string[] { string_CalibrationIlluminant2 };

                case(int)ExifTagNumber.BestQualityScale:
                    return new string[] { string_BestQualityScale };

                case(int)ExifTagNumber.RawUniqueID:
                    return new string[] { string_RawUniqueID };

                case(int)ExifTagNumber.AliasLayerMetaData:
                    return new string[] { string_AliasLayerMetaData };

                case(int)ExifTagNumber.OriginalRawFileName:
                    return new string[] { string_OriginalRawFileName };
                
                case (int)ExifTagNumber.OriginalRawFileData:
                    return new string[] { string_OriginalRawFileData };

                case(int)ExifTagNumber.ActiveArea:
                    return new string[] { string_ActiveArea };

                case(int)ExifTagNumber.MaskedAreas:
                    return new string[] { string_MaskedAreas };

                case(int)ExifTagNumber.AsShotICCProfile:
                    return new string[] { string_AsShotICCProfile };

                case(int)ExifTagNumber.AsShotPreProfileMatrix:
                    return new string[] { string_AsShotPreProfileMatrix };

                case(int)ExifTagNumber.CurrentICCProfile:
                    return new string[] { string_CurrentICCProfile };

                case(int)ExifTagNumber.CurrentPreProfileMatrix:
                    return new string[] { string_CurrentPreProfileMatrix };

                case(int)ExifTagNumber.Padding:
                    return new string[] { string_Padding };

                case(int)ExifTagNumber.OffsetSchema:
                    return new string[] { string_OffsetSchema };

                case(int)ExifTagNumber.OwnerName:
                    return new string[] { string_OwnerName };

                case(int)ExifTagNumber.SerialNumber:
                    return new string[] { string_SerialNumber };

                case(int)ExifTagNumber.Lens:
                    return new string[] { string_Lens };

                case(int)ExifTagNumber.RawFile:
                    return new string[] { string_RawFile };

                case(int)ExifTagNumber.Converter:
                    return new string[] { string_Converter }; 

                case(int)ExifTagNumber.Exposure:
                    return new string[]{string_Exposure};

                case(int)ExifTagNumber.Shadows:
                    return new string[]{string_Shadows};

                case(int)ExifTagNumber.Smoothness:
                    return new string[] { string_Smoothness };

                case(int)ExifTagNumber.MoireFilter:
                    return new string[] { string_MoireFilter };

                default:
                    return new string[] { string_Default };

            }
        }

        public string IPTCHelper(long dataSetType, byte dataSet)
        {
            string tagDescription = "";

            switch (dataSetType)
            {
                case 1:
                    {
                        switch (dataSet)
                        {
                            case 0:
                                {
                                    tagDescription = "Model Version";
                                    break;
                                }
                            case 5:
                                {
                                    tagDescription = "Destination";
                                    break;
                                }
                            case 20:
                                {
                                    tagDescription = "File Format";
                                    break;
                                }
                            case 22:
                                {
                                    tagDescription = "File Format Version";
                                    break;
                                }
                            case 30:
                                {
                                    tagDescription = "Service Identifier";
                                    break;
                                }
                            case 40:
                                {
                                    tagDescription = "Envelope Number";
                                    break;
                                }
                            case 50:
                                {
                                    tagDescription = "Product I.D.";
                                    break;
                                }
                            case 60:
                                {
                                    tagDescription = "Envelope Priority";
                                    break;
                                }
                            case 70:
                                {
                                    tagDescription = "Date Sent";
                                    break;
                                }
                            case 80:
                                {
                                    tagDescription = "Time Sent";
                                    break;
                                }
                            case 90:
                                {
                                    tagDescription = "Coded Character Set";
                                    break;
                                }
                            case 100:
                                {
                                    tagDescription = "UNO";
                                    break;
                                }
                            case 120:
                                {
                                    tagDescription = "ARM Identifier";
                                    break;
                                }
                            case 122:
                                {
                                    tagDescription = "Arm Version";
                                    break;
                                }
                        }
                        break;
                    }
                case 2:
                    {
                        switch (dataSet)
                        {
                            case 0:
                                {
                                    tagDescription = "Record Version";
                                    break;
                                }
                            case 3:
                                {
                                    tagDescription = "Object Type Reference";
                                    break;
                                }
                            case 4:
                                {
                                    tagDescription = "Object Attribute Reference";
                                    break;
                                }
                            case 5:
                                {
                                    tagDescription = "Object Name";
                                    break;
                                }
                            case 7:
                                {
                                    tagDescription = "Edit Status";
                                    break;
                                }
                            case 8:
                                {
                                    tagDescription = "Editorial Update";
                                    break;
                                }
                            case 10:
                                {
                                    tagDescription = "Urgency";
                                    break;
                                }
                            case 12:
                                {
                                    tagDescription = "Subject Reference";
                                    break;
                                }
                            case 15:
                                {
                                    tagDescription = "Category";
                                    break;
                                }
                            case 20:
                                {
                                    tagDescription = "Supplemental Category";
                                    break;
                                }
                            case 22:
                                {
                                    tagDescription = "Fixture Identifier";
                                    break;
                                }
                            case 25:
                                {
                                    tagDescription = "Keywords";
                                    break;
                                }
                            case 26:
                                {
                                    tagDescription = "Content Location Code";
                                    break;
                                }
                            case 27:
                                {
                                    tagDescription = "Content Location Name";
                                    break;
                                }
                            case 30:
                                {
                                    tagDescription = "Release Date";
                                    break;
                                }
                            case 35:
                                {
                                    tagDescription = "Release Time";
                                    break;
                                }
                            case 37:
                                {
                                    tagDescription = "Expiration Date";
                                    break;
                                }
                            case 38:
                                {
                                    tagDescription = "Expiration Time";
                                    break;
                                }
                            case 40:
                                {
                                    tagDescription = "Special Instructions";
                                    break;
                                }
                            case 42:
                                {
                                    tagDescription = "Action Advised";
                                    break;
                                }
                            case 45:
                                {
                                    tagDescription = "Reference Serivce";
                                    break;
                                }
                            case 47:
                                {
                                    tagDescription = "Reference Date";
                                    break;
                                }
                            case 50:
                                {
                                    tagDescription = "Reference Number";
                                    break;
                                }
                            case 55:
                                {
                                    tagDescription = "Date Created";
                                    break;
                                }
                            case 60:
                                {
                                    tagDescription = "Time Created";
                                    break;
                                }
                            case 62:
                                {
                                    tagDescription = "Digital Creation Time";
                                    break;
                                }
                            case 63:
                                {
                                    tagDescription = "Digital Creation Time";
                                    break;
                                }
                            case 65:
                                {
                                    tagDescription = "Originating Program";
                                    break;
                                }
                            case 70:
                                {
                                    tagDescription = "Program Version";
                                    break;
                                }
                            case 75:
                                {
                                    tagDescription = "Object Cycle";
                                    break;
                                }
                            case 80:
                                {
                                    tagDescription = "By-line";
                                    break;
                                }
                            case 85:
                                {
                                    tagDescription = "By-line Title";
                                    break;
                                }
                            case 90:
                                {
                                    tagDescription = "City";
                                    break;
                                }
                            case 92:
                                {
                                    tagDescription = "Sub-location";
                                    break;
                                }
                            case 95:
                                {
                                    tagDescription = "Province/State";
                                    break;
                                }
                            case 100:
                                {
                                    tagDescription = "Country/Primary Location Code";
                                    break;
                                }
                            case 101:
                                {
                                    tagDescription = "Country/Primary Location Name";
                                    break;
                                }
                            case 103:
                                {
                                    tagDescription = "Original Transmission Reference";
                                    break;
                                }
                            case 105:
                                {
                                    tagDescription = "Headline";
                                    break;
                                }
                            case 110:
                                {
                                    tagDescription = "Credit";
                                    break;
                                }
                            case 115:
                                {
                                    tagDescription = "Source";
                                    break;
                                }
                            case 116:
                                {
                                    tagDescription = "Copyright Notice";
                                    break;
                                }
                            case 118:
                                {
                                    tagDescription = "Contact";
                                    break;
                                }
                            case 120:
                                {
                                    tagDescription = "Caption/Abstract";
                                    break;
                                }
                            case 122:
                                {
                                    tagDescription = "Writer/Editor";
                                    break;
                                }
                            case 125:
                                {
                                    tagDescription = "Rasterized Caption";
                                    break;
                                }
                            case 130:
                                {
                                    tagDescription = "Image Type";
                                    break;
                                }
                            case 131:
                                {
                                    tagDescription = "Image Orientation";
                                    break;
                                }
                            case 135:
                                {
                                    tagDescription = "Language Identifier";
                                    break;
                                }
                            case 150:
                                {
                                    tagDescription = "Audio Type";
                                    break;
                                }
                            case 151:
                                {
                                    tagDescription = "Audio Sampling Rate";
                                    break;
                                }
                            case 152:
                                {
                                    tagDescription = "Audio Sampling Resolution";
                                    break;
                                }
                            case 153:
                                {
                                    tagDescription = "Audio Duration";
                                    break;
                                }
                            case 154:
                                {
                                    tagDescription = "Audio Outcue";
                                    break;
                                }
                            case 200:
                                {
                                    tagDescription = "ObjectData Preview File Format";
                                    break;
                                }
                            case 201:
                                {
                                    tagDescription = "ObjectData Preview File Version";
                                    break;
                                }
                            case 202:
                                {
                                    tagDescription = "ObjectData Preview Data";
                                    break;
                                }
                        }
                        break;
                    }
                case 7:
                    {
                        switch (dataSet)
                        {
                            case 10:
                                {
                                    tagDescription = "Size Mode";
                                    break;
                                }
                            case 20:
                                {
                                    tagDescription = "Max Subfile Size";
                                    break;
                                }
                            case 90:
                                {
                                    tagDescription = "ObjectData Size Announced";
                                    break;
                                }
                            case 95:
                                {
                                    tagDescription = "Maximum ObjectData Size";
                                    break;
                                }
                        }
                        break;
                    }
                case 8:
                    {
                        switch (dataSet)
                        {
                            case 10:
                                {
                                    tagDescription = "Subfile";
                                    break;
                                }
                        }
                        break;
                    }
                case 9:
                    {
                        switch (dataSet)
                        {
                            case 10:
                                {
                                    tagDescription = "Confirmed ObjectData Size";
                                    break;
                                }
                        }
                        break;
                    }
            }

            if (tagDescription == "")
                tagDescription = "Unknown";

            return tagDescription;
        }
    }
}

/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class MergeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (sourceImage != null)
            {
                sourceImage.Dispose();
                sourceImage = null;
            }
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MergeForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.labelSuperimposeIntensityLow = new System.Windows.Forms.Label();
            this.labelSuperimposeIntensityHigh = new System.Windows.Forms.Label();
            this.labelTransparentColor = new System.Windows.Forms.Label();
            this.buttonTransparentColor = new System.Windows.Forms.Button();
            this.checkBoxTransparency = new System.Windows.Forms.CheckBox();
            this.comboBoxSizing = new System.Windows.Forms.ComboBox();
            this.labelSizing = new System.Windows.Forms.Label();
            this.comboBoxStyle = new System.Windows.Forms.ComboBox();
            this.labelStyle = new System.Windows.Forms.Label();
            this.comboBoxSourceImage = new System.Windows.Forms.ComboBox();
            this.labelSourceImage = new System.Windows.Forms.Label();
            this.LowBox = new System.Windows.Forms.NumericUpDown();
            this.HighBox = new System.Windows.Forms.NumericUpDown();
            this.customLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.LowBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBox)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(397, 570);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(631, 570);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(513, 570);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(10, 570);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 36;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            // 
            // labelSuperimposeIntensityLow
            // 
            this.labelSuperimposeIntensityLow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSuperimposeIntensityLow.AutoSize = true;
            this.labelSuperimposeIntensityLow.Location = new System.Drawing.Point(8, 512);
            this.labelSuperimposeIntensityLow.Name = "labelSuperimposeIntensityLow";
            this.labelSuperimposeIntensityLow.Size = new System.Drawing.Size(180, 13);
            this.labelSuperimposeIntensityLow.TabIndex = 48;
            this.labelSuperimposeIntensityLow.Text = "Superimpose Intensity Lower Range:";
            this.labelSuperimposeIntensityLow.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelSuperimposeIntensityHigh
            // 
            this.labelSuperimposeIntensityHigh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSuperimposeIntensityHigh.AutoSize = true;
            this.labelSuperimposeIntensityHigh.Location = new System.Drawing.Point(8, 486);
            this.labelSuperimposeIntensityHigh.Name = "labelSuperimposeIntensityHigh";
            this.labelSuperimposeIntensityHigh.Size = new System.Drawing.Size(180, 13);
            this.labelSuperimposeIntensityHigh.TabIndex = 46;
            this.labelSuperimposeIntensityHigh.Text = "Superimpose Intensity Upper Range:";
            this.labelSuperimposeIntensityHigh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // labelTransparentColor
            // 
            this.labelTransparentColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTransparentColor.AutoSize = true;
            this.labelTransparentColor.Location = new System.Drawing.Point(175, 540);
            this.labelTransparentColor.Name = "labelTransparentColor";
            this.labelTransparentColor.Size = new System.Drawing.Size(94, 13);
            this.labelTransparentColor.TabIndex = 45;
            this.labelTransparentColor.Text = "Transparent Color:";
            // 
            // buttonTransparentColor
            // 
            this.buttonTransparentColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTransparentColor.BackColor = System.Drawing.Color.Black;
            this.buttonTransparentColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTransparentColor.Location = new System.Drawing.Point(275, 533);
            this.buttonTransparentColor.Name = "buttonTransparentColor";
            this.buttonTransparentColor.Size = new System.Drawing.Size(24, 26);
            this.buttonTransparentColor.TabIndex = 44;
            this.buttonTransparentColor.UseVisualStyleBackColor = false;
            this.buttonTransparentColor.Click += new System.EventHandler(this.buttonTransparentColor_Click);
            // 
            // checkBoxTransparency
            // 
            this.checkBoxTransparency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxTransparency.AutoSize = true;
            this.checkBoxTransparency.Checked = true;
            this.checkBoxTransparency.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTransparency.Location = new System.Drawing.Point(61, 539);
            this.checkBoxTransparency.Name = "checkBoxTransparency";
            this.checkBoxTransparency.Size = new System.Drawing.Size(91, 17);
            this.checkBoxTransparency.TabIndex = 43;
            this.checkBoxTransparency.Text = "Transparency";
            this.checkBoxTransparency.UseVisualStyleBackColor = true;
            // 
            // comboBoxSizing
            // 
            this.comboBoxSizing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxSizing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSizing.FormattingEnabled = true;
            this.comboBoxSizing.Items.AddRange(new object[] {
            "Crop",
            "Resize Area to Image",
            "Resize Image to Area",
            "Tile Image to Area"});
            this.comboBoxSizing.Location = new System.Drawing.Point(375, 483);
            this.comboBoxSizing.Name = "comboBoxSizing";
            this.comboBoxSizing.Size = new System.Drawing.Size(159, 21);
            this.comboBoxSizing.TabIndex = 42;
            // 
            // labelSizing
            // 
            this.labelSizing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSizing.AutoSize = true;
            this.labelSizing.Location = new System.Drawing.Point(331, 486);
            this.labelSizing.Name = "labelSizing";
            this.labelSizing.Size = new System.Drawing.Size(38, 13);
            this.labelSizing.TabIndex = 41;
            this.labelSizing.Text = "Sizing:";
            // 
            // comboBoxStyle
            // 
            this.comboBoxStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStyle.DropDownWidth = 280;
            this.comboBoxStyle.FormattingEnabled = true;
            this.comboBoxStyle.Items.AddRange(new object[] {
            "Normal",
            "If Darker",
            "If Lighter",
            "Additive",
            "Subtractive",
            "Superimpose",
            "Superimpose Left To Right",
            "Superimpose Right To Left",
            "Superimpose Vertical To Center",
            "Superimpose Vertical From Center",
            "Superimpose Top To Bottom",
            "Superimpose Bottom To Top",
            "Superimpose Horizontal From Center",
            "Superimpose Horizontal To Center",
            "Alpha Foregound Over Background",
            "Alpha Background Over Foreground",
            "Alpha Most Opaque",
            "Alpha Least Opaque"});
            this.comboBoxStyle.Location = new System.Drawing.Point(73, 447);
            this.comboBoxStyle.Name = "comboBoxStyle";
            this.comboBoxStyle.Size = new System.Drawing.Size(202, 21);
            this.comboBoxStyle.TabIndex = 40;
            // 
            // labelStyle
            // 
            this.labelStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStyle.AutoSize = true;
            this.labelStyle.Location = new System.Drawing.Point(7, 450);
            this.labelStyle.Name = "labelStyle";
            this.labelStyle.Size = new System.Drawing.Size(66, 13);
            this.labelStyle.TabIndex = 39;
            this.labelStyle.Text = "Merge Style:";
            // 
            // comboBoxSourceImage
            // 
            this.comboBoxSourceImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.comboBoxSourceImage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSourceImage.FormattingEnabled = true;
            this.comboBoxSourceImage.Items.AddRange(new object[] {
            "Alpha1",
            "Alpha2",
            "Alpha3",
            "alphabush_small",
            "Ball1",
            "Browse..."});
            this.comboBoxSourceImage.Location = new System.Drawing.Point(375, 447);
            this.comboBoxSourceImage.Name = "comboBoxSourceImage";
            this.comboBoxSourceImage.Size = new System.Drawing.Size(159, 21);
            this.comboBoxSourceImage.TabIndex = 38;
            this.comboBoxSourceImage.SelectedIndexChanged += new System.EventHandler(this.comboBoxSourceImage_SelectedIndexChanged);
            // 
            // labelSourceImage
            // 
            this.labelSourceImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSourceImage.AutoSize = true;
            this.labelSourceImage.Location = new System.Drawing.Point(293, 450);
            this.labelSourceImage.Name = "labelSourceImage";
            this.labelSourceImage.Size = new System.Drawing.Size(76, 13);
            this.labelSourceImage.TabIndex = 37;
            this.labelSourceImage.Text = "Source Image:";
            // 
            // LowBox
            // 
            this.LowBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LowBox.Location = new System.Drawing.Point(194, 510);
            this.LowBox.Name = "LowBox";
            this.LowBox.Size = new System.Drawing.Size(60, 20);
            this.LowBox.TabIndex = 50;
            // 
            // HighBox
            // 
            this.HighBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HighBox.Location = new System.Drawing.Point(194, 486);
            this.HighBox.Name = "HighBox";
            this.HighBox.Size = new System.Drawing.Size(60, 20);
            this.HighBox.TabIndex = 51;
            // 
            // customLabel
            // 
            this.customLabel.AutoSize = true;
            this.customLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customLabel.Location = new System.Drawing.Point(550, 450);
            this.customLabel.Name = "customLabel";
            this.customLabel.Size = new System.Drawing.Size(0, 13);
            this.customLabel.TabIndex = 52;
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(11, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 54;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(11, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 386);
            this.tableLayoutPanel.TabIndex = 53;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 380);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 380);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // MergeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 604);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.customLabel);
            this.Controls.Add(this.HighBox);
            this.Controls.Add(this.LowBox);
            this.Controls.Add(this.labelSuperimposeIntensityLow);
            this.Controls.Add(this.labelSuperimposeIntensityHigh);
            this.Controls.Add(this.labelTransparentColor);
            this.Controls.Add(this.buttonTransparentColor);
            this.Controls.Add(this.checkBoxTransparency);
            this.Controls.Add(this.comboBoxSizing);
            this.Controls.Add(this.labelSizing);
            this.Controls.Add(this.comboBoxStyle);
            this.Controls.Add(this.labelStyle);
            this.Controls.Add(this.comboBoxSourceImage);
            this.Controls.Add(this.labelSourceImage);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MergeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Merge";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LowBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBox)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.Label labelSuperimposeIntensityLow;
        private System.Windows.Forms.Label labelSuperimposeIntensityHigh;
        private System.Windows.Forms.Label labelTransparentColor;
        private System.Windows.Forms.Button buttonTransparentColor;
        private System.Windows.Forms.CheckBox checkBoxTransparency;
        private System.Windows.Forms.ComboBox comboBoxSizing;
        private System.Windows.Forms.Label labelSizing;
        private System.Windows.Forms.ComboBox comboBoxStyle;
        private System.Windows.Forms.Label labelStyle;
        private System.Windows.Forms.ComboBox comboBoxSourceImage;
        private System.Windows.Forms.Label labelSourceImage;
        private System.Windows.Forms.NumericUpDown LowBox;
        private System.Windows.Forms.NumericUpDown HighBox;
        private System.Windows.Forms.Label customLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
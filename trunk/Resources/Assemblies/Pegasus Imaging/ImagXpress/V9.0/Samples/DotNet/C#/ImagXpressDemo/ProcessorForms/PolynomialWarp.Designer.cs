/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class PolynomialWarpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PolynomialWarpForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.LLBox2 = new System.Windows.Forms.NumericUpDown();
            this.LRBox2 = new System.Windows.Forms.NumericUpDown();
            this.URBox2 = new System.Windows.Forms.NumericUpDown();
            this.LLBox = new System.Windows.Forms.NumericUpDown();
            this.LRBox = new System.Windows.Forms.NumericUpDown();
            this.URBox = new System.Windows.Forms.NumericUpDown();
            this.labelPoint4Seperator = new System.Windows.Forms.Label();
            this.labelPoint3Seperator = new System.Windows.Forms.Label();
            this.labelPoint2Seperator = new System.Windows.Forms.Label();
            this.labelPoint1Seperator = new System.Windows.Forms.Label();
            this.labelPoint4 = new System.Windows.Forms.Label();
            this.labelPoint3 = new System.Windows.Forms.Label();
            this.labelPoint2 = new System.Windows.Forms.Label();
            this.labelPoint1 = new System.Windows.Forms.Label();
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.buttonBackgroundColor = new System.Windows.Forms.Button();
            this.labelBackgroundColor = new System.Windows.Forms.Label();
            this.ULBox = new System.Windows.Forms.NumericUpDown();
            this.ULBox2 = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.LLBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LRBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.URBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LLBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LRBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.URBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ULBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ULBox2)).BeginInit();
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(397, 564);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(631, 564);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(513, 564);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // LLBox2
            // 
            this.LLBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LLBox2.Location = new System.Drawing.Point(253, 506);
            this.LLBox2.Name = "LLBox2";
            this.LLBox2.Size = new System.Drawing.Size(75, 20);
            this.LLBox2.TabIndex = 47;
            // 
            // LRBox2
            // 
            this.LRBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LRBox2.Location = new System.Drawing.Point(253, 480);
            this.LRBox2.Name = "LRBox2";
            this.LRBox2.Size = new System.Drawing.Size(75, 20);
            this.LRBox2.TabIndex = 46;
            // 
            // URBox2
            // 
            this.URBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.URBox2.Location = new System.Drawing.Point(253, 454);
            this.URBox2.Name = "URBox2";
            this.URBox2.Size = new System.Drawing.Size(75, 20);
            this.URBox2.TabIndex = 45;
            // 
            // LLBox
            // 
            this.LLBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LLBox.Location = new System.Drawing.Point(153, 505);
            this.LLBox.Name = "LLBox";
            this.LLBox.Size = new System.Drawing.Size(75, 20);
            this.LLBox.TabIndex = 43;
            // 
            // LRBox
            // 
            this.LRBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LRBox.Location = new System.Drawing.Point(153, 479);
            this.LRBox.Name = "LRBox";
            this.LRBox.Size = new System.Drawing.Size(75, 20);
            this.LRBox.TabIndex = 42;
            // 
            // URBox
            // 
            this.URBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.URBox.Location = new System.Drawing.Point(152, 454);
            this.URBox.Name = "URBox";
            this.URBox.Size = new System.Drawing.Size(75, 20);
            this.URBox.TabIndex = 41;
            // 
            // labelPoint4Seperator
            // 
            this.labelPoint4Seperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint4Seperator.AutoSize = true;
            this.labelPoint4Seperator.Location = new System.Drawing.Point(238, 508);
            this.labelPoint4Seperator.Name = "labelPoint4Seperator";
            this.labelPoint4Seperator.Size = new System.Drawing.Size(10, 13);
            this.labelPoint4Seperator.TabIndex = 37;
            this.labelPoint4Seperator.Text = ",";
            // 
            // labelPoint3Seperator
            // 
            this.labelPoint3Seperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint3Seperator.AutoSize = true;
            this.labelPoint3Seperator.Location = new System.Drawing.Point(238, 482);
            this.labelPoint3Seperator.Name = "labelPoint3Seperator";
            this.labelPoint3Seperator.Size = new System.Drawing.Size(10, 13);
            this.labelPoint3Seperator.TabIndex = 36;
            this.labelPoint3Seperator.Text = ",";
            // 
            // labelPoint2Seperator
            // 
            this.labelPoint2Seperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint2Seperator.AutoSize = true;
            this.labelPoint2Seperator.Location = new System.Drawing.Point(237, 457);
            this.labelPoint2Seperator.Name = "labelPoint2Seperator";
            this.labelPoint2Seperator.Size = new System.Drawing.Size(10, 13);
            this.labelPoint2Seperator.TabIndex = 35;
            this.labelPoint2Seperator.Text = ",";
            // 
            // labelPoint1Seperator
            // 
            this.labelPoint1Seperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint1Seperator.AutoSize = true;
            this.labelPoint1Seperator.Location = new System.Drawing.Point(237, 431);
            this.labelPoint1Seperator.Name = "labelPoint1Seperator";
            this.labelPoint1Seperator.Size = new System.Drawing.Size(10, 13);
            this.labelPoint1Seperator.TabIndex = 34;
            this.labelPoint1Seperator.Text = ",";
            // 
            // labelPoint4
            // 
            this.labelPoint4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint4.AutoSize = true;
            this.labelPoint4.Location = new System.Drawing.Point(26, 508);
            this.labelPoint4.Name = "labelPoint4";
            this.labelPoint4.Size = new System.Drawing.Size(114, 13);
            this.labelPoint4.TabIndex = 33;
            this.labelPoint4.Text = "Lower Left Coordinate:";
            // 
            // labelPoint3
            // 
            this.labelPoint3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint3.AutoSize = true;
            this.labelPoint3.Location = new System.Drawing.Point(26, 482);
            this.labelPoint3.Name = "labelPoint3";
            this.labelPoint3.Size = new System.Drawing.Size(121, 13);
            this.labelPoint3.TabIndex = 32;
            this.labelPoint3.Text = "Lower Right Coordinate:";
            // 
            // labelPoint2
            // 
            this.labelPoint2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint2.AutoSize = true;
            this.labelPoint2.Location = new System.Drawing.Point(32, 456);
            this.labelPoint2.Name = "labelPoint2";
            this.labelPoint2.Size = new System.Drawing.Size(121, 13);
            this.labelPoint2.TabIndex = 31;
            this.labelPoint2.Text = "Upper Right Coordinate:";
            // 
            // labelPoint1
            // 
            this.labelPoint1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPoint1.AutoSize = true;
            this.labelPoint1.Location = new System.Drawing.Point(32, 430);
            this.labelPoint1.Name = "labelPoint1";
            this.labelPoint1.Size = new System.Drawing.Size(114, 13);
            this.labelPoint1.TabIndex = 30;
            this.labelPoint1.Text = "Upper Left Coordinate:";
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(11, 564);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 48;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            this.HelpProcessorButton.Click += new System.EventHandler(this.HelpProcessorButton_Click);
            // 
            // buttonBackgroundColor
            // 
            this.buttonBackgroundColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBackgroundColor.BackColor = System.Drawing.Color.Black;
            this.buttonBackgroundColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackgroundColor.Location = new System.Drawing.Point(254, 542);
            this.buttonBackgroundColor.Name = "buttonBackgroundColor";
            this.buttonBackgroundColor.Size = new System.Drawing.Size(27, 26);
            this.buttonBackgroundColor.TabIndex = 50;
            this.buttonBackgroundColor.UseVisualStyleBackColor = false;
            this.buttonBackgroundColor.Click += new System.EventHandler(this.buttonBackgroundColor_Click);
            // 
            // labelBackgroundColor
            // 
            this.labelBackgroundColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelBackgroundColor.AutoSize = true;
            this.labelBackgroundColor.Location = new System.Drawing.Point(153, 549);
            this.labelBackgroundColor.Name = "labelBackgroundColor";
            this.labelBackgroundColor.Size = new System.Drawing.Size(95, 13);
            this.labelBackgroundColor.TabIndex = 49;
            this.labelBackgroundColor.Text = "Background Color:";
            // 
            // ULBox
            // 
            this.ULBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ULBox.Location = new System.Drawing.Point(152, 428);
            this.ULBox.Name = "ULBox";
            this.ULBox.Size = new System.Drawing.Size(75, 20);
            this.ULBox.TabIndex = 40;
            // 
            // ULBox2
            // 
            this.ULBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ULBox2.Location = new System.Drawing.Point(253, 428);
            this.ULBox2.Name = "ULBox2";
            this.ULBox2.Size = new System.Drawing.Size(75, 20);
            this.ULBox2.TabIndex = 44;
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(14, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 52;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(14, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 51;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // PolynomialWarpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 599);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.buttonBackgroundColor);
            this.Controls.Add(this.labelBackgroundColor);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.LLBox2);
            this.Controls.Add(this.LRBox2);
            this.Controls.Add(this.URBox2);
            this.Controls.Add(this.ULBox2);
            this.Controls.Add(this.LLBox);
            this.Controls.Add(this.LRBox);
            this.Controls.Add(this.URBox);
            this.Controls.Add(this.ULBox);
            this.Controls.Add(this.labelPoint4Seperator);
            this.Controls.Add(this.labelPoint3Seperator);
            this.Controls.Add(this.labelPoint2Seperator);
            this.Controls.Add(this.labelPoint1Seperator);
            this.Controls.Add(this.labelPoint4);
            this.Controls.Add(this.labelPoint3);
            this.Controls.Add(this.labelPoint2);
            this.Controls.Add(this.labelPoint1);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PolynomialWarpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PolynomialWarp";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LLBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LRBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.URBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LLBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LRBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.URBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ULBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ULBox2)).EndInit();
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.NumericUpDown LLBox2;
        private System.Windows.Forms.NumericUpDown LRBox2;
        private System.Windows.Forms.NumericUpDown URBox2;
        private System.Windows.Forms.NumericUpDown LLBox;
        private System.Windows.Forms.NumericUpDown LRBox;
        private System.Windows.Forms.NumericUpDown URBox;
        private System.Windows.Forms.Label labelPoint4Seperator;
        private System.Windows.Forms.Label labelPoint3Seperator;
        private System.Windows.Forms.Label labelPoint2Seperator;
        private System.Windows.Forms.Label labelPoint1Seperator;
        private System.Windows.Forms.Label labelPoint4;
        private System.Windows.Forms.Label labelPoint3;
        private System.Windows.Forms.Label labelPoint2;
        private System.Windows.Forms.Label labelPoint1;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.Button buttonBackgroundColor;
        private System.Windows.Forms.Label labelBackgroundColor;
        private System.Windows.Forms.NumericUpDown ULBox;
        private System.Windows.Forms.NumericUpDown ULBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress9;

namespace Binarize
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem mnuFileFile;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.String strCurrentDir;
		private System.String 	strCurrentImage;
		private System.String strImagePath;
		private System.Boolean bImageLoaded;
		private System.Windows.Forms.ComboBox cmbBlur;
		private System.Windows.Forms.HScrollBar hsLocalContrast;
		private System.Windows.Forms.HScrollBar hsEccentricity;
		private System.Windows.Forms.HScrollBar hsPitch;
		private System.Windows.Forms.HScrollBar hsAngle;
		private System.Windows.Forms.HScrollBar hsLowThresh;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbMode;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button cmdProcess;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress9.LoadOptions loLoadOptions;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnuToolbar;
        private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.TextBox textPitch;
		private System.Windows.Forms.TextBox textEccentricity;
        private System.Windows.Forms.TextBox textLocalContrast;
		private System.Windows.Forms.TextBox textLowThresh;
		private System.Windows.Forms.TextBox textHighThresh;
		private System.Windows.Forms.TextBox textAngle;
        private System.Windows.Forms.HScrollBar hsHighThresh;
        private System.Windows.Forms.ListBox lstDesc;
        private Panel panel1;
        private TableLayoutPanel tableLayoutPanel1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView2;
        private IContainer components;

        //new variables
        private BinarizeMode _mode = BinarizeMode.QuickText;
        private System.Int32 _lowThreshold = 0;
        private System.Int32 _gridAngle = 0;
        private System.Int32 _highThreshold = 1;
        private System.Int32 _gridPitch = 1;
        private System.Int32 _eccentricity = 0;
        private System.Int32 _lceFactor = 0;
        private BinarizeBlur _blur = BinarizeBlur.NoBlur;
        private Processor prc;
        private Button cmdReload;
        private Label statusLabel;
        
        private Button cmdBinarize;
       


		public FormMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();


            //set up event handlers
            imageXView2.ZoomFactorChanged += new ImageXView.ZoomFactorChangedEventHandler(imageXView2_ZoomFactorChanged);
            imageXView2.ScrollEvent += new ImageXView.ScrollEventHandler(imageXView2_ScrollEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				//  Don't forget to dispose IX
				// 
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (!(imageXView2 == null)) 
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}


                if (!(prc == null))
                {
                    prc.Dispose();
                    prc = null;
                }

				if (components != null) 
				{
					components.Dispose();
					
				}
			}
			
			

			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.mnuFile = new System.Windows.Forms.MainMenu(this.components);
            this.mnuFileFile = new System.Windows.Forms.MenuItem();
            this.mnuFileOpen = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuFileQuit = new System.Windows.Forms.MenuItem();
            this.mnuToolbar = new System.Windows.Forms.MenuItem();
            this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.cmbBlur = new System.Windows.Forms.ComboBox();
            this.hsLocalContrast = new System.Windows.Forms.HScrollBar();
            this.hsEccentricity = new System.Windows.Forms.HScrollBar();
            this.hsPitch = new System.Windows.Forms.HScrollBar();
            this.hsAngle = new System.Windows.Forms.HScrollBar();
            this.hsHighThresh = new System.Windows.Forms.HScrollBar();
            this.hsLowThresh = new System.Windows.Forms.HScrollBar();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbMode = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmdProcess = new System.Windows.Forms.Button();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.textPitch = new System.Windows.Forms.TextBox();
            this.textEccentricity = new System.Windows.Forms.TextBox();
            this.textLocalContrast = new System.Windows.Forms.TextBox();
            this.textLowThresh = new System.Windows.Forms.TextBox();
            this.textHighThresh = new System.Windows.Forms.TextBox();
            this.textAngle = new System.Windows.Forms.TextBox();
            this.lstDesc = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdBinarize = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXView2 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.prc = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.cmdReload = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuFile
            // 
            this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileFile,
            this.mnuToolbar,
            this.mnuAbout});
            // 
            // mnuFileFile
            // 
            this.mnuFileFile.Index = 0;
            this.mnuFileFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileOpen,
            this.menuItem3,
            this.mnuFileQuit});
            this.mnuFileFile.Text = "&File";
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.Index = 0;
            this.mnuFileOpen.Text = "&Open";
            this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "-";
            // 
            // mnuFileQuit
            // 
            this.mnuFileQuit.Index = 2;
            this.mnuFileQuit.Text = "&Quit";
            this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
            // 
            // mnuToolbar
            // 
            this.mnuToolbar.Index = 1;
            this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuToolbarShow});
            this.mnuToolbar.Text = "&Toolbar";
            // 
            // mnuToolbarShow
            // 
            this.mnuToolbarShow.Index = 0;
            this.mnuToolbarShow.Text = "&Show";
            this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 2;
            this.mnuAbout.Text = "&About";
            this.mnuAbout.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // cmbBlur
            // 
            this.cmbBlur.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmbBlur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBlur.ItemHeight = 13;
            this.cmbBlur.Items.AddRange(new object[] {
            "None",
            "Gaussian",
            "Smart"});
            this.cmbBlur.Location = new System.Drawing.Point(92, 497);
            this.cmbBlur.Name = "cmbBlur";
            this.cmbBlur.Size = new System.Drawing.Size(192, 21);
            this.cmbBlur.TabIndex = 31;
            // 
            // hsLocalContrast
            // 
            this.hsLocalContrast.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hsLocalContrast.LargeChange = 1;
            this.hsLocalContrast.Location = new System.Drawing.Point(381, 475);
            this.hsLocalContrast.Maximum = 255;
            this.hsLocalContrast.Name = "hsLocalContrast";
            this.hsLocalContrast.Size = new System.Drawing.Size(338, 16);
            this.hsLocalContrast.TabIndex = 30;
            this.hsLocalContrast.Value = 163;
            this.hsLocalContrast.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsLocalContrast_Scroll);
            // 
            // hsEccentricity
            // 
            this.hsEccentricity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hsEccentricity.LargeChange = 1;
            this.hsEccentricity.Location = new System.Drawing.Point(386, 448);
            this.hsEccentricity.Maximum = 255;
            this.hsEccentricity.Minimum = -255;
            this.hsEccentricity.Name = "hsEccentricity";
            this.hsEccentricity.Size = new System.Drawing.Size(338, 16);
            this.hsEccentricity.TabIndex = 29;
            this.hsEccentricity.Value = 255;
            this.hsEccentricity.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsEccentricity_Scroll);
            // 
            // hsPitch
            // 
            this.hsPitch.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hsPitch.Enabled = false;
            this.hsPitch.LargeChange = 1;
            this.hsPitch.Location = new System.Drawing.Point(381, 417);
            this.hsPitch.Maximum = 32;
            this.hsPitch.Minimum = 1;
            this.hsPitch.Name = "hsPitch";
            this.hsPitch.Size = new System.Drawing.Size(338, 16);
            this.hsPitch.TabIndex = 28;
            this.hsPitch.Value = 1;
            this.hsPitch.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsPitch_Scroll);
            // 
            // hsAngle
            // 
            this.hsAngle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hsAngle.LargeChange = 1;
            this.hsAngle.Location = new System.Drawing.Point(121, 467);
            this.hsAngle.Maximum = 360;
            this.hsAngle.Name = "hsAngle";
            this.hsAngle.Size = new System.Drawing.Size(128, 16);
            this.hsAngle.TabIndex = 27;
            this.hsAngle.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsAngle_Scroll);
            // 
            // hsHighThresh
            // 
            this.hsHighThresh.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hsHighThresh.LargeChange = 1;
            this.hsHighThresh.Location = new System.Drawing.Point(121, 443);
            this.hsHighThresh.Maximum = 255;
            this.hsHighThresh.Name = "hsHighThresh";
            this.hsHighThresh.Size = new System.Drawing.Size(128, 16);
            this.hsHighThresh.TabIndex = 25;
            this.hsHighThresh.Value = 126;
            this.hsHighThresh.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsHighThresh_Scroll);
            // 
            // hsLowThresh
            // 
            this.hsLowThresh.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hsLowThresh.LargeChange = 1;
            this.hsLowThresh.Location = new System.Drawing.Point(118, 416);
            this.hsLowThresh.Maximum = 255;
            this.hsLowThresh.Name = "hsLowThresh";
            this.hsLowThresh.Size = new System.Drawing.Size(128, 16);
            this.hsLowThresh.TabIndex = 24;
            this.hsLowThresh.Value = 86;
            this.hsLowThresh.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hsLowThresh_Scroll);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.Location = new System.Drawing.Point(30, 491);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = "Blur Type";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.Location = new System.Drawing.Point(300, 475);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 16);
            this.label7.TabIndex = 22;
            this.label7.Text = "Local Contrast";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.Location = new System.Drawing.Point(303, 448);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 21;
            this.label6.Text = "Eccentricity";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.Location = new System.Drawing.Point(298, 417);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 20;
            this.label5.Text = "Grid Pitch";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.Location = new System.Drawing.Point(30, 464);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "Grid Angle";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.Location = new System.Drawing.Point(30, 443);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 17;
            this.label2.Text = "High Threshold";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(30, 416);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Low Threshold";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbMode
            // 
            this.cmbMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMode.Items.AddRange(new object[] {
            "QuickText",
            "HalfTone"});
            this.cmbMode.Location = new System.Drawing.Point(349, 502);
            this.cmbMode.Name = "cmbMode";
            this.cmbMode.Size = new System.Drawing.Size(410, 21);
            this.cmbMode.TabIndex = 32;
            this.cmbMode.SelectedIndexChanged += new System.EventHandler(this.cmbMode_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.Location = new System.Drawing.Point(303, 502);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 16);
            this.label9.TabIndex = 33;
            this.label9.Text = "Mode:";
            // 
            // cmdProcess
            // 
            this.cmdProcess.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmdProcess.Location = new System.Drawing.Point(539, 600);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Size = new System.Drawing.Size(165, 39);
            this.cmdProcess.TabIndex = 41;
            this.cmdProcess.Text = "AutoBinarize";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // textPitch
            // 
            this.textPitch.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textPitch.Location = new System.Drawing.Point(727, 417);
            this.textPitch.Name = "textPitch";
            this.textPitch.Size = new System.Drawing.Size(32, 21);
            this.textPitch.TabIndex = 45;
            this.textPitch.Text = "1";
            this.textPitch.TextChanged += new System.EventHandler(this.textPitch_TextChanged);
            // 
            // textEccentricity
            // 
            this.textEccentricity.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textEccentricity.Location = new System.Drawing.Point(730, 448);
            this.textEccentricity.Name = "textEccentricity";
            this.textEccentricity.Size = new System.Drawing.Size(32, 21);
            this.textEccentricity.TabIndex = 46;
            this.textEccentricity.Text = "0";
            this.textEccentricity.TextChanged += new System.EventHandler(this.textEccentricity_TextChanged);
            // 
            // textLocalContrast
            // 
            this.textLocalContrast.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textLocalContrast.Location = new System.Drawing.Point(727, 475);
            this.textLocalContrast.Name = "textLocalContrast";
            this.textLocalContrast.Size = new System.Drawing.Size(32, 21);
            this.textLocalContrast.TabIndex = 47;
            this.textLocalContrast.Text = "0";
            this.textLocalContrast.TextChanged += new System.EventHandler(this.textLocalContrast_TextChanged);
            // 
            // textLowThresh
            // 
            this.textLowThresh.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textLowThresh.Location = new System.Drawing.Point(250, 416);
            this.textLowThresh.Name = "textLowThresh";
            this.textLowThresh.Size = new System.Drawing.Size(32, 21);
            this.textLowThresh.TabIndex = 50;
            this.textLowThresh.Text = "0";
            this.textLowThresh.TextChanged += new System.EventHandler(this.textLowThresh_TextChanged);
            // 
            // textHighThresh
            // 
            this.textHighThresh.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textHighThresh.Location = new System.Drawing.Point(250, 443);
            this.textHighThresh.Name = "textHighThresh";
            this.textHighThresh.Size = new System.Drawing.Size(32, 21);
            this.textHighThresh.TabIndex = 51;
            this.textHighThresh.Text = "0";
            this.textHighThresh.TextChanged += new System.EventHandler(this.textHighThresh_TextChanged);
            // 
            // textAngle
            // 
            this.textAngle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textAngle.Location = new System.Drawing.Point(252, 470);
            this.textAngle.Name = "textAngle";
            this.textAngle.Size = new System.Drawing.Size(32, 21);
            this.textAngle.TabIndex = 52;
            this.textAngle.Text = "0";
            this.textAngle.TextChanged += new System.EventHandler(this.textAngle_TextChanged);
            // 
            // lstDesc
            // 
            this.lstDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDesc.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1) Using the new AutoBinarize functionality to automatically process and image an" +
                "d then retrieve the settings used."});
            this.lstDesc.Location = new System.Drawing.Point(3, 3);
            this.lstDesc.Name = "lstDesc";
            this.lstDesc.Size = new System.Drawing.Size(741, 69);
            this.lstDesc.TabIndex = 55;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cmdBinarize);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.lstDesc);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textAngle);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textHighThresh);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textLowThresh);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textLocalContrast);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textEccentricity);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textPitch);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.hsLowThresh);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.hsHighThresh);
            this.panel1.Controls.Add(this.cmbMode);
            this.panel1.Controls.Add(this.hsAngle);
            this.panel1.Controls.Add(this.cmbBlur);
            this.panel1.Controls.Add(this.hsPitch);
            this.panel1.Controls.Add(this.hsLocalContrast);
            this.panel1.Controls.Add(this.hsEccentricity);
            this.panel1.Location = new System.Drawing.Point(23, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(769, 591);
            this.panel1.TabIndex = 57;
            // 
            // cmdBinarize
            // 
            this.cmdBinarize.Location = new System.Drawing.Point(516, 550);
            this.cmdBinarize.Name = "cmdBinarize";
            this.cmdBinarize.Size = new System.Drawing.Size(165, 37);
            this.cmdBinarize.TabIndex = 58;
            this.cmdBinarize.Text = "Binarize";
            this.cmdBinarize.UseVisualStyleBackColor = true;
            this.cmdBinarize.Click += new System.EventHandler(this.cmdBinarize_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.imageXView1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.imageXView2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 90);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(758, 286);
            this.tableLayoutPanel1.TabIndex = 56;
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.Location = new System.Drawing.Point(3, 3);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(373, 280);
            this.imageXView1.TabIndex = 0;
            // 
            // imageXView2
            // 
            this.imageXView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView2.Location = new System.Drawing.Point(382, 3);
            this.imageXView2.Name = "imageXView2";
            this.imageXView2.Size = new System.Drawing.Size(373, 280);
            this.imageXView2.TabIndex = 1;
            // 
            // cmdReload
            // 
            this.cmdReload.Location = new System.Drawing.Point(23, 610);
            this.cmdReload.Name = "cmdReload";
            this.cmdReload.Size = new System.Drawing.Size(168, 31);
            this.cmdReload.TabIndex = 58;
            this.cmdReload.Text = "ReloadImage";
            this.cmdReload.UseVisualStyleBackColor = true;
            this.cmdReload.Click += new System.EventHandler(this.cmdReload_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(26, 656);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 13);
            this.statusLabel.TabIndex = 59;
            // 
            // FormMain
            // 
            this.AcceptButton = this.cmdProcess;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(792, 686);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.cmdReload);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cmdProcess);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Menu = this.mnuFile;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Binarize";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

       

        //private void hsLowThresh_Changed(object sender, System.EventArgs e)
        //{
        //    hsHighThresh.Minimum = hsLowThresh.Value;
        //    textLowThresh.Text = hsLowThresh.Value.ToString(cultNumber);
        //    textHighThresh.Text = hsHighThresh.Value.ToString(cultNumber);
        //}

        //private void hsHighTresh_Changed(object sender, System.EventArgs e)
        //{
        //    textHighThresh.Text = hsHighThresh.Value.ToString(cultNumber);
        //}

        //private void hsAngle_Changed(object sender, System.EventArgs e)
        //{
        //    textAngle.Text = hsAngle.Value.ToString(cultNumber);		
        //}

        //private void hsPitch_Changed(object sender, System.EventArgs e)
        //{
        //    textPitch.Text = hsPitch.Value.ToString(cultNumber);
        //}

        //private void hsEccentricity_ValueChanged(object sender, System.EventArgs e)
        //{
        //    textEccentricity.Text = hsEccentricity.Value.ToString(cultNumber);
        //}

        //private void hsLocalContrast_ValueChanged(object sender, System.EventArgs e)
        //{
        //    textLocalContrast.Text = hsLocalContrast.Value.ToString(cultNumber);		
        //}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
            Application.EnableVisualStyles();

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.Licensing.UnlockRuntime(12345,12345,12345,12345);

			//Set the default directory to the common images directory
			strCurrentDir = System.IO.Path.Combine(System.Environment.CurrentDirectory,strCommonImagesDirectory);

			//Create a new load options object so we can recieve events from the images we load
			loLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();

			System.String strCurrentdir = System.IO.Directory.GetCurrentDirectory ().ToString ();
			
			strImagePath = System.IO.Path.Combine (strCurrentdir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\binarize.jpg");
			//Load the images and set some default parameters
         
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImagePath);
            imageXView1.AutoResize = AutoResizeType.BestFit;
            imageXView1.AutoResize = AutoResizeType.CropImage;

        
            imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImagePath);

            imageXView2.AutoResize = AutoResizeType.BestFit;
            imageXView2.AutoResize = AutoResizeType.CropImage;
            

            bImageLoaded = true;
			
			cmbBlur.SelectedIndex = 0;
			cmbMode.SelectedIndex = 0;

			textLowThresh.Text = hsLowThresh.Value.ToString(cultNumber);
			textHighThresh.Text = hsHighThresh.Value.ToString(cultNumber);
			textLocalContrast.Text = hsLocalContrast.Value.ToString(cultNumber);	


			cmdProcess_Click(sender, e);

		}

		private void ModeChange(bool bMode)
		{
			hsLowThresh.Enabled = bMode;
			hsHighThresh.Enabled = bMode;
			hsLocalContrast.Enabled = bMode;
			hsAngle.Enabled = true;
			hsPitch.Enabled = true;
			hsEccentricity.Enabled = true;
		}

		private void cmbMode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbMode.SelectedIndex == 0) 
			{
				//ModeChange(true);
				hsLowThresh.Enabled = true;
				hsHighThresh.Enabled = true;
				hsLocalContrast.Enabled = true;
				hsAngle.Enabled = false;
				hsPitch.Enabled = false;
				hsEccentricity.Enabled = false;
			} 
			else 
			{
				hsLowThresh.Enabled = false;
				hsHighThresh.Enabled = false;
				hsLocalContrast.Enabled = false;
				hsAngle.Enabled = true;
				hsPitch.Enabled = true;
				hsEccentricity.Enabled = true;
			}
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (bImageLoaded) 
			{
				try 
				{
                    PegasusImaging.WinForms.ImagXpress9.ImageX imageToProcess = imageXView1.Image.Copy();
                    prc.Image = imageXView1.Image.Copy();
                    prc.AutoBinarize();

                    _lceFactor = prc.BinarizeLceFactor;
                    textLocalContrast.Text = _lceFactor.ToString();
                    hsLocalContrast.Value = _lceFactor;

                    _lowThreshold = prc.BinarizeLowThreshold;
                    textLowThresh.Text = _lowThreshold.ToString();
                    hsLowThresh.Value = _lowThreshold;

                    _highThreshold = prc.BinarizeHighThreshold;
                    textHighThresh.Text = _highThreshold.ToString();
                    hsHighThresh.Value = _highThreshold;

                    _eccentricity = prc.BinarizeEccentricity;
                    textEccentricity.Text = _eccentricity.ToString();
                    hsEccentricity.Value = _eccentricity;

                    _gridAngle = prc.BinarizeGridAngle;
                    textAngle.Text = _gridAngle.ToString();
                    hsAngle.Value = _gridAngle;

                    _gridPitch = prc.BinarizeGridPitch;
                    if (_gridPitch <= 0)
                        _gridPitch = 1;
                    textPitch.Text = _gridPitch.ToString();
                    hsPitch.Value = _gridPitch;

                    _blur = prc.BinarizeBlur;
                    cmbBlur.SelectedIndex = (int)prc.BinarizeBlur;

                    _mode = prc.BinarizeMode;
                    cmbMode.SelectedIndex = (int)prc.BinarizeMode;

                    imageXView2.Image = prc.Image;
                    SynchScrollPosition();
                    
                   
             
  
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
				{
					PegasusError(ex,statusLabel);
				}
			}
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			System.String sFileName = PegasusOpenFile();
			if (sFileName.Length != 0) 
			{
				strCurrentImage = sFileName;
				try 
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, sFileName, loLoadOptions);
					imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress9.ZoomToFitType.FitBest);

					imageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, sFileName, loLoadOptions);
					imageXView2.ZoomToFit(PegasusImaging.WinForms.ImagXpress9.ZoomToFitType.FitBest);
	
				} 
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
				{
					PegasusError(ex,statusLabel);
				}
				bImageLoaded = true;
			}
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
			{
				PegasusError(ex,statusLabel);
			}
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			mnuToolbarShow.Text = (imageXView2.Toolbar.Activated)? "Show":"Hide";
			try 
			{
				imageXView2.Toolbar.Activated = !imageXView2.Toolbar.Activated;
			} 
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
			{
				PegasusError(ex,statusLabel);
			}
		}

		private void textPitch_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsPitch,textPitch);
		}

		private void textLocalContrast_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsLocalContrast,textLocalContrast);
		}

		private void textLowThresh_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsLowThresh,textLowThresh);
		}

		private void textHighThresh_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsHighThresh,textHighThresh);
		}

		private void textAngle_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsAngle,textAngle);
		}

		private void textEccentricity_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsEccentricity,textEccentricity);
		}

		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp;*.dwg;*.dxf;*.dwf;*.hdp;*.wdp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*.dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.hdp;*.wdp|All Files (*.*)|*.*";
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.ToolStripStatusLabel statusLabel)
        {
            statusLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }

        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
        }

        static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.ToolStripStatusLabel statusLabel)
        {
            statusLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
        }

		static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}

		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,statusLabel);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,statusLabel);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion

        private void SynchScrollPosition()
        {
            //set the output control's scroll view position to the source control's scroll view position
            imageXView2.ScrollPosition = imageXView1.ScrollPosition;
        }

        #region ImageXView object synchronization

        void imageXView2_ZoomFactorChanged(object sender, PegasusImaging.WinForms.ImagXpress9.ZoomFactorChangedEventArgs e)
        {
            imageXView1.ZoomFactor = e.NewValue;

            System.Drawing.Point point = new Point(imageXView2.ScrollPosition.X, imageXView2.ScrollPosition.Y);
            imageXView1.ScrollPosition = point;
            imageXView1.Refresh();
        }

        void imageXView2_ScrollEvent(object sender, PegasusImaging.WinForms.ImagXpress9.ScrollEventArgs e)
        {
            System.Drawing.Point point = new Point(e.NewValue.X, e.NewValue.Y);
            imageXView1.ScrollPosition = point;
            imageXView1.Refresh();
        }

        #endregion

        private void cmdBinarize_Click(object sender, EventArgs e)
        {
            prc.Image = imageXView1.Image.Copy();
            prc.Binarize(_mode, _lowThreshold, _highThreshold, _gridAngle, _gridPitch, _eccentricity, _lceFactor, _blur);
            imageXView2.Image = prc.Image;
        }

        private void hsPitch_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            textPitch.Text = hsPitch.Value.ToString();
            _gridPitch = hsPitch.Value;
        }

        private void hsEccentricity_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            textEccentricity.Text = hsEccentricity.Value.ToString();
            _eccentricity = hsEccentricity.Value;
        }

        private void hsLocalContrast_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            textLocalContrast.Text = hsLocalContrast.Value.ToString();
            _lceFactor = hsLocalContrast.Value;
        }

        private void hsLowThresh_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            if (hsLowThresh.Value > hsHighThresh.Value)
                hsLowThresh.Value = hsHighThresh.Value;


            textLowThresh.Text = hsLowThresh.Value.ToString();
            _lowThreshold = hsLowThresh.Value;
        }

        private void hsHighThresh_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            if (hsHighThresh.Value < hsLowThresh.Value)
                hsHighThresh.Value = hsLowThresh.Value;


            textHighThresh.Text = hsHighThresh.Value.ToString();
            _highThreshold = hsLowThresh.Value;
        }

        private void hsAngle_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            textAngle.Text = hsAngle.Value.ToString();
            _gridAngle = hsAngle.Value;
        }

        private void cmdReload_Click(object sender, EventArgs e)
        {
            imageXView2.Image = imageXView1.Image.Copy();
            SynchScrollPosition();
        }
		

	}
}

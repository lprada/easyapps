/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace ImagXpressDemo.ProcessorForms
{
    partial class ReplaceColorsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }

                processor1.Dispose();
                processor1 = null;
            }
            if (imageXViewCurrent != null)
            {
                if (imageXViewCurrent.Image != null)
                {
                    imageXViewCurrent.Image.Dispose();
                    imageXViewCurrent.Image = null;
                }

                imageXViewCurrent.Dispose();
                imageXViewCurrent = null;
            }
            if (imageXViewPreview != null)
            {
                if (imageXViewPreview.Image != null)
                {
                    imageXViewPreview.Image.Dispose();
                    imageXViewPreview.Image = null;
                }

                imageXViewPreview.Dispose();
                imageXViewPreview = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplaceColorsForm));
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.ApplyButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.HelpProcessorButton = new System.Windows.Forms.Button();
            this.buttonReplaceColor = new System.Windows.Forms.Button();
            this.labelReplaceColor = new System.Windows.Forms.Label();
            this.buttonToColor = new System.Windows.Forms.Button();
            this.labelToColor = new System.Windows.Forms.Label();
            this.buttonFromColor = new System.Windows.Forms.Button();
            this.labelFromColor = new System.Windows.Forms.Label();
            this.tableLayoutPanelTop = new System.Windows.Forms.TableLayoutPanel();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.imageXViewPreview = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imageXViewCurrent = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.tableLayoutPanelTop.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ApplyButton
            // 
            this.ApplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ApplyButton.Location = new System.Drawing.Point(403, 490);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(101, 23);
            this.ApplyButton.TabIndex = 0;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.Location = new System.Drawing.Point(637, 490);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(101, 23);
            this.CButton.TabIndex = 1;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.PreviewButton.Location = new System.Drawing.Point(519, 490);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(101, 23);
            this.PreviewButton.TabIndex = 2;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // HelpProcessorButton
            // 
            this.HelpProcessorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.HelpProcessorButton.Location = new System.Drawing.Point(12, 490);
            this.HelpProcessorButton.Name = "HelpProcessorButton";
            this.HelpProcessorButton.Size = new System.Drawing.Size(101, 23);
            this.HelpProcessorButton.TabIndex = 36;
            this.HelpProcessorButton.Text = "Help";
            this.HelpProcessorButton.UseVisualStyleBackColor = true;
            // 
            // buttonReplaceColor
            // 
            this.buttonReplaceColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonReplaceColor.BackColor = System.Drawing.Color.Green;
            this.buttonReplaceColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReplaceColor.Location = new System.Drawing.Point(296, 423);
            this.buttonReplaceColor.Name = "buttonReplaceColor";
            this.buttonReplaceColor.Size = new System.Drawing.Size(25, 26);
            this.buttonReplaceColor.TabIndex = 42;
            this.buttonReplaceColor.UseVisualStyleBackColor = false;
            this.buttonReplaceColor.Click += new System.EventHandler(this.buttonReplaceColor_Click);
            // 
            // labelReplaceColor
            // 
            this.labelReplaceColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelReplaceColor.AutoSize = true;
            this.labelReplaceColor.Location = new System.Drawing.Point(215, 430);
            this.labelReplaceColor.Name = "labelReplaceColor";
            this.labelReplaceColor.Size = new System.Drawing.Size(75, 13);
            this.labelReplaceColor.TabIndex = 41;
            this.labelReplaceColor.Text = "Replace With:";
            // 
            // buttonToColor
            // 
            this.buttonToColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonToColor.BackColor = System.Drawing.Color.Red;
            this.buttonToColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonToColor.Location = new System.Drawing.Point(159, 469);
            this.buttonToColor.Name = "buttonToColor";
            this.buttonToColor.Size = new System.Drawing.Size(27, 26);
            this.buttonToColor.TabIndex = 40;
            this.buttonToColor.UseVisualStyleBackColor = false;
            this.buttonToColor.Click += new System.EventHandler(this.buttonToColor_Click);
            // 
            // labelToColor
            // 
            this.labelToColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelToColor.AutoSize = true;
            this.labelToColor.Location = new System.Drawing.Point(120, 476);
            this.labelToColor.Name = "labelToColor";
            this.labelToColor.Size = new System.Drawing.Size(23, 13);
            this.labelToColor.TabIndex = 39;
            this.labelToColor.Text = "To:";
            // 
            // buttonFromColor
            // 
            this.buttonFromColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonFromColor.BackColor = System.Drawing.Color.Black;
            this.buttonFromColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFromColor.Location = new System.Drawing.Point(159, 423);
            this.buttonFromColor.Name = "buttonFromColor";
            this.buttonFromColor.Size = new System.Drawing.Size(27, 26);
            this.buttonFromColor.TabIndex = 38;
            this.buttonFromColor.UseVisualStyleBackColor = false;
            this.buttonFromColor.Click += new System.EventHandler(this.buttonFromColor_Click);
            // 
            // labelFromColor
            // 
            this.labelFromColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFromColor.AutoSize = true;
            this.labelFromColor.Location = new System.Drawing.Point(120, 430);
            this.labelFromColor.Name = "labelFromColor";
            this.labelFromColor.Size = new System.Drawing.Size(33, 13);
            this.labelFromColor.TabIndex = 37;
            this.labelFromColor.Text = "From:";
            // 
            // tableLayoutPanelTop
            // 
            this.tableLayoutPanelTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelTop.ColumnCount = 2;
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.06935F));
            this.tableLayoutPanelTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.93065F));
            this.tableLayoutPanelTop.Controls.Add(this.CurrentLabel, 0, 0);
            this.tableLayoutPanelTop.Controls.Add(this.PreviewLabel, 1, 0);
            this.tableLayoutPanelTop.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanelTop.Name = "tableLayoutPanelTop";
            this.tableLayoutPanelTop.RowCount = 1;
            this.tableLayoutPanelTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelTop.Size = new System.Drawing.Size(721, 32);
            this.tableLayoutPanelTop.TabIndex = 51;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CurrentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentLabel.Location = new System.Drawing.Point(96, 0);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(168, 27);
            this.CurrentLabel.TabIndex = 1;
            this.CurrentLabel.Text = "Current Image";
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PreviewLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PreviewLabel.Location = new System.Drawing.Point(458, 0);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(165, 25);
            this.PreviewLabel.TabIndex = 0;
            this.PreviewLabel.Text = "Preview Image";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.imageXViewPreview, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.imageXViewCurrent, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 47);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(723, 360);
            this.tableLayoutPanel.TabIndex = 50;
            // 
            // imageXViewPreview
            // 
            this.imageXViewPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewPreview.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewPreview.AutoScroll = true;
            this.imageXViewPreview.Location = new System.Drawing.Point(364, 3);
            this.imageXViewPreview.MouseWheelCapture = false;
            this.imageXViewPreview.Name = "imageXViewPreview";
            this.imageXViewPreview.Size = new System.Drawing.Size(356, 356);
            this.imageXViewPreview.TabIndex = 1;
            // 
            // imageXViewCurrent
            // 
            this.imageXViewCurrent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXViewCurrent.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXViewCurrent.AutoScroll = true;
            this.imageXViewCurrent.Location = new System.Drawing.Point(3, 3);
            this.imageXViewCurrent.MouseWheelCapture = false;
            this.imageXViewCurrent.Name = "imageXViewCurrent";
            this.imageXViewCurrent.Size = new System.Drawing.Size(355, 356);
            this.imageXViewCurrent.TabIndex = 0;
            // 
            // ReplaceColorsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 523);
            this.Controls.Add(this.tableLayoutPanelTop);
            this.Controls.Add(this.tableLayoutPanel);
            this.Controls.Add(this.buttonReplaceColor);
            this.Controls.Add(this.labelReplaceColor);
            this.Controls.Add(this.buttonToColor);
            this.Controls.Add(this.labelToColor);
            this.Controls.Add(this.buttonFromColor);
            this.Controls.Add(this.labelFromColor);
            this.Controls.Add(this.HelpProcessorButton);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReplaceColorsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ReplaceColors";
            this.Load += new System.EventHandler(this.ProcessorForm_Load);
            this.tableLayoutPanelTop.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Button PreviewButton;
        private PegasusImaging.WinForms.ImagXpress9.Processor processor1;
        private System.Windows.Forms.Button HelpProcessorButton;
        private System.Windows.Forms.Button buttonReplaceColor;
        private System.Windows.Forms.Label labelReplaceColor;
        private System.Windows.Forms.Button buttonToColor;
        private System.Windows.Forms.Label labelToColor;
        private System.Windows.Forms.Button buttonFromColor;
        private System.Windows.Forms.Label labelFromColor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelTop;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewPreview;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXViewCurrent;
    }
}
'****************************************************************'
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Public Class Rubberbanding
    Inherits System.Windows.Forms.Form

    Private strImageFile1 As System.String
    Private strImageFile2 As System.String
    Private pointBegin As System.Drawing.Point
    Private pointEnd As System.Drawing.Point
    Dim banding As System.Boolean
    Private imagX1 As PegasusImaging.WinForms.ImagXpress9.ImageX
    Private tempcopy As PegasusImaging.WinForms.ImagXpress9.ImageX
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            ''Dispose of the ImagXpress and ImageView objects and the Processor object created
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (ImageXView2 Is Nothing) Then
                ImageXView2.Dispose()
                ImageXView2 = Nothing
            End If

            If Not (tempcopy Is Nothing) Then
                tempcopy.Dispose()
                tempcopy = Nothing
            End If


            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ImageXView2 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnuShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents lstDesc As System.Windows.Forms.ListBox
    Friend WithEvents lblErrorDesc As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.ImageXView2 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.lblerror = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuOpen = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuQuit = New System.Windows.Forms.MenuItem
        Me.mnuTool = New System.Windows.Forms.MenuItem
        Me.mnuShow = New System.Windows.Forms.MenuItem
        Me.mnuAbout = New System.Windows.Forms.MenuItem
        Me.lstDesc = New System.Windows.Forms.ListBox
        Me.lblErrorDesc = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(10, 160)
        Me.ImageXView1.MouseWheelCapture = False
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(296, 240)
        Me.ImageXView1.TabIndex = 0
        '
        'ImageXView2
        '
        Me.ImageXView2.AutoScroll = True
        Me.ImageXView2.Location = New System.Drawing.Point(310, 160)
        Me.ImageXView2.MouseWheelCapture = False
        Me.ImageXView2.Name = "ImageXView2"
        Me.ImageXView2.Size = New System.Drawing.Size(234, 240)
        Me.ImageXView2.TabIndex = 1
        '
        'lblerror
        '
        Me.lblerror.Location = New System.Drawing.Point(96, 72)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(216, 72)
        Me.lblerror.TabIndex = 2
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuTool, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.MenuItem1, Me.mnuQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Text = "&Open"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 1
        Me.MenuItem1.Text = "-"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 2
        Me.mnuQuit.Text = "&Quit"
        '
        'mnuTool
        '
        Me.mnuTool.Index = 1
        Me.mnuTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuShow})
        Me.mnuTool.Text = "&ToolBar"
        '
        'mnuShow
        '
        Me.mnuShow.Index = 0
        Me.mnuShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'lstDesc
        '
        Me.lstDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDesc.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using the Rubberband property of the ImageXView class to draw a rubberband  on " & _
                        "1 ImageXView ", "  control and then using the Mouse events to copy the selected portion to a 2nd I" & _
                        "magXView control."})
        Me.lstDesc.Location = New System.Drawing.Point(8, 8)
        Me.lstDesc.Name = "lstDesc"
        Me.lstDesc.Size = New System.Drawing.Size(624, 56)
        Me.lstDesc.TabIndex = 4
        '
        'lblErrorDesc
        '
        Me.lblErrorDesc.Location = New System.Drawing.Point(8, 72)
        Me.lblErrorDesc.Name = "lblErrorDesc"
        Me.lblErrorDesc.Size = New System.Drawing.Size(88, 24)
        Me.lblErrorDesc.TabIndex = 6
        Me.lblErrorDesc.Text = "Last Error:"
        '
        'Rubberbanding
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(712, 525)
        Me.Controls.Add(Me.lblErrorDesc)
        Me.Controls.Add(Me.lstDesc)
        Me.Controls.Add(Me.lblerror)
        Me.Controls.Add(Me.ImageXView2)
        Me.Controls.Add(Me.ImageXView1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Menu = Me.MainMenu1
        Me.Name = "Rubberbanding"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ImagXpress 9 Rubberbanding"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"

    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\Common\Images\"
    Dim strCurrentDir As System.String = System.IO.Path.GetFullPath(Application.StartupPath & "\..\..\..\..\..\..\..\Common\Images")
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.dwf;*.dwg;*.dxf;*.gif;*.hdp;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wdp;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|CAD Drawing (*.DWG)|*" & _
 ".dwg|Autodesk Design Web Format (*.DWF)|*.dwf|AutoCAD DXF (*.DXF)|*.dxf|HD Photo (*.HDP & *.WDP)|*.h" & _
 "dp;*.wdp|All Files (*.*)|*.*"

    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region
    Private Sub Rubberbanding_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            '***Must call UnlockRuntime to Distribute Application*** 
            'ImagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

            'here we set the current directory and image so that the file open dialog box works well

            strImageFile1 = System.IO.Path.Combine(strCurrentDir, "window.jpg")
            strImageFile2 = System.IO.Path.Combine(strCurrentDir, "vermont.jpg")


            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFile1)
            ImageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFile2)

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub ImageXView1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ImageXView1.MouseDown

        Try
            pointBegin.X = e.X
            pointBegin.Y = e.Y
            banding = True
            ImageXView1.Rubberband.Enabled = True
            ImageXView1.Rubberband.Start(pointBegin, False)

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub ImageXView1_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ImageXView1.MouseMove
        Try
            If banding Then
                pointEnd.X = e.X
                pointEnd.Y = e.Y
            End If
            ImageXView1.Rubberband.Update(pointEnd)

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub ImageXView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ImageXView1.MouseUp
        Try
            pointEnd.X = e.X
            pointEnd.Y = e.Y
            ImageXView1.Rubberband.Update(pointEnd)
            'Disable the RubberBanding
            banding = False
            'Create a processor object
            Dim prc As New PegasusImaging.WinForms.ImagXpress9.Processor(ImagXpress1)
            'obtain the rubberbanded area via the Copy
            tempcopy = ImageXView1.Rubberband.Copy()
            prc = New PegasusImaging.WinForms.ImagXpress9.Processor(ImagXpress1, ImageXView2.Image)
            'Convert a System.Rectangle to a System.RectangleF
            Dim recF As System.Drawing.RectangleF = RectangleF.op_Implicit(ImageXView1.Rubberband.Dimensions)
            'Set the Area to copy the rubberbanded area to
            prc.SetArea(recF)
            If tempcopy Is Nothing = False Then
                prc.Merge(tempcopy, PegasusImaging.WinForms.ImagXpress9.MergeSize.Crop, PegasusImaging.WinForms.ImagXpress9.MergeStyle.Normal, False, System.Drawing.Color.Blue, 0, 0)
            End If

        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ImagXpress1.AboutBox()
    End Sub

    Private Sub mnuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then

            Try
                strImageFile1 = strtemp
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFile1)
            Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
                PegasusError(ex, lblerror)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub


    Private Sub mnuShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShow.Click
        If ImageXView1.Toolbar.Activated = True Then
            mnuShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False
        Else
            mnuShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True
        End If
    End Sub

    Private Sub mnuOpen_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then

            Try
                'clear error before next image load
                lblerror.Text = ""

                strImageFile1 = strtemp
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFile1)
            Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
                PegasusError(ex, lblerror)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub

    Private Sub mnuAbout_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()
    End Sub
End Class

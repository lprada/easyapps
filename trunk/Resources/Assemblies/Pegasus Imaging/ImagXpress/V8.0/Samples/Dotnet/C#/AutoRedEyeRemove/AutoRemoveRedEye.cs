/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace AutoRemoveRedEye
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class AutoRemoveRedEye : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress8.LoadOptions loLoadOptions;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.Processor prc;
		private PegasusImaging.WinForms.ImagXpress8.ImageX imagX1;
		private System.Windows.Forms.Button cmdRemove;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		
		//Declare global variables
		
		
		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.Label lblerror;
		private System.Windows.Forms.MenuItem mnuOpen;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem mnuQuit;
		private System.Windows.Forms.MenuItem mnuToolBar;
		private System.Windows.Forms.MenuItem mnuShow;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.ListBox liststatus;
		private System.Windows.Forms.Label lsterror;

		private System.String strCurrentDir;
		private  System.String 	strImageFile; 
		
	
		//redeye information
		System.Int32 redeyex;
		System.Int32 redeyey;
		System.Int32 redeyewidth;
		System.Int32 redeyeheight;
		private System.Windows.Forms.ComboBox cmbGlare;
		private System.Windows.Forms.ComboBox cmbShade;
		private System.Windows.Forms.Label lblGlare;
		private System.Windows.Forms.Label lblShade;
		private System.Windows.Forms.ListBox lstDesc;
		private System.Windows.Forms.GroupBox grpRedResult;
		private System.Windows.Forms.Label lblCount;
		private System.Windows.Forms.ColumnHeader RedEyeIndex;
		private System.Windows.Forms.ColumnHeader XPos;
		private System.Windows.Forms.ColumnHeader YPos;
	
		private System.Windows.Forms.ListView resultsList;
		private System.Windows.Forms.Label lblRedDesc;
		private System.Windows.Forms.ColumnHeader AreaWidth;
		private System.Windows.Forms.ColumnHeader AreaHeight;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AutoRemoveRedEye()
		{
			//The UnlockControl function must be called to distribute the runtime**
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				//Dispose of the ImagXpress and ImageXView objects
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}

				if (imageXView1	!= null)
				{
					imageXView1.Dispose();
					imageXView1	 = null;
				}

				if (prc	!= null)
				{
					prc.Dispose();
					prc	 = null;
				}

				if (components != null) 
				{
					components.Dispose();
				}
			}
		
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.liststatus = new System.Windows.Forms.ListBox();
			this.lstDesc = new System.Windows.Forms.ListBox();
			this.cmdRemove = new System.Windows.Forms.Button();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.mnuOpen = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.mnuQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolBar = new System.Windows.Forms.MenuItem();
			this.mnuShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.lblStatus = new System.Windows.Forms.Label();
			this.lblerror = new System.Windows.Forms.Label();
			this.lsterror = new System.Windows.Forms.Label();
			this.cmbGlare = new System.Windows.Forms.ComboBox();
			this.cmbShade = new System.Windows.Forms.ComboBox();
			this.lblGlare = new System.Windows.Forms.Label();
			this.lblShade = new System.Windows.Forms.Label();
			this.grpRedResult = new System.Windows.Forms.GroupBox();
			this.lblRedDesc = new System.Windows.Forms.Label();
			this.resultsList = new System.Windows.Forms.ListView();
			this.RedEyeIndex = new System.Windows.Forms.ColumnHeader();
			this.YPos = new System.Windows.Forms.ColumnHeader();
			this.XPos = new System.Windows.Forms.ColumnHeader();
			this.AreaWidth = new System.Windows.Forms.ColumnHeader();
			this.AreaHeight = new System.Windows.Forms.ColumnHeader();
			this.lblCount = new System.Windows.Forms.Label();
			this.grpRedResult.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView1.AutoScroll = true;
			this.imageXView1.Location = new System.Drawing.Point(16, 80);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(288, 384);
			this.imageXView1.TabIndex = 0;
			// 
			// liststatus
			// 
			this.liststatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.liststatus.Location = new System.Drawing.Point(584, 96);
			this.liststatus.Name = "liststatus";
			this.liststatus.Size = new System.Drawing.Size(168, 160);
			this.liststatus.TabIndex = 1;
			// 
			// lstDesc
			// 
			this.lstDesc.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstDesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstDesc.Items.AddRange(new object[] {
														 "This sample demonstrates using the AutoRemoveRedEye method in the ImagXpress 8 co" +
														 "ntrol. The sample",
														 "demonstrates using the RedeyeCollection Class to retrieve information about the r" +
														 "ed eyes found in the image."});
			this.lstDesc.Location = new System.Drawing.Point(24, 8);
			this.lstDesc.Name = "lstDesc";
			this.lstDesc.Size = new System.Drawing.Size(728, 56);
			this.lstDesc.TabIndex = 2;
			// 
			// cmdRemove
			// 
			this.cmdRemove.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdRemove.Location = new System.Drawing.Point(376, 224);
			this.cmdRemove.Name = "cmdRemove";
			this.cmdRemove.Size = new System.Drawing.Size(136, 32);
			this.cmdRemove.TabIndex = 3;
			this.cmdRemove.Text = "AutoRemoveRedEye";
			this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.mnuToolBar,
																					  this.mnuAbout});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuOpen,
																					  this.menuItem2,
																					  this.mnuQuit});
			this.menuItem1.Text = "&File";
			// 
			// mnuOpen
			// 
			this.mnuOpen.Index = 0;
			this.mnuOpen.Text = "&Open";
			this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click_1);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 2;
			this.mnuQuit.Text = "&Quit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// mnuToolBar
			// 
			this.mnuToolBar.Index = 1;
			this.mnuToolBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuShow});
			this.mnuToolBar.Text = "&Toolbar";
			// 
			// mnuShow
			// 
			this.mnuShow.Index = 0;
			this.mnuShow.Text = "&Show";
			this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// lblStatus
			// 
			this.lblStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblStatus.Location = new System.Drawing.Point(584, 72);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(168, 16);
			this.lblStatus.TabIndex = 4;
			this.lblStatus.Text = "Load Status:";
			this.lblStatus.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// lblerror
			// 
			this.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblerror.Location = new System.Drawing.Point(320, 96);
			this.lblerror.Name = "lblerror";
			this.lblerror.Size = new System.Drawing.Size(248, 56);
			this.lblerror.TabIndex = 5;
			// 
			// lsterror
			// 
			this.lsterror.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lsterror.Location = new System.Drawing.Point(320, 72);
			this.lsterror.Name = "lsterror";
			this.lsterror.Size = new System.Drawing.Size(112, 16);
			this.lsterror.TabIndex = 6;
			this.lsterror.Text = "Last Error Reported:";
			// 
			// cmbGlare
			// 
			this.cmbGlare.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmbGlare.Items.AddRange(new object[] {
														  "None",
														  "Slight",
														  "Full"});
			this.cmbGlare.Location = new System.Drawing.Point(320, 192);
			this.cmbGlare.Name = "cmbGlare";
			this.cmbGlare.Size = new System.Drawing.Size(112, 21);
			this.cmbGlare.TabIndex = 7;
			
			// 
			// cmbShade
			// 
			this.cmbShade.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmbShade.Items.AddRange(new object[] {
														  "Normal",
														  "Light",
														  "Dark"});
			this.cmbShade.Location = new System.Drawing.Point(456, 192);
			this.cmbShade.Name = "cmbShade";
			this.cmbShade.Size = new System.Drawing.Size(112, 21);
			this.cmbShade.TabIndex = 8;
			// 
			// lblGlare
			// 
			this.lblGlare.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblGlare.Location = new System.Drawing.Point(320, 168);
			this.lblGlare.Name = "lblGlare";
			this.lblGlare.Size = new System.Drawing.Size(112, 16);
			this.lblGlare.TabIndex = 9;
			this.lblGlare.Text = "Glare Settings";
			this.lblGlare.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// lblShade
			// 
			this.lblShade.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblShade.Location = new System.Drawing.Point(456, 168);
			this.lblShade.Name = "lblShade";
			this.lblShade.Size = new System.Drawing.Size(112, 16);
			this.lblShade.TabIndex = 10;
			this.lblShade.Text = "Eye Shade";
			this.lblShade.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// grpRedResult
			// 
			this.grpRedResult.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.grpRedResult.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.lblRedDesc,
																					   this.resultsList,
																					   this.lblCount});
			this.grpRedResult.Location = new System.Drawing.Point(312, 264);
			this.grpRedResult.Name = "grpRedResult";
			this.grpRedResult.Size = new System.Drawing.Size(440, 200);
			this.grpRedResult.TabIndex = 11;
			this.grpRedResult.TabStop = false;
			this.grpRedResult.Text = "RedEye Results";
			// 
			// lblRedDesc
			// 
			this.lblRedDesc.Location = new System.Drawing.Point(24, 32);
			this.lblRedDesc.Name = "lblRedDesc";
			this.lblRedDesc.Size = new System.Drawing.Size(160, 16);
			this.lblRedDesc.TabIndex = 2;
			this.lblRedDesc.Text = "Number of Red Eyes Found:";
			// 
			// resultsList
			// 
			this.resultsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						  this.RedEyeIndex,
																						  this.YPos,
																						  this.XPos,
																						  this.AreaWidth,
																						  this.AreaHeight});
			this.resultsList.Location = new System.Drawing.Point(16, 56);
			this.resultsList.Name = "resultsList";
			this.resultsList.Size = new System.Drawing.Size(408, 128);
			this.resultsList.TabIndex = 1;
			this.resultsList.View = System.Windows.Forms.View.Details;
			
			// 
			// RedEyeIndex
			// 
			this.RedEyeIndex.Text = "RedEyeIndex";
			this.RedEyeIndex.Width = 85;
			// 
			// YPos
			// 
			this.YPos.Text = "YPos";
			// 
			// XPos
			// 
			this.XPos.Text = "XPos";
			// 
			// AreaWidth
			// 
			this.AreaWidth.Text = "AreaWidth";
			this.AreaWidth.Width = 100;
			// 
			// AreaHeight
			// 
			this.AreaHeight.Text = "AreaHeight";
			this.AreaHeight.Width = 100;
			// 
			// lblCount
			// 
			this.lblCount.Location = new System.Drawing.Point(192, 32);
			this.lblCount.Name = "lblCount";
			this.lblCount.Size = new System.Drawing.Size(152, 16);
			this.lblCount.TabIndex = 0;
			// 
			// AutoRemoveRedEye
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(768, 481);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.grpRedResult,
																		  this.lblShade,
																		  this.lblGlare,
																		  this.cmbShade,
																		  this.cmbGlare,
																		  this.lsterror,
																		  this.lblerror,
																		  this.lblStatus,
																		  this.cmdRemove,
																		  this.lstDesc,
																		  this.liststatus,
																		  this.imageXView1});
			this.Menu = this.mainMenu1;
			this.Name = "AutoRemoveRedEye";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "C# AutoRemoveRedEye";
			this.Load += new System.EventHandler(this.AutoRemoveRedEye_Load);
			this.grpRedResult.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new AutoRemoveRedEye());
		}

		private void cmdRemove_Click(object sender, System.EventArgs e)
		{
			
			resultsList.Items.Clear();
			lblCount.Text = "";

		

				//Glare type
				PegasusImaging.WinForms.ImagXpress8.RedeyeGlare gglare = ((PegasusImaging.WinForms.ImagXpress8.RedeyeGlare )cmbGlare.SelectedIndex);
		
				//Shade type
				PegasusImaging.WinForms.ImagXpress8.RedeyeShade sshade = ((PegasusImaging.WinForms.ImagXpress8.RedeyeShade )cmbShade.SelectedIndex);
				//Create a processor object here
				prc = new PegasusImaging.WinForms.ImagXpress8.Processor(imageXView1.Image);
				//redeye collection/ to be added
				PegasusImaging.WinForms.ImagXpress8.RedeyeCollection myRedEyes = new PegasusImaging.WinForms.ImagXpress8.RedeyeCollection();
				prc.Redeyes = myRedEyes;

				//Remove the RedEyes from the image
				prc.AutoRemoveRedeye(sshade,gglare,false);
				//report the count of red eyes found in the image
				lblCount.Text = myRedEyes.Count.ToString();
			

			//get the rectangles thought to be redeyes and report information about them
			for(int counter = 0;counter < myRedEyes.Count; counter ++)
			{
				
				redeyex = myRedEyes.GetRedeyeRectangle(counter).Xposition;
				redeyey = myRedEyes.GetRedeyeRectangle(counter).Yposition;
				redeyewidth = myRedEyes.GetRedeyeRectangle(counter).Width;
				redeyeheight = myRedEyes.GetRedeyeRectangle(counter).Height;
			
				resultsList.Items.Add(

						new ListViewItem(
					new String[]{counter.ToString(),redeyex.ToString(),redeyey.ToString(),redeyewidth.ToString(),redeyeheight.ToString()}
				)

				);
			}
			prc.Dispose();
			prc = null;
		}

		
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblerror);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblerror);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion


		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			liststatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			liststatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				liststatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			liststatus.SelectedIndex = liststatus.Items.Count - 1;
		}


	

		private void AutoRemoveRedEye_Load(object sender, System.EventArgs e)
		{
				
			cmbGlare.SelectedIndex = 0;
			cmbShade.SelectedIndex = 0;
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);
			try
			{
				//Create a new load options object so we can recieve events from the images we load
				loLoadOptions = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblerror);
			}
			try
			{
				//this is where events are assigned. This happens before the file gets loaded.
				PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
				PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
			
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,lblerror);

			}
			
			try
			{
				imagX1 = new PegasusImaging.WinForms.ImagXpress8.ImageX();
		
			
				//here we set the current directory and image so that the file open dialog box works well
				strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString ();
				strImageFile = System.IO.Path.Combine (strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\redeye2.jpg");
				strCurrentDir = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\");
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile,loLoadOptions);
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,lblerror);

			}
			
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void mnuOpen_Click_1(object sender, System.EventArgs e)
		{
			System.String strTmp = PegasusOpenFile();
			if (strTmp.Length != 0) 
			{
				try
				{
					strImageFile = strTmp;

					PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = imageXView1.Image;

					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile,loLoadOptions);


					// clear out the error in case there was an error from a previous operation
					lblerror.Text = "";

					if (oldImage != null)
					{
						oldImage.Dispose();
						oldImage = null;
					}
				}
				catch(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
				{
					PegasusError(ex,lblerror);
				}
				catch(System.IO.IOException ex)
				{
					PegasusError(ex,lblerror);
				}
				resultsList.Items.Clear();
				
			}		
		}
		
		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();

		}

		private void mnuShow_Click(object sender, System.EventArgs e)
		{
			this.mnuShow.Text = (imageXView1.Toolbar.Activated) ? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				PegasusError(eX,lblerror);
			}
		}
		
	}
}

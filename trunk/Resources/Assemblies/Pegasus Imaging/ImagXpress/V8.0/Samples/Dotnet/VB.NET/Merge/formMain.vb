'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports PegasusImaging.WinForms.ImagXpress8

Namespace Merge
    ' <summary>
    ' Summary description for Form1.
    ' </summary>
    Public Class FormMain
        Inherits System.Windows.Forms.Form

        Private WithEvents buttonMerge As System.Windows.Forms.Button
        Private WithEvents buttonQuit As System.Windows.Forms.Button
        Private WithEvents checkBoxTransparent As System.Windows.Forms.CheckBox
        Private WithEvents comboBoxMergeStyle As System.Windows.Forms.ComboBox
        Private WithEvents comboBoxTransparentColor As System.Windows.Forms.ComboBox
        Private WithEvents comboBoxMergeType As System.Windows.Forms.ComboBox
        Private WithEvents hScrollBarMax As System.Windows.Forms.HScrollBar
        Private WithEvents hScrollBarMin As System.Windows.Forms.HScrollBar
        Private label1 As System.Windows.Forms.Label
        Private label2 As System.Windows.Forms.Label
        Private labelPctMax As System.Windows.Forms.Label
        Private labelPctMin As System.Windows.Forms.Label

        'File I/O Variables

        Dim strCurrentDir As System.String
        Dim strimageFile1 As System.String
        Dim strimageFile2 As System.String
        Dim process As PegasusImaging.WinForms.ImagXpress8.Processor
        Private WithEvents imagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Private WithEvents IxSource As PegasusImaging.WinForms.ImagXpress8.ImageXView
        Private ImageSource As PegasusImaging.WinForms.ImagXpress8.ImageX
        Private WithEvents mainMenu1 As System.Windows.Forms.MainMenu
        Private WithEvents menuFile As System.Windows.Forms.MenuItem
        Private WithEvents menuItem2 As System.Windows.Forms.MenuItem
        Private WithEvents menuItem5 As System.Windows.Forms.MenuItem
        Private WithEvents buttonReloadDestination As System.Windows.Forms.Button
        Private bSelectionActive As Boolean
        Private WithEvents IxDest As PegasusImaging.WinForms.ImagXpress8.ImageXView
        Private WithEvents menuToolbar As System.Windows.Forms.MenuItem
        Private WithEvents menuToolbarShow As System.Windows.Forms.MenuItem
        Private WithEvents menuFileOpenSource As System.Windows.Forms.MenuItem
        Private WithEvents menuFileOpenDest As System.Windows.Forms.MenuItem
        Private WithEvents menuFileQuit As System.Windows.Forms.MenuItem

        ' <summary>
        ' Required designer variable.
        ' </summary>
        Private components As System.ComponentModel.Container = Nothing


#Region "Pegasus Imaging Sample Application Standard Functions"
        '/*********************************************************************
        '*     Pegasus Imaging Corporation Standard Function Definitions     *
        ' *********************************************************************/


        Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
        Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
        Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
        Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"
        Dim printDocument1 As System.Drawing.Printing.PrintDocument
        Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
        ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
        "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
        "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
        "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
        ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
        "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
        "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
        "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


        Private Function GetFileName(ByVal FullName As String) As String

            Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
        End Function

        Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                        + (ErrorException.Source + "" & vbLf)))
        End Sub

        Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                        + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
        End Sub

        Private Overloads Function PegasusOpenFile() As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strDefaultImageFilter
            dlg.InitialDirectory = strCurrentDir
            If (dlg.ShowDialog = DialogResult.OK) Then
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

        Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strFilter
            dlg.InitialDirectory = strCurrentDir
            If (dlg.ShowDialog = DialogResult.OK) Then
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

#End Region

        Public Sub New()
            MyBase.New()


            '**The UnlockControl function must be called to distribute the runtime**
            'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

            '
            ' Required for Windows Form Designer support
            '
            InitializeComponent()
            '
            ' TODO: Add any constructor code after InitializeComponent call
            '
        End Sub

        ' <summary>
        ' Clean up any resources being used.
        ' </summary>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If (Not (IxDest) Is Nothing) Then
                    IxDest.Dispose()
                    IxDest = Nothing
                End If

                If (Not (IxSource) Is Nothing) Then
                    IxSource.Dispose()
                    IxSource = Nothing
                End If

                If (Not (components) Is Nothing) Then
                    components.Dispose()
                End If
            End If

            MyBase.Dispose(disposing)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Friend WithEvents lstStatus As System.Windows.Forms.ListBox
        Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
        Friend WithEvents lblError As System.Windows.Forms.Label
        Friend WithEvents lblLastError As System.Windows.Forms.Label
        Friend WithEvents lstInfo As System.Windows.Forms.ListBox
        Private Sub InitializeComponent()
            Me.buttonMerge = New System.Windows.Forms.Button()
            Me.buttonQuit = New System.Windows.Forms.Button()
            Me.checkBoxTransparent = New System.Windows.Forms.CheckBox()
            Me.comboBoxMergeStyle = New System.Windows.Forms.ComboBox()
            Me.IxSource = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            Me.comboBoxTransparentColor = New System.Windows.Forms.ComboBox()
            Me.comboBoxMergeType = New System.Windows.Forms.ComboBox()
            Me.hScrollBarMax = New System.Windows.Forms.HScrollBar()
            Me.hScrollBarMin = New System.Windows.Forms.HScrollBar()
            Me.label1 = New System.Windows.Forms.Label()
            Me.label2 = New System.Windows.Forms.Label()
            Me.labelPctMax = New System.Windows.Forms.Label()
            Me.labelPctMin = New System.Windows.Forms.Label()
            Me.imagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
            Me.mainMenu1 = New System.Windows.Forms.MainMenu()
            Me.menuFile = New System.Windows.Forms.MenuItem()
            Me.menuFileOpenSource = New System.Windows.Forms.MenuItem()
            Me.menuFileOpenDest = New System.Windows.Forms.MenuItem()
            Me.menuItem5 = New System.Windows.Forms.MenuItem()
            Me.menuFileQuit = New System.Windows.Forms.MenuItem()
            Me.menuToolbar = New System.Windows.Forms.MenuItem()
            Me.menuToolbarShow = New System.Windows.Forms.MenuItem()
            Me.menuItem2 = New System.Windows.Forms.MenuItem()
            Me.buttonReloadDestination = New System.Windows.Forms.Button()
            Me.IxDest = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            Me.lstStatus = New System.Windows.Forms.ListBox()
            Me.lblLoadStatus = New System.Windows.Forms.Label()
            Me.lblError = New System.Windows.Forms.Label()
            Me.lblLastError = New System.Windows.Forms.Label()
            Me.lstInfo = New System.Windows.Forms.ListBox()
            Me.SuspendLayout()
            '
            'buttonMerge
            '
            Me.buttonMerge.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.buttonMerge.Location = New System.Drawing.Point(416, 296)
            Me.buttonMerge.Name = "buttonMerge"
            Me.buttonMerge.Size = New System.Drawing.Size(168, 32)
            Me.buttonMerge.TabIndex = 1
            Me.buttonMerge.Text = "Merge"
            '
            'buttonQuit
            '
            Me.buttonQuit.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.buttonQuit.Location = New System.Drawing.Point(416, 376)
            Me.buttonQuit.Name = "buttonQuit"
            Me.buttonQuit.Size = New System.Drawing.Size(168, 32)
            Me.buttonQuit.TabIndex = 2
            Me.buttonQuit.Text = "Quit"
            '
            'checkBoxTransparent
            '
            Me.checkBoxTransparent.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
            Me.checkBoxTransparent.Location = New System.Drawing.Point(16, 408)
            Me.checkBoxTransparent.Name = "checkBoxTransparent"
            Me.checkBoxTransparent.Size = New System.Drawing.Size(88, 16)
            Me.checkBoxTransparent.TabIndex = 6
            Me.checkBoxTransparent.Text = "Transparent"
            '
            'comboBoxMergeStyle
            '
            Me.comboBoxMergeStyle.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.comboBoxMergeStyle.Items.AddRange(New Object() {"Normal", "If Darker", "If Lighter", "Additive", "Subtractive", "Superimpose", "Superimpose Bottom to Top", "Superimpose Horiz from Center", "Superimpose Horiz to Center", "Superimpose Left to Right", "Superimpose Right to Left", "Superimpose Top to Bottom", "Superimpose Vert. from Center", "Superimpose Vert. to Center"})
            Me.comboBoxMergeStyle.Location = New System.Drawing.Point(584, 432)
            Me.comboBoxMergeStyle.Name = "comboBoxMergeStyle"
            Me.comboBoxMergeStyle.Size = New System.Drawing.Size(248, 21)
            Me.comboBoxMergeStyle.TabIndex = 5
            Me.comboBoxMergeStyle.Text = "Merge Style"
            '
            'IxSource
            '
            Me.IxSource.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.IxSource.Location = New System.Drawing.Point(416, 80)
            Me.IxSource.Name = "IxSource"
            Me.IxSource.Size = New System.Drawing.Size(168, 208)
            Me.IxSource.TabIndex = 15
            '
            'comboBoxTransparentColor
            '
            Me.comboBoxTransparentColor.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
            Me.comboBoxTransparentColor.Enabled = False
            Me.comboBoxTransparentColor.Items.AddRange(New Object() {"Red", "Green", "Blue", "White"})
            Me.comboBoxTransparentColor.Location = New System.Drawing.Point(16, 432)
            Me.comboBoxTransparentColor.Name = "comboBoxTransparentColor"
            Me.comboBoxTransparentColor.Size = New System.Drawing.Size(104, 21)
            Me.comboBoxTransparentColor.TabIndex = 3
            Me.comboBoxTransparentColor.Text = "Transparent Color"
            '
            'comboBoxMergeType
            '
            Me.comboBoxMergeType.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.comboBoxMergeType.Items.AddRange(New Object() {"Crop", "Resize Area", "Resize Image", "Tile Image"})
            Me.comboBoxMergeType.Location = New System.Drawing.Point(152, 432)
            Me.comboBoxMergeType.Name = "comboBoxMergeType"
            Me.comboBoxMergeType.Size = New System.Drawing.Size(392, 21)
            Me.comboBoxMergeType.TabIndex = 4
            Me.comboBoxMergeType.Text = "Merge Type"
            '
            'hScrollBarMax
            '
            Me.hScrollBarMax.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.hScrollBarMax.LargeChange = 1
            Me.hScrollBarMax.Location = New System.Drawing.Point(176, 472)
            Me.hScrollBarMax.Name = "hScrollBarMax"
            Me.hScrollBarMax.Size = New System.Drawing.Size(656, 16)
            Me.hScrollBarMax.TabIndex = 7
            Me.hScrollBarMax.Value = 100
            '
            'hScrollBarMin
            '
            Me.hScrollBarMin.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.hScrollBarMin.LargeChange = 1
            Me.hScrollBarMin.Location = New System.Drawing.Point(176, 504)
            Me.hScrollBarMin.Name = "hScrollBarMin"
            Me.hScrollBarMin.Size = New System.Drawing.Size(656, 16)
            Me.hScrollBarMin.TabIndex = 7
            '
            'label1
            '
            Me.label1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
            Me.label1.Location = New System.Drawing.Point(8, 472)
            Me.label1.Name = "label1"
            Me.label1.Size = New System.Drawing.Size(144, 16)
            Me.label1.TabIndex = 18
            Me.label1.Text = "Superimpose Percent Max"
            Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'label2
            '
            Me.label2.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
            Me.label2.Location = New System.Drawing.Point(8, 504)
            Me.label2.Name = "label2"
            Me.label2.Size = New System.Drawing.Size(136, 16)
            Me.label2.TabIndex = 19
            Me.label2.Text = "Superimpose Percent Min"
            Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'labelPctMax
            '
            Me.labelPctMax.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
            Me.labelPctMax.Location = New System.Drawing.Point(144, 472)
            Me.labelPctMax.Name = "labelPctMax"
            Me.labelPctMax.Size = New System.Drawing.Size(24, 16)
            Me.labelPctMax.TabIndex = 20
            Me.labelPctMax.Text = "100"
            Me.labelPctMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'labelPctMin
            '
            Me.labelPctMin.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
            Me.labelPctMin.Location = New System.Drawing.Point(144, 504)
            Me.labelPctMin.Name = "labelPctMin"
            Me.labelPctMin.Size = New System.Drawing.Size(24, 16)
            Me.labelPctMin.TabIndex = 21
            Me.labelPctMin.Text = "0"
            Me.labelPctMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            'mainMenu1
            '
            Me.mainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuFile, Me.menuToolbar, Me.menuItem2})
            '
            'menuFile
            '
            Me.menuFile.Index = 0
            Me.menuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuFileOpenSource, Me.menuFileOpenDest, Me.menuItem5, Me.menuFileQuit})
            Me.menuFile.Text = "&File"
            '
            'menuFileOpenSource
            '
            Me.menuFileOpenSource.Index = 0
            Me.menuFileOpenSource.Text = "Open &Source Image"
            '
            'menuFileOpenDest
            '
            Me.menuFileOpenDest.Index = 1
            Me.menuFileOpenDest.Text = "Open &Destination Image"
            '
            'menuItem5
            '
            Me.menuItem5.Index = 2
            Me.menuItem5.Text = "-"
            '
            'menuFileQuit
            '
            Me.menuFileQuit.Index = 3
            Me.menuFileQuit.Text = "&Quit"
            '
            'menuToolbar
            '
            Me.menuToolbar.Index = 1
            Me.menuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuToolbarShow})
            Me.menuToolbar.Text = "&Toolbar"
            '
            'menuToolbarShow
            '
            Me.menuToolbarShow.Index = 0
            Me.menuToolbarShow.Text = "&Show"
            '
            'menuItem2
            '
            Me.menuItem2.Index = 2
            Me.menuItem2.Text = "&About"
            '
            'buttonReloadDestination
            '
            Me.buttonReloadDestination.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.buttonReloadDestination.Location = New System.Drawing.Point(416, 336)
            Me.buttonReloadDestination.Name = "buttonReloadDestination"
            Me.buttonReloadDestination.Size = New System.Drawing.Size(168, 32)
            Me.buttonReloadDestination.TabIndex = 26
            Me.buttonReloadDestination.Text = "Reload Destination Image"
            '
            'IxDest
            '
            Me.IxDest.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.IxDest.Location = New System.Drawing.Point(16, 80)
            Me.IxDest.Name = "IxDest"
            Me.IxDest.Size = New System.Drawing.Size(392, 320)
            Me.IxDest.TabIndex = 27
            '
            'lstStatus
            '
            Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.lstStatus.Location = New System.Drawing.Point(592, 136)
            Me.lstStatus.Name = "lstStatus"
            Me.lstStatus.Size = New System.Drawing.Size(248, 95)
            Me.lstStatus.TabIndex = 36
            '
            'lblLoadStatus
            '
            Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblLoadStatus.Location = New System.Drawing.Point(592, 96)
            Me.lblLoadStatus.Name = "lblLoadStatus"
            Me.lblLoadStatus.Size = New System.Drawing.Size(176, 24)
            Me.lblLoadStatus.TabIndex = 35
            Me.lblLoadStatus.Text = "Load Status:"
            '
            'lblError
            '
            Me.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblError.Location = New System.Drawing.Point(600, 296)
            Me.lblError.Name = "lblError"
            Me.lblError.Size = New System.Drawing.Size(200, 96)
            Me.lblError.TabIndex = 34
            '
            'lblLastError
            '
            Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblLastError.Location = New System.Drawing.Point(600, 256)
            Me.lblLastError.Name = "lblLastError"
            Me.lblLastError.Size = New System.Drawing.Size(184, 24)
            Me.lblLastError.TabIndex = 33
            Me.lblLastError.Text = "Last Error:"
            '
            'lstInfo
            '
            Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)1)Using the Area and Merge methods to combine images together."})
            Me.lstInfo.Location = New System.Drawing.Point(16, 8)
            Me.lstInfo.Name = "lstInfo"
            Me.lstInfo.Size = New System.Drawing.Size(816, 56)
            Me.lstInfo.TabIndex = 37
            '
            'FormMain
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(848, 545)
            Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstInfo, Me.lstStatus, Me.lblLoadStatus, Me.lblError, Me.lblLastError, Me.IxDest, Me.buttonReloadDestination, Me.labelPctMin, Me.labelPctMax, Me.label2, Me.label1, Me.hScrollBarMin, Me.hScrollBarMax, Me.comboBoxMergeType, Me.comboBoxTransparentColor, Me.IxSource, Me.comboBoxMergeStyle, Me.checkBoxTransparent, Me.buttonQuit, Me.buttonMerge})
            Me.Menu = Me.mainMenu1
            Me.Name = "FormMain"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "ImagXpress .NET Sample - Merge VB .NET"
            Me.ResumeLayout(False)

        End Sub


        Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)

            lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
            If (e.IsComplete) Then
                lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))

                lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
            End If
        End Sub

        Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

            '
            If (e.Status.Decoded) Then
                lstStatus.Items.Add(e.Status.ToString)
            End If
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End Sub

        Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                '**The UnlockRuntime function must be called to distribute the runtime**
                'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)
                AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
                AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

                bSelectionActive = False
                'here we set the current directory and image so that the file open dialog box works well
                strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString()
                strimageFile1 = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\dome.jpg")
                strimageFile2 = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\ball1.bmp")
                strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
                IxDest.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile1)

                ImageSource = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile2)

                IxSource.Image = ImageSource
                comboBoxMergeType.SelectedIndex = 0
                comboBoxMergeStyle.SelectedIndex = 0

            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try

        End Sub

        Private Sub hScrollBarMax_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarMax.ValueChanged
            labelPctMax.Text = hScrollBarMax.Value.ToString
        End Sub

        Private Sub hScrollBarMin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarMin.ValueChanged
            labelPctMin.Text = hScrollBarMin.Value.ToString
        End Sub

        Private Sub buttonQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonQuit.Click
            Application.Exit()
        End Sub

        Private Sub checkBoxTransparent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkBoxTransparent.Click
            comboBoxTransparentColor.Enabled = checkBoxTransparent.Checked
        End Sub

        Private Sub buttonMerge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonMerge.Click
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            process = New PegasusImaging.WinForms.ImagXpress8.Processor(IxDest.Image)
            Dim MergeTL As System.Drawing.PointF = New System.Drawing.PointF(IxDest.Rubberband.Dimensions.X, IxDest.Rubberband.Dimensions.Y)
            Dim MergeSize As System.Drawing.SizeF = New System.Drawing.SizeF(IxDest.Rubberband.Dimensions.Width, IxDest.Rubberband.Dimensions.Height)
            Dim MergeRegion As System.Drawing.RectangleF = New System.Drawing.RectangleF(MergeTL, MergeSize)
            process.SetArea(MergeRegion)
            Dim daColor As System.Drawing.Color
            Select Case (comboBoxTransparentColor.SelectedIndex)
                Case 0
                    daColor = System.Drawing.Color.Red
                Case 1
                    daColor = System.Drawing.Color.Green
                Case 2
                    daColor = System.Drawing.Color.Blue
                Case 3
                    daColor = System.Drawing.Color.White
                Case Else
                    daColor = System.Drawing.Color.Black
            End Select
            process.Merge(ImageSource, CType(comboBoxMergeType.SelectedIndex, PegasusImaging.WinForms.ImagXpress8.MergeSize), CType(comboBoxMergeStyle.SelectedIndex, PegasusImaging.WinForms.ImagXpress8.MergeStyle), checkBoxTransparent.Checked, daColor, hScrollBarMax.Value, hScrollBarMin.Value)
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Sub


        Private Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
            Dim iTmp As Integer
            Try
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
            Catch ex As System.NullReferenceException
                PegasusError(ex, lblError)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            Catch ex As System.Exception
                PegasusError(ex, lblError)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            End Try
            If ((iTmp < scrScroll.Maximum) _
                        AndAlso (iTmp > scrScroll.Minimum)) Then
                scrScroll.Value = iTmp
            Else
                iTmp = scrScroll.Value
            End If
            textTextBox.Text = iTmp.ToString(cultNumber)
        End Sub

        Private Sub buttonReloadDestination_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonReloadDestination.Click
            IxDest.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile1)
        End Sub

        Private Sub IxDest_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles IxDest.MouseDown
            IxDest.Rubberband.Enabled = False
            IxDest.Rubberband.Start(New System.Drawing.Point(e.X, e.Y))
            IxDest.Rubberband.Enabled = True
            bSelectionActive = True
        End Sub

        Private Sub IxDest_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles IxDest.MouseMove
            If bSelectionActive Then
                IxDest.Rubberband.Update(New System.Drawing.Point(e.X, e.Y))
            End If
        End Sub

        Private Sub IxDest_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles IxDest.MouseUp
            IxDest.Rubberband.Update(New System.Drawing.Point(e.X, e.Y))
            bSelectionActive = False
        End Sub

        Private Sub menuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuToolbarShow.Click
            If IxDest.Toolbar.Activated = True Then
                menuToolbarShow.Text = "Show"
                IxDest.Toolbar.Activated = False
                IxSource.Toolbar.Activated = False

            Else
                menuToolbarShow.Text = "Hide"
                IxDest.Toolbar.Activated = True
                IxSource.Toolbar.Activated = True
            End If
        End Sub

        Private Sub menuFileOpenSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuFileOpenSource.Click
            Dim strTmp As String = PegasusOpenFile()
            If (strTmp.Length <> 0) Then
                strimageFile2 = strTmp
                Try
                    'clear out the error in case there was an error from a previous operation
                    lblError.Text = ""
                    ImageSource = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile2)
                    IxSource.Image = ImageSource
                Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(eX, lblError)
                End Try
            End If
        End Sub

        Private Sub menuFileOpenDest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuFileOpenDest.Click
            Dim strTmp As String = PegasusOpenFile()
            If (strTmp.Length <> 0) Then
                strimageFile1 = strTmp
                Try
                    'clear out the error in case there was an error from a previous operation
                    lblError.Text = ""
                    IxDest.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile1)
                Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(eX, lblError)
                End Try
            End If
        End Sub

        Private Sub menuFileQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuFileQuit.Click
            Application.Exit()
        End Sub

        Private Sub menuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItem2.Click
            Try
                imagXpress1.AboutBox()
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try
        End Sub
    End Class
End Namespace
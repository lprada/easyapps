'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Public Class Form1
    Inherits System.Windows.Forms.Form


    Private strCurrentDir As System.String
    Private strimagefile As System.String
    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Private prc As PegasusImaging.WinForms.ImagXpress8.Processor
    Dim bSelectionActive As System.Boolean = False
    Dim rbRectStats As System.Drawing.Rectangle
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)
        'This call is required by the Windows Form Designer.
        InitializeComponent()
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (prc Is Nothing) Then
                prc.Dispose()
                prc = Nothing
            End If



            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        'Dispose of the imagXpress1 object and the ImageXView object


        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents cmdCountColors As System.Windows.Forms.Button
    Friend WithEvents lblColorInfo As System.Windows.Forms.Label
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents lblRubberBandInfo As System.Windows.Forms.Label
    Friend WithEvents lblStartPoint As System.Windows.Forms.Label
    Friend WithEvents lblStartPointVal As System.Windows.Forms.Label
    Friend WithEvents lblWidthVal As System.Windows.Forms.Label
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblHeightVal As System.Windows.Forms.Label
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.cmdCountColors = New System.Windows.Forms.Button()
        Me.lblColorInfo = New System.Windows.Forms.Label()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuFileOpen = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuFileQuit = New System.Windows.Forms.MenuItem()
        Me.mnuToolbar = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.lblRubberBandInfo = New System.Windows.Forms.Label()
        Me.lblStartPoint = New System.Windows.Forms.Label()
        Me.lblStartPointVal = New System.Windows.Forms.Label()
        Me.lblWidthVal = New System.Windows.Forms.Label()
        Me.lblWidth = New System.Windows.Forms.Label()
        Me.lblHeight = New System.Windows.Forms.Label()
        Me.lblHeightVal = New System.Windows.Forms.Label()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.ImageXView1.Location = New System.Drawing.Point(176, 96)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(328, 216)
        Me.ImageXView1.TabIndex = 0
        '
        'cmdCountColors
        '
        Me.cmdCountColors.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.cmdCountColors.Location = New System.Drawing.Point(264, 336)
        Me.cmdCountColors.Name = "cmdCountColors"
        Me.cmdCountColors.Size = New System.Drawing.Size(144, 32)
        Me.cmdCountColors.TabIndex = 3
        Me.cmdCountColors.Text = "Count Colors"
        '
        'lblColorInfo
        '
        Me.lblColorInfo.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.lblColorInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblColorInfo.Location = New System.Drawing.Point(16, 280)
        Me.lblColorInfo.Name = "lblColorInfo"
        Me.lblColorInfo.Size = New System.Drawing.Size(128, 112)
        Me.lblColorInfo.TabIndex = 4
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolbar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileOpen, Me.MenuItem3, Me.mnuFileQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Index = 0
        Me.mnuFileOpen.Text = "&Open Image"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuFileQuit
        '
        Me.mnuFileQuit.Index = 2
        Me.mnuFileQuit.Text = "&Quit"
        '
        'mnuToolbar
        '
        Me.mnuToolbar.Index = 1
        Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolbar.Text = "&Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'lblRubberBandInfo
        '
        Me.lblRubberBandInfo.Location = New System.Drawing.Point(16, 96)
        Me.lblRubberBandInfo.Name = "lblRubberBandInfo"
        Me.lblRubberBandInfo.Size = New System.Drawing.Size(96, 16)
        Me.lblRubberBandInfo.TabIndex = 5
        Me.lblRubberBandInfo.Text = "RubberBand Info:"
        '
        'lblStartPoint
        '
        Me.lblStartPoint.Location = New System.Drawing.Point(16, 120)
        Me.lblStartPoint.Name = "lblStartPoint"
        Me.lblStartPoint.Size = New System.Drawing.Size(72, 24)
        Me.lblStartPoint.TabIndex = 6
        Me.lblStartPoint.Text = "Start Point:"
        '
        'lblStartPointVal
        '
        Me.lblStartPointVal.Location = New System.Drawing.Point(96, 120)
        Me.lblStartPointVal.Name = "lblStartPointVal"
        Me.lblStartPointVal.Size = New System.Drawing.Size(72, 16)
        Me.lblStartPointVal.TabIndex = 7
        '
        'lblWidthVal
        '
        Me.lblWidthVal.Location = New System.Drawing.Point(96, 152)
        Me.lblWidthVal.Name = "lblWidthVal"
        Me.lblWidthVal.Size = New System.Drawing.Size(64, 16)
        Me.lblWidthVal.TabIndex = 11
        '
        'lblWidth
        '
        Me.lblWidth.Location = New System.Drawing.Point(16, 152)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(64, 24)
        Me.lblWidth.TabIndex = 12
        Me.lblWidth.Text = "Width:"
        '
        'lblHeight
        '
        Me.lblHeight.Location = New System.Drawing.Point(16, 184)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(64, 16)
        Me.lblHeight.TabIndex = 13
        Me.lblHeight.Text = "Height:"
        '
        'lblHeightVal
        '
        Me.lblHeightVal.Location = New System.Drawing.Point(96, 184)
        Me.lblHeightVal.Name = "lblHeightVal"
        Me.lblHeightVal.Size = New System.Drawing.Size(72, 16)
        Me.lblHeightVal.TabIndex = 14
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using the ColorCount method to determine the ratio of black to white pixels in " & _
        "the image.", "", "Instructions: Select a portion of the image and click the Count Colors button."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(752, 69)
        Me.lstInfo.TabIndex = 15
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(512, 136)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(256, 95)
        Me.lstStatus.TabIndex = 32
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(512, 104)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(160, 24)
        Me.lblLoadStatus.TabIndex = 31
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblError
        '
        Me.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblError.Location = New System.Drawing.Point(520, 280)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(192, 96)
        Me.lblError.TabIndex = 30
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(512, 248)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(168, 24)
        Me.lblLastError.TabIndex = 29
        Me.lblLastError.Text = "Last Error:"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = -1
        Me.MenuItem1.Text = "&Open"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(784, 401)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstStatus, Me.lblLoadStatus, Me.lblError, Me.lblLastError, Me.lstInfo, Me.lblHeightVal, Me.lblHeight, Me.lblWidth, Me.lblWidthVal, Me.lblStartPointVal, Me.lblStartPoint, Me.lblRubberBandInfo, Me.lblColorInfo, Me.cmdCountColors, Me.ImageXView1})
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ColorCount"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/

    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo

    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region


    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If

    End Sub

    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)

            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try


        Try

            'here we set the current directory and image so that the file open dialog box works well
            strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString()
            strimagefile = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\barcode.pcx")
            strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try



        rbRectStats = New System.Drawing.Rectangle()
    End Sub

    Private Sub cmdQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Application.Exit()
    End Sub

    Private Sub ImageXView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles ImageXView1.MouseDown
        ImageXView1.Rubberband.Enabled = False
        ImageXView1.Rubberband.Start(New System.Drawing.Point(e.X, e.Y))

        ImageXView1.Rubberband.Enabled = True
        bSelectionActive = True
        lblStartPointVal.Text = ImageXView1.Rubberband.Dimensions.X.ToString() + ","
        lblStartPointVal.Text = lblStartPointVal.Text + ImageXView1.Rubberband.Dimensions.Y.ToString()

    End Sub

    Private Sub ImageXView1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles ImageXView1.MouseUp
        ImageXView1.Rubberband.Update(New System.Drawing.Point(e.X, e.Y))
        bSelectionActive = False


    End Sub

    Private Sub ImageXView1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles ImageXView1.MouseMove
        If bSelectionActive Then
            ImageXView1.Rubberband.Update(New System.Drawing.Point(e.X, e.Y))

            lblWidthVal.Text = ImageXView1.Rubberband.Dimensions.Width.ToString()
            lblHeightVal.Text = ImageXView1.Rubberband.Dimensions.Height.ToString()
        End If
    End Sub

    Private Sub cmdLoadImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sTmp As System.String
        sTmp = PegasusOpenFile()
        If sTmp <> "" Then

        End If
    End Sub


    Private Sub cmdCountColors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCountColors.Click

        Dim cntBlack, cntWhite, cntTotal As Integer

        Try

            prc = New PegasusImaging.WinForms.ImagXpress8.Processor(ImageXView1.Image)
            If (ImageXView1.Rubberband.Enabled) Then

                Dim MergeTL As System.Drawing.PointF = New System.Drawing.PointF(ImageXView1.Rubberband.Dimensions.X, ImageXView1.Rubberband.Dimensions.Y)
                Dim MergeSize As System.Drawing.SizeF = New System.Drawing.SizeF(ImageXView1.Rubberband.Dimensions.Width, ImageXView1.Rubberband.Dimensions.Height)
                Dim MergeRegion As System.Drawing.RectangleF = New System.Drawing.RectangleF(MergeTL, MergeSize)

                prc.SetArea(MergeRegion)

                cntTotal = ImageXView1.Rubberband.Dimensions.Width * ImageXView1.Rubberband.Dimensions.Height
            Else
                cntTotal = ImageXView1.Image.ImageXData.Width * ImageXView1.Image.ImageXData.Height
            End If

            cntBlack = prc.ColorCount(System.Drawing.Color.FromArgb(0, 0, 0))
            cntWhite = prc.ColorCount(System.Drawing.Color.FromArgb(255, 255, 255))

            If cntTotal > 0 Then
                Dim sTmp As System.String
                sTmp = "Black Pixels: " & cntBlack.ToString() & Chr(10)
                sTmp = sTmp & "White Pixels: " & cntWhite.ToString() & Chr(10)
                sTmp = sTmp & "Total Pixels: " & cntTotal.ToString() & Chr(10)
                sTmp = sTmp & "Percent Black: " & (cntBlack / cntTotal * 100).ToString() & Chr(10)
                sTmp = sTmp & "Percent White: " & (cntWhite / cntTotal * 100).ToString() & Chr(10)
                lblColorInfo.Text = sTmp
            Else
                lblColorInfo.Text = "No Image"
            End If
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then

            'clear out the error before the next image load
            lblError.Text = ""

            Try
                strimagefile = strtemp
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimagefile)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblError)

            End Try
        End If

    End Sub

    Private Sub StatusChanged(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)
        lstStatus.Items.Add(e.Status.ToString(cultNumber))
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub

    Private Sub StatusChanged(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If e.IsComplete Then
            lstStatus.Items.Add((e.TotalBytes + " Bytes Completed Loading."))
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub



    Private Sub mnuFileQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileQuit.Click
        Application.Exit()
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click

        If ImageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False

        Else
            mnuToolbarShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True
        End If
    End Sub
End Class

/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Binarize
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem mnuFileFile;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.String strCurrentDir;
		private System.String 	strCurrentImage;
		private System.String strImagePath;
		private System.Boolean bImageLoaded;
		private System.Windows.Forms.ComboBox cmbBlur;
		private System.Windows.Forms.HScrollBar hsLocalContrast;
		private System.Windows.Forms.HScrollBar hsEccentricity;
		private System.Windows.Forms.HScrollBar hsPitch;
		private System.Windows.Forms.HScrollBar hsAngle;
		private System.Windows.Forms.HScrollBar hsLowThresh;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbMode;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button cmdProcess;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.LoadOptions loLoadOptions;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.TextBox textPitch;
		private System.Windows.Forms.TextBox textEccentricity;
		private System.Windows.Forms.TextBox textLocalContrast;
		private System.Windows.Forms.Label lblLoadStatus;
		private System.Windows.Forms.TextBox textLowThresh;
		private System.Windows.Forms.TextBox textHighThresh;
		private System.Windows.Forms.TextBox textAngle;
		private System.Windows.Forms.HScrollBar hsHighThresh;
		private System.Windows.Forms.ListBox lstStatus;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private System.Windows.Forms.Label labelError;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;
		private System.Windows.Forms.ListBox lstDesc;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			
			//The UnlockControl function must be called to distribute the runtime**
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				//  Don't forget to dispose IX
				// 
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (!(imageXView2 == null)) 
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}

				if (components != null) 
				{
					components.Dispose();
					
				}
			}
			
			

			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mnuFile = new System.Windows.Forms.MainMenu();
			this.mnuFileFile = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mnuFileQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.cmbBlur = new System.Windows.Forms.ComboBox();
			this.hsLocalContrast = new System.Windows.Forms.HScrollBar();
			this.hsEccentricity = new System.Windows.Forms.HScrollBar();
			this.hsPitch = new System.Windows.Forms.HScrollBar();
			this.hsAngle = new System.Windows.Forms.HScrollBar();
			this.hsHighThresh = new System.Windows.Forms.HScrollBar();
			this.hsLowThresh = new System.Windows.Forms.HScrollBar();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbMode = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.cmdProcess = new System.Windows.Forms.Button();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.lblLastError = new System.Windows.Forms.Label();
			this.labelError = new System.Windows.Forms.Label();
			this.textPitch = new System.Windows.Forms.TextBox();
			this.textEccentricity = new System.Windows.Forms.TextBox();
			this.textLocalContrast = new System.Windows.Forms.TextBox();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.textLowThresh = new System.Windows.Forms.TextBox();
			this.textHighThresh = new System.Windows.Forms.TextBox();
			this.textAngle = new System.Windows.Forms.TextBox();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lstDesc = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// mnuFile
			// 
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuFileFile,
																					this.mnuToolbar,
																					this.mnuAbout});
			// 
			// mnuFileFile
			// 
			this.mnuFileFile.Index = 0;
			this.mnuFileFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuFileOpen,
																						this.menuItem3,
																						this.mnuFileQuit});
			this.mnuFileFile.Text = "&File";
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Index = 0;
			this.mnuFileOpen.Text = "&Open";
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "-";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 2;
			this.mnuFileQuit.Text = "&Quit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "&Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "&Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// cmbBlur
			// 
			this.cmbBlur.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.cmbBlur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbBlur.ItemHeight = 13;
			this.cmbBlur.Items.AddRange(new object[] {
														 "None",
														 "Gaussian",
														 "Smart"});
			this.cmbBlur.Location = new System.Drawing.Point(72, 456);
			this.cmbBlur.Name = "cmbBlur";
			this.cmbBlur.Size = new System.Drawing.Size(192, 21);
			this.cmbBlur.TabIndex = 31;
			// 
			// hsLocalContrast
			// 
			this.hsLocalContrast.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.hsLocalContrast.LargeChange = 1;
			this.hsLocalContrast.Location = new System.Drawing.Point(360, 432);
			this.hsLocalContrast.Maximum = 255;
			this.hsLocalContrast.Name = "hsLocalContrast";
			this.hsLocalContrast.Size = new System.Drawing.Size(338, 16);
			this.hsLocalContrast.TabIndex = 30;
			this.hsLocalContrast.Value = 163;
			this.hsLocalContrast.ValueChanged += new System.EventHandler(this.hsLocalContrast_ValueChanged);
			// 
			// hsEccentricity
			// 
			this.hsEccentricity.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.hsEccentricity.LargeChange = 1;
			this.hsEccentricity.Location = new System.Drawing.Point(360, 408);
			this.hsEccentricity.Maximum = 255;
			this.hsEccentricity.Minimum = -255;
			this.hsEccentricity.Name = "hsEccentricity";
			this.hsEccentricity.Size = new System.Drawing.Size(338, 16);
			this.hsEccentricity.TabIndex = 29;
			this.hsEccentricity.Value = 255;
			this.hsEccentricity.ValueChanged += new System.EventHandler(this.hsEccentricity_ValueChanged);
			// 
			// hsPitch
			// 
			this.hsPitch.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.hsPitch.Enabled = false;
			this.hsPitch.LargeChange = 1;
			this.hsPitch.Location = new System.Drawing.Point(360, 384);
			this.hsPitch.Maximum = 32;
			this.hsPitch.Minimum = 1;
			this.hsPitch.Name = "hsPitch";
			this.hsPitch.Size = new System.Drawing.Size(338, 16);
			this.hsPitch.TabIndex = 28;
			this.hsPitch.Value = 1;
			this.hsPitch.ValueChanged += new System.EventHandler(this.hsPitch_Changed);
			// 
			// hsAngle
			// 
			this.hsAngle.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.hsAngle.LargeChange = 1;
			this.hsAngle.Location = new System.Drawing.Point(104, 432);
			this.hsAngle.Maximum = 360;
			this.hsAngle.Name = "hsAngle";
			this.hsAngle.Size = new System.Drawing.Size(128, 16);
			this.hsAngle.TabIndex = 27;
			this.hsAngle.ValueChanged += new System.EventHandler(this.hsAngle_Changed);
			// 
			// hsHighThresh
			// 
			this.hsHighThresh.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.hsHighThresh.LargeChange = 1;
			this.hsHighThresh.Location = new System.Drawing.Point(104, 408);
			this.hsHighThresh.Maximum = 255;
			this.hsHighThresh.Name = "hsHighThresh";
			this.hsHighThresh.Size = new System.Drawing.Size(128, 16);
			this.hsHighThresh.TabIndex = 25;
			this.hsHighThresh.Value = 126;
			this.hsHighThresh.ValueChanged += new System.EventHandler(this.hsHighTresh_Changed);
			// 
			// hsLowThresh
			// 
			this.hsLowThresh.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.hsLowThresh.LargeChange = 1;
			this.hsLowThresh.Location = new System.Drawing.Point(104, 384);
			this.hsLowThresh.Maximum = 255;
			this.hsLowThresh.Name = "hsLowThresh";
			this.hsLowThresh.Size = new System.Drawing.Size(128, 16);
			this.hsLowThresh.TabIndex = 24;
			this.hsLowThresh.Value = 86;
			this.hsLowThresh.ValueChanged += new System.EventHandler(this.hsLowThresh_Changed);
			
			// 
			// label8
			// 
			this.label8.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label8.Location = new System.Drawing.Point(8, 456);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(56, 16);
			this.label8.TabIndex = 23;
			this.label8.Text = "Blur Type";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label7
			// 
			this.label7.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label7.Location = new System.Drawing.Point(280, 432);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(80, 16);
			this.label7.TabIndex = 22;
			this.label7.Text = "Local Contrast";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label6.Location = new System.Drawing.Point(280, 408);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(80, 16);
			this.label6.TabIndex = 21;
			this.label6.Text = "Eccentricity";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			this.label5.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label5.Location = new System.Drawing.Point(280, 384);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 16);
			this.label5.TabIndex = 20;
			this.label5.Text = "Grid Pitch";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label4
			// 
			this.label4.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label4.Location = new System.Drawing.Point(16, 432);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(88, 16);
			this.label4.TabIndex = 19;
			this.label4.Text = "Grid Angle";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label2.Location = new System.Drawing.Point(16, 408);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 16);
			this.label2.TabIndex = 17;
			this.label2.Text = "High Threshold";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label1
			// 
			this.label1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label1.Location = new System.Drawing.Point(16, 384);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 16);
			this.label1.TabIndex = 16;
			this.label1.Text = "Low Threshold";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cmbMode
			// 
			this.cmbMode.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.cmbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbMode.Items.AddRange(new object[] {
														 "QuickText",
														 "HalfTone"});
			this.cmbMode.Location = new System.Drawing.Point(320, 456);
			this.cmbMode.Name = "cmbMode";
			this.cmbMode.Size = new System.Drawing.Size(410, 21);
			this.cmbMode.TabIndex = 32;
			this.cmbMode.SelectedIndexChanged += new System.EventHandler(this.cmbMode_SelectedIndexChanged);
			// 
			// label9
			// 
			this.label9.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label9.Location = new System.Drawing.Point(280, 456);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(40, 16);
			this.label9.TabIndex = 33;
			this.label9.Text = "Mode:";
			// 
			// cmdProcess
			// 
			this.cmdProcess.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdProcess.Location = new System.Drawing.Point(570, 344);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(160, 24);
			this.cmdProcess.TabIndex = 41;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblLastError.Location = new System.Drawing.Point(570, 224);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(64, 16);
			this.lblLastError.TabIndex = 43;
			this.lblLastError.Text = "Last Error:";
			// 
			// labelError
			// 
			this.labelError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.labelError.Location = new System.Drawing.Point(570, 248);
			this.labelError.Name = "labelError";
			this.labelError.Size = new System.Drawing.Size(160, 88);
			this.labelError.TabIndex = 44;
			// 
			// textPitch
			// 
			this.textPitch.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.textPitch.Location = new System.Drawing.Point(698, 376);
			this.textPitch.Name = "textPitch";
			this.textPitch.Size = new System.Drawing.Size(32, 21);
			this.textPitch.TabIndex = 45;
			this.textPitch.Text = "1";
			this.textPitch.TextChanged += new System.EventHandler(this.textPitch_TextChanged);
			// 
			// textEccentricity
			// 
			this.textEccentricity.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.textEccentricity.Location = new System.Drawing.Point(698, 400);
			this.textEccentricity.Name = "textEccentricity";
			this.textEccentricity.Size = new System.Drawing.Size(32, 21);
			this.textEccentricity.TabIndex = 46;
			this.textEccentricity.Text = "0";
			this.textEccentricity.TextChanged += new System.EventHandler(this.textEccentricity_TextChanged);
			// 
			// textLocalContrast
			// 
			this.textLocalContrast.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.textLocalContrast.Location = new System.Drawing.Point(698, 424);
			this.textLocalContrast.Name = "textLocalContrast";
			this.textLocalContrast.Size = new System.Drawing.Size(32, 21);
			this.textLocalContrast.TabIndex = 47;
			this.textLocalContrast.Text = "0";
			this.textLocalContrast.TextChanged += new System.EventHandler(this.textLocalContrast_TextChanged);
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(570, 80);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(72, 16);
			this.lblLoadStatus.TabIndex = 48;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(570, 96);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(160, 108);
			this.lstStatus.TabIndex = 49;
			// 
			// textLowThresh
			// 
			this.textLowThresh.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.textLowThresh.Location = new System.Drawing.Point(232, 376);
			this.textLowThresh.Name = "textLowThresh";
			this.textLowThresh.Size = new System.Drawing.Size(32, 21);
			this.textLowThresh.TabIndex = 50;
			this.textLowThresh.Text = "0";
			this.textLowThresh.TextChanged += new System.EventHandler(this.textLowThresh_TextChanged);
			// 
			// textHighThresh
			// 
			this.textHighThresh.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.textHighThresh.Location = new System.Drawing.Point(232, 400);
			this.textHighThresh.Name = "textHighThresh";
			this.textHighThresh.Size = new System.Drawing.Size(32, 21);
			this.textHighThresh.TabIndex = 51;
			this.textHighThresh.Text = "0";
			this.textHighThresh.TextChanged += new System.EventHandler(this.textHighThresh_TextChanged);
			// 
			// textAngle
			// 
			this.textAngle.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.textAngle.Location = new System.Drawing.Point(232, 424);
			this.textAngle.Name = "textAngle";
			this.textAngle.Size = new System.Drawing.Size(32, 21);
			this.textAngle.TabIndex = 52;
			this.textAngle.Text = "0";
			this.textAngle.TextChanged += new System.EventHandler(this.textAngle_TextChanged);
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView1.AutoScroll = true;
			this.imageXView1.Location = new System.Drawing.Point(16, 88);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(266, 284);
			this.imageXView1.TabIndex = 53;
			// 
			// imageXView2
			// 
			this.imageXView2.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView2.AutoScroll = true;
			this.imageXView2.Location = new System.Drawing.Point(288, 88);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(264, 288);
			this.imageXView2.TabIndex = 54;
			// 
			// lstDesc
			// 
			this.lstDesc.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstDesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstDesc.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1) Using the Binarize method of the Processor class to modify the color palette o" +
														 "f the image by setting it to ",
														 "   2 colors (black and white)."});
			this.lstDesc.Location = new System.Drawing.Point(16, 16);
			this.lstDesc.Name = "lstDesc";
			this.lstDesc.Size = new System.Drawing.Size(712, 56);
			this.lstDesc.TabIndex = 55;
			// 
			// FormMain
			// 
			this.AcceptButton = this.cmdProcess;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(746, 489);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstDesc,
																		  this.imageXView2,
																		  this.imageXView1,
																		  this.textAngle,
																		  this.textHighThresh,
																		  this.textLowThresh,
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.textLocalContrast,
																		  this.textEccentricity,
																		  this.textPitch,
																		  this.labelError,
																		  this.lblLastError,
																		  this.cmdProcess,
																		  this.label9,
																		  this.cmbMode,
																		  this.cmbBlur,
																		  this.hsLocalContrast,
																		  this.hsEccentricity,
																		  this.hsPitch,
																		  this.hsAngle,
																		  this.hsHighThresh,
																		  this.hsLowThresh,
																		  this.label8,
																		  this.label7,
																		  this.label6,
																		  this.label5,
																		  this.label4,
																		  this.label2,
																		  this.label1});
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Menu = this.mnuFile;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Binarize";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void hsLowThresh_Changed(object sender, System.EventArgs e)
		{
			hsHighThresh.Minimum = hsLowThresh.Value;
			textLowThresh.Text = hsLowThresh.Value.ToString(cultNumber);
			textHighThresh.Text = hsHighThresh.Value.ToString(cultNumber);
		}

		private void hsHighTresh_Changed(object sender, System.EventArgs e)
		{
			textHighThresh.Text = hsHighThresh.Value.ToString(cultNumber);
		}

		private void hsAngle_Changed(object sender, System.EventArgs e)
		{
			textAngle.Text = hsAngle.Value.ToString(cultNumber);		
		}

		private void hsPitch_Changed(object sender, System.EventArgs e)
		{
			textPitch.Text = hsPitch.Value.ToString(cultNumber);
		}

		private void hsEccentricity_ValueChanged(object sender, System.EventArgs e)
		{
			textEccentricity.Text = hsEccentricity.Value.ToString(cultNumber);
		}

		private void hsLocalContrast_ValueChanged(object sender, System.EventArgs e)
		{
			textLocalContrast.Text = hsLocalContrast.Value.ToString(cultNumber);		
		}

		private void frmMain_Load(object sender, System.EventArgs e)
		{
			
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

			//Set the default directory to the common images directory
			strCurrentDir = System.IO.Path.Combine(System.Environment.CurrentDirectory,strCommonImagesDirectory);

			//Create a new load options object so we can recieve events from the images we load
			loLoadOptions = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();

			//this is where events are assigned. This happens before the file gets loaded.
			PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
			PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );

			System.String strCurrentdir = System.IO.Directory.GetCurrentDirectory ().ToString ();
			
			strImagePath = System.IO.Path.Combine (strCurrentdir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\binarize.jpg");
			//Load the image
			imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath);
			imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath);
			bImageLoaded = true;
				
	
			cmbBlur.SelectedIndex = 0;
			cmbMode.SelectedIndex = 0;

			textLowThresh.Text = hsLowThresh.Value.ToString(cultNumber);
			textHighThresh.Text = hsHighThresh.Value.ToString(cultNumber);
			textLocalContrast.Text = hsLocalContrast.Value.ToString(cultNumber);	


			cmdProcess_Click(sender, e);

		}

		private void ModeChange(bool bMode)
		{
			hsLowThresh.Enabled = bMode;
			hsHighThresh.Enabled = bMode;
			hsLocalContrast.Enabled = bMode;
			hsAngle.Enabled = true;
			hsPitch.Enabled = true;
			hsEccentricity.Enabled = true;
		}

		private void cmbMode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbMode.SelectedIndex == 0) 
			{
				//ModeChange(true);
				hsLowThresh.Enabled = true;
				hsHighThresh.Enabled = true;
				hsLocalContrast.Enabled = true;
				hsAngle.Enabled = false;
				hsPitch.Enabled = false;
				hsEccentricity.Enabled = false;
			} 
			else 
			{
				hsLowThresh.Enabled = false;
				hsHighThresh.Enabled = false;
				hsLocalContrast.Enabled = false;
				hsAngle.Enabled = true;
				hsPitch.Enabled = true;
				hsEccentricity.Enabled = true;
			}
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			if (bImageLoaded) 
			{
				try 
				{
					PegasusImaging.WinForms.ImagXpress8.Processor prc = new PegasusImaging.WinForms.ImagXpress8.Processor(imageXView2.Image);
					PegasusImaging.WinForms.ImagXpress8.BinarizeMode bMode = ((PegasusImaging.WinForms.ImagXpress8.BinarizeMode )cmbMode.SelectedIndex);
					PegasusImaging.WinForms.ImagXpress8.BinarizeBlur bBlur = ((PegasusImaging.WinForms.ImagXpress8.BinarizeBlur )cmbBlur.SelectedIndex);
					prc.Binarize(bMode,hsLowThresh.Value,hsHighThresh.Value,hsAngle.Value,hsPitch.Value,hsEccentricity.Value,hsLocalContrast.Value,bBlur);
					imageXView2.Image = prc.Image;
					prc = null;
					
	
				} 
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
				{
					PegasusError(ex,labelError);
				}
			}
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			System.String sFileName = PegasusOpenFile();
			if (sFileName.Length != 0) 
			{
				strCurrentImage = sFileName;
				try 
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(sFileName,loLoadOptions);
					imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest);

					imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(sFileName,loLoadOptions);
					imageXView2.ZoomToFit(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest);
				
				
					// clear out the error in case there was an error from a previous operation
					labelError.Text = "";
				} 
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
				{
					PegasusError(ex,labelError);
				}
				bImageLoaded = true;
			}
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,labelError);
			}
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			mnuToolbarShow.Text = (imageXView1.Toolbar.Activated)? "Show":"Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,labelError);
			}
		}

		private void textPitch_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsPitch,textPitch);
		}

		private void textLocalContrast_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsLocalContrast,textLocalContrast);
		}

		private void textLowThresh_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsLowThresh,textLowThresh);
		}

		private void textHighThresh_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsHighThresh,textHighThresh);
		}

		private void textAngle_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsAngle,textAngle);
		}

		private void textEccentricity_TextChanged(object sender, System.EventArgs e)
		{
			PegasusTextBoxScrollBinder(hsEccentricity,textEccentricity);
		}

		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		


		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,labelError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,labelError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion


		

	}
}

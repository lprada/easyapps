/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Text;
using PegasusImaging.WinForms.ImagXpress8;

namespace ExifTags
{
public enum ExifTagNumber : int 
{
	TagNone = 0x0,
	TagArtist = 0x13b,
	TagBitsPerSample = 0x102,
	TagCellLength = 0x109,
	TagCellWidth = 0x108,
	TagColorMap = 0x140,
	TagCompression = 0x103,
	TagCopyright = 0x133,
	TagDateTime = 0x132,
	TagDocumentName = 0x10d,
	TagDotRange = 0x150,
	TagExifAperture = 0x9202,
	TagExifBrightness = 0x9203,
	TagExifCfaPattern = 0xA302,
	TagExifColorSpace = 0xA001,
	TagExifCompBPP = 0x9102,
	TagExifCompConfig = 0x9101,
	TagExifDTDigitized = 0x9004,
	TagExifDTDigSS = 0x9292 ,
	TagExifDTOrig = 0x9003 ,
	TagExifDTOrigSS = 0x9291 ,
	TagExifDTSubsec = 0x9290 ,
	TagExifExposureBias = 0x9204,
	TagExifExposureIndex = 0X9215,
	TagExifExposureProg = 0x8822,
	TagExifExposureTime = 0x829A,
	TagExifFileSource = 0xA300,
	TagExifFlash = 0x9209,
	TagExifFlashEnergy = 0X920B,
	TagExifFNumber = 0x829D,
	TagExifFocalLength = 0x920A,
	TagExifFocalResUnit = 0xA210 ,
	TagExifFocalXRes = 0xA20E ,
	TagExifFocalYRes = 0xA20F ,
	TagExifFPXVer = 0xA000,
	TagExifIFD = 0x8769,
	TagExifImageHistory = 0X9213,
	TagExifImageNumber = 0X9211,
	TagExifInterlace = 0X8829,
	TagExifInterop = 0xA005,
	TagExifISOSpeed = 0x8827,
	TagExifLightSource = 0x9208,
	TagExifMakerNote = 0x927C,
	TagExifMaxAperture = 0x9205,
	TagExifMeteringMode = 0x9207,
	TagExifNoise = 0X920D,
	TagExifOECF = 0x8828,
	TagExifPixXDim = 0xA002,
	TagExifPixYDim = 0xA003,
	TagExifRelatedWav = 0xA004 ,
	TagExifSceneType = 0xA301,
	TagExifSecurityClassification = 0X9212,
	TagExifSelfTimerMode = 0X882B,
	TagExifSensingMethod = 0xA217,
	TagExifShutterSpeed = 0x9201,
	TagExifSpatialFR = 0xA20C ,
	TagExifSpatialFrequencyResponse = 0X920C,
	TagExifSpectralSense = 0x8824,
	TagExifSubjectDist = 0x9206,
	TagExifSubjectLoc = 0xA214,
	TagExifSubjectLocation = 0X9214,
	TagExifTiff_EPStandardID = 0X9216,
	TagExifTimeZoneOffset = 0X882A,
	TagExifUserComment = 0x9286,
	TagExifVer = 0x9000,
	TagExtraSamples = 0x152,
	TagFillOrder = 0x10a,
	TagFreeByteCounts = 0x121,
	TagFreeOffsets = 0x120,
	TagGrayResponseCurve = 0x123,
	TagGrayResponseUnit = 0x122,
	TagHalftoneHints = 0x141,
	TagHostComputer = 0x13c,
	TagImageDescription = 0x10e,
	TagImageLength = 0x101,
	TagImageWidth = 0x100,
	TagInkNames = 0x14d,
	TagInkSet = 0x14c,
	TagJpegACTables = 0x209,
	TagJpegDCTables = 0x208,
	TagJpegDCTTables = 0x208,
	TagJpegInterchangeFormat = 0x201,
	TagJpegInterchangeFormatLength = 0x202,
	TagJpegLosslessPredictors = 0x205,
	TagJpegPointTransforms = 0x206,
	TagJpegProc = 0x200,
	TagJpegQTables = 0x207,
	TagJpegRestartInterval = 0x203,
	TagMake = 0x10f,
	TagMaxSampleValue = 0x119,
	TagMinSampleValue = 0x118,
	TagModel = 0x110,
	TagNewSubFileType = 0xfe,
	TagNumberOfInks = 0x14e,
	TagOrientation = 0x112,
	TagPageName = 0x11d,
	TagPageNumber = 0x129,
	TagPhotometricInterpretation = 0x106,
	TagPlanarConfiguration = 0x11c,
	TagPredictor = 0x13d,
	TagPrimaryChromaticities = 0x13f,
	TagReferenceBlackWhite = 0x214,
	TagResolutionUnit = 0x128,
	TagRowsPerStrip = 0x116,
	TagSampleFormat = 0x153,
	TagSamplesPerPixel = 0x115,
	TagSMaxSampleValue = 0x155,
	TagSMinSampleValue = 0x154,
	TagSoftware = 0x131,
	TagStripByteCounts = 0x117,
	TagStripOffsets = 0x111,
	TagSubFileType = 0xff,
	Tag4Options = 0x124,
	Tag6Options = 0x125,
	TagTargetPrinter = 0x151,
	TagThresholding = 0x107,
	TagFileByteCounts = 0x145,
	TagFileLength = 0x143,
	TagFileOffsets = 0x144,
	TagFileWidth = 0x142,
	TagTransferFunction = 0x12d,
	TagTransferRange = 0x156,
	TagWhitePoint = 0x13e,
	TagXPosition = 0x11e,
	TagXResolution = 0x11a,
	TagYCbCrCoefficients = 0x211,
	TagYCbCrPositioning = 0x213,
	TagYCbCrSubSampling = 0x212,
	TagYPosition = 0x11f,
	TagYResolution = 0x11b
};
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class ExifTags : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button cmdGet;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		/// 

		static string stringTag_ExifPixYDim       = ("ExifPixYDim");
		static string stringTag_Artist            = ("Artist");
		static string stringTag_BitsPerSample     = ("BitsPerSample");
		static string stringTag_CellLength        = ("CellLength");
		static string stringTag_CellWidth         = ("CellWidth");
		static string stringTag_ColorMap          = ("ColorMap");
		static string stringTag_Compression       = ("Compression");
		static string stringTag_Copyright         = ("Copyright");
		static string stringTag_DateTime          = ("DateTime");
		static string stringTag_DocumentName      = ("DocumentName");
		static string stringTag_DotRange          = ("DotRange");
		static string stringTag_ExifAperture      = ("ExifAperture");
		static string stringTag_ExifBrightness    = ("ExifBrightness");
		static string stringTag_ExifCfaPattern    = ("ExifCfaPattern");
		static string stringTag_ExifColorSpace    = ("ExifColorSpace");
		static string stringTag_ExifCompBPP       = ("ExifCompBPP");
		static string stringTag_ExifCompConfig    = ("ExifCompConfig");
		static string stringTag_ExifDTDigitized   = ("ExifDTDigitized");
		static string stringTag_ExifDTDigSS       = ("ExifDTDigSS");
		static string stringTag_ExifDTOrig        = ("ExifDTOrig");
		static string stringTag_ExifDTOrigSS      = ("ExifDTOrigSS");
		static string stringTag_ExifDTSubsec      = ("ExifDTSubsec");
		static string stringTag_ExifExposureBias  = ("ExifExposureBias");
		static string stringTag_ExifExposureIndex = ("ExifExposureIndex");
		static string stringTag_ExifExposureProg  = ("ExifExposureProg");
		static string stringTag_ExifExposureTime  = ("ExifExposureTime");
		static string stringTag_ExifFileSource    = ("ExifFileSource");
		static string stringTag_ExifFlash         = ("ExifFlash");
		static string stringTag_ExifFlashEnergy   = ("ExifFlashEnergy");
		static string stringTag_ExifFNumber       = ("ExifFNumber");
		static string stringTag_ExifFocalLength   = ("ExifFocalLength");
		static string stringTag_ExifFocalResUnit  = ("ExifFocalResUnit");
		static string stringTag_ExifFocalXRes     = ("ExifFocalXRes");
		static string stringTag_ExifFocalYRes     = ("ExifFocalYRes");
		static string stringTag_ExifFPXVer        = ("ExifFPXVer");
		static string stringTag_ExifImageHistory  = ("ExifImageHistory");
		static string stringTag_ExifImageNumber   = ("ExifImageNumber");
		static string stringTag_ExifInterlace     = ("ExifInterlace");
		static string stringTag_ExifInterop       = ("ExifInterop");
		static string stringTag_ExifISOSpeed      = ("ExifISOSpeed");
		static string stringTag_ExifLightSource   = ("ExifLightSource");
		static string stringTag_ExifMakerNote     = ("ExifMakerNote");
		static string stringTag_ExifMaxAperture   = ("ExifMaxAperture");
		static string stringTag_ExifMeteringMode  = ("ExifMeteringMode");
		static string stringTag_ExifNoise         = ("ExifNoise");
		static string stringTag_ExifOECF          = ("ExifOECF");
		static string stringTag_ExifPixXDim       = ("ExifPixXDim");
		static string stringTag_ExifRelatedWav    = ("ExifRelatedWav");
		static string stringTag_ExifSceneType     = ("ExifSceneType");
		static string stringTag_ExifSecurityClassification = ("ExifSecurityClassification");
		static string stringTag_ExifSelfTimerMode = ("ExifSelfTimerMode");
		static string stringTag_ExifSensingMethod = ("ExifSensingMethod");
		static string stringTag_ExifShutterSpeed = ("ExifShutterSpeed");
		static string stringTag_ExifSpatialFR     = ("ExifSpatialFR");
		static string stringTag_ExifSpectralSense = ("ExifSpectralSense");
		static string stringTag_ExifSubjectDist   = ("ExifSubjectDist");
		static string stringTag_ExifSubjectLoc    = ("ExifSubjectLoc");
		static string stringTag_ExifTiff_EPStandardID = ("ExifTiff_EPStandardID");
		static string stringTag_ExifTimeZoneOffset = ("ExifTimeZoneOffset");
		static string stringTag_ExifUserComment   = ("ExifUserComment");
		static string stringTag_ExifVer           = ("ExifVer");
		static string stringTag_ExtraSamples      = ("ExtraSamples");
		static string stringTag_FillOrder         = ("FillOrder");
		static string stringTag_FreeByteCounts    = ("FreeByteCounts");
		static string stringTag_FreeOffsets       = ("FreeOffsets");
		static string stringTag_GrayResponseCurve = ("GrayResponseCurve");
		static string stringTag_GrayResponseUnit  = ("GrayResponseUnit");
		static string stringTag_HalftoneHints     = ("HalftoneHints");
		static string stringTag_HostComputer      = ("HostComputer");
		static string stringTag_ImageDescription  = ("ImageDescription");
		static string stringTag_ImageLength       = ("ImageLength");
		static string stringTag_ImageWidth        = ("ImageWidth");
		static string stringTag_InkNames          = ("InkNames");
		static string stringTag_InkSet            = ("InkSet");
		static string stringTag_jpegACTables      = ("jpegACTables");

		static string stringTag_jpegDCTTables     = ("jpegDCTTables");
		static string stringTag_jpegInterchangeFormat = ("jpegInterchangeFormat");
		static string stringTag_jpegInterchangeFormatLength = ("jpegInterchangeFormatLength");
		static string stringTag_jpegLosslessPredictors = ("jpegLosslessPredictors");
		static string stringTag_jpegPointTransforms = ("jpegPointTransforms");
		static string stringTag_jpegProc          = ("jpegProc");
		static string stringTag_jpegQTables       = ("jpegQTables");
		static string stringTag_jpegRestartInterval = ("jpegRestartInterval");
		static string stringTag_Make              = ("Make");
		static string stringTag_MaxSampleValue    = ("MaxSampleValue");
		static string stringTag_MinSampleValue    = ("MinSampleValue");
		static string stringTag_Model             = ("Model");
		static string stringTag_NewSubFileType    = ("NewSubFileType");
		static string stringTag_NumberOfInks      = ("NumberOfInks");
		static string stringTag_Orientation       = ("Orientation");
		static string stringTag_PageName          = ("PageName");
		static string stringTag_PageNumber        = ("PageNumber");
		static string stringTag_PhotometricInterpretation = ("PhotometricInterpretation");
		static string stringTag_PlanarConfiguration = ("PlanarConfiguration");
		static string stringTag_Predictor         = ("Predictor");
		static string stringTag_PrimaryChromaticities = ("PrimaryChromaticities");
		static string stringTag_ReferenceBlackWhite = ("ReferenceBlackWhite");
		static string stringTag_ResolutionUnit    = ("ResolutionUnit");
		static string stringTag_RowsPerStrip      = ("RowsPerStrip");
		static string stringTag_SampleFormat      = ("SampleFormat");
		static string stringTag_SamplesPerPixel   = ("SamplesPerPixel");
		static string stringTag_SMaxSampleValue   = ("SMaxSampleValue");
		static string stringTag_SMinSampleValue   = ("SMinSampleValue");
		static string stringTag_Software          = ("Software");
		static string stringTag_StripByteCounts   = ("StripByteCounts");
		static string stringTag_StripOffsets      = ("StripOffsets");
		static string stringTag_SubFileType       = ("SubFileType");
		static string stringTag4Options         = ("T4Options");
		static string stringTag6Options         = ("T6Options");
		static string stringTagTargetPrinter     = ("TargetPrinter");
		static string stringTagThresholding     = ("Thresholding");
		static string stringTagFileByteCounts    = ("TileByteCounts");
		static string stringTagFileLength        = ("TileLength");
		static string stringTagFileOffsets       = ("TileOffsets");
		static string stringTagFileWidth         = ("TileWidth");
		static string stringTagTransferFunction  = ("TransferFunction");
		static string stringTagTransferRange     = ("TransferRange");
		static string stringTag_WhitePoint        = ("WhitePoint");
		static string stringTag_XPosition         = ("XPosition");
		static string stringTag_XResolution       = ("XResolution");
		static string stringTag_YCbCrCoefficients = ("YCbCrCoefficients");
		static string stringTag_YCbCrPositioning  = ("YCbCrPositioning");
		static string stringTag_YCbCrSubSampling  = ("YCbCrSubSampling");
		static string stringTag_YPosition         = ("YPosition");
		static string stringTag_YResolution       = ("YResolution");
		static string stringTag_Default           = ("Unknown");


		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ColumnHeader TagNumber;
		private System.Windows.Forms.ColumnHeader Type;
		private System.Windows.Forms.ColumnHeader Count;
		private System.Windows.Forms.ColumnHeader Data;
		private System.Windows.Forms.Button cmdCopy;
		private System.Windows.Forms.Button cmdDelete;
		private System.Windows.Forms.Button cmdModify;
		private System.Windows.Forms.Button cmdSet;
		private System.Windows.Forms.MainMenu mnuFile;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private PegasusImaging.WinForms.ImagXpress8.ImageXTagCollection colTags;
		private System.String strCurrentDir = "";
		private PegasusImaging.WinForms.ImagXpress8.LoadOptions loLoadOptions;
		private System.Windows.Forms.ListView lstView;
		private System.Windows.Forms.RichTextBox rtfInfo;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Label lblLastError;
		private System.Windows.Forms.Label lblError;
		
		private  System.String 	strImageFile; 
			
		public ExifTags()
		{
			
			// Required for Windows Form Designer support
			
			//****Must call the UnlockControl function *****
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				//**Dispose of the ImagXpress objects
				if (imagXpress1 != null) 
				{	
					imagXpress1.Dispose();
					imagXpress1 = null;
				}

				if (colTags != null) 
				{	
					colTags.Dispose();
					colTags  = null;
				}
			
				if (components != null) 
				{
					components.Dispose();
				}

				if (components != null) 
				{
					components.Dispose();
				}
			}
		
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ExifTags));
			this.cmdGet = new System.Windows.Forms.Button();
			this.lstView = new System.Windows.Forms.ListView();
			this.TagNumber = new System.Windows.Forms.ColumnHeader();
			this.Type = new System.Windows.Forms.ColumnHeader();
			this.Count = new System.Windows.Forms.ColumnHeader();
			this.Data = new System.Windows.Forms.ColumnHeader();
			this.cmdCopy = new System.Windows.Forms.Button();
			this.cmdDelete = new System.Windows.Forms.Button();
			this.cmdModify = new System.Windows.Forms.Button();
			this.cmdSet = new System.Windows.Forms.Button();
			this.mnuFile = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mnuFileQuit = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.rtfInfo = new System.Windows.Forms.RichTextBox();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cmdGet
			// 
			this.cmdGet.AccessibleDescription = ((string)(resources.GetObject("cmdGet.AccessibleDescription")));
			this.cmdGet.AccessibleName = ((string)(resources.GetObject("cmdGet.AccessibleName")));
			this.cmdGet.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdGet.Anchor")));
			this.cmdGet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdGet.BackgroundImage")));
			this.cmdGet.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdGet.Dock")));
			this.cmdGet.Enabled = ((bool)(resources.GetObject("cmdGet.Enabled")));
			this.cmdGet.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdGet.FlatStyle")));
			this.cmdGet.Font = ((System.Drawing.Font)(resources.GetObject("cmdGet.Font")));
			this.cmdGet.Image = ((System.Drawing.Image)(resources.GetObject("cmdGet.Image")));
			this.cmdGet.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdGet.ImageAlign")));
			this.cmdGet.ImageIndex = ((int)(resources.GetObject("cmdGet.ImageIndex")));
			this.cmdGet.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdGet.ImeMode")));
			this.cmdGet.Location = ((System.Drawing.Point)(resources.GetObject("cmdGet.Location")));
			this.cmdGet.Name = "cmdGet";
			this.cmdGet.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdGet.RightToLeft")));
			this.cmdGet.Size = ((System.Drawing.Size)(resources.GetObject("cmdGet.Size")));
			this.cmdGet.TabIndex = ((int)(resources.GetObject("cmdGet.TabIndex")));
			this.cmdGet.Text = resources.GetString("cmdGet.Text");
			this.cmdGet.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdGet.TextAlign")));
			this.cmdGet.Visible = ((bool)(resources.GetObject("cmdGet.Visible")));
			this.cmdGet.Click += new System.EventHandler(this.cmdGet_Click);
			// 
			// lstView
			// 
			this.lstView.AccessibleDescription = ((string)(resources.GetObject("lstView.AccessibleDescription")));
			this.lstView.AccessibleName = ((string)(resources.GetObject("lstView.AccessibleName")));
			this.lstView.Alignment = ((System.Windows.Forms.ListViewAlignment)(resources.GetObject("lstView.Alignment")));
			this.lstView.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lstView.Anchor")));
			this.lstView.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lstView.BackgroundImage")));
			this.lstView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					  this.TagNumber,
																					  this.Type,
																					  this.Count,
																					  this.Data});
			this.lstView.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lstView.Dock")));
			this.lstView.Enabled = ((bool)(resources.GetObject("lstView.Enabled")));
			this.lstView.Font = ((System.Drawing.Font)(resources.GetObject("lstView.Font")));
			this.lstView.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lstView.ImeMode")));
			this.lstView.LabelWrap = ((bool)(resources.GetObject("lstView.LabelWrap")));
			this.lstView.Location = ((System.Drawing.Point)(resources.GetObject("lstView.Location")));
			this.lstView.Name = "lstView";
			this.lstView.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lstView.RightToLeft")));
			this.lstView.Size = ((System.Drawing.Size)(resources.GetObject("lstView.Size")));
			this.lstView.TabIndex = ((int)(resources.GetObject("lstView.TabIndex")));
			this.lstView.Text = resources.GetString("lstView.Text");
			this.lstView.View = System.Windows.Forms.View.Details;
			this.lstView.Visible = ((bool)(resources.GetObject("lstView.Visible")));
			// 
			// TagNumber
			// 
			this.TagNumber.Text = resources.GetString("TagNumber.Text");
			this.TagNumber.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("TagNumber.TextAlign")));
			this.TagNumber.Width = ((int)(resources.GetObject("TagNumber.Width")));
			// 
			// Type
			// 
			this.Type.Text = resources.GetString("Type.Text");
			this.Type.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("Type.TextAlign")));
			this.Type.Width = ((int)(resources.GetObject("Type.Width")));
			// 
			// Count
			// 
			this.Count.Text = resources.GetString("Count.Text");
			this.Count.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("Count.TextAlign")));
			this.Count.Width = ((int)(resources.GetObject("Count.Width")));
			// 
			// Data
			// 
			this.Data.Text = resources.GetString("Data.Text");
			this.Data.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("Data.TextAlign")));
			this.Data.Width = ((int)(resources.GetObject("Data.Width")));
			// 
			// cmdCopy
			// 
			this.cmdCopy.AccessibleDescription = ((string)(resources.GetObject("cmdCopy.AccessibleDescription")));
			this.cmdCopy.AccessibleName = ((string)(resources.GetObject("cmdCopy.AccessibleName")));
			this.cmdCopy.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdCopy.Anchor")));
			this.cmdCopy.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdCopy.BackgroundImage")));
			this.cmdCopy.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdCopy.Dock")));
			this.cmdCopy.Enabled = ((bool)(resources.GetObject("cmdCopy.Enabled")));
			this.cmdCopy.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdCopy.FlatStyle")));
			this.cmdCopy.Font = ((System.Drawing.Font)(resources.GetObject("cmdCopy.Font")));
			this.cmdCopy.Image = ((System.Drawing.Image)(resources.GetObject("cmdCopy.Image")));
			this.cmdCopy.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdCopy.ImageAlign")));
			this.cmdCopy.ImageIndex = ((int)(resources.GetObject("cmdCopy.ImageIndex")));
			this.cmdCopy.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdCopy.ImeMode")));
			this.cmdCopy.Location = ((System.Drawing.Point)(resources.GetObject("cmdCopy.Location")));
			this.cmdCopy.Name = "cmdCopy";
			this.cmdCopy.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdCopy.RightToLeft")));
			this.cmdCopy.Size = ((System.Drawing.Size)(resources.GetObject("cmdCopy.Size")));
			this.cmdCopy.TabIndex = ((int)(resources.GetObject("cmdCopy.TabIndex")));
			this.cmdCopy.Text = resources.GetString("cmdCopy.Text");
			this.cmdCopy.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdCopy.TextAlign")));
			this.cmdCopy.Visible = ((bool)(resources.GetObject("cmdCopy.Visible")));
			this.cmdCopy.Click += new System.EventHandler(this.cmdCopy_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.AccessibleDescription = ((string)(resources.GetObject("cmdDelete.AccessibleDescription")));
			this.cmdDelete.AccessibleName = ((string)(resources.GetObject("cmdDelete.AccessibleName")));
			this.cmdDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdDelete.Anchor")));
			this.cmdDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdDelete.BackgroundImage")));
			this.cmdDelete.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdDelete.Dock")));
			this.cmdDelete.Enabled = ((bool)(resources.GetObject("cmdDelete.Enabled")));
			this.cmdDelete.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdDelete.FlatStyle")));
			this.cmdDelete.Font = ((System.Drawing.Font)(resources.GetObject("cmdDelete.Font")));
			this.cmdDelete.Image = ((System.Drawing.Image)(resources.GetObject("cmdDelete.Image")));
			this.cmdDelete.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdDelete.ImageAlign")));
			this.cmdDelete.ImageIndex = ((int)(resources.GetObject("cmdDelete.ImageIndex")));
			this.cmdDelete.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdDelete.ImeMode")));
			this.cmdDelete.Location = ((System.Drawing.Point)(resources.GetObject("cmdDelete.Location")));
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdDelete.RightToLeft")));
			this.cmdDelete.Size = ((System.Drawing.Size)(resources.GetObject("cmdDelete.Size")));
			this.cmdDelete.TabIndex = ((int)(resources.GetObject("cmdDelete.TabIndex")));
			this.cmdDelete.Text = resources.GetString("cmdDelete.Text");
			this.cmdDelete.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdDelete.TextAlign")));
			this.cmdDelete.Visible = ((bool)(resources.GetObject("cmdDelete.Visible")));
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdModify
			// 
			this.cmdModify.AccessibleDescription = ((string)(resources.GetObject("cmdModify.AccessibleDescription")));
			this.cmdModify.AccessibleName = ((string)(resources.GetObject("cmdModify.AccessibleName")));
			this.cmdModify.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdModify.Anchor")));
			this.cmdModify.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdModify.BackgroundImage")));
			this.cmdModify.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdModify.Dock")));
			this.cmdModify.Enabled = ((bool)(resources.GetObject("cmdModify.Enabled")));
			this.cmdModify.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdModify.FlatStyle")));
			this.cmdModify.Font = ((System.Drawing.Font)(resources.GetObject("cmdModify.Font")));
			this.cmdModify.Image = ((System.Drawing.Image)(resources.GetObject("cmdModify.Image")));
			this.cmdModify.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdModify.ImageAlign")));
			this.cmdModify.ImageIndex = ((int)(resources.GetObject("cmdModify.ImageIndex")));
			this.cmdModify.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdModify.ImeMode")));
			this.cmdModify.Location = ((System.Drawing.Point)(resources.GetObject("cmdModify.Location")));
			this.cmdModify.Name = "cmdModify";
			this.cmdModify.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdModify.RightToLeft")));
			this.cmdModify.Size = ((System.Drawing.Size)(resources.GetObject("cmdModify.Size")));
			this.cmdModify.TabIndex = ((int)(resources.GetObject("cmdModify.TabIndex")));
			this.cmdModify.Text = resources.GetString("cmdModify.Text");
			this.cmdModify.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdModify.TextAlign")));
			this.cmdModify.Visible = ((bool)(resources.GetObject("cmdModify.Visible")));
			this.cmdModify.Click += new System.EventHandler(this.button1_Click);
			// 
			// cmdSet
			// 
			this.cmdSet.AccessibleDescription = ((string)(resources.GetObject("cmdSet.AccessibleDescription")));
			this.cmdSet.AccessibleName = ((string)(resources.GetObject("cmdSet.AccessibleName")));
			this.cmdSet.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("cmdSet.Anchor")));
			this.cmdSet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdSet.BackgroundImage")));
			this.cmdSet.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("cmdSet.Dock")));
			this.cmdSet.Enabled = ((bool)(resources.GetObject("cmdSet.Enabled")));
			this.cmdSet.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("cmdSet.FlatStyle")));
			this.cmdSet.Font = ((System.Drawing.Font)(resources.GetObject("cmdSet.Font")));
			this.cmdSet.Image = ((System.Drawing.Image)(resources.GetObject("cmdSet.Image")));
			this.cmdSet.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdSet.ImageAlign")));
			this.cmdSet.ImageIndex = ((int)(resources.GetObject("cmdSet.ImageIndex")));
			this.cmdSet.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("cmdSet.ImeMode")));
			this.cmdSet.Location = ((System.Drawing.Point)(resources.GetObject("cmdSet.Location")));
			this.cmdSet.Name = "cmdSet";
			this.cmdSet.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("cmdSet.RightToLeft")));
			this.cmdSet.Size = ((System.Drawing.Size)(resources.GetObject("cmdSet.Size")));
			this.cmdSet.TabIndex = ((int)(resources.GetObject("cmdSet.TabIndex")));
			this.cmdSet.Text = resources.GetString("cmdSet.Text");
			this.cmdSet.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("cmdSet.TextAlign")));
			this.cmdSet.Visible = ((bool)(resources.GetObject("cmdSet.Visible")));
			this.cmdSet.Click += new System.EventHandler(this.cmdSet_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.menuItem1,
																					this.menuItem2});
			this.mnuFile.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("mnuFile.RightToLeft")));
			// 
			// menuItem1
			// 
			this.menuItem1.Enabled = ((bool)(resources.GetObject("menuItem1.Enabled")));
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuFileOpen,
																					  this.menuItem3,
																					  this.mnuFileQuit});
			this.menuItem1.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItem1.Shortcut")));
			this.menuItem1.ShowShortcut = ((bool)(resources.GetObject("menuItem1.ShowShortcut")));
			this.menuItem1.Text = resources.GetString("menuItem1.Text");
			this.menuItem1.Visible = ((bool)(resources.GetObject("menuItem1.Visible")));
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Enabled = ((bool)(resources.GetObject("mnuFileOpen.Enabled")));
			this.mnuFileOpen.Index = 0;
			this.mnuFileOpen.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("mnuFileOpen.Shortcut")));
			this.mnuFileOpen.ShowShortcut = ((bool)(resources.GetObject("mnuFileOpen.ShowShortcut")));
			this.mnuFileOpen.Text = resources.GetString("mnuFileOpen.Text");
			this.mnuFileOpen.Visible = ((bool)(resources.GetObject("mnuFileOpen.Visible")));
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Enabled = ((bool)(resources.GetObject("menuItem3.Enabled")));
			this.menuItem3.Index = 1;
			this.menuItem3.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItem3.Shortcut")));
			this.menuItem3.ShowShortcut = ((bool)(resources.GetObject("menuItem3.ShowShortcut")));
			this.menuItem3.Text = resources.GetString("menuItem3.Text");
			this.menuItem3.Visible = ((bool)(resources.GetObject("menuItem3.Visible")));
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Enabled = ((bool)(resources.GetObject("mnuFileQuit.Enabled")));
			this.mnuFileQuit.Index = 2;
			this.mnuFileQuit.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("mnuFileQuit.Shortcut")));
			this.mnuFileQuit.ShowShortcut = ((bool)(resources.GetObject("mnuFileQuit.ShowShortcut")));
			this.mnuFileQuit.Text = resources.GetString("mnuFileQuit.Text");
			this.mnuFileQuit.Visible = ((bool)(resources.GetObject("mnuFileQuit.Visible")));
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Enabled = ((bool)(resources.GetObject("menuItem2.Enabled")));
			this.menuItem2.Index = 1;
			this.menuItem2.Shortcut = ((System.Windows.Forms.Shortcut)(resources.GetObject("menuItem2.Shortcut")));
			this.menuItem2.ShowShortcut = ((bool)(resources.GetObject("menuItem2.ShowShortcut")));
			this.menuItem2.Text = resources.GetString("menuItem2.Text");
			this.menuItem2.Visible = ((bool)(resources.GetObject("menuItem2.Visible")));
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// rtfInfo
			// 
			this.rtfInfo.AccessibleDescription = ((string)(resources.GetObject("rtfInfo.AccessibleDescription")));
			this.rtfInfo.AccessibleName = ((string)(resources.GetObject("rtfInfo.AccessibleName")));
			this.rtfInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("rtfInfo.Anchor")));
			this.rtfInfo.AutoSize = ((bool)(resources.GetObject("rtfInfo.AutoSize")));
			this.rtfInfo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rtfInfo.BackgroundImage")));
			this.rtfInfo.BulletIndent = ((int)(resources.GetObject("rtfInfo.BulletIndent")));
			this.rtfInfo.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("rtfInfo.Dock")));
			this.rtfInfo.Enabled = ((bool)(resources.GetObject("rtfInfo.Enabled")));
			this.rtfInfo.Font = ((System.Drawing.Font)(resources.GetObject("rtfInfo.Font")));
			this.rtfInfo.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("rtfInfo.ImeMode")));
			this.rtfInfo.Location = ((System.Drawing.Point)(resources.GetObject("rtfInfo.Location")));
			this.rtfInfo.MaxLength = ((int)(resources.GetObject("rtfInfo.MaxLength")));
			this.rtfInfo.Multiline = ((bool)(resources.GetObject("rtfInfo.Multiline")));
			this.rtfInfo.Name = "rtfInfo";
			this.rtfInfo.RightMargin = ((int)(resources.GetObject("rtfInfo.RightMargin")));
			this.rtfInfo.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("rtfInfo.RightToLeft")));
			this.rtfInfo.ScrollBars = ((System.Windows.Forms.RichTextBoxScrollBars)(resources.GetObject("rtfInfo.ScrollBars")));
			this.rtfInfo.Size = ((System.Drawing.Size)(resources.GetObject("rtfInfo.Size")));
			this.rtfInfo.TabIndex = ((int)(resources.GetObject("rtfInfo.TabIndex")));
			this.rtfInfo.Text = resources.GetString("rtfInfo.Text");
			this.rtfInfo.Visible = ((bool)(resources.GetObject("rtfInfo.Visible")));
			this.rtfInfo.WordWrap = ((bool)(resources.GetObject("rtfInfo.WordWrap")));
			this.rtfInfo.ZoomFactor = ((System.Single)(resources.GetObject("rtfInfo.ZoomFactor")));
			// 
			// lblLastError
			// 
			this.lblLastError.AccessibleDescription = ((string)(resources.GetObject("lblLastError.AccessibleDescription")));
			this.lblLastError.AccessibleName = ((string)(resources.GetObject("lblLastError.AccessibleName")));
			this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblLastError.Anchor")));
			this.lblLastError.AutoSize = ((bool)(resources.GetObject("lblLastError.AutoSize")));
			this.lblLastError.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblLastError.Dock")));
			this.lblLastError.Enabled = ((bool)(resources.GetObject("lblLastError.Enabled")));
			this.lblLastError.Font = ((System.Drawing.Font)(resources.GetObject("lblLastError.Font")));
			this.lblLastError.Image = ((System.Drawing.Image)(resources.GetObject("lblLastError.Image")));
			this.lblLastError.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblLastError.ImageAlign")));
			this.lblLastError.ImageIndex = ((int)(resources.GetObject("lblLastError.ImageIndex")));
			this.lblLastError.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblLastError.ImeMode")));
			this.lblLastError.Location = ((System.Drawing.Point)(resources.GetObject("lblLastError.Location")));
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblLastError.RightToLeft")));
			this.lblLastError.Size = ((System.Drawing.Size)(resources.GetObject("lblLastError.Size")));
			this.lblLastError.TabIndex = ((int)(resources.GetObject("lblLastError.TabIndex")));
			this.lblLastError.Text = resources.GetString("lblLastError.Text");
			this.lblLastError.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblLastError.TextAlign")));
			this.lblLastError.Visible = ((bool)(resources.GetObject("lblLastError.Visible")));
			// 
			// lblError
			// 
			this.lblError.AccessibleDescription = ((string)(resources.GetObject("lblError.AccessibleDescription")));
			this.lblError.AccessibleName = ((string)(resources.GetObject("lblError.AccessibleName")));
			this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblError.Anchor")));
			this.lblError.AutoSize = ((bool)(resources.GetObject("lblError.AutoSize")));
			this.lblError.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblError.Dock")));
			this.lblError.Enabled = ((bool)(resources.GetObject("lblError.Enabled")));
			this.lblError.Font = ((System.Drawing.Font)(resources.GetObject("lblError.Font")));
			this.lblError.Image = ((System.Drawing.Image)(resources.GetObject("lblError.Image")));
			this.lblError.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblError.ImageAlign")));
			this.lblError.ImageIndex = ((int)(resources.GetObject("lblError.ImageIndex")));
			this.lblError.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblError.ImeMode")));
			this.lblError.Location = ((System.Drawing.Point)(resources.GetObject("lblError.Location")));
			this.lblError.Name = "lblError";
			this.lblError.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblError.RightToLeft")));
			this.lblError.Size = ((System.Drawing.Size)(resources.GetObject("lblError.Size")));
			this.lblError.TabIndex = ((int)(resources.GetObject("lblError.TabIndex")));
			this.lblError.Text = resources.GetString("lblError.Text");
			this.lblError.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblError.TextAlign")));
			this.lblError.Visible = ((bool)(resources.GetObject("lblError.Visible")));
			// 
			// ExifTags
			// 
			this.AccessibleDescription = ((string)(resources.GetObject("$this.AccessibleDescription")));
			this.AccessibleName = ((string)(resources.GetObject("$this.AccessibleName")));
			this.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("$this.Anchor")));
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lblError,
																		  this.lblLastError,
																		  this.rtfInfo,
																		  this.cmdSet,
																		  this.cmdModify,
																		  this.cmdDelete,
																		  this.cmdCopy,
																		  this.lstView,
																		  this.cmdGet});
			this.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("$this.Dock")));
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.Menu = this.mnuFile;
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "ExifTags";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Visible = ((bool)(resources.GetObject("$this.Visible")));
			this.Load += new System.EventHandler(this.ExifTags_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ExifTags());
		}
		private void cmdGet_Click(object sender, System.EventArgs e)
		{
			try 
			{
				colTags = PegasusImaging.WinForms.ImagXpress8.ImageX.GetTags(strImageFile ,1);
				
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,lblError);
			}
			lstView.Items.Clear();
			
			GetExifTags(colTags);
		}
		void GetExifTags(PegasusImaging.WinForms.ImagXpress8.ImageXTagCollection a2)
		{
			
			PegasusImaging.WinForms.ImagXpress8.ImageXTag Tag;		
			System.Int32 i = 0;
			System.Int32 j = 0;
			System.String exiftype;
			System.String exifdataparsed = "";
			lstView.BeginUpdate();
			for (i = 0; i < a2.Count; i++)
			{
				exifdataparsed = "";
				Tag = a2[i];
				exiftype = Tag.TagType.ToString(cultNumber);
				switch(Tag.TagType) 
				{
					case TagTypes.Byte:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += ((char)(Tag.GetTagBytes()[j]));
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						} break;
					case TagTypes.Ascii:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += ((char)Tag.GetTagBytes()[j]);
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						}	break;
					case TagTypes.Short:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += Tag.GetTagUInt16()[0].ToString(cultNumber);
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						}	break;
					case TagTypes.Long:
						exifdataparsed += Tag.GetTagRational()[0].ToString(cultNumber);
						break;
					case TagTypes.Rational:
						exifdataparsed += Tag.GetTagRational()[0].ToString(cultNumber) + "/" + Tag.GetTagRational()[1].ToString(cultNumber);
						break;
					case TagTypes.Sbyte:
						for (j = 0; j < Tag.TagElementsCount; j++)
						{
							try
							{
								exifdataparsed += Tag.GetTagSBytes().ToString();
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						} break;
					case TagTypes.Undefine:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += Tag.GetTagBytes()[j].ToString(cultNumber);
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						} break;
					case TagTypes.Sshort:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += Tag.GetTagUInt16()[j].ToString(cultNumber);
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						}break;
					case TagTypes.Slong:
						exifdataparsed += Tag.GetTagUInt32().ToString();
						break;
					case TagTypes.Srational:
						exifdataparsed += Tag.GetTagSRational()[0].ToString(cultNumber) + "/" + Tag.GetTagSRational()[1].ToString(cultNumber);
						break;
					case TagTypes.Float:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += Tag.GetTagFloat()[j].ToString(cultNumber);
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						}	break;
					case TagTypes.Double:
						for (j = 0; j < Tag.TagElementsCount; j++) 
						{
							try
							{
								exifdataparsed += Tag.GetTagDouble()[j].ToString(cultNumber);
							} 
							catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
							{
								PegasusError(ex,lblError);
							}
						}break;
					default: break;
				}
				System.String sDesc;
				sDesc = GetTagDesc(Tag.TagNumber);
				lstView.Items.Add(new ListViewItem(new System.String[] {
																	 sDesc,
																	 Tag.TagType.ToString(cultNumber),
																	 Tag.TagElementsCount.ToString(cultNumber),
																	 exifdataparsed
																 }));
			}
			lstView.EndUpdate();
		}

		private void ExifTags_Load(object sender, System.EventArgs e)
		{
			loLoadOptions = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);
			
			//here we set the current directory and image so that the file open dialog box works well
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString ();
			strImageFile = System.IO.Path.Combine (strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\exif.jpg");
			strCurrentDir = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\");
			
			
			
		}

		private void cmdCopy_Click(object sender, System.EventArgs e)
		{		
			System.String tagstring  = "Copyright (c) 2005, Pegasus Imaging Corp.";
			ImageXTag newtag = new PegasusImaging.WinForms.ImagXpress8.ImageXTag();
			for (int counter = 0; counter <= 3 ;counter++)
	
			{
				try 
				{
					newtag.SetTagBytes(new System.Text.UTF8Encoding().GetBytes(tagstring));
					newtag.TagLevel = ((byte)counter);
					newtag.TagType = TagTypes.Ascii;
					newtag.TagNumber = 307;
					colTags.Add(newtag);
				} 
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
				{
					PegasusError(ex,lblError);
				}
			}
			lstView.Items.Clear();
			GetExifTags(colTags);
		}

		private void cmdDelete_Click(object sender, System.EventArgs e)
		{
			for (int i = 0; i <= 3; i++)
			{
				try 
				{
					colTags.RemoveTag(307,((byte)i));
				} 
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
				{
					if (ex.Number != 0)
					{
						PegasusError(ex,lblError);
					}
				}
			}
			lblError.Text = "";
			lstView.Items.Clear();
			GetExifTags(colTags);
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			System.Int64[] numbers = new System.Int64[2];
			numbers[0] = 85;
			numbers[1] = 1;
			try 
			{
				ImageXTag tagMod = colTags.GetTag(282,(byte)0);
				tagMod.SetTagRational(numbers);
				tagMod = colTags.GetTag(283,(byte)0);
				tagMod.SetTagRational(numbers);
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			lstView.Items.Clear();
			GetExifTags(colTags);
		}

		private void cmdSet_Click(object sender, System.EventArgs e)
		{
			try 
			{
				PegasusImaging.WinForms.ImagXpress8.ImageX.SetTags(colTags,strImageFile,1);
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,lblError);
			}
			try 
			{
				colTags = PegasusImaging.WinForms.ImagXpress8.ImageX.GetTags(strImageFile,1);
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
			{
				PegasusError(ex,lblError);
			}
			lstView.Items.Clear();
			//get the tags here
			GetExifTags(colTags);

		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			System.String sTmp = PegasusOpenFile("Tiff Files (*.tif)|*.tif|jpeg Files (*.jpg)|*.jpg");
			if (sTmp.Length != 0) 
			{
				strImageFile = sTmp;
				try 
				{
					colTags = PegasusImaging.WinForms.ImagXpress8.ImageX.GetTags(strImageFile,1);
				} 
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
				{
					PegasusError(ex,lblError);
				}
				lstView.Items.Clear();
				GetExifTags(colTags);
			}
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
		static System.String GetTagDesc(int tagnumber)
		{
			switch (tagnumber)
			{
				case (int)ExifTagNumber.TagExifPixYDim: 
					return stringTag_ExifPixYDim; 
					
				case (int)ExifTagNumber.TagArtist: 
					return stringTag_Artist; 
					
				case (int)ExifTagNumber.TagBitsPerSample: 
					return stringTag_BitsPerSample; 
					
				case (int)ExifTagNumber.TagCellLength:
					return stringTag_CellLength; 
					
				case (int)ExifTagNumber.TagCellWidth:
					return stringTag_CellWidth; 
					
				case (int)ExifTagNumber.TagColorMap:
					return stringTag_ColorMap; 
					
				case (int)ExifTagNumber.TagCompression:
					return stringTag_Compression; 
					
				case (int)ExifTagNumber.TagCopyright:
					return stringTag_Copyright; 
					
				case (int)ExifTagNumber.TagDateTime:
					return stringTag_DateTime; 
					
				case (int)ExifTagNumber.TagDocumentName:
					return stringTag_DocumentName; 
					
				case (int)ExifTagNumber.TagDotRange:
					return stringTag_DotRange; 
					
				case (int)ExifTagNumber.TagExifAperture:
					return stringTag_ExifAperture; 
					
				case (int)ExifTagNumber.TagExifBrightness:
					return stringTag_ExifBrightness; 
					
				case (int)ExifTagNumber.TagExifCfaPattern:
					return stringTag_ExifCfaPattern; 
					
				case (int)ExifTagNumber.TagExifColorSpace:
					return stringTag_ExifColorSpace; 
					
				case (int)ExifTagNumber.TagExifCompBPP:
					return stringTag_ExifCompBPP; 
					
				case (int)ExifTagNumber.TagExifCompConfig:
					return stringTag_ExifCompConfig; 
					
				case (int)ExifTagNumber.TagExifDTDigitized:
					return stringTag_ExifDTDigitized; 
					
				case (int)ExifTagNumber.TagExifDTDigSS:
					return stringTag_ExifDTDigSS; 
					
				case (int)ExifTagNumber.TagExifDTOrig:
					return stringTag_ExifDTOrig; 
					
				case (int)ExifTagNumber.TagExifDTOrigSS:
					return stringTag_ExifDTOrigSS; 
					
				case (int)ExifTagNumber.TagExifDTSubsec:
					return stringTag_ExifDTSubsec; 
					
				case (int)ExifTagNumber.TagExifExposureBias:
					return stringTag_ExifExposureBias; 
					
				case (int)ExifTagNumber.TagExifExposureIndex:
					return stringTag_ExifExposureIndex; 
					
				case (int)ExifTagNumber.TagExifExposureProg:
					return stringTag_ExifExposureProg; 
					
				case (int)ExifTagNumber.TagExifExposureTime:
					return stringTag_ExifExposureTime; 
					
				case (int)ExifTagNumber.TagExifFileSource:
					return stringTag_ExifFileSource; 
					
				case (int)ExifTagNumber.TagExifFlash:
					return stringTag_ExifFlash; 
					
				case (int)ExifTagNumber.TagExifFlashEnergy:
					return stringTag_ExifFlashEnergy; 
					
				case (int)ExifTagNumber.TagExifFNumber:
					return stringTag_ExifFNumber; 
					
				case (int)ExifTagNumber.TagExifFocalLength:
					return stringTag_ExifFocalLength; 
					
				case (int)ExifTagNumber.TagExifFocalResUnit:
					return stringTag_ExifFocalResUnit; 
					
				case (int)ExifTagNumber.TagExifFocalXRes:
					return stringTag_ExifFocalXRes; 
					
				case (int)ExifTagNumber.TagExifFocalYRes:
					return stringTag_ExifFocalYRes; 
					
				case (int)ExifTagNumber.TagExifFPXVer:
					return stringTag_ExifFPXVer; 
					
				case (int)ExifTagNumber.TagExifImageHistory:
					return stringTag_ExifImageHistory; 
					
				case (int)ExifTagNumber.TagExifImageNumber:
					return stringTag_ExifImageNumber; 
					
				case (int)ExifTagNumber.TagExifInterlace:
					return stringTag_ExifInterlace; 
					
				case (int)ExifTagNumber.TagExifInterop:
					return stringTag_ExifInterop; 
					
				case (int)ExifTagNumber.TagExifISOSpeed:
					return stringTag_ExifISOSpeed; 
					
				case (int)ExifTagNumber.TagExifLightSource:
					return stringTag_ExifLightSource; 
					
				case (int)ExifTagNumber.TagExifMakerNote:
					return stringTag_ExifMakerNote; 
					
				case (int)ExifTagNumber.TagExifMaxAperture:
					return stringTag_ExifMaxAperture; 
					
				case (int)ExifTagNumber.TagExifMeteringMode:
					return stringTag_ExifMeteringMode; 
					
				case (int)ExifTagNumber.TagExifNoise:
					return stringTag_ExifNoise; 
					
				case (int)ExifTagNumber.TagExifOECF:
					return stringTag_ExifOECF; 
					
				case (int)ExifTagNumber.TagExifPixXDim:
					return stringTag_ExifPixXDim; 
					
				case (int)ExifTagNumber.TagExifRelatedWav:
					return stringTag_ExifRelatedWav; 
					
				case (int)ExifTagNumber.TagExifSceneType:
					return stringTag_ExifSceneType; 
					
				case (int)ExifTagNumber.TagExifSecurityClassification:
					return stringTag_ExifSecurityClassification; 
					
				case (int)ExifTagNumber.TagExifSelfTimerMode:
					return stringTag_ExifSelfTimerMode; 
					
				case (int)ExifTagNumber.TagExifSensingMethod:
					return stringTag_ExifSensingMethod; 
					
				case (int)ExifTagNumber.TagExifShutterSpeed:
					return stringTag_ExifShutterSpeed; 
					
				case (int)ExifTagNumber.TagExifSpatialFR:
					return stringTag_ExifSpatialFR; 
					
				case (int)ExifTagNumber.TagExifSpectralSense:
					return stringTag_ExifSpectralSense; 
					
				case (int)ExifTagNumber.TagExifSubjectDist:
					return stringTag_ExifSubjectDist; 
					
				case (int)ExifTagNumber.TagExifSubjectLoc:
					return stringTag_ExifSubjectLoc; 
					
				case (int)ExifTagNumber.TagExifTiff_EPStandardID:
					return stringTag_ExifTiff_EPStandardID; 
					
				case (int)ExifTagNumber.TagExifTimeZoneOffset:
					return stringTag_ExifTimeZoneOffset; 
					
				case (int)ExifTagNumber.TagExifUserComment:
					return stringTag_ExifUserComment; 
					
				case (int)ExifTagNumber.TagExifVer:
					return stringTag_ExifVer; 
					
				case (int)ExifTagNumber.TagExtraSamples:
					return stringTag_ExtraSamples; 
					
				case (int)ExifTagNumber.TagFillOrder:
					return stringTag_FillOrder; 
					
				case (int)ExifTagNumber.TagFreeByteCounts:
					return stringTag_FreeByteCounts; 
					
				case (int)ExifTagNumber.TagFreeOffsets:
					return stringTag_FreeOffsets; 
					
				case (int)ExifTagNumber.TagGrayResponseCurve:
					return stringTag_GrayResponseCurve; 
					
				case (int)ExifTagNumber.TagGrayResponseUnit:
					return stringTag_GrayResponseUnit; 
					
				case (int)ExifTagNumber.TagHalftoneHints:
					return stringTag_HalftoneHints; 
					
				case (int)ExifTagNumber.TagHostComputer:
					return stringTag_HostComputer; 
					
				case (int)ExifTagNumber.TagImageDescription:
					return stringTag_ImageDescription; 
					
				case (int)ExifTagNumber.TagImageLength:
					return stringTag_ImageLength; 
					
				case (int)ExifTagNumber.TagImageWidth:
					return stringTag_ImageWidth; 
					
				case (int)ExifTagNumber.TagInkNames:
					return stringTag_InkNames; 
					
				case (int)ExifTagNumber.TagInkSet:
					return stringTag_InkSet; 
					
				case (int)ExifTagNumber.TagJpegACTables:
					return stringTag_jpegACTables; 
					
				case (int)ExifTagNumber.TagJpegDCTTables:
					return stringTag_jpegDCTTables; 
					
				case (int)ExifTagNumber.TagJpegInterchangeFormat:
					return stringTag_jpegInterchangeFormat; 
					
				case (int)ExifTagNumber.TagJpegInterchangeFormatLength:
					return stringTag_jpegInterchangeFormatLength; 
					
				case (int)ExifTagNumber.TagJpegLosslessPredictors:
					return stringTag_jpegLosslessPredictors; 
					
				case (int)ExifTagNumber.TagJpegPointTransforms:
					return stringTag_jpegPointTransforms; 
					
				case (int)ExifTagNumber.TagJpegProc:
					return stringTag_jpegProc; 
					
				case (int)ExifTagNumber.TagJpegQTables:
					return stringTag_jpegQTables; 
					
				case (int)ExifTagNumber.TagJpegRestartInterval:
					return stringTag_jpegRestartInterval; 
					
				case (int)ExifTagNumber.TagMake:
					return stringTag_Make; 
					
				case (int)ExifTagNumber.TagMaxSampleValue:
					return stringTag_MaxSampleValue; 
					
				case (int)ExifTagNumber.TagMinSampleValue:
					return stringTag_MinSampleValue; 
					
				case (int)ExifTagNumber.TagModel:
					return stringTag_Model; 
					
				case (int)ExifTagNumber.TagNewSubFileType:
					return stringTag_NewSubFileType; 
					
				case (int)ExifTagNumber.TagNumberOfInks:
					return stringTag_NumberOfInks; 
					
				case (int)ExifTagNumber.TagOrientation:
					return stringTag_Orientation; 
					
				case (int)ExifTagNumber.TagPageName:
					return stringTag_PageName; 
					
				case (int)ExifTagNumber.TagPageNumber:
					return stringTag_PageNumber; 
					
				case (int)ExifTagNumber.TagPhotometricInterpretation:
					return stringTag_PhotometricInterpretation; 
					
				case (int)ExifTagNumber.TagPlanarConfiguration:
					return stringTag_PlanarConfiguration; 
					
				case (int)ExifTagNumber.TagPredictor:
					return stringTag_Predictor; 
					
				case (int)ExifTagNumber.TagPrimaryChromaticities:
					return stringTag_PrimaryChromaticities; 
					
				case (int)ExifTagNumber.TagReferenceBlackWhite:
					return stringTag_ReferenceBlackWhite; 
					
				case (int)ExifTagNumber.TagResolutionUnit:
					return stringTag_ResolutionUnit;
					
				case (int)ExifTagNumber.TagRowsPerStrip:
					return stringTag_RowsPerStrip; 
					
				case (int)ExifTagNumber.TagSampleFormat:
					return stringTag_SampleFormat; 
					
				case (int)ExifTagNumber.TagSamplesPerPixel:
					return stringTag_SamplesPerPixel; 
					
				case (int)ExifTagNumber.TagSMaxSampleValue: 
					return stringTag_SMaxSampleValue; 
					
				case (int)ExifTagNumber.TagSMinSampleValue:
					return stringTag_SMinSampleValue; 
					
				case (int)ExifTagNumber.TagSoftware:
					return stringTag_Software; 
					
				case (int)ExifTagNumber.TagStripByteCounts:
					return stringTag_StripByteCounts; 
					
				case (int)ExifTagNumber.TagStripOffsets:
					return stringTag_StripOffsets; 
					
				case (int)ExifTagNumber.TagSubFileType:
					return stringTag_SubFileType; 
					
				case (int)ExifTagNumber.Tag4Options:
					return stringTag4Options; 
					
				case (int)ExifTagNumber.Tag6Options:
					return stringTag6Options; 
					
				case (int)ExifTagNumber.TagTargetPrinter:
					return stringTagTargetPrinter; 
					
				case (int)ExifTagNumber.TagThresholding:
					return stringTagThresholding; 
					
				case (int)ExifTagNumber.TagFileByteCounts:
					return stringTagFileByteCounts; 
					
				case (int)ExifTagNumber.TagFileLength:
					return stringTagFileLength; 
					
				case (int)ExifTagNumber.TagFileOffsets:
					return stringTagFileOffsets; 
					
				case (int)ExifTagNumber.TagFileWidth:
					return stringTagFileWidth; 
					
				case (int)ExifTagNumber.TagTransferFunction:
					return stringTagTransferFunction; 
					
				case (int)ExifTagNumber.TagTransferRange:
					return stringTagTransferRange; 
					
				case (int)ExifTagNumber.TagWhitePoint:
					return stringTag_WhitePoint; 
					
				case (int)ExifTagNumber.TagXPosition: 
					return stringTag_XPosition; 
					
				case (int)ExifTagNumber.TagXResolution:
					return stringTag_XResolution; 
					
				case (int)ExifTagNumber.TagYCbCrCoefficients:
					return stringTag_YCbCrCoefficients; 
					
				case (int)ExifTagNumber.TagYCbCrPositioning: 
					return stringTag_YCbCrPositioning; 
					
				case (int)ExifTagNumber.TagYCbCrSubSampling:
					return stringTag_YCbCrSubSampling; 
					
				case (int)ExifTagNumber.TagYPosition:
					return stringTag_YPosition; 
					
				case (int)ExifTagNumber.TagYResolution:
					return stringTag_YResolution; 
					
				default: 
					return stringTag_Default; 
					
			}
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}


		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion
	}
}
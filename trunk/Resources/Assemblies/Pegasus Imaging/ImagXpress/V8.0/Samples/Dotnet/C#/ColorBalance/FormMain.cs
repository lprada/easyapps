/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;


using PegasusImaging.WinForms.ImagXpress8;

namespace ColorBalance
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.Processor processor1;
		internal System.Windows.Forms.Button cmdReloadImage;
		internal System.Windows.Forms.TabControl TabControl1;
		internal System.Windows.Forms.TabPage docAutoColorBalance;
		internal System.Windows.Forms.Button cmdAutoColorbalance;
		internal System.Windows.Forms.TabPage docAdjustColorBalance;
		internal System.Windows.Forms.Button cmdAdjustColorBalance;
		internal System.Windows.Forms.Label lblBlueVal;
		internal System.Windows.Forms.Label lblGreenVal;
		internal System.Windows.Forms.Label lblRedVal;
		internal System.Windows.Forms.HScrollBar hscrBlue;
		internal System.Windows.Forms.HScrollBar hscrGreen;
		internal System.Windows.Forms.HScrollBar hscrRed;
		internal System.Windows.Forms.ComboBox cboColor;
		internal System.Windows.Forms.Label lblBlue;
		internal System.Windows.Forms.Label lblGreen;
		internal System.Windows.Forms.Label lblRed;
		internal System.Windows.Forms.Label lblColor;
		internal System.Windows.Forms.ListBox lstStatus;
		internal System.Windows.Forms.Label lblLoadStatus;
		internal System.Windows.Forms.Label lblError;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem mnuReloadCurrentImage;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem mnuQuit;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout;


		string imgFileName;
		PegasusImaging.WinForms.ImagXpress8.ImageX imagX1;



		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			
			 //The UnlockControl function must be called to distribute the runtime**
             //PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)
			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				//  Don't forget to dispose IX
				// 
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (!(processor1 == null)) 
				{
					processor1.Dispose();
					processor1 = null;
				}
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.processor1 = new PegasusImaging.WinForms.ImagXpress8.Processor();
			this.cmdReloadImage = new System.Windows.Forms.Button();
			this.TabControl1 = new System.Windows.Forms.TabControl();
			this.docAutoColorBalance = new System.Windows.Forms.TabPage();
			this.cmdAutoColorbalance = new System.Windows.Forms.Button();
			this.docAdjustColorBalance = new System.Windows.Forms.TabPage();
			this.cmdAdjustColorBalance = new System.Windows.Forms.Button();
			this.lblBlueVal = new System.Windows.Forms.Label();
			this.lblGreenVal = new System.Windows.Forms.Label();
			this.lblRedVal = new System.Windows.Forms.Label();
			this.hscrBlue = new System.Windows.Forms.HScrollBar();
			this.hscrGreen = new System.Windows.Forms.HScrollBar();
			this.hscrRed = new System.Windows.Forms.HScrollBar();
			this.cboColor = new System.Windows.Forms.ComboBox();
			this.lblBlue = new System.Windows.Forms.Label();
			this.lblGreen = new System.Windows.Forms.Label();
			this.lblRed = new System.Windows.Forms.Label();
			this.lblColor = new System.Windows.Forms.Label();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnuFile = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.mnuReloadCurrentImage = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.mnuQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.TabControl1.SuspendLayout();
			this.docAutoColorBalance.SuspendLayout();
			this.docAdjustColorBalance.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.Location = new System.Drawing.Point(16, 72);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(400, 488);
			this.imageXView1.TabIndex = 0;
			// 
			// processor1
			// 
			this.processor1.BackgroundColor = System.Drawing.Color.Black;
			this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast;
			this.processor1.ProgressPercent = 10;
			this.processor1.Redeyes = null;
			// 
			// cmdReloadImage
			// 
			this.cmdReloadImage.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.cmdReloadImage.Location = new System.Drawing.Point(560, 88);
			this.cmdReloadImage.Name = "cmdReloadImage";
			this.cmdReloadImage.Size = new System.Drawing.Size(168, 32);
			this.cmdReloadImage.TabIndex = 46;
			this.cmdReloadImage.Text = "Reload Image";
			this.cmdReloadImage.Click += new System.EventHandler(this.cmdReloadImage_Click);
			// 
			// TabControl1
			// 
			this.TabControl1.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.TabControl1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.docAutoColorBalance,
																					  this.docAdjustColorBalance});
			this.TabControl1.Location = new System.Drawing.Point(440, 136);
			this.TabControl1.Name = "TabControl1";
			this.TabControl1.SelectedIndex = 0;
			this.TabControl1.Size = new System.Drawing.Size(408, 248);
			this.TabControl1.TabIndex = 45;
			// 
			// docAutoColorBalance
			// 
			this.docAutoColorBalance.Controls.AddRange(new System.Windows.Forms.Control[] {
																							  this.cmdAutoColorbalance});
			this.docAutoColorBalance.Location = new System.Drawing.Point(4, 22);
			this.docAutoColorBalance.Name = "docAutoColorBalance";
			this.docAutoColorBalance.Size = new System.Drawing.Size(392, 222);
			this.docAutoColorBalance.TabIndex = 1;
			this.docAutoColorBalance.Text = "AutoColorBalance";
			this.docAutoColorBalance.Visible = false;
			// 
			// cmdAutoColorbalance
			// 
			this.cmdAutoColorbalance.Location = new System.Drawing.Point(128, 168);
			this.cmdAutoColorbalance.Name = "cmdAutoColorbalance";
			this.cmdAutoColorbalance.Size = new System.Drawing.Size(128, 32);
			this.cmdAutoColorbalance.TabIndex = 0;
			this.cmdAutoColorbalance.Text = "Apply";
			this.cmdAutoColorbalance.Click += new System.EventHandler(this.cmdAutoColorbalance_Click);
			// 
			// docAdjustColorBalance
			// 
			this.docAdjustColorBalance.Controls.AddRange(new System.Windows.Forms.Control[] {
																								this.cmdAdjustColorBalance,
																								this.lblBlueVal,
																								this.lblGreenVal,
																								this.lblRedVal,
																								this.hscrBlue,
																								this.hscrGreen,
																								this.hscrRed,
																								this.cboColor,
																								this.lblBlue,
																								this.lblGreen,
																								this.lblRed,
																								this.lblColor});
			this.docAdjustColorBalance.Location = new System.Drawing.Point(4, 22);
			this.docAdjustColorBalance.Name = "docAdjustColorBalance";
			this.docAdjustColorBalance.Size = new System.Drawing.Size(400, 222);
			this.docAdjustColorBalance.TabIndex = 2;
			this.docAdjustColorBalance.Text = "AdjustColorBalance";
			this.docAdjustColorBalance.Visible = false;
			// 
			// cmdAdjustColorBalance
			// 
			this.cmdAdjustColorBalance.Location = new System.Drawing.Point(144, 184);
			this.cmdAdjustColorBalance.Name = "cmdAdjustColorBalance";
			this.cmdAdjustColorBalance.Size = new System.Drawing.Size(120, 24);
			this.cmdAdjustColorBalance.TabIndex = 11;
			this.cmdAdjustColorBalance.Text = "Apply";
			this.cmdAdjustColorBalance.Click += new System.EventHandler(this.cmdAdjustColorBalance_Click);
			// 
			// lblBlueVal
			// 
			this.lblBlueVal.Location = new System.Drawing.Point(336, 144);
			this.lblBlueVal.Name = "lblBlueVal";
			this.lblBlueVal.Size = new System.Drawing.Size(48, 16);
			this.lblBlueVal.TabIndex = 10;
			// 
			// lblGreenVal
			// 
			this.lblGreenVal.Location = new System.Drawing.Point(336, 96);
			this.lblGreenVal.Name = "lblGreenVal";
			this.lblGreenVal.Size = new System.Drawing.Size(48, 24);
			this.lblGreenVal.TabIndex = 9;
			// 
			// lblRedVal
			// 
			this.lblRedVal.Location = new System.Drawing.Point(336, 56);
			this.lblRedVal.Name = "lblRedVal";
			this.lblRedVal.Size = new System.Drawing.Size(48, 24);
			this.lblRedVal.TabIndex = 8;
			// 
			// hscrBlue
			// 
			this.hscrBlue.Location = new System.Drawing.Point(104, 144);
			this.hscrBlue.Minimum = -100;
			this.hscrBlue.Name = "hscrBlue";
			this.hscrBlue.Size = new System.Drawing.Size(224, 16);
			this.hscrBlue.TabIndex = 7;
			this.hscrBlue.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrBlue_Scroll);
			// 
			// hscrGreen
			// 
			this.hscrGreen.Location = new System.Drawing.Point(104, 96);
			this.hscrGreen.Minimum = -100;
			this.hscrGreen.Name = "hscrGreen";
			this.hscrGreen.Size = new System.Drawing.Size(224, 16);
			this.hscrGreen.TabIndex = 6;
			this.hscrGreen.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrGreen_Scroll);
			// 
			// hscrRed
			// 
			this.hscrRed.Location = new System.Drawing.Point(104, 56);
			this.hscrRed.Minimum = -100;
			this.hscrRed.Name = "hscrRed";
			this.hscrRed.Size = new System.Drawing.Size(224, 16);
			this.hscrRed.TabIndex = 5;
			this.hscrRed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrRed_Scroll);
			// 
			// cboColor
			// 
			this.cboColor.Location = new System.Drawing.Point(112, 16);
			this.cboColor.Name = "cboColor";
			this.cboColor.Size = new System.Drawing.Size(104, 21);
			this.cboColor.TabIndex = 4;
			// 
			// lblBlue
			// 
			this.lblBlue.Location = new System.Drawing.Point(16, 144);
			this.lblBlue.Name = "lblBlue";
			this.lblBlue.Size = new System.Drawing.Size(72, 24);
			this.lblBlue.TabIndex = 3;
			this.lblBlue.Text = "Blue:";
			// 
			// lblGreen
			// 
			this.lblGreen.Location = new System.Drawing.Point(16, 96);
			this.lblGreen.Name = "lblGreen";
			this.lblGreen.Size = new System.Drawing.Size(64, 24);
			this.lblGreen.TabIndex = 2;
			this.lblGreen.Text = "Green:";
			// 
			// lblRed
			// 
			this.lblRed.Location = new System.Drawing.Point(16, 56);
			this.lblRed.Name = "lblRed";
			this.lblRed.Size = new System.Drawing.Size(64, 24);
			this.lblRed.TabIndex = 1;
			this.lblRed.Text = "Red:";
			// 
			// lblColor
			// 
			this.lblColor.Location = new System.Drawing.Point(16, 16);
			this.lblColor.Name = "lblColor";
			this.lblColor.Size = new System.Drawing.Size(64, 24);
			this.lblColor.TabIndex = 0;
			this.lblColor.Text = "Color:";
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(424, 448);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(256, 108);
			this.lstStatus.TabIndex = 44;
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(432, 416);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(136, 24);
			this.lblLoadStatus.TabIndex = 43;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lblError
			// 
			this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblError.Location = new System.Drawing.Point(696, 456);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(160, 96);
			this.lblError.TabIndex = 42;
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblLastError.Location = new System.Drawing.Point(696, 416);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(160, 24);
			this.lblLastError.TabIndex = 41;
			this.lblLastError.Text = "Last Error:";
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Adjusting the image color using the AutoLightness, AutoColorBalance, and Adjust" +
														 "ColorBalance methods."});
			this.lstInfo.Location = new System.Drawing.Point(16, 8);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(832, 56);
			this.lstInfo.TabIndex = 40;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuFile,
																					  this.mnuToolbar,
																					  this.mnuAbout});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuFileOpen,
																					this.mnuReloadCurrentImage,
																					this.menuItem4,
																					this.mnuQuit});
			this.mnuFile.Text = "&File";
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Index = 0;
			this.mnuFileOpen.Text = "&Open Image...";
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// mnuReloadCurrentImage
			// 
			this.mnuReloadCurrentImage.Index = 1;
			this.mnuReloadCurrentImage.Text = "Reload Current Image";
			this.mnuReloadCurrentImage.Click += new System.EventHandler(this.mnuReloadCurrentImage_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 2;
			this.menuItem4.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 3;
			this.mnuQuit.Text = "&Quit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "&Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "&Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(872, 582);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdReloadImage,
																		  this.TabControl1,
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.lblError,
																		  this.lblLastError,
																		  this.lstInfo,
																		  this.imageXView1});
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "ColorBalance";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.TabControl1.ResumeLayout(false);
			this.docAutoColorBalance.ResumeLayout(false);
			this.docAdjustColorBalance.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";
		private System.String strCurrentDir = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		
		#endregion
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void ImageStatusEvent(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
			
		}
		private void ProgressEvent(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		private void LoadFile() 
		{
			try 
			{
				imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(imgFileName);
				if ((imagX1.ImageXData.BitsPerPixel != 24)) 
				{
					MessageBox.Show("Image must be 24-bit for sample methods. Converting image to 24-bit.");
					processor1.Image = imagX1;
					processor1.ColorDepth(24, PaletteType.Fixed, DitherType.NoDither);
				}
				imageXView1.Image = imagX1;
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}


		private void Form1_Load(object sender, System.EventArgs e)
		{
			int i;
			try 
			{
				// **The UnlockRuntime function must be called to distribute the runtime**
				// imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)
								
				//this is where events are assigned. This happens before the file gets loaded.
				PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEvent);
				PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEvent);
				
				
				
				imageXView1.AutoScroll = true;
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
			string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
			imgFileName = System.IO.Path.Combine(strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\Auto_Balance1.JPG");
			LoadFile();
			
			i = cboColor.Items.Add(0);
			i = cboColor.Items.Add(1);
			cboColor.SelectedIndex = 0;
			lblBlueVal.Text = hscrBlue.Value.ToString();
			lblGreenVal.Text = hscrGreen.Value.ToString();
			lblRedVal.Text = hscrRed.Value.ToString();


		}

		private void hscrRed_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblRedVal.Text = hscrRed.Value.ToString();
		}

		private void hscrGreen_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblGreenVal.Text = hscrGreen.Value.ToString();
		}

		private void hscrBlue_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblBlueVal.Text = hscrBlue.Value.ToString();
		}
		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			imgFileName = PegasusOpenFile();
			LoadFile();
		}
		private void mnuReloadCurrentImage_Click(object sender, System.EventArgs e)
		{
			LoadFile();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			if ((imageXView1.Toolbar.Activated == true)) 
			{
				mnuToolbarShow.Text = "Show";
				imageXView1.Toolbar.Activated = false;
			}
			else 
			{
				mnuToolbarShow.Text = "Hide";
				imageXView1.Toolbar.Activated = true;
			}
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		private void cmdReloadImage_Click(object sender, System.EventArgs e)
		{
			LoadFile();
		}

		private void cmdAutoLightness_Click(object sender, System.EventArgs e)
		{	
			try 
			{
				processor1.Image = imageXView1.Image;
				processor1.AutoLightness();
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}

		private void cmdAutoColorbalance_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image;
				processor1.AutoColorBalance();
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}

		}

		private void cmdAdjustColorBalance_Click(object sender, System.EventArgs e)
		{
	
			try 
			{
				processor1.Image = imageXView1.Image;
				processor1.AdjustColorBalance((short)System.Convert.ToInt32(cboColor.SelectedItem), (short)hscrRed.Value, (short)hscrGreen.Value, (short)hscrBlue.Value);
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}
		
	}
}

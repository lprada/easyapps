Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports PegasusImaging.WinForms.ImagXpress8
Namespace Threading

	Public Class ProcessorThread
		Private myReferenceToMyLabel As System.Windows.Forms.Label
		Private myReferenceToMyViewer As PegasusImaging.WinForms.ImagXpress8.ImageXView
		Private myImageFileName As System.String
		Private myResize As System.Int32
		Private myImage As PegasusImaging.WinForms.ImagXpress8.ImageX
		Private myProcessor As PegasusImaging.WinForms.ImagXpress8.Processor

		Shared Sub PegasusError(ByVal e As PegasusImaging.WinForms.ImagXpress8.ImagXpressException)
			MessageBox.Show(Nothing, e.Message, "ImageXPress Error " + e.Number.ToString)
		End Sub

		Public Sub New(ByVal daImageFileName As System.String, ByVal daViewer As PegasusImaging.WinForms.ImagXpress8.ImageXView, ByVal daLabel As System.Windows.Forms.Label, ByVal daSize As System.Int32)
			Me.myReferenceToMyViewer = daViewer
			Me.myReferenceToMyLabel = daLabel
			Me.myImageFileName = CType(daImageFileName.Clone, String)
			Me.myResize = daSize
		End Sub

		Private Delegate Sub SafeContextImageChanger(ByVal newImage As PegasusImaging.WinForms.ImagXpress8.ImageX)

		Private Delegate Sub SafeContextLabelChanger(ByVal newText As String)

		Private Delegate Sub SafeContextZoomChanger(ByVal newZoomFitType As PegasusImaging.WinForms.ImagXpress8.ZoomToFitType)

		Private Sub RealZoomToFitChangerInTheSafeContext(ByVal newZoomFitType As PegasusImaging.WinForms.ImagXpress8.ZoomToFitType)
			myReferenceToMyViewer.ZoomToFit(newZoomFitType)
		End Sub

		Private Sub RealImageChangerInTheSafeContext(ByVal newImage As PegasusImaging.WinForms.ImagXpress8.ImageX)
			Dim oldImage As PegasusImaging.WinForms.ImagXpress8.ImageX = myReferenceToMyViewer.Image
			myReferenceToMyViewer.Image = newImage
			If Not (oldImage Is Nothing) Then
				oldImage.Dispose
				oldImage = Nothing
			End If
			GC.Collect
			System.Threading.Thread.Sleep(25)
		End Sub

		Private Sub RealLabelChangerInTheSafeContext(ByVal newText As String)
			myReferenceToMyLabel.Text = newText
		End Sub

		Private Sub ChangeLabelText(ByVal newText As String)
			Dim theArgs As Object() =  {newText}
            myReferenceToMyLabel.Invoke(New SafeContextLabelChanger(AddressOf RealLabelChangerInTheSafeContext), theArgs)
		End Sub

		Private Sub ChangeImage(ByVal newImage As PegasusImaging.WinForms.ImagXpress8.ImageX)
			Dim theArgs As Object() =  {newImage.Copy}
            myReferenceToMyViewer.Invoke(New SafeContextImageChanger(AddressOf RealImageChangerInTheSafeContext), theArgs)
		End Sub

		Private Sub ChangeZoomToFitType(ByVal newZoomFitType As PegasusImaging.WinForms.ImagXpress8.ZoomToFitType)
			Dim theArgs As Object() =  {newZoomFitType}
            myReferenceToMyViewer.Invoke(New SafeContextZoomChanger(AddressOf RealZoomToFitChangerInTheSafeContext), theArgs)
		End Sub

		Public Sub Process()
			Dim imageOpened As Boolean = False
			Dim TryCount As Integer = 0
			While (Not imageOpened) AndAlso (TryCount < 25)
				Try
					myImage = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(myImageFileName)
					imageOpened = True
				Catch ex As ImagXpressException
					ChangeLabelText("Error opening image - " + ex.Number.ToString + ". Retrying...")
				End Try
				System.Math.Min(System.Threading.Interlocked.Increment(TryCount),TryCount-1)
			End While
			If TryCount >= 25 Then
				Throw New Exception("Unable to open the image for processing.")
			End If
			System.Threading.Thread.Sleep(10)
			Try
				myProcessor = New PegasusImaging.WinForms.ImagXpress8.Processor(myImage)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			Try
				ChangeImage(myImage)
				ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("Processing...")
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("Resizing...")
			Try
				myProcessor.Resize(New System.Drawing.Size(myImage.ImageXData.Width * (myResize), myImage.ImageXData.Height * myResize), PegasusImaging.WinForms.ImagXpress8.ResizeType.Quality)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			Try
				ChangeImage(myImage)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("Blurring...")
			Try
				myProcessor.Blur
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			Try
				ChangeImage(myImage)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("Negating...")
			Try
				myProcessor.Negate
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			Try
				ChangeImage(myImage)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("Zooming...")
			Try
				ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitWidth)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			Try
				ChangeImage(myImage)
				ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("(Un)Negating...")
			Try
				myProcessor.Negate
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			Try
				ChangeImage(myImage)
			Catch ex As ImagXpressException
				PegasusError(ex)
			End Try
			System.Threading.Thread.Sleep(10)
			ChangeLabelText("Processed.")
			System.Threading.Thread.Sleep(10)
			myProcessor.Dispose
			myProcessor = Nothing
			myImage.Dispose
			myImage = Nothing
			GC.Collect
			System.Threading.Thread.Sleep(100)
		End Sub
	End Class 
End Namespace


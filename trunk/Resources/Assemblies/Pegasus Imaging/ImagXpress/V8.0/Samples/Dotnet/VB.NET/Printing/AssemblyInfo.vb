'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Runtime.CompilerServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Printing")> 
<Assembly: AssemblyDescription("VB.NET Printing sample")> 
<Assembly: AssemblyCompany("Pegasus Imaging Corporation")> 
<Assembly: AssemblyProduct("ImagXpress8")> 
<Assembly: AssemblyCopyright("Copyright � 2006. All rights reserved.")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 
<Assembly: ComVisible(False)> 
<Assembly: SecurityPermissionAttribute(SecurityAction.RequestMinimum)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("7E4AE9A5-6FE1-4149-83F6-20F544E2AEEA")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 

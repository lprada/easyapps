using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Drawing.Printing;
using PegasusImaging.WinForms.ImagXpress8;

namespace PrintingImages
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class printing : System.Windows.Forms.Form
	{
		private System.String strCurrentDir;
		private System.String strCurrentImage;
		private System.Windows.Forms.Button cmdprint;
		
		private System.Drawing.Printing.PrintDocument pd;
		private PegasusImaging.WinForms.ImagXpress8.Processor processor1;
		private PegasusImaging.WinForms.ImagXpress8.LoadOptions loLoadOptions;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnufile;
		private System.Windows.Forms.MenuItem mnuopen;
		private System.Windows.Forms.MenuItem mnuquit;

		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.RichTextBox lblinfo;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.MenuItem mnuAboutBox;
		private System.Windows.Forms.ListBox lstStatus;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress2;
		private System.Windows.Forms.Label lsterror;
		private System.Windows.Forms.Label status;
		private System.Windows.Forms.MenuItem menuItem1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public printing()
		{
			//
			// Required for Windows Form Designer support
			
			//****Must call the UnlockControl function *****
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
			InitializeComponent();
			
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			imagXpress2.Dispose();
			imageXView1.Dispose();
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdprint = new System.Windows.Forms.Button();
			this.pd = new System.Drawing.Printing.PrintDocument();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnufile = new System.Windows.Forms.MenuItem();
			this.mnuopen = new System.Windows.Forms.MenuItem();
			this.mnuquit = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.mnuAboutBox = new System.Windows.Forms.MenuItem();
			this.lblinfo = new System.Windows.Forms.RichTextBox();
			this.lblError = new System.Windows.Forms.Label();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress2 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.lsterror = new System.Windows.Forms.Label();
			this.status = new System.Windows.Forms.Label();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// cmdprint
			// 
			this.cmdprint.Location = new System.Drawing.Point(216, 400);
			this.cmdprint.Name = "cmdprint";
			this.cmdprint.Size = new System.Drawing.Size(112, 32);
			this.cmdprint.TabIndex = 0;
			this.cmdprint.Text = "Print Image";
			this.cmdprint.Click += new System.EventHandler(this.cmdprint_Click);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnufile,
																					  this.mnuAbout,
																					  this.mnuAboutBox});
			// 
			// mnufile
			// 
			this.mnufile.Index = 0;
			this.mnufile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuopen,
																					this.menuItem1,
																					this.mnuquit});
			this.mnufile.Text = "&File";
			// 
			// mnuopen
			// 
			this.mnuopen.Index = 0;
			this.mnuopen.Text = "&Open";
			this.mnuopen.Click += new System.EventHandler(this.mnuopen_Click);
			// 
			// mnuquit
			// 
			this.mnuquit.Index = 2;
			this.mnuquit.Text = "&Quit";
			this.mnuquit.Click += new System.EventHandler(this.mnuquit_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 1;
			this.mnuAbout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuItem2});
			this.mnuAbout.Text = "&Toolbar";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "&Show";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// mnuAboutBox
			// 
			this.mnuAboutBox.Index = 2;
			this.mnuAboutBox.Text = "&About";
			this.mnuAboutBox.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// lblinfo
			// 
			this.lblinfo.Location = new System.Drawing.Point(24, 8);
			this.lblinfo.Name = "lblinfo";
			this.lblinfo.Size = new System.Drawing.Size(496, 64);
			this.lblinfo.TabIndex = 2;
			this.lblinfo.Text = "This example demonstrates the following:\n1) Using the ImagXpress Print method and" +
				" the .NET PrintPageEventHandler.";
			// 
			// lblError
			// 
			this.lblError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblError.Location = new System.Drawing.Point(288, 80);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(320, 48);
			this.lblError.TabIndex = 3;
			// 
			// lstStatus
			// 
			this.lstStatus.Location = new System.Drawing.Point(352, 168);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(240, 199);
			this.lstStatus.TabIndex = 4;
			// 
			// imageXView1
			// 
			this.imageXView1.AutoScroll = true;
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView1.Location = new System.Drawing.Point(24, 160);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(304, 224);
			this.imageXView1.TabIndex = 5;
			// 
			// lsterror
			// 
			this.lsterror.Location = new System.Drawing.Point(208, 80);
			this.lsterror.Name = "lsterror";
			this.lsterror.Size = new System.Drawing.Size(80, 24);
			this.lsterror.TabIndex = 6;
			this.lsterror.Text = "Last Error:";
			// 
			// status
			// 
			this.status.Location = new System.Drawing.Point(352, 136);
			this.status.Name = "status";
			this.status.Size = new System.Drawing.Size(232, 24);
			this.status.TabIndex = 7;
			this.status.Text = "Load Status";
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 1;
			this.menuItem1.Text = "-";
			// 
			// printing
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 477);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.status,
																		  this.lsterror,
																		  this.imageXView1,
																		  this.lstStatus,
																		  this.lblError,
																		  this.lblinfo,
																		  this.cmdprint});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Menu = this.mainMenu1;
			this.Name = "printing";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ImagXpress 8 - C# Printing";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new printing());
		}

		private void cmdprint_Click(object sender, System.EventArgs e)
		{
			// Assumes the default printer is selected.
			PrintDocument pd = new PrintDocument();
			pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
			pd.Print();
			
		}
		private void Form1_Load(object sender, System.EventArgs e)
		{

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress2.License.UnlockRuntime(1234,1234,1234,1234);

			loLoadOptions = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
			PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
			PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
		
			//here we set the current directory and image to initially load
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString (cultNumber);
			strCurrentImage = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\window.jpg");
			strCurrentDir = strCommonImagesDirectory;


			if (System.IO.File.Exists(strCurrentImage)) 
			{
				try 
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage,loLoadOptions);
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}
		}

		private void pd_PrintPage(object sender, PrintPageEventArgs ev) 
		{     
			//****set the Graphics Units to inches
			GraphicsUnit units = GraphicsUnit.Inch;
			//set the size of the rectangle to the margins bounds of the page
			Rectangle destRect = new Rectangle(ev.MarginBounds.Left,ev.MarginBounds.Top,ev.MarginBounds.Width,ev.MarginBounds.Height);
			//send the data to the printer
			imageXView1.Print(ev.Graphics,destRect);
			ev.HasMorePages = false;
			
		}
		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			imagXpress2.AboutBox();

		}

		private void mnuquit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();

		}
			
		
		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
				lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
					lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
				lstStatus.SelectedIndex = 	lstStatus.Items.Count - 1;
		}


		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}

		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress2.AboutBox();	
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}
	
		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			this.menuItem2.Text = (imageXView1.Toolbar.Activated) ? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}

		private void mnuopen_Click(object sender, System.EventArgs e)
		{

			System.String strLoadResult = PegasusOpenFile();

			PegasusImaging.WinForms.ImagXpress8.ImageX iImage;
	
			//we check first to make sure the file is valid
		
			if (strLoadResult.Length != 0) 
			{
				//If it is valid, we set our internal image filename equal to it
				strCurrentImage = strLoadResult;
			}
			try 
			{ 
			  //display the image in the control
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage,loLoadOptions);
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
			catch (System.IO.IOException ex) 
			{
				PegasusError(ex,lblError);
			}
		}
	
		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}
		#endregion
	}
	
}

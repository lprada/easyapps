'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Imports System.Drawing.Printing

Public Class Form1
    Inherits System.Windows.Forms.Form

    'File I/O Variables

    Dim strCurrentDir As System.String
    Dim strimageFile As System.String
    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'Must call the UnlockControl
        ' PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234)
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            'Dispose of any ImagXpress objects created
            If Not (view Is Nothing) Then
                view.Dispose()
                view = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If


            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If


        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents view As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents pd As System.Drawing.Printing.PrintDocument
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents mnuToolFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolFileOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolFileClose As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents lststatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoad As System.Windows.Forms.Label
    Friend WithEvents lblErrDesc As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.view = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuToolFile = New System.Windows.Forms.MenuItem()
        Me.mnuToolFileOpen = New System.Windows.Forms.MenuItem()
        Me.mnuToolFileClose = New System.Windows.Forms.MenuItem()
        Me.mnuTool = New System.Windows.Forms.MenuItem()
        Me.mnuToolShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.pd = New System.Drawing.Printing.PrintDocument()
        Me.cmdPrint = New System.Windows.Forms.Button()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.lststatus = New System.Windows.Forms.ListBox()
        Me.lblLoad = New System.Windows.Forms.Label()
        Me.lblErrDesc = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'view
        '
        Me.view.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.view.AutoScroll = True
        Me.view.Location = New System.Drawing.Point(24, 112)
        Me.view.Name = "view"
        Me.view.Size = New System.Drawing.Size(416, 304)
        Me.view.TabIndex = 0
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolFile, Me.mnuTool, Me.mnuAbout})
        '
        'mnuToolFile
        '
        Me.mnuToolFile.Index = 0
        Me.mnuToolFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolFileOpen, Me.mnuToolFileClose})
        Me.mnuToolFile.Text = "&File"
        '
        'mnuToolFileOpen
        '
        Me.mnuToolFileOpen.Index = 0
        Me.mnuToolFileOpen.Text = "&Open"
        '
        'mnuToolFileClose
        '
        Me.mnuToolFileClose.Index = 1
        Me.mnuToolFileClose.Text = "&Quit"
        '
        'mnuTool
        '
        Me.mnuTool.Index = 1
        Me.mnuTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolShow})
        Me.mnuTool.Text = "&ToolBar"
        '
        'mnuToolShow
        '
        Me.mnuToolShow.Index = 0
        Me.mnuToolShow.Text = "Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'cmdPrint
        '
        Me.cmdPrint.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdPrint.Location = New System.Drawing.Point(168, 456)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(136, 40)
        Me.cmdPrint.TabIndex = 1
        Me.cmdPrint.Text = "Print Images"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.RichTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox1.Location = New System.Drawing.Point(8, 8)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(648, 64)
        Me.RichTextBox1.TabIndex = 2
        Me.RichTextBox1.Text = "This example demonstrates the following:" & Microsoft.VisualBasic.ChrW(10) & "1) Using the ImagXpress Print method and" & _
        " the .NET PrintPageEventHandler."
        '
        'lblerror
        '
        Me.lblerror.Location = New System.Drawing.Point(480, 120)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(184, 88)
        Me.lblerror.TabIndex = 3
        '
        'lststatus
        '
        Me.lststatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)
        Me.lststatus.Location = New System.Drawing.Point(480, 240)
        Me.lststatus.Name = "lststatus"
        Me.lststatus.Size = New System.Drawing.Size(160, 264)
        Me.lststatus.TabIndex = 4
        '
        'lblLoad
        '
        Me.lblLoad.Location = New System.Drawing.Point(480, 216)
        Me.lblLoad.Name = "lblLoad"
        Me.lblLoad.Size = New System.Drawing.Size(160, 16)
        Me.lblLoad.TabIndex = 5
        Me.lblLoad.Text = "Load Status:"
        '
        'lblErrDesc
        '
        Me.lblErrDesc.Location = New System.Drawing.Point(472, 80)
        Me.lblErrDesc.Name = "lblErrDesc"
        Me.lblErrDesc.Size = New System.Drawing.Size(152, 24)
        Me.lblErrDesc.TabIndex = 6
        Me.lblErrDesc.Text = "Last Error:"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(672, 521)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblErrDesc, Me.lblLoad, Me.lststatus, Me.lblerror, Me.RichTextBox1, Me.cmdPrint, Me.view})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VB.NET Printing"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


#End Region

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        Dim pd As New PrintDocument()
        AddHandler pd.PrintPage, AddressOf Me.pd_PrintPage
        pd.Print()
    End Sub

    Private Sub pd_PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        '****set the Graphics Units to inches
        Dim units As GraphicsUnit
        units = GraphicsUnit.Inch
        '****set the size of the rectangle to the margins bounds of the page
        Dim destrect As New Rectangle(ev.MarginBounds.Left, ev.MarginBounds.Top, ev.MarginBounds.Width, ev.MarginBounds.Height)
        '****send the data to the printer
        view.Print(ev.Graphics, destRect)
        ev.HasMorePages = False
    End Sub

    Private Sub mnuToolFileClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolFileClose.Click
        Application.Exit()

    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub mnuToolFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolFileOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then

            Try
                'clear error before next image load
                lblerror.Text = ""
                strimageFile = strtemp
                view.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '***Must call UnlockRuntime to Distribute Application*** 
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        imagX1 = New PegasusImaging.WinForms.ImagXpress8.ImageX()

        '***** Event handlers
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

        Try
            'here we set the current directory and image so that the file open dialog box works well
            strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString()
            strimageFile = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\vermont.jpg")
            strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
            view.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub mnuToolShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolShow.Click
        If view.Toolbar.Activated = True Then
            mnuToolShow.Text = "Show"
            view.Toolbar.Activated = False
        Else
            mnuToolShow.Text = "Hide"
            view.Toolbar.Activated = True

        End If
    End Sub


    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lststatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lststatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
            lststatus.SelectedIndex = (lststatus.Items.Count - 1)
        End If

    End Sub


    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        If (e.Status.Decoded) Then
            lststatus.Items.Add(e.Status.ToString)
        End If
        lststatus.SelectedIndex = (lststatus.Items.Count - 1)

    End Sub

End Class

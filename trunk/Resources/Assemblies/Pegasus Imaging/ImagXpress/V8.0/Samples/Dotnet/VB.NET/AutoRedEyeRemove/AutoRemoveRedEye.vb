
'****************************************************************
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Namespace AutoRemoveRedEye

	Public Class AutoRemoveRedEye
	Inherits System.Windows.Forms.Form
		Private loLoadOptions As PegasusImaging.WinForms.ImagXpress8.LoadOptions
		Private imageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
		Private imagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
		Private prc As PegasusImaging.WinForms.ImagXpress8.Processor
		Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
		Private WithEvents cmdRemove As System.Windows.Forms.Button
		Private mainMenu1 As System.Windows.Forms.MainMenu
		Private menuItem1 As System.Windows.Forms.MenuItem
		Private lblStatus As System.Windows.Forms.Label
		Private lblerror As System.Windows.Forms.Label
		Private WithEvents mnuOpen As System.Windows.Forms.MenuItem
		Private menuItem2 As System.Windows.Forms.MenuItem
		Private WithEvents mnuQuit As System.Windows.Forms.MenuItem
		Private mnuToolBar As System.Windows.Forms.MenuItem
		Private WithEvents mnuShow As System.Windows.Forms.MenuItem
		Private WithEvents mnuAbout As System.Windows.Forms.MenuItem
		Private liststatus As System.Windows.Forms.ListBox
		Private lsterror As System.Windows.Forms.Label
		Private strCurrentDir As System.String
		Private strImageFile As System.String
		Private redeyex As System.Int32
		Private redeyey As System.Int32
		Private redeyewidth As System.Int32
		Private redeyeheight As System.Int32
		Private cmbGlare As System.Windows.Forms.ComboBox
		Private cmbShade As System.Windows.Forms.ComboBox
		Private lblGlare As System.Windows.Forms.Label
		Private lblShade As System.Windows.Forms.Label
		Private lstDesc As System.Windows.Forms.ListBox
		Private grpRedResult As System.Windows.Forms.GroupBox
		Private lblCount As System.Windows.Forms.Label
		Private RedEyeIndex As System.Windows.Forms.ColumnHeader
		Private XPos As System.Windows.Forms.ColumnHeader
		Private YPos As System.Windows.Forms.ColumnHeader
		Private resultsList As System.Windows.Forms.ListView
		Private lblRedDesc As System.Windows.Forms.Label
		Private AreaWidth As System.Windows.Forms.ColumnHeader
		Private AreaHeight As System.Windows.Forms.ColumnHeader
		Private components As System.ComponentModel.Container = Nothing

        Public Sub New()

            'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)

            InitializeComponent()
        End Sub

        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (imagXpress1 Is Nothing) Then
                    imagXpress1.Dispose()
                    imagXpress1 = Nothing
                End If
                If Not (imageXView1 Is Nothing) Then
                    imageXView1.Dispose()
                    imageXView1 = Nothing
                End If
                If Not (prc Is Nothing) Then
                    prc.Dispose()
                    prc = Nothing
                End If
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private Sub InitializeComponent()
            Me.imageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            Me.imagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
            Me.liststatus = New System.Windows.Forms.ListBox()
            Me.lstDesc = New System.Windows.Forms.ListBox()
            Me.cmdRemove = New System.Windows.Forms.Button()
            Me.mainMenu1 = New System.Windows.Forms.MainMenu()
            Me.menuItem1 = New System.Windows.Forms.MenuItem()
            Me.mnuOpen = New System.Windows.Forms.MenuItem()
            Me.menuItem2 = New System.Windows.Forms.MenuItem()
            Me.mnuQuit = New System.Windows.Forms.MenuItem()
            Me.mnuToolBar = New System.Windows.Forms.MenuItem()
            Me.mnuShow = New System.Windows.Forms.MenuItem()
            Me.mnuAbout = New System.Windows.Forms.MenuItem()
            Me.lblStatus = New System.Windows.Forms.Label()
            Me.lblerror = New System.Windows.Forms.Label()
            Me.lsterror = New System.Windows.Forms.Label()
            Me.cmbGlare = New System.Windows.Forms.ComboBox()
            Me.cmbShade = New System.Windows.Forms.ComboBox()
            Me.lblGlare = New System.Windows.Forms.Label()
            Me.lblShade = New System.Windows.Forms.Label()
            Me.grpRedResult = New System.Windows.Forms.GroupBox()
            Me.lblRedDesc = New System.Windows.Forms.Label()
            Me.resultsList = New System.Windows.Forms.ListView()
            Me.RedEyeIndex = New System.Windows.Forms.ColumnHeader()
            Me.YPos = New System.Windows.Forms.ColumnHeader()
            Me.XPos = New System.Windows.Forms.ColumnHeader()
            Me.AreaWidth = New System.Windows.Forms.ColumnHeader()
            Me.AreaHeight = New System.Windows.Forms.ColumnHeader()
            Me.lblCount = New System.Windows.Forms.Label()
            Me.grpRedResult.SuspendLayout()
            Me.SuspendLayout()
            '
            'imageXView1
            '
            Me.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
            Me.imageXView1.AutoScroll = True
            Me.imageXView1.Location = New System.Drawing.Point(16, 80)
            Me.imageXView1.Name = "imageXView1"
            Me.imageXView1.Size = New System.Drawing.Size(288, 384)
            Me.imageXView1.TabIndex = 0
            '
            'liststatus
            '
            Me.liststatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.liststatus.Location = New System.Drawing.Point(584, 96)
            Me.liststatus.Name = "liststatus"
            Me.liststatus.Size = New System.Drawing.Size(168, 160)
            Me.liststatus.TabIndex = 1
            '
            'lstDesc
            '
            Me.lstDesc.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.lstDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lstDesc.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using the AutoRemoveRedEye method in the ImagXpress 8 control.", "2)Using the RedeyeCollection Class to retrieve information about the red eyes fou" & _
            "nd in the image."})
            Me.lstDesc.Location = New System.Drawing.Point(16, 8)
            Me.lstDesc.Name = "lstDesc"
            Me.lstDesc.Size = New System.Drawing.Size(736, 56)
            Me.lstDesc.TabIndex = 2
            '
            'cmdRemove
            '
            Me.cmdRemove.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.cmdRemove.Location = New System.Drawing.Point(376, 224)
            Me.cmdRemove.Name = "cmdRemove"
            Me.cmdRemove.Size = New System.Drawing.Size(136, 32)
            Me.cmdRemove.TabIndex = 3
            Me.cmdRemove.Text = "AutoRemoveRedEye"
            '
            'mainMenu1
            '
            Me.mainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItem1, Me.mnuToolBar, Me.mnuAbout})
            '
            'menuItem1
            '
            Me.menuItem1.Index = 0
            Me.menuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.menuItem2, Me.mnuQuit})
            Me.menuItem1.Text = "&File"
            '
            'mnuOpen
            '
            Me.mnuOpen.Index = 0
            Me.mnuOpen.Text = "&Open"
            '
            'menuItem2
            '
            Me.menuItem2.Index = 1
            Me.menuItem2.Text = "-"
            '
            'mnuQuit
            '
            Me.mnuQuit.Index = 2
            Me.mnuQuit.Text = "&Quit"
            '
            'mnuToolBar
            '
            Me.mnuToolBar.Index = 1
            Me.mnuToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuShow})
            Me.mnuToolBar.Text = "&Toolbar"
            '
            'mnuShow
            '
            Me.mnuShow.Index = 0
            Me.mnuShow.Text = "&Show"
            '
            'mnuAbout
            '
            Me.mnuAbout.Index = 2
            Me.mnuAbout.Text = "&About"
            '
            'lblStatus
            '
            Me.lblStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblStatus.Location = New System.Drawing.Point(584, 72)
            Me.lblStatus.Name = "lblStatus"
            Me.lblStatus.Size = New System.Drawing.Size(168, 16)
            Me.lblStatus.TabIndex = 4
            Me.lblStatus.Text = "Load Status:"
            Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            '
            'lblerror
            '
            Me.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblerror.Location = New System.Drawing.Point(320, 96)
            Me.lblerror.Name = "lblerror"
            Me.lblerror.Size = New System.Drawing.Size(248, 56)
            Me.lblerror.TabIndex = 5
            '
            'lsterror
            '
            Me.lsterror.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
            Me.lsterror.Location = New System.Drawing.Point(320, 72)
            Me.lsterror.Name = "lsterror"
            Me.lsterror.Size = New System.Drawing.Size(112, 16)
            Me.lsterror.TabIndex = 6
            Me.lsterror.Text = "Last Error Reported:"
            '
            'cmbGlare
            '
            Me.cmbGlare.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.cmbGlare.Items.AddRange(New Object() {"None", "Slight", "Full"})
            Me.cmbGlare.Location = New System.Drawing.Point(320, 192)
            Me.cmbGlare.Name = "cmbGlare"
            Me.cmbGlare.Size = New System.Drawing.Size(112, 21)
            Me.cmbGlare.TabIndex = 7
            '
            'cmbShade
            '
            Me.cmbShade.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.cmbShade.Items.AddRange(New Object() {"Normal", "Light", "Dark"})
            Me.cmbShade.Location = New System.Drawing.Point(456, 192)
            Me.cmbShade.Name = "cmbShade"
            Me.cmbShade.Size = New System.Drawing.Size(112, 21)
            Me.cmbShade.TabIndex = 8
            '
            'lblGlare
            '
            Me.lblGlare.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblGlare.Location = New System.Drawing.Point(320, 168)
            Me.lblGlare.Name = "lblGlare"
            Me.lblGlare.Size = New System.Drawing.Size(112, 16)
            Me.lblGlare.TabIndex = 9
            Me.lblGlare.Text = "Glare Settings"
            Me.lblGlare.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            '
            'lblShade
            '
            Me.lblShade.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblShade.Location = New System.Drawing.Point(456, 168)
            Me.lblShade.Name = "lblShade"
            Me.lblShade.Size = New System.Drawing.Size(112, 16)
            Me.lblShade.TabIndex = 10
            Me.lblShade.Text = "Eye Shade"
            Me.lblShade.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            '
            'grpRedResult
            '
            Me.grpRedResult.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.grpRedResult.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblRedDesc, Me.resultsList, Me.lblCount})
            Me.grpRedResult.Location = New System.Drawing.Point(312, 264)
            Me.grpRedResult.Name = "grpRedResult"
            Me.grpRedResult.Size = New System.Drawing.Size(440, 200)
            Me.grpRedResult.TabIndex = 11
            Me.grpRedResult.TabStop = False
            Me.grpRedResult.Text = "RedEye Results"
            '
            'lblRedDesc
            '
            Me.lblRedDesc.Location = New System.Drawing.Point(24, 32)
            Me.lblRedDesc.Name = "lblRedDesc"
            Me.lblRedDesc.Size = New System.Drawing.Size(160, 16)
            Me.lblRedDesc.TabIndex = 2
            Me.lblRedDesc.Text = "Number of Red Eyes Found:"
            '
            'resultsList
            '
            Me.resultsList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.RedEyeIndex, Me.YPos, Me.XPos, Me.AreaWidth, Me.AreaHeight})
            Me.resultsList.Location = New System.Drawing.Point(16, 56)
            Me.resultsList.Name = "resultsList"
            Me.resultsList.Size = New System.Drawing.Size(408, 128)
            Me.resultsList.TabIndex = 1
            Me.resultsList.View = System.Windows.Forms.View.Details
            '
            'RedEyeIndex
            '
            Me.RedEyeIndex.Text = "RedEyeIndex"
            Me.RedEyeIndex.Width = 85
            '
            'YPos
            '
            Me.YPos.Text = "YPos"
            '
            'XPos
            '
            Me.XPos.Text = "XPos"
            '
            'AreaWidth
            '
            Me.AreaWidth.Text = "AreaWidth"
            Me.AreaWidth.Width = 100
            '
            'AreaHeight
            '
            Me.AreaHeight.Text = "AreaHeight"
            Me.AreaHeight.Width = 100
            '
            'lblCount
            '
            Me.lblCount.Location = New System.Drawing.Point(192, 32)
            Me.lblCount.Name = "lblCount"
            Me.lblCount.Size = New System.Drawing.Size(152, 16)
            Me.lblCount.TabIndex = 0
            '
            'AutoRemoveRedEye
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(768, 481)
            Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.grpRedResult, Me.lblShade, Me.lblGlare, Me.cmbShade, Me.cmbGlare, Me.lsterror, Me.lblerror, Me.lblStatus, Me.cmdRemove, Me.lstDesc, Me.liststatus, Me.imageXView1})
            Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Menu = Me.mainMenu1
            Me.Name = "AutoRemoveRedEye"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "AutoRemoveRedEye"
            Me.grpRedResult.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub

        <STAThread()> _
        Shared Sub Main()
            Application.Run(New AutoRemoveRedEye())
        End Sub

        Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
            resultsList.Items.Clear()
            lblCount.Text = ""
            Dim gglare As PegasusImaging.WinForms.ImagXpress8.RedeyeGlare = CType(cmbGlare.SelectedIndex, PegasusImaging.WinForms.ImagXpress8.RedeyeGlare)
            Dim sshade As PegasusImaging.WinForms.ImagXpress8.RedeyeShade = CType(cmbShade.SelectedIndex, PegasusImaging.WinForms.ImagXpress8.RedeyeShade)
            prc = New PegasusImaging.WinForms.ImagXpress8.Processor(imageXView1.Image)
            Dim myRedEyes As PegasusImaging.WinForms.ImagXpress8.RedeyeCollection = New PegasusImaging.WinForms.ImagXpress8.RedeyeCollection()
            prc.Redeyes = myRedEyes
            prc.AutoRemoveRedeye(sshade, gglare, False)
            lblCount.Text = myRedEyes.Count.ToString
            Dim counter As Integer = 0
            While counter < myRedEyes.Count
                redeyex = myRedEyes.GetRedeyeRectangle(counter).Xposition
                redeyey = myRedEyes.GetRedeyeRectangle(counter).Yposition
                redeyewidth = myRedEyes.GetRedeyeRectangle(counter).Width
                redeyeheight = myRedEyes.GetRedeyeRectangle(counter).Height
                resultsList.Items.Add(New ListViewItem(New String() {counter.ToString, redeyex.ToString, redeyey.ToString, redeyewidth.ToString, redeyeheight.ToString}))
                System.Math.Min(System.Threading.Interlocked.Increment(counter), counter - 1)
            End While
            prc.Dispose()
            prc = Nothing
        End Sub
        Private cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
        Private cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
        Private cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
        Private Const strDefaultImageFilter As System.String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"
        Private Const strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"

        Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source + "" & Microsoft.VisualBasic.Chr(10) & ""
        End Sub

        Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = ErrorException.Message + "" & Microsoft.VisualBasic.Chr(10) & "" + ErrorException.Source + "" & Microsoft.VisualBasic.Chr(10) & "" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat)
        End Sub

        Function PegasusOpenFile() As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strDefaultImageFilter
            dlg.InitialDirectory = strCurrentDir
            If dlg.ShowDialog = DialogResult.OK Then
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), dlg.FileName.Length - dlg.FileName.LastIndexOf("\"))
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

        Function PegasusOpenFile(ByVal strFilter As System.String) As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strFilter
            dlg.InitialDirectory = strCurrentDir
            If dlg.ShowDialog = DialogResult.OK Then
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), dlg.FileName.Length - dlg.FileName.LastIndexOf("\"))
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

        Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
            Dim iTmp As System.Int32
            Try
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
            Catch ex As System.NullReferenceException
                PegasusError(ex, lblerror)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            Catch ex As System.Exception
                PegasusError(ex, lblerror)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            End Try
            If (iTmp < scrScroll.Maximum) AndAlso (iTmp > scrScroll.Minimum) Then
                scrScroll.Value = iTmp
            Else
                iTmp = scrScroll.Value
            End If
            textTextBox.Text = iTmp.ToString(cultNumber)
        End Sub

        Private Sub ImageStatusEventHandler(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)
            liststatus.Items.Add(e.Status.ToString(cultNumber))

            liststatus.SelectedIndex = liststatus.Items.Count - 1
        End Sub

        Private Sub ProgressEventHandler(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
            liststatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.")
            If e.IsComplete Then
                liststatus.Items.Add(e.TotalBytes.ToString(cultNumber) + " Bytes Completed Loading.")
            End If
            liststatus.SelectedIndex = liststatus.Items.Count - 1
        End Sub

        Private Sub AutoRemoveRedEye_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            '***Must call UnlockRuntime to Distribute Application*** 
            'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)

            cmbGlare.SelectedIndex = 0
            cmbShade.SelectedIndex = 0
            Try
                loLoadOptions = New PegasusImaging.WinForms.ImagXpress8.LoadOptions()
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)
            End Try
            Try
                AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEventHandler
                AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEventHandler
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)
            End Try
            Try
                imagX1 = New PegasusImaging.WinForms.ImagXpress8.ImageX()
                strCurrentDir = System.IO.Directory.GetCurrentDirectory.ToString
                strImageFile = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\redeye2.jpg")
                strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\..\Common\Images\")
                imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile, loLoadOptions)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)
            End Try
        End Sub

        Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
            imagXpress1.AboutBox()
        End Sub

        Private Sub mnuOpen_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click
            Dim strTmp As System.String = PegasusOpenFile()
            If Not (strTmp.Length = 0) Then
                Try

                    'clear out any error before next operation
                    lblerror.Text = ""

                    strImageFile = strTmp
                    Dim oldImage As PegasusImaging.WinForms.ImagXpress8.ImageX = imageXView1.Image
                    imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile, loLoadOptions)
                    If Not (oldImage Is Nothing) Then
                        oldImage.Dispose()
                        oldImage = Nothing
                    End If
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.IOException
                    PegasusError(ex, lblerror)
                End Try
                resultsList.Items.Clear()
            End If
        End Sub

        Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
            Application.Exit()
        End Sub

        Private Sub mnuShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShow.Click
            Me.mnuShow.Text = Microsoft.VisualBasic.IIf((imageXView1.Toolbar.Activated), "&Show", "&Hide")
            Try
                imageXView1.Toolbar.Activated = Not imageXView1.Toolbar.Activated
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblerror)
            End Try
        End Sub
    End Class
End Namespace


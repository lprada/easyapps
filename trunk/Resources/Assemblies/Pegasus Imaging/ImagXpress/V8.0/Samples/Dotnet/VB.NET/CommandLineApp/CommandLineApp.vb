'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8

Module Module1

    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Private imagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Private imagProcessor As PegasusImaging.WinForms.ImagXpress8.Processor
    Private sInputFileName As String
    Private sOutputFileName As String
    Private soSaveOptions As PegasusImaging.WinForms.ImagXpress8.SaveOptions


    Sub Main()

        UnlockIXandProcessImg()

    End Sub

    Sub UnlockIXandProcessImg()

        Try

            Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()

            'use your ImagXpress 8.0 unlock codes here
            'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)

            imagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()

            'use your ImagXpress 8.0 unlock codes here    
            'imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)

            System.Console.WriteLine("ImagXpress 8.0 successfully unlocked.")

            imagProcessor = New PegasusImaging.WinForms.ImagXpress8.Processor()
            soSaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()

            soSaveOptions.Format = ImageXFormat.Tiff
            soSaveOptions.Tiff.Compression = Compression.Group4

            sInputFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\Benefits.tif")
            sOutputFileName = strCurrentDir + "\BenefitsRotated.tif"

            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(sInputFileName)
            imagProcessor.Image = imagX1

            imagProcessor.Rotate(180)

            imagX1 = imagProcessor.Image
            imagX1.Save(sOutputFileName, soSaveOptions)
            Dispose()


            System.Console.WriteLine("Rotated TIFF saved to file " + sOutputFileName)
            System.Console.ReadLine()


        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException

            Dispose()
            System.Console.WriteLine(ex.Message)
            System.Console.WriteLine(ex.Source)
            System.Console.ReadLine()

        Catch ex As System.Exception

            Dispose()
            System.Console.WriteLine(ex.Message)
            System.Console.WriteLine(ex.Source)
            System.Console.ReadLine()

        End Try


    End Sub

    'Don't forget to Dispose ImagXpress
    Sub Dispose()

        If Not (imagXpress1 Is Nothing) Then

            imagXpress1.Dispose()
            imagXpress1 = Nothing

        End If

        If Not (imagProcessor Is Nothing) Then

            imagProcessor.Dispose()
            imagProcessor = Nothing

        End If

        If Not (imagX1 Is Nothing) Then

            imagX1.Dispose()
            imagX1 = Nothing

        End If

    End Sub

End Module

'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Public Class Form1
    Inherits System.Windows.Forms.Form

    Private imgFileName As String


    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            ' Don't forget to dispose IX
            '
            If Not (ImagXpress1 Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (Processor1 Is Nothing) Then

                Processor1.Dispose()
                Processor1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents docAutoColorBalance As System.Windows.Forms.TabPage
    Friend WithEvents docAdjustColorBalance As System.Windows.Forms.TabPage
    Friend WithEvents cmdAutoColorbalance As System.Windows.Forms.Button
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents lblRed As System.Windows.Forms.Label
    Friend WithEvents lblGreen As System.Windows.Forms.Label
    Friend WithEvents lblBlue As System.Windows.Forms.Label
    Friend WithEvents cboColor As System.Windows.Forms.ComboBox
    Friend WithEvents hscrRed As System.Windows.Forms.HScrollBar
    Friend WithEvents hscrGreen As System.Windows.Forms.HScrollBar
    Friend WithEvents lblRedVal As System.Windows.Forms.Label
    Friend WithEvents lblGreenVal As System.Windows.Forms.Label
    Friend WithEvents lblBlueVal As System.Windows.Forms.Label
    Friend WithEvents cmdAdjustColorBalance As System.Windows.Forms.Button
    Friend WithEvents hscrBlue As System.Windows.Forms.HScrollBar
    Friend WithEvents Processor1 As PegasusImaging.WinForms.ImagXpress8.Processor
    Friend WithEvents cmdReloadImage As System.Windows.Forms.Button
    Friend WithEvents mnuReloadCurrentImage As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuFileOpen = New System.Windows.Forms.MenuItem()
        Me.mnuReloadCurrentImage = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuQuit = New System.Windows.Forms.MenuItem()
        Me.mnuToolbar = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.docAutoColorBalance = New System.Windows.Forms.TabPage()
        Me.cmdAutoColorbalance = New System.Windows.Forms.Button()
        Me.docAdjustColorBalance = New System.Windows.Forms.TabPage()
        Me.cmdAdjustColorBalance = New System.Windows.Forms.Button()
        Me.lblBlueVal = New System.Windows.Forms.Label()
        Me.lblGreenVal = New System.Windows.Forms.Label()
        Me.lblRedVal = New System.Windows.Forms.Label()
        Me.hscrBlue = New System.Windows.Forms.HScrollBar()
        Me.hscrGreen = New System.Windows.Forms.HScrollBar()
        Me.hscrRed = New System.Windows.Forms.HScrollBar()
        Me.cboColor = New System.Windows.Forms.ComboBox()
        Me.lblBlue = New System.Windows.Forms.Label()
        Me.lblGreen = New System.Windows.Forms.Label()
        Me.lblRed = New System.Windows.Forms.Label()
        Me.lblColor = New System.Windows.Forms.Label()
        Me.Processor1 = New PegasusImaging.WinForms.ImagXpress8.Processor()
        Me.cmdReloadImage = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.docAutoColorBalance.SuspendLayout()
        Me.docAdjustColorBalance.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.Location = New System.Drawing.Point(16, 72)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(376, 464)
        Me.ImageXView1.TabIndex = 0
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(400, 424)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(248, 108)
        Me.lstStatus.TabIndex = 37
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(400, 392)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(136, 24)
        Me.lblLoadStatus.TabIndex = 36
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblError
        '
        Me.lblError.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblError.Location = New System.Drawing.Point(664, 440)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(184, 96)
        Me.lblError.TabIndex = 35
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(656, 392)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(160, 24)
        Me.lblLastError.TabIndex = 34
        Me.lblLastError.Text = "Last Error:"
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Adjusting the image color using the AutoLightness, AutoColorBalance, and Adjust" & _
        "ColorBalance methods."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(824, 56)
        Me.lstInfo.TabIndex = 33
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolbar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileOpen, Me.mnuReloadCurrentImage, Me.MenuItem3, Me.mnuQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Index = 0
        Me.mnuFileOpen.Text = "&Open Image..."
        '
        'mnuReloadCurrentImage
        '
        Me.mnuReloadCurrentImage.Index = 1
        Me.mnuReloadCurrentImage.Text = "Reload Current Image"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "-"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 3
        Me.mnuQuit.Text = "&Quit"
        '
        'mnuToolbar
        '
        Me.mnuToolbar.Index = 1
        Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolbar.Text = "&Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.TabControl1.Controls.AddRange(New System.Windows.Forms.Control() {Me.docAutoColorBalance, Me.docAdjustColorBalance})
        Me.TabControl1.Location = New System.Drawing.Point(424, 128)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(392, 248)
        Me.TabControl1.TabIndex = 38
        '
        'docAutoColorBalance
        '
        Me.docAutoColorBalance.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdAutoColorbalance})
        Me.docAutoColorBalance.Location = New System.Drawing.Point(4, 22)
        Me.docAutoColorBalance.Name = "docAutoColorBalance"
        Me.docAutoColorBalance.Size = New System.Drawing.Size(384, 222)
        Me.docAutoColorBalance.TabIndex = 1
        Me.docAutoColorBalance.Text = "AutoColorBalance"
        '
        'cmdAutoColorbalance
        '
        Me.cmdAutoColorbalance.Location = New System.Drawing.Point(128, 168)
        Me.cmdAutoColorbalance.Name = "cmdAutoColorbalance"
        Me.cmdAutoColorbalance.Size = New System.Drawing.Size(128, 32)
        Me.cmdAutoColorbalance.TabIndex = 0
        Me.cmdAutoColorbalance.Text = "Apply"
        '
        'docAdjustColorBalance
        '
        Me.docAdjustColorBalance.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdAdjustColorBalance, Me.lblBlueVal, Me.lblGreenVal, Me.lblRedVal, Me.hscrBlue, Me.hscrGreen, Me.hscrRed, Me.cboColor, Me.lblBlue, Me.lblGreen, Me.lblRed, Me.lblColor})
        Me.docAdjustColorBalance.Location = New System.Drawing.Point(4, 22)
        Me.docAdjustColorBalance.Name = "docAdjustColorBalance"
        Me.docAdjustColorBalance.Size = New System.Drawing.Size(384, 222)
        Me.docAdjustColorBalance.TabIndex = 2
        Me.docAdjustColorBalance.Text = "AdjustColorBalance"
        '
        'cmdAdjustColorBalance
        '
        Me.cmdAdjustColorBalance.Location = New System.Drawing.Point(144, 184)
        Me.cmdAdjustColorBalance.Name = "cmdAdjustColorBalance"
        Me.cmdAdjustColorBalance.Size = New System.Drawing.Size(120, 24)
        Me.cmdAdjustColorBalance.TabIndex = 11
        Me.cmdAdjustColorBalance.Text = "Apply"
        '
        'lblBlueVal
        '
        Me.lblBlueVal.Location = New System.Drawing.Point(336, 144)
        Me.lblBlueVal.Name = "lblBlueVal"
        Me.lblBlueVal.Size = New System.Drawing.Size(48, 16)
        Me.lblBlueVal.TabIndex = 10
        '
        'lblGreenVal
        '
        Me.lblGreenVal.Location = New System.Drawing.Point(336, 96)
        Me.lblGreenVal.Name = "lblGreenVal"
        Me.lblGreenVal.Size = New System.Drawing.Size(48, 24)
        Me.lblGreenVal.TabIndex = 9
        '
        'lblRedVal
        '
        Me.lblRedVal.Location = New System.Drawing.Point(336, 56)
        Me.lblRedVal.Name = "lblRedVal"
        Me.lblRedVal.Size = New System.Drawing.Size(48, 24)
        Me.lblRedVal.TabIndex = 8
        '
        'hscrBlue
        '
        Me.hscrBlue.LargeChange = 1
        Me.hscrBlue.Location = New System.Drawing.Point(104, 144)
        Me.hscrBlue.Minimum = -100
        Me.hscrBlue.Name = "hscrBlue"
        Me.hscrBlue.Size = New System.Drawing.Size(224, 16)
        Me.hscrBlue.TabIndex = 7
        '
        'hscrGreen
        '
        Me.hscrGreen.LargeChange = 1
        Me.hscrGreen.Location = New System.Drawing.Point(104, 96)
        Me.hscrGreen.Minimum = -100
        Me.hscrGreen.Name = "hscrGreen"
        Me.hscrGreen.Size = New System.Drawing.Size(224, 16)
        Me.hscrGreen.TabIndex = 6
        '
        'hscrRed
        '
        Me.hscrRed.LargeChange = 1
        Me.hscrRed.Location = New System.Drawing.Point(104, 56)
        Me.hscrRed.Minimum = -100
        Me.hscrRed.Name = "hscrRed"
        Me.hscrRed.Size = New System.Drawing.Size(224, 16)
        Me.hscrRed.TabIndex = 5
        '
        'cboColor
        '
        Me.cboColor.Location = New System.Drawing.Point(112, 16)
        Me.cboColor.Name = "cboColor"
        Me.cboColor.Size = New System.Drawing.Size(104, 21)
        Me.cboColor.TabIndex = 4
        '
        'lblBlue
        '
        Me.lblBlue.Location = New System.Drawing.Point(16, 144)
        Me.lblBlue.Name = "lblBlue"
        Me.lblBlue.Size = New System.Drawing.Size(72, 24)
        Me.lblBlue.TabIndex = 3
        Me.lblBlue.Text = "Blue:"
        '
        'lblGreen
        '
        Me.lblGreen.Location = New System.Drawing.Point(16, 96)
        Me.lblGreen.Name = "lblGreen"
        Me.lblGreen.Size = New System.Drawing.Size(64, 24)
        Me.lblGreen.TabIndex = 2
        Me.lblGreen.Text = "Green:"
        '
        'lblRed
        '
        Me.lblRed.Location = New System.Drawing.Point(16, 56)
        Me.lblRed.Name = "lblRed"
        Me.lblRed.Size = New System.Drawing.Size(64, 24)
        Me.lblRed.TabIndex = 1
        Me.lblRed.Text = "Red:"
        '
        'lblColor
        '
        Me.lblColor.Location = New System.Drawing.Point(16, 16)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(64, 24)
        Me.lblColor.TabIndex = 0
        Me.lblColor.Text = "Color:"
        '
        'Processor1
        '
        Me.Processor1.BackgroundColor = System.Drawing.Color.Black
        Me.Processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast
        Me.Processor1.ProgressPercent = 10
        Me.Processor1.Redeyes = Nothing
        '
        'cmdReloadImage
        '
        Me.cmdReloadImage.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.cmdReloadImage.Location = New System.Drawing.Point(520, 80)
        Me.cmdReloadImage.Name = "cmdReloadImage"
        Me.cmdReloadImage.Size = New System.Drawing.Size(168, 32)
        Me.cmdReloadImage.TabIndex = 39
        Me.cmdReloadImage.Text = "Reload Image"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(864, 553)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdReloadImage, Me.TabControl1, Me.lstStatus, Me.lblLoadStatus, Me.lblError, Me.lblLastError, Me.lstInfo, Me.ImageXView1})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "ColorBalance"
        Me.TabControl1.ResumeLayout(False)
        Me.docAutoColorBalance.ResumeLayout(False)
        Me.docAdjustColorBalance.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/



    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim strCurrentDir As System.String = "..\..\..\..\..\..\..\Common\Images\"
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)

        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))

            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If

    End Sub

    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        '
        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim i As Integer

        Try
            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)


            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

            ImageXView1.AutoScroll = True
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
        imgFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\Auto_Balance1.JPG")

        LoadFile()

        i = cboColor.Items.Add(0)
        i = cboColor.Items.Add(1)

        cboColor.SelectedIndex = 0

        lblBlueVal.Text = hscrBlue.Value.ToString()
        lblGreenVal.Text = hscrGreen.Value.ToString()
        lblRedVal.Text = hscrRed.Value.ToString()

    End Sub

    Private Sub LoadFile()
        Try

            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""

            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(imgFileName)

            If imagX1.ImageXData.BitsPerPixel <> 24 Then
                MessageBox.Show("Image must be 24-bit for sample methods. Converting image to 24-bit.")
                Processor1.Image = imagX1

                Processor1.ColorDepth(24, PaletteType.Fixed, DitherType.NoDither)

            End If

            ImageXView1.Image = imagX1


        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub


    Private Sub hscrRed_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrRed.Scroll
        lblRedVal.Text = hscrRed.Value.ToString()
    End Sub

    Private Sub hscrGreen_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrGreen.Scroll
        lblGreenVal.Text = hscrGreen.Value.ToString()
    End Sub

    Private Sub hscrBlue_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscrBlue.Scroll
        lblBlueVal.Text = hscrBlue.Value.ToString()
    End Sub

    Private Sub cmdAutoLightness_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try

            Processor1.Image = ImageXView1.Image
            Processor1.AutoLightness()

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub cmdAutoColorbalance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAutoColorbalance.Click

        Try

            Processor1.Image = ImageXView1.Image
            Processor1.AutoColorBalance()

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub cmdAdjustColorBalance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdjustColorBalance.Click
        Try

            Processor1.Image = ImageXView1.Image
            Processor1.AdjustColorBalance(cboColor.SelectedValue, hscrRed.Value, hscrGreen.Value, hscrBlue.Value)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click
        imgFileName = PegasusOpenFile()
        LoadFile()
    End Sub


    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
        If ImageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False

        Else
            mnuToolbarShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True
        End If
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub mnuReloadCurrentImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReloadCurrentImage.Click
        LoadFile()
    End Sub

    Private Sub cmdReloadImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReloadImage.Click
        LoadFile()
    End Sub

    Private Const colorScrollMinPct As Long = -100
    Private Const colorScrollMaxPct As Long = 100
    Private Const colorScrollMinVal As Long = -100
    Private Const colorScrollMaxVal As Long = 100

    Private Sub cboColor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboColor.SelectedIndexChanged
        Select Case cboColor.SelectedIndex
            Case 0
                hscrBlue.Minimum = colorScrollMinPct
                hscrBlue.Maximum = colorScrollMaxPct
                hscrRed.Minimum = colorScrollMinPct
                hscrRed.Maximum = colorScrollMaxPct
                hscrGreen.Minimum = colorScrollMinPct
                hscrGreen.Maximum = colorScrollMaxPct
            Case 1
                hscrBlue.Minimum = colorScrollMinVal
                hscrBlue.Maximum = colorScrollMaxVal
                hscrRed.Minimum = colorScrollMinVal
                hscrRed.Maximum = colorScrollMaxVal
                hscrGreen.Minimum = colorScrollMinVal
                hscrGreen.Maximum = colorScrollMaxVal
        End Select
        lblRedVal.Text = hscrRed.Value.ToString()
        lblGreenVal.Text = hscrGreen.Value.ToString()
        lblBlueVal.Text = hscrBlue.Value.ToString()
    End Sub
End Class


'****************************************************************
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/

Public Class frmMain
    Inherits System.Windows.Forms.Form

    Dim strCurrentDir As String
    Dim strImageFile As String
    Dim pRgnPath As New System.Drawing.Drawing2D.GraphicsPath()
    Dim loLoadOptions As PegasusImaging.WinForms.ImagXpress8.LoadOptions
    Dim prc As PegasusImaging.WinForms.ImagXpress8.Processor

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.

        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12, 12, 12, 12)
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            'Dispose of ImagXpress objects
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (prc Is Nothing) Then
                prc.Dispose()
                prc = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents cmdBlue As System.Windows.Forms.Button
    Friend WithEvents cmdYellow As System.Windows.Forms.Button
    Friend WithEvents cmdGrey As System.Windows.Forms.Button
    Friend WithEvents cmdRed As System.Windows.Forms.Button
    Friend WithEvents cmdPurple As System.Windows.Forms.Button
    Friend WithEvents cmdGreen As System.Windows.Forms.Button
    Friend WithEvents cmdShowImage As System.Windows.Forms.Button
    Friend WithEvents grpRegion As System.Windows.Forms.GroupBox
    Friend WithEvents grpArea As System.Windows.Forms.GroupBox
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.grpRegion = New System.Windows.Forms.GroupBox()
        Me.cmdGreen = New System.Windows.Forms.Button()
        Me.cmdPurple = New System.Windows.Forms.Button()
        Me.cmdRed = New System.Windows.Forms.Button()
        Me.cmdGrey = New System.Windows.Forms.Button()
        Me.cmdYellow = New System.Windows.Forms.Button()
        Me.cmdBlue = New System.Windows.Forms.Button()
        Me.grpArea = New System.Windows.Forms.GroupBox()
        Me.cmdShowImage = New System.Windows.Forms.Button()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuFileQuit = New System.Windows.Forms.MenuItem()
        Me.mnuToolbar = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.grpRegion.SuspendLayout()
        Me.grpArea.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.Location = New System.Drawing.Point(192, 112)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(384, 300)
        Me.ImageXView1.TabIndex = 1
        '
        'grpRegion
        '
        Me.grpRegion.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.grpRegion.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdGreen, Me.cmdPurple, Me.cmdRed, Me.cmdGrey, Me.cmdYellow, Me.cmdBlue})
        Me.grpRegion.Location = New System.Drawing.Point(16, 188)
        Me.grpRegion.Name = "grpRegion"
        Me.grpRegion.Size = New System.Drawing.Size(168, 224)
        Me.grpRegion.TabIndex = 3
        Me.grpRegion.TabStop = False
        Me.grpRegion.Text = "Region Examples"
        '
        'cmdGreen
        '
        Me.cmdGreen.Location = New System.Drawing.Point(16, 184)
        Me.cmdGreen.Name = "cmdGreen"
        Me.cmdGreen.Size = New System.Drawing.Size(136, 24)
        Me.cmdGreen.TabIndex = 5
        Me.cmdGreen.Text = "Great Green"
        '
        'cmdPurple
        '
        Me.cmdPurple.Location = New System.Drawing.Point(16, 152)
        Me.cmdPurple.Name = "cmdPurple"
        Me.cmdPurple.Size = New System.Drawing.Size(136, 24)
        Me.cmdPurple.TabIndex = 4
        Me.cmdPurple.Text = "Pretty Purple"
        '
        'cmdRed
        '
        Me.cmdRed.Location = New System.Drawing.Point(16, 120)
        Me.cmdRed.Name = "cmdRed"
        Me.cmdRed.Size = New System.Drawing.Size(136, 24)
        Me.cmdRed.TabIndex = 3
        Me.cmdRed.Text = "Racing Red"
        '
        'cmdGrey
        '
        Me.cmdGrey.Location = New System.Drawing.Point(16, 88)
        Me.cmdGrey.Name = "cmdGrey"
        Me.cmdGrey.Size = New System.Drawing.Size(136, 24)
        Me.cmdGrey.TabIndex = 2
        Me.cmdGrey.Text = "Granite Grey"
        '
        'cmdYellow
        '
        Me.cmdYellow.Location = New System.Drawing.Point(16, 56)
        Me.cmdYellow.Name = "cmdYellow"
        Me.cmdYellow.Size = New System.Drawing.Size(136, 24)
        Me.cmdYellow.TabIndex = 1
        Me.cmdYellow.Text = "Mellow Yellow"
        '
        'cmdBlue
        '
        Me.cmdBlue.Location = New System.Drawing.Point(16, 24)
        Me.cmdBlue.Name = "cmdBlue"
        Me.cmdBlue.Size = New System.Drawing.Size(136, 24)
        Me.cmdBlue.TabIndex = 0
        Me.cmdBlue.Text = "Blazin Blue"
        '
        'grpArea
        '
        Me.grpArea.Controls.AddRange(New System.Windows.Forms.Control() {Me.cmdShowImage})
        Me.grpArea.Location = New System.Drawing.Point(16, 112)
        Me.grpArea.Name = "grpArea"
        Me.grpArea.Size = New System.Drawing.Size(168, 64)
        Me.grpArea.TabIndex = 4
        Me.grpArea.TabStop = False
        Me.grpArea.Text = "Area Example"
        '
        'cmdShowImage
        '
        Me.cmdShowImage.Location = New System.Drawing.Point(16, 32)
        Me.cmdShowImage.Name = "cmdShowImage"
        Me.cmdShowImage.Size = New System.Drawing.Size(136, 24)
        Me.cmdShowImage.TabIndex = 0
        Me.cmdShowImage.Text = "Show Image"
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using the SetRegion Method to apply effects to specific areas of the image."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(808, 69)
        Me.lstInfo.TabIndex = 5
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(608, 168)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(216, 69)
        Me.lstStatus.TabIndex = 32
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(608, 128)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(160, 24)
        Me.lblLoadStatus.TabIndex = 31
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblError
        '
        Me.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblError.Location = New System.Drawing.Point(608, 308)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(192, 96)
        Me.lblError.TabIndex = 30
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(608, 268)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(168, 24)
        Me.lblLastError.TabIndex = 29
        Me.lblLastError.Text = "Last Error:"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = -1
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4})
        Me.MenuItem1.Text = "&File"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "&Open"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "&Quit"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = -1
        Me.MenuItem5.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem6})
        Me.MenuItem5.Text = "&Toolbar"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 0
        Me.MenuItem6.Text = "&Show"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = -1
        Me.MenuItem7.Text = "&About"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolbar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuFileQuit
        '
        Me.mnuFileQuit.Index = 0
        Me.mnuFileQuit.Text = "&Quit"
        '
        'mnuToolbar
        '
        Me.mnuToolbar.Index = 1
        Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolbar.Text = "&Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(856, 446)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstStatus, Me.lblLoadStatus, Me.lblError, Me.lblLastError, Me.lstInfo, Me.grpArea, Me.grpRegion, Me.ImageXView1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Menu = Me.MainMenu1
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ImagXpress .NET Sample - Area"
        Me.grpRegion.ResumeLayout(False)
        Me.grpArea.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/



    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Private Sub cmdQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Application.Exit()
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '***Must call Unlock runtime to distribute app
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        Dim T As String
        Dim X1() As String
        Dim X, Y As Integer
        Dim delimStr As String = " "
        Dim delimiter As Char() = delimStr.ToCharArray()
        Dim pRgnPoints As New ArrayList()

        Try
            'Create a new load options object so we can recieve events from the images we load
            loLoadOptions = New PegasusImaging.WinForms.ImagXpress8.LoadOptions()
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try
        Try
            'this is where events are assigned. This happens before the file gets loaded.
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        strCurrentDir = System.IO.Directory.GetCurrentDirectory()
        strImageFile = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\truck.pic")

        Try
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile)
            ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try


        ' Create an instance of StreamReader to read from a file.
        Dim sr As System.IO.StreamReader = New System.IO.StreamReader(System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\truck.rgn"))
        Dim line As String
        ' Read and display the lines from the file until the end 
        ' of the file is reached.
        line = sr.ReadLine()
        While line <> Nothing
            X1 = Split(line, delimiter, 2, CompareMethod.Text)
            X = Convert.ToInt32(X1(0))
            Y = Convert.ToInt32(X1(1))
            pRgnPoints.Add(New Point(X, Y))
            line = sr.ReadLine()
        End While
        sr.Close()

        Dim pPoints() As Point
        ReDim pPoints(pRgnPoints.Count)

        Dim pTmpo As Point
        Dim i As Integer = 0

        For Each pTmpo In pRgnPoints
            If pTmpo.IsEmpty() = False Then
                pPoints.SetValue(New Point(pTmpo.X, pTmpo.Y), i)
                i = i + 1
            End If
        Next

        pRgnPath.AddPolygon(pPoints)
    End Sub

    Private Sub cmdBlue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBlue.Click
        AdjustRegionHue(60, 0, 0)
    End Sub

    Private Sub cmdYellow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdYellow.Click
        AdjustRegionHue(20, 0, 0)
    End Sub

    Private Sub cmdGrey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGrey.Click
        AdjustRegionHue(0, -100, 0)
    End Sub

    Private Sub cmdRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRed.Click
        AdjustRegionHue(0, 0, 0)
    End Sub

    Private Sub cmdPurple_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPurple.Click
        AdjustRegionHue(85, 0, 0)
    End Sub

    Private Sub cmdGreen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGreen.Click
        AdjustRegionHue(30, 0, 0)
    End Sub


    Sub AdjustRegionHue(ByVal Hue As Integer, ByVal Sat As Integer, ByVal Lum As Integer)

        Try
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc = New PegasusImaging.WinForms.ImagXpress8.Processor(ImageXView1.Image)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc.SetRegion(pRgnPath)
            prc.EnableRegion = True
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc.AdjustHSL(PegasusImaging.WinForms.ImagXpress8.AdjustType.AsPercent, Hue, Sat, Lum)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub

    Private Sub cmdShowImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdShowImage.Click
        Dim prc As PegasusImaging.WinForms.ImagXpress8.Processor
        Try
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc = New PegasusImaging.WinForms.ImagXpress8.Processor(ImageXView1.Image)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc.SetArea(New System.Drawing.RectangleF(5, 20, 200, 10))
            prc.EnableArea = True
            prc.Emboss()
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try

            prc.SetArea(New System.Drawing.RectangleF(220, 30, 80, 80))
            prc.EnableArea = True

            prc.Flip()
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc.SetArea(New System.Drawing.RectangleF(25, 130, 150, 50))
            prc.EnableArea = True
            prc.AdjustRGB(PegasusImaging.WinForms.ImagXpress8.AdjustType.InUnits, 50, -50, -30)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc.SetArea(New System.Drawing.RectangleF(25, 180, 100, 50))
            prc.EnableArea = True
            prc.Ripple(30, 15, 0)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try

        Try
            prc.SetArea(New System.Drawing.RectangleF(185, 160, 100, 50))
            prc.EnableArea = True
            prc.Noise(18, 128)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try
    End Sub



    Private Sub mnuFileQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileQuit.Click
        Application.Exit()
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
        If ImageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False

        Else
            mnuToolbarShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True
        End If
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If

    End Sub

    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

    End Sub





End Class

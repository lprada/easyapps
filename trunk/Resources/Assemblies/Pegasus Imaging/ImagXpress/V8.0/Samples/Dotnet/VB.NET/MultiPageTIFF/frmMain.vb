'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.Runtime.InteropServices
' DllImport
<StructLayout(LayoutKind.Sequential)> _
Public Structure RECT

    Public left As Integer

    Public top As Integer

    Public right As Integer

    Public bottom As Integer
End Structure
Public Class Win32

    Public Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As System.Int32, ByVal lpString As String) As Long
    Public Declare Function MoveWindow Lib "User32.Dll" (ByVal hWnd As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal bRepaint As Boolean) As Boolean
    Public Declare Function GetWindowRect Lib "User32.Dll" (ByVal hWnd As Integer, ByRef rc As RECT) As Boolean
End Class
Namespace MultipageTIFF


    ' <summary>
    ' Summary description for Form1.
    ' </summary>
    Public Class frmMain
        Inherits System.Windows.Forms.Form


        Private WithEvents cmdMake As System.Windows.Forms.Button

        Private WithEvents cmdRemove As System.Windows.Forms.Button

        Private WithEvents cmdInsert As System.Windows.Forms.Button

        Private WithEvents cmdCompact As System.Windows.Forms.Button


        Private lblMore As System.Windows.Forms.Label

        Private mnuFile As System.Windows.Forms.MainMenu

        Private WithEvents mnuFileFile As System.Windows.Forms.MenuItem


        Private WithEvents mnuFileQuit As System.Windows.Forms.MenuItem

        Private strImageFile1 As String

        Private strImageFile2 As String

        Private strImageFile3 As String

        Private strMPFile As String

        Private strTmpFile As String

        Private imagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress

        Private mnuToolbar As System.Windows.Forms.MenuItem

        Private WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem

        Private WithEvents mnuAbout As System.Windows.Forms.MenuItem

        ' <summary>
        ' Required designer variable.
        ' </summary>
        Private components As System.ComponentModel.Container = Nothing

#Region "Pegasus Imaging Sample Application Standard Functions"
        '/*********************************************************************
        '*     Pegasus Imaging Corporation Standard Function Definitions     *
        ' *********************************************************************/



        Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
        Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
        Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo


        Dim strCurrentDir As System.String = "..\..\..\..\..\..\..\Common\Images\"
        Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Dim printDocument1 As System.Drawing.Printing.PrintDocument
        Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
        ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
        "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
        "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
        "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
        ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
        "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
        "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
        "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


        Private Function GetFileName(ByVal FullName As String) As String

            Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
        End Function

        Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                        + (ErrorException.Source + "" & vbLf)))
        End Sub

        Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
            ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                        + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
        End Sub

        Private Overloads Function PegasusOpenFile() As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strDefaultImageFilter
            dlg.InitialDirectory = strCurrentDir
            If (dlg.ShowDialog = DialogResult.OK) Then
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

        Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Title = "Select an Image File"
            dlg.Filter = strFilter
            dlg.InitialDirectory = strCurrentDir
            If (dlg.ShowDialog = DialogResult.OK) Then
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
                Return dlg.FileName
            Else
                Return ""
            End If
        End Function

#End Region


        Public Sub New()
            MyBase.New()
            '
            ' Required for Windows Form Designer support
            '
            '**The UnlockControl function must be called to distribute the runtime**
            'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

            InitializeComponent()
            '
            ' TODO: Add any constructor code after InitializeComponent call
            '
        End Sub

        ' <summary>
        ' Clean up any resources being used.
        ' </summary>
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then

                'Dispose of the imagXpress1 object and the 3 ImageXView objects
                If (Not (ImageXView1) Is Nothing) Then
                    ImageXView1.Dispose()
                    ImageXView1 = Nothing
                End If

                If (Not (ImageXView2) Is Nothing) Then
                    ImageXView2.Dispose()
                    ImageXView2 = Nothing
                End If

                If (Not (ImageXView3) Is Nothing) Then
                    ImageXView3.Dispose()
                    ImageXView3 = Nothing
                End If

                If (Not (imagXpress1) Is Nothing) Then
                    imagXpress1.Dispose()
                    imagXpress1 = Nothing
                End If


                If (Not (components) Is Nothing) Then
                    components.Dispose()
                End If
            End If



            MyBase.Dispose(disposing)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
        Friend WithEvents ImageXView2 As PegasusImaging.WinForms.ImagXpress8.ImageXView
        Friend WithEvents ImageXView3 As PegasusImaging.WinForms.ImagXpress8.ImageXView
        Private WithEvents lblFileStats As System.Windows.Forms.Label
        Friend WithEvents lstInfo As System.Windows.Forms.ListBox
        Friend WithEvents lstStatus As System.Windows.Forms.ListBox
        Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
        Friend WithEvents lblError As System.Windows.Forms.Label
        Friend WithEvents lblLastError As System.Windows.Forms.Label
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.cmdMake = New System.Windows.Forms.Button()
            Me.cmdRemove = New System.Windows.Forms.Button()
            Me.cmdInsert = New System.Windows.Forms.Button()
            Me.cmdCompact = New System.Windows.Forms.Button()
            Me.lblFileStats = New System.Windows.Forms.Label()
            Me.lblMore = New System.Windows.Forms.Label()
            Me.mnuFile = New System.Windows.Forms.MainMenu()
            Me.mnuFileFile = New System.Windows.Forms.MenuItem()
            Me.mnuFileQuit = New System.Windows.Forms.MenuItem()
            Me.mnuToolbar = New System.Windows.Forms.MenuItem()
            Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
            Me.mnuAbout = New System.Windows.Forms.MenuItem()
            Me.imagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
            Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            Me.ImageXView2 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            Me.ImageXView3 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            Me.lstInfo = New System.Windows.Forms.ListBox()
            Me.lstStatus = New System.Windows.Forms.ListBox()
            Me.lblLoadStatus = New System.Windows.Forms.Label()
            Me.lblError = New System.Windows.Forms.Label()
            Me.lblLastError = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'cmdMake
            '
            Me.cmdMake.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdMake.Location = New System.Drawing.Point(376, 149)
            Me.cmdMake.Name = "cmdMake"
            Me.cmdMake.Size = New System.Drawing.Size(120, 24)
            Me.cmdMake.TabIndex = 4
            Me.cmdMake.Text = "Make MP.TIF"
            '
            'cmdRemove
            '
            Me.cmdRemove.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdRemove.Location = New System.Drawing.Point(376, 181)
            Me.cmdRemove.Name = "cmdRemove"
            Me.cmdRemove.Size = New System.Drawing.Size(120, 24)
            Me.cmdRemove.TabIndex = 5
            Me.cmdRemove.Text = "Remove Page 2"
            '
            'cmdInsert
            '
            Me.cmdInsert.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdInsert.Location = New System.Drawing.Point(376, 213)
            Me.cmdInsert.Name = "cmdInsert"
            Me.cmdInsert.Size = New System.Drawing.Size(120, 24)
            Me.cmdInsert.TabIndex = 6
            Me.cmdInsert.Text = "Insert Page 2"
            '
            'cmdCompact
            '
            Me.cmdCompact.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.cmdCompact.Location = New System.Drawing.Point(376, 245)
            Me.cmdCompact.Name = "cmdCompact"
            Me.cmdCompact.Size = New System.Drawing.Size(120, 24)
            Me.cmdCompact.TabIndex = 7
            Me.cmdCompact.Text = "Compact"
            '
            'lblFileStats
            '
            Me.lblFileStats.Location = New System.Drawing.Point(16, 192)
            Me.lblFileStats.Name = "lblFileStats"
            Me.lblFileStats.Size = New System.Drawing.Size(200, 40)
            Me.lblFileStats.TabIndex = 8
            '
            'lblMore
            '
            Me.lblMore.Location = New System.Drawing.Point(232, 200)
            Me.lblMore.Name = "lblMore"
            Me.lblMore.Size = New System.Drawing.Size(120, 24)
            Me.lblMore.TabIndex = 9
            Me.lblMore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            '
            'mnuFile
            '
            Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileFile, Me.mnuToolbar, Me.mnuAbout})
            '
            'mnuFileFile
            '
            Me.mnuFileFile.Index = 0
            Me.mnuFileFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileQuit})
            Me.mnuFileFile.Text = "&File"
            '
            'mnuFileQuit
            '
            Me.mnuFileQuit.Index = 0
            Me.mnuFileQuit.Text = "&Quit"
            '
            'mnuToolbar
            '
            Me.mnuToolbar.Index = 1
            Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
            Me.mnuToolbar.Text = "&Toolbar"
            '
            'mnuToolbarShow
            '
            Me.mnuToolbarShow.Index = 0
            Me.mnuToolbarShow.Text = "&Show"
            '
            'mnuAbout
            '
            Me.mnuAbout.Index = 2
            Me.mnuAbout.Text = "&About"
            '
            'ImageXView1
            '
            Me.ImageXView1.Location = New System.Drawing.Point(8, 288)
            Me.ImageXView1.Name = "ImageXView1"
            Me.ImageXView1.Size = New System.Drawing.Size(160, 168)
            Me.ImageXView1.TabIndex = 16
            '
            'ImageXView2
            '
            Me.ImageXView2.Location = New System.Drawing.Point(176, 288)
            Me.ImageXView2.Name = "ImageXView2"
            Me.ImageXView2.Size = New System.Drawing.Size(160, 168)
            Me.ImageXView2.TabIndex = 17
            '
            'ImageXView3
            '
            Me.ImageXView3.Location = New System.Drawing.Point(344, 288)
            Me.ImageXView3.Name = "ImageXView3"
            Me.ImageXView3.Size = New System.Drawing.Size(144, 168)
            Me.ImageXView3.TabIndex = 18
            '
            'lstInfo
            '
            Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates operations that ImagXpress can perform on Multiple page " & _
            "Tiff files.", "1)Creating  multiple page Tiff files.", "2)Displaying individual pages.", "3)Obtaining the number of Pages.", "4)Deleting a page.", "5)Inserting a page.", "6)Compacting unused pages from a file."})
            Me.lstInfo.Location = New System.Drawing.Point(8, 16)
            Me.lstInfo.Name = "lstInfo"
            Me.lstInfo.Size = New System.Drawing.Size(776, 108)
            Me.lstInfo.TabIndex = 19
            '
            'lstStatus
            '
            Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Right)
            Me.lstStatus.Location = New System.Drawing.Point(528, 192)
            Me.lstStatus.Name = "lstStatus"
            Me.lstStatus.Size = New System.Drawing.Size(248, 95)
            Me.lstStatus.TabIndex = 36
            '
            'lblLoadStatus
            '
            Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblLoadStatus.Location = New System.Drawing.Point(528, 152)
            Me.lblLoadStatus.Name = "lblLoadStatus"
            Me.lblLoadStatus.Size = New System.Drawing.Size(208, 24)
            Me.lblLoadStatus.TabIndex = 35
            Me.lblLoadStatus.Text = "Load Status:"
            '
            'lblError
            '
            Me.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblError.Location = New System.Drawing.Point(528, 354)
            Me.lblError.Name = "lblError"
            Me.lblError.Size = New System.Drawing.Size(240, 96)
            Me.lblError.TabIndex = 34
            '
            'lblLastError
            '
            Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
            Me.lblLastError.Location = New System.Drawing.Point(528, 306)
            Me.lblLastError.Name = "lblLastError"
            Me.lblLastError.Size = New System.Drawing.Size(216, 24)
            Me.lblLastError.TabIndex = 33
            Me.lblLastError.Text = "Last Error:"
            '
            'frmMain
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(816, 475)
            Me.ControlBox = False
            Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstStatus, Me.lblLoadStatus, Me.lblError, Me.lblLastError, Me.lstInfo, Me.ImageXView3, Me.ImageXView2, Me.ImageXView1, Me.lblMore, Me.lblFileStats, Me.cmdCompact, Me.cmdInsert, Me.cmdRemove, Me.cmdMake})
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.Menu = Me.mnuFile
            Me.Name = "frmMain"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "MultiPage TIFF"
            Me.ResumeLayout(False)

        End Sub

        Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)

            lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
            If (e.IsComplete) Then
                lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))

                lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
            End If

        End Sub

        Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

            If (e.Status.Decoded) Then
                lstStatus.Items.Add(e.Status.ToString)
            End If
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

        End Sub

        Private Sub ReloadViews()
            Try
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strMPFile)
                ImageXView1.Image.Page = 1
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImageXException
                PegasusError(ex, lblError)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            End Try
            Try
                If Not ImageXView1.Image Is Nothing Then
                    If (ImageXView1.Image.PageCount > 1) Then
                        ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strMPFile)
                        ImageXView2.Image.Page = 2
                    Else
                        ImageXView2.Image = Nothing
                    End If
                End If
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImageXException
                PegasusError(ex, lblError)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            End Try
            Try
                If Not ImageXView2.Image Is Nothing Then
                    If (ImageXView2.Image.PageCount > 2) Then
                        ImageXView3.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strMPFile)
                        ImageXView3.Image.Page = 3
                    Else
                        ImageXView3.Image = Nothing
                    End If
                End If
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImageXException
                PegasusError(ex, lblError)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            End Try
            lblFileStats.Text = ("File Size: " _
                        + (ImageXView1.Image.ImageXData.Size.ToString(cultNumber) + (" bytes." + Microsoft.VisualBasic.ChrW(10) + ("Pages: " + ImageXView1.Image.PageCount.ToString()))))
            If Not ImageXView1.Image Is Nothing Then
                If (ImageXView1.Image.PageCount > 3) Then
                    lblMore.Text = "More..."
                Else
                    lblMore.Text = ""
                End If
            End If
        End Sub

        Private Sub mnuFileQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileQuit.Click
            Application.Exit()
        End Sub

        Private Sub cmdMake_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMake.Click
            Dim imgTmp As PegasusImaging.WinForms.ImagXpress8.ImageX
            If System.IO.File.Exists(strMPFile) Then
                System.IO.File.Delete(strMPFile)
            End If
            Try
                imgTmp = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile1)
                Dim soOpts As PegasusImaging.WinForms.ImagXpress8.SaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
                Try
                    soOpts.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff
                    soOpts.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.NoCompression
                    soOpts.Tiff.MultiPage = True
                Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(eX, lblError)
                End Try
                imgTmp.Save(strMPFile, soOpts)
                Try
                    'clear out the error in case there was an error from a previous operation
                    lblError.Text = ""

                    PegasusImaging.WinForms.ImagXpress8.ImageX.InsertPage(strImageFile2, strMPFile, 2)
                Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(eX, lblError)
                End Try
                Try

                    'clear out the error in case there was an error from a previous operation
                    lblError.Text = ""
                    PegasusImaging.WinForms.ImagXpress8.ImageX.InsertPage(strImageFile3, strMPFile, 3)
                Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(eX, lblError)
                End Try
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
                Return
            End Try
            ReloadViews()
        End Sub

        Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            strImageFile1 = System.IO.Path.Combine(strCurrentDir, "page1.tif")
            strImageFile2 = System.IO.Path.Combine(strCurrentDir, "page2.tif")
            strImageFile3 = System.IO.Path.Combine(strCurrentDir, "page3.tif")
            strMPFile = System.IO.Path.Combine(strCurrentDir, "..\mp.tif")
            strTmpFile = System.IO.Path.Combine(strCurrentDir, "..\compact.tif")


            Try
                'imagXpress1.License.UnlockRuntime(12345,12345,12345)
                AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
                AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            End Try
        End Sub

        Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
            Try
                'clear out the error in case there was an error from a previous operation
                lblError.Text = ""

                PegasusImaging.WinForms.ImagXpress8.ImageX.DeletePage(strMPFile, 2)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImageXException
                PegasusError(ex, lblError)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            End Try
            ReloadViews()
        End Sub

        Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
            Try
                imagXpress1.AboutBox()
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try
        End Sub

        Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
            Dim hwndTB As System.IntPtr
            Dim controlRect As RECT = New RECT()
            Dim toolbarRect As RECT = New RECT()
            Me.mnuToolbarShow.Text = ImageXView1.Toolbar.Activated

            If ImageXView1.Toolbar.Activated Then
                Me.mnuToolbarShow.Text = "Show"
            Else
                Me.mnuToolbarShow.Text = "Hide"
            End If

            Try
                ImageXView1.Toolbar.Activated = Not ImageXView1.Toolbar.Activated
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            hwndTB = ImageXView1.Toolbar.Hwnd
            Win32.GetWindowRect(ImageXView1.Handle.ToInt32, controlRect)
            Win32.GetWindowRect(hwndTB.ToInt32, toolbarRect)
            Win32.SetWindowText(hwndTB.ToInt32, "Toolbar 1")
            Win32.MoveWindow(hwndTB.ToInt32, (controlRect.left + 8), (controlRect.bottom + 12), (toolbarRect.right - toolbarRect.left), (toolbarRect.bottom - toolbarRect.top), True)
            Try
                ImageXView2.Toolbar.Activated = Not ImageXView2.Toolbar.Activated
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            hwndTB = ImageXView2.Toolbar.Hwnd
            Win32.GetWindowRect(ImageXView2.Handle.ToInt32, controlRect)
            Win32.GetWindowRect(hwndTB.ToInt32, toolbarRect)
            Win32.SetWindowText(hwndTB.ToInt32, "Toolbar 2")
            Win32.MoveWindow(hwndTB.ToInt32, (controlRect.left + 8), (controlRect.bottom + 12), (toolbarRect.right - toolbarRect.left), (toolbarRect.bottom - toolbarRect.top), True)
            Try
                ImageXView3.Toolbar.Activated = Not ImageXView3.Toolbar.Activated
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            hwndTB = ImageXView3.Toolbar.Hwnd
            Win32.GetWindowRect(ImageXView3.Handle.ToInt32, controlRect)
            Win32.GetWindowRect(hwndTB.ToInt32, toolbarRect)
            Win32.SetWindowText(hwndTB.ToInt32, "Toolbar 3")
            Win32.MoveWindow(hwndTB.ToInt32, (controlRect.left + 8), (controlRect.bottom + 12), (toolbarRect.right - toolbarRect.left), (toolbarRect.bottom - toolbarRect.top), True)
        End Sub

        Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click
            Try
                'clear out the error in case there was an error from a previous operation
                lblError.Text = ""

                PegasusImaging.WinForms.ImagXpress8.ImageX.InsertPage(strImageFile2, strMPFile, 2)
            Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(eX, lblError)
            End Try
            ReloadViews()
        End Sub

        Private Sub cmdCompact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCompact.Click
            Try
                'clear out the error in case there was an error from a previous operation
                lblError.Text = ""

                PegasusImaging.WinForms.ImagXpress8.ImageX.CompactFile(strMPFile, strTmpFile)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)

            End Try
            If System.IO.File.Exists(strTmpFile) Then
                System.IO.File.Delete(strMPFile)
                System.IO.File.Move(strTmpFile, strMPFile)
            End If
            ReloadViews()
        End Sub


        Private Sub PegasusTextBoxScrollBinder(ByVal scrScroll As System.Windows.Forms.ScrollBar, ByVal textTextBox As System.Windows.Forms.TextBox)
            Dim iTmp As Integer
            Try
                iTmp = Convert.ToInt32(textTextBox.Text, cultNumber)
            Catch ex As System.NullReferenceException
                PegasusError(ex, lblError)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            Catch ex As System.Exception
                PegasusError(ex, lblError)
                textTextBox.Text = scrScroll.Value.ToString(cultNumber)
                Return
            End Try
            If ((iTmp < scrScroll.Maximum) _
                        AndAlso (iTmp > scrScroll.Minimum)) Then
                scrScroll.Value = iTmp
            Else
                iTmp = scrScroll.Value
            End If
            textTextBox.Text = iTmp.ToString(cultNumber)
        End Sub

    End Class
End Namespace
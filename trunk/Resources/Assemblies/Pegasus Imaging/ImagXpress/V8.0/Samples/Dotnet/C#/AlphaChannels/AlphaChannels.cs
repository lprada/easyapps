/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;
namespace AlphaChannels
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Alpha : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.Processor ixproc1;
		private System.Windows.Forms.MainMenu menuFile;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuOpen;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem mnuQuit;
		private System.Windows.Forms.MenuItem mnuToolBar;
		private System.Windows.Forms.MenuItem mnuShow;
		private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.ListBox lstStatus;

		
		private System.String strCurrentDir;
		private System.String strCurrentImage;
		private System.String strCurrentImage1;
		private System.String strCurrentImage2;
		private System.String strCurrentImage3;
	
		
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.Button cmdAddAlpha;
		private System.Windows.Forms.MenuItem mnuAlpha;
		private System.Windows.Forms.ListBox lstdesc;
		private System.Windows.Forms.ComboBox cmbAlpaha;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;
		private System.Windows.Forms.Label lblAlpha;
		private System.Windows.Forms.Label lblsource;
		private System.Windows.Forms.Label labelLastError;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Alpha()
		{
			//
			// Required for Windows Form Designer support
			//****Must call the UnlockControl function *****
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
			InitializeComponent();

			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				
				if (imagXpress1 != null) 
				{
					imagXpress1.Dispose();
					imagXpress1 =null;
				}

				if (imageXView1 != null) 
				{
					imageXView1.Dispose();
					imageXView1 =null;
				}

				if (imageXView2 != null) 
				{
					imageXView2.Dispose();
					imageXView2 =null;
				}

				if (ixproc1 != null) 
				{
					ixproc1.Dispose();
					ixproc1 =null;
				}


				if (components != null) 
				{
					components.Dispose();
				}

				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.menuFile = new System.Windows.Forms.MainMenu();
			this.mnuFile = new System.Windows.Forms.MenuItem();
			this.mnuOpen = new System.Windows.Forms.MenuItem();
			this.mnuAlpha = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.mnuQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolBar = new System.Windows.Forms.MenuItem();
			this.mnuShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.lblError = new System.Windows.Forms.Label();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.cmdAddAlpha = new System.Windows.Forms.Button();
			this.lstdesc = new System.Windows.Forms.ListBox();
			this.cmbAlpaha = new System.Windows.Forms.ComboBox();
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lblAlpha = new System.Windows.Forms.Label();
			this.lblsource = new System.Windows.Forms.Label();
			this.labelLastError = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView1.AutoScroll = true;
			this.imageXView1.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.imageXView1.Location = new System.Drawing.Point(176, 160);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(384, 296);
			this.imageXView1.TabIndex = 0;
			// 
			// menuFile
			// 
			this.menuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnuFile,
																					 this.mnuToolBar,
																					 this.mnuAbout});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuOpen,
																					this.mnuAlpha,
																					this.menuItem1,
																					this.mnuQuit});
			this.mnuFile.Text = "&File";
			// 
			// mnuOpen
			// 
			this.mnuOpen.Index = 0;
			this.mnuOpen.Text = "&Open Source Image";
			this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
			// 
			// mnuAlpha
			// 
			this.mnuAlpha.Index = 1;
			this.mnuAlpha.Text = "&Open Alpha Channel Image";
			this.mnuAlpha.Click += new System.EventHandler(this.mnuAlpha_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 2;
			this.menuItem1.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 3;
			this.mnuQuit.Text = "&Quit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// mnuToolBar
			// 
			this.mnuToolBar.Index = 1;
			this.mnuToolBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuShow});
			this.mnuToolBar.Text = "&ToolBar";
			// 
			// mnuShow
			// 
			this.mnuShow.Index = 0;
			this.mnuShow.Text = "&Show";
			this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// lblError
			// 
			this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblError.Location = new System.Drawing.Point(528, 96);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(216, 56);
			this.lblError.TabIndex = 1;
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(560, 160);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(192, 290);
			this.lstStatus.TabIndex = 2;
			// 
			// cmdAddAlpha
			// 
			this.cmdAddAlpha.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.cmdAddAlpha.Location = new System.Drawing.Point(8, 416);
			this.cmdAddAlpha.Name = "cmdAddAlpha";
			this.cmdAddAlpha.Size = new System.Drawing.Size(160, 40);
			this.cmdAddAlpha.TabIndex = 3;
			this.cmdAddAlpha.Text = "Blend Alpha Channel Image With Source Image";
			this.cmdAddAlpha.Click += new System.EventHandler(this.cmdAddAlpha_Click);
			// 
			// lstdesc
			// 
			this.lstdesc.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstdesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstdesc.Items.AddRange(new object[] {
														 "This sample demonstrates merging an image with an alpha channel to a source image" +
														 "",
														 "and displaying the combined image. You can select an Alpha Channel image in the d" +
														 "ropdown list or",
														 "load your own source image and/or alpha channel image in the sample."});
			this.lstdesc.Location = new System.Drawing.Point(16, 16);
			this.lstdesc.Name = "lstdesc";
			this.lstdesc.Size = new System.Drawing.Size(736, 69);
			this.lstdesc.TabIndex = 4;
			// 
			// cmbAlpaha
			// 
			this.cmbAlpaha.Items.AddRange(new object[] {
														   "AlphaImage1",
														   "AlphaImage2",
														   "AlpahaImage3"});
			this.cmbAlpaha.Location = new System.Drawing.Point(16, 96);
			this.cmbAlpaha.Name = "cmbAlpaha";
			this.cmbAlpaha.Size = new System.Drawing.Size(144, 21);
			this.cmbAlpaha.TabIndex = 5;
			this.cmbAlpaha.SelectedIndexChanged += new System.EventHandler(this.cmbAlpaha_SelectedIndexChanged);
			// 
			// imageXView2
			// 
			this.imageXView2.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
			this.imageXView2.Location = new System.Drawing.Point(16, 160);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(144, 248);
			this.imageXView2.TabIndex = 6;
			// 
			// lblAlpha
			// 
			this.lblAlpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAlpha.Location = new System.Drawing.Point(16, 136);
			this.lblAlpha.Name = "lblAlpha";
			this.lblAlpha.Size = new System.Drawing.Size(144, 16);
			this.lblAlpha.TabIndex = 7;
			this.lblAlpha.Text = "Alpha Channel Image";
			this.lblAlpha.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// lblsource
			// 
			this.lblsource.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblsource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblsource.Location = new System.Drawing.Point(216, 136);
			this.lblsource.Name = "lblsource";
			this.lblsource.Size = new System.Drawing.Size(288, 16);
			this.lblsource.TabIndex = 8;
			this.lblsource.Text = "Source Image";
			this.lblsource.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// labelLastError
			// 
			this.labelLastError.Location = new System.Drawing.Point(424, 96);
			this.labelLastError.Name = "labelLastError";
			this.labelLastError.Size = new System.Drawing.Size(72, 16);
			this.labelLastError.TabIndex = 9;
			this.labelLastError.Text = "Last Error:";
			this.labelLastError.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Alpha
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(760, 465);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.labelLastError,
																		  this.lblsource,
																		  this.lblAlpha,
																		  this.imageXView2,
																		  this.cmbAlpaha,
																		  this.lstdesc,
																		  this.cmdAddAlpha,
																		  this.lstStatus,
																		  this.lblError,
																		  this.imageXView1});
			this.Menu = this.menuFile;
			this.Name = "Alpha";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Alpha Channel Processing";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

        
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Alpha());
		}

		private void mnuOpen_Click(object sender, System.EventArgs e)
		{
			//First we obtain the filename of the image we want to open
			System.String strLoadResult = PegasusOpenFile();

			//we check first to make sure the file is valid
			if (strLoadResult.Length != 0) 
			{
				//If it is valid, we set our internal image filename equal to it
				strCurrentImage = strLoadResult;
			}

			//grab a reference to the "old" image for proper disposal
			PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = imageXView1.Image;


			try 
			{					
				//open the new image
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage);
				ixproc1.Image = imageXView1.Image;

				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";

				//dispose of the old image
				if (oldImage != null)
				{
					oldImage.Dispose();
					oldImage = null;
				}
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}

			
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
				Application.Exit();
		}

		private void mnuShow_Click(object sender, System.EventArgs e)
		{
			this.mnuShow.Text = (imageXView1.Toolbar.Activated) ? "&Show":"&Hide";
			try 
			{
				imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				PegasusError(eX,lblError);
			}
		}
		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
		 *     Pegasus Imaging Corporation Standard Function Definitions     *
		 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
		{
			System.Int32 iTmp;
			try 
			{
				iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
			} 
			catch (System.NullReferenceException ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			catch (System.Exception ex)
			{
				PegasusError(ex,lblError);
				textTextBox.Text = scrScroll.Value.ToString(cultNumber);
				return;
			}
			if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
			{
				scrScroll.Value = iTmp;
			} 
			else 
			{
				iTmp = scrScroll.Value;
			}
			textTextBox.Text = iTmp.ToString(cultNumber);
		}
		#endregion
		private void Form1_Load(object sender, System.EventArgs e)
		{
			
			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

			//this is where events are assigned. This happens before the file gets loaded.
		
			PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
			PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
			
			//here we set the current directory and image so that the file open dialog box works well
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString (cultNumber);
			strCurrentImage = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\alpha1.jpg");
			ixproc1 = new PegasusImaging.WinForms.ImagXpress8.Processor();
			
			strCurrentImage1 = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\alpha1.tif");
			strCurrentImage2 = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\alpha2.tif");
			strCurrentImage3 = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\alpha3.tif");
			strCurrentDir = strCommonImagesDirectory;
						
			//set some default values 
			cmbAlpaha.SelectedIndex = 0;
			imageXView2.Image  = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage1);
			
			if (System.IO.File.Exists(strCurrentImage)) 
			{
				try 
				{					
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage);
					ixproc1.Image = imageXView1.Image;
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}
		
		}	

		private void ReloadImage()
		{
			try 
			{
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage);
				lstStatus.Items.Add(imageXView1.Image.ImageXData.Width.ToString());

				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
				
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}
		}
		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
				imagXpress1.AboutBox();

		}

		private void cmdAddAlpha_Click(object sender, System.EventArgs e)
		{

		
			try
			{
				//grab a copy of the alpha image
				PegasusImaging.WinForms.ImagXpress8.ImageX alphaImage = imageXView2.Image.Copy();

				//merge the images
				ixproc1.Merge(ref alphaImage,MergeSize.Crop,MergeStyle.AlphaForeGroundOverBackGround,false,Color.Blue,90,90);
			
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";

				//dispose of the copy of the alpha image
				if (alphaImage != null)
				{
					alphaImage.Dispose();
					alphaImage = null;
				}
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}	
			
		}

		private void mnuAlpha_Click(object sender, System.EventArgs e)
		{
			//First we obtain the filename of the image we want to open
			System.String strLoadResult = PegasusOpenFile();

			//we check first to make sure the file is valid
			if (strLoadResult.Length != 0) 
			{
				//If it is valid, we set our internal image filename equal to it
				strCurrentImage1 = strLoadResult;
			}

			try 
			{				
				lstStatus.Items.Add(imageXView1.Image.ImageXData.Width.ToString());

				//Now, create a variable to hold the reference to the old image for proper disposal
				PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = imageXView2.Image;
			
				//Open the new image
				imageXView2.Image  = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage1);

				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";

				//dispose of the old image, if it's valid
				if (oldImage != null)
				{
					oldImage.Dispose();
					oldImage = null;
				}
				
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}	
					
		}

		private void cmbAlpaha_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
			try
			{
				switch(cmbAlpaha.SelectedIndex)
				{
					case 0 : 
						imageXView2.Image  = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage1);
						break;
					case 1 : 
						imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage2);
					
						break;
					case 2 : 
						imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage3);
						
						break;
						
				}
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
					PegasusError(ex,lblError);
			}	

		}

		private void cmdReload_Click(object sender, System.EventArgs e)
		{
			if (System.IO.File.Exists(strCurrentImage)) 
			{
				try 
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage);
					
					// clear out the error in case there was an error from a previous operation
					lblError.Text = "";
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}
			
		}

	
		


		
	}
}

/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;

using System.Globalization;

namespace BatchProcessing
{
	public class BatchProcessor: PegasusImaging.WinForms.ImagXpress8.Processor
	{
		public BatchProcessor(PegasusImaging.WinForms.ImagXpress8.ImageX image): base (image){}
		public void Process() 
		{
			this.ColorDepth(24,0,0);
			Application.DoEvents();
			this.Resize(new System.Drawing.Size(this.Image.ImageXData.Width/2 , this.Image.ImageXData.Height/3) , PegasusImaging.WinForms.ImagXpress8.ResizeType.Fast);
			Application.DoEvents();
			this.Blur();
			Application.DoEvents();
			this.Erode();
			Application.DoEvents();
			this.Crop(new System.Drawing.Rectangle(new System.Drawing.Point(0,0),new System.Drawing.Size(this.Image.ImageXData.Width/2 , this.Image.ImageXData.Height/2)));			
			Application.DoEvents();
			this.Emboss();
			Application.DoEvents();
			this.Resize(new System.Drawing.Size(this.Image.ImageXData.Width*2 , this.Image.ImageXData.Height*3) , PegasusImaging.WinForms.ImagXpress8.ResizeType.Fast);
			Application.DoEvents();
			this.Negate();
			Application.DoEvents();
			this.Swirl(180);
			Application.DoEvents();
		}
	}

	namespace BatchProcessing
	{
		/// <summary>
		/// Summary description for Form1.
		/// </summary>
		public
		class formProcess : System.Windows.Forms.Form
		{
			private System.String strCurrentDir;
			private System.String strCurrentImage;
			private PegasusImaging.WinForms.ImagXpress8.LoadOptions loOpts;
			private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
			private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
			private System.Windows.Forms.ListBox lststatus;
			private System.Windows.Forms.MainMenu mainMenu1;
			private System.Windows.Forms.MenuItem mnuOpen;
			private System.Windows.Forms.MenuItem menuItem2;
			private System.Windows.Forms.MenuItem mnuQuit;
			private System.Windows.Forms.MenuItem mnuFile;
			private System.Windows.Forms.MenuItem mnuToolbar;
			private System.Windows.Forms.MenuItem mnuShow;
			private System.Windows.Forms.MenuItem mnuAbout;
			private System.Windows.Forms.Button cmdProcess;
			private System.Windows.Forms.Label lsterror;
			private System.Windows.Forms.ListBox lstDesc;
			private System.Windows.Forms.Label lblError;
			/// <summary>
			/// Required designer variable.
			/// </summary>
			private System.ComponentModel.Container components = null;

			public formProcess()
			{
				//
				// Required for Windows Form Designer support
				//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
				InitializeComponent();

				//
				// TODO: Add any constructor code after InitializeComponent call
				//
			}

			/// <summary>
			/// Clean up any resources being used.
			/// </summary>
			protected override void Dispose( bool disposing )
			{
				if( disposing )
				{

					//  Don't forget to dispose IX
					// 
					if (!(imagXpress1 == null)) 
					{
						imagXpress1.Dispose();
						imagXpress1 = null;
					}
					if (!(imageXView1 == null)) 
					{
						imageXView1.Dispose();
						imageXView1 = null;
					}

					if (components != null) 
					{
						components.Dispose();
					}
				}
				base.Dispose( disposing );
			}

			#region Pegasus Imaging Sample Application Standard Functions
			/// <summary>
			/// /*********************************************************************
			///  *     Pegasus Imaging Corporation Standard Function Definitions     *
			///  *********************************************************************/
			/// </summary>
			/*********************************************************************
			 *     Pegasus Imaging Corporation Standard Function Definitions     *
			 *********************************************************************/

			private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
			private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
			private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
			const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";

			static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
			{
				ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
			}

			static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
			{
				ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
			}
			string PegasusOpenFile() 
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.Title = "Select an Image File";
				dlg.Filter = strDefaultImageFilter;
				dlg.InitialDirectory = strCurrentDir;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
					return dlg.FileName;
				} 
				else 
				{
					return "";
				}
			}

			string PegasusOpenFile(System.String strFilter) 
			{
				OpenFileDialog dlg = new OpenFileDialog();
				dlg.Title = "Select an Image File";
				dlg.Filter = strFilter;
				dlg.InitialDirectory = strCurrentDir;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
					return dlg.FileName;
				} 
				else 
				{
					return "";
				}
			}

			void PegasusTextBoxScrollBinder(System.Windows.Forms.ScrollBar scrScroll, System.Windows.Forms.TextBox textTextBox)
			{
				System.Int32 iTmp;
				try 
				{
					iTmp = Convert.ToInt32(textTextBox.Text,cultNumber);
				} 
				catch (System.NullReferenceException ex)
				{
					PegasusError(ex,lblError);
					textTextBox.Text = scrScroll.Value.ToString(cultNumber);
					return;
				}
				catch (System.Exception ex)
				{
					PegasusError(ex,lblError);
					textTextBox.Text = scrScroll.Value.ToString(cultNumber);
					return;
				}
				if ((iTmp < scrScroll.Maximum) && (iTmp > scrScroll.Minimum))
				{
					scrScroll.Value = iTmp;
				} 
				else 
				{
					iTmp = scrScroll.Value;
				}
				textTextBox.Text = iTmp.ToString(cultNumber);
			}

		
		#endregion

			private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
			{
				lststatus.Items.Add(e.Status.ToString(cultNumber));
			
			}
			private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
			{
				lststatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
				if (e.IsComplete) 
				{
					lststatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
				}
				lststatus.SelectedIndex = lststatus.Items.Count - 1;
			}


		#region Windows Form Designer generated code
			/// <summary>
			/// Required method for Designer support - do not modify
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
				this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
				this.lblError = new System.Windows.Forms.Label();
				this.lststatus = new System.Windows.Forms.ListBox();
				this.mainMenu1 = new System.Windows.Forms.MainMenu();
				this.mnuFile = new System.Windows.Forms.MenuItem();
				this.mnuOpen = new System.Windows.Forms.MenuItem();
				this.menuItem2 = new System.Windows.Forms.MenuItem();
				this.mnuQuit = new System.Windows.Forms.MenuItem();
				this.mnuToolbar = new System.Windows.Forms.MenuItem();
				this.mnuShow = new System.Windows.Forms.MenuItem();
				this.mnuAbout = new System.Windows.Forms.MenuItem();
				this.cmdProcess = new System.Windows.Forms.Button();
				this.lsterror = new System.Windows.Forms.Label();
				this.lstDesc = new System.Windows.Forms.ListBox();
				this.SuspendLayout();
				// 
				// imageXView1
				// 
				this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
					| System.Windows.Forms.AnchorStyles.Left) 
					| System.Windows.Forms.AnchorStyles.Right);
				this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
				this.imageXView1.Location = new System.Drawing.Point(24, 64);
				this.imageXView1.Name = "imageXView1";
				this.imageXView1.Size = new System.Drawing.Size(552, 160);
				this.imageXView1.TabIndex = 0;
				// 
				// lblError
				// 
				this.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
				this.lblError.Location = new System.Drawing.Point(280, 256);
				this.lblError.Name = "lblError";
				this.lblError.Size = new System.Drawing.Size(296, 96);
				this.lblError.TabIndex = 1;
				// 
				// lststatus
				// 
				this.lststatus.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
					| System.Windows.Forms.AnchorStyles.Right);
				this.lststatus.Location = new System.Drawing.Point(24, 240);
				this.lststatus.Name = "lststatus";
				this.lststatus.Size = new System.Drawing.Size(232, 121);
				this.lststatus.TabIndex = 2;
				// 
				// mainMenu1
				// 
				this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.mnuFile,
																						  this.mnuToolbar,
																						  this.mnuAbout});
				// 
				// mnuFile
				// 
				this.mnuFile.Index = 0;
				this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuOpen,
																						this.menuItem2,
																						this.mnuQuit});
				this.mnuFile.Text = "&File";
				// 
				// mnuOpen
				// 
				this.mnuOpen.Index = 0;
				this.mnuOpen.Text = "&Open";
				this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
				// 
				// menuItem2
				// 
				this.menuItem2.Index = 1;
				this.menuItem2.Text = "-";
				// 
				// mnuQuit
				// 
				this.mnuQuit.Index = 2;
				this.mnuQuit.Text = "&Quit";
				this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
				// 
				// mnuToolbar
				// 
				this.mnuToolbar.Index = 1;
				this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						   this.mnuShow});
				this.mnuToolbar.Text = "&ToolBar";
				// 
				// mnuShow
				// 
				this.mnuShow.Index = 0;
				this.mnuShow.Text = "&Show";
				this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
				// 
				// mnuAbout
				// 
				this.mnuAbout.Index = 2;
				this.mnuAbout.Text = "&About";
				this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
				// 
				// cmdProcess
				// 
				this.cmdProcess.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
				this.cmdProcess.Location = new System.Drawing.Point(432, 360);
				this.cmdProcess.Name = "cmdProcess";
				this.cmdProcess.Size = new System.Drawing.Size(144, 24);
				this.cmdProcess.TabIndex = 3;
				this.cmdProcess.Text = "Process Images";
				this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
				// 
				// lsterror
				// 
				this.lsterror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
				this.lsterror.Location = new System.Drawing.Point(272, 232);
				this.lsterror.Name = "lsterror";
				this.lsterror.Size = new System.Drawing.Size(200, 16);
				this.lsterror.TabIndex = 4;
				this.lsterror.Text = "Last Error:";
				// 
				// lstDesc
				// 
				this.lstDesc.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
					| System.Windows.Forms.AnchorStyles.Right);
				this.lstDesc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
				this.lstDesc.Items.AddRange(new object[] {
															 "This sample demonstrates the following functionality:",
															 "1)Using the Processor object to perform various image processing operations."});
				this.lstDesc.Location = new System.Drawing.Point(24, 8);
				this.lstDesc.Name = "lstDesc";
				this.lstDesc.Size = new System.Drawing.Size(560, 43);
				this.lstDesc.TabIndex = 5;
				// 
				// formProcess
				// 
				this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
				this.ClientSize = new System.Drawing.Size(592, 393);
				this.Controls.AddRange(new System.Windows.Forms.Control[] {
																			  this.lstDesc,
																			  this.lsterror,
																			  this.cmdProcess,
																			  this.lststatus,
																			  this.lblError,
																			  this.imageXView1});
				this.Menu = this.mainMenu1;
				this.Name = "formProcess";
				this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
				this.Text = "C# Image Processor";
				this.Load += new System.EventHandler(this.BatchProcessor_Load);
				this.ResumeLayout(false);

			}
		#endregion

			/// <summary>
			/// The main entry point for the application.
			/// </summary>
			[STAThread]
			static void Main() 
			{
				Application.Run(new formProcess());
			}

			private void BatchProcessor_Load(object sender, System.EventArgs e)
			{
				//**The UnlockRuntime function must be called to distribute the runtime**
				//imagXpress2.License.UnlockRuntime(12345,12345,12345,12345);	
				loOpts = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
				//loOpts.Events.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
				//loOpts.Events.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler);
				
				PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
				PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );




				strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString ();
				strCurrentImage = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\window.jpg");
				strCurrentDir = System.IO.Directory.GetParent(strCurrentImage).FullName;

				if (System.IO.File.Exists(strCurrentImage)) 
				{
					try 
					{
						imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage,loOpts);
					}
					catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
					{
						PegasusError(eX,lblError);
					}
				}
			}

			private void mnuOpen_Click(object sender, System.EventArgs e)
			{
				System.String sFileName = PegasusOpenFile();
				if (sFileName.Length != 0) 
				{
					strCurrentImage = sFileName;
					strCurrentDir = System.IO.Directory.GetParent(strCurrentImage).FullName;
					try 
					{
						imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(sFileName,loOpts);
						imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest);
					} 
					catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
					{
						PegasusError(ex,lblError);
					}
				}
			}

			private void mnuQuit_Click(object sender, System.EventArgs e)
			{
				Application.Exit();
			}

			private void mnuShow_Click(object sender, System.EventArgs e)
			{
				this.mnuShow.Text = (imageXView1.Toolbar.Activated) ? "&Show":"&Hide";
				try 
				{
					imageXView1.Toolbar.Activated = !imageXView1.Toolbar.Activated;
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
				{
					PegasusError(eX,lblError);
				}
			}

			private void mnuAbout_Click(object sender, System.EventArgs e)
			{
					imagXpress1.AboutBox();
			}

			private void cmdProcess_Click(object sender, System.EventArgs e)
			{
				PegasusImaging.WinForms.ImagXpress8.ImageX currentImage;
				System.String[] strTIFFImages;
				System.String[] strJPGImages;

				strTIFFImages = System.IO.Directory.GetFiles(strCurrentDir,"*.tif");
				strJPGImages = System.IO.Directory.GetFiles(strCurrentDir,"*.jpg");

				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			
				foreach (System.String currImage in strJPGImages)
				{
					PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = imageXView1.Image;
					currentImage = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(currImage,loOpts);
					Application.DoEvents();
					imageXView1.Image = currentImage;
					Application.DoEvents();
					BatchProcessor prc = new BatchProcessor(currentImage);
					Application.DoEvents();
					try
					{
						prc.Process();
					} 
					catch (Exception eX)
					{
						PegasusError(eX,lblError);
					}
					Application.DoEvents();
					if (oldImage != null)
					{
						oldImage.Dispose();
						oldImage = null;
					}
				}
				foreach (System.String currImage in strTIFFImages)
				{
					PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = imageXView1.Image;
					currentImage = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(currImage,loOpts);
					Application.DoEvents();
					imageXView1.Image = currentImage;
					Application.DoEvents();
					BatchProcessor prc = new BatchProcessor(currentImage);
					Application.DoEvents();
					try
					{
						prc.Process();
					} 
					catch (Exception eX)
					{
						PegasusError(eX,lblError);
					}
					Application.DoEvents();
					if (oldImage != null)
					{
						oldImage.Dispose();
						oldImage = null;
					}
				}
			}

				
		}
	}
}

/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;

namespace DocumentImagingandImageCleanup
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	/// 
	
	public class Form1 : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;

		private PegasusImaging.WinForms.ImagXpress8.LoadOptions loLoadOptions;

		string imgFileName;
		string strCurrentDir;
		PegasusImaging.WinForms.ImagXpress8.ImageX imagX1;
		PegasusImaging.WinForms.ImagXpress8.Processor ixProcessor1;


		System.Drawing.Color cPadColor;
		int iDeskewPadCol;
		int iShearPadCol;
		private double dTemp;


		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuFileOpen;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem mnuFileQuit;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout;

		


		internal System.Windows.Forms.ListBox lstInfo;
		internal System.Windows.Forms.ListBox lstStatus;
		internal System.Windows.Forms.Label lblLoadStatus;
		internal System.Windows.Forms.Label lblError;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.TabControl TabControl1;
		internal System.Windows.Forms.TabPage docBorderCrop;
		internal System.Windows.Forms.GroupBox grbxDocBorderCrop;
		internal System.Windows.Forms.Button cmdBorderCrop;
		internal System.Windows.Forms.TabPage docLineRemoval;
		internal System.Windows.Forms.GroupBox grbxDocLineRemoval;
		internal System.Windows.Forms.Button cmdLineRemoval;
		internal System.Windows.Forms.HScrollBar hscrlMaxCharRepSize;
		internal System.Windows.Forms.Label lblMaxCharRepSizeVal;
		internal System.Windows.Forms.Label lblMaxCharRepSize;
		internal System.Windows.Forms.HScrollBar hscrMaxGap;
		internal System.Windows.Forms.Label lblMaxGapVal;
		internal System.Windows.Forms.Label lblMaxGap;
		internal System.Windows.Forms.HScrollBar hscrMinAspRatio;
		internal System.Windows.Forms.Label lblMinAspRatioVal;
		internal System.Windows.Forms.Label lblMinAspRatio;
		internal System.Windows.Forms.HScrollBar hscrMaxThickness;
		internal System.Windows.Forms.Label lblMaxThicknessVal;
		internal System.Windows.Forms.Label lblMaxThickness;
		internal System.Windows.Forms.Label lblMinLengthVal;
		internal System.Windows.Forms.HScrollBar hscrMinLength;
		internal System.Windows.Forms.Label lblMinLength;
		internal System.Windows.Forms.TabPage docDeskew;
		internal System.Windows.Forms.GroupBox grbxDocDeskew;
		internal System.Windows.Forms.RadioButton rbDeskewBlack;
		internal System.Windows.Forms.RadioButton rbDeskewWhite;
		internal System.Windows.Forms.Button cmdDeskew;
		internal System.Windows.Forms.CheckBox chkMaintOrigSize;
		internal System.Windows.Forms.Label lblQualityVal;
		internal System.Windows.Forms.HScrollBar hscrQuality;
		internal System.Windows.Forms.Label lblQuality;
		internal System.Windows.Forms.Label lblPadColor;
		internal System.Windows.Forms.Label lblMinConfidenceVal;
		internal System.Windows.Forms.HScrollBar hscrMinConfidence;
		internal System.Windows.Forms.Label lblMinConfidence;
		internal System.Windows.Forms.Label lblMinAngleVal;
		internal System.Windows.Forms.HScrollBar hscrMinAngle;
		internal System.Windows.Forms.Label lblMinAngle;
		internal System.Windows.Forms.TabPage docShear;
		internal System.Windows.Forms.GroupBox grbxDocShear;
		internal System.Windows.Forms.Button cmdShear;
		internal System.Windows.Forms.ComboBox cboShearType;
		internal System.Windows.Forms.Label lblShearType;
		internal System.Windows.Forms.RadioButton rbShearBlack;
		internal System.Windows.Forms.RadioButton rbShearWhite;
		internal System.Windows.Forms.Label lblShearPadColor;
		internal System.Windows.Forms.Label lblShearAngleVal;
		internal System.Windows.Forms.HScrollBar hscrShearAngle;
		internal System.Windows.Forms.Label lblShearAngle;
		internal System.Windows.Forms.TabPage docErode;
		internal System.Windows.Forms.GroupBox grbxDocErode;
		internal System.Windows.Forms.Button cmdErode;
		internal System.Windows.Forms.ComboBox cboErodeDirection;
		internal System.Windows.Forms.Label lblErodeDirection;
		internal System.Windows.Forms.Label lblErodeAmountVal;
		internal System.Windows.Forms.HScrollBar hscrErodeAmount;
		internal System.Windows.Forms.Label lblErodeAmount;
		internal System.Windows.Forms.TabPage docDilate;
		internal System.Windows.Forms.GroupBox grbxDocDilate;
		internal System.Windows.Forms.Button cmdDilate;
		internal System.Windows.Forms.ComboBox cboDilateDirection;
		internal System.Windows.Forms.Label lblDilateDirection;
		internal System.Windows.Forms.Label lblDilateAmountVal;
		internal System.Windows.Forms.HScrollBar hscrDilateAmount;
		internal System.Windows.Forms.Label lblDilateAmount;
		internal System.Windows.Forms.TabPage docDespeckle;
		internal System.Windows.Forms.GroupBox grbxDocDespeckle;
		internal System.Windows.Forms.Button cmdDespeckle;
		internal System.Windows.Forms.Label lblDespSpeckHVal;
		internal System.Windows.Forms.HScrollBar hscrDespSpeckHeight;
		internal System.Windows.Forms.Label lblDespSpeckHeight;
		internal System.Windows.Forms.Label lblDespSpeckWVal;
		internal System.Windows.Forms.HScrollBar hscrDespSpeckWidth;
		internal System.Windows.Forms.Label lblSpeckWidth;
		internal System.Windows.Forms.TabPage docZoomSmooth;
		internal System.Windows.Forms.GroupBox grbxDocZoomSmooth;
		internal System.Windows.Forms.Button cmdZoomSmooth;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		#region Pegasus Imaging Sample Application Standard Functions
		/*********************************************************************
			 *     Pegasus Imaging Corporation Standard Function Definitions     *
			 *********************************************************************/

		private System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		private System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		private System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		
		
		
		const System.String strDefaultImageFilter = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independent Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.pcx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphics (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision TARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*";

		string GetFileName(System.String FullName) 
		{
			return (FullName.Substring(FullName.LastIndexOf("\\")+1,FullName.Length - FullName.LastIndexOf("\\") - 1));
		}
		static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n";
		}

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = ErrorException.Message + "\n" + ErrorException.Source + "\n" + "Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat);
		}
		string PegasusOpenFile() 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strDefaultImageFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}

		string PegasusOpenFile(System.String strFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = "Select an Image File";
			dlg.Filter = strFilter;
			dlg.InitialDirectory = strCurrentDir;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf(@"\"),dlg.FileName.Length - dlg.FileName.LastIndexOf(@"\"));
				return dlg.FileName;
			} 
			else 
			{
				return "";
			}
		}
	
		#endregion

		public Form1()
		{
			
			//The UnlockControl function must be called to distribute the runtime**
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)
			
			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				//  Don't forget to dispose IX
				// 
				if (!(imagXpress1 == null)) 
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (!(imageXView1 == null)) 
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if (!(imageXView2 == null)) 
				{
					imageXView2.Dispose();
					imageXView2 = null;
				}
				if (!(ixProcessor1 == null)) 
				{
					ixProcessor1.Dispose();
					ixProcessor1 = null;
				}

				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnuFile = new System.Windows.Forms.MenuItem();
			this.mnuFileOpen = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.mnuFileQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lblError = new System.Windows.Forms.Label();
			this.lblLastError = new System.Windows.Forms.Label();
			this.TabControl1 = new System.Windows.Forms.TabControl();
			this.docBorderCrop = new System.Windows.Forms.TabPage();
			this.grbxDocBorderCrop = new System.Windows.Forms.GroupBox();
			this.cmdBorderCrop = new System.Windows.Forms.Button();
			this.docLineRemoval = new System.Windows.Forms.TabPage();
			this.grbxDocLineRemoval = new System.Windows.Forms.GroupBox();
			this.cmdLineRemoval = new System.Windows.Forms.Button();
			this.hscrlMaxCharRepSize = new System.Windows.Forms.HScrollBar();
			this.lblMaxCharRepSizeVal = new System.Windows.Forms.Label();
			this.lblMaxCharRepSize = new System.Windows.Forms.Label();
			this.hscrMaxGap = new System.Windows.Forms.HScrollBar();
			this.lblMaxGapVal = new System.Windows.Forms.Label();
			this.lblMaxGap = new System.Windows.Forms.Label();
			this.hscrMinAspRatio = new System.Windows.Forms.HScrollBar();
			this.lblMinAspRatioVal = new System.Windows.Forms.Label();
			this.lblMinAspRatio = new System.Windows.Forms.Label();
			this.hscrMaxThickness = new System.Windows.Forms.HScrollBar();
			this.lblMaxThicknessVal = new System.Windows.Forms.Label();
			this.lblMaxThickness = new System.Windows.Forms.Label();
			this.lblMinLengthVal = new System.Windows.Forms.Label();
			this.hscrMinLength = new System.Windows.Forms.HScrollBar();
			this.lblMinLength = new System.Windows.Forms.Label();
			this.docDeskew = new System.Windows.Forms.TabPage();
			this.grbxDocDeskew = new System.Windows.Forms.GroupBox();
			this.rbDeskewBlack = new System.Windows.Forms.RadioButton();
			this.rbDeskewWhite = new System.Windows.Forms.RadioButton();
			this.cmdDeskew = new System.Windows.Forms.Button();
			this.chkMaintOrigSize = new System.Windows.Forms.CheckBox();
			this.lblQualityVal = new System.Windows.Forms.Label();
			this.hscrQuality = new System.Windows.Forms.HScrollBar();
			this.lblQuality = new System.Windows.Forms.Label();
			this.lblPadColor = new System.Windows.Forms.Label();
			this.lblMinConfidenceVal = new System.Windows.Forms.Label();
			this.hscrMinConfidence = new System.Windows.Forms.HScrollBar();
			this.lblMinConfidence = new System.Windows.Forms.Label();
			this.lblMinAngleVal = new System.Windows.Forms.Label();
			this.hscrMinAngle = new System.Windows.Forms.HScrollBar();
			this.lblMinAngle = new System.Windows.Forms.Label();
			this.docShear = new System.Windows.Forms.TabPage();
			this.grbxDocShear = new System.Windows.Forms.GroupBox();
			this.cmdShear = new System.Windows.Forms.Button();
			this.cboShearType = new System.Windows.Forms.ComboBox();
			this.lblShearType = new System.Windows.Forms.Label();
			this.rbShearBlack = new System.Windows.Forms.RadioButton();
			this.rbShearWhite = new System.Windows.Forms.RadioButton();
			this.lblShearPadColor = new System.Windows.Forms.Label();
			this.lblShearAngleVal = new System.Windows.Forms.Label();
			this.hscrShearAngle = new System.Windows.Forms.HScrollBar();
			this.lblShearAngle = new System.Windows.Forms.Label();
			this.docErode = new System.Windows.Forms.TabPage();
			this.grbxDocErode = new System.Windows.Forms.GroupBox();
			this.cmdErode = new System.Windows.Forms.Button();
			this.cboErodeDirection = new System.Windows.Forms.ComboBox();
			this.lblErodeDirection = new System.Windows.Forms.Label();
			this.lblErodeAmountVal = new System.Windows.Forms.Label();
			this.hscrErodeAmount = new System.Windows.Forms.HScrollBar();
			this.lblErodeAmount = new System.Windows.Forms.Label();
			this.docDilate = new System.Windows.Forms.TabPage();
			this.grbxDocDilate = new System.Windows.Forms.GroupBox();
			this.cmdDilate = new System.Windows.Forms.Button();
			this.cboDilateDirection = new System.Windows.Forms.ComboBox();
			this.lblDilateDirection = new System.Windows.Forms.Label();
			this.lblDilateAmountVal = new System.Windows.Forms.Label();
			this.hscrDilateAmount = new System.Windows.Forms.HScrollBar();
			this.lblDilateAmount = new System.Windows.Forms.Label();
			this.docDespeckle = new System.Windows.Forms.TabPage();
			this.grbxDocDespeckle = new System.Windows.Forms.GroupBox();
			this.cmdDespeckle = new System.Windows.Forms.Button();
			this.lblDespSpeckHVal = new System.Windows.Forms.Label();
			this.hscrDespSpeckHeight = new System.Windows.Forms.HScrollBar();
			this.lblDespSpeckHeight = new System.Windows.Forms.Label();
			this.lblDespSpeckWVal = new System.Windows.Forms.Label();
			this.hscrDespSpeckWidth = new System.Windows.Forms.HScrollBar();
			this.lblSpeckWidth = new System.Windows.Forms.Label();
			this.docZoomSmooth = new System.Windows.Forms.TabPage();
			this.grbxDocZoomSmooth = new System.Windows.Forms.GroupBox();
			this.cmdZoomSmooth = new System.Windows.Forms.Button();
			this.TabControl1.SuspendLayout();
			this.docBorderCrop.SuspendLayout();
			this.grbxDocBorderCrop.SuspendLayout();
			this.docLineRemoval.SuspendLayout();
			this.grbxDocLineRemoval.SuspendLayout();
			this.docDeskew.SuspendLayout();
			this.grbxDocDeskew.SuspendLayout();
			this.docShear.SuspendLayout();
			this.grbxDocShear.SuspendLayout();
			this.docErode.SuspendLayout();
			this.grbxDocErode.SuspendLayout();
			this.docDilate.SuspendLayout();
			this.grbxDocDilate.SuspendLayout();
			this.docDespeckle.SuspendLayout();
			this.grbxDocDespeckle.SuspendLayout();
			this.docZoomSmooth.SuspendLayout();
			this.grbxDocZoomSmooth.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.imageXView1.Location = new System.Drawing.Point(24, 96);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(440, 267);
			this.imageXView1.TabIndex = 0;
			// 
			// imageXView2
			// 
			this.imageXView2.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView2.Location = new System.Drawing.Point(480, 96);
			this.imageXView2.Name = "imageXView2";
			this.imageXView2.Size = new System.Drawing.Size(416, 267);
			this.imageXView2.TabIndex = 1;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuFile,
																					  this.mnuToolbar,
																					  this.mnuAbout});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuFileOpen,
																					this.menuItem3,
																					this.mnuFileQuit});
			this.mnuFile.Text = "&File";
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Index = 0;
			this.mnuFileOpen.Text = "&Open";
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "-";
			// 
			// mnuFileQuit
			// 
			this.mnuFileQuit.Index = 2;
			this.mnuFileQuit.Text = "&Quit";
			this.mnuFileQuit.Click += new System.EventHandler(this.mnuFileQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "&Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "&Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "",
														 "1)Using the DocumentBorderCrop, DocumentDeskew, DocumentDespeckle, DocumentDilate" +
														 ", DocumentErode, DocumentLineRemoval,",
														 "   DocumentShear and DocumentSmoothZoom methods."});
			this.lstInfo.Location = new System.Drawing.Point(8, 16);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(880, 69);
			this.lstInfo.TabIndex = 34;
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(672, 539);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(216, 95);
			this.lstStatus.TabIndex = 33;
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(672, 499);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(160, 24);
			this.lblLoadStatus.TabIndex = 32;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lblError
			// 
			this.lblError.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblError.Location = new System.Drawing.Point(672, 411);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(192, 72);
			this.lblError.TabIndex = 31;
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblLastError.Location = new System.Drawing.Point(672, 387);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(168, 24);
			this.lblLastError.TabIndex = 30;
			this.lblLastError.Text = "Last Error:";
			// 
			// TabControl1
			// 
			this.TabControl1.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.TabControl1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.docBorderCrop,
																					  this.docLineRemoval,
																					  this.docDeskew,
																					  this.docShear,
																					  this.docErode,
																					  this.docDilate,
																					  this.docDespeckle,
																					  this.docZoomSmooth});
			this.TabControl1.Location = new System.Drawing.Point(24, 400);
			this.TabControl1.Name = "TabControl1";
			this.TabControl1.SelectedIndex = 0;
			this.TabControl1.Size = new System.Drawing.Size(640, 243);
			this.TabControl1.TabIndex = 35;
			// 
			// docBorderCrop
			// 
			this.docBorderCrop.Controls.AddRange(new System.Windows.Forms.Control[] {
																						this.grbxDocBorderCrop});
			this.docBorderCrop.Location = new System.Drawing.Point(4, 22);
			this.docBorderCrop.Name = "docBorderCrop";
			this.docBorderCrop.Size = new System.Drawing.Size(632, 217);
			this.docBorderCrop.TabIndex = 0;
			this.docBorderCrop.Text = "Doc Border Crop";
			// 
			// grbxDocBorderCrop
			// 
			this.grbxDocBorderCrop.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.grbxDocBorderCrop.Controls.AddRange(new System.Windows.Forms.Control[] {
																							this.cmdBorderCrop});
			this.grbxDocBorderCrop.Location = new System.Drawing.Point(8, 8);
			this.grbxDocBorderCrop.Name = "grbxDocBorderCrop";
			this.grbxDocBorderCrop.Size = new System.Drawing.Size(616, 224);
			this.grbxDocBorderCrop.TabIndex = 0;
			this.grbxDocBorderCrop.TabStop = false;
			this.grbxDocBorderCrop.Text = "Document Border Crop";
			// 
			// cmdBorderCrop
			// 
			this.cmdBorderCrop.Location = new System.Drawing.Point(216, 168);
			this.cmdBorderCrop.Name = "cmdBorderCrop";
			this.cmdBorderCrop.Size = new System.Drawing.Size(176, 40);
			this.cmdBorderCrop.TabIndex = 0;
			this.cmdBorderCrop.Text = "Document Border Crop";
			this.cmdBorderCrop.Click += new System.EventHandler(this.cmdBorderCrop_Click);
			// 
			// docLineRemoval
			// 
			this.docLineRemoval.Controls.AddRange(new System.Windows.Forms.Control[] {
																						 this.grbxDocLineRemoval});
			this.docLineRemoval.Location = new System.Drawing.Point(4, 22);
			this.docLineRemoval.Name = "docLineRemoval";
			this.docLineRemoval.Size = new System.Drawing.Size(632, 217);
			this.docLineRemoval.TabIndex = 2;
			this.docLineRemoval.Text = "Doc Line Removal";
			this.docLineRemoval.Visible = false;
			// 
			// grbxDocLineRemoval
			// 
			this.grbxDocLineRemoval.Controls.AddRange(new System.Windows.Forms.Control[] {
																							 this.cmdLineRemoval,
																							 this.hscrlMaxCharRepSize,
																							 this.lblMaxCharRepSizeVal,
																							 this.lblMaxCharRepSize,
																							 this.hscrMaxGap,
																							 this.lblMaxGapVal,
																							 this.lblMaxGap,
																							 this.hscrMinAspRatio,
																							 this.lblMinAspRatioVal,
																							 this.lblMinAspRatio,
																							 this.hscrMaxThickness,
																							 this.lblMaxThicknessVal,
																							 this.lblMaxThickness,
																							 this.lblMinLengthVal,
																							 this.hscrMinLength,
																							 this.lblMinLength});
			this.grbxDocLineRemoval.Location = new System.Drawing.Point(8, 8);
			this.grbxDocLineRemoval.Name = "grbxDocLineRemoval";
			this.grbxDocLineRemoval.Size = new System.Drawing.Size(616, 224);
			this.grbxDocLineRemoval.TabIndex = 0;
			this.grbxDocLineRemoval.TabStop = false;
			this.grbxDocLineRemoval.Text = "Document Line Removal";
			// 
			// cmdLineRemoval
			// 
			this.cmdLineRemoval.Location = new System.Drawing.Point(224, 192);
			this.cmdLineRemoval.Name = "cmdLineRemoval";
			this.cmdLineRemoval.Size = new System.Drawing.Size(200, 24);
			this.cmdLineRemoval.TabIndex = 15;
			this.cmdLineRemoval.Text = "Document Line Removal";
			this.cmdLineRemoval.Click += new System.EventHandler(this.cmdLineRemoval_Click);
			// 
			// hscrlMaxCharRepSize
			// 
			this.hscrlMaxCharRepSize.Location = new System.Drawing.Point(152, 160);
			this.hscrlMaxCharRepSize.Name = "hscrlMaxCharRepSize";
			this.hscrlMaxCharRepSize.Size = new System.Drawing.Size(368, 16);
			this.hscrlMaxCharRepSize.TabIndex = 14;
			this.hscrlMaxCharRepSize.Value = 20;
			this.hscrlMaxCharRepSize.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrlMaxCharRepSize_Scroll);
			// 
			// lblMaxCharRepSizeVal
			// 
			this.lblMaxCharRepSizeVal.Location = new System.Drawing.Point(536, 160);
			this.lblMaxCharRepSizeVal.Name = "lblMaxCharRepSizeVal";
			this.lblMaxCharRepSizeVal.Size = new System.Drawing.Size(72, 16);
			this.lblMaxCharRepSizeVal.TabIndex = 13;
			// 
			// lblMaxCharRepSize
			// 
			this.lblMaxCharRepSize.Location = new System.Drawing.Point(8, 160);
			this.lblMaxCharRepSize.Name = "lblMaxCharRepSize";
			this.lblMaxCharRepSize.Size = new System.Drawing.Size(144, 16);
			this.lblMaxCharRepSize.TabIndex = 12;
			this.lblMaxCharRepSize.Text = "Max Character Repair Size:";
			// 
			// hscrMaxGap
			// 
			this.hscrMaxGap.Location = new System.Drawing.Point(152, 128);
			this.hscrMaxGap.Maximum = 20;
			this.hscrMaxGap.Name = "hscrMaxGap";
			this.hscrMaxGap.Size = new System.Drawing.Size(368, 16);
			this.hscrMaxGap.TabIndex = 11;
			this.hscrMaxGap.Value = 1;
			this.hscrMaxGap.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrMaxGap_Scroll);
			// 
			// lblMaxGapVal
			// 
			this.lblMaxGapVal.Location = new System.Drawing.Point(536, 128);
			this.lblMaxGapVal.Name = "lblMaxGapVal";
			this.lblMaxGapVal.Size = new System.Drawing.Size(64, 16);
			this.lblMaxGapVal.TabIndex = 10;
			// 
			// lblMaxGap
			// 
			this.lblMaxGap.Location = new System.Drawing.Point(8, 128);
			this.lblMaxGap.Name = "lblMaxGap";
			this.lblMaxGap.Size = new System.Drawing.Size(120, 16);
			this.lblMaxGap.TabIndex = 9;
			this.lblMaxGap.Text = "Max Gap:";
			// 
			// hscrMinAspRatio
			// 
			this.hscrMinAspRatio.Location = new System.Drawing.Point(152, 96);
			this.hscrMinAspRatio.Maximum = 1000;
			this.hscrMinAspRatio.Minimum = 1;
			this.hscrMinAspRatio.Name = "hscrMinAspRatio";
			this.hscrMinAspRatio.Size = new System.Drawing.Size(368, 16);
			this.hscrMinAspRatio.TabIndex = 8;
			this.hscrMinAspRatio.Value = 10;
			this.hscrMinAspRatio.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrMinAspRatio_Scroll);
			// 
			// lblMinAspRatioVal
			// 
			this.lblMinAspRatioVal.Location = new System.Drawing.Point(536, 96);
			this.lblMinAspRatioVal.Name = "lblMinAspRatioVal";
			this.lblMinAspRatioVal.Size = new System.Drawing.Size(64, 16);
			this.lblMinAspRatioVal.TabIndex = 7;
			// 
			// lblMinAspRatio
			// 
			this.lblMinAspRatio.Location = new System.Drawing.Point(8, 96);
			this.lblMinAspRatio.Name = "lblMinAspRatio";
			this.lblMinAspRatio.Size = new System.Drawing.Size(128, 24);
			this.lblMinAspRatio.TabIndex = 6;
			this.lblMinAspRatio.Text = "Min Aspect Ratio:";
			// 
			// hscrMaxThickness
			// 
			this.hscrMaxThickness.Location = new System.Drawing.Point(152, 64);
			this.hscrMaxThickness.Name = "hscrMaxThickness";
			this.hscrMaxThickness.Size = new System.Drawing.Size(368, 16);
			this.hscrMaxThickness.TabIndex = 5;
			this.hscrMaxThickness.Value = 20;
			this.hscrMaxThickness.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrMaxThickness_Scroll);
			// 
			// lblMaxThicknessVal
			// 
			this.lblMaxThicknessVal.Location = new System.Drawing.Point(536, 64);
			this.lblMaxThicknessVal.Name = "lblMaxThicknessVal";
			this.lblMaxThicknessVal.Size = new System.Drawing.Size(56, 16);
			this.lblMaxThicknessVal.TabIndex = 4;
			// 
			// lblMaxThickness
			// 
			this.lblMaxThickness.Location = new System.Drawing.Point(8, 64);
			this.lblMaxThickness.Name = "lblMaxThickness";
			this.lblMaxThickness.Size = new System.Drawing.Size(128, 16);
			this.lblMaxThickness.TabIndex = 3;
			this.lblMaxThickness.Text = "Max Thickness:";
			// 
			// lblMinLengthVal
			// 
			this.lblMinLengthVal.Location = new System.Drawing.Point(536, 24);
			this.lblMinLengthVal.Name = "lblMinLengthVal";
			this.lblMinLengthVal.Size = new System.Drawing.Size(56, 16);
			this.lblMinLengthVal.TabIndex = 2;
			// 
			// hscrMinLength
			// 
			this.hscrMinLength.Location = new System.Drawing.Point(152, 24);
			this.hscrMinLength.Maximum = 20000;
			this.hscrMinLength.Minimum = 10;
			this.hscrMinLength.Name = "hscrMinLength";
			this.hscrMinLength.Size = new System.Drawing.Size(368, 16);
			this.hscrMinLength.TabIndex = 1;
			this.hscrMinLength.Value = 50;
			this.hscrMinLength.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrMinLength_Scroll_1);
			// 
			// lblMinLength
			// 
			this.lblMinLength.Location = new System.Drawing.Point(8, 24);
			this.lblMinLength.Name = "lblMinLength";
			this.lblMinLength.Size = new System.Drawing.Size(120, 16);
			this.lblMinLength.TabIndex = 0;
			this.lblMinLength.Text = "Minimum Length:";
			// 
			// docDeskew
			// 
			this.docDeskew.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.grbxDocDeskew});
			this.docDeskew.Location = new System.Drawing.Point(4, 22);
			this.docDeskew.Name = "docDeskew";
			this.docDeskew.Size = new System.Drawing.Size(632, 238);
			this.docDeskew.TabIndex = 6;
			this.docDeskew.Text = "Doc Deskew";
			this.docDeskew.Visible = false;
			// 
			// grbxDocDeskew
			// 
			this.grbxDocDeskew.Controls.AddRange(new System.Windows.Forms.Control[] {
																						this.rbDeskewBlack,
																						this.rbDeskewWhite,
																						this.cmdDeskew,
																						this.chkMaintOrigSize,
																						this.lblQualityVal,
																						this.hscrQuality,
																						this.lblQuality,
																						this.lblPadColor,
																						this.lblMinConfidenceVal,
																						this.hscrMinConfidence,
																						this.lblMinConfidence,
																						this.lblMinAngleVal,
																						this.hscrMinAngle,
																						this.lblMinAngle});
			this.grbxDocDeskew.Location = new System.Drawing.Point(8, 8);
			this.grbxDocDeskew.Name = "grbxDocDeskew";
			this.grbxDocDeskew.Size = new System.Drawing.Size(616, 224);
			this.grbxDocDeskew.TabIndex = 0;
			this.grbxDocDeskew.TabStop = false;
			this.grbxDocDeskew.Text = "Document Deskew";
			// 
			// rbDeskewBlack
			// 
			this.rbDeskewBlack.Location = new System.Drawing.Point(304, 96);
			this.rbDeskewBlack.Name = "rbDeskewBlack";
			this.rbDeskewBlack.Size = new System.Drawing.Size(96, 24);
			this.rbDeskewBlack.TabIndex = 15;
			this.rbDeskewBlack.Text = "Black";
			this.rbDeskewBlack.CheckedChanged += new System.EventHandler(this.rbDeskewBlack_CheckedChanged);
			// 
			// rbDeskewWhite
			// 
			this.rbDeskewWhite.Checked = true;
			this.rbDeskewWhite.Location = new System.Drawing.Point(184, 96);
			this.rbDeskewWhite.Name = "rbDeskewWhite";
			this.rbDeskewWhite.Size = new System.Drawing.Size(96, 24);
			this.rbDeskewWhite.TabIndex = 14;
			this.rbDeskewWhite.TabStop = true;
			this.rbDeskewWhite.Text = "White";
			this.rbDeskewWhite.CheckedChanged += new System.EventHandler(this.rbDeskewWhite_CheckedChanged);
			// 
			// cmdDeskew
			// 
			this.cmdDeskew.Location = new System.Drawing.Point(232, 184);
			this.cmdDeskew.Name = "cmdDeskew";
			this.cmdDeskew.Size = new System.Drawing.Size(160, 32);
			this.cmdDeskew.TabIndex = 13;
			this.cmdDeskew.Text = "Document Deskew";
			this.cmdDeskew.Click += new System.EventHandler(this.cmdDeskew_Click);
			// 
			// chkMaintOrigSize
			// 
			this.chkMaintOrigSize.Location = new System.Drawing.Point(16, 184);
			this.chkMaintOrigSize.Name = "chkMaintOrigSize";
			this.chkMaintOrigSize.Size = new System.Drawing.Size(136, 24);
			this.chkMaintOrigSize.TabIndex = 12;
			this.chkMaintOrigSize.Text = "Maintain Original Size";
			// 
			// lblQualityVal
			// 
			this.lblQualityVal.Location = new System.Drawing.Point(544, 136);
			this.lblQualityVal.Name = "lblQualityVal";
			this.lblQualityVal.Size = new System.Drawing.Size(48, 24);
			this.lblQualityVal.TabIndex = 11;
			// 
			// hscrQuality
			// 
			this.hscrQuality.Location = new System.Drawing.Point(176, 136);
			this.hscrQuality.Name = "hscrQuality";
			this.hscrQuality.Size = new System.Drawing.Size(344, 16);
			this.hscrQuality.TabIndex = 10;
			this.hscrQuality.Value = 80;
			this.hscrQuality.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrQuality_Scroll);
			// 
			// lblQuality
			// 
			this.lblQuality.Location = new System.Drawing.Point(16, 136);
			this.lblQuality.Name = "lblQuality";
			this.lblQuality.Size = new System.Drawing.Size(96, 24);
			this.lblQuality.TabIndex = 9;
			this.lblQuality.Text = "Quality:";
			// 
			// lblPadColor
			// 
			this.lblPadColor.Location = new System.Drawing.Point(16, 96);
			this.lblPadColor.Name = "lblPadColor";
			this.lblPadColor.Size = new System.Drawing.Size(104, 24);
			this.lblPadColor.TabIndex = 6;
			this.lblPadColor.Text = "Pad Color:";
			// 
			// lblMinConfidenceVal
			// 
			this.lblMinConfidenceVal.Location = new System.Drawing.Point(544, 64);
			this.lblMinConfidenceVal.Name = "lblMinConfidenceVal";
			this.lblMinConfidenceVal.Size = new System.Drawing.Size(56, 24);
			this.lblMinConfidenceVal.TabIndex = 5;
			// 
			// hscrMinConfidence
			// 
			this.hscrMinConfidence.Location = new System.Drawing.Point(176, 64);
			this.hscrMinConfidence.Name = "hscrMinConfidence";
			this.hscrMinConfidence.Size = new System.Drawing.Size(344, 16);
			this.hscrMinConfidence.TabIndex = 4;
			this.hscrMinConfidence.Value = 50;
			this.hscrMinConfidence.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrMinConfidence_Scroll);
			// 
			// lblMinConfidence
			// 
			this.lblMinConfidence.Location = new System.Drawing.Point(16, 64);
			this.lblMinConfidence.Name = "lblMinConfidence";
			this.lblMinConfidence.Size = new System.Drawing.Size(120, 24);
			this.lblMinConfidence.TabIndex = 3;
			this.lblMinConfidence.Text = "Minimum Confidence:";
			// 
			// lblMinAngleVal
			// 
			this.lblMinAngleVal.Location = new System.Drawing.Point(544, 32);
			this.lblMinAngleVal.Name = "lblMinAngleVal";
			this.lblMinAngleVal.Size = new System.Drawing.Size(48, 16);
			this.lblMinAngleVal.TabIndex = 2;
			// 
			// hscrMinAngle
			// 
			this.hscrMinAngle.Location = new System.Drawing.Point(176, 24);
			this.hscrMinAngle.Maximum = 500;
			this.hscrMinAngle.Name = "hscrMinAngle";
			this.hscrMinAngle.Size = new System.Drawing.Size(344, 16);
			this.hscrMinAngle.TabIndex = 1;
			this.hscrMinAngle.Value = 20;
			this.hscrMinAngle.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrMinAngle_Scroll);
			// 
			// lblMinAngle
			// 
			this.lblMinAngle.Location = new System.Drawing.Point(16, 24);
			this.lblMinAngle.Name = "lblMinAngle";
			this.lblMinAngle.Size = new System.Drawing.Size(112, 24);
			this.lblMinAngle.TabIndex = 0;
			this.lblMinAngle.Text = "Minimum Angle:";
			// 
			// docShear
			// 
			this.docShear.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.grbxDocShear});
			this.docShear.Location = new System.Drawing.Point(4, 22);
			this.docShear.Name = "docShear";
			this.docShear.Size = new System.Drawing.Size(632, 217);
			this.docShear.TabIndex = 3;
			this.docShear.Text = "Doc Shear";
			this.docShear.Visible = false;
			// 
			// grbxDocShear
			// 
			this.grbxDocShear.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.cmdShear,
																					   this.cboShearType,
																					   this.lblShearType,
																					   this.rbShearBlack,
																					   this.rbShearWhite,
																					   this.lblShearPadColor,
																					   this.lblShearAngleVal,
																					   this.hscrShearAngle,
																					   this.lblShearAngle});
			this.grbxDocShear.Location = new System.Drawing.Point(8, 8);
			this.grbxDocShear.Name = "grbxDocShear";
			this.grbxDocShear.Size = new System.Drawing.Size(616, 224);
			this.grbxDocShear.TabIndex = 0;
			this.grbxDocShear.TabStop = false;
			this.grbxDocShear.Text = "Document Shear";
			// 
			// cmdShear
			// 
			this.cmdShear.Location = new System.Drawing.Point(200, 176);
			this.cmdShear.Name = "cmdShear";
			this.cmdShear.Size = new System.Drawing.Size(200, 32);
			this.cmdShear.TabIndex = 8;
			this.cmdShear.Text = "Document Shear";
			this.cmdShear.Click += new System.EventHandler(this.cmdShear_Click);
			// 
			// cboShearType
			// 
			this.cboShearType.Items.AddRange(new object[] {
															  "DocShearVertical",
															  "DocShearHorizontal"});
			this.cboShearType.Location = new System.Drawing.Point(152, 128);
			this.cboShearType.Name = "cboShearType";
			this.cboShearType.Size = new System.Drawing.Size(232, 21);
			this.cboShearType.TabIndex = 7;
			// 
			// lblShearType
			// 
			this.lblShearType.Location = new System.Drawing.Point(24, 128);
			this.lblShearType.Name = "lblShearType";
			this.lblShearType.Size = new System.Drawing.Size(120, 24);
			this.lblShearType.TabIndex = 6;
			this.lblShearType.Text = "Shear Type:";
			// 
			// rbShearBlack
			// 
			this.rbShearBlack.Location = new System.Drawing.Point(304, 72);
			this.rbShearBlack.Name = "rbShearBlack";
			this.rbShearBlack.Size = new System.Drawing.Size(128, 24);
			this.rbShearBlack.TabIndex = 5;
			this.rbShearBlack.Text = "Black";
			this.rbShearBlack.CheckedChanged += new System.EventHandler(this.rbShearBlack_CheckedChanged);
			// 
			// rbShearWhite
			// 
			this.rbShearWhite.Checked = true;
			this.rbShearWhite.Location = new System.Drawing.Point(168, 72);
			this.rbShearWhite.Name = "rbShearWhite";
			this.rbShearWhite.Size = new System.Drawing.Size(112, 24);
			this.rbShearWhite.TabIndex = 4;
			this.rbShearWhite.TabStop = true;
			this.rbShearWhite.Text = "White";
			this.rbShearWhite.CheckedChanged += new System.EventHandler(this.rbShearWhite_CheckedChanged);
			// 
			// lblShearPadColor
			// 
			this.lblShearPadColor.Location = new System.Drawing.Point(24, 72);
			this.lblShearPadColor.Name = "lblShearPadColor";
			this.lblShearPadColor.Size = new System.Drawing.Size(120, 24);
			this.lblShearPadColor.TabIndex = 3;
			this.lblShearPadColor.Text = "Pad Color:";
			// 
			// lblShearAngleVal
			// 
			this.lblShearAngleVal.Location = new System.Drawing.Point(536, 32);
			this.lblShearAngleVal.Name = "lblShearAngleVal";
			this.lblShearAngleVal.Size = new System.Drawing.Size(56, 16);
			this.lblShearAngleVal.TabIndex = 2;
			// 
			// hscrShearAngle
			// 
			this.hscrShearAngle.Location = new System.Drawing.Point(152, 32);
			this.hscrShearAngle.Maximum = 70;
			this.hscrShearAngle.Minimum = -70;
			this.hscrShearAngle.Name = "hscrShearAngle";
			this.hscrShearAngle.Size = new System.Drawing.Size(368, 16);
			this.hscrShearAngle.TabIndex = 1;
			this.hscrShearAngle.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrShearAngle_Scroll);
			// 
			// lblShearAngle
			// 
			this.lblShearAngle.Location = new System.Drawing.Point(24, 32);
			this.lblShearAngle.Name = "lblShearAngle";
			this.lblShearAngle.Size = new System.Drawing.Size(104, 24);
			this.lblShearAngle.TabIndex = 0;
			this.lblShearAngle.Text = "Shear Angle:";
			// 
			// docErode
			// 
			this.docErode.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.grbxDocErode});
			this.docErode.Location = new System.Drawing.Point(4, 22);
			this.docErode.Name = "docErode";
			this.docErode.Size = new System.Drawing.Size(632, 238);
			this.docErode.TabIndex = 1;
			this.docErode.Text = "Doc Erode";
			this.docErode.Visible = false;
			// 
			// grbxDocErode
			// 
			this.grbxDocErode.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.cmdErode,
																					   this.cboErodeDirection,
																					   this.lblErodeDirection,
																					   this.lblErodeAmountVal,
																					   this.hscrErodeAmount,
																					   this.lblErodeAmount});
			this.grbxDocErode.Location = new System.Drawing.Point(8, 8);
			this.grbxDocErode.Name = "grbxDocErode";
			this.grbxDocErode.Size = new System.Drawing.Size(616, 224);
			this.grbxDocErode.TabIndex = 0;
			this.grbxDocErode.TabStop = false;
			this.grbxDocErode.Text = "Document Erode";
			// 
			// cmdErode
			// 
			this.cmdErode.Location = new System.Drawing.Point(240, 176);
			this.cmdErode.Name = "cmdErode";
			this.cmdErode.Size = new System.Drawing.Size(144, 32);
			this.cmdErode.TabIndex = 5;
			this.cmdErode.Text = "Document Erode";
			this.cmdErode.Click += new System.EventHandler(this.cmdErode_Click);
			// 
			// cboErodeDirection
			// 
			this.cboErodeDirection.Items.AddRange(new object[] {
																   "EnhancementDirectionAll",
																   "EnhancementDirectionLeft",
																   "EnhancementDirectionRight",
																   "EnhancementDirectionUp",
																   "EnhancementDirectionDown",
																   "EnhancementDirectionLeftUp",
																   "EnhancementDirectionLeftDown",
																   "EnhancementDirectionRightUp",
																   "EnhancementDirectionRightDown"});
			this.cboErodeDirection.Location = new System.Drawing.Point(192, 112);
			this.cboErodeDirection.Name = "cboErodeDirection";
			this.cboErodeDirection.Size = new System.Drawing.Size(216, 21);
			this.cboErodeDirection.TabIndex = 4;
			// 
			// lblErodeDirection
			// 
			this.lblErodeDirection.Location = new System.Drawing.Point(24, 112);
			this.lblErodeDirection.Name = "lblErodeDirection";
			this.lblErodeDirection.Size = new System.Drawing.Size(128, 24);
			this.lblErodeDirection.TabIndex = 3;
			this.lblErodeDirection.Text = "Direction:";
			// 
			// lblErodeAmountVal
			// 
			this.lblErodeAmountVal.Location = new System.Drawing.Point(536, 40);
			this.lblErodeAmountVal.Name = "lblErodeAmountVal";
			this.lblErodeAmountVal.Size = new System.Drawing.Size(64, 24);
			this.lblErodeAmountVal.TabIndex = 2;
			// 
			// hscrErodeAmount
			// 
			this.hscrErodeAmount.Location = new System.Drawing.Point(192, 40);
			this.hscrErodeAmount.Maximum = 500;
			this.hscrErodeAmount.Minimum = 1;
			this.hscrErodeAmount.Name = "hscrErodeAmount";
			this.hscrErodeAmount.Size = new System.Drawing.Size(320, 16);
			this.hscrErodeAmount.TabIndex = 1;
			this.hscrErodeAmount.Value = 1;
			this.hscrErodeAmount.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrErodeAmount_Scroll);
			// 
			// lblErodeAmount
			// 
			this.lblErodeAmount.Location = new System.Drawing.Point(24, 40);
			this.lblErodeAmount.Name = "lblErodeAmount";
			this.lblErodeAmount.Size = new System.Drawing.Size(120, 24);
			this.lblErodeAmount.TabIndex = 0;
			this.lblErodeAmount.Text = "Erode Amount:";
			// 
			// docDilate
			// 
			this.docDilate.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.grbxDocDilate});
			this.docDilate.Location = new System.Drawing.Point(4, 22);
			this.docDilate.Name = "docDilate";
			this.docDilate.Size = new System.Drawing.Size(632, 217);
			this.docDilate.TabIndex = 4;
			this.docDilate.Text = "Doc Dilate";
			this.docDilate.Visible = false;
			// 
			// grbxDocDilate
			// 
			this.grbxDocDilate.Controls.AddRange(new System.Windows.Forms.Control[] {
																						this.cmdDilate,
																						this.cboDilateDirection,
																						this.lblDilateDirection,
																						this.lblDilateAmountVal,
																						this.hscrDilateAmount,
																						this.lblDilateAmount});
			this.grbxDocDilate.Location = new System.Drawing.Point(8, 8);
			this.grbxDocDilate.Name = "grbxDocDilate";
			this.grbxDocDilate.Size = new System.Drawing.Size(616, 224);
			this.grbxDocDilate.TabIndex = 0;
			this.grbxDocDilate.TabStop = false;
			this.grbxDocDilate.Text = "Document Dilate";
			// 
			// cmdDilate
			// 
			this.cmdDilate.Location = new System.Drawing.Point(232, 176);
			this.cmdDilate.Name = "cmdDilate";
			this.cmdDilate.Size = new System.Drawing.Size(152, 32);
			this.cmdDilate.TabIndex = 5;
			this.cmdDilate.Text = "Document Dilate";
			this.cmdDilate.Click += new System.EventHandler(this.cmdDilate_Click);
			// 
			// cboDilateDirection
			// 
			this.cboDilateDirection.Items.AddRange(new object[] {
																	"EnhancementDirectionAll",
																	"EnhancementDirectionLeft",
																	"EnhancementDirectionRight",
																	"EnhancementDirectionUp",
																	"EnhancementDirectionDown",
																	"EnhancementDirectionLeftUp",
																	"EnhancementDirectionLeftDown",
																	"EnhancementDirectionRightUp",
																	"EnhancementDirectionRightDown"});
			this.cboDilateDirection.Location = new System.Drawing.Point(184, 112);
			this.cboDilateDirection.Name = "cboDilateDirection";
			this.cboDilateDirection.Size = new System.Drawing.Size(224, 21);
			this.cboDilateDirection.TabIndex = 4;
			// 
			// lblDilateDirection
			// 
			this.lblDilateDirection.Location = new System.Drawing.Point(24, 112);
			this.lblDilateDirection.Name = "lblDilateDirection";
			this.lblDilateDirection.Size = new System.Drawing.Size(128, 16);
			this.lblDilateDirection.TabIndex = 3;
			this.lblDilateDirection.Text = "Direction:";
			// 
			// lblDilateAmountVal
			// 
			this.lblDilateAmountVal.Location = new System.Drawing.Point(536, 40);
			this.lblDilateAmountVal.Name = "lblDilateAmountVal";
			this.lblDilateAmountVal.Size = new System.Drawing.Size(64, 24);
			this.lblDilateAmountVal.TabIndex = 2;
			// 
			// hscrDilateAmount
			// 
			this.hscrDilateAmount.Location = new System.Drawing.Point(184, 40);
			this.hscrDilateAmount.Maximum = 500;
			this.hscrDilateAmount.Minimum = 1;
			this.hscrDilateAmount.Name = "hscrDilateAmount";
			this.hscrDilateAmount.Size = new System.Drawing.Size(336, 16);
			this.hscrDilateAmount.TabIndex = 1;
			this.hscrDilateAmount.Value = 1;
			this.hscrDilateAmount.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrDilateAmount_Scroll);
			// 
			// lblDilateAmount
			// 
			this.lblDilateAmount.Location = new System.Drawing.Point(24, 40);
			this.lblDilateAmount.Name = "lblDilateAmount";
			this.lblDilateAmount.Size = new System.Drawing.Size(136, 16);
			this.lblDilateAmount.TabIndex = 0;
			this.lblDilateAmount.Text = "Dilate Amount:";
			// 
			// docDespeckle
			// 
			this.docDespeckle.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.grbxDocDespeckle});
			this.docDespeckle.Location = new System.Drawing.Point(4, 22);
			this.docDespeckle.Name = "docDespeckle";
			this.docDespeckle.Size = new System.Drawing.Size(632, 238);
			this.docDespeckle.TabIndex = 5;
			this.docDespeckle.Text = "Doc Despeckle";
			this.docDespeckle.Visible = false;
			// 
			// grbxDocDespeckle
			// 
			this.grbxDocDespeckle.Controls.AddRange(new System.Windows.Forms.Control[] {
																						   this.cmdDespeckle,
																						   this.lblDespSpeckHVal,
																						   this.hscrDespSpeckHeight,
																						   this.lblDespSpeckHeight,
																						   this.lblDespSpeckWVal,
																						   this.hscrDespSpeckWidth,
																						   this.lblSpeckWidth});
			this.grbxDocDespeckle.Location = new System.Drawing.Point(8, 8);
			this.grbxDocDespeckle.Name = "grbxDocDespeckle";
			this.grbxDocDespeckle.Size = new System.Drawing.Size(616, 224);
			this.grbxDocDespeckle.TabIndex = 0;
			this.grbxDocDespeckle.TabStop = false;
			this.grbxDocDespeckle.Text = "Document Despeckle";
			// 
			// cmdDespeckle
			// 
			this.cmdDespeckle.Location = new System.Drawing.Point(240, 168);
			this.cmdDespeckle.Name = "cmdDespeckle";
			this.cmdDespeckle.Size = new System.Drawing.Size(136, 40);
			this.cmdDespeckle.TabIndex = 6;
			this.cmdDespeckle.Text = "Document Despeckle";
			this.cmdDespeckle.Click += new System.EventHandler(this.cmdDespeckle_Click);
			// 
			// lblDespSpeckHVal
			// 
			this.lblDespSpeckHVal.Location = new System.Drawing.Point(528, 96);
			this.lblDespSpeckHVal.Name = "lblDespSpeckHVal";
			this.lblDespSpeckHVal.Size = new System.Drawing.Size(56, 24);
			this.lblDespSpeckHVal.TabIndex = 5;
			// 
			// hscrDespSpeckHeight
			// 
			this.hscrDespSpeckHeight.Location = new System.Drawing.Point(192, 96);
			this.hscrDespSpeckHeight.Minimum = 1;
			this.hscrDespSpeckHeight.Name = "hscrDespSpeckHeight";
			this.hscrDespSpeckHeight.Size = new System.Drawing.Size(312, 16);
			this.hscrDespSpeckHeight.TabIndex = 4;
			this.hscrDespSpeckHeight.Value = 2;
			this.hscrDespSpeckHeight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrDespSpeckHeight_Scroll);
			// 
			// lblDespSpeckHeight
			// 
			this.lblDespSpeckHeight.Location = new System.Drawing.Point(24, 96);
			this.lblDespSpeckHeight.Name = "lblDespSpeckHeight";
			this.lblDespSpeckHeight.Size = new System.Drawing.Size(128, 32);
			this.lblDespSpeckHeight.TabIndex = 3;
			this.lblDespSpeckHeight.Text = "Speck Height:";
			// 
			// lblDespSpeckWVal
			// 
			this.lblDespSpeckWVal.Location = new System.Drawing.Point(528, 40);
			this.lblDespSpeckWVal.Name = "lblDespSpeckWVal";
			this.lblDespSpeckWVal.Size = new System.Drawing.Size(64, 24);
			this.lblDespSpeckWVal.TabIndex = 2;
			// 
			// hscrDespSpeckWidth
			// 
			this.hscrDespSpeckWidth.Location = new System.Drawing.Point(192, 40);
			this.hscrDespSpeckWidth.Minimum = 1;
			this.hscrDespSpeckWidth.Name = "hscrDespSpeckWidth";
			this.hscrDespSpeckWidth.Size = new System.Drawing.Size(312, 16);
			this.hscrDespSpeckWidth.TabIndex = 1;
			this.hscrDespSpeckWidth.Value = 2;
			this.hscrDespSpeckWidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscrDespSpeckWidth_Scroll);
			// 
			// lblSpeckWidth
			// 
			this.lblSpeckWidth.Location = new System.Drawing.Point(24, 40);
			this.lblSpeckWidth.Name = "lblSpeckWidth";
			this.lblSpeckWidth.Size = new System.Drawing.Size(136, 24);
			this.lblSpeckWidth.TabIndex = 0;
			this.lblSpeckWidth.Text = "Speck Width:";
			// 
			// docZoomSmooth
			// 
			this.docZoomSmooth.Controls.AddRange(new System.Windows.Forms.Control[] {
																						this.grbxDocZoomSmooth});
			this.docZoomSmooth.Location = new System.Drawing.Point(4, 22);
			this.docZoomSmooth.Name = "docZoomSmooth";
			this.docZoomSmooth.Size = new System.Drawing.Size(632, 238);
			this.docZoomSmooth.TabIndex = 7;
			this.docZoomSmooth.Text = "Doc ZoomSmooth";
			this.docZoomSmooth.Visible = false;
			// 
			// grbxDocZoomSmooth
			// 
			this.grbxDocZoomSmooth.Controls.AddRange(new System.Windows.Forms.Control[] {
																							this.cmdZoomSmooth});
			this.grbxDocZoomSmooth.Location = new System.Drawing.Point(8, 8);
			this.grbxDocZoomSmooth.Name = "grbxDocZoomSmooth";
			this.grbxDocZoomSmooth.Size = new System.Drawing.Size(616, 224);
			this.grbxDocZoomSmooth.TabIndex = 0;
			this.grbxDocZoomSmooth.TabStop = false;
			this.grbxDocZoomSmooth.Text = "Document ZoomSmooth";
			// 
			// cmdZoomSmooth
			// 
			this.cmdZoomSmooth.Location = new System.Drawing.Point(208, 136);
			this.cmdZoomSmooth.Name = "cmdZoomSmooth";
			this.cmdZoomSmooth.Size = new System.Drawing.Size(184, 40);
			this.cmdZoomSmooth.TabIndex = 0;
			this.cmdZoomSmooth.Text = "Document ZoomSmooth";
			this.cmdZoomSmooth.Click += new System.EventHandler(this.cmdZoomSmooth_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(912, 660);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.TabControl1,
																		  this.lstInfo,
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.lblError,
																		  this.lblLastError,
																		  this.imageXView2,
																		  this.imageXView1});
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DocumentImagingandCleanUp";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.TabControl1.ResumeLayout(false);
			this.docBorderCrop.ResumeLayout(false);
			this.grbxDocBorderCrop.ResumeLayout(false);
			this.docLineRemoval.ResumeLayout(false);
			this.grbxDocLineRemoval.ResumeLayout(false);
			this.docDeskew.ResumeLayout(false);
			this.grbxDocDeskew.ResumeLayout(false);
			this.docShear.ResumeLayout(false);
			this.grbxDocShear.ResumeLayout(false);
			this.docErode.ResumeLayout(false);
			this.grbxDocErode.ResumeLayout(false);
			this.docDilate.ResumeLayout(false);
			this.grbxDocDilate.ResumeLayout(false);
			this.docDespeckle.ResumeLayout(false);
			this.grbxDocDespeckle.ResumeLayout(false);
			this.docZoomSmooth.ResumeLayout(false);
			this.grbxDocZoomSmooth.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		
		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			try 
			{
				// **The UnlockRuntime function must be called to distribute the runtime**
				// imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)

				//Create a new load options object
				loLoadOptions = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
				
				//this is where events are assigned. This happens before the file gets loaded.
				PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
				PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex,lblError);
			}

			cPadColor = new System.Drawing.Color();
			ixProcessor1 = new PegasusImaging.WinForms.ImagXpress8.Processor();
			dTemp = hscrMinAngle.Value * 0.01;
			lblMinAngleVal.Text = dTemp.ToString();
			lblMinConfidenceVal.Text = hscrMinConfidence.Value.ToString();
			lblQualityVal.Text = hscrQuality.Value.ToString();
			lblDespSpeckWVal.Text = hscrDespSpeckWidth.Value.ToString();
			lblDespSpeckHVal.Text = hscrDespSpeckHeight.Value.ToString();
			lblDilateAmountVal.Text = hscrDilateAmount.Value.ToString();
			lblErodeAmountVal.Text = hscrErodeAmount.Value.ToString();
			lblShearAngleVal.Text = hscrShearAngle.Value.ToString();
			lblMinLengthVal.Text = hscrMinLength.Value.ToString();
			lblMaxThicknessVal.Text = hscrMaxThickness.Value.ToString();
			lblMinAspRatioVal.Text = hscrMinAspRatio.Value.ToString();
			lblMaxGapVal.Text = hscrMaxGap.Value.ToString();
			lblMaxCharRepSizeVal.Text = hscrlMaxCharRepSize.Value.ToString();
			cboDilateDirection.SelectedIndex = 0;
			cboErodeDirection.SelectedIndex = 0;
			cboShearType.SelectedIndex = 0;
			cPadColor = Color.FromKnownColor(KnownColor.White);
			iDeskewPadCol = cPadColor.ToArgb();
			iShearPadCol = cPadColor.ToArgb();

			strCurrentDir = System.IO.Directory.GetCurrentDirectory();
			imgFileName = System.IO.Path.Combine(strCurrentDir, @"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\barcode.pcx");
				strCurrentDir = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\");
			LoadFile();
			
		}

		private void LoadFile() 
		{
			try 
			{
				imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(imgFileName,loLoadOptions);
				
				imageXView1.Image = imagX1;
				imageXView1.ZoomToFit(ZoomToFitType.FitBest);

     			// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
			
		}
		private void cmdBorderCrop_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}


				ixProcessor1.DocumentBorderCrop();
				imageXView2.Image = ixProcessor1.Image;
								
				imageXView1.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		
		}
		private void cmdLineRemoval_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}
				ixProcessor1.DocumentLineRemoval((int)hscrMinLength.Value,(short) hscrMaxThickness.Value,(double) hscrMinAspRatio.Value,(short) hscrMaxGap.Value, (short)hscrlMaxCharRepSize.Value);
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}
			
		private void cmdDeskew_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}								
				ixProcessor1.DocumentDeskew(((double)hscrMinAngle.Value * 0.01),(short) hscrMinConfidence.Value,(int) iDeskewPadCol, chkMaintOrigSize.Checked,(short) hscrQuality.Value);
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}

		private void cmdShear_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}

				ixProcessor1.DocumentShear((double)hscrShearAngle.Value,iShearPadCol,(PegasusImaging.WinForms.ImagXpress8.ShearType) cboShearType.SelectedIndex);
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}

		private void cmdErode_Click(object sender, System.EventArgs e)
		{
			
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}

				ixProcessor1.DocumentErode((short)hscrErodeAmount.Value, (PegasusImaging.WinForms.ImagXpress8.EnhancementDirection)cboErodeDirection.SelectedIndex);
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}
		private void cmdDilate_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}

				ixProcessor1.DocumentDilate((short)hscrDilateAmount.Value, (PegasusImaging.WinForms.ImagXpress8.EnhancementDirection)cboDilateDirection.SelectedIndex);
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}
		
		private void cmdDespeckle_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}

				ixProcessor1.DocumentDespeckle((short)hscrDespSpeckWidth.Value, (short)hscrDespSpeckHeight.Value);
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}

		private void cmdZoomSmooth_Click(object sender, System.EventArgs e)
		{
			try 
			{
				ixProcessor1.Image = imagX1.Copy();

				if ((imagX1.ImageXData.BitsPerPixel > 1)) 
				{
					MessageBox.Show(("Image is " 
						+ (imagX1.ImageXData.BitsPerPixel.ToString() + " BPP.  Converting image to 1 BPP.")));
					ixProcessor1.ColorDepth(1, PaletteType.Fixed, DitherType.BinarizePhotoHalftone);
				}

				ixProcessor1.DocumentZoomSmooth();
				imageXView2.Image = ixProcessor1.Image;
				imageXView2.ZoomToFit(ZoomToFitType.FitBest);
				// clear out the error in case there was an error from a previous operation
				lblError.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblError);
			}
		}

		private void hscrDespSpeckWidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblDespSpeckWVal.Text = hscrDespSpeckWidth.Value.ToString();
		}

		private void hscrDespSpeckHeight_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblDespSpeckHVal.Text = hscrDespSpeckHeight.Value.ToString();
		}

		private void hscrMinLength_Scroll_1(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblMinLengthVal.Text = hscrMinLength.Value.ToString();
		}

		private void hscrMaxThickness_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblMaxThicknessVal.Text = hscrMaxThickness.Value.ToString();
		}

		private void hscrMinAspRatio_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblMinAspRatioVal.Text = hscrMinAspRatio.Value.ToString();
		}

		private void hscrMaxGap_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblMaxGapVal.Text = hscrMaxGap.Value.ToString();
		}

		private void hscrlMaxCharRepSize_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblMaxCharRepSizeVal.Text = hscrlMaxCharRepSize.Value.ToString();
		}
		
		private void hscrMinAngle_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{

			dTemp = (hscrMinAngle.Value * 0.01);
			lblMinAngleVal.Text = dTemp.ToString();
			
		}

		private void hscrMinConfidence_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblMinConfidenceVal.Text = hscrMinConfidence.Value.ToString();
		}

		private void hscrQuality_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblQualityVal.Text = hscrQuality.Value.ToString();
		}

		private void hscrShearAngle_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblShearAngleVal.Text = hscrShearAngle.Value.ToString();
		}

		private void hscrErodeAmount_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblErodeAmountVal.Text = hscrErodeAmount.Value.ToString();
		}

		private void hscrDilateAmount_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblDilateAmountVal.Text = hscrDilateAmount.Value.ToString();
		}

		private void rbDeskewWhite_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((rbDeskewWhite.Checked == true)) 
			{
				cPadColor = Color.FromKnownColor(KnownColor.White);
				iDeskewPadCol = cPadColor.ToArgb();
			}
		}

		private void rbDeskewBlack_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((rbDeskewBlack.Checked == true)) 
			{
				cPadColor = Color.FromKnownColor(KnownColor.Black);
				iDeskewPadCol = cPadColor.ToArgb();
			}
		}

		private void rbShearWhite_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((rbShearWhite.Checked == true)) 
			{
				cPadColor = Color.FromKnownColor(KnownColor.White);
				iShearPadCol = cPadColor.ToArgb();
			}
		}

		private void rbShearBlack_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((rbShearBlack.Checked == true)) 
			{
				cPadColor = Color.FromKnownColor(KnownColor.Black);
				iShearPadCol = cPadColor.ToArgb();
			}
		}

		private void mnuFileOpen_Click(object sender, System.EventArgs e)
		{
			imgFileName = PegasusOpenFile();
			LoadFile();
		}

		private void mnuFileQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();

		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			if ((imageXView1.Toolbar.Activated == true)) 
			{
				mnuToolbarShow.Text = "Show";
				imageXView1.Toolbar.Activated = false;
				imageXView2.Toolbar.Activated = false;
			}
			else 
			{
				mnuToolbarShow.Text = "Hide";
				imageXView1.Toolbar.Activated = true;
				imageXView2.Toolbar.Activated = true;
			}
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			try 
			{
				imagXpress1.AboutBox();
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				PegasusError(eX, lblError);
			}
		}
	}
}

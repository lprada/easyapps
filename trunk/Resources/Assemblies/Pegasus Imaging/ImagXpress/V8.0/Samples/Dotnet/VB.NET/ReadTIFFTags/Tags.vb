
'****************************************************************
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/


Imports PegasusImaging.WinForms.ImagXpress8
Public Class Form1
    Inherits System.Windows.Forms.Form
    Private strImageFileName As System.String
    Private strCurrentImage As System.String
    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Private colTags As PegasusImaging.WinForms.ImagXpress8.ImageXTagCollection
    Private itag As PegasusImaging.WinForms.ImagXpress8.ImageXTag

    'File I/O Variables

    Dim strCurrentDir As System.String
    Dim strimageFile As System.String
    Dim sSaveFileName As System.String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        'Must call the UnlockControl function
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (Processor1 Is Nothing) Then
                Processor1.Dispose()
                Processor1 = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents Processor1 As PegasusImaging.WinForms.ImagXpress8.Processor
    Friend WithEvents cmdTags As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents lstTags As System.Windows.Forms.ListBox
    Friend WithEvents lstDesc As System.Windows.Forms.ListBox
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents lblLoadFileName As System.Windows.Forms.Label
    Friend WithEvents txtLoadFile As System.Windows.Forms.TextBox
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAddMulti As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents lblSaveFileName As System.Windows.Forms.Label
    Friend WithEvents txtSaveFile As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cmdTags = New System.Windows.Forms.Button()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.Processor1 = New PegasusImaging.WinForms.ImagXpress8.Processor()
        Me.lstTags = New System.Windows.Forms.ListBox()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuOpen = New System.Windows.Forms.MenuItem()
        Me.mnuQuit = New System.Windows.Forms.MenuItem()
        Me.mnuTool = New System.Windows.Forms.MenuItem()
        Me.mnuToolShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.lstDesc = New System.Windows.Forms.ListBox()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lblLoadFileName = New System.Windows.Forms.Label()
        Me.txtLoadFile = New System.Windows.Forms.TextBox()
        Me.cmdAdd = New System.Windows.Forms.Button()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdAddMulti = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.lblSaveFileName = New System.Windows.Forms.Label()
        Me.txtSaveFile = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cmdTags
        '
        Me.cmdTags.Location = New System.Drawing.Point(8, 72)
        Me.cmdTags.Name = "cmdTags"
        Me.cmdTags.Size = New System.Drawing.Size(208, 24)
        Me.cmdTags.TabIndex = 0
        Me.cmdTags.Text = "1)Load an Image and Show the Tags"
        '
        'ImageXView1
        '
        Me.ImageXView1.Location = New System.Drawing.Point(280, 80)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(296, 264)
        Me.ImageXView1.TabIndex = 1
        '
        'Processor1
        '
        Me.Processor1.BackgroundColor = System.Drawing.Color.Black
        Me.Processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast
        Me.Processor1.ProgressPercent = 10
        Me.Processor1.Redeyes = Nothing
        '
        'lstTags
        '
        Me.lstTags.Location = New System.Drawing.Point(8, 432)
        Me.lstTags.Name = "lstTags"
        Me.lstTags.Size = New System.Drawing.Size(440, 134)
        Me.lstTags.TabIndex = 2
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.mnuTool, Me.mnuAbout})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.mnuQuit})
        Me.MenuItem1.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Text = "&Open"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 1
        Me.mnuQuit.Text = "&Quit"
        '
        'mnuTool
        '
        Me.mnuTool.Index = 1
        Me.mnuTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolShow})
        Me.mnuTool.Text = "&ToolBar"
        '
        'mnuToolShow
        '
        Me.mnuToolShow.Index = 0
        Me.mnuToolShow.Text = "Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'lstDesc
        '
        Me.lstDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDesc.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Loading an image and viewing all the TIFF tags via the ImageXTagCollection Clas" & _
        "s.", "2)Adding and saving tags in a TIFF image."})
        Me.lstDesc.Location = New System.Drawing.Point(8, 8)
        Me.lstDesc.Name = "lstDesc"
        Me.lstDesc.Size = New System.Drawing.Size(760, 56)
        Me.lstDesc.TabIndex = 3
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(592, 112)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(224, 82)
        Me.lstStatus.TabIndex = 40
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(592, 72)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(176, 24)
        Me.lblLoadStatus.TabIndex = 39
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblerror
        '
        Me.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblerror.Location = New System.Drawing.Point(600, 307)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(200, 96)
        Me.lblerror.TabIndex = 38
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(600, 267)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(184, 24)
        Me.lblLastError.TabIndex = 37
        Me.lblLastError.Text = "Last Error:"
        '
        'lblLoadFileName
        '
        Me.lblLoadFileName.Location = New System.Drawing.Point(16, 112)
        Me.lblLoadFileName.Name = "lblLoadFileName"
        Me.lblLoadFileName.Size = New System.Drawing.Size(112, 32)
        Me.lblLoadFileName.TabIndex = 41
        Me.lblLoadFileName.Text = "Load File Name:"
        '
        'txtLoadFile
        '
        Me.txtLoadFile.Enabled = False
        Me.txtLoadFile.Location = New System.Drawing.Point(16, 152)
        Me.txtLoadFile.Multiline = True
        Me.txtLoadFile.Name = "txtLoadFile"
        Me.txtLoadFile.Size = New System.Drawing.Size(248, 80)
        Me.txtLoadFile.TabIndex = 42
        Me.txtLoadFile.Text = ""
        '
        'cmdAdd
        '
        Me.cmdAdd.Location = New System.Drawing.Point(8, 248)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(264, 24)
        Me.cmdAdd.TabIndex = 43
        Me.cmdAdd.Text = "2) Add Some Tiff Tags  (Tags 101 - 112)"
        '
        'cmdDelete
        '
        Me.cmdDelete.Location = New System.Drawing.Point(8, 272)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(264, 24)
        Me.cmdDelete.TabIndex = 44
        Me.cmdDelete.Text = "3) Delete and ReAdd  (Tags 101 - 112)"
        '
        'cmdAddMulti
        '
        Me.cmdAddMulti.Location = New System.Drawing.Point(8, 296)
        Me.cmdAddMulti.Name = "cmdAddMulti"
        Me.cmdAddMulti.Size = New System.Drawing.Size(264, 24)
        Me.cmdAddMulti.TabIndex = 45
        Me.cmdAddMulti.Text = "4) Add Tags with multiple Data (Tags 201 - 212)"
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(8, 320)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(264, 24)
        Me.cmdSave.TabIndex = 46
        Me.cmdSave.Text = "5) Save the file and reload it with Modified Tags"
        '
        'lblSaveFileName
        '
        Me.lblSaveFileName.Location = New System.Drawing.Point(16, 352)
        Me.lblSaveFileName.Name = "lblSaveFileName"
        Me.lblSaveFileName.Size = New System.Drawing.Size(168, 24)
        Me.lblSaveFileName.TabIndex = 47
        Me.lblSaveFileName.Text = "Save File Name:"
        '
        'txtSaveFile
        '
        Me.txtSaveFile.Location = New System.Drawing.Point(8, 384)
        Me.txtSaveFile.Multiline = True
        Me.txtSaveFile.Name = "txtSaveFile"
        Me.txtSaveFile.Size = New System.Drawing.Size(440, 40)
        Me.txtSaveFile.TabIndex = 48
        Me.txtSaveFile.Text = ""
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(824, 585)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.txtSaveFile, Me.lblSaveFileName, Me.cmdSave, Me.cmdAddMulti, Me.cmdDelete, Me.cmdAdd, Me.txtLoadFile, Me.lblLoadFileName, Me.lstStatus, Me.lblLoadStatus, Me.lblerror, Me.lblLastError, Me.lstDesc, Me.lstTags, Me.ImageXView1, Me.cmdTags})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ImagXpress 8 Tags"
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


#End Region


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTags.Click

        Try


            'pass in the file that contains the TIFFTags
            colTags = PegasusImaging.WinForms.ImagXpress8.ImageX.GetTags(strimageFile, 1)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""

            LoadFile()

            ImageXView1.Image = imagX1

            lstTags.Items.Clear()

            PopulateTIFFTagListBox(colTags)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Public Sub PopulateTIFFTagListBox(ByVal ixTagColl As PegasusImaging.WinForms.ImagXpress8.ImageXTagCollection)

        Dim ret As String

        lstTags.Items.Clear()

        lstTags.Items.Add("Tag" & Chr(9) & "Type" & Chr(9) & "Count" & Chr(9) & "Data")

        'iterate through the tags collection
        For Each itag In ixTagColl
            ret = GetTagString(itag)
            'Add the returned items here 
            lstTags.Items.Add(itag.TagNumber & Chr(9) & itag.TagType & Chr(9) & itag.TagElementsCount & Chr(9) & ret)
        Next

    End Sub
    Public Function GetTagString(ByVal itag As PegasusImaging.WinForms.ImagXpress8.ImageXTag) As String

        Dim counter As Integer

        Dim sTagInfo As String

        Dim tagcounter As Integer
        Dim tagstring As String

        Select Case (itag.TagType)
            'Ascii Tag data
        Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Ascii
                Dim outdata As String

                For tagcounter = 0 To itag.TagElementsCount - 1
                    outdata = outdata & Convert.ToChar((itag.GetTagBytes(tagcounter)))
                Next

                tagstring = outdata
                Return tagstring
                'Byte Tag data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Byte
                Dim outdata As String
                'outdata = "Byte:"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagBytes(tagcounter)
                Next
                tagstring = outdata
                Return tagstring
                'Double Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Double
                Dim outdata As String
                'outdata = "Double:"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagDouble(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring
                'Float Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Float
                Dim outdata As String
                'outdata = "Float:"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagFloat(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring
                'Long Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Long
                Dim outdata As String
                'tagstring = "Long"

                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagUInt32(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring
                'Rational Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Rational

                Dim outdata As String
                'tagstring = "Rational"
                For tagcounter = 0 To itag.TagElementsCount / 2 - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagRational(tagcounter * 2).ToString() & "/" & itag.GetTagRational((tagcounter * 2) + 1).ToString
                Next
                tagstring = outdata
                Return tagstring

                'Sbyte Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Sbyte
                Dim outdata As String
                'outdata = "Sbyte"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagSBytes(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring

                'Short Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Short
                Dim outdata As String
                'outdata = "Short"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagUInt16(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring
                'Slong Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Slong
                Dim outdata As String
                'outdata = "SLong"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagInt32(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring
                'SRational Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Srational
                Dim outdata As String
                'outdata = "SRational"
                For tagcounter = 0 To itag.TagElementsCount / 2 - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagSRational(tagcounter * 2).ToString() & "/" & itag.GetTagSRational((tagcounter * 2) + 1).ToString
                Next
                tagstring = outdata
                Return tagstring
                'Short Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Sshort
                Dim outdata As String
                'outdata = "SShort"
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata & itag.GetTagInt16(tagcounter).ToString()
                Next
                tagstring = outdata
                Return tagstring
                'Undefined Tag Data
            Case PegasusImaging.WinForms.ImagXpress8.TagTypes.Undefine
                Dim outdata As String
                For tagcounter = 0 To itag.TagElementsCount - 1
                    If tagcounter > 0 Then
                        outdata = outdata & ", "
                    End If
                    outdata = outdata + itag.GetTagBytes(tagcounter).ToString()
                Next

                tagstring = outdata
                Return tagstring
        End Select


    End Function


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '***Must call UnlockRuntime to Distribute Application*** 
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)

        Try
            'here we set the current directory and image so that the file open dialog box works well
            strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString()
            strimageFile = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\benefits.tif")
            sSaveFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\vbDotNetTagsOutput.tif")
            strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")

            txtLoadFile.Text = strimageFile
            txtSaveFile.Text = sSaveFileName

            '***** Event handlers
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent


        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub


    Private Sub mnuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then

            Try
                strimageFile = strtemp

                lblerror.Text = ""

                LoadFile()

            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub

    Private Sub LoadFile()
        Try

            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)

            txtLoadFile.Text = strimageFile


        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub mnuToolShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolShow.Click
        If ImageXView1.Toolbar.Activated = True Then
            mnuToolShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False
        Else
            mnuToolShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True

        End If
    End Sub

    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub


    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        Try

            ' We assemble and add some bogus TIFF tags
            ' This demonstrates adding each type of tag

            ' Byte - TIFF Type 1
            Dim byteTag(0) As Byte
            byteTag(0) = 1
            Dim my101Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my101Tag.TagNumber = 101
            my101Tag.TagType = TagTypes.Byte
            my101Tag.TagLevel = 0
            my101Tag.SetTagBytes(byteTag)
            colTags.Add(my101Tag)

            ' Ascii - TIFF Type 2
            Dim asciiTag As Byte()
            Dim encoding As New System.Text.ASCIIEncoding()
            asciiTag = encoding.GetBytes("Tag 2")
            Dim my102Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my102Tag.SetTagBytes(asciiTag)
            my102Tag.TagNumber = 102
            my102Tag.TagType = TagTypes.Ascii
            my102Tag.TagLevel = 0
            colTags.Add(my102Tag)

            ' Short - TIFF Type 3
            Dim shortTag(0) As Integer
            shortTag(0) = 3
            Dim my103Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my103Tag.TagNumber = 103
            my103Tag.TagType = TagTypes.Short
            my103Tag.TagLevel = 0
            my103Tag.SetTagUInt16(shortTag)
            colTags.Add(my103Tag)

            ' Long - TIFF Type 4
            Dim longTag(0) As Long
            longTag(0) = 4
            Dim my104Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my104Tag.TagNumber = 104
            my104Tag.TagType = TagTypes.Long
            my104Tag.TagLevel = 0
            my104Tag.SetTagUInt32(longTag)
            colTags.Add(my104Tag)

            ' Rational - TIFF Type 5
            Dim rationalTag(1) As Long ' Holder for rational data
            rationalTag(0) = 600 ' Numerator
            rationalTag(1) = 2 ' Denominator
            Dim my105Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my105Tag.TagNumber = 105
            my105Tag.TagType = TagTypes.Rational
            my105Tag.TagLevel = 0
            my105Tag.SetTagRational(rationalTag)
            colTags.Add(my105Tag)

            ' SByte - TIFF Type 6
            Dim sbyteTag(0) As Short
            sbyteTag(0) = -6
            Dim my106Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my106Tag.TagNumber = 106
            my106Tag.TagType = TagTypes.Sbyte
            my106Tag.TagLevel = 0
            my106Tag.SetTagSBytes(sbyteTag)
            colTags.Add(my106Tag)

            ' Undefine - TIFF Type 7
            Dim undefineTag(0) As Byte
            undefineTag(0) = 7
            Dim my107Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my107Tag.SetTagBytes(undefineTag)
            my107Tag.TagNumber = 107
            my107Tag.TagType = TagTypes.Undefine
            my107Tag.TagLevel = 0
            colTags.Add(my107Tag)

            ' SShort - TIFF Type 8
            Dim sshortTag(0) As Short
            sshortTag(0) = -8
            Dim my108Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my108Tag.TagNumber = 108
            my108Tag.TagType = TagTypes.Sshort
            my108Tag.TagLevel = 0
            my108Tag.SetTagInt16(sshortTag)
            colTags.Add(my108Tag)

            ' SLong - TIFF Type 9
            Dim slongTag(0) As Integer
            slongTag(0) = -9
            Dim my109Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my109Tag.TagNumber = 109
            my109Tag.TagType = TagTypes.Slong
            my109Tag.TagLevel = 0
            my109Tag.SetTagInt32(slongTag)
            colTags.Add(my109Tag)

            ' SRational - TIFF Type 10
            Dim srationalTag(1) As Integer ' Holder for srational data
            srationalTag(0) = -600 ' Numerator
            srationalTag(1) = 2 ' Denominator
            Dim my110Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my110Tag.TagNumber = 110
            my110Tag.TagType = TagTypes.Srational
            my110Tag.TagLevel = 0
            my110Tag.SetTagSRational(srationalTag)
            colTags.Add(my110Tag)

            ' Float - TIFF Type 11
            Dim floatTag(0) As Single
            floatTag(0) = 11.65
            Dim my111Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my111Tag.TagNumber = 111
            my111Tag.TagType = TagTypes.Float
            my111Tag.TagLevel = 0
            my111Tag.SetTagFloat(floatTag)
            colTags.Add(my111Tag)

            ' Double - TIFF Type 12
            Dim doubleTag(0) As Double
            doubleTag(0) = 12.42
            Dim my112Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my112Tag.TagNumber = 112
            my112Tag.TagType = TagTypes.Double
            my112Tag.TagLevel = 0
            my112Tag.SetTagDouble(doubleTag)
            colTags.Add(my112Tag)

            PopulateTIFFTagListBox(colTags)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click

        Try

            ' Delete our bogus tags and readd them to test
            ' Retrieve, Add, and Delete for each tag type

            Dim i As Integer
            For i = 101 To 112
                Dim myTag As PegasusImaging.WinForms.ImagXpress8.ImageXTag
                myTag = colTags.GetTag(i, 0)
                colTags.RemoveTag(i, 0)
                colTags.Add(myTag)

            Next

            PopulateTIFFTagListBox(colTags)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub cmdAddMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddMulti.Click

        Try

            ' Byte - TIFF Type 1
            Dim byteTag(2) As Byte
            byteTag(0) = 1
            byteTag(1) = 2
            byteTag(2) = 3
            Dim my201Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my201Tag.TagNumber = 201
            my201Tag.TagType = TagTypes.Byte
            my201Tag.TagLevel = 0
            my201Tag.SetTagBytes(byteTag)
            colTags.Add(my201Tag)

            ' Ascii - TIFF Type 2
            Dim asciiTag As Byte()
            Dim encoding As New System.Text.ASCIIEncoding()
            asciiTag = encoding.GetBytes("Tag 2")
            Dim my202Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my202Tag.SetTagBytes(asciiTag)
            my202Tag.TagNumber = 202
            my202Tag.TagType = TagTypes.Ascii
            my202Tag.TagLevel = 0
            colTags.Add(my202Tag)

            ' Short - TIFF Type 3
            Dim shortTag(2) As Integer
            shortTag(0) = 3
            shortTag(1) = 4
            shortTag(2) = 5
            Dim my203Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my203Tag.TagNumber = 203
            my203Tag.TagType = TagTypes.Short
            my203Tag.TagLevel = 0
            my203Tag.SetTagUInt16(shortTag)
            colTags.Add(my203Tag)

            ' Long - TIFF Type 4
            Dim longTag(2) As Long
            longTag(0) = 4
            longTag(1) = 5
            longTag(2) = 6
            Dim my204Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my204Tag.TagNumber = 204
            my204Tag.TagType = TagTypes.Long
            my204Tag.TagLevel = 0
            my204Tag.SetTagUInt32(longTag)
            colTags.Add(my204Tag)

            ' Rational - TIFF Type 5
            Dim rationalTag(5) As Long ' Holder for rational data
            rationalTag(0) = 600 ' Numerator
            rationalTag(1) = 2 ' Denominator
            rationalTag(2) = 720 ' Numerator
            rationalTag(3) = 10 ' Denominator
            rationalTag(4) = 300 ' Numerator
            rationalTag(5) = 1 ' Denominator
            Dim my205Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my205Tag.TagNumber = 205
            my205Tag.TagType = TagTypes.Rational
            my205Tag.TagLevel = 0
            my205Tag.SetTagRational(rationalTag)
            colTags.Add(my205Tag)

            ' SByte - TIFF Type 6
            Dim sbyteTag(2) As Short
            sbyteTag(0) = -6
            sbyteTag(1) = -7
            sbyteTag(2) = -8
            Dim my206Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my206Tag.TagNumber = 206
            my206Tag.TagType = TagTypes.Sbyte
            my206Tag.TagLevel = 0
            my206Tag.SetTagSBytes(sbyteTag)
            colTags.Add(my206Tag)

            ' Undefine - TIFF Type 7
            Dim undefineTag(2) As Byte
            undefineTag(0) = 7
            undefineTag(1) = 8
            undefineTag(2) = 9
            Dim my207Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my207Tag.SetTagBytes(undefineTag)
            my207Tag.TagNumber = 207
            my207Tag.TagType = TagTypes.Undefine
            my207Tag.TagLevel = 0
            colTags.Add(my207Tag)

            ' SShort - TIFF Type 8
            Dim sshortTag(2) As Short
            sshortTag(0) = -8
            sshortTag(1) = -9
            sshortTag(2) = -10
            Dim my208Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my208Tag.TagNumber = 208
            my208Tag.TagType = TagTypes.Sshort
            my208Tag.TagLevel = 0
            my208Tag.SetTagInt16(sshortTag)
            colTags.Add(my208Tag)

            ' SLong - TIFF Type 9
            Dim slongTag(2) As Integer
            slongTag(0) = -9
            slongTag(1) = -10
            slongTag(2) = -11
            Dim my209Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my209Tag.TagNumber = 209
            my209Tag.TagType = TagTypes.Slong
            my209Tag.TagLevel = 0
            my209Tag.SetTagInt32(slongTag)
            colTags.Add(my209Tag)

            ' SRational - TIFF Type 10
            Dim srationalTag(5) As Integer ' Holder for srational data
            srationalTag(0) = -600 ' Numerator
            srationalTag(1) = 2 ' Denominator
            srationalTag(2) = -720 ' Numerator
            srationalTag(3) = 10 ' Denominator
            srationalTag(4) = -300 ' Numerator
            srationalTag(5) = 1 ' Denominator
            Dim my210Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my210Tag.TagNumber = 210
            my210Tag.TagType = TagTypes.Srational
            my210Tag.TagLevel = 0
            my210Tag.SetTagSRational(srationalTag)
            colTags.Add(my210Tag)

            ' Float - TIFF Type 11
            Dim floatTag(2) As Single
            floatTag(0) = 11.65
            floatTag(1) = -12.65
            floatTag(2) = -13.65
            Dim my211Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my211Tag.TagNumber = 211
            my211Tag.TagType = TagTypes.Float
            my211Tag.TagLevel = 0
            my211Tag.SetTagFloat(floatTag)
            colTags.Add(my211Tag)

            ' Double - TIFF Type 12
            Dim doubleTag(2) As Double
            doubleTag(0) = 12.42
            doubleTag(1) = -13.42
            doubleTag(2) = -14.42
            Dim my212Tag As New PegasusImaging.WinForms.ImagXpress8.ImageXTag()
            my212Tag.TagNumber = 212
            my212Tag.TagType = TagTypes.Double
            my212Tag.TagLevel = 0
            my212Tag.SetTagDouble(doubleTag)
            colTags.Add(my212Tag)

            PopulateTIFFTagListBox(colTags)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim so As PegasusImaging.WinForms.ImagXpress8.SaveOptions
        Try

            ImageXView1.Image.Tags = colTags
            so = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
            so.Format = ImageXFormat.Tiff
            so.Tiff.Compression = Compression.NoCompression
            ImageXView1.Image.Save(txtSaveFile.Text, so)

            Application.DoEvents()
            'pass in the file that contains the TIFFTags
            colTags = PegasusImaging.WinForms.ImagXpress8.ImageX.GetTags(txtSaveFile.Text, 1)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""

            LoadFile()

            ImageXView1.Image = imagX1

            lstTags.Items.Clear()

            PopulateTIFFTagListBox(colTags)


        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.IO.FileLoadException

        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If

    End Sub


    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

    End Sub
End Class

Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("CommanLineApp")> 
<Assembly: AssemblyDescription("CommanLineApp VBNet Sample")> 
<Assembly: AssemblyCompany("Pegasus Imaging Corporation")> 
<Assembly: AssemblyProduct("ImagXpress8")> 
<Assembly: AssemblyCopyright("Copyright � 2006. All rights reserved.")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 
<Assembly: SecurityPermission(SecurityAction.RequestMinimum)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("3059C06E-1ECF-4cbe-B214-CEF67EBC6DCA")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 

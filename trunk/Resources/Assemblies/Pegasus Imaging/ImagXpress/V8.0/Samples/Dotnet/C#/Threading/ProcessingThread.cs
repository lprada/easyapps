/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress8;

namespace Threading
{
	/// <summary>
	/// Summary description for ProcessingThread
	/// </summary>
	public class ProcessorThread
	{
		//changing any of the ReferenceToMy stuff is not allowed. In order to properly manipulate
		private System.Windows.Forms.Label myReferenceToMyLabel; 
		private PegasusImaging.WinForms.ImagXpress8.ImageXView myReferenceToMyViewer; 
		//these, you need to use Invoke. The reason is that these objects are owned by a different thread

		private System.String myImageFileName;
		private System.Int32 myResize;
		private PegasusImaging.WinForms.ImagXpress8.ImageX myImage;
		private PegasusImaging.WinForms.ImagXpress8.Processor myProcessor;

		static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException e) 
		{
			MessageBox.Show(null,e.Message,"ImageXPress Error " + e.Number.ToString());
		}

		public ProcessorThread (System.String daImageFileName,
			PegasusImaging.WinForms.ImagXpress8.ImageXView daViewer,
			System.Windows.Forms.Label daLabel,
			System.Int32 daSize)
		{			
			//set our references to these objects
			this.myReferenceToMyViewer = daViewer;
			this.myReferenceToMyLabel = daLabel;

			//since we do not need to pass back the filename, we can clone it to eliminate cross thread fears
			this.myImageFileName = (string)daImageFileName.Clone(); 
			
			//types such as System.Int32 are not real objects, so no cross thread fears.
			this.myResize = daSize;
		}

		private delegate void SafeContextImageChanger(PegasusImaging.WinForms.ImagXpress8.ImageX newImage);
		private delegate void SafeContextLabelChanger(string newText);
		private delegate void SafeContextZoomChanger(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType newZoomFitType);

		private void RealZoomToFitChangerInTheSafeContext(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType newZoomFitType)
		{
			myReferenceToMyViewer.ZoomToFit(newZoomFitType);
		}

		private void RealImageChangerInTheSafeContext(PegasusImaging.WinForms.ImagXpress8.ImageX newImage)
		{
			PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = myReferenceToMyViewer.Image;
	
			myReferenceToMyViewer.Image = newImage;

			if (oldImage != null)
			{
				oldImage.Dispose();
				oldImage = null;
			}
			GC.Collect();
			System.Threading.Thread.Sleep(25);
		}
		private void RealLabelChangerInTheSafeContext(string newText)
		{
			myReferenceToMyLabel.Text = newText;
		}

		private void ChangeLabelText(string newText)
		{
			object[] theArgs = {newText};
			myReferenceToMyLabel.Invoke(new SafeContextLabelChanger(this.RealLabelChangerInTheSafeContext),theArgs);
		}
		private void ChangeImage(PegasusImaging.WinForms.ImagXpress8.ImageX newImage)
		{
			object[] theArgs = {newImage.Copy()};
			myReferenceToMyViewer.Invoke(new SafeContextImageChanger(this.RealImageChangerInTheSafeContext),theArgs);
		}
		private void ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType newZoomFitType)
		{
			object[] theArgs = {newZoomFitType};
			myReferenceToMyViewer.Invoke(new SafeContextZoomChanger(this.RealZoomToFitChangerInTheSafeContext),theArgs);
		}

		public void Process()
		{	
			bool imageOpened = false;
			int TryCount = 0;
			while((!imageOpened) && (TryCount < 25))
			{
				try 
				{
					//Create the image object and open the image
					myImage = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(myImageFileName);
					imageOpened = true;
				} 
				catch (ImagXpressException ex) 
				{
					ChangeLabelText("Error opening image - " + ex.Number.ToString() + ". Retrying...");
				}
				TryCount++;
			}

			if (TryCount >= 25)
			{
				throw new Exception("Unable to open the image for processing.");
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try
			{
				//Create the processor object and tie it to the image we just opened
				myProcessor = new PegasusImaging.WinForms.ImagXpress8.Processor(myImage);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try 
			{
				//Set the viewer's image object equal to the one we are operating on...
				//we have to do this each time the image changes because we are making
				//copies of the image each time
				ChangeImage(myImage);

				//Set the viewing zoom type
				ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("Processing...");

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("Resizing...");

			try
			{
				//Resize the image
				myProcessor.Resize(new System.Drawing.Size(myImage.ImageXData.Width * (myResize), myImage.ImageXData.Height * myResize), PegasusImaging.WinForms.ImagXpress8.ResizeType.Quality);
			}
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try 
			{
				//Set the viewer's image object equal to the one we are operating on...
				//we have to do this each time the image changes because we are making
				//copies of the image each time
				ChangeImage(myImage);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("Blurring...");

			try
			{
				//Resize the image
				myProcessor.Blur();
			}
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try 
			{
				//Set the viewer's image object equal to the one we are operating on...
				//we have to do this each time the image changes because we are making
				//copies of the image each time
				ChangeImage(myImage);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("Negating...");

			try
			{
				//Resize the image
				myProcessor.Negate();
			}
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try 
			{
				//Set the viewer's image object equal to the one we are operating on...
				//we have to do this each time the image changes because we are making
				//copies of the image each time
				ChangeImage(myImage);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("Zooming...");

			try 
			{
				//Set the viewing zoom type
				ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitWidth);
			}
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try 
			{
				//Set the viewer's image object equal to the one we are operating on...
				//we have to do this each time the image changes because we are making
				//copies of the image each time
				ChangeImage(myImage);

				//Set the viewing zoom type
				ChangeZoomToFitType(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("(Un)Negating...");

			try
			{
				//Resize the image
				myProcessor.Negate();
			}
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			try 
			{
				//Set the viewer's image object equal to the one we are operating on...
				//we have to do this each time the image changes because we are making
				//copies of the image each time
				ChangeImage(myImage);
			} 
			catch (ImagXpressException ex) 
			{
				PegasusError(ex);
			}

			//Rest
			System.Threading.Thread.Sleep(10);

			//Change the viewer's label.
			ChangeLabelText("Processed.");
          
			//Rest
			System.Threading.Thread.Sleep(10);

			//kill the processor
			myProcessor.Dispose();
			myProcessor = null;

			//kill the local copy of the image
			myImage.Dispose();
			myImage = null;

			GC.Collect();
			System.Threading.Thread.Sleep(100);
		}
	}
}

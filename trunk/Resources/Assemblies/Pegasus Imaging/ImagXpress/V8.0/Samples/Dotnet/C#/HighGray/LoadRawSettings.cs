/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace HighGray
{
	/// <summary>
	/// Summary description for LoadRawSettings.
	/// </summary>
	public class LoadRawSettings : System.Windows.Forms.Form
	{
		private static readonly string[]	pixelFormats = new string[] {"Dib (0x00000001) - Device independent bitmap. ",
																			"Raw (0x00000002) - AW image format. ",
                                                                            "BigEndian (0x00000004) - Motorola byte order format. ",
																			"TopDown (0x00000008) - Top down bitmap format. ",
																			"Signed (0x00000010) - Signed byte order format. ",
																			"Gray (0x00000020) - Grey scale image. ",
																			"HighGray (0x00000040) - DICOM Medical standard grey scale image format. ",
																			"Alpha (0x00000080) - Alpha channel. ",
																			"Rgb (0x00000100) - Red, Green, Blue byte order format. ",
																			"Bgr (0x00000200) - Blue, Green, Red byte order format. ",
																			"Rgb555 (0x00000400) - RGB555 byte order format. ",
																			"Rgb565 (0x00000800) - RGB565 byte order format. ",
																			"Argb (0x00001000) - Alpha, red, green, blue byte order format. ",
																			"Rgba (0x00002000) - Red, green, blue, alpha byte order format. ",
																			"Abgr (0x00004000) - Alpha, blue, green, red byte order format. ",
																			"Bgra (0x00008000) - Blue, green, red, alpha byte order format. ",
																			"ColorMapped (0x00010000) - 8 bit colormapped format. ",
																			"Cmyk (0x00020000) - Cyan, yellow, magenta, black color format. ",
																			"Compressed (0x00100000) - Compressed image format. ",
																			"NonInterleaved (0x00200000) - Noninterleaved image format. ",
																			"NoDibPad (0x00400000) - DIB without padding. "};
		private static readonly int[]	pixelFormatNums = new int[] {0x00000001,
																		0x00000002,
																		0x00000004,
																		0x00000008,
																		0x00000010,
																		0x00000020,
																		0x00000040,
																		0x00000080,
																		0x00000100,
																		0x00000200,
																		0x00000400,
																		0x00000800,
																		0x00001000,
																		0x00002000,
																		0x00004000,
																		0x00008000,
																		0x00010000,
																		0x00020000,
																		0x00100000,
																		0x00200000,
																		0x00400000};
		private System.String				myFileName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxBitsPer;
		private System.Windows.Forms.TextBox textBoxBytesPer;
		private System.Windows.Forms.TextBox textBoxWidth;
		private System.Windows.Forms.TextBox textBoxHeight;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBoxHighBit;
		private System.Windows.Forms.TextBox textBoxStride;
		private System.Windows.Forms.ListBox listBoxPixelFormatSelected;
		private System.Windows.Forms.ListBox listBoxPixelFormatAvailable;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button buttonToUsing;
		private System.Windows.Forms.Button buttonToAvailable;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox textBoxOffset;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public LoadRawSettings(System.String daFileName)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myFileName = System.IO.Path.GetFileName(daFileName);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.textBoxBitsPer = new System.Windows.Forms.TextBox();
			this.textBoxBytesPer = new System.Windows.Forms.TextBox();
			this.textBoxWidth = new System.Windows.Forms.TextBox();
			this.textBoxHeight = new System.Windows.Forms.TextBox();
			this.textBoxHighBit = new System.Windows.Forms.TextBox();
			this.textBoxStride = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.listBoxPixelFormatSelected = new System.Windows.Forms.ListBox();
			this.listBoxPixelFormatAvailable = new System.Windows.Forms.ListBox();
			this.label8 = new System.Windows.Forms.Label();
			this.buttonToUsing = new System.Windows.Forms.Button();
			this.buttonToAvailable = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.textBoxOffset = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "Bits Per Pixel:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 32);
			this.label2.TabIndex = 2;
			this.label2.Text = "Bytes Per Pixel:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 72);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 24);
			this.label3.TabIndex = 3;
			this.label3.Text = "Width:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(40, 24);
			this.label4.TabIndex = 4;
			this.label4.Text = "Height:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 128);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 24);
			this.label5.TabIndex = 5;
			this.label5.Text = "High Bit Index:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 224);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(144, 16);
			this.label6.TabIndex = 6;
			this.label6.Text = "Pixel Format(s):";
			this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 200);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(40, 24);
			this.label7.TabIndex = 7;
			this.label7.Text = "Stride:";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxBitsPer
			// 
			this.textBoxBitsPer.Location = new System.Drawing.Point(72, 8);
			this.textBoxBitsPer.Name = "textBoxBitsPer";
			this.textBoxBitsPer.Size = new System.Drawing.Size(120, 20);
			this.textBoxBitsPer.TabIndex = 8;
			this.textBoxBitsPer.Text = "";
			// 
			// textBoxBytesPer
			// 
			this.textBoxBytesPer.Location = new System.Drawing.Point(72, 40);
			this.textBoxBytesPer.Name = "textBoxBytesPer";
			this.textBoxBytesPer.Size = new System.Drawing.Size(120, 20);
			this.textBoxBytesPer.TabIndex = 9;
			this.textBoxBytesPer.Text = "";
			// 
			// textBoxWidth
			// 
			this.textBoxWidth.Location = new System.Drawing.Point(72, 72);
			this.textBoxWidth.Name = "textBoxWidth";
			this.textBoxWidth.Size = new System.Drawing.Size(120, 20);
			this.textBoxWidth.TabIndex = 10;
			this.textBoxWidth.Text = "";
			// 
			// textBoxHeight
			// 
			this.textBoxHeight.Location = new System.Drawing.Point(72, 96);
			this.textBoxHeight.Name = "textBoxHeight";
			this.textBoxHeight.Size = new System.Drawing.Size(120, 20);
			this.textBoxHeight.TabIndex = 11;
			this.textBoxHeight.Text = "";
			// 
			// textBoxHighBit
			// 
			this.textBoxHighBit.Location = new System.Drawing.Point(72, 128);
			this.textBoxHighBit.Name = "textBoxHighBit";
			this.textBoxHighBit.Size = new System.Drawing.Size(120, 20);
			this.textBoxHighBit.TabIndex = 12;
			this.textBoxHighBit.Text = "";
			// 
			// textBoxStride
			// 
			this.textBoxStride.Location = new System.Drawing.Point(72, 200);
			this.textBoxStride.Name = "textBoxStride";
			this.textBoxStride.Size = new System.Drawing.Size(120, 20);
			this.textBoxStride.TabIndex = 13;
			this.textBoxStride.Text = "";
			// 
			// button1
			// 
			this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button1.Location = new System.Drawing.Point(216, 384);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(96, 24);
			this.button1.TabIndex = 15;
			this.button1.Text = "Cancel";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.button2.Location = new System.Drawing.Point(32, 384);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(96, 24);
			this.button2.TabIndex = 16;
			this.button2.Text = "OK";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// listBoxPixelFormatSelected
			// 
			this.listBoxPixelFormatSelected.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.listBoxPixelFormatSelected.HorizontalScrollbar = true;
			this.listBoxPixelFormatSelected.Location = new System.Drawing.Point(8, 248);
			this.listBoxPixelFormatSelected.Name = "listBoxPixelFormatSelected";
			this.listBoxPixelFormatSelected.Size = new System.Drawing.Size(144, 121);
			this.listBoxPixelFormatSelected.TabIndex = 17;
			// 
			// listBoxPixelFormatAvailable
			// 
			this.listBoxPixelFormatAvailable.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.listBoxPixelFormatAvailable.HorizontalScrollbar = true;
			this.listBoxPixelFormatAvailable.Items.AddRange(new object[] {
																			 "Dib (0x00000001) - Device independent bitmap. ",
																			 "Raw (0x00000002) - AW image format. ",
																			 "BigEndian (0x00000004) - Motorola byte order format. ",
																			 "TopDown (0x00000008) - Top down bitmap format. ",
																			 "Signed (0x00000010) - Signed byte order format. ",
																			 "Gray (0x00000020) - Grey scale image. ",
																			 "HighGray (0x00000040) - DICOM Medical standard grey scale image format. ",
																			 "Alpha (0x00000080) - Alpha channel. ",
																			 "Rgb (0x00000100) - Red, Green, Blue byte order format. ",
																			 "Bgr (0x00000200) - Blue, Green, Red byte order format. ",
																			 "Rgb555 (0x00000400) - RGB555 byte order format. ",
																			 "Rgb565 (0x00000800) - RGB565 byte order format. ",
																			 "Argb (0x00001000) - Alpha, red, green, blue byte order format. ",
																			 "Rgba (0x00002000) - Red, green, blue, alpha byte order format. ",
																			 "Abgr (0x00004000) - Alpha, blue, green, red byte order format. ",
																			 "Bgra (0x00008000) - Blue, green, red, alpha byte order format. ",
																			 "ColorMapped (0x00010000) - 8 bit colormapped format. ",
																			 "Cmyk (0x00020000) - Cyan, yellow, magenta, black color format. ",
																			 "Compressed (0x00100000) - Compressed image format. ",
																			 "NonInterleaved (0x00200000) - Noninterleaved image format. ",
																			 "NoDibPad "});
			this.listBoxPixelFormatAvailable.Location = new System.Drawing.Point(192, 257);
			this.listBoxPixelFormatAvailable.Name = "listBoxPixelFormatAvailable";
			this.listBoxPixelFormatAvailable.Size = new System.Drawing.Size(256, 108);
			this.listBoxPixelFormatAvailable.TabIndex = 18;
			// 
			// label8
			// 
			this.label8.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.label8.Location = new System.Drawing.Point(208, 224);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(128, 16);
			this.label8.TabIndex = 19;
			this.label8.Text = "Available Pixel Formats:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// buttonToUsing
			// 
			this.buttonToUsing.Location = new System.Drawing.Point(164, 264);
			this.buttonToUsing.Name = "buttonToUsing";
			this.buttonToUsing.Size = new System.Drawing.Size(24, 32);
			this.buttonToUsing.TabIndex = 20;
			this.buttonToUsing.Text = "<";
			this.buttonToUsing.Click += new System.EventHandler(this.buttonToUsing_Click);
			// 
			// buttonToAvailable
			// 
			this.buttonToAvailable.Location = new System.Drawing.Point(164, 312);
			this.buttonToAvailable.Name = "buttonToAvailable";
			this.buttonToAvailable.Size = new System.Drawing.Size(24, 32);
			this.buttonToAvailable.TabIndex = 21;
			this.buttonToAvailable.Text = ">";
			this.buttonToAvailable.Click += new System.EventHandler(this.buttonToAvailable_Click);
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(8, 168);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(56, 24);
			this.label9.TabIndex = 22;
			this.label9.Text = "OffSet(bytes)";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBoxOffset
			// 
			this.textBoxOffset.Location = new System.Drawing.Point(72, 168);
			this.textBoxOffset.Name = "textBoxOffset";
			this.textBoxOffset.Size = new System.Drawing.Size(120, 20);
			this.textBoxOffset.TabIndex = 23;
			this.textBoxOffset.Text = "";
			// 
			// LoadRawSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(456, 414);
			this.ControlBox = false;
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.textBoxOffset,
																		  this.label9,
																		  this.buttonToAvailable,
																		  this.buttonToUsing,
																		  this.label8,
																		  this.listBoxPixelFormatAvailable,
																		  this.listBoxPixelFormatSelected,
																		  this.button2,
																		  this.button1,
																		  this.textBoxStride,
																		  this.textBoxHighBit,
																		  this.textBoxHeight,
																		  this.textBoxWidth,
																		  this.textBoxBytesPer,
																		  this.textBoxBitsPer,
																		  this.label7,
																		  this.label6,
																		  this.label5,
																		  this.label4,
																		  this.label3,
																		  this.label2,
																		  this.label1});
			this.Name = "LoadRawSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Raw Image Settings:";
			this.Load += new System.EventHandler(this.LoadRawSettings_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void LoadRawSettings_Load(object sender, System.EventArgs e)
		{
						
			listBoxPixelFormatSelected.Items.Clear();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
		public int GetRawBitsPerPixel()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxBitsPer.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public int GetRawBytesPerPixel()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxBytesPer.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public int GetRawHeight()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxHeight.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public int GetRawWidth()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxWidth.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public int GetRawHighBitIndex()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxHighBit.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public int GetRawStride()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxStride.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public int GetRawOffset()
		{
			int daRet;
			try 
			{
				daRet = Convert.ToInt32(textBoxOffset.Text);
			} 
			catch 
			{
				return 0;
			}
			return daRet;
		}
		public PegasusImaging.WinForms.ImagXpress8.ImageXPixelFormat GetRawPixelFormat()
		{
			System.Int32 daOptions = 0;
			for (int i = 0; i < listBoxPixelFormatSelected.Items.Count; i++)
			{
				daOptions |= pixelFormatNums[GetIndexOfItemString(listBoxPixelFormatSelected.Items[i].ToString())];
			}
			return ((PegasusImaging.WinForms.ImagXpress8.ImageXPixelFormat)daOptions);
		}

		public int GetIndexOfItemString(string PixelFormatString)
		{
			for (int i = 0; i < pixelFormats.Length; i++)
			{
				if (pixelFormats[i] == PixelFormatString)
				{
					return i;
				}
			}
			return -1;
		}

		private void buttonToUsing_Click(object sender, System.EventArgs e)
		{
			if (listBoxPixelFormatAvailable.SelectedItem != null)
			{
				listBoxPixelFormatSelected.Items.Add(listBoxPixelFormatAvailable.SelectedItem);
				listBoxPixelFormatAvailable.Items.Remove(listBoxPixelFormatAvailable.SelectedItem);
			}
		}

		private void buttonToAvailable_Click(object sender, System.EventArgs e)
		{
			if (listBoxPixelFormatSelected.SelectedItem != null)
			{
				listBoxPixelFormatAvailable.Items.Add(listBoxPixelFormatSelected.SelectedItem);
				listBoxPixelFormatSelected.Items.Remove(listBoxPixelFormatSelected.SelectedItem);
			}
		}

		
	}
}

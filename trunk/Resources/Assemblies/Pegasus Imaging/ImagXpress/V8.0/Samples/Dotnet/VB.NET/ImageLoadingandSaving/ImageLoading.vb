'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Public Class ImageLoading
    Inherits System.Windows.Forms.Form

    'declare variables

    Private strCurrentImage As System.String
    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Private so As PegasusImaging.WinForms.ImagXpress8.SaveOptions

    'File I/O Variables

    Dim strCurrentDir As System.String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)


        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If


            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents lstDesc As System.Windows.Forms.ListBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents mnuToolBar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents cmbMethod As System.Windows.Forms.ComboBox
    Friend WithEvents cmbLoad As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmbSave As System.Windows.Forms.ComboBox
    Friend WithEvents lststatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.lstDesc = New System.Windows.Forms.ListBox()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuOpen = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.mnuQuit = New System.Windows.Forms.MenuItem()
        Me.mnuToolBar = New System.Windows.Forms.MenuItem()
        Me.mnuShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.cmbMethod = New System.Windows.Forms.ComboBox()
        Me.cmbLoad = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.cmbSave = New System.Windows.Forms.ComboBox()
        Me.lststatus = New System.Windows.Forms.ListBox()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(16, 184)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(384, 248)
        Me.ImageXView1.TabIndex = 0
        '
        'lstDesc
        '
        Me.lstDesc.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDesc.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Loading image data into the control via the FromFile, FromStream, FromHBitmap a" & _
        "nd FromHdib methods.", "2)Using the SaveOptions class for saving to various image formats."})
        Me.lstDesc.Location = New System.Drawing.Point(16, 8)
        Me.lstDesc.Name = "lstDesc"
        Me.lstDesc.Size = New System.Drawing.Size(608, 56)
        Me.lstDesc.TabIndex = 1
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolBar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.MenuItem2, Me.mnuQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Text = "&Open"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 2
        Me.mnuQuit.Text = "&Quit"
        '
        'mnuToolBar
        '
        Me.mnuToolBar.Index = 1
        Me.mnuToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuShow})
        Me.mnuToolBar.Text = "&ToolBar"
        '
        'mnuShow
        '
        Me.mnuShow.Index = 0
        Me.mnuShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'lblerror
        '
        Me.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblerror.Location = New System.Drawing.Point(320, 104)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(312, 72)
        Me.lblerror.TabIndex = 2
        '
        'cmbMethod
        '
        Me.cmbMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMethod.Items.AddRange(New Object() {"From File", "From Stream", "From HBITMAP"})
        Me.cmbMethod.Location = New System.Drawing.Point(128, 88)
        Me.cmbMethod.Name = "cmbMethod"
        Me.cmbMethod.Size = New System.Drawing.Size(176, 21)
        Me.cmbMethod.TabIndex = 3
        '
        'cmbLoad
        '
        Me.cmbLoad.Location = New System.Drawing.Point(16, 88)
        Me.cmbLoad.Name = "cmbLoad"
        Me.cmbLoad.Size = New System.Drawing.Size(104, 32)
        Me.cmbLoad.TabIndex = 4
        Me.cmbLoad.Text = "Load Image By Method"
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(16, 128)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(104, 32)
        Me.cmdSave.TabIndex = 5
        Me.cmdSave.Text = "Save Image"
        '
        'cmbSave
        '
        Me.cmbSave.Items.AddRange(New Object() {"Save as BMP", "Save as JPG", "Save as TIFF"})
        Me.cmbSave.Location = New System.Drawing.Point(128, 128)
        Me.cmbSave.Name = "cmbSave"
        Me.cmbSave.Size = New System.Drawing.Size(176, 21)
        Me.cmbSave.TabIndex = 6
        '
        'lststatus
        '
        Me.lststatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lststatus.Location = New System.Drawing.Point(416, 184)
        Me.lststatus.Name = "lststatus"
        Me.lststatus.Size = New System.Drawing.Size(208, 251)
        Me.lststatus.TabIndex = 7
        '
        'lblLastError
        '
        Me.lblLastError.Location = New System.Drawing.Point(320, 72)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(160, 24)
        Me.lblLastError.TabIndex = 8
        Me.lblLastError.Text = "Last Error:"
        '
        'ImageLoading
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(640, 441)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblLastError, Me.lststatus, Me.cmbSave, Me.cmdSave, Me.cmbLoad, Me.cmbMethod, Me.lblerror, Me.lstDesc, Me.ImageXView1})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu = Me.MainMenu1
        Me.Name = "ImageLoading"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ImageLoading"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region


    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()
    End Sub

    Private Sub mnuOpen_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then
            Try
                lblerror.Text = ""
                strCurrentImage = strtemp
                ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblerror)

            End Try
        End If

    End Sub

    Private Sub LoadFile()
        Try
            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage)
            ImageXView1.Image = imagX1

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub mnuShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShow.Click
        If ImageXView1.Toolbar.Activated = True Then
            mnuShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False
        Else
            mnuShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True

        End If
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub ImageLoading_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)

        cmbMethod.SelectedIndex = 0
        cmbSave.SelectedIndex = 0
        '***** Event handlers
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

        Try

            'here we set the current directory and image so that the file open dialog box works well
            strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString()
            strCurrentImage = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\vermont.jpg")
            strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub cmbLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLoad.Click
        lblerror.Text = ""
        Select Case cmbMethod.SelectedIndex
            Case 0
                Try
                    ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage)
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.FileLoadException
                    PegasusError(ex, lblerror)
                Catch ex As System.Exception
                    PegasusError(ex, lblerror)
                End Try
            Case 1
                Try
                    Dim fs As System.IO.FileStream = New System.IO.FileStream(strCurrentImage, IO.FileMode.Open, IO.FileAccess.Read)
                    ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromStream(fs)
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.FileLoadException
                    PegasusError(ex, lblerror)
                Catch ex As System.Exception
                    PegasusError(ex, lblerror)
                End Try
            Case 2
                Try
                    Dim bbmp As New System.Drawing.Bitmap(strCurrentImage)
                    If Not bbmp Is Nothing Then
                        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHbitmap(bbmp.GetHbitmap())
                    Else
                        lblerror.Text = "Unable to load image into a Bitmap object."
                    End If
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.FileLoadException

                Catch ex As System.Exception
                    PegasusError(ex, lblerror)
                End Try
        End Select

    End Sub
        
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Select Case cmbSave.SelectedIndex
            'bmp save
            Case 0
                Try
                    so = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
                    so.Format = ImageXFormat.Bmp
                    ImageXView1.Image.Save("tmp.bmp", so)

                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.FileLoadException
                    PegasusError(ex, lblerror)
                Catch ex As System.Exception
                    PegasusError(ex, lblerror)
                End Try
                'jpg save
            Case 1
                Try
                    so = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
                    so.Format = ImageXFormat.Jpeg
                    so.Jpeg.Chrominance = 13
                    so.Jpeg.Luminance = 13
                    so.Jpeg.SubSampling = SubSampling.SubSampling111
                    ImageXView1.Image.Save("temp.jpg", so)

                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.FileLoadException
                    PegasusError(ex, lblerror)
                Catch ex As System.Exception
                    PegasusError(ex, lblerror)
                End Try

                'tiff save
            Case 2
                Try
                    so = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
                    so.Format = ImageXFormat.Tiff
                    so.Tiff.Compression = Compression.Group4
                    ImageXView1.Image.Save("temp.tiff", so)
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                Catch ex As System.IO.FileLoadException

                Catch ex As System.Exception
                    PegasusError(ex, lblerror)
                End Try
        End Select

    End Sub


    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If

    End Sub


    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

    End Sub

End Class

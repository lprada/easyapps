'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.IO
Imports PegasusImaging.WinForms.ImagXpress8

Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Dim soSaveOptions As PegasusImaging.WinForms.ImagXpress8.SaveOptions
    Dim loLoadOptions As PegasusImaging.WinForms.ImagXpress8.LoadOptions
    Dim lCompressedSize As Long
    Dim lFileSize As Long
    Dim dCompressionRatio As Double

    'File I/O Variables

    Dim strCurrentDir As System.String
    Dim strimageFile As System.String

#Region " Pegasus Imaging Sample Application Standard Functions "
    '*********************************************************************
    'Pegasus Imaging Corporation Standard Function Definitions     *
    '*********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As System.String = "..\..\..\..\..\..\..\..\Common\Images\"

    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String
        Return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length _
                        - (FullName.LastIndexOf("\\") - 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile() As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strDefaultImageFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusOpenFile(ByVal strFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = "Select an Image File"
        dlg.Filter = strFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            If Not (imageXView1 Is Nothing) Then
                imageXView1.Dispose()
                imageXView1 = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblShowCR As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rb211v As System.Windows.Forms.RadioButton
    Friend WithEvents rb411 As System.Windows.Forms.RadioButton
    Friend WithEvents rb211 As System.Windows.Forms.RadioButton
    Friend WithEvents rb111 As System.Windows.Forms.RadioButton
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents hsLumin As System.Windows.Forms.HScrollBar
    Friend WithEvents hsChrom As System.Windows.Forms.HScrollBar
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents imageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    Friend WithEvents lblLumin As System.Windows.Forms.Label
    Friend WithEvents lblChrom As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblError = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblShowCR = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.rb211v = New System.Windows.Forms.RadioButton()
        Me.rb411 = New System.Windows.Forms.RadioButton()
        Me.rb211 = New System.Windows.Forms.RadioButton()
        Me.rb111 = New System.Windows.Forms.RadioButton()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.hsLumin = New System.Windows.Forms.HScrollBar()
        Me.hsChrom = New System.Windows.Forms.HScrollBar()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.imageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuFileOpen = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuQuit = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.mnuToolbar = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.lblLumin = New System.Windows.Forms.Label()
        Me.lblChrom = New System.Windows.Forms.Label()
        Me.groupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblError
        '
        Me.lblError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblError.Location = New System.Drawing.Point(496, 431)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(216, 208)
        Me.lblError.TabIndex = 24
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(496, 391)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(184, 32)
        Me.lblLastError.TabIndex = 23
        Me.lblLastError.Text = "Last Error:"
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(496, 167)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(216, 173)
        Me.lstStatus.TabIndex = 22
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(496, 135)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(160, 24)
        Me.lblLoadStatus.TabIndex = 21
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblShowCR
        '
        Me.lblShowCR.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.lblShowCR.Location = New System.Drawing.Point(152, 624)
        Me.lblShowCR.Name = "lblShowCR"
        Me.lblShowCR.Size = New System.Drawing.Size(104, 16)
        Me.lblShowCR.TabIndex = 20
        Me.lblShowCR.Text = "lblShowCR"
        '
        'label3
        '
        Me.label3.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.label3.Location = New System.Drawing.Point(24, 624)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(104, 24)
        Me.label3.TabIndex = 19
        Me.label3.Text = "Compression Ratio:"
        '
        'groupBox1
        '
        Me.groupBox1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.groupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.rb211v, Me.rb411, Me.rb211, Me.rb111})
        Me.groupBox1.Location = New System.Drawing.Point(24, 559)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(432, 48)
        Me.groupBox1.TabIndex = 18
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Sub Sampling"
        '
        'rb211v
        '
        Me.rb211v.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.rb211v.Location = New System.Drawing.Point(288, 16)
        Me.rb211v.Name = "rb211v"
        Me.rb211v.Size = New System.Drawing.Size(96, 24)
        Me.rb211v.TabIndex = 3
        Me.rb211v.Text = "2:1:1 v"
        '
        'rb411
        '
        Me.rb411.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.rb411.Location = New System.Drawing.Point(184, 16)
        Me.rb411.Name = "rb411"
        Me.rb411.Size = New System.Drawing.Size(64, 24)
        Me.rb411.TabIndex = 2
        Me.rb411.Text = "4:1:1"
        '
        'rb211
        '
        Me.rb211.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.rb211.Location = New System.Drawing.Point(112, 16)
        Me.rb211.Name = "rb211"
        Me.rb211.Size = New System.Drawing.Size(56, 24)
        Me.rb211.TabIndex = 1
        Me.rb211.Text = "2:1:1"
        '
        'rb111
        '
        Me.rb111.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.rb111.Location = New System.Drawing.Point(24, 16)
        Me.rb111.Name = "rb111"
        Me.rb111.Size = New System.Drawing.Size(64, 24)
        Me.rb111.TabIndex = 0
        Me.rb111.Text = "1:1:1"
        '
        'label2
        '
        Me.label2.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.label2.Location = New System.Drawing.Point(24, 520)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(48, 24)
        Me.label2.TabIndex = 17
        Me.label2.Text = "Lum."
        '
        'label1
        '
        Me.label1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.label1.Location = New System.Drawing.Point(24, 464)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(48, 25)
        Me.label1.TabIndex = 16
        Me.label1.Text = "Chrom."
        '
        'hsLumin
        '
        Me.hsLumin.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.hsLumin.Location = New System.Drawing.Point(72, 520)
        Me.hsLumin.Maximum = 255
        Me.hsLumin.Minimum = 1
        Me.hsLumin.Name = "hsLumin"
        Me.hsLumin.Size = New System.Drawing.Size(344, 24)
        Me.hsLumin.TabIndex = 15
        Me.hsLumin.Value = 1
        '
        'hsChrom
        '
        Me.hsChrom.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.hsChrom.Location = New System.Drawing.Point(72, 464)
        Me.hsChrom.Maximum = 250
        Me.hsChrom.Minimum = 1
        Me.hsChrom.Name = "hsChrom"
        Me.hsChrom.Size = New System.Drawing.Size(344, 24)
        Me.hsChrom.TabIndex = 14
        Me.hsChrom.Value = 1
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Varying the JPEG quality settings using the SaveJPEGChromFactor, SaveJPEGLumFac" & _
        "tor ", "    and SaveJPEGSubSampling properties.", "2)Saving an image to a Memory Stream."})
        Me.lstInfo.Location = New System.Drawing.Point(24, 16)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(696, 69)
        Me.lstInfo.TabIndex = 13
        '
        'imageXView1
        '
        Me.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.imageXView1.AutoScroll = True
        Me.imageXView1.Location = New System.Drawing.Point(24, 120)
        Me.imageXView1.Name = "imageXView1"
        Me.imageXView1.Size = New System.Drawing.Size(400, 296)
        Me.imageXView1.TabIndex = 25
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.mnuAbout, Me.mnuToolbar})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileOpen, Me.MenuItem3, Me.mnuQuit})
        Me.MenuItem1.Text = "File"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Index = 0
        Me.mnuFileOpen.Text = "Open"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 2
        Me.mnuQuit.Text = "Quit"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 1
        Me.mnuAbout.Text = "About"
        '
        'mnuToolbar
        '
        Me.mnuToolbar.Index = 2
        Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolbar.Text = "Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "Show"
        '
        'lblLumin
        '
        Me.lblLumin.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.lblLumin.Location = New System.Drawing.Point(432, 520)
        Me.lblLumin.Name = "lblLumin"
        Me.lblLumin.Size = New System.Drawing.Size(40, 32)
        Me.lblLumin.TabIndex = 27
        '
        'lblChrom
        '
        Me.lblChrom.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.lblChrom.Location = New System.Drawing.Point(432, 464)
        Me.lblChrom.Name = "lblChrom"
        Me.lblChrom.Size = New System.Drawing.Size(40, 32)
        Me.lblChrom.TabIndex = 26
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(760, 670)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblLumin, Me.lblChrom, Me.imageXView1, Me.lblError, Me.lblLastError, Me.lstStatus, Me.lblLoadStatus, Me.lblShowCR, Me.label3, Me.groupBox1, Me.label2, Me.label1, Me.hsLumin, Me.hsChrom, Me.lstInfo})
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "JPGCompression"
        Me.groupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
   



    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '***Must call UnlockRuntime to Distribute Application*** 
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        imagX1 = New PegasusImaging.WinForms.ImagXpress8.ImageX()

        '***** Event handlers
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

        Try

            'here we set the current directory and image so that the file open dialog box works well
            strCurrentDir = System.IO.Directory.GetCurrentDirectory().ToString()
            strimageFile = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\vermont.jpg")
            strCurrentDir = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try
     

        soSaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
        lblChrom.Text = soSaveOptions.Jpeg.Chrominance.ToString
        lblLumin.Text = soSaveOptions.Jpeg.Luminance.ToString
        hsChrom.Value = soSaveOptions.Jpeg.Chrominance
        hsLumin.Value = soSaveOptions.Jpeg.Luminance
        rb411.Checked = True
        LoadFile()
    End Sub

    Private Sub UpdateImage()
        soSaveOptions.Jpeg.Chrominance = hsChrom.Value
        soSaveOptions.Jpeg.Luminance = hsLumin.Value
        soSaveOptions.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Jpeg
        lblChrom.Text = soSaveOptions.Jpeg.Chrominance.ToString
        lblLumin.Text = soSaveOptions.Jpeg.Luminance.ToString
        Dim fsStream As System.IO.MemoryStream = New System.IO.MemoryStream()
        Try
            'save the file to memory
            imagX1.SaveStream(fsStream, soSaveOptions)
            fsStream.Flush()
            fsStream.Position = 0
            'load the stream
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromStream(fsStream)
            'Compute the compression ratio
            lCompressedSize = fsStream.Length
            lFileSize = imagX1.ImageXData.Width
            lFileSize = (lFileSize _
                        * (imagX1.ImageXData.Height * 3))
            If (lCompressedSize > 0) Then
                dCompressionRatio = (CType(lFileSize, Double) / CType(lCompressedSize, Double))
                lblShowCR.Text = dCompressionRatio.ToString("##0.0 : 1")
            Else
                lblShowCR.Text = "N/A"
            End If
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try
        fsStream.Close()
    End Sub

    Private Sub LoadFile()
        Try
            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)
            'clear out the error in case there was an error from a previous operation
            lblError.Text = ""
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblError)
        End Try
        UpdateImage()
    End Sub

    Private Sub rb111_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress8.SubSampling.SubSampling111
        LoadFile()
    End Sub

    Private Sub rb211_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress8.SubSampling.SubSampling211
        LoadFile()
    End Sub

    Private Sub rb411_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress8.SubSampling.SubSampling411
        LoadFile()
    End Sub

    Private Sub rb211v_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        soSaveOptions.Jpeg.SubSampling = PegasusImaging.WinForms.ImagXpress8.SubSampling.SubSampling211v
        LoadFile()
    End Sub

    Private Sub hsChrom_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hsChrom.Scroll
        lblChrom.Text = hsChrom.Value
        LoadFile()
    End Sub

    Private Sub hsLumin_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hsLumin.Scroll
        lblLumin.Text = hsLumin.Value
        LoadFile()
    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click
        'obtain the filename of the image to open
        Dim strtemp As System.String = PegasusOpenFile()
        If strtemp.Length <> 0 Then
            Try
                strimageFile = strtemp
                imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strimageFile)
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblError)
            Catch ex As System.IO.IOException
                PegasusError(ex, lblError)

            End Try
        End If
        LoadFile()
    End Sub

    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        Try
            ImagXpress1.AboutBox()
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(eX, lblError)
        End Try
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
        If imageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            imageXView1.Toolbar.Activated = False

        Else
            mnuToolbarShow.Text = "Hide"
            imageXView1.Toolbar.Activated = True
        End If
    End Sub

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If

    End Sub


    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)

    End Sub

End Class

/****************************************************************
 * Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/


using System;
using System.Threading;

namespace ThreadPoolSample
{
	public class IXWorkerThread
	{
		private const int numOfOptions = 19;
		private static System.Drawing.Color[] daColors = { //19 items
															 System.Drawing.Color.Aqua,
															 System.Drawing.Color.Blue,
															 System.Drawing.Color.Green,
															 System.Drawing.Color.Red,
															 System.Drawing.Color.Purple,
															 System.Drawing.Color.MediumSeaGreen,
															 System.Drawing.Color.LemonChiffon,
															 System.Drawing.Color.Salmon,
															 System.Drawing.Color.Indigo,
															 System.Drawing.Color.GreenYellow,
															 System.Drawing.Color.Azure,
															 System.Drawing.Color.MistyRose,
															 System.Drawing.Color.LightSteelBlue,
															 System.Drawing.Color.Bisque,
															 System.Drawing.Color.Moccasin,
															 System.Drawing.Color.Fuchsia,
															 System.Drawing.Color.Gainsboro,
															 System.Drawing.Color.Gold,
															 System.Drawing.Color.Goldenrod};


		private int myWorkID;
		private System.Drawing.Image myImage;
		private ManualResetEvent myFinishEvent;
		private System.Random Rand;
		
		public IXWorkerThread(int seed, ManualResetEvent finishEvent)
		{
			myWorkID = seed;
			myFinishEvent = finishEvent;
			Rand = new System.Random(seed);
		}

		public void ThreadPoolCallback(object threadContext)
		{
			int threadInddex = (int) threadContext;
			myImage = DoWork(myWorkID);
			//GC.Collect();
			//System.Threading.Thread.Sleep(100);
			myFinishEvent.Set();
		}		
		private void IXDrawCircle(int x, int y, int d, System.Drawing.Color c, float ar, 
			PegasusImaging.WinForms.ImagXpress8.ImageX daImage,
			PegasusImaging.WinForms.ImagXpress8.Processor daPrc)
		{
			daImage.Resolution.Dimensions = new System.Drawing.SizeF( 96.0f, 96.0f );
			System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(daImage.ToBitmap(false));
			System.Drawing.Pen daPen = new System.Drawing.Pen(c);//,(float)((float)Rand.Next(10,100))/10.0);
			g.DrawEllipse(daPen,x,y,d,(int)(d*ar));           
		}
		private void IXDrawPie(int x, int y, int w, int h, short start, short end, System.Drawing.Color c, 
			PegasusImaging.WinForms.ImagXpress8.ImageX daImage,
			PegasusImaging.WinForms.ImagXpress8.Processor daPrc)
		{
			daImage.Resolution.Dimensions = new System.Drawing.SizeF( 96.0f, 96.0f );
			System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(daImage.ToBitmap(false));
			System.Drawing.Pen daPen = new System.Drawing.Pen(c);//,(float)((float)Rand.Next(10,100))/10.0);
			g.DrawPie(daPen,x,y,w,h,start,end);
		}
		private void IXDrawRoundRect(int x, int y, int w, int h, System.Drawing.Color c, 
			PegasusImaging.WinForms.ImagXpress8.ImageX daImage,
			PegasusImaging.WinForms.ImagXpress8.Processor daPrc)
		{
			daImage.Resolution.Dimensions = new System.Drawing.SizeF( 96.0f, 96.0f );
			System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(daImage.ToBitmap(false));
			System.Drawing.Pen daPen = new System.Drawing.Pen(c);//,(float)((float)Rand.Next(10,100))/10.0);
			g.DrawRectangle(daPen,x,y,w,h);
		}
		private void DoJob(PegasusImaging.WinForms.ImagXpress8.ImageX daImage, int daJob)
		{
			PegasusImaging.WinForms.ImagXpress8.Processor daIX = new PegasusImaging.WinForms.ImagXpress8.Processor(daImage);
			try 
			{
				switch(daJob)
				{
					//case 0: daIX.Rotate(((double)Rand.Next(1000))/100.0); break;
					case 1: daIX.Soften((short)Rand.Next(1,10)); break;
					case 2: daIX.Dilate(); break;
					case 3: daIX.Mosaic((short)Rand.Next(2,16)); break;
					case 4: daIX.Unsharpen((short)Rand.Next(1,10)); break;
					case 5: daIX.Blend((short)Rand.Next(25)); break;
					case 6: daIX.ReplaceColors(daColors[Rand.Next(numOfOptions)],daColors[Rand.Next(numOfOptions)],daColors[Rand.Next(numOfOptions)]); break;
					case 7: IXDrawCircle(64,64,Rand.Next(32),daColors[Rand.Next(numOfOptions)],(float)1.0,daImage,daIX); break;
					case 8: IXDrawCircle(Rand.Next(64),Rand.Next(64),Rand.Next(64),daColors[Rand.Next(numOfOptions)],(float)1.33,daImage,daIX); break;
					case 9: daIX.Rotate(((double)(((double)Rand.Next(0,3600))/10.0))); break;
					case 10: daIX.Resize(new System.Drawing.Size(Rand.Next(384,512),Rand.Next(256,384)),(PegasusImaging.WinForms.ImagXpress8.ResizeType)Rand.Next(0,1)); break;
					case 11: IXDrawPie(Rand.Next(32),Rand.Next(32),Rand.Next(32,64),Rand.Next(32,64),(short)Rand.Next(90),(short)Rand.Next(90,360),daColors[Rand.Next(numOfOptions)],daImage,daIX); break;
					case 12: IXDrawCircle(Rand.Next(128),Rand.Next(128),Rand.Next(24,64),daColors[Rand.Next(numOfOptions)],(float)1.0,daImage,daIX); break;
					case 13: IXDrawRoundRect(Rand.Next(64),Rand.Next(64),Rand.Next(64,128),Rand.Next(64,128),daColors[Rand.Next(numOfOptions)],daImage,daIX); break;
					case 14: daIX.MotionBlur((short)Rand.Next(1,10),(short)Rand.Next(-5,5),(short)Rand.Next(-5,5)); break;
					case 15: daIX.Diffuse(); break;
					case 16: daIX.Mosaic((short)Rand.Next(12,32)); break;
					case 17: daIX.Pinch((short)Rand.Next(-25,25)); break;
					case 18: daIX.Ripple(Rand.Next(20,50),Rand.Next(1,10),(PegasusImaging.WinForms.ImagXpress8.RippleDirection)Rand.Next(0,2)); break;
					case 19: daIX.Sharpen((short)Rand.Next(1,10)); break;
					case 20: daIX.Swirl((double) ((double)(Rand.Next(1,360000)))/1000.000); break;
					case 21: daIX.Tile(new System.Drawing.Size(Rand.Next(1024,2048),Rand.Next(1024,2048))); daIX.Resize(new System.Drawing.Size(Rand.Next(384,512),Rand.Next(256,384)),PegasusImaging.WinForms.ImagXpress8.ResizeType.Fast,false,false); break;
					case 22: daIX.Twist(Rand.Next(2,63),(PegasusImaging.WinForms.ImagXpress8.TwistRotation)Rand.Next(0,3)); break;
					case 23: daIX.Median(); break;
					case 24: IXDrawPie(Rand.Next(96),Rand.Next(96),Rand.Next(8,128),Rand.Next(8,128),(short)Rand.Next(180),(short)Rand.Next(180,360),daColors[Rand.Next(numOfOptions)],daImage,daIX); break;
					case 25: IXDrawCircle(Rand.Next(32,64),Rand.Next(64,128),Rand.Next(16,128),daColors[Rand.Next(numOfOptions)],(float)1.0,daImage,daIX); break;
					case 26: IXDrawRoundRect(Rand.Next(16,96),Rand.Next(16,96),Rand.Next(8,128),Rand.Next(8,128),daColors[Rand.Next(numOfOptions)],daImage,daIX); break;
					case 27: IXDrawCircle(Rand.Next(16,96),Rand.Next(16,96),Rand.Next(16,96),daColors[Rand.Next(numOfOptions)],(float)1.0,daImage,daIX); break;
					case 28: IXDrawCircle(Rand.Next(128),Rand.Next(128),Rand.Next(64),daColors[Rand.Next(numOfOptions)],(float)1.33,daImage,daIX); break;
					case 29: IXDrawPie(Rand.Next(64),Rand.Next(64),Rand.Next(8,64),Rand.Next(8,64),(short)Rand.Next(180),(short)Rand.Next(180,360),daColors[Rand.Next(numOfOptions)],daImage,daIX); break;
					case 30: IXDrawCircle(Rand.Next(16,32),Rand.Next(64,128),Rand.Next(48,96),daColors[Rand.Next(numOfOptions)],(float)1.0,daImage,daIX); break;
					case 31: IXDrawRoundRect(Rand.Next(32,64),Rand.Next(32,64),Rand.Next(64,128),Rand.Next(64,128),daColors[Rand.Next(numOfOptions)],daImage,daIX); break;
					case 32: IXDrawCircle(Rand.Next(16,96),Rand.Next(16,96),Rand.Next(128),daColors[Rand.Next(numOfOptions)],(float)1.0,daImage,daIX); break;
					case 33: IXDrawCircle(Rand.Next(64),Rand.Next(64),Rand.Next(64),daColors[Rand.Next(numOfOptions)],(float)1.33,daImage,daIX); break;
					default: daIX.Dilate(); break;
				}
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX)
			{
				System.Windows.Forms.MessageBox.Show(eX.Message);
			}
			daIX = null;
		}

		public System.Drawing.Image GetResult()
		{
			return myImage;
		}
		private System.Drawing.Image DoWork(int workID)
		{			
			PegasusImaging.WinForms.ImagXpress8.ImageX myNewImage = new PegasusImaging.WinForms.ImagXpress8.ImageX(Rand.Next(384,640),Rand.Next(384,480),24,daColors[Rand.Next(numOfOptions)]);
						
			for (int i = 0; i < Rand.Next(48,192); i++)
			{
				DoJob(myNewImage,Rand.Next(34));
			}

			myNewImage .Resolution.Dimensions = new System.Drawing.SizeF( 96.0f, 96.0f );
			System.Drawing.Image retter = ((System.Drawing.Image)myNewImage.ToBitmap(false));
			myNewImage.Dispose();
			myNewImage = null;
			return retter;
		}

	}
}
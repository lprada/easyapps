VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form KeyEvents 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Key Events Sample"
   ClientHeight    =   4005
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   5130
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4005
   ScaleWidth      =   5130
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "KeyEvents.frx":0000
      Left            =   120
      List            =   "KeyEvents.frx":000A
      TabIndex        =   3
      Top             =   240
      Width           =   4695
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   1335
      Left            =   3120
      TabIndex        =   2
      Top             =   1320
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   2355
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   1398895397
      ErrInfo         =   -1774795010
      Persistence     =   -1  'True
      _cx             =   2990
      _cy             =   2355
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   615
      Left            =   3360
      TabIndex        =   1
      Top             =   2880
      Width           =   1215
   End
   Begin VB.ListBox List1 
      Height          =   1425
      Left            =   120
      TabIndex        =   0
      Top             =   2160
      Width           =   2415
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "KeyEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Function KeyString(ByVal KeyCode As Integer, ByVal Shift As Integer) As String
  Select Case KeyCode
    Case 16:  KeyStr$ = "SHIFT"
    Case 17:  KeyStr$ = "CTRL"
    Case 18:  KeyStr$ = "ALT"
    Case Else: KeyStr$ = Chr(KeyCode)
  End Select
  If Shift And 4 Then KeyStr$ = KeyStr$ & " ALT"
  If Shift And 2 Then KeyStr$ = KeyStr$ & " CTL"
  If Shift And 1 Then KeyStr$ = KeyStr$ & " SHFT"
  KeyString = KeyStr$
End Function

Private Sub cmdClear_Click()
  List1.Clear
  ImagXpress1.SetFocus
End Sub

Private Sub Form_Activate()
    
    ImagXpress1.SetFocus
End Sub

Private Sub ImagXpress1_KeyDown(KeyCode As Integer, Shift As Integer)
  List1.AddItem "KeyDown " & KeyString(KeyCode, Shift)
End Sub

Private Sub ImagXpress1_KeyPress(KeyAscii As Integer)
  List1.AddItem "KeyPress " & Chr(KeyAscii)
End Sub

Private Sub ImagXpress1_KeyUp(KeyCode As Integer, Shift As Integer)
  List1.AddItem "KeyUp " & KeyString(KeyCode, Shift)
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

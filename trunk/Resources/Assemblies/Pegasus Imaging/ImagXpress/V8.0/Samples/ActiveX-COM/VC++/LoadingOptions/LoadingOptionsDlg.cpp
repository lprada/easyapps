// LoadingOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LoadingOptions.h"
#include "LoadingOptionsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Include ImagXpress Initialization functions

#include "..\Include\ix8_open.cpp"


static TCHAR szFileTitle[]          = _T("Open Image File");

static TCHAR szFilter[]             = _T("Tagged Image Format (*.tif)\0*.tif\0Windows BMP (*.bmp)\0*.bmp\0JPEG (*.jpg)\0*.jpg\0CALS (*.CAL)\0*.CAL\0Windows Device Independent Bitmap (*.DIB)\0*.DIB\0MO:DCA (*.DCA + MOD)\0*.DCA;*.MOD\0ZSoft Multiple Page (*.DCX)\0*.DCX\0GIF (*.GIF)\0*.GIF\0JPEG 2000 (*.JP2)\0*.JP2\0JPEG LS (*.JLS)\0*.JLS\0Lossless JPG (*.LJP)\0*.LJP\0Portable BMP (*.PBM)\0*.PBM\0ZSoft PaintBrush (*.PCX)\0*.PCX\0Portable Graymap (*.PGM)\0*.PGM\0Pegasus PIC (*.PIC)\0*.PIC\0PNG (*.PNG)\0*.PNG\0Portable Pixmap (*.PPM)\0*.PPM\0Truevision TARGA (*.TGA)\0*.TGA\0WSQ (*.WSQ)\0*.WSQ\0JBIG2 File (*.JB2)\0*.JB2\0 All Files (*.*)\0*.*\0 IXSupportedFiles (*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2)\0*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2");
static TCHAR szMediaDirectory[]     = _T("..\\..\\..\\..\\..\\..\\..\\Common\\Images");
static TCHAR szBarcodeInfoFormat[]  = _T("Barcode #%1!d!\n\rBarode value = %2!s!\n\rBarcode type = %3!s!\n\r");
static TCHAR szErrorMessageFormat[] = _T("No barcodes were found. Error = %1!d!");
static TCHAR szMsgBoxTitle[]        = _T("ImagXpress 8 COM Print Sample");

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App Abo


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoadingOptionsDlg dialog

CLoadingOptionsDlg::CLoadingOptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoadingOptionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoadingOptionsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLoadingOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoadingOptionsDlg)
	DDX_Control(pDX, IDC_LROTATED, m_chkRotated);
	DDX_Control(pDX, IDC_LRESIZE, m_chkResize);
	DDX_Control(pDX, IDC_LCROP, m_chkCrop);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoadingOptionsDlg, CDialog)
	//{{AFX_MSG_MAP(CLoadingOptionsDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LCROP, OnLCropCheck)
	ON_BN_CLICKED(IDC_LRESIZE, OnLResizeCheck)
	ON_BN_CLICKED(IDC_LROTATED, OnLRotatedCheck)
	ON_BN_CLICKED(IDC_CMDLOADIMAGE, OnCmdLoadImage)
	ON_BN_CLICKED(IDC_EXIT, OnExit)
	ON_COMMAND(ID_FILE, OnFile)
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
	ON_COMMAND(ID_TOOLBAR_SHOW, OnToolbarShow)
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_BN_CLICKED(IDC_Load, OnLoad)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoadingOptionsDlg message handlers

BOOL CLoadingOptionsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// ImagXpress initialization
	HINSTANCE hDLL = IX_Open();
	
	// Create an ImagXpress Visible object
	ppCImagXpress1 = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 10, 10, 420, 300);
	pImagXpress1 = ppCImagXpress1->pImagXpress;
	pImagXpress1->AutoSize = ISIZE_CropImage;
	pImagXpress1->ScrollBars = SB_Both;
		
	

	// ImagXpress uninitialize
	IX_Close(hDLL);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLoadingOptionsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLoadingOptionsDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLoadingOptionsDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CLoadingOptionsDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// Delete the ImagXpress Object
	if (pImagXpress1)
	{	
		pImagXpress1 = NULL;
		if (ppCImagXpress1) 
			delete ppCImagXpress1;
	}	
}

void CLoadingOptionsDlg::OnLCropCheck() 
{

	
}

void CLoadingOptionsDlg::OnLResizeCheck() 
{

	
}

void CLoadingOptionsDlg::OnLRotatedCheck() 
{

	
}

void CLoadingOptionsDlg::OnCmdLoadImage() 
{
	char	buf[300];
	int		ret;

	ret = sprintf(buf, "Load Status:\n");

	if (m_chkCrop.GetCheck())
	{
		pImagXpress1->LoadCropEnabled = true;
		pImagXpress1->LoadCropX = 10;
		pImagXpress1->LoadCropY = 10;
		pImagXpress1->LoadCropWidth = 150;
		pImagXpress1->LoadCropHeight = 150;
		ret += sprintf(buf + ret, "\tLoad Crop Enabled: X: 10, Y: 10, Wid: 150, Hgt: 150\n");
	}
	else
	{
		pImagXpress1->LoadCropEnabled = false;
		ret += sprintf(buf + ret, "\tLoad Crop Disabled\n");
	}

	if (m_chkResize.GetCheck())
	{
		pImagXpress1->LoadResizeEnabled = true;
		pImagXpress1->LoadResizeWidth = 200;
		pImagXpress1->LoadResizeHeight = 200;
		pImagXpress1->LoadResizeMaintainAspectRatio = true;
		ret += sprintf(buf + ret, "\tLoad Resize Enabled: Width: 200, Height: 200\n");
	}
	else
	{
		pImagXpress1->LoadResizeEnabled = false;
		ret += sprintf(buf + ret, "\tLoad Resize Disabled\n");
	}

	if (m_chkRotated.GetCheck())
	{
		pImagXpress1->LoadRotated = LR_FLIP;
		ret += sprintf(buf + ret, "\tLoad Rotated Enabled: Rotate: Flip\n");
	}
	else
	{
		pImagXpress1->LoadRotated = LR_NONE;
		ret += sprintf(buf + ret, "\tLoad Rotated Disabled\n");
	}
	
	//Load a default image

    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];
    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
	CString strInitialDirectory = CString(drive) + CString(dir) + "..\\..\\..\\..\\..\\..\\..\\Common\\Images\\bird.jpg";
	pImagXpress1->FileName = _bstr_t(strInitialDirectory);

	if (pImagXpress1->ImagError == 0)
		ret += sprintf(buf + ret, "\tImage Loaded Successfully!");
	else
		ret += sprintf(buf + ret, "Error Loading Image! Error: %d", pImagXpress1->ImagError);

	SetDlgItemText(IDC_STATUS, buf);
}

void CLoadingOptionsDlg::OnExit() 
{
	// we're done
	EndDialog(0);
	
}

void CLoadingOptionsDlg::OnFile() 
{
		//***open file code here	
	
    CFileDialog ofd(TRUE);

    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];

    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
    CString strInitialDirectory = CString(drive) + CString(dir) 
        + szMediaDirectory;

    ofd.m_ofn.lpstrInitialDir   = strInitialDirectory;
    ofd.m_ofn.lpstrTitle        = szFileTitle;
    ofd.m_ofn.lpstrFilter       = szFilter;

    if (ofd.DoModal() == IDOK)
    {
      char	buf[300];
	int		ret;

	ret = sprintf(buf, "Load Status:\n");

	if (m_chkCrop.GetCheck())
	{
		pImagXpress1->LoadCropEnabled = true;
		pImagXpress1->LoadCropX = 10;
		pImagXpress1->LoadCropY = 10;
		pImagXpress1->LoadCropWidth = 150;
		pImagXpress1->LoadCropHeight = 150;
		ret += sprintf(buf + ret, "\tLoad Crop Enabled: X: 10, Y: 10, Wid: 150, Hgt: 150\n");
	}
	else
	{
		pImagXpress1->LoadCropEnabled = false;
		ret += sprintf(buf + ret, "\tLoad Crop Disabled\n");
	}

	if (m_chkResize.GetCheck())
	{
		pImagXpress1->LoadResizeEnabled = true;
		pImagXpress1->LoadResizeWidth = 200;
		pImagXpress1->LoadResizeHeight = 200;
		pImagXpress1->LoadResizeMaintainAspectRatio = true;
		ret += sprintf(buf + ret, "\tLoad Resize Enabled: Width: 200, Height: 200\n");
	}
	else
	{
		pImagXpress1->LoadResizeEnabled = false;
		ret += sprintf(buf + ret, "\tLoad Resize Disabled\n");
	}

	if (m_chkRotated.GetCheck())
	{
		pImagXpress1->LoadRotated = LR_FLIP;
		ret += sprintf(buf + ret, "\tLoad Rotated Enabled: Rotate: Flip\n");
	}
	else
	{
		pImagXpress1->LoadRotated = LR_NONE;
		ret += sprintf(buf + ret, "\tLoad Rotated Disabled\n");
	}


	pImagXpress1->FileName = ofd.m_ofn.lpstrFile ;


	if (pImagXpress1->ImagError == 0)
		ret += sprintf(buf + ret, "\tImage Loaded Successfully!");
	else
		ret += sprintf(buf + ret, "Error Loading Image! Error: %d", pImagXpress1->ImagError);
	

	SetDlgItemText(IDC_STATUS, buf);

	
    }   
	
  
}

void CLoadingOptionsDlg::OnFileQuit() 
{
	EndDialog(IDOK);	
}

void CLoadingOptionsDlg::OnToolbarShow() 
{
	CWnd* pMain = AfxGetMainWnd();
	CMenu* mmenu = pMain->GetMenu();
	CMenu* submenu = mmenu->GetSubMenu(1);

	if(pImagXpress1->ToolbarActivated)
	{
		
		submenu->ModifyMenu(ID_TOOLBAR_SHOW, MF_BYCOMMAND,ID_TOOLBAR_SHOW,"&Show");
	}
	else
	{
		submenu->ModifyMenu(ID_TOOLBAR_SHOW, MF_BYCOMMAND,ID_TOOLBAR_SHOW,"&Hide");

	}
	//enable/disable the toolbar
    pImagXpress1->ToolbarActivated = ! pImagXpress1->ToolbarActivated;	
}

void CLoadingOptionsDlg::OnHelpAbout() 
{
	 pImagXpress1->AboutBox();


	
}

void CLoadingOptionsDlg::OnLoad() 
{

	OnCmdLoadImage();
	
}

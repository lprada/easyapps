VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form FileOffset 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress 8 FileOffset Example"
   ClientHeight    =   6300
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   10155
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   420
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   677
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   2010
      Left            =   6960
      TabIndex        =   6
      Top             =   1680
      Width           =   2895
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "FileOffset.frx":0000
      Left            =   240
      List            =   "FileOffset.frx":000A
      TabIndex        =   4
      Top             =   240
      Width           =   9615
   End
   Begin VB.CommandButton cmd1 
      Caption         =   "Load Image 1"
      Height          =   615
      Left            =   720
      TabIndex        =   2
      Top             =   2640
      Width           =   2775
   End
   Begin VB.CommandButton cmd3 
      Caption         =   "Load Image 3"
      Height          =   615
      Left            =   720
      TabIndex        =   1
      Top             =   3840
      Width           =   2775
   End
   Begin VB.CommandButton cmd2 
      Caption         =   "Load Image 2"
      Height          =   615
      Left            =   720
      TabIndex        =   0
      Top             =   3240
      Width           =   2775
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   2415
      Left            =   3840
      TabIndex        =   3
      Top             =   2280
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   4260
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   577191687
      ErrInfo         =   999784943
      Persistence     =   -1  'True
      _cx             =   4683
      _cy             =   4260
      AutoSize        =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1695
      Left            =   6960
      TabIndex        =   8
      Top             =   4560
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   6960
      TabIndex        =   7
      Top             =   3840
      Width           =   2295
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Top             =   1200
      Width           =   2055
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "FileOffset"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Private Sub cmd1_Click()
  
  ImagXpress1.FileOffset = 0
  LoadFile

End Sub

Private Sub cmd2_Click()
  
  ImagXpress1.FileOffset = 5134
  LoadFile

End Sub
Private Sub cmd3_Click()
  ImagXpress1.FileOffset = 7582
  LoadFile

End Sub

Private Sub LoadFile()
   
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub


Private Sub Form_Load()
    
    imgParentDir = App.Path
    
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\threejpegs.jpg"
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
 End Sub

//
// INLCUDE
//

//****************************************************************'
//* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
//* This sample code is provided to Pegasus licensees "as is"    *'
//* with no restrictions on use or modification. No warranty for *'
//* use of this sample code is provided by Pegasus.              *'
//****************************************************************'

#include "stdafx.h"
#include "EXIFTags.h"
#include "EXIFTagsDlg.h"
#include "..\Include\ix8_open.cpp"

//
// DEFINES
//

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CEXIFTagsDlg, CDialog)
	//{{AFX_MSG_MAP(CEXIFTagsDlg)
    ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_LOADEXIFTAGS, OnBtnLoadexiftags)
	ON_BN_CLICKED(IDC_BTN_ADDACOPYRIGHTTAG, OnBtnAddacopyrighttag)
	ON_BN_CLICKED(IDC_BTN_MODIFYRESOLUTIONTAGS, OnBtnModifyresolutiontags)
	ON_BN_CLICKED(IDC_BTN_DELETECOPYRIGHTTAG, OnBtnDeletecopyrighttag)
	ON_BN_CLICKED(IDC_BTN_SETTAGS, OnBtnSettags)
	ON_BN_CLICKED(IDC_BTN_EXIT, OnBtnExit)
	ON_COMMAND(ID_FILE, OnFile)
	ON_COMMAND(ID_FILE_EXIT, OnFileExit)
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//
// GLOBALS
//

//
// STATICS
//

static TCHAR szFileTitle[]          = _T("Open Image File");
//static TCHAR BASED_CODE szFilter[] = "Chart Files (*.xlc)|*.xlc|Worksheet Files (*.xls)|*.xls|Data Files (*.xlc;*.xls)|*.xlc; *.xls|All Files (*.*)|*.*||";


static TCHAR szFilter[]             = _T("Tagged Image Format (*.tif)\0*.tif\0Windows BMP (*.bmp)\0*.bmp\0JPEG (*.jpg)\0*.jpg\0CALS (*.CAL)\0*.CAL\0Windows Device Independent Bitmap (*.DIB)\0*.DIB\0MO:DCA (*.DCA + MOD)\0*.DCA;*.MOD\0ZSoft Multiple Page (*.DCX)\0*.DCX\0GIF (*.GIF)\0*.GIF\0JPEG 2000 (*.JP2)\0*.JP2\0JPEG LS (*.JLS)\0*.JLS\0Lossless JPG (*.LJP)\0*.LJP\0Portable BMP (*.PBM)\0*.PBM\0ZSoft PaintBrush (*.PCX)\0*.PCX\0Portable Graymap (*.PGM)\0*.PGM\0Pegasus PIC (*.PIC)\0*.PIC\0PNG (*.PNG)\0*.PNG\0Portable Pixmap (*.PPM)\0*.PPM\0Truevision TARGA (*.TGA)\0*.TGA\0WSQ (*.WSQ)\0*.WSQ\0JBIG2 File (*.JB2)\0*.JB2\0 All Files (*.*)\0*.*\0 IXSupportedFiles (*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2)\0*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2"); //IXFiles (*.bmp;*.jpg)\0*.bmp;*.jpg";
static TCHAR szMediaDirectory[]     = _T("..\\..\\..\\..\\..\\..\\..\\Common\\Images");
static TCHAR szBarcodeInfoFormat[]  = _T("Barcode #%1!d!\n\rBarode value = %2!s!\n\rBarcode type = %3!s!\n\r");
static TCHAR szErrorMessageFormat[] = _T("Image Error. Error = %1!d!");
static TCHAR szMsgBoxTitle[]        = _T("ImagXpress 8 COM Print Sample");

static LPCTSTR lpctstrMediaDirectory        = _T("..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");
static LPCTSTR lpctstrDefaultFileName       = _T("exif.jpg");
static LPCTSTR lpctstrLoadFailureFormat     = _T("ImagError #%1!d! encountered.");
static LPCTSTR lpctstrCopyrightString       = _T("Copyright (c) 2004, Pegasus Imaging Corp.\0");
static LPCTSTR lpctstrDescription           = _T("This example demonstrates performing EXIF tag manipulation using the ImagXpress control. The sample demonstrates the following operations:\n\n1) Loading the EXIF tags via the FileGetTags methods. This just loads the tags, but not the image data.\n2) Adding a copyright tag via the TagAdd method.\n3) Deleting a tag via the TagLevel and TagDelete methods.\n4) Modifying the Resolution tags via the TagModify method. These are tags 282 and 283.\n5) This uses the FileSetTags method to write the Tags to file without writing the image data.");
static LPCTSTR lpctstrTagType_Byte          = _T("Byte");
static LPCTSTR lpctstrTagType_Asciichar     = _T("Ascii char");
static LPCTSTR lpctstrTagType_Short         = _T("Short");
static LPCTSTR lpctstrTagType_Long          = _T("Long");
static LPCTSTR lpctstrTagType_Rational      = _T("Rational");
static LPCTSTR lpctstrTagType_Signedbyte    = _T("Signed byte");
static LPCTSTR lpctstrTagType_8bitbyte      = _T("8-bit byte");
static LPCTSTR lpctstrTagType_SignedShort   = _T("Signed Short");
static LPCTSTR lpctstrTagType_SignedLong    = _T("Signed Long");
static LPCTSTR lpctstrTagType_SignedRational = _T("Signed Rational");
static LPCTSTR lpctstrTagType_Float         = _T("Float");
static LPCTSTR lpctstrTagType_Double        = _T("Double");
static LPCTSTR lpctstrTagType_Unknown       = _T("Unknown");
static LPCTSTR lpctstrTag_ExifPixYDim       = _T("ExifPixYDim");
static LPCTSTR lpctstrTag_Artist            = _T("Artist");
static LPCTSTR lpctstrTag_BitsPerSample     = _T("BitsPerSample");
static LPCTSTR lpctstrTag_CellLength        = _T("CellLength");
static LPCTSTR lpctstrTag_CellWidth         = _T("CellWidth");
static LPCTSTR lpctstrTag_ColorMap          = _T("ColorMap");
static LPCTSTR lpctstrTag_Compression       = _T("Compression");
static LPCTSTR lpctstrTag_Copyright         = _T("Copyright");
static LPCTSTR lpctstrTag_DateTime          = _T("DateTime");
static LPCTSTR lpctstrTag_DocumentName      = _T("DocumentName");
static LPCTSTR lpctstrTag_DotRange          = _T("DotRange");
static LPCTSTR lpctstrTag_ExifAperture      = _T("ExifAperture");
static LPCTSTR lpctstrTag_ExifBrightness    = _T("ExifBrightness");
static LPCTSTR lpctstrTag_ExifCfaPattern    = _T("ExifCfaPattern");
static LPCTSTR lpctstrTag_ExifColorSpace    = _T("ExifColorSpace");
static LPCTSTR lpctstrTag_ExifCompBPP       = _T("ExifCompBPP");
static LPCTSTR lpctstrTag_ExifCompConfig    = _T("ExifCompConfig");
static LPCTSTR lpctstrTag_ExifDTDigitized   = _T("ExifDTDigitized");
static LPCTSTR lpctstrTag_ExifDTDigSS       = _T("ExifDTDigSS");
static LPCTSTR lpctstrTag_ExifDTOrig        = _T("ExifDTOrig");
static LPCTSTR lpctstrTag_ExifDTOrigSS      = _T("ExifDTOrigSS");
static LPCTSTR lpctstrTag_ExifDTSubsec      = _T("ExifDTSubsec");
static LPCTSTR lpctstrTag_ExifExposureBias  = _T("ExifExposureBias");
static LPCTSTR lpctstrTag_ExifExposureIndex = _T("ExifExposureIndex");
static LPCTSTR lpctstrTag_ExifExposureProg  = _T("ExifExposureProg");
static LPCTSTR lpctstrTag_ExifExposureTime  = _T("ExifExposureTime");
static LPCTSTR lpctstrTag_ExifFileSource    = _T("ExifFileSource");
static LPCTSTR lpctstrTag_ExifFlash         = _T("ExifFlash");
static LPCTSTR lpctstrTag_ExifFlashEnergy   = _T("ExifFlashEnergy");
static LPCTSTR lpctstrTag_ExifFNumber       = _T("ExifFNumber");
static LPCTSTR lpctstrTag_ExifFocalLength   = _T("ExifFocalLength");
static LPCTSTR lpctstrTag_ExifFocalResUnit  = _T("ExifFocalResUnit");
static LPCTSTR lpctstrTag_ExifFocalXRes     = _T("ExifFocalXRes");
static LPCTSTR lpctstrTag_ExifFocalYRes     = _T("ExifFocalYRes");
static LPCTSTR lpctstrTag_ExifFPXVer        = _T("ExifFPXVer");
static LPCTSTR lpctstrTag_ExifImageHistory  = _T("ExifImageHistory");
static LPCTSTR lpctstrTag_ExifImageNumber   = _T("ExifImageNumber");
static LPCTSTR lpctstrTag_ExifInterlace     = _T("ExifInterlace");
static LPCTSTR lpctstrTag_ExifInterop       = _T("ExifInterop");
static LPCTSTR lpctstrTag_ExifISOSpeed      = _T("ExifISOSpeed");
static LPCTSTR lpctstrTag_ExifLightSource   = _T("ExifLightSource");
static LPCTSTR lpctstrTag_ExifMakerNote     = _T("ExifMakerNote");
static LPCTSTR lpctstrTag_ExifMaxAperture   = _T("ExifMaxAperture");
static LPCTSTR lpctstrTag_ExifMeteringMode  = _T("ExifMeteringMode");
static LPCTSTR lpctstrTag_ExifNoise         = _T("ExifNoise");
static LPCTSTR lpctstrTag_ExifOECF          = _T("ExifOECF");
static LPCTSTR lpctstrTag_ExifPixXDim       = _T("ExifPixXDim");
static LPCTSTR lpctstrTag_ExifRelatedWav    = _T("ExifRelatedWav");
static LPCTSTR lpctstrTag_ExifSceneType     = _T("ExifSceneType");
static LPCTSTR lpctstrTag_ExifSecurityClassification = _T("ExifSecurityClassification");
static LPCTSTR lpctstrTag_ExifSelfTimerMode = _T("ExifSelfTimerMode");
static LPCTSTR lpctstrTag_ExifSensingMethod = _T("ExifSensingMethod");
static LPCTSTR lpctstrTag_ExifShutterSpeed = _T("ExifShutterSpeed");
static LPCTSTR lpctstrTag_ExifSpatialFR     = _T("ExifSpatialFR");
static LPCTSTR lpctstrTag_ExifSpectralSense = _T("ExifSpectralSense");
static LPCTSTR lpctstrTag_ExifSubjectDist   = _T("ExifSubjectDist");
static LPCTSTR lpctstrTag_ExifSubjectLoc    = _T("ExifSubjectLoc");
static LPCTSTR lpctstrTag_ExifTIFF_EPStandardID = _T("ExifTIFF_EPStandardID");
static LPCTSTR lpctstrTag_ExifTimeZoneOffset = _T("ExifTimeZoneOffset");
static LPCTSTR lpctstrTag_ExifUserComment   = _T("ExifUserComment");
static LPCTSTR lpctstrTag_ExifVer           = _T("ExifVer");
static LPCTSTR lpctstrTag_ExtraSamples      = _T("ExtraSamples");
static LPCTSTR lpctstrTag_FillOrder         = _T("FillOrder");
static LPCTSTR lpctstrTag_FreeByteCounts    = _T("FreeByteCounts");
static LPCTSTR lpctstrTag_FreeOffsets       = _T("FreeOffsets");
static LPCTSTR lpctstrTag_GrayResponseCurve = _T("GrayResponseCurve");
static LPCTSTR lpctstrTag_GrayResponseUnit  = _T("GrayResponseUnit");
static LPCTSTR lpctstrTag_HalftoneHints     = _T("HalftoneHints");
static LPCTSTR lpctstrTag_HostComputer      = _T("HostComputer");
static LPCTSTR lpctstrTag_ImageDescription  = _T("ImageDescription");
static LPCTSTR lpctstrTag_ImageLength       = _T("ImageLength");
static LPCTSTR lpctstrTag_ImageWidth        = _T("ImageWidth");
static LPCTSTR lpctstrTag_InkNames          = _T("InkNames");
static LPCTSTR lpctstrTag_InkSet            = _T("InkSet");
static LPCTSTR lpctstrTag_JPEGACTables      = _T("JPEGACTables");
static LPCTSTR lpctstrTag_JPEGDCTables      = _T("JPEGDCTables");
static LPCTSTR lpctstrTag_JPEGDCTTables     = _T("JPEGDCTTables");
static LPCTSTR lpctstrTag_JPEGInterchangeFormat = _T("JPEGInterchangeFormat");
static LPCTSTR lpctstrTag_JPEGInterchangeFormatLength = _T("JPEGInterchangeFormatLength");
static LPCTSTR lpctstrTag_JPEGLosslessPredictors = _T("JPEGLosslessPredictors");
static LPCTSTR lpctstrTag_JPEGPointTransforms = _T("JPEGPointTransforms");
static LPCTSTR lpctstrTag_JPEGProc          = _T("JPEGProc");
static LPCTSTR lpctstrTag_JPEGQTables       = _T("JPEGQTables");
static LPCTSTR lpctstrTag_JPEGRestartInterval = _T("JPEGRestartInterval");
static LPCTSTR lpctstrTag_Make              = _T("Make");
static LPCTSTR lpctstrTag_MaxSampleValue    = _T("MaxSampleValue");
static LPCTSTR lpctstrTag_MinSampleValue    = _T("MinSampleValue");
static LPCTSTR lpctstrTag_Model             = _T("Model");
static LPCTSTR lpctstrTag_NewSubfileType    = _T("NewSubfileType");
static LPCTSTR lpctstrTag_NumberOfInks      = _T("NumberOfInks");
static LPCTSTR lpctstrTag_Orientation       = _T("Orientation");
static LPCTSTR lpctstrTag_PageName          = _T("PageName");
static LPCTSTR lpctstrTag_PageNumber        = _T("PageNumber");
static LPCTSTR lpctstrTag_PhotometricInterpretation = _T("PhotometricInterpretation");
static LPCTSTR lpctstrTag_PlanarConfiguration = _T("PlanarConfiguration");
static LPCTSTR lpctstrTag_Predictor         = _T("Predictor");
static LPCTSTR lpctstrTag_PrimaryChromaticities = _T("PrimaryChromaticities");
static LPCTSTR lpctstrTag_ReferenceBlackWhite = _T("ReferenceBlackWhite");
static LPCTSTR lpctstrTag_ResolutionUnit    = _T("ResolutionUnit");
static LPCTSTR lpctstrTag_RowsPerStrip      = _T("RowsPerStrip");
static LPCTSTR lpctstrTag_SampleFormat      = _T("SampleFormat");
static LPCTSTR lpctstrTag_SamplesPerPixel   = _T("SamplesPerPixel");
static LPCTSTR lpctstrTag_SMaxSampleValue   = _T("SMaxSampleValue");
static LPCTSTR lpctstrTag_SMinSampleValue   = _T("SMinSampleValue");
static LPCTSTR lpctstrTag_Software          = _T("Software");
static LPCTSTR lpctstrTag_StripByteCounts   = _T("StripByteCounts");
static LPCTSTR lpctstrTag_StripOffsets      = _T("StripOffsets");
static LPCTSTR lpctstrTag_SubfileType       = _T("SubfileType");
static LPCTSTR lpctstrTag_T4Options         = _T("T4Options");
static LPCTSTR lpctstrTag_T6Options         = _T("T6Options");
static LPCTSTR lpctstrTag_TargetPrinter     = _T("TargetPrinter");
static LPCTSTR lpctstrTag_Threshholding     = _T("Threshholding");
static LPCTSTR lpctstrTag_TileByteCounts    = _T("TileByteCounts");
static LPCTSTR lpctstrTag_TileLength        = _T("TileLength");
static LPCTSTR lpctstrTag_TileOffsets       = _T("TileOffsets");
static LPCTSTR lpctstrTag_TileWidth         = _T("TileWidth");
static LPCTSTR lpctstrTag_TransferFunction  = _T("TransferFunction");
static LPCTSTR lpctstrTag_TransferRange     = _T("TransferRange");
static LPCTSTR lpctstrTag_WhitePoint        = _T("WhitePoint");
static LPCTSTR lpctstrTag_XPosition         = _T("XPosition");
static LPCTSTR lpctstrTag_XResolution       = _T("XResolution");
static LPCTSTR lpctstrTag_YCbCrCoefficients = _T("YCbCrCoefficients");
static LPCTSTR lpctstrTag_YCbCrPositioning  = _T("YCbCrPositioning");
static LPCTSTR lpctstrTag_YCbCrSubSampling  = _T("YCbCrSubSampling");
static LPCTSTR lpctstrTag_YPosition         = _T("YPosition");
static LPCTSTR lpctstrTag_YResolution       = _T("YResolution");
static LPCTSTR lpctstrTag_Default           = _T("Unknown");
static LPCTSTR lpctstrListBoxHeadingFormat  = _T("Tag #\t\tType\t\tCount\tData\0");
static LPCTSTR lpctstrListBoxDataFormat     = _T("  %1!s!\t\t%2!s!\t\t%3!d!\t%4!s!\0");
static LPCTSTR lpctstrPropertyLevelFormat   = _T("Property Level: %1!d!");
static LPCTSTR lpctstrParsedFormat          = _T("%1!s!");
static LPCTSTR lpctstrParsedRawFormat       = _T("%1!u! / %2!u!");

//
// FUNCTIONS
//

CEXIFTagsDlg::CEXIFTagsDlg(
    CWnd* pParent /*=NULL*/)
	: CDialog(CEXIFTagsDlg::IDD, pParent)
    , m_ppCImagXpress(NULL)
{
	//{{AFX_DATA_INIT(CEXIFTagsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void 
CEXIFTagsDlg::DoDataExchange(
    CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEXIFTagsDlg)
	DDX_Control(pDX, IDC_LIST_EXIFTAGS, m_ctlExifTagsList);
	//}}AFX_DATA_MAP
}

BOOL 
CEXIFTagsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

    // Set dialog description text
    //
    CWnd* pControl = NULL;
    pControl = GetDlgItem(IDC_LBL_DESCRIPTION);
    if (pControl)
    {
        pControl->SetWindowText(lpctstrDescription);
    }

    // Calculate dimensions of ImagXpress COM control client area.
    //
    CRect oIXArea;
    oIXArea.left = 216;
    oIXArea.top = 175;
    oIXArea.right = 525;
    oIXArea.bottom = 356;

    // Create an ImagXpress Object
    //
    HINSTANCE hDLL = IX_Open();
    m_ppCImagXpress = new CImagXpress((DWORD) this, 1, (long) m_hWnd, 
        oIXArea.left, oIXArea.top, oIXArea.Width(), oIXArea.Height());
    m_pImagXpress = m_ppCImagXpress->pImagXpress;
	m_pImagXpress->MenuSetEnabled(Menu_Context, TOOL_None,false);
	
	m_pImagXpress->PutCtlVisible(false);
	m_pImagXpress->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\EXIF.jpg";




    IX_Close(hDLL);
    
    // Disable buttons
    //
    pControl = GetDlgItem(IDC_BTN_ADDACOPYRIGHTTAG);
    if (pControl)
        pControl->EnableWindow(FALSE);
    pControl = GetDlgItem(IDC_BTN_DELETECOPYRIGHTTAG);
    if (pControl)
        pControl->EnableWindow(FALSE);
    pControl = GetDlgItem(IDC_BTN_MODIFYRESOLUTIONTAGS);
    if (pControl)
        pControl->EnableWindow(FALSE);
	
    pControl = GetDlgItem(IDC_BTN_LOADEXIFTAGS);
    if (pControl)
        pControl->EnableWindow(TRUE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void 
CEXIFTagsDlg::OnDestroy()
{
    CDialog::OnDestroy();
    if (m_pImagXpress)
    {
        m_pImagXpress = NULL;
        if (m_ppCImagXpress)
        {
            delete m_ppCImagXpress;
        }
    }
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void 
CEXIFTagsDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

/**
 *  The system calls this to obtain the cursor to display while the user drags
 *  the minimized window.
 */
HCURSOR 
CEXIFTagsDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CEXIFTagsDlg::OnBtnLoadexiftags() 
{
  
    if (m_pImagXpress->ImagError != 0)
    {
        CString strMessage;
        strMessage.FormatMessage(lpctstrLoadFailureFormat, 
            m_pImagXpress->ImagError);
        MessageBox(strMessage, NULL, MB_OK | MB_ICONERROR);
    }
    else
    {
        CWnd* pControl = NULL;
        pControl = GetDlgItem(IDC_BTN_ADDACOPYRIGHTTAG);
        if (pControl)    
            pControl->EnableWindow(TRUE);
        pControl = GetDlgItem(IDC_BTN_MODIFYRESOLUTIONTAGS);
        if (pControl)    
            pControl->EnableWindow(TRUE);

		//****get the Tags here
        getExifTags(m_pImagXpress, m_ctlExifTagsList);
    }
}

void 
CEXIFTagsDlg::OnBtnAddacopyrighttag() 
{
    CString strCopyright(lpctstrCopyrightString);
    int i = 0;
    for (i = 0; i <= 3; i++)
    {
        m_pImagXpress->TagLevel = i;
        m_pImagXpress->TagAdd(307, TAG_ASCII, 
            strCopyright.GetLength(), strCopyright.AllocSysString());
    }
    getExifTags(m_pImagXpress, m_ctlExifTagsList);
    CWnd* pControl = GetDlgItem(IDC_BTN_DELETECOPYRIGHTTAG);
    if (pControl)
        pControl->EnableWindow(TRUE);
}

void 
CEXIFTagsDlg::OnBtnModifyresolutiontags() 
{
    int i = 0;
    int aiResolutionData[2] = {100, 1};
    for (i = 0; i <= 3; i++)
    {
        SAFEARRAYBOUND rgsabound[1];        
        rgsabound[0].lLbound    = 0;
        rgsabound[0].cElements  = 2;
        tagSAFEARRAY* resolution = SafeArrayCreate(VT_I4, 1, rgsabound);
        if (resolution)
        {
            resolution->pvData = (void*) aiResolutionData;

            VARIANT varResolution;
            varResolution.vt = VT_ARRAY | VT_I4;
            varResolution.parray = resolution;
        
            m_pImagXpress->TagLevel = i;
            m_pImagXpress->TagFind(TAG_XResolution);
            m_pImagXpress->TagModify(TAG_XResolution, TAG_RATIONAL, 1, varResolution);
            m_pImagXpress->TagFind(TAG_YResolution);
            m_pImagXpress->TagModify(TAG_YResolution, TAG_RATIONAL, 1, varResolution);
        }
    }
    getExifTags(m_pImagXpress, m_ctlExifTagsList);
}

void 
CEXIFTagsDlg::OnBtnDeletecopyrighttag() 
{
    int i = 0;
    for (i = 0; i <= 3; i++)
    {
        m_pImagXpress->TagLevel = i;
        m_pImagXpress->TagDelete(307);
    }
    getExifTags(m_pImagXpress, m_ctlExifTagsList);
}

void 
CEXIFTagsDlg::OnBtnSettags() 
{
    const int k_iPageNumber = 1;
    CString strInputFilePath;
    getInputFilePath(strInputFilePath);
    m_pImagXpress->FileSetTags(strInputFilePath.AllocSysString(), 
        k_iPageNumber);
    m_pImagXpress->FileGetTags(strInputFilePath.AllocSysString(), 
        k_iPageNumber);
    if (m_pImagXpress->ImagError != 0)
    {
        CString strMessage;
        strMessage.FormatMessage(lpctstrLoadFailureFormat, 
            m_pImagXpress->ImagError);
        MessageBox(strMessage, NULL, MB_OK | MB_ICONERROR);
    }

    CWnd* pControl = GetDlgItem(IDC_BTN_MODIFYRESOLUTIONTAGS);
    if (pControl)    
        pControl->EnableWindow(TRUE);
    getExifTags(m_pImagXpress, m_ctlExifTagsList);
}

void 
CEXIFTagsDlg::OnBtnExit() 
{
    EndDialog(IDOK);
}

void        
CEXIFTagsDlg::getInputFilePath(
    CString& strFilePath) const
{
    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];
    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
    strFilePath = CString(drive) + CString(dir) 
        + lpctstrMediaDirectory + lpctstrDefaultFileName;
}

void        
CEXIFTagsDlg::getTagDescription(
    const EExifTagNumber eExifTagNumber, 
    CString& strDescription)
{
    switch (eExifTagNumber)
    {
    case TAG_ExifPixYDim: 
        strDescription = lpctstrTag_ExifPixYDim; 
        break;
    case TAG_Artist: 
        strDescription = lpctstrTag_Artist; 
        break;
    case TAG_BitsPerSample: 
        strDescription = lpctstrTag_BitsPerSample; 
        break;
    case TAG_CellLength:
        strDescription = lpctstrTag_CellLength; 
        break;
    case TAG_CellWidth:
        strDescription = lpctstrTag_CellWidth; 
        break;
    case TAG_ColorMap:
        strDescription = lpctstrTag_ColorMap; 
        break;
    case TAG_Compression:
        strDescription = lpctstrTag_Compression; 
        break;
    case TAG_Copyright:
        strDescription = lpctstrTag_Copyright; 
        break;
    case TAG_DateTime:
        strDescription = lpctstrTag_DateTime; 
        break;
    case TAG_DocumentName:
        strDescription = lpctstrTag_DocumentName; 
        break;
    case TAG_DotRange:
        strDescription = lpctstrTag_DotRange; 
        break;
    case TAG_ExifAperture:
        strDescription = lpctstrTag_ExifAperture; 
        break;
    case TAG_ExifBrightness:
        strDescription = lpctstrTag_ExifBrightness; 
        break;
    case TAG_ExifCfaPattern:
        strDescription = lpctstrTag_ExifCfaPattern; 
        break;
    case TAG_ExifColorSpace:
        strDescription = lpctstrTag_ExifColorSpace; 
        break;
    case TAG_ExifCompBPP:
        strDescription = lpctstrTag_ExifCompBPP; 
        break;
    case TAG_ExifCompConfig:
        strDescription = lpctstrTag_ExifCompConfig; 
        break;
    case TAG_ExifDTDigitized:
        strDescription = lpctstrTag_ExifDTDigitized; 
        break;
    case TAG_ExifDTDigSS:
        strDescription = lpctstrTag_ExifDTDigSS; 
        break;
    case TAG_ExifDTOrig:
        strDescription = lpctstrTag_ExifDTOrig; 
        break;
    case TAG_ExifDTOrigSS:
        strDescription = lpctstrTag_ExifDTOrigSS; 
        break;
    case TAG_ExifDTSubsec:
        strDescription = lpctstrTag_ExifDTSubsec; 
        break;
    case TAG_ExifExposureBias:
        strDescription = lpctstrTag_ExifExposureBias; 
        break;
    case TAG_ExifExposureIndex:
        strDescription = lpctstrTag_ExifExposureIndex; 
        break;
    case TAG_ExifExposureProg:
        strDescription = lpctstrTag_ExifExposureProg; 
        break;
    case TAG_ExifExposureTime:
        strDescription = lpctstrTag_ExifExposureTime; 
        break;
    case TAG_ExifFileSource:
        strDescription = lpctstrTag_ExifFileSource; 
        break;
    case TAG_ExifFlash:
        strDescription = lpctstrTag_ExifFlash; 
        break;
    case TAG_ExifFlashEnergy:
        strDescription = lpctstrTag_ExifFlashEnergy; 
        break;
    case TAG_ExifFNumber:
        strDescription = lpctstrTag_ExifFNumber; 
        break;
    case TAG_ExifFocalLength:
        strDescription = lpctstrTag_ExifFocalLength; 
        break;
    case TAG_ExifFocalResUnit:
        strDescription = lpctstrTag_ExifFocalResUnit; 
        break;
    case TAG_ExifFocalXRes:
        strDescription = lpctstrTag_ExifFocalXRes; 
        break;
    case TAG_ExifFocalYRes:
        strDescription = lpctstrTag_ExifFocalYRes; 
        break;
    case TAG_ExifFPXVer:
        strDescription = lpctstrTag_ExifFPXVer; 
        break;
    case TAG_ExifImageHistory:
        strDescription = lpctstrTag_ExifImageHistory; 
        break;
    case TAG_ExifImageNumber:
        strDescription = lpctstrTag_ExifImageNumber; 
        break;
    case TAG_ExifInterlace:
        strDescription = lpctstrTag_ExifInterlace; 
        break;
    case TAG_ExifInterop:
        strDescription = lpctstrTag_ExifInterop; 
        break;
    case TAG_ExifISOSpeed:
        strDescription = lpctstrTag_ExifISOSpeed; 
        break;
    case TAG_ExifLightSource:
        strDescription = lpctstrTag_ExifLightSource; 
        break;
    case TAG_ExifMakerNote:
        strDescription = lpctstrTag_ExifMakerNote; 
        break;
    case TAG_ExifMaxAperture:
        strDescription = lpctstrTag_ExifMaxAperture; 
        break;
    case TAG_ExifMeteringMode:
        strDescription = lpctstrTag_ExifMeteringMode; 
        break;
    case TAG_ExifNoise:
        strDescription = lpctstrTag_ExifNoise; 
        break;
    case TAG_ExifOECF: strDescription = lpctstrTag_ExifOECF; 
        break;
    case TAG_ExifPixXDim:
        strDescription = lpctstrTag_ExifPixXDim; 
        break;
    case TAG_ExifRelatedWav:
        strDescription = lpctstrTag_ExifRelatedWav; 
        break;
    case TAG_ExifSceneType:
        strDescription = lpctstrTag_ExifSceneType; 
        break;
    case TAG_ExifSecurityClassification:
        strDescription = lpctstrTag_ExifSecurityClassification; 
        break;
    case TAG_ExifSelfTimerMode:
        strDescription = lpctstrTag_ExifSelfTimerMode; 
        break;
    case TAG_ExifSensingMethod:
        strDescription = lpctstrTag_ExifSensingMethod; 
        break;
    case TAG_ExifShutterSpeed:
        strDescription = lpctstrTag_ExifShutterSpeed; 
        break;
    case TAG_ExifSpatialFR:
        strDescription = lpctstrTag_ExifSpatialFR; 
        break;
    case TAG_ExifSpectralSense:
        strDescription = lpctstrTag_ExifSpectralSense; 
        break;
    case TAG_ExifSubjectDist:
        strDescription = lpctstrTag_ExifSubjectDist; 
        break;
    case TAG_ExifSubjectLoc:
        strDescription = lpctstrTag_ExifSubjectLoc; 
        break;
    case TAG_ExifTIFF_EPStandardID:
        strDescription = lpctstrTag_ExifTIFF_EPStandardID; 
        break;
    case TAG_ExifTimeZoneOffset:
        strDescription = lpctstrTag_ExifTimeZoneOffset; 
        break;
    case TAG_ExifUserComment:
        strDescription = lpctstrTag_ExifUserComment; 
        break;
    case TAG_ExifVer:
        strDescription = lpctstrTag_ExifVer; 
        break;
    case TAG_ExtraSamples:
        strDescription = lpctstrTag_ExtraSamples; 
        break;
    case TAG_FillOrder:
        strDescription = lpctstrTag_FillOrder; 
        break;
    case TAG_FreeByteCounts:
        strDescription = lpctstrTag_FreeByteCounts; 
        break;
    case TAG_FreeOffsets:
        strDescription = lpctstrTag_FreeOffsets; 
        break;
    case TAG_GrayResponseCurve:
        strDescription = lpctstrTag_GrayResponseCurve; 
        break;
    case TAG_GrayResponseUnit:
        strDescription = lpctstrTag_GrayResponseUnit; 
        break;
    case TAG_HalftoneHints:
        strDescription = lpctstrTag_HalftoneHints; 
        break;
    case TAG_HostComputer:
        strDescription = lpctstrTag_HostComputer; 
        break;
    case TAG_ImageDescription:
        strDescription = lpctstrTag_ImageDescription; 
        break;
    case TAG_ImageLength:
        strDescription = lpctstrTag_ImageLength; 
        break;
    case TAG_ImageWidth:
        strDescription = lpctstrTag_ImageWidth; 
        break;
    case TAG_InkNames:
        strDescription = lpctstrTag_InkNames; 
        break;
    case TAG_InkSet:
        strDescription = lpctstrTag_InkSet; 
        break;
    case TAG_JPEGACTables:
        strDescription = lpctstrTag_JPEGACTables; 
        break;
//    case TAG_JPEGDCTables:
//        strDescription = lpctstrTag_JPEGDCTables; 
//        break;
    case TAG_JPEGDCTTables:
        strDescription = lpctstrTag_JPEGDCTTables; 
        break;
    case TAG_JPEGInterchangeFormat:
        strDescription = lpctstrTag_JPEGInterchangeFormat; 
        break;
    case TAG_JPEGInterchangeFormatLength:
        strDescription = lpctstrTag_JPEGInterchangeFormatLength; 
        break;
    case TAG_JPEGLosslessPredictors:
        strDescription = lpctstrTag_JPEGLosslessPredictors; 
        break;
    case TAG_JPEGPointTransforms:
        strDescription = lpctstrTag_JPEGPointTransforms; 
        break;
    case TAG_JPEGProc:
        strDescription = lpctstrTag_JPEGProc; 
        break;
    case TAG_JPEGQTables:
        strDescription = lpctstrTag_JPEGQTables; 
        break;
    case TAG_JPEGRestartInterval:
        strDescription = lpctstrTag_JPEGRestartInterval; 
        break;
    case TAG_Make:
        strDescription = lpctstrTag_Make; 
        break;
    case TAG_MaxSampleValue:
        strDescription = lpctstrTag_MaxSampleValue; 
        break;
    case TAG_MinSampleValue:
        strDescription = lpctstrTag_MinSampleValue; 
        break;
    case TAG_Model:
        strDescription = lpctstrTag_Model; 
        break;
    case TAG_NewSubfileType:
        strDescription = lpctstrTag_NewSubfileType; 
        break;
    case TAG_NumberOfInks:
        strDescription = lpctstrTag_NumberOfInks; 
        break;
    case TAG_Orientation:
        strDescription = lpctstrTag_Orientation; 
        break;
    case TAG_PageName:
        strDescription = lpctstrTag_PageName; 
        break;
    case TAG_PageNumber:
        strDescription = lpctstrTag_PageNumber; 
        break;
    case TAG_PhotometricInterpretation:
        strDescription = lpctstrTag_PhotometricInterpretation; 
        break;
    case TAG_PlanarConfiguration:
        strDescription = lpctstrTag_PlanarConfiguration; 
        break;
    case TAG_Predictor:
        strDescription = lpctstrTag_Predictor; 
        break;
    case TAG_PrimaryChromaticities:
        strDescription = lpctstrTag_PrimaryChromaticities; 
        break;
    case TAG_ReferenceBlackWhite:
        strDescription = lpctstrTag_ReferenceBlackWhite; 
        break;
    case TAG_ResolutionUnit:
        strDescription = lpctstrTag_ResolutionUnit;
        break;
    case TAG_RowsPerStrip:
        strDescription = lpctstrTag_RowsPerStrip; 
        break;
    case TAG_SampleFormat:
        strDescription = lpctstrTag_SampleFormat; 
        break;
    case TAG_SamplesPerPixel:
        strDescription = lpctstrTag_SamplesPerPixel; 
        break;
    case TAG_SMaxSampleValue: 
        strDescription = lpctstrTag_SMaxSampleValue; 
        break;
    case TAG_SMinSampleValue:
        strDescription = lpctstrTag_SMinSampleValue; 
        break;
    case TAG_Software:
        strDescription = lpctstrTag_Software; 
        break;
    case TAG_StripByteCounts:
        strDescription = lpctstrTag_StripByteCounts; 
        break;
    case TAG_StripOffsets:
        strDescription = lpctstrTag_StripOffsets; 
        break;
    case TAG_SubfileType:
        strDescription = lpctstrTag_SubfileType; 
        break;
    case TAG_T4Options:
        strDescription = lpctstrTag_T4Options; 
        break;
    case TAG_T6Options:
        strDescription = lpctstrTag_T6Options; 
        break;
    case TAG_TargetPrinter:
        strDescription = lpctstrTag_TargetPrinter; 
        break;
    case TAG_Threshholding:
        strDescription = lpctstrTag_Threshholding; 
        break;
    case TAG_TileByteCounts:
        strDescription = lpctstrTag_TileByteCounts; 
        break;
    case TAG_TileLength:
        strDescription = lpctstrTag_TileLength; 
        break;
    case TAG_TileOffsets:
        strDescription = lpctstrTag_TileOffsets; 
        break;
    case TAG_TileWidth:
        strDescription = lpctstrTag_TileWidth; 
        break;
    case TAG_TransferFunction:
        strDescription = lpctstrTag_TransferFunction; 
        break;
    case TAG_TransferRange:
        strDescription = lpctstrTag_TransferRange; 
        break;
    case TAG_WhitePoint:
        strDescription = lpctstrTag_WhitePoint; 
        break;
    case TAG_XPosition: 
        strDescription = lpctstrTag_XPosition; 
        break;
    case TAG_XResolution:
        strDescription = lpctstrTag_XResolution; 
        break;
    case TAG_YCbCrCoefficients:
        strDescription = lpctstrTag_YCbCrCoefficients; 
        break;
    case TAG_YCbCrPositioning: 
        strDescription = lpctstrTag_YCbCrPositioning; 
        break;
    case TAG_YCbCrSubSampling:
        strDescription = lpctstrTag_YCbCrSubSampling; 
        break;
    case TAG_YPosition:
        strDescription = lpctstrTag_YPosition; 
        break;
    case TAG_YResolution:
        strDescription = lpctstrTag_YResolution; 
        break;
    default: 
        strDescription = lpctstrTag_Default; 
        break;
    }
}
    
void        
CEXIFTagsDlg::getTagTypeDescription(
    const EExifTagType eTagType, 
    CString& strDescription)
{
    switch (eTagType)
    {
    case TAG_BYTE: strDescription = lpctstrTagType_Byte;
        break;
    case TAG_ASCII:
        strDescription = lpctstrTagType_Asciichar;
        break;
    case TAG_SHORT:
        strDescription = lpctstrTagType_Short;
        break;
    case TAG_LONG:
        strDescription = lpctstrTagType_Long;
        break;
    case TAG_RATIONAL:
        strDescription = lpctstrTagType_Rational;
        break;
    case TAG_SBYTE:
        strDescription = lpctstrTagType_Signedbyte;
        break;
    case TAG_UNDEFINE:
        strDescription = lpctstrTagType_8bitbyte;
        break;
    case TAG_SSHORT:
        strDescription = lpctstrTagType_SignedShort;
        break;
    case TAG_SLONG: 
        strDescription = lpctstrTagType_SignedLong;
        break;
    case TAG_SRATIONAL:
        strDescription = lpctstrTagType_SignedRational;
        break;
    case TAG_FLOAT:
        strDescription = lpctstrTagType_Float;
        break;
    case TAG_DOUBLE:
        strDescription = lpctstrTagType_Double;
        break;
    default:
        strDescription = lpctstrTagType_Unknown;
    }
}

void
CEXIFTagsDlg::getExifTags(
    IImagXpressPtr& IX, 
    CListBox& ctlExifListBox)
{
    ctlExifListBox.ResetContent();
    ctlExifListBox.AddString(lpctstrListBoxHeadingFormat);
    int i = 0;
    for (i = 0; i <= 3; i++)
    {
        IX->TagLevel = i;
        CString strListBoxItem;
        CString strTagLevel;
        strTagLevel.FormatMessage(lpctstrPropertyLevelFormat, IX->TagLevel);
        ctlExifListBox.AddString(strTagLevel);
        IX->TagFirst();
        
        while (IX->ImagError == 0)
        {
            int j = 0;
            int iExifNum = 0; 
            int iExifCount = 0;
            CString strExifTagLabel;
            CString strExifTypeDescription;
            CString strExifDataParsed;
            int aiExifDataRaw[2];

            iExifNum = IX->TagNumber;
            iExifCount = IX->TagCount;          
            for (j = 1; j <= iExifCount; j++)
            {
                CString strExifData;
                EExifTagType eExifTagType = (EExifTagType) IX->TagType;
                switch (eExifTagType)
                {
                case TAG_SBYTE:
                case TAG_BYTE: 
                case TAG_UNDEFINE:
                    strExifData.Format(_T("0x%02X "), (BYTE) IX->TagGetDataItem(j));
                    break;
                case TAG_ASCII:
                    strExifData.Format(_T("%s"), 
                        CString(IX->TagGetDataItem(j).bstrVal));
                    j = iExifCount;
                    break;
                case TAG_SHORT:
                    strExifData.Format(_T("%d "), (short) IX->TagGetDataItem(j));
                    break;
                case TAG_LONG:
                    strExifData.Format(_T("%d "), (long) IX->TagGetDataItem(j));
                    break;
                case TAG_SSHORT:
                    strExifData.Format(_T("%u "), (short) IX->TagGetDataItem(j));
                    break;
                case TAG_SLONG:
                    strExifData.Format(_T("%u "), (long) IX->TagGetDataItem(j));
                    break;
                case TAG_RATIONAL:
                case TAG_SRATIONAL:
                    aiExifDataRaw[0] = ((int*) (IX->TagGetDataItem(j).parray->pvData))[0];
                    aiExifDataRaw[1] = ((int*) (IX->TagGetDataItem(j).parray->pvData))[1];
                    strExifData.FormatMessage(lpctstrParsedRawFormat, aiExifDataRaw[0], 
                        aiExifDataRaw[1]);
                    break;
                case TAG_FLOAT:
                    strExifData.Format(_T("t%f "), (float) IX->TagGetDataItem(j));
                    break;
                case TAG_DOUBLE:
                    strExifData.Format(_T("%lf "), (float) IX->TagGetDataItem(j));
                    break;
                }
                strExifDataParsed += strExifData;
            }

            getTagTypeDescription((EExifTagType) IX->TagType, 
                strExifTypeDescription);
            strExifTagLabel.Format(_T("%d"), iExifNum);

            if (strExifDataParsed.GetLength() > 60)
                strExifDataParsed = strExifDataParsed.Left(60) + CString("...");

            strListBoxItem.FormatMessage(lpctstrListBoxDataFormat,
                strExifTagLabel,  
                strExifTypeDescription, iExifCount, 
                strExifDataParsed);
            ctlExifListBox.AddString(strListBoxItem);  
            
            IX->TagNext();
			
        }
    }
}

void CEXIFTagsDlg::OnFile() 
{
//***open file code here	
  CFileDialog ofd(TRUE);
 
  
    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];

    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
    CString strInitialDirectory = CString(drive) + CString(dir) 
        + szMediaDirectory;
	
    ofd.m_ofn.lpstrInitialDir   = strInitialDirectory;
    ofd.m_ofn.lpstrTitle        = szFileTitle;
    ofd.m_ofn.lpstrFilter      = szFilter;

    if (ofd.DoModal() == IDOK)
   {
        CWaitCursor oHourglass;

       m_pImagXpress->FileGetTags(ofd.m_ofn.lpstrFile,1);
	    CWnd*  pControl = GetDlgItem(IDC_BTN_LOADEXIFTAGS);
       if (pControl)    
            pControl->EnableWindow(TRUE);
    }  
  		
}

void CEXIFTagsDlg::OnFileExit() 
{
	EndDialog(IDOK);	
}

void CEXIFTagsDlg::OnHelpAbout() 
{
	
		m_pImagXpress->AboutBox();

}

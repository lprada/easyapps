// MergingImagesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MergingImages.h"
#include "MergingImagesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Include ImagXpress Initialization functions
#include "..\Include\ix8_open.cpp"

/////////////////////////////////////////////////////////////////////////////
// CMergingImagesDlg dialog

CMergingImagesDlg::CMergingImagesDlg(CWnd* pParent /*=NULL*/)
: CDialog(CMergingImagesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMergingImagesDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMergingImagesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMergingImagesDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMergingImagesDlg, CDialog)
//{{AFX_MSG_MAP(CMergingImagesDlg)
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(IDC_QUIT, OnQuit)
ON_WM_DESTROY()
ON_BN_CLICKED(IDC_MERGEIMAGES, OnMergeimages)
	ON_COMMAND(ID_FILE_EXIT, OnFileExit)
	ON_COMMAND(IDHELP, OnHelp)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMergingImagesDlg message handlers

BOOL CMergingImagesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// ImagXpress initialization
	HINSTANCE hDLL = IX_Open();
	
	// Create an ImagXpress Visible object
	ppCImagXpress1 = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 30, 50, 400, 325);
	pImagXpress1 = ppCImagXpress1->pImagXpress;
	pImagXpress1->AutoSize = ISIZE_BestFit;
	pImagXpress1->put_ScrollBars(SB_Both );
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\bird.jpg";

	
	
	// ImagXpress uninitialize
	IX_Close(hDLL);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMergingImagesDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMergingImagesDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CMergingImagesDlg::OnQuit() 
{
	EndDialog(IDOK);	
}

void CMergingImagesDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// Delete the ImagXpress Object
	if (pImagXpress1)
	{	
		pImagXpress1 = NULL;
		if (ppCImagXpress1) 
			delete ppCImagXpress1;
	}
}

void CMergingImagesDlg::OnMergeimages() 
{
	
	pImagXpress1->Merge(TRUE, MSZ_ResizeArea, MST_Normal, TRUE, RGB(255,255,255), 0, 0 );
	
	pImagXpress1->Area(TRUE, 100, 120, 100, 100);
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\ball1.bmp";
	
	pImagXpress1->Area(TRUE, 10, 10, 100, 100);
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\ball1.bmp";
	
	pImagXpress1->Area(TRUE, 200, 10, 100, 100);
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\ball1.bmp";
	
	pImagXpress1->Area(TRUE, 10, 150, 100, 100);
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\ball1.bmp";
	
	pImagXpress1->Area(TRUE, 280, 170, 100, 100);
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\ball1.bmp";
	
	pImagXpress1->Area(TRUE, 250, 100, 100, 100);
	pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\ball1.bmp";
	
	pImagXpress1->Merge(FALSE, MSZ_ResizeArea, MST_Normal, TRUE, RGB(255,255,255), 0, 0 );
	
	pImagXpress1->Area(FALSE, 0, 0, 0, 0);
	
}

void CMergingImagesDlg::OnFileExit() 
{
	EndDialog(IDOK);
	
}

void CMergingImagesDlg::OnHelp() 
{
		pImagXpress1->AboutBox();
	
}

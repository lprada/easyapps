VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress - JPEG Quality / Save to Buffer Sample"
   ClientHeight    =   8655
   ClientLeft      =   4605
   ClientTop       =   1920
   ClientWidth     =   10215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   10215
   ShowInTaskbar   =   0   'False
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressHidden 
      Height          =   1095
      Left            =   6120
      TabIndex        =   19
      Top             =   4320
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   1931
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   603548493
      ErrInfo         =   1538847897
      Persistence     =   -1  'True
      _cx             =   1085
      _cy             =   1931
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressShow 
      Height          =   3735
      Left            =   600
      TabIndex        =   18
      Top             =   1800
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   6588
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   603548493
      ErrInfo         =   1538847897
      Persistence     =   -1  'True
      _cx             =   9340
      _cy             =   6588
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.ListBox lstStatus 
      Height          =   2400
      Left            =   6960
      TabIndex        =   17
      Top             =   2040
      Width           =   3015
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "JPGCompression.frx":0000
      Left            =   600
      List            =   "JPGCompression.frx":0010
      TabIndex        =   15
      Top             =   360
      Width           =   9375
   End
   Begin VB.Frame Frame1 
      Caption         =   "Sub Sampling"
      Height          =   855
      Left            =   480
      TabIndex        =   6
      Top             =   6960
      Width           =   5535
      Begin VB.OptionButton Opt211v 
         Caption         =   "2: 1: 1 v"
         Height          =   255
         Left            =   4320
         TabIndex        =   10
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton Opt411 
         Caption         =   "4: 1: 1"
         Height          =   255
         Left            =   2880
         TabIndex        =   9
         Top             =   360
         Width           =   855
      End
      Begin VB.OptionButton Opt211 
         Caption         =   "2: 1: 1"
         Height          =   255
         Left            =   1560
         TabIndex        =   8
         Top             =   360
         Width           =   855
      End
      Begin VB.OptionButton Opt111 
         Caption         =   "1: 1: 1"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.HScrollBar HSLumin 
      Height          =   375
      LargeChange     =   10
      Left            =   1200
      Max             =   255
      Min             =   1
      TabIndex        =   1
      Top             =   6360
      Value           =   1
      Width           =   4815
   End
   Begin VB.HScrollBar HSChrom 
      Height          =   375
      LargeChange     =   10
      Left            =   1200
      Max             =   255
      Min             =   1
      TabIndex        =   0
      Top             =   5880
      Value           =   1
      Width           =   4815
   End
   Begin VB.Label Label4 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   6960
      TabIndex        =   16
      Top             =   1560
      Width           =   2055
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   255
      Left            =   6960
      TabIndex        =   14
      Top             =   4560
      Width           =   2295
   End
   Begin VB.Label lblError 
      Height          =   2055
      Left            =   6960
      TabIndex        =   13
      Top             =   5040
      Width           =   3015
   End
   Begin VB.Label Label3 
      Caption         =   "Compression Ratio:"
      Height          =   375
      Left            =   480
      TabIndex        =   12
      Top             =   8040
      Width           =   1455
   End
   Begin VB.Label lblShowCR 
      Caption         =   "lblShowCR"
      Height          =   375
      Left            =   2040
      TabIndex        =   11
      Top             =   8040
      Width           =   1695
   End
   Begin VB.Label lblLumin 
      Height          =   375
      Left            =   6120
      TabIndex        =   5
      Top             =   6360
      Width           =   495
   End
   Begin VB.Label lblChrom 
      Height          =   375
      Left            =   6120
      TabIndex        =   4
      Top             =   5880
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "Lum."
      Height          =   375
      Left            =   480
      TabIndex        =   3
      Top             =   6360
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Chrom."
      Height          =   375
      Left            =   480
      TabIndex        =   2
      Top             =   5880
      Width           =   615
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim bLoading  ' Set True during form load
Dim CompressedSize As Long

Sub UpdateImage()
  Dim hBuffer As Long
  Dim FSize As Long
  
  ' 1. Set the slider values
  lblChrom.Caption = HSChrom.Value
  XpressHidden.SaveJPEGChromFactor = HSChrom.Value
  lblLumin.Caption = HSLumin.Value
  XpressHidden.SaveJPEGLumFactor = HSLumin.Value
  
  ' 2. Save the file to memory
  XpressHidden.SaveToBuffer = True
  XpressHidden.SaveFileType = FT_JPEG
  XpressHidden.SaveFile
  
  ' 3. Get the Buffer Handle
  hBuffer = XpressHidden.SaveBufferHandle
  
  ' 4. Load the Blob data from the buffer into the control to prove that it works
  XpressShow.LoadBuffer hBuffer
  
  ' 5. Compute the compression ratio
  CompressedSize = GlobalSize(hBuffer)
  FSize = XpressHidden.IWidth
  FSize = FSize * XpressHidden.IHeight * 3
  If (CompressedSize > 0) Then
    lblShowCR.Caption = Format(FSize / CompressedSize, "##0.0 : 1")
  Else
    lblShowCR.Caption = "N/A"
  End If
  
  ' 6. Delete the Buffer
  XpressHidden.DeleteSaveBuffer
  
End Sub


Private Sub cmdQuit_Click()
  End
End Sub


Private Sub Form_Load()
  Dim desc As String
    
  XpressShow.EventSetEnabled EVENT_PROGRESS, True
    
  bLoading = True
  lblChrom.Caption = XpressHidden.SaveJPEGChromFactor
  lblLumin.Caption = XpressHidden.SaveJPEGLumFactor
  HSChrom.Value = XpressHidden.SaveJPEGChromFactor
  HSLumin.Value = XpressHidden.SaveJPEGLumFactor
  XpressShow.AutoSize = ISIZE_BestFit
  Opt411.Value = True
  XpressHidden.Visible = False
  
  Err = XpressHidden.ImagError
  PegasusError Err, lblError
  
  imgFileName = App.Path & "\..\..\..\..\..\..\Common\Images\window.jpg"
  
  LoadFile
   
  bLoading = False
 
End Sub

Private Sub XpressShow_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub HSChrom_Change()
  If Not bLoading Then UpdateImage
End Sub

Private Sub HSLumin_Change()
  If Not bLoading Then UpdateImage
End Sub

Private Sub ImagXpress1_Click()

End Sub

Private Sub Opt111_Click()
  XpressHidden.SaveJPEGSubSampling = SS_111
  UpdateImage
End Sub

Private Sub Opt211_Click()
  XpressHidden.SaveJPEGSubSampling = SS_211
  UpdateImage
End Sub

Private Sub Opt211v_Click()
  If Not bLoading Then
    XpressHidden.SaveJPEGSubSampling = SS_211v
    UpdateImage
  End If
End Sub

Private Sub Opt411_Click()
  If Not bLoading Then
    XpressHidden.SaveJPEGSubSampling = SS_411
    UpdateImage
  End If
End Sub

Private Sub LoadFile()
    XpressHidden.FileName = imgFileName
        
   Err = XpressHidden.ImagError
   PegasusError Err, lblError
   
    UpdateImage
End Sub
Private Sub mnuAbout_Click()
    XpressShow.AboutBox

    Err = XpressShow.ImagError
    PegasusError Err, lblError
End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If XpressShow.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
     XpressShow.ToolbarActivated = Not XpressShow.ToolbarActivated
    
    Err = XpressShow.ImagError
    PegasusError Err, lblError
End Sub


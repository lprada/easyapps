//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Event.rc
//
#define IDButton2                       2
#define IDButton1                       3
#define IDQuit                          4
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_EVENT_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDC_PROGRESS1                   1000
#define IDC_PCTDONE                     1001
#define IDC_MOUSE                       1002
#define IDC_LBLSTRESS                   1004
#define IDC_MESSAGE                     1004
#define IDC_SPIN1                       1005
#define ID_FILE_QUIT                    32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form RedEye 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress 8 AutoRemoveRedEye"
   ClientHeight    =   9060
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   12120
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9060
   ScaleWidth      =   12120
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmRedRes 
      Caption         =   "RedEye Results"
      Height          =   3135
      Left            =   7320
      TabIndex        =   11
      Top             =   3720
      Width           =   4695
      Begin VB.ListBox lstRedResults 
         Height          =   2010
         Left            =   240
         TabIndex        =   14
         Top             =   720
         Width           =   4335
      End
      Begin VB.Label lblRedDesc 
         Caption         =   "Red Eyes Found"
         Height          =   375
         Left            =   240
         TabIndex        =   13
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label lblCount 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Height          =   375
         Left            =   1560
         TabIndex        =   12
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.ListBox lstStatus 
      Height          =   1230
      Left            =   2280
      TabIndex        =   8
      Top             =   7680
      Width           =   4335
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "RedEyeRemove.frx":0000
      Left            =   120
      List            =   "RedEyeRemove.frx":000D
      TabIndex        =   6
      Top             =   360
      Width           =   11655
   End
   Begin VB.CommandButton cmdReloadImage 
      Caption         =   "Reload Image"
      Height          =   495
      Left            =   10560
      TabIndex        =   5
      Top             =   8400
      Width           =   1455
   End
   Begin VB.ComboBox cmbGlare 
      Height          =   315
      Left            =   9960
      TabIndex        =   3
      Text            =   "Glare"
      Top             =   2160
      Width           =   1935
   End
   Begin VB.ComboBox cmbShade 
      Height          =   315
      Left            =   7440
      TabIndex        =   2
      Text            =   "Shade"
      Top             =   2160
      Width           =   1695
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove Red-Eye"
      Height          =   495
      Left            =   8520
      TabIndex        =   1
      Top             =   2760
      Width           =   2295
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   5895
      Left            =   240
      TabIndex        =   0
      Top             =   1680
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   10398
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1014834258
      ErrInfo         =   -1436313311
      Persistence     =   -1  'True
      _cx             =   12091
      _cy             =   10398
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   975
      Left            =   7320
      TabIndex        =   10
      Top             =   7800
      Width           =   2415
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   7440
      TabIndex        =   9
      Top             =   7080
      Width           =   2055
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   360
      TabIndex        =   7
      Top             =   7680
      Width           =   1815
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "AutoRemoveRedeye Settings"
      Height          =   375
      Left            =   7440
      TabIndex        =   4
      Top             =   1680
      Width           =   4455
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "RedEye"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Option Explicit
Const OBorder = 4
Dim imgFileName As String
Dim imgParentDir As String
Dim redeyecounter As Long
Dim redeyex As Integer
Dim redeyey As Integer
Dim redeyewidth As Integer
Dim redeyeheight As Integer
Dim redeyeindex As Integer


Dim Tabulator(1 To OBorder) As Long

Private Declare Function SendMessage Lib "user32" Alias _
        "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As _
        Long, ByVal wParam As Long, lParam As Any) As Long
Private Const LB_SETTABSTOPS = &H192

Private Sub cmdReloadImage_Click()
    Dim XScr As Integer, YScr As Integer
    
    XScr = ImagXpress1.ScrollX
    YScr = ImagXpress1.ScrollY
    ImagXpress1.FileName = ImagXpress1.FileName
    ImagXpress1.ScrollX = XScr
    ImagXpress1.ScrollY = YScr
    
     Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub

Private Sub cmdRemove_Click()

    'Remove the RedEye(s) in the image
    lstRedResults.Clear
    ImagXpress1.AutoRemoveRedeye cmbShade.ListIndex, cmbGlare.ListIndex, False
    
    lstRedResults.AddItem "RedEye Index" & vbTab & "XPos" & vbTab & " YPos" & vbTab & "Width" & vbTab & "Height" & Chr(32)
    lstRedResults.AddItem "___________________________________________________________________________________________________"
    'report the count of total red eyes found
    lblCount.Caption = ImagXpress1.RedeyeTotalCount
    'retrieve information about the red eye rectangles
    For redeyecounter = 1 To ImagXpress1.RedeyeTotalCount
            ImagXpress1.RedeyeRectangleIndex = redeyecounter
            redeyeindex = ImagXpress1.RedeyeRectangleIndex
            redeyex = ImagXpress1.RedeyeRectangleX
            redeyey = ImagXpress1.RedeyeRectangleY
            redeyewidth = ImagXpress1.RedeyeRectangleWidth
            redeyeheight = ImagXpress1.RedeyeRectangleHeight
            SendMessage lstRedResults.hwnd, LB_SETTABSTOPS, OBorder, Tabulator(1)
            lstRedResults.AddItem redeyeindex & vbTab & vbTab & redeyex & vbTab & redeyey & vbTab & redeyewidth & vbTab & redeyeheight
    Next


    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub
Private Sub Form_Load()


    imgParentDir = App.Path
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    ImagXpress1.Edition = EDITION_Photo
    

    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\redeye2.jpg"
 
    'Eye Shade Settings
    cmbShade.AddItem "EyeShadeNormal", 0
    cmbShade.AddItem "EyeShadeDark", 1
    cmbShade.AddItem "EyeShadeLight", 2
    cmbShade.ListIndex = 0
    
    'Eye Glare Settings
    cmbGlare.AddItem "EyeGlareNormal", 0
    cmbGlare.AddItem "EyeGlareSlight", 1
    cmbGlare.AddItem "EyeGlareFull", 2
    cmbGlare.ListIndex = 0
    
    LoadFile
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub



Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub
Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub




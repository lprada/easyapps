VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSoftenSharpenAutoLevel 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Soften, Sharpen, AutoLevel"
   ClientHeight    =   10605
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   12240
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10605
   ScaleMode       =   0  'User
   ScaleWidth      =   12240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      ItemData        =   "frmSoftenSharpenAutoLevel.frx":0000
      Left            =   120
      List            =   "frmSoftenSharpenAutoLevel.frx":001F
      TabIndex        =   32
      Top             =   120
      Width           =   11895
   End
   Begin VB.ListBox lstStatus 
      Height          =   2205
      Left            =   5640
      TabIndex        =   29
      Top             =   7800
      Width           =   3135
   End
   Begin VB.CommandButton cmdReloadImage 
      Caption         =   "Reload Image"
      Height          =   495
      Left            =   7440
      TabIndex        =   27
      Top             =   2280
      Width           =   2895
   End
   Begin MSComDlg.CommonDialog OpenDialog1 
      Left            =   4920
      Top             =   7560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Picture1 
      Height          =   15
      Left            =   5160
      ScaleHeight     =   15
      ScaleWidth      =   15
      TabIndex        =   26
      Top             =   2400
      Width           =   15
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3495
      Left            =   5640
      TabIndex        =   1
      Top             =   2880
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   6165
      _Version        =   393216
      Tabs            =   5
      Tab             =   2
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "AutoLevel"
      TabPicture(0)   =   "frmSoftenSharpenAutoLevel.frx":037A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraCommands(4)"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Unsharpen"
      TabPicture(1)   =   "frmSoftenSharpenAutoLevel.frx":0396
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraCommands(0)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "SharpenEx"
      TabPicture(2)   =   "frmSoftenSharpenAutoLevel.frx":03B2
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "fraCommands(1)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "UnsharpenEx"
      TabPicture(3)   =   "frmSoftenSharpenAutoLevel.frx":03CE
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraCommands(2)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "SoftenEx"
      TabPicture(4)   =   "frmSoftenSharpenAutoLevel.frx":03EA
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraCommands(3)"
      Tab(4).ControlCount=   1
      Begin VB.Frame fraCommands 
         BorderStyle     =   0  'None
         Caption         =   "SoftenEx"
         Height          =   2655
         Index           =   3
         Left            =   -73080
         TabIndex        =   19
         Top             =   720
         Width           =   2535
         Begin VB.ComboBox cboSoftenExFilters 
            Height          =   315
            Left            =   240
            TabIndex        =   21
            ToolTipText     =   "Sharpen Filter"
            Top             =   1440
            Width           =   2055
         End
         Begin VB.CommandButton btnApplySoftenEx 
            Caption         =   "Apply"
            Height          =   375
            Left            =   360
            TabIndex        =   20
            Top             =   2040
            Width           =   1815
         End
         Begin MSComctlLib.Slider sliderSoftenExIntensity 
            Height          =   495
            Left            =   240
            TabIndex        =   22
            Top             =   600
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   873
            _Version        =   393216
            SelStart        =   5
            Value           =   5
         End
         Begin VB.Label lblSoftenExFilter 
            Caption         =   "Filter:"
            Height          =   255
            Left            =   240
            TabIndex        =   24
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label lblSoftenExIntensity 
            Caption         =   "Intensity: "
            Height          =   255
            Left            =   240
            TabIndex        =   23
            Top             =   360
            Width           =   2055
         End
      End
      Begin VB.Frame fraCommands 
         BorderStyle     =   0  'None
         Caption         =   "UnsharpenEx"
         Height          =   2655
         Index           =   2
         Left            =   -73080
         TabIndex        =   13
         Top             =   720
         Width           =   2535
         Begin VB.ComboBox cboUnsharpenExFilters 
            Height          =   315
            ItemData        =   "frmSoftenSharpenAutoLevel.frx":0406
            Left            =   240
            List            =   "frmSoftenSharpenAutoLevel.frx":0408
            TabIndex        =   15
            ToolTipText     =   "Sharpen Filter"
            Top             =   1440
            Width           =   2055
         End
         Begin VB.CommandButton btnApplyUnSharpenEx 
            Caption         =   "Apply"
            Height          =   375
            Left            =   360
            TabIndex        =   14
            Top             =   2040
            Width           =   1815
         End
         Begin MSComctlLib.Slider sliderUnsharpenExIntensity 
            Height          =   495
            Left            =   240
            TabIndex        =   16
            Top             =   600
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   873
            _Version        =   393216
            LargeChange     =   10
            SelStart        =   5
            Value           =   5
         End
         Begin VB.Label lblUnsharpenExFilter 
            Caption         =   "Filter:"
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   1200
            Width           =   2055
         End
         Begin VB.Label lblUnSharpenExIntensity 
            Caption         =   "Intensity: "
            Height          =   255
            Left            =   240
            TabIndex        =   17
            Top             =   360
            Width           =   2055
         End
      End
      Begin VB.Frame fraCommands 
         BorderStyle     =   0  'None
         Caption         =   "SharpenEx"
         Height          =   2655
         Index           =   1
         Left            =   1920
         TabIndex        =   8
         Top             =   720
         Width           =   2535
         Begin VB.CommandButton btnApplySharpenEx 
            Caption         =   "Apply"
            Height          =   375
            Left            =   360
            TabIndex        =   25
            Top             =   2040
            Width           =   1815
         End
         Begin VB.ComboBox cboSharpenExFilters 
            Height          =   315
            Left            =   240
            TabIndex        =   9
            ToolTipText     =   "Sharpen Filter"
            Top             =   1440
            Width           =   2055
         End
         Begin MSComctlLib.Slider sliderSharpenExIntensity 
            Height          =   495
            Left            =   240
            TabIndex        =   10
            Top             =   600
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   873
            _Version        =   393216
            SelStart        =   5
            Value           =   5
         End
         Begin VB.Label lblSharpenExIntensity 
            Caption         =   "Intensity: "
            Height          =   255
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Width           =   2055
         End
         Begin VB.Label lblSharpenExFilter 
            Caption         =   "Filter:"
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   1200
            Width           =   2055
         End
      End
      Begin VB.Frame fraCommands 
         BorderStyle     =   0  'None
         Caption         =   "Unsharpen"
         Height          =   2655
         Index           =   0
         Left            =   -73080
         TabIndex        =   4
         Top             =   720
         Width           =   2535
         Begin VB.CommandButton btnApplyUnsharpen 
            Caption         =   "Apply"
            Height          =   375
            Left            =   360
            TabIndex        =   5
            Top             =   2040
            Width           =   1815
         End
         Begin MSComctlLib.Slider sliderUnsharpenIntensity 
            Height          =   495
            Left            =   240
            TabIndex        =   6
            Top             =   960
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   873
            _Version        =   393216
            SelStart        =   5
            Value           =   5
         End
         Begin VB.Label lblUnsharpenIntensity 
            Caption         =   "Intensity: "
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   720
            Width           =   2055
         End
      End
      Begin VB.Frame fraCommands 
         BorderStyle     =   0  'None
         Caption         =   "AutoLevel"
         Height          =   2655
         Index           =   4
         Left            =   -73080
         TabIndex        =   2
         Top             =   720
         Width           =   2535
         Begin VB.CommandButton btnAutoLevel 
            Caption         =   "Apply"
            Height          =   375
            Left            =   360
            TabIndex        =   3
            Top             =   1200
            Width           =   1815
         End
      End
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   8175
      Left            =   120
      TabIndex        =   0
      Top             =   2160
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   14420
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1036672740
      ErrInfo         =   798544069
      Persistence     =   -1  'True
      _cx             =   9340
      _cy             =   14420
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   2295
      Left            =   9000
      TabIndex        =   31
      Top             =   7800
      Width           =   3015
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   9000
      TabIndex        =   30
      Top             =   7200
      Width           =   2055
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   5640
      TabIndex        =   28
      Top             =   7200
      Width           =   1815
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "Open"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuReloadCurrentImage 
         Caption         =   "&Reload Current Image..."
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmSoftenSharpenAutoLevel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Private Const k_maxUnSharpenExIntensity As Long = 10
Private Const k_minUnSharpenExIntensity As Long = 1
Private Const k_maxSharpenExIntensity As Long = 10
Private Const k_minSharpenExIntensity As Long = 1
Private Const k_maxSharpenIntensity As Long = 10
Private Const k_minSharpenIntensity As Long = 1
Private Const k_maxSoftenExIntensity As Long = 10
Private Const k_minSoftenExIntensity As Long = 1

Private k_dialogWidth As Long
Private k_dialogHeight As Long

Private Enum EnumCommands
    enumSharpen = 0
    enumSharpenEx
    enumUnSharpenEx
    enumSoftenEx
    enumAutoLevel
End Enum

Private Sub btnApplyUnsharpen_Click()
    Me.MousePointer = vbHourglass
    With ImagXpress1
        .Sharpen sliderUnsharpenIntensity.Value
    End With
    Me.MousePointer = vbDefault
End Sub

Private Sub btnApplySharpenEx_Click()
    Me.MousePointer = vbHourglass
    With ImagXpress1
        .SharpenEx sliderSharpenExIntensity.Value, _
            cboSharpenExFilters.ItemData(cboSharpenExFilters.ListIndex)
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End With
    Me.MousePointer = vbDefault
End Sub

Private Sub btnApplySoftenEx_Click()
    Me.MousePointer = vbHourglass
    With ImagXpress1
        .SoftenEx CLng(sliderSoftenExIntensity.Value), _
            cboSoftenExFilters.ItemData(cboSoftenExFilters.ListIndex)
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End With
    Me.MousePointer = vbDefault
End Sub

Private Sub btnApplyUnSharpenEx_Click()
    Me.MousePointer = vbHourglass
    With ImagXpress1
        .UnsharpenEx CLng(sliderUnsharpenExIntensity.Value), _
            cboUnsharpenExFilters.ItemData(cboUnsharpenExFilters.ListIndex)
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End With
    Me.MousePointer = vbDefault
End Sub

Private Sub btnAutoLevel_Click()
    Me.MousePointer = vbHourglass
    With ImagXpress1
        .AutoLevel
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End With
    Me.MousePointer = vbDefault
End Sub

Private Sub mnuReloadCurrentImage_Click()
    Me.MousePointer = vbHourglass
    With ImagXpress1
        .FileName = .FileName
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
        .ZoomFactor = 1
             
        .ScrollX = (.IWidth * .ZoomFactor - .CtlWidth) / 2
        .ScrollY = (.IHeight * .ZoomFactor - .CtlHeight) / 2
    End With
    Me.MousePointer = vbDefault
End Sub



Private Sub cmdReloadImage_Click()
    Dim XScr As Integer, YScr As Integer
    XScr = ImagXpress1.ScrollX
    YScr = ImagXpress1.ScrollY
    ImagXpress1.FileName = ImagXpress1.FileName
    
    ImagXpress1.ZoomFactor = 1
    
    ImagXpress1.ScrollX = XScr
    ImagXpress1.ScrollY = YScr
End Sub


Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
   
End Sub

Private Sub Form_Load()
          
         
    imgParentDir = App.Path
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\autoLevel.jpg"
         
   
    k_dialogWidth = Me.Width
    k_dialogHeight = Me.Height
    
    LoadFile
    
        
    With ImagXpress1
         .ScrollBars = SB_Both
        .ScrollX = (.IWidth * .ZoomFactor - .CtlWidth) / 2
        .ScrollY = (.IHeight * .ZoomFactor - .CtlHeight) / 2
    End With
    
    With cboSharpenExFilters
        .Clear
        .AddItem "SharpenFilter1", 0
        .AddItem "SharpenFilter2", 1
        .AddItem "SharpenFilter3", 2
        .ListIndex = 0
    End With
    With cboUnsharpenExFilters
        .Clear
        .AddItem "UnSharpenFilter1", 0
        .AddItem "UnSharpenFilter2", 1
        .AddItem "UnSharpenFilter3", 2
        .ListIndex = 0
    End With
    With cboSoftenExFilters
        .Clear
        .AddItem "SoftenFilter1", 0
        .AddItem "SoftenFilter2", 1
        .AddItem "SoftenFilter3", 2
        .AddItem "SoftenFilter4", 3
        .ListIndex = 0
    End With
    With sliderSharpenExIntensity
        .TickFrequency = 1
        .Max = k_maxSharpenExIntensity
        .Min = k_minSharpenExIntensity
        .Value = CLng((.Max - .Min) / 2)
    End With
    With sliderUnsharpenExIntensity
        .TickFrequency = 1
        .Max = k_maxSharpenExIntensity
        .Min = k_minSharpenExIntensity
        .Value = CLng((.Max - .Min) / 2)
    End With
    With sliderSoftenExIntensity
        .TickFrequency = 1
        .Max = k_maxSharpenExIntensity
        .Min = k_minSharpenExIntensity
        .Value = CLng((.Max - .Min) / 2)
    End With
    With sliderUnsharpenIntensity
        .TickFrequency = 1
        .Max = k_maxSharpenExIntensity
        .Min = k_minSharpenExIntensity
        .Value = CLng((.Max - .Min) / 2)
    End With
    
    Dim i As Long
    For i = 0 To fraCommands.Count - 1
        With fraCommands(i)
            .ToolTipText = .Caption
            .Visible = True
        End With
    Next i
    
    sliderSharpenExIntensity_Click
    sliderSoftenExIntensity_Click
    sliderUnsharpenExIntensity_Click
    sliderUnsharpenIntensity_Click
    
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub
Private Sub mnuAbout_Click()
    
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
End Sub




Private Sub sliderSharpenExIntensity_Click()
    lblSharpenExIntensity.Caption = "Intensity: " & CStr(sliderSharpenExIntensity.Value)
End Sub

Private Sub sliderSoftenExIntensity_Click()
    lblSoftenExIntensity.Caption = "Intensity: " & CStr(sliderSoftenExIntensity.Value)
End Sub

Private Sub sliderUnsharpenExIntensity_Click()
    lblUnSharpenExIntensity.Caption = "Intensity: " & CStr(sliderUnsharpenExIntensity.Value)
End Sub

Private Sub sliderUnsharpenIntensity_Click()
    lblUnsharpenIntensity.Caption = "Intensity: " & CStr(sliderUnsharpenIntensity.Value)
End Sub

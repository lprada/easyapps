VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form multipagetiff 
   Caption         =   "ImagXpress 8 Multi-Page TIFF"
   ClientHeight    =   7320
   ClientLeft      =   60
   ClientTop       =   645
   ClientWidth     =   11550
   LinkTopic       =   "Form1"
   ScaleHeight     =   7320
   ScaleWidth      =   11550
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   8760
      TabIndex        =   11
      Top             =   2400
      Width           =   2655
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      ItemData        =   "multipagetiff.frx":0000
      Left            =   240
      List            =   "multipagetiff.frx":0019
      TabIndex        =   9
      Top             =   240
      Width           =   11055
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressShow 
      Height          =   1935
      Index           =   0
      Left            =   480
      TabIndex        =   6
      Top             =   4920
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   3413
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   127675555
      ErrInfo         =   1075164123
      Persistence     =   -1  'True
      _cx             =   4683
      _cy             =   3413
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   1575
      Left            =   4080
      TabIndex        =   5
      Top             =   2640
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   2778
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   127675555
      ErrInfo         =   1075164123
      Persistence     =   -1  'True
      _cx             =   2778
      _cy             =   2778
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdCompact 
      Caption         =   "Compact File"
      Height          =   495
      Left            =   6000
      TabIndex        =   2
      Top             =   3840
      Width           =   1935
   End
   Begin VB.CommandButton cmdInsert 
      Caption         =   "Insert Page 2"
      Height          =   495
      Left            =   6000
      TabIndex        =   3
      Top             =   3360
      Width           =   1935
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove Page 2"
      Height          =   495
      Left            =   6000
      TabIndex        =   1
      Top             =   2880
      Width           =   1935
   End
   Begin VB.CommandButton cmdMake 
      Caption         =   "Make MP.TIF"
      Height          =   495
      Left            =   6000
      TabIndex        =   0
      Top             =   2400
      Width           =   1935
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressShow 
      Height          =   1935
      Index           =   1
      Left            =   3120
      TabIndex        =   7
      Top             =   4920
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   3413
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   127675555
      ErrInfo         =   1075164123
      Persistence     =   -1  'True
      _cx             =   4683
      _cy             =   3413
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   3
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressShow 
      Height          =   1935
      Index           =   2
      Left            =   5760
      TabIndex        =   8
      Top             =   4920
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   3413
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   127675555
      ErrInfo         =   1075164123
      Persistence     =   -1  'True
      _cx             =   4683
      _cy             =   3413
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   4
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1695
      Left            =   8760
      TabIndex        =   13
      Top             =   5040
      Width           =   2535
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   8760
      TabIndex        =   12
      Top             =   4320
      Width           =   2055
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   8760
      TabIndex        =   10
      Top             =   1920
      Width           =   1935
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   720
      TabIndex        =   4
      Top             =   3000
      Width           =   2535
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "multipagetiff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim MPFileName As String
Dim FileName1, FileName2, FileName3 As String
Dim TempFileName As String

Private Sub cmdCompact_Click()
  ' Compacts the file
  ImagXpress1.CompactFile MPFileName, TempFileName
  
 Err = ImagXpress1.ImagError
 PegasusError Err, lblError
  
  If ImagXpress1.ImagError = 0 Then
    Kill MPFileName
    Name TempFileName As MPFileName ' Rename the compacted file
    cmdShow_Click ' Reload
  End If
End Sub

Private Sub cmdInsert_Click()
  ' Inserts Page 2
 
  ImagXpress1.InsertPage FileName2, MPFileName, 2
   If ImagXpress1.NumPages(MPFileName) = 3 Then
    cmdInsert.Enabled = False
  End If
  
  If ImagXpress1.ImagError = 0 Then
    cmdShow_Click
  Else
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
  End If
  
  cmdRemove.Enabled = True
  cmdCompact.Enabled = False
  
End Sub

Private Sub cmdMake_Click()
  ' Creates a Multiple page TIFF file
  ImagXpress1.SaveMultiPage = False
  ImagXpress1.PageNbr = 0
  ImagXpress1.FileName = FileName1
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Error Loading Image"
    End If
  ImagXpress1.SaveFileName = MPFileName
  ImagXpress1.SaveTIFFCompression = TIFF_Uncompressed
  ImagXpress1.SaveFile
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Error Saving Image"
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
    End If
  ImagXpress1.SaveMultiPage = True
  ImagXpress1.FileName = FileName2
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Error Loading Image"
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End If
  ImagXpress1.SaveFile
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Error Saving Image"
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End If
  ImagXpress1.FileName = FileName3
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Error Loading Image"
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End If
  ImagXpress1.SaveFile
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Error Saving Image"
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End If
  ImagXpress1.SaveMultiPage = False
   cmdRemove.Enabled = True
  cmdShow_Click
End Sub

Private Sub cmdShow_Click()
  ' Displays pages 1 to 3
  NbrPages = ImagXpress1.NumPages(MPFileName)
  ' Show the first 3 pages
  For i = 1 To 3
    If i <= NbrPages Then
      XpressShow(i - 1).PageNbr = i
      XpressShow(i - 1).FileName = MPFileName
      
      Err = ImagXpress1.ImagError
      PegasusError Err, lblError
    Else
      XpressShow(i - 1).FileName = ""
      
      Err = ImagXpress1.ImagError
      PegasusError Err, lblError
    End If
  Next i
  Label1.Caption = "Number of Pages: " & NbrPages & Chr$(10) & "FileSize: " & FileLen(MPFileName$)

End Sub


Private Sub cmdRemove_Click()
   cmdInsert.Enabled = True
   cmdRemove.Enabled = False
   cmdMake.Enabled = False
   cmdCompact.Enabled = True
  ImagXpress1.DeletePage MPFileName, 2
  
  If ImagXpress1.ImagError = 0 Then
    cmdShow_Click
  Else
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
  End If
  
End Sub

Private Sub Form_Load()
  cmdInsert.Enabled = False
  cmdRemove.Enabled = False
  cmdCompact.Enabled = False
  
  ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
  MPFileName = App.Path & "\mp.tif"
  FileName1 = App.Path & "\..\..\..\..\..\..\Common\Images\page1.tif"
  FileName2 = App.Path & "\..\..\..\..\..\..\Common\Images\page2.tif"
  FileName3 = App.Path & "\..\..\..\..\..\..\Common\Images\page3.tif"
  TempFileName = "\compact.tif"
  
  
  
End Sub
Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub
Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub



// SDIView.cpp : implementation of the CSDIView class
//

#include "stdafx.h"
#include "SDI.h"

#include "SDIDoc.h"
#include "SDIView.h"
#include "ZoomDlg.h"
#include <winspool.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDIView

IMPLEMENT_DYNCREATE(CSDIView, CView)

BEGIN_MESSAGE_MAP(CSDIView, CView)
	//{{AFX_MSG_MAP(CSDIView)
	ON_COMMAND(ID_PROCESSING_FLIP, OnProcessingFlip)
	ON_COMMAND(ID_PROCESSING_GRAYSCALE, OnProcessingGrayscale)
	ON_COMMAND(ID_PROCESSING_MIRROR, OnProcessingMirror)
	ON_COMMAND(ID_PROCESSING_ROTATE180, OnProcessingRotate180)
	ON_COMMAND(ID_PROCESSING_ROTATE270, OnProcessingRotate270)
	ON_COMMAND(ID_PROCESSING_ROTATE90, OnProcessingRotate90)
	ON_COMMAND(ID_PROCESSING_ZOOM, OnProcessingZoom)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

#include "..\Include\ix8_open.cpp"

/////////////////////////////////////////////////////////////////////////////
// CSDIView construction/destruction

CSDIView::CSDIView()
{
	// Initialize COM
	HRESULT hRes = CoInitialize(NULL);

}

CSDIView::~CSDIView()
{
	// Delete the ImagXpress Object
	if (pImagXpress)
	{
		pImagXpress = NULL;
		if (ppCImagXpress) 
			delete ppCImagXpress;
	}
	// Close COM
	CoUninitialize();
}

BOOL CSDIView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSDIView drawing

void CSDIView::OnDraw(CDC* pDC)
{
	CSDIDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// CSDIView printing

BOOL CSDIView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	// Compute the Page Width & Height
	int TwipsPerInch = 1440;
	int PageWidth  = 8 * TwipsPerInch;
	int PageHeight = int(10.5 * (float)TwipsPerInch);

	// Set the Print area and center the image
	pImagXpress->PrinterWidth = 4 * TwipsPerInch;  // 4 Inches Wide
	pImagXpress->PrinterHeight = 3 * TwipsPerInch; // 3 Inches High
	pImagXpress->PrinterLeft = (PageWidth-pImagXpress->PrinterWidth)/2;  // Center the image
	pImagXpress->PrinterTop = (PageHeight-pImagXpress->PrinterHeight)/2; // Center the Image


	return DoPreparePrinting (pInfo);
}

void CSDIView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSDIView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CSDIView diagnostics

#ifdef _DEBUG
void CSDIView::AssertValid() const
{
	CView::AssertValid();
}

void CSDIView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CSDIDoc* CSDIView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSDIDoc)));
	return (CSDIDoc*)m_pDocument;
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CSDIView message handlers

int CSDIView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// Create an ImagXpress Object
	HINSTANCE hDLL = IX_Open();  // ImagXpress initialization
	ppCImagXpress = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 1, 1, 50, 50);
	pImagXpress = ppCImagXpress->pImagXpress;

	if (pImagXpress)
	{
		pImagXpress->AutoSize = ISIZE_CropImage;
		pImagXpress->ScrollBars = SB_Both;
		pImagXpress->UndoEnabled = true;
	}
	IX_Close(hDLL);  // Our object is created, close the initialization
	
	return 0;
}

void CSDIView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
	// Always make ImagXpress the same size as our view
	RECT rView;
	GetClientRect(&rView);
	::MoveWindow((HWND)pImagXpress->hWnd, rView.left, rView.top,
		rView.right-rView.left, rView.bottom-rView.top, FALSE);
}

void CSDIView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{

	
	

	// Get the filename from the View and send it to ImagXpress
  
	CSDIDoc* pDoc = GetDocument();
	pDoc->SetTitle("ImagXpress 8 SDI Example");	
	CString strFName = pDoc->GetFileName();
	pImagXpress->FileName = strFName.GetBuffer(0);
	
	if (pImagXpress->ImagError != 0)
		MessageBox("ImagXpress could not load " + strFName, "Error", MB_OK);
}

void CSDIView::OnProcessingFlip() 
{
	pImagXpress->Flip();
	GetDocument()->SetModifiedFlag();
}

void CSDIView::OnProcessingMirror() 
{
	pImagXpress->Mirror();
	GetDocument()->SetModifiedFlag();
}

void CSDIView::OnProcessingRotate180() 
{
	pImagXpress->Rotate(180);
	GetDocument()->SetModifiedFlag();
}

void CSDIView::OnProcessingRotate270() 
{
	pImagXpress->Rotate(270);
	GetDocument()->SetModifiedFlag();
}

void CSDIView::OnProcessingRotate90() 
{
	pImagXpress->Rotate(90);
	GetDocument()->SetModifiedFlag();
}

void CSDIView::OnProcessingZoom() 
{
	CZoomDlg zoomDlg(this);
    zoomDlg.m_fZoomF = pImagXpress->ZoomFactor;
	zoomDlg.m_fZoomF = pImagXpress->ZoomFactor;
	int nRetCode = zoomDlg.DoModal();
	if (nRetCode == IDOK)
	{
		pImagXpress->ZoomFactor = ((float)zoomDlg.m_fZoomF);
		pImagXpress->ZoomFactor = (float)zoomDlg.m_fZoomF;

	}
}

void CSDIView::OnProcessingGrayscale() 
{

	pImagXpress->ColorDepth(8, IPAL_Gray, DI_Pegasus );
	GetDocument()->SetModifiedFlag();
}

void CSDIView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	HANDLE hPrinter;
	if(pInfo->m_bPreview == FALSE) {

		// Get the Printer DC
		CPrintDialog PrintDlg(FALSE);
		if (PrintDlg.DoModal() != IDOK)
		return;
		HDC hdcPrinter = PrintDlg.GetPrinterDC();

		// Open the Printer
		OpenPrinter(NULL, &hPrinter, NULL);
		DOCINFO dInfo;
		memset ((PVOID)&dInfo, 0, sizeof (DOCINFO));
		dInfo.cbSize = sizeof (DOCINFO);
		dInfo.lpszDocName = NULL;
		StartDoc(hdcPrinter, &dInfo);
		StartPage(hdcPrinter);

		// Print the image
		pImagXpress->PrinterhDC = (long)hdcPrinter;

		// Cleanup/Close Printer
		EndPage(hdcPrinter);
		EndDoc(hdcPrinter);
		ClosePrinter(hPrinter);
	} else {
		
		CBitmap tempBmp;
		CBitmap *myBmp = tempBmp.FromHandle((HBITMAP)pImagXpress->hBMP);
		CDC source;
		source.CreateCompatibleDC(GetDC());
		source.SelectObject(myBmp);

		long x, y, width, height, srcWidth, srcHeight, logx, logy;
		x = pImagXpress->PrinterLeft;
		y = pImagXpress->PrinterTop;
		width = pImagXpress->PrinterWidth;
		height = pImagXpress->PrinterHeight;
		logx = pDC->GetDeviceCaps(LOGPIXELSX);
		logy = pDC->GetDeviceCaps(LOGPIXELSY);

		x = (long)(x * logx)/1440L;
		y = (long)(y * logy)/1440L;
		width = (long)(width * logx)/1440L;
		height = (long)(height * logy)/1440L;


		srcWidth = pImagXpress->IWidth;
		srcHeight = pImagXpress->IHeight;

		
		pDC->StretchBlt(x, y, width, height,
			&source, 0, 0, srcWidth, srcHeight, SRCCOPY);
		
	}	
	


}

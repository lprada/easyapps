// SDIDoc.cpp : implementation of the CSDIDoc class
//

#include "stdafx.h"
#include "SDI.h"

#include "SDIDoc.h"
#include "SDIView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSDIDoc

IMPLEMENT_DYNCREATE(CSDIDoc, CDocument)

BEGIN_MESSAGE_MAP(CSDIDoc, CDocument)
//{{AFX_MSG_MAP(CSDIDoc)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSDIDoc construction/destruction

CSDIDoc::CSDIDoc()
{
	// TODO: add one-time construction code here
	
}

CSDIDoc::~CSDIDoc()
{
}

BOOL CSDIDoc::OnNewDocument()
{	
	if (!CDocument::OnNewDocument())
		return FALSE;
	
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	m_strFileName = "";
		
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSDIDoc serialization

void CSDIDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here				
	}
	else
	{
		// TODO: add loading code here
		m_strFileName = ar.m_strFileName;		
	}	
}

/////////////////////////////////////////////////////////////////////////////
// CSDIDoc diagnostics

#ifdef _DEBUG
void CSDIDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSDIDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSDIDoc commands

CString CSDIDoc::GetFileName()
{
	return m_strFileName;
}

BOOL CSDIDoc::IsActual()
{
	return (BOOL)m_strFileName.GetLength();
}

BOOL CSDIDoc::SaveModified() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDocument::SaveModified();
}

BOOL CSDIDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// There should be only one view, but this project could be modified to have
	// multiple views, there would be an ImagXpress object in each view object.
	// We just need to use one of the ImagXpress objects on one of the view objects 
	// to perform the save operation...
	POSITION pos = GetFirstViewPosition();
	CSDIView* pFirstView = (CSDIView*)GetNextView( pos );
	pFirstView->pImagXpress->SaveFileName = lpszPathName;
	pFirstView->pImagXpress->SaveFile();	
	//return CDocument::OnSaveDocument(lpszPathName);
	return true;
	
}

VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress ActiveX/COM Sample - Deskew"
   ClientHeight    =   6390
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   9975
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6390
   ScaleWidth      =   9975
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cmbdeskewtype 
      Height          =   315
      Left            =   1080
      TabIndex        =   8
      Text            =   "Combo1"
      Top             =   5880
      Width           =   2535
   End
   Begin VB.ListBox lstStatus 
      Height          =   2400
      ItemData        =   "frmMain.frx":0000
      Left            =   6960
      List            =   "frmMain.frx":0007
      TabIndex        =   5
      Top             =   1200
      Width           =   2895
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "frmMain.frx":0016
      Left            =   240
      List            =   "frmMain.frx":001D
      TabIndex        =   3
      Top             =   120
      Width           =   9615
   End
   Begin MSComDlg.CommonDialog dlgFileOpen 
      Left            =   240
      Top             =   5760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DefaultExt      =   ".jpg"
   End
   Begin VB.CommandButton cmdDeskew 
      Caption         =   "Deskew"
      Height          =   495
      Left            =   3720
      TabIndex        =   1
      Top             =   5760
      Width           =   1575
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4575
      Left            =   240
      TabIndex        =   0
      Top             =   960
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   8070
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   529684005
      ErrInfo         =   -568000196
      Persistence     =   -1  'True
      _cx             =   11668
      _cy             =   8070
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   2055
      Left            =   6960
      TabIndex        =   7
      Top             =   4080
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   255
      Left            =   6960
      TabIndex        =   6
      Top             =   3720
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Load Status:"
      Height          =   255
      Left            =   6960
      TabIndex        =   4
      Top             =   960
      Width           =   1695
   End
   Begin VB.Label lblDeskewType 
      Alignment       =   1  'Right Justify
      Caption         =   "Deskew Type:"
      Height          =   255
      Left            =   960
      TabIndex        =   2
      Top             =   5640
      Width           =   1215
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Private Sub cmdDeskew_Click()
    If imgFileName <> "" Then
        ImagXpress1.Deskew cmbdeskewtype.ListIndex
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    Else
        MsgBox "Please load an image first.", vbOKOnly, "Error"
    End If
End Sub


Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub


Private Sub Form_Load()
    imgParentDir = App.Path
    cmbdeskewtype.AddItem "Deskew_Normal", 0
    cmbdeskewtype.AddItem "Deskew_AdjustSize", 1
    cmbdeskewtype.AddItem "Deskew_PreserveCrop", 2
    cmbdeskewtype.AddItem "Deskew_Preserve_Resize", 3
    cmbdeskewtype.AddItem "Deskew_Preserve_Resample", 4
    cmbdeskewtype.AddItem "Deskew_Preserve_PICResize", 5
    cmbdeskewtype.AddItem "Deskew_Photo_Crop", 6
    
    cmbdeskewtype.ListIndex = 0
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\deskew.tif"
    LoadFile
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
 End Sub

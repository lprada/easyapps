// Color Profiles.h : main header file for the COLOR PROFILES application
//

#if !defined(AFX_COLORPROFILES_H__B98EA8FD_2FB4_4EDD_9039_2DC07B45DBDC__INCLUDED_)
#define AFX_COLORPROFILES_H__B98EA8FD_2FB4_4EDD_9039_2DC07B45DBDC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


/////////////////////////////////////////////////////////////////////////////
// CColorProfilesApp:
// See Color Profiles.cpp for the implementation of this class
//

class CColorProfilesApp : public CWinApp
{
public:
	CColorProfilesApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorProfilesApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CColorProfilesApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORPROFILES_H__B98EA8FD_2FB4_4EDD_9039_2DC07B45DBDC__INCLUDED_)

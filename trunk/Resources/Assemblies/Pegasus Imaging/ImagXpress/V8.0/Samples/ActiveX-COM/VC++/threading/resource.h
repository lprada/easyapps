//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by threading.rc
//
#define IDD_THREADING_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       131
#define IDC_ASYNC                       1000
#define IDC_ASYNCPRIORITY               1001
#define IDC_LOADIMAGE1                  1003
#define IDC_LOADIMAGE2                  1004
#define IDC_LIST1                       1005
#define IDC_MAXTHREADS                  1006
#define IDC_SPIN1                       1007
#define IDC_LOADRESIZE                  1009
#define ID_FILE_QUIT                    32771
#define ID_ABOUT_ABOUTBOX               32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

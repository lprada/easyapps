unit options;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AxCtrls, OleCtrls, PegasusImagingActiveXImagXpress8_TLB,
  StdCtrls, Menus, ExtCtrls, ComCtrls;

type
  TForm1 = class(TForm)
    ImagXpress1: TImagXpress;
    cd: TOpenDialog;
    menu: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Quit1: TMenuItem;
    oolbar1: TMenuItem;
    Show1: TMenuItem;
    About1: TMenuItem;
    grpLoadCrop: TGroupBox;
    chkLoadCropEnabled: TCheckBox;
    scrCropX: TScrollBar;
    scrCropY: TScrollBar;
    scrCropWidth: TScrollBar;
    scrCropHeight: TScrollBar;
    grpLoadResize: TGroupBox;
    chkPreserveAspect: TCheckBox;
    chkLoadResizeEnabled: TCheckBox;
    scrResizeWidth: TScrollBar;
    scrResizeHeight: TScrollBar;
    grpLoadRotated: TGroupBox;
    chkLoadRotatedEnabled: TCheckBox;
    cboLoadRotated: TComboBox;
    lblLoadRotateSetting: TLabel;
    lblCropX: TLabel;
    lblCropY: TLabel;
    lblCropWidth: TLabel;
    lblCropHeight: TLabel;
    lblResizeWidth: TLabel;
    lblResizeHeight: TLabel;
    ListBox1: TListBox;
    ProgressBar1: TProgressBar;
    lblLoadStatus: TLabel;
    Button1: TButton;
  
    procedure Quit1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);

    procedure cmdclearClick(Sender: TObject);
    procedure Show1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure setCropControlsEnabled(bEnabled: Boolean);
    procedure setResizeControlsEnabled(bEnabled: Boolean);
    procedure setRotateControlsEnabled(bEnabled: Boolean);
    procedure chkLoadCropEnabledClick(Sender: TObject);
    procedure initRotateSettingsCombo();
    procedure chkLoadResizeEnabledClick(Sender: TObject);
    procedure chkLoadRotatedEnabledClick(Sender: TObject);
    procedure scrCropXChange(Sender: TObject);
    procedure scrCropYChange(Sender: TObject);
    procedure scrCropWidthChange(Sender: TObject);
    procedure scrCropHeightChange(Sender: TObject);
    procedure scrResizeWidthChange(Sender: TObject);
    procedure scrResizeHeightChange(Sender: TObject);

    procedure LoadImage(Sender : TObject);
    function BtoS(bVal: Boolean): String;
    function ENUMtoS(iVal: integer): String;
    procedure ImagXpress1Progress(ASender: TObject; ImageID, OperationID,
      BytesProcessed, TotalBytes, PctDone: Integer; bDone,
      bAsync: WordBool; Error: Integer);
    procedure Button1Click(Sender: TObject);
  private

    imageWidth: integer;
    imageHeight: integer;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  FNam: String;
  filter : string;

implementation

{$R *.dfm}

procedure TForm1.Quit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.Open1Click(Sender: TObject);
var
err : integer;
begin

  lblloadstatus.Caption := '';
  progressbar1.Position := 0;
  filter := 'IX Supported Files|*.BMP;*TIF;*.TIFF;*.CAL;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.JPG;*.JIF;*.LJP;PCX;*.PIC;*.PNG;*.JB2*.WSQ|All File (*.*)|*.*';
  cd.InitialDir := '..\..\..\..\..\..\Common\Images\';
  cd.Filter := filter;
  cd.Execute;
  FNam := cd.FileName;
  ImagXpress1.FileName := FNam;

  Form1.LoadImage(Sender);

end;
procedure TForm1.cmdclearClick(Sender: TObject);
begin
  ImagXpress1.hDIB := 0;
end;

procedure TForm1.Show1Click(Sender: TObject);
begin
    If ImagXpress1.ToolbarActivated Then
    begin
        show1.Caption := '&Show';
    end
    Else
        show1.Caption := '&Hide';

    ImagXpress1.ToolbarActivated := Not ImagXpress1.ToolbarActivated
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  ImagXpress1.AboutBox;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
      //Load filename to get image width and height. These values are

      //used to set maximum values for the scroll bars used in this sample
        FNam:= 'C:\benefits.tif';
        //*****Must call the UnlockRuntime method at startup
       //ImagXpress1.UnlockRuntime(1234,1234,1234,1234);
        ImagXpress1.FileName := FNam;
        ImagXpress1.EventSetEnabled(EVENT_PROGRESS, True) ;
        imageWidth := ImagXpress1.IWidth;
        imageHeight := ImagXpress1.IHeight;
        ImagXpress1.FileName := '';

        // Set max values for Crop scroll bars. Setting the crop properties
        // outside the boundaries of an image will return an error to the
        // ImagError property
        scrCropHeight.Max := imageHeight - 1;
        scrCropWidth.Max := imageWidth - 1;
        scrCropX.Max := imageWidth - 1;
        scrCropY.Max := imageHeight - 1;

        //set min width and height to 1, as 0 will return a -12(Invalid Option)
        //error to ImagError
        scrCropHeight.Min := 1;
        scrCropWidth.Min := 1;
        scrResizeHeight.Min := 1;
        scrResizeWidth.Min := 1;

        ImagXpress1.AutoSize := ISIZE_CropImage;
        ImagXpress1.ScrollBars := SB_Both;

        initRotateSettingsCombo;
        setCropControlsEnabled(FALSE);
        setResizeControlsEnabled(FALSE);
        setRotateControlsEnabled(FALSE);

        lblCropHeight.Caption := IntToStr(scrCropHeight.Position);
        lblCropWidth.Caption := IntToStr(scrCropWidth.Position);
        lblCropX.Caption := IntToStr(scrCropX.Position);
        lblCropY.Caption := IntToStr(scrCropY.Position);

end;

procedure TForm1.initRotateSettingsCombo();
begin
        //Populate ComboBox with settings for LoadRotated method
        cboLoadRotated.Items.Add ('None');
        cboLoadRotated.Items.Add ('90 Degrees');
        cboLoadRotated.Items.Add ('180 Degrees');
        cboLoadRotated.Items.Add ('270 Degrees');
        cboLoadRotated.Items.Add ('Flip');
        cboLoadRotated.Items.Add ('Mirror');

        // Set selected list index to None
        cboLoadRotated.ItemIndex := 0;
end;

procedure TForm1.setCropControlsEnabled(bEnabled: Boolean);
begin

        scrCropHeight.Enabled := bEnabled;
        scrCropWidth.Enabled := bEnabled;
        scrCropX.Enabled := bEnabled;
        scrCropY.Enabled := bEnabled;

        lblCropHeight.Enabled := bEnabled;
        lblCropWidth.Enabled := bEnabled;
        lblCropX.Enabled := bEnabled;
        lblCropY.Enabled := bEnabled;

        lblCropHeight.Enabled := bEnabled;
        lblCropWidth.Enabled := bEnabled;
        lblCropX.Enabled := bEnabled;
        lblCropY.Enabled := bEnabled;

        scrCropHeight.Position := 100;
        scrCropWidth.Position := 100;
        scrCropX.Position := 30;
        scrCropY.Position := 30;

end;

procedure TForm1.setResizeControlsEnabled(bEnabled: Boolean);
begin

        scrResizeHeight.Enabled := bEnabled;
        scrResizeWidth.Enabled := bEnabled;

        lblResizeHeight.Enabled := bEnabled;
        lblResizeWidth.Enabled := bEnabled;

        lblResizeHeight.Enabled := bEnabled;
        lblResizeWidth.Enabled := bEnabled;

        chkPreserveAspect.Enabled := bEnabled;

        scrResizeHeight.Position := 150;
        scrResizeWidth.Position := 200;

end;

procedure TForm1.setRotateControlsEnabled(bEnabled: Boolean);
begin
        cboLoadRotated.Enabled := bEnabled;
        lblLoadRotateSetting.Enabled := bEnabled;
end;

procedure TForm1.chkLoadCropEnabledClick(Sender: TObject);
begin
        if chkLoadCropEnabled.Checked then
                setCropControlsEnabled(TRUE)
        else
                setCropControlsEnabled(FALSE);

end;

procedure TForm1.chkLoadResizeEnabledClick(Sender: TObject);
begin
        if chkLoadResizeEnabled.Checked then
                setResizeControlsEnabled(TRUE)
        else
                setResizeControlsEnabled(FALSE);
end;

procedure TForm1.chkLoadRotatedEnabledClick(Sender: TObject);
begin
        if chkLoadRotatedEnabled.Checked then
                setRotateControlsEnabled(TRUE)
        else
                setRotateControlsEnabled(FALSE)
end;

procedure TForm1.scrCropXChange(Sender: TObject);
begin
        lblCropX.Caption := IntToStr(scrCropX.Position);
        if (scrCropX.Position + scrCropWidth.Position >= imageWidth) then
                scrCropWidth.Position := imageWidth - scrCropX.Position;
end;

procedure TForm1.scrCropYChange(Sender: TObject);
begin
        lblCropY.Caption := IntToStr(scrCropY.Position);
        if (scrCropY.Position + scrCropHeight.Position >= imageHeight) then
                scrCropHeight.Position := imageHeight - scrCropY.Position;
end;

procedure TForm1.scrCropWidthChange(Sender: TObject);
begin
        lblCropWidth.Caption := IntToStr(scrCropWidth.Position);
        if (scrCropX.Position + scrCropWidth.Position >= imageWidth) then
                scrCropX.Position := imageWidth - scrCropWidth.Position;
end;

procedure TForm1.scrCropHeightChange(Sender: TObject);
begin
        lblCropHeight.Caption := IntToStr(scrCropHeight.Position);
        if (scrCropY.Position + scrCropHeight.Position >= imageHeight) then
                scrCropY.Position := imageHeight - scrCropHeight.Position;
end;

procedure TForm1.scrResizeWidthChange(Sender: TObject);
begin
        lblResizeWidth.Caption := IntToStr(scrResizeWidth.Position);
end;

procedure TForm1.scrResizeHeightChange(Sender: TObject);
begin
        lblResizeHeight.Caption := IntToStr(scrResizeHeight.Position);
end;

procedure TForm1.LoadImage(Sender: TObject);
var
        Err: Int64;
        Txt: String;
begin

    if (chkLoadCropEnabled.Checked) then
    begin
        // Set LoadCrop properties, to crop image as it's loaded
        ImagXpress1.LoadCropEnabled := TRUE;
        ImagXpress1.LoadCropX := scrCropX.Position;
        ImagXpress1.LoadCropY := scrCropY.Position;
        ImagXpress1.LoadCropWidth := scrCropWidth.Position;
        ImagXpress1.LoadCropHeight := scrCropHeight.Position;
    end
    else
        ImagXpress1.LoadCropEnabled := FALSE;

    if (chkLoadResizeEnabled.Checked) then
    begin
        // Set LoadResize properties, to resize image as it's loaded
        ImagXpress1.LoadResizeEnabled := TRUE;
        ImagXpress1.LoadResizeWidth := scrResizeWidth.Position;
        ImagXpress1.LoadResizeHeight := scrResizeHeight.Position;
        ImagXpress1.LoadResizeMaintainAspectRatio := chkPreserveAspect.Checked;
    end
    else
        ImagXpress1.LoadResizeEnabled := FALSE;

    if (chkLoadRotatedEnabled.Checked) then
    begin
        // Set LoadRotated property, to rotate image as it's loaded
        ImagXpress1.LoadRotated := cboLoadRotated.ItemIndex;
    end
    else
        ImagXpress1.LoadRotated := LR_NONE;

    //filename loaded here
    ImagXpress1.FileName := FNam;

    Err := ImagXpress1.ImagError;

    // Check for error and report status of image load
    if (Err = 0) then
    begin
        Txt := 'Image Load Successful! ';
        if (ImagXpress1.LoadCropEnabled) then
            Txt := Txt + Chr(13) + ' Crop X = ' + IntToStr(ImagXpress1.LoadCropX) + ', Y = ' + IntToStr(ImagXpress1.LoadCropY) + ', Wid = ' + IntToStr(ImagXpress1.LoadCropWidth) + ', Hgt = ' + IntToStr(ImagXpress1.LoadCropHeight)
        else
            Txt := Txt + Chr(13) + ' LoadCrop Disabled';

        if ImagXpress1.LoadResizeEnabled then
            Txt := Txt + Chr(13) + ' Resize: ' + IntToStr(ImagXpress1.LoadResizeWidth) + 'x' + IntToStr(ImagXpress1.LoadResizeHeight) + ', Preserve Aspect: ' + BtoS(ImagXpress1.LoadResizeMaintainAspectRatio)
        else
            Txt := Txt + Chr(13) + ' LoadResize Disabled';

        Txt := Txt + Chr(13) + ' Rotate: ' + ENUMtoS(ImagXpress1.LoadRotated);
    end
    else
        Txt := 'Load Failed! Error: ' + IntToStr(ImagXpress1.ImagError);

        lblLoadStatus.Caption := Txt

end;

function TForm1.BtoS(bVal: Boolean): String;
begin
        if bVal then
                BtoS := 'True'
        else
                BtoS := 'False';
end;

function TForm1.ENUMtoS(iVal: integer): String;
begin
        if iVal = 0 then ENUMtoS := 'None'
        else if iVal = 1 then ENUMtoS := '90 Degrees'
        else if iVal = 2 then ENUMtoS := '180 Degrees'
        else if iVal = 3 then ENUMtoS := '270 Degrees'
        else if iVal = 4 then ENUMtoS := 'Flip'
        else if iVal = 5 then ENUMtoS := 'Mirror';
end;




procedure TForm1.ImagXpress1Progress(ASender: TObject; ImageID,
  OperationID, BytesProcessed, TotalBytes, PctDone: Integer; bDone,
  bAsync: WordBool; Error: Integer);
begin
       progressbar1.Position := Pctdone;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
 LoadImage(Sender);
end;

end.

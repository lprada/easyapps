VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form FormMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Toolbar Sample and Menu Sample"
   ClientHeight    =   7065
   ClientLeft      =   3465
   ClientTop       =   1755
   ClientWidth     =   10815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7065
   ScaleWidth      =   10815
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox lstStatus 
      Height          =   2205
      Left            =   7080
      TabIndex        =   9
      Top             =   1920
      Width           =   3015
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "ViewingTools.frx":0000
      Left            =   120
      List            =   "ViewingTools.frx":0010
      TabIndex        =   7
      Top             =   120
      Width           =   10335
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4335
      Left            =   120
      TabIndex        =   4
      Top             =   1320
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   7646
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1264138436
      ErrInfo         =   -531550553
      Persistence     =   -1  'True
      _cx             =   11245
      _cy             =   7646
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Frame fraViewTools 
      Caption         =   "Viewing Tools"
      Height          =   1095
      Left            =   1440
      TabIndex        =   0
      Top             =   5880
      Width           =   3495
      Begin VB.ComboBox cboToolSelect 
         Height          =   315
         Left            =   1320
         TabIndex        =   1
         Text            =   "cboToolSelect"
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label lblToolSelected 
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   720
         Width           =   3015
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Select Tool:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   495
      Left            =   7200
      TabIndex        =   8
      Top             =   1320
      Width           =   2655
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   7080
      TabIndex        =   6
      Top             =   4320
      Width           =   2415
   End
   Begin VB.Label lblerror 
      Height          =   1935
      Left            =   7080
      TabIndex        =   5
      Top             =   4920
      Width           =   2775
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuFileSpaceer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "FormMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Dim imgFileName As String
Dim imgParentDir As String

Private Sub cboToolSelect_Click()
   If cboToolSelect.ListIndex = 0 Then
    ImagXpress1.MenuSetEnabled Menu_Context, TOOL_None, True
    ImagXpress1.ToolSet TOOL_None, IXMOUSEBUTTON_Left, 0
    Else
        ImagXpress1.MenuSetEnabled Menu_Context, TOOL_None, False
    End If
    
    ImagXpress1.ToolSet cboToolSelect.ListIndex, IXMOUSEBUTTON_Left, 0
    lblToolSelected.Caption = "Tool Selected: " & cboToolSelect.List(ImagXpress1.ToolGet(IXMOUSEBUTTON_Left, 0))
End Sub

Private Sub chkShowToolbar_Click()
    If chkShowToolbar Then
        ImagXpress1.MenuSetEnabled Menu_Context, TOOL_None, False
        ImagXpress1.ToolbarVisible = True
    Else
        ImagXpress1.MenuSetEnabled Menu_Context, TOOL_None, True
        ImagXpress1.ToolbarVisible = False
    End If
End Sub

Private Sub Form_Load()

    ImagXpress1.MenuAddItem Menu_Context, 0, 100, 0, " Rotate Image", 0, 0
    ImagXpress1.MenuAddItem Menu_Context, 0, 100, 110, "Rotate Left", 0, 0
    ImagXpress1.MenuAddItem Menu_Context, 0, 100, 120, "Rotate Right", 0, 0
    ImagXpress1.MenuAddItem Menu_Context, 0, 100, 130, "Flip Image", 0, 0
    ImagXpress1.MenuAddItem Menu_Context, 0, 200, 0, "ScrollBars", 0, 0
    ImagXpress1.MenuAddItem Menu_Context, 0, 300, 0, "Mirror Image", 0, 0
    
    
    ImagXpress1.MenuSetEnabled Menu_Context, TOOL_None, False
    ImagXpress1.MenuSetItemChecked Menu_Context, TOOL_None, 200, 0, True, False
    
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    imgParentDir = App.Path
    
    imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\window.jpg"
    
    LoadFile
      
    
    cboToolSelect.AddItem "None", 0
    cboToolSelect.AddItem "Zoom In", 1
    cboToolSelect.AddItem "Zoom Out", 2
    cboToolSelect.AddItem "Zoom Rect", 3
    cboToolSelect.AddItem "Hand Tool", 4
    cboToolSelect.AddItem "Magnifier Tool", 5
    cboToolSelect.ListIndex = 0

      
    'Set some Tool tips
    ImagXpress1.ToolbarSetToolTip TOOL_ZoomIn, "Zoom In on the image!"
    ImagXpress1.ToolbarSetToolTip TOOL_ZoomOut, "Zoom Out on the image!"
    ImagXpress1.ToolbarSetToolTip TOOL_ZoomRect, "Zoom In on a selection!"
    ImagXpress1.ToolbarSetToolTip TOOL_Hand, "Grab and Move Image!"
    ImagXpress1.ToolbarSetToolTip TOOL_Mag, "Magnifier Glass!"
    
    'Disable some tools
    ImagXpress1.ToolbarSetToolEnabled TOOL_Select, False
    
    'Set some Tool attributes
    ImagXpress1.ToolSetAttribute TOOL_ZoomIn, TOOLATTR_ZoomInFactor, 25
    ImagXpress1.ToolSetAttribute TOOL_ZoomOut, TOOLATTR_ZoomOutFactor, 25
    
End Sub

Private Sub ImagXpress1_MenuSelect(ByVal Menu As PegasusImagingActiveXImagXpress8Ctl.enumMenu, ByVal Tool As PegasusImagingActiveXImagXpress8Ctl.enumIXTool, ByVal TopMenuID As Long, ByVal SubMenuID As Long, ByVal User1 As Long, ByVal User2 As Long)
    Select Case TopMenuID
    Case 100
Select Case SubMenuID

Case 110
    ImagXpress1.Rotate 90
Case 120
    ImagXpress1.Rotate -90
Case 130
    ImagXpress1.Flip
End Select

Case 200
  If ImagXpress1.MenuGetItemChecked(Menu_Context, TOOL_None, 200, 0) = True And ImagXpress1.ScrollBars = SB_Both Then
     ImagXpress1.MenuSetItemChecked Menu_Context, 0, 200, 0, False, False
     ImagXpress1.ScrollBars = SB_None
  Else
       ImagXpress1.MenuSetItemChecked Menu_Context, 0, 200, 0, True, False
    ImagXpress1.ScrollBars = SB_Both
  End If
  Case 300
    ImagXpress1.Mirror

End Select
End Sub

Private Sub Label2_Click()

End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuFileOpen_Click()
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub


Private Sub LoadFile()
        
        ImagXpress1.FileName = imgFileName
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblerror
   
End Sub
Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub
Private Sub mnuToolbarShow_Click()
     If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblerror
End Sub

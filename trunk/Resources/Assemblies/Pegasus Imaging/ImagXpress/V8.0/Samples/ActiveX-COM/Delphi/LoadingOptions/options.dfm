object Form1: TForm1
  Left = 152
  Top = 116
  Width = 868
  Height = 589
  Caption = 'ImagXpress 8 Loading Options'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = menu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblLoadStatus: TLabel
    Left = 568
    Top = 120
    Width = 64
    Height = 13
    Caption = 'lblLoadStatus'
  end
  object ImagXpress1: TImagXpress
    Left = 24
    Top = 88
    Width = 473
    Height = 233
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    OnProgress = ImagXpress1Progress
    ControlData = {
      0800420000004600410034004500370037004100390037003900460042003300
      3000440032003200330035004400460041003500300035004200310031004400
      31003400360000008609883FA6E129CF03000100000000070000E33000001518
      000008000200000000000300050000001300C0C0C0000B00FFFF09000352E30B
      918FCE119DE300AA004BB85101000000BC02444201000D4D532053616E732053
      6572696609000452E30B918FCE119DE300AA004BB8516C740000AC0200000100
      00006C0000000000000000000000FFFFFFFFFFFFFFFF0000000000000000D084
      00007869000020454D4600000100AC0200001000000004000000000000000000
      0000000000000004000000030000540100000E01000000000000000000000000
      000020300500B01E04001B000000100000000000000000000000520000007001
      000001000000F5FFFFFF00000000000000000000000090010000000000010000
      00004D0053002000530061006E00730020005300650072006900660000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000040FD13001395917C5195917C00000000F81718000020
      000072070000C4AC6D014D0053002000530061006E0073002000530065007200
      690066000000E030887C000015004CFD1300AB37917CC737917C080615000800
      25016D05917C6D05917C8CFD1300C7FE807CCFFE807C00000000F81718000020
      000000002501C4AC6D0100000000F817180058FD130050386D0138386D01B6F0
      0000B6F00000C8AD0240B6C70040000400000100000084376E010A00000086A7
      D6EA00000000B2895FFB128A67B6D8FD1300D823917C86A7D6EA9F0B00000EB9
      67FAEB50D7C61A000000C066D2001C000000F4206E012843D20010FE13000D00
      00006476000800000000250000000C00000001000000180000000C0000000000
      0000260000001C00000002000000000000000100000000000000000000002500
      00000C00000002000000140000000C0000000D00000027000000180000000300
      000000000000FFFFFF0000000000250000000C00000003000000190000000C00
      0000FFFFFF00120000000C00000002000000250000000C000000070000802500
      00000C00000005000080250000000C0000000D0000800E000000140000000000
      0000100000001400000013000000000013000000000009000000000000000000
      00000000000000000B0000000300000000000B0000000B0000000B0000000B00
      00000B0000000300000000000B00000003000000000003000000000003000000
      00000300000000000B0000000B0000000300000000000B0000000B0000000800
      02000000000002000A0002000A00030002000000030001000000030001000000
      03000100000003000D0000000300000000000B00FFFF03000A00000003000100
      00000300010000000B00FFFF03000300000003000A0000000300010000000300
      0000000008000200000000000300000000000300000000000300010000000300
      010000000B000000030000000000030000000000030000000000030000000000
      0B0000000B00FFFF0300000000000300000000000300000000000B00FFFF0B00
      00000300000000000B00FFFF0300010000000B00FFFF0B00FFFF0B00FFFF0300
      102700000B0000000300000000000300000000000B00FFFF0300000000000300
      000000000300000000000300000000000B00FFFF0B0000000300080000000B00
      00000B0000000300010000000800020000000000080002000000000008000200
      000000000300010000000300010000000300010000000B000000030004000000
      0300000000000300000000000B00000003000000000003000000000003000100
      000003000100000003000A000000030000000000020000000300000000000300
      0000000003000000000003000000000003000000000003000000000003000000
      00000B000000030000000000020001000B0000000B0000000B00000003000000
      00000300000000000300000000000B0000000B00000003000000000003000000
      000002000000020000000B00FFFF030000000000020000000500000000000000
      F03F0200FF000B000000030000000000}
  end
  object grpLoadCrop: TGroupBox
    Left = 24
    Top = 352
    Width = 281
    Height = 177
    Caption = 'Load Crop'
    TabOrder = 1
    object lblCropX: TLabel
      Left = 184
      Top = 56
      Width = 39
      Height = 13
      Caption = 'lblCropX'
    end
    object lblCropY: TLabel
      Left = 184
      Top = 80
      Width = 39
      Height = 13
      Caption = 'lblCropY'
    end
    object lblCropWidth: TLabel
      Left = 184
      Top = 104
      Width = 60
      Height = 13
      Caption = 'lblCropWidth'
    end
    object lblCropHeight: TLabel
      Left = 184
      Top = 128
      Width = 63
      Height = 13
      Caption = 'lblCropHeight'
    end
    object chkLoadCropEnabled: TCheckBox
      Left = 24
      Top = 24
      Width = 145
      Height = 25
      Caption = 'Load Crop Enabled'
      TabOrder = 0
      OnClick = chkLoadCropEnabledClick
    end
    object scrCropX: TScrollBar
      Left = 24
      Top = 56
      Width = 153
      Height = 17
      LargeChange = 72
      PageSize = 0
      TabOrder = 1
      OnChange = scrCropXChange
    end
    object scrCropY: TScrollBar
      Left = 24
      Top = 80
      Width = 153
      Height = 17
      PageSize = 0
      TabOrder = 2
      OnChange = scrCropYChange
    end
    object scrCropWidth: TScrollBar
      Left = 24
      Top = 104
      Width = 153
      Height = 17
      PageSize = 0
      TabOrder = 3
      OnChange = scrCropWidthChange
    end
    object scrCropHeight: TScrollBar
      Left = 24
      Top = 128
      Width = 153
      Height = 17
      PageSize = 0
      TabOrder = 4
      OnChange = scrCropHeightChange
    end
  end
  object grpLoadResize: TGroupBox
    Left = 312
    Top = 360
    Width = 281
    Height = 169
    Caption = 'Load Resize'
    TabOrder = 2
    object lblResizeWidth: TLabel
      Left = 200
      Top = 48
      Width = 3
      Height = 13
    end
    object lblResizeHeight: TLabel
      Left = 200
      Top = 80
      Width = 3
      Height = 13
    end
    object chkLoadResizeEnabled: TCheckBox
      Left = 40
      Top = 24
      Width = 169
      Height = 17
      Caption = 'Load Resize Enabled'
      TabOrder = 0
      OnClick = chkLoadResizeEnabledClick
    end
    object chkPreserveAspect: TCheckBox
      Left = 40
      Top = 120
      Width = 177
      Height = 17
      Caption = 'Preserve Aspect'
      TabOrder = 1
    end
    object scrResizeWidth: TScrollBar
      Left = 40
      Top = 48
      Width = 153
      Height = 17
      LargeChange = 50
      Max = 1400
      PageSize = 0
      TabOrder = 2
      OnChange = scrResizeWidthChange
    end
    object scrResizeHeight: TScrollBar
      Left = 40
      Top = 80
      Width = 153
      Height = 17
      LargeChange = 1200
      Max = 1200
      PageSize = 0
      TabOrder = 3
      OnChange = scrResizeHeightChange
    end
  end
  object grpLoadRotated: TGroupBox
    Left = 608
    Top = 368
    Width = 241
    Height = 161
    Caption = 'Load Rotated'
    TabOrder = 3
    object lblLoadRotateSetting: TLabel
      Left = 24
      Top = 48
      Width = 100
      Height = 13
      Caption = 'Load Rotate Settings'
    end
    object chkLoadRotatedEnabled: TCheckBox
      Left = 24
      Top = 24
      Width = 153
      Height = 17
      Caption = 'Load Rotated'
      TabOrder = 0
      OnClick = chkLoadRotatedEnabledClick
    end
    object cboLoadRotated: TComboBox
      Left = 24
      Top = 80
      Width = 153
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'cboLoadRotated'
    end
  end
  object ListBox1: TListBox
    Left = 24
    Top = 8
    Width = 817
    Height = 65
    ItemHeight = 13
    Items.Strings = (
      'This sample demonstrates the following functionality:'
      '1)Using the LoadCrop, LoadResize, and LoadRotated properties.'
      '                                                      '
      
        'Select any of the 3 loading options by clicking the box next to ' +
        'the appropriate option, then open the desired image via File | O' +
        'pen.')
    TabOrder = 4
  end
  object ProgressBar1: TProgressBar
    Left = 568
    Top = 88
    Width = 273
    Height = 25
    TabOrder = 5
  end
  object Button1: TButton
    Left = 576
    Top = 200
    Width = 169
    Height = 33
    Caption = 'Load Image'
    TabOrder = 6
    OnClick = Button1Click
  end
  object cd: TOpenDialog
    Left = 664
    Top = 320
  end
  object menu: TMainMenu
    Left = 704
    Top = 304
    object File1: TMenuItem
      Caption = '&File'
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = Open1Click
      end
      object Quit1: TMenuItem
        Caption = 'Quit'
        OnClick = Quit1Click
      end
    end
    object oolbar1: TMenuItem
      Caption = '&Toolbar'
      object Show1: TMenuItem
        Caption = '&Show'
        OnClick = Show1Click
      end
    end
    object About1: TMenuItem
      Caption = '&About'
      OnClick = About1Click
    end
  end
end

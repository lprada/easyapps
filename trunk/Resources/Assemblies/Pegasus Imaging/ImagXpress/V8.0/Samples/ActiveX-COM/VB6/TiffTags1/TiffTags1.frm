VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Show Tiff Tags"
   ClientHeight    =   9345
   ClientLeft      =   3780
   ClientTop       =   1995
   ClientWidth     =   12420
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9345
   ScaleWidth      =   12420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "TiffTags1.frx":0000
      Left            =   120
      List            =   "TiffTags1.frx":000A
      TabIndex        =   13
      Top             =   120
      Width           =   12015
   End
   Begin VB.TextBox txtSaveFile 
      Height          =   735
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   12
      Top             =   4200
      Width           =   4095
   End
   Begin VB.TextBox txtLoadFile 
      Height          =   855
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   10
      Top             =   2160
      Width           =   4095
   End
   Begin VB.ListBox lstStatus 
      Height          =   2595
      Left            =   8640
      TabIndex        =   6
      Top             =   2040
      Width           =   3615
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3495
      Left            =   4320
      TabIndex        =   4
      Top             =   1440
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   6165
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1055095215
      ErrInfo         =   -1091440122
      Persistence     =   -1  'True
      _cx             =   7011
      _cy             =   6165
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "2) Save and Reload"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   3120
      Width           =   3615
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "1) Load an Image and Show the Tags"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Width           =   3615
   End
   Begin VB.ListBox lstTiffTagList 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   5640
      Width           =   8175
   End
   Begin VB.Label lblSaveFileName 
      Caption         =   "Save File Name:"
      Height          =   495
      Left            =   240
      TabIndex        =   11
      Top             =   3600
      Width           =   1815
   End
   Begin VB.Label lblLoadFileName 
      Caption         =   "Load File Name:"
      Height          =   375
      Left            =   240
      TabIndex        =   9
      Top             =   1680
      Width           =   1575
   End
   Begin VB.Label lblError 
      Height          =   2535
      Left            =   8760
      TabIndex        =   8
      Top             =   6240
      Width           =   3375
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   8760
      TabIndex        =   7
      Top             =   5400
      Width           =   1935
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   495
      Left            =   8640
      TabIndex        =   5
      Top             =   1320
      Width           =   2175
   End
   Begin VB.Label lblStatus 
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   5160
      Width           =   2295
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim sSaveFileName As String
Dim imgParentDir As String

Dim TTag, TLength, TType, TCount
Dim TData

Const TAG_BYTE = 1
Const TAG_ASCII = 2
Const TAG_SHORT = 3
Const TAG_LONG = 4
Const TAG_RATIONAL = 5
Const TAG_SBYTE = 6
Const TAG_UNDEFINE = 7  '* 8 bit byte
Const TAG_SSHORT = 8
Const TAG_SLONG = 9
Const TAG_SRATIONAL = 10
Const TAG_FLOAT = 11
Const TAG_DOUBLE = 12

Private Sub DumpTagInfo(lstTiffTagList As ListBox, ImagXpress1 As ImagXpress)
  Dim RV As Variant ' Rational Values
  Dim RVal() As Long
  Dim sTagInfo As String
  Dim i As Integer
  
  
  sTagInfo = ImagXpress1.TagNumber
  sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagType
  sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagCount
  If ImagXpress1.TagType = TAG_RATIONAL Or ImagXpress1.TagType = TAG_SRATIONAL Then
    RV = ImagXpress1.TagGetDataItem(1)
    sTagInfo = sTagInfo & Chr$(9) & RV(0) & " / " & RV(1)
  Else
    sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagGetDataItem(1)
  End If

  ' If there are more than 1 Items in this tag, then we'll dump out
  ' items 2 and 3 to demonstrate how to get the additional items
  If ImagXpress1.TagType <> TAG_ASCII Then
    If (ImagXpress1.TagCount > 1) Then
      For i = 2 To 2
        If ImagXpress1.TagType = TAG_RATIONAL Or ImagXpress1.TagType = TAG_RATIONAL Then
          RVal = ImagXpress1.TagGetDataItem(i)
          sTagInfo = sTagInfo & Chr$(9) & RVal(0) & " / " & RVal(1)
          Else
          sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagGetDataItem(i)
        End If
      Next i
      If ImagXpress1.TagCount > 2 Then
        sTagInfo = sTagInfo & Chr$(9) & Chr$(9) & "etc..."
      End If
    End If
  End If
  lstTiffTagList.AddItem sTagInfo
End Sub
Private Sub ShowTags(lstTiffTagList As ListBox, ImagXpress1 As ImagXpress)
  lstTiffTagList.Clear
  lstTiffTagList.AddItem "Tag" & Chr$(9) & "Type" & Chr$(9) & "Count" & Chr$(9) & "Data"
  ImagXpress1.TagFirst
  While (ImagXpress1.ImagError = 0)
    DumpTagInfo lstTiffTagList, ImagXpress1
    ImagXpress1.TagNext
  Wend
End Sub

Private Sub cmdExit_Click()
    End
End Sub

Private Sub cmdLoad_Click()
  
  LoadFile
  txtSaveFile.Text = sSaveFileName
  
  lblStatus.Caption = "W: " & ImagXpress1.IWidth & "  H: " & ImagXpress1.IHeight
  
  ' Call Procedure ShowTags to populate lstTiffTagList ListBox
  ShowTags lstTiffTagList, ImagXpress1
  
End Sub


Private Sub cmdSave_Click()
  ' Save the Image
  ImagXpress1.SaveFileName = txtSaveFile.Text
  ImagXpress1.SaveTIFFCompression = TIFF_Uncompressed
  ImagXpress1.SaveFile
  
  
  If ImagXpress1.ImagError = 0 Then
  
  
    DoEvents
    ' Clear everything (for demonstration)
    ImagXpress1.FileName = ""
    ShowTags lstTiffTagList, ImagXpress1
    DoEvents
    ' Load the Saved image back in and show the tags
    ImagXpress1.FileName = txtSaveFile.Text
    
    Err = ImagXpress1.ImagError
     PegasusError Err, lblError
     
    ShowTags lstTiffTagList, ImagXpress1
  
  Else
  
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
  End If
End Sub

Private Sub LoadFile()
   ImagXpress1.FileName = txtLoadFile.Text
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub

Private Sub Form_Load()
    Dim S As String
    
    ImagXpress1.ScrollBars = SB_Both
    
   imgParentDir = App.Path
   ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
   
   imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\shack8.tif"
   sSaveFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\test.tif"
   
   txtLoadFile.Text = imgFileName
   txtSaveFile.Text = sSaveFileName
   
          
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        
        txtLoadFile.Text = imgFileName
                
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
 End Sub


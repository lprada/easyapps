// EventDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Event.h"
#include "EventDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
	
	// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
//{{AFX_MSG_MAP(CAboutDlg)
ON_WM_DESTROY()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventDlg dialog

CEventDlg::CEventDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEventDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEventDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEventDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEventDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CEventDlg, CDialog)
//{{AFX_MSG_MAP(CEventDlg)
ON_WM_SYSCOMMAND()
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(IDButton1, OnButton1)
ON_BN_CLICKED(IDButton2, OnButton2)
ON_BN_CLICKED(IDQuit, OnQuit)
ON_WM_DESTROY()
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
ON_WM_CLOSE()
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEventDlg message handlers

// Include the ImagXpress COM initialization routines
#include "..\Include\ix8_open.cpp"


BOOL CEventDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Add "About..." menu item to system menu.
	
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// Initialize the ImagXpress COM interface.  MUST be done before creating the first object.
	HINSTANCE hDLL = IX_Open();
	
	// Create ImagXpress Object 1
	ppCImagXpress1 = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 30, 90, 50, 50);
	pImagXpress1 = ppCImagXpress1->pImagXpress;
	ppCImagXpress1->SetProgressEvent(Progress);
	ppCImagXpress1->SetNotifyEvent(Notify);
	ppCImagXpress1->SetClickEvent(Click);
	ppCImagXpress1->SetDblClickEvent(DblClick);
	ppCImagXpress1->SetMouseMoveEvent(MouseMove);
	pImagXpress1->AutoSize = ISIZE_ResizeControl;
	pImagXpress1->MousePointer = MP_Pencil1;
	
	// Create ImagXpress Object 2
	ppCImagXpress2 = new CImagXpress((DWORD)this, 2, (long)m_hWnd, 130, 170, 50, 50);
	pImagXpress2 = ppCImagXpress2->pImagXpress;
	ppCImagXpress2->SetProgressEvent(Progress);
	ppCImagXpress2->SetNotifyEvent(Notify);
	ppCImagXpress2->SetClickEvent(Click);
	ppCImagXpress2->SetDblClickEvent(DblClick);
	ppCImagXpress2->SetMouseMoveEvent(MouseMove);
	pImagXpress2->AutoSize = ISIZE_ResizeControl;
	pImagXpress2->MousePointer = MP_Hand1;
	
	// Close the ImagXpress COM initialization.  MUST be done anytime
	// after the first ImagXpress object is created.
	IX_Close(hDLL);
	
	// ImagXpress Initialization
	if (ppCImagXpress1->pImagXpress)
	{
		ppCImagXpress1->pImagXpress->BorderType = BORD_Raised;
		ppCImagXpress1->pImagXpress->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\boat.jpg";
		ppCImagXpress1->pImagXpress->EventSetEnabled(EVENT_PROGRESS,true);
		ppCImagXpress1->pImagXpress->EventSetEnabled(EVENT_NOTIFY,true);
		ppCImagXpress1->pImagXpress->NotifyDelay = 2000;  // 2 seconds
		ppCImagXpress1->pImagXpress->Notify = TRUE;
	}
	
	if (ppCImagXpress2)
	{
		ppCImagXpress2->pImagXpress->BorderType = BORD_Raised;
		ppCImagXpress2->pImagXpress->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\boat.jpg";
	    ppCImagXpress2->pImagXpress->EventSetEnabled(EVENT_PROGRESS,true);
		ppCImagXpress2->pImagXpress->EventSetEnabled(EVENT_NOTIFY,true);
		ppCImagXpress2->pImagXpress->NotifyDelay = 2000;  // 2 seconds
		ppCImagXpress2->pImagXpress->Notify = TRUE;
	}
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEventDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else if ((nID & 0xFFF0) == SC_CLOSE)
	{
		EndDialog(IDOK);	
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CEventDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//Progress Event Handler
HRESULT CEventDlg::Progress(DWORD instancePtr,
							DWORD objID,
							long ImageID, 
							long OperationID, 
							long BytesProcessed, 
							long TotalBytes, 
							long PctDone, 
							VARIANT_BOOL bDone, 
							VARIANT_BOOL bAsync, 
							long Error) 
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	p->SetDlgItemInt(IDC_PCTDONE, PctDone, TRUE);
	CProgressCtrl *pPB = (CProgressCtrl *)p->GetDlgItem(IDC_PROGRESS1);
	pPB->SetPos(PctDone);
	return S_OK;
}

// Notify Event Handler
HRESULT CEventDlg::Notify(DWORD instancePtr, DWORD objID) 
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	p->SetDlgItemText(IDC_PCTDONE, "");
	CProgressCtrl *pPB = (CProgressCtrl *)p->GetDlgItem(IDC_PROGRESS1);
	pPB->SetPos(0);
	return S_OK;
}

// Mouse Move Event Handler
HRESULT CEventDlg::MouseMove(DWORD instancePtr, DWORD objID, short Button, short Shift, OLE_XPOS_PIXELS x, OLE_YPOS_PIXELS y)
{
	char buffer[40];
	CEventDlg *p = (CEventDlg *) instancePtr;
	if (objID == p->ppCImagXpress1->m_objID)
		sprintf(buffer, "(%d,%d) ImagXpress #1", x, y);
	if (objID == p->ppCImagXpress2->m_objID)
		sprintf(buffer, "(%d,%d) ImagXpress #2", x, y);
	p->SetDlgItemText(IDC_MOUSE, buffer);
	return S_OK;
}

// Click Event Handler
HRESULT CEventDlg::Click(DWORD instancePtr, DWORD objID)
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	if (objID == p->ppCImagXpress1->m_objID)
		p->SetDlgItemText(IDC_MESSAGE, "Click - ImagXpress #1");
	if (objID == p->ppCImagXpress2->m_objID)
		p->SetDlgItemText(IDC_MESSAGE, "Click - ImagXpress #2");
	return S_OK;
}

// DblClick Event Handler
HRESULT CEventDlg::DblClick(DWORD instancePtr, DWORD objID)
{
	CEventDlg *p = (CEventDlg *) instancePtr;
	if (objID == p->ppCImagXpress1->m_objID)
		p->SetDlgItemText(IDC_MESSAGE, "Double Click - ImagXpress #1");
	if (objID == p->ppCImagXpress2->m_objID)
		p->SetDlgItemText(IDC_MESSAGE, "Double Click - ImagXpress #2");
	return S_OK;
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEventDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CEventDlg::OnButton1() 
{
	if (strcmp(pImagXpress1->FileName,"..\\..\\..\\..\\..\\..\\Common\\Images\\train.pic")==0)
		pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\boat.jpg";
	else
		pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\train.pic";
}

void CEventDlg::OnButton2() 
{
	if (strcmp(pImagXpress2->FileName,"..\\..\\..\\..\\..\\..\\Common\\Images\\train.pic")==0)
		pImagXpress2->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\boat.jpg";
	else
		pImagXpress2->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\train.pic";
}

void CEventDlg::OnQuit() 
{
	EndDialog(IDOK);
}

void CEventDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// Delete our ImagXpress Objects
	pImagXpress1 = NULL;
	if (ppCImagXpress1) 
		delete ppCImagXpress1;
	
	pImagXpress2 = NULL;
	if (ppCImagXpress2) 
		delete ppCImagXpress2;
	
}

void CEventDlg::OnFileQuit() 
{
	EndDialog(IDOK);
	
}

void CEventDlg::OnAppAbout() 
{
	pImagXpress1->AboutBox();
	
}

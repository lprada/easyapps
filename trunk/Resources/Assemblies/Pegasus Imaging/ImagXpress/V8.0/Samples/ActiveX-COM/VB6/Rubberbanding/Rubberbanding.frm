VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form RubberBanding 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ImagXpress Rubberband"
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   13065
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   13065
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1620
      Left            =   9720
      TabIndex        =   5
      Top             =   2640
      Width           =   2895
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "Rubberbanding.frx":0000
      Left            =   360
      List            =   "Rubberbanding.frx":0013
      TabIndex        =   3
      Top             =   360
      Width           =   12255
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Clear Copied Sections"
      Height          =   375
      Left            =   6240
      TabIndex        =   2
      Top             =   6000
      Width           =   2295
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress2 
      Height          =   3735
      Left            =   5280
      TabIndex        =   1
      Top             =   1920
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   6588
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1021076970
      ErrInfo         =   -766799840
      Persistence     =   -1  'True
      _cx             =   7223
      _cy             =   6588
      AutoSize        =   5
      BackColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      UndoEnabled     =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3735
      Left            =   360
      TabIndex        =   0
      Top             =   1920
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   6588
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1021076970
      ErrInfo         =   -766799840
      Persistence     =   -1  'True
      _cx             =   7223
      _cy             =   6588
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1455
      Left            =   9720
      TabIndex        =   7
      Top             =   5160
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   9720
      TabIndex        =   6
      Top             =   4440
      Width           =   2055
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   495
      Left            =   9720
      TabIndex        =   4
      Top             =   1920
      Width           =   2415
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "RubberBanding"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim a As Integer
Dim band As Boolean

Private Sub Command1_Click()
  
    Xpress2.Area False, 0, 0, 0, 0
        
    Xpress2.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
     If Xpress2.ImagError <> 0 Then
        MsgBox "Image did not load"
    End If
    
    Err = Xpress2.ImagError
    PegasusError Err, lblError
    
End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub
Private Sub Form_Load()
  
  
   ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
   Xpress2.EventSetEnabled EVENT_PROGRESS, True
      
    ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\window.jpg"
    If ImagXpress1.ImagError <> 0 Then
        MsgBox "Image did not load"
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    End If
    
    Xpress2.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
     If Xpress2.ImagError <> 0 Then
        MsgBox "Image did not load"
        Err = Xpress2.ImagError
        PegasusError Err, lblError
    End If
        
    
    band = False
    ImagXpress1.ScrollBars = SB_Both
End Sub

Private Sub ImagXpress1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If band Then
       x = x / Screen.TwipsPerPixelX
       y = y / Screen.TwipsPerPixelY
       ImagXpress1.RubberbandUpdate x, y
    End If
End Sub

Private Sub ImagXpress1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim x1, y1 As Integer
  
    x = x / Screen.TwipsPerPixelX
    y = y / Screen.TwipsPerPixelY
    ImagXpress1.RubberBand True, x, y, False
    band = True
End Sub

Private Sub ImagXpress1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim x1 As Integer
    Dim y1 As Integer
    band = False
    x = x / Screen.TwipsPerPixelX
    y = y / Screen.TwipsPerPixelY
    Xpress2.Area True, ImagXpress1.RubberBandL, ImagXpress1.RubberBandT, ImagXpress1.RubberBandW, ImagXpress1.RubberBandH
    Xpress2.Merge True, MSZ_Crop, MST_Normal, False, 0, 0, 0
    Xpress2.hDIB = ImagXpress1.CopyRubberbandDIB
    
      
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub Xpress2_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    Xpress2.ToolbarActivated = Not Xpress2.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
 End Sub

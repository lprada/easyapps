VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   Caption         =   "Edit Tiff Tags"
   ClientHeight    =   9810
   ClientLeft      =   4140
   ClientTop       =   1620
   ClientWidth     =   12510
   LinkTopic       =   "Form1"
   ScaleHeight     =   9810
   ScaleWidth      =   12510
   StartUpPosition =   1  'CenterOwner
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3735
      Left            =   8640
      TabIndex        =   15
      Top             =   1440
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   6588
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1032394540
      ErrInfo         =   1042695639
      Persistence     =   -1  'True
      _cx             =   6588
      _cy             =   6588
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.TextBox txtSaveFile 
      Height          =   615
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   14
      Top             =   4320
      Width           =   7815
   End
   Begin VB.TextBox txtLoadFile 
      Height          =   735
      Left            =   240
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   2400
      Width           =   7815
   End
   Begin VB.ListBox lstStatus 
      Height          =   1620
      Left            =   8520
      TabIndex        =   8
      Top             =   6480
      Width           =   3735
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "SetTags.frx":0000
      Left            =   240
      List            =   "SetTags.frx":000D
      TabIndex        =   6
      Top             =   240
      Width           =   12015
   End
   Begin VB.CommandButton cmdReload 
      Caption         =   "4) Load saved file - Image + Tags"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   5520
      Width           =   3015
   End
   Begin VB.CommandButton cmdSaveTags 
      Caption         =   "3) Save The Tags"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   5040
      Width           =   4335
   End
   Begin VB.CommandButton cmdAddTag 
      Caption         =   "2) Add a Copyright Tag to Save File Name"
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   3240
      Width           =   3495
   End
   Begin VB.CommandButton cmdShowTags 
      Caption         =   "1) Load Tags"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   1440
      Width           =   3015
   End
   Begin VB.ListBox List1 
      Height          =   2400
      Left            =   120
      TabIndex        =   0
      Top             =   6480
      Width           =   8295
   End
   Begin VB.Label lblSaveFileName 
      Caption         =   "Save File Name:"
      Height          =   375
      Left            =   240
      TabIndex        =   13
      Top             =   3840
      Width           =   2055
   End
   Begin VB.Label lblLoadFileName 
      Caption         =   "Load File Name:"
      Height          =   255
      Left            =   240
      TabIndex        =   12
      Top             =   2040
      Width           =   2175
   End
   Begin VB.Label lblError 
      Height          =   735
      Left            =   8520
      TabIndex        =   10
      Top             =   8760
      Width           =   3735
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   8520
      TabIndex        =   9
      Top             =   8280
      Width           =   2775
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   8520
      TabIndex        =   7
      Top             =   5880
      Width           =   2535
   End
   Begin VB.Label lblTagList 
      Caption         =   "Tag List"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   6120
      Width           =   2295
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Dim TTag, TLength, TType, TCount
Dim TData
Dim FNam As String, NewFNam As String

Const TAG_BYTE = 1
Const TAG_ASCII = 2
Const TAG_SHORT = 3
Const TAG_LONG = 4
Const TAG_RATIONAL = 5
Const TAG_SBYTE = 6
Const TAG_UNDEFINE = 7  '* 8 bit byte
Const TAG_SSHORT = 8
Const TAG_SLONG = 9
Const TAG_SRATIONAL = 10
Const TAG_FLOAT = 11
Const TAG_DOUBLE = 12

Private Sub DumpTagInfo(List1 As ListBox, ImagXpress1 As ImagXpress)
    Dim RV As Variant ' Rational Values
    Dim sTagInfo As String
    Dim i As Integer
    
    sTagInfo = ImagXpress1.TagNumber
    sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagType
    sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagCount
    If ImagXpress1.TagType = TAG_RATIONAL Or ImagXpress1.TagType = TAG_SRATIONAL Then
      RV = ImagXpress1.TagGetDataItem(1)
      sTagInfo = sTagInfo & Chr$(9) & RV(0) & " / " & RV(1)
    Else
      sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagGetDataItem(1)
      
    End If
    
    ' If there are more than 1 Items in this tag, then we'll dump out
    ' items 2 and 3 to demonstrate how to get the additional items
    If ImagXpress1.TagType <> TAG_ASCII Then
      If (ImagXpress1.TagCount > 1) Then
        For i = 2 To 3
          If ImagXpress1.TagType = TAG_RATIONAL Or ImagXpress1.TagType = TAG_RATIONAL Then
            RV = ImagXpress1.TagGetDataItem(i)
            sTagInfo = sTagInfo & Chr$(9) & RV(0) & " / " & RV(1)
          Else
            sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagGetDataItem(i)
          End If
        Next i
        If ImagXpress1.TagCount > 3 Then
          sTagInfo = sTagInfo & Chr$(9) & "etc..."
        End If
      End If
    End If
    List1.AddItem sTagInfo
End Sub
Private Sub ShowTags(List1 As ListBox, ImagXpress1 As ImagXpress)
    List1.Clear
    List1.AddItem "Tag" & Chr$(9) & "Type" & Chr$(9) & "Count" & Chr$(9) & "Data"
    ImagXpress1.TagFirst
    While (ImagXpress1.ImagError = 0)
      DumpTagInfo List1, ImagXpress1
      ImagXpress1.TagNext
    Wend
End Sub

Private Sub Form_Load()
    
    cmdAddTag.Enabled = False
    cmdSaveTags.Enabled = False
    cmdReload.Enabled = False
    
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
      
   
    ' First, let's copy import.tif to MyFile.Tif.
    ' We'll use MyFile.Tif for this demonstration
    FNam = App.Path$ & "\..\..\..\..\..\..\Common\Images\Benefits.tif"
    NewFNam = App.Path$ & "\..\..\..\..\..\..\Common\Images\newtest.tif"
    
    txtLoadFile.Text = FNam
    txtSaveFile.Text = NewFNam
    
        
    ImagXpress1.AutoSize = ISIZE_ResizeImage
    ImagXpress1.ViewAntialias = True
End Sub
Private Sub cmdShowTags_Click()
    
    
    
    CopyFile txtLoadFile.Text, txtSaveFile.Text, False
    
    ' Get the tags without loading the image
    ImagXpress1.FileGetTags txtLoadFile.Text, 1
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    ' Show the Tag set
    ShowTags List1, ImagXpress1

    cmdAddTag.Enabled = True
    txtSaveFile.Enabled = True
   
End Sub

Private Sub cmdAddTag_Click()

    Dim sTagString As String

    ' Add Tag 307 with our copyright info
    sTagString = "Copyright (c) 2003, Pegasus Imaging Corp."
    ImagXpress1.TagAdd 305, TAG_ASCII, Len(sTagString) + 1, sTagString
    
     Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    ' Show the Tag set
    ShowTags List1, ImagXpress1
    
    txtSaveFile.Enabled = False
    cmdSaveTags.Enabled = True
End Sub

Private Sub cmdSaveTags_Click()
    
    txtSaveFile.Locked = True
    
    
    ' Save the tags to the file without disturbing the image
    ImagXpress1.FileSetTags txtSaveFile.Text, 1
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    ' Show the Tag set
    ShowTags List1, ImagXpress1
    
    cmdReload.Enabled = True
End Sub
Private Sub cmdReload_Click()
    ' Load the image to show that the image has not been disturbed
    ' and to verify that the tag we added (tag 305) is in the image's tag set.
    ImagXpress1.FileName = txtSaveFile.Text
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    ' Show the Tag set
    ShowTags List1, ImagXpress1
    
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFilePF(imgParentDir, "Tagged Image Format (*.TIFF)" + vbNullChar + "*.tif;*.tiff" _
                                + vbNullChar + "All Files (*.*)" + vbNullChar + "*.*" + vbNullChar + vbNullChar)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
               
        txtLoadFile.Text = imgFileName
        txtSaveFile.Text = imgFileName
        
        cmdAddTag.Enabled = False
        cmdSaveTags.Enabled = False
        cmdReload.Enabled = False
           
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
End Sub


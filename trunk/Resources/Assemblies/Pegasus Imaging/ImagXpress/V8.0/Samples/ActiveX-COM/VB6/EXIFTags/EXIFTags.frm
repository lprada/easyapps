VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   Caption         =   "EXIF Tag Manipulation"
   ClientHeight    =   8955
   ClientLeft      =   3795
   ClientTop       =   1995
   ClientWidth     =   9510
   LinkTopic       =   "Form1"
   ScaleHeight     =   8955
   ScaleWidth      =   9510
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstdesc 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1620
      ItemData        =   "EXIFTags.frx":0000
      Left            =   240
      List            =   "EXIFTags.frx":0019
      TabIndex        =   9
      Top             =   120
      Width           =   9015
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   975
      Left            =   4320
      TabIndex        =   8
      Top             =   2040
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   1720
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   788222166
      ErrInfo         =   762798220
      Persistence     =   -1  'True
      _cx             =   5741
      _cy             =   1720
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdSet 
      Caption         =   "5)Set Tags"
      Height          =   495
      Left            =   360
      TabIndex        =   5
      Top             =   4560
      Width           =   3615
   End
   Begin MSComDlg.CommonDialog cd 
      Left            =   8760
      Top             =   6480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdDel 
      Caption         =   "3)Delete the CopyRight Tag"
      Enabled         =   0   'False
      Height          =   495
      Left            =   360
      TabIndex        =   4
      Top             =   3120
      Width           =   3615
   End
   Begin VB.CommandButton cmdMod 
      Caption         =   "4)Modify Resolution Tags"
      Enabled         =   0   'False
      Height          =   495
      Left            =   360
      TabIndex        =   3
      Top             =   3840
      Width           =   3615
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "2)Add a CopyRight Tag"
      Enabled         =   0   'False
      Height          =   495
      Left            =   360
      TabIndex        =   2
      Top             =   2520
      Width           =   3615
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "1) Load the EXIF Tags"
      Height          =   495
      Left            =   360
      TabIndex        =   1
      Top             =   1920
      Width           =   3615
   End
   Begin VB.ListBox List1 
      Height          =   3375
      Left            =   240
      TabIndex        =   0
      Top             =   5280
      Width           =   8175
   End
   Begin VB.Label lblerror 
      Height          =   1215
      Left            =   4320
      TabIndex        =   7
      Top             =   3840
      Width           =   4455
   End
   Begin VB.Label lbllasterror 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   4320
      TabIndex        =   6
      Top             =   3240
      Width           =   2295
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuFileSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim TTag, TLength, TType, TCount
Dim TData

Dim imgFileName As String
Dim imgParentDir As String
Dim counter As Integer

Const TAG_BYTE = 1
Const TAG_ASCII = 2
Const TAG_SHORT = 3
Const TAG_LONG = 4
Const TAG_RATIONAL = 5
Const TAG_SBYTE = 6
Const TAG_UNDEFINE = 7  '* 8 bit byte
Const TAG_SSHORT = 8
Const TAG_SLONG = 9
Const TAG_SRATIONAL = 10
Const TAG_FLOAT = 11
Const TAG_DOUBLE = 12

Private Sub cmdDel_Click()
        For counter = 0 To 3
        ImagXpress1.TagLevel = counter
        ImagXpress1.TagDelete 307
    Next
    List1.Clear
    Call getexiftags(ImagXpress1, List1)
End Sub

Private Sub cmdLoad_Click()
cmdSave.Enabled = True
'demonstrate using FileGetTags to load the EXIF tags
ImagXpress1.FileGetTags App.Path & "\..\..\..\..\..\..\Common\Images\exif.jpg", 1

 If ImagXpress1.ImagError <> 0 Then
    MsgBox "Image did not load"
 End If
  'display the EXIF tags
  List1.Clear
  Call getexiftags(ImagXpress1, List1)
  cmdMod.Enabled = True

End Sub
Private Sub cmdMod_Click()
     Dim R(2) As Long
     R(0) = 80
     R(1) = 1
    For counter = 0 To 2
     ImagXpress1.TagLevel = counter
     ImagXpress1.TagFind 282
     ImagXpress1.TagModify 282, TAG_RATIONAL, 1, R
     ImagXpress1.TagFind 283
     ImagXpress1.TagModify 283, TAG_RATIONAL, 1, R
   Next
    List1.Clear
    Call getexiftags(ImagXpress1, List1)
    
End Sub

Private Sub cmdSave_Click()
    Dim tagstring As String

    tagstring = "Copyright (c) 2003, Pegasus Imaging Corp."
    For counter = 0 To 3
        ImagXpress1.TagLevel = counter
        ImagXpress1.TagAdd 307, TAG_ASCII, Len(tagstring) + 1, tagstring
    Next
   List1.Clear
   Call getexiftags(ImagXpress1, List1)
   cmdDel.Enabled = True

End Sub


Private Sub cmdSet_Click()
    'demonstrate using FileGetTags to load the EXIF tags
   ImagXpress1.FileSetTags App.Path & "\..\..\..\..\..\..\Common\Images\exif.jpg", 1
   
  List1.Clear
  'demonstrate using FileGetTags to load the EXIF tags
  ImagXpress1.FileGetTags App.Path & "\..\..\..\..\..\..\Common\Images\exif.jpg", 1

 If ImagXpress1.ImagError <> 0 Then
    MsgBox "Image did not load"
 End If
  'display the EXIF tags
  List1.Clear
  Call getexiftags(ImagXpress1, List1)
  cmdMod.Enabled = True
 
End Sub

Private Sub Form_Load()
    cmdSave.Enabled = False
    
   
End Sub

Function interprettag(tagnum As Variant) As String
   Dim hexval
    hexval = Hex(tagnum)
    
    Select Case tagnum
    
        Case TAG_Artist: interprettag = "Artist"
        Case TAG_BitsPerSample: interprettag = "BitsPerSample"
        Case TAG_CellLength: interprettag = "CellLength"
        Case TAG_CellWidth: interprettag = "CellWidth"
        Case TAG_ColorMap: interprettag = "ColorMap"
        Case TAG_Compression: interprettag = "Compression"
        Case TAG_Copyright: interprettag = "Copyright"
        Case TAG_DateTime: interprettag = "DateTime"
        Case TAG_ExtraSamples: interprettag = "ExtraSamples"
        Case TAG_FillOrder: interprettag = "FillOrder"
        Case TAG_FreeByteCounts: interprettag = "FreeByteCounts"
        Case TAG_FreeOffsets: interprettag = "FreeOffsets"
        Case TAG_GrayResponseCurve: interprettag = "GrayResponseCurve"
        Case TAG_GrayResponseUnit: interprettag = "GrayResponseUnit"
        Case TAG_HostComputer: interprettag = "HostComputer"
        Case TAG_ImageDescription: interprettag = "ImageDescription"
        Case TAG_ImageLength: interprettag = "ImageLength"
        Case TAG_ImageWidth: interprettag = "ImageWidth"
        Case TAG_Make: interprettag = "Make"
        Case TAG_MaxSampleValue: interprettag = "MaxSampleValue"
        Case TAG_MinSampleValue: interprettag = "MinSampleValue"
        Case TAG_Model: interprettag = "Model"
        Case TAG_NewSubfileType: interprettag = "NewSubfileType"
        Case TAG_Orientation: interprettag = "Orientation"
        Case TAG_PhotometricInterpretation: interprettag = "PhotometricInterpretation"
        Case TAG_PlanarConfiguration: interprettag = "PlanarConfiguration"
        Case TAG_ResolutionUnit: interprettag = "ResolutionUnit"
        Case TAG_RowsPerStrip: interprettag = "RowsPerStrip"
        Case TAG_SamplesPerPixel: interprettag = "SamplesPerPixel"
        Case TAG_Software: interprettag = "Software"
        Case TAG_StripByteCounts: interprettag = "StripByteCounts"
        Case TAG_StripOffsets: interprettag = "StripOffsets"
        Case TAG_SubfileType: interprettag = "SubfileType"
        Case TAG_Threshholding: interprettag = "Threshholding"
        Case TAG_XResolution: interprettag = "XResolution"
        Case TAG_YResolution: interprettag = "YResolution"
        
        '/*CCITT BiLevel Encodings:*/
        Case TAG_T4Options: interprettag = "T4Options"
        Case TAG_T6Options: interprettag = "T6Options"
         
        '/*Document Storage and Retrieval:*/
        Case TAG_DocumentName: interprettag = "DocumentName"
        Case TAG_PageName: interprettag = "PageName"
        Case TAG_PageNumber: interprettag = "PageNumber"
        Case TAG_XPosition: interprettag = "XPosition"
        Case TAG_YPosition: interprettag = "YPosition"
         
        '/*Differencing Predictor:*/
        Case TAG_Predictor: interprettag = "Predictor"
         
        '/*Tiled Images:*/
        Case TAG_TileWidth: interprettag = "TileWidth"
        Case TAG_TileLength: interprettag = "TileLength"
        Case TAG_TileOffsets: interprettag = "TileOffsets"
        Case TAG_TileByteCounts: interprettag = "TileByteCounts"
          
        '/*CMYK Images:*/
        Case TAG_InkSet: interprettag = "InkSet"
        Case TAG_NumberOfInks: interprettag = "NumberOfInks"
        Case TAG_InkNames: interprettag = "InkNames"
        Case TAG_DotRange: interprettag = "DotRange"
        Case TAG_TargetPrinter: interprettag = "TargetPrinter"
         
        '/*Halftone Hints:*/
        Case TAG_HalftoneHints: interprettag = "HalftoneHints"
         
        '/*Data Sample Format:*/
        Case TAG_SampleFormat: interprettag = "SampleFormat"
        Case TAG_SMinSampleValue: interprettag = "SMinSampleValue"
        Case TAG_SMaxSampleValue: interprettag = "SMaxSampleValue"
         
        '/*RGB Image Colorimetry:*/
        Case TAG_WhitePoint: interprettag = "WhitePoint"
        Case TAG_PrimaryChromaticities: interprettag = "PrimaryChromaticities"
        Case TAG_TransferFunction: interprettag = "TransferFunction"
        Case TAG_TransferRange: interprettag = "TransferRange"
        Case TAG_ReferenceBlackWhite: interprettag = "ReferenceBlackWhite"
         
        '/*YCbCr Images:*/
        Case TAG_YCbCrCoefficients: interprettag = "YCbCrCoefficients"
        Case TAG_YCbCrSubSampling: interprettag = "YCbCrSubSampling"
        Case TAG_YCbCrPositioning: interprettag = "YCbCrPositioning"
         
        '/*JPEG Compression:*/
        Case TAG_JPEGProc: interprettag = "JPEGProc"
        Case TAG_JPEGInterchangeFormat: interprettag = "JPEGInterchangeFormat"
        Case TAG_JPEGInterchangeFormatLength: interprettag = "JPEGInterchangeFormatLength"
        Case TAG_JPEGRestartInterval: interprettag = "JPEGRestartInterval"
        Case TAG_JPEGLosslessPredictors: interprettag = "JPEGLosslessPredictors"
        Case TAG_JPEGPointTransforms: interprettag = "JPEGPointTransforms"
        Case TAG_JPEGQTables: interprettag = "JPEGQTables"
        Case TAG_JPEGDCTTables: interprettag = "JPEGDCTTables"
        Case TAG_JPEGDCTables: interprettag = "JPEGDCTables"
        Case TAG_JPEGACTables: interprettag = "JPEGACTables"
         
        '// EXIF definitions
        Case TAG_ExifExposureTime: interprettag = "ExifExposureTime"
        Case TAG_ExifFNumber: interprettag = "ExifFNumber"
         
        Case TAG_ExifExposureProg: interprettag = "ExifExposureProg"
        Case TAG_ExifSpectralSense: interprettag = "ExifSpectralSense"
        Case TAG_ExifISOSpeed: interprettag = "ExifISOSpeed"
        Case TAG_ExifOECF: interprettag = "ExifOECF"
         
        Case TAG_ExifInterlace: interprettag = "ExifInterlace"
        Case TAG_ExifTimeZoneOffset: interprettag = "ExifTimeZoneOffset"
        Case TAG_ExifSelfTimerMode: interprettag = "ExifSelfTimerMode"
         
        Case TAG_ExifVer: interprettag = "ExifVer"
        Case TAG_ExifDTOrig: interprettag = "ExifDTOrig"
        Case TAG_ExifDTDigitized: interprettag = "ExifDTDigitized"
         
        Case TAG_ExifCompConfig: interprettag = "ExifCompConfig"
        Case TAG_ExifCompBPP: interprettag = "ExifCompBPP"
         
        Case TAG_ExifShutterSpeed: interprettag = "ExifShutterSpeed"
        Case TAG_ExifAperture: interprettag = "ExifAperture"
        Case TAG_ExifBrightness: interprettag = "ExifBrightness"
        Case TAG_ExifExposureBias: interprettag = "ExifExposureBias"
        Case TAG_ExifMaxAperture: interprettag = "ExifMaxAperture"
        Case TAG_ExifSubjectDist: interprettag = "ExifSubjectDist"
        Case TAG_ExifMeteringMode: interprettag = "ExifMeteringMode"
        Case TAG_ExifLightSource: interprettag = "ExifLightSource"
        Case TAG_ExifFlash: interprettag = "ExifFlash"
        Case TAG_ExifFocalLength: interprettag = "ExifFocalLength"
        '// Case  TAG_ExifFlashEnergy  =&H920B //Redundant
        '// Case  TAG_ExifSpatialFrequencyResponse=&H920C //Redundant
        Case TAG_ExifNoise: interprettag = "ExifNoise"
        Case TAG_ExifImageNumber: interprettag = "ExifImageNumber"
        Case TAG_ExifSecurityClassification: interprettag = "ExifSecurityClassification"
        Case TAG_ExifImageHistory: interprettag = "ExifImageHistory"
        '// Case  TAG_ExifSubjectLocation =&H9214 //Redundant
        '// Case  TAG_ExifExposureIndex =&H9215 //Redundant
        Case TAG_ExifTIFF_EPStandardID: interprettag = "ExifTIFF_EPStandardID"
         
        Case TAG_ExifMakerNote: interprettag = "ExifMakerNote"
        Case TAG_ExifUserComment: interprettag = "ExifUserComment"  '// Want to treat this as a Generic Comment case as well"
        Case TAG_ExifDTSubsec: interprettag = "ExifDTSubsec"
        Case TAG_ExifDTOrigSS: interprettag = "ExifDTOrigSS"
        Case TAG_ExifDTDigSS: interprettag = "ExifDTDigSS"
         
        Case TAG_ExifFPXVer: interprettag = "ExifFPXVer"
        Case TAG_ExifColorSpace: interprettag = "ExifColorSpace"
        Case TAG_ExifPixXDim: interprettag = "ExifPixXDim"
        Case TAG_ExifPixYDim: interprettag = "&ExifPixYDim"
        Case TAG_ExifRelatedWav: interprettag = "ExifRelatedWav"
        Case TAG_ExifInterop: interprettag = "ExifInterop"
        Case TAG_ExifFlashEnergy: interprettag = "ExifFlashEnergy"
        Case TAG_ExifSpatialFR: interprettag = "ExifSpatialFR"
        Case TAG_ExifFocalXRes: interprettag = "ExifFocalXRes"
        Case TAG_ExifFocalYRes: interprettag = "ExifFocalYRes"
        Case TAG_ExifFocalResUnit: interprettag = "ExifFocalResUnit"
        Case TAG_ExifSubjectLoc: interprettag = "ExifSubjectLoc"
        Case TAG_ExifExposureIndex: interprettag = "ExifExposureIndex"
        Case TAG_ExifSensingMethod: interprettag = "ExifSensingMethod"
        Case TAG_ExifFileSource: interprettag = "ExifFileSource"
        Case TAG_ExifSceneType: interprettag = "ExifSceneType"
        Case TAG_ExifCfaPattern: interprettag = "ExifCfaPattern"
        Case Else: interprettag = ""
    
    End Select
End Function
    Public Function interprettype(tagtype As Integer) As String
    Select Case tagtype
        Case 1:     interprettype = "Byte"
        Case 2:     interprettype = "Ascii char"
        Case 3:     interprettype = "Short"
        Case 4:     interprettype = "Long"
        Case 5:     interprettype = "Rational"
        Case 6:     interprettype = "Signed byte"
        Case 7:     interprettype = "8 bit byte"
        Case 8:     interprettype = "Signed Short"
        Case 9:     interprettype = "Signed Long"
        Case 10:    interprettype = "Signed Rational"
        Case 11:    interprettype = "Float"
        Case 12:    interprettype = "Double"
        Case Else:  interprettype = "Unknown"
    End Select
End Function
    
    
Public Sub getexiftags(ImagXpress As ImagXpress, list As ListBox)
    Dim exifnum As String 'Long
    Dim exiftype As String
    Dim exifcount As Variant
    Dim exifdataraw As Variant
    Dim i As Integer
    Dim x As Integer
    
    Dim exifdataparsed As String
    
    list.AddItem "Tag #" & Chr$(9) & Chr$(9) & "Type" & Chr$(9) & Chr$(9) & "Count" & Chr$(9) & Chr$(9) & "Data"
    
    For i = 0 To 3
    ImagXpress.TagLevel = i
    list.AddItem "Property Level: " & ImagXpress.TagLevel
    ImagXpress.TagFirst
        
        While ImagXpress.ImagError = 0 ' -601
            'exifnum = interprettag(ImagXpress1.TagNumber)
            'If exifnum = "" Then
            exifnum = ImagXpress.TagNumber
            'End If
            exiftype = interprettype(ImagXpress.tagtype)
            
            exifdataparsed = ""
            exifcount = ImagXpress.TagCount
            For x = 1 To exifcount
                If (exiftype = "Rational") Or (exiftype = "Signed Rational") Then
                    exifdataraw = ImagXpress.TagGetDataItem(x)
                    exifdataparsed = exifdataparsed & Chr$(9) & exifdataraw(0) & " / " & exifdataraw(1)
                Else
                    exifdataparsed = exifdataparsed & Chr$(9) & ImagXpress.TagGetDataItem(x)
                End If
            Next x
            list.AddItem "  " & exifnum & Chr$(9) & Chr$(9) & exiftype & Chr$(9) & Chr$(9) & exifcount & Chr$(9) & exifdataparsed
            
            ImagXpress.TagNext
        Wend
    Next

End Sub

Private Sub mnuAbout_Click()
        ImagXpress1.AboutBox
End Sub

Private Sub mnuFileOpen_Click()
      Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub LoadFile()
        ImagXpress1.FileName = imgFileName
        Err = ImagXpress1.ImagError
        PegasusError Err, lblerror
End Sub

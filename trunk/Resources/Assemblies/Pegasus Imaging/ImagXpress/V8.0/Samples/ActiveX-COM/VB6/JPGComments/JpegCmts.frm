VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form JpegCmts 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress 8 JPEG Comment Edit "
   ClientHeight    =   7665
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   11835
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7665
   ScaleWidth      =   11835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   8640
      TabIndex        =   14
      Top             =   2280
      Width           =   2895
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "JpegCmts.frx":0000
      Left            =   240
      List            =   "JpegCmts.frx":000A
      TabIndex        =   12
      Top             =   240
      Width           =   11295
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   1935
      Left            =   240
      TabIndex        =   11
      Top             =   1680
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   3413
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   585780856
      ErrInfo         =   1248257963
      Persistence     =   -1  'True
      _cx             =   4471
      _cy             =   3413
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save File and Reload"
      Height          =   735
      Left            =   3120
      TabIndex        =   10
      Top             =   6600
      Width           =   3135
   End
   Begin VB.Frame Frame1 
      Caption         =   "Edit Jpeg Comments "
      Height          =   2055
      Left            =   3000
      TabIndex        =   1
      Top             =   1560
      Width           =   5055
      Begin VB.CommandButton cmdApply 
         Caption         =   "Apply "
         Height          =   375
         Left            =   1800
         TabIndex        =   8
         Top             =   1440
         Width           =   1815
      End
      Begin VB.TextBox txtComment 
         Height          =   285
         Left            =   1080
         TabIndex        =   7
         Top             =   1080
         Width           =   3735
      End
      Begin VB.TextBox txtCommentNbr 
         Height          =   285
         Left            =   1680
         TabIndex        =   5
         Text            =   "1"
         Top             =   720
         Width           =   495
      End
      Begin VB.OptionButton OptDel 
         Caption         =   "Delete"
         Height          =   255
         Left            =   2400
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton optAdd 
         Caption         =   "Add"
         Height          =   255
         Left            =   1440
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Comment:"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Comment Number:"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   720
         Width           =   1455
      End
   End
   Begin VB.ListBox List1 
      Height          =   2400
      Left            =   240
      TabIndex        =   0
      Top             =   4080
      Width           =   7815
   End
   Begin VB.Label lblError 
      Height          =   1935
      Left            =   8760
      TabIndex        =   16
      Top             =   5160
      Width           =   2775
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   8760
      TabIndex        =   15
      Top             =   4440
      Width           =   2415
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   8640
      TabIndex        =   13
      Top             =   1560
      Width           =   2175
   End
   Begin VB.Label Label3 
      Caption         =   "Comments:"
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   3840
      Width           =   975
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "JpegCmts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Private Sub ShowComments()
  
 Dim i As Integer
  List1.Clear
  For i = 1 To ImagXpress1.JPEGCommentCount
    List1.AddItem i & ".  " & ImagXpress1.JPEGGetComment(i)
  Next i
End Sub

Private Sub cmdApply_Click()
  If (optAdd.Value = True) Then
    ImagXpress1.JPEGCommentAdd txtCommentNbr.Text, txtComment.Text
  Else
    ImagXpress1.JPEGCommentDelete txtCommentNbr.Text
  End If
  ShowComments
End Sub

Private Sub cmdSave_Click()

  ImagXpress1.SaveFileName = "test.jpg"
  ImagXpress1.SaveFile
  
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
  
  ImagXpress1.FileName = ImagXpress1.SaveFileName
  ShowComments
End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub
Private Sub Form_Load()

    imgParentDir = App.Path
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\dome.jpg"
    LoadFile
  ShowComments
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
    
       lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
     
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
 End Sub

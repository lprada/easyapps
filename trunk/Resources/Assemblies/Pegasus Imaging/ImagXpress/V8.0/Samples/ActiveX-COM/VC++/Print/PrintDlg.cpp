// PrintDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Print.h"
#include "PrintDlg.h"
#include <winspool.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CPrintDlg dialog

CPrintDlg::CPrintDlg(CWnd* pParent /*=NULL*/)
: CDialog(CPrintDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPrintDlg, CDialog)
//{{AFX_MSG_MAP(CPrintDlg)
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(IDC_PRINT1, OnPrint1)
ON_BN_CLICKED(IDC_PRINT2, OnPrint2)
ON_BN_CLICKED(IDQuit, OnQuit)
ON_WM_DESTROY()
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
	ON_COMMAND(ID_FILE, OnFile)
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_SHOW, OnUpdateToolbarShow)
ON_WM_SYSCOMMAND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintDlg message handlers

// Include the ImagXpress 8 COM initialization routines
#include "..\Include\ix8_open.cpp"


//
// STATICS
//
//0CALS (*.CAL\0*.CAL\0
static TCHAR szFileTitle[]          = _T("Open Image File");
static TCHAR szFilter[]             = _T("Tagged Image Format (*.tif)\0*.tif\0Windows BMP (*.bmp)\0*.bmp\0JPEG (*.jpg)\0*.jpg\0CALS (*.CAL)\0*.CAL\0Windows Device Independent Bitmap (*.DIB)\0*.DIB\0MO:DCA (*.DCA + MOD)\0*.DCA;*.MOD\0ZSoft Multiple Page (*.DCX)\0*.DCX\0GIF (*.GIF)\0*.GIF\0JPEG 2000 (*.JP2)\0*.JP2\0JPEG LS (*.JLS)\0*.JLS\0Lossless JPG (*.LJP)\0*.LJP\0Portable BMP (*.PBM)\0*.PBM\0ZSoft PaintBrush (*.PCX)\0*.PCX\0Portable Graymap (*.PGM)\0*.PGM\0Pegasus PIC (*.PIC)\0*.PIC\0PNG (*.PNG)\0*.PNG\0Portable Pixmap (*.PPM)\0*.PPM\0Truevision TARGA (*.TGA)\0*.TGA\0WSQ (*.WSQ)\0*.WSQ\0JBIG2 File (*.JB2)\0*.JB2\0 All Files (*.*)\0*.*\0 IXSupportedFiles (*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2)\0*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2");
static TCHAR szMediaDirectory[]     = _T("..\\..\\..\\..\\..\\..\\..\\Common\\Images");
static TCHAR szBarcodeInfoFormat[]  = _T("Barcode #%1!d!\n\rBarode value = %2!s!\n\rBarcode type = %3!s!\n\r");
static TCHAR szErrorMessageFormat[] = _T("No barcodes were found. Error = %1!d!");
static TCHAR szMsgBoxTitle[]        = _T("ImagXpress 8 COM Print Sample");

BOOL CPrintDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// Initialize the ImagXpress COM interface.  MUST be done before creating the first object.
	HINSTANCE hDLL = IX_Open();
	
	// Create an ImagXpress Object
	ppCImagXpress = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 100, 100, 200, 200);
	pImagXpress = ppCImagXpress->pImagXpress;
	
	if (pImagXpress)
	{
		pImagXpress->BorderType = BORD_Raised;
		pImagXpress->AutoSize = ISIZE_CropImage;
		pImagXpress->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\bird.jpg";
	}
	
	// Close the ImagXpress COM initialization.  MUST be done anytime
	// after the first ImagXpress object is created.
	IX_Close(hDLL);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPrintDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPrintDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CPrintDlg::OnPrint1() 
{
	
	
	HANDLE hPrinter;
	
	// Compute the Page Width & Height
	int TwipsPerInch = 1440;
	int PageWidth  = 8 * TwipsPerInch;
	int PageHeight = int(10.5 * (float)TwipsPerInch);
	
	// Get the Printer DC
	CPrintDialog PrintDlg(FALSE);
	if (PrintDlg.DoModal() != IDOK)
		return;
	HDC hdcPrinter = PrintDlg.GetPrinterDC();
	
	// Set the Print area and center the image
	pImagXpress->PrinterWidth = 4 * TwipsPerInch;  // 4 Inches Wide
	pImagXpress->PrinterHeight = 3 * TwipsPerInch; // 3 Inches High
	pImagXpress->PrinterLeft = (PageWidth-pImagXpress->PrinterWidth)/2;  // Center the image
	pImagXpress->PrinterTop = (PageHeight-pImagXpress->PrinterHeight)/2; // Center the Image
	
	// Open the Printer
	OpenPrinter(NULL, &hPrinter, NULL);
	DOCINFO dInfo;
	memset ((PVOID)&dInfo, 0, sizeof (DOCINFO));
	dInfo.cbSize = sizeof (DOCINFO);
	dInfo.lpszDocName = NULL;
	StartDoc(hdcPrinter, &dInfo);
	StartPage(hdcPrinter);
	
	// Print the image
	pImagXpress->PrinterhDC = (long)hdcPrinter;
	
	// Cleanup/Close Printer
	EndPage(hdcPrinter);
	EndDoc(hdcPrinter);
	ClosePrinter(hPrinter);
	
}

void CPrintDlg::OnPrint2() 
{
	HANDLE hPrinter;
	
	// Compute the Page Width & Height
	int TwipsPerInch = 1440;
	int PageWidth  = 8 * TwipsPerInch;
	int PageHeight = int(10.5 * (float)TwipsPerInch);
	
	// Get the Printer DC
	CPrintDialog PrintDlg(FALSE);
	if (PrintDlg.DoModal() != IDOK)
		return;
	HDC hdcPrinter = PrintDlg.GetPrinterDC();
	
	// Set the Print area and center the image
	pImagXpress->PrinterWidth = 4 * TwipsPerInch;  // 4 Inches Wide
	pImagXpress->PrinterHeight = 3 * TwipsPerInch; // 3 Inches High
	pImagXpress->PrinterLeft = (PageWidth-pImagXpress->PrinterWidth)/2;  // Center the image
	
	// Open the Printer
	OpenPrinter(NULL, &hPrinter, NULL);
	DOCINFO dInfo;
	memset ((PVOID)&dInfo, 0, sizeof (DOCINFO));
	dInfo.cbSize = sizeof (DOCINFO);
	dInfo.lpszDocName = NULL;
	StartDoc(hdcPrinter, &dInfo);
	StartPage(hdcPrinter);
	
	// Print the first image 2 inches from the top
	pImagXpress->PrinterTop = 2 * TwipsPerInch;    // 2 Inches from Top
	pImagXpress->PrinterhDC = (long)hdcPrinter;
	
	// Print the 2nd image 6 inches from the top
	pImagXpress->PrinterTop = 6 * TwipsPerInch;    // 6 Inches from Top
	pImagXpress->PrinterhDC = (long)hdcPrinter;
	
	// Cleanup/Close Printer
	EndPage(hdcPrinter);
	EndDoc(hdcPrinter);
	ClosePrinter(hPrinter);
	
}

void CPrintDlg::OnQuit() 
{
//	EndDialog(IDOK);
}

void CPrintDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// Delete the ImagXpress Object
	if (pImagXpress)
	{
		pImagXpress = NULL;
		if (ppCImagXpress) 
			delete ppCImagXpress;
	}
	
}

void CPrintDlg::OnFileQuit() 
{
		EndDialog(IDOK);
	
}

void CPrintDlg::OnFile() 
{
	
	//***open file code here	
	CFileDialog ofd(TRUE);
  
    // Open the media subdirectory first
    //
    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];

    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
    CString strInitialDirectory = CString(drive) + CString(dir) 
        + szMediaDirectory;

    ofd.m_ofn.lpstrInitialDir   = strInitialDirectory;
    ofd.m_ofn.lpstrTitle        = szFileTitle;
    ofd.m_ofn.lpstrFilter       = szFilter;

    if (ofd.DoModal() == IDOK)
    {
        CWaitCursor oHourglass;
      	pImagXpress->PutFileName(ofd.m_ofn.lpstrFile);
    }    	

}

void CPrintDlg::OnHelpAbout() 
{
	pImagXpress->AboutBox();
	
}

void CPrintDlg::OnUpdateToolbarShow(CCmdUI* pCmdUI) 
{

	CWnd* pMain = AfxGetMainWnd();
	CMenu* mmenu = pMain->GetMenu();
	CMenu* submenu = mmenu->GetSubMenu(1);

	if(pImagXpress->ToolbarActivated)
	{
		
		submenu->ModifyMenu(ID_TOOLBAR_SHOW, MF_BYCOMMAND,ID_TOOLBAR_SHOW,"&Show");
	}
	else
	{
		submenu->ModifyMenu(ID_TOOLBAR_SHOW, MF_BYCOMMAND,ID_TOOLBAR_SHOW,"&Hide");

	}
	//enable/disable the toolbar
    pImagXpress->ToolbarActivated = ! pImagXpress->ToolbarActivated;


}


    
int CPrintDlg::FindMenuItem(CMenu* Menu, LPCTSTR MenuString)
{
   ASSERT(Menu);
   ASSERT(::IsMenu(Menu->GetSafeHmenu()));

   int count = Menu->GetMenuItemCount();
   for (int i = 0; i < count; i++)
   {
      CString str;
      if (Menu->GetMenuString(i, str, MF_BYPOSITION) &&
         (strcmp(str, MenuString) == 0))
         return i;
   }

   return -1;
}



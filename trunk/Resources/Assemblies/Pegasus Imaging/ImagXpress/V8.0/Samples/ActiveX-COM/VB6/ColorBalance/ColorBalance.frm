VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmColorBalance 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ColorBalance"
   ClientHeight    =   8475
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   12645
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8475
   ScaleWidth      =   12645
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TabDlg.SSTab SSTab1 
      Height          =   3255
      Left            =   6360
      TabIndex        =   7
      Top             =   1800
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   5741
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "AutoColorBalance"
      TabPicture(0)   =   "ColorBalance.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdAutoColorBalance"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "AdjustColorBalance"
      TabPicture(1)   =   "ColorBalance.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblBlueVal"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblGreenVal"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lblRedVal"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lblBlue"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "lblGreen"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Label1"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "lblColor"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cboColor"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "hscrBlue"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "hscrGreen"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "hscrRed"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "cmdAudjustColorBalance"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).ControlCount=   12
      Begin VB.CommandButton cmdAutoColorBalance 
         Caption         =   "Apply"
         Height          =   495
         Left            =   -72840
         TabIndex        =   20
         Top             =   2400
         Width           =   1815
      End
      Begin VB.CommandButton cmdAudjustColorBalance 
         Caption         =   "Apply"
         Height          =   495
         Left            =   2160
         TabIndex        =   12
         Top             =   2640
         Width           =   1695
      End
      Begin VB.HScrollBar hscrRed 
         Height          =   255
         Left            =   1680
         Max             =   100
         Min             =   -100
         TabIndex        =   11
         Top             =   1200
         Width           =   3495
      End
      Begin VB.HScrollBar hscrGreen 
         Height          =   255
         Left            =   1680
         Max             =   100
         Min             =   -100
         TabIndex        =   10
         Top             =   1800
         Width           =   3495
      End
      Begin VB.HScrollBar hscrBlue 
         Height          =   255
         Left            =   1680
         Max             =   100
         Min             =   -100
         TabIndex        =   9
         Top             =   2280
         Width           =   3495
      End
      Begin VB.ComboBox cboColor 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   315
         ItemData        =   "ColorBalance.frx":0038
         Left            =   1680
         List            =   "ColorBalance.frx":0042
         TabIndex        =   8
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblColor 
         Caption         =   "Color:"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Red:"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblGreen 
         Caption         =   "Green:"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1800
         Width           =   855
      End
      Begin VB.Label lblBlue 
         Caption         =   "Blue:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label lblRedVal 
         Height          =   255
         Left            =   5280
         TabIndex        =   15
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label lblGreenVal 
         Height          =   255
         Left            =   5280
         TabIndex        =   14
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label lblBlueVal 
         Height          =   255
         Left            =   5280
         TabIndex        =   13
         Top             =   2280
         Width           =   495
      End
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "ColorBalance.frx":004C
      Left            =   240
      List            =   "ColorBalance.frx":0056
      TabIndex        =   6
      Top             =   120
      Width           =   12135
   End
   Begin VB.ListBox lstStatus 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Left            =   6360
      TabIndex        =   2
      Top             =   5880
      Width           =   3135
   End
   Begin VB.CommandButton cmdReloadImage 
      Caption         =   "Reload Image"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7800
      TabIndex        =   1
      Top             =   960
      Width           =   2895
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   7215
      Left            =   240
      TabIndex        =   0
      Top             =   960
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   12726
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   312374311
      ErrInfo         =   -1320369294
      Persistence     =   -1  'True
      _cx             =   10398
      _cy             =   12726
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6480
      TabIndex        =   5
      Top             =   5280
      Width           =   1815
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9960
      TabIndex        =   4
      Top             =   5280
      Width           =   2055
   End
   Begin VB.Label lblError 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   9960
      TabIndex        =   3
      Top             =   5880
      Width           =   2535
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "Open"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuReloadCurrentImage 
         Caption         =   "&Reload Current Image..."
         Shortcut        =   ^R
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmColorBalance"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
   
   If ImagXpress1.IBPP <> 24 Then
        MsgBox ("Image must be 24-bit for sample methods. Converting image to 24-bit.")
        ImagXpress1.ColorDepth 24, IPAL_Fixed, DI_None
   End If
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
   
End Sub

Private Sub cmdAudjustColorBalance_Click()

      With ImagXpress1
        .AdjustColorBalance cboColor.ItemData(cboColor.ListIndex) _
        , hscrRed.Value, hscrGreen.Value, hscrBlue.Value
        
      End With
        
      Err = ImagXpress1.ImagError
      PegasusError Err, lblError
      
       
End Sub

Private Sub cmdAutoColorBalance_Click()
    ImagXpress1.AutoColorBalance
    Err = ImagXpress1.ImagError
      PegasusError Err, lblError
End Sub

Private Sub cmdAutoLightNess_Click()
    ImagXpress1.AutoLightness
    Err = ImagXpress1.ImagError
      PegasusError Err, lblError
End Sub

Private Sub cmdReloadImage_Click()
     LoadFile
End Sub

Private Sub Form_Load()

    imgParentDir = App.Path
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    ImagXpress1.ScrollBars = SB_Both
    
    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\Auto_Balance1.JPG"
     
    LoadFile
   
    With cboColor
        .Clear
        .AddItem "0", 0
        .ItemData(0) = 0
        .AddItem "1", 1
        .ItemData(1) = 1
        
        .ListIndex = 0
    End With
    
       
    lblBlueVal.Caption = hscrBlue.Value
    lblGreenVal.Caption = hscrGreen.Value
    lblRedVal.Caption = hscrRed.Value
     
End Sub

Private Sub hscrBlue_Change()
    lblBlueVal = hscrBlue.Value
End Sub

Private Sub hscrGreen_Change()
    lblGreenVal = hscrGreen.Value
End Sub

Private Sub hscrRed_Change()
    lblRedVal = hscrRed.Value
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuReloadCurrentImage_Click()
    LoadFile
End Sub

Private Sub mnuToolbarShow_Click()
    
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
End Sub



VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form AllEvents 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress 8 Events "
   ClientHeight    =   6420
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   9675
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6420
   ScaleWidth      =   9675
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "AllEvents.frx":0000
      Left            =   240
      List            =   "AllEvents.frx":000A
      TabIndex        =   24
      Top             =   120
      Width           =   9255
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3495
      Left            =   120
      TabIndex        =   23
      Top             =   840
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   6165
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   1571515083
      ErrInfo         =   -454147074
      Persistence     =   -1  'True
      _cx             =   7011
      _cy             =   6165
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   6240
      TabIndex        =   20
      Top             =   1560
      Width           =   3135
   End
   Begin VB.CheckBox chkScroll 
      Caption         =   "Scroll"
      Height          =   255
      Left            =   4200
      TabIndex        =   18
      Top             =   5280
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CommandButton cmdClearChecks 
      Caption         =   "Clear Checks"
      Height          =   495
      Left            =   3960
      TabIndex        =   17
      Top             =   5760
      Width           =   2175
   End
   Begin VB.CheckBox chkOleDragDrop 
      Caption         =   "OLE Drag Drop"
      Height          =   255
      Left            =   4200
      TabIndex        =   16
      Top             =   4920
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkOleDragOver 
      Caption         =   "OLE Drag Over"
      Height          =   255
      Left            =   4200
      TabIndex        =   15
      Top             =   4560
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkNotify 
      Caption         =   "Notify"
      Height          =   255
      Left            =   4200
      TabIndex        =   14
      Top             =   4200
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkProgress 
      Caption         =   "Progress"
      Height          =   255
      Left            =   4200
      TabIndex        =   13
      Top             =   3840
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CommandButton cmdForceNotify 
      Caption         =   "Force Notify or Progress"
      Height          =   495
      Left            =   1800
      TabIndex        =   12
      Top             =   5760
      Width           =   1695
   End
   Begin VB.CommandButton cmdSetFocus 
      Caption         =   "Set Focus"
      Height          =   495
      Left            =   120
      TabIndex        =   11
      Top             =   5760
      Width           =   1695
   End
   Begin VB.CheckBox chkKeyDown 
      Caption         =   "Key Down"
      Height          =   255
      Left            =   4200
      TabIndex        =   10
      Top             =   3480
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkKeyPress 
      Caption         =   "Key Press"
      Height          =   255
      Left            =   4200
      TabIndex        =   9
      Top             =   3120
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkKeyUp 
      Caption         =   "Key Up"
      Height          =   255
      Left            =   4200
      TabIndex        =   8
      Top             =   2760
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkMouseUp 
      Caption         =   "Mouse Up"
      Height          =   255
      Left            =   4200
      TabIndex        =   7
      Top             =   2400
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkMouseMove 
      Caption         =   "Mouse Move"
      Height          =   255
      Left            =   4200
      TabIndex        =   6
      Top             =   2040
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkMouseDown 
      Caption         =   "Mouse Down"
      Height          =   255
      Left            =   4200
      TabIndex        =   5
      Top             =   1680
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkDblClick 
      Caption         =   "Double Click"
      Height          =   255
      Left            =   4200
      TabIndex        =   4
      Top             =   1320
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   4920
      Width           =   1695
   End
   Begin VB.CheckBox chkClick 
      Caption         =   "Click"
      Height          =   255
      Left            =   4200
      TabIndex        =   0
      Top             =   960
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.Label lblError 
      Height          =   2175
      Left            =   6240
      TabIndex        =   22
      Top             =   4200
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   6240
      TabIndex        =   21
      Top             =   3600
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   6240
      TabIndex        =   19
      Top             =   960
      Width           =   1815
   End
   Begin VB.Label lblEventLbl 
      Caption         =   "Event:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   4560
      Width           =   495
   End
   Begin VB.Label lblEvent 
      Height          =   375
      Left            =   720
      TabIndex        =   1
      Top             =   4560
      Width           =   2415
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "AllEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Dim imgFileName As String
Dim imgParentDir As String

Private Sub chkClick_Click()
  ImagXpress1.EventSetEnabled EVENT_CLICK, chkClick.Value = 1
End Sub

Private Sub chkDblClick_Click()
  ImagXpress1.EventSetEnabled EVENT_DBLCLICK, chkDblClick.Value = 1
End Sub

Private Sub chkMouseDown_Click()
    ImagXpress1.EventSetEnabled EVENT_MOUSEDOWN, chkMouseDown.Value = 1
End Sub

Private Sub chkMouseMove_Click()
    ImagXpress1.EventSetEnabled EVENT_MOUSEMOVE, chkMouseMove.Value = 1
End Sub

Private Sub chkMouseUp_Click()
  ImagXpress1.EventSetEnabled EVENT_MOUSEUP, chkMouseUp.Value = 1
End Sub

Private Sub chkNotify_Click()
  ImagXpress1.EventSetEnabled EVENT_NOTIFY, chkNotify.Value = 1
End Sub

Private Sub chkOleDragDrop_Click()
  ImagXpress1.EventSetEnabled EVENT_OLEDRAGDROP, chkOleDragDrop.Value = 1
End Sub

Private Sub chkOleDragOver_Click()
 ImagXpress1.EventSetEnabled EVENT_OLEDRAGOVER, chkOleDragOver.Value = 1
End Sub

Private Sub chkProgress_Click()
  ImagXpress1.EventSetEnabled EVENT_PROGRESS, chkProgress.Value = 1
End Sub

Private Sub chkScroll_Click()
  ImagXpress1.EventSetEnabled EVENT_SCROLL, chkScroll.Value = 1
End Sub

Private Sub cmdClear_Click()
  lblEvent.Caption = ""
End Sub

Private Sub chkKeyDown_Click()
  ImagXpress1.EventSetEnabled EVENT_KEYDOWN, chkKeyDown.Value = 1
End Sub

Private Sub chkKeyPress_Click()
  ImagXpress1.EventSetEnabled EVENT_KEYPRESS, chkKeyPress.Value = 1
End Sub

Private Sub chkKeyUp_Click()
  ImagXpress1.EventSetEnabled EVENT_KEYUP, chkKeyUp.Value = 1
End Sub

Private Sub cmdClearChecks_Click()
  chkClick.Value = 0
  chkDblClick.Value = 0
  chkMouseDown.Value = 0
  chkMouseMove.Value = 0
  chkMouseUp.Value = 0
  chkKeyUp.Value = 0
  chkKeyPress.Value = 0
  chkKeyDown.Value = 0
  chkProgress.Value = 0
  chkNotify.Value = 0
  chkOleDragOver.Value = 0
  chkOleDragDrop.Value = 0
  chkScroll.Value = 0
End Sub

Private Sub cmdForceNotify_Click()
  ImagXpress1.NotifyDelay = 0
  ImagXpress1.Notify = True
End Sub

Private Sub cmdSetFocus_Click()
  ImagXpress1.SetFocus
End Sub

Private Sub Command1_Click()
  ImagXpress1.Sharpen 1
  ImagXpress1.Undo
End Sub

Private Sub Form_Load()

  ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
  imgParentDir = App.Path

  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
  
  imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\benefits.tif"
  LoadFile
  
End Sub

Private Sub ImagXpress1_Click()
  lblEvent.Caption = "Click!"
End Sub

Private Sub ImagXpress1_DblClick()
  lblEvent.Caption = "Double Click!"
End Sub

Private Sub ImagXpress1_DragOver(Source As Control, x As Single, y As Single, State As Integer)
  lblEvent.Caption = "Drag Over"
End Sub

Private Sub ImagXpress1_KeyDown(KeyCode As Integer, Shift As Integer)
  lblEvent.Caption = "Key Down!"
End Sub

Private Sub ImagXpress1_KeyPress(KeyASCII As Integer)
  lblEvent.Caption = "Key Press!"
End Sub

Private Sub ImagXpress1_KeyUp(KeyCode As Integer, Shift As Integer)
  lblEvent.Caption = "Key Up!"
End Sub

Private Sub ImagXpress1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  lblEvent.Caption = "Mouse Down!"
End Sub

Private Sub ImagXpress1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  lblEvent.Caption = "Mouse Move!"
End Sub

Private Sub ImagXpress1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
  lblEvent.Caption = "Mouse Up!"
End Sub

Private Sub ImagXpress1_Notify()
  lblEvent.Caption = "Notify!"
End Sub

Private Sub ImagXpress1_OLEDragDrop(ByVal Data As Variant, ByVal Format As Integer, ByVal Effect As Long, ByVal Button As Long, ByVal Shift As Long, ByVal x As Integer, ByVal y As Integer)
  lblEvent.Caption = "OLE DragDrop!"
End Sub

Private Sub ImagXpress1_OLEDragOver(ByVal Data As Variant, ByVal Format As Integer, ByVal Effect As Long, ByVal Shift As Long, ByVal x As Integer, ByVal y As Integer, ByVal State As Long)
  lblEvent.Caption = "OLE DragOver!"
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
   
    lblEvent.Caption = "Progress!"
      
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub ImagXpress1_Scroll(ByVal Bar As Integer, ByVal Action As Integer)
  lblEvent.Caption = "Scroll " & Bar & " " & Action
End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub

Private Sub mnuAbout_Click()
        ImagXpress1.AboutBox
        
End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub


VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "LoadOptions Sample"
   ClientHeight    =   9015
   ClientLeft      =   3465
   ClientTop       =   2085
   ClientWidth     =   10845
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9015
   ScaleWidth      =   10845
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox lstStatus 
      Height          =   2010
      Left            =   6720
      TabIndex        =   31
      Top             =   2280
      Width           =   3855
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "LoadingOptions.frx":0000
      Left            =   240
      List            =   "LoadingOptions.frx":0010
      TabIndex        =   30
      Top             =   120
      Width           =   10335
   End
   Begin VB.Frame fraLoadRotated 
      Caption         =   "Load Rotated"
      Height          =   2415
      Left            =   7800
      TabIndex        =   4
      Top             =   6480
      Width           =   2535
      Begin VB.ComboBox cboLoadRotate 
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1080
         Width           =   1695
      End
      Begin VB.CheckBox chkRotateEnabled 
         Caption         =   "Rotate Image on Load"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   2055
      End
      Begin VB.Label lblRotateSetting 
         Alignment       =   1  'Right Justify
         Caption         =   "Rotate Setting:"
         Height          =   255
         Left            =   360
         TabIndex        =   15
         Top             =   840
         Width           =   1215
      End
   End
   Begin VB.Frame fraLoadResize 
      Caption         =   "Load Resize"
      Height          =   2415
      Left            =   3840
      TabIndex        =   3
      Top             =   6480
      Width           =   3855
      Begin VB.HScrollBar scrResizeHeight 
         Height          =   255
         Left            =   1440
         Max             =   1200
         TabIndex        =   23
         Top             =   1200
         Width           =   1575
      End
      Begin VB.HScrollBar scrResizeWidth 
         Height          =   255
         Left            =   1440
         Max             =   1400
         TabIndex        =   22
         Top             =   840
         Width           =   1575
      End
      Begin VB.CheckBox chkPreserveAspect 
         Caption         =   "Preserve Aspect Ratio"
         Height          =   255
         Left            =   600
         TabIndex        =   16
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CheckBox chkResizeEnabled 
         Caption         =   "Resize Image on Load"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label lblResizeHeightVal 
         Height          =   255
         Left            =   3120
         TabIndex        =   29
         Top             =   1200
         Width           =   615
      End
      Begin VB.Label lblResizeWidthVal 
         Height          =   255
         Left            =   3120
         TabIndex        =   28
         Top             =   840
         Width           =   615
      End
      Begin VB.Label lblResizeHeight 
         Alignment       =   1  'Right Justify
         Caption         =   "Resize Height:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblResizeWidth 
         Alignment       =   1  'Right Justify
         Caption         =   "Resize Width:"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   1215
      End
   End
   Begin VB.Frame fraLoadCrop 
      Caption         =   "Load Crop"
      Height          =   2415
      Left            =   120
      TabIndex        =   2
      Top             =   6480
      Width           =   3615
      Begin VB.HScrollBar scrCropHeight 
         Height          =   255
         Left            =   1200
         Max             =   1200
         TabIndex        =   21
         Top             =   1920
         Width           =   1695
      End
      Begin VB.HScrollBar scrCropWidth 
         Height          =   255
         Left            =   1200
         Max             =   1200
         TabIndex        =   20
         Top             =   1560
         Width           =   1695
      End
      Begin VB.HScrollBar scrCropY 
         Height          =   255
         Left            =   1200
         Max             =   400
         TabIndex        =   19
         Top             =   1200
         Width           =   1695
      End
      Begin VB.HScrollBar scrCropX 
         Height          =   255
         Left            =   1200
         Max             =   400
         TabIndex        =   18
         Top             =   840
         Width           =   1695
      End
      Begin VB.CheckBox chkLoadCropEnabled 
         Caption         =   "Crop Image on Load"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label lblCropHeightVal 
         Height          =   255
         Left            =   3000
         TabIndex        =   27
         Top             =   1920
         Width           =   495
      End
      Begin VB.Label lblCropWidthVal 
         Height          =   255
         Left            =   3000
         TabIndex        =   26
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label lblCropYVal 
         Height          =   255
         Left            =   3000
         TabIndex        =   25
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label lblCropXVal 
         Height          =   255
         Left            =   3000
         TabIndex        =   24
         Top             =   840
         Width           =   495
      End
      Begin VB.Label lblCropHeight 
         Alignment       =   1  'Right Justify
         Caption         =   "Crop Height:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label lblCropWidth 
         Alignment       =   1  'Right Justify
         Caption         =   "Crop Width:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label lblCropY 
         Alignment       =   1  'Right Justify
         Caption         =   "Crop Y:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1200
         Width           =   975
      End
      Begin VB.Label lblCropX 
         Alignment       =   1  'Right Justify
         Caption         =   "Crop X:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdLoadImage 
      Caption         =   "Load Image"
      Height          =   375
      Left            =   6720
      TabIndex        =   1
      Top             =   1320
      Width           =   3375
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4335
      Left            =   240
      TabIndex        =   0
      Top             =   1320
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   7646
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   983369827
      ErrInfo         =   194338428
      Persistence     =   -1  'True
      _cx             =   11245
      _cy             =   7646
      AutoSize        =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1335
      Left            =   6720
      TabIndex        =   33
      Top             =   5040
      Width           =   3855
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   6720
      TabIndex        =   32
      Top             =   4440
      Width           =   2055
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   255
      Left            =   6720
      TabIndex        =   17
      Top             =   1800
      Width           =   1095
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Private imageWidth, imageHeight As Integer


Private Sub chkLoadCropEnabled_Click()
    If chkLoadCropEnabled Then
        Call setCropControlsEnabled(True)
    Else
        Call setCropControlsEnabled(False)
    End If
End Sub

Private Sub chkResizeEnabled_Click()
    If chkResizeEnabled Then
        Call setResizeControlsEnabled(True)
    Else
        Call setResizeControlsEnabled(False)
    End If
End Sub

Private Sub chkRotateEnabled_Click()
    If chkRotateEnabled Then
        Call setRotateControlsEnabled(True)
    Else
        Call setRotateControlsEnabled(False)
    End If
End Sub



Private Sub cmdLoadImage_Click()
    
    Dim Err As Long
    Dim Txt As String
    
    If cmdLoadImage.Caption = "Load Image" Then
        cmdLoadImage.Caption = "Reload Image"
    End If
    
    If chkLoadCropEnabled Then
        ' Set LoadCrop properties, to crop image as it's loaded
        ImagXpress1.LoadCropEnabled = True
        ImagXpress1.LoadCropX = Int(scrCropX.Value)
        ImagXpress1.LoadCropY = Int(scrCropY.Value)
        ImagXpress1.LoadCropWidth = Int(scrCropWidth.Value)
        ImagXpress1.LoadCropHeight = Int(scrCropHeight.Value)
    Else
        ImagXpress1.LoadCropEnabled = False
    End If

    If chkResizeEnabled Then
        ' Set LoadResize properties, to resize image as it's loaded
        ImagXpress1.LoadResizeEnabled = True
        ImagXpress1.LoadResizeWidth = Int(scrResizeWidth.Value)
        ImagXpress1.LoadResizeHeight = Int(scrResizeHeight.Value)
        ImagXpress1.LoadResizeMaintainAspectRatio = chkPreserveAspect
    Else
        ImagXpress1.LoadResizeEnabled = False
    End If
    
    If chkRotateEnabled Then
        ' Set LoadRotated property, to rotate image as it's loaded
        ImagXpress1.LoadRotated = cboLoadRotate.ListIndex
    Else
        ImagXpress1.LoadRotated = LR_NONE
    End If

    LoadFile
   
    
    ' Check for error and report status of image load
    If ImagXpress1.ImagError = 0 Then
        
        If ImagXpress1.LoadCropEnabled Then
                  
            lstStatus.AddItem " Crop X = " & ImagXpress1.LoadCropX & ", Y = " & ImagXpress1.LoadCropY & ", Wid = " & ImagXpress1.LoadCropWidth & ", Hgt = " & ImagXpress1.LoadCropHeight
            lstStatus.ListIndex = lstStatus.ListCount - 1
        
        Else
            
            lstStatus.AddItem " LoadCrop Disabled"
            lstStatus.ListIndex = lstStatus.ListCount - 1
            
        End If
        If ImagXpress1.LoadResizeEnabled Then
                        
            lstStatus.AddItem " Resize: " & ImagXpress1.LoadResizeWidth & "x" & ImagXpress1.LoadResizeHeight & ", Preserve Aspect: " & ImagXpress1.LoadResizeMaintainAspectRatio
            lstStatus.ListIndex = lstStatus.ListCount - 1
        
        
        Else
            
            lstStatus.AddItem " LoadResize Disabled"
            lstStatus.ListIndex = lstStatus.ListCount - 1
            
        End If
        
        
        lstStatus.AddItem " Rotate: " & ImagXpress1.LoadRotated
        lstStatus.ListIndex = lstStatus.ListCount - 1
    Else
    
        lstStatus.AddItem "Load Failed! Error: " & ImagXpress1.ImagError
        lstStatus.ListIndex = lstStatus.ListCount - 1
        
    End If
    
    
End Sub

Private Sub initRotateSettingsCombo()

    ' Populate ComboBox with settings for LoadRotated method
    cboLoadRotate.AddItem "None"
    cboLoadRotate.AddItem "90 Degrees"
    cboLoadRotate.AddItem "180 Degrees"
    cboLoadRotate.AddItem "270 Degrees"
    cboLoadRotate.AddItem "Flip"
    cboLoadRotate.AddItem "Mirror"
    
    ' Set selected list index to None
    cboLoadRotate.ListIndex = 0

End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub
Private Sub Form_Load()
    
    
    imgParentDir = App.Path
       
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError

    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\window.jpg"
    
   
    ' Load filename to get image width and height. These values are
    ' used to set maximum values for the scroll bars used in this sample
    LoadFile
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    If ImagXpress1.ImagError <> 0 Then
        Exit Sub
    End If
    imageWidth = ImagXpress1.IWidth
    imageHeight = ImagXpress1.IHeight
    ImagXpress1.FileName = ""
    
    ' Set max values for Crop scroll bars. Setting the crop properties
    ' outside the boundaries of an image will return an error to the
    ' ImagError property
    scrCropHeight.Max = imageHeight - 1
    scrCropWidth.Max = imageWidth - 1
    scrCropX.Max = imageWidth - 1
    scrCropY.Max = imageHeight - 1
    
    'set min width and height to 1, as 0 will return a -12(Invalid Option)
    'error to ImagError
    scrCropHeight.Min = 1
    scrCropWidth.Min = 1
    scrResizeHeight.Min = 1
    scrResizeWidth.Min = 1
    
    ImagXpress1.AutoSize = ISIZE_CropImage
    ImagXpress1.ScrollBars = SB_Both
    
    Call initRotateSettingsCombo
    Call setCropControlsEnabled(False)
    Call setResizeControlsEnabled(False)
    Call setRotateControlsEnabled(False)
    
    lblCropHeightVal.Caption = scrCropHeight.Value
    lblCropWidthVal.Caption = scrCropWidth.Value
    lblCropXVal.Caption = scrCropX.Value
    lblCropYVal.Caption = scrCropY.Value
    
   
      
     lstStatus.AddItem "No Image Loaded!"
     lstStatus.ListIndex = lstStatus.ListCount - 1
     lstStatus.AddItem "Click the Load Image button to load an image "
     lstStatus.ListIndex = lstStatus.ListCount - 1
     lstStatus.AddItem "with the settings below."
     lstStatus.ListIndex = lstStatus.ListCount - 1
     
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
       
        lstStatus.AddItem Str(PctDone) + "% Loaded."
        If bDone Then
            lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
        End If
        lstStatus.ListIndex = lstStatus.ListCount - 1
   
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        'LoadFile
        
        cmdLoadImage_Click
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
 End Sub


Private Sub setCropControlsEnabled(bEnabled As Boolean)
    
    scrCropHeight.Enabled = bEnabled
    scrCropWidth.Enabled = bEnabled
    scrCropX.Enabled = bEnabled
    scrCropY.Enabled = bEnabled
    
    lblCropHeight.Enabled = bEnabled
    lblCropWidth.Enabled = bEnabled
    lblCropX.Enabled = bEnabled
    lblCropY.Enabled = bEnabled
    
    lblCropHeightVal.Enabled = bEnabled
    lblCropWidthVal.Enabled = bEnabled
    lblCropXVal.Enabled = bEnabled
    lblCropYVal.Enabled = bEnabled
        
    scrCropHeight.Value = 100
    scrCropWidth.Value = 100
    scrCropX.Value = 30
    scrCropY.Value = 30
   
End Sub

Private Sub setResizeControlsEnabled(bEnabled As Boolean)
    
    scrResizeHeight.Enabled = bEnabled
    scrResizeWidth.Enabled = bEnabled
    
    lblResizeHeight.Enabled = bEnabled
    lblResizeWidth.Enabled = bEnabled
    
    lblResizeHeightVal.Enabled = bEnabled
    lblResizeWidthVal.Enabled = bEnabled
    
    chkPreserveAspect.Enabled = bEnabled
    
    scrResizeHeight = 150
    scrResizeWidth = 200
        
End Sub

Private Sub setRotateControlsEnabled(bEnabled As Boolean)
    
    cboLoadRotate.Enabled = bEnabled
    lblRotateSetting.Enabled = bEnabled

End Sub

Private Sub scrCropHeight_Change()
    lblCropHeightVal.Caption = scrCropHeight.Value
    If (scrCropY.Value + scrCropHeight.Value >= imageHeight) Then
        scrCropY.Value = imageHeight - scrCropHeight.Value
    End If
End Sub

Private Sub scrCropWidth_Change()
    lblCropWidthVal.Caption = scrCropWidth.Value
    If (scrCropX.Value + scrCropWidth.Value >= imageWidth) Then
        scrCropX.Value = imageWidth - scrCropWidth.Value
    End If
End Sub

Private Sub scrCropX_Change()
    lblCropXVal.Caption = scrCropX.Value
    If (scrCropX.Value + scrCropWidth.Value >= imageWidth) Then
        scrCropWidth.Value = imageWidth - scrCropX.Value
    End If
End Sub

Private Sub scrCropY_Change()
    lblCropYVal.Caption = scrCropY.Value
    If (scrCropY.Value + scrCropHeight.Value >= imageHeight) Then
        scrCropHeight.Value = imageHeight - scrCropY.Value
    End If
End Sub

Private Sub scrResizeHeight_Change()
    lblResizeHeightVal.Caption = scrResizeHeight.Value
End Sub

Private Sub scrResizeWidth_Change()
    lblResizeWidthVal.Caption = scrResizeWidth.Value
End Sub

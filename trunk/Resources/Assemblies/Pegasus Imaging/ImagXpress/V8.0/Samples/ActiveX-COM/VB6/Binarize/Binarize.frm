VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Binarize 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Binarize & Thresholding Demonstration"
   ClientHeight    =   9750
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   14220
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9750
   ScaleWidth      =   14220
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   2985
      Left            =   11280
      TabIndex        =   38
      Top             =   1680
      Width           =   2775
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "Binarize.frx":0000
      Left            =   240
      List            =   "Binarize.frx":0010
      TabIndex        =   36
      Top             =   120
      Width           =   13815
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   4215
      Left            =   240
      TabIndex        =   33
      Top             =   1320
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   7435
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1926191384
      ErrInfo         =   -1838908169
      Persistence     =   -1  'True
      _cx             =   9128
      _cy             =   7435
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      UndoEnabled     =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4215
      Left            =   5760
      TabIndex        =   32
      Top             =   1320
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   7435
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1926191384
      ErrInfo         =   -1838908169
      Persistence     =   -1  'True
      _cx             =   9128
      _cy             =   7435
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      UndoEnabled     =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin MSComDlg.CommonDialog dlgFileOpen 
      Left            =   360
      Top             =   6960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CheckBox Check2 
      Caption         =   "HalfTone or QuickText Mode"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   17
      Top             =   6000
      Width           =   2175
   End
   Begin VB.Frame halftone 
      Caption         =   "HalfTone Mode"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3615
      Left            =   2880
      TabIndex        =   10
      Top             =   5760
      Visible         =   0   'False
      Width           =   5895
      Begin VB.OptionButton Option6 
         Caption         =   "Auto-Blur"
         Height          =   255
         Left            =   4080
         TabIndex        =   31
         Top             =   3120
         Width           =   1575
      End
      Begin VB.OptionButton Option5 
         Caption         =   "Gaussian"
         Height          =   255
         Left            =   2040
         TabIndex        =   30
         Top             =   3120
         Width           =   1455
      End
      Begin VB.OptionButton Option4 
         Caption         =   "None"
         Height          =   375
         Left            =   240
         TabIndex        =   29
         Top             =   3120
         Width           =   1455
      End
      Begin MSComctlLib.Slider Slider9 
         Height          =   375
         Left            =   1440
         TabIndex        =   19
         Top             =   1440
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   661
         _Version        =   393216
         Min             =   -255
         Max             =   255
         TickStyle       =   3
      End
      Begin MSComctlLib.Slider Slider8 
         Height          =   375
         Left            =   1440
         TabIndex        =   13
         Top             =   2040
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   661
         _Version        =   393216
         Max             =   255
         TickStyle       =   3
      End
      Begin MSComctlLib.Slider Slider7 
         Height          =   375
         Left            =   1440
         TabIndex        =   12
         Top             =   960
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   661
         _Version        =   393216
         Min             =   1
         Max             =   32
         SelStart        =   1
         TickStyle       =   3
         Value           =   1
      End
      Begin MSComctlLib.Slider Slider6 
         Height          =   375
         Left            =   1440
         TabIndex        =   11
         Top             =   480
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   661
         _Version        =   393216
         Max             =   360
         TickStyle       =   3
      End
      Begin VB.Label Label19 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Blur Options"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   177
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1920
         TabIndex        =   28
         Top             =   2520
         Width           =   1935
      End
      Begin VB.Label Label15 
         Caption         =   "Label15"
         Height          =   255
         Left            =   4440
         TabIndex        =   23
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label Label14 
         Caption         =   "Label14"
         Height          =   255
         Left            =   4440
         TabIndex        =   22
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Label6"
         Height          =   255
         Left            =   4440
         TabIndex        =   21
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "Label5"
         Height          =   375
         Left            =   4440
         TabIndex        =   20
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label13 
         Caption         =   "Eccentricity"
         Height          =   375
         Left            =   120
         TabIndex        =   18
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label12 
         Caption         =   "Contrast Adjustment"
         Height          =   495
         Left            =   120
         TabIndex        =   16
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Grid Pitch"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label7 
         Caption         =   "Grid Angle"
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "QuickText Mode"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   2880
      TabIndex        =   0
      Top             =   5640
      Width           =   5655
      Begin VB.OptionButton Option3 
         Caption         =   "Auto-Blur"
         Height          =   255
         Left            =   3720
         TabIndex        =   27
         Top             =   3000
         Width           =   1695
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Gaussian"
         Height          =   255
         Left            =   2040
         TabIndex        =   26
         Top             =   3000
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "No Blur"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   3000
         Width           =   1335
      End
      Begin MSComctlLib.Slider Slider3 
         Height          =   375
         Left            =   1440
         TabIndex        =   1
         Top             =   1920
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   661
         _Version        =   393216
         Max             =   255
         TickStyle       =   3
      End
      Begin MSComctlLib.Slider Slider2 
         Height          =   495
         Left            =   1440
         TabIndex        =   2
         Top             =   1440
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   873
         _Version        =   393216
         Max             =   255
         TickStyle       =   3
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   495
         Left            =   1440
         TabIndex        =   3
         Top             =   960
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   873
         _Version        =   393216
         Max             =   255
         TickStyle       =   3
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Blur Options"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   177
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   24
         Top             =   2520
         Width           =   1695
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Low Threshold"
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "High Threshold"
         Height          =   375
         Left            =   240
         TabIndex        =   8
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Contrast Adjustment"
         Height          =   495
         Left            =   120
         TabIndex        =   7
         Top             =   1920
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "Label9"
         Height          =   255
         Left            =   4440
         TabIndex        =   6
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label Label10 
         Caption         =   "Label10"
         Height          =   255
         Left            =   4560
         TabIndex        =   5
         Top             =   1440
         Width           =   495
      End
      Begin VB.Label Label11 
         Caption         =   "Label11"
         Height          =   255
         Left            =   4680
         TabIndex        =   4
         Top             =   1080
         Width           =   495
      End
   End
   Begin VB.Label Label16 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   11400
      TabIndex        =   37
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   11280
      TabIndex        =   35
      Top             =   5160
      Width           =   1815
   End
   Begin VB.Label lblError 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   11280
      TabIndex        =   34
      Top             =   5760
      Width           =   2655
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuspacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuquit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Binarize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim blur As Integer
Dim temp As Long
Dim temp2 As Long
Dim orig As String
Dim orig1 As String
Dim formloaded As Boolean
Dim load As Boolean

Dim imgFileName As String
Dim imgParentDir As String
'determine which file was
Dim man As Boolean


Private Sub Check2_Click()
   If Check2.Value = 1 Then

     Frame1.Move halftone.Left, halftone.Top, halftone.Width, halftone.Height
     Frame1.Visible = True
     halftone.Visible = False
        
    orig = App.Path & "\..\..\..\..\..\..\Common\Images\binarize.jpg"
    orig1 = App.Path & "\..\..\..\..\..\..\Common\Images\binarize.jpg"
If man = True Then
    ImagXpress1.FileName = imgFileName
    ImagXpress2.FileName = imgFileName
Else
    ImagXpress1.FileName = orig
    ImagXpress2.FileName = orig1

End If
    DO_Binarize
   
Else
'use the half tone settings
     Option6.Value = True
     halftone.Move Frame1.Left, Frame1.Top, Frame1.Width, Frame1.Height
     Slider6.Value = 0
     Slider7.Value = 1
     Slider9.Value = 255
     Slider8.Value = 163
     Label5.Caption = Slider6.Value
     Label6.Caption = Slider7.Value
     Label14.Caption = Slider9.Value
     Label15.Caption = Slider8.Value
     halftone.Visible = True
     Frame1.Visible = False
     
    orig = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
    orig1 = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
 If man = True Then
    ImagXpress1.FileName = imgFileName
    ImagXpress2.FileName = imgFileName
Else
   
    ImagXpress1.FileName = orig
    ImagXpress2.FileName = orig1

End If
 
    DO_HalfTone
    End If
End Sub


Private Sub Form_Load()
  
  load = False
  Check2.Value = 1
   
  ImagXpress1.EventSetEnabled EVENT_PROGRESS, True

  halftone.Visible = False
  orig = App.Path & "\..\..\..\..\..\..\Common\Images\binarize.jpg"
  orig1 = App.Path & "\..\..\..\..\..\..\Common\Images\binarize.jpg"

  formloaded = False
        
   ImagXpress2.FileName = orig
   
   Err = ImagXpress2.ImagError
   PegasusError Err, lblError
   

   ImagXpress1.UndoEnabled = True
   ImagXpress1.FileName = orig1
   
   Err = ImagXpress2.ImagError
   PegasusError Err, lblError
   
   
   Slider2.Value = 126
   Slider1.Value = 86
   Slider3.Value = 163
   Option3.Value = True
   
   ' These happen to be good settings for this particular image in quick-text mode.

   Label9.Caption = Slider3.Value
   Label10.Caption = Slider2.Value
   Label11.Caption = Slider1.Value
   
   formloaded = True
 Check2.Value = 1
   DO_Binarize
   
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuExit_Click()
    End
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub

Private Sub mnuOpen_Click()
    Dim tmpFN As String
    man = True
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuquit_Click()
    End
End Sub

Private Sub mnutoolbar_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbar.Caption = "&Show"
    Else
        mnuToolbar.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    ImagXpress2.ToolbarActivated = Not ImagXpress2.ToolbarActivated
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub

Private Sub Option1_Click()
    If Option1.Value = True Then
        blur = 0
    End If
    If formloaded Then
    DO_Binarize
  End If
End Sub

Private Sub Option2_Click()
    If Option2.Value = True Then
        blur = 1
    End If
    If formloaded Then
    DO_Binarize
  End If
End Sub

Private Sub Option3_Click()
    If Option3.Value = True Then
        blur = 2
    End If
    If formloaded Then
    DO_Binarize
  End If
End Sub

Private Sub Option4_Click()
    If Option4.Value = True Then
        blur = 0
    End If
    If formloaded Then
    DO_HalfTone
  End If
End Sub

Private Sub Option5_Click()
    If Option5.Value = True Then
        blur = 1
    End If
    If formloaded Then
    DO_HalfTone
  End If
End Sub

Private Sub Option6_Click()
    If Option6.Value = True Then
        blur = 2
    End If
    If formloaded Then
    DO_HalfTone
  End If
End Sub

Private Sub Slider1_Change()
If Slider2.Value = 0 Then
    Slider1.Value = 0
 Else
    If Slider1.Value >= Slider2.Value Then
    Slider1.Value = (Slider2.Value - 1)
 End If
 End If
  
Label11.Caption = Slider1.Value

  If formloaded Then
  DO_Binarize
  End If
End Sub
Private Sub Slider2_Change()
If Slider2.Value < Slider1.Value Then
    Slider2.Value = (Slider1.Value + 1)
    End If
     Label10.Caption = Slider2.Value
  If formloaded Then
    DO_Binarize
  End If
End Sub
Private Sub Slider3_Change()
  Label9.Caption = Slider3.Value
  If formloaded Then
      DO_Binarize
  End If
End Sub

Public Function DO_Binarize()

  ImagXpress1.ImagError = 0
  ImagXpress1.Undo
  If ImagXpress1.ImagError <> 0 And ImagXpress1.ImagError <> -25 Then
      Err = ImagXpress1.ImagError
      PegasusError Err, lblError
  End If
 
  ImagXpress1.ImagError = 0
  ImagXpress1.Binarize BIN_MODE_QUICK_TEXT, Slider1.Value, Slider2.Value, 0, 3, 48, Slider3.Value, blur
          
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError

End Function

Public Function DO_HalfTone()
  
  ImagXpress1.ImagError = 0
  ImagXpress1.Undo
  If ImagXpress1.ImagError <> 0 And ImagXpress1.ImagError <> -25 Then
      Err = ImagXpress1.ImagError
      PegasusError Err, lblError
  End If
  
  ImagXpress1.ImagError = 0
  ImagXpress1.Binarize BIN_MODE_PHOTO_HALFTONE, 0, 0, Slider6.Value, Slider7.Value, Slider9.Value, Slider8.Value, blur
          
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError

End Function


Private Sub Slider6_Click()
    
    DO_HalfTone
    Label5.Caption = Slider6.Value
  
End Sub

Private Sub Slider7_Click()
      DO_HalfTone
      Label6.Caption = Slider7.Value
End Sub

Private Sub Slider8_Click()
     DO_HalfTone
     Label15.Caption = Slider8.Value
End Sub

Private Sub Slider9_Click()
     DO_HalfTone
     Label14.Caption = Slider9.Value
End Sub

Private Sub LoadFile()
   ImagXpress2.FileName = imgFileName
   ImagXpress1.FileName = imgFileName
    DO_Binarize
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub

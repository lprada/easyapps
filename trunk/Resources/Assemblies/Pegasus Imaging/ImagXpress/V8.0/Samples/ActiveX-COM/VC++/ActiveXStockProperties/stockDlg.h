// stockDlg.h : header file
//

#if !defined(AFX_STOCKDLG_H__E21CA24A_560C_11D3_A8FB_EED2BEB31C39__INCLUDED_)
#define AFX_STOCKDLG_H__E21CA24A_560C_11D3_A8FB_EED2BEB31C39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"

/////////////////////////////////////////////////////////////////////////////
// CStockDlg dialog
class CStockDlg : public CDialog
{
	// Construction
public:
	CStockDlg(CWnd* pParent = NULL);	// standard constructor
	
	void GetIXCoords(IImagXpressPtr pImagXpress, int *x, int*y, int *width, int *height);
	void ShowInfo();
	
	// this will be the pointer to our COM instance 
	CImagXpress *ppCImagXpress;
	IImagXpressPtr pImagXpress;
	CRichEditCtrl * pEdtInfo;
	// Dialog Data
	//{{AFX_DATA(CStockDlg)
	enum { IDD = IDD_STOCK_DIALOG };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStockDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	HICON m_hIcon;
	
	// Generated message map functions
	//{{AFX_MSG(CStockDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnChangebackcolor();
	afx_msg void OnChangesize();
	afx_msg void OnChangeposition();
	afx_msg void OnChangevisibility();
	afx_msg void OnDestroy();
	afx_msg void OnFileQuit();
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STOCKDLG_H__E21CA24A_560C_11D3_A8FB_EED2BEB31C39__INCLUDED_)

unit ImagXpress8EXIFTags;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AxCtrls, OleCtrls, PegasusImagingActiveXImagXpress8_TLB,
  StdCtrls, Menus;

type
  TForm1 = class(TForm)
    Xpress1: TImagXpress;
    cmdLoadExif: TButton;
    cmdAddCopy: TButton;
    cmdDeleteTag: TButton;
    cmdModify: TButton;
    List1: TListBox;
    cmdGetTags: TButton;
    menu: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Quit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    cd: TOpenDialog;
    ListBox1: TListBox;
    function interprettype(tagtype : Integer): string;
    function interprettag(tagnum : Variant): string;
    procedure getexiftags(IX : TImagXpress);
    procedure cmdLoadExifClick(Sender: TObject);
    procedure cmdAddCopyClick(Sender: TObject);
    procedure cmdDeleteTagClick(Sender: TObject);
    procedure cmdModifyClick(Sender: TObject);
    procedure cmdGetTagsClick(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure Quit1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
   
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  IX : TImagXpress;
implementation

{$R *.dfm}

procedure TForm1.cmdLoadExifClick(Sender: TObject);
var

 imagefile : string;
begin
     imagefile := ExtractFilePath(Application.ExeName) + '..\..\..\..\..\..\Common\Images\exif.jpg';
      Xpress1.FileGetTags(imagefile,1);
        If Xpress1.ImagError <> 0 then
                showmessage('Error loading the image');
        List1.Clear;
        getexiftags(IX);
end;

//tagtype function implemented here
function TForm1.interprettype(tagtype : Integer): String;
begin
     Case tagtype of
        1:     interprettype := 'Byte';
        2:     interprettype := 'Ascii char';
        3:     interprettype := 'Short';
        4:     interprettype := 'Long';
        5:     interprettype := 'Rational';
        6:     interprettype := 'Signed byte';
        7:     interprettype := '8 bit byte';
        8:     interprettype := 'Signed Short';
        9:     interprettype := 'Signed Long';
        10:    interprettype := 'Signed Rational';
        11:    interprettype := 'Float';
        12:    interprettype := 'Double';
     Else
          interprettype := 'Unknown';
    End;
end;

function TForm1.interprettag(tagnum : Variant): string;
var
hexval : Longint;
begin
          //hexval := HexL(tagnum);
end;


procedure TForm1.getexiftags(IX : TImagXpress);
const MAX_TABS = 4;
Tab = #9;
var
Tabulators: array[0..MAX_TABS] of Integer;
i : integer;
j : integer;
exifnum : string;
exiftype : string;
exifcount : integer;
exifdataraw : Variant;
exifdataraw1 : Variant;
exifdataparsed :Variant;
final : string;
counter : integer;
x : integer;
begin
  Tabulators[0] := 50;
  Tabulators[1] :=  100;
  Tabulators[2] := 150;
  Tabulators[3] := 250;
  List1.TabWidth := 2;


   SendMessage(List1.Handle, LB_SETTABSTOPS, MAX_TABS, Longint(@Tabulators));
   List1.Items.Add('Tag#' + Tab + 'Type' + Tab + 'Count' + Tab + Tab  +'Tag Data' );

    For counter := 0 To 3 do
    begin
    xpress1.TagLevel := counter;
    List1.Items.Add('Property Level:' + IntToStr(xpress1.TagLevel));
    xpress1.tagfirst;

        While Xpress1.ImagError = 0  do
        begin
           // exifnum = interprettag(xpress1.TagNumber)
            //If exifnum = "" Then
            exifnum := IntToStr(xpress1.TagNumber);
            //End If
            exiftype := interprettype(xpress1.tagtype);
            exifdataparsed := '';
            exifcount := xpress1.TagCount;
            For x := 0 To exifcount do
            begin
                If (exiftype = 'Rational') Or (exiftype = 'Signed Rational') Then
                Begin
                     exifdataraw := xpress1.TagGetDataItem(x);
                     exifdataraw1 :=  VarArrayGet(exifdataraw,[x]);
                     exifdataparsed := exifdataparsed +  '|' +  VarToStr(exifdataraw1);
                end
                Else if  (exiftype <> 'Rational') Or (exiftype <> 'Signed Rational') then
                  exifdataparsed := exifdataparsed +  VarToStr(xpress1.TagGetDataItem(x));
            End;
             SendMessage(List1.Handle, LB_SETTABSTOPS, MAX_TABS, Longint(@Tabulators));
             List1.items.Add(exifnum +  Tab +   exiftype + Tab + IntToStr(exifcount) + Tab + exifdataparsed);
             xpress1.TagNext();
        end;
end;

end;

procedure TForm1.cmdDeleteTagClick(Sender: TObject);
var
counter : integer;
begin
        For Counter := 0 To 3  do
        begin
        xpress1.TagLevel := Counter;
        xpress1.TagDelete(307);
    end;
    List1.Clear;
    getexiftags(IX);

end;

procedure TForm1.cmdModifyClick(Sender: TObject);
var
counter : integer;

A: Variant;

begin
      A := VarArrayCreate([0,2], varInteger);
      A[0] := 80;
      A[1] := 1;
      For Counter := 0 To 2  do
      begin
      xpress1.TagLevel := counter;
      xpress1.TagFind(282);
      xpress1.TagModify(282, 5, 1, A);
      xpress1.TagFind(283);
      xpress1.TagModify(283, 5, 1, A);
      end;
      List1.Clear;
      getexiftags(IX);

end;


procedure TForm1.cmdAddCopyClick(Sender: TObject);
var
 TagString : string;
 counter : integer;
 count : integer;
begin
   TagString := 'Copyright (c) 2003, Pegasus Imaging Corp.';
    For Counter := 0 To 3 do
     Begin
        xpress1.TagLevel := Counter;
        count := StrLen(PChar(TagString));
        xpress1.TagAdd(307, 2, count, TagString);
    End;
    List1.Clear;
    getexiftags(IX);
end;

procedure TForm1.cmdGetTagsClick(Sender: TObject);
var
   imagefile : string;
begin
      imagefile := ExtractFilePath(Application.ExeName) + '..\..\..\..\..\Common\Images\exif.jpg';
      Xpress1.FileSetTags(imagefile,1);
      imagefile := ExtractFilePath(Application.ExeName) + '..\..\..\..\..\Common\Images\exif.jpg';
      Xpress1.FileGetTags(imagefile,1);
      List1.Clear;
      getexiftags(IX);
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  Xpress1.AboutBox;
end;

procedure TForm1.Open1Click(Sender: TObject);
var
filter : string;
FNam : string;
begin

  filter := 'IX Supported Files|*.BMP;*TIF;*.TIFF;*.CAL;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.JPG;*.JIF;*.LJP;PCX;*.PIC;*.PNG;*.JB2*.WSQ|All File (*.*)|*.*';
  cd.Filter := filter;
  cd.Execute;
  FNam := cd.FileName;
  
  Xpress1.FileGetTags(FNam,1);
end;

procedure TForm1.Quit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  //***Must use the UnlockRuntime function
  Xpress1.UnlockRuntime(1234,1234,1234,1234);
end;

end.

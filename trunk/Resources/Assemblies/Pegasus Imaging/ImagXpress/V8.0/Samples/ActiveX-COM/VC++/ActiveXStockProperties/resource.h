//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by stock.rc
//
#define IDD_STOCK_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define ID_QUIT                         1000
#define ID_CHANGEBACKCOLOR              1001
#define ID_CHANGESIZE                   1002
#define ID_CHANGEPOSITION               1003
#define ID_CHANGEVISIBILITY             1004
#define IDC_INFO                        1005
#define IDC_RICHEDIT                    1006
#define ID_FILE_QUIT                    32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

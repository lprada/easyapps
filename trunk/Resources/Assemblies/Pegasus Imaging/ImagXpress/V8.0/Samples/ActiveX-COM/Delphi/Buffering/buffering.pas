unit buffering;

interface

uses
{$IFDEF VER140} // Delphi6
Variants,
{$ENDIF}
{$IFDEF VER150} // Delphi7
Variants,
{$ENDIF}
  Windows, Messages, SysUtils,  Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AxCtrls, OleCtrls,
  PegasusImagingActiveXImagXpress8_TLB, Menus;

type
  TForm1 = class(TForm)
    cboSelectImage: TComboBox;
    cmdRotate: TButton;
    cmdColorDepth: TButton;
    cmdEmboss: TButton;
    cmdReloadImage: TButton;
    lstIPStatus: TListBox;
    ImagXpress1: TImagXpress;
    IXView1: TImagXpress;
    IXView2: TImagXpress;
    IXView3: TImagXpress;
    IXView4: TImagXpress;
    menu: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    Quit1: TMenuItem;
    oolBar1: TMenuItem;
    Show1: TMenuItem;
    About1: TMenuItem;
    cd: TOpenDialog;
    ListBox1: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure cboSelectImageChange(Sender: TObject);
    procedure cmdRotateClick(Sender: TObject);
    procedure cmdReloadImageClick(Sender: TObject);
    procedure IXView1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IXView2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IXView3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IXView4MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImagXpress1ImageStatusChanged(ASender: TObject; ID: Smallint;
      eOPID: TOleEnum; lStatus: Integer);
    procedure ImagXpress1CB(ASender: TObject; ID: Smallint;
      CBID: TOleEnum);
    procedure Open1Click(Sender: TObject);
    procedure Quit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure cmdColorDepthClick(Sender: TObject);
    procedure cmdEmbossClick(Sender: TObject);
    procedure Show1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  ChunkSize = 128;  //Size of image chunks to load in bytes.

var
  Form1: TForm1;
  PathName: String;
  FNam1: String;
  Siz: Integer;
  VarArray: OleVariant;
  filter : string;

implementation

{$R *.dfm}

function StatusStr(Status: ixSTATUS): String;
var
   Txt: String;
begin
     //Takes the status flags and returns a list of the enabled ones as a string.
     Txt:='';
     if Status=BUFSTATUS_EMPTY then Txt:='Empty';
     if (Status and BUFSTATUS_OPENED)>0 then Txt:=Txt+'Opened ';
     if (Status and BUFSTATUS_DEFINED)>0 then Txt:=Txt+'Defined ';
     if (Status and BUFSTATUS_DECODING)>0 then Txt:=Txt+'Decoding ';
     if (Status and BUFSTATUS_DECODED)>0 then Txt:=Txt+'Decoded ';
     if (Status and BUFSTATUS_CANCELED)>0 then Txt:=Txt+'Canceled ';
     if (Status and BUFSTATUS_ERROR)>0 then Txt:=Txt+'Error ';
     if (Status and BUFSTATUS_IMAGEPROCESSED)>0 then Txt:=Txt+'ImageProcessed ';
     if (Status and BUFSTATUS_IMAGEPROCESSING)>0 then Txt:=Txt+'ImageProcessing ';
     if (Status and BUFSTATUS_WAIT)>0 then Txt:=Txt+'Wait ';
     StatusStr:=Trim(Txt);
end;

function FileToMem(Filename: OleVariant): OleVariant;
var
        buf: pchar;
        fs:  TFileStream;
begin
        if not fileexists(trim(vartostr(filename))) then begin
                result:=VarArrayCreate([0,0],varbyte);
                exit;
        end;
        try
                fs:=TFileStream.Create (trim(vartostr(filename)), fmOpenRead);
                result:=vararraycreate([0,fs.size-1], varbyte);
                buf:=vararraylock(result);
                Siz:=fs.Size;
                fs.ReadBuffer (buf^, Siz);
                fs.Free;
        finally
                vararrayunlock(result);
        end;
end;

procedure TForm1.FormCreate(Sender: TObject);
  var
     i: integer;
     picName: String;
     T: String;
begin

     //Set starting defaults.
     ImagXpress1.AutoSize:=ISIZE_BestFit;
     ImagXpress1.ScrollBars:=SB_Both;
     ImagXpress1.ProcessImageID:=1;
     ImagXpress1.ViewImageID:=1;

     PathName := ExtractFilePath(ParamStr(0)) + '..\..\..\..\..\..\Common\Images\';

     for i:=1 to 4 do
     begin
        case i of
        1: begin
                IXView1.AutoSize:=ISIZE_BestFit;
                picName:='window.jpg';
                ImagXpress1.ProcessImageID := 1;
                IXView1.ViewImageID :=1;
           end;
        2: begin
                IXView2.AutoSize:=ISIZE_BestFit;
                picName:='door.jpg';
                ImagXpress1.ProcessImageID := 2;
                IXView2.ViewImageID :=2;
           end;
        3: begin
                IXView3.AutoSize:=ISIZE_BestFit;
                picName:='water.jpg';
                ImagXpress1.ProcessImageID := 3;
                IXView3.ViewImageID :=3;
           end;
        4: begin
                IXView4.AutoSize:=ISIZE_BestFit;
                picName:='boat.jpg';
                ImagXpress1.ProcessImageID := 4;
                IXView4.ViewImageID :=4;
           end;
        end;

        FNam1 := PathName + picName;
        ImagXpress1.LoadCB;
        cboSelectImage.Items.Add (picName);
     end;

     ImagXpress1.ProcessImageID := 1;
     cboSelectImage.ItemIndex := 0;
end;


procedure TForm1.cboSelectImageChange(Sender: TObject);
begin
                case cboSelectImage.ItemIndex of
                0: begin
                        ImagXpress1.ProcessImageID := 1;
                        ImagXpress1.ViewImageID := 1;
                   end;
                1: begin
                        ImagXpress1.ProcessImageID := 2;
                        ImagXpress1.ViewImageID := 2;
                   end;
                2: begin
                        ImagXpress1.ProcessImageID := 3;
                        ImagXpress1.ViewImageID := 3;
                   end;
                3: begin
                        ImagXpress1.ProcessImageID := 4;
                        ImagXpress1.ViewImageID := 4;
                   end;
        end;
end;

procedure TForm1.cmdRotateClick(Sender: TObject);
begin
   ImagXpress1.Rotate(180);
end;


procedure TForm1.cmdReloadImageClick(Sender: TObject);
begin
    FNam1:=PathName + cboSelectImage.Text;
        ImagXpress1.LoadCB;
end;

procedure TForm1.IXView1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ImagXpress1.ProcessImageID := 1;
        ImagXpress1.ViewImageID := 1;
        cboSelectImage.ItemIndex := 0;
end;

procedure TForm1.IXView2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    ImagXpress1.ProcessImageID := 2;
        ImagXpress1.ViewImageID := 2;
        cboSelectImage.ItemIndex := 1;
end;

procedure TForm1.IXView3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ImagXpress1.ProcessImageID := 3;
        ImagXpress1.ViewImageID := 3;
        cboSelectImage.ItemIndex := 2;
end;

procedure TForm1.IXView4MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    ImagXpress1.ProcessImageID := 4;
    ImagXpress1.ViewImageID := 4;
    cboSelectImage.ItemIndex := 3;
end;

procedure TForm1.ImagXpress1ImageStatusChanged(ASender: TObject;
  ID: Smallint; eOPID: TOleEnum; lStatus: Integer);
begin
        lstIPStatus.Items.Add ('Buffer ' + IntToStr(ID) + ': ' + StatusStr(lStatus));
        lstIPStatus.ItemIndex := lstIPStatus.Items.Count - 1;
end;

procedure TForm1.ImagXpress1CB(ASender: TObject; ID: Smallint;
  CBID: TOleEnum);

    var

     IErr: Integer;
begin
    case CBID of
          CBID_OPEN: begin
                          VarArray:=FileToMem(FNam1);
                     end;
          CBID_READ: begin
                          ImagXpress1.WriteCBData(ID, VarArray, Siz);
                          //Check for and report any errors.
                          IErr:=ImagXpress1.ImagError;
                          if IErr<>0 then
                          begin
                               lstIPStatus.Items.Add('WriteCB error: ' + IntToStr(IErr));
                          end;
                     end;
     end;
end;

procedure TForm1.Open1Click(Sender: TObject);
var
err : integer;
begin
 lstIPStatus.Items.Clear;

  filter := 'IX Supported Files|*.BMP;*TIF;*.TIFF;*.CAL;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.JPG;*.JIF;*.LJP;PCX;*.PIC;*.PNG;*.JB2*.WSQ|All File (*.*)|*.*';
  cd.InitialDir := '..\..\..\..\..\..\Common\Images\';
  cd.Filter := filter;
  cd.Execute;
   FNam1 := cd.FileName;
  ImagXpress1.FileName :=  FNam1;
end;

procedure TForm1.Quit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  ImagXpress1.AboutBox;
end;

procedure TForm1.cmdColorDepthClick(Sender: TObject);
begin
  ImagXpress1.ColorDepth(1,0,0);
end;

procedure TForm1.cmdEmbossClick(Sender: TObject);
begin
  ImagXpress1.Emboss();
end;

procedure TForm1.Show1Click(Sender: TObject);
begin
        If ImagXpress1.ToolbarActivated Then
    begin
        show1.Caption := '&Show';
    end
    Else
        show1.Caption := '&Hide';

    ImagXpress1.ToolbarActivated := Not ImagXpress1.ToolbarActivated
end;

end.

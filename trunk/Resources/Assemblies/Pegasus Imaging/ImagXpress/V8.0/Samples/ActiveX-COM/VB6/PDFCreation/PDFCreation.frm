VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form PDF 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress 8-PDF Creation"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11115
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   11115
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1230
      Left            =   120
      TabIndex        =   12
      Top             =   6120
      Width           =   2895
   End
   Begin VB.CommandButton cmdnext 
      Caption         =   "Next Page"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8880
      TabIndex        =   8
      Top             =   6000
      Width           =   1455
   End
   Begin VB.CommandButton cmdprev 
      Caption         =   "Previous Page"
      Enabled         =   0   'False
      Height          =   375
      Left            =   6000
      TabIndex        =   7
      Top             =   6000
      Width           =   1455
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3855
      Left            =   5280
      TabIndex        =   6
      Top             =   1080
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   6800
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   1626502518
      ErrInfo         =   -970643591
      Persistence     =   -1  'True
      _cx             =   9763
      _cy             =   6800
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   495
      Left            =   120
      TabIndex        =   5
      Top             =   7920
      Visible         =   0   'False
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   873
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   1626502518
      ErrInfo         =   -970643591
      Persistence     =   -1  'True
      _cx             =   1720
      _cy             =   873
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.ListBox lstdesc 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "PDFCreation.frx":0000
      Left            =   360
      List            =   "PDFCreation.frx":0010
      TabIndex        =   4
      Top             =   120
      Width           =   10455
   End
   Begin VB.Frame frmoption 
      Caption         =   "PDF Creation Options"
      Height          =   1815
      Left            =   600
      TabIndex        =   1
      Top             =   1680
      Width           =   3015
      Begin VB.OptionButton Option2 
         Caption         =   "Create and View Multi-Page PDF"
         Height          =   495
         Left            =   360
         TabIndex        =   3
         Top             =   960
         Width           =   1935
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Create and View Single Page PDF"
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Create PDF"
      Height          =   495
      Left            =   1200
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   3720
      Width           =   1575
   End
   Begin VB.Label lblPageNbr 
      BackColor       =   &H80000009&
      Height          =   375
      Left            =   9840
      TabIndex        =   16
      Top             =   5040
      Width           =   855
   End
   Begin VB.Label lblPageNbrProperty 
      Caption         =   "IX PageNbr Property"
      Height          =   495
      Index           =   0
      Left            =   8160
      TabIndex        =   15
      Top             =   5040
      Width           =   1455
   End
   Begin VB.Label lblPageCount 
      BackColor       =   &H80000004&
      Height          =   375
      Left            =   7080
      TabIndex        =   14
      Top             =   5040
      Width           =   615
   End
   Begin VB.Label lblFilePageCount 
      Caption         =   "File Page Count:"
      Height          =   375
      Left            =   5400
      TabIndex        =   13
      Top             =   5040
      Width           =   1335
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   5520
      Width           =   1695
   End
   Begin VB.Label lblError 
      Height          =   1095
      Left            =   3360
      TabIndex        =   10
      Top             =   6240
      Width           =   2415
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   3360
      TabIndex        =   9
      Top             =   5520
      Width           =   1815
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "PDF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim file1 As String
Dim file2 As String
Dim counter As Integer
Dim choice As Boolean

Private Sub cmdnext_Click()

    ImagXpress1.PageNbr = ImagXpress1.PageNbr + 1
    If ImagXpress1.PageNbr > ImagXpress1.Pages Then
            ImagXpress1.PageNbr = ImagXpress1.Pages
    End If
    ImagXpress1.FileName = App.Path & "\multipage.pdf"
    
    
    lblPageNbr.Caption = ImagXpress1.PageNbr

End Sub

Private Sub cmdprev_Click()
   
    ImagXpress1.PageNbr = ImagXpress1.PageNbr - 1
    
    If ImagXpress1.PageNbr < 1 Then
            ImagXpress1.PageNbr = 1
    End If
    
    ImagXpress1.FileName = App.Path & "\multipage.pdf"
    
    lblPageNbr.Caption = ImagXpress1.PageNbr
          

End Sub

Private Sub Command1_Click()

ImagXpress1.PageNbr = 1
ImagXpress2.PageNbr = 1


'multi page PDF

If Option2.Value = True And choice = True Then
    For counter = 1 To ImagXpress2.NumPages(imgFileName)
        ImagXpress2.PageNbr = counter
        ImagXpress2.FileName = imgFileName
        ImagXpress2.SavePDFCompression = PDF_CCITTFAX4
        ImagXpress2.SaveFileType = FT_PDF
        ImagXpress2.SaveMultiPage = True
        ImagXpress2.SaveFileName = App.Path & "\multipage.pdf"
        ImagXpress2.SaveFile
    Next
     ImagXpress1.FileName = App.Path & "\multipage.pdf"
     
     Err = ImagXpress1.ImagError
    PegasusError Err, lblError
     
     
     ImagXpress1.Visible = True
     cmdprev.Enabled = True
     cmdnext.Enabled = True
 
Else
    If Option2.Value = True Then
    For counter = 1 To ImagXpress2.NumPages(file2)
        ImagXpress2.PageNbr = counter
        ImagXpress2.FileName = file2
        ImagXpress2.SavePDFCompression = PDF_CCITTFAX4
        ImagXpress2.SaveFileType = FT_PDF
        ImagXpress2.SaveMultiPage = True
        ImagXpress2.SaveFileName = App.Path & "\multipage.pdf"
        ImagXpress2.SaveFile
    Next
     ImagXpress1.FileName = App.Path & "\multipage.pdf"
     
     Err = ImagXpress1.ImagError
    PegasusError Err, lblError
     
     ImagXpress1.Visible = True
     cmdprev.Enabled = True
     cmdnext.Enabled = True
 
 
'single page PDF
Else
    
    ImagXpress1.hDIB = ImagXpress2.CopyDIB
    ImagXpress1.Visible = False
    '***Please note: The SavePDFCompression property has other options(i.e. the ability to save as _
    '***a Sequential JPEG). Please refer to the ImagXpress 7 help file.
    ImagXpress2.SavePDFCompression = PDF_CCITTFAX4
    ImagXpress2.SaveFileType = FT_PDF
    
    
    'delete the previous singlepage.pdf
    If (Dir(App.Path & "\singlepage.pdf") <> "") Then
         Kill App.Path & "\singlepage.pdf"
    
    End If
    
    
    ImagXpress2.SaveFileName = App.Path & "\singlepage.pdf"
    ImagXpress2.SaveFile
    ImagXpress1.FileName = App.Path & "\singlepage.pdf"
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    ImagXpress1.Visible = True
    
    cmdprev.Enabled = False
     cmdnext.Enabled = False
End If
End If

lblPageCount.Caption = ImagXpress1.Pages
lblPageNbr.Caption = ImagXpress1.PageNbr

ImagXpress2.PageNbr = 1

End Sub

Private Sub Form_Load()
 file1 = App.Path & "\..\..\..\..\..\..\Common\Images\benefits.tif"
 file2 = App.Path & "\..\..\..\..\..\..\Common\Images\multi.tif"
 
 ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
 
 Option1.Value = True

End Sub

Private Sub mnuAbout_Click()
    ImagXpress2.AboutBox

End Sub

Private Sub mnuOpen_Click()
    choice = True
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
     If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub
Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub Option1_Click()

ImagXpress1.PageNbr = 1
ImagXpress2.PageNbr = 1

  If Option1.Value = True And choice = True Then
        
        ImagXpress2.FileName = imgFileName
        Err = ImagXpress2.ImagError
       PegasusError Err, lblError
  ElseIf Option1.Value = True And choice = False Then
       
       ImagXpress2.FileName = file1
       
       Err = ImagXpress2.ImagError
       PegasusError Err, lblError
  End If
  
End Sub

Private Sub Option2_Click()

ImagXpress1.PageNbr = 1
ImagXpress2.PageNbr = 1

 If Option2.Value = True And choice = True Then
        
        ImagXpress2.FileName = imgFileName
        Err = ImagXpress2.ImagError
       PegasusError Err, lblError
  
  ElseIf Option2.Value = True And choice = False Then
        ImagXpress2.FileName = file2
        Err = ImagXpress2.ImagError
       PegasusError Err, lblError
       
  End If
End Sub


Private Sub LoadFile()
    'open the file here
    If imgFileName <> "" Then
        ImagXpress2.FileName = imgFileName
        
        Err = ImagXpress2.ImagError
        PegasusError Err, lblError
    Else
        MsgBox "Please load an image first.", vbOKOnly, "Error"
    End If
End Sub

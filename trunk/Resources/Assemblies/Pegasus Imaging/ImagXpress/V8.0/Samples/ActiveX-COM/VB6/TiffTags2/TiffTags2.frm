VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Show Tiff Tags"
   ClientHeight    =   10035
   ClientLeft      =   4125
   ClientTop       =   2085
   ClientWidth     =   13230
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10035
   ScaleWidth      =   13230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "TiffTags2.frx":0000
      Left            =   120
      List            =   "TiffTags2.frx":000D
      TabIndex        =   16
      Top             =   120
      Width           =   12855
   End
   Begin VB.TextBox txtSaveFile 
      Height          =   615
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   15
      Top             =   5400
      Width           =   3855
   End
   Begin VB.TextBox txtLoadFile 
      Height          =   975
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   13
      Top             =   2160
      Width           =   3975
   End
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   9000
      TabIndex        =   9
      Top             =   2520
      Width           =   3975
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3855
      Left            =   4440
      TabIndex        =   7
      Top             =   2040
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   6800
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1699237444
      ErrInfo         =   963300954
      Persistence     =   -1  'True
      _cx             =   7435
      _cy             =   6800
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "5) Save the file and reload it with Modified Tags"
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   4440
      Width           =   4095
   End
   Begin VB.CommandButton cmdAddMulti 
      Caption         =   "4) Add Tags with multiple Data (Tags 201 - 212)"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   4080
      Width           =   4095
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "3) Delete and ReAdd  (Tags 101 - 112)"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   3720
      Width           =   4095
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "2) Add Some Tiff Tags  (Tags 101 - 112)"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   3360
      Width           =   4095
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "1) Load an Image and Show the Tags"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   4095
   End
   Begin VB.ListBox lstTiffTagList 
      Height          =   2985
      Left            =   120
      TabIndex        =   0
      Top             =   6720
      Width           =   8535
   End
   Begin VB.Label lblSaveFileName 
      Caption         =   "Save File Name:"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   5040
      Width           =   2175
   End
   Begin VB.Label lblLoadFileName 
      Caption         =   "Load File Name:"
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label lblError 
      Height          =   2895
      Left            =   8880
      TabIndex        =   11
      Top             =   6600
      Width           =   3615
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   615
      Left            =   9000
      TabIndex        =   10
      Top             =   5520
      Width           =   2055
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   495
      Left            =   9120
      TabIndex        =   8
      Top             =   1680
      Width           =   2295
   End
   Begin VB.Label Label1 
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   6240
      Width           =   2655
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSeperator 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim sSaveFileName As String
Dim imgParentDir As String


Dim TTag, TLength, TType, TCount
Dim TData

Const TAG_BYTE = 1
Const TAG_ASCII = 2
Const TAG_SHORT = 3
Const TAG_LONG = 4
Const TAG_RATIONAL = 5
Const TAG_SBYTE = 6
Const TAG_UNDEFINE = 7  '* 8 bit byte
Const TAG_SSHORT = 8
Const TAG_SLONG = 9
Const TAG_SRATIONAL = 10
Const TAG_FLOAT = 11
Const TAG_DOUBLE = 12

' Dumps all of the tags into a List box
Private Sub DumpTagInfo(lstTiffTagList As ListBox, ImagXpress1 As ImagXpress)
  Dim RV As Variant ' Rational Values
  Dim RVal() As Long
  Dim sTagInfo As String
  Dim i As Integer
  
  sTagInfo = ImagXpress1.TagNumber
  sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagLength
  sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagType

  sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagCount
  
  If ImagXpress1.TagType = TAG_RATIONAL Or ImagXpress1.TagType = TAG_SRATIONAL Then
    RV = ImagXpress1.TagGetDataItem(1)
    sTagInfo = sTagInfo & Chr$(9) & RV(0) & " / " & RV(1)
  Else
    sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagGetDataItem(1)
  End If
  ' If there are more than 1 Items in this tag, then we'll dump out
  ' items 2 and 3 to demonstrate how to get the additional items
  If ImagXpress1.TagType <> TAG_ASCII Then
    If (ImagXpress1.TagCount > 1) Then
      For i = 2 To 2
        If ImagXpress1.TagType = TAG_RATIONAL Or ImagXpress1.TagType = TAG_SRATIONAL Then
          RVal = ImagXpress1.TagGetDataItem(i)
          sTagInfo = sTagInfo & Chr$(9) & RVal(0) & " / " & RVal(1) & " "
        Else
          sTagInfo = sTagInfo & Chr$(9) & ImagXpress1.TagGetDataItem(i)
        End If
      Next i
      If ImagXpress1.TagCount > 2 Then
        sTagInfo = sTagInfo & Chr$(9) & "etc..."
      End If
    End If
  End If
  lstTiffTagList.AddItem sTagInfo
End Sub
Private Sub ShowTags(lstTiffTagList As ListBox, ImagXpress1 As ImagXpress)
  lstTiffTagList.Clear
  lstTiffTagList.AddItem "Tag" & Chr$(9) & "Length" & Chr$(9) & "Type" & Chr$(9) & "Count" & Chr$(9) & "Data"
  ImagXpress1.TagFirst
  While (ImagXpress1.ImagError = 0)
    DumpTagInfo lstTiffTagList, ImagXpress1
    ImagXpress1.TagNext
  
  Wend
    
End Sub

Private Sub cmdAdd_Click()
  ' We Assemble and add some bogus TIFF Tags
  ' This demonstrates adding each type of TAG

  Dim R(2) As Long ' Holder for Rational Data
  ' Byte - TIFF Type 1
  ImagXpress1.TagAdd 101, TAG_BYTE, 1, 1
  ' Ascii - TIFF Type 2
  ImagXpress1.TagAdd 102, TAG_ASCII, 6, "Tag 2"
  ' Short - TIFF Type 3
  ImagXpress1.TagAdd 103, TAG_SHORT, 1, 3
  ' Long - TIFF Type 4
  ImagXpress1.TagAdd 104, TAG_LONG, 1, 4
  ' Rational - TIFF Type 5
  R(0) = 600 ' Numerator
  R(1) = 2 ' Denominator
  ImagXpress1.TagAdd 105, TAG_RATIONAL, 1, R
  ' SByte - TIFF Type 6
  ImagXpress1.TagAdd 106, TAG_SBYTE, 1, -6
  ' Undefine - TIFF Type 7
  ImagXpress1.TagAdd 107, TAG_UNDEFINE, 1, 7
  ' SShort - TIFF Type 8
  ImagXpress1.TagAdd 108, TAG_SSHORT, 1, -8
  ' SLong - TIFF Type 9
  ImagXpress1.TagAdd 109, TAG_SLONG, 1, -9
  ' SRational - TIFF Type 10
  R(0) = -600 ' Numerator
  R(1) = 2 ' Denominator
  ImagXpress1.TagAdd 110, TAG_SRATIONAL, 1, R
  ' Float  - TIFF Type 11
  ImagXpress1.TagAdd 111, TAG_FLOAT, 1, 11.65
  ' Double  - TIFF Type 12
  ImagXpress1.TagAdd 112, TAG_DOUBLE, 1, 12.42
   
  
  ShowTags lstTiffTagList, ImagXpress1
End Sub



Private Sub cmdDelete_Click()
  ' Delete our bogus tags and readd them to test
  ' Retrieve, Add, and Delete for each Tag type
  Dim RV As Variant
  Dim i As Integer
  
  For i = 101 To 112
    ' Find the Tag
    ImagXpress1.TagFind i
    ' Get the Tag Data
    TTag = ImagXpress1.TagNumber
    TLength = ImagXpress1.TagLength
    TType = ImagXpress1.TagType
    TCount = ImagXpress1.TagCount
    TData = ImagXpress1.TagGetDataItem(1)
    ImagXpress1.TagDelete TTag
    ' ReAdd the Tag
    ImagXpress1.TagAdd TTag, TType, TCount, TData
  Next i
  
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
  
  
  ShowTags lstTiffTagList, ImagXpress1
End Sub

Private Sub cmdExit_Click()
    End
End Sub

Private Sub cmdLoad_Click()
  LoadFile
  txtSaveFile.Text = sSaveFileName
  
  Label1.Caption = "W: " & ImagXpress1.IWidth & "  H: " & ImagXpress1.IHeight
  ShowTags lstTiffTagList, ImagXpress1
End Sub

Private Sub cmdAddMulti_Click()
  Dim R(2) As Long ' Holder for Rational Data
  ' Byte - TIFF Type 1
  ImagXpress1.TagAdd 201, TAG_BYTE, 3, 1
  ImagXpress1.TagModify 201, TAG_BYTE, 2, 2
  ImagXpress1.TagModify 201, TAG_BYTE, 3, 3
  ' Ascii - TIFF Type 2
  ImagXpress1.TagAdd 202, TAG_ASCII, 4, "A"
  ImagXpress1.TagModify 202, TAG_ASCII, 2, "B"
  ImagXpress1.TagModify 202, TAG_ASCII, 3, "C"
  ' Short - TIFF Type 3
  ImagXpress1.TagAdd 203, TAG_SHORT, 3, 3
  ImagXpress1.TagModify 203, TAG_SHORT, 2, 4
  ImagXpress1.TagModify 203, TAG_SHORT, 3, 5
  ' Long - TIFF Type 4
  ImagXpress1.TagAdd 204, TAG_LONG, 3, 4
  ImagXpress1.TagModify 204, TAG_LONG, 2, 5
  ImagXpress1.TagModify 204, TAG_LONG, 3, 6
  ' Rational - TIFF Type 5
  R(0) = 600 ' Numerator
  R(1) = 2 ' Denominator
  ImagXpress1.TagAdd 205, TAG_RATIONAL, 3, R
  R(0) = 720 ' Numerator
  R(1) = 10 ' Denominator
  ImagXpress1.TagModify 205, TAG_RATIONAL, 2, R
  R(0) = 300 ' Numerator
  R(1) = 1 ' Denominator
  ImagXpress1.TagModify 205, TAG_RATIONAL, 3, R
  ' SByte - TIFF Type 6
  ImagXpress1.TagAdd 206, TAG_SBYTE, 3, -6
  ImagXpress1.TagModify 206, TAG_SBYTE, 2, -7
  ImagXpress1.TagModify 206, TAG_SBYTE, 3, -8
  ' Undefine - TIFF Type 7
  ImagXpress1.TagAdd 207, TAG_UNDEFINE, 3, 7
  ImagXpress1.TagModify 207, TAG_UNDEFINE, 2, 8
  ImagXpress1.TagModify 207, TAG_UNDEFINE, 3, 9
  ' SShort - TIFF Type 8
  ImagXpress1.TagAdd 208, TAG_SSHORT, 3, -8
  ImagXpress1.TagModify 208, TAG_SSHORT, 2, -9
  ImagXpress1.TagModify 208, TAG_SSHORT, 3, -10
  ' SLong - TIFF Type 9
  ImagXpress1.TagAdd 209, TAG_SLONG, 3, -9
  ImagXpress1.TagModify 209, TAG_SLONG, 2, -10
  ImagXpress1.TagModify 209, TAG_SLONG, 3, -11
  ' SRational - TIFF Type 10
  R(0) = -600 ' Numerator
  R(1) = 2 ' Denominator
  ImagXpress1.TagAdd 210, TAG_RATIONAL, 3, R
  R(0) = -720 ' Numerator
  R(1) = 10 ' Denominator
  ImagXpress1.TagModify 210, TAG_RATIONAL, 2, R
  R(0) = -300 ' Numerator
  R(1) = 1 ' Denominator
  ImagXpress1.TagModify 210, TAG_RATIONAL, 3, R
  ' Float  - TIFF Type 11
  ImagXpress1.TagAdd 211, TAG_FLOAT, 3, 11.65
  ImagXpress1.TagModify 211, TAG_FLOAT, 2, -12.65
  ImagXpress1.TagModify 211, TAG_FLOAT, 3, -13.65
  ' Double  - TIFF Type 12
  ImagXpress1.TagAdd 212, TAG_DOUBLE, 3, 12.42
  ImagXpress1.TagModify 212, TAG_DOUBLE, 2, -13.65
  ImagXpress1.TagModify 212, TAG_DOUBLE, 3, -14.65
  
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
  
  ShowTags lstTiffTagList, ImagXpress1

End Sub


Private Sub cmdSave_Click()
    
  ' Save the File
  ImagXpress1.SaveFileName = txtSaveFile.Text
  ImagXpress1.SaveTIFFCompression = TIFF_Uncompressed
  ImagXpress1.SaveFile
  
  If ImagXpress1.ImagError = 0 Then
  
  
    DoEvents
    ' Clear everything (for demonstration)
    ImagXpress1.FileName = ""
    ShowTags lstTiffTagList, ImagXpress1
    DoEvents
    ' Load the Saved image back in and show the tags
    ImagXpress1.FileName = txtSaveFile.Text
    
    Err = ImagXpress1.ImagError
     PegasusError Err, lblError
     
    ShowTags lstTiffTagList, ImagXpress1
  
  Else
  
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
  End If
End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = txtLoadFile.Text
           
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub
Private Sub Form_Load()
    
    
    ImagXpress1.ScrollBars = SB_Both
    
    imgParentDir = App.Path
   ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
   
   imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\shack8.tif"
   sSaveFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\test.tif"
   
   txtLoadFile.Text = imgFileName
   txtSaveFile.Text = sSaveFileName
    
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        
        txtLoadFile.Text = imgFileName
                
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    
 End Sub




// stock.h : main header file for the STOCK application
//

#if !defined(AFX_STOCK_H__E21CA248_560C_11D3_A8FB_EED2BEB31C39__INCLUDED_)
#define AFX_STOCK_H__E21CA248_560C_11D3_A8FB_EED2BEB31C39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CStockApp:
// See stock.cpp for the implementation of this class
//

class CStockApp : public CWinApp
{
public:
	CStockApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStockApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CStockApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STOCK_H__E21CA248_560C_11D3_A8FB_EED2BEB31C39__INCLUDED_)

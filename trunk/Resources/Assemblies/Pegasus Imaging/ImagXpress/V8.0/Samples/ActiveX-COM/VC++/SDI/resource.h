//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SDI.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_SDITYPE                     129
#define IDD_ZOOMDLG                     130
#define IDC_ZOOMF                       1000
#define IDC_SLIDER1                     1001
#define IDC_SCROLLBAR1                  1002
#define ID_FLIP                         32771
#define ID_PROCESSING_MIRROR            32772
#define ID_PROCESSING_FLIP              32773
#define ID_PROCESSING_ROTATE90          32774
#define ID_PROCESSING_ROTATE180         32775
#define ID_PROCESSING_ROTATE270         32776
#define ID_PROCESSING_ZOOM              32777
#define ID_PROCESSING_GRAYSCALE         32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

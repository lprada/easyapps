VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ImagXpress - Tiff Large Data Tags Example"
   ClientHeight    =   7605
   ClientLeft      =   3630
   ClientTop       =   2730
   ClientWidth     =   13980
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   13980
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "TiffData.frx":0000
      Left            =   240
      List            =   "TiffData.frx":000D
      TabIndex        =   12
      Top             =   240
      Width           =   13335
   End
   Begin VB.TextBox txtLoadFile 
      Height          =   855
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   11
      Top             =   3000
      Width           =   4935
   End
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   10200
      TabIndex        =   8
      Top             =   1920
      Width           =   3495
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   3495
      Left            =   5160
      TabIndex        =   6
      Top             =   3960
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   6165
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1041845902
      ErrInfo         =   534271641
      Persistence     =   -1  'True
      _cx             =   8705
      _cy             =   6165
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3495
      Left            =   120
      TabIndex        =   5
      Top             =   3960
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   6165
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1041845902
      ErrInfo         =   534271641
      Persistence     =   -1  'True
      _cx             =   8705
      _cy             =   6165
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "3) Load Embedded TIFF"
      Height          =   375
      Left            =   240
      TabIndex        =   0
      Top             =   2160
      Width           =   2535
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "2) Clear"
      Height          =   375
      Left            =   240
      TabIndex        =   4
      Top             =   1800
      Width           =   2535
   End
   Begin VB.CommandButton cmdCreate 
      Caption         =   "1) Create Embedded TIFF"
      Default         =   -1  'True
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   1440
      Width           =   2535
   End
   Begin VB.Label lblError 
      Height          =   1695
      Left            =   10320
      TabIndex        =   10
      Top             =   4680
      Width           =   3375
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   10320
      TabIndex        =   9
      Top             =   4080
      Width           =   2535
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   495
      Left            =   10200
      TabIndex        =   7
      Top             =   1440
      Width           =   2415
   End
   Begin VB.Label Label3 
      Caption         =   "Tag 33122 Image (Image to be embedded):"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5160
      TabIndex        =   2
      Top             =   3600
      Width           =   4335
   End
   Begin VB.Label Label2 
      Caption         =   "Tiff Image:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   2640
      Width           =   1695
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Const TAG_UNDEFINE = 7
Private Sub cmdClear_Click()
  ' Clear the controls
  ImagXpress1.FileName = ""
  ImagXpress2.FileName = ""
  
  txtLoadFile.Text = ""
End Sub

Private Sub cmdCreate_Click()
  ' Load an image into ImagXpress2 and save it to a Buffer
  ' This is the embedded Image
  ImagXpress2.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\shack8.tif"
  ImagXpress2.SaveFileType = FT_TIFF
  ImagXpress2.SaveToBuffer = True
  ImagXpress2.SaveFile

  
  'reload the Image if the Clear button was clicked
  If txtLoadFile.Text = "" Then
   
    txtLoadFile.Text = imgFileName
    ImagXpress1.FileName = txtLoadFile.Text
    
     Err = ImagXpress1.ImagError
     PegasusError Err, lblError
    
  End If
  
  ' Embed the ImagXpress2 Image into private tag 33122
  ImagXpress1.TagSetData 33122, TAG_UNDEFINE, ImagXpress2.SaveBufferHandle
  
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
  
  ' Save the embedded image
  ImagXpress1.SaveFileName = App.Path & "\..\..\..\..\..\..\Common\Images\Embedded.tif"
  ImagXpress1.SaveFile
  
  ImagXpress2.DeleteSaveBuffer
End Sub



Private Sub cmdExit_Click()
    End
End Sub

Private Sub cmdLoad_Click()
  ' Load the embedded TIFF image
  Dim vData As Variant
  Dim tCount As Long
  
  txtLoadFile.Text = App.Path & "\..\..\..\..\..\..\Common\Images\Embedded.tif"

  ' Load and display the image
  ImagXpress1.FileName = txtLoadFile.Text
  
  ' Look for our private tag
  ImagXpress1.TagFind 33122
  If ImagXpress1.ImagError <> 0 Then
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
        Exit Sub
  
  End If
  
  tCount = ImagXpress1.TagCount
  
  ' Load the Private Tag Info
  vData = ImagXpress1.TagData
  
  ' Send the Private Tag data to ImagXpress2
  ImagXpress2.LoadBlob vData, tCount

End Sub

Private Sub Form_Load()

  Dim sDesc As String

 
  
  ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
  ImagXpress2.EventSetEnabled EVENT_PROGRESS, True
  
  ImagXpress1.ScrollBars = SB_Both
  ImagXpress2.ScrollBars = SB_Both
   
  ' Load a TIFF image into ImagXpress1
  
  imgFileName = App.Path + "\..\..\..\..\..\..\Common\Images\shack1.tif"
  
  txtLoadFile.Text = imgFileName
  ImagXpress1.FileName = txtLoadFile.Text
  
   
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
  
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub ImagXpress2_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub lblTitle_Click()

End Sub

Private Sub mnuAbout_Click()
    
    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        
        txtLoadFile.Text = imgFileName
        ImagXpress1.FileName = txtLoadFile.Text
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
              
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
    If ImagXpress2.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress2.ToolbarActivated = Not ImagXpress2.ToolbarActivated
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
    
End Sub


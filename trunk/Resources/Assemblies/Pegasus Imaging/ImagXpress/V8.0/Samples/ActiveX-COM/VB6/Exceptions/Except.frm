VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Except 
   Caption         =   "ImagXpress 7 - Raise Exceptions Example"
   ClientHeight    =   6555
   ClientLeft      =   60
   ClientTop       =   645
   ClientWidth     =   7635
   LinkTopic       =   "Form1"
   ScaleHeight     =   6555
   ScaleWidth      =   7635
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "Except.frx":0000
      Left            =   240
      List            =   "Except.frx":000A
      TabIndex        =   4
      Top             =   360
      Width           =   6975
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   2295
      Left            =   360
      TabIndex        =   3
      Top             =   1800
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   735279837
      ErrInfo         =   -109218741
      Persistence     =   -1  'True
      _cx             =   5106
      _cy             =   4048
      AutoSize        =   3
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdDoNotRaise 
      Caption         =   "Do Not Raise Exceptions"
      Height          =   615
      Left            =   240
      TabIndex        =   1
      Top             =   5160
      Width           =   3015
   End
   Begin VB.CommandButton cmdRaise 
      Caption         =   "Raise Exceptions"
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   4320
      Width           =   3015
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   4560
      TabIndex        =   5
      Top             =   4080
      Width           =   2175
   End
   Begin VB.Label lblError 
      BorderStyle     =   1  'Fixed Single
      Height          =   1815
      Left            =   4560
      TabIndex        =   2
      Top             =   4560
      Width           =   2775
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Except"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Private Sub cmdDoNotRaise_Click()
  ' Demonstrate reporting an error by setting the filename
  ' property to a bogus name that cannot be found.
  ImagXpress1.RaiseExceptions = False ' Do Not raise an exception if there is an error!
  ImagXpress1.FileName = "This file cannot be found"
  Err = ImagXpress1.ImagError
  If Err <> 0 Then ' If we had an error...
    lblError.Caption = "Error number " + Str(ImagXpress1.ImagError) + " occured." & Chr$(10) & "An error has occurred.  We have successfully recovered!"
  End If
End Sub

Private Sub cmdRaise_Click()
  ' Demonstrate raising an exception by setting the filename
  ' property to a bogus name that cannot be found. This is handy because
  ' you can use the On Error as a generic catch-all for errors as
  ' opposed to querying ImagError after every operation that could
  ' potentially cause an error.
  On Error GoTo HandleError
  ImagXpress1.RaiseExceptions = True ' Raise an exception if there is an error!
  ImagXpress1.FileName = "This file cannot be found"
  Exit Sub
  
HandleError:
  ' An Exception was raised and an error has ocurred
  
  lblError.Caption = "Error number " + Str(ImagXpress1.ImagError) + " occured." & Chr$(10) + "An error has occurred and an exception has been raised.  We have successfully recovered!"
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
    
End Sub

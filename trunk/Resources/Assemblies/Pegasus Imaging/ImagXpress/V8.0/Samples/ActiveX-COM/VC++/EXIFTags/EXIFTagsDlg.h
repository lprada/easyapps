#ifndef _EXIFTAGSDLG_H_
#define _EXIFTAGSDLG_H_

//
// INCLUDES
//

//{{AFX_INCLUDES()
//}}AFX_INCLUDES

using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"

//
// DEFINES
//

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEXIFTagsDlg 
    : public CDialog
{
// Construction
public:
	CEXIFTagsDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CEXIFTagsDlg)
	enum { IDD = IDD_EXIFTAGS_DIALOG };
	CListBox	m_ctlExifTagsList;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEXIFTagsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

    enum EExifTagType
    {
        TAG_BYTE = 1,
        TAG_ASCII = 2,
        TAG_SHORT = 3,
        TAG_LONG = 4,
        TAG_RATIONAL = 5,
        TAG_SBYTE = 6,
        TAG_UNDEFINE = 7, //* 8 bit byte
        TAG_SSHORT = 8,
        TAG_SLONG = 9,
        TAG_SRATIONAL = 10,
        TAG_FLOAT = 11,
        TAG_DOUBLE = 12,
    };

    enum EExifTagNumber
    {
        TAG_Artist = 0x13b,
        TAG_BitsPerSample = 0x102,
        TAG_CellLength = 0x109,
        TAG_CellWidth = 0x108,
        TAG_ColorMap = 0x140,
        TAG_Compression = 0x103,
        TAG_Copyright = 0x8298,
        TAG_DateTime = 0x132,
        TAG_DocumentName = 0x10d,
        TAG_DotRange = 0x150,
        TAG_ExifAperture = 0x9202,
        TAG_ExifBrightness = 0x9203,
        TAG_ExifCfaPattern = 0xA302,
        TAG_ExifColorSpace = 0xA001,
        TAG_ExifCompBPP = 0x9102,
        TAG_ExifCompConfig = 0x9101,
        TAG_ExifDTDigitized = 0x9004,
        TAG_ExifDTDigSS = 0x9292 ,
        TAG_ExifDTOrig = 0x9003 ,
        TAG_ExifDTOrigSS = 0x9291 ,
        TAG_ExifDTSubsec = 0x9290 ,
        TAG_ExifExposureBias = 0x9204,
//        TAG_ExifExposureIndex = 0X9215,
        TAG_ExifExposureIndex = 0xA215,
        TAG_ExifExposureProg = 0x8822,
        TAG_ExifExposureTime = 0x829A,
        TAG_ExifFileSource = 0xA300,
        TAG_ExifFlash = 0x9209,
//        TAG_ExifFlashEnergy = 0X920B,
        TAG_ExifFlashEnergy = 0xA20B,
        TAG_ExifFNumber = 0x829D,
        TAG_ExifFocalLength = 0x920A,
        TAG_ExifFocalResUnit = 0xA210 ,
        TAG_ExifFocalXRes = 0xA20E ,
        TAG_ExifFocalYRes = 0xA20F ,
        TAG_ExifFPXVer = 0xA000,
        TAG_ExifIFD = 0x8769,
        TAG_ExifImageHistory = 0X9213,
        TAG_ExifImageNumber = 0X9211,
        TAG_ExifInterlace = 0X8829,
        TAG_ExifInterop = 0xA005,
        TAG_ExifISOSpeed = 0x8827,
        TAG_ExifLightSource = 0x9208,
        TAG_ExifMakerNote = 0x927C,
        TAG_ExifMaxAperture = 0x9205,
        TAG_ExifMeteringMode = 0x9207,
        TAG_ExifNoise = 0X920D,
        TAG_ExifOECF = 0x8828,
        TAG_ExifPixXDim = 0xA002,
        TAG_ExifPixYDim = 0xA003,
        TAG_ExifRelatedWav = 0xA004 ,
        TAG_ExifSceneType = 0xA301,
        TAG_ExifSecurityClassification = 0X9212,
        TAG_ExifSelfTimerMode = 0X882B,
        TAG_ExifSensingMethod = 0xA217,
        TAG_ExifShutterSpeed = 0x9201,
        TAG_ExifSpatialFR = 0xA20C ,
        TAG_ExifSpatialFrequencyResponse = 0X920C,
        TAG_ExifSpectralSense = 0x8824,
        TAG_ExifSubjectDist = 0x9206,
        TAG_ExifSubjectLoc = 0xA214,
        TAG_ExifSubjectLocation = 0X9214,
        TAG_ExifTIFF_EPStandardID = 0X9216,
        TAG_ExifTimeZoneOffset = 0X882A,
        TAG_ExifUserComment = 0x9286,
        TAG_ExifVer = 0x9000,
        TAG_ExtraSamples = 0x152,
        TAG_FillOrder = 0x10a,
        TAG_FreeByteCounts = 0x121,
        TAG_FreeOffsets = 0x120,
        TAG_GrayResponseCurve = 0x123,
        TAG_GrayResponseUnit = 0x122,
        TAG_HalftoneHints = 0x141,
        TAG_HostComputer = 0x13c,
        TAG_ImageDescription = 0x10e,
        TAG_ImageLength = 0x101,
        TAG_ImageWidth = 0x100,
        TAG_InkNames = 0x14d,
        TAG_InkSet = 0x14c,
        TAG_JPEGACTables = 0x209,
        TAG_JPEGDCTables = 0x208,
        TAG_JPEGDCTTables = 0x208,
        TAG_JPEGInterchangeFormat = 0x201,
        TAG_JPEGInterchangeFormatLength = 0x202,
        TAG_JPEGLosslessPredictors = 0x205,
        TAG_JPEGPointTransforms = 0x206,
        TAG_JPEGProc = 0x200,
        TAG_JPEGQTables = 0x207,
        TAG_JPEGRestartInterval = 0x203,
        TAG_Make = 0x10f,
        TAG_MaxSampleValue = 0x119,
        TAG_MinSampleValue = 0x118,
        TAG_Model = 0x110,
        TAG_NewSubfileType = 0xfe,
        TAG_NumberOfInks = 0x14e,
        TAG_Orientation = 0x112,
        TAG_PageName = 0x11d,
        TAG_PageNumber = 0x129,
        TAG_PhotometricInterpretation = 0x106,
        TAG_PlanarConfiguration = 0x11c,
        TAG_Predictor = 0x13d,
        TAG_PrimaryChromaticities = 0x13f,
        TAG_ReferenceBlackWhite = 0x214,
        TAG_ResolutionUnit = 0x128,
        TAG_RowsPerStrip = 0x116,
        TAG_SampleFormat = 0x153,
        TAG_SamplesPerPixel = 0x115,
        TAG_SMaxSampleValue = 0x155,
        TAG_SMinSampleValue = 0x154,
        TAG_Software = 0x131,
        TAG_StripByteCounts = 0x117,
        TAG_StripOffsets = 0x111,
        TAG_SubfileType = 0xff,
        TAG_T4Options = 0x124,
        TAG_T6Options = 0x125,
        TAG_TargetPrinter = 0x151,
        TAG_Threshholding = 0x107,
        TAG_TileByteCounts = 0x145,
        TAG_TileLength = 0x143,
        TAG_TileOffsets = 0x144,
        TAG_TileWidth = 0x142,
        TAG_TransferFunction = 0x12d,
        TAG_TransferRange = 0x156,
        TAG_WhitePoint = 0x13e,
        TAG_XPosition = 0x11e,
        TAG_XResolution = 0x11a,
        TAG_YCbCrCoefficients = 0x211,
        TAG_YCbCrPositioning = 0x213,
        TAG_YCbCrSubSampling = 0x212,
        TAG_YPosition = 0x11f,
        TAG_YResolution = 0x11b,
    };

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CEXIFTagsDlg)
	virtual BOOL OnInitDialog();
    virtual void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnLoadexiftags();
	afx_msg void OnBtnAddacopyrighttag();
	afx_msg void OnBtnModifyresolutiontags();
	afx_msg void OnBtnDeletecopyrighttag();
	afx_msg void OnBtnSettags();
	afx_msg void OnBtnExit();
	afx_msg void OnFileOpen();
	afx_msg void OnToolbarShow();
	afx_msg void OnFile();
	afx_msg void OnFileExit();
	afx_msg void OnHelpAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

bool            GetImagePath                (CString& strImageFilePath) const;
//ImagXpress pointers
  CImagXpress*    m_ppCImagXpress;
  IImagXpressPtr  m_pImagXpress;

    void            getInputFilePath            (CString&) const;
    static void     getTagTypeDescription       (const EExifTagType, CString&);
    static void     getTagDescription           (const EExifTagNumber, CString&);
    void            getExifTags                 (IImagXpressPtr&, CListBox&);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _EXIFTAGSDLG_H_

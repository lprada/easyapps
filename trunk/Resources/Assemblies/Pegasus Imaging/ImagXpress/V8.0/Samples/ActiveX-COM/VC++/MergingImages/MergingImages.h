// MergingImages.h : main header file for the MERGINGIMAGES application
//

#if !defined(AFX_MERGINGIMAGES_H__C51DFD91_DB1C_43A5_A3EC_AAEA4A4504D6__INCLUDED_)
#define AFX_MERGINGIMAGES_H__C51DFD91_DB1C_43A5_A3EC_AAEA4A4504D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CMergingImagesApp:
// See MergingImages.cpp for the implementation of this class
//

class CMergingImagesApp : public CWinApp
{
public:
	CMergingImagesApp();
	
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMergingImagesApp)
public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL
	
	// Implementation
	
	//{{AFX_MSG(CMergingImagesApp)
	// NOTE - the ClassWizard will add and remove member functions here.
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MERGINGIMAGES_H__C51DFD91_DB1C_43A5_A3EC_AAEA4A4504D6__INCLUDED_)

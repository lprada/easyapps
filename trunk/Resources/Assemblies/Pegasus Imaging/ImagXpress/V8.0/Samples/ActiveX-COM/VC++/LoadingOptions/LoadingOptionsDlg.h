// LoadingOptionsDlg.h : header file
//

#if !defined(AFX_LOADINGOPTIONSDLG_H__390EC92D_7E43_42F0_867E_37831AF97BCD__INCLUDED_)
#define AFX_LOADINGOPTIONSDLG_H__390EC92D_7E43_42F0_867E_37831AF97BCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"

/////////////////////////////////////////////////////////////////////////////
// CLoadingOptionsDlg dialog

class CLoadingOptionsDlg : public CDialog
{
// Construction
public:
	CLoadingOptionsDlg(CWnd* pParent = NULL);	// standard constructor

	CImagXpress *ppCImagXpress1;
	IImagXpressPtr pImagXpress1;

// Dialog Data
	//{{AFX_DATA(CLoadingOptionsDlg)
	enum { IDD = IDD_LOADINGOPTIONS_DIALOG };
	CButton	m_chkRotated;
	CButton	m_chkResize;
	CButton	m_chkCrop;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoadingOptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CLoadingOptionsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnLCropCheck();
	afx_msg void OnLResizeCheck();
	afx_msg void OnLRotatedCheck();
	afx_msg void OnCmdLoadImage();
	afx_msg void OnExit();
	afx_msg void OnFile();
	afx_msg void OnFileQuit();
	afx_msg void OnToolbarShow();
	afx_msg void OnHelpAbout();
	afx_msg void OnLoad();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOADINGOPTIONSDLG_H__390EC92D_7E43_42F0_867E_37831AF97BCD__INCLUDED_)

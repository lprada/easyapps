VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Despeckle 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress Despeckle Example"
   ClientHeight    =   8655
   ClientLeft      =   150
   ClientTop       =   735
   ClientWidth     =   14550
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   14550
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "Despeckle.frx":0000
      Left            =   120
      List            =   "Despeckle.frx":000A
      TabIndex        =   8
      Top             =   240
      Width           =   12255
   End
   Begin VB.ListBox lstStatus 
      Height          =   2790
      Left            =   10560
      TabIndex        =   5
      Top             =   1920
      Width           =   3135
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress2 
      Height          =   4335
      Left            =   5640
      TabIndex        =   3
      Top             =   3240
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   7646
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   547451643
      ErrInfo         =   1000365026
      Persistence     =   -1  'True
      _cx             =   7435
      _cy             =   7646
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdDespeckle 
      Caption         =   "Despeckle Image"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4800
      TabIndex        =   0
      Top             =   1800
      Width           =   2295
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress1 
      Height          =   4335
      Left            =   120
      TabIndex        =   9
      Top             =   3240
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   7646
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   547451643
      ErrInfo         =   1000365026
      Persistence     =   -1  'True
      _cx             =   7435
      _cy             =   7646
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   2775
      Left            =   10560
      TabIndex        =   7
      Top             =   5640
      Width           =   3015
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   10560
      TabIndex        =   6
      Top             =   4920
      Width           =   2535
   End
   Begin VB.Label lblLooadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   10560
      TabIndex        =   4
      Top             =   1440
      Width           =   2535
   End
   Begin VB.Label Label2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   6000
      TabIndex        =   2
      Top             =   2640
      Width           =   2295
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      TabIndex        =   1
      Top             =   2640
      Width           =   2295
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Despeckle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Private Sub cmdDespeckle_Click()
  Xpress2.FileName = Xpress1.FileName
  Xpress2.Despeckle
  DoEvents
  Label1.Caption = "Before Despeckle:"
  Label2.Caption = "After Despeckle:"
End Sub

Private Sub cmdQuit_Click()
    End
End Sub

Private Sub LoadFile()
   Xpress1.FileName = imgFileName
        
   Err = Xpress1.ImagError
   PegasusError Err, lblError
   
End Sub

Private Sub Form_Load()
  
  Xpress1.ScrollBars = 3
  Xpress2.ScrollBars = 3
  
  
  imgParentDir = App.Path
  
  Xpress1.EventSetEnabled EVENT_PROGRESS, True
  Xpress2.EventSetEnabled EVENT_PROGRESS, True
  
  Err = Xpress1.ImagError
  PegasusError Err, lblError
  
  imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\despeckle.tif"
  
  LoadFile
End Sub

Private Sub Xpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub Xpress2_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuAbout_Click()
    Xpress1.AboutBox
End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    
    If Xpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    Xpress1.ToolbarActivated = Not Xpress1.ToolbarActivated
    
    Err = Xpress1.ImagError
    PegasusError Err, lblError
    
     If Xpress2.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    Xpress2.ToolbarActivated = Not Xpress2.ToolbarActivated
    
    Err = Xpress2.ImagError
    PegasusError Err, lblError
    
    
    End Sub



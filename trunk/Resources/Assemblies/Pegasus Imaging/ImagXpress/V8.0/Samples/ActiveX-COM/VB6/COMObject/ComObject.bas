Attribute VB_Name = "ComObjectMod"
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

' Creates an ImagXpress Object
Public Sub CreateImagXpress(ByRef ImagXpressObj As PegasusImagingActiveXImagXpress8.ImagXpress)

Dim x As Long

On Error GoTo Done
  x = ImagXpressObj.hWnd
  ' If there was no error (ie: ImagXpress Object exists), tell us
  ' that it's already been created!
  MsgBox "Can't create ImagXpress Object because it already exists!"
  Exit Sub
  ' There's no ImagXpress so we can go ahead and create it
Done:
  Set ImagXpressObj = New PegasusImagingActiveXImagXpress8.ImagXpress
  
    
  ImagXpressObj.CreateCtlWindow comobject.hWnd, 210, 170, 200, 200
  
End Sub


' Destroys the ImagXpress Object
Public Sub DestroyImagXpress(ImagXpressObj As PegasusImagingActiveXImagXpress8.ImagXpress)

Dim x As Long

On Error GoTo Done
  x = ImagXpressObj.hWnd
  ' If there was an error (ie: No ImagXpress Object), then jump over this
  ImagXpressObj.DestroyCtlWindow
  Set ImagXpressObj = Nothing
Done:
End Sub


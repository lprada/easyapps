VERSION 5.00
Begin VB.Form frmSave 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Saving Options"
   ClientHeight    =   6975
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7275
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6975
   ScaleWidth      =   7275
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "Close"
      Height          =   375
      Left            =   5280
      TabIndex        =   2
      Top             =   6480
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Save Image"
      Height          =   495
      Left            =   2520
      TabIndex        =   1
      Top             =   4680
      Width           =   1575
   End
   Begin VB.Frame save 
      Caption         =   "Saving Options"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5295
      Left            =   480
      TabIndex        =   0
      Top             =   240
      Width           =   6255
      Begin VB.OptionButton Option5 
         Caption         =   "RAW"
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   2760
         Width           =   1815
      End
      Begin VB.OptionButton Option4 
         Caption         =   "Lossless JPG"
         Height          =   195
         Left            =   360
         TabIndex        =   10
         Top             =   2280
         Width           =   1695
      End
      Begin VB.OptionButton Option3 
         Caption         =   "JPEG 2000"
         Height          =   375
         Left            =   360
         TabIndex        =   9
         Top             =   1680
         Width           =   1575
      End
      Begin VB.OptionButton Option2 
         Caption         =   "JPEG-LS"
         Height          =   375
         Left            =   360
         TabIndex        =   8
         Top             =   1200
         Width           =   1575
      End
      Begin VB.OptionButton Option1 
         Caption         =   "JPEG"
         Height          =   375
         Left            =   360
         TabIndex        =   7
         Top             =   720
         Width           =   1575
      End
      Begin VB.Frame jpg2000 
         Caption         =   "JPEG 2000 Types"
         Height          =   1935
         Left            =   2880
         TabIndex        =   3
         Top             =   1680
         Width           =   3135
         Begin VB.OptionButton Option8 
            Caption         =   "JPEG 2000 Lossless"
            Height          =   255
            Left            =   360
            TabIndex        =   6
            Top             =   1440
            Width           =   1815
         End
         Begin VB.OptionButton Option7 
            Caption         =   "JPEG 2000 GrayScale"
            Height          =   255
            Left            =   360
            TabIndex        =   5
            Top             =   960
            Width           =   2175
         End
         Begin VB.OptionButton Option6 
            Caption         =   "JPEG 2000 Lossy"
            Height          =   375
            Left            =   360
            TabIndex        =   4
            Top             =   360
            Width           =   2415
         End
      End
   End
End
Attribute VB_Name = "frmSave"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()

      

        frmHighGray.ImagXpress.SaveFile
        
        Err = frmHighGray.ImagXpress.ImagError
        PegasusError Err, frmHighGray.lblError
        
End Sub

Private Sub Command2_Click()
    frmSave.Hide
End Sub

Private Sub Form_Load()
    jpg2000.Visible = False
End Sub

Private Sub Option1_Click()
         jpg2000.Visible = False
        frmHighGray.ImagXpress.SaveFileName = App.Path & "\jpeg.jpg"
        frmHighGray.ImagXpress.SaveFileType = FT_JPG
End Sub

Private Sub Option2_Click()
    jpg2000.Visible = False
    frmHighGray.ImagXpress.SaveFileType = FT_JLS
    frmHighGray.ImagXpress.SaveFileName = App.Path & "\jlsnear.jls"
    
End Sub

Private Sub Option3_Click()
    If Option3.Value = True Then
        jpg2000.Visible = True
    Else
        jpg2000.Visible = False
    End If
End Sub

Private Sub Option4_Click()
     jpg2000.Visible = False
     frmHighGray.ImagXpress.SaveLJPCompMethod = LJPMETHOD_JPEG
     frmHighGray.ImagXpress.SaveLJPCompType = LJPTYPE_RGB
     frmHighGray.ImagXpress.SaveFileType = FT_LJP
     frmHighGray.ImagXpress.SaveFileName = App.Path & "\ljpRGB.ljp"
End Sub

Private Sub Option5_Click()
      jpg2000.Visible = False
    frmHighGray.ImagXpress.SaveFileName = App.Path & "\raw.raw"
    frmHighGray.ImagXpress.SaveFileType = FT_RAW
End Sub

Private Sub Option6_Click()
    ' JPEG 2000 lossy
     frmHighGray.ImagXpress.SaveCompressSize = 5000
     frmHighGray.ImagXpress.SaveFileName = App.Path & "\ jp2lossy.jp2"
     frmHighGray.ImagXpress.SaveFileType = FT_JP2
     frmHighGray.ImagXpress.SaveJP2Order = PO_DEFAULT
     frmHighGray.ImagXpress.SaveJP2Type = JP2_LOSSY
End Sub

Private Sub Option7_Click()
    'JPEG 2000 GrayScale
     frmHighGray.ImagXpress.SaveFileName = App.Path & "\jp2lossygray.jp2"
     frmHighGray.ImagXpress.SaveFileType = FT_JP2
     frmHighGray.ImagXpress.SaveJPEGGrayscale = True
     frmHighGray.ImagXpress.SaveJP2Type = JP2_LOSSY
End Sub

Private Sub Option8_Click()
    'lossless JPEG2000
    frmHighGray.ImagXpress.SaveCompressSize = 55000
    frmHighGray.ImagXpress.SaveFileName = App.Path & "\jp2lossless.jp2"
   frmHighGray.ImagXpress.SaveFileType = FT_JP2
    frmHighGray.ImagXpress.SaveJP2Order = PO_DEFAULT
    frmHighGray.ImagXpress.SaveJP2Type = JP2_LOSSLESS
End Sub

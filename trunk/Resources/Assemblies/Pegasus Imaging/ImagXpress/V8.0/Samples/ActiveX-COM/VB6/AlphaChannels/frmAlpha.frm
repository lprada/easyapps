VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmAlpha 
   Caption         =   "ImagXpress 8 Alpha Channels"
   ClientHeight    =   6585
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   10455
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   6585
   ScaleWidth      =   10455
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbAlpha 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      ItemData        =   "frmAlpha.frx":0000
      Left            =   120
      List            =   "frmAlpha.frx":0002
      TabIndex        =   5
      Text            =   "Combo1"
      Top             =   1920
      Width           =   2055
   End
   Begin VB.CommandButton cmdAlpha 
      Caption         =   "Merger Alpha Channel Image with Main Image"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   4
      Top             =   2640
      Width           =   1815
   End
   Begin VB.ListBox lstDesc 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "frmAlpha.frx":0004
      Left            =   120
      List            =   "frmAlpha.frx":0014
      TabIndex        =   2
      Top             =   120
      Width           =   10215
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   3615
      Left            =   6240
      TabIndex        =   1
      Top             =   2760
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   6376
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   55326525
      ErrInfo         =   817985230
      Persistence     =   -1  'True
      _cx             =   5318
      _cy             =   6376
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3615
      Left            =   2640
      TabIndex        =   0
      Top             =   2760
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   6376
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   55326525
      ErrInfo         =   817985230
      Persistence     =   -1  'True
      _cx             =   5318
      _cy             =   6376
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblSource 
      Alignment       =   2  'Center
      Caption         =   "Source Image"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6480
      TabIndex        =   7
      Top             =   2160
      Width           =   2415
   End
   Begin VB.Label lblADesc 
      Alignment       =   2  'Center
      Caption         =   "Alpha Channel Image"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3000
      TabIndex        =   6
      Top             =   2160
      Width           =   2295
   End
   Begin VB.Label lblerror 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5520
      TabIndex        =   3
      Top             =   1320
      Width           =   3855
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpen 
         Caption         =   "&OpenAlphaChannelImage"
      End
      Begin VB.Menu mnuAlphaChannel 
         Caption         =   "&OpenSourceImage"
      End
      Begin VB.Menu Quit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&ToolBar"
      Begin VB.Menu mnuShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmAlpha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Dim imgFileName As String
Dim imgFileName2 As String
Dim imgParentDir As String
Private Sub cmbAlpha_Click()
    Select Case cmbAlpha.ListIndex
    Case 0
            ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\alpha1.tif"
    Case 1
            ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\alpha2.tif"
       
    Case 2
            ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\alpha3.tif"
    End Select
End Sub

Private Sub cmdAlpha_Click()
    '1)Define the area to merge the Alpha Image
     ImagXpress2.Area True, 250, 250, 400, 400
     '2)Use the Merge method with the desired parameters
     ImagXpress2.Merge True, MSZ_Crop, MST_Alpha_ForeGround_Over_BackGround, False, RGB(0, 0, 255), 90, 90
     '3)Set the chosen file name to merge with the alpha channel
     ImagXpress2.FileName = ImagXpress1.FileName
    ' 4)Set Merge to False
     ImagXpress2.Merge False, MSZ_Crop, MST_Alpha_Most_Opaque, False, RGB(255, 0, 0), 20, 20
     
End Sub

Private Sub Command1_Click()
    ImagXpress2.FileName = ""
    
End Sub

Private Sub Form_Load()
    
        cmbAlpha.AddItem "Alpha1.tif", 0
        cmbAlpha.AddItem "Alpha2.tif", 1
        cmbAlpha.AddItem "Alpha3.tif", 2
        cmbAlpha.ListIndex = 0
    
        ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\alpha1.tif"
        ImagXpress2.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\alpha1.jpg"
      
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuAlphaChannel_Click()
    Dim tmpFN As String
    man = True
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName2 = tmpFN
        imgParentDir = PathFromFile(imgFileName2)
        MsgBox imgFileName2
        ImagXpress2.FileName = imgFileName2
        
        Err = ImagXpress2.ImagError
        PegasusError Err, lblerror
    End If
End Sub

Private Sub mnuOpen_Click()
    Dim tmpFN As String
    man = True
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        ImagXpress1.FileName = imgFileName
         Err = ImagXpress1.ImagError
        PegasusError Err, lblerror
    
    End If
End Sub

Private Sub mnuShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbar.Caption = "&Show"
    Else
        mnuToolbar.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    ImagXpress2.ToolbarActivated = Not ImagXpress2.ToolbarActivated
    Err = ImagXpress1.ImagError
    PegasusError Err, lblerror
End Sub

Private Sub Quit_Click()
    End
End Sub

// ZoomDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SDI.h"
#include "ZoomDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CZoomDlg dialog
#define SCROLL_MIN   1
#define SCROLL_MAX   1000
#define SCROLL_LINE  5
#define SCROLL_PAGE  100


CZoomDlg::CZoomDlg(CWnd* pParent /*=NULL*/)
: CDialog(CZoomDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CZoomDlg)
	//}}AFX_DATA_INIT
}


void CZoomDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CZoomDlg)
	DDX_Control(pDX, IDC_SCROLLBAR1, m_ZoomScroll);
	DDX_Text(pDX, IDC_ZOOMF, m_fZoomF);
	DDV_MinMaxFloat(pDX, m_fZoomF, 1.e-002f, 10.f);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CZoomDlg, CDialog)
//{{AFX_MSG_MAP(CZoomDlg)
ON_WM_HSCROLL()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZoomDlg message handlers

void CZoomDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	
	// TODO: Add your message handler code here and/or call default
	if (pScrollBar->GetDlgCtrlID() == IDC_SCROLLBAR1)
	{
		// NOTE: We made the scroll bar units 100 times the zoom units.  This is because the
		// scroll bar operates in integers.  This will give us zoom resolution of 1/100
		// of the original image.
		int spos = pScrollBar->GetScrollPos();
		
		switch(nSBCode)
		{
		case SB_LEFT:       // Scroll to far left
			spos = SCROLL_MIN;  
			break;
		case SB_ENDSCROLL:  // End scroll
			return;
		case SB_RIGHT:      // Scroll to far right.
			spos = SCROLL_MAX;
			break;
		case SB_LINELEFT:   // Scroll one line left
			spos -= SCROLL_LINE;
			spos = spos>SCROLL_MIN ? spos : SCROLL_MIN;
			break;
		case SB_LINERIGHT:  // Scroll one line right
			spos += SCROLL_LINE;
			spos = spos<SCROLL_MAX ? spos : SCROLL_MAX;
			break;
		case SB_PAGELEFT:   // Scroll one page left
			spos -= SCROLL_PAGE;
			spos = spos>SCROLL_MIN ? spos : SCROLL_MIN;
			break;
		case SB_PAGERIGHT:  // Scroll one page right
			spos += SCROLL_PAGE;
			spos = spos<SCROLL_MAX ? spos : SCROLL_MAX;
			break;
		case SB_THUMBPOSITION: // Scroll to absolute position. 
		case SB_THUMBTRACK:    // Drag scroll box to specified position. 
			spos = (unsigned short)nPos; 
			break;
		}
		char buffer[20];
		pScrollBar->SetScrollPos(spos, TRUE);
		m_fZoomF = (float)((float)pScrollBar->GetScrollPos()/100.0);
		sprintf(buffer, "%5.2f", m_fZoomF);
		SetDlgItemText(IDC_ZOOMF,  buffer); 
	}
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CZoomDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_ZoomScroll.SetScrollRange(SCROLL_MIN, SCROLL_MAX, TRUE);
	m_ZoomScroll.SetScrollPos((int)(m_fZoomF*100), TRUE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmAdjust 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Adjust Image"
   ClientHeight    =   5610
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5610
   ScaleWidth      =   7470
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraMedicalTransform 
      Caption         =   "Medical Grayscale Transform"
      Enabled         =   0   'False
      Height          =   2775
      Left            =   120
      TabIndex        =   12
      Top             =   2040
      Width           =   7215
      Begin MSComctlLib.Slider sliderCenter 
         Height          =   495
         Left            =   120
         TabIndex        =   18
         Top             =   1800
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   873
         _Version        =   393216
         TickStyle       =   1
      End
      Begin MSComctlLib.Slider sliderWidth 
         Height          =   495
         Left            =   120
         TabIndex        =   17
         Top             =   840
         Width           =   6255
         _ExtentX        =   11033
         _ExtentY        =   873
         _Version        =   393216
         TickStyle       =   1
      End
      Begin VB.TextBox txtRescaleIntercept 
         Alignment       =   1  'Right Justify
         Height          =   375
         Left            =   4680
         TabIndex        =   16
         Text            =   "0"
         Top             =   360
         Width           =   1695
      End
      Begin VB.TextBox txtRescaleSlope 
         Alignment       =   1  'Right Justify
         Height          =   375
         Left            =   1320
         TabIndex        =   15
         Text            =   "1"
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label lblCenter 
         Caption         =   "Window Center"
         Height          =   255
         Left            =   3000
         TabIndex        =   22
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label lblWinCenter 
         Caption         =   "0"
         Height          =   255
         Left            =   6600
         TabIndex        =   21
         Top             =   2040
         Width           =   495
      End
      Begin VB.Label lblWinWidth 
         Caption         =   "0"
         Height          =   255
         Left            =   6600
         TabIndex        =   20
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label lblWidth 
         Caption         =   "Window Width"
         Height          =   255
         Left            =   3000
         TabIndex        =   19
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblIntercept 
         Caption         =   "Rescale Intercept:"
         Height          =   255
         Left            =   3240
         TabIndex        =   14
         Top             =   420
         Width           =   1455
      End
      Begin VB.Label lblSlope 
         Caption         =   "Rescale Slope:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   435
         Width           =   1575
      End
   End
   Begin VB.Frame fraStdTransform 
      Caption         =   "Standard Grayscale Transform"
      Height          =   1695
      Left            =   2280
      TabIndex        =   5
      Top             =   120
      Width           =   5055
      Begin MSComctlLib.Slider sliderBrightness 
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   661
         _Version        =   393216
         Min             =   -50
         Max             =   50
         TickStyle       =   1
         TickFrequency   =   50
      End
      Begin MSComctlLib.Slider sliderContrast 
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   960
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   661
         _Version        =   393216
         Min             =   -50
         Max             =   50
         TickStyle       =   1
         TickFrequency   =   50
      End
      Begin VB.Label lblBrightnessValue 
         Caption         =   "0"
         Height          =   375
         Left            =   4200
         TabIndex        =   11
         Top             =   480
         Width           =   615
      End
      Begin VB.Label lblBrightness 
         Caption         =   "Brightness"
         Height          =   255
         Left            =   1800
         TabIndex        =   10
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblContrast 
         Caption         =   "Contrast"
         Height          =   255
         Left            =   1800
         TabIndex        =   9
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label lblContrastValue 
         Caption         =   "0"
         Height          =   255
         Left            =   4200
         TabIndex        =   8
         Top             =   1080
         Width           =   735
      End
   End
   Begin VB.OptionButton optMedTransform 
      Caption         =   "Medical Transform"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   2295
   End
   Begin VB.OptionButton optStdTransform 
      Caption         =   "Standard Transform"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   360
      Value           =   -1  'True
      Width           =   2295
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      Height          =   375
      Left            =   1680
      TabIndex        =   2
      Top             =   5040
      Width           =   1815
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   3600
      TabIndex        =   1
      Top             =   5040
      Width           =   1815
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   5520
      TabIndex        =   0
      Top             =   5040
      Width           =   1815
   End
End
Attribute VB_Name = "frmAdjust"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public bCanceled As Boolean

Private Sub cmdCancel_Click()
    bCanceled = True
    Me.Hide
End Sub

Private Sub cmdOK_Click()
    Call Me.Hide
End Sub

Private Sub cmdReset_Click()
    If (optStdTransform.Value) Then
        sliderBrightness.Value = 0
        sliderContrast.Value = 0
    Else
        sliderCenter.Value = 2048
        sliderWidth.Value = 4095
        txtRescaleSlope.Text = 1
        txtRescaleIntercept.Text = 0
    End If
End Sub

Private Sub Form_Activate()
    If (optStdTransform.Value) Then
        EnableStdTransform True
        EnableMedicalTransform False
    Else
        EnableStdTransform False
        EnableMedicalTransform True
    End If
End Sub

Private Sub Form_Load()
    bCanceled = False

    ' fill in defaults as required
    lblWinWidth.Caption = sliderWidth.Value
    lblWinCenter.Caption = sliderCenter.Value
End Sub

Private Sub optMedTransform_Click()
    EnableMedicalTransform True
    EnableStdTransform False
    
    ' switch to the proper gray mode in IX
    frmHighGray.ImagXpress.ViewGrayMode = GRAY_Medical
    frmHighGray.ImagXpress.MEDModalityLUTEnabled = False
    frmHighGray.ImagXpress.MEDValuesOfInterestLUTEnabled = False
End Sub

Private Sub optStdTransform_Click()
    EnableStdTransform True
    EnableMedicalTransform False
    
    ' switch to the proper gray mode in IX
    frmHighGray.ImagXpress.ViewGrayMode = GRAY_Standard
End Sub

Private Sub sliderBrightness_Change()
    lblBrightnessValue.Caption = sliderBrightness.Value
    frmHighGray.ImagXpress.ViewBrightness = CInt(lblBrightnessValue.Caption)
End Sub

Private Sub sliderCenter_Change()
    lblWinCenter.Caption = sliderCenter.Value
    frmHighGray.ImagXpress.MEDWindowCenter = CDbl(lblWinCenter.Caption)
End Sub


Private Sub sliderContrast_Change()
    lblContrastValue.Caption = sliderContrast.Value
    frmHighGray.ImagXpress.ViewContrast = CInt(lblContrastValue.Caption)
End Sub

Private Sub EnableStdTransform(bEnable As Boolean)
    fraStdTransform.Enabled = bEnable
    sliderBrightness.Enabled = bEnable
    sliderContrast.Enabled = bEnable
    lblBrightness.Enabled = bEnable
    lblContrast.Enabled = bEnable
    lblBrightnessValue.Enabled = bEnable
    lblContrastValue.Enabled = bEnable
End Sub

Private Sub EnableMedicalTransform(bEnable As Boolean)
    fraMedicalTransform.Enabled = bEnable
    lblSlope.Enabled = bEnable
    lblIntercept.Enabled = bEnable
    txtRescaleSlope.Enabled = bEnable
    txtRescaleIntercept.Enabled = bEnable
    sliderWidth.Enabled = bEnable
    sliderCenter.Enabled = bEnable
    lblWidth.Enabled = bEnable
    lblCenter.Enabled = bEnable
    lblWinWidth.Enabled = bEnable
    lblWinCenter.Enabled = bEnable
End Sub

Private Sub sliderWidth_Change()
    lblWinWidth.Caption = sliderWidth.Value
    frmHighGray.ImagXpress.MEDWindowWidth = CDbl(lblWinWidth.Caption)
End Sub

Private Sub txtRescaleIntercept_LostFocus()
    frmHighGray.ImagXpress.MEDRescaleIntercept = CDbl(txtRescaleIntercept.Text)
End Sub

Private Sub txtRescaleSlope_LostFocus()
    frmHighGray.ImagXpress.MEDRescaleSlope = CDbl(txtRescaleSlope.Text)
End Sub

VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmHighGray 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "High Gray Sample"
   ClientHeight    =   9150
   ClientLeft      =   150
   ClientTop       =   450
   ClientWidth     =   13515
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9150
   ScaleWidth      =   13515
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   2400
      Left            =   9960
      TabIndex        =   3
      Top             =   1800
      Width           =   3015
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "HighGray.frx":0000
      Left            =   120
      List            =   "HighGray.frx":000D
      TabIndex        =   1
      Top             =   240
      Width           =   12975
   End
   Begin MSComDlg.CommonDialog CommonDialogOpenFile 
      Left            =   9120
      Top             =   8520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress 
      Height          =   7935
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   13996
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   1305571652
      ErrInfo         =   -225243824
      Persistence     =   -1  'True
      _cx             =   16748
      _cy             =   13996
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   2895
      Left            =   10080
      TabIndex        =   5
      Top             =   5400
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   10080
      TabIndex        =   4
      Top             =   4800
      Width           =   2895
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   9960
      TabIndex        =   2
      Top             =   1200
      Width           =   2535
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpenFile 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuFileSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFileSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "View"
      Enabled         =   0   'False
      Begin VB.Menu mnuViewAdjust 
         Caption         =   "Adjust"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmHighGray"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Dim sImageFilter As String

Dim bSaving As Boolean

Private nlWidth As Long
Private nlHeight As Long
Private nlStride As Long
Private nlBitsPerPixel As Long
Private nlBytesPerPixel As Long
Private nlPixType As Long
Private bIsIntelOrder As Boolean
Private bIsTopDown As Boolean

Private nCurrentBrightness As Integer
Private nCurrentContrast As Integer
Private nlCurrentSlope As Long
Private nlCurrentIntercept As Long
Private nlCurrentWinWidth As Long
Private nlCurrentWinCenter As Long

Private Sub Form_Load()

    imgParentDir = App.Path
    ImagXpress.EventSetEnabled EVENT_PROGRESS, True
    
    Err = ImagXpress.ImagError
    PegasusError Err, lblError

    sImageFilter = "Raw Images (*.raw)" + vbNullChar + "*.raw" + vbNullChar + "JPEG Images (*.jpg)" + vbNullChar + "*.jpg" + vbNullChar + "Lossless JPEG Images (*.ljp)" + vbNullChar + "*.ljp" + _
                vbNullChar + "JPEG 2000 Images (*.jp2)" + vbNullChar + "*.jp2" + vbNullChar + "JPEG-LS Images (*.jls)" + vbNullChar + "*.jls" + vbNullChar + "All Images (*.*)" + vbNullChar + "*.*"
    
      

End Sub
Private Sub ImagXpress_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuAbout_Click()
    ImagXpress.AboutBox
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuFileExit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress.ToolbarActivated = Not ImagXpress.ToolbarActivated
    
    Err = ImagXpress.ImagError
    PegasusError Err, lblError
End Sub


Private Sub LoadFile()
   ImagXpress.FileName = imgFileName
        
   Err = ImagXpress.ImagError
   PegasusError Err, lblError
End Sub

Private Sub mnuFileOpenFile_Click()
    mnuSave.Enabled = True
    mnuView.Enabled = True
    
    Dim strFileExt As String
    Dim tmpFN As String
    
    On Error GoTo ErrorHandler
    
    ' ensure we are cleared out from any previous calls to load a file
    ImagXpress.FileName = ""
    ImagXpress.LoadRaw False, 0, 0, 0, 0, 0, 0, 0
    
       
    tmpFN = PegasusOpenFilePF(App.Path & "\..\..\..\..\..\..\Common\Images\", sImageFilter)
      
     
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        
        imgFileName = RTrim(imgFileName)
    
        ' get the last 5 characters from the right side of the file name
        ' in order to determine the file extension
        
        strFileExt = Right(imgFileName, 5)
        If (StrComp(strFileExt, ".raw" + Chr(0), vbTextCompare) = 0) Then
            frmRawDef.Show vbModal
            If frmRawDef.bCancel = False Then
                ' load in the raw parameters
                nlWidth = CLng(frmRawDef.txtWidth)
                nlHeight = CLng(frmRawDef.txtHeight)
                nlStride = CLng(frmRawDef.txtStride)
                nlBitsPerPixel = CLng(frmRawDef.txtBitsPerPixel)
                nlBytesPerPixel = CLng(frmRawDef.txtBytesPerPixel)
                bIsIntelOrder = CBool(frmRawDef.optIntel.Value)
                bIsTopDown = CBool(frmRawDef.optTopDown.Value)
                    
                If (bIsIntelOrder = False And bIsTopDown = False) Then
                    nlPixType = PIXTYPE_RAW Or PIXTYPE_HighGray Or PIXTYPE_BigEndian
                ElseIf (bIsIntelOrder = False And bIsTopDown = True) Then
                    nlPixType = PIXTYPE_RAW Or PIXTYPE_HighGray Or PIXTYPE_BigEndian Or PIXTYPE_TopDown
                ElseIf (bIsIntelOrder = True And bIsTopDown = False) Then
                    nlPixType = PIXTYPE_RAW Or PIXTYPE_HighGray
                ElseIf (bIsIntelOrder = True And bIsTopDown = True) Then
                    nlPixType = PIXTYPE_RAW Or PIXTYPE_HighGray Or PIXTYPE_TopDown
                End If
                
                ImagXpress.LoadRaw True, nlWidth, nlHeight, nlStride, nlBitsPerPixel, nlBytesPerPixel, nlBitsPerPixel - 1, nlPixType
                
                LoadFile
            
            
            Else
                Unload frmRawDef
                 ImagXpress.ViewUpdate = False
                Err.Raise cdlCancel, Me, "Cancel was selected."
            End If
        Else
            ' we are a compressed type, so we need to extract the image dimensions
            ' for us to use
            LoadFile
            nlWidth = ImagXpress.IWidth
            nlHeight = ImagXpress.IHeight
            nlBitsPerPixel = ImagXpress.IBPP
            nlStride = -1
        End If
    
   End If
    
    ' set default parameters based on the incoming image
    nCurrentBrightness = 0
    nCurrentContrast = 0
    
    nlCurrentSlope = 1
    nlCurrentIntercept = 0
    nlCurrentWinWidth = (2 ^ CLng(nlBitsPerPixel)) - 1
    nlCurrentWinCenter = nlCurrentWinWidth / 2
    
    ' we are going to start with standard unless the user tells us otherwise
    ImagXpress.ViewGrayMode = GRAY_Standard
    ImagXpress.ViewBrightness = nCurrentBrightness
    ImagXpress.ViewContrast = nCurrentContrast

    ImagXpress.ViewUpdate = True
        
    Exit Sub
ErrorHandler:
    ' handle your error here
End Sub

Private Sub mnuSave_Click()
    frmSave.Show vbModal
End Sub

Private Sub mnuViewAdjust_Click()
On Error GoTo errorTrap

    Call Load(frmAdjust)
    
    ' set up defaults of ranges that we need to
    frmAdjust.sliderWidth.Min = 1
    frmAdjust.sliderWidth.Max = 2 ^ CLng(nlBitsPerPixel)
    frmAdjust.sliderWidth.TickFrequency = (frmAdjust.sliderWidth.Max - frmAdjust.sliderWidth.Min) / nlBitsPerPixel
    frmAdjust.sliderWidth.Value = nlCurrentWinWidth

    frmAdjust.sliderCenter.Min = -2000
    frmAdjust.sliderCenter.Max = 2 ^ CLng(nlBitsPerPixel)
    frmAdjust.sliderCenter.TickFrequency = (frmAdjust.sliderCenter.Max - frmAdjust.sliderCenter.Min) / nlBitsPerPixel
    frmAdjust.sliderCenter.Value = nlCurrentWinCenter
    
    frmAdjust.txtRescaleSlope = nlCurrentSlope
    frmAdjust.txtRescaleIntercept = nlCurrentIntercept
    
    ' set up the std sliders
    frmAdjust.sliderBrightness.Value = nCurrentBrightness
    frmAdjust.sliderContrast.Value = nCurrentContrast
    
    If (ImagXpress.ViewGrayMode = GRAY_Standard) Then
        frmAdjust.optStdTransform.Value = True
    Else
        frmAdjust.optMedTransform.Value = True
    End If
    
    ' show the form
    frmAdjust.Show vbModal
    
    ' we just returned
    ImagXpress.ViewUpdate = False
    If (ImagXpress.ViewGrayMode = GRAY_Standard) Then
        If (Not frmAdjust.bCanceled) Then
            nCurrentBrightness = CInt(frmAdjust.lblBrightnessValue.Caption)
            nCurrentContrast = CInt(frmAdjust.lblContrastValue.Caption)
        End If
        
        ImagXpress.ViewBrightness = nCurrentBrightness
        ImagXpress.ViewContrast = nCurrentContrast
    ElseIf (ImagXpress.ViewGrayMode = GRAY_Medical) Then
        If (Not frmAdjust.bCanceled) Then
            nlCurrentSlope = CDbl(frmAdjust.txtRescaleSlope.Text)
            nlCurrentIntercept = CDbl(frmAdjust.txtRescaleIntercept.Text)
            
            nlCurrentWinWidth = CDbl(frmAdjust.lblWinWidth.Caption)
            nlCurrentWinCenter = CDbl(frmAdjust.lblWinCenter.Caption)
        End If
        
        ImagXpress.MEDRescaleSlope = nlCurrentSlope
        ImagXpress.MEDRescaleIntercept = nlCurrentIntercept
            
        ImagXpress.MEDWindowWidth = nlCurrentWinWidth
        ImagXpress.MEDWindowCenter = nlCurrentWinCenter
    End If
    ImagXpress.ViewUpdate = True
    
errorTrap:
    
    Call Unload(frmAdjust)
    
End Sub

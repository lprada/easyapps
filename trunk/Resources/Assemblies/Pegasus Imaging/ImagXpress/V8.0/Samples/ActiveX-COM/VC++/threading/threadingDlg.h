// threadingDlg.h : header file
//

#if !defined(AFX_THREADINGDLG_H__CC2C2F3D_34E1_4D03_B499_D23CED63A463__INCLUDED_)
#define AFX_THREADINGDLG_H__CC2C2F3D_34E1_4D03_B499_D23CED63A463__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\Include\ImagXpress8Events.h"
/////////////////////////////////////////////////////////////////////////////
// CThreadingDlg dialog

struct IX_TIMER
{
	long ProcessID;
	long Timer;
	IX_TIMER(long a = 0, long b = -1) : ProcessID(a), Timer(b) {};
};

class CThreadingDlg : public CDialog
{
// Construction
public:
	CThreadingDlg(CWnd* pParent = NULL);	// standard constructor
	
	CImagXpress *ppCIXBase;
	IImagXpressPtr pIXBase;

	CImagXpress *ppCIXView[6];
	IImagXpressPtr pIXView[6];

	CStatic *pLabel[6];

	CArray<IX_TIMER, IX_TIMER&> timer_array;
	
	static HRESULT ImageStatusChanged(DWORD classPtr, DWORD objID, short ID, enumIPEffect eOPID, long lStatus);
	static HRESULT TimerTick(DWORD classPtr, DWORD objID);

// Dialog Data
	//{{AFX_DATA(CThreadingDlg)
	enum { IDD = IDD_THREADING_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThreadingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CThreadingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeAsyncpriority();
	afx_msg void OnDestroy();
	afx_msg void OnChangeMaxthreads();
	afx_msg void OnAsync();
	afx_msg void OnLoadresize();
	afx_msg void OnFileQuit();
	afx_msg void OnAboutAboutbox();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THREADINGDLG_H__CC2C2F3D_34E1_4D03_B499_D23CED63A463__INCLUDED_)

VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Image 
   Caption         =   "DataBase"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   660
   ClientWidth     =   8490
   LinkTopic       =   "Form1"
   ScaleHeight     =   6945
   ScaleWidth      =   8490
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   2010
      Left            =   5280
      TabIndex        =   5
      Top             =   2160
      Width           =   2895
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "Image.frx":0000
      Left            =   480
      List            =   "Image.frx":0010
      TabIndex        =   3
      Top             =   360
      Width           =   7695
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3375
      Left            =   480
      TabIndex        =   2
      Top             =   1800
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   5953
      ErrStr          =   "3E9CA9144E27BA609D8F12924B60D000"
      ErrCode         =   535123737
      ErrInfo         =   -1752437861
      Persistence     =   -1  'True
      _cx             =   7223
      _cy             =   5953
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Save Image from File to BLOB"
      Height          =   495
      Left            =   1200
      TabIndex        =   1
      Top             =   6120
      Width           =   2535
   End
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1200
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   5520
      Width           =   2535
   End
   Begin VB.Label lblError 
      Height          =   1335
      Left            =   5280
      TabIndex        =   7
      Top             =   5040
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   5280
      TabIndex        =   6
      Top             =   4320
      Width           =   2415
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   5280
      TabIndex        =   4
      Top             =   1560
      Width           =   2655
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Image"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Dim cn As ADODB.Connection, rs As ADODB.Recordset, SQL As String

Private Sub Combo1_Click()
    
    rs.MoveFirst
    rs.Find "ID=" & Combo1.ItemData(Combo1.ListIndex)
    
  
    Dim BlobSize As Long
    Dim ReadData As Variant
    
   BlobSize = rs("Images").ActualSize
   If (BlobSize > 0) Then
        ReadData = rs("Images").GetChunk(BlobSize)
            
        'loads the image data into ImagXpress 7 via the LoadBLOB
        ImagXpress1.LoadBlob ReadData, BlobSize
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
  End If
 
End Sub

Private Sub Command2_Click()
    End
End Sub

Private Sub Command3_Click()
    rs.AddNew
    'call function to write image data from a file into BLOB field
    rs!Description = "Image From File"
    Combo1.AddItem rs!Description
    Combo1.ItemData(Combo1.NewIndex) = Combo1.NewIndex
    rs!ID = Combo1.NewIndex
    'place the path to image here
    FileToBlob App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg", rs!Images, 131072
    rs.Update
    
    'End If
 ' loads the combo box with names of images
     '   With rs
      '      Do Until .EOF
       '          Combo1.AddItem !Description
        '         Combo1.ItemData(Combo1.NewIndex) = !ID
         '        .MoveNext
         '   Loop
        'End With
       ' Combo1.Text = Combo1.List(0)
'End Sub
    
'******This code below shows how the image data could be loaded into memory from the IX control _
'******and the loaded directly into the database********
'Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
'Private Declare Function GlobalSize Lib "kernel32" (ByVal hMem As Long) As Long
'Private Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
'
'Private Sub Command3_Click()
'    Xpress1.SaveToBuffer = True
'    Xpress1.SaveFileType = FT_JPG
'    Xpress1.SaveFile
'    Dim lngHBufSize       As Long
'    Dim lngBufferAddress  As Long
'    Dim lReturn           As Long
'    Dim bBlob()           As Byte
'    Dim varBlob         As Variant
'    lngBufferAddress = GlobalLock(Xpress1.SaveBufferHandle)
'    lngHBufSize = GlobalSize(Xpress1.SaveBufferHandle)
'    ReDim bBlob(0 To lngHBufSize - 1) As Byte
'    CopyMemory bBlob(0), ByVal lngBufferAddress, lngHBufSize
'    varBlob = bBlob
'    rs.AddNew
'    'call function
'    rs!Description = "Image"
'    Binary varBlob, rs!Images, lngHBufSize
'    rs.Update
'End Sub

'
'Public Sub Binary(ByVal d As Variant, ByVal fld As ADODB.Field, filesize As Long)
'          'append data to field in database
'        fld.AppendChunk d
'End Sub
    
End Sub

Private Sub Form_Load()
 
     Dim mdbpath As String
     Dim ConnectionString As String
     
     ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
 
     mdbpath = App.Path & "\Peg.mdb"
       
     Set cn = New ADODB.Connection
     Set rs = New ADODB.Recordset
     cn.CursorLocation = adUseClient
     
     ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False; Data Source="
     cn.ConnectionString = ConnectionString & mdbpath
     cn.Open
     SQL = "SELECT * FROM Data"
     rs.Open SQL, cn, adOpenKeyset, adLockOptimistic
        
    ' getting info from database
    Dim BlobSize As Long
    Dim ReadData As Variant
    
   BlobSize = rs("Images").ActualSize
   If (BlobSize > 0) Then
        ReadData = rs("Images").GetChunk(BlobSize)
        'load the image data into the control
        ImagXpress1.LoadBlob ReadData, BlobSize
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
    
  End If
 ' loads the combo box with names of images
        With rs
            Do Until .EOF
                 Combo1.AddItem !Description
                 Combo1.ItemData(Combo1.NewIndex) = !ID
                 .MoveNext
            Loop
        End With
        Combo1.Text = Combo1.List(0)
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    End Sub
Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub
    


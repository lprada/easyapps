VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Print 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress ActiveX/COM sample- Print"
   ClientHeight    =   8205
   ClientLeft      =   1125
   ClientTop       =   1785
   ClientWidth     =   10035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8205
   ScaleWidth      =   10035
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1425
      Left            =   5880
      TabIndex        =   7
      Top             =   2520
      Width           =   3255
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "Print.frx":0000
      Left            =   240
      List            =   "Print.frx":000A
      TabIndex        =   5
      Top             =   360
      Width           =   9615
   End
   Begin MSComDlg.CommonDialog dlgFileOpen 
      Left            =   5400
      Top             =   7560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Print Image Fit to Page"
      Height          =   495
      Left            =   240
      TabIndex        =   3
      Top             =   7320
      Width           =   4815
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3975
      Left            =   240
      TabIndex        =   2
      Top             =   1680
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   7011
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1004826363
      ErrInfo         =   -1385622388
      Persistence     =   -1  'True
      _cx             =   8493
      _cy             =   7011
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdPrint2 
      Caption         =   "Print 2 images Centered on Page"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   6600
      Width           =   4815
   End
   Begin VB.CommandButton cmdPrint1 
      Caption         =   "Print Image Centered on Page"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   5880
      Width           =   4815
   End
   Begin VB.Label lblError 
      Height          =   1815
      Left            =   5880
      TabIndex        =   8
      Top             =   6000
      Width           =   3375
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   5880
      TabIndex        =   6
      Top             =   1680
      Width           =   1935
   End
   Begin VB.Label error 
      Caption         =   "Last Error"
      Height          =   375
      Left            =   5880
      TabIndex        =   4
      Top             =   5280
      Width           =   1335
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuspace 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Print"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim twipsperinch As Integer
Dim pagewidth As Long
Dim pageheight As Long
Dim res As String

Private Sub cmdPrint1_Click()



On Error GoTo ErrHandler1

' The printer is scaled in twips
twipsperinch = 1440

' Set the Printer Height/Width
' Since the image size is 320x240, we'll set the print size
' to 4 inches by 3 inches to keep our 4:3 aspect ratio.
ImagXpress1.PrinterWidth = 4 * twipsperinch     ' 4 inches
ImagXpress1.PrinterHeight = 3 * twipsperinch    ' 3 inches

' Center the Image on an 8-1/2 X 11 inch page
' ( Account for 1/4 inch borders)
pagewidth = 8 * twipsperinch
pageheight = 10.5 * twipsperinch
ImagXpress1.PrinterLeft = (pagewidth - ImagXpress1.PrinterWidth) / 2
ImagXpress1.PrinterTop = (pageheight - ImagXpress1.PrinterHeight) / 2

' Print the Image
Printer.Print ""  ' Initialize Printer
ImagXpress1.PrinterhDC = Printer.hDC
Printer.EndDoc
Exit Sub

' Handle errors
ErrHandler1:
  res = MsgBox("There was a printer error.  Please make sure that your printer is properly installed and connected.", 16, "Printer Error")

End Sub


Private Sub cmdPrint2_Click()

On Error GoTo ErrHandler2

' The printer is scaled in twips
twipsperinch = 1440

' Set the Printer Height/Width
' Since the image size is 320x240, we'll set the print size
' to 4 inches by 3 inches to keep our 4:3 aspect ratio.
ImagXpress1.PrinterWidth = 4 * twipsperinch     ' 4 inches
ImagXpress1.PrinterHeight = 3 * twipsperinch    ' 3 inches

' Center the Image horizontally on an 8-1/2 X 11 inch page
' ( Account for 1/4 inch borders)
pagewidth = 8 * twipsperinch
pageheight = 10.5 * twipsperinch
ImagXpress1.PrinterLeft = (pagewidth - ImagXpress1.PrinterWidth) / 2

' Position the image 2 inches from the top of the page
ImagXpress1.PrinterTop = 2 * twipsperinch

' Print the first Image
Printer.Print ""  ' Initialize Printer
ImagXpress1.PrinterhDC = Printer.hDC

' Position the image 6 inches from the top of the page
ImagXpress1.PrinterTop = 6 * twipsperinch

' Print the 2nd Image
ImagXpress1.PrinterhDC = Printer.hDC

' Page Eject
Printer.EndDoc
Exit Sub

' Handle errors
ErrHandler2:

  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
    
  res = MsgBox("There was a printer error.  Please make sure that your printer is properly installed and connected.", 16, "Printer Error")

End Sub



Private Sub Command1_Click()
Dim ImgHgt As Integer, ImgWid As Integer
Dim AreaHgt As Integer, AreaWid As Integer
Dim H As Integer, W As Integer
Dim WScl As Double, HScl As Double

    Printer.ScaleMode = vbTwips
    ImgHgt = ImagXpress1.IHeight
    ImgWid = ImagXpress1.IWidth
    AreaHgt = Printer.ScaleHeight
    AreaWid = Printer.ScaleWidth
    HScl = AreaHgt / ImgHgt
    WScl = AreaWid / ImgWid
    If WScl < HScl Then
       W = AreaWid
       H = Int(ImgHgt * WScl)
    Else
       H = AreaHgt
       W = Int(ImgWid * HScl)
    End If
    If H <= 0 Then H = 1
    If W <= 0 Then W = 1
    ImagXpress1.PrinterHeight = H
    ImagXpress1.PrinterTop = Int((Printer.ScaleHeight - H) / 2) - 1
    ImagXpress1.PrinterWidth = W
    ImagXpress1.PrinterLeft = Int((Printer.ScaleWidth - W) / 2) - 1
    Printer.Print ""  ' Initialize Printer
    ImagXpress1.PrinterhDC = Printer.hDC
    Printer.EndDoc
End Sub

Private Sub Form_Load()
  Dim txt As String
  
  
  ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
  
  ImagXpress1.ZOrder
  ImagXpress1.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
  
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuOpen_Click()
    lblError = ""
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub

Private Sub mnuQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
  If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub

VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Viewing 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ImagXpress 8 Viewing Options"
   ClientHeight    =   10380
   ClientLeft      =   150
   ClientTop       =   450
   ClientWidth     =   11415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10380
   ScaleWidth      =   11415
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstStatus 
      Height          =   1230
      Left            =   3960
      TabIndex        =   38
      Top             =   1680
      Width           =   3495
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "Viewing.frx":0000
      Left            =   120
      List            =   "Viewing.frx":0013
      TabIndex        =   36
      Top             =   120
      Width           =   11055
   End
   Begin VB.Frame fraTools 
      Caption         =   "Viewing Tools"
      Height          =   975
      Left            =   8160
      TabIndex        =   32
      Top             =   3240
      Width           =   3135
      Begin VB.ComboBox cboToolSelect 
         Height          =   315
         Left            =   240
         TabIndex        =   33
         Text            =   "cboToolSelect"
         Top             =   360
         Width           =   2655
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Border Options"
      Height          =   2295
      Left            =   8160
      TabIndex        =   24
      Top             =   7920
      Width           =   3135
      Begin VB.OptionButton Option10 
         Caption         =   "Inset resizer"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   960
         Width           =   2055
      End
      Begin VB.OptionButton Option11 
         Caption         =   "Inset ThickResizer"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   1200
         Width           =   1935
      End
      Begin VB.OptionButton Option9 
         Caption         =   "Inset Border"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   720
         Width           =   1935
      End
      Begin VB.OptionButton Option8 
         Caption         =   "Flat Border"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   480
         Width           =   1815
      End
      Begin VB.OptionButton Option12 
         Caption         =   "No Border"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   1440
         Width           =   1695
      End
      Begin VB.OptionButton Option13 
         Caption         =   "Raised Border"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   1680
         Width           =   1815
      End
      Begin VB.OptionButton Option14 
         Caption         =   "Raised Resizer"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1920
         Width           =   1695
      End
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3855
      Left            =   120
      TabIndex        =   23
      Top             =   3000
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   6800
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   665134914
      ErrInfo         =   1682361142
      Persistence     =   -1  'True
      _cx             =   13573
      _cy             =   6800
      AutoSize        =   7
      BackColor       =   8421504
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      Notify          =   -1  'True
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Frame Frame5 
      Caption         =   "Horizontal Alignment Options"
      Height          =   1575
      Left            =   8160
      TabIndex        =   19
      Top             =   6240
      Width           =   3135
      Begin VB.OptionButton Option7 
         Caption         =   "Horizintal Align Right"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   1200
         Width           =   2175
      End
      Begin VB.OptionButton Option5 
         Caption         =   "Horizintal Align Left"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   480
         Width           =   2175
      End
      Begin VB.OptionButton Option4 
         Caption         =   "Horizintal Align Center"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   840
         Width           =   2295
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "BrightNess and Contrast Adjustment"
      Height          =   2775
      Left            =   120
      TabIndex        =   12
      Top             =   7080
      Width           =   4215
      Begin VB.CommandButton Command2 
         Caption         =   "Reset Original"
         Height          =   375
         Left            =   1080
         TabIndex        =   17
         Top             =   2280
         Width           =   1935
      End
      Begin MSComctlLib.Slider Slider2 
         Height          =   495
         Left            =   240
         TabIndex        =   14
         Top             =   1680
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   873
         _Version        =   393216
         Min             =   -100
         Max             =   100
         TickStyle       =   3
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   495
         Left            =   240
         TabIndex        =   13
         Top             =   720
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   873
         _Version        =   393216
         Min             =   -100
         Max             =   100
         TickStyle       =   3
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Contrast Adjustment"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   600
         TabIndex        =   16
         Top             =   1320
         Width           =   3015
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Brightness Adjustment"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   480
         TabIndex        =   15
         Top             =   360
         Width           =   3255
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Display Options"
      Height          =   2775
      Left            =   4560
      TabIndex        =   8
      Top             =   7080
      Width           =   3255
      Begin VB.CheckBox Check3 
         Caption         =   "Dithering"
         Height          =   495
         Left            =   240
         TabIndex        =   11
         Top             =   1920
         Width           =   2655
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Smoothing(Color Image)"
         Height          =   375
         Left            =   240
         TabIndex        =   10
         Top             =   1440
         Width           =   2655
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Anti-Alias(Black and White Image)"
         Height          =   615
         Left            =   240
         TabIndex        =   9
         Top             =   720
         Width           =   2655
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Image Zooming"
      Height          =   1695
      Left            =   8160
      TabIndex        =   4
      Top             =   1320
      Width           =   3135
      Begin VB.CommandButton Command3 
         Caption         =   "Zoom Image Out"
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   1200
         Width           =   2535
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Zoom Image In"
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   720
         Width           =   2535
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Enable Zooming"
         Height          =   375
         Left            =   360
         TabIndex        =   5
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Vertical Alignment Options"
      Height          =   1695
      Left            =   8160
      TabIndex        =   0
      Top             =   4320
      Width           =   3135
      Begin VB.OptionButton Option6 
         Caption         =   "Horizonatal Align Right"
         Height          =   375
         Left            =   360
         TabIndex        =   18
         Top             =   3120
         Width           =   2295
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Vertical Align Top"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   1815
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Vertical Align Bottom"
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   1080
         Width           =   2055
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Vertical Align Center"
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   720
         Width           =   1815
      End
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   255
      Left            =   3960
      TabIndex        =   37
      Top             =   1320
      Width           =   1695
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   35
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label lblError 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   240
      TabIndex        =   34
      Top             =   1680
      Width           =   3615
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpen 
         Caption         =   "Open Image"
      End
      Begin VB.Menu mnuspacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Viewing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

Dim cw As Long
Dim ch As Long
Dim iw As Long
Dim ih As Long

Dim data As Long

Private Sub cboToolSelect_Click()
    ImagXpress1.ToolSet cboToolSelect.ListIndex, IXMOUSEBUTTON_Left, 0
End Sub

Private Sub Check1_Click()
    If Check1.Value = 1 Then
        ImagXpress1.ViewAntialias = True
    Else
        ImagXpress1.ViewAntialias = False
    End If
End Sub

Private Sub Check2_Click()
    If Check2.Value = 1 Then
        ImagXpress1.ViewSmoothing = True
    Else
        ImagXpress1.ViewSmoothing = False
    End If
End Sub

Private Sub Check3_Click()
    If Check3.Value = 1 Then
        ImagXpress1.ViewDithered = True
    Else
        ImagXpress1.ViewDithered = False
    End If
End Sub

Private Sub Check4_Click()
    If Check4.Value = 1 Then
           Frame1.Enabled = True
          Frame5.Enabled = True
        ImagXpress1.AutoSize = ISIZE_CropImage
        Command1.Enabled = True
        Command3.Enabled = True
    Else
        ImagXpress1.AutoSize = ISIZE_BestFit
        Command1.Enabled = False
        Command3.Enabled = False
    End If
End Sub



Private Sub Command1_Click()
 ImagXpress1.ZoomFactor = ImagXpress1.ZoomFactor * (4 / 3)
End Sub


Private Sub Command2_Click()
     ImagXpress1.ViewBrightness = 0
     ImagXpress1.ViewContrast = 0
     Slider1.Value = 0
     Slider2.Value = 0
End Sub

Private Sub Command3_Click()
ImagXpress1.ZoomFactor = ImagXpress1.ZoomFactor * (3 / 4)
End Sub

Private Sub Form_Load()

   ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
 
    Command1.Enabled = False
    Command3.Enabled = False
    ImagXpress1.AutoSize = ISIZE_BestFit
    
    
    imgParentDir = App.Path
    imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\benefits.tif"
    
    LoadFile
        
    cboToolSelect.AddItem "None", 0
    cboToolSelect.AddItem "Zoom In", 1
    cboToolSelect.AddItem "Zoom Out", 2
    cboToolSelect.AddItem "Zoom Rect", 3
    cboToolSelect.AddItem "Hand Tool", 4
    cboToolSelect.AddItem "Select", 5
    cboToolSelect.ListIndex = 0
    
    cw = ImagXpress1.CtlWidth
    ch = ImagXpress1.CtlHeight
    iw = ImagXpress1.ImagWidth
    ih = ImagXpress1.ImagHeight
    data = iw / cw
    If data > (ih / ch) Then
        Frame1.Enabled = True
        Frame5.Enabled = False
    Else
        Frame1.Enabled = False
        Frame5.Enabled = True
    End If
    If ImagXpress1.IBPP = 1 Then
        Check1.Value = 1
    ElseIf ImagXpress1.IBPP <> 1 Then
        Check1.Value = 0
    End If
    Slider1.Value = 0
    Slider2.Value = 0
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
    
End Sub

Private Sub mnuExit_Click()
   End
End Sub

Private Sub mnuOpen_Click()

    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If

    cw = ImagXpress1.CtlWidth
    ch = ImagXpress1.CtlHeight
    iw = ImagXpress1.ImagWidth
    ih = ImagXpress1.ImagHeight
    
    If (iw / cw) > (ih / ch) Then
       Frame1.Enabled = True
       Frame5.Enabled = False
    Else
      Frame1.Enabled = False
      Frame5.Enabled = True
    End If
    If ImagXpress1.IBPP = 1 Then
        Check1.Value = 1
        Check2.Value = 0
    Else
        Check2.Value = 1
        Check1.Value = 0
    End If
End Sub
Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub

Private Sub Option1_Click()
    If Option1.Value = True Then
        ImagXpress1.AlignV = ALIGNV_Center
    End If
End Sub

Private Sub Option10_Click()
       If Option10.Value = True Then
        ImagXpress1.BorderType = BORD_InsetResizer
    End If
End Sub

Private Sub Option11_Click()
       If Option11.Value = True Then
        ImagXpress1.BorderType = BORD_InsetThickResizer
    End If
End Sub

Private Sub Option12_Click()
       If Option12.Value = True Then
        ImagXpress1.BorderType = BORD_None
    End If
End Sub

Private Sub Option13_Click()
       If Option13.Value = True Then
        ImagXpress1.BorderType = BORD_Raised
    End If
End Sub

Private Sub Option14_Click()
       If Option14.Value = True Then
        ImagXpress1.BorderType = BORD_RaisedResizer
    End If
End Sub

Private Sub Option2_Click()
    If Option2.Value = True Then
        ImagXpress1.AlignV = ALIGNV_Bottom
    End If
End Sub

Private Sub Option3_Click()
    If Option3.Value = True Then
        ImagXpress1.AlignV = ALIGNV_Top
    End If
End Sub

Private Sub Option4_Click()
    If Option4.Value = True Then
         ImagXpress1.AlignH = ALIGNH_Center
    End If
End Sub

Private Sub Option5_Click()
    If Option5.Value = True Then
        ImagXpress1.AlignH = ALIGNH_Left
    End If
End Sub

Private Sub Option7_Click()
        If Option7.Value = True Then
         ImagXpress1.AlignH = ALIGNH_Right
    End If
End Sub



Private Sub Option8_Click()
    If Option8.Value = True Then
        ImagXpress1.BorderType = BORD_Flat
    End If
End Sub

Private Sub Option9_Click()
       If Option9.Value = True Then
        ImagXpress1.BorderType = BORD_Inset
    End If
End Sub

Private Sub Slider1_Change()
         'ImagXpress1.hDib = temp
        'temp = ImagXpress1.CopyDib
        ImagXpress1.ViewBrightness = Slider1.Value
         'ImagXpress1.ViewContrast = Slider2.Value
End Sub

Private Sub Slider2_Click()
     ImagXpress1.ViewContrast = Slider2.Value
End Sub

Private Sub LoadFile()
    ImagXpress1.FileName = imgFileName
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError

   
   
End Sub

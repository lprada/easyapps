VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmClean 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Remove Dust and Scratches"
   ClientHeight    =   9855
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   14685
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9855
   ScaleWidth      =   14685
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      ItemData        =   "ScratchandDustRemove.frx":0000
      Left            =   7440
      List            =   "ScratchandDustRemove.frx":001F
      TabIndex        =   25
      Top             =   120
      Width           =   7215
   End
   Begin VB.ListBox lstInfo1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      ItemData        =   "ScratchandDustRemove.frx":0208
      Left            =   120
      List            =   "ScratchandDustRemove.frx":022D
      TabIndex        =   24
      Top             =   120
      Width           =   7215
   End
   Begin VB.ListBox lstStatus 
      Height          =   1620
      Left            =   7560
      TabIndex        =   21
      Top             =   7800
      Width           =   3375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Reload Original Image"
      Height          =   375
      Left            =   2040
      TabIndex        =   19
      Top             =   6360
      Width           =   2280
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   3615
      Left            =   7800
      TabIndex        =   18
      Top             =   2520
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   6376
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1024460209
      ErrInfo         =   1450424658
      Persistence     =   -1  'True
      _cx             =   11668
      _cy             =   6376
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Frame ScratchFrm 
      Caption         =   "Scratch Dust Settings"
      Height          =   2775
      Left            =   1080
      TabIndex        =   2
      Top             =   6960
      Width           =   4695
      Begin VB.CommandButton ScratchCmd 
         Caption         =   "Scratch Removal"
         Height          =   510
         Left            =   2400
         TabIndex        =   17
         Top             =   360
         Width           =   1440
      End
      Begin VB.CommandButton DustCmd 
         Caption         =   "Dust Removal"
         Height          =   510
         Left            =   2400
         TabIndex        =   16
         Top             =   1080
         Width           =   1440
      End
      Begin VB.TextBox ThresholdText 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   375
         Left            =   1185
         TabIndex        =   10
         Text            =   "0"
         Top             =   360
         Width           =   555
      End
      Begin VB.TextBox FilterAdjustText 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   375
         Left            =   1185
         TabIndex        =   9
         Text            =   "0"
         Top             =   990
         Width           =   555
      End
      Begin VB.TextBox IterAdjustText 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   375
         Left            =   1185
         TabIndex        =   7
         Text            =   "0"
         Top             =   1545
         Width           =   600
      End
      Begin VB.CheckBox DarkCheck 
         Caption         =   "Dark Scratches"
         Height          =   375
         Left            =   2400
         TabIndex        =   5
         Top             =   1680
         Width           =   1515
      End
      Begin VB.TextBox PenWidthText 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   375
         Left            =   1185
         TabIndex        =   4
         Text            =   "4"
         Top             =   2115
         Width           =   555
      End
      Begin MSComCtl2.UpDown PenWidthUpDown 
         Height          =   375
         Left            =   1740
         TabIndex        =   3
         Top             =   2115
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   661
         _Version        =   393216
         Value           =   2
         AutoBuddy       =   -1  'True
         BuddyControl    =   "PenWidthText"
         BuddyDispid     =   196620
         OrigLeft        =   10830
         OrigTop         =   4290
         OrigRight       =   11115
         OrigBottom      =   4755
         Max             =   100
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown IterAdjustUpDown 
         Height          =   375
         Left            =   1740
         TabIndex        =   6
         Top             =   1545
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   661
         _Version        =   393216
         BuddyControl    =   "IterAdjustText"
         BuddyDispid     =   196618
         OrigLeft        =   10905
         OrigTop         =   3450
         OrigRight       =   11190
         OrigBottom      =   3975
         Max             =   100
         Min             =   -5
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown FilterAdjustUpDown 
         Height          =   375
         Left            =   1740
         TabIndex        =   8
         Top             =   990
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   661
         _Version        =   393216
         BuddyControl    =   "FilterAdjustText"
         BuddyDispid     =   196617
         OrigLeft        =   10860
         OrigTop         =   2835
         OrigRight       =   11100
         OrigBottom      =   3210
         Max             =   2
         Min             =   -6
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown ThresholdUpDown 
         Height          =   375
         Left            =   1740
         TabIndex        =   11
         Top             =   360
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   661
         _Version        =   393216
         BuddyControl    =   "ThresholdText"
         BuddyDispid     =   196616
         OrigLeft        =   10950
         OrigTop         =   2190
         OrigRight       =   11190
         OrigBottom      =   2625
         Max             =   255
         Min             =   -255
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.Label ThresholdLbl 
         Caption         =   "Threshold:"
         Height          =   315
         Left            =   240
         TabIndex        =   15
         Top             =   420
         Width           =   750
      End
      Begin VB.Label FiltAdjustLbl 
         Caption         =   "FilterAdjust:"
         Height          =   315
         Left            =   240
         TabIndex        =   14
         Top             =   1035
         Width           =   990
      End
      Begin VB.Label IterAdjustLbl 
         Caption         =   "Smoothing:"
         Height          =   315
         Left            =   240
         TabIndex        =   13
         Top             =   1575
         Width           =   990
      End
      Begin VB.Label PenLbl 
         Caption         =   "Pen Width:"
         Height          =   315
         Left            =   255
         TabIndex        =   12
         Top             =   2145
         Width           =   990
      End
   End
   Begin VB.CommandButton ReloadCmd 
      Caption         =   "Reload Original Image"
      Height          =   375
      Left            =   9720
      TabIndex        =   1
      Top             =   6360
      Width           =   2280
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3615
      Left            =   360
      TabIndex        =   0
      Top             =   2520
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   6376
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1024460209
      ErrInfo         =   1450424658
      Persistence     =   -1  'True
      _cx             =   11668
      _cy             =   6376
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      DrawFillColor   =   12632256
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   0
      AlignV          =   0
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1575
      Left            =   11160
      TabIndex        =   23
      Top             =   7800
      Width           =   3015
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   255
      Left            =   11160
      TabIndex        =   22
      Top             =   7080
      Width           =   1935
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   7560
      TabIndex        =   20
      Top             =   7080
      Width           =   1695
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpenScratched 
         Caption         =   "&Open Scratched Image"
      End
      Begin VB.Menu mnuOpenDust 
         Caption         =   "&Open Dust Image"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmClean"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim fSX, fSY, fEX, fEY As Single
Dim SX, SY, EX, EY As Long
Dim penWidth As Long
Dim imgFileName As String
Dim imgParentDir As String
Dim Threshold As Long
Dim FilterAjust As Long
Dim SmoothAdjust As Long
Dim XOFF As Long
Dim YOFF As Long
Dim bDirtyVector As Boolean
Dim bDirtyBox As Boolean
Dim bDrawVector
Dim filteradjust As String
Dim getFileName As String
Dim getFileName2 As String
Dim txt As String
Dim txt1 As String
Dim band As Boolean

Dim bToolbarActive As Boolean


Private Sub Command1_Click()
    XOFF = ImagXpress1.ScrollX
    YOFF = ImagXpress1.ScrollY
    ImagXpress1.FileName = getFileName
    ImagXpress1.ScrollX = XOFF
    ImagXpress1.ScrollY = YOFF
    bDirtyVector = False
    bDirtyBox = False
End Sub

Private Sub DustCmd_Click()
    Dim dx, dy, tmp As Long
    Dim x1, y1, x2, y2, x, y As Long
    Dim SpeckType
    Dim specktypewhite
    Dim specktypeDark
    x1 = SX
    y1 = SY
    x2 = EX
    y2 = EY
    dx = x2 - x1
    If dx < 0 Then
        tmp = x2
        x2 = x1
        x1 = tmp
        dx = -dx
    End If
    
    dy = y2 - y1
    If dy < 0 Then
        tmp = y2
        y2 = y1
        y1 = tmp
        dy = -dy
    End If
        
    XOFF = ImagXpress1.ScrollX
    YOFF = ImagXpress1.ScrollY
    ImagXpress1.ScrollX = XOFF
    ImagXpress1.ScrollY = YOFF
     If bDirtyBox Then
        DrawLine
        bDirtyBox = False
    End If
    If bDirtyVector Then
        DrawLine
        bDirtyVector = False
    End If
    Threshold = ThresholdText.Text
    filteradjust = FilterAdjustText.Text
    SmoothAdjust = IterAdjustText.Text
    ImagXpress2.Area True, ImagXpress2.RubberBandL, ImagXpress2.RubberBandT, ImagXpress2.RubberBandW, ImagXpress2.RubberBandH
    If DarkCheck.Value = 0 Then
        SpeckType = specktypewhite
    Else
        SpeckType = specktypeDark
    End If
    ImagXpress2.RemoveDust Threshold, filteradjust, SmoothAdjust, DefectTypeDark
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
    
    ImagXpress1.Area False, x1, y1, dx, dy
End Sub



Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub ScratchCmd_Click()
    Dim x, y, dx, dy, tmp As Long
    Dim x1, y1, x2, y2 As Long
    Dim SpeckType
    x1 = SX
    y1 = SY
    x2 = EX
    y2 = EY
    dx = x2 - x1
    If dx < 0 Then
        tmp = x2
        x2 = x1
        x1 = tmp
        dx = -dx
    End If
    
    dy = y2 - y1
    If dy < 0 Then
        tmp = y2
        y2 = y1
        y1 = tmp
        dy = -dy
    End If
    
   
    XOFF = ImagXpress1.ScrollX
    YOFF = ImagXpress1.ScrollY
    ImagXpress1.ScrollX = XOFF
    ImagXpress1.ScrollY = YOFF
    
    If bDirtyBox Then
       DrawLine
        bDirtyBox = False
    End If
    If bDirtyVector Then
        DrawLine
        bDirtyVector = False
    End If

    If x1 > penWidth Then
        x = x1 - penWidth
        dx = dx + 2 * penWidth
        If x + dx > ImagXpress1.IWidth Then
            dx = ImagXpress1.IWidth - x
        End If
    Else
        x = x1
        dx = dx + 2 * penWidth
        If x + dx > ImagXpress1.IWidth Then
            dx = ImagXpress1.IWidth - x
        End If
   End If
    
    If y1 > penWidth Then
        y = y1 - penWidth
        dy = dy + 2 * penWidth
        If y + dy > ImagXpress1.IHeight Then
            dy = ImagXpress1.IHeight - y
        End If
    Else
        y = y1
        dy = dy + 2 * penWidth
        If y + dy > ImagXpress1.IHeight Then
            dy = ImagXpress1.IHeight - y
        End If
   End If
    Threshold = ThresholdText.Text
    filteradjust = FilterAdjustText.Text
    SmoothAdjust = IterAdjustText.Text
    ImagXpress1.Area True, x, y, dx, dy
    ImagXpress1.RemoveScratches SX, SY, EX, EY, penWidth, Threshold, filteradjust, SmoothAdjust, SpeckType
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
   
    ImagXpress1.Area False, x, y, dx, dy
End Sub


Private Sub Form_Load()
     
   
    
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    ImagXpress2.EventSetEnabled EVENT_PROGRESS, True
    
    getFileName = App.Path & "\..\..\..\..\..\..\Common\Images\scratched.jpg"
    getFileName2 = App.Path & "\..\..\..\..\..\..\Common\Images\dust.jpg"
    
    imgParentDir = App.Path
    
    ImagXpress1.FileName = getFileName
    ImagXpress1.ScrollX = 250
    ImagXpress1.ScrollY = 289
    ImagXpress2.FileName = getFileName2
    ImagXpress2.ScrollBars = SB_Both
    fSX = 0
    fSY = 0
    fEX = 0
    fEY = 0
    penWidth = 4
    ImagXpress1.DrawStyle = STYLE_Solid
    ImagXpress1.DrawWidth = penWidth
    ImagXpress1.DrawMode = PEN_Xor
    Threshold = 0
    FilterAjust = 0
    SmoothAdjust = 0
    bDirtyVector = False
    bDirtyBox = False
    bDrawVector = 1
    
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub ImagXpress2_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub ImagXpress1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    
    
    If bDirtyVector Then
        DrawLine
    End If
    If bDirtyBox Then
       DrawLine
    End If
    x = x / Screen.TwipsPerPixelX
    y = y / Screen.TwipsPerPixelY
    If x > ((ImagXpress1.IWidth + ImagXpress1.ScrollX) * ImagXpress1.ZoomFactor) Then
        Exit Sub
    End If

    If y > ((ImagXpress1.IHeight + ImagXpress1.ScrollY) * ImagXpress1.ZoomFactor) Then
        Exit Sub
    End If
    
    fSX = (x + ImagXpress1.ScrollX) / ImagXpress1.ZoomFactor
    fSY = (y + ImagXpress1.ScrollY) / ImagXpress1.ZoomFactor
    fEX = fSX
    fEY = fSY
    SX = fSX
    SY = fSY
    EX = fEX
    EY = fEY
    SX = SX
    SY = SY
   
    
    
End Sub

Private Sub ImagXpress1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 1 Then

       If bDirtyVector Then
           DrawLine
           bDirtyVector = False
       End If
        x = x / Screen.TwipsPerPixelX
        y = y / Screen.TwipsPerPixelY
        If x > ((ImagXpress1.IWidth + ImagXpress1.ScrollX) * ImagXpress1.ZoomFactor) Then
            Exit Sub
        End If
    
        If y > ((ImagXpress1.IHeight + ImagXpress1.ScrollY) * ImagXpress1.ZoomFactor) Then
            Exit Sub
        End If
        fEX = (x + ImagXpress1.ScrollX) / ImagXpress1.ZoomFactor
        fEY = (y + ImagXpress1.ScrollY) / ImagXpress1.ZoomFactor
        EX = fEX
        EY = fEY
        If bDrawVector Then
            DrawLine
        End If
    End If
    
End Sub

Private Sub ImagXpress1_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim dx, dy, tmp As Long
    Dim x1, x2, y1, y2
    x = x / Screen.TwipsPerPixelX
    y = y / Screen.TwipsPerPixelY
    If x > ((ImagXpress1.IWidth + ImagXpress1.ScrollX) * ImagXpress1.ZoomFactor) Then
        Exit Sub
    End If

    If y > ((ImagXpress1.IHeight + ImagXpress1.ScrollY) * ImagXpress1.ZoomFactor) Then
        Exit Sub
    End If
    
    fEX = (x + ImagXpress1.ScrollX) / ImagXpress1.ZoomFactor
    fEY = (y + ImagXpress1.ScrollY) / ImagXpress1.ZoomFactor
    EX = fEX
    EY = fEY
    If bDrawVector Then

        If bDirtyVector = False Then
            DrawLine
       End If
    End If
End Sub


Private Sub ImagXpress2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    x = x / Screen.TwipsPerPixelX
    y = y / Screen.TwipsPerPixelY
    ImagXpress2.RubberBand True, x, y, False
    band = True
End Sub

Private Sub ImagXpress2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If band Then
       x = x / Screen.TwipsPerPixelX
       y = y / Screen.TwipsPerPixelY
       ImagXpress2.RubberbandUpdate x, y
    End If
End Sub

Private Sub ImagXpress2_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    band = False
    x = x / Screen.TwipsPerPixelX
    y = y / Screen.TwipsPerPixelY
End Sub



Private Sub mnuOpenDust_Click()
 
Dim tmpFN As String

tmpFN = PegasusOpenFileP(imgParentDir)
If Len(tmpFN) <> 0 Then
    imgFileName = tmpFN
    ImagXpress2.FileName = imgFileName
        
   Err = ImagXpress2.ImagError
   PegasusError Err, lblError
    
End If
End Sub



Private Sub mnuOpenScratched_Click()

Dim tmpFN As String

tmpFN = PegasusOpenFileP(imgParentDir)
If Len(tmpFN) <> 0 Then
    imgFileName = tmpFN
    ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
    
End If


End Sub

Private Sub Option1_Click()
    bDrawVector = Option1.Value
End Sub

Private Sub Option2_Click()
    bDrawVector = Option1.Value
End Sub

Private Sub mnuQuit_Click()
    End
End Sub

Private Sub PenWidthUpDown_Change()
    If bDirtyVector Then
        DrawLine
        bDirtyVector = False
    End If
    penWidth = PenWidthUpDown.Value
End Sub



Private Sub DrawLine()
    ImagXpress1.DrawWidth = penWidth
    ImagXpress1.DrawMode = PEN_Xor
    ImagXpress1.DrawLine SX, SY, EX, EY, &HFFFFFF, False, True
    If (bDirtyVector = False) Then
        bDirtyVector = True
    Else
        bDirtyVector = False
    End If

End Sub

Private Sub ReloadCmd_Click()
    XOFF = ImagXpress2.ScrollX
    YOFF = ImagXpress2.ScrollY
    ImagXpress2.FileName = getFileName2
    ImagXpress2.ScrollX = XOFF
    ImagXpress2.ScrollY = YOFF
    bDirtyVector = False
    bDirtyBox = False
End Sub


Private Sub mnuToolbarShow_Click()
    
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    
     If ImagXpress2.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress2.ToolbarActivated = Not ImagXpress2.ToolbarActivated
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
    
    
    End Sub

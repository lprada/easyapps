VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Async 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress 8 Threading Sample"
   ClientHeight    =   9705
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   10725
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9705
   ScaleWidth      =   10725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1620
      ItemData        =   "Async.frx":0000
      Left            =   240
      List            =   "Async.frx":0019
      TabIndex        =   20
      Top             =   240
      Width           =   9975
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXViewer 
      Height          =   2295
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   2640
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   4048
      _cy             =   4048
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXBase 
      Height          =   375
      Left            =   9720
      TabIndex        =   7
      Top             =   5400
      Visible         =   0   'False
      Width           =   615
      _ExtentX        =   1085
      _ExtentY        =   661
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   1085
      _cy             =   661
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton LoadResize 
      Caption         =   "Load and Resize two images"
      Height          =   615
      Left            =   7680
      TabIndex        =   6
      Top             =   3480
      Width           =   2535
   End
   Begin VB.CheckBox Async 
      Caption         =   "Async Enabled"
      Height          =   495
      Left            =   7680
      TabIndex        =   5
      Top             =   5400
      Width           =   1695
   End
   Begin VB.TextBox StatusBox 
      Height          =   1815
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Top             =   7800
      Width           =   10335
   End
   Begin VB.TextBox MaxThreadsTxt 
      Height          =   285
      Left            =   7680
      TabIndex        =   3
      Text            =   "2"
      Top             =   7200
      Width           =   855
   End
   Begin VB.ComboBox AsyncPriorityCmbx 
      Height          =   315
      ItemData        =   "Async.frx":01B7
      Left            =   7680
      List            =   "Async.frx":01C5
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   6360
      Width           =   2655
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXViewer 
      Height          =   2295
      Index           =   1
      Left            =   2640
      TabIndex        =   9
      Top             =   2640
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   4048
      _cy             =   4048
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   3
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXViewer 
      Height          =   2295
      Index           =   2
      Left            =   5160
      TabIndex        =   10
      Top             =   2640
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   4048
      _cy             =   4048
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   4
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXViewer 
      Height          =   2295
      Index           =   3
      Left            =   120
      TabIndex        =   11
      Top             =   5400
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   4048
      _cy             =   4048
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   5
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXViewer 
      Height          =   2295
      Index           =   4
      Left            =   2640
      TabIndex        =   12
      Top             =   5400
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   4048
      _cy             =   4048
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   6
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXViewer 
      Height          =   2295
      Index           =   5
      Left            =   5160
      TabIndex        =   13
      Top             =   5400
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   4048
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   2065644578
      ErrInfo         =   796430214
      Persistence     =   -1  'True
      _cx             =   4048
      _cy             =   4048
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   7
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label ViewerLabel 
      AutoSize        =   -1  'True
      Caption         =   "Viewer Label 5"
      Height          =   195
      Index           =   5
      Left            =   5160
      TabIndex        =   19
      Top             =   5040
      Width           =   1050
   End
   Begin VB.Label ViewerLabel 
      AutoSize        =   -1  'True
      Caption         =   "Viewer Label 4"
      Height          =   195
      Index           =   4
      Left            =   2640
      TabIndex        =   18
      Top             =   5040
      Width           =   1050
   End
   Begin VB.Label ViewerLabel 
      AutoSize        =   -1  'True
      Caption         =   "Viewer Label 3"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   17
      Top             =   5040
      Width           =   1050
   End
   Begin VB.Label ViewerLabel 
      AutoSize        =   -1  'True
      Caption         =   "Viewer Label 2"
      Height          =   195
      Index           =   2
      Left            =   5160
      TabIndex        =   16
      Top             =   2280
      Width           =   1050
   End
   Begin VB.Label ViewerLabel 
      AutoSize        =   -1  'True
      Caption         =   "Viewer Label 1"
      Height          =   195
      Index           =   1
      Left            =   2640
      TabIndex        =   15
      Top             =   2400
      Width           =   1050
   End
   Begin VB.Label ViewerLabel 
      AutoSize        =   -1  'True
      Caption         =   "Viewer Label 0"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   2400
      Width           =   1050
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Max Number of concurrent threads:"
      Height          =   195
      Left            =   7680
      TabIndex        =   2
      Top             =   6840
      Width           =   2505
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Asynchronous Thread(s) priority:"
      Height          =   195
      Left            =   7680
      TabIndex        =   1
      Top             =   6000
      Width           =   2265
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuExit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Async"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Const TIMER_INTERVAL = 100
Const IMAGE1_WIDTH = 640
Const IMAGE1_HEIGHT = 480
Const IMAGE2_WIDTH = 320
Const IMAGE2_HEIGHT = 240

Private Type IX_TIMER
    ProcessID As Long
    Timer As Long
End Type

Dim timers() As IX_TIMER
Dim viewIndex As Integer

Private Sub Async_Click()
If IXBase.Async Then
    IXBase.Async = False
Else
    IXBase.Async = True
End If
End Sub

Private Sub Form_Activate()

Dim i As Integer

IXBase.AutoAssignID = False
IXBase.AsyncPriority = ASYNC_Normal
IXBase.AsyncCancelOnClose = True
AsyncPriorityCmbx.ListIndex = 0
Async.Value = 1
IXBase.AsyncMaxThreads = 2

ReDim timers(0)
timers(0).Timer = -1
viewIndex = 0
For i = 0 To 5
    ViewerLabel(i).Caption = ""
Next

IXBase.Timer = TIMER_INTERVAL

End Sub

Private Sub Form_Unload(Cancel As Integer)
  '****this is necessary to close the threads***
    IXBase.Async = False
    While Not IXBase.Idle
    DoEvents
    Wend
End Sub

Private Sub IXBase_ImageStatusChanged(ByVal ID As Integer, ByVal eOPID As PegasusImagingActiveXImagXpress8Ctl.enumIPEffect, ByVal lStatus As Long)
Dim S As String
Dim i As Integer

 If BUFSTATUS_EMPTY = lStatus Then
    S = "Empty"
 End If
   
 If BUFSTATUS_DEFINED And lStatus Then
    S = "Defined"
 End If
 
 If BUFSTATUS_OPENED And lStatus Then
    S = S + ", Opened"
 End If
 
 If BUFSTATUS_DECODED And lStatus Then
    S = S + ", Decoded"
 End If
 
 If BUFSTATUS_DECODING And lStatus Then
    S = S + ", Decoding"
 End If
 
 If BUFSTATUS_IMAGEPROCESSING And lStatus Then
    S = S + ", Image-Processing"
 End If
 
 If BUFSTATUS_IMAGEPROCESSED And lStatus Then
    For i = 0 To UBound(timers)
        If timers(i).ProcessID = ID Then
            S = S + ", Image-Processed ( took " & timers(i).Timer / 1000 & " seconds )"
            timers(i).Timer = -1
        End If
    Next
    
 End If
 
 If BUFSTATUS_SAVING And lStatus Then
    S = S + ", Saving"
 End If
 
 If BUFSTATUS_SAVED And lStatus Then
    S = S + ", Saved"
 End If
 
 If BUFSTATUS_WAIT And lStatus Then
    S = S + ", Wait"
 End If
 
 If BUFSTATUS_CANCELED And lStatus Then S = S + ", Canceled"
 
 If BUFSTATUS_ERROR And lStatus Then
    S = S + ", Error"
 End If

StatusBox.Text = StatusBox.Text & "Image " & ID & " " & S & vbNewLine
End Sub

Private Sub IXBase_TimerTick()
Dim i As Integer

For i = 0 To UBound(timers)
    If timers(i).Timer >= 0 Then timers(i).Timer = timers(i).Timer + TIMER_INTERVAL
Next
End Sub

'Drop box to change ImagXpress.AsyncPriority property
Private Sub AsyncPriorityCmbx_Click()
IXBase.AsyncPriority = AsyncPriorityCmbx.ItemData(AsyncPriorityCmbx.ListIndex)
End Sub




Private Sub LoadResize_Click()

Dim i As Integer
Dim bAllTimersDone As Boolean
bAllTimersDone = True
For i = 0 To UBound(timers)
    If timers(i).Timer <> -1 Then bAllTimersDone = False
Next

If bAllTimersDone Then
    ' go through all buffers and close them ( you must close a buffer when it is
    ' no longer needed so that the resources used by that buffer are freed )
    For i = 0 To UBound(timers)
        IXBase.CloseImage timers(i).ProcessID
    Next
    ReDim timers(0)
    viewIndex = 0
    For i = 0 To 5
        ViewerLabel(i).Caption = "" 'clear all of the labels
        IXViewer(i).ViewImageID = 0 ' clear all of the IX View objects
    Next
End If

If UBound(timers) = 0 Then
ReDim timers(1)
Else
ReDim Preserve timers(UBound(timers) + 2) 'make timers array grow by 2
End If

' load an image and resize it
If viewIndex > 5 Then viewIndex = 0

IXBase.ProcessImageID = IXBase.GetUniqueImageID()
ViewerLabel(viewIndex).Caption = "Process ID - " & IXBase.ProcessImageID
IXViewer(viewIndex).ViewImageID = IXBase.ProcessImageID

timers(viewIndex).ProcessID = IXBase.ProcessImageID
timers(viewIndex).Timer = 0 'timers begin incrementing when >= 0
IXBase.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\window.jpg"
IXBase.ResizeEx RT_PICQuality, IMAGE1_WIDTH * 8, IMAGE1_HEIGHT * 8


viewIndex = viewIndex + 1

'load another image and resize it
If viewIndex > 5 Then viewIndex = 0

IXBase.ProcessImageID = IXBase.GetUniqueImageID()
ViewerLabel(viewIndex).Caption = "Process ID - " & IXBase.ProcessImageID
IXViewer(viewIndex).ViewImageID = IXBase.ProcessImageID

timers(viewIndex).ProcessID = IXBase.ProcessImageID
timers(viewIndex).Timer = 0 'timers begin incrementing when >= 0
IXBase.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\Vermont.jpg"
IXBase.ResizeEx RT_PICQuality, IMAGE2_WIDTH * 8, IMAGE2_HEIGHT * 8

viewIndex = viewIndex + 1

End Sub

'Input box to change ImagXpress.AsyncMaxThreads property
Private Sub MaxThreadsTxt_Change()
Dim MaxThreads
If Not IsNumeric(MaxThreadsTxt.Text) Then
MaxThreads = 1
MaxThreadsTxt.Text = "1"
Else
MaxThreads = CInt(MaxThreadsTxt.Text)
End If
IXBase.AsyncMaxThreads = MaxThreads
End Sub

Private Sub mnuExit_Click()
    End
End Sub

Private Sub mnuToolbar_Click()
    IXBase.AboutBox
End Sub

'so the statusbox scrolls to show the last line entered
Private Sub StatusBox_Change()
StatusBox.SelStart = Len(StatusBox.Text)
End Sub

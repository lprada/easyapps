// EXIFTags.h : main header file for the EXIFTAGS application
//

#if !defined(AFX_EXIFTAGS_H__70768E54_83A3_4241_AFAD_5FB3CBACF2C0__INCLUDED_)
#define AFX_EXIFTAGS_H__70768E54_83A3_4241_AFAD_5FB3CBACF2C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CEXIFTagsApp:
// See EXIFTags.cpp for the implementation of this class
//

class CEXIFTagsApp : public CWinApp
{
public:
	CEXIFTagsApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEXIFTagsApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CEXIFTagsApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXIFTAGS_H__70768E54_83A3_4241_AFAD_5FB3CBACF2C0__INCLUDED_)

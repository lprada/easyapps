VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DocumentImagingandImageCleanup"
   ClientHeight    =   9975
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   14025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9975
   ScaleWidth      =   14025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "DocumentImagingandImageCleanup.frx":0000
      Left            =   360
      List            =   "DocumentImagingandImageCleanup.frx":0010
      TabIndex        =   71
      Top             =   120
      Width           =   13455
   End
   Begin VB.Frame frmDocZoomSmooth 
      Caption         =   "Document ZoomSmooth"
      Height          =   3375
      Left            =   960
      TabIndex        =   69
      Top             =   3840
      Width           =   9255
      Begin VB.CommandButton cmdZoomSmooth 
         Caption         =   "Document ZoomSmooth"
         Height          =   615
         Left            =   3360
         TabIndex        =   70
         Top             =   2040
         Width           =   2415
      End
   End
   Begin VB.Frame frmDocShear 
      Caption         =   "Document Shear"
      Height          =   3375
      Left            =   840
      TabIndex        =   61
      Top             =   3480
      Width           =   9255
      Begin VB.OptionButton optShearBlack 
         Caption         =   "Black"
         Height          =   495
         Left            =   4440
         TabIndex        =   75
         Top             =   1320
         Width           =   1575
      End
      Begin VB.OptionButton optShearWhite 
         Caption         =   "White"
         Height          =   375
         Left            =   2160
         TabIndex        =   74
         Top             =   1320
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.HScrollBar hscrShearAngle 
         Height          =   375
         Left            =   2160
         Max             =   70
         Min             =   -70
         TabIndex        =   64
         Top             =   600
         Width           =   5415
      End
      Begin VB.ComboBox cboShearType 
         Height          =   315
         ItemData        =   "DocumentImagingandImageCleanup.frx":00F7
         Left            =   2160
         List            =   "DocumentImagingandImageCleanup.frx":0101
         TabIndex        =   63
         Top             =   2040
         Width           =   3255
      End
      Begin VB.CommandButton cmdDocShear 
         Caption         =   "Document Shear"
         Height          =   615
         Left            =   3360
         TabIndex        =   62
         Top             =   2520
         Width           =   2535
      End
      Begin VB.Label lblShearAngle 
         Caption         =   "Shear Angle:"
         Height          =   375
         Left            =   240
         TabIndex        =   68
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label lblShearAngleVal 
         Height          =   375
         Left            =   7920
         TabIndex        =   67
         Top             =   720
         Width           =   495
      End
      Begin VB.Label lblShearPadColor 
         Caption         =   "Pad Color:"
         Height          =   495
         Left            =   240
         TabIndex        =   66
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label lblShearType 
         Caption         =   "Shear Type:"
         Height          =   375
         Left            =   240
         TabIndex        =   65
         Top             =   2040
         Width           =   1575
      End
   End
   Begin VB.Frame frmDocLineRemoval 
      Caption         =   "Document Line Removal"
      Height          =   3495
      Left            =   720
      TabIndex        =   18
      Top             =   3000
      Width           =   9495
      Begin VB.HScrollBar hscrLineRemMaxGap 
         Height          =   375
         Left            =   2520
         Max             =   20
         TabIndex        =   59
         Top             =   2040
         Value           =   1
         Width           =   5775
      End
      Begin VB.HScrollBar hscrMaxCharRepSize 
         Height          =   375
         Left            =   2520
         Max             =   100
         TabIndex        =   56
         Top             =   2520
         Value           =   20
         Width           =   5775
      End
      Begin VB.HScrollBar hscrLineRemMinAspRat 
         Height          =   375
         Left            =   2520
         Max             =   1000
         Min             =   1
         TabIndex        =   53
         Top             =   1560
         Value           =   10
         Width           =   5775
      End
      Begin VB.HScrollBar hscrLineRemMaxThick 
         Height          =   375
         Left            =   2520
         Max             =   50
         Min             =   1
         TabIndex        =   50
         Top             =   960
         Value           =   20
         Width           =   5775
      End
      Begin VB.HScrollBar hscrLineRemMinLen 
         Height          =   375
         Left            =   2520
         Max             =   20000
         Min             =   10
         TabIndex        =   47
         Top             =   480
         Value           =   50
         Width           =   5775
      End
      Begin VB.CommandButton cmdRemoveLines 
         Caption         =   "Document Line Removal"
         Height          =   375
         Left            =   3840
         TabIndex        =   19
         Top             =   3000
         Width           =   2295
      End
      Begin VB.Label lblLineRemMaxGapVal 
         Height          =   375
         Left            =   8520
         TabIndex        =   60
         Top             =   2040
         Width           =   615
      End
      Begin VB.Label lblLineRemMaxGap 
         Caption         =   "Max Gap:"
         Height          =   375
         Left            =   240
         TabIndex        =   58
         Top             =   2040
         Width           =   1695
      End
      Begin VB.Label lblMaxCharRepSizeVal 
         Height          =   375
         Left            =   8520
         TabIndex        =   57
         Top             =   2520
         Width           =   615
      End
      Begin VB.Label lblMaxCharRepSize 
         Caption         =   "Max Character Repair Size:"
         Height          =   375
         Left            =   240
         TabIndex        =   55
         Top             =   2640
         Width           =   2175
      End
      Begin VB.Label lblLineRemMinAspRatVal 
         Height          =   255
         Left            =   8520
         TabIndex        =   54
         Top             =   1680
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Min Aspect Ratio:"
         Height          =   375
         Left            =   240
         TabIndex        =   52
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label lblLineRemMaxThickVal 
         Height          =   255
         Left            =   8520
         TabIndex        =   51
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblLineRemMaxThick 
         Caption         =   "Max Thickness:"
         Height          =   375
         Left            =   240
         TabIndex        =   49
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label lblLineRemMinLenVal 
         Height          =   255
         Left            =   8520
         TabIndex        =   48
         Top             =   600
         Width           =   735
      End
      Begin VB.Label lblLineRemMinLen 
         Caption         =   "Minimum Length:"
         Height          =   375
         Left            =   240
         TabIndex        =   46
         Top             =   480
         Width           =   1335
      End
   End
   Begin VB.Frame frmDocErode 
      Caption         =   "Document Erode"
      Height          =   3375
      Left            =   720
      TabIndex        =   16
      Top             =   2760
      Width           =   9255
      Begin VB.HScrollBar hscrErodeAmount 
         Height          =   375
         Left            =   2040
         Max             =   500
         TabIndex        =   42
         Top             =   720
         Value           =   1
         Width           =   6015
      End
      Begin VB.ComboBox cboErodeDir 
         Height          =   315
         ItemData        =   "DocumentImagingandImageCleanup.frx":012B
         Left            =   2040
         List            =   "DocumentImagingandImageCleanup.frx":014A
         TabIndex        =   41
         Top             =   1560
         Width           =   3735
      End
      Begin VB.CommandButton cmdErode 
         Caption         =   "Document Erode"
         Height          =   735
         Left            =   3600
         TabIndex        =   17
         Top             =   2400
         Width           =   2055
      End
      Begin VB.Label lblErodeAmount 
         Caption         =   "Erode Amount:"
         Height          =   375
         Left            =   240
         TabIndex        =   45
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label lblErodeAmountVal 
         Height          =   255
         Left            =   8280
         TabIndex        =   44
         Top             =   840
         Width           =   615
      End
      Begin VB.Label blErodeDirection 
         Caption         =   "Direction:"
         Height          =   375
         Left            =   240
         TabIndex        =   43
         Top             =   1560
         Width           =   1455
      End
   End
   Begin VB.Frame frmDocDilate 
      Caption         =   "Document Dilate"
      Height          =   3495
      Left            =   840
      TabIndex        =   14
      Top             =   2520
      Width           =   9255
      Begin VB.ComboBox cboDilateDir 
         Height          =   315
         ItemData        =   "DocumentImagingandImageCleanup.frx":0244
         Left            =   2400
         List            =   "DocumentImagingandImageCleanup.frx":0263
         TabIndex        =   40
         Top             =   1440
         Width           =   3255
      End
      Begin VB.HScrollBar hscrDilateAmount 
         Height          =   375
         Left            =   2280
         Max             =   500
         TabIndex        =   37
         Top             =   600
         Value           =   1
         Width           =   5415
      End
      Begin VB.CommandButton cmdDilate 
         Caption         =   "Document Dilate"
         Height          =   615
         Left            =   3720
         TabIndex        =   15
         Top             =   2520
         Width           =   1695
      End
      Begin VB.Label lblDilateDirection 
         Caption         =   "Direction:"
         Height          =   375
         Left            =   360
         TabIndex        =   39
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label lblDilateAmountVal 
         Height          =   375
         Left            =   7920
         TabIndex        =   38
         Top             =   720
         Width           =   735
      End
      Begin VB.Label lblDilateAmount 
         Caption         =   "Dilate Amount:"
         Height          =   495
         Left            =   360
         TabIndex        =   36
         Top             =   600
         Width           =   1575
      End
   End
   Begin VB.Frame frmDocDespeckle 
      Caption         =   "Document Despeckle"
      Height          =   3495
      Left            =   720
      TabIndex        =   12
      Top             =   2280
      Width           =   9375
      Begin VB.HScrollBar hscrDespecSpeckH 
         Height          =   375
         Left            =   2400
         Max             =   100
         TabIndex        =   33
         Top             =   1440
         Value           =   2
         Width           =   5895
      End
      Begin VB.HScrollBar hscrDespecSpeckW 
         Height          =   375
         Left            =   2400
         Max             =   100
         TabIndex        =   31
         Top             =   600
         Value           =   2
         Width           =   5895
      End
      Begin VB.CommandButton cmdDespeckle 
         Caption         =   "Document Despeckle"
         Height          =   615
         Left            =   4080
         TabIndex        =   13
         Top             =   2640
         Width           =   1695
      End
      Begin VB.Label lblDespSpeckHVal 
         Height          =   375
         Left            =   8400
         TabIndex        =   35
         Top             =   1560
         Width           =   735
      End
      Begin VB.Label lblDespSpeckWVal 
         Height          =   375
         Left            =   8400
         TabIndex        =   34
         Top             =   720
         Width           =   735
      End
      Begin VB.Label lblDespSpeckH 
         Caption         =   "Speck Height:"
         Height          =   375
         Left            =   480
         TabIndex        =   32
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label lblDespSpeckW 
         Caption         =   "Speck Width:"
         Height          =   375
         Left            =   480
         TabIndex        =   30
         Top             =   720
         Width           =   1335
      End
   End
   Begin VB.ListBox lstStatus 
      Height          =   1230
      Left            =   10440
      TabIndex        =   8
      Top             =   7200
      Width           =   3375
   End
   Begin VB.Frame frmDocDeskew 
      Caption         =   "Document Deskew"
      Height          =   3375
      Left            =   720
      TabIndex        =   5
      Top             =   1440
      Width           =   9135
      Begin VB.OptionButton optDeskewBlack 
         Caption         =   "Black"
         Height          =   375
         Left            =   4200
         TabIndex        =   73
         Top             =   1560
         Width           =   1455
      End
      Begin VB.OptionButton optDeskewWhite 
         Caption         =   "White"
         Height          =   375
         Left            =   2160
         TabIndex        =   72
         Top             =   1560
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.HScrollBar hscrQuality 
         Height          =   375
         Left            =   2160
         Max             =   100
         TabIndex        =   28
         Top             =   2040
         Value           =   80
         Width           =   5535
      End
      Begin VB.CheckBox chkMaintainOrgSize 
         Caption         =   "Maint Original Size"
         Height          =   375
         Left            =   240
         TabIndex        =   26
         Top             =   2760
         Width           =   1935
      End
      Begin VB.HScrollBar hscrMinConfidence 
         Height          =   375
         Left            =   2160
         Max             =   100
         TabIndex        =   23
         Top             =   960
         Value           =   50
         Width           =   5535
      End
      Begin VB.HScrollBar hscrMinAngle 
         Height          =   375
         Left            =   2160
         Max             =   500
         TabIndex        =   20
         Top             =   360
         Value           =   20
         Width           =   5535
      End
      Begin VB.CommandButton cmdDeskew 
         Caption         =   "Document Deskew"
         Height          =   615
         Left            =   3600
         TabIndex        =   7
         Top             =   2640
         Width           =   1575
      End
      Begin VB.Label lblQualityVal 
         Height          =   375
         Left            =   7800
         TabIndex        =   29
         Top             =   2160
         Width           =   615
      End
      Begin VB.Label lblQuality 
         Caption         =   "Quality:"
         Height          =   375
         Left            =   240
         TabIndex        =   27
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label lblPadColor 
         Caption         =   "Pad Color:"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1560
         Width           =   1575
      End
      Begin VB.Label lblMinConfVal 
         Height          =   255
         Left            =   7800
         TabIndex        =   24
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label lblMinConfidence 
         Caption         =   "Minimum Confidence:"
         Height          =   375
         Left            =   240
         TabIndex        =   22
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label lblMinAngleVal 
         Height          =   375
         Left            =   7800
         TabIndex        =   21
         Top             =   480
         Width           =   495
      End
      Begin VB.Label lblMinAngle 
         Caption         =   "Minimum Angle:"
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   480
         Width           =   1215
      End
   End
   Begin VB.Frame frmDocBorderCrop 
      Caption         =   "Document Border Crop"
      Height          =   3255
      Left            =   480
      TabIndex        =   1
      Top             =   4920
      Width           =   9255
      Begin VB.CommandButton cmdBorderCrop 
         Caption         =   "Document Border Crop"
         Height          =   615
         Left            =   3600
         TabIndex        =   4
         Top             =   2400
         Width           =   1935
      End
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   3975
      Left            =   360
      TabIndex        =   3
      Top             =   5880
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   7011
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   8
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Border Crop"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Deskew"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Despeckle"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Dilate"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab5 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Erode"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab6 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Line Removal"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab7 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc Shear"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab8 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Doc ZoomSmooth"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress2 
      Height          =   4575
      Left            =   7440
      TabIndex        =   2
      Top             =   1200
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   8070
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1115557516
      ErrInfo         =   1800535531
      Persistence     =   -1  'True
      _cx             =   11668
      _cy             =   8070
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4575
      Left            =   360
      TabIndex        =   0
      Top             =   1200
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   8070
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1115557516
      ErrInfo         =   1800535531
      Persistence     =   -1  'True
      _cx             =   12091
      _cy             =   8070
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   10440
      TabIndex        =   11
      Top             =   6720
      Width           =   1815
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   10560
      TabIndex        =   10
      Top             =   8640
      Width           =   1935
   End
   Begin VB.Label lblError 
      Height          =   735
      Left            =   10560
      TabIndex        =   9
      Top             =   9120
      Width           =   2775
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim lDeskewPadColor As Long
Dim lShearPadColor As Long


Private Sub cmdBorderCrop_Click()
        
    
    If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
        
    ImagXpress2.DocumentBorderCrop
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
    
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
    
End Sub

Private Sub cmdDeskew_Click()
        
   If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
    
    ImagXpress2.DocumentDeskew hscrMinAngle.Value * 0.01, hscrMinConfidence.Value, _
                                lDeskewPadColor, chkMaintainOrgSize.Value, hscrQuality.Value
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
    
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
    
End Sub

Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
         
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
   
   ImagXpress1.ZoomToFit ZOOMFIT_BEST
     
End Sub

Private Sub cmdDeskResetToWhite_Click()
    txtPadColor = vbWhite
End Sub

Private Sub cmdDespeckle_Click()

       
    If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
        
    ImagXpress2.DocumentDespeckle hscrDespecSpeckW.Value, hscrDespecSpeckH.Value
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
   
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
    
End Sub

Private Sub cmdDilate_Click()

       
     If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
    
    ImagXpress2.DocumentDilate hscrDilateAmount.Value, cboDilateDir.ListIndex
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
   
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
    
End Sub

Private Sub cmdDocShear_Click()

    If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
    
    ImagXpress2.DocumentShear hscrShearAngle.Value, lShearPadColor, cboShearType.ListIndex
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
   
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
    
End Sub

Private Sub cmdErode_Click()
   
    
   If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
    
    ImagXpress2.DocumentErode hscrErodeAmount.Value, cboErodeDir.ListIndex
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
   
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
    
End Sub

Private Sub cmdRemoveLines_Click()

    If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
    
    ImagXpress2.DocumentLineRemoval hscrLineRemMinLen.Value, hscrLineRemMaxThick.Value, hscrLineRemMinAspRat.Value, _
                                    hscrLineRemMaxGap.Value, hscrMaxCharRepSize.Value
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
   
    ImagXpress2.ZoomToFit ZOOMFIT_BEST
        
End Sub

Private Sub cmdResetToWhite_Click()
    txtPadColorVal = vbWhite
End Sub

Private Sub cmdZoomSmooth_Click()

     If ImagXpress1.IBPP > 1 Then
            MsgBox "Image is " & ImagXpress1.IBPP & " BPP.  Converting image to 1 BPP."
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
            ImagXpress2.ColorDepth 1, IPAL_Fixed, DI_BinarizePhotoHalftone
    Else
            ImagXpress2.hDIB = ImagXpress1.CopyDIB
    End If
    
    ImagXpress2.DocumentZoomSmooth
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
   
       
End Sub

Private Sub Form_Load()

    
    ImagXpress1.ScrollBars = SB_Both
    ImagXpress2.ScrollBars = SB_Both

    frmDocBorderCrop.Top = TabStrip1.ClientTop
    frmDocBorderCrop.Left = TabStrip1.ClientLeft
    
    frmDocDeskew.Top = TabStrip1.ClientTop
    frmDocDeskew.Left = TabStrip1.ClientLeft
    
    frmDocDespeckle.Top = TabStrip1.ClientTop
    frmDocDespeckle.Left = TabStrip1.ClientLeft
    
    frmDocDilate.Top = TabStrip1.ClientTop
    frmDocDilate.Left = TabStrip1.ClientLeft
    
    frmDocErode.Top = TabStrip1.ClientTop
    frmDocErode.Left = TabStrip1.ClientLeft
    
    frmDocLineRemoval.Top = TabStrip1.ClientTop
    frmDocLineRemoval.Left = TabStrip1.ClientLeft
    
    
    frmDocShear.Top = TabStrip1.ClientTop
    frmDocShear.Left = TabStrip1.ClientLeft
    
    
    frmDocZoomSmooth.Top = TabStrip1.ClientTop
    frmDocZoomSmooth.Left = TabStrip1.ClientLeft
    
    
    cboDilateDir.ListIndex = 0
    cboErodeDir.ListIndex = 0
    cboShearType.ListIndex = 0
    
    lDeskewPadColor = vbWhite
    lShearPadColor = vbWhite
    
   
    
    lblMinAngleVal.Caption = hscrMinAngle.Value * 0.01
    lblMinConfVal.Caption = hscrMinConfidence.Value
    lblQualityVal = hscrQuality.Value
    lblDespSpeckHVal.Caption = hscrDespecSpeckH.Value
    lblDespSpeckWVal.Caption = hscrDespecSpeckW.Value
    lblDilateAmountVal.Caption = hscrDilateAmount.Value
    lblErodeAmountVal.Caption = hscrErodeAmount.Value
    lblLineRemMinLenVal = hscrLineRemMinLen.Value
    lblLineRemMaxThickVal.Caption = hscrLineRemMaxThick.Value
    lblLineRemMinAspRatVal.Caption = hscrLineRemMinAspRat.Value
    lblMaxCharRepSizeVal.Caption = hscrMaxCharRepSize.Value
    lblLineRemMaxGapVal.Caption = hscrLineRemMaxGap.Value
    lblShearAngleVal.Caption = hscrShearAngle.Value
    
    
    
    frmDocBorderCrop.Visible = True
    frmDocDeskew.Visible = False
    frmDocDespeckle.Visible = False
     frmDocDilate.Visible = False
    frmDocErode.Visible = False
    frmDocLineRemoval.Visible = False
    frmDocShear.Visible = False
    frmDocZoomSmooth.Visible = False
    
    imgParentDir = App.Path
    
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    ImagXpress2.EventSetEnabled EVENT_PROGRESS, True
    
       
    imgParentDir = App.Path
    
    imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\barcode.pcx"
    LoadFile
    
End Sub

Private Sub hscrDespecSpeckH_Change()
    lblDespSpeckHVal.Caption = hscrDespecSpeckH.Value
End Sub

Private Sub hscrDespecSpeckW_Change()
    
        lblDespSpeckWVal.Caption = hscrDespecSpeckW.Value
        
End Sub

Private Sub hscrDilateAmount_Change()
      lblDilateAmountVal.Caption = hscrDilateAmount.Value
End Sub

Private Sub hscrErodeAmount_Change()
    
    lblErodeAmountVal.Caption = hscrErodeAmount.Value

End Sub

Private Sub hscrLineRemMaxGap_Change()
    lblLineRemMaxGapVal.Caption = hscrLineRemMaxGap.Value
End Sub

Private Sub hscrLineRemMaxThick_Change()
    lblLineRemMaxThickVal.Caption = hscrLineRemMaxThick.Value
End Sub

Private Sub hscrLineRemMinLen_Change()
    lblLineRemMinLenVal = hscrLineRemMinLen.Value
End Sub

Private Sub hscrLineRemMinAspRat_Change()
    lblLineRemMinAspRatVal.Caption = hscrLineRemMinAspRat.Value
    
End Sub



Private Sub hscrMaxCharRepSize_Change()
    lblMaxCharRepSizeVal.Caption = hscrMaxCharRepSize.Value
End Sub

Private Sub hscrMinAngle_Change()
    lblMinAngleVal.Caption = hscrMinAngle.Value * 0.01
End Sub

Private Sub hscrMinConfidence_Change()
    lblMinConfVal.Caption = hscrMinConfidence.Value
End Sub



Private Sub hscrQuality_Change()
    lblQualityVal = hscrQuality.Value
End Sub

Private Sub hscrShearAngle_Change()
    
    lblShearAngleVal.Caption = hscrShearAngle.Value
    
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub ImagXpress2_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuAbout_Click()

    ImagXpress1.AboutBox

End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
   ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
         
    ImagXpress2.ToolbarActivated = Not ImagXpress2.ToolbarActivated
    
    Err = ImagXpress2.ImagError
    PegasusError Err, lblError
    
    
    End Sub

Private Sub optDeskewBlack_Click()
    If optDeskewBlack.Value = True Then
        lDeskewPadColor = vbBlack
    End If
End Sub

Private Sub optDeskewWhite_Click()
    If optDeskewWhite.Value = True Then
        lDeskewPadColor = vbWhite
    End If
End Sub


Private Sub optShearBlack_Click()
    If optShearBlack.Value = True Then
        lShearPadColor = vbBlack
    End If
End Sub

Private Sub optShearWhite_Click()
     If optShearWhite.Value = True Then
        lShearPadColor = vbWhite
    End If
End Sub

Private Sub TabStrip1_Click()

  Select Case TabStrip1.SelectedItem.Index
    
    Case 1
    
        frmDocBorderCrop.Visible = True
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = False
        frmDocDilate.Visible = False
        frmDocErode.Visible = False
        frmDocLineRemoval.Visible = False
         frmDocShear.Visible = False
         frmDocZoomSmooth.Visible = False
        
    Case 2
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = True
        frmDocDespeckle.Visible = False
         frmDocDilate.Visible = False
         frmDocErode.Visible = False
          frmDocLineRemoval.Visible = False
           frmDocShear.Visible = False
           frmDocZoomSmooth.Visible = False
        
    Case 3
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = True
         frmDocDilate.Visible = False
         frmDocErode.Visible = False
         frmDocLineRemoval.Visible = False
          frmDocShear.Visible = False
          frmDocZoomSmooth.Visible = False
   Case 4
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = False
        frmDocDilate.Visible = True
         frmDocErode.Visible = False
          frmDocLineRemoval.Visible = False
           frmDocShear.Visible = False
           frmDocZoomSmooth.Visible = False
   Case 5
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = False
        frmDocDilate.Visible = False
         frmDocErode.Visible = True
         frmDocLineRemoval.Visible = False
          frmDocShear.Visible = False
         frmDocZoomSmooth.Visible = False
    Case 6
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = False
        frmDocDilate.Visible = False
         frmDocErode.Visible = False
         frmDocLineRemoval.Visible = True
          frmDocShear.Visible = False
         frmDocZoomSmooth.Visible = False
     Case 7
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = False
        frmDocDilate.Visible = False
        frmDocErode.Visible = False
        frmDocLineRemoval.Visible = False
        frmDocShear.Visible = True
        frmDocZoomSmooth.Visible = False
        
    Case 8
    
        frmDocBorderCrop.Visible = False
        frmDocDeskew.Visible = False
        frmDocDespeckle.Visible = False
        frmDocDilate.Visible = False
        frmDocErode.Visible = False
        frmDocLineRemoval.Visible = False
        frmDocShear.Visible = False
        frmDocZoomSmooth.Visible = True
    End Select
    
End Sub

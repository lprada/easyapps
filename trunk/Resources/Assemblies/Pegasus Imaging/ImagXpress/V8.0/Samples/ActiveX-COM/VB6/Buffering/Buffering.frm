VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buffering Sample"
   ClientHeight    =   9960
   ClientLeft      =   3795
   ClientTop       =   960
   ClientWidth     =   9945
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9960
   ScaleWidth      =   9945
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1230
      ItemData        =   "Buffering.frx":0000
      Left            =   120
      List            =   "Buffering.frx":0016
      TabIndex        =   17
      Top             =   240
      Width           =   9255
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXView 
      Height          =   1575
      Index           =   0
      Left            =   0
      TabIndex        =   13
      Top             =   8280
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   2778
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   477723534
      ErrInfo         =   1731679785
      Persistence     =   -1  'True
      _cx             =   3836
      _cy             =   2778
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      AutoAssignID    =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   4215
      Left            =   0
      TabIndex        =   12
      Top             =   1800
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   7435
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   477723534
      ErrInfo         =   1731679785
      Persistence     =   -1  'True
      _cx             =   11245
      _cy             =   7435
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      AutoAssignID    =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdReloadImage 
      Caption         =   "Reload Image"
      Height          =   375
      Left            =   7200
      TabIndex        =   11
      Top             =   3720
      Width           =   1815
   End
   Begin VB.CommandButton cmdEmboss 
      Caption         =   "Emboss"
      Height          =   375
      Left            =   7200
      TabIndex        =   9
      Top             =   3240
      Width           =   1815
   End
   Begin VB.CommandButton cmdColorDepth 
      Caption         =   "Make 1 bit"
      Height          =   375
      Left            =   7200
      TabIndex        =   8
      Top             =   2880
      Width           =   1815
   End
   Begin VB.CommandButton cmdRotate 
      Caption         =   "Rotate 90"
      Height          =   375
      Left            =   7200
      TabIndex        =   7
      Top             =   2520
      Width           =   1815
   End
   Begin VB.ComboBox cboSelectImage 
      Height          =   315
      Left            =   7200
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   2040
      Width           =   1815
   End
   Begin VB.ListBox lstIPStatus 
      Height          =   645
      Left            =   240
      TabIndex        =   0
      Top             =   6720
      Width           =   9375
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXView 
      Height          =   1575
      Index           =   1
      Left            =   2400
      TabIndex        =   14
      Top             =   8280
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   2778
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   477723534
      ErrInfo         =   1731679785
      Persistence     =   -1  'True
      _cx             =   3836
      _cy             =   2778
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      AutoAssignID    =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXView 
      Height          =   1575
      Index           =   2
      Left            =   4800
      TabIndex        =   15
      Top             =   8280
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   2778
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   477723534
      ErrInfo         =   1731679785
      Persistence     =   -1  'True
      _cx             =   3836
      _cy             =   2778
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      AutoAssignID    =   0   'False
      ProcessImageID  =   3
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXView 
      Height          =   1575
      Index           =   3
      Left            =   7200
      TabIndex        =   16
      Top             =   8280
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   2778
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   477723534
      ErrInfo         =   1731679785
      Persistence     =   -1  'True
      _cx             =   3836
      _cy             =   2778
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      AutoAssignID    =   0   'False
      ProcessImageID  =   4
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1335
      Left            =   6840
      TabIndex        =   19
      Top             =   5040
      Width           =   2895
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   6840
      TabIndex        =   18
      Top             =   4440
      Width           =   1815
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000003&
      X1              =   240
      X2              =   9600
      Y1              =   7560
      Y2              =   7560
   End
   Begin VB.Label lblImageProcessStatus 
      Caption         =   "Image Process Status"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   6360
      Width           =   2535
   End
   Begin VB.Label lblSelectImage 
      Alignment       =   1  'Right Justify
      Caption         =   "Select Image to Process:"
      Height          =   255
      Left            =   6960
      TabIndex        =   6
      Top             =   1560
      Width           =   2055
   End
   Begin VB.Label lblViewBuf4 
      Caption         =   "View Buffer 4"
      Height          =   255
      Left            =   7320
      TabIndex        =   4
      Top             =   7800
      Width           =   2175
   End
   Begin VB.Label lblViewBuf3 
      Caption         =   "View Buffer 3"
      Height          =   255
      Left            =   4920
      TabIndex        =   3
      Top             =   7800
      Width           =   2175
   End
   Begin VB.Label lblViewBuf2 
      Caption         =   "View Buffer 2"
      Height          =   255
      Left            =   2520
      TabIndex        =   2
      Top             =   7800
      Width           =   2175
   End
   Begin VB.Label lblViewBuf1 
      Caption         =   "View Buffer 1"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   7800
      Width           =   2175
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim bBlob() As Byte, lCBDataLength As Long, PathStr As String, LoadCBFNam As String
Dim numBufs As Integer

Function StatusStr(Status As ixSTATUS) As String
'Takes the status flags and returns a list of
'the enabled ones as a string.
Dim Txt As String

    Txt = ""
    If Status = BUFSTATUS_EMPTY Then Txt = "Empty"
    If (Status And BUFSTATUS_OPENED) > 0 Then Txt = Txt & "Opened "
    If (Status And BUFSTATUS_DEFINED) > 0 Then Txt = Txt & "Defined "
    If (Status And BUFSTATUS_DECODING) > 0 Then Txt = Txt & "Decoding "
    If (Status And BUFSTATUS_DECODED) > 0 Then Txt = Txt & "Decoded "
    If (Status And BUFSTATUS_CANCELED) > 0 Then Txt = Txt & "Canceled "
    If (Status And BUFSTATUS_ERROR) > 0 Then Txt = Txt & "Error "
    If (Status And BUFSTATUS_IMAGEPROCESSED) > 0 Then Txt = Txt & "ImageProcessed "
    If (Status And BUFSTATUS_IMAGEPROCESSING) > 0 Then Txt = Txt & "ImageProcessing "
    If (Status And BUFSTATUS_WAIT) > 0 Then Txt = Txt & "Wait "
    StatusStr = Trim(Txt)
    
End Function

Private Sub cboSelectImage_Click()
    ' Set ViewImageID and ProcessImageID to the desired image buffer
    ImagXpress1.ViewImageID = cboSelectImage.ListIndex + 1
    ImagXpress1.ProcessImageID = cboSelectImage.ListIndex + 1
End Sub

Private Sub cmdColorDepth_Click()
    ImagXpress1.ColorDepth 1, 0, 0
End Sub

Private Sub cmdEmboss_Click()
    ImagXpress1.Emboss
End Sub

Private Sub cmdExit_Click()
    End
End Sub

Private Sub cmdReloadImage_Click()
    LoadCBFNam = PathStr & "\" & cboSelectImage.List(cboSelectImage.ListIndex)
    ImagXpress1.LoadCB
End Sub

Private Sub cmdRotate_Click()
    ImagXpress1.Rotate 270
End Sub


Private Sub Form_Load()
    Dim i As Integer
    Dim S As Long
        
    ImagXpress1.AutoSize = ISIZE_BestFit
    ImagXpress1.ScrollBars = SB_Both
    ImagXpress1.ViewAntialias = True
    ImagXpress1.AutoAssignID = False
    ImagXpress1.AutoInvalidate = True
    ImagXpress1.AutoClose = True
    ImagXpress1.OwnDIB = True
    ImagXpress1.ProcessImageID = 1
    ImagXpress1.ViewImageID = 1
    
    
     
    
    PathStr = App.Path & "\..\..\..\..\..\..\Common\Images"
     
    
    For i = 0 To 3
        IXView(i).AutoSize = ISIZE_BestFit
    Next

    numBufs = 4

    ' Load 4 image buffers with 4 different images
    For i = 1 To numBufs

        ' Set ProcessImageID to a new buffer, so that we can load the image
        ' into a new buffer in the CB event.  The ID parameter of the CB Event
        ' will be set to the ProcessImageID of the ImagXpress control which
        ' called the LoadCB method.
        ImagXpress1.ProcessImageID = i

        ' Set one of the IXView ImagXpress controls to view the image in this
        ' buffer, as a demonstration
        IXView(i - 1).ViewImageID = i
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError

        Select Case i
            Case 1:
                LoadCBFNam = PathStr & "\window.jpg"
            Case 2:
                LoadCBFNam = PathStr & "\door.jpg"
            Case 3:
                LoadCBFNam = PathStr & "\water.jpg"
            Case 4:
                LoadCBFNam = PathStr & "\boat.jpg"
        End Select
        
        ' LoadCB method with cause the ImagXpress CB Event to trigger
        ImagXpress1.LoadCB
        
        cboSelectImage.AddItem ParseFNStr(LoadCBFNam)
    Next
    
    cboSelectImage.ListIndex = 0
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call CloseAllBuffers
End Sub

Private Sub ImagXpress1_CB(ByVal ID As Integer, ByVal CBID As PegasusImagingActiveXImagXpress8Ctl.enumIXEVENT_CB_ID)
Dim ReadData As String, lCBReadSize As Long

    Select Case CBID
        Case CBID_OPEN:
            ' Get length of file to be opened
            lCBDataLength = FileLen(LoadCBFNam)
            If (lCBDataLength <= 0) Then
                lCBDataLength = 0
            Else
                ReDim bBlob(lCBDataLength) As Byte
            End If
            
            ' Open image file as binary
            Open LoadCBFNam For Binary Access Read As #1
            Get #1, , bBlob
            Close #1
            
        Case CBID_READ:
            ' Write binary data of image to buffer in memory
            ImagXpress1.WriteCBData ID, bBlob(0), lCBDataLength
            
            Err = ImagXpress1.ImagError
            PegasusError Err, lblError
    End Select
End Sub

Private Sub CloseAllBuffers()
    Dim i As Integer
    
    ' Clear all buffers used in this sample (ProcessImagID 1 thru 4)
    For i = 1 To numBufs
        ImagXpress1.CloseImage i
    Next
End Sub

Private Sub ImagXpress1_ImageStatusChanged(ByVal ID As Integer, ByVal eOPID As PegasusImagingActiveXImagXpress8Ctl.enumIPEffect, ByVal lStatus As Long)
    ' Display image status
    lstIPStatus.AddItem "Buffer " & ID & ": " & StatusStr(lStatus)
    lstIPStatus.ListIndex = lstIPStatus.ListCount - 1
End Sub

Private Function ParseFNStr(inputStr As String) As String
    ' Function to return the image filename to use in the cboSelectImage Combo
    
    Dim result, i, strLen As Integer
    strLen = Len(inputStr)
    
    Do Until result > 0
        result = InStr(strLen - i, inputStr, "\")
        i = i + 1
    Loop
    
    ParseFNStr = Right(inputStr, strLen - result)

End Function

Private Sub IXView_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    ImagXpress1.ProcessImageID = Index + 1
    ImagXpress1.ViewImageID = Index + 1
    cboSelectImage.ListIndex = Index
End Sub

Private Sub lblTitle_Click()

End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub


Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    End Sub


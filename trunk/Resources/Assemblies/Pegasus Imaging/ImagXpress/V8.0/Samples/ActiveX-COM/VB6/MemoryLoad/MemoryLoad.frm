VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmmemory 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "IX 8 Memory Load"
   ClientHeight    =   6225
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   9945
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6225
   ScaleWidth      =   9945
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox List1 
      Height          =   840
      ItemData        =   "MemoryLoad.frx":0000
      Left            =   360
      List            =   "MemoryLoad.frx":000A
      TabIndex        =   7
      Top             =   240
      Width           =   9375
   End
   Begin VB.CommandButton cmdclear 
      Caption         =   "Clear Image Data"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Top             =   5040
      Width           =   1815
   End
   Begin VB.CommandButton cmdbuffer 
      Caption         =   "Load via LoadBuffer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   4
      Top             =   5640
      Width           =   1935
   End
   Begin VB.CommandButton cmdblob 
      Caption         =   "Load via LoadBlob"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   3
      Top             =   5040
      Width           =   1935
   End
   Begin VB.CommandButton cmdByte 
      Caption         =   "Load Via LoadBufferPtr"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3960
      TabIndex        =   2
      Top             =   4440
      Width           =   1935
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress xpress2 
      Height          =   3015
      Left            =   5160
      TabIndex        =   1
      Top             =   1200
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   5318
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   2095342153
      ErrInfo         =   -1639138342
      Persistence     =   -1  'True
      _cx             =   8070
      _cy             =   5318
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress xpress1 
      Height          =   3015
      Left            =   360
      TabIndex        =   0
      Top             =   1200
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   5318
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   2095342153
      ErrInfo         =   -1639138342
      Persistence     =   -1  'True
      _cx             =   8070
      _cy             =   5318
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblerror 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6960
      TabIndex        =   6
      Top             =   4440
      Width           =   2175
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu Quit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&ToolBar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmmemory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String

'Declare API memory functions for image manipulation

Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function GlobalSize Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long

Private Sub cmdblob_Click()
 Dim lngHBufSize       As Long
  Dim lngBufferAddress  As Long
  Dim lReturn           As Long
  Dim bBlob()           As Byte
  Dim strBlob     As String
  SavePictureToMemory
  lngBufferAddress = GlobalLock(xpress1.SaveBufferHandle)
  If lngBufferAddress <> 0 Then
    lngHBufSize = GlobalSize(xpress1.SaveBufferHandle)
    If lngHBufSize > 0 Then
      ReDim bBlob(0 To lngHBufSize - 1) As Byte
      CopyMemory bBlob(0), ByVal lngBufferAddress, lngHBufSize
      lReturn = GlobalUnlock(xpress1.SaveBufferHandle)
      lReturn = GlobalFree(xpress1.SaveBufferHandle)
      xpress1.DeleteSaveBuffer
      strBlob = bBlob
      xpress2.LoadBlob strBlob, LenB(strBlob)
    Else
      MsgBox "Cannot Get Buffer Size", , "String"
    End If
  Else
    MsgBox "Cannot Get Buffer Address", , "String"
  End If
End Sub

Private Sub cmdbuffer_Click()
    'Copies image in Xpress1 to memory as TIFF G4 Compressed Data
   SavePictureToMemory
  xpress2.LoadBuffer xpress1.SaveBufferHandle
  Err = xpress1.ImagError

 PegasusError Err, lblerror
  xpress1.DeleteSaveBuffer
End Sub

Private Sub cmdByte_Click()
  Dim lngHBufSize       As Long
  Dim lngBufferAddress  As Long
  Dim lReturn           As Long
  Dim bBlob()           As Byte
 
  xpress1.SaveToBuffer = True
  xpress1.SaveFileType = FT_TIFF_G4
  xpress1.SaveFile
  lngBufferAddress = GlobalLock(xpress1.SaveBufferHandle)
  If lngBufferAddress <> 0 Then
    lngHBufSize = GlobalSize(xpress1.SaveBufferHandle)
    If lngHBufSize > 0 Then
      ReDim bBlob(0 To lngHBufSize - 1) As Byte
      CopyMemory bBlob(0), ByVal lngBufferAddress, lngHBufSize
      lReturn = GlobalUnlock(xpress1.SaveBufferHandle)
      lReturn = GlobalFree(xpress1.SaveBufferHandle)
      xpress2.LoadBufferPtr bBlob(0), lngHBufSize
      Err = xpress1.ImagError

    PegasusError Err, lblerror
    Else
      MsgBox "Cannot Get Buffer Size", , "Byte Array"
    End If
  Else
    MsgBox "Cannot Get Buffer Address", , "Byte Array"
  End If
End Sub


Private Sub cmdclear_Click()
    xpress2.hDIB = 0
End Sub

Private Sub SavePictureToMemory()
      'Copies image in Xpress1 to memory as TIF
  With xpress1
    .SaveToBuffer = True
    .SaveFileType = FT_TIFF
    .SaveFile  'Now Xpress1.SaveBufferHandle is defined
  End With
  
  Err = xpress1.ImagError

 PegasusError Err, lblerror
End Sub


Private Sub Form_Load()
    
  imgParentDir = App.Path
  
  Err = xpress1.ImagError
  PegasusError Err, lblerror
  
  imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\despeckle.tif"
  
  LoadFile
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    xpress1.hDIB = 0
    xpress2.hDIB = 0
End Sub



Private Sub mnuAbout_Click()
    xpress1.AboutBox
    
End Sub

Private Sub mnuOpen_Click()
       Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub


Private Sub mnuToolbarShow_Click()
         If xpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    xpress1.ToolbarActivated = Not xpress1.ToolbarActivated
    
    Err = xpress1.ImagError

    PegasusError Err, lblerror
End Sub

Private Sub Quit_Click()
    End
End Sub

Private Sub LoadFile()
   xpress1.FileName = imgFileName
  

   Err = xpress1.ImagError
   PegasusError Err, lblerror
   
End Sub

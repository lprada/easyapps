VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{89F92CAF-B12C-4D3B-B92B-F75DDAB357B6}#1.0#0"; "PrintPRO3.dll"
Begin VB.Form frmProfile 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Color Management"
   ClientHeight    =   8235
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   13605
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8235
   ScaleWidth      =   13605
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   2010
      Left            =   10440
      TabIndex        =   12
      Top             =   3600
      Width           =   2895
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      ItemData        =   "ICMProfiles.frx":0000
      Left            =   5520
      List            =   "ICMProfiles.frx":0028
      TabIndex        =   10
      Top             =   120
      Width           =   7815
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print Image"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8280
      TabIndex        =   5
      Top             =   6360
      Width           =   2055
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Reset to Defaults"
      Height          =   375
      Left            =   8280
      TabIndex        =   4
      Top             =   5880
      Width           =   2055
   End
   Begin VB.CheckBox chkEnabled 
      Caption         =   "ICM Enabled"
      Height          =   375
      Left            =   5640
      TabIndex        =   3
      Top             =   5880
      Width           =   1815
   End
   Begin VB.CheckBox chkProof 
      Caption         =   "Enable/Disable ICM Proofing"
      Height          =   375
      Left            =   5640
      TabIndex        =   2
      Top             =   6360
      Width           =   2415
   End
   Begin VB.ComboBox cmbRender 
      Height          =   315
      ItemData        =   "ICMProfiles.frx":0343
      Left            =   5640
      List            =   "ICMProfiles.frx":0353
      TabIndex        =   1
      Text            =   "Render Intent"
      Top             =   5160
      Width           =   2655
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   7695
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   13573
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   500118549
      ErrInfo         =   -1766688169
      Persistence     =   -1  'True
      _cx             =   9340
      _cy             =   13573
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblError 
      Height          =   1815
      Left            =   10440
      TabIndex        =   14
      Top             =   6240
      Width           =   2775
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   10440
      TabIndex        =   13
      Top             =   5760
      Width           =   1815
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   10440
      TabIndex        =   11
      Top             =   3000
      Width           =   1815
   End
   Begin VB.Label lblTargetProf 
      Height          =   255
      Left            =   5640
      TabIndex        =   9
      Top             =   4200
      Width           =   3855
   End
   Begin VB.Label lblPrinterProf 
      Height          =   255
      Left            =   5640
      TabIndex        =   8
      Top             =   3840
      Width           =   3855
   End
   Begin VB.Label lblMonitorProf 
      Height          =   255
      Left            =   5640
      TabIndex        =   7
      Top             =   3480
      Width           =   3855
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Render Intent Settings"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5640
      TabIndex        =   6
      Top             =   4800
      Width           =   2775
   End
   Begin PrintPRO3Ctl.PrintPRO PrintPRO1 
      Left            =   9600
      Top             =   7320
      _ExtentX        =   953
      _ExtentY        =   794
      ErrStr          =   "465D0233B3B3160635F08BE1741F44E0"
      ErrCode         =   500118549
      ErrInfo         =   1129510961
      _cx             =   953
      _cy             =   794
      AddCR           =   -1  'True
      Alignment       =   0
      BackColor       =   16777215
      BackStyle       =   0
      BMargin         =   266.4
      CurrentX        =   360
      CurrentY        =   266.4
      DocName         =   "PrintPRO Document"
      DrawMode        =   13
      DrawStyle       =   1
      DrawWidth       =   1
      FillColor       =   0
      FillStyle       =   1
      ForeColor       =   0
      LMargin         =   360
      ScaleMode       =   1
      TMargin         =   266.4
      WordWrap        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoMargin      =   -1  'True
      OutputFileName  =   ""
      PicTransparent  =   0   'False
      PicTransparentColor=   0
      UseDefaultPrinter=   -1  'True
      PrintPreviewScale=   1
      PrintPreviewOnly=   0   'False
      OwnDIB          =   0   'False
      PrintToFile     =   0   'False
      SaveFileType    =   4
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuMon 
         Caption         =   "Load Monitor Profile"
      End
      Begin VB.Menu mnuPrinter 
         Caption         =   "Load Printer Profile"
      End
      Begin VB.Menu mnuTarget 
         Caption         =   "Load Target Profile"
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open Image"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim renderint As Long
Dim txt As String

Dim imgFileName As String
Dim imgParentDir As String

Public gStrSavedICMMonitorProfile As String
Public gStrSavedICMPrinterrProfile As String
Public gStrSavedICMTargetProfile As String
Public geSavedRenderIntent As enumRenderIntent

Private Sub chkEnabled_Click()
    If chkEnabled.Value = 1 Then
        ImagXpress1.ICMEnabled = True
    ElseIf chkEnabled.Value = 0 Then
        ImagXpress1.ICMEnabled = False
    End If
End Sub

Private Sub chkProof_Click()
     If chkProof.Value = 1 Then
        ImagXpress1.ICMProofingEnabled = False
    ElseIf chkProof.Value = 0 Then
        ImagXpress1.ICMProofingEnabled = True
    End If
End Sub

Private Sub cmbRender_Click()
    renderint = cmbRender.ListIndex
    ImagXpress1.ICMRenderIntent = renderint
End Sub

Private Sub cmdClear_Click()
    ImagXpress1.ICMEnabled = False
    chkEnabled.Value = 0
    ImagXpress1.ICMProofingEnabled = False
    chkProof.Value = 0
    ImagXpress1.ICMMonitorProfileName = gStrSavedICMMonitorProfile
    ImagXpress1.ICMPrinterProfileName = gStrSavedICMPrinterrProfile
    ImagXpress1.ICMTargetProfileName = gStrSavedICMTargetProfile
    ImagXpress1.ICMRenderIntent = geSavedRenderIntent
    cmbRender.ListIndex = geSavedRenderIntent
    
    updateProfileLbls
    
End Sub

Private Sub cmdPrint_Click()
Dim a As Integer
Dim b As Integer
PrintPRO1.hDIB = ImagXpress1.hDIB
PrintPRO1.ScaleMode = SCALE_Pixel
PrintPRO1.StartPrintDoc
a = PrintPRO1.ScaleWidth - PrintPRO1.Lmargin
b = PrintPRO1.ScaleHeight - PrintPRO1.TMargin - PrintPRO1.BMargin
PrintPRO1.PrintDIB PrintPRO1.Lmargin, PrintPRO1.TMargin, a, b, 0, 0, 0, 0, True
PrintPRO1.EndPrintDoc
End Sub
Private Sub LoadFile()
   ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
End Sub

Private Sub Form_Load()
        
     ImagXpress1.Edition = EDITION_Photo
        
     ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
     
     imgParentDir = App.Path
     imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\window.jpg"
     LoadFile
     
     
    
     chkEnabled.Value = 1
     chkProof.Value = 1
     ImagXpress1.ScrollBars = SB_Both
     ImagXpress1.AutoSize = ISIZE_BestFit
     cmbRender.ListIndex = 0
     gStrSavedICMMonitorProfile = ImagXpress1.ICMMonitorProfileName
     gStrSavedICMPrinterrProfile = ImagXpress1.ICMPrinterProfileName
     gStrSavedICMTargetProfile = ImagXpress1.ICMTargetProfileName
     geSavedRenderIntent = ImagXpress1.ICMRenderIntent
     updateProfileLbls
End Sub
Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuExit_Click()
    End
End Sub

Private Sub mnuMon_Click()
    Dim strProfile As String
   
    ' Display a file open dialog for profiles
    strProfile = GetICMProfile(Me.hWnd, "OPEN")
    If Len(strProfile) <> 0 Then
        '**load the monitor profile here
        ImagXpress1.ICMMonitorProfileName = strProfile
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
        chkEnabled.Value = 1
        ImagXpress1.ICMEnabled = True
        chkProof.Value = 1
        ImagXpress1.ICMProofingEnabled = False
    End If
    updateProfileLbls
      
End Sub

Private Sub mnuPrinter_Click()
        Dim strProfile As String
   

    ' Display a file open dialog for profiles
    strProfile = GetICMProfile(Me.hWnd, "OPEN")
    If Len(strProfile) <> 0 Then
        ImagXpress1.ICMPrinterProfileName = strProfile
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
        cmdPrint.Enabled = True
        chkEnabled.Value = 1
        ImagXpress1.ICMEnabled = True
        chkProof.Value = 1
        ImagXpress1.ICMProofingEnabled = False
    End If
    updateProfileLbls
   
    
End Sub

Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuTarget_Click()
    Dim strProfile As String
   
    ' Display a file open dialog for profiles
    strProfile = GetICMProfile(Me.hWnd, "OPEN")
    If Len(strProfile) <> 0 Then
        '**load the monitor profile here
        ImagXpress1.ICMTargetProfileName = strProfile
        
        Err = ImagXpress1.ImagError
        PegasusError Err, lblError
        
        cmdPrint.Enabled = True
        chkEnabled.Value = 1
        ImagXpress1.ICMEnabled = True
        chkProof.Value = 1
        ImagXpress1.ICMProofingEnabled = False
    End If
    updateProfileLbls
  
   
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub
Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub

Private Sub updateProfileLbls()
    lblMonitorProf = "Monitor Profile: " & ImagXpress1.ICMMonitorProfileName
    lblPrinterProf = "Printer Profile: " & ImagXpress1.ICMPrinterProfileName
    lblTargetProf = "Target Profile: " & ImagXpress1.ICMTargetProfileName
End Sub

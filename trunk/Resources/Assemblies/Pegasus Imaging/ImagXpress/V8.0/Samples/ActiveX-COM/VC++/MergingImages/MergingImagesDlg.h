// MergingImagesDlg.h : header file
//

#if !defined(AFX_MERGINGIMAGESDLG_H__7C38A973_8FE1_4906_9D0D_E4FCC3D5BAB7__INCLUDED_)
#define AFX_MERGINGIMAGESDLG_H__7C38A973_8FE1_4906_9D0D_E4FCC3D5BAB7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"

/////////////////////////////////////////////////////////////////////////////
// CMergingImagesDlg dialog

class CMergingImagesDlg : public CDialog
{
	// Construction
public:
	CMergingImagesDlg(CWnd* pParent = NULL);	// standard constructor
	
	CImagXpress *ppCImagXpress1;
	IImagXpressPtr pImagXpress1;
	
	// Dialog Data
	//{{AFX_DATA(CMergingImagesDlg)
	enum { IDD = IDD_MERGINGIMAGES_DIALOG };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMergingImagesDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	HICON m_hIcon;
	
	// Generated message map functions
	//{{AFX_MSG(CMergingImagesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnQuit();
	afx_msg void OnDestroy();
	afx_msg void OnMergeimages();
	afx_msg void OnFileExit();
	afx_msg void OnHelp();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MERGINGIMAGESDLG_H__7C38A973_8FE1_4906_9D0D_E4FCC3D5BAB7__INCLUDED_)

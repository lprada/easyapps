//
// INCLUDES
//


#include "stdafx.h"
#include "ColorProfiles.h"
#include "ColorProfilesDlg.h"
#include "..\Include\ix8_open.cpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//
// DEFINES
//

BEGIN_MESSAGE_MAP(CColorProfilesDlg, CDialog)
	//{{AFX_MSG_MAP(CColorProfilesDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_CLEARALLPROFILES, OnBtnClearallprofiles)
	ON_BN_CLICKED(IDC_CHK_ICMENABLED, OnChkIcmenabled)
	ON_BN_CLICKED(IDC_CHK_ICMPROOFINGENABLED, OnChkIcmproofingenabled)
    ON_COMMAND(ID_FILE_LOADMONITORPROFILE, OnCmdLoadMonitorProfile)
    ON_COMMAND(ID_FILE_LOADPRINTERPROFILE, OnCmdLoadPrinterProfile)
    ON_COMMAND(ID_FILE_LOADTARGETPROFILE, OnCmdLoadTargetProfile)
    ON_COMMAND(ID_FILE_OPENIMAGE, OnCmdOpenImage) 
	ON_COMMAND(ID_FILE_EXIT, OnCmdExit) 
	ON_CBN_SELCHANGE(IDC_CBO_RENDERINTENTSETTINGS, OnSelchangeCboRenderintentsettings)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_SHOW, OnUpdateToolbarShow)
	ON_BN_CLICKED(IDC_BTN_PRINTIMAGE, OnBtnPrintimage)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_COMMAND(ID_HELP_ABOUTBOX, OnHelpAboutbox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//
// GLOBALS
//

const int k_iControlSpacingPixels       = 6;


//
// STATICS
//


LPCTSTR lpctstrRenderIntentNone         = _T("RI_GM_NONE");
LPCTSTR lpctstrRenderIntentImages       = _T("RI_GM_IMAGES");
LPCTSTR lpctstrRenderIntentBusiness     = _T("RI_GM_BUSINESS");
LPCTSTR lpctstrRenderIntentGraphics     = _T("RI_GM_GRAPHICS");
LPCTSTR lpctstrRenderIntentColorIMetric = _T("RI_GM_ABS_COLORIMETRIC");
LPCTSTR lpctstrMediaDirectory           = _T("..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");
LPCTSTR lpctstrDefaultImage             = _T("window.jpg");
LPCTSTR lpctstrDescription              = _T("This sample demonstrates using the IMCProfile capabilities of the PhotoEdition control.\nThe steps to run the sample are as follows:\n\n1) Choose the appropriate Load Profile option from the pull-down menu. If you are going to be performing printer proofing and want to use a printer/ink that is different than the selected printer please choose the Load Target Profile option.\n\n2) Select an image to load. The sample opens with a default image and profiles loaded.\n\n3) The ICMEnabled option and the ICRProofing options can be used to disable the ICM functionality and the proofing functionality respectively\n\n4) The RenderIntent options controls how the profiles should be applied. The RI_GM_IMAGES option is the default option.");
LPCTSTR lpctstrProfileDirectory         = _T("\\spool\\drivers\\color\\");
LPCTSTR lpctstrOpenProfileTitle         = _T("Open ICMProfile");
LPCTSTR lpctstrOpenProfileFilter        = _T("All Files (*.*)\0*.*\0ICM Files (*.icm)\0*.icm\0ICC Files (*.icc)\0*.icc\0");
LPCTSTR lpctstrOpenProfileDefExt        = _T("icm");
LPCTSTR lpctstrOpenImageTitle           = _T("Open Image File");
LPCTSTR lpctstrOpenImageFilter          = _T("Tagged Image Format (*.tif)\0*.tif\0Windows BMP (*.bmp)\0*.bmp\0JPEG (*.jpg)\0*.jpg\0CALS (*.CAL)\0*.CAL\0Windows Device Independent Bitmap (*.DIB)\0*.DIB\0MO:DCA (*.DCA + MOD)\0*.DCA;*.MOD\0ZSoft Multiple Page (*.DCX)\0*.DCX\0GIF (*.GIF)\0*.GIF\0JPEG 2000 (*.JP2)\0*.JP2\0JPEG LS (*.JLS)\0*.JLS\0Lossless JPG (*.LJP)\0*.LJP\0Portable BMP (*.PBM)\0*.PBM\0ZSoft PaintBrush (*.PCX)\0*.PCX\0Portable Graymap (*.PGM)\0*.PGM\0Pegasus PIC (*.PIC)\0*.PIC\0PNG (*.PNG)\0*.PNG\0Portable Pixmap (*.PPM)\0*.PPM\0Truevision TARGA (*.TGA)\0*.TGA\0WSQ (*.WSQ)\0*.WSQ\0JBIG2 File (*.JB2)\0*.JB2\0 All Files (*.*)\0*.*\0 IXSupportedFiles (*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2)\0*.BMP;*.JPG;*.TIF;*.TIFF;*.CALS;*.DIB;*.DCA;*.MOD;*.DCX;*.GIF;*.JP2;*.JLS;*.LJP;*.PBM;*.PCX;*.PGM;*.PIC;*.PNG;*.PPM;*.TGA;*.WSQ;*.JB2");
LPCTSTR lpctstrOpenImageDefExt          = _T("jpg");
LPCTSTR lpctstrOpenImageErrorMessage    = _T("An error occurred loading the image.");


//
// FUNCTIONS
//


CColorProfilesDlg::CColorProfilesDlg(
    CWnd* pParent /*=NULL*/)
	: CDialog(CColorProfilesDlg::IDD, pParent)
    , m_ppCImagXpress(NULL)
   , m_ppCPrintPRO(NULL)
    , m_iRenderIntent(PegasusImagingActiveXImagXpress8::RI_GM_IMAGES)
{
	//{{AFX_DATA_INIT(CColorProfilesDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}


void 
CColorProfilesDlg::DoDataExchange(
    CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CColorProfilesDlg)
	DDX_Control(pDX, IDC_CBO_RENDERINTENTSETTINGS, m_cboRenderIntentSettings);
	//}}AFX_DATA_MAP
}


BOOL 
CColorProfilesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    // Open the media subdirectory first
    //
    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];
    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
    CString strDefaultFilePath = CString(drive) + CString(dir) 
        + lpctstrMediaDirectory + lpctstrDefaultImage;    

    HINSTANCE hDLL = IX_Open();
    if (m_ppCImagXpress == NULL)
    {
        CRect clientArea;
        this->GetClientRect(clientArea);
        m_ppCImagXpress = new CImagXpress(
            (DWORD) this, 1, (long) m_hWnd, 
            k_iControlSpacingPixels, k_iControlSpacingPixels, 
            (clientArea.right - clientArea.left - 2 * k_iControlSpacingPixels) / 2,
            (clientArea.bottom - clientArea.top  - 2 * k_iControlSpacingPixels));

        m_pImagXpress = m_ppCImagXpress->pImagXpress;
        m_pImagXpress->FileName = strDefaultFilePath.AllocSysString();
        m_pImagXpress->ScrollBars = SB_Both;

        m_strSavedICMMonitorProfile     = (char*) m_pImagXpress->ICMMonitorProfileName;
        m_strSavedICMPrinterrProfile    = (char*) m_pImagXpress->ICMPrinterProfileName;
        m_strSavedICMTargetProfile      = (char*) m_pImagXpress->ICMTargetProfileName;
        m_iRenderIntent                 = m_pImagXpress->ICMRenderIntent;
    }
    if (m_ppCPrintPRO == NULL)
    {
        m_ppCPrintPRO = new CPrintPRO((DWORD) this, 2);
        m_pPrintPRO = m_ppCPrintPRO->pPrintPRO;
   }
    IX_Close(hDLL);

    CWnd* pDescription = this->GetDlgItem(IDC_LBL_DESCRIPTION);
    if (pDescription)
    {
        pDescription->SetWindowText(lpctstrDescription);
    }
    
    this->CheckDlgButton(IDC_CHK_ICMENABLED, BST_CHECKED);
    this->CheckDlgButton(IDC_CHK_ICMPROOFINGENABLED, BST_CHECKED);

    CWnd* pBtnPrintImage = GetDlgItem(IDC_BTN_PRINTIMAGE);
    if (pBtnPrintImage)
    {
        pBtnPrintImage->EnableWindow(FALSE);
    }

    // Initalize Render Intent combo box
    //
    m_cboRenderIntentSettings.ResetContent();
    m_cboRenderIntentSettings.AddString(lpctstrRenderIntentImages);
    m_cboRenderIntentSettings.AddString(lpctstrRenderIntentBusiness);
    m_cboRenderIntentSettings.AddString(lpctstrRenderIntentGraphics);
    m_cboRenderIntentSettings.AddString(lpctstrRenderIntentColorIMetric);    
    m_cboRenderIntentSettings.SetCurSel(0);


	TCHAR fnText[LF_FACESIZE] = "Tahoma";
	pEdtInfo = new CRichEditCtrl;
	pEdtInfo->Create(WS_CHILD|WS_VISIBLE|WS_BORDER|ES_MULTILINE,CRect(366,6,720,268),this,1);
	pEdtInfo->SetOptions(ECOOP_SET,ECO_READONLY);
	
	CHARFORMAT cf;
	ZeroMemory(&cf, sizeof(CHARFORMAT));
	cf.cbSize = sizeof(CHARFORMAT);
	cf.dwMask = CFM_BOLD | CFM_FACE | CFM_SIZE;
	cf.dwEffects = CFE_BOLD;
	cf.yHeight = 160;
	strcpy(cf.szFaceName,fnText);
	pEdtInfo->SetDefaultCharFormat(cf);
	pEdtInfo->ReplaceSel(lpctstrDescription);

	return TRUE;  // return TRUE  unless you set the focus to a control
}


/** 
 * If you add a minimize button to your dialog, you will need the code below
 * to draw the icon.  For MFC applications using the document/view model,
 * this is automatically done for you by the framework.
 */
void 
CColorProfilesDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


/**
 * The system calls this to obtain the cursor to display while the user drags
 *  the minimized window.
 */
HCURSOR 
CColorProfilesDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void 
CColorProfilesDlg::OnDestroy()
{
    CDialog::OnDestroy();
    if (m_pImagXpress)
    {
        m_pImagXpress = NULL;    
        if (m_ppCImagXpress)
        {
            delete m_ppCImagXpress;
            m_ppCImagXpress = NULL;
        }
    }
   if (m_pPrintPRO)
    {
        m_pPrintPRO = NULL;
        if (m_ppCPrintPRO)
        {
            delete m_ppCPrintPRO;
            m_ppCPrintPRO = NULL;
        }
    }
}
 

void 
CColorProfilesDlg::OnBtnClearallprofiles() 
{
    if (m_pImagXpress)
    {
        m_pImagXpress->ICMEnabled = FALSE;
        m_pImagXpress->ICMProofingEnabled = FALSE;
        m_pImagXpress->ICMMonitorProfileName = 
            m_strSavedICMMonitorProfile.AllocSysString();
        m_pImagXpress->ICMPrinterProfileName = 
            m_strSavedICMPrinterrProfile.AllocSysString();
        m_pImagXpress->ICMTargetProfileName = 
            m_strSavedICMTargetProfile.AllocSysString();
        m_pImagXpress->ICMRenderIntent = m_iRenderIntent;
    }
    CheckDlgButton(IDC_CHK_ICMENABLED, BST_UNCHECKED);
    CheckDlgButton(IDC_CHK_ICMPROOFINGENABLED, BST_UNCHECKED);
}






void
CColorProfilesDlg::OnChkIcmenabled() 
{
    if (m_pImagXpress)
    {
        m_pImagXpress->ICMEnabled = 
            !(IsDlgButtonChecked(IDC_CHK_ICMENABLED) != 0);
    }
}


void 
CColorProfilesDlg::OnChkIcmproofingenabled() 
{
    if (m_pImagXpress)
    {
        m_pImagXpress->ICMProofingEnabled = 
            !(IsDlgButtonChecked(IDC_CHK_ICMPROOFINGENABLED) != 0);
    }
}


void 
CColorProfilesDlg::OnCmdLoadMonitorProfile()
{
    CString strProfile;
    if (m_pImagXpress != NULL
        && GetICMProfilePath(strProfile)
        && strProfile.GetLength() > 0)
    {
        CWaitCursor oHourGlass;
        m_pImagXpress->ICMMonitorProfileName = strProfile.AllocSysString();
        m_pImagXpress->ICMProofingEnabled = FALSE;
        m_pImagXpress->ICMEnabled = TRUE;

        this->CheckDlgButton(IDC_CHK_ICMENABLED, BST_CHECKED);
        this->CheckDlgButton(IDC_CHK_ICMPROOFINGENABLED, BST_CHECKED);   
    }
}



void 
CColorProfilesDlg::OnCmdLoadPrinterProfile()
{
    CString strProfile;
    if (m_pImagXpress != NULL
        && GetICMProfilePath(strProfile)
        && strProfile.GetLength() > 0)
    {
        CWaitCursor oHourGlass;
        m_pImagXpress->ICMPrinterProfileName = strProfile.AllocSysString();
        m_pImagXpress->ICMEnabled = TRUE;
        m_pImagXpress->ICMProofingEnabled = FALSE;

        this->CheckDlgButton(IDC_CHK_ICMENABLED, BST_CHECKED);
        this->CheckDlgButton(IDC_CHK_ICMPROOFINGENABLED, BST_CHECKED);   
        CWnd* pBtnPrintImage = GetDlgItem(IDC_BTN_PRINTIMAGE);
        if (pBtnPrintImage)
        {
            pBtnPrintImage->EnableWindow(TRUE);
        }
    }
}


void 
CColorProfilesDlg::OnCmdLoadTargetProfile()
{
    CString strProfile;
    if (m_pImagXpress != NULL
        && GetICMProfilePath(strProfile)
        && strProfile.GetLength() > 0)
    {
        CWaitCursor oHourGlass;
        m_pImagXpress->ICMTargetProfileName = strProfile.AllocSysString();
        m_pImagXpress->ICMEnabled = TRUE;
        m_pImagXpress->ICMProofingEnabled = FALSE;

        this->CheckDlgButton(IDC_CHK_ICMENABLED, BST_CHECKED);
        this->CheckDlgButton(IDC_CHK_ICMPROOFINGENABLED, BST_CHECKED);   
        CWnd* pBtnPrintImage = GetDlgItem(IDC_BTN_PRINTIMAGE);
        if (pBtnPrintImage)
        {
            pBtnPrintImage->EnableWindow(TRUE);
        }
    }

}


void 
CColorProfilesDlg::OnCmdOpenImage()
{
    CString strImageFilePath;
    if (m_pImagXpress != NULL
        && GetImagePath(strImageFilePath)
        && strImageFilePath.GetLength() > 0)
    {
        CWaitCursor oHourGlass;
        m_pImagXpress->FileName = strImageFilePath.AllocSysString();
        this->CheckDlgButton(IDC_CHK_ICMENABLED, BST_CHECKED);
        this->CheckDlgButton(IDC_CHK_ICMPROOFINGENABLED, BST_CHECKED); 
        if (m_pImagXpress->ImagError != 0)
        {
            CString strMessage;
            this->GetWindowText(strMessage);
            MessageBox(lpctstrOpenImageErrorMessage, 
                strMessage, MB_ICONERROR);
        }
    }
}


void 
CColorProfilesDlg::OnCmdExit()
{
    EndDialog(IDOK);
}

/**
 * If None is selected the RenderIntent will be assigned -1.
 */
void 
CColorProfilesDlg::OnSelchangeCboRenderintentsettings() 
{
    m_iRenderIntent = (PegasusImagingActiveXImagXpress8::enumRenderIntent)
        (m_cboRenderIntentSettings.GetCurSel());
    if (m_pImagXpress)
    {
        m_pImagXpress->ICMRenderIntent = m_iRenderIntent; 
    }
}


bool 
CColorProfilesDlg::GetICMProfilePath(
    CString& strICMProfilePath) const
{    
    bool results = false;
    const BOOL bOpenFileDialog = TRUE;
    const DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
    DWORD dwSize = 0;

    CFileDialog ofd(TRUE);

    TCHAR szPath[MAX_PATH];
    ::GetSystemDirectory(szPath, sizeof(szPath) - 1);
    CString strProfileDirectory = CString(szPath) + lpctstrProfileDirectory;

    ofd.m_ofn.lpstrInitialDir   = strProfileDirectory;
    ofd.m_ofn.lpstrTitle        = lpctstrOpenProfileTitle;
    ofd.m_ofn.lpstrFilter       = lpctstrOpenProfileFilter;
    ofd.m_ofn.lpstrDefExt       = lpctstrOpenProfileDefExt;

    if (ofd.DoModal() == IDOK)
    {
        strICMProfilePath = CString(ofd.m_ofn.lpstrFile);
        results = true;
    }
    return results;
}


bool CColorProfilesDlg::GetImagePath(
    CString& strImageFilePath) const
{    
    bool results = false;
    const BOOL bOpenFileDialog = TRUE;
    const DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
    DWORD dwSize = 0;

    CFileDialog ofd(TRUE);

    TCHAR szPath[MAX_PATH];
    GetModuleFileName(NULL, szPath, sizeof(szPath) - 1);
    TCHAR drive[_MAX_DRIVE];
    TCHAR dir[_MAX_DIR];
    TCHAR fname[_MAX_FNAME];
    TCHAR ext[_MAX_EXT];    
    _splitpath(szPath, drive, dir, fname, ext);
    CString strDefaultFilePath = CString(drive) + CString(dir) 
        + lpctstrMediaDirectory + lpctstrDefaultImage;   

    ofd.m_ofn.lpstrInitialDir   = strDefaultFilePath;
    ofd.m_ofn.lpstrTitle        = lpctstrOpenImageTitle;
    ofd.m_ofn.lpstrFilter       = lpctstrOpenImageFilter;
    ofd.m_ofn.lpstrDefExt       = lpctstrOpenImageDefExt;

    if (ofd.DoModal() == IDOK)
    {
        strImageFilePath = CString(ofd.m_ofn.lpstrFile);
        results = true;
    }
    return results;
}

void CColorProfilesDlg::OnUpdateToolbarShow(CCmdUI* pCmdUI) 
{
	

	CWnd* pMain = AfxGetMainWnd();
	CMenu* mmenu = pMain->GetMenu();
	CMenu* submenu = mmenu->GetSubMenu(1);

	if(m_pImagXpress->ToolbarActivated)
	{
		
		submenu->ModifyMenu(ID_TOOLBAR_SHOW, MF_BYCOMMAND,ID_TOOLBAR_SHOW,"&Show");
	}
	else
	{
		submenu->ModifyMenu(ID_TOOLBAR_SHOW, MF_BYCOMMAND,ID_TOOLBAR_SHOW,"&Hide");

	}
	//enable/disable the toolbar
    m_pImagXpress->ToolbarActivated = ! m_pImagXpress->ToolbarActivated;


}

	
void CColorProfilesDlg::OnBtnPrintimage() 
{
 if (m_pPrintPRO != NULL
        && m_pImagXpress != NULL)
    {
        float width     = 0.0;
        float height    = 0.0;

        m_pPrintPRO->hDIB = m_pImagXpress->hDIB;
        m_pPrintPRO->ScaleMode = PrintPRO3::SCALE_Pixel;
        m_pPrintPRO->StartPrintDoc();

        width = m_pPrintPRO->ScaleWidth - m_pPrintPRO->Lmargin;
        height = m_pPrintPRO->ScaleHeight - m_pPrintPRO->TMargin
            - m_pPrintPRO->BMargin;
        m_pPrintPRO->PrintDIB(m_pPrintPRO->Lmargin, m_pPrintPRO->TMargin, 
            width, height, 0, 0, 0, 0, TRUE);

        m_pPrintPRO->EndPrintDoc();	
	}

}

void CColorProfilesDlg::OnButton1() 
{
 if (m_pPrintPRO != NULL
        && m_pImagXpress != NULL)
    {
        float width     = 0.0;
        float height    = 0.0;

        m_pPrintPRO->hDIB = m_pImagXpress->CopyDIB();
        m_pPrintPRO->ScaleMode = PrintPRO3::SCALE_Pixel;
        m_pPrintPRO->StartPrintDoc();

        width = m_pPrintPRO->ScaleWidth - m_pPrintPRO->Lmargin;
        height = m_pPrintPRO->ScaleHeight - m_pPrintPRO->TMargin
            - m_pPrintPRO->BMargin;
        m_pPrintPRO->PrintDIB(m_pPrintPRO->Lmargin, m_pPrintPRO->TMargin, 
            width, height, 0, 0, 0, 0, TRUE);

        m_pPrintPRO->EndPrintDoc();	
}


}

void CColorProfilesDlg::OnHelpAboutbox() 
{
	m_pImagXpress->AboutBox();	
}

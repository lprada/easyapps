
// ImagXpress COM VC++ Initialization routines.
// Copyright (c) 2005-2006 Pegasus Imaging Corp.

// ----------------------------------------------------------------------------------
// Queries the system registry to get the path for registered version of PegasusImaging.ActiveX.ImagXpress8.dll
// Performs LoadLibrary on that DLL.
// ----------------------------------------------------------------------------------
HINSTANCE LoadLibraryImagXpr8DLL()
{
  HANDLE  hKey;
  long    len=255;
  long    result;
  TCHAR    buf[256];
  memset(&buf, 0x00, sizeof(buf));

  // Query the system registry for the PegasusImaging.ActiveX.ImagXpress8.dll path
  #ifdef __BORLANDC__
    result = RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("Software\\Classes\\CLSID\\{6277B638-833D-4315-9D78-60FC451DAF07}"), (Windows::PHKEY)&hKey);
  #else
    result = RegOpenKey(HKEY_LOCAL_MACHINE, TEXT("Software\\Classes\\CLSID\\{6277B638-833D-4315-9D78-60FC451DAF07}"), (PHKEY)&hKey);
  #endif

  if (result==ERROR_SUCCESS)
  {
    result = RegQueryValue((HKEY)hKey, TEXT("InprocServer32\0"), buf, &len);
    RegCloseKey((HKEY)hKey);
  }

  // If we found the path from the registry; LoadLibrary from that path
  // Otherwise attempt to load it from the system path.
  if (result==ERROR_SUCCESS)
    return LoadLibrary(buf);  
  else
    return LoadLibrary(TEXT("PegasusImaging.ActiveX.ImagXpress8.dll"));
}

typedef HRESULT (WINAPI* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE IX_Open()
{
  HINSTANCE    hDLL;            // Handle to ImagXpress DLL
  LPFNDLL_PSU  lpfnDllFunc1;    // Function pointer to the UnlockControl function
  HRESULT      hr;
  
  hDLL = LoadLibraryImagXpr8DLL();
  if (hDLL)
  {
    // Get the address of the UnlockControl function.  You need to give the COM object
    // the serial number and registration code that you received when
    // you purchased ImagXpress
    lpfnDllFunc1 = (LPFNDLL_PSU)GetProcAddress(hDLL, "UnlockControl");

    if (lpfnDllFunc1)
      // Call the unlock function with the serial number and registration code
      // that you received when you purchased ImagXpress.
      // NOTE: The unlock codes shown below are for
      // illustration purposes only.
      hr = lpfnDllFunc1(123456789, 123456789, 123456789, 123456789);
    else
    {
      FreeLibrary(hDLL);
      hDLL = NULL;
    }
  }
  return hDLL;
}

void IX_Close(HINSTANCE hDLL)
{
  if (hDLL)
    FreeLibrary(hDLL);
}

// stockDlg.cpp : implementation file

// ------------------------------------------------------------------------------------------- 
// Importing the ActiveX control via COM does not import the container stock properties.
// Implementing Stock properties must be done manually.
// Many of these stock properties can be implemented by obtaining the ImagXpress Window handle
// and making Windows API calls.  This example demonstrates how to do this.
// ------------------------------------------------------------------------------------------- 

#include "stdafx.h"
#include "stock.h"
#include "stockDlg.h"
#include <richedit.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Include the ImagXpress Initialization routines
#include "..\Include\ix8_open.cpp"

/////////////////////////////////////////////////////////////////////////////
// CStockDlg dialog

CStockDlg::CStockDlg(CWnd* pParent /*=NULL*/)
: CDialog(CStockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStockDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CStockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStockDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CStockDlg, CDialog)
//{{AFX_MSG_MAP(CStockDlg)
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_BN_CLICKED(ID_CHANGEBACKCOLOR, OnChangebackcolor)
ON_BN_CLICKED(ID_CHANGESIZE, OnChangesize)
ON_BN_CLICKED(ID_CHANGEPOSITION, OnChangeposition)
ON_BN_CLICKED(ID_CHANGEVISIBILITY, OnChangevisibility)
ON_WM_DESTROY()
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStockDlg message handlers

BOOL CStockDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// Create an ImagXpress Object
	HINSTANCE hDLL = IX_Open();  // ImagXpress Initialize
	ppCImagXpress = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 50, 50, 100, 250);
	pImagXpress = ppCImagXpress->pImagXpress;
	
	if (pImagXpress)
	{
		pImagXpress->BorderType = BORD_Raised;
		pImagXpress->AutoSize = ISIZE_ResizeControl;
		pImagXpress->BackColor = RGB(0,255,0);
	}
	
	IX_Close(hDLL); // ImagXpress UnInitialize
	

	TCHAR fnText[LF_FACESIZE] = "Tahoma";
	char msgText[] = "This sample demonstrates the following:\n1.) Implementing  ActiveX  stock properties using the ImagXpress Window handle and API  functions.";


	pEdtInfo = new CRichEditCtrl;
	//pEdtInfo->Create
	pEdtInfo->Create(WS_CHILD|WS_VISIBLE|WS_BORDER|ES_MULTILINE,CRect(8,8,470,56),this,1);
	pEdtInfo->SetOptions(ECOOP_SET,ECO_READONLY);
	
	CHARFORMAT cf;
	ZeroMemory(&cf, sizeof(CHARFORMAT));
	cf.cbSize = sizeof(CHARFORMAT);
	cf.dwMask = CFM_BOLD | CFM_FACE | CFM_SIZE;
	cf.dwEffects = CFE_BOLD;
	cf.yHeight = 160;
	strcpy(cf.szFaceName,fnText);
	pEdtInfo->SetDefaultCharFormat(cf);
	pEdtInfo->ReplaceSel(msgText);


	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CStockDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
		ShowInfo();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CStockDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}



void CStockDlg::OnChangebackcolor() 
{
	OLE_COLOR clrBackColor = pImagXpress->GetBackColor();
	switch (clrBackColor)
	{
    case RGB(255,0,0):
		pImagXpress->BackColor = RGB(0,255,0);
		break;
    case RGB(0,255,0):
		pImagXpress->BackColor = RGB(0,0,255);
		break;
    default:
		pImagXpress->BackColor = RGB(255,0,0);
		break;
	}
	ShowInfo();
}

void CStockDlg::GetIXCoords(IImagXpressPtr pImagXpress, int *x, int*y, int *width, int *height)
{
	RECT IRect, PRect, PCRect;
	HWND hWndParent = ::GetParent((HWND)pImagXpress->GethWnd());
	
	// Note: ImagXpress BorderSize is 2 (or 0 if no borders are specified)
	int xBorderSize = GetSystemMetrics(SM_CXBORDER) + ((pImagXpress->GetBorderType()>1) ? 2: 0);
	int yBorderSize = GetSystemMetrics(SM_CYBORDER) + ((pImagXpress->GetBorderType()>1) ? 2: 0);
	
	::GetWindowRect((HWND)pImagXpress->hWnd, &IRect);  // ImagXpress Rect
	::GetWindowRect(hWndParent, &PRect);  // Parent Rect
	::GetClientRect(hWndParent, &PCRect);  // Parent ClientRect
	*x = IRect.left - PRect.left - (PRect.right - PRect.left - PCRect.right)  + xBorderSize; 
	*y = IRect.top - PRect.top   - (PRect.bottom - PRect.top - PCRect.bottom) + yBorderSize; 
	*width = IRect.right - IRect.left;
	*height = IRect.bottom - IRect.top;
}

void CStockDlg::ShowInfo()
{
	char buffer[100];
	int x, y, width, height;
	OLE_COLOR clrBackColor = pImagXpress->BackColor;
	GetIXCoords(pImagXpress, &x, &y, &width, &height);
	sprintf(buffer, "x: %i   y: %i   width: %i   height: %i   Visible: %s   Color: %s", 
		x, y, width, height,
		::IsWindowVisible((HWND)pImagXpress->hWnd) ? "YES" : "NO",
		clrBackColor==RGB(255,0,0) ? "RED" : clrBackColor==RGB(0,0,255) ? "BLUE" : "GREEN");
	SetDlgItemText(IDC_INFO, buffer);
}

void CStockDlg::OnChangesize() 
{
	int x, y, width, height;
	GetIXCoords(pImagXpress, &x, &y, &width, &height);
	
	switch(width)
	{
    case 100:
		::MoveWindow((HWND)pImagXpress->hWnd, x, y, 250, 100, TRUE);
		break;
    default:
		::MoveWindow((HWND)pImagXpress->hWnd, x, y, 100, 250, TRUE);
		break;
	}
}

void CStockDlg::OnChangeposition() 
{
	int x, y, width, height;
	GetIXCoords(pImagXpress, &x, &y, &width, &height);
	
	switch(x)
	{
    case 50:
		::MoveWindow((HWND)pImagXpress->hWnd, 75, 75, width, height, TRUE);
		break;
    default:
		::MoveWindow((HWND)pImagXpress->hWnd, 50, 50, width, height, TRUE);
		break;
	}
}

void CStockDlg::OnChangevisibility() 
{
	if (::IsWindowVisible((HWND)pImagXpress->hWnd))
	{
		::ShowWindow((HWND)pImagXpress->hWnd, SW_HIDE);  // Make the object invisible
		SetDlgItemText(ID_CHANGEVISIBILITY, "Make Visible");
	}
	else
	{
		::ShowWindow((HWND)pImagXpress->hWnd, SW_SHOW);  // Make the object visible
		SetDlgItemText(ID_CHANGEVISIBILITY, "Make Invisible");
	}
}

void CStockDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// Delete ImagXpress
	if (pImagXpress)
	{
		pImagXpress = NULL;
		if (ppCImagXpress) 
			delete ppCImagXpress;
	}
}

void CStockDlg::OnFileQuit() 
{
	EndDialog(IDOK);
}

void CStockDlg::OnAppAbout() 
{
	pImagXpress->AboutBox();
	
}

VERSION 5.00
Begin VB.Form comobject 
   Caption         =   "ImagXpress 8  COM Object Example"
   ClientHeight    =   6840
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   10755
   LinkTopic       =   "Form1"
   ScaleHeight     =   6840
   ScaleWidth      =   10755
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "comobject.frx":0000
      Left            =   120
      List            =   "comobject.frx":000D
      TabIndex        =   2
      Top             =   120
      Width           =   10335
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete 2nd IX Control"
      Height          =   495
      Left            =   5640
      TabIndex        =   1
      Top             =   1080
      Width           =   2175
   End
   Begin VB.CommandButton cmdCreate 
      Caption         =   "Create 2nd IX Control"
      Height          =   495
      Left            =   2640
      TabIndex        =   0
      Top             =   1080
      Width           =   2175
   End
   Begin VB.Label lblError 
      Height          =   2295
      Left            =   7440
      TabIndex        =   4
      Top             =   3840
      Width           =   2775
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   7560
      TabIndex        =   3
      Top             =   3240
      Width           =   2535
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "comobject"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

' SETUP:
' In VB 5 or VB 6, select Project/References
' Check  "Pegasus ImagXpress v8.0 ActiveX Control
' This sets up the reference for PegasusImagingActiveXImagXpress8Lib

' ImagXpress1 is created once at startup.  This is the simpler case
' because the control is always available as long as the form is loaded,
' and the form takes care of removing the object when it unloades.
 Dim ImagXpress1 As PegasusImagingActiveXImagXpress8.ImagXpress
Attribute ImagXpress1.VB_VarHelpID = -1
' ImagXpress2 gets allocated later, but we're responsible for destroying
' it prior to the form unloading.  If we don't destroy it, then we could leave
' a dangling reference and we'll get a nasty Assertion Error
 Dim ImagXpress2 As PegasusImagingActiveXImagXpress8.ImagXpress
Attribute ImagXpress2.VB_VarHelpID = -1
' Unlock function - required to remove registration dialog when control created dynamically
Private Declare Sub IX_Unlock Lib "PegasusImaging.ActiveX.ImagXpress8.dll" Alias "UnlockControl" (ByVal pw1 As Long, ByVal pw2 As Long, ByVal pw3 As Long, ByVal pw4 As Long)


Dim imgFileName As String
Dim imgParentDir As String

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub



Private Sub mnuFileQuit_Click()
    End
End Sub



' Initialize the ImagXpress1 Control
' Notice that after creating the Object, we must call
' CreateCtlWindow to manually create the control window.
' This is because we've bypassed ActiveX which normally does this for you.
Private Sub Form_Load()
  ' Must unlock the object using YOUR unlock codes!
  
  imgParentDir = App.Path
  
  cmdDelete.Enabled = False
  
  
  '*****place your unlock codes here******
  IX_Unlock 1234, 1234, 1234, 1234
  Set ImagXpress1 = New PegasusImagingActiveXImagXpress8.ImagXpress
  
  '*****place your unlock codes here******
  ImagXpress1.UnlockRuntime 1234, 1234, 1234, 1234
  
      
  ImagXpress1.CreateCtlWindow comobject.hWnd, 10, 170, 200, 200
  ImagXpress1.AutoSize = ISIZE_BestFit
  ImagXpress1.BorderType = BORD_Raised
  
  
  imgFileName = App.Path & "\..\..\..\..\..\..\Common\Images\benefits.tif"
  
  ImagXpress1.FileName = imgFileName
        
   Err = ImagXpress1.ImagError
   PegasusError Err, lblError
    
  Set ImagXpress2 = Nothing
End Sub



' Create an ImagXpress2 Object
Private Sub cmdCreate_Click()
  
  CreateImagXpress ImagXpress2
  
    
  cmdDelete.Enabled = True
  ImagXpress2.AutoSize = ISIZE_BestFit
  ImagXpress2.BorderType = BORD_Raised
  
  
  ImagXpress2.FileName = imgFileName
        
  Err = ImagXpress1.ImagError
  PegasusError Err, lblError
  
   
  
End Sub

' Delete the ImagXpress2 Object
Private Sub cmdDelete_Click()
  DestroyImagXpress ImagXpress2
End Sub


Private Sub Form_Unload(Cancel As Integer)
  DestroyImagXpress ImagXpress2
End Sub


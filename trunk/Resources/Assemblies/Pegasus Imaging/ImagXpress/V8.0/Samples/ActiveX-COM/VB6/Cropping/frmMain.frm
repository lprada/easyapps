VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress ActiveX Sample - Cropping"
   ClientHeight    =   7455
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   12660
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7455
   ScaleWidth      =   12660
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   9240
      TabIndex        =   4
      Top             =   1680
      Width           =   3135
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "frmMain.frx":0000
      Left            =   240
      List            =   "frmMain.frx":000A
      TabIndex        =   2
      Top             =   120
      Width           =   12135
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   5775
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   10186
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   951523450
      ErrInfo         =   1294702143
      Persistence     =   -1  'True
      _cx             =   15266
      _cy             =   10186
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin MSComDlg.CommonDialog dlgFileOpen 
      Left            =   240
      Top             =   1080
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCrop 
      Caption         =   "Crop"
      Height          =   375
      Left            =   3240
      TabIndex        =   0
      Top             =   6840
      Width           =   2775
   End
   Begin VB.Label lblError 
      Height          =   2175
      Left            =   9360
      TabIndex        =   6
      Top             =   4560
      Width           =   3015
   End
   Begin VB.Label lblLasterror 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   9360
      TabIndex        =   5
      Top             =   3840
      Width           =   2775
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   9240
      TabIndex        =   3
      Top             =   1080
      Width           =   3015
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuFileDefault 
         Caption         =   "Open &Default Image"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim bSaveFile As Boolean

Dim imgFileName As String
Dim imgParentDir As String



Private Sub cmdCrop_Click()
    ImagXpress1.CropBorder 0.9, 2
    ImagXpress1.AutoCrop 0.025, 10, 10
    ImagXpress1.SaveFileName = App.Path & "\cropped.tif"
    ImagXpress1.SaveFile
    
     Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub


Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        If bSaveFile = True Then
            
            lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Saving."
            bSaveFile = False
        Else
        
            lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
        End If
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub mnuAbout_Click()
    ImagXpress1.AboutBox
End Sub

Private Sub mnuToolbarShow_Click()
    If ImagXpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    ImagXpress1.ToolbarActivated = Not ImagXpress1.ToolbarActivated
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
    End Sub



Private Sub Form_Load()
    
    ImagXpress1.EventSetEnabled EVENT_PROGRESS, True
    
    ImagXpress1.ScrollBars = SB_Both
    ImagXpress1.SaveTIFFCompression = TIFF_CCITTFAX4
    
    imgFileName = App.Path & "\..\..\..\..\..\..\Common\Images\scan_1_borders.tif"
    imgParentDir = PathFromFile(imgFileName)
    
    LoadFile
End Sub

Private Sub LoadFile()
    ImagXpress1.FileName = imgFileName
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblError
End Sub


Private Function PathFromFile(sFileName As String)
    Dim iLastPos As Integer
    iLastPos = InStrRev(sFileName, "\")
    PathFromFile = Left(sFileName, iLastPos)
End Function

Private Sub mnuFileDefault_Click()
    imgFileName = App.Path & "\..\..\..\..\..\..\Common\Images\scan_1_borders.tif"
    imgParentDir = PathFromFile(imgFileName)
    
    LoadFile
End Sub

Private Sub mnuFileOpen_Click()
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuFileSave_Click()

   Dim tmpFN As String
    tmpFN = PegasusSaveFilePF(imgParentDir, "Tiff File (*.tif)", "tif")
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)

        bSaveFile = True
        ImagXpress1.SaveFileName = imgFileName
        ImagXpress1.SaveFile

        Err = ImagXpress1.ImagError
        PegasusError Err, lblError

    End If
    
End Sub

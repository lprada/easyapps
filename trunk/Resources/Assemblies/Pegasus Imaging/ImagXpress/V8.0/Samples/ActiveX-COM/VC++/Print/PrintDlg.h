// PrintDlg.h : header file
//

#if !defined(AFX_PRINTDLG_H__B0A9FE67_3B8B_11D3_9CFE_00400543FF49__INCLUDED_)
#define AFX_PRINTDLG_H__B0A9FE67_3B8B_11D3_9CFE_00400543FF49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"

/////////////////////////////////////////////////////////////////////////////
// CPrintDlg dialog

class CPrintDlg : public CDialog
{
	// Construction
public:
	CPrintDlg(CWnd* pParent = NULL);	// standard constructor
	
	// this will be the pointer to our IX 8 COM instance
	
	CImagXpress *ppCImagXpress;
	IImagXpressPtr pImagXpress;
	
	
	// Dialog Data
	//{{AFX_DATA(CPrintDlg)
	enum { IDD = IDD_PRINT_DIALOG };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	HICON m_hIcon;
	
	// Generated message map functions
	//{{AFX_MSG(CPrintDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPrint1();
	afx_msg void OnPrint2();
	afx_msg void OnQuit();
	afx_msg void OnDestroy();
	afx_msg void OnFileQuit();
	afx_msg void OnFile();
	afx_msg void OnHelpAbout();
	afx_msg int FindMenuItem(CMenu* Menu, LPCTSTR MenuString);
	afx_msg void OnUpdateToolbarShow(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTDLG_H__B0A9FE67_3B8B_11D3_9CFE_00400543FF49__INCLUDED_)

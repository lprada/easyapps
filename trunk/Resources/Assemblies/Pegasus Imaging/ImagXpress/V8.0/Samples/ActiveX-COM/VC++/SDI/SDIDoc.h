// SDIDoc.h : interface of the CSDIDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDIDOC_H__169785B4_73B2_4939_BEA0_8480AF6DB104__INCLUDED_)
#define AFX_SDIDOC_H__169785B4_73B2_4939_BEA0_8480AF6DB104__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"

class CSDIDoc : public CDocument
{
protected: // create from serialization only
	CSDIDoc();
	DECLARE_DYNCREATE(CSDIDoc)

	CString m_strFileName;

// Attributes
public:	

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSDIDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL SaveModified();
	//}}AFX_VIRTUAL

// Implementation
public:
	CString GetFileName();
	BOOL IsActual();
	virtual ~CSDIDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSDIDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDIDOC_H__169785B4_73B2_4939_BEA0_8480AF6DB104__INCLUDED_)

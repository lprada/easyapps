//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EXIFTags.rc
//
#define IDD_EXIFTAGS_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDR_MNU_MAIN                    130
#define IDC_LBL_DESCRIPTION             1000
#define IDC_BTN_LOADEXIFTAGS            1001
#define IDC_BTN_ADDACOPYRIGHTTAG        1002
#define IDC_BTN_DELETECOPYRIGHTTAG      1003
#define IDC_BTN_MODIFYRESOLUTIONTAGS    1005
#define IDC_BTN_SETTAGS                 1006
#define IDC_LIST_EXIFTAGS               1008
#define IDC_BTN_EXIT                    1009
#define ID_FILE                         32774
#define ID_FILE_EXIT                    32775
#define ID_HELP_ABOUT                   32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

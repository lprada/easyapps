//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ColorProfiles.rc
//
#define IDD_COLORPROFILES_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDR_MNU_MAIN                    129
#define IDC_CHK_ICMENABLED              1000
#define IDC_CHECK2                      1001
#define IDC_CHK_ICMPROOFINGENABLED      1001
#define IDC_BTN_CLEARALLPROFILES        1002
#define IDC_BTN_CLEARALLPROFILES2       1003
#define IDC_BTN_PRINTIMAGE              1003
#define IDC_LBL_RENDERINTENTSETTINGS    1004
#define IDC_CBO_RENDERINTENTSETTINGS    1005
#define IDC_LBL_DESCRIPTION             1006
#define IDC_RCH_INFO                    1007
#define IDC_BUTTON1                     1008
#define ID_FILE_LOADMONITORPROFILE      32771
#define ID_FILE_LOADPRINTERPROFILE      32772
#define ID_FILE_LOADTARGETPROFILE       32773
#define ID_FILE_OPENIMAGE               32774
#define ID_FILE_EXIT                    32775
#define ID_TOOLBAR_SHOW                 32776
#define ID_HELP_ABOUTBOX                32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

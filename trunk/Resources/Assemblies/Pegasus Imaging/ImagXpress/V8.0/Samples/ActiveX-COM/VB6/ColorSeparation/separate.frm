VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form frmSeparate 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "ImagXpress - Color Separation Example"
   ClientHeight    =   8085
   ClientLeft      =   660
   ClientTop       =   1650
   ClientWidth     =   12945
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8085
   ScaleWidth      =   12945
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   1095
      Left            =   6360
      TabIndex        =   21
      Top             =   6240
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   1931
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   507670935
      ErrInfo         =   140366475
      Persistence     =   -1  'True
      _cx             =   5530
      _cy             =   1931
      AutoSize        =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Frame frmjoin 
      Caption         =   "ColorJoin Options"
      Height          =   2055
      Left            =   4440
      TabIndex        =   16
      Top             =   5760
      Width           =   5175
      Begin VB.OptionButton jncmyk 
         Caption         =   "CMYK ColorJoin"
         Height          =   195
         Left            =   360
         TabIndex        =   20
         Top             =   1680
         Width           =   2055
      End
      Begin VB.OptionButton jncmy 
         Caption         =   "CMY ColorJoin"
         Height          =   255
         Left            =   360
         TabIndex        =   19
         Top             =   1200
         Width           =   1935
      End
      Begin VB.OptionButton jnhsl 
         Caption         =   "HSL ColorJoin"
         Height          =   375
         Left            =   360
         TabIndex        =   18
         Top             =   720
         Width           =   1815
      End
      Begin VB.OptionButton jnrgb 
         Caption         =   "RGB ColorJoin"
         Height          =   195
         Left            =   360
         TabIndex        =   17
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   9720
      TabIndex        =   12
      Top             =   1800
      Width           =   3135
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "separate.frx":0000
      Left            =   240
      List            =   "separate.frx":000A
      TabIndex        =   11
      Top             =   120
      Width           =   10935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "RGB"
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   4800
      Width           =   1695
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressPlane3 
      Height          =   1335
      Left            =   6000
      TabIndex        =   9
      Top             =   4320
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2355
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   507670935
      ErrInfo         =   140366475
      Persistence     =   -1  'True
      _cx             =   5318
      _cy             =   2355
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressPlane2 
      Height          =   1335
      Left            =   5640
      TabIndex        =   8
      Top             =   3480
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2355
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   507670935
      ErrInfo         =   140366475
      Persistence     =   -1  'True
      _cx             =   5318
      _cy             =   2355
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   3
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressPlane1 
      Height          =   1455
      Left            =   5400
      TabIndex        =   7
      Top             =   2520
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   2566
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   507670935
      ErrInfo         =   140366475
      Persistence     =   -1  'True
      _cx             =   5106
      _cy             =   2566
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   4
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress XpressPlane0 
      Height          =   1455
      Left            =   5040
      TabIndex        =   6
      Top             =   1800
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   2566
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   507670935
      ErrInfo         =   140366475
      Persistence     =   -1  'True
      _cx             =   5318
      _cy             =   2566
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   5
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress1 
      Height          =   3375
      Left            =   240
      TabIndex        =   5
      Top             =   1320
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   5953
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   507670935
      ErrInfo         =   140366475
      Persistence     =   -1  'True
      _cx             =   7858
      _cy             =   5953
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   177
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      ScrollBars      =   3
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   6
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      ManagePalette   =   0   'False
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdCMYK 
      Caption         =   "CMYK"
      Height          =   495
      Left            =   1800
      TabIndex        =   3
      Top             =   5280
      Width           =   1695
   End
   Begin VB.CommandButton cmdCMY 
      Caption         =   "CMY"
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   5280
      Width           =   1695
   End
   Begin VB.CommandButton cmdHSL 
      Caption         =   "HSL"
      Height          =   495
      Left            =   1800
      TabIndex        =   1
      Top             =   4800
      Width           =   1455
   End
   Begin VB.Label lblLooadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   9720
      TabIndex        =   15
      Top             =   1320
      Width           =   2535
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   375
      Left            =   9720
      TabIndex        =   14
      Top             =   3840
      Width           =   2535
   End
   Begin VB.Label lblError 
      Height          =   2175
      Left            =   9840
      TabIndex        =   13
      Top             =   4440
      Width           =   2775
   End
   Begin VB.Label Label2 
      Caption         =   "Click on a Color Plane."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5040
      TabIndex        =   4
      Top             =   1320
      Width           =   2535
   End
   Begin VB.Label Label1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   8160
      TabIndex        =   0
      Top             =   1560
      Width           =   1335
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
      End
      Begin VB.Menu mnuSpacer 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "&Toolbar"
      Begin VB.Menu mnuToolbarShow 
         Caption         =   "&Show"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "frmSeparate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim Plane(4) As String

'Declare API memory functions for "freeing" the hDIB's
Private Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long

Private Sub cmdCMY_Click()
  ReSetZOrder
  Xpress1.ZOrder  ' Bring to front for 256 color displays
  Xpress1.ColorSeparation 2
  Plane(0) = "Cyan"
  Plane(1) = "Magenta"
  Plane(2) = "Yellow"
  XpressPlane0.hDIB = Xpress1.IPGetColorPlane(0)
  XpressPlane1.hDIB = Xpress1.IPGetColorPlane(1)
  XpressPlane2.hDIB = Xpress1.IPGetColorPlane(2)
  XpressPlane3.FileName = ""
  XpressPlane3.Visible = False
  XpressPlane0_Click
End Sub


Private Sub cmdCMYK_Click()
  ReSetZOrder
  Xpress1.ColorSeparation 3
  Plane(0) = "Cyan"
  Plane(1) = "Magenta"
  Plane(2) = "Yellow"
  Plane(3) = "Black"
  XpressPlane3.Visible = True
  XpressPlane0.hDIB = Xpress1.IPGetColorPlane(0)
  XpressPlane1.hDIB = Xpress1.IPGetColorPlane(1)
  XpressPlane2.hDIB = Xpress1.IPGetColorPlane(2)
  XpressPlane3.hDIB = Xpress1.IPGetColorPlane(3)
  XpressPlane0_Click
End Sub

Private Sub cmdHSL_Click()
  ReSetZOrder
  Xpress1.ColorSeparation 1
  Plane(0) = "Hue"
  Plane(1) = "Saturation"
  Plane(2) = "Luminance"
  XpressPlane0.hDIB = Xpress1.IPGetColorPlane(0)
  XpressPlane1.hDIB = Xpress1.IPGetColorPlane(1)
  XpressPlane2.hDIB = Xpress1.IPGetColorPlane(2)
  XpressPlane3.FileName = ""
  XpressPlane3.Visible = False
  XpressPlane0_Click
End Sub

Private Sub cmdQuit_Click()
  End
End Sub

Private Sub ReSetZOrder()
  XpressPlane3.ZOrder
  XpressPlane2.ZOrder
  XpressPlane1.ZOrder
  XpressPlane0.ZOrder
  Xpress1.ZOrder  ' Bring to front for 256 color displays
End Sub

Private Sub Command1_Click()
      ReSetZOrder
  Xpress1.ColorSeparation 0
  Plane(0) = "Red"
  Plane(1) = "Green"
  Plane(2) = "Blue"
  XpressPlane0.hDIB = Xpress1.IPGetColorPlane(0)
  XpressPlane1.hDIB = Xpress1.IPGetColorPlane(1)
  XpressPlane2.hDIB = Xpress1.IPGetColorPlane(2)
  XpressPlane3.FileName = ""
  XpressPlane3.Visible = False
  XpressPlane0_Click
End Sub

Private Sub LoadFile()
   Xpress1.FileName = imgFileName
        
   Err = Xpress1.ImagError
   PegasusError Err, lblError
   
End Sub

Private Sub Command2_Click()
    
End Sub

Private Sub Form_Load()
   
   Xpress1.EventSetEnabled EVENT_PROGRESS, True
  imgParentDir = App.Path

  imgFileName = imgParentDir + "\..\..\..\..\..\..\Common\Images\bird.jpg"
  
  LoadFile
End Sub

Private Sub Form_Unload(Cancel As Integer)
   'The hDIB's need to be "freed" after using the ColorJoin method
   GlobalFree XpressPlane0.hDIB
   GlobalFree XpressPlane1.hDIB
   GlobalFree XpressPlane2.hDIB
   GlobalFree XpressPlane3.hDIB
End Sub

Private Sub jncmy_Click()
    'please see GlobalFree in Form_Close
    ImagXpress1.ColorJoin 2, XpressPlane0.hDIB, XpressPlane1.hDIB, XpressPlane2.hDIB, XpressPlane3.hDIB
End Sub

Private Sub jncmyk_Click()
    'please see GlobalFree in Form_Close
    ImagXpress1.ColorJoin 3, XpressPlane0.hDIB, XpressPlane1.hDIB, XpressPlane2.hDIB, XpressPlane3.hDIB
End Sub

Private Sub jnhsl_Click()
    'please see GlobalFree in Form_Close
    ImagXpress1.ColorJoin 1, XpressPlane0.hDIB, XpressPlane1.hDIB, XpressPlane2.hDIB, XpressPlane3.hDIB
End Sub

Private Sub jnrgb_Click()
    'please see GlobalFree in Form_Close
   ImagXpress1.ColorJoin 0, XpressPlane0.hDIB, XpressPlane1.hDIB, XpressPlane2.hDIB, XpressPlane3.hDIB
End Sub

Private Sub XpressPlane0_Click()
  XpressPlane0.ZOrder
  Label1.Caption = Plane(0)
End Sub

Private Sub XpressPlane1_Click()
  XpressPlane1.ZOrder
  Label1.Caption = Plane(1)
End Sub

Private Sub XpressPlane2_Click()
  XpressPlane2.ZOrder
  Label1.Caption = Plane(2)
End Sub

Private Sub XpressPlane3_Click()
  XpressPlane3.ZOrder
  Label1.Caption = Plane(3)
End Sub

Private Sub mnuAbout_Click()
    Xpress1.AboutBox

End Sub

Private Sub Xpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub


Private Sub mnuFileOpen_Click()
     Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
        LoadFile
    End If
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuToolbarShow_Click()
    
    If Xpress1.ToolbarActivated Then
        mnuToolbarShow.Caption = "&Show"
    Else
        mnuToolbarShow.Caption = "&Hide"
    End If
    Xpress1.ToolbarActivated = Not Xpress1.ToolbarActivated
    
    Err = Xpress1.ImagError
    PegasusError Err, lblError
    
         
End Sub



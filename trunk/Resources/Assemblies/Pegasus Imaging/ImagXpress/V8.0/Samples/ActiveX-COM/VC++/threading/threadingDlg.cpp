// threadingDlg.cpp : implementation file
//
/************************************************************************************
*                                                                                  *
*  Copyright (C) 1996-2003 Pegasus Imaging Corporation                             *
*                                                                                  *
*  This source code is provided to you as sample source code.  Pegasus Imaging     *
*  Corporation grants you the right to use this source code in any way you         *
*  wish. Pegasus Imaging Corporation assumes no responsibility and explicitly      *
*  disavows any responsibility for any and every use you make of this source       *
*  code.                                                                           *
*                                                                                  *
************************************************************************************
*                                                                                  *
*  Name:             Threading Sample                                              *
*  Description:      This sample demonstrates the asynchronous feature of          *
*                    ImagXpress. It shows how 2 or more images can be loaded       *
*                    and perform image processing simultaneously. It also shows    *
*                    the timer feature and the use of multiple buffers.            *
*  Written By:       Pegasus Imaging Corporation                                   *
************************************************************************************/

#include "stdafx.h"
#include "threading.h"
#include "threadingDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Include ImagXpress Initialization functions
#include "..\Include\ix8_open.cpp"

#define TIMER_INTERVAL 100
#define IMAGE1_WIDTH  408
#define IMAGE1_HEIGHT  280
#define IMAGE2_WIDTH  320
#define IMAGE2_HEIGHT  240

int viewIndex;

/////////////////////////////////////////////////////////////////////////////
// CThreadingDlg dialog

CThreadingDlg::CThreadingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CThreadingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CThreadingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CThreadingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CThreadingDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CThreadingDlg, CDialog)
	//{{AFX_MSG_MAP(CThreadingDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(IDC_ASYNCPRIORITY, OnSelchangeAsyncpriority)
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_MAXTHREADS, OnChangeMaxthreads)
	ON_BN_CLICKED(IDC_ASYNC, OnAsync)
	ON_BN_CLICKED(IDC_LOADRESIZE, OnLoadresize)
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
	ON_COMMAND(ID_ABOUT_ABOUTBOX, OnAboutAboutbox)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThreadingDlg message handlers

BOOL CThreadingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// ImagXpress initialization
	HINSTANCE hDLL = IX_Open();
	
	// Create an ImagXpress Base object
	// this object will be used to load and resize two images
	ppCIXBase = new CImagXpress((DWORD)this, 0, (long)m_hWnd, 0, 0, 0, 0);
	pIXBase = ppCIXBase->pImagXpress;

	// Create 6 ImagXpress View objects and 6 labels
	// these objects will be used to display the images being loaded/resized
	int x=10, y=10;
	for(int i=0; i < 6; i++)
	{
		pLabel[ i ] = new CStatic();
		pLabel[ i ]->Create("", WS_CHILD|WS_VISIBLE, CRect(x,y,x+150,y+15), this );
		
		ppCIXView[ i ] = new CImagXpress((DWORD)this, i+1, (long)m_hWnd, x, y+15, 150, 150);
		pIXView[ i ] = ppCIXView[ i ]->pImagXpress;			

		x+=160;
		if ( x > 330 ) 
		{
			x = 10;
			y += 175;
		}
	}
	
	// ImagXpress uninitialize
	IX_Close(hDLL);

	// Set our 2 event handlers for the Base object
	ppCIXBase->SetImageStatusChangedEvent(ImageStatusChanged);
	ppCIXBase->SetTimerTickEvent(TimerTick);
	
	pIXBase->Async = true;
	pIXBase->AutoAssignID = false;
	pIXBase->AsyncPriority = ASYNC_Normal;
	pIXBase->AsyncCancelOnClose = true;
	pIXBase->AsyncMaxThreads = 2;

	CComboBox *pAsyncPriority = (CComboBox*)GetDlgItem(IDC_ASYNCPRIORITY);	
	pAsyncPriority->SetItemData(0, ASYNC_Normal);
	pAsyncPriority->SetItemData(1, ASYNC_Low);
	pAsyncPriority->SetItemData(2, ASYNC_High);
	pAsyncPriority->SetCurSel(0);

	CButton *pAsyncButton = (CButton*)GetDlgItem(IDC_ASYNC);
	pAsyncButton->SetCheck( BST_CHECKED );

	CSpinButtonCtrl *pMaxThreads = (CSpinButtonCtrl*)GetDlgItem(IDC_SPIN1);
	pMaxThreads->SetRange( 0, 3200 );
	pMaxThreads->SetPos( 2 );
	
	pIXBase->Timer = TIMER_INTERVAL;
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CThreadingDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CThreadingDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

HRESULT CThreadingDlg::ImageStatusChanged(DWORD classPtr, DWORD objID, short ID, enumIPEffect eOPID, long lStatus)
{
	CThreadingDlg *pDialog = (CThreadingDlg*)classPtr;
	CString sStatus, sHelper;	

	sStatus.Format("Process ID %d ", ID);

	if( lStatus & BUFSTATUS_EMPTY ) sStatus += "Empty";

	if( lStatus & BUFSTATUS_DEFINED ) sStatus += "Defined";

	if( lStatus & BUFSTATUS_OPENED ) sStatus += ", Opened";

	if( lStatus & BUFSTATUS_DECODED ) sStatus += ", Decoded";

	if( lStatus & BUFSTATUS_DECODING ) sStatus += ", Decoding";

	if( lStatus & BUFSTATUS_IMAGEPROCESSING ) sStatus += ", Image-Processing";

	if( lStatus & BUFSTATUS_IMAGEPROCESSED )
	{
		// The process has finished. Find the timer for this process id
		// in the array of timers, set it to -1 to flag it as finished.
		for ( int i=0; i < pDialog->timer_array.GetSize(); i++ )
		{			
			if ( pDialog->timer_array[i].ProcessID == ID )
			{
				sStatus += ", Image-Processed";
				sHelper.Format( " ( took %d seconds )", pDialog->timer_array[i].Timer / 1000 );
				sStatus += sHelper;
				pDialog->timer_array[i].Timer = -1; // flag it as finished
				break;
			}
		}		
	}

	if( lStatus & BUFSTATUS_SAVING ) sStatus += ", Saving";

	if( lStatus & BUFSTATUS_SAVED ) sStatus += ", Saved";

	if( lStatus & BUFSTATUS_WAIT ) sStatus += ", Wait";

	if( lStatus & BUFSTATUS_CANCELED ) sStatus += ", Canceled";

	if( lStatus & BUFSTATUS_ERROR ) sStatus += ", Error";

	CListBox *pList = (CListBox*)pDialog->GetDlgItem(IDC_LIST1);
	pList->AddString( sStatus );
	pList->SetTopIndex( pList->GetCount()-1 );

	return S_OK;
}

HRESULT CThreadingDlg::TimerTick(DWORD classPtr, DWORD objID)
{
	CThreadingDlg *pDialog = (CThreadingDlg*)classPtr;
	
	for ( int i=0; i < pDialog->timer_array.GetSize(); i++ )
	{
		// a timer set at -1 will not be incremented. -1 indicates
		// the timer has finished.
		if ( pDialog->timer_array[i].Timer >= 0 )
		{
			pDialog->timer_array[i].Timer += TIMER_INTERVAL;
		}
	}
	
	return S_OK;
}

void CThreadingDlg::OnAsync() 
{
	CButton *pAsyncButton = (CButton*)GetDlgItem(IDC_ASYNC);

	if( pIXBase->Async )
	{
		pIXBase->Async = false;
		pAsyncButton->SetCheck( BST_UNCHECKED );
	}
	else
	{
		pIXBase->Async = true;
		pAsyncButton->SetCheck( BST_CHECKED );
	}
}

void CThreadingDlg::OnSelchangeAsyncpriority() 
{	
	CComboBox *pAsyncPriority = (CComboBox*)GetDlgItem(IDC_ASYNCPRIORITY);
	int selectedIndex = pAsyncPriority->GetCurSel();
	int selectedValue = pAsyncPriority->GetItemData( selectedIndex );
	pIXBase->AsyncPriority = enumAsyncPriority(selectedValue);
}

void DoEvents()
{ 
	MSG msg; 
	while(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
	{ 
		TranslateMessage(&msg); 
		DispatchMessage(&msg); 
	}
}

void CThreadingDlg::OnDestroy() 
{
	// Delete our ImagXpress Objects

	// when using Async, make sure to turn off Async and wait for IX 
	// to become idle before destroying!
	pIXBase->Async = false;
	while ( ! pIXBase->Idle )
		DoEvents();

	pIXBase = NULL;
	delete ppCIXBase;		

	for(int i=0; i < 6; i++)
	{
		pIXView[ i ] = NULL;
		delete ppCIXView[ i ];
	}
	
	CDialog::OnDestroy();		
}

void CThreadingDlg::OnChangeMaxthreads() 
{
	CString sText;
	CEdit *pEdit = (CEdit*)GetDlgItem(IDC_MAXTHREADS);
	pEdit->GetWindowText(sText);
	
	// spin button sets edit window content before OnInitDialog, 
	// so check if IX is created
	if (pIXBase) 
		pIXBase->AsyncMaxThreads = atoi(sText);	
}



void CThreadingDlg::OnLoadresize() 
{	
	CString sWindowTitle;	

	bool bAllTimersDone = true;
	for(int i=0; i < timer_array.GetSize(); i++ )
	{
		if ( timer_array[i].Timer != -1 ) bAllTimersDone = false;
	}
	// if all timers have finished, empty the array
	if ( bAllTimersDone ) 
	{
		for( i=0; i < timer_array.GetSize(); i++ )
		{
			pIXBase->CloseImage( timer_array[i].ProcessID );
		}

		timer_array.RemoveAll();
		for( i = 0; i < 6; i++ )
		{
			pLabel[ i ]->SetWindowText( "" ); // clear all the labels
			pIXView[ i ]->ViewImageID = 0;// clear all the IX View objects
			viewIndex = 0;
		}
	}
	
	// if more than 6 processes running at once, the 7th will be
	// displayed in Viewer 1	
	if (viewIndex > 5) viewIndex = 0;	
	
	pIXBase->ProcessImageID = pIXBase->GetUniqueImageID();
		
	sWindowTitle.Format("Process ID - %d", pIXBase->ProcessImageID);	
	pLabel[ viewIndex ]->SetWindowText( sWindowTitle );
	pIXView[ viewIndex ]->ViewImageID = pIXBase->ProcessImageID;
	
	timer_array.Add( IX_TIMER(pIXBase->ProcessImageID, 0) );
	pIXBase->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\window.jpg";	
	pIXBase->ResizeEx( RT_PICQuality, IMAGE1_WIDTH * 8, IMAGE1_HEIGHT * 8 );

	viewIndex++;
	
	if (viewIndex > 5) viewIndex = 0;
	
	pIXBase->ProcessImageID = pIXBase->GetUniqueImageID();	
	
	
	sWindowTitle.Format("Process ID - %d", pIXBase->ProcessImageID);	
	pLabel[ viewIndex ]->SetWindowText( sWindowTitle );
	pIXView[ viewIndex ]->ViewImageID = pIXBase->ProcessImageID;
	
	timer_array.Add( IX_TIMER(pIXBase->ProcessImageID, 0) );	
	pIXBase->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg";
	pIXBase->ResizeEx( RT_PICQuality, IMAGE2_WIDTH * 8, IMAGE2_HEIGHT * 8 );

	viewIndex++;

}

void CThreadingDlg::OnFileQuit() 
{
		EndDialog(IDOK);
	
}

void CThreadingDlg::OnAboutAboutbox() 
{
	pIXBase->AboutBox();
	
}

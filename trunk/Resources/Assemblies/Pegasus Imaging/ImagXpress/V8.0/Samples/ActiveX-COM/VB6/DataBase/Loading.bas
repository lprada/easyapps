Attribute VB_Name = "Loading"
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Const BLOCK_SIZE = 16384
Const conchunksize = 100
Dim lngoffset As Long
Dim lnglogosize As Long
Dim varlogo As Variant
Dim varchunk As Variant

      Sub FileToBlob(ByVal FName As String, fld As ADODB.Field, _
                     Optional Threshold As Long = 1048576)
      '
      ' Assumes file exists
      ' Assumes calling routine does the UPDATE
      ' File cannot exceed approx. 2Gb in size
      '
      Dim F As Long, data() As Byte, filesize As Long
        F = FreeFile
        Open FName For Binary As #F
        filesize = LOF(F)
        Select Case fld.Type
          Case adLongVarBinary
            If filesize > Threshold Then
              ReadToBinary F, fld, filesize
            Else
              data = InputB(filesize, F)
              fld.Value = data
            End If
          Case adLongVarChar, adLongVarWChar
            If filesize > Threshold Then
              ReadToText F, fld, filesize
            Else
              fld.Value = Input(filesize, F)
            End If
        End Select
        Close #F
      End Sub

      Sub ReadToBinary(ByVal F As Long, fld As ADODB.Field, _
                       ByVal filesize As Long)
                       
      Dim data() As Byte, BytesRead As Long
      
        Do While filesize <> BytesRead
          If filesize - BytesRead < BLOCK_SIZE Then
            data = InputB(filesize - BytesRead, F)
            BytesRead = filesize
          Else
            data = InputB(BLOCK_SIZE, F)
            BytesRead = BytesRead + BLOCK_SIZE
          End If
          fld.AppendChunk data
        
        Loop
      End Sub

Sub ReadToText(ByVal F As Long, fld As ADODB.Field, _
                     ByVal filesize As Long)
      Dim data As String, CharsRead As Long
        Do While filesize <> CharsRead
          If filesize - CharsRead < BLOCK_SIZE Then
            data = Input(filesize - CharsRead, F)
            CharsRead = filesize
          Else
            data = Input(BLOCK_SIZE, F)
            CharsRead = CharsRead + BLOCK_SIZE
          End If
          fld.AppendChunk data
          
        Loop
      End Sub


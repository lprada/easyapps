unit threading;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, AxCtrls,Contnrs, OleCtrls,PegasusImagingActiveXImagXpress8_TLB, Menus;

type
  TForm1 = class(TForm)
    AsyncPriority: TComboBox;
    Timer1: TTimer;
    menu: TMainMenu;
    LoadResize: TButton;
    Asyncbutton: TCheckBox;
    ListBox1: TListBox;
    Maxthreads: TEdit;
    File1: TMenuItem;
    Quit1: TMenuItem;
    About1: TMenuItem;
    IxBase: TImagXpress;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure AsyncPriorityChange(Sender: TObject);
    procedure MaxThreadsChange(Sender: TObject);



    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure AsyncbuttonClick(Sender: TObject);
    procedure IXBaseImageStatusChanged(ASender: TObject; ID: Smallint;
      eOPID: TOleEnum; lStatus: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Quit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
  private

  ViewIndex: Integer;
     IXViewer: array[0..5] of TImagXpress;
     ViewerLabel: array[0..5] of TLabel;
     timers: TObjectList;
    { Private declarations }
  public
    { Public declarations }
  end;


  type
  IX_TIMER = class
    Timer : Integer;
    ProcessID : Integer;
    constructor Create( ProcessID:Integer; Timer:Integer );
  end;

var
  Form1: TForm1;

  const
  TIMER_INTERVAL = 100;
  IMAGE1_WIDTH = 408;
  IMAGE1_HEIGHT = 280;
  IMAGE2_WIDTH = 320;
  IMAGE2_HEIGHT = 240;

  LABEL_WIDTH = 150;
  LABEL_HEIGHT = 15;
  VIEWER_WIDTH = 150;
  VIEWER_HEIGHT = 150;
  VIEWER_SPACING = 10;

implementation

constructor IX_TIMER.Create( ProcessID:Integer; Timer:Integer );
begin
   Self.ProcessID := ProcessID;
   Self.Timer := Timer;
end;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  x, y, i: Integer;
begin

// the following code creates a 2 row 3 column grid of ImagXpress controls
// and label controls above each.
x := VIEWER_SPACING; y := VIEWER_SPACING;
for i := 0 to 5 do
begin
  // create a label for each ImagXpress view object
  ViewerLabel[i] := TLabel.Create(Self);

  ViewerLabel[i].Parent :=  Self;
  ViewerLabel[i].SetBounds( x,
                        y,
                        LABEL_WIDTH,
                        LABEL_HEIGHT );

  // create ImagXpress view objects
  IXViewer[i] := TImagXpress.Create(Self);
  IXViewer[i].UnlockRuntime(1234,1234,1234,1234);
  IXViewer[i].CreateCtlWindow(Self.Handle,
                                x,
                                y+LABEL_HEIGHT,
                                VIEWER_WIDTH,
                                VIEWER_HEIGHT);

  x := x + VIEWER_WIDTH + VIEWER_SPACING;
  if x > (( VIEWER_WIDTH + VIEWER_SPACING ) * 2 ) + VIEWER_SPACING then
  begin
    y := y + VIEWER_HEIGHT + LABEL_HEIGHT + VIEWER_SPACING;
    x := VIEWER_SPACING;
  end;

end;
timers := TObjectList.Create();
ViewIndex := 0;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //******this is necessary to wait until close the threads *****
    IXBase.Async := false;
    While Not IXBase.Idle do
        Application.ProcessMessages;
end;

procedure TForm1.IXBaseImageStatusChanged(ASender: TObject; ID: Smallint;
  eOPID: TOleEnum; lStatus: Integer);
  var
  S: String;
  i: Integer;
begin

If lStatus = BUFSTATUS_EMPTY Then
    S := 'Empty';
 If bool(lStatus and BUFSTATUS_DEFINED) Then
    S := 'Defined';
 If bool(lStatus and BUFSTATUS_OPENED) Then
    S := S + ', Opened';
 If bool(lStatus and BUFSTATUS_DECODED) Then
    S := S + ', Decoded';
 If bool(lStatus and BUFSTATUS_DECODING) Then
    S := S + ', Decoding';
 If bool(lStatus and BUFSTATUS_IMAGEPROCESSING) Then
    S := S + ', Image-Processing';
 // if this process is finished, find the timer for this
 // process and display how much time elapsed for this process
 If bool(lStatus and BUFSTATUS_IMAGEPROCESSED) Then
   begin
      for i:= 0 to timers.Count-1 do
      begin
        if IX_TIMER(timers.Items[i]).ProcessID = ID then
        begin
          S := S + Format(', Image-Processed ( took %d seconds )', [IX_TIMER(timers.Items[i]).Timer div 1000] );
          IX_TIMER(timers.Items[i]).Timer := -1; // flag it as finished
        end;
      end;
   end;
 If bool(lStatus and BUFSTATUS_SAVING) Then
    S := S + ', Saving';
 If bool(lStatus and BUFSTATUS_SAVED) Then
    S := S + ', Saved';
 If bool(lStatus and BUFSTATUS_WAIT) Then
    S := S + ', Wait';
 If bool(lStatus and BUFSTATUS_CANCELED) Then
    S := S + ', Canceled';
 If bool(lStatus and BUFSTATUS_ERROR) Then
    S := S + ', Error';
 // add the status string to the listbox
 ListBox1.Items.Add( 'Image ' + IntToStr(ID) + '  ' + S );

end;

procedure TForm1.FormActivate(Sender: TObject);
begin
IXBase.AsyncPriority := ASYNC_Normal;
IXBase.AsyncCancelOnClose := True;
IXBase.AsyncMaxThreads := 2;

Timer1.Interval := TIMER_INTERVAL;
Asyncbutton.Checked := True;
MaxThreads.Text := '2';
end;

procedure TForm1.AsyncbuttonClick(Sender: TObject);
begin
      IXBase.Async := not IXBase.Async;
end;

procedure TForm1.AsyncPriorityChange(Sender: TObject);
begin
case AsyncPriority.ItemIndex of
  0: IXBase.AsyncPriority := ASYNC_Normal;
  1: IXBase.AsyncPriority := ASYNC_Low;
  2: IXBase.AsyncPriority := ASYNC_High;
end

end;

procedure TForm1.MaxThreadsChange(Sender: TObject);
var
  nThreads : Integer;
begin
  nThreads := abs( StrToInt( MaxThreads.Text ) );
  IXBase.AsyncMaxThreads := nThreads;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
  var
   i : Integer;
begin
// go through the timers collection and find timers that are
// not -1, if so then increment

for i := 0 to timers.Count-1 do
   if IX_TIMER(timers.Items[i]).Timer >= 0 then
      IX_TIMER(timers.Items[i]).Timer := IX_TIMER(timers.Items[i]).Timer + TIMER_INTERVAL;
end;


procedure TForm1.Button1Click(Sender: TObject);

var
  bAllTimersDone : Boolean;
  i : Integer;
begin
  bAllTimersDone := True;
  for i := 0 to timers.Count-1 do
  begin
        if IX_TIMER(timers.Items[i]).Timer <> -1 Then bAllTimersDone := False;
  end;
  // if all timers are done, this means all current processes
  // are finished, so start over by clearing all the labels and IX controls
  if bAllTimersDone = True then
  begin
        for i := 0 to timers.Count-1 do
        begin
                IXBase.CloseImage( IX_TIMER(timers.Items[i]).ProcessID );
        end;
        timers.Clear();
        ViewIndex := 0;
        for i:= 0 to 5 do
        begin
                ViewerLabel[i].Caption := '';  //clear the labels
                IXViewer[i].ViewImageID := 0;  //clear the IX view objects
        end;
  end;

  // load up and resize first image
  if ViewIndex > 5 then ViewIndex := 0;

  IXBase.ProcessImageID := IXBase.GetUniqueImageID();
  ViewerLabel[ViewIndex].Caption :=  Format('Process ID - %d', [IXBase.ProcessImageID] );
  IXViewer[ViewIndex].ViewImageID := IXBase.ProcessImageID;

  timers.Add( IX_TIMER.Create( IXBase.ProcessImageID, 0 ) );
  IXBase.FileName := '..\..\..\..\..\..\Common\Images\window.jpg';


  IXBase.ResizeEx( RT_PICQuality, IMAGE1_WIDTH * 8, IMAGE1_HEIGHT * 8 );

  ViewIndex := ViewIndex + 1;

  // load up and resize the second image
  if ViewIndex > 5 then ViewIndex := 0;

  IXBase.ProcessImageID := IXBase.GetUniqueImageID();
  ViewerLabel[ViewIndex].Caption :=  Format('Process ID - %d', [IXBase.ProcessImageID] );
  IXViewer[ViewIndex].ViewImageID := IXBase.ProcessImageID;

  timers.Add( IX_TIMER.Create( IXBase.ProcessImageID, 0 ) );
  IXBase.FileName := '..\..\..\..\..\..\Common\Images\Vermont.jpg';

 IXBase.ResizeEx( RT_PICQuality, IMAGE2_WIDTH * 8, IMAGE2_HEIGHT * 8 );

  ViewIndex := ViewIndex + 1;

end;


procedure TForm1.Quit1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.About1Click(Sender: TObject);
begin
  IXBase.AboutBox;
end;

end.



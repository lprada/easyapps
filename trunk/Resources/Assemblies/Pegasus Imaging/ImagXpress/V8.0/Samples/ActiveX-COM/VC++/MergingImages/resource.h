//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MergingImages.rc
//
#define IDD_MERGINGIMAGES_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDC_QUIT                        1000
#define IDC_MERGEIMAGES                 1001
#define ID_FILE_EXIT                    32771
#define ID_FILE                         32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

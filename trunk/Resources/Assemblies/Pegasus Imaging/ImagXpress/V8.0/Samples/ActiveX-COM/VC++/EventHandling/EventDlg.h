// EventDlg.h : header file
//

#if !defined(AFX_EVENTDLG_H__CEEB89E7_40D9_11D3_9CFE_00400543FF49__INCLUDED_)
#define AFX_EVENTDLG_H__CEEB89E7_40D9_11D3_9CFE_00400543FF49__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

using namespace PegasusImagingActiveXImagXpress8;

#include "..\Include\ImagXpress8Events.h"


/////////////////////////////////////////////////////////////////////////////
// CEventDlg dialog

class CEventDlg : public CDialog //, CImagXpressEventHandler
{

//   Event Handlers
static HRESULT Progress(DWORD instancePtr,
						DWORD objID,
						long ImageID, 
						long OperationID, 
						long BytesProcessed, 
						long TotalBytes, 
						long PctDone, 
						VARIANT_BOOL bDone, 
						VARIANT_BOOL bAsync, 
						long Error); 
static HRESULT Notify(DWORD instancePtr, DWORD objID); 
static HRESULT MouseMove(DWORD instancePtr, DWORD objID, short Button, short Shift, OLE_XPOS_PIXELS x, OLE_YPOS_PIXELS y);
//static HRESULT MouseMove2(DWORD instancePtr, DWORD objID, short Button, short Shift, OLE_XPOS_PIXELS x, OLE_YPOS_PIXELS y);
static HRESULT Click(DWORD instancePtr, DWORD objID);
static HRESULT DblClick(DWORD instancePtr, DWORD objID);

// Construction
public:

   CEventDlg(CWnd* pParent = NULL);	// standard constructor

  // this will be the pointer to our COM instance 
  // CImagXpress pCImagXpress;

  CImagXpress *ppCImagXpress1;
  IImagXpressPtr pImagXpress1;
  CImagXpress *ppCImagXpress2;
  IImagXpressPtr pImagXpress2;

  
  // Dialog Data
	//{{AFX_DATA(CEventDlg)
	enum { IDD = IDD_EVENT_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEventDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CEventDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnQuit();
	afx_msg void OnDestroy();
	afx_msg void OnFileQuit();
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVENTDLG_H__CEEB89E7_40D9_11D3_9CFE_00400543FF49__INCLUDED_)

Attribute VB_Name = "PegasusFile"
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Function GetCurrentDirectory Lib "kernel32" Alias "GetCurrentDirectoryA" (ByVal nSize As Long, ByVal lpBuffer As String) As Long
Const strCurrentDir As String = "..\..\..\..\..\..\Common\Images\"
'A public constant for the default open file filter, to override on non-filter funcs, use a filter func with "" as the filter, etc...
Const strDefaultImageFilter As String = "All ImagXpress Supported File Types" + vbNullChar + _
            "*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif;*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig2;*.ico;*.rle;*.lzw;*.wbmp" + _
            vbNullChar + "Windows Bitmap (*.BMP)" + vbNullChar + "*.bmp" + vbNullChar + "CALS (*.CAL)" + vbNullChar + "*.cal" + vbNullChar + "Windows Device Independent Bitmap(*.DIB)" + vbNullChar + "*.dib" + _
            vbNullChar + "MO:DCA (*.DCA & *.MOD)" + vbNullChar + "*.dca;*.mod" + vbNullChar + "Zsoft Multiple Page (*.DCX)" + vbNullChar + "*.dcx" + vbNullChar + "CompuServe GIF (*.GIF)" + vbNullChar + "*.gif" + _
            vbNullChar + "JPEG 2000 (*.JP2)" + vbNullChar + "*.jp2" + vbNullChar + "JPEG LS (*.JLS)" + vbNullChar + "*.jls" + vbNullChar + "JFIF Compliant JPEG (*.JPG & *.JIF)" + vbNullChar + "*.jpg;*.jif" + _
            vbNullChar + "Lossless JPEG (*.LJP)" + vbNullChar + "*.ljp" + vbNullChar + "Portable Bitmap (*.PBM)" + vbNullChar + "*.pbm" + vbNullChar + "Zsoft PaintBrush (*.PCX)" + vbNullChar + "*.pcx" + _
            vbNullChar + "Portable Graymap (*.PGM)" + vbNullChar + "*.pgm" + vbNullChar + "Pegasus PIC or Enhanced PIC (*.PIC)" + vbNullChar + "*.pic" + vbNullChar + "Portable Network Graphics (*.PNG)" + _
            vbNullChar + "*.png" + vbNullChar + "Portable Pixmap (*.PPM)" + vbNullChar + "*.ppm" + vbNullChar + "Tagged Image Format (*.TIFF)" + vbNullChar + "*.tif;*.tiff" + vbNullChar + "Truevision TARGA (*.TGA)" + _
            vbNullChar + "*.tga" + vbNullChar + "WSQ Fingerprint File (*.WSQ)" + vbNullChar + "*.wsq" + vbNullChar + "JBIG2 File (*.JB2)" + vbNullChar + "*.jb2" + vbNullChar + "All Files (*.*)" + vbNullChar + "*.*" + vbNullChar + vbNullChar

Declare Function GetOpenFileName Lib "COMDLG32.DLL" Alias "GetOpenFileNameA" (pOpenfilename As OPENFILENAME) As Long
Declare Function GetSaveFileName Lib "COMDLG32.DLL" Alias "GetSaveFileNameA" (pOpenfilename As OPENFILENAME) As Long

Public Type OPENFILENAME
   lStructSize As Long
   hwndOwner As Long
   hInstance As Long
   lpstrFilter As String
   lpstrCustomFilter As String
   nMaxCustFilter As Long
   nFilterIndex As Long
   lpstrFile As String
   nMaxFile As Long
   lpstrFileTitle As String
   nMaxFileTitle As Long
   lpstrInitialDir As String
   lpstrTitle As String
   flags As Long
   nFileOffset As Integer
   nFileExtension As Integer
   lpstrDefExt As String
   lCustData As Long
   lpfnHook As Long
   lpTemplateName As String
End Type
Public Sub PegasusError(ByRef errNo As Integer, ByRef errControl As Label)
    If errNo <> 0 Then
        errControl.Caption = "Error number " + Str(errNo) + " occured." + Chr(10) + "Error Description: " + GetErrorDesc(errNo)
    Else
        errControl.Caption = ""
    End If
        
End Sub

Public Sub PegasusSystemError(errNo As Object, ByRef errControl As Label)
    If errNo <> 0 Then
        errControl.Caption = errNo.Number + Chr(10) + errNo.Description
    End If
End Sub


Public Function PathFromFile(sFileName As String)
    Dim iLastPos As Integer
    iLastPos = InStrRev(sFileName, "\")
    PathFromFile = Left(sFileName, iLastPos)
End Function

Public Function PegasusOpenFileHPF(hParentWnd As Long, ByRef sPath As String, sFilter As String)
    Dim oOpenFileBox As OPENFILENAME
    Dim sFileName As String * 520
    Dim lRet As Long

    sFileName = Chr(0)
    oOpenFileBox.lStructSize = Len(oOpenFileBox)
    oOpenFileBox.hwndOwner = hParentWnd
    oOpenFileBox.lpstrInitialDir = strCurrentDir
 
    oOpenFileBox.lpstrTitle = "Open File"
    oOpenFileBox.lpstrFilter = sFilter
    oOpenFileBox.lpstrFile = sFileName
    oOpenFileBox.nMaxFile = 520
    lRet = GetOpenFileName(oOpenFileBox)
    
    If lRet = 0 Then
        PegasusOpenFileHPF = ""
    Else
        sPath = PathFromFile(oOpenFileBox.lpstrFile)
        PegasusOpenFileHPF = oOpenFileBox.lpstrFile
    End If
End Function

Public Function PegasusOpenFileHP(hParentWnd As Long, ByRef sPath As String)
    Dim oOpenFileBox As OPENFILENAME
    Dim sFileName As String * 520
    Dim lRet As Long

    sFileName = Chr(0)
    oOpenFileBox.lStructSize = Len(oOpenFileBox)
    oOpenFileBox.hwndOwner = hParentWnd
    oOpenFileBox.lpstrInitialDir = strCurrentDir
    oOpenFileBox.lpstrTitle = "Open File"
    oOpenFileBox.lpstrFilter = strDefaultImageFilter
    oOpenFileBox.lpstrFile = sFileName
    oOpenFileBox.nMaxFile = 520
    lRet = GetOpenFileName(oOpenFileBox)
    
    If lRet = 0 Then
        PegasusOpenFileHP = ""
    Else
        sPath = PathFromFile(oOpenFileBox.lpstrFile)
        PegasusOpenFileHP = oOpenFileBox.lpstrFile
    End If
End Function

Public Function PegasusOpenFilePF(ByRef sPath As String, sFilter As String)
    Dim oOpenFileBox As OPENFILENAME
    Dim sFileName As String * 520
    Dim lRet As Long

    sFileName = Chr(0)
    oOpenFileBox.lStructSize = Len(oOpenFileBox)
    oOpenFileBox.lpstrInitialDir = strCurrentDir
 
    oOpenFileBox.lpstrTitle = "Open File"
    oOpenFileBox.lpstrFilter = sFilter
    oOpenFileBox.lpstrFile = sFileName
    oOpenFileBox.nMaxFile = 520
    lRet = GetOpenFileName(oOpenFileBox)
    
    If lRet = 0 Then
        PegasusOpenFilePF = ""
    Else
        sPath = PathFromFile(oOpenFileBox.lpstrFile)
        PegasusOpenFilePF = oOpenFileBox.lpstrFile
    End If
End Function

Public Function PegasusOpenFileP(ByRef sPath As String)
    Dim oOpenFileBox As OPENFILENAME
    Dim sFileName As String * 520
    Dim lRet As Long

    sFileName = Chr(0)
    oOpenFileBox.lStructSize = Len(oOpenFileBox)
    oOpenFileBox.lpstrInitialDir = strCurrentDir
    oOpenFileBox.lpstrTitle = "Open File"
    oOpenFileBox.lpstrFilter = strDefaultImageFilter
    oOpenFileBox.lpstrFile = sFileName
    oOpenFileBox.nMaxFile = 520
    lRet = GetOpenFileName(oOpenFileBox)
    
    If lRet = 0 Then
        PegasusOpenFileP = ""
    Else
        sPath = PathFromFile(oOpenFileBox.lpstrFile)
        PegasusOpenFileP = oOpenFileBox.lpstrFile
    End If
End Function

Public Function PegasusSaveFilePF(ByRef sPath As String, sFilter As String, defExt As String)
    Dim saSaveFileBox As OPENFILENAME
    Dim sFileName As String * 520
    Dim lRet As Long

    sFileName = Chr(0)
    saSaveFileBox.lStructSize = Len(saSaveFileBox)
    saSaveFileBox.lpstrInitialDir = strCurrentDir
    saSaveFileBox.lpstrTitle = "Save File"
    saSaveFileBox.lpstrFilter = sFilter
    saSaveFileBox.lpstrDefExt = defExt
    saSaveFileBox.lpstrFile = sFileName
    saSaveFileBox.nMaxFile = 520
    lRet = GetSaveFileName(saSaveFileBox)
    
    If lRet = 0 Then
        PegasusSaveFilePF = ""
    Else
        sPath = PathFromFile(saSaveFileBox.lpstrFile)
        PegasusSaveFilePF = saSaveFileBox.lpstrFile
    End If
    
    PegasusSaveFilePF = Trim$(saSaveFileBox.lpstrFile)
    If (Right$(PegasusSaveFilePF, 1) = Chr$(0)) Then
        PegasusSaveFilePF = Left$(PegasusSaveFilePF, Len(PegasusSaveFilePF) - 1)
    End If
    
End Function

Public Function GetErrorDesc(ByRef errNo As Integer) As String
    Select Case errNo
       Case IX_Ok: GetErrorDesc = "No error"
        Case IX_Error_None: GetErrorDesc = "No error"
        Case IX_Error_FileOpen: GetErrorDesc = "Cannot find or cannot open the image file"
        Case IX_Error_FormatUnknown: GetErrorDesc = "The image file type is unknown or unable to read the image file type"
        Case IX_Error_MemoryLock: GetErrorDesc = "Cannot lock memory"
        Case IX_Error_FileRead: GetErrorDesc = "Error reading the fi"
        Case IX_Error_MemoryAlloc: GetErrorDesc = "Cannot allocate memory for the image"
        Case IX_Error_Canceled: GetErrorDesc = "Image load or decompression was canceled"
        Case IX_Error_CreateFile: GetErrorDesc = "Cannot create file."
        Case IX_Error_FileWrite: GetErrorDesc = "Error writing file"
        Case IX_Error_InvalidFormat: GetErrorDesc = "Invalid image format"
        Case IX_Error_InvalidOption: GetErrorDesc = "Invalid option"
        Case IX_Error_InvalidPaletteFile: GetErrorDesc = "Invalid palette file"
        Case IX_Error_ClipboardPaste: GetErrorDesc = "Error pasting from clipboard"
        Case IX_Error_ClipboardCopy: GetErrorDesc = "Error copying image to the clipboard."
        Case IX_Error_InvalidPicPassword: GetErrorDesc = "Attempting to read a password protected PIC file and the password in the PICPassword property does not match the password in the file."
        Case IX_Error_BadAngle: GetErrorDesc = "The operation failed because an invalid angle was specified. "
        Case IX_Error_BadRadius: GetErrorDesc = "The operation failed because an invalid radius was specified. "
        Case IX_Error_InvalidCrop: GetErrorDesc = "Loading a JPEG or ePIC image failed because PICCropEnabled property was set and the PICCropX or PICCropY property was out of range for the specified image."
        Case IX_Error_InvalidPropertyValue: GetErrorDesc = "Invalid property value."
        Case IX_Error_FTP: GetErrorDesc = "FTP error."
        Case IX_Error_InvalidFileName: GetErrorDesc = "Invalid file name."
        Case IX_Error_CaptureFailed: GetErrorDesc = "Capture failed."
        Case IX_Error_InvalidLicense: GetErrorDesc = "Invalid License. Attempting to access a function using a function that you are not licensed to use. See ImagXpress Professional/ ImagXpress Standard Differences for more information."
        Case IX_Error_UnableToCrop: GetErrorDesc = "Unable to Crop"
        Case IX_Error_NoWindow: GetErrorDesc = "No Window Handle"
        Case IX_Error_JpegCommentNotFound: GetErrorDesc = "JPEG comment not found. The requested comment does not exist."
        Case IX_Error_JpegCommentAddFailure: GetErrorDesc = "Unable to add a JPEG comment."
        Case IX_Error_InvalidHandle: GetErrorDesc = "Invalid Handle."
        Case IX_Error_InvalidImageId: GetErrorDesc = "Invalid Image ID."
        Case IX_Error_InvalidPointer: GetErrorDesc = "Invalid Pointer."
        Case IX_Error_InvalidAreaOrRegion: GetErrorDesc = "Invalid Area or Region."
        Case IX_Error_NoCurrentBuffer: GetErrorDesc = "No Current Buffer."
        Case IX_Error_ImageAccessTimeout: GetErrorDesc = "Image Access Timeout."
        Case IX_Error_BufferAccessTimeout: GetErrorDesc = "Buffer Access Timeout."
        Case IX_Error_EventResponse: GetErrorDesc = "Event Response Error."
        Case IX_Error_ImageHasTasksPending: GetErrorDesc = "Image has tasks pending..."
        Case IX_Error_ResumeThreadFailure: GetErrorDesc = "Resume thread error."
        Case IX_Error_Shutdown: GetErrorDesc = "Shutdown error."
        Case IX_Error_OperationNotApplicableForRawImages: GetErrorDesc = "Operation not applicable for raw data."
        Case IX_Error_NoLicense: GetErrorDesc = "No valid license."
        Case IX_Error_InvalidWebIntegratorLicense: GetErrorDesc = "Invalid web license."
        Case IX_Error_InvalidAddOnLicense: GetErrorDesc = "Invalid addon license."
        Case IX_Error_LoadWatermarkImage: GetErrorDesc = "Load watermark image error."
        Case IX_Error_InternetInValidCA: GetErrorDesc = "Internet error."
         Case IX_Error_InternetPostIsNonSecure: GetErrorDesc = "Non-secure post error."
         Case IX_Error_InternetSecurityCertificateCNInvalid: GetErrorDesc = "Secure certification invalid."
        Case IX_Error_InternetSecurityCertificateDateInvalid: GetErrorDesc = "Secure certification date invalid."
       Case IX_Error_InternetHTTPtoHTTPsOnRedirect: GetErrorDesc = "Redirection to secure site."
        Case IX_Error_InvalidICMProfile: GetErrorDesc = "Invalid profile."
        Case IX_Error_MissingOrBadCMYKProfile: GetErrorDesc = "Missing or bad CMYK color profile."
        Case IX_Error_ICMNotEnabled: GetErrorDesc = "Image color matching not enabled."
        Case IX_Error_NoResources: GetErrorDesc = "No resources."
        Case IX_Error_PrintNotSupported: GetErrorDesc = "Print not supported."
        Case IX_Error_PrintOutOfRange: GetErrorDesc = "Print out of range."
        Case IX_Error_TagNotFound: GetErrorDesc = "TIFF tag not found."
        Case IX_Error_TagItemNotFound:  GetErrorDesc = "TIFF item not found."
        Case IX_Error_TiffTag_AddError: GetErrorDesc = "TIFF add error."
        Case IX_Error_NoResources: GetErrorDesc = "Image capture failed."
        Case IX_Error_PrintNotSupported: GetErrorDesc = "Printer does not support the StretchDIBits API function "
        Case IX_Error_PrintOutOfRange: GetErrorDesc = "Cannot print outside the valid printer area"
        Case IX_Error_TagNotFound: GetErrorDesc = "Tag not found."
        Case IX_Error_TagItemNotFound: GetErrorDesc = "Tag item not found."
        Case IX_Error_TiffTag_AddError: GetErrorDesc = "Tag error adding tag."
              
    End Select
End Function

Function GetICMProfile(hParentWnd As Long, FileType As String) As String
    ' Call the GetOpenFileName function directly so we don't
    ' have to use the Common Dialog custom control

    Dim of As OPENFILENAME
    Dim fn As String * 260
    Dim rc As Long
    Dim strRetWinDir As String * 260
    Dim strWinDir As String
    Dim strRetSysDir As String * 260
    Dim strSysDir As String
    Dim strProfileDir As String * 384
    Dim strProfile As String
    
    Dim fileSysObj As Object
    
    Set fileSysObj = CreateObject("Scripting.FileSystemObject")

    
    Dim strDirChk As String
    Dim bFoundICMDir As Boolean

    bFoundICMDir = False
    strRetSysDir = ""
    strRetWinDir = ""
    strProfileDir = ""
    
    GetWindowsDirectory strRetWinDir, 260
    strWinDir = Trim$(strRetWinDir)
    strWinDir = Left$(strWinDir, Len(strWinDir) - 1)
    
    GetSystemDirectory strRetSysDir, 260
    strSysDir = Trim$(strRetSysDir)
    strSysDir = Left$(strSysDir, Len(strSysDir) - 1)
    
    strProfileDir = strSysDir & "\spool\drivers\color"
    
    ' TODO: Need to add check to see if that directory exists
     bFoundICMDir = True

    If bFoundICMDir = False Then
        GetICMProfile = ""
        Exit Function
    End If
    
    fn = Chr(0)
    of.lStructSize = Len(of)
    of.hwndOwner = hParentWnd
    of.lpstrInitialDir = strProfileDir
    Select Case FileType
        Case "OPEN"
            of.lpstrTitle = "Open ICMProfile"
            of.lpstrFilter = "ALL" & Chr(0) & "*.*" & Chr(0) & "ICM" & Chr(0) & "*.icm" & Chr(0) & "ICC" & Chr(0) & "*.icc" & Chr(0)
            of.lpstrDefExt = "icm"
    End Select

    of.lpstrFile = fn
    of.nMaxFile = 260

    
    rc = GetOpenFileName(of)

    If rc = 0 Then
        GetICMProfile = ""
    Else
        strProfile = Trim$(of.lpstrFile)
        strProfile = Left$(strProfile, Len(strProfile) - 1)

        strProfile = fileSysObj.GetFileName(strProfile)
        Set fileSysObj = Nothing
        GetICMProfile = strProfile
    End If

End Function




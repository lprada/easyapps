VERSION 5.00
Begin VB.Form frmRawDef 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Raw Image Parameters"
   ClientHeight    =   4965
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4785
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleWidth      =   4785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtBytesPerPixel 
      Height          =   375
      Left            =   1440
      TabIndex        =   16
      Top             =   2832
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   3120
      TabIndex        =   20
      Top             =   4440
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Default         =   -1  'True
      Height          =   375
      Left            =   1560
      TabIndex        =   19
      Top             =   4440
      Width           =   1455
   End
   Begin VB.TextBox txtStride 
      Height          =   375
      Left            =   1440
      TabIndex        =   18
      Top             =   3840
      Width           =   1215
   End
   Begin VB.TextBox txtOffset 
      Height          =   375
      Left            =   1440
      TabIndex        =   17
      Top             =   3336
      Width           =   1215
   End
   Begin VB.TextBox txtBitsPerPixel 
      Height          =   375
      Left            =   1440
      TabIndex        =   15
      Top             =   2328
      Width           =   1215
   End
   Begin VB.TextBox txtHeight 
      Height          =   375
      Left            =   1440
      TabIndex        =   14
      Top             =   1824
      Width           =   1215
   End
   Begin VB.TextBox txtWidth 
      Height          =   375
      Left            =   1440
      TabIndex        =   13
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Frame fraByteOrder 
      Caption         =   "Byte Order"
      Height          =   1215
      Left            =   2880
      TabIndex        =   8
      Top             =   1200
      Width           =   1695
      Begin VB.OptionButton optMotorola 
         Caption         =   "Motorola"
         Height          =   195
         Left            =   240
         TabIndex        =   12
         Top             =   720
         Width           =   1095
      End
      Begin VB.OptionButton optIntel 
         Caption         =   "Intel"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   360
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.Frame fraOrientation 
      Caption         =   "Orientation"
      Height          =   1215
      Left            =   2880
      TabIndex        =   7
      Top             =   2520
      Width           =   1695
      Begin VB.OptionButton optBottomUp 
         Caption         =   "Bottom-up"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton optTopDown 
         Caption         =   "Top-down"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Value           =   -1  'True
         Width           =   1095
      End
   End
   Begin VB.Label lblBytesPerPixel 
      Caption         =   "Bytes Per Pixel:"
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   2892
      Width           =   1215
   End
   Begin VB.Label lblStride 
      Caption         =   "Stride (bytes)"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   3900
      Width           =   1335
   End
   Begin VB.Label lblOffset 
      Caption         =   "Offset (bytes):"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Label lblBitsPerPixel 
      Caption         =   "Bits Per Pixel:"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   2400
      Width           =   1215
   End
   Begin VB.Label lblHeight 
      Caption         =   "Height (lines):"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   1890
      Width           =   1215
   End
   Begin VB.Label lblWidth 
      Caption         =   "Width (pixels):"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1380
      Width           =   1215
   End
   Begin VB.Line Line1 
      X1              =   240
      X2              =   4560
      Y1              =   1080
      Y2              =   1080
   End
   Begin VB.Label lblFileSize 
      Caption         =   "File Size:"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label lblFileName 
      Caption         =   "File Name: "
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmRawDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public bCancel As Boolean

Private Sub cmdCancel_Click()
    bCancel = True
    Me.Hide
End Sub

Private Sub cmdOK_Click()
    ' validate that there are valid entries before we return...
    bCancel = False
    Me.Hide
End Sub

Private Sub Form_Load()
    bCancel = False
   ' lblFileName =
End Sub

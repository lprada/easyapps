// Color ProfilesDlg.h : header file
//

#if !defined(AFX_COLORPROFILESDLG_H__C7A8514C_97DA_424C_8B51_66F3D411746C__INCLUDED_)
#define AFX_COLORPROFILESDLG_H__C7A8514C_97DA_424C_8B51_66F3D411746C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\ImagXpress8Events.h"
using namespace PrintPRO3;
#include "..\..\..\..\..\..\PrintPRO\V3.0\Samples\ActiveX-COM\VC++\Include\PrintPRO3Events.h"


/////////////////////////////////////////////////////////////////////////////
// CColorProfilesDlg dialog

class CColorProfilesDlg : public CDialog
{
// Construction
public:
	CColorProfilesDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CColorProfilesDlg)
	enum { IDD = IDD_COLORPROFILES_DIALOG };
	CComboBox	m_cboRenderIntentSettings;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorProfilesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CRichEditCtrl * pEdtInfo;

	// Generated message map functions
	//{{AFX_MSG(CColorProfilesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnDestroy();
	afx_msg void OnBtnClearallprofiles();
	afx_msg void OnChkIcmenabled();
	afx_msg void OnChkIcmproofingenabled();
    afx_msg void OnCmdLoadMonitorProfile();
    afx_msg void OnCmdLoadPrinterProfile();
    afx_msg void OnCmdLoadTargetProfile();
    afx_msg void OnCmdOpenImage();
    afx_msg void OnCmdExit();
	afx_msg void OnSelchangeCboRenderintentsettings();
	afx_msg void OnUpdateToolbarShow(CCmdUI* pCmdUI);
	afx_msg void OnBtnPrintimage();
	afx_msg void OnButton1();
	afx_msg void OnHelpAboutbox();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
 
    bool            GetICMProfilePath           (CString& strICMProfilePath) const;
    bool            GetImagePath                (CString& strImageFilePath) const;

	//ImagXpress pointers
    CImagXpress*    m_ppCImagXpress;
    IImagXpressPtr  m_pImagXpress;

	//PrintPRO pointers
	 CPrintPRO*      m_ppCPrintPRO;
     IPrintPROPtr    m_pPrintPRO;

    CString         m_strSavedICMMonitorProfile;
    CString         m_strSavedICMPrinterrProfile;
    CString         m_strSavedICMTargetProfile;
	PegasusImagingActiveXImagXpress8::enumRenderIntent m_iRenderIntent;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORPROFILESDLG_H__C7A8514C_97DA_424C_8B51_66F3D411746C__INCLUDED_)

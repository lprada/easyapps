/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TwainPROBasics
{
    public class frmTwainProBasics : Form
    {

        //Declare the Twain Device and TwainPro objects
        private PegasusImaging.WinForms.TwainPro5.TwainDevice twainDevice;
        private System.Windows.Forms.Button cmdScanWith;
        private System.Windows.Forms.Button cmdScanWithOut;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ListBox lstDesc;
        private PegasusImaging.WinForms.TwainPro5.TwainPro twainPro1;
        private IContainer components;

        public frmTwainProBasics()
        {
            InitializeComponent();
        }

        private void frmTwainProBasics_Load(object sender, EventArgs e)
        {
            try
            {
                //***Must call the UnlockRuntime method  
                //twainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

                //***Setup the device object
                twainDevice = new PegasusImaging.WinForms.TwainPro5.TwainDevice(twainPro1);
                twainDevice.Scanned += new PegasusImaging.WinForms.TwainPro5.ScannedEventHandler(this.twainDevice_Scanned);
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                ErrorLabel.Text = ex.Message;
            
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

                if (!(components == null))
                {
                    components.Dispose();

                }

                if (!(imagXpress1 == null))
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }

                if (!(imageXView1 == null))
                {
                    imageXView1.Dispose();
                    imageXView1 = null;
                }

            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmdScanWith = new System.Windows.Forms.Button();
            this.cmdScanWithOut = new System.Windows.Forms.Button();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lstDesc = new System.Windows.Forms.ListBox();
            this.twainPro1 = new PegasusImaging.WinForms.TwainPro5.TwainPro(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdScanWith
            // 
            this.cmdScanWith.Location = new System.Drawing.Point(249, 425);
            this.cmdScanWith.Name = "cmdScanWith";
            this.cmdScanWith.Size = new System.Drawing.Size(152, 37);
            this.cmdScanWith.TabIndex = 0;
            this.cmdScanWith.Text = "Scan with Scanner UI";
            this.cmdScanWith.UseVisualStyleBackColor = true;
            this.cmdScanWith.Click += new System.EventHandler(this.cmdScanWith_Click);
            // 
            // cmdScanWithOut
            // 
            this.cmdScanWithOut.Location = new System.Drawing.Point(441, 425);
            this.cmdScanWithOut.Name = "cmdScanWithOut";
            this.cmdScanWithOut.Size = new System.Drawing.Size(152, 37);
            this.cmdScanWithOut.TabIndex = 1;
            this.cmdScanWithOut.Text = "Scan without Scanner UI";
            this.cmdScanWithOut.UseVisualStyleBackColor = true;
            this.cmdScanWithOut.Click += new System.EventHandler(this.cmdScanWithOut_Click);
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(206, 120);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(406, 288);
            this.imageXView1.TabIndex = 2;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.Location = new System.Drawing.Point(621, 449);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.ErrorLabel.TabIndex = 3;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(824, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // lstDesc
            // 
            this.lstDesc.FormattingEnabled = true;
            this.lstDesc.Items.AddRange(new object[] {
            "This sample demonstrates using the TwainPRO .NET control to perform the following" +
                ":",
            "1)Scan with and without the Scanner Interface.",
            "2)Catch exceptions with the TwainPRO control.",
            "3)Use the scanned event to transfer image data from the TwainPRO control to ImagX" +
                "press."});
            this.lstDesc.Location = new System.Drawing.Point(194, 27);
            this.lstDesc.Name = "lstDesc";
            this.lstDesc.Size = new System.Drawing.Size(465, 69);
            this.lstDesc.TabIndex = 5;
            // 
            // frmTwainProBasics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 477);
            this.Controls.Add(this.lstDesc);
            this.Controls.Add(this.ErrorLabel);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.cmdScanWithOut);
            this.Controls.Add(this.cmdScanWith);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmTwainProBasics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TwainPRO Basics";
            this.Load += new System.EventHandler(this.frmTwainProBasics_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new frmTwainProBasics());
        }

        public void twainDevice_Scanned(object sender, PegasusImaging.WinForms.TwainPro5.ScannedEventArgs e)
        {
            try
            {
                imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(imagXpress1, e.ScannedImage.ToHdib(), true);
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                ErrorLabel.Text = ex.Message;

            }

        }

        static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = ErrorException.Message;
        }

        private void cmdScanWith_Click(object sender, EventArgs e)
        {
            try
            {
                twainDevice.ShowUserInterface = true;
                twainDevice.SelectSource();
                twainDevice.StartSession();
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                PegasusError(ex, ErrorLabel);

            }
        }

        private void cmdScanWithOut_Click(object sender, EventArgs e)
        {
            try
            {
                twainDevice.ShowUserInterface = false;
                twainDevice.SelectSource();
                twainDevice.StartSession();
            }

            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                ErrorLabel.Text = ex.Message;

            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            twainPro1.AboutBox();
        }
    }
}
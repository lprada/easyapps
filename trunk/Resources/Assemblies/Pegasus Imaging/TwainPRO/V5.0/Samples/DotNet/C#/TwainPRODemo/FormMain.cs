/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.TwainPro5;

namespace TwainProDemoCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonOpenSession;
		private System.Windows.Forms.Button buttonStartSession;
		private System.Windows.Forms.Button buttonCloseSession;
		private System.Windows.Forms.Button buttonShowCaps;
		private System.Windows.Forms.Button buttonShowLayout;
		private System.Windows.Forms.Button buttonShowCaption;
		private System.Windows.Forms.Button buttonSelectSource;
        private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.GroupBox groupBoxCaption;
		private System.Windows.Forms.CheckBox checkBoxClipCaption;
		private System.Windows.Forms.CheckBox checkBoxShadowText;
		private System.Windows.Forms.Label labelCapLeft;
		private System.Windows.Forms.Label labelCapTop;
		private System.Windows.Forms.Label labelCapWidth;
		private System.Windows.Forms.Label labelCapHeight;
		private System.Windows.Forms.Label labelCaption;
		private System.Windows.Forms.Label labelHAlign;
		private System.Windows.Forms.Label labelVAlign;
		private System.Windows.Forms.TextBox textBoxCapLeft;
		private System.Windows.Forms.TextBox textBoxCapTop;
		private System.Windows.Forms.TextBox textBoxCapWidth;
		private System.Windows.Forms.TextBox textBoxCapHeight;
		private System.Windows.Forms.TextBox textBoxCaption;
		private System.Windows.Forms.ComboBox comboBoxHAlign;
		private System.Windows.Forms.ComboBox comboBoxVAlign;
		private System.Windows.Forms.GroupBox groupBoxCaps;
		private System.Windows.Forms.ComboBox comboBoxCaps;
		private System.Windows.Forms.Label labelDefault;
		private System.Windows.Forms.ListBox listBoxCaps;
		private System.Windows.Forms.Label labelCurrent;
		private System.Windows.Forms.TextBox textBoxCurrent;
		private System.Windows.Forms.Button buttonUpdate;
		private System.Windows.Forms.GroupBox groupBoxLayout;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonLayoutUpdate;
		private System.Windows.Forms.CheckBox checkBoxShowUI;
        private System.Windows.Forms.CheckBox checkBoxSave;
		private System.Windows.Forms.TextBox textBoxIL;
		private System.Windows.Forms.TextBox textBoxIT;
		private System.Windows.Forms.TextBox textBoxIR;
		private System.Windows.Forms.TextBox textBoxIB;
		private System.Windows.Forms.Label labelMin;
        private System.Windows.Forms.Label labelMax;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
        private MenuItem TwainProMenuItem;
        private IContainer components;

        private PegasusImaging.WinForms.TwainPro5.TwainPro twainPro1;
        private PegasusImaging.WinForms.TwainPro5.TwainDevice twainDevice;
        private PegasusImaging.WinForms.TwainPro5.Capability capability;
        private PegasusImaging.WinForms.TwainPro5.CapabilityContainer capcontainer;

        private GroupBox groupBoxStart;
        private MenuItem ImagXpressMenuItem;
        private Label labelUnits;

        private System.Int32 _nImageCount;

        public FormMain()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
                if (twainDevice != null)
                {
                    twainDevice.Dispose();
                    twainDevice = null;
                }
            }
            base.Dispose(disposing);
        }

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.buttonOpenSession = new System.Windows.Forms.Button();
            this.buttonStartSession = new System.Windows.Forms.Button();
            this.buttonCloseSession = new System.Windows.Forms.Button();
            this.buttonShowCaps = new System.Windows.Forms.Button();
            this.buttonShowLayout = new System.Windows.Forms.Button();
            this.buttonShowCaption = new System.Windows.Forms.Button();
            this.buttonSelectSource = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.groupBoxCaption = new System.Windows.Forms.GroupBox();
            this.comboBoxVAlign = new System.Windows.Forms.ComboBox();
            this.comboBoxHAlign = new System.Windows.Forms.ComboBox();
            this.textBoxCaption = new System.Windows.Forms.TextBox();
            this.textBoxCapHeight = new System.Windows.Forms.TextBox();
            this.textBoxCapWidth = new System.Windows.Forms.TextBox();
            this.textBoxCapTop = new System.Windows.Forms.TextBox();
            this.textBoxCapLeft = new System.Windows.Forms.TextBox();
            this.labelVAlign = new System.Windows.Forms.Label();
            this.labelHAlign = new System.Windows.Forms.Label();
            this.labelCaption = new System.Windows.Forms.Label();
            this.labelCapHeight = new System.Windows.Forms.Label();
            this.labelCapWidth = new System.Windows.Forms.Label();
            this.labelCapTop = new System.Windows.Forms.Label();
            this.labelCapLeft = new System.Windows.Forms.Label();
            this.checkBoxShadowText = new System.Windows.Forms.CheckBox();
            this.checkBoxClipCaption = new System.Windows.Forms.CheckBox();
            this.groupBoxLayout = new System.Windows.Forms.GroupBox();
            this.textBoxIB = new System.Windows.Forms.TextBox();
            this.textBoxIR = new System.Windows.Forms.TextBox();
            this.textBoxIT = new System.Windows.Forms.TextBox();
            this.textBoxIL = new System.Windows.Forms.TextBox();
            this.buttonLayoutUpdate = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxCaps = new System.Windows.Forms.GroupBox();
            this.labelUnits = new System.Windows.Forms.Label();
            this.labelDefault = new System.Windows.Forms.Label();
            this.labelMax = new System.Windows.Forms.Label();
            this.labelMin = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textBoxCurrent = new System.Windows.Forms.TextBox();
            this.labelCurrent = new System.Windows.Forms.Label();
            this.listBoxCaps = new System.Windows.Forms.ListBox();
            this.comboBoxCaps = new System.Windows.Forms.ComboBox();
            this.checkBoxSave = new System.Windows.Forms.CheckBox();
            this.checkBoxShowUI = new System.Windows.Forms.CheckBox();
            this.lstInfo = new System.Windows.Forms.ListBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.mnu_File = new System.Windows.Forms.MenuItem();
            this.mnu_Quit = new System.Windows.Forms.MenuItem();
            this.mnu_About = new System.Windows.Forms.MenuItem();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.TwainProMenuItem = new System.Windows.Forms.MenuItem();
            this.twainPro1 = new PegasusImaging.WinForms.TwainPro5.TwainPro(this.components);
            this.groupBoxStart = new System.Windows.Forms.GroupBox();
            this.groupBoxCaption.SuspendLayout();
            this.groupBoxLayout.SuspendLayout();
            this.groupBoxCaps.SuspendLayout();
            this.groupBoxStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOpenSession
            // 
            this.buttonOpenSession.Enabled = false;
            this.buttonOpenSession.Location = new System.Drawing.Point(12, 189);
            this.buttonOpenSession.Name = "buttonOpenSession";
            this.buttonOpenSession.Size = new System.Drawing.Size(128, 23);
            this.buttonOpenSession.TabIndex = 0;
            this.buttonOpenSession.Text = "Open Session";
            this.buttonOpenSession.Click += new System.EventHandler(this.buttonOpenSession_Click);
            // 
            // buttonStartSession
            // 
            this.buttonStartSession.Enabled = false;
            this.buttonStartSession.Location = new System.Drawing.Point(12, 221);
            this.buttonStartSession.Name = "buttonStartSession";
            this.buttonStartSession.Size = new System.Drawing.Size(128, 23);
            this.buttonStartSession.TabIndex = 1;
            this.buttonStartSession.Text = "Start Session\\Acquire";
            this.buttonStartSession.Click += new System.EventHandler(this.buttonStartSession_Click);
            // 
            // buttonCloseSession
            // 
            this.buttonCloseSession.Location = new System.Drawing.Point(12, 253);
            this.buttonCloseSession.Name = "buttonCloseSession";
            this.buttonCloseSession.Size = new System.Drawing.Size(128, 23);
            this.buttonCloseSession.TabIndex = 2;
            this.buttonCloseSession.Text = "Close Session";
            this.buttonCloseSession.Click += new System.EventHandler(this.buttonCloseSession_Click);
            // 
            // buttonShowCaps
            // 
            this.buttonShowCaps.Enabled = false;
            this.buttonShowCaps.Location = new System.Drawing.Point(12, 285);
            this.buttonShowCaps.Name = "buttonShowCaps";
            this.buttonShowCaps.Size = new System.Drawing.Size(128, 23);
            this.buttonShowCaps.TabIndex = 3;
            this.buttonShowCaps.Text = "Show Capabilities";
            this.buttonShowCaps.Click += new System.EventHandler(this.buttonShowCaps_Click);
            // 
            // buttonShowLayout
            // 
            this.buttonShowLayout.Enabled = false;
            this.buttonShowLayout.Location = new System.Drawing.Point(12, 317);
            this.buttonShowLayout.Name = "buttonShowLayout";
            this.buttonShowLayout.Size = new System.Drawing.Size(128, 23);
            this.buttonShowLayout.TabIndex = 4;
            this.buttonShowLayout.Text = "Show Layout";
            this.buttonShowLayout.Click += new System.EventHandler(this.buttonShowLayout_Click);
            // 
            // buttonShowCaption
            // 
            this.buttonShowCaption.Location = new System.Drawing.Point(12, 349);
            this.buttonShowCaption.Name = "buttonShowCaption";
            this.buttonShowCaption.Size = new System.Drawing.Size(128, 23);
            this.buttonShowCaption.TabIndex = 5;
            this.buttonShowCaption.Text = "Show Caption";
            this.buttonShowCaption.Click += new System.EventHandler(this.buttonShowCaption_Click);
            // 
            // buttonSelectSource
            // 
            this.buttonSelectSource.Location = new System.Drawing.Point(12, 157);
            this.buttonSelectSource.Name = "buttonSelectSource";
            this.buttonSelectSource.Size = new System.Drawing.Size(128, 23);
            this.buttonSelectSource.TabIndex = 6;
            this.buttonSelectSource.Text = "Select Source";
            this.buttonSelectSource.Click += new System.EventHandler(this.buttonSelectSource_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStatus.Location = new System.Drawing.Point(3, 430);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(602, 44);
            this.labelStatus.TabIndex = 8;
            // 
            // groupBoxCaption
            // 
            this.groupBoxCaption.Controls.Add(this.comboBoxVAlign);
            this.groupBoxCaption.Controls.Add(this.comboBoxHAlign);
            this.groupBoxCaption.Controls.Add(this.textBoxCaption);
            this.groupBoxCaption.Controls.Add(this.textBoxCapHeight);
            this.groupBoxCaption.Controls.Add(this.textBoxCapWidth);
            this.groupBoxCaption.Controls.Add(this.textBoxCapTop);
            this.groupBoxCaption.Controls.Add(this.textBoxCapLeft);
            this.groupBoxCaption.Controls.Add(this.labelVAlign);
            this.groupBoxCaption.Controls.Add(this.labelHAlign);
            this.groupBoxCaption.Controls.Add(this.labelCaption);
            this.groupBoxCaption.Controls.Add(this.labelCapHeight);
            this.groupBoxCaption.Controls.Add(this.labelCapWidth);
            this.groupBoxCaption.Controls.Add(this.labelCapTop);
            this.groupBoxCaption.Controls.Add(this.labelCapLeft);
            this.groupBoxCaption.Controls.Add(this.checkBoxShadowText);
            this.groupBoxCaption.Controls.Add(this.checkBoxClipCaption);
            this.groupBoxCaption.Location = new System.Drawing.Point(683, 41);
            this.groupBoxCaption.Name = "groupBoxCaption";
            this.groupBoxCaption.Size = new System.Drawing.Size(312, 161);
            this.groupBoxCaption.TabIndex = 0;
            this.groupBoxCaption.TabStop = false;
            this.groupBoxCaption.Text = "Caption";
            this.groupBoxCaption.Visible = false;
            // 
            // comboBoxVAlign
            // 
            this.comboBoxVAlign.Items.AddRange(new object[] {
            "Center",
            "Left",
            "Right"});
            this.comboBoxVAlign.Location = new System.Drawing.Point(184, 104);
            this.comboBoxVAlign.Name = "comboBoxVAlign";
            this.comboBoxVAlign.Size = new System.Drawing.Size(120, 21);
            this.comboBoxVAlign.TabIndex = 15;
            // 
            // comboBoxHAlign
            // 
            this.comboBoxHAlign.Items.AddRange(new object[] {
            "Center",
            "Left",
            "Right"});
            this.comboBoxHAlign.Location = new System.Drawing.Point(184, 56);
            this.comboBoxHAlign.Name = "comboBoxHAlign";
            this.comboBoxHAlign.Size = new System.Drawing.Size(120, 21);
            this.comboBoxHAlign.TabIndex = 14;
            // 
            // textBoxCaption
            // 
            this.textBoxCaption.Location = new System.Drawing.Point(112, 134);
            this.textBoxCaption.Name = "textBoxCaption";
            this.textBoxCaption.Size = new System.Drawing.Size(192, 20);
            this.textBoxCaption.TabIndex = 13;
            this.textBoxCaption.Text = "Pegasus Imaging Corp.";
            // 
            // textBoxCapHeight
            // 
            this.textBoxCapHeight.Location = new System.Drawing.Point(112, 110);
            this.textBoxCapHeight.Name = "textBoxCapHeight";
            this.textBoxCapHeight.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapHeight.TabIndex = 12;
            this.textBoxCapHeight.Text = "0";
            this.textBoxCapHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapWidth
            // 
            this.textBoxCapWidth.Location = new System.Drawing.Point(112, 86);
            this.textBoxCapWidth.Name = "textBoxCapWidth";
            this.textBoxCapWidth.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapWidth.TabIndex = 11;
            this.textBoxCapWidth.Text = "0";
            this.textBoxCapWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapTop
            // 
            this.textBoxCapTop.Location = new System.Drawing.Point(112, 62);
            this.textBoxCapTop.Name = "textBoxCapTop";
            this.textBoxCapTop.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapTop.TabIndex = 10;
            this.textBoxCapTop.Text = "0";
            this.textBoxCapTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapLeft
            // 
            this.textBoxCapLeft.Location = new System.Drawing.Point(112, 38);
            this.textBoxCapLeft.Name = "textBoxCapLeft";
            this.textBoxCapLeft.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapLeft.TabIndex = 9;
            this.textBoxCapLeft.Text = "0";
            this.textBoxCapLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelVAlign
            // 
            this.labelVAlign.Location = new System.Drawing.Point(184, 88);
            this.labelVAlign.Name = "labelVAlign";
            this.labelVAlign.Size = new System.Drawing.Size(92, 16);
            this.labelVAlign.TabIndex = 8;
            this.labelVAlign.Text = "Vertical Align";
            // 
            // labelHAlign
            // 
            this.labelHAlign.Location = new System.Drawing.Point(184, 40);
            this.labelHAlign.Name = "labelHAlign";
            this.labelHAlign.Size = new System.Drawing.Size(105, 16);
            this.labelHAlign.TabIndex = 7;
            this.labelHAlign.Text = "Horizontal Align";
            // 
            // labelCaption
            // 
            this.labelCaption.Location = new System.Drawing.Point(16, 136);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(48, 16);
            this.labelCaption.TabIndex = 6;
            this.labelCaption.Text = "Caption:";
            // 
            // labelCapHeight
            // 
            this.labelCapHeight.Location = new System.Drawing.Point(16, 112);
            this.labelCapHeight.Name = "labelCapHeight";
            this.labelCapHeight.Size = new System.Drawing.Size(88, 16);
            this.labelCapHeight.TabIndex = 5;
            this.labelCapHeight.Text = "Caption Height:";
            // 
            // labelCapWidth
            // 
            this.labelCapWidth.Location = new System.Drawing.Point(16, 88);
            this.labelCapWidth.Name = "labelCapWidth";
            this.labelCapWidth.Size = new System.Drawing.Size(80, 16);
            this.labelCapWidth.TabIndex = 4;
            this.labelCapWidth.Text = "Caption Width:";
            // 
            // labelCapTop
            // 
            this.labelCapTop.Location = new System.Drawing.Point(16, 64);
            this.labelCapTop.Name = "labelCapTop";
            this.labelCapTop.Size = new System.Drawing.Size(72, 16);
            this.labelCapTop.TabIndex = 3;
            this.labelCapTop.Text = "Caption Top:";
            // 
            // labelCapLeft
            // 
            this.labelCapLeft.Location = new System.Drawing.Point(16, 40);
            this.labelCapLeft.Name = "labelCapLeft";
            this.labelCapLeft.Size = new System.Drawing.Size(72, 16);
            this.labelCapLeft.TabIndex = 2;
            this.labelCapLeft.Text = "Caption Left:";
            // 
            // checkBoxShadowText
            // 
            this.checkBoxShadowText.Location = new System.Drawing.Point(128, 16);
            this.checkBoxShadowText.Name = "checkBoxShadowText";
            this.checkBoxShadowText.Size = new System.Drawing.Size(96, 16);
            this.checkBoxShadowText.TabIndex = 1;
            this.checkBoxShadowText.Text = "Shadow Text";
            // 
            // checkBoxClipCaption
            // 
            this.checkBoxClipCaption.Location = new System.Drawing.Point(16, 16);
            this.checkBoxClipCaption.Name = "checkBoxClipCaption";
            this.checkBoxClipCaption.Size = new System.Drawing.Size(88, 16);
            this.checkBoxClipCaption.TabIndex = 0;
            this.checkBoxClipCaption.Text = "Clip Caption";
            // 
            // groupBoxLayout
            // 
            this.groupBoxLayout.Controls.Add(this.textBoxIB);
            this.groupBoxLayout.Controls.Add(this.textBoxIR);
            this.groupBoxLayout.Controls.Add(this.textBoxIT);
            this.groupBoxLayout.Controls.Add(this.textBoxIL);
            this.groupBoxLayout.Controls.Add(this.buttonLayoutUpdate);
            this.groupBoxLayout.Controls.Add(this.label4);
            this.groupBoxLayout.Controls.Add(this.label3);
            this.groupBoxLayout.Controls.Add(this.label2);
            this.groupBoxLayout.Controls.Add(this.label1);
            this.groupBoxLayout.Location = new System.Drawing.Point(146, 175);
            this.groupBoxLayout.Name = "groupBoxLayout";
            this.groupBoxLayout.Size = new System.Drawing.Size(312, 216);
            this.groupBoxLayout.TabIndex = 0;
            this.groupBoxLayout.TabStop = false;
            this.groupBoxLayout.Text = "Layout";
            this.groupBoxLayout.Visible = false;
            // 
            // textBoxIB
            // 
            this.textBoxIB.Location = new System.Drawing.Point(156, 126);
            this.textBoxIB.Name = "textBoxIB";
            this.textBoxIB.Size = new System.Drawing.Size(88, 20);
            this.textBoxIB.TabIndex = 8;
            // 
            // textBoxIR
            // 
            this.textBoxIR.Location = new System.Drawing.Point(156, 94);
            this.textBoxIR.Name = "textBoxIR";
            this.textBoxIR.Size = new System.Drawing.Size(88, 20);
            this.textBoxIR.TabIndex = 7;
            // 
            // textBoxIT
            // 
            this.textBoxIT.Location = new System.Drawing.Point(156, 62);
            this.textBoxIT.Name = "textBoxIT";
            this.textBoxIT.Size = new System.Drawing.Size(88, 20);
            this.textBoxIT.TabIndex = 6;
            // 
            // textBoxIL
            // 
            this.textBoxIL.Location = new System.Drawing.Point(156, 30);
            this.textBoxIL.Name = "textBoxIL";
            this.textBoxIL.Size = new System.Drawing.Size(88, 20);
            this.textBoxIL.TabIndex = 5;
            // 
            // buttonLayoutUpdate
            // 
            this.buttonLayoutUpdate.Location = new System.Drawing.Point(112, 160);
            this.buttonLayoutUpdate.Name = "buttonLayoutUpdate";
            this.buttonLayoutUpdate.Size = new System.Drawing.Size(96, 24);
            this.buttonLayoutUpdate.TabIndex = 4;
            this.buttonLayoutUpdate.Text = "Update";
            this.buttonLayoutUpdate.Click += new System.EventHandler(this.buttonLayoutUpdate_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(68, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Image Bottom:";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(68, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Image Right:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(68, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Image Top:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(68, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image Left:";
            // 
            // groupBoxCaps
            // 
            this.groupBoxCaps.Controls.Add(this.labelUnits);
            this.groupBoxCaps.Controls.Add(this.labelDefault);
            this.groupBoxCaps.Controls.Add(this.labelMax);
            this.groupBoxCaps.Controls.Add(this.labelMin);
            this.groupBoxCaps.Controls.Add(this.buttonUpdate);
            this.groupBoxCaps.Controls.Add(this.textBoxCurrent);
            this.groupBoxCaps.Controls.Add(this.labelCurrent);
            this.groupBoxCaps.Controls.Add(this.listBoxCaps);
            this.groupBoxCaps.Controls.Add(this.comboBoxCaps);
            this.groupBoxCaps.Location = new System.Drawing.Point(628, 124);
            this.groupBoxCaps.Name = "groupBoxCaps";
            this.groupBoxCaps.Size = new System.Drawing.Size(459, 310);
            this.groupBoxCaps.TabIndex = 0;
            this.groupBoxCaps.TabStop = false;
            this.groupBoxCaps.Text = "Capabilities";
            this.groupBoxCaps.Visible = false;
            // 
            // labelUnits
            // 
            this.labelUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnits.Location = new System.Drawing.Point(294, 174);
            this.labelUnits.Name = "labelUnits";
            this.labelUnits.Size = new System.Drawing.Size(159, 24);
            this.labelUnits.TabIndex = 8;
            // 
            // labelDefault
            // 
            this.labelDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDefault.Location = new System.Drawing.Point(40, 200);
            this.labelDefault.Name = "labelDefault";
            this.labelDefault.Size = new System.Drawing.Size(413, 25);
            this.labelDefault.TabIndex = 1;
            this.labelDefault.Text = "labelDefault";
            // 
            // labelMax
            // 
            this.labelMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMax.Location = new System.Drawing.Point(40, 236);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(417, 25);
            this.labelMax.TabIndex = 7;
            this.labelMax.Text = "labelMax";
            // 
            // labelMin
            // 
            this.labelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMin.Location = new System.Drawing.Point(40, 270);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(417, 25);
            this.labelMin.TabIndex = 6;
            this.labelMin.Text = "labelMin";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Enabled = false;
            this.buttonUpdate.Location = new System.Drawing.Point(208, 168);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(80, 24);
            this.buttonUpdate.TabIndex = 5;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textBoxCurrent
            // 
            this.textBoxCurrent.Location = new System.Drawing.Point(96, 170);
            this.textBoxCurrent.Name = "textBoxCurrent";
            this.textBoxCurrent.Size = new System.Drawing.Size(96, 20);
            this.textBoxCurrent.TabIndex = 4;
            // 
            // labelCurrent
            // 
            this.labelCurrent.Location = new System.Drawing.Point(40, 172);
            this.labelCurrent.Name = "labelCurrent";
            this.labelCurrent.Size = new System.Drawing.Size(48, 16);
            this.labelCurrent.TabIndex = 3;
            this.labelCurrent.Text = "Current:";
            // 
            // listBoxCaps
            // 
            this.listBoxCaps.Location = new System.Drawing.Point(32, 51);
            this.listBoxCaps.Name = "listBoxCaps";
            this.listBoxCaps.Size = new System.Drawing.Size(256, 95);
            this.listBoxCaps.TabIndex = 2;
            // 
            // comboBoxCaps
            // 
            this.comboBoxCaps.Items.AddRange(new object[] {
            "AutoBright",
            "Brightness",
            "Contrast",
            "Gamma",
            "Physical Width",
            "Physical Height",
            "Pixel Type",
            "Rotation",
            "Units",
            "X Native Resolution",
            "Y Native Resolution",
            "X Resolution",
            "Y Resolution",
            "X Scaling",
            "Y Scaling",
            "Device Online",
            "Indicators",
            "Lamp State",
            "UIControllable",
            "Transfer Count",
            "Auto Feed",
            "Auto Scan",
            "Clear Page",
            "Duplex",
            "Duplex Enabled",
            "Feeder Enabled",
            "Feeder Loaded",
            "Feed Page",
            "Paper Detectable"});
            this.comboBoxCaps.Location = new System.Drawing.Point(32, 24);
            this.comboBoxCaps.Name = "comboBoxCaps";
            this.comboBoxCaps.Size = new System.Drawing.Size(256, 21);
            this.comboBoxCaps.TabIndex = 0;
            this.comboBoxCaps.SelectedIndexChanged += new System.EventHandler(this.comboBoxCaps_SelectedIndexChanged);
            // 
            // checkBoxSave
            // 
            this.checkBoxSave.Location = new System.Drawing.Point(7, 41);
            this.checkBoxSave.Name = "checkBoxSave";
            this.checkBoxSave.Size = new System.Drawing.Size(190, 22);
            this.checkBoxSave.TabIndex = 1;
            this.checkBoxSave.Text = "Save Image as  Image-0.tif";
            // 
            // checkBoxShowUI
            // 
            this.checkBoxShowUI.Location = new System.Drawing.Point(6, 19);
            this.checkBoxShowUI.Name = "checkBoxShowUI";
            this.checkBoxShowUI.Size = new System.Drawing.Size(96, 16);
            this.checkBoxShowUI.TabIndex = 0;
            this.checkBoxShowUI.Text = "Show UI";
            this.checkBoxShowUI.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lstInfo
            // 
            this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstInfo.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Selecting the device with or without the scanner\'s UI via the ShowUI property.",
            "2)Querying and setting the various TWAIN capabilities.",
            "3)Scanning a specific size page via the SetImageLayout method.",
            "4)Saving the scanned image."});
            this.lstInfo.Location = new System.Drawing.Point(3, 3);
            this.lstInfo.Name = "lstInfo";
            this.lstInfo.Size = new System.Drawing.Size(602, 82);
            this.lstInfo.TabIndex = 10;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_File,
            this.mnu_About});
            // 
            // mnu_File
            // 
            this.mnu_File.Index = 0;
            this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_Quit});
            this.mnu_File.Text = "&File";
            // 
            // mnu_Quit
            // 
            this.mnu_Quit.Index = 0;
            this.mnu_Quit.Text = "E&xit";
            this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
            // 
            // mnu_About
            // 
            this.mnu_About.Index = 1;
            this.mnu_About.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ImagXpressMenuItem,
            this.TwainProMenuItem});
            this.mnu_About.Text = "&About";
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 0;
            this.ImagXpressMenuItem.Text = "Imag&Xpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // TwainProMenuItem
            // 
            this.TwainProMenuItem.Index = 1;
            this.TwainProMenuItem.Text = "&TwainPro5";
            this.TwainProMenuItem.Click += new System.EventHandler(this.TwainProMenuItem_Click);
            // 
            // groupBoxStart
            // 
            this.groupBoxStart.Controls.Add(this.checkBoxSave);
            this.groupBoxStart.Controls.Add(this.checkBoxShowUI);
            this.groupBoxStart.Location = new System.Drawing.Point(146, 91);
            this.groupBoxStart.Name = "groupBoxStart";
            this.groupBoxStart.Size = new System.Drawing.Size(203, 77);
            this.groupBoxStart.TabIndex = 11;
            this.groupBoxStart.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(634, 480);
            this.Controls.Add(this.groupBoxStart);
            this.Controls.Add(this.groupBoxCaps);
            this.Controls.Add(this.lstInfo);
            this.Controls.Add(this.groupBoxLayout);
            this.Controls.Add(this.groupBoxCaption);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonSelectSource);
            this.Controls.Add(this.buttonShowLayout);
            this.Controls.Add(this.buttonShowCaps);
            this.Controls.Add(this.buttonCloseSession);
            this.Controls.Add(this.buttonStartSession);
            this.Controls.Add(this.buttonOpenSession);
            this.Controls.Add(this.buttonShowCaption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TwainPro 5 Demo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBoxCaption.ResumeLayout(false);
            this.groupBoxCaption.PerformLayout();
            this.groupBoxLayout.ResumeLayout(false);
            this.groupBoxLayout.PerformLayout();
            this.groupBoxCaps.ResumeLayout(false);
            this.groupBoxCaps.PerformLayout();
            this.groupBoxStart.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            Application.EnableVisualStyles();
			Application.Run(new FormMain());
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
            try
            {

                //***Must call the UnlockRuntime method to unlock the control
                //twainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

                twainDevice = new TwainDevice(twainPro1);

                // Set Debug to true to write information to a debug log file
                //twainPro1.Debug = true;
                //twainPro1.ErrorLevel = ErrorLevel.Detailed;

                twainDevice.Scanned += new PegasusImaging.WinForms.TwainPro5.ScannedEventHandler(twainDevice_Scanned);

                comboBoxHAlign.SelectedIndex = 0;
                comboBoxVAlign.SelectedIndex = 0;

                groupBoxCaps.Left = groupBoxStart.Left;
                groupBoxCaption.Left = groupBoxStart.Left;
                groupBoxLayout.Left = groupBoxStart.Left;

                groupBoxCaps.Top = groupBoxStart.Top;
                groupBoxCaption.Top = groupBoxStart.Top;
                groupBoxLayout.Top = groupBoxStart.Top;
                
                _nImageCount = 0;
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

		private void buttonOpenSession_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";
                twainDevice.OpenSession();
				buttonShowCaps.Enabled = true;
				buttonShowLayout.Enabled = true;
                buttonStartSession.Enabled = true;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
				GetError();
			}
		}

        private void buttonStartSession_Click(object sender, System.EventArgs e)
        {
            labelStatus.Text = "";
            buttonShowCaps.Enabled = false;
            buttonShowLayout.Enabled = false;
            UpdateProps();
            
            try
            {
                twainDevice.StartSession();
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
                GetError();
            }
        }

        private void buttonCloseSession_Click(object sender, System.EventArgs e)
        {
            try
            {
                labelStatus.Text = "";
                twainDevice.CloseSession();
                buttonShowCaps.Enabled = false;
                buttonShowLayout.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                GetError();
            }
        }

		private void buttonShowCaps_Click(object sender, System.EventArgs e)
		{
            if (groupBoxCaps.Visible == false)
			{
				buttonShowCaption.Text = "Show Caption";
				buttonShowLayout.Text = "Show Layout";
				buttonShowCaps.Text = "Hide Capabilities"; 
                groupBoxLayout.Visible = false;
                groupBoxStart.Visible = false;
                groupBoxCaption.Visible = false;
                groupBoxCaps.Visible = true;
				comboBoxCaps.SelectedIndex = 0;
			}
			else
			{
				buttonShowCaps.Text = "Show Capabilities";
                groupBoxCaps.Visible = false;
                groupBoxStart.Visible = true;
			}
		}

		private void buttonShowLayout_Click(object sender, System.EventArgs e)
		{
            try
            {
                labelStatus.Text = "";
                if (groupBoxLayout.Visible == false)
                {
                    buttonShowCaption.Text = "Show Caption";
                    buttonShowCaps.Text = "Show Capabilities";
                    buttonShowLayout.Text = "Hide Layout";
                    groupBoxCaption.Visible = false;
                    groupBoxCaps.Visible = false;
                    groupBoxStart.Visible = false;
                    groupBoxLayout.Visible = true;

                    float l = -1, t = -1, r = -1, b = -1;

                    l = twainDevice.ImageLayout.Left;
                    t = twainDevice.ImageLayout.Top;
                    r = twainDevice.ImageLayout.Right;
                    b = twainDevice.ImageLayout.Bottom;

                    textBoxIL.Text = l.ToString();
                    textBoxIT.Text = t.ToString();
                    textBoxIR.Text = r.ToString();
                    textBoxIB.Text = b.ToString();
                }
                else
                {
                    buttonShowLayout.Text = "Show Layout";
                    groupBoxLayout.Visible = false;
                    groupBoxStart.Visible = true;
                }
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
		}

		private void buttonShowCaption_Click(object sender, System.EventArgs e)
		{
			if (groupBoxCaption.Visible == false)
			{
				buttonShowLayout.Text = "Show Layout";
				buttonShowCaps.Text = "Show Capabilities";
				buttonShowCaption.Text = "Hide Caption";
                groupBoxCaps.Visible = false;
                groupBoxStart.Visible = false;
                groupBoxLayout.Visible = false;
                groupBoxCaption.Visible = true;
			}
			else
			{
				buttonShowCaption.Text = "Show Caption";
                groupBoxCaption.Visible = false;
                groupBoxStart.Visible = true;
			}
		}

        private void buttonSelectSource_Click(object sender, System.EventArgs e)
        {
            try
            {
                labelStatus.Text = "";
                twainDevice.CloseSession();
                buttonShowCaps.Enabled = false;
                buttonShowLayout.Enabled = false;
                twainDevice.SelectSource();
                buttonOpenSession.Enabled = true;
                buttonStartSession.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateProps()
        {
            try
            {
                // Copy values from fields to TwainPRO control
                twainDevice.ShowUserInterface = checkBoxShowUI.Checked;
                twainDevice.Caption.Text = textBoxCaption.Text;
                twainDevice.Caption.Clip = checkBoxClipCaption.Checked;
                twainDevice.Caption.ShadowText = checkBoxShadowText.Checked;

                twainDevice.Caption.Area = new System.Drawing.Rectangle(Int32.Parse(textBoxCapLeft.Text), Int32.Parse(textBoxCapTop.Text), Int32.Parse(textBoxCapWidth.Text), Int32.Parse(textBoxCapHeight.Text));

                twainDevice.Caption.HorizontalAlignment = (PegasusImaging.WinForms.TwainPro5.HorizontalAlignment)comboBoxHAlign.SelectedIndex;
                twainDevice.Caption.VerticalAlignment = (PegasusImaging.WinForms.TwainPro5.VerticalAlignment)comboBoxVAlign.SelectedIndex;
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        private void Units()
        {
            try
            {
                labelUnits.Visible = true;
                float unitsFound = 0;
                Capability capabilityUnits = Capability.IcapUnits;
                PegasusImaging.WinForms.TwainPro5.CapabilityContainer capcontainer = twainDevice.GetCapability(capabilityUnits);

                if (capcontainer.GetType().ToString() == "PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum")
                {
                    if (twainDevice.IsCapabilitySupported(capabilityUnits) == true)
                    {
                        PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(capabilityUnits);

                        unitsFound = ((CapabilityContainerOneValueFloat)myCap.CurrentValue).Value;
                    }
                }
                if (capcontainer.GetType().ToString() == "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat")
                {
                    if (twainDevice.IsCapabilitySupported(capabilityUnits) == true)
                    {
                        PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat myCap = (CapabilityContainerOneValueFloat)twainDevice.GetCapability(capabilityUnits);

                        unitsFound = myCap.Value;
                    }
                }

                switch ((int)unitsFound)
                {
                    case 0:
                        labelUnits.Text = "Units of Measure:  Inches";
                        break;
                    case 1:
                        labelUnits.Text = "Units of Measure:  Centimeters";
                        break;
                    case 2:
                        labelUnits.Text = "Units of Measure:  Picas";
                        break;
                    case 3:
                        labelUnits.Text = "Units of Measure:  Points";
                        break;
                    case 4:
                        labelUnits.Text = "Units of Measure:  Twips";
                        break;
                    case 5:
                        labelUnits.Text = "Units of Measure:  Pixels";
                        break;
                    case 6:
                        labelUnits.Text = "Units of Measure:  Millimeters";
                        break;
                }
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        private void comboBoxCaps_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                // Only process when session is open and FrmCaps can only be visible
                // after OpenSession is called
                if (groupBoxCaps.Visible == false)
                {
                    return;
                }

                buttonUpdate.Enabled = true;

                // Reset all the controls in FrmCaps
                labelDefault.Visible = false;
                labelMin.Visible = false;
                labelMax.Visible = false;
                listBoxCaps.Visible = false;
                listBoxCaps.Items.Clear();

                textBoxCurrent.Text = "";
                textBoxCurrent.Enabled = true;

                capability = Capability.IcapAutoBright;

                labelUnits.Visible = false;

                switch (comboBoxCaps.SelectedIndex)
                {
                    case 0:
                        capability = Capability.IcapAutoBright;
                        break;
                    case 1:
                        capability = Capability.IcapBrightness;
                        break;
                    case 2:
                        capability = Capability.IcapContrast;
                        break;
                    case 3:
                        capability = Capability.IcapGamma;
                        break;
                    case 4:
                        capability = Capability.IcapPhysicalWidth;
                        Units();
                        break;
                    case 5:
                        capability = Capability.IcapPhysicalHeight;
                        Units();
                        break;
                    case 6:
                        capability = Capability.IcapPixelType;
                        break;
                    case 7:
                        capability = Capability.IcapRotation;
                        break;
                    case 8:
                        capability = Capability.IcapUnits;
                        break;
                    case 9:
                        capability = Capability.IcapXNativeResolution;
                        Units();
                        break;
                    case 10:
                        capability = Capability.IcapYNativeResolution;
                        Units();
                        break;
                    case 11:
                        capability = Capability.IcapXResolution;
                        Units();
                        break;
                    case 12:
                        capability = Capability.IcapYResolution;
                        Units();
                        break;
                    case 13:
                        capability = Capability.IcapXScaling;
                        break;
                    case 14:
                        capability = Capability.IcapYScaling;
                        break;
                    case 15:
                        capability = Capability.CapDeviceOnline;
                        break;
                    case 16:
                        capability = Capability.CapIndicators;
                        break;
                    case 17:
                        capability = Capability.IcapLampstate;
                        break;
                    case 18:
                        capability = Capability.CapUIControllable;
                        break;
                    case 19:
                        capability = Capability.CapXferCount;
                        break;
                    case 20:
                        capability = Capability.CapAutoFeed;
                        break;
                    case 21:
                        capability = Capability.CapAutoScan;
                        break;
                    case 22:
                        capability = Capability.CapClearPage;
                        break;
                    case 23:
                        capability = Capability.CapDuplex;
                        break;
                    case 24:
                        capability = Capability.CapDuplexEnabled;
                        break;
                    case 25:
                        capability = Capability.CapFeederEnabled;
                        break;
                    case 26:
                        capability = Capability.CapFeederLoaded;
                        break;
                    case 27:
                        capability = Capability.CapFeedPage;
                        break;
                    case 28:
                        capability = Capability.CapPaperDetectable;
                        break;
                }


                // Check for support and stop if none
                if (twainDevice.IsCapabilitySupported(capability) == false)
                {
                    labelDefault.Text = "Not Supported";
                    labelDefault.Visible = true;
                    textBoxCurrent.Enabled = false;
                    buttonUpdate.Enabled = false;
                    return;

                }
                else
                {
                    capcontainer = twainDevice.GetCapability(capability);
                    // What data type is the Cap?  We have to check the data type
                    // to know which properties are valid
                    try
                    {
                        switch (capcontainer.GetType().ToString())
                        {
                            // Type ONEVALUE only returns a single value
                            case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat":
                                {
                                    PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat myCap = (CapabilityContainerOneValueFloat)twainDevice.GetCapability(capability);

                                    textBoxCurrent.Text = myCap.Value.ToString();
                                    textBoxCurrent.Visible = true;
                                    break;
                                }
                            case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueString":
                                {
                                    PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueString myCap = (CapabilityContainerOneValueString)twainDevice.GetCapability(capability);

                                    textBoxCurrent.Text = myCap.Value;
                                    textBoxCurrent.Visible = true;
                                    break;
                                }

                            // Type ENUM returns a list of legal values as well as current and
                            // default values.  A list of constants is returned and the CapDesc
                            // property can be used to find out what the constants mean
                            case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum":
                                {
                                    PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(capability);



                                    textBoxCurrent.Text = ((CapabilityContainerOneValueFloat)myCap.CurrentValue).Value.ToString();
                                    textBoxCurrent.Visible = true;

                                    labelDefault.Text = "Default  =  " + ((CapabilityContainerOneValueFloat)myCap.DefaultValue).Value.ToString();
                                    labelDefault.Visible = true;

                                    listBoxCaps.Items.Add("Legal Values:");

                                    for (int nIndex = 0; nIndex < myCap.Values.Count; nIndex++)
                                    {
                                        float fResult = ((CapabilityContainerOneValueFloat)myCap.Values[nIndex]).Value;// (float)myCap.Values[1];

                                        string strResult = twainDevice.GetCapabilityConstantDescription(capability, nIndex);

                                        if (strResult == null)
                                        {
                                            strResult = "";
                                        }

                                        listBoxCaps.Items.Add(fResult.ToString() + " - " + strResult.ToString());
                                    }
                                    listBoxCaps.Visible = true;
                                    break;
                                }

                            // Type ARRAY returns a list of values, but no current or default values
                            // This is a less common type that many sources don't use
                            case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerArray":
                                {
                                    PegasusImaging.WinForms.TwainPro5.CapabilityContainerArray myCap = (CapabilityContainerArray)twainDevice.GetCapability(capability);


                                    listBoxCaps.Items.Add("Legal Values:");
                                    for (int nIndex = 0; nIndex < myCap.Values.Count; nIndex++)
                                    {
                                        float fResult = ((CapabilityContainerOneValueFloat)myCap.Values[nIndex]).Value;
                                        listBoxCaps.Items.Add(fResult);
                                    }
                                    break;
                                }

                            // Returns a range of values as well as current and default values
                            case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerRange":
                                {
                                    PegasusImaging.WinForms.TwainPro5.CapabilityContainerRange myCap = (CapabilityContainerRange)twainDevice.GetCapability(capability);


                                    textBoxCurrent.Text = myCap.Value.ToString();
                                    textBoxCurrent.Visible = true;

                                    labelDefault.Text = "Default  =  " + myCap.Default.ToString();
                                    labelDefault.Visible = true;

                                    labelMin.Text = "MinValue  =  " + myCap.Minimum.ToString(); ;
                                    labelMin.Visible = true;

                                    labelMax.Text = "MaxValue  =  " + myCap.Maximum.ToString();
                                    labelMax.Visible = true;
                                    break;
                                }
                        }

                        if (capability == Capability.IcapAutoBright | capability == Capability.CapDeviceOnline
                          | capability == Capability.CapIndicators | capability == Capability.CapUIControllable
                          | capability == Capability.IcapLampstate | capability == Capability.CapAutoFeed
                          | capability == Capability.CapAutoScan | capability == Capability.CapClearPage
                          | capability == Capability.CapDuplexEnabled | capability == Capability.CapFeederEnabled
                          | capability == Capability.CapFeederLoaded | capability == Capability.CapFeedPage
                          | capability == Capability.CapPaperDetectable)
                        {
                            if (textBoxCurrent.Text == "0")
                            {
                                textBoxCurrent.Text = "False";
                            }
                            else
                            {
                                textBoxCurrent.Text = "True";
                            }
                        }

                        if (capability == Capability.IcapPhysicalHeight | capability == Capability.IcapPhysicalWidth
 | capability == Capability.IcapXNativeResolution | capability == Capability.IcapYNativeResolution
 | capability == Capability.CapDeviceOnline | capability == Capability.CapUIControllable
 | capability == Capability.CapAutoScan | capability == Capability.CapClearPage
 | capability == Capability.CapDuplex | capability == Capability.CapFeederLoaded
 | capability == Capability.CapPaperDetectable)
                        {
                            buttonUpdate.Enabled = false;
                        }

                        labelStatus.Text = "";
                    }
                    catch (System.InvalidCastException ex)
                    {
                        labelStatus.Text = ex.Message;
                    }
                    catch (System.StackOverflowException ex)
                    {
                        labelStatus.Text = ex.Message;
                    }
                }
            }


            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

		// Update the current Capability
		// This can only be done after calling OpenSession and before calling
        // StartSession (ie. in state 4 as described in the Twain spec)
		private void buttonUpdate_Click(object sender, System.EventArgs e)
		{
            try
            {
                labelStatus.Text = "";

                switch (capcontainer.GetType().ToString())
                {
                    case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum":
                    case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat":
                        {
                            try
                            {
                                PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat myCap4 = new CapabilityContainerOneValueFloat(capability);
                                myCap4.Value = Single.Parse(textBoxCurrent.Text);

                                twainDevice.SetCapability(myCap4);

                                // Force an update of the Caps frame with the new values
                                comboBoxCaps.SelectedIndex = comboBoxCaps.SelectedIndex;

                                labelStatus.Text = "Capability set!";
                            }
                            catch (FormatException ex)
                            {
                                labelStatus.Text = ex.Message;
                            }
                            break;
                        }
                }
            }
            catch (PegasusImaging.WinForms.TwainPro5.TwainException ex)
            {               
                labelStatus.Text = ex.Message;
            }
            catch (FormatException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.AccessViolationException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.StackOverflowException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
		}
		
		// Update the Image Layout
		// This can only be done after calling OpenSession and before calling
        // StartSession (ie. in state 4 as described in the Twain spec)
        private void buttonLayoutUpdate_Click(object sender, System.EventArgs e)
        {
            try
            {
                labelStatus.Text = "";
                int l = System.Int32.Parse(textBoxIL.Text);
                int t = System.Int32.Parse(textBoxIT.Text);
                int r = System.Int32.Parse(textBoxIR.Text);
                int b = System.Int32.Parse(textBoxIB.Text);

                twainDevice.ImageLayout = new System.Drawing.Rectangle(l, t, r, b);
            }
            catch (FormatException)
            {
                MessageBox.Show("You must enter a real number for each field.");
                GetError();
            }
        }

        public void twainDevice_Scanned(object sender, PegasusImaging.WinForms.TwainPro5.ScannedEventArgs e)
        {
            try
            {
                FormViewer viewer = new FormViewer();
                
                viewer.AddImage(sender, e);

                if (checkBoxSave.Checked)
                {
                    string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
                    string strPath = System.IO.Path.Combine(strCurrentDir, "image-" + _nImageCount.ToString() + ".tif");

                    PegasusImaging.WinForms.ImagXpress9.SaveOptions so = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
                    so.Format = PegasusImaging.WinForms.ImagXpress9.ImageXFormat.Tiff;
                    so.Tiff.Compression = PegasusImaging.WinForms.ImagXpress9.Compression.Group4;

                    viewer.imageXView1.Image.Save(strPath,so);

                    _nImageCount++;

                    checkBoxSave.Text = "Save Image as  Image-" + _nImageCount.ToString() + ".tif";
                }

                Application.DoEvents();
            }

            catch (PegasusImaging.WinForms.TwainPro5.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

		//  Update the status bar with the current error code and description
		private void GetError() 
		{
			if ((twainPro1.ErrorLevel != 0)) 
			{
				labelStatus.Text = ("Data Source Error " 
					+ twainPro1.ErrorLevel);
			}
		}

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
			 Application.Exit();
		}

        private void TwainProMenuItem_Click(object sender, EventArgs e)
        {
            twainPro1.AboutBox();
        }

        private void ImagXpressMenuItem_Click(object sender, EventArgs e)
        {
            FormViewer frm = new FormViewer();
            frm.imagXpress1.AboutBox();
        }		
	}
}

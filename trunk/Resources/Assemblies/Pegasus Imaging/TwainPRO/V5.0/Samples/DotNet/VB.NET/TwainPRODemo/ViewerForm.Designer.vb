<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewerForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ImageXView1.Location = New System.Drawing.Point(0, 0)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(594, 436)
        Me.ImageXView1.TabIndex = 0
        '
        'ViewerForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 436)
        Me.Controls.Add(Me.ImageXView1)
        Me.MinimizeBox = False
        Me.Name = "ViewerForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Viewer"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
End Class

/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;

namespace TwainProDemoCSharp
{
	/// <summary>
	/// Summary description for FormViewer.
	/// </summary>
	public class FormViewer : System.Windows.Forms.Form
    {
        private IContainer components;
        public PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        public PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;

		public FormViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(12, 12);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(442, 344);
            this.imageXView1.TabIndex = 0;
            // 
            // FormViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(466, 368);
            this.Controls.Add(this.imageXView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormViewer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Viewer";
            this.Load += new System.EventHandler(this.FormViewer_Load);
            this.ResumeLayout(false);

		}
		#endregion

        public void AddImage(Object sender, PegasusImaging.WinForms.TwainPro5.ScannedEventArgs e)
        {
            imageXView1.AutoScroll = true;
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(imagXpress1, e.ScannedImage.ToHdib());

            if (this.Visible == false)
            {
                this.ShowDialog();
            }
        }

        private void FormViewer_Load(object sender, EventArgs e)
        {
            //***Must have the UnlockRuntime call to unlock the control
            //imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);
        }
	}
}

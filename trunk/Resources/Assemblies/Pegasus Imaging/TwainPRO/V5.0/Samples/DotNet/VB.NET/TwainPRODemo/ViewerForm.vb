Public Class ViewerForm

    Private Sub ViewerForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '***Must have the UnlockRuntime call to unlock the control
        'imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
    End Sub

    Public Sub AddImage(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.TwainPro5.ScannedEventArgs)
        ImageXView1.AutoScroll = True
        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(ImagXpress1, e.ScannedImage.ToHdib())

        If (Visible = False) Then
            ShowDialog()
        End If
    End Sub
End Class
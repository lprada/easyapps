Imports PegasusImaging.WinForms.TwainPro5

Public Class MainForm

    Dim twainDevice As PegasusImaging.WinForms.TwainPro5.TwainDevice
    Dim capability As PegasusImaging.WinForms.TwainPro5.Capability
    Dim capcontainer As PegasusImaging.WinForms.TwainPro5.CapabilityContainer

    Dim _nImageCount As Int32


    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            '***Must call the UnlockRuntime method to unlock the control
            'twainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)


            Application.EnableVisualStyles()

            twainDevice = New TwainDevice(TwainPro1)

            ' Set the Debug property to True to write information to a debug log file
            'TwainPro1.Debug = True
            'TwainPro1.ErrorLevel = ErrorLevel.Detailed

            AddHandler twainDevice.Scanned, AddressOf twainDevice_Scanned

            HComboBox.SelectedIndex = 0
            VComboBox.SelectedIndex = 0

            CapabilitiesGroupBox.Left = DefaultGroupBox.Left
            CaptionGroupBox.Left = DefaultGroupBox.Left
            LayoutGroupBox.Left = DefaultGroupBox.Left

            CaptionGroupBox.Top = DefaultGroupBox.Top
            CapabilitiesGroupBox.Top = DefaultGroupBox.Top
            LayoutGroupBox.Top = DefaultGroupBox.Top

            _nImageCount = 0

        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        Catch Ex As System.Exception

            ErrorLabel.Text = Ex.Message

        End Try

    End Sub

    Public Sub twainDevice_Scanned(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.TwainPro5.ScannedEventArgs)
        Try

            Dim viewer As ViewerForm = New ViewerForm()

            viewer.AddImage(sender, e)

            If (SaveCheckBox.Checked) Then

                Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
                Dim strPath As String = System.IO.Path.Combine(strCurrentDir, "image-" + _nImageCount.ToString() + ".tif")

                Dim so As PegasusImaging.WinForms.ImagXpress9.SaveOptions = New PegasusImaging.WinForms.ImagXpress9.SaveOptions()
                so.Format = PegasusImaging.WinForms.ImagXpress9.ImageXFormat.Tiff
                so.Tiff.Compression = PegasusImaging.WinForms.ImagXpress9.Compression.Group4

                viewer.ImageXView1.Image.Save(strPath, so)

                _nImageCount = _nImageCount + 1

                SaveCheckBox.Text = "Save Image as  Image-" + _nImageCount.ToString() + ".tif"

            End If
            Application.DoEvents()



        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try

    End Sub

    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenButton.Click
        Try

            ErrorLabel.Text = ""
            twainDevice.OpenSession()
            CapButton.Enabled = True
            LayoutButton.Enabled = True
            StartButton.Enabled = True

        Catch ex As Exception

            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub

    Private Sub GetError()
        If Not ((TwainPro1.ErrorLevel = 0)) Then
            ErrorLabel.Text = ("Data Source Error " & TwainPro1.ErrorLevel)
        End If
    End Sub

    Private Sub StartButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartButton.Click
        ErrorLabel.Text = ""
        CapButton.Enabled = False
        LayoutButton.Enabled = False
        UpdateProps()

        Try

            twainDevice.StartSession()

        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
            GetError()

        End Try

    End Sub

    Private Sub UpdateProps()
        Try

            ' Copy values from fields to TwainPRO control
            twainDevice.ShowUserInterface = UICheckBox.Checked
            twainDevice.Caption.Text = CaptionTextBox.Text
            twainDevice.Caption.Clip = ClipCheckBox.Checked
            twainDevice.Caption.ShadowText = ShadowCheckBox.Checked

            twainDevice.Caption.Area = New System.Drawing.Rectangle(Int32.Parse(CapLTextBox.Text), Int32.Parse(CapTTextBox.Text), Int32.Parse(CapWTextBox.Text), Int32.Parse(CapHTextBox.Text))

            twainDevice.Caption.HorizontalAlignment = CType(HComboBox.SelectedIndex, PegasusImaging.WinForms.TwainPro5.HorizontalAlignment)
            twainDevice.Caption.VerticalAlignment = CType(VComboBox.SelectedIndex, PegasusImaging.WinForms.TwainPro5.HorizontalAlignment)

            catch ex as PegasusImaging.WinForms.TwainPro5.TwainProException
            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Try

            ErrorLabel.Text = ""
            twainDevice.CloseSession()
            CapButton.Enabled = False
            LayoutButton.Enabled = False

        Catch ex As Exception

            MessageBox.Show(ex.Message)
            GetError()
        End Try

    End Sub

    Private Sub CapButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CapButton.Click
        If (CapabilitiesGroupBox.Visible = False) Then

            CaptionButton.Text = "Show Caption"
            LayoutButton.Text = "Show Layout"
            CapButton.Text = "Hide Capabilities"
            LayoutGroupBox.Visible = False
            DefaultGroupBox.Visible = False
            captiongroupbox.Visible = False
            CapabilitiesGroupBox.Visible = True
            CapComboBox.SelectedIndex = 0

        Else

            CapButton.Text = "Show Capabilities"
            CapabilitiesGroupBox.Visible = False
            DefaultGroupBox.Visible = True
        End If
    End Sub

    Private Sub LayoutButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LayoutButton.Click
        Try

            ErrorLabel.Text = ""
            If (LayoutGroupBox.Visible = False) Then

                CaptionButton.Text = "Show Caption"
                CapButton.Text = "Show Capabilities"
                LayoutButton.Text = "Hide Layout"
                CaptionGroupBox.Visible = False
                CapabilitiesGroupBox.Visible = False
                DefaultGroupBox.Visible = False
                LayoutGroupBox.Visible = True

                Dim l As Single = -1
                Dim t As Single = -1
                Dim r As Single = -1
                Dim b As Single = -1

                l = twainDevice.ImageLayout.Left
                t = twainDevice.ImageLayout.Top
                r = twainDevice.ImageLayout.Right
                b = twainDevice.ImageLayout.Bottom

                LayoutLTextBox.Text = l.ToString()
                LayoutTTextBox.Text = t.ToString()
                LayoutrTextBox.Text = r.ToString()
                LayoutBTextBox.Text = b.ToString()

            Else

                LayoutButton.Text = "Show Layout"
                LayoutGroupBox.Visible = False
                DefaultGroupBox.Visible = True

            End If
        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message

        End Try
    End Sub

    Private Sub CaptionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CaptionButton.Click
        If (CaptionGroupBox.Visible = False) Then

            LayoutButton.Text = "Show Layout"
            CapButton.Text = "Show Capabilities"
            CaptionButton.Text = "Hide Caption"
            CapabilitiesGroupBox.Visible = False
            DefaultGroupBox.Visible = False
            LayoutGroupBox.Visible = False
            CaptionGroupBox.Visible = True

        Else

            CaptionButton.Text = "Show Caption"
            CaptionGroupBox.Visible = False
            DefaultGroupBox.Visible = True
        End If
    End Sub

    Private Sub SourceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SourceButton.Click
        Try

            ErrorLabel.Text = ""
            twainDevice.CloseSession()
            CapButton.Enabled = False
            LayoutButton.Enabled = False
            twainDevice.SelectSource()
            OpenButton.Enabled = True
            StartButton.Enabled = True

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Units()
        Try

            labelUnits.Visible = True
            Dim unitsFound As Single = 0
            Dim capabilityUnits As Capability = capability.IcapUnits
            Dim capcontainer As CapabilityContainer = twainDevice.GetCapability(capabilityUnits)

            If (capcontainer.GetType().ToString() = "PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum") Then

                If (twainDevice.IsCapabilitySupported(capabilityUnits) = True) Then

                    Dim myCap As CapabilityContainerEnum = CType(twainDevice.GetCapability(capabilityUnits), CapabilityContainerEnum)

                    unitsFound = CType(myCap.CurrentValue, CapabilityContainerOneValueFloat).Value
                End If
            End If
            If (capcontainer.GetType().ToString() = "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat") Then

                If (twainDevice.IsCapabilitySupported(capabilityUnits) = True) Then

                    Dim myCap As CapabilityContainerOneValueFloat = CType(twainDevice.GetCapability(capabilityUnits), CapabilityContainerOneValueFloat)

                    unitsFound = myCap.Value
                End If
            End If

            Select Case (unitsFound)

                Case 0
                    LabelUnits.Text = "Units of Measure:  Inches"
                Case 1
                    LabelUnits.Text = "Units of Measure:  Centimeters"
                Case 2
                    LabelUnits.Text = "Units of Measure:  Picas"
                Case 3
                    LabelUnits.Text = "Units of Measure:  Points"
                Case 4
                    LabelUnits.Text = "Units of Measure:  Twips"
                Case 5
                    LabelUnits.Text = "Units of Measure:  Pixels"
                Case 6
                    LabelUnits.Text = "Units of Measure:  Millimeters"
            End Select

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

    Private Sub CapComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CapComboBox.SelectedIndexChanged
        Try

            'Only process when session is open and FrmCaps can only be visible
            ' after OpenSession is called
            If (CapabilitiesGroupBox.Visible = False) Then

                Return
            End If

            UpdateButton.Enabled = True

            ' Reset all the controls in FrmCaps
            DefaultLabel.Visible = False
            MinLabel.Visible = False
            MaxLabel.Visible = False
            CapListBox.Visible = False
            CapListBox.Items.Clear()

            CurrentTextBox.Text = ""
            CurrentTextBox.Enabled = True

            capability = capability.IcapAutoBright

            LabelUnits.Visible = False

            Select Case (CapComboBox.SelectedIndex)

                Case 0
                    capability = capability.IcapAutoBright
                Case 1
                    capability = capability.IcapBrightness
                Case 2
                    capability = capability.IcapContrast
                Case 3
                    capability = capability.IcapGamma
                Case 4
                    capability = capability.IcapPhysicalWidth
                    Units()
                Case 5
                    capability = capability.IcapPhysicalHeight
                    Units()
                Case 6
                    capability = capability.IcapPixelType
                Case 7
                    capability = capability.IcapRotation
                Case 8
                    capability = capability.IcapUnits
                Case 9
                    capability = capability.IcapXNativeResolution
                    Units()
                Case 10
                    capability = capability.IcapYNativeResolution
                    Units()
                Case 11
                    capability = capability.IcapXResolution
                    Units()
                Case 12
                    capability = capability.IcapYResolution
                    Units()
                Case 13
                    capability = capability.IcapXScaling
                Case 14
                    capability = capability.IcapYScaling
                Case 15
                    capability = capability.CapDeviceOnline
                Case 16
                    capability = capability.CapIndicators
                Case 17
                    capability = capability.IcapLampstate
                Case 18
                    capability = capability.CapUIControllable

                Case 19
                    capability = capability.CapXferCount
                Case 20
                    capability = capability.CapAutoFeed
                Case 21
                    capability = capability.CapAutoScan
                Case 22
                    capability = capability.CapClearPage
                Case 23
                    capability = capability.CapDuplex
                Case 24
                    capability = capability.CapDuplexEnabled
                Case 25
                    capability = capability.CapFeederEnabled
                Case 26
                    capability = capability.CapFeederLoaded
                Case 27
                    capability = capability.CapFeedPage
                Case 28
                    capability = capability.CapPaperDetectable
            End Select


            ' Check for support and stop if none
            If (twainDevice.IsCapabilitySupported(capability) = False) Then

                DefaultLabel.Text = "Not Supported"
                DefaultLabel.Visible = True
                CurrentTextBox.Enabled = False
                UpdateButtonLayout.Enabled = False
                Return

            Else

                capcontainer = twainDevice.GetCapability(capability)
                ' What data type is the Cap?  We have to check the data type
                ' to know which properties are valid
                Try

                    Select Case (capcontainer.GetType().ToString())

                        ' Type ONEVALUE only returns a single value
                        Case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat"

                            Dim myCap As PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat = CType(twainDevice.GetCapability(capability), CapabilityContainerOneValueFloat)

                            CurrentTextBox.Text = myCap.Value.ToString()
                            CurrentTextBox.Visible = True


                        Case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueString"

                            Dim myCap As PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueString = CType(twainDevice.GetCapability(capability), CapabilityContainerOneValueString)

                            CurrentTextBox.Text = myCap.Value
                            CurrentTextBox.Visible = True



                            ' Type ENUM returns a list of legal values as well as current and
                            ' default values.  A list of constants is returned and the CapDesc
                            ' property can be used to find out what the constants mean
                        Case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum"

                            Dim myCap As PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum = CType(twainDevice.GetCapability(capability), CapabilityContainerEnum)



                            CurrentTextBox.Text = CType(myCap.CurrentValue, CapabilityContainerOneValueFloat).Value.ToString()
                            CurrentTextBox.Visible = True

                            DefaultLabel.Text = "Default  =  " + CType(myCap.DefaultValue, CapabilityContainerOneValueFloat).Value.ToString()
                            DefaultLabel.Visible = True

                            CapListBox.Items.Add("Legal Values:")

                            Dim nIndex As Int32
                            For nIndex = 0 To myCap.Values.Count - 1

                                Dim fResult As Single = CType(myCap.Values(nIndex), CapabilityContainerOneValueFloat).Value

                                Dim strResult As String = twainDevice.GetCapabilityConstantDescription(capability, nIndex)

                                If (strResult Is Nothing) Then

                                    strResult = ""
                                End If

                                CapListBox.Items.Add(fResult.ToString() + " - " + strResult.ToString())
                            Next nIndex
                            CapListBox.Visible = True



                            ' Type ARRAY returns a list of values, but no current or default values
                            ' This is a less common type that many sources don't use
                        Case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerArray"

                            Dim myCap As PegasusImaging.WinForms.TwainPro5.CapabilityContainerArray = CType(twainDevice.GetCapability(capability), CapabilityContainerArray)


                            CapListBox.Items.Add("Legal Values:")
                            Dim nIndex As Int32

                            For nIndex = 0 To myCap.Values.Count - 1
                                Dim fResult As Single = CType(myCap.Values(nIndex), CapabilityContainerOneValueFloat).Value
                                CapListBox.Items.Add(fResult)
                            Next nIndex



                            ' Returns a range of values as well as current and default values
                        Case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerRange"

                            Dim myCap As PegasusImaging.WinForms.TwainPro5.CapabilityContainerRange = CType(twainDevice.GetCapability(capability), CapabilityContainerRange)

                            CurrentTextBox.Text = myCap.Value.ToString()
                            CurrentTextBox.Visible = True

                            DefaultLabel.Text = "Default  =  " + myCap.Default.ToString()
                            DefaultLabel.Visible = True

                            MinLabel.Text = "MinValue  =  " + myCap.Minimum.ToString()
                            MinLabel.Visible = True

                            MaxLabel.Text = "MaxValue  =  " + myCap.Maximum.ToString()
                            MaxLabel.Visible = True


                    End Select

                    If (capability = capability.IcapAutoBright Or capability = capability.CapDeviceOnline Or capability = capability.CapIndicators Or capability = capability.CapUIControllable Or capability = capability.IcapLampstate Or capability = capability.CapAutoFeed Or capability = capability.CapAutoScan Or capability = capability.CapClearPage Or capability = capability.CapDuplexEnabled Or capability = capability.CapFeederEnabled Or capability = capability.CapFeederLoaded Or capability = capability.CapFeedPage Or capability = capability.CapPaperDetectable) Then
                        If (CurrentTextBox.Text = "0") Then

                            CurrentTextBox.Text = "False"
                        Else
                            CurrentTextBox.Text = "True"
                        End If
                    End If

                    If (capability = capability.IcapPhysicalHeight Or capability = capability.IcapPhysicalWidth Or capability = capability.IcapXNativeResolution Or capability = capability.IcapYNativeResolution Or capability = capability.CapDeviceOnline Or capability = capability.CapUIControllable Or capability = capability.CapAutoScan Or capability = capability.CapClearPage Or capability = capability.CapDuplex Or capability = capability.CapFeederLoaded Or capability = capability.CapPaperDetectable) Then
                        UpdateButton.Enabled = False
                    End If

                    ErrorLabel.Text = ""

                Catch ex As System.InvalidCastException
                    ErrorLabel.Text = ex.Message
                Catch ex As System.StackOverflowException
                    ErrorLabel.Text = ex.Message
                End Try
            End If

        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

   
    Private Sub UpdateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateButton.Click
        Try

            ErrorLabel.Text = ""

            Dim test As String
            test = capcontainer.GetType().ToString()

            Select Case (test)

                Case "PegasusImaging.WinForms.TwainPro5.CapabilityContainerEnum", "PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat"

                    Try

                        Dim myCap4 As PegasusImaging.WinForms.TwainPro5.CapabilityContainerOneValueFloat = New CapabilityContainerOneValueFloat(capability)
                        myCap4.Value = Single.Parse(CurrentTextBox.Text)

                        twainDevice.SetCapability(myCap4)

                        ' Force an update of the Caps frame with the new values
                        CapComboBox.SelectedIndex = CapComboBox.SelectedIndex

                        ErrorLabel.Text = "Capability set!"

                    Catch ex As FormatException

                        ErrorLabel.Text = ex.Message
                    End Try
            End Select



        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainException

            ErrorLabel.Text = ex.Message

        Catch ex As FormatException

            ErrorLabel.Text = ex.Message

        Catch ex As System.AccessViolationException

            ErrorLabel.Text = ex.Message

        Catch ex As System.StackOverflowException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try

    End Sub

    ' Update the Image Layout
    ' This can only be done after calling OpenSession and before calling
    ' StartSession (ie. in state 4 as described in the Twain spec)
    Private Sub UpdateButtonLayout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateButtonLayout.Click
        Try

            ErrorLabel.Text = ""
            Dim l As Int32 = System.Int32.Parse(LayoutLTextBox.Text)
            Dim t As Int32 = System.Int32.Parse(LayoutTTextBox.Text)
            Dim r As Int32 = System.Int32.Parse(LayoutRTextBox.Text)
            Dim b As Int32 = System.Int32.Parse(LayoutBTextBox.Text)

            twainDevice.ImageLayout = New System.Drawing.Rectangle(l, t, r, b)

        Catch ex As System.FormatException

            MessageBox.Show("You must enter a real number for each field.")
            GetError()
        End Try
    End Sub

    Private Sub TwainProToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TwainProToolStripMenuItem.Click
        TwainPro1.AboutBox()
    End Sub

    Private Sub ImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressToolStripMenuItem.Click
        Dim frm As ViewerForm = New ViewerForm()
        frm.ImagXpress1.AboutBox()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub
End Class

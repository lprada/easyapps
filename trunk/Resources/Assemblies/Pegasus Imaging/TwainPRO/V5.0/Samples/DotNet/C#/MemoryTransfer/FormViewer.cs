/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.TwainPro5;

namespace TwainProMemCSharp
{
	/// <summary>
	/// Summary description for FormViewer.
	/// </summary>
	public class FormViewer : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonZoomIn;
		private System.Windows.Forms.Button buttonMirror;
		private System.Windows.Forms.Button buttonFlip;
		private System.Windows.Forms.Button buttonInvert;
		private System.Windows.Forms.Button buttonDeskew;
		private System.Windows.Forms.Button buttonDespeckle;
		private System.Windows.Forms.Button buttonZoomOut;
		private System.Windows.Forms.Button buttonFit;

		private System.Double dZoomFactor;

        private ImagXpress imagXpress1;
        private ImageXView imageXView1;
        private Processor processor1;
        private IContainer components;

		public FormViewer()
		{

			InitializeComponent();

			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{	
				// Don't forget to dispose IX
				//
                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }	
                if (imageXView1 != null)
                {
                    imageXView1.Dispose();
                    imageXView1 = null;
                }							
				if (processor1 != null)
				{
					processor1.Dispose();
					processor1 = null;
				}
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.buttonZoomIn = new System.Windows.Forms.Button();
            this.buttonMirror = new System.Windows.Forms.Button();
            this.buttonFlip = new System.Windows.Forms.Button();
            this.buttonInvert = new System.Windows.Forms.Button();
            this.buttonDeskew = new System.Windows.Forms.Button();
            this.buttonDespeckle = new System.Windows.Forms.Button();
            this.buttonZoomOut = new System.Windows.Forms.Button();
            this.buttonFit = new System.Windows.Forms.Button();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.processor1 = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.SuspendLayout();
            // 
            // buttonZoomIn
            // 
            this.buttonZoomIn.Location = new System.Drawing.Point(3, 0);
            this.buttonZoomIn.Name = "buttonZoomIn";
            this.buttonZoomIn.Size = new System.Drawing.Size(64, 24);
            this.buttonZoomIn.TabIndex = 1;
            this.buttonZoomIn.Text = "Zoom In";
            this.buttonZoomIn.Click += new System.EventHandler(this.buttonZoomIn_Click);
            // 
            // buttonMirror
            // 
            this.buttonMirror.Location = new System.Drawing.Point(459, 0);
            this.buttonMirror.Name = "buttonMirror";
            this.buttonMirror.Size = new System.Drawing.Size(64, 24);
            this.buttonMirror.TabIndex = 2;
            this.buttonMirror.Text = "Mirror";
            this.buttonMirror.Click += new System.EventHandler(this.buttonMirror_Click);
            // 
            // buttonFlip
            // 
            this.buttonFlip.Location = new System.Drawing.Point(395, 0);
            this.buttonFlip.Name = "buttonFlip";
            this.buttonFlip.Size = new System.Drawing.Size(64, 24);
            this.buttonFlip.TabIndex = 3;
            this.buttonFlip.Text = "Flip";
            this.buttonFlip.Click += new System.EventHandler(this.buttonFlip_Click);
            // 
            // buttonInvert
            // 
            this.buttonInvert.Location = new System.Drawing.Point(331, 0);
            this.buttonInvert.Name = "buttonInvert";
            this.buttonInvert.Size = new System.Drawing.Size(64, 24);
            this.buttonInvert.TabIndex = 4;
            this.buttonInvert.Text = "Invert";
            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
            // 
            // buttonDeskew
            // 
            this.buttonDeskew.Location = new System.Drawing.Point(267, 0);
            this.buttonDeskew.Name = "buttonDeskew";
            this.buttonDeskew.Size = new System.Drawing.Size(64, 24);
            this.buttonDeskew.TabIndex = 5;
            this.buttonDeskew.Text = "Deskew";
            this.buttonDeskew.Click += new System.EventHandler(this.buttonDeskew_Click);
            // 
            // buttonDespeckle
            // 
            this.buttonDespeckle.Location = new System.Drawing.Point(195, 0);
            this.buttonDespeckle.Name = "buttonDespeckle";
            this.buttonDespeckle.Size = new System.Drawing.Size(72, 24);
            this.buttonDespeckle.TabIndex = 6;
            this.buttonDespeckle.Text = "Despeckle";
            this.buttonDespeckle.Click += new System.EventHandler(this.buttonDespeckle_Click);
            // 
            // buttonZoomOut
            // 
            this.buttonZoomOut.Location = new System.Drawing.Point(131, 0);
            this.buttonZoomOut.Name = "buttonZoomOut";
            this.buttonZoomOut.Size = new System.Drawing.Size(64, 24);
            this.buttonZoomOut.TabIndex = 7;
            this.buttonZoomOut.Text = "Zoom Out";
            this.buttonZoomOut.Click += new System.EventHandler(this.buttonZoomOut_Click);
            // 
            // buttonFit
            // 
            this.buttonFit.Location = new System.Drawing.Point(67, 0);
            this.buttonFit.Name = "buttonFit";
            this.buttonFit.Size = new System.Drawing.Size(64, 24);
            this.buttonFit.TabIndex = 8;
            this.buttonFit.Text = "Fit";
            this.buttonFit.Click += new System.EventHandler(this.buttonFit_Click);
            // 
            // imageXView1
            // 
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(3, 30);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(523, 387);
            this.imageXView1.TabIndex = 9;
            // 
            // processor1
            // 
            this.processor1.BackgroundColor = System.Drawing.Color.Black;
            this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.processor1.ProgressPercent = 10;
            this.processor1.Redeyes = null;
            // 
            // FormViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(530, 429);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.buttonFit);
            this.Controls.Add(this.buttonZoomOut);
            this.Controls.Add(this.buttonDespeckle);
            this.Controls.Add(this.buttonDeskew);
            this.Controls.Add(this.buttonInvert);
            this.Controls.Add(this.buttonFlip);
            this.Controls.Add(this.buttonMirror);
            this.Controls.Add(this.buttonZoomIn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormViewer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Viewer";
            this.ResumeLayout(false);

		}
		#endregion

		private void buttonZoomIn_Click(object sender, System.EventArgs e)
		{
		
			dZoomFactor += 0.05;
			try 
			{
                //imageXView1.AutoResize = AutoResizeType.CropImage;
				imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonFit_Click(object sender, System.EventArgs e)
		{
			
			try 
			{
				imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress9.ZoomToFitType.FitBest);
				dZoomFactor = imageXView1.ZoomFactor;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
			
		}

		private void buttonZoomOut_Click(object sender, System.EventArgs e)
		{
			dZoomFactor -= 0.05;
			try 
			{
                //imageXView1.AutoResize = AutoResizeType.CropImage;
				imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonDespeckle_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Despeckle();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonDeskew_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Deskew();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonInvert_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Negate();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonFlip_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Flip();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonMirror_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Mirror();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		public void ShowEx (PegasusImaging.WinForms.ImagXpress9.ImageX im, bool bIsHDib)
		{
			if (bIsHDib)
			{
				imageXView1.Image = im;
			}
			else	
			{
				imageXView1.Image = im;
			}

			dZoomFactor = Convert.ToDouble(imageXView1.ZoomFactor);

			if (this.Visible == false)
			{
                this.Show();
                imageXView1.ZoomToFit(ZoomToFitType.FitBest);
                dZoomFactor = Convert.ToDouble(imageXView1.ZoomFactor);
			}
			else
			{
				this.Invalidate ();
			}   
		
		}
	}
}

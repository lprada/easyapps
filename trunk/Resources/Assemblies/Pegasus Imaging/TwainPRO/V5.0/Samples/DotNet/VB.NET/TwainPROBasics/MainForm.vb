'****************************************************************
' Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
' This sample code is provided to Pegasus licensees "as is"    *
' with no restrictions on use or modification. No warranty for *
' use of this sample code is provided by Pegasus.              *
'***************************************************************/
Public Class MainForm

    Dim twainDevice As PegasusImaging.WinForms.TwainPro5.TwainDevice
    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Application.EnableVisualStyles()


            '***Must call the UnlockRuntime method to unlock the control
            'TwainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
            'ImagXpress1.Licensing.UnlockRuntime(5678, 5678, 5678, 5678)

            '***Setup the device object
            twainDevice = New PegasusImaging.WinForms.TwainPro5.TwainDevice(TwainPro1)
            AddHandler twainDevice.Scanned, AddressOf twainDevice_Scanned

        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        End Try
    End Sub

    Public Sub twainDevice_Scanned(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.TwainPro5.ScannedEventArgs)
        Try

            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromHdib(ImagXpress1, e.ScannedImage.ToHdib(), True)

        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        End Try
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub ImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressToolStripMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub TwainProToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TwainProToolStripMenuItem.Click
        TwainPro1.AboutBox()
    End Sub

    Private Sub NoUIButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoUIButton.Click
        Try

            twainDevice.ShowUserInterface = False
            twainDevice.SelectSource()
            twainDevice.StartSession()


        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        End Try
    End Sub

    Private Sub UIButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UIButton.Click
        Try

            twainDevice.ShowUserInterface = True
            twainDevice.SelectSource()
            twainDevice.StartSession()

        Catch ex As PegasusImaging.WinForms.TwainPro5.TwainProException

            ErrorLabel.Text = ex.Message

        End Try
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.UIButton = New System.Windows.Forms.Button
        Me.NoUIButton = New System.Windows.Forms.Button
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.DescriptionListBox = New System.Windows.Forms.ListBox
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TwainProToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ErrorLabel = New System.Windows.Forms.Label
        Me.TwainPro1 = New PegasusImaging.WinForms.TwainPro5.TwainPro(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UIButton
        '
        Me.UIButton.Location = New System.Drawing.Point(98, 393)
        Me.UIButton.Name = "UIButton"
        Me.UIButton.Size = New System.Drawing.Size(150, 37)
        Me.UIButton.TabIndex = 0
        Me.UIButton.Text = "Scan with Scanner UI"
        Me.UIButton.UseVisualStyleBackColor = True
        '
        'NoUIButton
        '
        Me.NoUIButton.Location = New System.Drawing.Point(331, 393)
        Me.NoUIButton.Name = "NoUIButton"
        Me.NoUIButton.Size = New System.Drawing.Size(145, 37)
        Me.NoUIButton.TabIndex = 1
        Me.NoUIButton.Text = "Scan without Scanner UI"
        Me.NoUIButton.UseVisualStyleBackColor = True
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(12, 102)
        Me.ImageXView1.MouseWheelCapture = False
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(548, 285)
        Me.ImageXView1.TabIndex = 2
        '
        'DescriptionListBox
        '
        Me.DescriptionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionListBox.FormattingEnabled = True
        Me.DescriptionListBox.Items.AddRange(New Object() {"This sample demonstrates using the TwainPRO .NET control to perform the following" & _
                        ":", "1) Scan with and without the Scanner Interface.", "2) Catch exceptions with the TwainPRO control.", "3) Use the scanned event to transfer image data from the TwainPRO control to Imag" & _
                        "Xpress."})
        Me.DescriptionListBox.Location = New System.Drawing.Point(12, 27)
        Me.DescriptionListBox.Name = "DescriptionListBox"
        Me.DescriptionListBox.Size = New System.Drawing.Size(548, 69)
        Me.DescriptionListBox.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(572, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImagXpressToolStripMenuItem, Me.TwainProToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'ImagXpressToolStripMenuItem
        '
        Me.ImagXpressToolStripMenuItem.Name = "ImagXpressToolStripMenuItem"
        Me.ImagXpressToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.ImagXpressToolStripMenuItem.Text = "Imag&Xpress"
        '
        'TwainProToolStripMenuItem
        '
        Me.TwainProToolStripMenuItem.Name = "TwainProToolStripMenuItem"
        Me.TwainProToolStripMenuItem.Size = New System.Drawing.Size(130, 22)
        Me.TwainProToolStripMenuItem.Text = "&TwainPro"
        '
        'ErrorLabel
        '
        Me.ErrorLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ErrorLabel.Location = New System.Drawing.Point(12, 444)
        Me.ErrorLabel.Name = "ErrorLabel"
        Me.ErrorLabel.Size = New System.Drawing.Size(548, 52)
        Me.ErrorLabel.TabIndex = 4
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 505)
        Me.Controls.Add(Me.ErrorLabel)
        Me.Controls.Add(Me.DescriptionListBox)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.NoUIButton)
        Me.Controls.Add(Me.UIButton)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TwainPro Basics"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents UIButton As System.Windows.Forms.Button
    Friend WithEvents NoUIButton As System.Windows.Forms.Button
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents DescriptionListBox As System.Windows.Forms.ListBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TwainProToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErrorLabel As System.Windows.Forms.Label
    Friend WithEvents TwainPro1 As PegasusImaging.WinForms.TwainPro5.TwainPro

End Class

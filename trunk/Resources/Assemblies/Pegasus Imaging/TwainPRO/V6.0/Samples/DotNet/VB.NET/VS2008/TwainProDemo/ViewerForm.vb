'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Public Class ViewerForm

    Private Sub ViewerForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '***Must have the UnlockRuntime call to unlock the control
        'imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
    End Sub

    Public Sub AddImage(ByVal sender As Object, ByVal e As Accusoft.TwainProSdk.ScannedEventArgs)
        ImageXView1.AutoScroll = True
        ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(ImagXpress1, e.ScannedImage.ToHdib())

        If (Visible = False) Then
            ShowDialog()
        End If
    End Sub

    Public Function GetImagXpress() As Accusoft.ImagXpressSdk.ImagXpress
        Return ImagXpress1
    End Function

    Public Function GetImage() As Accusoft.ImagXpressSdk.ImageX
        Return ImageXView1.Image
    End Function


End Class
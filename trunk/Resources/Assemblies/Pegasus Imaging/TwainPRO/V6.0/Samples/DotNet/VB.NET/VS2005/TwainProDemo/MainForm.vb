'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Imports Accusoft.TwainProSdk

Public Class MainForm

    Private twainDevice As Accusoft.TwainProSdk.TwainDevice
    Private selectedCapability As Accusoft.TwainProSdk.Capability
    Private capContainer As Accusoft.TwainProSdk.CapabilityContainer

    Private imageCount As Int32


    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            '***Must call the UnlockRuntime method to unlock the control
            'twainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

            Application.EnableVisualStyles()

            twainDevice = New TwainDevice(TwainPro1)

            ' Set the Debug property to True to write information to a debug log file
            'TwainPro1.Debug = True
            'TwainPro1.ErrorLevel = ErrorLevel.Detailed

            ' Add the Scanned event
            AddHandler twainDevice.Scanned, AddressOf twainDevice_Scanned

            HComboBox.SelectedIndex = 0
            VComboBox.SelectedIndex = 0

            ' Position the group boxes
            CapabilitiesGroupBox.Left = DefaultGroupBox.Left
            CaptionGroupBox.Left = DefaultGroupBox.Left
            LayoutGroupBox.Left = DefaultGroupBox.Left

            CaptionGroupBox.Top = DefaultGroupBox.Top
            CapabilitiesGroupBox.Top = DefaultGroupBox.Top
            LayoutGroupBox.Top = DefaultGroupBox.Top

            imageCount = 0

        Catch ex As Accusoft.TwainProSdk.TwainProException

            ErrorLabel.Text = ex.Message

        Catch Ex As System.Exception

            ErrorLabel.Text = Ex.Message

        End Try

    End Sub

    Private Sub twainDevice_Scanned(ByVal sender As Object, ByVal e As Accusoft.TwainProSdk.ScannedEventArgs)
        Try

            ' Display the scanned image
            Dim viewer As ViewerForm = New ViewerForm()
            viewer.AddImage(sender, e)

            ' Save the scanned image if the save image check box is checked
            If (SaveCheckBox.Checked) Then

                Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
                Dim strPath As String = System.IO.Path.Combine(strCurrentDir, "image-" + imageCount.ToString() + ".tif")

                Dim saveOptions As Accusoft.ImagXpressSdk.SaveOptions = New Accusoft.ImagXpressSdk.SaveOptions()
                saveOptions.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff
                saveOptions.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4

                viewer.GetImage().Save(strPath, saveOptions)

                imageCount = imageCount + 1

                SaveCheckBox.Text = "Save Image as  Image-" + imageCount.ToString() + ".tif"

            End If
            Application.DoEvents()

        Catch ex As Accusoft.TwainProSdk.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try

    End Sub

    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenButton.Click
        Try

            ErrorLabel.Text = ""
            ' Open session so that capabilities can be negotiated
            twainDevice.OpenSession()
            CapButton.Enabled = True
            LayoutButton.Enabled = True
            CaptionButton.Enabled = True
            StartButton.Enabled = True
        Catch ex As TwainException
            ErrorLabel.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub

    Private Sub GetError()
        If Not ((TwainPro1.ErrorLevel = 0)) Then
            ErrorLabel.Text = ("Data Source Error " & TwainPro1.ErrorLevel)
        End If
    End Sub

    Private Sub StartButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StartButton.Click
        ErrorLabel.Text = ""
        CapabilitiesGroupBox.Visible = False
        CaptionGroupBox.Visible = False
        LayoutGroupBox.Visible = False
        DefaultGroupBox.Visible = True
        CapButton.Enabled = False
        LayoutButton.Enabled = False
        CaptionButton.Enabled = False
        CapButton.Text = "Show Capabilities"
        LayoutButton.Text = "Show Layout"
        CaptionButton.Text = "Show Caption"
        UpdateProps()

        Try
            ' Begin scanning
            twainDevice.StartSession()

        Catch ex As TwainException
            ErrorLabel.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription

        Catch ex As Accusoft.TwainProSdk.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
            GetError()

        End Try

    End Sub

    Private Sub UpdateProps()
        Try

            ' Copy values from fields to TwainPRO control
            twainDevice.ShowUserInterface = UICheckBox.Checked
            twainDevice.Caption.Text = CaptionTextBox.Text
            twainDevice.Caption.Clip = ClipCheckBox.Checked
            twainDevice.Caption.ShadowText = ShadowCheckBox.Checked

            twainDevice.Caption.Area = New System.Drawing.Rectangle(Int32.Parse(CapLTextBox.Text), Int32.Parse(CapTTextBox.Text), Int32.Parse(CapWTextBox.Text), Int32.Parse(CapHTextBox.Text))

            twainDevice.Caption.HorizontalAlignment = CType(HComboBox.SelectedIndex, Accusoft.TwainProSdk.HorizontalAlignment)
            twainDevice.Caption.VerticalAlignment = CType(VComboBox.SelectedIndex, Accusoft.TwainProSdk.HorizontalAlignment)

        Catch ex As Accusoft.TwainProSdk.TwainProException
            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Try

            ErrorLabel.Text = ""
            twainDevice.CloseSession()
            CapButton.Enabled = False
            LayoutButton.Enabled = False
        Catch ex As TwainException
            ErrorLabel.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try

    End Sub

    ' Display the Capabilities group box
    Private Sub CapButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CapButton.Click
        If (CapabilitiesGroupBox.Visible = False) Then

            CaptionButton.Text = "Show Caption"
            LayoutButton.Text = "Show Layout"
            CapButton.Text = "Hide Capabilities"
            LayoutGroupBox.Visible = False
            DefaultGroupBox.Visible = False
            CaptionGroupBox.Visible = False
            CapabilitiesGroupBox.Visible = True
            CapComboBox.SelectedIndex = 0

        Else

            CapButton.Text = "Show Capabilities"
            CapabilitiesGroupBox.Visible = False
            DefaultGroupBox.Visible = True
        End If
    End Sub

    ' Display the Image Layout group box and the data source's image layout
    Private Sub LayoutButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LayoutButton.Click
        Try

            ErrorLabel.Text = ""
            If (LayoutGroupBox.Visible = False) Then

                CaptionButton.Text = "Show Caption"
                CapButton.Text = "Show Capabilities"
                LayoutButton.Text = "Hide Layout"
                CaptionGroupBox.Visible = False
                CapabilitiesGroupBox.Visible = False
                DefaultGroupBox.Visible = False
                LayoutGroupBox.Visible = True

                Dim left As Single = -1
                Dim top As Single = -1
                Dim right As Single = -1
                Dim bottom As Single = -1

                left = twainDevice.ImageLayout.Left
                top = twainDevice.ImageLayout.Top
                right = twainDevice.ImageLayout.Right
                bottom = twainDevice.ImageLayout.Bottom

                LayoutLTextBox.Text = left.ToString()
                LayoutTTextBox.Text = top.ToString()
                LayoutRTextBox.Text = right.ToString()
                LayoutBTextBox.Text = bottom.ToString()

            Else

                LayoutButton.Text = "Show Layout"
                LayoutGroupBox.Visible = False
                DefaultGroupBox.Visible = True

            End If
        Catch ex As Accusoft.TwainProSdk.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message

        End Try
    End Sub

    ' Show the Caption group box
    Private Sub CaptionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CaptionButton.Click
        If (CaptionGroupBox.Visible = False) Then

            LayoutButton.Text = "Show Layout"
            CapButton.Text = "Show Capabilities"
            CaptionButton.Text = "Hide Caption"
            CapabilitiesGroupBox.Visible = False
            DefaultGroupBox.Visible = False
            LayoutGroupBox.Visible = False
            CaptionGroupBox.Visible = True

        Else

            CaptionButton.Text = "Show Caption"
            CaptionGroupBox.Visible = False
            DefaultGroupBox.Visible = True
        End If
    End Sub

    Private Sub SourceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SourceButton.Click
        Try

            ErrorLabel.Text = ""
            twainDevice.CloseSession()
            CapButton.Enabled = False
            LayoutButton.Enabled = False
            ' Display the Select Source dialog
            twainDevice.SelectSource()
            OpenButton.Enabled = True
            StartButton.Enabled = True
        Catch ex As TwainException
            ErrorLabel.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub

    Private Sub DisplayUnits()
        Try

            LabelUnits.Visible = True
            Dim unitsFound As Single = 0
            Dim capabilityUnits As Capability = Capability.IcapUnits
            Dim unitsCapContainer As CapabilityContainer = twainDevice.GetCapability(capabilityUnits)

            If (unitsCapContainer.GetType().ToString() = "Accusoft.TwainProSdk.CapabilityContainerEnum") Then

                If (twainDevice.IsCapabilitySupported(capabilityUnits) = True) Then

                    Dim myCap As CapabilityContainerEnum = CType(twainDevice.GetCapability(capabilityUnits), CapabilityContainerEnum)

                    unitsFound = CType(myCap.CurrentValue, CapabilityContainerOneValueFloat).Value
                End If
            ElseIf (unitsCapContainer.GetType().ToString() = "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat") Then

                If (twainDevice.IsCapabilitySupported(capabilityUnits) = True) Then

                    Dim myCap As CapabilityContainerOneValueFloat = CType(twainDevice.GetCapability(capabilityUnits), CapabilityContainerOneValueFloat)

                    unitsFound = myCap.Value
                End If
            End If

            Select Case (unitsFound)

                Case 0
                    LabelUnits.Text = "Units of Measure:  Inches"
                Case 1
                    LabelUnits.Text = "Units of Measure:  Centimeters"
                Case 2
                    LabelUnits.Text = "Units of Measure:  Picas"
                Case 3
                    LabelUnits.Text = "Units of Measure:  Points"
                Case 4
                    LabelUnits.Text = "Units of Measure:  Twips"
                Case 5
                    LabelUnits.Text = "Units of Measure:  Pixels"
                Case 6
                    LabelUnits.Text = "Units of Measure:  Millimeters"
            End Select

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

    Private Sub CapComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CapComboBox.SelectedIndexChanged
        Try

            ' Only process when session is open and the Capabilities group box can only be visible
            ' after OpenSession is called
            If (CapabilitiesGroupBox.Visible = False) Then
                Return
            End If

            UpdateButton.Enabled = True

            ' Reset all the controls in the Capabilities group box
            DefaultLabel.Visible = False
            MinLabel.Visible = False
            MaxLabel.Visible = False
            CapListBox.Visible = False
            CapListBox.Items.Clear()

            CurrentTextBox.Text = ""
            CurrentTextBox.Enabled = True

            LabelUnits.Visible = False

            DetermineSelectedCapability()
            DisplayCapabilityInfo()

        Catch ex As Accusoft.TwainProSdk.TwainProException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

    ' Determine the capability that was selected in the capability combo box
    Private Sub DetermineSelectedCapability()

        selectedCapability = Capability.IcapAutoBright

        Select Case (CapComboBox.SelectedIndex)

            Case 0
                selectedCapability = Capability.IcapAutoBright
            Case 1
                selectedCapability = Capability.IcapBrightness
            Case 2
                selectedCapability = Capability.IcapContrast
            Case 3
                selectedCapability = Capability.IcapGamma
            Case 4
                selectedCapability = Capability.IcapPhysicalWidth
                DisplayUnits()
            Case 5
                selectedCapability = Capability.IcapPhysicalHeight
                DisplayUnits()
            Case 6
                selectedCapability = Capability.IcapPixelType
            Case 7
                selectedCapability = Capability.IcapRotation
            Case 8
                selectedCapability = Capability.IcapUnits
            Case 9
                selectedCapability = Capability.IcapXNativeResolution
                DisplayUnits()
            Case 10
                selectedCapability = Capability.IcapYNativeResolution
                DisplayUnits()
            Case 11
                selectedCapability = Capability.IcapXResolution
                DisplayUnits()
            Case 12
                selectedCapability = Capability.IcapYResolution
                DisplayUnits()
            Case 13
                selectedCapability = Capability.IcapXScaling
            Case 14
                selectedCapability = Capability.IcapYScaling
            Case 15
                selectedCapability = Capability.CapDeviceOnline
            Case 16
                selectedCapability = Capability.CapIndicators
            Case 17
                selectedCapability = Capability.IcapLampstate
            Case 18
                selectedCapability = Capability.CapUIControllable
            Case 19
                selectedCapability = Capability.CapXferCount
            Case 20
                selectedCapability = Capability.CapAutoFeed
            Case 21
                selectedCapability = Capability.CapAutoScan
            Case 22
                selectedCapability = Capability.CapClearPage
            Case 23
                selectedCapability = Capability.CapDuplex
            Case 24
                selectedCapability = Capability.CapDuplexEnabled
            Case 25
                selectedCapability = Capability.CapFeederEnabled
            Case 26
                selectedCapability = Capability.CapFeederLoaded
            Case 27
                selectedCapability = Capability.CapFeedPage
            Case 28
                selectedCapability = Capability.CapPaperDetectable
        End Select
    End Sub

    ' Display the info for the selected capability
    Private Sub DisplayCapabilityInfo()
        ' Check for support and stop if none
        If (twainDevice.IsCapabilitySupported(selectedCapability) = False) Then

            DefaultLabel.Text = "Not Supported"
            DefaultLabel.Visible = True
            CurrentTextBox.Enabled = False
            UpdateButtonLayout.Enabled = False
            Return

        Else

            capContainer = twainDevice.GetCapability(selectedCapability)
            ' What data type is the Cap?  We have to check the data type
            ' to know which properties are valid
            Try

                Select Case (capContainer.GetType().ToString())

                    ' Type ONEVALUE only returns a single value
                    Case "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat"

                        Dim myCap As CapabilityContainerOneValueFloat = CType(twainDevice.GetCapability(selectedCapability), CapabilityContainerOneValueFloat)

                        CurrentTextBox.Text = myCap.Value.ToString()
                        CurrentTextBox.Visible = True


                    Case "Accusoft.TwainProSdk.CapabilityContainerOneValueString"

                        Dim myCap As CapabilityContainerOneValueString = CType(twainDevice.GetCapability(selectedCapability), CapabilityContainerOneValueString)

                        CurrentTextBox.Text = myCap.Value
                        CurrentTextBox.Visible = True

                        ' Type ENUM returns a list of legal values as well as current and
                        ' default values.  A list of constants is returned and the CapDesc
                        ' property can be used to find out what the constants mean
                    Case "Accusoft.TwainProSdk.CapabilityContainerEnum"

                        Dim myCap As CapabilityContainerEnum = CType(twainDevice.GetCapability(selectedCapability), CapabilityContainerEnum)

                        CurrentTextBox.Text = CType(myCap.CurrentValue, CapabilityContainerOneValueFloat).Value.ToString()
                        CurrentTextBox.Visible = True

                        DefaultLabel.Text = "Default  =  " + CType(myCap.DefaultValue, CapabilityContainerOneValueFloat).Value.ToString()
                        DefaultLabel.Visible = True

                        CapListBox.Items.Add("Legal Values:")

                        Dim index As Int32
                        For index = 0 To myCap.Values.Count - 1

                            Dim floatResult As Single = CType(myCap.Values(index), CapabilityContainerOneValueFloat).Value

                            Dim stringResult As String = twainDevice.GetCapabilityConstantDescription(selectedCapability, index)

                            If (stringResult Is Nothing) Then

                                stringResult = ""
                            End If

                            CapListBox.Items.Add(floatResult.ToString() + " - " + stringResult.ToString())
                        Next index
                        CapListBox.Visible = True

                        ' Type ARRAY returns a list of values, but no current or default values
                        ' This is a less common type that many sources don't use
                    Case "Accusoft.TwainProSdk.CapabilityContainerArray"

                        Dim myCap As CapabilityContainerArray = CType(twainDevice.GetCapability(selectedCapability), CapabilityContainerArray)

                        CapListBox.Items.Add("Legal Values:")
                        Dim index As Int32

                        For index = 0 To myCap.Values.Count - 1
                            Dim fResult As Single = CType(myCap.Values(index), CapabilityContainerOneValueFloat).Value
                            CapListBox.Items.Add(fResult)
                        Next index

                        ' Returns a range of values as well as current and default values
                    Case "Accusoft.TwainProSdk.CapabilityContainerRange"

                        Dim myCap As CapabilityContainerRange = CType(twainDevice.GetCapability(selectedCapability), CapabilityContainerRange)

                        CurrentTextBox.Text = myCap.Value.ToString()
                        CurrentTextBox.Visible = True

                        DefaultLabel.Text = "Default  =  " + myCap.Default.ToString()
                        DefaultLabel.Visible = True

                        MinLabel.Text = "MinValue  =  " + myCap.Minimum.ToString()
                        MinLabel.Visible = True

                        MaxLabel.Text = "MaxValue  =  " + myCap.Maximum.ToString()
                        MaxLabel.Visible = True

                End Select

                ' The following capabilities can only be read, so disable the update button
                If (selectedCapability = Capability.IcapPhysicalHeight Or selectedCapability = Capability.IcapPhysicalWidth Or selectedCapability = Capability.IcapXNativeResolution Or selectedCapability = Capability.IcapYNativeResolution Or selectedCapability = Capability.CapDeviceOnline Or selectedCapability = Capability.CapUIControllable Or selectedCapability = Capability.CapAutoScan Or selectedCapability = Capability.CapClearPage Or selectedCapability = Capability.CapDuplex Or selectedCapability = Capability.CapFeederLoaded Or selectedCapability = Capability.CapPaperDetectable) Then
                    UpdateButton.Enabled = False
                End If

                ErrorLabel.Text = ""

            Catch ex As System.InvalidCastException
                ErrorLabel.Text = ex.Message
            Catch ex As System.StackOverflowException
                ErrorLabel.Text = ex.Message
            End Try
        End If
    End Sub

    Private Sub UpdateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateButton.Click
        Try

            ErrorLabel.Text = ""

            Select Case (capContainer.GetType().ToString())

                Case "Accusoft.TwainProSdk.CapabilityContainerEnum", "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat", "Accusoft.TwainProSdk.CapabilityContainerRange"

                    Try

                        Dim updateCap As Accusoft.TwainProSdk.CapabilityContainerOneValueFloat = New CapabilityContainerOneValueFloat(selectedCapability)
                        updateCap.Value = Single.Parse(CurrentTextBox.Text)

                        twainDevice.SetCapability(updateCap)

                        ' Force an update of the Caps frame with the new values
                        CapComboBox.SelectedIndex = CapComboBox.SelectedIndex

                        ErrorLabel.Text = "Capability set!"

                    Catch ex As FormatException

                        ErrorLabel.Text = ex.Message
                    End Try
            End Select



        Catch ex As TwainException
            ErrorLabel.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription

        Catch ex As FormatException

            ErrorLabel.Text = ex.Message

        Catch ex As System.AccessViolationException

            ErrorLabel.Text = ex.Message

        Catch ex As System.StackOverflowException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try

    End Sub

    ' Update the Image Layout
    ' This can only be done after calling OpenSession and before calling
    ' StartSession (ie. in state 4 as described in the Twain spec)
    Private Sub UpdateButtonLayout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateButtonLayout.Click
        Try

            ErrorLabel.Text = ""
            Dim left As Single = System.Double.Parse(LayoutLTextBox.Text)
            Dim top As Single = System.Double.Parse(LayoutTTextBox.Text)
            Dim width As Single = System.Double.Parse(LayoutRTextBox.Text)
            Dim height As Single = System.Double.Parse(LayoutBTextBox.Text)

            twainDevice.ImageLayout = New System.Drawing.RectangleF(left, top, width, height)

        Catch ex As System.FormatException

            MessageBox.Show("You must enter a real number for each field.")
            GetError()
        Catch ex As TwainException
            ErrorLabel.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription

        Catch ex As System.AccessViolationException

            ErrorLabel.Text = ex.Message

        Catch ex As System.StackOverflowException

            ErrorLabel.Text = ex.Message

        Catch ex As System.Exception

            ErrorLabel.Text = ex.Message
        End Try
    End Sub

    Private Sub TwainProToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TwainProToolStripMenuItem.Click
        TwainPro1.AboutBox()
    End Sub

    Private Sub ImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressToolStripMenuItem.Click
        Dim frm As ViewerForm = New ViewerForm()
        frm.GetImagXpress().AboutBox()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub
End Class

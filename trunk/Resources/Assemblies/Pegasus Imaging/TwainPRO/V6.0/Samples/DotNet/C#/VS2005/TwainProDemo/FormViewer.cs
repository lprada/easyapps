/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Accusoft.ImagXpressSdk;

namespace TwainProDemoCSharp
{
	/// <summary>
	/// Summary description for FormViewer.
	/// </summary>
	public class FormViewer : System.Windows.Forms.Form
    {
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private IContainer components;

		public FormViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.Location = new System.Drawing.Point(7, 8);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(771, 472);
            this.imageXView1.TabIndex = 0;
            // 
            // FormViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(784, 482);
            this.Controls.Add(this.imageXView1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Viewer";
            this.Load += new System.EventHandler(this.FormViewer_Load);
            this.ResumeLayout(false);

		}
		#endregion

        public void AddImage(Object sender, Accusoft.TwainProSdk.ScannedEventArgs e)
        {
            imageXView1.AutoScroll = true;
            imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress1, e.ScannedImage.ToHdib());

            if (this.Visible == false)
            {
                this.ShowDialog();
            }
        }

        public ImagXpress GetImagXpress()
        {
            return imagXpress1;
        }

        public ImageX GetImage()
        {
            return imageXView1.Image;
        }

        private void FormViewer_Load(object sender, EventArgs e)
        {
            //***Must have the UnlockRuntime call to unlock the control
            //imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);
        }
	}
}

/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using Accusoft.ImagXpressSdk;
using Accusoft.TwainProSdk;

namespace TwainProMemCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		[DllImport ("kernel32.dll")]
		private static extern int GlobalFree (int hMem);

		private System.String strCurrentDir;

		private System.Windows.Forms.Label labelCompressTypes;
		private System.Windows.Forms.Label lblCompressTypes;
		private System.Windows.Forms.Button buttonScan;
		private System.Windows.Forms.Button buttonSelectSource;
		private System.Windows.Forms.ComboBox comboBoxCompression;
		private System.Windows.Forms.CheckBox checkBoxUseViewer;
		private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.Label labelJpegQual;
		private System.Windows.Forms.Label labelJpegQualVal;
        private System.Windows.Forms.HScrollBar hScrollBarJpegQual;
		internal System.Windows.Forms.ListBox lstStatus;
		internal System.Windows.Forms.Label lblLoadStatus;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.Label lblerror;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.MainMenu mainmenu;
		private System.Windows.Forms.MenuItem menuItemFile;
		private System.Windows.Forms.MenuItem menuItemQuit;
		private System.Windows.Forms.MenuItem menuItemToolbar;
		private System.Windows.Forms.MenuItem menuItemToolbarShow;
        private System.Windows.Forms.MenuItem menuItemAbout;
        private IContainer components;
        private MenuItem menuItemImagXpress;
        private MenuItem menuItemTwainPro;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private Accusoft.TwainProSdk.TwainPro twainPro1;
        private CheckBox checkBoxShowUI;

        private Accusoft.TwainProSdk.TwainDevice twainDevice;

		public FormMain()
		{
			InitializeComponent();

			// Initialize TwainPRO to receive events
            twainDevice = new TwainDevice(twainPro1);
            
            // Set the Transfer Mode to Buffered Memory Mode
            twainDevice.TransferMode = Accusoft.TwainProSdk.TransferMode.TwsxMemory;
            Application.EnableVisualStyles();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
            if (disposing)
            {
                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }
                if (imageXView1 != null)
                {
                    imageXView1.Dispose();
                    imageXView1 = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (twainDevice != null)
                {
                    twainDevice.Dispose();
                    twainDevice = null;
                }
                if (twainPro1 != null)
                {
                    twainPro1.Dispose();
                    twainPro1 = null;
                }
                if (components != null)
                {
                    components.Dispose();
                }
            }
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.labelCompressTypes = new System.Windows.Forms.Label();
            this.checkBoxUseViewer = new System.Windows.Forms.CheckBox();
            this.hScrollBarJpegQual = new System.Windows.Forms.HScrollBar();
            this.comboBoxCompression = new System.Windows.Forms.ComboBox();
            this.buttonScan = new System.Windows.Forms.Button();
            this.buttonSelectSource = new System.Windows.Forms.Button();
            this.labelJpegQual = new System.Windows.Forms.Label();
            this.labelJpegQualVal = new System.Windows.Forms.Label();
            this.lblCompressTypes = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.lstStatus = new System.Windows.Forms.ListBox();
            this.lblLoadStatus = new System.Windows.Forms.Label();
            this.lblLastError = new System.Windows.Forms.Label();
            this.lblerror = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.mainmenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemFile = new System.Windows.Forms.MenuItem();
            this.menuItemQuit = new System.Windows.Forms.MenuItem();
            this.menuItemToolbar = new System.Windows.Forms.MenuItem();
            this.menuItemToolbarShow = new System.Windows.Forms.MenuItem();
            this.menuItemAbout = new System.Windows.Forms.MenuItem();
            this.menuItemImagXpress = new System.Windows.Forms.MenuItem();
            this.menuItemTwainPro = new System.Windows.Forms.MenuItem();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.twainPro1 = new Accusoft.TwainProSdk.TwainPro(this.components);
            this.checkBoxShowUI = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelCompressTypes
            // 
            this.labelCompressTypes.Location = new System.Drawing.Point(8, 176);
            this.labelCompressTypes.Name = "labelCompressTypes";
            this.labelCompressTypes.Size = new System.Drawing.Size(112, 16);
            this.labelCompressTypes.TabIndex = 4;
            this.labelCompressTypes.Text = "Compression Types:";
            // 
            // checkBoxUseViewer
            // 
            this.checkBoxUseViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxUseViewer.BackColor = System.Drawing.SystemColors.Control;
            this.checkBoxUseViewer.Checked = true;
            this.checkBoxUseViewer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUseViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.checkBoxUseViewer.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUseViewer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkBoxUseViewer.Location = new System.Drawing.Point(24, 315);
            this.checkBoxUseViewer.Name = "checkBoxUseViewer";
            this.checkBoxUseViewer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBoxUseViewer.Size = new System.Drawing.Size(129, 17);
            this.checkBoxUseViewer.TabIndex = 17;
            this.checkBoxUseViewer.Text = "Use Viewer";
            this.checkBoxUseViewer.UseVisualStyleBackColor = false;
            // 
            // hScrollBarJpegQual
            // 
            this.hScrollBarJpegQual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.hScrollBarJpegQual.Cursor = System.Windows.Forms.Cursors.Default;
            this.hScrollBarJpegQual.Enabled = false;
            this.hScrollBarJpegQual.LargeChange = 1;
            this.hScrollBarJpegQual.Location = new System.Drawing.Point(24, 287);
            this.hScrollBarJpegQual.Name = "hScrollBarJpegQual";
            this.hScrollBarJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.hScrollBarJpegQual.Size = new System.Drawing.Size(129, 17);
            this.hScrollBarJpegQual.TabIndex = 14;
            this.hScrollBarJpegQual.TabStop = true;
            this.hScrollBarJpegQual.Value = 80;
            this.hScrollBarJpegQual.ValueChanged += new System.EventHandler(this.hScrollBarJPEGQual_ValueChanged);
            // 
            // comboBoxCompression
            // 
            this.comboBoxCompression.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxCompression.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBoxCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCompression.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxCompression.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboBoxCompression.Location = new System.Drawing.Point(24, 208);
            this.comboBoxCompression.Name = "comboBoxCompression";
            this.comboBoxCompression.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.comboBoxCompression.Size = new System.Drawing.Size(129, 22);
            this.comboBoxCompression.TabIndex = 12;
            this.comboBoxCompression.SelectedIndexChanged += new System.EventHandler(this.comboBoxCompression_SelectedIndexChanged);
            // 
            // buttonScan
            // 
            this.buttonScan.BackColor = System.Drawing.SystemColors.Control;
            this.buttonScan.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonScan.Enabled = false;
            this.buttonScan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonScan.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonScan.Location = new System.Drawing.Point(32, 121);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonScan.Size = new System.Drawing.Size(121, 41);
            this.buttonScan.TabIndex = 10;
            this.buttonScan.Text = "Scan";
            this.buttonScan.UseVisualStyleBackColor = false;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // buttonSelectSource
            // 
            this.buttonSelectSource.BackColor = System.Drawing.SystemColors.Control;
            this.buttonSelectSource.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonSelectSource.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectSource.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonSelectSource.Location = new System.Drawing.Point(32, 72);
            this.buttonSelectSource.Name = "buttonSelectSource";
            this.buttonSelectSource.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonSelectSource.Size = new System.Drawing.Size(121, 41);
            this.buttonSelectSource.TabIndex = 9;
            this.buttonSelectSource.Text = "Select Source";
            this.buttonSelectSource.UseVisualStyleBackColor = false;
            this.buttonSelectSource.Click += new System.EventHandler(this.buttonSelectSource_Click);
            // 
            // labelJpegQual
            // 
            this.labelJpegQual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelJpegQual.BackColor = System.Drawing.SystemColors.Control;
            this.labelJpegQual.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelJpegQual.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJpegQual.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelJpegQual.Location = new System.Drawing.Point(24, 259);
            this.labelJpegQual.Name = "labelJpegQual";
            this.labelJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelJpegQual.Size = new System.Drawing.Size(80, 17);
            this.labelJpegQual.TabIndex = 16;
            this.labelJpegQual.Text = "JPEG Quality:";
            // 
            // labelJpegQualVal
            // 
            this.labelJpegQualVal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelJpegQualVal.BackColor = System.Drawing.SystemColors.Control;
            this.labelJpegQualVal.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelJpegQualVal.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJpegQualVal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelJpegQualVal.Location = new System.Drawing.Point(128, 259);
            this.labelJpegQualVal.Name = "labelJpegQualVal";
            this.labelJpegQualVal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelJpegQualVal.Size = new System.Drawing.Size(25, 17);
            this.labelJpegQualVal.TabIndex = 15;
            this.labelJpegQualVal.Text = "80";
            // 
            // lblCompressTypes
            // 
            this.lblCompressTypes.BackColor = System.Drawing.SystemColors.Control;
            this.lblCompressTypes.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCompressTypes.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompressTypes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCompressTypes.Location = new System.Drawing.Point(24, 176);
            this.lblCompressTypes.Name = "lblCompressTypes";
            this.lblCompressTypes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCompressTypes.Size = new System.Drawing.Size(121, 17);
            this.lblCompressTypes.TabIndex = 13;
            this.lblCompressTypes.Text = "Compression Types:";
            // 
            // labelStatus
            // 
            this.labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStatus.BackColor = System.Drawing.SystemColors.Control;
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelStatus.Location = new System.Drawing.Point(13, 382);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelStatus.Size = new System.Drawing.Size(444, 33);
            this.labelStatus.TabIndex = 11;
            this.labelStatus.Text = "Select and Scan";
            // 
            // lstStatus
            // 
            this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstStatus.Location = new System.Drawing.Point(478, 89);
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.Size = new System.Drawing.Size(224, 173);
            this.lstStatus.TabIndex = 24;
            // 
            // lblLoadStatus
            // 
            this.lblLoadStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoadStatus.Location = new System.Drawing.Point(480, 72);
            this.lblLoadStatus.Name = "lblLoadStatus";
            this.lblLoadStatus.Size = new System.Drawing.Size(192, 24);
            this.lblLoadStatus.TabIndex = 23;
            this.lblLoadStatus.Text = "Load Status:";
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(480, 283);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(216, 16);
            this.lblLastError.TabIndex = 22;
            this.lblLastError.Text = "Last Error:";
            // 
            // lblerror
            // 
            this.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblerror.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblerror.Location = new System.Drawing.Point(480, 299);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(216, 116);
            this.lblerror.TabIndex = 21;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Using the Memory Transfer mode and ICAP_COMPRESSION capability."});
            this.listBox1.Location = new System.Drawing.Point(16, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(680, 43);
            this.listBox1.TabIndex = 25;
            // 
            // mainmenu
            // 
            this.mainmenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemToolbar,
            this.menuItemAbout});
            // 
            // menuItemFile
            // 
            this.menuItemFile.Index = 0;
            this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemQuit});
            this.menuItemFile.Text = "File";
            // 
            // menuItemQuit
            // 
            this.menuItemQuit.Index = 0;
            this.menuItemQuit.Text = "Quit";
            this.menuItemQuit.Click += new System.EventHandler(this.menuItemQuit_Click);
            // 
            // menuItemToolbar
            // 
            this.menuItemToolbar.Index = 1;
            this.menuItemToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemToolbarShow});
            this.menuItemToolbar.Text = "Toolbar";
            // 
            // menuItemToolbarShow
            // 
            this.menuItemToolbarShow.Index = 0;
            this.menuItemToolbarShow.Text = "Show";
            this.menuItemToolbarShow.Click += new System.EventHandler(this.menuItemToolbarShow_Click);
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Index = 2;
            this.menuItemAbout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemImagXpress,
            this.menuItemTwainPro});
            this.menuItemAbout.Text = "About";
            // 
            // menuItemImagXpress
            // 
            this.menuItemImagXpress.Index = 0;
            this.menuItemImagXpress.Text = "ImagXpress";
            this.menuItemImagXpress.Click += new System.EventHandler(this.menuItemImagXpress_Click);
            // 
            // menuItemTwainPro
            // 
            this.menuItemTwainPro.Index = 1;
            this.menuItemTwainPro.Text = "&TwainPRO";
            this.menuItemTwainPro.Click += new System.EventHandler(this.menuItemTwainPro_Click);
            // 
            // imagXpress1
            // 
            this.imagXpress1.ProgressEvent += new Accusoft.ImagXpressSdk.ImagXpress.ProgressEventHandler(this.ProgressEventHandler);
            this.imagXpress1.ImageStatusEvent += new Accusoft.ImagXpressSdk.ImagXpress.ImageStatusEventHandler(this.ImageStatusEventHandler);
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;
            this.imageXView1.Location = new System.Drawing.Point(161, 60);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(295, 313);
            this.imageXView1.TabIndex = 26;
            // 
            // checkBoxShowUI
            // 
            this.checkBoxShowUI.AutoSize = true;
            this.checkBoxShowUI.Checked = true;
            this.checkBoxShowUI.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShowUI.Location = new System.Drawing.Point(24, 343);
            this.checkBoxShowUI.Name = "checkBoxShowUI";
            this.checkBoxShowUI.Size = new System.Drawing.Size(123, 17);
            this.checkBoxShowUI.TabIndex = 27;
            this.checkBoxShowUI.Text = "Show User Interface";
            this.checkBoxShowUI.UseVisualStyleBackColor = true;
            this.checkBoxShowUI.CheckedChanged += new System.EventHandler(this.checkBoxShowUI_CheckedChanged);
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(714, 424);
            this.Controls.Add(this.checkBoxShowUI);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lstStatus);
            this.Controls.Add(this.lblLoadStatus);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.hScrollBarJpegQual);
            this.Controls.Add(this.comboBoxCompression);
            this.Controls.Add(this.buttonScan);
            this.Controls.Add(this.buttonSelectSource);
            this.Controls.Add(this.labelJpegQual);
            this.Controls.Add(this.labelJpegQualVal);
            this.Controls.Add(this.lblCompressTypes);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.checkBoxUseViewer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainmenu;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Memory Transfer";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region Pegasus Imaging Sample Application Standard Functions

		System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

    
		private string GetFileName(string FullName) 
		{
			return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length 
				- (FullName.LastIndexOf("\\") + 1)));
		}
    
		private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ ((ErrorException.Source + "") 
				+ "\n"))));
		}
    
		private static void PegasusError(Accusoft.ImagXpressSdk.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
		}
    
		private string PegasusOpenFile(string sTitle, string sFileFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFileFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}
    
		private string PegasusSaveFile(string sTitle, string sFilter) 
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}

		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void buttonSelectSource_Click(object sender, System.EventArgs e)
		{
			labelStatus.Text = "Select Scanner";
			try
			{
				twainDevice.SelectSource ();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			UpdateCompressionTypes ();
			buttonScan.Enabled = true;
		}

		private void UpdateCompressionTypes ()
		{
            try
            {
                labelStatus.Text = "Querying Compression Capabilities";
                Application.DoEvents();

                twainDevice.OpenSession();

                AdvancedCapability capability = AdvancedCapability.IcapCompression;

                comboBoxCompression.Items.Clear();
                comboBoxCompression.Items.Add("TWCP_NONE");
                comboBoxCompression.SelectedIndex = 0;

                if (twainDevice.IsCapabilitySupported(capability))
                {
                    CapabilityContainer capcontainer = twainDevice.GetCapability(capability);

                    if (capcontainer.GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerEnum")
                    {

                        CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(capability);

                        for (int index = 0; index < myCap.Values.Count; index++)
                        {
                            float result = ((CapabilityContainerOneValueFloat)myCap.Values[index]).Value;

                            if (result == (float)AdvancedCapabilityConstants.TwifNone)
                            {
                                // Already handled
                            }
                            else if (result == (float)AdvancedCapabilityConstants.TwcpPackbits)
                            {
                                comboBoxCompression.Items.Add("TWCP_PACKBITS");
                            }
                            else if (result == (float)AdvancedCapabilityConstants.TwcpGroup31D)
                            {
                                comboBoxCompression.Items.Add("TWCP_GROUP31D");
                            }
                            else if (result == (float)AdvancedCapabilityConstants.TwcpGroup31DEol)
                            {
                                comboBoxCompression.Items.Add("TWCP_GROUP31DEOL");
                            }
                            else if (result == (float)AdvancedCapabilityConstants.TwcpGroup32D)
                            {
                                comboBoxCompression.Items.Add("TWCP_GROUP32D");
                            }
                            else if (result == (float)AdvancedCapabilityConstants.TwcpGroup4)
                            {
                                comboBoxCompression.Items.Add("TWCP_GROUP4");
                            }
                            else if (result == (float)AdvancedCapabilityConstants.TwcpJpeg)
                            {
                                comboBoxCompression.Items.Add("TWCP_JPEG");
                            }
                        }
                    }
                }

                twainDevice.CloseSession();
                labelStatus.Text = "Select and Scan";
            }
            catch (Accusoft.TwainProSdk.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
		}

		private void hScrollBarJPEGQual_ValueChanged(object sender, System.EventArgs e)
		{
			labelJpegQualVal.Text = hScrollBarJpegQual.Value.ToString ();
		}

		private void buttonScan_Click(object sender, System.EventArgs e)
		{
            try
            {
                labelStatus.Text = "Opening Scanner";
                Application.DoEvents();
                twainDevice.OpenSession();

                if (twainDevice.TransferMode == TransferMode.TwsxMemory)
                {
                    AdvancedCapability capability = AdvancedCapability.IcapCompression;

                    // Only do the following if the Capability is supported and
                    // there is more than one compression type to set. Otherwise,
                    // the data source may say they support this capability but
                    // might return an error if you try to set to TWCP_NONE and
                    // it is the only type. All data sources must support TWCP_NONE.
                    if (twainDevice.IsCapabilitySupported(capability) && comboBoxCompression.Items.Count > 1 && twainDevice.GetCapability(capability).GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerEnum")
                    {
                        labelStatus.Text = "Setting Compression Mode";

                        string strCompressType = comboBoxCompression.SelectedItem.ToString();

                        CapabilityContainerOneValueFloat myCap = new CapabilityContainerOneValueFloat(capability);

                        switch (strCompressType)
                        {
                            case "TWCP_NONE":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpNone;
                                break;

                            case "TWCP_PACKBITS":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpPackbits;
                                break;

                            case "TWCP_GROUP31D":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpGroup31D;
                                break;

                            case "TWCP_GROUP31DEOL":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpGroup31DEol;
                                break;

                            case "TWCP_GROUP32D":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpGroup32D;
                                break;

                            case "TWCP_GROUP4":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpGroup4;
                                break;

                            case "TWCP_JPEG":
                                myCap.Value = (float)AdvancedCapabilityConstants.TwcpJpeg;
                                break;
                        }
                        twainDevice.SetCapability(myCap);

                        if (strCompressType == "TWCP_JPEG")
                        {
                            // Set the JPEG Quality factor

                            AdvancedCapability capJpeg = AdvancedCapability.IcapJpegPixelType;
                            CapabilityContainer capJpegContainer = twainDevice.GetCapability(capJpeg);

                            if (twainDevice.IsCapabilitySupported(capJpeg))
                            {
                                if (capJpegContainer.GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat")
                                {
                                    CapabilityContainerOneValueFloat myJpegCap = new CapabilityContainerOneValueFloat(capJpeg);

                                    myJpegCap.Value = (float)hScrollBarJpegQual.Value;
                                    twainDevice.SetCapability(myJpegCap);
                                }
                            }

                            // You could also negotiate the ICAP_JPEGPIXELTYPE capability.
                            // I am leaving this to the DataSource
                        }
                        else if (strCompressType != "TWCP_NONE")
                        {
                            // The following is done to ensure the TIFF file can be read correctly by the Imaging for Windows
                            // Apparently Imaging for Windows must ignore the baseline tiff tag, PhotometricInterpretation.
                            // ImagXpress does not need the following as it correctly reads in the PhotometricInterpretation tag
                            // This will ensure that the TIFF file created has 0 as white and 1 as black

                            // We will set the ICAP_PIXELFLAVOR first, although for CCITT compression types
                            // ICAP_PIXELFLAVORCODES can override ICAP_PIXELFLAVOR

                            AdvancedCapability capPixel = AdvancedCapability.IcapPixelFlavor;
                            CapabilityContainer capPixelContainer = twainDevice.GetCapability(capPixel);

                            if (twainDevice.IsCapabilitySupported(capPixel))
                            {
                                if (capPixelContainer.GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat")
                                {
                                    CapabilityContainerOneValueFloat myPixelCap = new CapabilityContainerOneValueFloat(capPixel);

                                    myPixelCap.Value = (float)AdvancedCapabilityConstants.TwpfVanilla;
                                    twainDevice.SetCapability(myPixelCap);
                                }
                            }

                            AdvancedCapability cappixelcode = AdvancedCapability.IcapPixelFlavorCodes;
                            CapabilityContainer cappixelcodecontainer = twainDevice.GetCapability(cappixelcode);

                            if (strCompressType != "TWCP_PACKBITS" && twainDevice.IsCapabilitySupported(cappixelcode))
                            {
                                if (cappixelcodecontainer.GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat")
                                {

                                    CapabilityContainerOneValueFloat capFlavor = (CapabilityContainerOneValueFloat)twainDevice.GetCapability(cappixelcode);

                                    if (capFlavor.Value != (float)AdvancedCapabilityConstants.TwpfVanilla)
                                    {
                                        CapabilityContainerOneValueFloat myPixelCodeCap = new CapabilityContainerOneValueFloat(cappixelcode);

                                        myPixelCodeCap.Value = (float)AdvancedCapabilityConstants.TwpfVanilla;
                                        twainDevice.SetCapability(myPixelCodeCap);	// Set it only if we have to
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        twainDevice.TransferMode = TransferMode.TwsxNative;
                        labelStatus.Text = "TwainPRO forced to Native Transfer Mode. Compression unavailable.";
                    }
                }
                else
                {
                    twainDevice.TransferMode = TransferMode.TwsxNative;
                    labelStatus.Text = "TwainPRO forced to Native Transfer Mode. Compression unavailable.";
                }
                twainDevice.StartSession();
                labelStatus.Text = "Select and Scan";
            }
            catch (Accusoft.TwainProSdk.TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            {
                //Set the transfer mode back to memory
                twainDevice.TransferMode = TransferMode.TwsxMemory;
            }
		}


		private void ImageStatusEventHandler(object sender, Accusoft.ImagXpressSdk.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString());
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;	
		}

		private void ProgressEventHandler(object sender, Accusoft.ImagXpressSdk.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}
		
		private void FormMain_Load(object sender, System.EventArgs e)
		{		

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.Licensing.UnlockRuntime(1234,1234,1234,1234);
            //twainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

			try 
			{
				//this is where events are assigned. This happens before the file gets loaded.
				twainDevice.Scanned += new Accusoft.TwainProSdk.ScannedEventHandler(twainDevice_Scanned);
                twainDevice.Scanning += new Accusoft.TwainProSdk.ScanningEventHandler(twainDevice_Scanning);
			}
			catch (Accusoft.ImagXpressSdk.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}

			// Set Transfer Mode to Buffered Memory Mode
			twainDevice.TransferMode = TransferMode.TwsxMemory;
		}

        private void twainDevice_Scanned(object sender, Accusoft.TwainProSdk.ScannedEventArgs e)
        {
            try
            {
                labelStatus.Text = "";

                // Need to check the Compression type to see if we are transferring a DIB
                // or we are transferring compressed data.

                if (e.ScannedImage.ScannedImageData.Compression == ScannedImageCompression.TwcpNone)
                {
                    int hImage = e.ScannedImage.ToHdib().ToInt32();
                    if (hImage != 0)
                    {
                        try
                        {
                            if (imageXView1.Image != null)
                            {
                                imageXView1.Image.Dispose();
                                imageXView1.Image = null;
                            }
                            imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress1, (System.IntPtr)hImage);
                            if (checkBoxUseViewer.Checked == true)
                            {
                                FormViewer formViewer = new FormViewer();
                                formViewer.ShowEx(imageXView1.Image);
                            }
                        }
                        catch (Accusoft.ImagXpressSdk.ImagXpressException ex)
                        {
                            PegasusError(ex, lblerror);
                        }
                    }
                }
                else
                {
                    int hImage = e.ScannedImage.ToHbitmap().ToInt32();
                    if (hImage != 0)
                    {
                        try
                        {
                            if (imageXView1.Image != null)
                            {
                                imageXView1.Image.Dispose();
                                imageXView1.Image = null;
                            }
                            imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHbitmap(imagXpress1, (System.IntPtr)hImage);
                            if (checkBoxUseViewer.Checked == true)
                            {
                                FormViewer formViewer = new FormViewer();
                                formViewer.ShowEx(imageXView1.Image);
                            }
                        }
                        catch (Accusoft.ImagXpressSdk.ImagXpressException ex)
                        {
                            PegasusError(ex, lblerror);
                        }
                        GlobalFree(hImage);
                    }
                }

                Application.DoEvents();
                e.Cancel = false;
            }
            catch (Accusoft.ImagXpressSdk.ImagXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Accusoft.TwainProSdk.TwainProException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void twainDevice_Scanning(object sender, Accusoft.TwainProSdk.ScanningEventArgs e)
        {
            labelStatus.Text = "Scanning...";
        }

		private void menuItemQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
        }

		private void menuItemToolbarShow_Click(object sender, System.EventArgs e)
		{
			if ((imageXView1.Toolbar.Activated == true)) 
			{
				menuItemToolbarShow.Text = "Show";
				imageXView1.Toolbar.Activated = false;
			}
			else 
			{
				menuItemToolbarShow.Text = "Hide";
				imageXView1.Toolbar.Activated = true;
			}
		
		}

		private void comboBoxCompression_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (comboBoxCompression.SelectedItem.ToString() == "TWCP_JPEG")
			{
				hScrollBarJpegQual.Enabled = true;
			}
			else
			{
				hScrollBarJpegQual.Enabled = false;
			}
		}

        private void menuItemImagXpress_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void menuItemTwainPro_Click(object sender, EventArgs e)
        {
            twainPro1.AboutBox();
        }

        private void checkBoxShowUI_CheckedChanged(object sender, EventArgs e)
        {
            if (twainDevice != null)
            {
                if (checkBoxShowUI.Checked == true)
                    twainDevice.ShowUserInterface = true;
                else
                    twainDevice.ShowUserInterface = false;
            }
        }
			
	}
}

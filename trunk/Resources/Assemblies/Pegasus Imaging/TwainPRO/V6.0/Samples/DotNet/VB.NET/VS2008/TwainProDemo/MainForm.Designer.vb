<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not (twainDevice Is Nothing) Then
                twainDevice.Dispose()
                twainDevice = Nothing
            End If
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.MainMenu = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TwainProToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SourceButton = New System.Windows.Forms.Button
        Me.OpenButton = New System.Windows.Forms.Button
        Me.CloseButton = New System.Windows.Forms.Button
        Me.StartButton = New System.Windows.Forms.Button
        Me.LayoutButton = New System.Windows.Forms.Button
        Me.CapButton = New System.Windows.Forms.Button
        Me.CaptionButton = New System.Windows.Forms.Button
        Me.DefaultGroupBox = New System.Windows.Forms.GroupBox
        Me.SaveCheckBox = New System.Windows.Forms.CheckBox
        Me.UICheckBox = New System.Windows.Forms.CheckBox
        Me.ErrorLabel = New System.Windows.Forms.Label
        Me.DescriptionListBox = New System.Windows.Forms.ListBox
        Me.CaptionGroupBox = New System.Windows.Forms.GroupBox
        Me.HAlignLabel = New System.Windows.Forms.Label
        Me.VAlignLabel = New System.Windows.Forms.Label
        Me.VComboBox = New System.Windows.Forms.ComboBox
        Me.HComboBox = New System.Windows.Forms.ComboBox
        Me.CaptionTextBox = New System.Windows.Forms.TextBox
        Me.CapHTextBox = New System.Windows.Forms.TextBox
        Me.CapWTextBox = New System.Windows.Forms.TextBox
        Me.CapTTextBox = New System.Windows.Forms.TextBox
        Me.CapLTextBox = New System.Windows.Forms.TextBox
        Me.CaptionLabel = New System.Windows.Forms.Label
        Me.CapHLabel = New System.Windows.Forms.Label
        Me.CapWLabel = New System.Windows.Forms.Label
        Me.CapTLabel = New System.Windows.Forms.Label
        Me.CapLLabel = New System.Windows.Forms.Label
        Me.ShadowCheckBox = New System.Windows.Forms.CheckBox
        Me.ClipCheckBox = New System.Windows.Forms.CheckBox
        Me.LayoutGroupBox = New System.Windows.Forms.GroupBox
        Me.LayoutLTextBox = New System.Windows.Forms.TextBox
        Me.LayoutLLabel = New System.Windows.Forms.Label
        Me.LayoutTTextBox = New System.Windows.Forms.TextBox
        Me.LayoutTLabel = New System.Windows.Forms.Label
        Me.LayoutRTextBox = New System.Windows.Forms.TextBox
        Me.LayoutRLabel = New System.Windows.Forms.Label
        Me.LayoutBTextBox = New System.Windows.Forms.TextBox
        Me.LayoutBLabel = New System.Windows.Forms.Label
        Me.UpdateButtonLayout = New System.Windows.Forms.Button
        Me.CapabilitiesGroupBox = New System.Windows.Forms.GroupBox
        Me.LabelUnits = New System.Windows.Forms.Label
        Me.MinLabel = New System.Windows.Forms.Label
        Me.MaxLabel = New System.Windows.Forms.Label
        Me.DefaultLabel = New System.Windows.Forms.Label
        Me.UpdateButton = New System.Windows.Forms.Button
        Me.CurrentTextBox = New System.Windows.Forms.TextBox
        Me.CurrentLabel = New System.Windows.Forms.Label
        Me.CapListBox = New System.Windows.Forms.ListBox
        Me.CapComboBox = New System.Windows.Forms.ComboBox
        Me.TwainPro1 = New Accusoft.TwainProSdk.TwainPro(Me.components)
        Me.MainMenu.SuspendLayout()
        Me.DefaultGroupBox.SuspendLayout()
        Me.CaptionGroupBox.SuspendLayout()
        Me.LayoutGroupBox.SuspendLayout()
        Me.CapabilitiesGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu
        '
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(710, 24)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImagXpressToolStripMenuItem, Me.TwainProToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'ImagXpressToolStripMenuItem
        '
        Me.ImagXpressToolStripMenuItem.Name = "ImagXpressToolStripMenuItem"
        Me.ImagXpressToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ImagXpressToolStripMenuItem.Text = "Imag&Xpress"
        '
        'TwainProToolStripMenuItem
        '
        Me.TwainProToolStripMenuItem.Name = "TwainProToolStripMenuItem"
        Me.TwainProToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TwainProToolStripMenuItem.Text = "&TwainPro"
        '
        'SourceButton
        '
        Me.SourceButton.Location = New System.Drawing.Point(11, 185)
        Me.SourceButton.Name = "SourceButton"
        Me.SourceButton.Size = New System.Drawing.Size(155, 23)
        Me.SourceButton.TabIndex = 1
        Me.SourceButton.Text = "Select Source"
        Me.SourceButton.UseVisualStyleBackColor = True
        '
        'OpenButton
        '
        Me.OpenButton.Enabled = False
        Me.OpenButton.Location = New System.Drawing.Point(11, 214)
        Me.OpenButton.Name = "OpenButton"
        Me.OpenButton.Size = New System.Drawing.Size(155, 23)
        Me.OpenButton.TabIndex = 2
        Me.OpenButton.Text = "Open Session"
        Me.OpenButton.UseVisualStyleBackColor = True
        '
        'CloseButton
        '
        Me.CloseButton.Location = New System.Drawing.Point(11, 272)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(155, 23)
        Me.CloseButton.TabIndex = 4
        Me.CloseButton.Text = "Close Session"
        Me.CloseButton.UseVisualStyleBackColor = True
        '
        'StartButton
        '
        Me.StartButton.Enabled = False
        Me.StartButton.Location = New System.Drawing.Point(11, 243)
        Me.StartButton.Name = "StartButton"
        Me.StartButton.Size = New System.Drawing.Size(155, 23)
        Me.StartButton.TabIndex = 3
        Me.StartButton.Text = "Start Session\Acquire"
        Me.StartButton.UseVisualStyleBackColor = True
        '
        'LayoutButton
        '
        Me.LayoutButton.Enabled = False
        Me.LayoutButton.Location = New System.Drawing.Point(11, 330)
        Me.LayoutButton.Name = "LayoutButton"
        Me.LayoutButton.Size = New System.Drawing.Size(155, 23)
        Me.LayoutButton.TabIndex = 6
        Me.LayoutButton.Text = "Show Layout"
        Me.LayoutButton.UseVisualStyleBackColor = True
        '
        'CapButton
        '
        Me.CapButton.Enabled = False
        Me.CapButton.Location = New System.Drawing.Point(11, 301)
        Me.CapButton.Name = "CapButton"
        Me.CapButton.Size = New System.Drawing.Size(155, 23)
        Me.CapButton.TabIndex = 5
        Me.CapButton.Text = "Show Capabilities"
        Me.CapButton.UseVisualStyleBackColor = True
        '
        'CaptionButton
        '
        Me.CaptionButton.Location = New System.Drawing.Point(11, 359)
        Me.CaptionButton.Name = "CaptionButton"
        Me.CaptionButton.Size = New System.Drawing.Size(155, 23)
        Me.CaptionButton.TabIndex = 7
        Me.CaptionButton.Text = "Show Caption"
        Me.CaptionButton.UseVisualStyleBackColor = True
        '
        'DefaultGroupBox
        '
        Me.DefaultGroupBox.Controls.Add(Me.SaveCheckBox)
        Me.DefaultGroupBox.Controls.Add(Me.UICheckBox)
        Me.DefaultGroupBox.Location = New System.Drawing.Point(183, 122)
        Me.DefaultGroupBox.Name = "DefaultGroupBox"
        Me.DefaultGroupBox.Size = New System.Drawing.Size(227, 74)
        Me.DefaultGroupBox.TabIndex = 8
        Me.DefaultGroupBox.TabStop = False
        '
        'SaveCheckBox
        '
        Me.SaveCheckBox.AutoSize = True
        Me.SaveCheckBox.Location = New System.Drawing.Point(38, 40)
        Me.SaveCheckBox.Name = "SaveCheckBox"
        Me.SaveCheckBox.Size = New System.Drawing.Size(152, 17)
        Me.SaveCheckBox.TabIndex = 1
        Me.SaveCheckBox.Text = "Save Image as  Image-0.tif"
        Me.SaveCheckBox.UseVisualStyleBackColor = True
        '
        'UICheckBox
        '
        Me.UICheckBox.AutoSize = True
        Me.UICheckBox.Location = New System.Drawing.Point(38, 17)
        Me.UICheckBox.Name = "UICheckBox"
        Me.UICheckBox.Size = New System.Drawing.Size(67, 17)
        Me.UICheckBox.TabIndex = 0
        Me.UICheckBox.Text = "Show UI"
        Me.UICheckBox.UseVisualStyleBackColor = True
        '
        'ErrorLabel
        '
        Me.ErrorLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ErrorLabel.Location = New System.Drawing.Point(11, 423)
        Me.ErrorLabel.Name = "ErrorLabel"
        Me.ErrorLabel.Size = New System.Drawing.Size(694, 55)
        Me.ErrorLabel.TabIndex = 9
        '
        'DescriptionListBox
        '
        Me.DescriptionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionListBox.FormattingEnabled = True
        Me.DescriptionListBox.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1) Selecting the device with or without the scanner's UI via the ShowUI property." & _
                        "", "2) Querying and setting the various TWAIN capabilities.", "3) Scanning a specific size page via the SetImageLayout method.", "4) Saving the scanned image."})
        Me.DescriptionListBox.Location = New System.Drawing.Point(12, 34)
        Me.DescriptionListBox.Name = "DescriptionListBox"
        Me.DescriptionListBox.Size = New System.Drawing.Size(693, 82)
        Me.DescriptionListBox.TabIndex = 2
        '
        'CaptionGroupBox
        '
        Me.CaptionGroupBox.Controls.Add(Me.HAlignLabel)
        Me.CaptionGroupBox.Controls.Add(Me.VAlignLabel)
        Me.CaptionGroupBox.Controls.Add(Me.VComboBox)
        Me.CaptionGroupBox.Controls.Add(Me.HComboBox)
        Me.CaptionGroupBox.Controls.Add(Me.CaptionTextBox)
        Me.CaptionGroupBox.Controls.Add(Me.CapHTextBox)
        Me.CaptionGroupBox.Controls.Add(Me.CapWTextBox)
        Me.CaptionGroupBox.Controls.Add(Me.CapTTextBox)
        Me.CaptionGroupBox.Controls.Add(Me.CapLTextBox)
        Me.CaptionGroupBox.Controls.Add(Me.CaptionLabel)
        Me.CaptionGroupBox.Controls.Add(Me.CapHLabel)
        Me.CaptionGroupBox.Controls.Add(Me.CapWLabel)
        Me.CaptionGroupBox.Controls.Add(Me.CapTLabel)
        Me.CaptionGroupBox.Controls.Add(Me.CapLLabel)
        Me.CaptionGroupBox.Controls.Add(Me.ShadowCheckBox)
        Me.CaptionGroupBox.Controls.Add(Me.ClipCheckBox)
        Me.CaptionGroupBox.Location = New System.Drawing.Point(501, 171)
        Me.CaptionGroupBox.Name = "CaptionGroupBox"
        Me.CaptionGroupBox.Size = New System.Drawing.Size(282, 194)
        Me.CaptionGroupBox.TabIndex = 10
        Me.CaptionGroupBox.TabStop = False
        Me.CaptionGroupBox.Text = "Caption"
        Me.CaptionGroupBox.Visible = False
        '
        'HAlignLabel
        '
        Me.HAlignLabel.AutoSize = True
        Me.HAlignLabel.Location = New System.Drawing.Point(142, 46)
        Me.HAlignLabel.Name = "HAlignLabel"
        Me.HAlignLabel.Size = New System.Drawing.Size(83, 13)
        Me.HAlignLabel.TabIndex = 15
        Me.HAlignLabel.Text = "Horizontal Align:"
        '
        'VAlignLabel
        '
        Me.VAlignLabel.AutoSize = True
        Me.VAlignLabel.Location = New System.Drawing.Point(142, 109)
        Me.VAlignLabel.Name = "VAlignLabel"
        Me.VAlignLabel.Size = New System.Drawing.Size(71, 13)
        Me.VAlignLabel.TabIndex = 14
        Me.VAlignLabel.Text = "Vertical Align:"
        '
        'VComboBox
        '
        Me.VComboBox.FormattingEnabled = True
        Me.VComboBox.Items.AddRange(New Object() {"Top", "Bottom", "Center"})
        Me.VComboBox.Location = New System.Drawing.Point(145, 129)
        Me.VComboBox.Name = "VComboBox"
        Me.VComboBox.Size = New System.Drawing.Size(121, 21)
        Me.VComboBox.TabIndex = 13
        '
        'HComboBox
        '
        Me.HComboBox.FormattingEnabled = True
        Me.HComboBox.Items.AddRange(New Object() {"Left", "Right", "Center"})
        Me.HComboBox.Location = New System.Drawing.Point(145, 67)
        Me.HComboBox.Name = "HComboBox"
        Me.HComboBox.Size = New System.Drawing.Size(121, 21)
        Me.HComboBox.TabIndex = 12
        '
        'CaptionTextBox
        '
        Me.CaptionTextBox.AutoCompleteCustomSource.AddRange(New String() {"Pegasus Imaging Corp."})
        Me.CaptionTextBox.Location = New System.Drawing.Point(56, 159)
        Me.CaptionTextBox.Name = "CaptionTextBox"
        Me.CaptionTextBox.Size = New System.Drawing.Size(194, 20)
        Me.CaptionTextBox.TabIndex = 11
        Me.CaptionTextBox.Text = "Pegasus Imaging Corp."
        '
        'CapHTextBox
        '
        Me.CapHTextBox.Location = New System.Drawing.Point(84, 130)
        Me.CapHTextBox.Name = "CapHTextBox"
        Me.CapHTextBox.Size = New System.Drawing.Size(39, 20)
        Me.CapHTextBox.TabIndex = 10
        Me.CapHTextBox.Text = "0"
        Me.CapHTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CapWTextBox
        '
        Me.CapWTextBox.Location = New System.Drawing.Point(84, 101)
        Me.CapWTextBox.Name = "CapWTextBox"
        Me.CapWTextBox.Size = New System.Drawing.Size(39, 20)
        Me.CapWTextBox.TabIndex = 9
        Me.CapWTextBox.Text = "0"
        Me.CapWTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CapTTextBox
        '
        Me.CapTTextBox.Location = New System.Drawing.Point(84, 72)
        Me.CapTTextBox.Name = "CapTTextBox"
        Me.CapTTextBox.Size = New System.Drawing.Size(39, 20)
        Me.CapTTextBox.TabIndex = 8
        Me.CapTTextBox.Text = "0"
        Me.CapTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CapLTextBox
        '
        Me.CapLTextBox.Location = New System.Drawing.Point(84, 43)
        Me.CapLTextBox.Name = "CapLTextBox"
        Me.CapLTextBox.Size = New System.Drawing.Size(39, 20)
        Me.CapLTextBox.TabIndex = 7
        Me.CapLTextBox.Text = "0"
        Me.CapLTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CaptionLabel
        '
        Me.CaptionLabel.AutoSize = True
        Me.CaptionLabel.Location = New System.Drawing.Point(6, 162)
        Me.CaptionLabel.Name = "CaptionLabel"
        Me.CaptionLabel.Size = New System.Drawing.Size(46, 13)
        Me.CaptionLabel.TabIndex = 6
        Me.CaptionLabel.Text = "Caption:"
        '
        'CapHLabel
        '
        Me.CapHLabel.AutoSize = True
        Me.CapHLabel.Location = New System.Drawing.Point(6, 132)
        Me.CapHLabel.Name = "CapHLabel"
        Me.CapHLabel.Size = New System.Drawing.Size(80, 13)
        Me.CapHLabel.TabIndex = 5
        Me.CapHLabel.Text = "Caption Height:"
        '
        'CapWLabel
        '
        Me.CapWLabel.AutoSize = True
        Me.CapWLabel.Location = New System.Drawing.Point(4, 104)
        Me.CapWLabel.Name = "CapWLabel"
        Me.CapWLabel.Size = New System.Drawing.Size(77, 13)
        Me.CapWLabel.TabIndex = 4
        Me.CapWLabel.Text = "Caption Width:"
        '
        'CapTLabel
        '
        Me.CapTLabel.AutoSize = True
        Me.CapTLabel.Location = New System.Drawing.Point(6, 75)
        Me.CapTLabel.Name = "CapTLabel"
        Me.CapTLabel.Size = New System.Drawing.Size(68, 13)
        Me.CapTLabel.TabIndex = 3
        Me.CapTLabel.Text = "Caption Top:"
        '
        'CapLLabel
        '
        Me.CapLLabel.AutoSize = True
        Me.CapLLabel.Location = New System.Drawing.Point(6, 46)
        Me.CapLLabel.Name = "CapLLabel"
        Me.CapLLabel.Size = New System.Drawing.Size(67, 13)
        Me.CapLLabel.TabIndex = 2
        Me.CapLLabel.Text = "Caption Left:"
        '
        'ShadowCheckBox
        '
        Me.ShadowCheckBox.AutoSize = True
        Me.ShadowCheckBox.Location = New System.Drawing.Point(185, 17)
        Me.ShadowCheckBox.Name = "ShadowCheckBox"
        Me.ShadowCheckBox.Size = New System.Drawing.Size(89, 17)
        Me.ShadowCheckBox.TabIndex = 1
        Me.ShadowCheckBox.Text = "Shadow Text"
        Me.ShadowCheckBox.UseVisualStyleBackColor = True
        '
        'ClipCheckBox
        '
        Me.ClipCheckBox.AutoSize = True
        Me.ClipCheckBox.Location = New System.Drawing.Point(14, 17)
        Me.ClipCheckBox.Name = "ClipCheckBox"
        Me.ClipCheckBox.Size = New System.Drawing.Size(82, 17)
        Me.ClipCheckBox.TabIndex = 0
        Me.ClipCheckBox.Text = "Clip Caption"
        Me.ClipCheckBox.UseVisualStyleBackColor = True
        '
        'LayoutGroupBox
        '
        Me.LayoutGroupBox.Controls.Add(Me.LayoutLTextBox)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutLLabel)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutTTextBox)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutTLabel)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutRTextBox)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutRLabel)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutBTextBox)
        Me.LayoutGroupBox.Controls.Add(Me.LayoutBLabel)
        Me.LayoutGroupBox.Controls.Add(Me.UpdateButtonLayout)
        Me.LayoutGroupBox.Location = New System.Drawing.Point(412, 272)
        Me.LayoutGroupBox.Name = "LayoutGroupBox"
        Me.LayoutGroupBox.Size = New System.Drawing.Size(269, 160)
        Me.LayoutGroupBox.TabIndex = 16
        Me.LayoutGroupBox.TabStop = False
        Me.LayoutGroupBox.Text = "Layout"
        Me.LayoutGroupBox.Visible = False
        '
        'LayoutLTextBox
        '
        Me.LayoutLTextBox.Location = New System.Drawing.Point(139, 13)
        Me.LayoutLTextBox.Name = "LayoutLTextBox"
        Me.LayoutLTextBox.Size = New System.Drawing.Size(91, 20)
        Me.LayoutLTextBox.TabIndex = 15
        Me.LayoutLTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LayoutLLabel
        '
        Me.LayoutLLabel.AutoSize = True
        Me.LayoutLLabel.Location = New System.Drawing.Point(50, 16)
        Me.LayoutLLabel.Name = "LayoutLLabel"
        Me.LayoutLLabel.Size = New System.Drawing.Size(60, 13)
        Me.LayoutLLabel.TabIndex = 14
        Me.LayoutLLabel.Text = "Image Left:"
        '
        'LayoutTTextBox
        '
        Me.LayoutTTextBox.Location = New System.Drawing.Point(139, 43)
        Me.LayoutTTextBox.Name = "LayoutTTextBox"
        Me.LayoutTTextBox.Size = New System.Drawing.Size(91, 20)
        Me.LayoutTTextBox.TabIndex = 13
        Me.LayoutTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LayoutTLabel
        '
        Me.LayoutTLabel.AutoSize = True
        Me.LayoutTLabel.Location = New System.Drawing.Point(50, 46)
        Me.LayoutTLabel.Name = "LayoutTLabel"
        Me.LayoutTLabel.Size = New System.Drawing.Size(61, 13)
        Me.LayoutTLabel.TabIndex = 12
        Me.LayoutTLabel.Text = "Image Top:"
        '
        'LayoutRTextBox
        '
        Me.LayoutRTextBox.Location = New System.Drawing.Point(139, 70)
        Me.LayoutRTextBox.Name = "LayoutRTextBox"
        Me.LayoutRTextBox.Size = New System.Drawing.Size(91, 20)
        Me.LayoutRTextBox.TabIndex = 11
        Me.LayoutRTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LayoutRLabel
        '
        Me.LayoutRLabel.AutoSize = True
        Me.LayoutRLabel.Location = New System.Drawing.Point(50, 73)
        Me.LayoutRLabel.Name = "LayoutRLabel"
        Me.LayoutRLabel.Size = New System.Drawing.Size(70, 13)
        Me.LayoutRLabel.TabIndex = 10
        Me.LayoutRLabel.Text = "Image Width:"
        '
        'LayoutBTextBox
        '
        Me.LayoutBTextBox.Location = New System.Drawing.Point(139, 96)
        Me.LayoutBTextBox.Name = "LayoutBTextBox"
        Me.LayoutBTextBox.Size = New System.Drawing.Size(91, 20)
        Me.LayoutBTextBox.TabIndex = 9
        Me.LayoutBTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LayoutBLabel
        '
        Me.LayoutBLabel.AutoSize = True
        Me.LayoutBLabel.Location = New System.Drawing.Point(52, 99)
        Me.LayoutBLabel.Name = "LayoutBLabel"
        Me.LayoutBLabel.Size = New System.Drawing.Size(73, 13)
        Me.LayoutBLabel.TabIndex = 8
        Me.LayoutBLabel.Text = "Image Height:"
        '
        'UpdateButtonLayout
        '
        Me.UpdateButtonLayout.Location = New System.Drawing.Point(102, 128)
        Me.UpdateButtonLayout.Name = "UpdateButtonLayout"
        Me.UpdateButtonLayout.Size = New System.Drawing.Size(75, 23)
        Me.UpdateButtonLayout.TabIndex = 0
        Me.UpdateButtonLayout.Text = "Update"
        Me.UpdateButtonLayout.UseVisualStyleBackColor = True
        '
        'CapabilitiesGroupBox
        '
        Me.CapabilitiesGroupBox.Controls.Add(Me.LabelUnits)
        Me.CapabilitiesGroupBox.Controls.Add(Me.MinLabel)
        Me.CapabilitiesGroupBox.Controls.Add(Me.MaxLabel)
        Me.CapabilitiesGroupBox.Controls.Add(Me.DefaultLabel)
        Me.CapabilitiesGroupBox.Controls.Add(Me.UpdateButton)
        Me.CapabilitiesGroupBox.Controls.Add(Me.CurrentTextBox)
        Me.CapabilitiesGroupBox.Controls.Add(Me.CurrentLabel)
        Me.CapabilitiesGroupBox.Controls.Add(Me.CapListBox)
        Me.CapabilitiesGroupBox.Controls.Add(Me.CapComboBox)
        Me.CapabilitiesGroupBox.Location = New System.Drawing.Point(325, 276)
        Me.CapabilitiesGroupBox.Name = "CapabilitiesGroupBox"
        Me.CapabilitiesGroupBox.Size = New System.Drawing.Size(522, 286)
        Me.CapabilitiesGroupBox.TabIndex = 16
        Me.CapabilitiesGroupBox.TabStop = False
        Me.CapabilitiesGroupBox.Text = "Capabilities"
        Me.CapabilitiesGroupBox.Visible = False
        '
        'LabelUnits
        '
        Me.LabelUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelUnits.Location = New System.Drawing.Point(345, 155)
        Me.LabelUnits.Name = "LabelUnits"
        Me.LabelUnits.Size = New System.Drawing.Size(164, 20)
        Me.LabelUnits.TabIndex = 8
        '
        'MinLabel
        '
        Me.MinLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinLabel.Location = New System.Drawing.Point(38, 249)
        Me.MinLabel.Name = "MinLabel"
        Me.MinLabel.Size = New System.Drawing.Size(470, 26)
        Me.MinLabel.TabIndex = 7
        '
        'MaxLabel
        '
        Me.MaxLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaxLabel.Location = New System.Drawing.Point(35, 210)
        Me.MaxLabel.Name = "MaxLabel"
        Me.MaxLabel.Size = New System.Drawing.Size(471, 30)
        Me.MaxLabel.TabIndex = 6
        '
        'DefaultLabel
        '
        Me.DefaultLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DefaultLabel.Location = New System.Drawing.Point(38, 184)
        Me.DefaultLabel.Name = "DefaultLabel"
        Me.DefaultLabel.Size = New System.Drawing.Size(471, 26)
        Me.DefaultLabel.TabIndex = 5
        '
        'UpdateButton
        '
        Me.UpdateButton.Location = New System.Drawing.Point(251, 149)
        Me.UpdateButton.Name = "UpdateButton"
        Me.UpdateButton.Size = New System.Drawing.Size(75, 23)
        Me.UpdateButton.TabIndex = 4
        Me.UpdateButton.Text = "Update"
        Me.UpdateButton.UseVisualStyleBackColor = True
        '
        'CurrentTextBox
        '
        Me.CurrentTextBox.Location = New System.Drawing.Point(95, 151)
        Me.CurrentTextBox.Name = "CurrentTextBox"
        Me.CurrentTextBox.Size = New System.Drawing.Size(127, 20)
        Me.CurrentTextBox.TabIndex = 3
        '
        'CurrentLabel
        '
        Me.CurrentLabel.AutoSize = True
        Me.CurrentLabel.Location = New System.Drawing.Point(38, 155)
        Me.CurrentLabel.Name = "CurrentLabel"
        Me.CurrentLabel.Size = New System.Drawing.Size(44, 13)
        Me.CurrentLabel.TabIndex = 2
        Me.CurrentLabel.Text = "Current:"
        '
        'CapListBox
        '
        Me.CapListBox.FormattingEnabled = True
        Me.CapListBox.Location = New System.Drawing.Point(38, 47)
        Me.CapListBox.Name = "CapListBox"
        Me.CapListBox.Size = New System.Drawing.Size(318, 95)
        Me.CapListBox.TabIndex = 1
        '
        'CapComboBox
        '
        Me.CapComboBox.FormattingEnabled = True
        Me.CapComboBox.Items.AddRange(New Object() {"AutoBright", "Brightness", "Contrast", "Gamma", "Physical Width", "Physical Height", "Pixel Type", "Rotation", "Units", "X Native Resolution", "Y Native Resolution", "X Resolution", "Y Resolution", "X Scaling", "Y Scaling", "Device Online", "Indicators", "Lamp State", "UIControllable", "Transfer Count", "Auto Feed", "Auto Scan", "Clear Page", "Duplex", "Duplex Enabled", "Feeder Enabled", "Feeder Loaded", "Feed Page", "Paper Detectable"})
        Me.CapComboBox.Location = New System.Drawing.Point(38, 20)
        Me.CapComboBox.Name = "CapComboBox"
        Me.CapComboBox.Size = New System.Drawing.Size(321, 21)
        Me.CapComboBox.TabIndex = 0
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(710, 484)
        Me.Controls.Add(Me.LayoutGroupBox)
        Me.Controls.Add(Me.CaptionGroupBox)
        Me.Controls.Add(Me.CapabilitiesGroupBox)
        Me.Controls.Add(Me.DescriptionListBox)
        Me.Controls.Add(Me.ErrorLabel)
        Me.Controls.Add(Me.DefaultGroupBox)
        Me.Controls.Add(Me.CaptionButton)
        Me.Controls.Add(Me.LayoutButton)
        Me.Controls.Add(Me.CapButton)
        Me.Controls.Add(Me.CloseButton)
        Me.Controls.Add(Me.StartButton)
        Me.Controls.Add(Me.OpenButton)
        Me.Controls.Add(Me.SourceButton)
        Me.Controls.Add(Me.MainMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.MainMenu
        Me.MinimizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TwainPRO Demo"
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.DefaultGroupBox.ResumeLayout(False)
        Me.DefaultGroupBox.PerformLayout()
        Me.CaptionGroupBox.ResumeLayout(False)
        Me.CaptionGroupBox.PerformLayout()
        Me.LayoutGroupBox.ResumeLayout(False)
        Me.LayoutGroupBox.PerformLayout()
        Me.CapabilitiesGroupBox.ResumeLayout(False)
        Me.CapabilitiesGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents SourceButton As System.Windows.Forms.Button
    Friend WithEvents OpenButton As System.Windows.Forms.Button
    Friend WithEvents CloseButton As System.Windows.Forms.Button
    Friend WithEvents StartButton As System.Windows.Forms.Button
    Friend WithEvents LayoutButton As System.Windows.Forms.Button
    Friend WithEvents CapButton As System.Windows.Forms.Button
    Friend WithEvents CaptionButton As System.Windows.Forms.Button
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TwainProToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DefaultGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents SaveCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents UICheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ErrorLabel As System.Windows.Forms.Label
    Friend WithEvents DescriptionListBox As System.Windows.Forms.ListBox
    Friend WithEvents CaptionGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents HAlignLabel As System.Windows.Forms.Label
    Friend WithEvents VAlignLabel As System.Windows.Forms.Label
    Friend WithEvents VComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents HComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents CaptionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CapHTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CapWTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CapTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CapLTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CaptionLabel As System.Windows.Forms.Label
    Friend WithEvents CapHLabel As System.Windows.Forms.Label
    Friend WithEvents CapWLabel As System.Windows.Forms.Label
    Friend WithEvents CapTLabel As System.Windows.Forms.Label
    Friend WithEvents CapLLabel As System.Windows.Forms.Label
    Friend WithEvents ShadowCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ClipCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents LayoutGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents LayoutLTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LayoutLLabel As System.Windows.Forms.Label
    Friend WithEvents LayoutTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LayoutTLabel As System.Windows.Forms.Label
    Friend WithEvents LayoutRTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LayoutRLabel As System.Windows.Forms.Label
    Friend WithEvents LayoutBTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LayoutBLabel As System.Windows.Forms.Label
    Friend WithEvents UpdateButtonLayout As System.Windows.Forms.Button
    Friend WithEvents CapabilitiesGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents MinLabel As System.Windows.Forms.Label
    Friend WithEvents MaxLabel As System.Windows.Forms.Label
    Friend WithEvents DefaultLabel As System.Windows.Forms.Label
    Friend WithEvents UpdateButton As System.Windows.Forms.Button
    Friend WithEvents CurrentTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CurrentLabel As System.Windows.Forms.Label
    Friend WithEvents CapListBox As System.Windows.Forms.ListBox
    Friend WithEvents CapComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents LabelUnits As System.Windows.Forms.Label
    Friend WithEvents TwainPro1 As Accusoft.TwainProSdk.TwainPro

End Class

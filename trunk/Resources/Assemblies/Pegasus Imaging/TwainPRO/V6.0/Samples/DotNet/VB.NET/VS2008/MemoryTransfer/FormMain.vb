'/********************************************************************
'* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"         *
'* with no restrictions on use or modification. No warranty for      *
'* use of this sample code is provided by Pegasus.                   *
'*********************************************************************/

Imports System.Runtime.InteropServices
Imports Accusoft.TwainProSdk
Imports Accusoft.ImagXpressSdk

Public Class FormMain
    Inherits System.Windows.Forms.Form

    <DllImport("kernel32.dll")> _
    Private Shared Function GlobalFree(ByVal hMem As Integer) As Integer

    End Function
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents TwainPro1 As Accusoft.TwainProSdk.TwainPro
    Friend WithEvents checkBoxShowUI As System.Windows.Forms.CheckBox

    Dim twainDevice As Accusoft.TwainProSdk.TwainDevice

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (ImageXView1.Image Is Nothing) Then

                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If (Not twainDevice Is Nothing) Then

                twainDevice.Dispose()
                twainDevice = Nothing
            End If
            If Not (TwainPro1 Is Nothing) Then

                TwainPro1.Dispose()
                TwainPro1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If

        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents buttonScan As System.Windows.Forms.Button
    Friend WithEvents labelCompressTypes As System.Windows.Forms.Label
    Friend WithEvents hScrollBarJpegQual As System.Windows.Forms.HScrollBar
    Friend WithEvents comboBoxCompression As System.Windows.Forms.ComboBox
    Friend WithEvents buttonSelectSource As System.Windows.Forms.Button
    Friend WithEvents labelJpegQual As System.Windows.Forms.Label
    Friend WithEvents labelJpegQualVal As System.Windows.Forms.Label
    Friend WithEvents labelStatus As System.Windows.Forms.Label
    Friend WithEvents checkBoxUseViewer As System.Windows.Forms.CheckBox
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolBar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpresMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents TwainProMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.buttonScan = New System.Windows.Forms.Button
        Me.labelCompressTypes = New System.Windows.Forms.Label
        Me.hScrollBarJpegQual = New System.Windows.Forms.HScrollBar
        Me.comboBoxCompression = New System.Windows.Forms.ComboBox
        Me.buttonSelectSource = New System.Windows.Forms.Button
        Me.labelJpegQual = New System.Windows.Forms.Label
        Me.labelJpegQualVal = New System.Windows.Forms.Label
        Me.labelStatus = New System.Windows.Forms.Label
        Me.checkBoxUseViewer = New System.Windows.Forms.CheckBox
        Me.lstInfo = New System.Windows.Forms.ListBox
        Me.lstStatus = New System.Windows.Forms.ListBox
        Me.lblLoadStatus = New System.Windows.Forms.Label
        Me.lblLastError = New System.Windows.Forms.Label
        Me.lblerror = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuQuit = New System.Windows.Forms.MenuItem
        Me.mnuToolBar = New System.Windows.Forms.MenuItem
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem
        Me.mnuAbout = New System.Windows.Forms.MenuItem
        Me.ImagXpresMenuItem = New System.Windows.Forms.MenuItem
        Me.TwainProMenuItem = New System.Windows.Forms.MenuItem
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.TwainPro1 = New Accusoft.TwainProSdk.TwainPro(Me.components)
        Me.checkBoxShowUI = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'buttonScan
        '
        Me.buttonScan.BackColor = System.Drawing.SystemColors.Control
        Me.buttonScan.Cursor = System.Windows.Forms.Cursors.Default
        Me.buttonScan.Enabled = False
        Me.buttonScan.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonScan.ForeColor = System.Drawing.SystemColors.ControlText
        Me.buttonScan.Location = New System.Drawing.Point(24, 136)
        Me.buttonScan.Name = "buttonScan"
        Me.buttonScan.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.buttonScan.Size = New System.Drawing.Size(121, 41)
        Me.buttonScan.TabIndex = 20
        Me.buttonScan.Text = "Scan"
        Me.buttonScan.UseVisualStyleBackColor = False
        '
        'labelCompressTypes
        '
        Me.labelCompressTypes.Location = New System.Drawing.Point(21, 190)
        Me.labelCompressTypes.Name = "labelCompressTypes"
        Me.labelCompressTypes.Size = New System.Drawing.Size(112, 16)
        Me.labelCompressTypes.TabIndex = 18
        Me.labelCompressTypes.Text = "Compression Types:"
        '
        'hScrollBarJpegQual
        '
        Me.hScrollBarJpegQual.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.hScrollBarJpegQual.Cursor = System.Windows.Forms.Cursors.Default
        Me.hScrollBarJpegQual.Enabled = False
        Me.hScrollBarJpegQual.LargeChange = 1
        Me.hScrollBarJpegQual.Location = New System.Drawing.Point(21, 273)
        Me.hScrollBarJpegQual.Name = "hScrollBarJpegQual"
        Me.hScrollBarJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.hScrollBarJpegQual.Size = New System.Drawing.Size(129, 17)
        Me.hScrollBarJpegQual.TabIndex = 24
        Me.hScrollBarJpegQual.TabStop = True
        Me.hScrollBarJpegQual.Value = 80
        '
        'comboBoxCompression
        '
        Me.comboBoxCompression.BackColor = System.Drawing.SystemColors.Window
        Me.comboBoxCompression.Cursor = System.Windows.Forms.Cursors.Default
        Me.comboBoxCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxCompression.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboBoxCompression.ForeColor = System.Drawing.SystemColors.WindowText
        Me.comboBoxCompression.Location = New System.Drawing.Point(21, 214)
        Me.comboBoxCompression.Name = "comboBoxCompression"
        Me.comboBoxCompression.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.comboBoxCompression.Size = New System.Drawing.Size(129, 22)
        Me.comboBoxCompression.TabIndex = 22
        '
        'buttonSelectSource
        '
        Me.buttonSelectSource.BackColor = System.Drawing.SystemColors.Control
        Me.buttonSelectSource.Cursor = System.Windows.Forms.Cursors.Default
        Me.buttonSelectSource.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonSelectSource.ForeColor = System.Drawing.SystemColors.ControlText
        Me.buttonSelectSource.Location = New System.Drawing.Point(24, 80)
        Me.buttonSelectSource.Name = "buttonSelectSource"
        Me.buttonSelectSource.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.buttonSelectSource.Size = New System.Drawing.Size(121, 41)
        Me.buttonSelectSource.TabIndex = 19
        Me.buttonSelectSource.Text = "Select Source"
        Me.buttonSelectSource.UseVisualStyleBackColor = False
        '
        'labelJpegQual
        '
        Me.labelJpegQual.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.labelJpegQual.BackColor = System.Drawing.SystemColors.Control
        Me.labelJpegQual.Cursor = System.Windows.Forms.Cursors.Default
        Me.labelJpegQual.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelJpegQual.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelJpegQual.Location = New System.Drawing.Point(21, 249)
        Me.labelJpegQual.Name = "labelJpegQual"
        Me.labelJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labelJpegQual.Size = New System.Drawing.Size(84, 17)
        Me.labelJpegQual.TabIndex = 26
        Me.labelJpegQual.Text = "JPEG Quality:"
        '
        'labelJpegQualVal
        '
        Me.labelJpegQualVal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.labelJpegQualVal.BackColor = System.Drawing.SystemColors.Control
        Me.labelJpegQualVal.Cursor = System.Windows.Forms.Cursors.Default
        Me.labelJpegQualVal.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelJpegQualVal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelJpegQualVal.Location = New System.Drawing.Point(109, 249)
        Me.labelJpegQualVal.Name = "labelJpegQualVal"
        Me.labelJpegQualVal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labelJpegQualVal.Size = New System.Drawing.Size(25, 17)
        Me.labelJpegQualVal.TabIndex = 25
        Me.labelJpegQualVal.Text = "80"
        '
        'labelStatus
        '
        Me.labelStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.labelStatus.BackColor = System.Drawing.SystemColors.Control
        Me.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelStatus.Cursor = System.Windows.Forms.Cursors.Default
        Me.labelStatus.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelStatus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelStatus.Location = New System.Drawing.Point(24, 363)
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labelStatus.Size = New System.Drawing.Size(440, 33)
        Me.labelStatus.TabIndex = 21
        Me.labelStatus.Text = "Select and Scan"
        '
        'checkBoxUseViewer
        '
        Me.checkBoxUseViewer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.checkBoxUseViewer.BackColor = System.Drawing.SystemColors.Control
        Me.checkBoxUseViewer.Checked = True
        Me.checkBoxUseViewer.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkBoxUseViewer.Cursor = System.Windows.Forms.Cursors.Default
        Me.checkBoxUseViewer.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkBoxUseViewer.ForeColor = System.Drawing.SystemColors.ControlText
        Me.checkBoxUseViewer.Location = New System.Drawing.Point(21, 306)
        Me.checkBoxUseViewer.Name = "checkBoxUseViewer"
        Me.checkBoxUseViewer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.checkBoxUseViewer.Size = New System.Drawing.Size(129, 17)
        Me.checkBoxUseViewer.TabIndex = 27
        Me.checkBoxUseViewer.Text = "Use Viewer"
        Me.checkBoxUseViewer.UseVisualStyleBackColor = False
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using the Memory Transfer mode and ICAP_COMPRESSION capability."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(688, 56)
        Me.lstInfo.TabIndex = 33
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstStatus.Location = New System.Drawing.Point(472, 97)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(232, 121)
        Me.lstStatus.TabIndex = 32
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLoadStatus.Location = New System.Drawing.Point(472, 80)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(192, 13)
        Me.lblLoadStatus.TabIndex = 31
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLastError.Location = New System.Drawing.Point(472, 232)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(112, 22)
        Me.lblLastError.TabIndex = 30
        Me.lblLastError.Text = "Last Error:"
        '
        'lblerror
        '
        Me.lblerror.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblerror.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblerror.Location = New System.Drawing.Point(472, 254)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(224, 141)
        Me.lblerror.TabIndex = 29
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolBar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuQuit})
        Me.mnuFile.Text = "&File"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 0
        Me.mnuQuit.Text = "E&xit"
        '
        'mnuToolBar
        '
        Me.mnuToolBar.Index = 1
        Me.mnuToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolBar.Text = "&ToolBar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ImagXpresMenuItem, Me.TwainProMenuItem})
        Me.mnuAbout.Text = "&About"
        '
        'ImagXpresMenuItem
        '
        Me.ImagXpresMenuItem.Index = 0
        Me.ImagXpresMenuItem.Text = "ImagXpress"
        '
        'TwainProMenuItem
        '
        Me.TwainProMenuItem.Index = 1
        Me.TwainProMenuItem.Text = "TwainPro"
        '
        'ImagXpress1
        '
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit
        Me.ImageXView1.Location = New System.Drawing.Point(160, 80)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(304, 280)
        Me.ImageXView1.TabIndex = 34
        '
        'checkBoxShowUI
        '
        Me.checkBoxShowUI.AutoSize = True
        Me.checkBoxShowUI.Checked = True
        Me.checkBoxShowUI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkBoxShowUI.Location = New System.Drawing.Point(21, 329)
        Me.checkBoxShowUI.Name = "checkBoxShowUI"
        Me.checkBoxShowUI.Size = New System.Drawing.Size(125, 17)
        Me.checkBoxShowUI.TabIndex = 35
        Me.checkBoxShowUI.Text = "Show User Interface"
        Me.checkBoxShowUI.UseVisualStyleBackColor = True
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(722, 404)
        Me.Controls.Add(Me.checkBoxShowUI)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.lstStatus)
        Me.Controls.Add(Me.lblLoadStatus)
        Me.Controls.Add(Me.lblLastError)
        Me.Controls.Add(Me.lblerror)
        Me.Controls.Add(Me.buttonScan)
        Me.Controls.Add(Me.labelCompressTypes)
        Me.Controls.Add(Me.hScrollBarJpegQual)
        Me.Controls.Add(Me.comboBoxCompression)
        Me.Controls.Add(Me.buttonSelectSource)
        Me.Controls.Add(Me.labelJpegQual)
        Me.Controls.Add(Me.labelJpegQualVal)
        Me.Controls.Add(Me.labelStatus)
        Me.Controls.Add(Me.checkBoxUseViewer)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Memory Transfer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As Accusoft.ImagXpressSdk.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

#End Region

    Private Sub buttonSelectSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSelectSource.Click
        labelStatus.Text = "Select Scanner"
        Try
            twainDevice.SelectSource()
        Catch ex As System.Runtime.InteropServices.SEHException
            MessageBox.Show(ex.Message)

        End Try
        UpdateCompressionTypes()
        buttonScan.Enabled = True
    End Sub

    Private Sub buttonScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonScan.Click
        Try
            labelStatus.Text = "Opening Scanner"
            Application.DoEvents()
            twainDevice.OpenSession()

            If (twainDevice.TransferMode = TransferMode.TwsxMemory) Then
                Dim capability As AdvancedCapability = AdvancedCapability.IcapCompression

                ' Only do the following if the Capability is supported and
                ' there is more than one compression type to set. Otherwise,
                ' the data source may say they support this capability but
                ' might return an error if you try to set to TWCP_NONE and
                ' it is the only type. All data sources must support TWCP_NONE.
                If (twainDevice.IsCapabilitySupported(capability) And comboBoxCompression.Items.Count > 1) Then

                    labelStatus.Text = "Setting Compression Mode"

                    Dim strCompressType As String = comboBoxCompression.SelectedItem

                    Dim myCap As CapabilityContainerOneValueFloat = New CapabilityContainerOneValueFloat(capability)

                    Select Case (strCompressType)

                        Case "TWCP_NONE"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpNone)
                            Exit Select

                        Case "TWCP_PACKBITS"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpPackbits)
                            Exit Select

                        Case "TWCP_GROUP31D"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpGroup31D)
                            Exit Select

                        Case "TWCP_GROUP31DEOL"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpGroup31DEol)
                            Exit Select

                        Case "TWCP_GROUP32D"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpGroup32D)
                            Exit Select

                        Case "TWCP_GROUP4"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpGroup4)
                            Exit Select

                        Case "TWCP_JPEG"
                            myCap.Value = CSng(AdvancedCapabilityConstants.TwcpJpeg)
                            Exit Select
                    End Select

                    twainDevice.SetCapability(myCap)

                    If (strCompressType = "TWCP_JPEG") Then

                        ' Set the JPEG Quality factor

                        Dim capJpeg As AdvancedCapability = AdvancedCapability.IcapJpegPixelType
                        Dim capJpegContainer As CapabilityContainer = twainDevice.GetCapability(capJpeg)

                        If (twainDevice.IsCapabilitySupported(capJpeg)) Then
                            If (capJpegContainer.GetType().ToString() = "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat") Then

                                Dim myJpegCap As CapabilityContainerOneValueFloat = New CapabilityContainerOneValueFloat(capJpeg)

                                myJpegCap.Value = CType(hScrollBarJpegQual.Value, Single)
                                twainDevice.SetCapability(myJpegCap)
                            End If
                        End If

                        ' You could also negotiate the ICAP_JPEGPIXELTYPE capability.
                        ' I am leaving this to the DataSource
                    ElseIf (strCompressType <> "TWCP_NONE") Then

                        ' The following is done to ensure the TIFF file can be read correctly by the Imaging for Windows
                        ' Apparently Imaging for Windows must ignore the baseline tiff tag, PhotometricInterpretation.
                        ' ImagXpress does not need the following as it correctly reads in the PhotometricInterpretation tag
                        ' This will ensure that the TIFF file created has 0 as white and 1 as black

                        ' We will set the ICAP_PIXELFLAVOR first, although for CCITT compression types
                        ' ICAP_PIXELFLAVORCODES can override ICAP_PIXELFLAVOR
                        Dim capPixel As AdvancedCapability = AdvancedCapability.IcapPixelFlavor
                        Dim capPixelContainer As CapabilityContainer = twainDevice.GetCapability(capPixel)

                        If (twainDevice.IsCapabilitySupported(capPixel)) Then

                            If (capPixelContainer.GetType().ToString() = "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat") Then

                                Dim myPixelCap As CapabilityContainerOneValueFloat = New CapabilityContainerOneValueFloat(capPixel)

                                myPixelCap.Value = CType(AdvancedCapabilityConstants.TwpfVanilla, Single)
                                twainDevice.SetCapability(myPixelCap)
                            End If
                        End If

                        Dim capPixelCode As AdvancedCapability = AdvancedCapability.IcapPixelFlavorCodes
                        Dim capPixelCodeContainer As CapabilityContainer = twainDevice.GetCapability(capPixelCode)

                        If (strCompressType <> "TWCP_PACKBITS" And twainDevice.IsCapabilitySupported(capPixelCode)) Then

                            If (capPixelCodeContainer.GetType().ToString() = "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat") Then


                                Dim capFlavor As CapabilityContainerOneValueFloat = CType(twainDevice.GetCapability(capPixelCode), CapabilityContainerOneValueFloat)

                                If (capFlavor.Value <> CType(AdvancedCapabilityConstants.TwpfVanilla, Single)) Then

                                    Dim myPixelCodeCap As CapabilityContainerOneValueFloat = New CapabilityContainerOneValueFloat(capPixelCode)

                                    myPixelCodeCap.Value = CSng(AdvancedCapabilityConstants.TwpfVanilla)
                                    twainDevice.SetCapability(myPixelCodeCap)   ' Set it only if we have to
                                End If
                            End If

                        End If
                    End If
                Else
                    twainDevice.TransferMode = TransferMode.TwsxNative
                    labelStatus.Text = "TwainPRO forced to Native Transfer Mode. Compression unavailable."
                End If
            Else
                twainDevice.TransferMode = TransferMode.TwsxNative
                labelStatus.Text = "TwainPRO forced to Native Transfer Mode. Compression unavailable."
            End If
            twainDevice.StartSession()
            labelStatus.Text = "Select and Scan"
        Catch ex As Accusoft.TwainProSdk.TwainProException
            MessageBox.Show(ex.Message)
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        Finally
            'Set transfer mode back to memory
            twainDevice.TransferMode = TransferMode.TwsxMemory
        End Try
    End Sub

    Private Sub UpdateCompressionTypes()
        Try

            labelStatus.Text = "Querying Compression Capabilities"
            Application.DoEvents()

            twainDevice.OpenSession()

            Dim capability As AdvancedCapability = AdvancedCapability.IcapCompression

            comboBoxCompression.Items.Clear()
            comboBoxCompression.Items.Add("TWCP_NONE")
            comboBoxCompression.SelectedIndex = 0

            If (twainDevice.IsCapabilitySupported(capability)) Then

                Dim capcontainer As CapabilityContainer = twainDevice.GetCapability(capability)

                If (capcontainer.GetType().ToString() = "Accusoft.TwainProSdk.CapabilityContainerEnum") Then
                    Dim index As Integer
                    Dim myCap As CapabilityContainerEnum = CType(twainDevice.GetCapability(capability), CapabilityContainerEnum)

                    For index = 0 To myCap.Values.Count - 1 Step 1
                        Dim result As Single = CType(myCap.Values(index), CapabilityContainerOneValueFloat).Value

                        If (result = CSng(AdvancedCapabilityConstants.TwifNone)) Then
                            ' Already handled
                        ElseIf (result = CSng(AdvancedCapabilityConstants.TwcpPackbits)) Then
                            comboBoxCompression.Items.Add("TWCP_PACKBITS")
                        ElseIf (result = CSng(AdvancedCapabilityConstants.TwcpGroup31D)) Then
                            comboBoxCompression.Items.Add("TWCP_GROUP31D")
                        ElseIf (result = CSng(AdvancedCapabilityConstants.TwcpGroup31DEol)) Then
                            comboBoxCompression.Items.Add("TWCP_GROUP31DEOL")
                        ElseIf (result = CSng(AdvancedCapabilityConstants.TwcpGroup32D)) Then
                            comboBoxCompression.Items.Add("TWCP_GROUP32D")
                        ElseIf (result = CSng(AdvancedCapabilityConstants.TwcpGroup4)) Then
                            comboBoxCompression.Items.Add("TWCP_GROUP4")
                        ElseIf (result = CSng(AdvancedCapabilityConstants.TwcpJpeg)) Then
                            comboBoxCompression.Items.Add("TWCP_JPEG")
                        End If
                    Next
                End If
            End If

            twainDevice.CloseSession()
            labelStatus.Text = "Select and Scan"
        Catch ex As Accusoft.TwainProSdk.TwainProException
            MessageBox.Show(ex.Message)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub hScrollBarJpegQual_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarJpegQual.ValueChanged
        labelJpegQualVal.Text = hScrollBarJpegQual.Value.ToString()
    End Sub

    Private Sub comboBoxCompression_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles comboBoxCompression.SelectedValueChanged
        If (comboBoxCompression.SelectedItem = "TWCP_JPEG") Then
            hScrollBarJpegQual.Enabled = True
        Else
            hScrollBarJpegQual.Enabled = False
        End If
    End Sub

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As Accusoft.ImagXpressSdk.ProgressEventArgs) Handles ImagXpress1.ProgressEvent

        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))

            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If
    End Sub

    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As Accusoft.ImagXpressSdk.ImageStatusEventArgs) Handles ImagXpress1.ImageStatusEvent
        lstStatus.Items.Add(e.Status.ToString)
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub


    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '**The UnlockRuntime function must be called to distribute the runtime**
        'ImagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
        'TwainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

        ' Initialize TwainPRO to receive events
        twainDevice = New TwainDevice(TwainPro1)

        AddHandler twainDevice.Scanned, AddressOf twainDevice_Scanned
        AddHandler twainDevice.Scanning, AddressOf twainDevice_Scanning

        ' Let TwainPRO know that you wish to use Buffered Memory Mode
        twainDevice.TransferMode = TransferMode.TwsxMemory

        Application.EnableVisualStyles()
    End Sub

    Private Sub twainDevice_Scanning(ByVal sender As Object, ByVal e As ScanningEventArgs)
        labelStatus.Text = "Scanning..."
    End Sub

    Private Sub twainDevice_Scanned(ByVal sender As Object, ByVal e As ScannedEventArgs)
        Try
            labelStatus.Text = ""
            ' Need to check the Compression type to see if we are transferring a DIB
            ' or we are transferring compressed data.
            If (e.ScannedImage.ScannedImageData.Compression = ScannedImageCompression.TwcpNone) Then
                Dim hImage As Integer = e.ScannedImage.ToHdib().ToInt32()
                If (hImage <> 0) Then
                    Try
                        If (Not (ImageXView1.Image) Is Nothing) Then
                            ImageXView1.Image.Dispose()
                            ImageXView1.Image = Nothing
                        End If
                        ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(ImagXpress1, New System.IntPtr(hImage))
                        If (checkBoxUseViewer.Checked = True) Then
                            Dim formViewer As FormViewer = New FormViewer()
                            formViewer.ShowEx(ImageXView1.Image)
                        End If
                    Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
                        PegasusError(ex, lblerror)
                    End Try
                End If
            Else
                Dim hImage As Integer = e.ScannedImage.ToHbitmap().ToInt32()
                If (hImage <> 0) Then
                    Try
                        If (Not (ImageXView1.Image) Is Nothing) Then
                            ImageXView1.Image.Dispose()
                            ImageXView1.Image = Nothing
                        End If
                        ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHbitmap(ImagXpress1, New System.IntPtr(hImage))
                        If (checkBoxUseViewer.Checked = True) Then
                            Dim formViewer As FormViewer = New FormViewer()
                            formViewer.ShowEx(ImageXView1.Image)
                        End If
                    Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
                        PegasusError(ex, lblerror)
                    End Try
                    GlobalFree(hImage)
                End If
            End If
            Application.DoEvents()
            e.Cancel = False
        Catch ex As Accusoft.ImagXpressSdk.ImagXpressException

            MessageBox.Show(ex.Message)

        Catch ex As Accusoft.TwainProSdk.TwainProException

            MessageBox.Show(ex.Message)

        Catch ex As System.Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()

    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click

        If ImageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False
        Else
            mnuToolbarShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True

        End If

    End Sub


    Private Sub ImagXpresMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpresMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub TwainProMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TwainProMenuItem.Click
        TwainPro1.AboutBox()
    End Sub

    Private Sub checkBoxShowUI_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkBoxShowUI.CheckedChanged
        If (Not twainDevice Is Nothing) Then
            If (checkBoxShowUI.Checked = True) Then
                twainDevice.ShowUserInterface = True
            Else
                twainDevice.ShowUserInterface = False
            End If
        End If
    End Sub
End Class

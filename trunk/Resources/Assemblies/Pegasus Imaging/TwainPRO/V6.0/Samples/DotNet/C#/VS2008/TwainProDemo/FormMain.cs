/********************************************************************'
* Copyright 2008-2009 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"         *'
* with no restrictions on use or modification. No warranty for      *'
* use of this sample code is provided by Pegasus.                   *'
*********************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Accusoft.ImagXpressSdk;
using Accusoft.TwainProSdk;

namespace TwainProDemoCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonOpenSession;
		private System.Windows.Forms.Button buttonStartSession;
		private System.Windows.Forms.Button buttonCloseSession;
		private System.Windows.Forms.Button buttonShowCaps;
		private System.Windows.Forms.Button buttonShowLayout;
		private System.Windows.Forms.Button buttonShowCaption;
		private System.Windows.Forms.Button buttonSelectSource;
        private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.GroupBox groupBoxCaption;
		private System.Windows.Forms.CheckBox checkBoxClipCaption;
		private System.Windows.Forms.CheckBox checkBoxShadowText;
		private System.Windows.Forms.Label labelCapLeft;
		private System.Windows.Forms.Label labelCapTop;
		private System.Windows.Forms.Label labelCapWidth;
		private System.Windows.Forms.Label labelCapHeight;
		private System.Windows.Forms.Label labelCaption;
		private System.Windows.Forms.Label labelHAlign;
		private System.Windows.Forms.Label labelVAlign;
		private System.Windows.Forms.TextBox textBoxCapLeft;
		private System.Windows.Forms.TextBox textBoxCapTop;
		private System.Windows.Forms.TextBox textBoxCapWidth;
		private System.Windows.Forms.TextBox textBoxCapHeight;
		private System.Windows.Forms.TextBox textBoxCaption;
		private System.Windows.Forms.ComboBox comboBoxHAlign;
		private System.Windows.Forms.ComboBox comboBoxVAlign;
		private System.Windows.Forms.GroupBox groupBoxCaps;
		private System.Windows.Forms.ComboBox comboBoxCaps;
		private System.Windows.Forms.Label labelDefault;
		private System.Windows.Forms.ListBox listBoxCaps;
		private System.Windows.Forms.Label labelCurrent;
		private System.Windows.Forms.TextBox textBoxCurrent;
		private System.Windows.Forms.Button buttonUpdate;
		private System.Windows.Forms.GroupBox groupBoxLayout;
		private System.Windows.Forms.Label labelImageLeft;
		private System.Windows.Forms.Label labelImageTop;
		private System.Windows.Forms.Label labelImageWidth;
		private System.Windows.Forms.Label labelImageHeight;
        private System.Windows.Forms.Button buttonLayoutUpdate;
		private System.Windows.Forms.CheckBox checkBoxShowUI;
        private System.Windows.Forms.CheckBox checkBoxSave;
		private System.Windows.Forms.TextBox textBoxIL;
		private System.Windows.Forms.TextBox textBoxIT;
		private System.Windows.Forms.TextBox textBoxIR;
		private System.Windows.Forms.TextBox textBoxIB;
		private System.Windows.Forms.Label labelMin;
        private System.Windows.Forms.Label labelMax;
		private System.Windows.Forms.ListBox listBoxInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItemFile;
		private System.Windows.Forms.MenuItem menuItemQuit;
		private System.Windows.Forms.MenuItem menuItemAbout;
        private MenuItem menuItemTwainPro;
        private IContainer components;
        private Accusoft.TwainProSdk.TwainDevice twainDevice;
        private Accusoft.TwainProSdk.CapabilityContainer capContainer;
        private Accusoft.TwainProSdk.Capability selectedCapability;

        private GroupBox groupBoxStart;
        private MenuItem menuItemImagXpress;
        private Label labelUnits;
        private Accusoft.TwainProSdk.TwainPro twainPro1;

        private System.Int32 imageCount;

        public FormMain()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
                if (twainDevice != null)
                {
                    twainDevice.Dispose();
                    twainDevice = null;
                }
            }
            base.Dispose(disposing);
        }

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.buttonOpenSession = new System.Windows.Forms.Button();
            this.buttonStartSession = new System.Windows.Forms.Button();
            this.buttonCloseSession = new System.Windows.Forms.Button();
            this.buttonShowCaps = new System.Windows.Forms.Button();
            this.buttonShowLayout = new System.Windows.Forms.Button();
            this.buttonShowCaption = new System.Windows.Forms.Button();
            this.buttonSelectSource = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.groupBoxCaption = new System.Windows.Forms.GroupBox();
            this.comboBoxVAlign = new System.Windows.Forms.ComboBox();
            this.comboBoxHAlign = new System.Windows.Forms.ComboBox();
            this.textBoxCaption = new System.Windows.Forms.TextBox();
            this.textBoxCapHeight = new System.Windows.Forms.TextBox();
            this.textBoxCapWidth = new System.Windows.Forms.TextBox();
            this.textBoxCapTop = new System.Windows.Forms.TextBox();
            this.textBoxCapLeft = new System.Windows.Forms.TextBox();
            this.labelVAlign = new System.Windows.Forms.Label();
            this.labelHAlign = new System.Windows.Forms.Label();
            this.labelCaption = new System.Windows.Forms.Label();
            this.labelCapHeight = new System.Windows.Forms.Label();
            this.labelCapWidth = new System.Windows.Forms.Label();
            this.labelCapTop = new System.Windows.Forms.Label();
            this.labelCapLeft = new System.Windows.Forms.Label();
            this.checkBoxShadowText = new System.Windows.Forms.CheckBox();
            this.checkBoxClipCaption = new System.Windows.Forms.CheckBox();
            this.groupBoxLayout = new System.Windows.Forms.GroupBox();
            this.textBoxIB = new System.Windows.Forms.TextBox();
            this.textBoxIR = new System.Windows.Forms.TextBox();
            this.textBoxIT = new System.Windows.Forms.TextBox();
            this.textBoxIL = new System.Windows.Forms.TextBox();
            this.buttonLayoutUpdate = new System.Windows.Forms.Button();
            this.labelImageHeight = new System.Windows.Forms.Label();
            this.labelImageWidth = new System.Windows.Forms.Label();
            this.labelImageTop = new System.Windows.Forms.Label();
            this.labelImageLeft = new System.Windows.Forms.Label();
            this.groupBoxCaps = new System.Windows.Forms.GroupBox();
            this.labelUnits = new System.Windows.Forms.Label();
            this.labelDefault = new System.Windows.Forms.Label();
            this.labelMax = new System.Windows.Forms.Label();
            this.labelMin = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textBoxCurrent = new System.Windows.Forms.TextBox();
            this.labelCurrent = new System.Windows.Forms.Label();
            this.listBoxCaps = new System.Windows.Forms.ListBox();
            this.comboBoxCaps = new System.Windows.Forms.ComboBox();
            this.checkBoxSave = new System.Windows.Forms.CheckBox();
            this.checkBoxShowUI = new System.Windows.Forms.CheckBox();
            this.listBoxInfo = new System.Windows.Forms.ListBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemFile = new System.Windows.Forms.MenuItem();
            this.menuItemQuit = new System.Windows.Forms.MenuItem();
            this.menuItemAbout = new System.Windows.Forms.MenuItem();
            this.menuItemImagXpress = new System.Windows.Forms.MenuItem();
            this.menuItemTwainPro = new System.Windows.Forms.MenuItem();
            this.groupBoxStart = new System.Windows.Forms.GroupBox();
            this.twainPro1 = new Accusoft.TwainProSdk.TwainPro(this.components);
            this.groupBoxCaption.SuspendLayout();
            this.groupBoxLayout.SuspendLayout();
            this.groupBoxCaps.SuspendLayout();
            this.groupBoxStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOpenSession
            // 
            this.buttonOpenSession.Enabled = false;
            this.buttonOpenSession.Location = new System.Drawing.Point(12, 189);
            this.buttonOpenSession.Name = "buttonOpenSession";
            this.buttonOpenSession.Size = new System.Drawing.Size(128, 23);
            this.buttonOpenSession.TabIndex = 0;
            this.buttonOpenSession.Text = "Open Session";
            this.buttonOpenSession.Click += new System.EventHandler(this.buttonOpenSession_Click);
            // 
            // buttonStartSession
            // 
            this.buttonStartSession.Enabled = false;
            this.buttonStartSession.Location = new System.Drawing.Point(12, 221);
            this.buttonStartSession.Name = "buttonStartSession";
            this.buttonStartSession.Size = new System.Drawing.Size(128, 23);
            this.buttonStartSession.TabIndex = 1;
            this.buttonStartSession.Text = "Start Session\\Acquire";
            this.buttonStartSession.Click += new System.EventHandler(this.buttonStartSession_Click);
            // 
            // buttonCloseSession
            // 
            this.buttonCloseSession.Location = new System.Drawing.Point(12, 253);
            this.buttonCloseSession.Name = "buttonCloseSession";
            this.buttonCloseSession.Size = new System.Drawing.Size(128, 23);
            this.buttonCloseSession.TabIndex = 2;
            this.buttonCloseSession.Text = "Close Session";
            this.buttonCloseSession.Click += new System.EventHandler(this.buttonCloseSession_Click);
            // 
            // buttonShowCaps
            // 
            this.buttonShowCaps.Enabled = false;
            this.buttonShowCaps.Location = new System.Drawing.Point(12, 285);
            this.buttonShowCaps.Name = "buttonShowCaps";
            this.buttonShowCaps.Size = new System.Drawing.Size(128, 23);
            this.buttonShowCaps.TabIndex = 3;
            this.buttonShowCaps.Text = "Show Capabilities";
            this.buttonShowCaps.Click += new System.EventHandler(this.buttonShowCaps_Click);
            // 
            // buttonShowLayout
            // 
            this.buttonShowLayout.Enabled = false;
            this.buttonShowLayout.Location = new System.Drawing.Point(12, 317);
            this.buttonShowLayout.Name = "buttonShowLayout";
            this.buttonShowLayout.Size = new System.Drawing.Size(128, 23);
            this.buttonShowLayout.TabIndex = 4;
            this.buttonShowLayout.Text = "Show Layout";
            this.buttonShowLayout.Click += new System.EventHandler(this.buttonShowLayout_Click);
            // 
            // buttonShowCaption
            // 
            this.buttonShowCaption.Location = new System.Drawing.Point(12, 349);
            this.buttonShowCaption.Name = "buttonShowCaption";
            this.buttonShowCaption.Size = new System.Drawing.Size(128, 23);
            this.buttonShowCaption.TabIndex = 5;
            this.buttonShowCaption.Text = "Show Caption";
            this.buttonShowCaption.Click += new System.EventHandler(this.buttonShowCaption_Click);
            // 
            // buttonSelectSource
            // 
            this.buttonSelectSource.Location = new System.Drawing.Point(12, 157);
            this.buttonSelectSource.Name = "buttonSelectSource";
            this.buttonSelectSource.Size = new System.Drawing.Size(128, 23);
            this.buttonSelectSource.TabIndex = 6;
            this.buttonSelectSource.Text = "Select Source";
            this.buttonSelectSource.Click += new System.EventHandler(this.buttonSelectSource_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStatus.Location = new System.Drawing.Point(3, 404);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(602, 70);
            this.labelStatus.TabIndex = 8;
            // 
            // groupBoxCaption
            // 
            this.groupBoxCaption.Controls.Add(this.comboBoxVAlign);
            this.groupBoxCaption.Controls.Add(this.comboBoxHAlign);
            this.groupBoxCaption.Controls.Add(this.textBoxCaption);
            this.groupBoxCaption.Controls.Add(this.textBoxCapHeight);
            this.groupBoxCaption.Controls.Add(this.textBoxCapWidth);
            this.groupBoxCaption.Controls.Add(this.textBoxCapTop);
            this.groupBoxCaption.Controls.Add(this.textBoxCapLeft);
            this.groupBoxCaption.Controls.Add(this.labelVAlign);
            this.groupBoxCaption.Controls.Add(this.labelHAlign);
            this.groupBoxCaption.Controls.Add(this.labelCaption);
            this.groupBoxCaption.Controls.Add(this.labelCapHeight);
            this.groupBoxCaption.Controls.Add(this.labelCapWidth);
            this.groupBoxCaption.Controls.Add(this.labelCapTop);
            this.groupBoxCaption.Controls.Add(this.labelCapLeft);
            this.groupBoxCaption.Controls.Add(this.checkBoxShadowText);
            this.groupBoxCaption.Controls.Add(this.checkBoxClipCaption);
            this.groupBoxCaption.Location = new System.Drawing.Point(396, 147);
            this.groupBoxCaption.Name = "groupBoxCaption";
            this.groupBoxCaption.Size = new System.Drawing.Size(312, 161);
            this.groupBoxCaption.TabIndex = 0;
            this.groupBoxCaption.TabStop = false;
            this.groupBoxCaption.Text = "Caption";
            this.groupBoxCaption.Visible = false;
            // 
            // comboBoxVAlign
            // 
            this.comboBoxVAlign.Items.AddRange(new object[] {
            "Top",
            "Bottom",
            "Center"});
            this.comboBoxVAlign.Location = new System.Drawing.Point(184, 104);
            this.comboBoxVAlign.Name = "comboBoxVAlign";
            this.comboBoxVAlign.Size = new System.Drawing.Size(120, 21);
            this.comboBoxVAlign.TabIndex = 15;
            // 
            // comboBoxHAlign
            // 
            this.comboBoxHAlign.Items.AddRange(new object[] {
            "Left",
            "Right",
            "Center"});
            this.comboBoxHAlign.Location = new System.Drawing.Point(184, 56);
            this.comboBoxHAlign.Name = "comboBoxHAlign";
            this.comboBoxHAlign.Size = new System.Drawing.Size(120, 21);
            this.comboBoxHAlign.TabIndex = 14;
            // 
            // textBoxCaption
            // 
            this.textBoxCaption.Location = new System.Drawing.Point(112, 134);
            this.textBoxCaption.Name = "textBoxCaption";
            this.textBoxCaption.Size = new System.Drawing.Size(192, 20);
            this.textBoxCaption.TabIndex = 13;
            this.textBoxCaption.Text = "Pegasus Imaging Corp.";
            // 
            // textBoxCapHeight
            // 
            this.textBoxCapHeight.Location = new System.Drawing.Point(112, 110);
            this.textBoxCapHeight.Name = "textBoxCapHeight";
            this.textBoxCapHeight.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapHeight.TabIndex = 12;
            this.textBoxCapHeight.Text = "0";
            this.textBoxCapHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapWidth
            // 
            this.textBoxCapWidth.Location = new System.Drawing.Point(112, 86);
            this.textBoxCapWidth.Name = "textBoxCapWidth";
            this.textBoxCapWidth.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapWidth.TabIndex = 11;
            this.textBoxCapWidth.Text = "0";
            this.textBoxCapWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapTop
            // 
            this.textBoxCapTop.Location = new System.Drawing.Point(112, 62);
            this.textBoxCapTop.Name = "textBoxCapTop";
            this.textBoxCapTop.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapTop.TabIndex = 10;
            this.textBoxCapTop.Text = "0";
            this.textBoxCapTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxCapLeft
            // 
            this.textBoxCapLeft.Location = new System.Drawing.Point(112, 38);
            this.textBoxCapLeft.Name = "textBoxCapLeft";
            this.textBoxCapLeft.Size = new System.Drawing.Size(48, 20);
            this.textBoxCapLeft.TabIndex = 9;
            this.textBoxCapLeft.Text = "0";
            this.textBoxCapLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelVAlign
            // 
            this.labelVAlign.Location = new System.Drawing.Point(184, 88);
            this.labelVAlign.Name = "labelVAlign";
            this.labelVAlign.Size = new System.Drawing.Size(92, 16);
            this.labelVAlign.TabIndex = 8;
            this.labelVAlign.Text = "Vertical Align";
            // 
            // labelHAlign
            // 
            this.labelHAlign.Location = new System.Drawing.Point(184, 40);
            this.labelHAlign.Name = "labelHAlign";
            this.labelHAlign.Size = new System.Drawing.Size(105, 16);
            this.labelHAlign.TabIndex = 7;
            this.labelHAlign.Text = "Horizontal Align";
            // 
            // labelCaption
            // 
            this.labelCaption.Location = new System.Drawing.Point(16, 136);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(48, 16);
            this.labelCaption.TabIndex = 6;
            this.labelCaption.Text = "Caption:";
            // 
            // labelCapHeight
            // 
            this.labelCapHeight.Location = new System.Drawing.Point(16, 112);
            this.labelCapHeight.Name = "labelCapHeight";
            this.labelCapHeight.Size = new System.Drawing.Size(88, 16);
            this.labelCapHeight.TabIndex = 5;
            this.labelCapHeight.Text = "Caption Height:";
            // 
            // labelCapWidth
            // 
            this.labelCapWidth.Location = new System.Drawing.Point(16, 88);
            this.labelCapWidth.Name = "labelCapWidth";
            this.labelCapWidth.Size = new System.Drawing.Size(80, 16);
            this.labelCapWidth.TabIndex = 4;
            this.labelCapWidth.Text = "Caption Width:";
            // 
            // labelCapTop
            // 
            this.labelCapTop.Location = new System.Drawing.Point(16, 64);
            this.labelCapTop.Name = "labelCapTop";
            this.labelCapTop.Size = new System.Drawing.Size(72, 16);
            this.labelCapTop.TabIndex = 3;
            this.labelCapTop.Text = "Caption Top:";
            // 
            // labelCapLeft
            // 
            this.labelCapLeft.Location = new System.Drawing.Point(16, 40);
            this.labelCapLeft.Name = "labelCapLeft";
            this.labelCapLeft.Size = new System.Drawing.Size(72, 16);
            this.labelCapLeft.TabIndex = 2;
            this.labelCapLeft.Text = "Caption Left:";
            // 
            // checkBoxShadowText
            // 
            this.checkBoxShadowText.Location = new System.Drawing.Point(128, 16);
            this.checkBoxShadowText.Name = "checkBoxShadowText";
            this.checkBoxShadowText.Size = new System.Drawing.Size(96, 16);
            this.checkBoxShadowText.TabIndex = 1;
            this.checkBoxShadowText.Text = "Shadow Text";
            // 
            // checkBoxClipCaption
            // 
            this.checkBoxClipCaption.Location = new System.Drawing.Point(16, 16);
            this.checkBoxClipCaption.Name = "checkBoxClipCaption";
            this.checkBoxClipCaption.Size = new System.Drawing.Size(88, 16);
            this.checkBoxClipCaption.TabIndex = 0;
            this.checkBoxClipCaption.Text = "Clip Caption";
            // 
            // groupBoxLayout
            // 
            this.groupBoxLayout.Controls.Add(this.textBoxIB);
            this.groupBoxLayout.Controls.Add(this.textBoxIR);
            this.groupBoxLayout.Controls.Add(this.textBoxIT);
            this.groupBoxLayout.Controls.Add(this.textBoxIL);
            this.groupBoxLayout.Controls.Add(this.buttonLayoutUpdate);
            this.groupBoxLayout.Controls.Add(this.labelImageHeight);
            this.groupBoxLayout.Controls.Add(this.labelImageWidth);
            this.groupBoxLayout.Controls.Add(this.labelImageTop);
            this.groupBoxLayout.Controls.Add(this.labelImageLeft);
            this.groupBoxLayout.Location = new System.Drawing.Point(146, 175);
            this.groupBoxLayout.Name = "groupBoxLayout";
            this.groupBoxLayout.Size = new System.Drawing.Size(312, 216);
            this.groupBoxLayout.TabIndex = 0;
            this.groupBoxLayout.TabStop = false;
            this.groupBoxLayout.Text = "Layout";
            this.groupBoxLayout.Visible = false;
            // 
            // textBoxIB
            // 
            this.textBoxIB.Location = new System.Drawing.Point(156, 126);
            this.textBoxIB.Name = "textBoxIB";
            this.textBoxIB.Size = new System.Drawing.Size(88, 20);
            this.textBoxIB.TabIndex = 8;
            // 
            // textBoxIR
            // 
            this.textBoxIR.Location = new System.Drawing.Point(156, 94);
            this.textBoxIR.Name = "textBoxIR";
            this.textBoxIR.Size = new System.Drawing.Size(88, 20);
            this.textBoxIR.TabIndex = 7;
            // 
            // textBoxIT
            // 
            this.textBoxIT.Location = new System.Drawing.Point(156, 62);
            this.textBoxIT.Name = "textBoxIT";
            this.textBoxIT.Size = new System.Drawing.Size(88, 20);
            this.textBoxIT.TabIndex = 6;
            // 
            // textBoxIL
            // 
            this.textBoxIL.Location = new System.Drawing.Point(156, 30);
            this.textBoxIL.Name = "textBoxIL";
            this.textBoxIL.Size = new System.Drawing.Size(88, 20);
            this.textBoxIL.TabIndex = 5;
            // 
            // buttonLayoutUpdate
            // 
            this.buttonLayoutUpdate.Location = new System.Drawing.Point(112, 160);
            this.buttonLayoutUpdate.Name = "buttonLayoutUpdate";
            this.buttonLayoutUpdate.Size = new System.Drawing.Size(96, 24);
            this.buttonLayoutUpdate.TabIndex = 4;
            this.buttonLayoutUpdate.Text = "Update";
            this.buttonLayoutUpdate.Click += new System.EventHandler(this.buttonLayoutUpdate_Click);
            // 
            // labelImageHeight
            // 
            this.labelImageHeight.Location = new System.Drawing.Point(68, 128);
            this.labelImageHeight.Name = "labelImageHeight";
            this.labelImageHeight.Size = new System.Drawing.Size(80, 16);
            this.labelImageHeight.TabIndex = 3;
            this.labelImageHeight.Text = "Image Height:";
            // 
            // labelImageWidth
            // 
            this.labelImageWidth.Location = new System.Drawing.Point(68, 96);
            this.labelImageWidth.Name = "labelImageWidth";
            this.labelImageWidth.Size = new System.Drawing.Size(72, 16);
            this.labelImageWidth.TabIndex = 2;
            this.labelImageWidth.Text = "Image Width:";
            // 
            // labelImageTop
            // 
            this.labelImageTop.Location = new System.Drawing.Point(68, 64);
            this.labelImageTop.Name = "labelImageTop";
            this.labelImageTop.Size = new System.Drawing.Size(64, 16);
            this.labelImageTop.TabIndex = 1;
            this.labelImageTop.Text = "Image Top:";
            // 
            // labelImageLeft
            // 
            this.labelImageLeft.Location = new System.Drawing.Point(68, 32);
            this.labelImageLeft.Name = "labelImageLeft";
            this.labelImageLeft.Size = new System.Drawing.Size(64, 16);
            this.labelImageLeft.TabIndex = 0;
            this.labelImageLeft.Text = "Image Left:";
            // 
            // groupBoxCaps
            // 
            this.groupBoxCaps.Controls.Add(this.labelUnits);
            this.groupBoxCaps.Controls.Add(this.labelDefault);
            this.groupBoxCaps.Controls.Add(this.labelMax);
            this.groupBoxCaps.Controls.Add(this.labelMin);
            this.groupBoxCaps.Controls.Add(this.buttonUpdate);
            this.groupBoxCaps.Controls.Add(this.textBoxCurrent);
            this.groupBoxCaps.Controls.Add(this.labelCurrent);
            this.groupBoxCaps.Controls.Add(this.listBoxCaps);
            this.groupBoxCaps.Controls.Add(this.comboBoxCaps);
            this.groupBoxCaps.Location = new System.Drawing.Point(387, 110);
            this.groupBoxCaps.Name = "groupBoxCaps";
            this.groupBoxCaps.Size = new System.Drawing.Size(459, 310);
            this.groupBoxCaps.TabIndex = 0;
            this.groupBoxCaps.TabStop = false;
            this.groupBoxCaps.Text = "Capabilities";
            this.groupBoxCaps.Visible = false;
            // 
            // labelUnits
            // 
            this.labelUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUnits.Location = new System.Drawing.Point(294, 174);
            this.labelUnits.Name = "labelUnits";
            this.labelUnits.Size = new System.Drawing.Size(159, 24);
            this.labelUnits.TabIndex = 8;
            // 
            // labelDefault
            // 
            this.labelDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDefault.Location = new System.Drawing.Point(40, 200);
            this.labelDefault.Name = "labelDefault";
            this.labelDefault.Size = new System.Drawing.Size(413, 25);
            this.labelDefault.TabIndex = 1;
            this.labelDefault.Text = "labelDefault";
            // 
            // labelMax
            // 
            this.labelMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMax.Location = new System.Drawing.Point(40, 236);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(417, 25);
            this.labelMax.TabIndex = 7;
            this.labelMax.Text = "labelMax";
            // 
            // labelMin
            // 
            this.labelMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMin.Location = new System.Drawing.Point(40, 270);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(417, 25);
            this.labelMin.TabIndex = 6;
            this.labelMin.Text = "labelMin";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Enabled = false;
            this.buttonUpdate.Location = new System.Drawing.Point(208, 168);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(80, 24);
            this.buttonUpdate.TabIndex = 5;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textBoxCurrent
            // 
            this.textBoxCurrent.Location = new System.Drawing.Point(96, 170);
            this.textBoxCurrent.Name = "textBoxCurrent";
            this.textBoxCurrent.Size = new System.Drawing.Size(96, 20);
            this.textBoxCurrent.TabIndex = 4;
            // 
            // labelCurrent
            // 
            this.labelCurrent.Location = new System.Drawing.Point(40, 172);
            this.labelCurrent.Name = "labelCurrent";
            this.labelCurrent.Size = new System.Drawing.Size(48, 16);
            this.labelCurrent.TabIndex = 3;
            this.labelCurrent.Text = "Current:";
            // 
            // listBoxCaps
            // 
            this.listBoxCaps.Location = new System.Drawing.Point(32, 51);
            this.listBoxCaps.Name = "listBoxCaps";
            this.listBoxCaps.Size = new System.Drawing.Size(256, 95);
            this.listBoxCaps.TabIndex = 2;
            // 
            // comboBoxCaps
            // 
            this.comboBoxCaps.Items.AddRange(new object[] {
            "AutoBright",
            "Brightness",
            "Contrast",
            "Gamma",
            "Physical Width",
            "Physical Height",
            "Pixel Type",
            "Rotation",
            "Units",
            "X Native Resolution",
            "Y Native Resolution",
            "X Resolution",
            "Y Resolution",
            "X Scaling",
            "Y Scaling",
            "Device Online",
            "Indicators",
            "Lamp State",
            "UIControllable",
            "Transfer Count",
            "Auto Feed",
            "Auto Scan",
            "Clear Page",
            "Duplex",
            "Duplex Enabled",
            "Feeder Enabled",
            "Feeder Loaded",
            "Feed Page",
            "Paper Detectable"});
            this.comboBoxCaps.Location = new System.Drawing.Point(32, 24);
            this.comboBoxCaps.Name = "comboBoxCaps";
            this.comboBoxCaps.Size = new System.Drawing.Size(256, 21);
            this.comboBoxCaps.TabIndex = 0;
            this.comboBoxCaps.SelectedIndexChanged += new System.EventHandler(this.comboBoxCaps_SelectedIndexChanged);
            // 
            // checkBoxSave
            // 
            this.checkBoxSave.Location = new System.Drawing.Point(7, 41);
            this.checkBoxSave.Name = "checkBoxSave";
            this.checkBoxSave.Size = new System.Drawing.Size(190, 22);
            this.checkBoxSave.TabIndex = 1;
            this.checkBoxSave.Text = "Save Image as  Image-0.tif";
            // 
            // checkBoxShowUI
            // 
            this.checkBoxShowUI.Location = new System.Drawing.Point(6, 19);
            this.checkBoxShowUI.Name = "checkBoxShowUI";
            this.checkBoxShowUI.Size = new System.Drawing.Size(96, 16);
            this.checkBoxShowUI.TabIndex = 0;
            this.checkBoxShowUI.Text = "Show UI";
            this.checkBoxShowUI.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // listBoxInfo
            // 
            this.listBoxInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxInfo.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1) Selecting the device with or without the scanner\'s UI via the ShowUI property." +
                "",
            "2) Querying and setting the various TWAIN capabilities.",
            "3) Scanning a specific size page via the SetImageLayout method.",
            "4) Saving the scanned image."});
            this.listBoxInfo.Location = new System.Drawing.Point(3, 3);
            this.listBoxInfo.Name = "listBoxInfo";
            this.listBoxInfo.Size = new System.Drawing.Size(602, 82);
            this.listBoxInfo.TabIndex = 10;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemAbout});
            // 
            // menuItemFile
            // 
            this.menuItemFile.Index = 0;
            this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemQuit});
            this.menuItemFile.Text = "&File";
            // 
            // menuItemQuit
            // 
            this.menuItemQuit.Index = 0;
            this.menuItemQuit.Text = "E&xit";
            this.menuItemQuit.Click += new System.EventHandler(this.menuItemQuit_Click);
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Index = 1;
            this.menuItemAbout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemImagXpress,
            this.menuItemTwainPro});
            this.menuItemAbout.Text = "&About";
            // 
            // menuItemImagXpress
            // 
            this.menuItemImagXpress.Index = 0;
            this.menuItemImagXpress.Text = "Imag&Xpress";
            this.menuItemImagXpress.Click += new System.EventHandler(this.menuItemImagXpress_Click);
            // 
            // menuItemTwainPro
            // 
            this.menuItemTwainPro.Index = 1;
            this.menuItemTwainPro.Text = "&TwainPRO";
            this.menuItemTwainPro.Click += new System.EventHandler(this.menuItemTwainPro_Click);
            // 
            // groupBoxStart
            // 
            this.groupBoxStart.Controls.Add(this.checkBoxSave);
            this.groupBoxStart.Controls.Add(this.checkBoxShowUI);
            this.groupBoxStart.Location = new System.Drawing.Point(146, 91);
            this.groupBoxStart.Name = "groupBoxStart";
            this.groupBoxStart.Size = new System.Drawing.Size(203, 77);
            this.groupBoxStart.TabIndex = 11;
            this.groupBoxStart.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(634, 480);
            this.Controls.Add(this.groupBoxCaps);
            this.Controls.Add(this.groupBoxCaption);
            this.Controls.Add(this.groupBoxStart);
            this.Controls.Add(this.listBoxInfo);
            this.Controls.Add(this.groupBoxLayout);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonSelectSource);
            this.Controls.Add(this.buttonShowLayout);
            this.Controls.Add(this.buttonShowCaps);
            this.Controls.Add(this.buttonCloseSession);
            this.Controls.Add(this.buttonStartSession);
            this.Controls.Add(this.buttonOpenSession);
            this.Controls.Add(this.buttonShowCaption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TwainPRO Demo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBoxCaption.ResumeLayout(false);
            this.groupBoxCaption.PerformLayout();
            this.groupBoxLayout.ResumeLayout(false);
            this.groupBoxLayout.PerformLayout();
            this.groupBoxCaps.ResumeLayout(false);
            this.groupBoxCaps.PerformLayout();
            this.groupBoxStart.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            Application.EnableVisualStyles();
			Application.Run(new FormMain());
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
            try
            {

                //***Must call the UnlockRuntime method to unlock the control
                //twainPro1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

                twainDevice = new TwainDevice(twainPro1);

                // Set Debug to true to write information to a debug log file
                //twainPro1.Debug = true;
                //twainPro1.ErrorLevel = ErrorLevel.Detailed;

                // Add the Scanned event
                twainDevice.Scanned += new Accusoft.TwainProSdk.ScannedEventHandler(twainDevice_Scanned);

                comboBoxHAlign.SelectedIndex = 0;
                comboBoxVAlign.SelectedIndex = 0;

                // Position the group boxes
                groupBoxCaps.Left = groupBoxStart.Left;
                groupBoxCaption.Left = groupBoxStart.Left;
                groupBoxLayout.Left = groupBoxStart.Left;
                groupBoxCaps.Top = groupBoxStart.Top;
                groupBoxCaption.Top = groupBoxStart.Top;
                groupBoxLayout.Top = groupBoxStart.Top;
                
                imageCount = 0;
            }
            catch(TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

		private void buttonOpenSession_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";
                // Open session so that capabilities can be negotiated
                twainDevice.OpenSession();
				buttonShowCaps.Enabled = true;
				buttonShowLayout.Enabled = true;
                buttonShowCaption.Enabled = true;
                buttonStartSession.Enabled = true;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
				GetError();
			}
		}

        private void buttonStartSession_Click(object sender, System.EventArgs e)
        {
            groupBoxCaps.Visible = false;
            groupBoxCaption.Visible = false;
            groupBoxLayout.Visible = false;
            groupBoxStart.Visible = true;
            labelStatus.Text = "";
            buttonShowCaps.Enabled = false;
            buttonShowLayout.Enabled = false;
            buttonShowCaption.Enabled = false;
            buttonShowCaption.Text = "Show Caption";
            buttonShowCaps.Text = "Show Capabilities";
            buttonShowLayout.Text = "Show Layout";
            UpdateProps();
            
            try
            {
                // Begin scanning
                twainDevice.StartSession();
            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
                GetError();
            }
        }

        private void buttonCloseSession_Click(object sender, System.EventArgs e)
        {
            try
            {
                labelStatus.Text = "";
                twainDevice.CloseSession();
                buttonShowCaps.Enabled = false;
                buttonShowLayout.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                GetError();
            }
        }

        // Display the Capabilities group box
		private void buttonShowCaps_Click(object sender, System.EventArgs e)
		{
            if (groupBoxCaps.Visible == false)
			{
				buttonShowCaption.Text = "Show Caption";
				buttonShowLayout.Text = "Show Layout";
				buttonShowCaps.Text = "Hide Capabilities"; 
                groupBoxLayout.Visible = false;
                groupBoxStart.Visible = false;
                groupBoxCaption.Visible = false;
                groupBoxCaps.Visible = true;
				comboBoxCaps.SelectedIndex = 0;
			}
			else
			{
				buttonShowCaps.Text = "Show Capabilities";
                groupBoxCaps.Visible = false;
                groupBoxStart.Visible = true;
			}
		}

        // Display the Image Layout group box and the data source's image layout
		private void buttonShowLayout_Click(object sender, System.EventArgs e)
		{
            try
            {
                labelStatus.Text = "";
                if (groupBoxLayout.Visible == false)
                {
                    buttonShowCaption.Text = "Show Caption";
                    buttonShowCaps.Text = "Show Capabilities";
                    buttonShowLayout.Text = "Hide Layout";
                    groupBoxCaption.Visible = false;
                    groupBoxCaps.Visible = false;
                    groupBoxStart.Visible = false;
                    groupBoxLayout.Visible = true;

                    float left = -1, top = -1, right = -1, bottom = -1;

                    left = twainDevice.ImageLayout.Left;
                    top = twainDevice.ImageLayout.Top;
                    right = twainDevice.ImageLayout.Right;
                    bottom = twainDevice.ImageLayout.Bottom;

                    textBoxIL.Text = left.ToString();
                    textBoxIT.Text = top.ToString();
                    textBoxIR.Text = right.ToString();
                    textBoxIB.Text = bottom.ToString();
                }
                else
                {
                    buttonShowLayout.Text = "Show Layout";
                    groupBoxLayout.Visible = false;
                    groupBoxStart.Visible = true;
                }
            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
		}

        // Show the Caption group box
		private void buttonShowCaption_Click(object sender, System.EventArgs e)
		{
			if (groupBoxCaption.Visible == false)
			{
				buttonShowLayout.Text = "Show Layout";
				buttonShowCaps.Text = "Show Capabilities";
				buttonShowCaption.Text = "Hide Caption";
                groupBoxCaps.Visible = false;
                groupBoxStart.Visible = false;
                groupBoxLayout.Visible = false;
                groupBoxCaption.Visible = true;
			}
			else
			{
				buttonShowCaption.Text = "Show Caption";
                groupBoxCaption.Visible = false;
                groupBoxStart.Visible = true;
			}
		}

        private void buttonSelectSource_Click(object sender, System.EventArgs e)
        {
            try
            {
                labelStatus.Text = "";
                twainDevice.CloseSession();
                buttonShowCaps.Enabled = false;
                buttonShowLayout.Enabled = false;
                // Display the Select Source dialog
                twainDevice.SelectSource();
                buttonOpenSession.Enabled = true;
                buttonStartSession.Enabled = true;
            }
            catch (Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        private void UpdateProps()
        {
            try
            {
                // Copy values from fields to TwainPRO control
                twainDevice.ShowUserInterface = checkBoxShowUI.Checked;
                twainDevice.Caption.Text = textBoxCaption.Text;
                twainDevice.Caption.Clip = checkBoxClipCaption.Checked;
                twainDevice.Caption.ShadowText = checkBoxShadowText.Checked;

                twainDevice.Caption.Area = new System.Drawing.Rectangle(Int32.Parse(textBoxCapLeft.Text), Int32.Parse(textBoxCapTop.Text), Int32.Parse(textBoxCapWidth.Text), Int32.Parse(textBoxCapHeight.Text));

                twainDevice.Caption.HorizontalAlignment = (Accusoft.TwainProSdk.HorizontalAlignment)comboBoxHAlign.SelectedIndex;
                twainDevice.Caption.VerticalAlignment = (Accusoft.TwainProSdk.VerticalAlignment)comboBoxVAlign.SelectedIndex;
            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        private void DisplayUnits()
        {
            try
            {
                labelUnits.Visible = true;
                float unitsFound = 0;
                Capability capabilityUnits = Capability.IcapUnits;
                CapabilityContainer unitsCapContainer = twainDevice.GetCapability(capabilityUnits);

                if (unitsCapContainer.GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerEnum")
                {
                    if (twainDevice.IsCapabilitySupported(capabilityUnits) == true)
                    {
                        CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(capabilityUnits);

                        unitsFound = ((CapabilityContainerOneValueFloat)myCap.CurrentValue).Value;
                    }
                }
                if (unitsCapContainer.GetType().ToString() == "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat")
                {
                    if (twainDevice.IsCapabilitySupported(capabilityUnits) == true)
                    {
                        CapabilityContainerOneValueFloat myCap = (CapabilityContainerOneValueFloat)twainDevice.GetCapability(capabilityUnits);

                        unitsFound = myCap.Value;
                    }
                }

                switch ((int)unitsFound)
                {
                    case 0:
                        labelUnits.Text = "Units of Measure:  Inches";
                        break;
                    case 1:
                        labelUnits.Text = "Units of Measure:  Centimeters";
                        break;
                    case 2:
                        labelUnits.Text = "Units of Measure:  Picas";
                        break;
                    case 3:
                        labelUnits.Text = "Units of Measure:  Points";
                        break;
                    case 4:
                        labelUnits.Text = "Units of Measure:  Twips";
                        break;
                    case 5:
                        labelUnits.Text = "Units of Measure:  Pixels";
                        break;
                    case 6:
                        labelUnits.Text = "Units of Measure:  Millimeters";
                        break;
                }
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        private void comboBoxCaps_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                // Only process when session is open and groupBoxCaps can only be visible
                // after OpenSession is called
                if (groupBoxCaps.Visible == false)
                {
                    return;
                }

                buttonUpdate.Enabled = true;

                // Reset all the controls in groupBoxCaps
                labelDefault.Visible = false;
                labelMin.Visible = false;
                labelMax.Visible = false;
                listBoxCaps.Visible = false;
                listBoxCaps.Items.Clear();

                textBoxCurrent.Text = "";
                textBoxCurrent.Enabled = true;

                labelUnits.Visible = false;

                DetermineSelectedCapability();
                DisplayCapabilityInfo();

            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        // Determine the capability that was selected in the capability combo box.
        private void DetermineSelectedCapability()
        {
            selectedCapability = Capability.IcapAutoBright;
            switch (comboBoxCaps.SelectedIndex)
            {
                case 0:
                    selectedCapability = Capability.IcapAutoBright;
                    break;
                case 1:
                    selectedCapability = Capability.IcapBrightness;
                    break;
                case 2:
                    selectedCapability = Capability.IcapContrast;
                    break;
                case 3:
                    selectedCapability = Capability.IcapGamma;
                    break;
                case 4:
                    selectedCapability = Capability.IcapPhysicalWidth;
                    DisplayUnits();
                    break;
                case 5:
                    selectedCapability = Capability.IcapPhysicalHeight;
                    DisplayUnits();
                    break;
                case 6:
                    selectedCapability = Capability.IcapPixelType;
                    break;
                case 7:
                    selectedCapability = Capability.IcapRotation;
                    break;
                case 8:
                    selectedCapability = Capability.IcapUnits;
                    break;
                case 9:
                    selectedCapability = Capability.IcapXNativeResolution;
                    DisplayUnits();
                    break;
                case 10:
                    selectedCapability = Capability.IcapYNativeResolution;
                    DisplayUnits();
                    break;
                case 11:
                    selectedCapability = Capability.IcapXResolution;
                    DisplayUnits();
                    break;
                case 12:
                    selectedCapability = Capability.IcapYResolution;
                    DisplayUnits();
                    break;
                case 13:
                    selectedCapability = Capability.IcapXScaling;
                    break;
                case 14:
                    selectedCapability = Capability.IcapYScaling;
                    break;
                case 15:
                    selectedCapability = Capability.CapDeviceOnline;
                    break;
                case 16:
                    selectedCapability = Capability.CapIndicators;
                    break;
                case 17:
                    selectedCapability = Capability.IcapLampstate;
                    break;
                case 18:
                    selectedCapability = Capability.CapUIControllable;
                    break;
                case 19:
                    selectedCapability = Capability.CapXferCount;
                    break;
                case 20:
                    selectedCapability = Capability.CapAutoFeed;
                    break;
                case 21:
                    selectedCapability = Capability.CapAutoScan;
                    break;
                case 22:
                    selectedCapability = Capability.CapClearPage;
                    break;
                case 23:
                    selectedCapability = Capability.CapDuplex;
                    break;
                case 24:
                    selectedCapability = Capability.CapDuplexEnabled;
                    break;
                case 25:
                    selectedCapability = Capability.CapFeederEnabled;
                    break;
                case 26:
                    selectedCapability = Capability.CapFeederLoaded;
                    break;
                case 27:
                    selectedCapability = Capability.CapFeedPage;
                    break;
                case 28:
                    selectedCapability = Capability.CapPaperDetectable;
                    break;
            }
        }

        // Display the info for the selected capability.
        private void DisplayCapabilityInfo()
        {
            // Check for support and stop if none
            if (twainDevice.IsCapabilitySupported(selectedCapability) == false)
            {
                labelDefault.Text = "Not Supported";
                labelDefault.Visible = true;
                textBoxCurrent.Enabled = false;
                buttonUpdate.Enabled = false;
                return;
            }
            else
            {
                capContainer = twainDevice.GetCapability(selectedCapability);

                // What data type is the Cap?  We have to check the data type
                // to know which properties are valid
                try
                {
                    switch (capContainer.GetType().ToString())
                    {
                        // Type ONEVALUE only returns a single value
                        case "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat":
                            {
                                CapabilityContainerOneValueFloat myCap = (CapabilityContainerOneValueFloat)twainDevice.GetCapability(selectedCapability);

                                textBoxCurrent.Text = myCap.Value.ToString();
                                textBoxCurrent.Visible = true;
                                break;
                            }
                        case "Accusoft.TwainProSdk.CapabilityContainerOneValueString":
                            {
                                CapabilityContainerOneValueString myCap = (CapabilityContainerOneValueString)twainDevice.GetCapability(selectedCapability);

                                textBoxCurrent.Text = myCap.Value;
                                textBoxCurrent.Visible = true;
                                break;
                            }

                        // Type ENUM returns a list of legal values as well as current and
                        // default values.  A list of constants is returned and the CapDesc
                        // property can be used to find out what the constants mean
                        case "Accusoft.TwainProSdk.CapabilityContainerEnum":
                            {
                                CapabilityContainerEnum myCap = (CapabilityContainerEnum)twainDevice.GetCapability(selectedCapability);

                                textBoxCurrent.Text = ((CapabilityContainerOneValueFloat)myCap.CurrentValue).Value.ToString();
                                textBoxCurrent.Visible = true;

                                labelDefault.Text = "Default  =  " + ((CapabilityContainerOneValueFloat)myCap.DefaultValue).Value.ToString();
                                labelDefault.Visible = true;

                                listBoxCaps.Items.Add("Legal Values:");

                                for (int index = 0; index < myCap.Values.Count; index++)
                                {
                                    float floatResult = ((CapabilityContainerOneValueFloat)myCap.Values[index]).Value;

                                    string stringResult = twainDevice.GetCapabilityConstantDescription(selectedCapability, index);

                                    if (stringResult == null)
                                    {
                                        stringResult = "";
                                    }

                                    listBoxCaps.Items.Add(floatResult.ToString() + " - " + stringResult.ToString());
                                }
                                listBoxCaps.Visible = true;
                                break;
                            }

                        // Type ARRAY returns a list of values, but no current or default values
                        // This is a less common type that many sources don't use
                        case "Accusoft.TwainProSdk.CapabilityContainerArray":
                            {
                                CapabilityContainerArray myCap = (CapabilityContainerArray)twainDevice.GetCapability(selectedCapability);

                                listBoxCaps.Items.Add("Legal Values:");
                                for (int index = 0; index < myCap.Values.Count; index++)
                                {
                                    float result = ((CapabilityContainerOneValueFloat)myCap.Values[index]).Value;
                                    listBoxCaps.Items.Add(result);
                                }
                                break;
                            }

                        // Returns a range of values as well as current and default values
                        case "Accusoft.TwainProSdk.CapabilityContainerRange":
                            {
                                CapabilityContainerRange myCap = (CapabilityContainerRange)twainDevice.GetCapability(selectedCapability);

                                textBoxCurrent.Text = myCap.Value.ToString();
                                textBoxCurrent.Visible = true;

                                labelDefault.Text = "Default  =  " + myCap.Default.ToString();
                                labelDefault.Visible = true;

                                labelMin.Text = "MinValue  =  " + myCap.Minimum.ToString();
                                labelMin.Visible = true;

                                labelMax.Text = "MaxValue  =  " + myCap.Maximum.ToString();
                                labelMax.Visible = true;
                                break;
                            }
                    }

                    // The following capabilities can only be read, so disable the update button
                    if (selectedCapability == Capability.IcapPhysicalHeight | selectedCapability == Capability.IcapPhysicalWidth
| selectedCapability == Capability.IcapXNativeResolution | selectedCapability == Capability.IcapYNativeResolution
| selectedCapability == Capability.CapDeviceOnline | selectedCapability == Capability.CapUIControllable
| selectedCapability == Capability.CapAutoScan | selectedCapability == Capability.CapClearPage
| selectedCapability == Capability.CapDuplex | selectedCapability == Capability.CapFeederLoaded
| selectedCapability == Capability.CapPaperDetectable)
                    {
                        buttonUpdate.Enabled = false;
                    }

                    labelStatus.Text = "";
                }
                catch (System.InvalidCastException ex)
                {
                    labelStatus.Text = ex.Message;
                }
                catch (System.StackOverflowException ex)
                {
                    labelStatus.Text = ex.Message;
                }
            }
        }

		// Update the current Capability
		// This can only be done after calling OpenSession and before calling
        // StartSession (ie. in state 4 as described in the Twain spec)
		private void buttonUpdate_Click(object sender, System.EventArgs e)
		{
            try
            {
                labelStatus.Text = "";

                switch (capContainer.GetType().ToString())
                {
                    case "Accusoft.TwainProSdk.CapabilityContainerRange":
                    case "Accusoft.TwainProSdk.CapabilityContainerEnum":
                    case "Accusoft.TwainProSdk.CapabilityContainerOneValueFloat":
                        {
                            try
                            {
                                CapabilityContainerOneValueFloat updateCap = new CapabilityContainerOneValueFloat(selectedCapability);
                                updateCap.Value = Single.Parse(textBoxCurrent.Text);

                                twainDevice.SetCapability(updateCap);

                                // Force an update of the Caps frame with the new values
                                comboBoxCaps.SelectedIndex = comboBoxCaps.SelectedIndex;

                                labelStatus.Text = "Capability set!";
                            }
                            catch (FormatException ex)
                            {
                                labelStatus.Text = ex.Message;
                            }
                            break;
                        }
                }
            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (FormatException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.AccessViolationException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.StackOverflowException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
		}
		
		// Update the Image Layout
		// This can only be done after calling OpenSession and before calling
        // StartSession (ie. in state 4 as described in the Twain spec)
        private void buttonLayoutUpdate_Click(object sender, System.EventArgs e)
        {
            try
            {
                labelStatus.Text = "";
                float left = (float)System.Double.Parse(textBoxIL.Text);
                float top = (float)System.Double.Parse(textBoxIT.Text);
                float width = (float)System.Double.Parse(textBoxIR.Text);
                float height = (float)System.Double.Parse(textBoxIB.Text);

                twainDevice.ImageLayout = new System.Drawing.RectangleF(left, top, width, height);
            }
            catch (FormatException)
            {
                MessageBox.Show("You must enter a real number for each field.");
                GetError();
            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

        private void twainDevice_Scanned(object sender, Accusoft.TwainProSdk.ScannedEventArgs e)
        {
            try
            {
                // Display the scanned image
                FormViewer viewer = new FormViewer();
                viewer.AddImage(sender, e);

                // Save the scanned image if the save image check box is checked
                if (checkBoxSave.Checked)
                {
                    string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
                    string strPath = System.IO.Path.Combine(strCurrentDir, "image-" + imageCount.ToString() + ".tif");

                    Accusoft.ImagXpressSdk.SaveOptions saveOptions = new Accusoft.ImagXpressSdk.SaveOptions();
                    saveOptions.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff;
                    saveOptions.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4;

                    viewer.GetImage().Save(strPath, saveOptions);

                    imageCount++;

                    checkBoxSave.Text = "Save Image as  Image-" + imageCount.ToString() + ".tif";
                }

                Application.DoEvents();
            }
            catch (TwainException ex)
            {
                labelStatus.Text = ex.Message + " Code: " + ex.ConditionCode.ToString() + ", Description: " + ex.ConditionDescription;
            }
            catch (TwainProException ex)
            {
                labelStatus.Text = ex.Message;
            }
            catch (System.Exception ex)
            {
                labelStatus.Text = ex.Message;
            }
        }

		//  Update the status bar with the current error code and description
		private void GetError() 
		{
			if ((twainPro1.ErrorLevel != 0)) 
			{
				labelStatus.Text = ("Data Source Error " 
					+ twainPro1.ErrorLevel);
			}
		}

		private void menuItemQuit_Click(object sender, System.EventArgs e)
		{
			 Application.Exit();
		}

        private void menuItemTwainPro_Click(object sender, EventArgs e)
        {
            twainPro1.AboutBox();
        }

        private void menuItemImagXpress_Click(object sender, EventArgs e)
        {
            FormViewer frm = new FormViewer();
            frm.GetImagXpress().AboutBox();
        }		
	}
}


TwainPro4-Changes.txt
January 14, 2008



Version 4.0.22.1 changed January 14, 2008

(#5920) - .NET only Fixed a DEP issue that occurs in limited situations.

=====================================================================

Version 4.0.22.0 Apr. 18, 2007

(#53) - Fixed issue where the number of handles increased by 1 each time a TwainPRO object was created
(#49) - Fixed issue with C++ samples in Visual Studio 2005
(#43) - Added TW_STR32 type support
(#37) - Fixed issue with closing a Visual Studio 2005 application after adding the TwainPRO control to the form

=====================================================================
Version 4.0.19.3 Sep. 19, 2006

1.  - Upgrade for .NET only, Fixed a LogFont issue


=====================================================================
Version 4.0.19.2 May 11, 2006

1.  - Upgrade for .NET only, Fixed a .NET 2.0, VS 2005 Font issue

=====================================================================
Version 4.0.18.2 changes Sep. 21, 2005

1.  - Upgrade for .NET only, Fixed another DEP problem 

=====================================================================
Version 4.0.19.0 changes - Mar. 17, 2006

1. Fixed the DEP problem with XP SP2 running on newer 64-bit CPUs in 32-bit mode, for MS RTM

=====================================================================
Version 4.0.18.0 changes - Aug. 19, 2004

1. Fixed the DEP problem with XP SP2 running on newer 64-bit CPUs in 32-bit mode, for MS RTM

=====================================================================
Version 4.0.15.0 changes - Jul. 23, 2004

1. Fixed the issue with sending a close session while an ADF has more documents
2. Added support for the ImagXpress Photo Edition


=====================================================================
Version 4.0.14.0 changes - Jul. 9, 2004

1. Fixed the DEP problem with XP SP2 running on newer 64-bit CPUs in 32-bit mode


=====================================================================
Version 4.0.12.1 changes - Jun. 11, 2004

1. .NET updated licensing unlocks to support Delphi customers.  
   Developers using Borland tools must use PegasusImaging.WinForms.TwainPro4.Licensing instead of UnlockPICTwainPro.PS_Unlock.  


=====================================================================
Version 4.0.12.0 changes - Apr. 19, 2004

1. Memory leak corrected using Picture property


=====================================================================
Version 4.0.10.1 changes - Feb. 26, 2004

1. .Net only, Issue with ImageLayout

=====================================================================
Version 4.0.10.0 changes - Feb. 9, 2004


1. not accepting parent licenses (i.e. ImagXpress)






Known issues
------------

1. When TransferMode is set to Memory, DPI is not correctly maintained if units are set to anything other than Inches
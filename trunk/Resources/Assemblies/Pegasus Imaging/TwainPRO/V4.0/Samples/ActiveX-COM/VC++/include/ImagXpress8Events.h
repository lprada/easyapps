//-------------------------------------
// Copyright Pegasus Imaging Corp., 2006
//   Header Created on 04/10/06
//     For ImagXpress from Version 8.0.35.0
//         
//         
// Modification History
// ---------------------
//                      
//-------------------------------------

#ifndef IMAGXPRESS8EVENTS_H
#define IMAGXPRESS8EVENTS_H

#ifndef __BORLANDC__
using namespace PegasusImagingActiveXImagXpress8;
#endif


class CImagXpressEventHandler : public IDispatch
{
public:
  long m_dwRef;

  CImagXpressEventHandler()
  {
     m_dwRef = 0;
  }

  ~CImagXpressEventHandler()
  {
  }

   long __stdcall GetTypeInfoCount(unsigned int *)
   {
     return 0;
   }

   long __stdcall GetTypeInfo(unsigned int,unsigned long,struct ITypeInfo ** )
   {
     return 0;
   }

   long __stdcall GetIDsOfNames(const struct _GUID &, BSTR * ,unsigned int,unsigned long,long *)
   {
     return 0;
   }

   long __stdcall GetIID()
   {
     return 0;
   }

   HRESULT __stdcall Invoke (DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult, EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr)
   {
   // this is where all the events come -- just call directly into your methods exactly
   //   as the import wrapper will show you
   switch (dispidMember)
     {
     case -600:
        Click(   );
     break;

     case -601:
        DblClick(   );
     break;

     case -605:
        MouseDown(pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

     case -606:
        MouseMove(pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

     case -607:
        MouseUp(pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

     case -604:
        KeyUp(pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal   );
     break;

     case -602:
        KeyDown(pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal   );
     break;

     case -603:
        KeyPress(pdispparams->rgvarg[0].iVal   );
     break;

     case 1:
        Notify(   );
     break;

     case 2:
        Progress(pdispparams->rgvarg[7].lVal, pdispparams->rgvarg[6].lVal, pdispparams->rgvarg[5].lVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].lVal, pdispparams->rgvarg[2].boolVal, pdispparams->rgvarg[1].boolVal, pdispparams->rgvarg[0].lVal   );
     break;

     case 4:
        OLEDragOver(*pdispparams->rgvarg[6].pvarVal, pdispparams->rgvarg[5].iVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].lVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].lVal   );
     break;

     case 5:
        OLEDragDrop(*pdispparams->rgvarg[6].pvarVal, pdispparams->rgvarg[5].iVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].lVal, pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal   );
     break;

     case 6:
        Scroll(pdispparams->rgvarg[1].iVal, pdispparams->rgvarg[0].iVal   );
     break;

     case 7:
        TimerTick(   );
     break;

     case 8:
        CB(pdispparams->rgvarg[1].iVal, (enumIXEVENT_CB_ID)pdispparams->rgvarg[0].lVal   );
     break;

     case 9:
        ImageStatusChanged(pdispparams->rgvarg[2].iVal, (enumIPEffect)pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

     case 10:
        MenuSelect((PegasusImagingActiveXImagXpress8::enumMenu)pdispparams->rgvarg[5].lVal, (enumIXTool)pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].lVal, pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

     case 11:
        ToolbarSelect((enumIXTool)pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

     case 12:
        ToolUse((enumIXTool)pdispparams->rgvarg[3].lVal, (enumIXToolAction)pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
     break;

  }
   return S_OK;
   }

   HRESULT __stdcall QueryInterface(REFIID a_riid, void** a_ppv)
   {
      if (a_ppv == NULL)
         return E_POINTER;
      *a_ppv = NULL;
#ifdef __BORLANDC__
      if (a_riid == DIID__IImagXpressEvents)
#else
      if (a_riid == __uuidof(_IImagXpressEvents))
#endif
         *a_ppv = static_cast<CImagXpressEventHandler*>(this);
      else if (a_riid == IID_IUnknown)
         *a_ppv = static_cast<IUnknown *>(this);
      else
         return E_NOINTERFACE;
      AddRef();
      return S_OK;
   }

   ULONG __stdcall AddRef()
   {
      return InterlockedIncrement(&m_dwRef);
   }

   ULONG __stdcall Release()
   {
      if (InterlockedDecrement(&m_dwRef) == 0)
         return 0;
      return m_dwRef;
   }

   virtual HRESULT Click(   )
   {
     return S_OK;
   }

   virtual HRESULT DblClick(   )
   {
     return S_OK;
   }

   virtual HRESULT MouseDown(short Button, short Shift, long x, long y   )
   {
     return S_OK;
   }

   virtual HRESULT MouseMove(short Button, short Shift, long x, long y   )
   {
     return S_OK;
   }

   virtual HRESULT MouseUp(short Button, short Shift, long x, long y   )
   {
     return S_OK;
   }

   virtual HRESULT KeyUp(short KeyCode, short Shift   )
   {
     return S_OK;
   }

   virtual HRESULT KeyDown(short KeyCode, short Shift   )
   {
     return S_OK;
   }

   virtual HRESULT KeyPress(short KeyASCII   )
   {
     return S_OK;
   }

   virtual HRESULT Notify(   )
   {
     return S_OK;
   }

   virtual HRESULT Progress(long ImageID, long OperationID, long BytesProcessed, long TotalBytes, long PctDone, VARIANT_BOOL bDone, VARIANT_BOOL bAsync, long Error   )
   {
     return S_OK;
   }

   virtual HRESULT OLEDragOver(VARIANT Data, short Format, long Effect, long Shift, short x, short y, long State   )
   {
     return S_OK;
   }

   virtual HRESULT OLEDragDrop(VARIANT Data, short Format, long Effect, long Button, long Shift, short x, short y   )
   {
     return S_OK;
   }

   virtual HRESULT Scroll(short Bar, short Action   )
   {
     return S_OK;
   }

   virtual HRESULT TimerTick(   )
   {
     return S_OK;
   }

   virtual HRESULT CB(short ID, enumIXEVENT_CB_ID CBID   )
   {
     return S_OK;
   }

   virtual HRESULT ImageStatusChanged(short ID, enumIPEffect eOPID, long lStatus   )
   {
     return S_OK;
   }

   virtual HRESULT MenuSelect(PegasusImagingActiveXImagXpress8::enumMenu Menu, enumIXTool Tool, long TopMenuID, long SubMenuID, long User1, long User2   )
   {
     return S_OK;
   }

   virtual HRESULT ToolbarSelect(enumIXTool Tool, long User1, long User2   )
   {
     return S_OK;
   }

   virtual HRESULT ToolUse(enumIXTool Tool, enumIXToolAction Action, long x, long y   )
   {
     return S_OK;
   }

};

class CImagXpress : CImagXpressEventHandler
{
  private:
    DWORD m_dwCookie;

    // Event Handlers
   HRESULT (*pClick) (DWORD classPtr, DWORD objID);
   HRESULT (*pDblClick) (DWORD classPtr, DWORD objID);
   HRESULT (*pMouseDown) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y);
   HRESULT (*pMouseMove) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y);
   HRESULT (*pMouseUp) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y);
   HRESULT (*pKeyUp) (DWORD classPtr, DWORD objID, short KeyCode, short Shift);
   HRESULT (*pKeyDown) (DWORD classPtr, DWORD objID, short KeyCode, short Shift);
   HRESULT (*pKeyPress) (DWORD classPtr, DWORD objID, short KeyASCII);
   HRESULT (*pNotify) (DWORD classPtr, DWORD objID);
   HRESULT (*pProgress) (DWORD classPtr, DWORD objID, long ImageID, long OperationID, long BytesProcessed, long TotalBytes, long PctDone, VARIANT_BOOL bDone, VARIANT_BOOL bAsync, long Error);
   HRESULT (*pOLEDragOver) (DWORD classPtr, DWORD objID, VARIANT Data, short Format, long Effect, long Shift, short x, short y, long State);
   HRESULT (*pOLEDragDrop) (DWORD classPtr, DWORD objID, VARIANT Data, short Format, long Effect, long Button, long Shift, short x, short y);
   HRESULT (*pScroll) (DWORD classPtr, DWORD objID, short Bar, short Action);
   HRESULT (*pTimerTick) (DWORD classPtr, DWORD objID);
   HRESULT (*pCB) (DWORD classPtr, DWORD objID, short ID, enumIXEVENT_CB_ID CBID);
   HRESULT (*pImageStatusChanged) (DWORD classPtr, DWORD objID, short ID, enumIPEffect eOPID, long lStatus);
   HRESULT (*pMenuSelect) (DWORD classPtr, DWORD objID, PegasusImagingActiveXImagXpress8::enumMenu Menu, enumIXTool Tool, long TopMenuID, long SubMenuID, long User1, long User2);
   HRESULT (*pToolbarSelect) (DWORD classPtr, DWORD objID, enumIXTool Tool, long User1, long User2);
   HRESULT (*pToolUse) (DWORD classPtr, DWORD objID, enumIXTool Tool, enumIXToolAction Action, long x, long y);

public:
CImagXpress()
{
  CImagXpress(0,0,0,0,0,0,0);
}

CImagXpress(DWORD classPtr, DWORD objID, long hWndParent, long left, long top, long width, long height)
{
   // Call the event connection function in the ImagXpress ATL COM Control
   typedef bool (__cdecl* LPFN_IX_CONNECTEVENTS)(long, DWORD *, IUnknown *, long *);
   LPFN_IX_CONNECTEVENTS  lpfn_IX_ConnectEvents;

   HINSTANCE hDLL = LoadLibrary("PegasusImaging.ActiveX.ImagXpress8.dll");   // ImagXpress ATL COM control
   if (hDLL != NULL)
      {
      lpfn_IX_ConnectEvents = (LPFN_IX_CONNECTEVENTS)GetProcAddress(hDLL, "IX_ConnectEvents");
      if (lpfn_IX_ConnectEvents)
         {
         lpfn_IX_ConnectEvents((long)(&pImagXpress), &m_dwCookie, this, &m_dwRef);
         }
      FreeLibrary(hDLL);
      }

   // Initialize our instance pointer and object id.
   m_classPtr = classPtr;
   m_objID    = objID;

   // NULL out the Event procedures

   pClick = NULL;
   pDblClick = NULL;
   pMouseDown = NULL;
   pMouseMove = NULL;
   pMouseUp = NULL;
   pKeyUp = NULL;
   pKeyDown = NULL;
   pKeyPress = NULL;
   pNotify = NULL;
   pProgress = NULL;
   pOLEDragOver = NULL;
   pOLEDragDrop = NULL;
   pScroll = NULL;
   pTimerTick = NULL;
   pCB = NULL;
   pImageStatusChanged = NULL;
   pMenuSelect = NULL;
   pToolbarSelect = NULL;
   pToolUse = NULL;

// Create the control's Window
if (hWndParent)
   pImagXpress->CreateCtlWindow(hWndParent, left, top, width, height);
}

~CImagXpress()
{
// Call the event disconnection function in the ImagXpress ATL COM Control
typedef bool (__cdecl* LPFN_IX_DISCONNECTEVENTS)(long, DWORD);
LPFN_IX_DISCONNECTEVENTS  lpfn_IX_DisConnectEvents;

HINSTANCE hDLL = LoadLibrary("PegasusImaging.ActiveX.ImagXpress8.dll");   // ImagXpress ATL COM control
if (hDLL != NULL)
   {
   lpfn_IX_DisConnectEvents = (LPFN_IX_DISCONNECTEVENTS)GetProcAddress(hDLL, "IX_DisConnectEvents");
   if (lpfn_IX_DisConnectEvents)
      {
      lpfn_IX_DisConnectEvents((long)(static_cast<IImagXpress *>(pImagXpress)), m_dwCookie);
      }
   FreeLibrary(hDLL);
   }
}

   void SetClickEvent(HRESULT (*pMyClick) (DWORD classPtr, DWORD objID))
   {
     pClick = pMyClick;
   }

   HRESULT Click()
   {
      if (pClick)
         (*pClick)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetDblClickEvent(HRESULT (*pMyDblClick) (DWORD classPtr, DWORD objID))
   {
     pDblClick = pMyDblClick;
   }

   HRESULT DblClick()
   {
      if (pDblClick)
         (*pDblClick)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetMouseDownEvent(HRESULT (*pMyMouseDown) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y))
   {
     pMouseDown = pMyMouseDown;
   }

   HRESULT MouseDown(short Button, short Shift, long x, long y)
   {
      if (pMouseDown)
         (*pMouseDown)(m_classPtr, m_objID, Button, Shift, x, y);
      return S_OK;
   }

   void SetMouseMoveEvent(HRESULT (*pMyMouseMove) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y))
   {
     pMouseMove = pMyMouseMove;
   }

   HRESULT MouseMove(short Button, short Shift, long x, long y)
   {
      if (pMouseMove)
         (*pMouseMove)(m_classPtr, m_objID, Button, Shift, x, y);
      return S_OK;
   }

   void SetMouseUpEvent(HRESULT (*pMyMouseUp) (DWORD classPtr, DWORD objID, short Button, short Shift, long x, long y))
   {
     pMouseUp = pMyMouseUp;
   }

   HRESULT MouseUp(short Button, short Shift, long x, long y)
   {
      if (pMouseUp)
         (*pMouseUp)(m_classPtr, m_objID, Button, Shift, x, y);
      return S_OK;
   }

   void SetKeyUpEvent(HRESULT (*pMyKeyUp) (DWORD classPtr, DWORD objID, short KeyCode, short Shift))
   {
     pKeyUp = pMyKeyUp;
   }

   HRESULT KeyUp(short KeyCode, short Shift)
   {
      if (pKeyUp)
         (*pKeyUp)(m_classPtr, m_objID, KeyCode, Shift);
      return S_OK;
   }

   void SetKeyDownEvent(HRESULT (*pMyKeyDown) (DWORD classPtr, DWORD objID, short KeyCode, short Shift))
   {
     pKeyDown = pMyKeyDown;
   }

   HRESULT KeyDown(short KeyCode, short Shift)
   {
      if (pKeyDown)
         (*pKeyDown)(m_classPtr, m_objID, KeyCode, Shift);
      return S_OK;
   }

   void SetKeyPressEvent(HRESULT (*pMyKeyPress) (DWORD classPtr, DWORD objID, short KeyASCII))
   {
     pKeyPress = pMyKeyPress;
   }

   HRESULT KeyPress(short KeyASCII)
   {
      if (pKeyPress)
         (*pKeyPress)(m_classPtr, m_objID, KeyASCII);
      return S_OK;
   }

   void SetNotifyEvent(HRESULT (*pMyNotify) (DWORD classPtr, DWORD objID))
   {
     pNotify = pMyNotify;
   }

   HRESULT Notify()
   {
      if (pNotify)
         (*pNotify)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetProgressEvent(HRESULT (*pMyProgress) (DWORD classPtr, DWORD objID, long ImageID, long OperationID, long BytesProcessed, long TotalBytes, long PctDone, VARIANT_BOOL bDone, VARIANT_BOOL bAsync, long Error))
   {
     pProgress = pMyProgress;
   }

   HRESULT Progress(long ImageID, long OperationID, long BytesProcessed, long TotalBytes, long PctDone, VARIANT_BOOL bDone, VARIANT_BOOL bAsync, long Error)
   {
      if (pProgress)
         (*pProgress)(m_classPtr, m_objID, ImageID, OperationID, BytesProcessed, TotalBytes, PctDone, bDone, bAsync, Error);
      return S_OK;
   }

   void SetOLEDragOverEvent(HRESULT (*pMyOLEDragOver) (DWORD classPtr, DWORD objID, VARIANT Data, short Format, long Effect, long Shift, short x, short y, long State))
   {
     pOLEDragOver = pMyOLEDragOver;
   }

   HRESULT OLEDragOver(VARIANT Data, short Format, long Effect, long Shift, short x, short y, long State)
   {
      if (pOLEDragOver)
         (*pOLEDragOver)(m_classPtr, m_objID, Data, Format, Effect, Shift, x, y, State);
      return S_OK;
   }

   void SetOLEDragDropEvent(HRESULT (*pMyOLEDragDrop) (DWORD classPtr, DWORD objID, VARIANT Data, short Format, long Effect, long Button, long Shift, short x, short y))
   {
     pOLEDragDrop = pMyOLEDragDrop;
   }

   HRESULT OLEDragDrop(VARIANT Data, short Format, long Effect, long Button, long Shift, short x, short y)
   {
      if (pOLEDragDrop)
         (*pOLEDragDrop)(m_classPtr, m_objID, Data, Format, Effect, Button, Shift, x, y);
      return S_OK;
   }

   void SetScrollEvent(HRESULT (*pMyScroll) (DWORD classPtr, DWORD objID, short Bar, short Action))
   {
     pScroll = pMyScroll;
   }

   HRESULT Scroll(short Bar, short Action)
   {
      if (pScroll)
         (*pScroll)(m_classPtr, m_objID, Bar, Action);
      return S_OK;
   }

   void SetTimerTickEvent(HRESULT (*pMyTimerTick) (DWORD classPtr, DWORD objID))
   {
     pTimerTick = pMyTimerTick;
   }

   HRESULT TimerTick()
   {
      if (pTimerTick)
         (*pTimerTick)(m_classPtr, m_objID);
      return S_OK;
   }

   void SetCBEvent(HRESULT (*pMyCB) (DWORD classPtr, DWORD objID, short ID, enumIXEVENT_CB_ID CBID))
   {
     pCB = pMyCB;
   }

   HRESULT CB(short ID, enumIXEVENT_CB_ID CBID)
   {
      if (pCB)
         (*pCB)(m_classPtr, m_objID, ID, CBID);
      return S_OK;
   }

   void SetImageStatusChangedEvent(HRESULT (*pMyImageStatusChanged) (DWORD classPtr, DWORD objID, short ID, enumIPEffect eOPID, long lStatus))
   {
     pImageStatusChanged = pMyImageStatusChanged;
   }

   HRESULT ImageStatusChanged(short ID, enumIPEffect eOPID, long lStatus)
   {
      if (pImageStatusChanged)
         (*pImageStatusChanged)(m_classPtr, m_objID, ID, eOPID, lStatus);
      return S_OK;
   }

   void SetMenuSelectEvent(HRESULT (*pMyMenuSelect) (DWORD classPtr, DWORD objID, PegasusImagingActiveXImagXpress8::enumMenu Menu, enumIXTool Tool, long TopMenuID, long SubMenuID, long User1, long User2))
   {
     pMenuSelect = pMyMenuSelect;
   }

   HRESULT MenuSelect(PegasusImagingActiveXImagXpress8::enumMenu Menu, enumIXTool Tool, long TopMenuID, long SubMenuID, long User1, long User2)
   {
      if (pMenuSelect)
         (*pMenuSelect)(m_classPtr, m_objID, Menu, Tool, TopMenuID, SubMenuID, User1, User2);
      return S_OK;
   }

   void SetToolbarSelectEvent(HRESULT (*pMyToolbarSelect) (DWORD classPtr, DWORD objID, enumIXTool Tool, long User1, long User2))
   {
     pToolbarSelect = pMyToolbarSelect;
   }

   HRESULT ToolbarSelect(enumIXTool Tool, long User1, long User2)
   {
      if (pToolbarSelect)
         (*pToolbarSelect)(m_classPtr, m_objID, Tool, User1, User2);
      return S_OK;
   }

   void SetToolUseEvent(HRESULT (*pMyToolUse) (DWORD classPtr, DWORD objID, enumIXTool Tool, enumIXToolAction Action, long x, long y))
   {
     pToolUse = pMyToolUse;
   }

   HRESULT ToolUse(enumIXTool Tool, enumIXToolAction Action, long x, long y)
   {
      if (pToolUse)
         (*pToolUse)(m_classPtr, m_objID, Tool, Action, x, y);
      return S_OK;
   }

  DWORD m_objID;
  DWORD m_classPtr;
  IImagXpressPtr pImagXpress;

};

#endif // end ifndef IMAGXPRESS8EVENTS_H
VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Viewer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Viewer"
   ClientHeight    =   7515
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6660
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7515
   ScaleWidth      =   6660
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Imag 
      Height          =   6615
      Left            =   360
      TabIndex        =   8
      Top             =   600
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   11668
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   272238623
      ErrInfo         =   -2075618488
      Persistence     =   -1  'True
      _cx             =   10398
      _cy             =   11668
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton CmdMirror 
      Caption         =   "Mirror"
      Height          =   255
      Left            =   5910
      TabIndex        =   7
      Top             =   0
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Flip"
      Height          =   255
      Left            =   5175
      TabIndex        =   6
      Top             =   0
      Width           =   735
   End
   Begin VB.CommandButton CmdInvert 
      Caption         =   "Invert"
      Height          =   255
      Left            =   4365
      TabIndex        =   5
      Top             =   0
      Width           =   810
   End
   Begin VB.CommandButton CmdDeskew 
      Caption         =   "Deskew"
      Height          =   255
      Left            =   3390
      TabIndex        =   4
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton CmdDespeckle 
      Caption         =   "Despeckle"
      Height          =   255
      Left            =   2415
      TabIndex        =   3
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton CmdZoomOut 
      Caption         =   "Zoom Out"
      Height          =   255
      Left            =   1440
      TabIndex        =   2
      Top             =   0
      Width           =   975
   End
   Begin VB.CommandButton CmdZoomFit 
      Caption         =   "Fit"
      Height          =   255
      Left            =   960
      TabIndex        =   1
      Top             =   0
      Width           =   495
   End
   Begin VB.CommandButton CmdZoomIn 
      Caption         =   "Zoom In"
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   975
   End
End
Attribute VB_Name = "Viewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Public Sub ShowEx(hImage As Long, bDIB As Boolean)

If (bDIB) Then
    Imag.hDIB = hImage
Else
    Imag.LoadBuffer (hImage)
End If

If (Imag.Width > 0) Then
   If ((Imag.Width / Screen.TwipsPerPixelX) / Imag.IWidth) > 0.1 Then
      Imag.ZoomFactor = (Imag.Width / Screen.TwipsPerPixelX) / Imag.IWidth
   Else
      Imag.ZoomFactor = 0.1
   End If
End If

If (Me.Visible = False) Then
   Me.Show
Else
   InvalidateRect Me.hwnd, 0, 0
End If
   
Viewer.Top = MainForm.Top
Viewer.Left = MainForm.Left + MainForm.Width
If (Viewer.Left + Viewer.Width > Screen.Width) Then
   Viewer.Left = Screen.Width - Viewer.Width
End If

End Sub


Private Sub CmdDeskew_Click()
Imag.Deskew DESKEW_Normal
End Sub

Private Sub CmdDespeckle_Click()
Imag.Despeckle
End Sub

Private Sub CmdInvert_Click()
Imag.Negate
End Sub

Private Sub CmdMirror_Click()
Imag.Mirror
End Sub

Private Sub CmdZoomFit_Click()
If ((Imag.Width / Screen.TwipsPerPixelX) / Imag.IWidth) > 0.1 Then
   Imag.ZoomFactor = (Imag.Width / Screen.TwipsPerPixelX) / Imag.IWidth
Else
   Imag.ZoomFactor = 0.1
End If
End Sub

Private Sub CmdZoomIn_Click()
If (Imag.ZoomFactor * 2) < 10 Then
   Imag.ZoomFactor = Imag.ZoomFactor * 2
End If
End Sub

Private Sub CmdZoomOut_Click()
If (Imag.ZoomFactor / 2) > 0.1 Then
   Imag.ZoomFactor = Imag.ZoomFactor / 2
End If
End Sub

Private Sub Command1_Click()
Imag.Flip
End Sub



unit FrmStage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, ComObj, ComCtrls, StdCtrls, FrmView,  TwnPRO4v, TWNPRO4Lib_TLB,
  Menus;

type
  TStage = class(TForm)
    FrmLayout: TGroupBox;
    FrmCaps: TGroupBox;
    FrmCaption: TGroupBox;
    CmdSelSource: TButton;
    ChkShowUI: TCheckBox;
    ChkSave: TCheckBox;
    LblStatus: TStatusBar;
    GroupBox1: TGroupBox;
    CmdOpen: TButton;
    CmdStart: TButton;
    CmdClose: TButton;
    CmdCaps: TButton;
    CmdLayout: TButton;
    CmdCaption: TButton;
    ChkClip: TCheckBox;
    ChkShadow: TCheckBox;
    EdtCapLeft: TEdit;
    EdtCapTop: TEdit;
    EdtCapWidth: TEdit;
    EdtCapHeight: TEdit;
    EdtCaption: TEdit;
    CmbVAlign: TComboBox;
    CmbHAlign: TComboBox;
    Label1: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CmbCaps: TComboBox;
    LblDefault: TLabel;
    List1: TListBox;
    EdtCurrent: TEdit;
    Label2: TLabel;
    CmdUC: TButton;
    EdtIL: TEdit;
    EdtIT: TEdit;
    EdtIR: TEdit;
    EdtIB: TEdit;
    CmdUL: TButton;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    LblMin: TLabel;
    LblMax: TLabel;
    TwainPRO: TTwainPRO;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Quit1: TMenuItem;
    About1: TMenuItem;
    Label13: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CmdOpenClick(Sender: TObject);
    procedure CmdSelSourceClick(Sender: TObject);
    procedure CmdStartClick(Sender: TObject);
    procedure CmdCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCapsClick(Sender: TObject);
    procedure CmdLayoutClick(Sender: TObject);
    procedure CmdULClick(Sender: TObject);
    procedure CmbCapsChange(Sender: TObject);
    procedure CmdUCClick(Sender: TObject);
    procedure CmdCaptionClick(Sender: TObject);
    procedure HandleException( var E: Exception );
    procedure UpdateProps;
    procedure TwainPROPostScan(Sender: TObject; var Cancel: WordBool);
    procedure Quit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);





  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Stage: TStage;

implementation

{$R *.DFM}

procedure TStage.FormCreate(Sender: TObject);
begin
     CmbHAlign.ItemIndex:=0;
     CmbVAlign.ItemIndex:=0;

     // This property must be set when using the TwainPRO VCL
     // or the Twain device will NOT function correctly!!!!!!
     TwainPRO.hParentWnd := Stage.Handle;
     
end;

procedure TStage.HandleException( var E: Exception );
var
   RetPos, TwnPROErr: LongInt;
begin
     if ( E is EOleException ) then
     begin
          with E as EOleException do
          begin
               if ( Pos( 'TwainPRO', Source ) <> 0 ) then // the COM Object was TwainPRO
               begin
                    TwnPROErr := ( ErrorCode and $0000ffff ); // TwainPRO Error number is in the low word
                    if TwnPROErr = 1000 then
                       LblStatus.Panels[0].Text:= 'Data Source Error ' + IntToStr( TwainPRO.ErrorCode ) + ': ' + TwainPRO.ErrorString
                    else begin  // Remove carriage return from Description.
                        RetPos:=Pos( Chr(10), Message );
                        if RetPos > 0 then
                           LblStatus.Panels[0].Text := IntToStr(TwnPROErr) + ': ' + Copy(Message, 1, RetPos - 1) + '  ' + Copy( Message, RetPos + 1, Length(Message) - RetPos - 1)
                        else
                           LblStatus.Panels[0].Text := IntToStr(TwnPROErr) + ': ' + Message;
                    end
               end
               else // some other ActiveX control or COM object
                   LblStatus.Panels[0].Text :=  'The ActiveX control/COM Object ' + Source + ' caused an error!';
          end
     end
     else
         LblStatus.Panels[0].Text := 'Non-COM error.';
end;

procedure TStage.UpdateProps;
var
   N: Integer;
   Code: Integer;
begin
     // Copy values from fields to TwainPRO control
     TwainPRO.ShowUI:=ChkShowUI.Checked;
     TwainPRO.Caption:=EdtCaption.Text;
     TwainPRO.ClipCaption:=ChkClip.Checked;
     TwainPRO.ShadowText:=ChkShadow.Checked;
     Val(EdtCapLeft.Text, N, Code);
     TwainPRO.CaptionLeft:=N;
     Val(EdtCapTop.Text, N, Code);
     TwainPRO.CaptionTop:=N;
     Val(EdtCapWidth.Text, N, Code);
     TwainPRO.CaptionWidth:=N;
     Val(EdtCapHeight.Text, N, Code);
     TwainPRO.CaptionHeight:=N;
     TwainPRO.HAlign:=CmbHAlign.ItemIndex;
     TwainPRO.VAlign:=CmbVAlign.ItemIndex;
end;

procedure TStage.CmdOpenClick(Sender: TObject);
begin
     // Open a session (move to state 4 in the Twain spec).
     // This opens the data source and prepares it for capability
     // negotiation.  At this point, capabilities and layout can be
     // set for use in the upcoming image acquisition.
     Screen.Cursor:=-11;
     try
        LblStatus.Panels[0].Text:='';
        TwainPRO.OpenSession;
        CmdCaps.Enabled:=True;
        CmdLayout.Enabled:=True;
     except
        on E: Exception do begin
           HandleException( E );
           Screen.Cursor:=0;
        end
     end;
     Screen.Cursor:=0;
end;

procedure TStage.CmdSelSourceClick(Sender: TObject);
begin
     // Request the Twain Data Source Manager display a list of
     // available Twain Sources.
     try
        LblStatus.Panels[0].Text:='';
        TwainPRO.CloseSession;
        CmdCaps.Enabled:=False;
        CmdLayout.Enabled:=False;
        TwainPRO.SelectSource;
     except
        on E: Exception do HandleException( E );
     end;
end;

procedure TStage.CmdStartClick(Sender: TObject);
begin
     // Start the session (move to state 5 in the Twain spec).
     // This will bring up the UI if ShowUI is enabled or start
     // a scan if ShowUI is disabled.
     try
        LblStatus.Panels[0].Text:='';
        CmdCaps.Enabled:=False;
        CmdLayout.Enabled:=False;
        FrmCaps.Visible:=False;
        FrmLayout.Visible:=False;
        UpdateProps;
        m_ImageCount:=0;
        TwainPRO.StartSession;
     except
        on E: Exception do HandleException( E );
     end;
end;

procedure TStage.CmdCloseClick(Sender: TObject);
begin
     // Close a session.
     // This is necessary only after calling OpenDSM or OpenSession methods.
     // StartSession automatically calls CloseSession before returning.
     try
        LblStatus.Panels[0].Text:='';
        TwainPRO.CloseSession;
        CmdCaps.Enabled:=False;
        CmdLayout.Enabled:=False;
        FrmCaps.Visible:=False;
        FrmLayout.Visible:=False;
     except
        on E: Exception do HandleException( E );
     end;
end;

procedure TStage.CmdCapsClick(Sender: TObject);
begin
     // Show the Capabilities frame.
     if FrmCaps.Visible = False then
     begin
          CmdCaption.Caption:='Show Caption';
          CmdLayout.Caption:='Show Layout';
          CmdCaps.Caption:='Hide Capabilities';
          FrmCaption.Visible:=False;
          FrmLayout.Visible:=False;
          FrmCaps.Align:=alClient;
          FrmCaps.Visible:=True;
          CmbCaps.ItemIndex:=0;
     end else begin
          CmdCaps.Caption:='Show Capabilities';
          FrmCaps.Visible:=False;
     end;
end;

procedure TStage.CmdLayoutClick(Sender: TObject);
var
   L, T, R, B: Single;
   OleT, OleR, OleB: OleVariant;
begin
     //Show the Image Layout frame.
     LblStatus.Panels[0].Text:='';
     If FrmLayout.Visible = False Then
     begin
        CmdCaption.Caption:='Show Caption';
        CmdCaps.Caption:='Show Capabilities';
        CmdLayout.Caption:='Hide Layout';
        FrmLayout.Align:=alClient;
        FrmLayout.Visible:=True;
        FrmCaption.Visible:=False;
        FrmCaps.Visible:=False;
        Application.ProcessMessages;
        TwainPRO.GetImageLayout(L, OleT, OleR, OleB);
        T:=TVarData(OleT).vSingle;
        R:=TVarData(OleR).vSingle;
        B:=TVarData(OleB).vSingle;
        EdtIL.Text:=FloatToStr(L);
        EdtIT.Text:=FloatToStr(T);
        EdtIR.Text:=FloatToStr(R);
        EdtIB.Text:=FloatToStr(B);
     end else begin
        CmdLayout.Caption:='Show Layout';
        FrmLayout.Visible:=False;
     end;
end;

procedure TStage.CmdCaptionClick(Sender: TObject);
begin
     // Show the Caption Layout frame
     if not FrmCaption.Visible then
     begin
          CmdLayout.Caption:='Show Layout';
          CmdCaps.Caption:='Show Capabilities';
          CmdCaption.Caption:='Hide Caption';
          FrmCaps.Visible:=False;
          FrmLayout.Visible:=False;
          FrmCaption.Align:=alClient;
          FrmCaption.Visible:=True;
     end else begin
          CmdCaption.Caption:='Show Caption';
          FrmCaption.Visible:=False;
     end;
end;

procedure TStage.CmdULClick(Sender: TObject);
var
   L, T, R, B: Single;
   OleT, OleR, OleB: OleVariant;
   Code: Integer;
begin
     // Update the Image Layout.
     // This can only be done after calling OpenSession and before calling
     // StartSession (ie. in state 4 if you've read the Twain spec).
     try
        LblStatus.Panels[0].Text:='';
        // Let's make sure the user entered valid data.
        Val(EdtIL.Text, L, Code);
        if Code<>0 then
        begin
             ShowMessage('Value of "' + EdtIL.Text + '" in ImageLeft is not a number.');
             Exit;
        end;
        Val(EdtIT.Text, T, Code);
        if Code<>0 then
        begin
             ShowMessage('Value of "' + EdtIT.Text + '" in ImageTop is not a number.');
             Exit;
        end;
        OleT:=T;
        Val(EdtIR.Text, R, Code);
        if Code<>0 then
        begin
             ShowMessage('Value of "' + EdtIR.Text + '" in ImageRight is not a number.');
             Exit;
        end;
        OleR:=R;
        Val(EdtIB.Text, B, Code);
        if Code<>0 then
        begin
             ShowMessage('Value of "' + EdtIB.Text + '" in ImageBottom is not a number.');
             Exit;
        end;
        OleB:=B;
        TwainPRO.SetImageLayout(L, OleT, OleR, OleB);
     except
        on E: Exception do HandleException( E );
     end;
end;

procedure TStage.CmbCapsChange(Sender: TObject);
var
   Indx: LongInt;
begin
     // Cababilities selection.
     // Only process when session is open and FrmCaps can only be visible
     // after OpenSession is called.
     if not FrmCaps.Visible then Exit;
     // Reset all the controls in FrmCaps.
     LblDefault.Visible:=False;
     LblMin.Visible:=False;
     LblMax.Visible:=False;
     List1.Visible:=False;
     List1.Clear;
     EdtCurrent.Text:='';
     CmdUC.Enabled:=False;
     TwainPRO.Capability:=CmbCaps.ItemIndex;

     // Check for support and stop if none.
     if Not TwainPRO.CapSupported then
     begin
          LblDefault.Caption:='Not supported';
          LblDefault.Visible:=True;
          Exit;
     end;
     // What data type is the Cap?  We have to check the data type
     // to know which properties are valid.
     case TwainPRO.CapType of
        // Type ONEVALUE only returns a single value.
        0: begin
                EdtCurrent.Text:=FloatToStr(TwainPRO.CapValue);
                EdtCurrent.Visible:=True;
           end;
        // Type ENUM returns a list of legal values as well as current and
        // default values.  A list of constants is returned and the CapDesc
        // property can be used to find out what the constants mean.
        1: begin
                EdtCurrent.Text:=FloatToStr(TwainPRO.CapValue);
                EdtCurrent.Visible:=True;
                LblDefault.Caption:='Default = ' + FloatToStr(TwainPRO.CapDefault);
                LblDefault.Visible:=True;
                List1.Items.Add('Legal Values:');
                For Indx:=0 To TwainPRO.CapNumItems - 1 do
                    List1.Items.Add(FloatToStr(TwainPRO.CapItem[Indx]) + ' - ' + TwainPRO.CapDesc[Round(TwainPRO.CapItem[Indx])]);
                List1.Visible:=True;
           end;
        // Returns a range of values as well as current and default values.
        2: begin
                EdtCurrent.Text:=FloatToStr(TwainPRO.CapValue);
                EdtCurrent.Visible:=True;
                LblDefault.Caption:='Default = ' + FloatToStr(TwainPRO.CapDefault);
                LblDefault.Visible:=True;
                LblMin.Caption:='MinValue = ' + FloatToStr(TwainPRO.CapMin);
                LblMin.Visible:=True;
                LblMax.Caption:='MaxValue = ' + FloatToStr(TwainPRO.CapMax);
                LblMax.Visible:=True;
           end;
        // Type ARRAY returns a list of values, but no current or default values.
        // This is a less common type that many sources don't use.
        3: begin
                List1.Items.Add('Legal Values:');
                for Indx:=0 To TwainPRO.CapNumItems - 1 do
                   List1.Items.Add(FloatToStr(TwainPRO.CapItem[Indx]));
           end;
     end;
     CmdUC.Enabled:=True;
end;

procedure TStage.CmdUCClick(Sender: TObject);
var
   N: Integer;
   Code: Integer;
begin
     // Update the current Capability.
     // This can only be done after calling OpenSession and before calling
     // StartSession (ie. in state 4 if you've read the Twain spec).
     try
        LblStatus.Panels[0].Text:='';
        // Let's make sure the user entered valid data.
        Val(EdtCurrent.Text, N, Code);
        if Code<>0 then
           ShowMessage('You must enter a real number for the new current value.');
        // In this sample we only set the current value.  Other data types can
        // be used to provide the Source with a new list of legal values to
        // present the user when the UI is enabled, however this requires strict
        // Twain compliance by the Source which is not found in many Twain drivers.
        TwainPRO.CapTypeOut:=0; //TWCAP_ONEVALUE
        TwainPRO.CapValueOut:=N;
        TwainPRO.SetCapOut;
        // Force an update of the Caps frame with the new values.
        CmbCapsChange(Sender);
     except
        on E: Exception do HandleException( E );
     end;
end;

procedure TStage.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Viewer.Close;
end;


procedure TStage.TwainPROPostScan(Sender: TObject; var Cancel: WordBool);

               var
   fn: String;
begin

   // Send the image to the Viewer form and save to file if requested.
     try
        If ChkSave.Checked then
        begin
           fn:=ExtractFilePath(Application.ExeName) + '\image-' + IntToStr(m_ImageCount) + '.bmp';
           TwainPRO.SaveFile(fn);
        end;
        m_ImageCount:=m_ImageCount + 1;
        Viewer.Visible:=True;
        Viewer.AddImage(TwainPRO.hDib);
        Application.ProcessMessages;
     except
        on E: Exception do HandleException( E );
     end;
end;

procedure TStage.Quit1Click(Sender: TObject);
begin
  application.Terminate;
end;

procedure TStage.About1Click(Sender: TObject);
begin
   TwainPRO.About();
end;

end.








end;

end.

unit twain101;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, OleCtrls, StdCtrls, Buttons, TwnPRO4v,
  TWNPRO4Lib_TLB, AxCtrls, Menus;

type
  TForm1 = class(TForm)
    ButtonPanel: TPanel;
    GetImageBitBtn: TBitBtn;
    NoDriverBitBtn: TBitBtn;
    TwnPRO1: TTwainPRO;
    Image1: TImage;
    ListBox1: TListBox;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Quit1: TMenuItem;
    About1: TMenuItem;
    procedure GetImageBitBtnClick(Sender: TObject);
    procedure NoDriverBitBtnClick(Sender: TObject);
   
    procedure FormCreate(Sender: TObject);
    procedure TwnPRO1PostScan(Sender: TObject; var Cancel: WordBool);
    procedure Quit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
 



  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.GetImageBitBtnClick(Sender: TObject);
begin
  TWNPRO1.SelectSource;
  TWNPRO1.ShowUI := True;
  TWNPRO1.StartSession;
end;

procedure TForm1.NoDriverBitBtnClick(Sender: TObject);
begin
  TWNPRO1.SelectSource;
  TWNPRO1.ShowUI := False;
  TWNPRO1.StartSession;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  TwnPRO1.hParentWnd := Form1.Handle;

end;

procedure TForm1.TwnPRO1PostScan(Sender: TObject; var Cancel: WordBool);
var
 TempFileName : String;
begin
   TempFileName := ExtractFileDir(Application.ExeName) + '\MyScan.bmp';
   TwnPRO1.SaveFile( TempFileName );
   Image1.Picture.LoadFromFile(TempFileName);
end;

procedure TForm1.Quit1Click(Sender: TObject);
begin

        Close;

end;

procedure TForm1.About1Click(Sender: TObject);
begin

        TwnPRO1.About;
     
end;

end.

program TwnPro;

uses
  Forms,
  FrmStage in 'FrmStage.pas' {Stage},
  FrmView in 'FrmView.pas' {Viewer};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TStage, Stage);
  Application.CreateForm(TViewer, Viewer);
  Application.Run;
end.

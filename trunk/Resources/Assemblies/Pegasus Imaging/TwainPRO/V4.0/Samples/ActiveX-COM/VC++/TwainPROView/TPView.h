// TPView.h : main header file for the TPVIEW application
//

#if !defined(AFX_TPVIEW_H__5124B3C5_70F3_11D3_A8FB_DA889974722E__INCLUDED_)
#define AFX_TPVIEW_H__5124B3C5_70F3_11D3_A8FB_DA889974722E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTPViewApp:
// See TPView.cpp for the implementation of this class
//

class CTPViewApp : public CWinApp
{
public:
	CTPViewApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTPViewApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTPViewApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TPVIEW_H__5124B3C5_70F3_11D3_A8FB_DA889974722E__INCLUDED_)

// TwnPRODlg.cpp : implementation file
//

#include "stdafx.h"
#include "TwnPRO.h"
#include "TwnPRODlg.h"
#include <string.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTwnPRODlg dialog

CTwnPRODlg::CTwnPRODlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTwnPRODlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTwnPRODlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTwnPRODlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTwnPRODlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTwnPRODlg, CDialog)
	//{{AFX_MSG_MAP(CTwnPRODlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SELECTSOURCE, OnSelectSource)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SELFN, OnSelectFilename)
	ON_BN_CLICKED(IDC_STARTSESSION, OnStartSession)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#include "..\Include\tp4_open.cpp"



/////////////////////////////////////////////////////////////////////////////
// CTwnPRODlg message handlers

BOOL CTwnPRODlg::OnInitDialog()
{
	CDialog::OnInitDialog();

  // Open TwainPRO Initialization
  HINSTANCE hDLL = TP_Open();

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

  // Create a TwainPRO object
  ppCTwainPRO = new CTwainPRO((DWORD)this, 1);
  pTwainPRO = ppCTwainPRO->pTwainPRO;
  // Set the application window handle to TwainPRO. This property was added
  // specfically for creating TwainPRO in-proc and the control will not work
  // unless this property is set with the application's window handle.
  pTwainPRO->hParentWnd = (long)m_hWnd;
  // Set up the PostScan event Handler
  ppCTwainPRO->SetPostScanEvent(PostScan);


  // Init controls
  char buf[_MAX_PATH];
  GetCurrentDirectory( _MAX_PATH, buf );
  lstrcat( buf, "\\image.bmp" );
  SetDlgItemText( IDC_FILENAME, buf );

  // Must Close the initialization AFTER the first instance of the TwainPRO control is created
  TP_Close(hDLL);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CTwnPRODlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTwnPRODlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTwnPRODlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


/////////////////////////////////////////////////////////////////////////////
// We have to unhook our events and destroy the CapturePRO control before
// exiting the application.
//
void CTwnPRODlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
  //clean up
  pTwainPRO = NULL;
  if (ppCTwainPRO) 
    delete ppCTwainPRO;
}


/////////////////////////////////////////////////////////////////////////////
// Save the image when the scan is complete
//
// This event occurs when the scanning operation is complete.
// We just pass the DIB that we acuired on the ImagXpress for viewing.
// (Of course, once ImagXpress has it, it can save it, sharpen it,
// soften it, etc...
HRESULT CTwnPRODlg::PostScan(DWORD instancePtr, DWORD ObjID, VARIANT_BOOL *Cancel)
{
  CTwnPRODlg *p = (CTwnPRODlg *) instancePtr;
  char buf[_MAX_PATH];
  p->GetDlgItemText( IDC_FILENAME, buf, _MAX_PATH );
  p->pTwainPRO->SaveFile( buf );
  return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Pop up a dialog to choose a filename for the image.
//
void CTwnPRODlg::OnSelectFilename() 
{
   OPENFILENAME   ofn;
   char           buf[_MAX_PATH] = {0};

   GetDlgItemText( IDC_FILENAME, buf, _MAX_PATH );

   memset( &ofn, 0, sizeof (OPENFILENAME) );
   ofn.lStructSize       = sizeof (OPENFILENAME);
   ofn.hwndOwner         = m_hWnd;
   ofn.lpstrFilter       = "Image files\0*.bmp;*.jpg;*.pic;*.tif\0";
   ofn.lpstrFile         = buf;
   ofn.nMaxFile          = _MAX_PATH;
   ofn.lpstrFileTitle    = "TwainPRO";
   ofn.lpstrDefExt       = (PSTR) "jpg";

   if (GetOpenFileName ( &ofn ))
      SetDlgItemText( IDC_FILENAME, buf );
}


/////////////////////////////////////////////////////////////////////////////
// Ask Twain to pop up a dialog of available Twain sources.
//
void CTwnPRODlg::OnSelectSource() 
{
	
 try  
      {
      pTwainPRO->SelectSource();
      }
   catch(_com_error &e)
      {
      HandleError( e );
      }	
}


/////////////////////////////////////////////////////////////////////////////
// Start a session - Brings the Twain Source's UI up for scanning.
//
void CTwnPRODlg::OnStartSession() 
{


   try  
      {
      pTwainPRO->OpenSession();
      }
   catch(_com_error &e)
      {
      HandleError( e );
      }	


   	pTwainPRO->Capability = TWCAP_UNITS;
	if(pTwainPRO->CapSupported) {

		pTwainPRO->CapTypeOut = TWCAP_ONEVALUE;
		pTwainPRO->CapValueOut = TWUN_INCHES;
	
		try  
			{
			pTwainPRO->SetCapOut();
			}
		catch(_com_error &e)
			{
			HandleError( e );
			return;
			}	
	}

	pTwainPRO->Capability = TWCAP_PIXELTYPE;
	pTwainPRO->CapTypeOut = TWCAP_ONEVALUE;
	pTwainPRO->CapValueOut = 0;
	try  
		{
		pTwainPRO->SetCapOut();
		}
	catch(_com_error &e)
		{
		HandleError( e );
		return;
		}	

	pTwainPRO->Capability = TWCAP_XRESOLUTION;
	pTwainPRO->CapValueOut = 300;
	pTwainPRO->CapTypeOut = TWCAP_ONEVALUE;
	try  
		{
		pTwainPRO->SetCapOut();
		}
	catch(_com_error &e)
		{
		HandleError( e );
		return;
		}	

	pTwainPRO->Capability = TWCAP_YRESOLUTION;
	if(pTwainPRO->CapSupported) {
		pTwainPRO->CapValueOut = 300;
		pTwainPRO->CapTypeOut = TWCAP_ONEVALUE;
		try  
			{
			pTwainPRO->SetCapOut();
			}
		catch(_com_error &e)
			{
			HandleError( e );
			return;
			}	
		}

	try  
		{
		}
	catch(_com_error &e)
		{
		HandleError( e );
		return;
		}	
	
	pTwainPRO->Capability = TWCAP_UICONTROLLABLE;
	if(pTwainPRO->CapSupported)
		pTwainPRO->ShowUI = FALSE;

	try  
		{
		pTwainPRO->StartSession();
		}
	catch(_com_error &e)
		{
		HandleError( e );
		return;
		}	
}

/////////////////////////////////////////////////////////////////////////////
// Handle exception and display error information
//
HRESULT CTwnPRODlg::HandleError( _com_error e )
{
   if (&e)
      {
      char buf[200], buf2[200];
      long i;
      WORD wErr = LOWORD(e.Error());

      // If TwainPRO returns 1000, then the error came from the Twain DSM
      // and there is additional info in the ErrorCode and ErrorString props
      if (wErr == 1000)
         {
         // Form a string in the style: ErrCode: ErrString
         itoa( pTwainPRO->GetErrorCode(), buf, 10 );
         _bstr_t bs = pTwainPRO->GetErrorString();
	      WideCharToMultiByte(CP_ACP, 0, bs, -1, buf2, 200, NULL, NULL);
         lstrcat( buf, ": " );
         lstrcat( buf, buf2 );
         }
      else
         {
         itoa( wErr, buf, 10 );
         _bstr_t bs = e.Description();
	      WideCharToMultiByte(CP_ACP, 0, bs, -1, buf2, 200, NULL, NULL);
         // Strip the newline (\n) character out of the error message
         for (i=0; buf2[i] != 10; i++);
         buf2[i] = 32;
         lstrcat( buf, ": " );
         lstrcat( buf, buf2 );
         }

      SetDlgItemText( IDC_ERROR, buf );
      }

   return S_OK;
}


void CTwnPRODlg::OnClose() 
{
  EndDialog(IDOK);
}

VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Viewer 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Viewer"
   ClientHeight    =   7005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9060
   Icon            =   "View.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7005
   ScaleWidth      =   9060
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXpr 
      Height          =   6135
      Left            =   600
      TabIndex        =   5
      Top             =   600
      Width           =   8055
      _ExtentX        =   14208
      _ExtentY        =   10821
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   311637386
      ErrInfo         =   892405924
      Persistence     =   -1  'True
      _cx             =   14208
      _cy             =   10821
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.TextBox EdtTotal 
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "0"
      Top             =   0
      Width           =   615
   End
   Begin VB.CommandButton CmdNext 
      Caption         =   "-->"
      Enabled         =   0   'False
      Height          =   315
      Left            =   780
      TabIndex        =   2
      Top             =   0
      Width           =   375
   End
   Begin VB.TextBox EdtImag 
      Height          =   285
      Left            =   360
      Locked          =   -1  'True
      TabIndex        =   1
      Top             =   0
      Width           =   435
   End
   Begin VB.CommandButton CmdPrev 
      Caption         =   "<--"
      Enabled         =   0   'False
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   375
   End
   Begin VB.Label Label1 
      Caption         =   "Total:"
      Height          =   195
      Left            =   1440
      TabIndex        =   4
      Top             =   60
      Width           =   435
   End
End
Attribute VB_Name = "Viewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Public Sub AddImage()
Dim Index As Long

Index = CLng(EdtTotal.Text) + 1
EdtTotal.Text = Index
IXpr.hDIB = Stage.TwainPRO.hDIB
If Index > 1 Then
   CmdPrev.Enabled = True
End If
CmdNext.Enabled = False
EdtImag.Text = Index
If Viewer.Visible = False Then
   Viewer.Show
End If
End Sub

Private Sub CmdNext_Click()
Dim Index As Long

Index = CLng(EdtImag.Text) + 1
EdtImag.Text = Index

If Index >= CLng(EdtTotal.Text) Then
   CmdNext.Enabled = False
End If
If CmdPrev.Enabled = False Then
   CmdPrev.Enabled = True
End If
Resize
End Sub

Private Sub CmdPrev_Click()
Dim Index As Long

Index = CLng(EdtImag.Text) - 1
EdtImag.Text = Index
If Index <= 1 Then
   CmdPrev.Enabled = False
End If
If CmdNext.Enabled = False Then
   CmdNext.Enabled = True
End If
Resize
End Sub

Private Sub Resize()
Dim Index As Long, Width As Long, Height As Long

Index = CLng(EdtImag.Text)

Width = IXpr.Width * Screen.TwipsPerPixelX
If Width < 2600 Then
   Width = 2600
End If

Height = IXpr.Height * Screen.TwipsPerPixelY
Viewer.Move Viewer.Left, Viewer.Top, Width, Height

End Sub


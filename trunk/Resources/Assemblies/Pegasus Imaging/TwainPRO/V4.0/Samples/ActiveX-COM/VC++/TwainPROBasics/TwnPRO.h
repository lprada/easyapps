// TwnPRO.h : main header file for the TWNPRO application
//

#if !defined(AFX_TWNPRO_H__B3B17446_3620_11D3_BF3E_B8EE28000000__INCLUDED_)
#define AFX_TWNPRO_H__B3B17446_3620_11D3_BF3E_B8EE28000000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTwnPROApp:
// See TwnPRO.cpp for the implementation of this class
//

class CTwnPROApp : public CWinApp
{
public:
	CTwnPROApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTwnPROApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTwnPROApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TWNPRO_H__B3B17446_3620_11D3_BF3E_B8EE28000000__INCLUDED_)

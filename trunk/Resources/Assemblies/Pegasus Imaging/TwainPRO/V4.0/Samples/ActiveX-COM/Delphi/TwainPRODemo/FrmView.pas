unit FrmView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Clipbrd;

type
  TViewer = class(TForm)
    Image1: TImage;
    procedure AddImage(hDIB: LongInt);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   Viewer: TViewer;
   m_ImageCount: LongInt;

implementation

uses FrmStage;

{$R *.DFM}

procedure TViewer.AddImage(hDIB: LongInt) ;
type
  BITMAPINFOPTR = ^TBITMAPINFO;
var
   Stream : TMemoryStream;
   ptr: Pointer;
   HeaderPtr: BITMAPINFOPTR;
   BMP: TBITMAPFILEHEADER;
   Header: TBITMAPINFOHEADER;
   HeadSize, DibSize, Size: longint;
begin
//Finds the size of the DIB
     DibSize:=GlobalSize(hDIB);
//Locks it and gets a real pointer
     ptr:=GlobalLock(hDIB);
//Figures out the size of the BitmapInfoHeader
     HeaderPtr := Ptr;
     Header := HeaderPtr^.bmiHeader;
     Stream:=Tmemorystream.Create;
     HeadSize := Header.biSize + (sizeof(TRGBQUAD) * Header.biClrUsed);
//Moves to the end of where the BitmapFileHeader will be
     Size := DibSize + HeadSize;
     Stream.SetSize(Size + sizeof(TBITMAPFILEHEADER));
//Writes the DIBs info to the stream
     Stream.Seek(sizeof(TBITMAPFILEHEADER),0);
     Stream.write(Ptr^, Size);
//Unlocks and frees the DIB handle
     GlobalUnLock(hDIB);
     GlobalFree(hDIB);
//Sets BitmapFileHeader info
     BMP.bfType := $4D42;
     BMP.bfSize := Size;
     BMP.bfReserved1 := 0;
     BMP.bfReserved2 := 0;
     BMP.bfOffBits := sizeof(TBITMAPFILEHEADER) + HeadSize;
//Writes the BitmapFileHeader to the front of the stream
     Stream.Seek(0,0);
     Stream.Write(BMP, sizeof(BMP));
//Writes the stream to a TImage component
     Stream.Seek(0,0);
     Image1.Picture.Bitmap.LoadFromStream(Stream);
     Stream.Free;

//Resize form to size of image
     Viewer.ClientWidth := Image1.Width;
     Viewer.ClientHeight := Image1.Height;
end;

end.

// TwnPRO.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TwnPRO.h"
#include "TwnPRODlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTwnPROApp

BEGIN_MESSAGE_MAP(CTwnPROApp, CWinApp)
	//{{AFX_MSG_MAP(CTwnPROApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTwnPROApp construction

CTwnPROApp::CTwnPROApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTwnPROApp object

CTwnPROApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTwnPROApp initialization

BOOL CTwnPROApp::InitInstance()
{
   // Initialize the COM library on the current apartment and 
   // identify the currency model as single-thread apartment (STA).
   // Applications must initialize the COM library before they 
   // can call COM library functions other than CoGetMalloc and 
   // memory allocation functions. 
   CoInitialize(NULL);

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CTwnPRODlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

   // Closes the COM library on the current apartment, 
   // unloads all DLLs loaded by apartment, 
   // frees any other resources that the apartment maintains, and 
   // forces all RPC connections on the apartment to close.
   CoUninitialize();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

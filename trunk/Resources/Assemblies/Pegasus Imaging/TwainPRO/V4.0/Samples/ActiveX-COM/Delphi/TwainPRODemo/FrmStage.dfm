�
 TSTAGE 0�  TPF0TStageStageLeft� Top� Width"Height�CaptionTwainPro SampleColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	Icon.Data
>           (     (                �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ww� wwwwwpwww�pwpww wp w pwpwpw ppw p��p w���pwp�����ww�p  wwp ��	ww���yww�w�y�wpww�yywwwww�y�wwww���w                                                                Menu	MainMenu1OldCreateOrder	OnClose	FormCloseOnCreate
FormCreatePixelsPerInch`
TextHeight TLabelLabel13Left� TopWidth&Height4Caption�This sample demonstrates using the TwainPRO 4  control and accessing various properties and methods. It also demonstrates how to get the scanned image data into a TImage control. WordWrap	  	TCheckBoxChkSaveLeft� ToppWidth� HeightCaptionSave as image-n.bmpTabOrder  	TCheckBox	ChkShowUILeft� TopXWidthAHeightCaptionShowUIChecked	State	cbCheckedTabOrder  TButtonCmdSelSourceLeft TopHWidthyHeightCaptionSelect SourceTabOrderOnClickCmdSelSourceClick  
TStatusBar	LblStatusLeft TopMWidthHeightPanelsWidth2    	TGroupBox	GroupBox1Left Top Width� HeightMAlignalLeftTabOrder TButtonCmdOpenLeftTopWidthyHeightCaptionOpenSessionTabOrder OnClickCmdOpenClick  TButtonCmdStartLeftTop0WidthyHeightCaptionStartSession/AcquireTabOrderOnClickCmdStartClick  TButtonCmdCloseLeftTopPWidthyHeightCaptionCloseSessionTabOrderOnClickCmdCloseClick  TButtonCmdCapsLeftToppWidthyHeightCaptionShow CapabilitiesEnabledTabOrderOnClickCmdCapsClick  TButton	CmdLayoutLeftTop� WidthyHeightCaptionShow LayoutEnabledTabOrderOnClickCmdLayoutClick  TButton
CmdCaptionLeftTop� WidthyHeightCaptionShow CaptionTabOrderOnClickCmdCaptionClick   	TGroupBox	FrmLayoutLeft� TophWidth� Height� CaptionLayoutTabOrder Visible TLabelLabel9LeftTopWidth2HeightCaption
ImageLeft:  TLabelLabel10LeftTop<Width3HeightCaption	ImageTop:  TLabelLabel11LeftTop\Width9HeightCaptionImageRight:  TLabelLabel12LeftTop|WidthAHeightCaptionImageBottom:  TEditEdtILLeftXTopWidthIHeightTabOrder   TEditEdtITLeftXTop8WidthIHeightTabOrder  TEditEdtIRLeftXTopXWidthIHeightTabOrder  TEditEdtIBLeftXTopxWidthIHeightTabOrder  TButtonCmdULLeftTop� WidthAHeightCaptionUpdateTabOrderOnClick
CmdULClick   	TGroupBoxFrmCapsLeft� TopxWidthHeight� CaptionCapabilitiesTabOrderVisible TLabel
LblDefaultLeftTop0Width0HeightCaption
LblDefaultVisible  TLabelLabel2Left(Top� Width%HeightCaptionCurrent:  TLabelLblMinLeft TopPWidthHeightCaptionLblMinVisible  TLabelLblMaxLeft ToppWidth"HeightCaptionLblMaxVisible  	TComboBoxCmbCapsLeftTopWidth� HeightStylecsDropDownList
ItemHeightTabOrder OnChangeCmbCapsChangeItems.Strings
AutoBright
BrightnessContrastGammaPhysical WidthPhysical Height
Pixel TypeRotationUnitsX Native ResolutionY Native ResolutionX ResolutionY Resolution	X Scaling	Y ScalingDevice Online
Indicators
Lamp StateUIControllableTransfer Count	Auto Feed	Auto Scan
Clear PageDuplexDuplex EnabledFeeder EnabledFeeder Loaded	Feed PagePaper Detectable   TListBoxList1LeftTop@Width� HeightY
ItemHeightTabOrderVisible  TEdit
EdtCurrentLeftXTop� Width1HeightTabOrder  TButtonCmdUCLeft� Top� Width9HeightCaptionUpdateTabOrderOnClick
CmdUCClick   	TGroupBox
FrmCaptionLeft Top� WidthHeight� CaptionCaptionTabOrderVisible TLabelLabel1LeftTop;Width9Height	AlignmenttaRightJustifyCaptionCaptionLeft:  TLabelLabel8LeftTopSWidth:Height	AlignmenttaRightJustifyCaptionCaptionTop:  TLabelLabel3LeftTopkWidthCHeight	AlignmenttaRightJustifyCaptionCaptionWidth:  TLabelLabel4LeftTop� WidthFHeight	AlignmenttaRightJustifyCaptionCaptionHeight:  TLabelLabel5LeftTop� Width'Height	AlignmenttaRightJustifyCaptionCaption:  TLabelLabel6Left� Top@Width"Height	AlignmenttaRightJustifyCaptionHAlign:  TLabelLabel7Left� ToppWidth!Height	AlignmenttaRightJustifyCaptionVAlign:  	TCheckBoxChkClipLeftTopWidthJHeight	AlignmenttaLeftJustifyCaptionClip CaptionTabOrder   	TCheckBox	ChkShadowLeft� TopWidthSHeight	AlignmenttaLeftJustifyCaptionShadow TextChecked	State	cbCheckedTabOrder  TEdit
EdtCapLeftLeftPTop8Width1HeightTabOrderText0  TEdit	EdtCapTopLeftPTopPWidth1HeightTabOrderText0  TEditEdtCapWidthLeftPTophWidth1HeightTabOrderText0  TEditEdtCapHeightLeftPTop� Width1HeightTabOrderText0  TEdit
EdtCaptionLeftTop� Width� HeightTabOrderText.Sample Caption (Change this in "Show Caption")  	TComboBox	CmbVAlignLeft� Top� WidthIHeightStylecsDropDownList
ItemHeightTabOrderItems.Strings	VAlignTopVAlignBottomVAlignCenter   	TComboBox	CmbHAlignLeft� TopPWidthIHeightStylecsDropDownList
ItemHeightTabOrderItems.Strings
HAlignLeftHAlignRightHAlignCenter    	TTwainPROTwainPROLeft� Top� Width Height 
OnPostScanTwainPROPostScanControlData
4   B   7 2 C 6 E 6 6 0 3 6 5 7 0 2 4 4 9 5 1 7 5 E C E 4 B 7 8 9 F 4 1   �pV,7���  O  O        ude%	 R������ � K�Q   �DB MS Sans Serif                         ����        $                                                                                         	TMainMenu	MainMenu1Left� Top0 	TMenuItemFile1CaptionFile 	TMenuItemQuit1CaptionQuitOnClick
Quit1Click   	TMenuItemAbout1CaptionAboutOnClickAbout1Click    
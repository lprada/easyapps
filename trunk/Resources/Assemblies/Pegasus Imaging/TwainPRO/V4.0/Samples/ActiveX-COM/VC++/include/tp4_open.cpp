
// TwainPRO COM VC++ Initialization routines.
// Copyright (c) 1999-2003 Pegasus Imaging Corp


typedef HRESULT (WINAPI* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE TP_Open()
{
  HINSTANCE    hDLL;            // Handle to TwainPro DLL
  LPFNDLL_PSU  lpfnDllFunc1;    // Function pointer to the PS_Unlock function
  HRESULT      hr;

  hDLL = LoadLibrary("TwnPRO4.dll");   // TwainPRO ATL COM control
  if (hDLL)
  {
    // Get the address of the PS_US function.  You need to give the COM object
    // the serial number and registration code that you received when
    // you purchased TwainPRO
    lpfnDllFunc1 = (LPFNDLL_PSU)GetProcAddress(hDLL, "PS_Unlock");

    if (lpfnDllFunc1)
      // Call the unlock function with the serial number and registration code
      // that you received when you purchased ImagXpress.
      // NOTE: The serial number and registration code shown below is for
      // illustration purposes only.
      hr = lpfnDllFunc1(123456789, 123456789, 123456789, 123456789);
    else
    {
      FreeLibrary(hDLL);
      hDLL = NULL;
    }
  }
  return hDLL;
}

void TP_Close(HINSTANCE hDLL)
{
  if (hDLL)
    FreeLibrary(hDLL);
}

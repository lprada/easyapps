VERSION 5.00
Object = "{F514BE21-8D87-4DB5-9B1B-4204C81D2AD4}#1.0#0"; "TwnPRO4.dll"
Begin VB.Form Stage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "TwainPRO Sample"
   ClientHeight    =   6630
   ClientLeft      =   45
   ClientTop       =   630
   ClientWidth     =   7515
   Icon            =   "TwainPRO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6630
   ScaleWidth      =   7515
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      ItemData        =   "TwainPRO.frx":0442
      Left            =   120
      List            =   "TwainPRO.frx":0455
      TabIndex        =   47
      Top             =   120
      Width           =   7215
   End
   Begin VB.CommandButton CmdCaption 
      Caption         =   "Show Caption"
      Height          =   435
      Left            =   120
      TabIndex        =   46
      Top             =   4320
      Width           =   1875
   End
   Begin VB.Frame FrmCaption 
      Caption         =   "Caption"
      Height          =   2895
      Left            =   3600
      TabIndex        =   28
      Top             =   2880
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CheckBox ChkClip 
         Alignment       =   1  'Right Justify
         Caption         =   "Clip Caption"
         Height          =   195
         Left            =   120
         TabIndex        =   42
         Top             =   360
         Width           =   1215
      End
      Begin VB.ComboBox CmbVAlign 
         Height          =   315
         ItemData        =   "TwainPRO.frx":0577
         Left            =   2160
         List            =   "TwainPRO.frx":0584
         Style           =   2  'Dropdown List
         TabIndex        =   41
         Top             =   1965
         Width           =   1395
      End
      Begin VB.ComboBox CmbHAlign 
         Height          =   315
         ItemData        =   "TwainPRO.frx":05AF
         Left            =   2160
         List            =   "TwainPRO.frx":05BC
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   1125
         Width           =   1395
      End
      Begin VB.TextBox EdtCapWidth 
         Height          =   285
         Left            =   1140
         TabIndex        =   37
         Text            =   "0"
         Top             =   1560
         Width           =   675
      End
      Begin VB.TextBox EdtCapHeight 
         Height          =   285
         Left            =   1140
         TabIndex        =   36
         Text            =   "0"
         Top             =   1980
         Width           =   675
      End
      Begin VB.CheckBox ChkShadow 
         Alignment       =   1  'Right Justify
         Caption         =   "Shadow Text"
         Height          =   195
         Left            =   2220
         TabIndex        =   32
         Top             =   360
         Value           =   1  'Checked
         Width           =   1260
      End
      Begin VB.TextBox EdtCaption 
         Height          =   285
         Left            =   1140
         TabIndex        =   31
         Text            =   "TwainPRO 4"
         Top             =   2400
         Width           =   2415
      End
      Begin VB.TextBox EdtCapLeft 
         Height          =   285
         Left            =   1140
         TabIndex        =   30
         Text            =   "0"
         Top             =   720
         Width           =   675
      End
      Begin VB.TextBox EdtCapTop 
         Height          =   285
         Left            =   1140
         TabIndex        =   29
         Text            =   "0"
         Top             =   1140
         Width           =   675
      End
      Begin VB.Label Label12 
         Caption         =   "VAlign:"
         Height          =   255
         Left            =   2040
         TabIndex        =   44
         Top             =   1740
         Width           =   915
      End
      Begin VB.Label Label8 
         Caption         =   "HAlign:"
         Height          =   195
         Left            =   2040
         TabIndex        =   43
         Top             =   900
         Width           =   915
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "CaptionHeight:"
         Height          =   255
         Left            =   60
         TabIndex        =   40
         Top             =   2025
         Width           =   1035
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "CaptionWidth:"
         Height          =   255
         Left            =   60
         TabIndex        =   39
         Top             =   1605
         Width           =   1035
      End
      Begin VB.Label Label9 
         Caption         =   "Caption:"
         Height          =   255
         Left            =   480
         TabIndex        =   35
         Top             =   2445
         Width           =   615
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "CaptionLeft:"
         Height          =   255
         Left            =   180
         TabIndex        =   34
         Top             =   765
         Width           =   915
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "CaptionTop:"
         Height          =   255
         Left            =   180
         TabIndex        =   33
         Top             =   1185
         Width           =   915
      End
   End
   Begin VB.CommandButton CmdOpen 
      Caption         =   "OpenSession"
      Height          =   435
      Left            =   120
      TabIndex        =   45
      Top             =   1920
      Width           =   1875
   End
   Begin VB.Frame FrmCaps 
      Caption         =   "Capabilities"
      Height          =   2895
      Left            =   3180
      TabIndex        =   2
      Top             =   2520
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CommandButton CmdUC 
         Caption         =   "Update"
         Height          =   315
         Left            =   2280
         TabIndex        =   12
         Top             =   2280
         Width           =   855
      End
      Begin VB.TextBox EdtCurrent 
         Height          =   315
         Left            =   1140
         TabIndex        =   10
         Top             =   2280
         Width           =   855
      End
      Begin VB.ListBox List1 
         Height          =   840
         Left            =   480
         TabIndex        =   9
         Top             =   1140
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.ComboBox CmbCaps 
         Height          =   315
         ItemData        =   "TwainPRO.frx":05E7
         Left            =   480
         List            =   "TwainPRO.frx":0642
         TabIndex        =   3
         Top             =   360
         Width           =   2655
      End
      Begin VB.Label Label2 
         Caption         =   "Current:"
         Height          =   195
         Left            =   480
         TabIndex        =   11
         Top             =   2340
         Width           =   555
      End
      Begin VB.Label LblMax 
         Caption         =   "LblMax"
         Height          =   195
         Left            =   480
         TabIndex        =   8
         Top             =   1560
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.Label LblMin 
         Caption         =   "LblMin"
         Height          =   195
         Left            =   480
         TabIndex        =   7
         Top             =   1200
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.Label LblDefault 
         Caption         =   "LblDefault"
         Height          =   195
         Left            =   480
         TabIndex        =   6
         Top             =   840
         Visible         =   0   'False
         Width           =   2715
      End
   End
   Begin VB.Frame FrmLayout 
      Caption         =   "Layout"
      Height          =   2895
      Left            =   2400
      TabIndex        =   13
      Top             =   2280
      Visible         =   0   'False
      Width           =   3735
      Begin VB.CommandButton CmdUL 
         Caption         =   "Update"
         Height          =   315
         Left            =   360
         TabIndex        =   22
         Top             =   2280
         Width           =   855
      End
      Begin VB.TextBox EdtIB 
         Height          =   315
         Left            =   1500
         TabIndex        =   20
         Top             =   1680
         Width           =   975
      End
      Begin VB.TextBox EdtIR 
         Height          =   315
         Left            =   1500
         TabIndex        =   18
         Top             =   1260
         Width           =   975
      End
      Begin VB.TextBox EdtIT 
         Height          =   315
         Left            =   1500
         TabIndex        =   16
         Top             =   840
         Width           =   975
      End
      Begin VB.TextBox EdtIL 
         Height          =   315
         Left            =   1500
         TabIndex        =   14
         Top             =   420
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "ImageBottom:"
         Height          =   195
         Left            =   360
         TabIndex        =   21
         Top             =   1740
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "ImageRight:"
         Height          =   195
         Left            =   360
         TabIndex        =   19
         Top             =   1320
         Width           =   915
      End
      Begin VB.Label Label4 
         Caption         =   "ImageTop:"
         Height          =   195
         Left            =   360
         TabIndex        =   17
         Top             =   900
         Width           =   915
      End
      Begin VB.Label Label3 
         Caption         =   "ImageLeft:"
         Height          =   195
         Left            =   360
         TabIndex        =   15
         Top             =   480
         Width           =   915
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   255
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   5955
      TabIndex        =   26
      Top             =   5280
      Width           =   6015
      Begin VB.Label lblStatus 
         Height          =   300
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   5895
      End
   End
   Begin VB.CommandButton CmdLayout 
      Caption         =   "Show Layout"
      Enabled         =   0   'False
      Height          =   435
      Left            =   120
      TabIndex        =   5
      Top             =   3840
      Width           =   1875
   End
   Begin VB.CommandButton CmdCaps 
      Caption         =   "Show Capabilities"
      Enabled         =   0   'False
      Height          =   435
      Left            =   120
      TabIndex        =   4
      Top             =   3360
      Width           =   1875
   End
   Begin VB.CommandButton CmdClose 
      Caption         =   "CloseSession"
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   2880
      Width           =   1875
   End
   Begin VB.CommandButton CmdStart 
      Caption         =   "StartSession/Acquire"
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   2400
      Width           =   1875
   End
   Begin VB.CheckBox ChkShowUI 
      Caption         =   "ShowUI"
      Height          =   255
      Left            =   2400
      TabIndex        =   23
      Top             =   1320
      Value           =   1  'Checked
      Width           =   1395
   End
   Begin VB.CheckBox ChkSave 
      Caption         =   "Save as image-n.bmp"
      Height          =   195
      Left            =   2400
      TabIndex        =   24
      Top             =   1920
      Width           =   2055
   End
   Begin VB.CommandButton CmdSelSource 
      Caption         =   "Select Source"
      Height          =   495
      Left            =   120
      TabIndex        =   25
      Top             =   1320
      Width           =   1875
   End
   Begin TWNPRO4LibCtl.TwainPRO TwainPRO 
      Left            =   480
      Top             =   6120
      _ExtentX        =   847
      _ExtentY        =   847
      ErrStr          =   "D747977B85B3CE33CE1003B846074273"
      ErrCode         =   557978749
      ErrInfo         =   777488823
      _cx             =   847
      _cy             =   847
      Caption         =   ""
      ForeColor       =   -2147483630
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ProductName     =   ""
      ProductFamily   =   ""
      Manufacturer    =   ""
      VersionInfo     =   ""
      MaxImages       =   -1
      ShowUI          =   -1  'True
      SaveJPEGLumFactor=   32
      SaveJPEGChromFactor=   36
      SaveJPEGSubSampling=   2
      SaveJPEGProgressive=   0   'False
      PICPassword     =   ""
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveTIFFCompression=   0
      SaveMultiPage   =   0   'False
      CaptionLeft     =   0
      CaptionTop      =   0
      CaptionWidth    =   0
      CaptionHeight   =   0
      ShadowText      =   -1  'True
      ClipCaption     =   0   'False
      HAlign          =   0
      VAlign          =   0
      EnableExtCaps   =   -1  'True
      CloseOnCancel   =   -1  'True
      TransferMode    =   0
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Stage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Dim m_ImageCount As Long
' Cababilities selection
Private Sub CmbCaps_Click()
Dim Index As Long

' Only process when session is open and FrmCaps can only be visible
' after OpenSession is called
If FrmCaps.Visible = False Then
   Exit Sub
End If

' Reset all the controls in FrmCaps
LblDefault.Visible = False
LblMin.Visible = False
LblMax.Visible = False
List1.Visible = False
List1.Clear
EdtCurrent.Text = ""
CmdUC.Enabled = False
TwainPRO.Capability = CmbCaps.ListIndex

' Check for support and stop if none
If (TwainPRO.CapSupported) Then
    MsgBox "True"
Else
    MsgBox "False"
   LblDefault.Caption = "Not supported"
   LblDefault.Visible = True
   Exit Sub
End If

' What data type is the Cap?  We have to check the data type
' to know which properties are valid
Select Case TwainPRO.CapType
   ' Type ONEVALUE only returns a single value
   Case TWCAP_ONEVALUE
      EdtCurrent.Text = TwainPRO.CapValue
      EdtCurrent.Visible = True
      
   ' Type ENUM returns a list of legal values as well as current and
   ' default values.  A list of constants is returned and the CapDesc
   ' property can be used to find out what the constants mean
   Case TWCAP_ENUM
      EdtCurrent.Text = TwainPRO.CapValue
      EdtCurrent.Visible = True
      LblDefault.Caption = "Default = " & TwainPRO.CapDefault
      LblDefault.Visible = True
      List1.AddItem "Legal Values:"
      For Index = 0 To TwainPRO.CapNumItems - 1
         List1.AddItem TwainPRO.CapItem(Index) & " - " & TwainPRO.CapDesc(TwainPRO.CapItem(Index))
      Next
      List1.Visible = True
      
   ' Type ARRAY returns a list of values, but no current or default values
   ' This is a less common type that many sources don't use
   Case TWCAP_ARRAY
      List1.AddItem "Legal Values:"
      For Index = 0 To TwainPRO.CapNumItems - 1
         List1.AddItem TwainPRO.CapItem(Index)
      Next
   
   ' Returns a range of values as well as current and default values
   Case TWCAP_RANGE
      EdtCurrent.Text = TwainPRO.CapValue
      EdtCurrent.Visible = True
      LblDefault.Caption = "Default = " & TwainPRO.CapDefault
      LblDefault.Visible = True
      LblMin.Caption = "MinValue = " & TwainPRO.CapMin
      LblMin.Visible = True
      LblMax.Caption = "MaxValue = " & TwainPRO.CapMax
      LblMax.Visible = True
     
End Select
CmdUC.Enabled = True
End Sub
' Show the Capabilities frame
Private Sub CmdCaps_Click()

If FrmCaps.Visible = False Then
   CmdCaption.Caption = "Show Caption"
   CmdLayout.Caption = "Show Layout"
   CmdCaps.Caption = "Hide Capabilities"
   FrmCaption.Visible = False
   FrmLayout.Visible = False
   FrmCaps.Move 2400, 2280
   FrmCaps.Visible = True
   CmbCaps.ListIndex = 0
Else
   CmdCaps.Caption = "Show Capabilities"
   FrmCaps.Visible = False
End If

End Sub

Private Sub CmdCaption_Click()

If FrmCaption.Visible = False Then
   CmdLayout.Caption = "Show Layout"
   CmdCaps.Caption = "Show Capabilities"
   CmdCaption.Caption = "Hide Caption"
   FrmCaps.Visible = False
   FrmLayout.Visible = False
   FrmCaption.Move 2400, 2280
   FrmCaption.Visible = True
Else
   CmdCaption.Caption = "Show Caption"
   FrmCaption.Visible = False
End If

End Sub

' Show the Image Layout frame
Private Sub CmdLayout_Click()
Dim l As Single, t As Single, r As Single, b As Single
lblStatus.Caption = ""

If FrmLayout.Visible = False Then
   CmdCaption.Caption = "Show Caption"
   CmdCaps.Caption = "Show Capabilities"
   CmdLayout.Caption = "Hide Layout"
   FrmCaption.Visible = False
   FrmCaps.Visible = False
   TwainPRO.GetImageLayout l, t, r, b
   EdtIL.Text = l
   EdtIT.Text = t
   EdtIR.Text = r
   EdtIB.Text = b
   FrmLayout.Move 2400, 2280
   FrmLayout.Visible = True
Else
   CmdLayout.Caption = "Show Layout"
   FrmLayout.Visible = False
End If

End Sub
' Open a session (move to state 4 in the Twain spec)
' This opens the data source and prepares it for capability
' negotiation.  At this point, capabilities and layout can be
' set for use in the upcoming image acquisition
Private Sub CmdOpen_Click()
On Error GoTo OSError
lblStatus.Caption = ""

TwainPRO.OpenSession
CmdCaps.Enabled = True
CmdLayout.Enabled = True
Exit Sub

OSError:
GetError
End Sub
' Request the Twain Data Source Manager display a list of
' available Twain Sources
Private Sub CmdSelSource_Click()
On Error GoTo SelError
lblStatus.Caption = ""

TwainPRO.CloseSession
CmdCaps.Enabled = False
CmdLayout.Enabled = False
TwainPRO.SelectSource
Exit Sub

SelError:
GetError
End Sub
' Start the session (move to state 5 in the Twain spec)
' This will bring up the UI if ShowUI is enabled or start
' a scan if ShowUI is disabled.
Private Sub CmdStart_Click()
On Error GoTo SSError
lblStatus.Caption = ""

CmdCaps.Enabled = False
CmdLayout.Enabled = False
FrmCaps.Visible = False
FrmLayout.Visible = False
UpdateProps
m_ImageCount = 0
TwainPRO.StartSession
Exit Sub

SSError:
GetError
End Sub
' Close a session
' This is necessary only after calling OpenDSM or OpenSession methods.
' StartSession automatically calls CloseSession before returning.
Private Sub CmdClose_Click()
On Error GoTo CSError
lblStatus.Caption = ""

TwainPRO.CloseSession
CmdCaps.Enabled = False
CmdLayout.Enabled = False
FrmCaps.Visible = False
FrmLayout.Visible = False
Exit Sub

CSError:
GetError
End Sub
' Update the Image Layout
' This can only be done after calling OpenSession and before calling
' StartSession (ie. in state 4 if you've read the Twain spec)
Private Sub CmdUL_Click()
Dim l As Single, t As Single, r As Single, b As Single
On Error GoTo ULError
lblStatus.Caption = ""

' Let's make sure the user entered valid data
If False = IsNumeric(EdtIL.Text) Or False = IsNumeric(EdtIT.Text) Or False = IsNumeric(EdtIR.Text) Or False = IsNumeric(EdtIB.Text) Then
   MsgBox "You must enter a real number for each field."
End If

l = EdtIL.Text
t = EdtIT.Text
r = EdtIR.Text
b = EdtIB.Text
TwainPRO.SetImageLayout l, t, r, b
Exit Sub

ULError:
GetError
End Sub
' Update the current Capability
' This can only be done after calling OpenSession and before calling
' StartSession (ie. in state 4 if you've read the Twain spec)
Private Sub CmdUC_Click()
On Error GoTo UpdateError
lblStatus.Caption = ""

' Let's make sure the user entered valid data
If False = IsNumeric(EdtCurrent.Text) Then
   MsgBox "You must enter a real number for the new current value."
End If

' In this sample we only set the current value.  Other data types can
' be used to provide the Source with a new list of legal values to
' present the user when the UI is enabled, however this requires strict
' Twain compliance by the Source which is not found in many Twain drivers
TwainPRO.CapTypeOut = TWCAP_ONEVALUE
TwainPRO.CapValueOut = EdtCurrent.Text
TwainPRO.SetCapOut

' Force an update of the Caps frame with the new values
CmbCaps.ListIndex = CmbCaps.ListIndex
Exit Sub

UpdateError:
GetError
End Sub
' Update the status bar with the current error code and description
Sub GetError()
Dim RetPos As Long
Dim TwnProErr As Long

' Error from an ActiveX object or a user-defined error.
If (Err.Number > vbObjectError And Err.Number < vbObjectError + 65536) Then

    ' This definitively lets us know that that TwainPRO generated the error
    If InStr(1, Err.Source, "TwainPRO", vbTextCompare) <> 0 Then
        
        TwnProErr = Err.Number - vbObjectError
        
        ' ErrorCode TWERR_TWAIN indicates the Twain Source returned an error
        ' and we should check the ErrorCode and ErrorString properties
        ' for extended information
        If TwnProErr = TWERR_TWAIN Then
            lblStatus.Caption = "Data Source Error " & TwainPRO.ErrorCode & ": " & TwainPRO.ErrorString
        Else
            ' Remove carriage return from Description
            RetPos = InStr(1, Err.Description, Chr$(10))
            If RetPos > 0 Then
                lblStatus.Caption = TwnProErr & ": " & Left$(Err.Description, RetPos - 1) & "  " & Mid$(Err.Description, RetPos + 1)
            Else
                lblStatus.Caption = TwnProErr & ": " & Err.Description
            End If
        End If
        
    End If
    
End If

End Sub
' Helper functions for setting Checkbox values to boolean properties
Function ItoB(Value As Integer) As Boolean
If (Value <> 0) Then
   ItoB = True
Else
   ItoB = False
End If
End Function

Function BtoI(Value As Boolean) As Integer
If (Value = True) Then
   BtoI = 1
Else
   BtoI = 0
End If
End Function
Function EtoL(Value As String) As Long
If True = IsNumeric(Value) Then
   EtoL = CLng(Value)
Else
   EtoL = 0
End If
End Function



Private Sub Form_Load()
 Dim txt As String
  
CmbHAlign.ListIndex = 0
CmbVAlign.ListIndex = 0
TwainPRO.Debug = True
End Sub

' Make sure the Viewer form closes when the main for closes
Private Sub Form_Terminate()
Unload Viewer
End Sub

Private Sub Form_Unload(Cancel As Integer)
Unload Viewer
End Sub
' Send the image to the Viewer form and save to file if requested
Private Sub TwainPRO_PostScan(Cancel As Boolean)
Dim temp As Long
Dim fn As String

If ChkSave.Value = 1 Then
   fn = App.Path & "\image-" & m_ImageCount & ".bmp"
   TwainPRO.SaveFile fn
End If

m_ImageCount = m_ImageCount + 1
Viewer.AddImage
DoEvents
Cancel = True
End Sub

' Copy values from fields to TwainPRO control
Sub UpdateProps()
TwainPRO.ShowUI = ItoB(ChkShowUI.Value)
TwainPRO.Caption = EdtCaption.Text
TwainPRO.ClipCaption = ItoB(ChkClip.Value)
TwainPRO.ShadowText = ItoB(ChkShadow.Value)
TwainPRO.CaptionLeft = EtoL(EdtCapLeft.Text)
TwainPRO.CaptionTop = EtoL(EdtCapTop.Text)
TwainPRO.CaptionWidth = EtoL(EdtCapWidth.Text)
TwainPRO.CaptionHeight = EtoL(EdtCapHeight.Text)
TwainPRO.HAlign = CmbHAlign.ListIndex
TwainPRO.VAlign = CmbVAlign.ListIndex
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuAbout_Click()
    TwainPRO.About
End Sub

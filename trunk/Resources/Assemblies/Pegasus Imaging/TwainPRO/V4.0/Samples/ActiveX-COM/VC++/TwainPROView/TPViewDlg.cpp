// TPViewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TPView.h"
#include "TPViewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTPViewDlg dialog

CTPViewDlg::CTPViewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTPViewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTPViewDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTPViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTPViewDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTPViewDlg, CDialog)
	//{{AFX_MSG_MAP(CTPViewDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTNACQUIRE, OnBtnAcquire)
	ON_BN_CLICKED(IDC_BTNSELECT, OnBtnSelect)
	ON_COMMAND(ID_FILE_QUIT, OnFileQuit)
	ON_WM_CLOSE()
	ON_COMMAND(ID_ABOUT, OnAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// Include or event handler routines
#include "..\Include\tp4_open.cpp"
#include "..\Include\ix8_open.cpp"

/////////////////////////////////////////////////////////////////////////////
// CTPViewDlg message handlers

BOOL CTPViewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
  // Open TwainPRO and ImagXpress Initialization
  HINSTANCE hDLL_TP = TP_Open();
  HINSTANCE hDLL_IX = IX_Open();
  
  // Create a TwainPRO object
  ppCTwainPRO = new CTwainPRO((DWORD)this, 1);
  pTwainPRO = ppCTwainPRO->pTwainPRO;
  // Set the application window handle to TwainPRO. This property was added
  // specfically for creating TwainPRO in-proc and the control will not work
  // unless this property is set with the application's window handle.
  pTwainPRO->hParentWnd = (long)m_hWnd;

  // Set up the PostScan event Handler
	ppCTwainPRO->SetPostScanEvent(PostScan);

  // Create an ImagXpress object
  ppCImagXpress = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 155, 20, 400, 300);
  pImagXpress = ppCImagXpress->pImagXpress;
  pImagXpress->BorderType = BORD_Raised;
  pImagXpress->AutoSize = ISIZE_BestFit;
   pImagXpress->PutScrollBars(  SB_Both);

  // MUST close the initialization anytime AFTER the first instance of the object is created
  TP_Close(hDLL_TP);
  IX_Close(hDLL_IX);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTPViewDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTPViewDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CTPViewDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// When the dialog gets destroyed, we
  // delete our ImagXpress and TwainPRO Objects
  pTwainPRO = NULL;
  if (ppCTwainPRO) 
    delete ppCTwainPRO;
  pImagXpress = NULL;
  if (ppCImagXpress) 
    delete ppCImagXpress;
}

void CTPViewDlg::OnBtnAcquire() 
{
  try  
  {
    pTwainPRO->StartSession();
  }
  catch(_com_error &e)
  {
    HandleError( e );
  }	
}

void CTPViewDlg::OnBtnSelect() 
{
  try  
  {
    pTwainPRO->SelectSource();
  }
  catch(_com_error &e)
  {
    HandleError( e );
  }	
}


// TwainPRO Error Handler
HRESULT CTPViewDlg::HandleError( _com_error e )
{
   if (&e)
   {
     char buf[200], buf2[200];
     long i;
     WORD wErr = LOWORD(e.Error());

     // If TwainPRO returns 1000, then the error came from the Twain DSM
     // and there is additional info in the ErrorCode and ErrorString props
     if (wErr == 1000)
     {
       // Form a string in the style: ErrCode: ErrString
       itoa( pTwainPRO->GetErrorCode(), buf, 10 );
       _bstr_t bs = pTwainPRO->GetErrorString();
       WideCharToMultiByte(CP_ACP, 0, bs, -1, buf2, 200, NULL, NULL);
       lstrcat( buf, ": " );
       lstrcat( buf, buf2 );
     }
     else
     {
       itoa( wErr, buf, 10 );
       _bstr_t bs = e.Description();
       WideCharToMultiByte(CP_ACP, 0, bs, -1, buf2, 200, NULL, NULL);
       // Strip the newline (\n) character out of the error message
       for (i=0; buf2[i] != 10; i++)
         buf2[i] = 32;
       lstrcat( buf, ": " );
       lstrcat( buf, buf2 );
      }
      ::MessageBox(m_hWnd, buf, "TwainPRO VC Sample", MB_OK | MB_ICONERROR); 
   }

   return S_OK;
}

// This event occurs when the scanning operation is complete.
// We just pass the DIB that we acuired on the ImagXpress for viewing.
// (Of course, once ImagXpress has it, it can save it, sharpen it,
// soften it, etc...
HRESULT CTPViewDlg::PostScan( DWORD instancePtr, DWORD ObjID, VARIANT_BOOL *Cancel )
{
  CTPViewDlg *p = (CTPViewDlg *) instancePtr;
  ITwainPROPtr   pTP = p->ppCTwainPRO->pTwainPRO;      // Cast the pointer to the TwainPRO Object
  IImagXpressPtr pIX = p->ppCImagXpress->pImagXpress;  // Cast the pointer to the ImagXpress object

  pIX->hDIB = pTP->hDIB;  // Transfer the DIB from TwainPRO to ImagXpress
  return S_OK;
}

void CTPViewDlg::OnFileQuit() 
{
	
	EndDialog(IDOK);
	
}

void CTPViewDlg::OnAbout() 
{
	pTwainPRO->About();
	
}


// ImagXpress COM VC++ Initialization routines.
// Copyright (c) 2005-2006 Pegasus Imaging Corp.


typedef HRESULT (WINAPI* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE IX_Open();
void IX_Close(HINSTANCE hDLL);

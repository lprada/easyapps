
// TwainPRO COM VC++ Initialization routines.
// Copyright (c) 1999-2003 Pegasus Imaging Corp


typedef HRESULT (__cdecl* LPFNDLL_PSU)(long, long, long, long);

HINSTANCE TP_Open();
void TP_Close(HINSTANCE hDLL);

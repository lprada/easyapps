VERSION 5.00
Object = "{F514BE21-8D87-4DB5-9B1B-4204C81D2AD4}#1.0#0"; "TwnPRO4.dll"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form MainForm 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "TwainPro Memory and Compression Sample"
   ClientHeight    =   7125
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   6600
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7125
   ScaleWidth      =   6600
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "MainForm.frx":0000
      Left            =   240
      List            =   "MainForm.frx":000A
      TabIndex        =   10
      Top             =   240
      Width           =   6135
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImgXpr 
      Height          =   4695
      Left            =   2760
      TabIndex        =   9
      Top             =   1320
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   8281
      ErrStr          =   "DE01F52D7352D65450DC456093EC64EB"
      ErrCode         =   259676915
      ErrInfo         =   971704873
      Persistence     =   -1  'True
      _cx             =   6376
      _cy             =   8281
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CheckBox chkUseViewer 
      Caption         =   "Use Viewer"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   5640
      Value           =   1  'Checked
      Width           =   1935
   End
   Begin VB.HScrollBar hscrlJpegQual 
      Height          =   255
      Left            =   120
      Max             =   100
      TabIndex        =   5
      Top             =   5040
      Value           =   80
      Width           =   1935
   End
   Begin VB.ComboBox cmbCompression 
      Height          =   315
      ItemData        =   "MainForm.frx":0088
      Left            =   240
      List            =   "MainForm.frx":008A
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   3720
      Width           =   1935
   End
   Begin VB.CommandButton cmdScan 
      Caption         =   "Scan"
      Enabled         =   0   'False
      Height          =   615
      Left            =   240
      TabIndex        =   1
      Top             =   2400
      Width           =   1815
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "Select Source"
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   1440
      Width           =   1815
   End
   Begin TWNPRO4LibCtl.TwainPRO TwainPRO 
      Left            =   5760
      Top             =   6600
      _ExtentX        =   847
      _ExtentY        =   847
      ErrStr          =   "D747977B85B3CE33CE1003B846074273"
      ErrCode         =   259676915
      ErrInfo         =   -491804405
      _cx             =   847
      _cy             =   847
      Caption         =   ""
      ForeColor       =   -2147483630
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ProductName     =   ""
      ProductFamily   =   ""
      Manufacturer    =   ""
      VersionInfo     =   ""
      MaxImages       =   -1
      ShowUI          =   -1  'True
      SaveJPEGLumFactor=   32
      SaveJPEGChromFactor=   36
      SaveJPEGSubSampling=   2
      SaveJPEGProgressive=   0   'False
      PICPassword     =   ""
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveTIFFCompression=   0
      SaveMultiPage   =   0   'False
      CaptionLeft     =   0
      CaptionTop      =   0
      CaptionWidth    =   0
      CaptionHeight   =   0
      ShadowText      =   -1  'True
      ClipCaption     =   0   'False
      HAlign          =   0
      VAlign          =   0
      EnableExtCaps   =   -1  'True
      CloseOnCancel   =   -1  'True
      TransferMode    =   0
      EvalMode        =   0
   End
   Begin VB.Label lblJpegQualText 
      Caption         =   "JPEG Quality:"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   4560
      Width           =   1215
   End
   Begin VB.Label lblJpegQual 
      Caption         =   "80"
      Height          =   255
      Left            =   1680
      TabIndex        =   6
      Top             =   4560
      Width           =   375
   End
   Begin VB.Label lblCompressTypes 
      Caption         =   "Compression Types:"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   3240
      Width           =   1815
   End
   Begin VB.Label lblStatus 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Select and Scan"
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   6240
      Width           =   6375
      WordWrap        =   -1  'True
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Private Sub chkUseViewer_Click()
    Unload Viewer
End Sub

Private Sub cmbCompression_Change()
Dim nCompressType As Long
   
    nCompressType = cmbCompression.ItemData(cmbCompression.ListIndex)
    If (nCompressType = TWCP_JPEG) Then
        hscrlJpegQual.Enabled = True
    Else
        hscrlJpegQual.Enabled = False
    End If
    

End Sub

Private Sub cmdScan_Click()
Dim nCompressType As Long
On Error GoTo ScanError
    lblStatus.Caption = "Opening Scanner"
    DoEvents
    TwainPRO.OpenSession
    
    If TwainPRO.TransferMode = TWSX_MEMORY Then
        TwainPRO.CapTypeOut = TWCAP_ONEVALUE
        TwainPRO.Capability = TWCAP_USECAPADVANCED
        TwainPRO.CapAdvanced = ICAP_COMPRESSION
                
        ' Only do the following if the Capability is supported and
        ' there is more than one compression type to set. Otherwise,
        ' the data source may say they support this capability but
        ' might return an error if you try to set to TWCP_NONE and
        ' it is the only type. All data sources must support TWCP_NONE.
        If TwainPRO.CapSupported And cmbCompression.ListCount > 1 Then
            lblStatus.Caption = "Setting Compression Mode"
            nCompressType = cmbCompression.ItemData(cmbCompression.ListIndex)
            TwainPRO.CapValueOut = nCompressType
            TwainPRO.SetCapOut
                    
            If (nCompressType = TWCP_JPEG) Then
                'Set the JPEG Quality factor
                TwainPRO.CapAdvanced = ICAP_JPEGQUALITY
                If TwainPRO.CapSupported Then
                    TwainPRO.CapValueOut = hscrlJpegQual.Value
                    TwainPRO.SetCapOut
                End If
                'You could also negotiate the ICAP_JPEGPIXELTYPE capability.
                'I am leaving this to the DataSource
                
            ElseIf (nCompressType <> TWCP_NONE) Then
                ' The following is done to ensure the TIFF file can be read correctly by the Imaging for Windows
                ' Apparently Imaging for Windows must ignore the baseline tiff tag, PhotometricInterpretation.
                ' ImagXpress does not need the following as it correctly reads in the PhotometricInterpreation tag
                ' This will ensure that the TIFF file created has 0 as white and 1 as black
                
                ' We will set the ICAP_PIXELFLAVOR first, although for CCITT compression types
                ' ICAP_PIXELFLAVORCODES can override ICAP_PIXELFLAVOR
                TwainPRO.CapAdvanced = ICAP_PIXELFLAVOR
                If TwainPRO.CapSupported Then
                    TwainPRO.CapValueOut = TWPF_VANILLA
                    TwainPRO.SetCapOut
                End If
                
                If (nCompressType <> TWCP_PACKBITS) Then
                    TwainPRO.CapAdvanced = ICAP_PIXELFLAVORCODES
                    If TwainPRO.CapSupported Then
                        If (TwainPRO.CapValue <> TWPF_VANILLA) Then
                            TwainPRO.CapValueOut = TWPF_VANILLA
                            TwainPRO.SetCapOut ' Set it only if we have to
                        End If
                    End If
                End If
            End If
        End If
    
    Else
        lblStatus.Caption = "TwainPRO forced to Native Transfer Mode. Compression unavailable."
    End If
    
    TwainPRO.StartSession
    If Err.Number = 0 Then
        lblStatus.Caption = "Select and Scan"
    End If
    Exit Sub
    
ScanError:
    GetError
End Sub

' Update the status bar with the current error code and description
Sub GetError()
Dim RetPos As Long
Dim TwnProErr As Long

' Error from an ActiveX object or a user-defined error.
If (Err.Number > vbObjectError And Err.Number < vbObjectError + 65536) Then

    ' This definitively lets us know that that TwainPRO generated the error
    If InStr(1, Err.Source, "TwainPRO", vbTextCompare) <> 0 Then
        
        TwnProErr = Err.Number - vbObjectError
        
        ' ErrorCode TWERR_TWAIN indicates the Twain Source returned an error
        ' and we should check the ErrorCode and ErrorString properties
        ' for extended information
        If TwnProErr = TWERR_TWAIN Then
            lblStatus.Caption = "Data Source Error " & TwainPRO.ErrorCode & ": " & TwainPRO.ErrorString
        Else
            ' Remove carriage return from Description
            RetPos = InStr(1, Err.Description, Chr$(10))
            If RetPos > 0 Then
                lblStatus.Caption = TwnProErr & ": " & Left$(Err.Description, RetPos - 1) & "  " & Mid$(Err.Description, RetPos + 1)
            Else
                lblStatus.Caption = TwnProErr & ": " & Err.Description
            End If
        End If
        
    End If
    
End If

End Sub
Private Sub cmdSelect_Click()
    On Error GoTo selectError
  
    lblStatus.Caption = "Select Scanner"
    TwainPRO.SelectSource
    UpdateCompressionTypes
    cmdScan.Enabled = True
    Exit Sub
selectError:
  GetError
End Sub

Private Sub UpdateCompressionTypes()
On Error GoTo UpdateCompError
    Dim Index As Long
    
    lblStatus.Caption = "Querying Compression Capabilities"
    DoEvents
    
    TwainPRO.OpenSession
    TwainPRO.Capability = TWCAP_USECAPADVANCED
    TwainPRO.CapAdvanced = ICAP_COMPRESSION
    
    cmbCompression.Clear
    cmbCompression.AddItem "TWCP_NONE", 0
    cmbCompression.ItemData(0) = TWCP_NONE
    cmbCompression.ListIndex = 0

    
    If (TwainPRO.CapSupported) Then
        If (TwainPRO.CapType = TWCAP_ENUM) Then
            For Index = 0 To TwainPRO.CapNumItems - 1
                Select Case TwainPRO.CapItem(Index)
                
                    Case TWCP_NONE
                        ' Already handled
                    
                    Case TWCP_PACKBITS
                        cmbCompression.AddItem "TWCP_PACKBITS"
                        cmbCompression.ItemData(cmbCompression.NewIndex) = TWCP_PACKBITS
                      
                    Case TWCP_GROUP31D
                        cmbCompression.AddItem "TWCP_GROUP31D"
                        cmbCompression.ItemData(cmbCompression.NewIndex) = TWCP_GROUP31D
                    
                    Case TWCP_GROUP31DEOL
                        cmbCompression.AddItem "TWCP_GROUP31DEOL"
                        cmbCompression.ItemData(cmbCompression.NewIndex) = TWCP_GROUP31DEOL
                    
                    Case TWCP_GROUP32D
                        cmbCompression.AddItem "TWCP_GROUP32D"
                        cmbCompression.ItemData(cmbCompression.NewIndex) = TWCP_GROUP32D
                    
                    Case TWCP_GROUP4
                        cmbCompression.AddItem "TWCP_GROUP4"
                        cmbCompression.ItemData(cmbCompression.NewIndex) = TWCP_GROUP4
                    
                    Case TWCP_JPEG
                        cmbCompression.AddItem "TWCP_JPEG"
                        cmbCompression.ItemData(cmbCompression.NewIndex) = TWCP_JPEG
                                    
                End Select
                
            Next
        End If
    End If

    TwainPRO.CloseSession
    cmbCompression_Change
    lblStatus.Caption = "Select and Scan"
    Exit Sub
    
UpdateCompError:
    GetError
End Sub

Private Sub Form_Activate()

    MainForm.Show
    ' Let TwainPRO know that you wish to use Buffered Memory Mode
    TwainPRO.TransferMode = TWSX_MEMORY
End Sub

Private Sub Form_Unload(Cancel As Integer)
Unload Viewer
TwainPRO.CloseSession
End
End Sub

Private Sub hscrlJpegQual_Change()
    lblJpegQual.Caption = hscrlJpegQual.Value
End Sub

Private Sub TwainPRO_PostScan(Cancel As Boolean)
On Error GoTo psError
    Dim hImage As Long
  
    lblStatus.Caption = ""
    ' Need to check the Compression type to see if we are transferring a DIB
    ' or we are transferring compressed data.
    If TwainPRO.ICompression = TWCP_NONE Then
        hImage = TwainPRO.hDIB
        If (hImage <> 0) Then
            ImgXpr.hDIB = hImage
            If (chkUseViewer.Value = 1) Then
                Viewer.ShowEx ImgXpr.CopyDIB, True
            End If
        End If
    Else
        hImage = TwainPRO.hImage
        If (hImage <> 0) Then
            ImgXpr.LoadBuffer (hImage)
            If (chkUseViewer.Value = 1) Then
                Viewer.ShowEx hImage, False
            End If
            GlobalFree (hImage)
        End If
    End If
    
    DoEvents
    Cancel = False
    Exit Sub
    
psError:
    GetError
    Cancel = True
End Sub

Private Sub TwainPRO_PreScan(Cancel As Boolean)
    lblStatus.Caption = "Scanning..."
End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuAbout_Click()
    TwainPRO.About
End Sub


//-------------------------------------
// Copyright Pegasus Imaging Corp., 1999-2003
//   Header Created on 09/21/1999
//         
//         
// Modification History
// ---------------------
//                      
//-------------------------------------


class CTwainPROEventHandler : public IDispatch
{
public:

  long m_dwRef;
  DWORD m_dwCookie;

  CTwainPROEventHandler()
  {
     m_dwRef = 0;
  }

  ~CTwainPROEventHandler()
  {
  }

// this is the constructor we call so that way we can get a pointer
//   to our com instance

long __stdcall GetTypeInfoCount(unsigned int *)
{
  return 0;
}

long __stdcall GetTypeInfo(unsigned int,unsigned long,struct ITypeInfo ** )
{
  return 0;
}

long __stdcall GetIDsOfNames(const struct _GUID &,BSTR * ,unsigned int,unsigned long,long *)
{
  return 0;
}

long __stdcall GetIID()
{
  return 0;
}

HRESULT __stdcall Invoke (DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult, EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr)
{
// this is where all the events come -- just call directly into your methods exactly
//   as the import wrapper will show you
switch (dispidMember)
  {
  case 1:
     PreScan(pdispparams->rgvarg[0].pboolVal);
     break;

  case 2:
     PostScan(pdispparams->rgvarg[0].pboolVal);
     break;

  }
return S_OK;
}

HRESULT __stdcall QueryInterface(REFIID a_riid, void** a_ppv)
{
   if (a_ppv == NULL)
      return E_POINTER;
   *a_ppv = NULL;
   if (a_riid == __uuidof(ITwainPROEvents))
      *a_ppv = static_cast<CTwainPROEventHandler*>(this);
   else if (a_riid == IID_IUnknown)
      *a_ppv = static_cast<IUnknown *>(this);
   else
      return E_NOINTERFACE;
   AddRef();
   return S_OK;
}

ULONG __stdcall AddRef()
{
   return InterlockedIncrement(&m_dwRef);
}

ULONG __stdcall Release()
{
   if (InterlockedDecrement(&m_dwRef) == 0)
      return 0;
   return m_dwRef;
}

virtual HRESULT PreScan(VARIANT_BOOL *Cancel)
{
  return S_OK;
}

virtual HRESULT PostScan(VARIANT_BOOL *Cancel)
{
  return S_OK;
}

};

class CTwainPRO : CTwainPROEventHandler
{
  private:
    DWORD m_dwCookie;

    // Event Handlers
HRESULT (*pPreScan) (DWORD classPtr, DWORD objID, VARIANT_BOOL *Cancel);
HRESULT (*pPostScan) (DWORD classPtr, DWORD objID, VARIANT_BOOL *Cancel);

public:
CTwainPRO()
{
  CTwainPRO(0,0);
}

CTwainPRO(DWORD classPtr, DWORD objID)
{
   // Call the event connection function in the TwainPRO ATL COM Control
   typedef void (__cdecl* LPFN_IX_CONNECTEVENTS)(long, DWORD *, IUnknown *, long *);
   LPFN_IX_CONNECTEVENTS  lpfn_IX_ConnectEvents;

   HINSTANCE hDLL = LoadLibrary("twnpro4.dll");   // TwainPRO ATL COM control
   if (hDLL != NULL)
      {
      lpfn_IX_ConnectEvents = (LPFN_IX_CONNECTEVENTS)GetProcAddress(hDLL, "TP_ConnectEvents");
      if (lpfn_IX_ConnectEvents)
         {
         lpfn_IX_ConnectEvents((long)(&pTwainPRO), &m_dwCookie, this, &m_dwRef);
         }
      FreeLibrary(hDLL);
      }

   // Initialize our instance pointer and object id.
   m_classPtr = classPtr;
   m_objID    = objID;

   // NULL out the Event procedures

   pPreScan = NULL;
   pPostScan = NULL;

}

~CTwainPRO()
{
// Call the event disconnection function in the TwainPRO ATL COM Control
typedef void (__cdecl* LPFN_IX_DISCONNECTEVENTS)(long, DWORD);
LPFN_IX_DISCONNECTEVENTS  lpfn_IX_DisConnectEvents;

HINSTANCE hDLL = LoadLibrary("twnpro4.dll");   // TwainPRO ATL COM control
if (hDLL != NULL)
   {
   lpfn_IX_DisConnectEvents = (LPFN_IX_DISCONNECTEVENTS)GetProcAddress(hDLL, "TP_DisConnectEvents");
   if (lpfn_IX_DisConnectEvents)
      {
      lpfn_IX_DisConnectEvents((long)(pTwainPRO.GetInterfacePtr()), m_dwCookie);
      }
   FreeLibrary(hDLL);
   }
}

void SetPreScanEvent(HRESULT (*pMyPreScan) (DWORD classPtr, DWORD objID, VARIANT_BOOL *Cancel))
{
  pPreScan = pMyPreScan;
}

HRESULT PreScan(VARIANT_BOOL *Cancel)
{
   if (pPreScan)
      (*pPreScan)(m_classPtr, m_objID, Cancel);
   return S_OK;
}

void SetPostScanEvent(HRESULT (*pMyPostScan) (DWORD classPtr, DWORD objID, VARIANT_BOOL *Cancel))
{
  pPostScan = pMyPostScan;
}

HRESULT PostScan(VARIANT_BOOL *Cancel)
{
   if (pPostScan)
      (*pPostScan)(m_classPtr, m_objID, Cancel);
   return S_OK;
}

  DWORD m_objID;
  DWORD m_classPtr;
  ITwainPROPtr pTwainPRO;

};

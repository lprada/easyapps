VERSION 5.00
Object = "{F514BE21-8D87-4DB5-9B1B-4204C81D2AD4}#1.0#0"; "TwnPRO4.dll"
Begin VB.Form Main 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Advanced Capabilities Sample"
   ClientHeight    =   4725
   ClientLeft      =   4770
   ClientTop       =   3390
   ClientWidth     =   10350
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4725
   ScaleWidth      =   10350
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1230
      ItemData        =   "TwainBC.frx":0000
      Left            =   240
      List            =   "TwainBC.frx":0013
      TabIndex        =   9
      Top             =   240
      Width           =   9735
   End
   Begin VB.CommandButton cmdScan 
      Caption         =   "Scan"
      Height          =   495
      Left            =   5520
      TabIndex        =   7
      Top             =   3600
      Width           =   1575
   End
   Begin VB.CommandButton cmdSelSource 
      Caption         =   "Select Source"
      Height          =   495
      Left            =   2760
      TabIndex        =   6
      Top             =   3600
      Width           =   1575
   End
   Begin VB.ListBox listBCStrings 
      Height          =   840
      Left            =   5280
      TabIndex        =   5
      Top             =   2640
      Width           =   1935
   End
   Begin VB.ListBox listBCTypes 
      Height          =   840
      Left            =   2520
      TabIndex        =   0
      Top             =   2640
      Width           =   1935
   End
   Begin TWNPRO4LibCtl.TwainPRO TwainPRO 
      Left            =   360
      Top             =   3000
      _ExtentX        =   847
      _ExtentY        =   847
      ErrStr          =   "D747977B85B3CE33CE1003B846074273"
      ErrCode         =   1971597039
      ErrInfo         =   -399290663
      _cx             =   847
      _cy             =   847
      Caption         =   ""
      ForeColor       =   -2147483630
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ProductName     =   ""
      ProductFamily   =   ""
      Manufacturer    =   ""
      VersionInfo     =   ""
      MaxImages       =   -1
      ShowUI          =   -1  'True
      SaveJPEGLumFactor=   32
      SaveJPEGChromFactor=   36
      SaveJPEGSubSampling=   2
      SaveJPEGProgressive=   0   'False
      PICPassword     =   ""
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveTIFFCompression=   0
      SaveMultiPage   =   0   'False
      CaptionLeft     =   0
      CaptionTop      =   0
      CaptionWidth    =   0
      CaptionHeight   =   0
      ShadowText      =   -1  'True
      ClipCaption     =   0   'False
      HAlign          =   0
      VAlign          =   0
      EnableExtCaps   =   -1  'True
      CloseOnCancel   =   -1  'True
      TransferMode    =   0
      EvalMode        =   0
   End
   Begin VB.Label lblStatus 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2640
      TabIndex        =   8
      Top             =   4200
      Width           =   4695
   End
   Begin VB.Label Label4 
      Caption         =   "BarCode Strings"
      Height          =   255
      Left            =   5280
      TabIndex        =   4
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "BarCode Types"
      Height          =   255
      Left            =   2520
      TabIndex        =   3
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label lblNumBCs 
      Caption         =   "0"
      Height          =   255
      Left            =   4080
      TabIndex        =   2
      Top             =   1680
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Num Barcodes:"
      Height          =   255
      Left            =   2520
      TabIndex        =   1
      Top             =   1680
      Width           =   1215
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Dim gbBarcodeSupported As Boolean

Private Sub InitBarcode()
On Error GoTo InitBCError

    'Enable the barcode capability
    TwainPRO.Capability = TWCAP_USECAPADVANCED
    TwainPRO.CapAdvanced = ICAP_BARCODEDETECTIONENABLED
    If (TwainPRO.CapSupported) Then
        gbBarcodeSupported = True
        'Turn on barcode detection
        TwainPRO.CapValueOut = 1
        TwainPRO.SetCapOut
        
        TwainPRO.CapExtInfoCount = 4
        TwainPRO.Capability = TWCAP_USECAPADVANCEDFOREXTINFO

        TwainPRO.CapExtInfoIndex = 0
        TwainPRO.CapAdvanced = TWEI_BARCODECOUNT
    
        TwainPRO.CapExtInfoIndex = 1
        TwainPRO.CapAdvanced = TWEI_BARCODETYPE
    
        TwainPRO.CapExtInfoIndex = 2
        TwainPRO.CapAdvanced = TWEI_BARCODETEXTLENGTH
    
        TwainPRO.CapExtInfoIndex = 3
        TwainPRO.CapAdvanced = TWEI_BARCODETEXT
        
    Else
        gbBarcodeSupported = False
   End If
   
   Exit Sub
   
InitBCError:
    GetError
      
End Sub

Private Sub cmdScan_Click()
On Error GoTo ScanError
    lblStatus.Caption = ""
    
    TwainPRO.OpenSession
    InitBarcode
    TwainPRO.StartSession
    Exit Sub

ScanError:
    GetError
End Sub

Private Sub cmdSelSource_Click()
On Error GoTo SelSourceError
    TwainPRO.SelectSource
    Exit Sub
    
SelSourceError:
    GetError
End Sub


Private Sub Form_Load()
   
    TwainPRO.Debug = True
End Sub


Private Sub TwainPRO_PostScan(Cancel As Boolean)
On Error GoTo PostScanError
    Dim nIndex As Long
    
    If (gbBarcodeSupported) Then
        'Get the Extended Image Info
        TwainPRO.GetExtInfo
        'Now the ExtInfo structure has been filled
    
        ' Get the Barcode Count
        TwainPRO.CapExtInfoIndex = 0
        If (TwainPRO.CapSupported) Then
            lblNumBCs.Caption = TwainPRO.CapValue
        End If
    
        ' Get the Barcode Type
        TwainPRO.CapExtInfoIndex = 1
        If (TwainPRO.CapSupported) Then
            For nIndex = 0 To TwainPRO.CapNumItems - 1
                Select Case TwainPRO.CapItem(nIndex)
                    Case TWBT_3OF9:
                        listBCTypes.AddItem "TWBT_3OF9"
                    
                    Case TWBT_2OF5INTERLEAVED:
                        listBCTypes.AddItem "TWBT_2OF5INTERLEAVED"
                    
                    Case TWBT_2OF5NONINTERLEAVED:
                        listBCTypes.AddItem "TWBT_2OF5NONINTERLEAVED"
                    
                    Case TWBT_CODE93:
                        listBCTypes.AddItem "TWBT_CODE93"
                    
                    Case TWBT_CODE128:
                        listBCTypes.AddItem "TWBT_CODE128"
                    
                    Case TWBT_UCC128:
                        listBCTypes.AddItem "TWBT_UCC128"
                    
                    Case TWBT_CODABAR:
                        listBCTypes.AddItem "TWBT_CODABAR"
                    
                    Case TWBT_UPCA:
                        listBCTypes.AddItem "TWBT_UPCA"
                    
                    Case TWBT_UPCE:
                        listBCTypes.AddItem "TWBT_UPCE"
                End Select
            Next
        End If
  
  
        'Get the Barcode Strings
        TwainPRO.CapExtInfoIndex = 3
        If (TwainPRO.CapSupported) Then
            For nIndex = 0 To TwainPRO.CapNumItems - 1
                listBCStrings.AddItem TwainPRO.CapItemString(nIndex)
            Next
        End If
        
    End If
    Exit Sub
    
PostScanError:
    GetError
End Sub

' Update the status bar with the current error code and description
Sub GetError()
Dim RetPos As Long
Dim TwnProErr As Long

' Error from an ActiveX object or a user-defined error.
If (Err.Number > vbObjectError And Err.Number < vbObjectError + 65536) Then

    ' This definitively lets us know that that TwainPRO generated the error
    If InStr(1, Err.Source, "TwainPRO", vbTextCompare) <> 0 Then
        
        TwnProErr = Err.Number - vbObjectError
        
        ' ErrorCode TWERR_TWAIN indicates the Twain Source returned an error
        ' and we should check the ErrorCode and ErrorString properties
        ' for extended information
        If TwnProErr = TWERR_TWAIN Then
            lblStatus.Caption = "Data Source Error " & TwainPRO.ErrorCode & ": " & TwainPRO.ErrorString
        Else
            ' Remove carriage return from Description
            RetPos = InStr(1, Err.Description, Chr$(10))
            If RetPos > 0 Then
                lblStatus.Caption = TwnProErr & ": " & Left$(Err.Description, RetPos - 1) & "  " & Mid$(Err.Description, RetPos + 1)
            Else
                lblStatus.Caption = TwnProErr & ": " & Err.Description
            End If
        End If
        
    End If
    
End If


End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuAbout_Click()
    TwainPRO.About
End Sub



Attribute VB_Name = "TwnPro"
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Type OPENFILENAME
   lStructSize As Long
   hwndOwner As Long
   hInstance As Long
   lpstrFilter As String
   lpstrCustomFilter As String
   nMaxCustFilter As Long
   nFilterIndex As Long
   lpstrFile As String
   nMaxFile As Long
   lpstrFileTitle As String
   nMaxFileTitle As Long
   lpstrInitialDir As String
   lpstrTitle As String
   Flags As Long
   nFileOffset As Integer
   nFileExtension As Integer
   lpstrDefExt As String
   lCustData As Long
   lpfnHook As Long
   lpTemplateName As String
End Type

Public Const DIB_RGB_COLORS = 0 '  color table in RGBs
Public Const SRCCOPY = &HCC0020 ' (DWORD) dest = source
Public Const SRCINVERT = &H660046       ' (DWORD) dest = source XOR dest
Public Const BLACKONWHITE = 1
Public Const COLORONCOLOR = 3
Public Const WHITEONBLACK = 2
Public Const SM_CYCAPTION = 4
Public Const SM_CXDLGFRAME = 7
Public Const SM_CYDLGFRAME = 8

Declare Function GetSaveFileName Lib "comdlg32.dll" Alias "GetSaveFileNameA" (pOpenfilename As OPENFILENAME) As Long
Declare Function InvalidateRect Lib "user32" (ByVal hwnd As Long, ByVal lpRect As Long, ByVal bErase As Long) As Long
Declare Function GetSystemMetrics Lib "user32" (ByVal what As Integer) As Long
Declare Function StretchDIBits Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal dx As Long, ByVal dy As Long, ByVal SrcX As Long, ByVal SrcY As Long, ByVal wSrcWidth As Long, ByVal wSrcHeight As Long, ByVal lpBits As Long, ByVal lpBitsInfo As Long, ByVal wUsage As Long, ByVal dwRop As Long) As Long
Declare Function SetStretchBltMode Lib "gdi32" (ByVal hdc As Long, ByVal nStretchMode As Long) As Long
Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Declare Function GlobalSize Lib "kernel32" (ByVal hMem As Long) As Long
Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long

Function ItoB(Value As Integer) As Boolean
If (Value <> 0) Then
   ItoB = True
Else
   ItoB = False
End If
End Function

Function BtoI(Value As Boolean) As Integer
If (Value = True) Then
   BtoI = 1
Else
   BtoI = 0
End If
End Function

Function GetSaveName(Initial As String) As String
' Call the GetOpenFileName function directly so we don't
' have to use the Common Dialog custom control

Dim of As OPENFILENAME
Dim fn As String * 260
Dim rc As Long, Index As Long, Pos As Long

fn = Chr(0)
If (Len(Initial) > 0) Then
   Index = 0
   Do
      Pos = Index
      Index = InStr(Pos + 1, Initial, "\", vbTextCompare)
   Loop While (Index > 0)
   of.lpstrFile = Right(Initial, Len(Initial) - Pos)
   of.lpstrInitialDir = Left$(Initial, Pos - 1)
End If
of.lStructSize = Len(of)
of.hwndOwner = Form1.hwnd
of.lpstrFilter = "TIFF Files" & Chr(0) & "*.tif" & Chr(0)
of.lpstrDefExt = "tif"
of.lpstrFile = fn
of.nMaxFile = 260

rc = GetSaveFileName(of)

If (rc > 0) Then
   GetSaveName = of.lpstrFile
Else
   GetSaveName = Initial
End If

End Function



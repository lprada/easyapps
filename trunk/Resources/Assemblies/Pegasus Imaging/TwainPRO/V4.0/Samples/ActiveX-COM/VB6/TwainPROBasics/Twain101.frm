VERSION 5.00
Object = "{F514BE21-8D87-4DB5-9B1B-4204C81D2AD4}#1.0#0"; "TwnPRO4.dll"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Begin VB.Form Form1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "TwainPRO 4 Basics Sample"
   ClientHeight    =   6840
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   7395
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6840
   ScaleWidth      =   7395
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "Twain101.frx":0000
      Left            =   240
      List            =   "Twain101.frx":000A
      TabIndex        =   4
      Top             =   240
      Width           =   6855
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   3735
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   6588
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   553348326
      ErrInfo         =   -2037510602
      Persistence     =   -1  'True
      _cx             =   12091
      _cy             =   6588
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Get an Image with out Device Dialog"
      Height          =   615
      Left            =   4080
      TabIndex        =   1
      Top             =   5040
      Width           =   2895
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Get an Image with Device Dialog"
      Height          =   615
      Left            =   480
      TabIndex        =   0
      Top             =   5040
      Width           =   2775
   End
   Begin VB.Label lblstatus 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   1680
      TabIndex        =   2
      Top             =   5880
      Width           =   4095
   End
   Begin TWNPRO4LibCtl.TwainPRO TwainPRO1 
      Left            =   480
      Top             =   5880
      _ExtentX        =   847
      _ExtentY        =   847
      ErrStr          =   "D747977B85B3CE33CE1003B846074273"
      ErrCode         =   553348326
      ErrInfo         =   -1565469072
      _cx             =   847
      _cy             =   847
      Caption         =   ""
      ForeColor       =   -2147483630
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ProductName     =   ""
      ProductFamily   =   ""
      Manufacturer    =   ""
      VersionInfo     =   ""
      MaxImages       =   -1
      ShowUI          =   -1  'True
      SaveJPEGLumFactor=   32
      SaveJPEGChromFactor=   36
      SaveJPEGSubSampling=   2
      SaveJPEGProgressive=   0   'False
      PICPassword     =   ""
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveTIFFCompression=   0
      SaveMultiPage   =   0   'False
      CaptionLeft     =   0
      CaptionTop      =   0
      CaptionWidth    =   0
      CaptionHeight   =   0
      ShadowText      =   -1  'True
      ClipCaption     =   0   'False
      HAlign          =   0
      VAlign          =   0
      EnableExtCaps   =   -1  'True
      CloseOnCancel   =   -1  'True
      TransferMode    =   0
      EvalMode        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileQuit 
         Caption         =   "&Quit"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "&About"
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Private Sub Command1_Click()
  'Run with Driver Dialog Box
  On Error GoTo InitBCError
  TwainPRO1.SelectSource
  TwainPRO1.ShowUI = True
  TwainPRO1.StartSession
InitBCError:
 GetError

End Sub

Private Sub Command2_Click()
  'Run with Driver Dialog Box
    On Error GoTo InitBCError
  TwainPRO1.SelectSource
  TwainPRO1.ShowUI = False
  TwainPRO1.StartSession
InitBCError:
    GetError
End Sub

Private Sub TwainPRO1_PostScan(Cancel As Boolean)
   ImagXpress1.Picture = TwainPRO1.Picture
End Sub

Public Sub GetError()
    
Dim RetPos As Long
Dim TwnProErr As Long

' Error from an ActiveX object or a user-defined error.
If (Err.Number > vbObjectError And Err.Number < vbObjectError + 65536) Then

    ' This definitively lets us know that that TwainPRO generated the error
    If InStr(1, Err.Source, "TwainPRO", vbTextCompare) <> 0 Then
        
        TwnProErr = Err.Number - vbObjectError
        
        ' ErrorCode TWERR_TWAIN indicates the Twain Source returned an error
        ' and we should check the ErrorCode and ErrorString properties
        ' for extended information
        If TwnProErr = TWERR_TWAIN Then
            lblstatus.Caption = "Data Source Error " & TwainPRO1.ErrorCode & ": " & TwainPRO1.ErrorString
        Else
            ' Remove carriage return from Description
            RetPos = InStr(1, Err.Description, Chr$(10))
            If RetPos > 0 Then
                lblstatus.Caption = TwnProErr & ": " & Left$(Err.Description, RetPos - 1) & "  " & Mid$(Err.Description, RetPos + 1)
            Else
                lblstatus.Caption = TwnProErr & ": " & Err.Description
            End If
        End If
        
    End If
    
End If

End Sub

Private Sub mnuFileQuit_Click()
    End
End Sub

Private Sub mnuAbout_Click()
    TwainPRO1.About
End Sub

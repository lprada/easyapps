// TwnPRODlg.h : header file
//

#if !defined(AFX_TWNPRODLG_H__B3B17448_3620_11D3_BF3E_B8EE28000000__INCLUDED_)
#define AFX_TWNPRODLG_H__B3B17448_3620_11D3_BF3E_B8EE28000000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

using namespace TWNPRO4Lib;

#include "..\Include\TwainPRO4Events.h"


/////////////////////////////////////////////////////////////////////////////
// CTwnPRODlg dialog

class CTwnPRODlg : public CDialog
{
// Construction
public:
	CTwnPRODlg(CWnd* pParent = NULL);	// standard constructor

  // TwainPRO Event Handlers
  static HRESULT PreScan (DWORD classPtr, DWORD objID, VARIANT_BOOL Cancel);
  static HRESULT PostScan(DWORD classPtr, DWORD objID,  VARIANT_BOOL *Cancel);

  HRESULT HandleError( _com_error e );

   // this will be the pointer to our COM instance 
  CTwainPRO *ppCTwainPRO;
  ITwainPROPtr pTwainPRO;

// Dialog Data
	//{{AFX_DATA(CTwnPRODlg)
	enum { IDD = IDD_TWNPRO_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTwnPRODlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTwnPRODlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelectSource();
	afx_msg void OnDestroy();
	afx_msg void OnSelectFilename();
	afx_msg void OnStartSession();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TWNPRODLG_H__B3B17448_3620_11D3_BF3E_B8EE28000000__INCLUDED_)

// TPViewDlg.h : header file
//

#if !defined(AFX_TPVIEWDLG_H__5124B3C7_70F3_11D3_A8FB_DA889974722E__INCLUDED_)
#define AFX_TPVIEWDLG_H__5124B3C7_70F3_11D3_A8FB_DA889974722E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

using namespace TWNPRO4Lib;
using namespace PegasusImagingActiveXImagXpress8;
#include "..\Include\TwainPRO4Events.h"
#include "..\Include\ImagXpress8Events.h"

/////////////////////////////////////////////////////////////////////////////
// CTPViewDlg dialog

class CTPViewDlg : public CDialog
{

  // TwainPRO Event Handlers
  static HRESULT PreScan (DWORD classPtr, DWORD objID, VARIANT_BOOL Cancel);
  static HRESULT PostScan(DWORD classPtr, DWORD objID,  VARIANT_BOOL *Cancel);
  
  HRESULT HandleError( _com_error e );

// Construction
public:
	CTPViewDlg(CWnd* pParent = NULL);	// standard constructor

  // this will be the pointers to our COM instances 
  CTwainPRO *ppCTwainPRO;
  ITwainPROPtr pTwainPRO;
  CImagXpress *ppCImagXpress;
  IImagXpressPtr pImagXpress;

  // Dialog Data
	//{{AFX_DATA(CTPViewDlg)
	enum { IDD = IDD_TPVIEW_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTPViewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTPViewDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnBtnAcquire();
	afx_msg void OnBtnSelect();
	afx_msg void OnFileQuit();
	afx_msg void OnAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TPVIEWDLG_H__5124B3C7_70F3_11D3_A8FB_DA889974722E__INCLUDED_)

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.TwainPro4;

namespace TwainProDemoCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonOpenSession;
		private System.Windows.Forms.Button buttonStartSession;
		private System.Windows.Forms.Button buttonCloseSession;
		private System.Windows.Forms.Button buttonShowCaps;
		private System.Windows.Forms.Button buttonShowLayout;
		private System.Windows.Forms.Button buttonShowCaption;
		private System.Windows.Forms.Button buttonSelectSource;
		private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.Panel panelMain;
		private System.Windows.Forms.Panel panelLayout;
		private System.Windows.Forms.Panel panelCaps;
		private System.Windows.Forms.Panel panelCaption;
		private System.Windows.Forms.GroupBox groupBoxCaption;
		private System.Windows.Forms.CheckBox checkBoxClipCaption;
		private System.Windows.Forms.CheckBox checkBoxShadowText;
		private System.Windows.Forms.Label labelCapLeft;
		private System.Windows.Forms.Label labelCapTop;
		private System.Windows.Forms.Label labelCapWidth;
		private System.Windows.Forms.Label labelCapHeight;
		private System.Windows.Forms.Label labelCaption;
		private System.Windows.Forms.Label labelHAlign;
		private System.Windows.Forms.Label labelVAlign;
		private System.Windows.Forms.TextBox textBoxCapLeft;
		private System.Windows.Forms.TextBox textBoxCapTop;
		private System.Windows.Forms.TextBox textBoxCapWidth;
		private System.Windows.Forms.TextBox textBoxCapHeight;
		private System.Windows.Forms.TextBox textBoxCaption;
		private System.Windows.Forms.ComboBox comboBoxHAlign;
		private System.Windows.Forms.ComboBox comboBoxVAlign;
		private System.Windows.Forms.GroupBox groupBoxCaps;
		private System.Windows.Forms.ComboBox comboBoxCaps;
		private System.Windows.Forms.Label labelDefault;
		private System.Windows.Forms.ListBox listBoxCaps;
		private System.Windows.Forms.Label labelCurrent;
		private System.Windows.Forms.TextBox textBoxCurrent;
		private System.Windows.Forms.Button buttonUpdate;
		private System.Windows.Forms.GroupBox groupBoxLayout;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button buttonLayoutUpdate;
		private System.Windows.Forms.Panel panelStart;
		private System.Windows.Forms.CheckBox checkBoxShowUI;
		private System.Windows.Forms.CheckBox checkBoxSaveAsBMP;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox textBoxIL;
		private System.Windows.Forms.TextBox textBoxIT;
		private System.Windows.Forms.TextBox textBoxIR;
		private System.Windows.Forms.TextBox textBoxIB;
		private System.Windows.Forms.Label labelMin;
		private System.Windows.Forms.Label labelMax;
		private PegasusImaging.WinForms.TwainPro4.PICTwainPro picTwainPro1;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		//private PegasusImaging.WinForms.TwainPro4.PICTwainPro picTwainPro1;

		private System.Int32 _nImageCount;

		public FormMain()
		{
			// Before the call to InitializeComponent, we have to unlock
			UnlockPICTwainPro.PS_Unlock (123456789, 123456789, 123456789, 123456789);

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			// This call is required!
			picTwainPro1.hParentWnd = this.Handle.ToInt32 ();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
				picTwainPro1.Dispose ();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonOpenSession = new System.Windows.Forms.Button();
			this.buttonStartSession = new System.Windows.Forms.Button();
			this.buttonCloseSession = new System.Windows.Forms.Button();
			this.buttonShowCaps = new System.Windows.Forms.Button();
			this.buttonShowLayout = new System.Windows.Forms.Button();
			this.buttonShowCaption = new System.Windows.Forms.Button();
			this.buttonSelectSource = new System.Windows.Forms.Button();
			this.labelStatus = new System.Windows.Forms.Label();
			this.panelMain = new System.Windows.Forms.Panel();
			this.panelCaption = new System.Windows.Forms.Panel();
			this.groupBoxCaption = new System.Windows.Forms.GroupBox();
			this.comboBoxVAlign = new System.Windows.Forms.ComboBox();
			this.comboBoxHAlign = new System.Windows.Forms.ComboBox();
			this.textBoxCaption = new System.Windows.Forms.TextBox();
			this.textBoxCapHeight = new System.Windows.Forms.TextBox();
			this.textBoxCapWidth = new System.Windows.Forms.TextBox();
			this.textBoxCapTop = new System.Windows.Forms.TextBox();
			this.textBoxCapLeft = new System.Windows.Forms.TextBox();
			this.labelVAlign = new System.Windows.Forms.Label();
			this.labelHAlign = new System.Windows.Forms.Label();
			this.labelCaption = new System.Windows.Forms.Label();
			this.labelCapHeight = new System.Windows.Forms.Label();
			this.labelCapWidth = new System.Windows.Forms.Label();
			this.labelCapTop = new System.Windows.Forms.Label();
			this.labelCapLeft = new System.Windows.Forms.Label();
			this.checkBoxShadowText = new System.Windows.Forms.CheckBox();
			this.checkBoxClipCaption = new System.Windows.Forms.CheckBox();
			this.panelLayout = new System.Windows.Forms.Panel();
			this.groupBoxLayout = new System.Windows.Forms.GroupBox();
			this.textBoxIB = new System.Windows.Forms.TextBox();
			this.textBoxIR = new System.Windows.Forms.TextBox();
			this.textBoxIT = new System.Windows.Forms.TextBox();
			this.textBoxIL = new System.Windows.Forms.TextBox();
			this.buttonLayoutUpdate = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.panelCaps = new System.Windows.Forms.Panel();
			this.groupBoxCaps = new System.Windows.Forms.GroupBox();
			this.labelMax = new System.Windows.Forms.Label();
			this.labelMin = new System.Windows.Forms.Label();
			this.buttonUpdate = new System.Windows.Forms.Button();
			this.textBoxCurrent = new System.Windows.Forms.TextBox();
			this.labelCurrent = new System.Windows.Forms.Label();
			this.listBoxCaps = new System.Windows.Forms.ListBox();
			this.labelDefault = new System.Windows.Forms.Label();
			this.comboBoxCaps = new System.Windows.Forms.ComboBox();
			this.panelStart = new System.Windows.Forms.Panel();
			this.checkBoxSaveAsBMP = new System.Windows.Forms.CheckBox();
			this.checkBoxShowUI = new System.Windows.Forms.CheckBox();
			this.picTwainPro1 = new PegasusImaging.WinForms.TwainPro4.PICTwainPro();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.panelMain.SuspendLayout();
			this.panelCaption.SuspendLayout();
			this.groupBoxCaption.SuspendLayout();
			this.panelLayout.SuspendLayout();
			this.groupBoxLayout.SuspendLayout();
			this.panelCaps.SuspendLayout();
			this.groupBoxCaps.SuspendLayout();
			this.panelStart.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonOpenSession
			// 
			this.buttonOpenSession.Location = new System.Drawing.Point(8, 152);
			this.buttonOpenSession.Name = "buttonOpenSession";
			this.buttonOpenSession.Size = new System.Drawing.Size(128, 23);
			this.buttonOpenSession.TabIndex = 0;
			this.buttonOpenSession.Text = "Open Session";
			this.buttonOpenSession.Click += new System.EventHandler(this.buttonOpenSession_Click);
			// 
			// buttonStartSession
			// 
			this.buttonStartSession.Location = new System.Drawing.Point(8, 184);
			this.buttonStartSession.Name = "buttonStartSession";
			this.buttonStartSession.Size = new System.Drawing.Size(128, 23);
			this.buttonStartSession.TabIndex = 1;
			this.buttonStartSession.Text = "Start Session\\Acquire";
			this.buttonStartSession.Click += new System.EventHandler(this.buttonStartSession_Click);
			// 
			// buttonCloseSession
			// 
			this.buttonCloseSession.Location = new System.Drawing.Point(8, 216);
			this.buttonCloseSession.Name = "buttonCloseSession";
			this.buttonCloseSession.Size = new System.Drawing.Size(128, 23);
			this.buttonCloseSession.TabIndex = 2;
			this.buttonCloseSession.Text = "Close Session";
			this.buttonCloseSession.Click += new System.EventHandler(this.buttonCloseSession_Click);
			// 
			// buttonShowCaps
			// 
			this.buttonShowCaps.Enabled = false;
			this.buttonShowCaps.Location = new System.Drawing.Point(8, 248);
			this.buttonShowCaps.Name = "buttonShowCaps";
			this.buttonShowCaps.Size = new System.Drawing.Size(128, 23);
			this.buttonShowCaps.TabIndex = 3;
			this.buttonShowCaps.Text = "Show Capabilities";
			this.buttonShowCaps.Click += new System.EventHandler(this.buttonShowCaps_Click);
			// 
			// buttonShowLayout
			// 
			this.buttonShowLayout.Enabled = false;
			this.buttonShowLayout.Location = new System.Drawing.Point(8, 280);
			this.buttonShowLayout.Name = "buttonShowLayout";
			this.buttonShowLayout.Size = new System.Drawing.Size(128, 23);
			this.buttonShowLayout.TabIndex = 4;
			this.buttonShowLayout.Text = "Show Layout";
			this.buttonShowLayout.Click += new System.EventHandler(this.buttonShowLayout_Click);
			// 
			// buttonShowCaption
			// 
			this.buttonShowCaption.Location = new System.Drawing.Point(8, 312);
			this.buttonShowCaption.Name = "buttonShowCaption";
			this.buttonShowCaption.Size = new System.Drawing.Size(128, 23);
			this.buttonShowCaption.TabIndex = 5;
			this.buttonShowCaption.Text = "Show Caption";
			this.buttonShowCaption.Click += new System.EventHandler(this.buttonShowCaption_Click);
			// 
			// buttonSelectSource
			// 
			this.buttonSelectSource.Location = new System.Drawing.Point(8, 120);
			this.buttonSelectSource.Name = "buttonSelectSource";
			this.buttonSelectSource.Size = new System.Drawing.Size(128, 23);
			this.buttonSelectSource.TabIndex = 6;
			this.buttonSelectSource.Text = "Select Source";
			this.buttonSelectSource.Click += new System.EventHandler(this.buttonSelectSource_Click);
			// 
			// labelStatus
			// 
			this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.labelStatus.Location = new System.Drawing.Point(8, 344);
			this.labelStatus.Name = "labelStatus";
			this.labelStatus.Size = new System.Drawing.Size(456, 16);
			this.labelStatus.TabIndex = 8;
			// 
			// panelMain
			// 
			this.panelMain.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.panelCaption,
																					this.panelLayout,
																					this.panelCaps,
																					this.panelStart});
			this.panelMain.Location = new System.Drawing.Point(144, 112);
			this.panelMain.Name = "panelMain";
			this.panelMain.Size = new System.Drawing.Size(312, 216);
			this.panelMain.TabIndex = 9;
			// 
			// panelCaption
			// 
			this.panelCaption.BackColor = System.Drawing.SystemColors.Control;
			this.panelCaption.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.groupBoxCaption});
			this.panelCaption.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelCaption.Name = "panelCaption";
			this.panelCaption.Size = new System.Drawing.Size(312, 216);
			this.panelCaption.TabIndex = 2;
			this.panelCaption.Visible = false;
			// 
			// groupBoxCaption
			// 
			this.groupBoxCaption.Controls.AddRange(new System.Windows.Forms.Control[] {
																						  this.comboBoxVAlign,
																						  this.comboBoxHAlign,
																						  this.textBoxCaption,
																						  this.textBoxCapHeight,
																						  this.textBoxCapWidth,
																						  this.textBoxCapTop,
																						  this.textBoxCapLeft,
																						  this.labelVAlign,
																						  this.labelHAlign,
																						  this.labelCaption,
																						  this.labelCapHeight,
																						  this.labelCapWidth,
																						  this.labelCapTop,
																						  this.labelCapLeft,
																						  this.checkBoxShadowText,
																						  this.checkBoxClipCaption});
			this.groupBoxCaption.Name = "groupBoxCaption";
			this.groupBoxCaption.Size = new System.Drawing.Size(312, 216);
			this.groupBoxCaption.TabIndex = 0;
			this.groupBoxCaption.TabStop = false;
			this.groupBoxCaption.Text = "Caption";
			// 
			// comboBoxVAlign
			// 
			this.comboBoxVAlign.Location = new System.Drawing.Point(184, 104);
			this.comboBoxVAlign.Name = "comboBoxVAlign";
			this.comboBoxVAlign.Size = new System.Drawing.Size(120, 21);
			this.comboBoxVAlign.TabIndex = 15;
			this.comboBoxVAlign.Text = "comboBox2";
			// 
			// comboBoxHAlign
			// 
			this.comboBoxHAlign.Location = new System.Drawing.Point(184, 56);
			this.comboBoxHAlign.Name = "comboBoxHAlign";
			this.comboBoxHAlign.Size = new System.Drawing.Size(120, 21);
			this.comboBoxHAlign.TabIndex = 14;
			this.comboBoxHAlign.Text = "comboBox1";
			// 
			// textBoxCaption
			// 
			this.textBoxCaption.Location = new System.Drawing.Point(112, 134);
			this.textBoxCaption.Name = "textBoxCaption";
			this.textBoxCaption.Size = new System.Drawing.Size(192, 20);
			this.textBoxCaption.TabIndex = 13;
			this.textBoxCaption.Text = "Pegasus Imaging Corp.";
			// 
			// textBoxCapHeight
			// 
			this.textBoxCapHeight.Location = new System.Drawing.Point(112, 110);
			this.textBoxCapHeight.Name = "textBoxCapHeight";
			this.textBoxCapHeight.Size = new System.Drawing.Size(48, 20);
			this.textBoxCapHeight.TabIndex = 12;
			this.textBoxCapHeight.Text = "0";
			this.textBoxCapHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxCapWidth
			// 
			this.textBoxCapWidth.Location = new System.Drawing.Point(112, 86);
			this.textBoxCapWidth.Name = "textBoxCapWidth";
			this.textBoxCapWidth.Size = new System.Drawing.Size(48, 20);
			this.textBoxCapWidth.TabIndex = 11;
			this.textBoxCapWidth.Text = "0";
			this.textBoxCapWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxCapTop
			// 
			this.textBoxCapTop.Location = new System.Drawing.Point(112, 62);
			this.textBoxCapTop.Name = "textBoxCapTop";
			this.textBoxCapTop.Size = new System.Drawing.Size(48, 20);
			this.textBoxCapTop.TabIndex = 10;
			this.textBoxCapTop.Text = "0";
			this.textBoxCapTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// textBoxCapLeft
			// 
			this.textBoxCapLeft.Location = new System.Drawing.Point(112, 38);
			this.textBoxCapLeft.Name = "textBoxCapLeft";
			this.textBoxCapLeft.Size = new System.Drawing.Size(48, 20);
			this.textBoxCapLeft.TabIndex = 9;
			this.textBoxCapLeft.Text = "0";
			this.textBoxCapLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// labelVAlign
			// 
			this.labelVAlign.Location = new System.Drawing.Point(184, 88);
			this.labelVAlign.Name = "labelVAlign";
			this.labelVAlign.Size = new System.Drawing.Size(40, 16);
			this.labelVAlign.TabIndex = 8;
			this.labelVAlign.Text = "VAlign";
			// 
			// labelHAlign
			// 
			this.labelHAlign.Location = new System.Drawing.Point(184, 40);
			this.labelHAlign.Name = "labelHAlign";
			this.labelHAlign.Size = new System.Drawing.Size(40, 16);
			this.labelHAlign.TabIndex = 7;
			this.labelHAlign.Text = "HAlign";
			// 
			// labelCaption
			// 
			this.labelCaption.Location = new System.Drawing.Point(16, 136);
			this.labelCaption.Name = "labelCaption";
			this.labelCaption.Size = new System.Drawing.Size(48, 16);
			this.labelCaption.TabIndex = 6;
			this.labelCaption.Text = "Caption:";
			// 
			// labelCapHeight
			// 
			this.labelCapHeight.Location = new System.Drawing.Point(16, 112);
			this.labelCapHeight.Name = "labelCapHeight";
			this.labelCapHeight.Size = new System.Drawing.Size(88, 16);
			this.labelCapHeight.TabIndex = 5;
			this.labelCapHeight.Text = "Caption Height:";
			// 
			// labelCapWidth
			// 
			this.labelCapWidth.Location = new System.Drawing.Point(16, 88);
			this.labelCapWidth.Name = "labelCapWidth";
			this.labelCapWidth.Size = new System.Drawing.Size(80, 16);
			this.labelCapWidth.TabIndex = 4;
			this.labelCapWidth.Text = "Caption Width:";
			// 
			// labelCapTop
			// 
			this.labelCapTop.Location = new System.Drawing.Point(16, 64);
			this.labelCapTop.Name = "labelCapTop";
			this.labelCapTop.Size = new System.Drawing.Size(72, 16);
			this.labelCapTop.TabIndex = 3;
			this.labelCapTop.Text = "Caption Top:";
			// 
			// labelCapLeft
			// 
			this.labelCapLeft.Location = new System.Drawing.Point(16, 40);
			this.labelCapLeft.Name = "labelCapLeft";
			this.labelCapLeft.Size = new System.Drawing.Size(72, 16);
			this.labelCapLeft.TabIndex = 2;
			this.labelCapLeft.Text = "Caption Left:";
			// 
			// checkBoxShadowText
			// 
			this.checkBoxShadowText.Location = new System.Drawing.Point(128, 16);
			this.checkBoxShadowText.Name = "checkBoxShadowText";
			this.checkBoxShadowText.Size = new System.Drawing.Size(96, 16);
			this.checkBoxShadowText.TabIndex = 1;
			this.checkBoxShadowText.Text = "Shadow Text";
			// 
			// checkBoxClipCaption
			// 
			this.checkBoxClipCaption.Location = new System.Drawing.Point(16, 16);
			this.checkBoxClipCaption.Name = "checkBoxClipCaption";
			this.checkBoxClipCaption.Size = new System.Drawing.Size(88, 16);
			this.checkBoxClipCaption.TabIndex = 0;
			this.checkBoxClipCaption.Text = "Clip Caption";
			// 
			// panelLayout
			// 
			this.panelLayout.BackColor = System.Drawing.SystemColors.Control;
			this.panelLayout.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this.groupBoxLayout});
			this.panelLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelLayout.Name = "panelLayout";
			this.panelLayout.Size = new System.Drawing.Size(312, 216);
			this.panelLayout.TabIndex = 0;
			this.panelLayout.Visible = false;
			// 
			// groupBoxLayout
			// 
			this.groupBoxLayout.Controls.AddRange(new System.Windows.Forms.Control[] {
																						 this.textBoxIB,
																						 this.textBoxIR,
																						 this.textBoxIT,
																						 this.textBoxIL,
																						 this.buttonLayoutUpdate,
																						 this.label4,
																						 this.label3,
																						 this.label2,
																						 this.label1});
			this.groupBoxLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxLayout.Name = "groupBoxLayout";
			this.groupBoxLayout.Size = new System.Drawing.Size(312, 216);
			this.groupBoxLayout.TabIndex = 0;
			this.groupBoxLayout.TabStop = false;
			this.groupBoxLayout.Text = "Layout";
			// 
			// textBoxIB
			// 
			this.textBoxIB.Location = new System.Drawing.Point(156, 126);
			this.textBoxIB.Name = "textBoxIB";
			this.textBoxIB.Size = new System.Drawing.Size(88, 20);
			this.textBoxIB.TabIndex = 8;
			this.textBoxIB.Text = "";
			// 
			// textBoxIR
			// 
			this.textBoxIR.Location = new System.Drawing.Point(156, 94);
			this.textBoxIR.Name = "textBoxIR";
			this.textBoxIR.Size = new System.Drawing.Size(88, 20);
			this.textBoxIR.TabIndex = 7;
			this.textBoxIR.Text = "";
			// 
			// textBoxIT
			// 
			this.textBoxIT.Location = new System.Drawing.Point(156, 62);
			this.textBoxIT.Name = "textBoxIT";
			this.textBoxIT.Size = new System.Drawing.Size(88, 20);
			this.textBoxIT.TabIndex = 6;
			this.textBoxIT.Text = "";
			// 
			// textBoxIL
			// 
			this.textBoxIL.Location = new System.Drawing.Point(156, 30);
			this.textBoxIL.Name = "textBoxIL";
			this.textBoxIL.Size = new System.Drawing.Size(88, 20);
			this.textBoxIL.TabIndex = 5;
			this.textBoxIL.Text = "";
			// 
			// buttonLayoutUpdate
			// 
			this.buttonLayoutUpdate.Location = new System.Drawing.Point(112, 160);
			this.buttonLayoutUpdate.Name = "buttonLayoutUpdate";
			this.buttonLayoutUpdate.Size = new System.Drawing.Size(96, 24);
			this.buttonLayoutUpdate.TabIndex = 4;
			this.buttonLayoutUpdate.Text = "Update";
			this.buttonLayoutUpdate.Click += new System.EventHandler(this.buttonLayoutUpdate_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(68, 128);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 16);
			this.label4.TabIndex = 3;
			this.label4.Text = "Image Bottom:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(68, 96);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "Image Right:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(68, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Image Top:";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(68, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Image Left:";
			// 
			// panelCaps
			// 
			this.panelCaps.BackColor = System.Drawing.SystemColors.Control;
			this.panelCaps.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.groupBoxCaps});
			this.panelCaps.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelCaps.Name = "panelCaps";
			this.panelCaps.Size = new System.Drawing.Size(312, 216);
			this.panelCaps.TabIndex = 1;
			this.panelCaps.Visible = false;
			// 
			// groupBoxCaps
			// 
			this.groupBoxCaps.Controls.AddRange(new System.Windows.Forms.Control[] {
																					   this.labelMax,
																					   this.labelMin,
																					   this.buttonUpdate,
																					   this.textBoxCurrent,
																					   this.labelCurrent,
																					   this.listBoxCaps,
																					   this.labelDefault,
																					   this.comboBoxCaps});
			this.groupBoxCaps.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBoxCaps.Name = "groupBoxCaps";
			this.groupBoxCaps.Size = new System.Drawing.Size(312, 216);
			this.groupBoxCaps.TabIndex = 0;
			this.groupBoxCaps.TabStop = false;
			this.groupBoxCaps.Text = "Capabilities";
			// 
			// labelMax
			// 
			this.labelMax.Location = new System.Drawing.Point(32, 104);
			this.labelMax.Name = "labelMax";
			this.labelMax.Size = new System.Drawing.Size(256, 16);
			this.labelMax.TabIndex = 7;
			this.labelMax.Text = "labelMax";
			// 
			// labelMin
			// 
			this.labelMin.Location = new System.Drawing.Point(32, 80);
			this.labelMin.Name = "labelMin";
			this.labelMin.Size = new System.Drawing.Size(256, 16);
			this.labelMin.TabIndex = 6;
			this.labelMin.Text = "labelMin";
			// 
			// buttonUpdate
			// 
			this.buttonUpdate.Location = new System.Drawing.Point(208, 168);
			this.buttonUpdate.Name = "buttonUpdate";
			this.buttonUpdate.Size = new System.Drawing.Size(80, 24);
			this.buttonUpdate.TabIndex = 5;
			this.buttonUpdate.Text = "Update";
			this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
			// 
			// textBoxCurrent
			// 
			this.textBoxCurrent.Location = new System.Drawing.Point(96, 170);
			this.textBoxCurrent.Name = "textBoxCurrent";
			this.textBoxCurrent.Size = new System.Drawing.Size(96, 20);
			this.textBoxCurrent.TabIndex = 4;
			this.textBoxCurrent.Text = "";
			// 
			// labelCurrent
			// 
			this.labelCurrent.Location = new System.Drawing.Point(40, 172);
			this.labelCurrent.Name = "labelCurrent";
			this.labelCurrent.Size = new System.Drawing.Size(48, 16);
			this.labelCurrent.TabIndex = 3;
			this.labelCurrent.Text = "Current:";
			// 
			// listBoxCaps
			// 
			this.listBoxCaps.Location = new System.Drawing.Point(32, 56);
			this.listBoxCaps.Name = "listBoxCaps";
			this.listBoxCaps.Size = new System.Drawing.Size(256, 95);
			this.listBoxCaps.TabIndex = 2;
			// 
			// labelDefault
			// 
			this.labelDefault.Location = new System.Drawing.Point(32, 56);
			this.labelDefault.Name = "labelDefault";
			this.labelDefault.Size = new System.Drawing.Size(256, 16);
			this.labelDefault.TabIndex = 1;
			this.labelDefault.Text = "labelDefault";
			// 
			// comboBoxCaps
			// 
			this.comboBoxCaps.Items.AddRange(new object[] {
															  "AutoBright",
															  "Brightness",
															  "Contrast",
															  "Gamma",
															  "Physical Width",
															  "Physical Height",
															  "Pixel Type",
															  "Rotation",
															  "Units",
															  "X Native Resolution",
															  "Y Native Resolution",
															  "X Resolution",
															  "Y Resolution",
															  "X Scaling",
															  "Y Scaling",
															  "Device Online",
															  "Indicators",
															  "Lamp State",
															  "UIControllable",
															  "Transfer Count",
															  "Auto Feed",
															  "Auto Scan",
															  "Clear Page",
															  "Duplex",
															  "Duplex Enabled",
															  "Feeder Enabled",
															  "Feeder Loaded",
															  "Feed Page",
															  "Paper Detectable"});
			this.comboBoxCaps.Location = new System.Drawing.Point(32, 24);
			this.comboBoxCaps.Name = "comboBoxCaps";
			this.comboBoxCaps.Size = new System.Drawing.Size(256, 21);
			this.comboBoxCaps.TabIndex = 0;
			this.comboBoxCaps.Text = "comboBox1";
			this.comboBoxCaps.SelectedIndexChanged += new System.EventHandler(this.comboBoxCaps_SelectedIndexChanged);
			// 
			// panelStart
			// 
			this.panelStart.Controls.AddRange(new System.Windows.Forms.Control[] {
																					 this.checkBoxSaveAsBMP,
																					 this.checkBoxShowUI});
			this.panelStart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelStart.Name = "panelStart";
			this.panelStart.Size = new System.Drawing.Size(312, 216);
			this.panelStart.TabIndex = 3;
			// 
			// checkBoxSaveAsBMP
			// 
			this.checkBoxSaveAsBMP.Location = new System.Drawing.Point(8, 40);
			this.checkBoxSaveAsBMP.Name = "checkBoxSaveAsBMP";
			this.checkBoxSaveAsBMP.Size = new System.Drawing.Size(128, 16);
			this.checkBoxSaveAsBMP.TabIndex = 1;
			this.checkBoxSaveAsBMP.Text = "Save as imag-n.bmp";
			// 
			// checkBoxShowUI
			// 
			this.checkBoxShowUI.Location = new System.Drawing.Point(8, 8);
			this.checkBoxShowUI.Name = "checkBoxShowUI";
			this.checkBoxShowUI.Size = new System.Drawing.Size(96, 16);
			this.checkBoxShowUI.TabIndex = 0;
			this.checkBoxShowUI.Text = "Show UI";
			this.checkBoxShowUI.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			// 
			// picTwainPro1
			// 
			this.picTwainPro1.Caption = "";
			this.picTwainPro1.CaptionHeight = 0;
			this.picTwainPro1.CaptionLeft = 0;
			this.picTwainPro1.CaptionTop = 0;
			this.picTwainPro1.CaptionWidth = 0;
			this.picTwainPro1.ClipCaption = false;
			this.picTwainPro1.CloseOnCancel = true;
			this.picTwainPro1.Debug = false;
			this.picTwainPro1.EnableExtCaps = true;
			this.picTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic;
			this.picTwainPro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picTwainPro1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(69)), ((System.Byte)(61)), ((System.Byte)(85)));
			this.picTwainPro1.FTPPassword = "";
			this.picTwainPro1.FTPUserName = "";
			this.picTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft;
			this.picTwainPro1.Manufacturer = "";
			this.picTwainPro1.MaxImages = -1;
			this.picTwainPro1.PICPassword = "";
			this.picTwainPro1.ProductFamily = "";
			this.picTwainPro1.ProductName = "";
			this.picTwainPro1.ProxyServer = "";
			this.picTwainPro1.SaveJPEGChromFactor = 36;
			this.picTwainPro1.SaveJPEGLumFactor = 32;
			this.picTwainPro1.SaveJPEGProgressive = false;
			this.picTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411;
			this.picTwainPro1.SaveMultiPage = false;
			this.picTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed;
			this.picTwainPro1.ShadowText = true;
			this.picTwainPro1.ShowUI = true;
			this.picTwainPro1.SN = "PEXHJ700AA-PEG0Q000195";
			this.picTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE;
			this.picTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop;
			this.picTwainPro1.VersionInfo = "";
			this.picTwainPro1.PostScandotNet += new PegasusImaging.WinForms.TwainPro4.PostScanDelegate(this.picTwainPro1_PostScandotNet);
			// 
			// lstInfo
			// 
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Selecting the device with or without the scanner\'s UI via the ShowUI property.",
														 "2)Querying and setting the various TWAIN capabilities.",
														 "3)Scanning a specific size page via the SetImageLayout method.",
														 "4)Saving the scanned image."});
			this.lstInfo.Location = new System.Drawing.Point(8, 16);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(448, 82);
			this.lstInfo.TabIndex = 10;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(464, 376);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstInfo,
																		  this.panelMain,
																		  this.labelStatus,
																		  this.buttonSelectSource,
																		  this.buttonShowLayout,
																		  this.buttonShowCaps,
																		  this.buttonCloseSession,
																		  this.buttonStartSession,
																		  this.buttonOpenSession,
																		  this.buttonShowCaption});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Twain PRO Sample";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.panelMain.ResumeLayout(false);
			this.panelCaption.ResumeLayout(false);
			this.groupBoxCaption.ResumeLayout(false);
			this.panelLayout.ResumeLayout(false);
			this.groupBoxLayout.ResumeLayout(false);
			this.panelCaps.ResumeLayout(false);
			this.groupBoxCaps.ResumeLayout(false);
			this.panelStart.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
			picTwainPro1.Debug = true;
			
		}

		private void buttonOpenSession_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";
				picTwainPro1.OpenSession ();
				buttonShowCaps.Enabled = true;
				buttonShowLayout.Enabled = true;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
				GetError();
			}
		}

		private void buttonStartSession_Click(object sender, System.EventArgs e)
		{
			
				labelStatus.Text = "";
				buttonShowCaps.Enabled = false;
				buttonShowLayout.Enabled = false;
				panelCaps.Visible = false;
				panelLayout.Visible = false;
				UpdateProps ();
				_nImageCount = 0;
			try
			{
				picTwainPro1.StartSession ();
			}
			catch (System.Runtime.InteropServices.SEHException exception)
			{
				MessageBox.Show (exception.Message);
				GetError();
			}
		}

		private void buttonCloseSession_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";
				picTwainPro1.CloseSession ();
				buttonShowCaps.Enabled = false;
				buttonShowLayout.Enabled = false;
				panelCaps.Visible = false;
				panelLayout.Visible = false;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
				GetError();
			}
		}

		private void buttonShowCaps_Click(object sender, System.EventArgs e)
		{
			if (panelCaps.Visible == false)
			{
				buttonShowCaption.Text = "Show Caption";
				buttonShowLayout.Text = "Show Layout";
				buttonShowCaps.Text = "Hide Capabilities"; 
				panelCaption.Visible = false;
				panelLayout.Visible = false;
				panelCaps.Visible = true;
				panelCaps.BringToFront ();
				comboBoxCaps.SelectedIndex = 0;
			}
			else
			{
				buttonShowCaps.Text = "Show Capabilities";
				panelCaps.Visible = false;
			}
		}

		private void buttonShowLayout_Click(object sender, System.EventArgs e)
		{
			labelStatus.Text = "";
			if (panelLayout.Visible == false)
			{
				buttonShowCaption.Text = "Show Caption";
				buttonShowCaps.Text = "Show Capabilities";
				buttonShowLayout.Text = "Hide Layout";
				panelCaption.Visible = false;
				panelCaps.Visible = false;
				
				float l = -1;
				object t = null, r = null, b = null;
				picTwainPro1.GetImageLayout  (ref l, ref t, ref r, ref b);
				
				textBoxIL.Text = l.ToString ();
				textBoxIT.Text = t.ToString ();
				textBoxIR.Text = r.ToString ();
				textBoxIB.Text = b.ToString ();

				panelLayout.Visible = true;
				panelLayout.BringToFront ();
			}
			else
			{
				buttonShowLayout.Text = "Show Layout";
				panelLayout.Visible = false;
			}
		}

		private void buttonShowCaption_Click(object sender, System.EventArgs e)
		{
			if (panelCaption.Visible == false)
			{
				buttonShowLayout.Text = "Show Layout";
				buttonShowCaps.Text = "Show Capabilities";
				buttonShowCaption.Text = "Hide Caption";
				panelCaps.Visible = false;
				panelLayout.Visible = false;
				panelCaption.Visible = true;
				panelCaption.BringToFront ();
			}
			else
			{
				buttonShowCaption.Text = "Show Caption";
				panelCaption.Visible = false;
			}
		}

		private void buttonSelectSource_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";
				picTwainPro1.CloseSession ();
				buttonShowCaps.Enabled = false;
				buttonShowLayout.Enabled = false;
				picTwainPro1.SelectSource ();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
			
		private void UpdateProps ()
		{
			// Copy values from fields to TwainPRO control
			picTwainPro1.ShowUI = checkBoxShowUI.Checked;
			picTwainPro1.Caption = textBoxCaption.Text;
			picTwainPro1.ClipCaption = checkBoxClipCaption.Checked;
			picTwainPro1.ShadowText = checkBoxShadowText.Checked;

			picTwainPro1.CaptionLeft = Int32.Parse (textBoxCapLeft.Text);
			picTwainPro1.CaptionTop = Int32.Parse (textBoxCapTop.Text);
			picTwainPro1.CaptionWidth = Int32.Parse (textBoxCapWidth.Text);
			picTwainPro1.CaptionHeight = Int32.Parse (textBoxCapHeight.Text);

//			picTwainPro1.HAlign = (tagenumHAlign) comboBoxHAlign.SelectedIndex;
//			picTwainPro1.VAlign = (tagenumVAlign) comboBoxVAlign.SelectedIndex;
		}

		private void comboBoxCaps_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Only process when session is open and FrmCaps can only be visible
			// after OpenSession is called
			if (panelCaps.Visible == false)
			{
				return;
			}

			// Reset all the controls in FrmCaps
			labelDefault.Visible = false;
			labelMin.Visible = false;
			labelMax.Visible = false;
			listBoxCaps.Visible = false;
			listBoxCaps.Items.Clear ();
		
			textBoxCurrent.Text = "";
			buttonUpdate.Enabled = false;
			picTwainPro1.Capability = (PegasusImaging.WinForms.TwainPro4.tagenumCapability) comboBoxCaps.SelectedIndex;
			
			// Check for support and stop if none
			if (picTwainPro1.CapSupported == false)
			{
				labelDefault.Text = "Not Supported";
				labelDefault.Visible = true;
				return;
			}
			
			// What data type is the Cap?  We have to check the data type
			// to know which properties are valid
			switch (picTwainPro1.CapType)
			{
				// Type ONEVALUE only returns a single value
				case tagenumCapType.TWCAP_ONEVALUE:
				{
					textBoxCurrent.Text = picTwainPro1.CapValue.ToString ();
					textBoxCurrent.Visible = true;
					break;
				}

				// Type ENUM returns a list of legal values as well as current and
				// default values.  A list of constants is returned and the CapDesc
				// property can be used to find out what the constants mean
				case tagenumCapType.TWCAP_ENUM:
				{
					textBoxCurrent.Text = picTwainPro1.CapValue.ToString ();
					textBoxCurrent.Visible = true;
					
					labelDefault.Text = "Default = " + picTwainPro1.CapDefault.ToString ();
					labelDefault.Visible = true;

					listBoxCaps.Items.Add ("Legal Values:");
					
					for (int nIndex = 0; nIndex < picTwainPro1.CapNumItems; nIndex++)
					{
						float fResult = picTwainPro1.CapItem_Get (nIndex);
						string strResult = picTwainPro1.CapDesc_Get (nIndex);

						listBoxCaps.Items.Add (fResult.ToString () + " - " + strResult.ToString ());
					}
					listBoxCaps.Visible = true;
					break;
				}	

				// Type ARRAY returns a list of values, but no current or default values
				// This is a less common type that many sources don't use
				case tagenumCapType.TWCAP_ARRAY:
				{
					listBoxCaps.Items.Add ("Legal Values:");
					for (int nIndex = 0; nIndex < picTwainPro1.CapNumItems; nIndex++)
					{
						float fResult = picTwainPro1.CapItem_Get (nIndex);
						listBoxCaps.Items.Add (fResult);
					}
					break;
				}

				// Returns a range of values as well as current and default values
				case tagenumCapType.TWCAP_RANGE:
				{	
					textBoxCurrent.Text = picTwainPro1.CapValue.ToString ();
					textBoxCurrent.Visible = true;
					
					labelDefault.Text = "Default = " + picTwainPro1.CapDefault.ToString ();
					labelDefault.Visible = true;
					
					labelMin.Text = "MinValue = " + picTwainPro1.CapMin.ToString ();
					labelMin.Visible = true;
					
					labelMax.Text = "MaxValue = " + picTwainPro1.CapMax.ToString ();
					labelMax.Visible = true;
					break;
				}
			}
			buttonUpdate.Enabled = true;
		}

		
			

		// Update the current Capability
		// This can only be done after calling OpenSession and before calling
		// StartSession (ie. in state 4 if you've read the Twain spec)
		private void buttonUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";

				// In this sample we only set the current value.  Other data types can
				// be used to provide the Source with a new list of legal values to
				// present the user when the UI is enabled, however this requires strict
				// Twain compliance by the Source which is not found in many Twain drivers
				picTwainPro1.CapTypeOut = PegasusImaging.WinForms.TwainPro4.tagenumCapType.TWCAP_ONEVALUE;
				picTwainPro1.CapValueOut = System.Single.Parse (textBoxCurrent.Text);
				picTwainPro1.SetCapOut ();
				
				// Force an update of the Caps frame with the new values
				comboBoxCaps.SelectedIndex = comboBoxCaps.SelectedIndex;
			}
			catch (FormatException)
			{
				MessageBox.Show ("You must enter a real number for the new current value.");
				GetError();
			}
		}
		
		// Update the Image Layout
		// This can only be done after calling OpenSession and before calling
		// StartSession (ie. in state 4 if you've read the Twain spec)
		private void buttonLayoutUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "";
				float l = System.Single.Parse (textBoxIL.Text);
				float t = System.Single.Parse (textBoxIT.Text);
				float r = System.Single.Parse (textBoxIR.Text);
				float b = System.Single.Parse (textBoxIB.Text);
				
				picTwainPro1.SetImageLayout (l, t, r, b);
			}
			catch (FormatException)
			{
				MessageBox.Show ("You must enter a real number for each field.");
				GetError();
			}
		}

		private void picTwainPro1_PostScandotNet(int classPtr, int objID, bool Cancel)
		{
			if (checkBoxSaveAsBMP.Checked)
			{
				string strCurrentDir = System.IO.Directory.GetCurrentDirectory ();
				string strPath = System.IO.Path.Combine (strCurrentDir, "image-" + _nImageCount.ToString () + ".bmp");
				picTwainPro1.SaveFile (strPath);
			}
			
			_nImageCount++;

			FormViewer viewer = new FormViewer ();
			viewer.PrimaryTP = picTwainPro1;
			viewer.AddImage ();
			
			Application.DoEvents ();
		}
		//  Update the status bar with the current error code and description
		private void GetError() 
		{
			if ((picTwainPro1.ErrorCode != 0)) 
			{
				labelStatus.Text = ("Data Source Error " 
					+ (picTwainPro1.ErrorCode + (": " + picTwainPro1.ErrorString)));
			}
		}


		

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
			 Application.Exit();
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			picTwainPro1.About();
		}

		
	}
}

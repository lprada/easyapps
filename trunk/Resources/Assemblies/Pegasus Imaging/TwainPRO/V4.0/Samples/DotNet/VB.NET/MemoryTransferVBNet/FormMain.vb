Imports System.Runtime.InteropServices
Imports PegasusImaging.WinForms.TwainPro4
Imports PegasusImaging.WinForms.ImagXpress8

Public Class FormMain
    Inherits System.Windows.Forms.Form

    <DllImport("kernel32.dll")> _
    Private Shared Function GlobalFree(ByVal hMem As Integer) As Integer

    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        ' Before the call to InitializeComponent, we have to unlock
        UnlockPICTwainPro.PS_Unlock(123456789, 123456789, 123456789, 123456789)

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Initialize TwainPRO to receive events
        PicTwainPro1.hParentWnd = Me.Handle.ToInt32()

        ' Let TwainPRO know that you wish to use Buffered Memory Mode
        PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_MEMORY
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            PicTwainPro1.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents buttonScan As System.Windows.Forms.Button
    Friend WithEvents labelCompressTypes As System.Windows.Forms.Label
    Friend WithEvents hScrollBarJpegQual As System.Windows.Forms.HScrollBar
    Friend WithEvents comboBoxCompression As System.Windows.Forms.ComboBox
    Friend WithEvents buttonSelectSource As System.Windows.Forms.Button
    Friend WithEvents labelJpegQual As System.Windows.Forms.Label
    Friend WithEvents labelJpegQualVal As System.Windows.Forms.Label
    Friend WithEvents labelStatus As System.Windows.Forms.Label
    Friend WithEvents checkBoxUseViewer As System.Windows.Forms.CheckBox
    Friend WithEvents PicTwainPro1 As PegasusImaging.WinForms.TwainPro4.PICTwainPro
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolBar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonScan = New System.Windows.Forms.Button()
        Me.labelCompressTypes = New System.Windows.Forms.Label()
        Me.hScrollBarJpegQual = New System.Windows.Forms.HScrollBar()
        Me.comboBoxCompression = New System.Windows.Forms.ComboBox()
        Me.buttonSelectSource = New System.Windows.Forms.Button()
        Me.labelJpegQual = New System.Windows.Forms.Label()
        Me.labelJpegQualVal = New System.Windows.Forms.Label()
        Me.labelStatus = New System.Windows.Forms.Label()
        Me.checkBoxUseViewer = New System.Windows.Forms.CheckBox()
        Me.PicTwainPro1 = New PegasusImaging.WinForms.TwainPro4.PICTwainPro()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuQuit = New System.Windows.Forms.MenuItem()
        Me.mnuToolBar = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.SuspendLayout()
        '
        'buttonScan
        '
        Me.buttonScan.BackColor = System.Drawing.SystemColors.Control
        Me.buttonScan.Cursor = System.Windows.Forms.Cursors.Default
        Me.buttonScan.Enabled = False
        Me.buttonScan.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonScan.ForeColor = System.Drawing.SystemColors.ControlText
        Me.buttonScan.Location = New System.Drawing.Point(24, 136)
        Me.buttonScan.Name = "buttonScan"
        Me.buttonScan.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.buttonScan.Size = New System.Drawing.Size(121, 41)
        Me.buttonScan.TabIndex = 20
        Me.buttonScan.Text = "Scan"
        '
        'labelCompressTypes
        '
        Me.labelCompressTypes.Location = New System.Drawing.Point(24, 208)
        Me.labelCompressTypes.Name = "labelCompressTypes"
        Me.labelCompressTypes.Size = New System.Drawing.Size(112, 16)
        Me.labelCompressTypes.TabIndex = 18
        Me.labelCompressTypes.Text = "Compression Types:"
        '
        'hScrollBarJpegQual
        '
        Me.hScrollBarJpegQual.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.hScrollBarJpegQual.Cursor = System.Windows.Forms.Cursors.Default
        Me.hScrollBarJpegQual.LargeChange = 1
        Me.hScrollBarJpegQual.Location = New System.Drawing.Point(24, 312)
        Me.hScrollBarJpegQual.Name = "hScrollBarJpegQual"
        Me.hScrollBarJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.hScrollBarJpegQual.Size = New System.Drawing.Size(129, 17)
        Me.hScrollBarJpegQual.TabIndex = 24
        Me.hScrollBarJpegQual.TabStop = True
        Me.hScrollBarJpegQual.Value = 80
        '
        'comboBoxCompression
        '
        Me.comboBoxCompression.BackColor = System.Drawing.SystemColors.Window
        Me.comboBoxCompression.Cursor = System.Windows.Forms.Cursors.Default
        Me.comboBoxCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxCompression.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboBoxCompression.ForeColor = System.Drawing.SystemColors.WindowText
        Me.comboBoxCompression.Location = New System.Drawing.Point(24, 232)
        Me.comboBoxCompression.Name = "comboBoxCompression"
        Me.comboBoxCompression.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.comboBoxCompression.Size = New System.Drawing.Size(129, 22)
        Me.comboBoxCompression.TabIndex = 22
        '
        'buttonSelectSource
        '
        Me.buttonSelectSource.BackColor = System.Drawing.SystemColors.Control
        Me.buttonSelectSource.Cursor = System.Windows.Forms.Cursors.Default
        Me.buttonSelectSource.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonSelectSource.ForeColor = System.Drawing.SystemColors.ControlText
        Me.buttonSelectSource.Location = New System.Drawing.Point(24, 80)
        Me.buttonSelectSource.Name = "buttonSelectSource"
        Me.buttonSelectSource.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.buttonSelectSource.Size = New System.Drawing.Size(121, 41)
        Me.buttonSelectSource.TabIndex = 19
        Me.buttonSelectSource.Text = "Select Source"
        '
        'labelJpegQual
        '
        Me.labelJpegQual.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.labelJpegQual.BackColor = System.Drawing.SystemColors.Control
        Me.labelJpegQual.Cursor = System.Windows.Forms.Cursors.Default
        Me.labelJpegQual.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelJpegQual.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelJpegQual.Location = New System.Drawing.Point(24, 288)
        Me.labelJpegQual.Name = "labelJpegQual"
        Me.labelJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labelJpegQual.Size = New System.Drawing.Size(84, 17)
        Me.labelJpegQual.TabIndex = 26
        Me.labelJpegQual.Text = "JPEG Quality:"
        '
        'labelJpegQualVal
        '
        Me.labelJpegQualVal.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.labelJpegQualVal.BackColor = System.Drawing.SystemColors.Control
        Me.labelJpegQualVal.Cursor = System.Windows.Forms.Cursors.Default
        Me.labelJpegQualVal.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelJpegQualVal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelJpegQualVal.Location = New System.Drawing.Point(112, 288)
        Me.labelJpegQualVal.Name = "labelJpegQualVal"
        Me.labelJpegQualVal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labelJpegQualVal.Size = New System.Drawing.Size(25, 17)
        Me.labelJpegQualVal.TabIndex = 25
        Me.labelJpegQualVal.Text = "80"
        '
        'labelStatus
        '
        Me.labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.labelStatus.BackColor = System.Drawing.SystemColors.Control
        Me.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelStatus.Cursor = System.Windows.Forms.Cursors.Default
        Me.labelStatus.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelStatus.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelStatus.Location = New System.Drawing.Point(24, 384)
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.labelStatus.Size = New System.Drawing.Size(440, 33)
        Me.labelStatus.TabIndex = 21
        Me.labelStatus.Text = "Select and Scan"
        '
        'checkBoxUseViewer
        '
        Me.checkBoxUseViewer.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.checkBoxUseViewer.BackColor = System.Drawing.SystemColors.Control
        Me.checkBoxUseViewer.Checked = True
        Me.checkBoxUseViewer.CheckState = System.Windows.Forms.CheckState.Checked
        Me.checkBoxUseViewer.Cursor = System.Windows.Forms.Cursors.Default
        Me.checkBoxUseViewer.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.checkBoxUseViewer.ForeColor = System.Drawing.SystemColors.ControlText
        Me.checkBoxUseViewer.Location = New System.Drawing.Point(24, 352)
        Me.checkBoxUseViewer.Name = "checkBoxUseViewer"
        Me.checkBoxUseViewer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.checkBoxUseViewer.Size = New System.Drawing.Size(129, 17)
        Me.checkBoxUseViewer.TabIndex = 27
        Me.checkBoxUseViewer.Text = "Use Viewer"
        '
        'PicTwainPro1
        '
        Me.PicTwainPro1.Caption = ""
        Me.PicTwainPro1.CaptionHeight = 0
        Me.PicTwainPro1.CaptionLeft = 0
        Me.PicTwainPro1.CaptionTop = 0
        Me.PicTwainPro1.CaptionWidth = 0
        Me.PicTwainPro1.ClipCaption = False
        Me.PicTwainPro1.CloseOnCancel = True
        Me.PicTwainPro1.Debug = False
        Me.PicTwainPro1.EnableExtCaps = True
        Me.PicTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic
        Me.PicTwainPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicTwainPro1.ForeColor = System.Drawing.Color.FromArgb(CType(92, Byte), CType(117, Byte), CType(115, Byte))
        Me.PicTwainPro1.FTPPassword = ""
        Me.PicTwainPro1.FTPUserName = ""
        Me.PicTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft
        Me.PicTwainPro1.Manufacturer = ""
        Me.PicTwainPro1.MaxImages = -1
        Me.PicTwainPro1.PICPassword = ""
        Me.PicTwainPro1.ProductFamily = ""
        Me.PicTwainPro1.ProductName = ""
        Me.PicTwainPro1.ProxyServer = ""
        Me.PicTwainPro1.SaveJPEGChromFactor = 36
        Me.PicTwainPro1.SaveJPEGLumFactor = 32
        Me.PicTwainPro1.SaveJPEGProgressive = False
        Me.PicTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411
        Me.PicTwainPro1.SaveMultiPage = False
        Me.PicTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed
        Me.PicTwainPro1.ShadowText = True
        Me.PicTwainPro1.ShowUI = True
        Me.PicTwainPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE
        Me.PicTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop
        Me.PicTwainPro1.VersionInfo = ""
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.Location = New System.Drawing.Point(168, 80)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(296, 296)
        Me.ImageXView1.TabIndex = 28
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Using the Memory Transfer mode and ICAP_Compression capability."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(688, 56)
        Me.lstInfo.TabIndex = 33
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(472, 136)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(232, 82)
        Me.lstStatus.TabIndex = 32
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(472, 96)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(192, 24)
        Me.lblLoadStatus.TabIndex = 31
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(472, 240)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(112, 32)
        Me.lblLastError.TabIndex = 30
        Me.lblLastError.Text = "Last Error:"
        '
        'lblerror
        '
        Me.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblerror.Location = New System.Drawing.Point(480, 288)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(224, 104)
        Me.lblerror.TabIndex = 29
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuToolBar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuQuit})
        Me.mnuFile.Text = "File"
        '
        'mnuQuit
        '
        Me.mnuQuit.Index = 0
        Me.mnuQuit.Text = "Quit"
        '
        'mnuToolBar
        '
        Me.mnuToolBar.Index = 1
        Me.mnuToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolBar.Text = "&ToolBar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 2
        Me.mnuAbout.Text = "&About"
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(722, 425)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstInfo, Me.lstStatus, Me.lblLoadStatus, Me.lblLastError, Me.lblerror, Me.ImageXView1, Me.buttonScan, Me.labelCompressTypes, Me.hScrollBarJpegQual, Me.comboBoxCompression, Me.buttonSelectSource, Me.labelJpegQual, Me.labelJpegQualVal, Me.labelStatus, Me.checkBoxUseViewer})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "FormMain"
        Me.Text = "TwainPRO Memory and Compression Sample"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument

    Dim strCurrentDir As System.String = "..\..\..\..\..\..\..\Common\Images\"
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile(ByVal sTitle As String, ByVal sFileFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFileFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusSaveFile(ByVal sTitle As String, ByVal sFilter As String) As String
        Dim dlg As SaveFileDialog = New SaveFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


#End Region

    Private Sub buttonSelectSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSelectSource.Click
        labelStatus.Text = "Select Scanner"
        Try
            PicTwainPro1.SelectSource()
        Catch ex As System.Runtime.InteropServices.SEHException
            MessageBox.Show(ex.Message)

        End Try
        UpdateCompressionTypes()
        buttonScan.Enabled = True
    End Sub

    Private Sub buttonScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonScan.Click
        Try
            labelStatus.Text = "Opening Scanner"
            Application.DoEvents()
            PicTwainPro1.OpenSession()

            If (PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_MEMORY) Then
                PicTwainPro1.CapTypeOut = PegasusImaging.WinForms.TwainPro4.tagenumCapType.TWCAP_ONEVALUE
                PicTwainPro1.Capability = PegasusImaging.WinForms.TwainPro4.tagenumCapability.TWCAP_USECAPADVANCED
                PicTwainPro1.CapAdvanced = CInt(PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_COMPRESSION)

                ' Only do the following if the Capability is supported and
                ' there is more than one compression type to set. Otherwise,
                ' the data source may say they support this capability but
                ' might return an error if you try to set to TWCP_NONE and
                ' it is the only type. All data sources must support TWCP_NONE.
                If (PicTwainPro1.CapSupported And comboBoxCompression.Items.Count > 1) Then

                    labelStatus.Text = "Setting Compression Mode"

                    Dim strCompressType As String = comboBoxCompression.SelectedItem

                    ' we have to figure out how to get back to the enumeration value
                    ' Microsoft removed the ItemData property from the combo box control
                    ' so this is definitely not an elegant solution, but it works...
                    Select Case (strCompressType)

                        Case "TWCP_NONE"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_NONE)
                            Exit Select

                        Case "TWCP_PACKBITS"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_PACKBITS)
                            Exit Select

                        Case "TWCP_GROUP31D"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31D)
                            Exit Select

                        Case "TWCP_GROUP31DEOL"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31DEOL)
                            Exit Select

                        Case "TWCP_GROUP32D"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP32D)
                            Exit Select

                        Case "TWCP_GROUP4"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP4)
                            Exit Select

                        Case "TWCP_JPEG"
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_JPEG)
                            Exit Select
                    End Select

                    PicTwainPro1.SetCapOut()

                    If (strCompressType = "TWCP_JPEG") Then

                        ' Set the JPEG Quality factor
                        PicTwainPro1.CapAdvanced = CSng(PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_JPEGQUALITY)
                        If (PicTwainPro1.CapSupported) Then
                            PicTwainPro1.CapValueOut = CSng(hScrollBarJpegQual.Value)
                            PicTwainPro1.SetCapOut()
                        End If

                        ' You could also negotiate the ICAP_JPEGPIXELTYPE capability.
                        ' I am leaving this to the DataSource
                    ElseIf (strCompressType <> "TWCP_NONE") Then

                        ' The following is done to ensure the TIFF file can be read correctly by the Imaging for Windows
                        ' Apparently Imaging for Windows must ignore the baseline tiff tag, PhotometricInterpretation.
                        ' ImagXpress does not need the following as it correctly reads in the PhotometricInterpreation tag
                        ' This will ensure that the TIFF file created has 0 as white and 1 as black

                        ' We will set the ICAP_PIXELFLAVOR first, although for CCITT compression types
                        ' ICAP_PIXELFLAVORCODES can override ICAP_PIXELFLAVOR
                        PicTwainPro1.CapAdvanced = CSng(PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_PIXELFLAVOR)
                        If (PicTwainPro1.CapSupported) Then
                            PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWPF_VANILLA)
                            PicTwainPro1.SetCapOut()
                        End If

                        If (strCompressType <> "TWCP_PACKBITS") Then
                            PicTwainPro1.CapAdvanced = CInt(PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_PIXELFLAVORCODES)
                            If (PicTwainPro1.CapSupported) Then
                                If (PicTwainPro1.CapValue <> CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWPF_VANILLA)) Then
                                    PicTwainPro1.CapValueOut = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWPF_VANILLA)
                                    PicTwainPro1.SetCapOut()  ' Set it only if we have to
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                labelStatus.Text = "TwainPRO forced to Native Transfer Mode. Compression unavailable."
            End If

            PicTwainPro1.StartSession()
            labelStatus.Text = "Select and Scan"

        Catch ex As System.Runtime.InteropServices.SEHException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub UpdateCompressionTypes()
        labelStatus.Text = "Querying Compression Capabilities"
        Application.DoEvents()

        PicTwainPro1.OpenSession()
        PicTwainPro1.Capability = PegasusImaging.WinForms.TwainPro4.tagenumCapability.TWCAP_USECAPADVANCED
        PicTwainPro1.CapAdvanced = CInt(PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_COMPRESSION)

        comboBoxCompression.Items.Clear()
        comboBoxCompression.Items.Add("TWCP_NONE")
        comboBoxCompression.SelectedIndex = 0

        If (PicTwainPro1.CapSupported) Then
            If (PicTwainPro1.CapType = PegasusImaging.WinForms.TwainPro4.tagenumCapType.TWCAP_ENUM) Then
                Dim nIndex As Integer
                For nIndex = 0 To PicTwainPro1.CapNumItems - 1 Step 1
                    Dim fResult As Single = PicTwainPro1.CapItem_Get(nIndex)

                    If (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_NONE)) Then
                        ' Already handled
                    ElseIf (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_PACKBITS)) Then
                        Dim nPosition As Integer = comboBoxCompression.Items.Add("TWCP_PACKBITS")
                    ElseIf (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31D)) Then
                        comboBoxCompression.Items.Add("TWCP_GROUP31D")
                    ElseIf (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31DEOL)) Then
                        comboBoxCompression.Items.Add("TWCP_GROUP31DEOL")
                    ElseIf (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP32D)) Then
                        comboBoxCompression.Items.Add("TWCP_GROUP32D")
                    ElseIf (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP4)) Then
                        comboBoxCompression.Items.Add("TWCP_GROUP4")
                    ElseIf (fResult = CSng(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_JPEG)) Then
                        comboBoxCompression.Items.Add("TWCP_JPEG")
                    End If
                Next
            End If
        End If

        PicTwainPro1.CloseSession()
        labelStatus.Text = "Select and Scan"
    End Sub

    Private Sub hScrollBarJpegQual_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarJpegQual.ValueChanged
        labelJpegQualVal.Text = hScrollBarJpegQual.Value.ToString()
    End Sub

    Private Sub comboBoxCompression_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles comboBoxCompression.SelectedValueChanged
        If (comboBoxCompression.SelectedItem = "TWCP_JPEG") Then
            hScrollBarJpegQual.Enabled = True
        Else
            hScrollBarJpegQual.Enabled = False
        End If
    End Sub

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)

        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))

            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If
    End Sub

    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        '
        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub


    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '**The UnlockRuntime function must be called to distribute the runtime**
        'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)

        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
        AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

        ' Let TwainPRO know that you wish to use Buffered Memory Mode
        PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_MEMORY
    End Sub

    Private Sub PicTwainPro1_PostScandotNet(ByVal classPtr As Integer, ByVal objID As Integer, ByVal Cancel As Boolean) Handles PicTwainPro1.PostScandotNet
        labelStatus.Text = ""
        ' Need to check the Compression type to see if we are transferring a DIB
        ' or we are transferring compressed data.
        If (PicTwainPro1.ICompression = CType(PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_NONE, Integer)) Then
            Dim hImage As Integer = PicTwainPro1.hDIB
            If (hImage <> 0) Then
                Try
                    ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(hImage))
                    If (checkBoxUseViewer.Checked = True) Then
                        Dim formViewer As FormViewer = New FormViewer()
                        formViewer.ShowEx(ImageXView1.Image, True)
                    End If
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                End Try
            End If
        Else
            Dim hImage As Integer = PicTwainPro1.hImage
            If (hImage <> 0) Then
                Try
                    ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(hImage))
                    If (checkBoxUseViewer.Checked = True) Then
                        Dim formViewer As FormViewer = New FormViewer()
                        formViewer.ShowEx(ImageXView1.Image, False)
                    End If
                Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                    PegasusError(ex, lblerror)
                End Try
                GlobalFree(hImage)
            End If
        End If
        Application.DoEvents()
        Cancel = False
    End Sub

    Private Sub PicTwainPro1_PreScandotNet(ByVal classPtr As Integer, ByVal objID As Integer, ByVal Cancel As Boolean) Handles PicTwainPro1.PreScandotNet
        labelStatus.Text = "Scanning..."
    End Sub

    Private Sub mnuQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuit.Click
        Application.Exit()

    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        PicTwainPro1.About()
    End Sub

   
    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click

        If ImageXView1.Toolbar.Activated = True Then
            mnuToolbarShow.Text = "Show"
            ImageXView1.Toolbar.Activated = False
        Else
            mnuToolbarShow.Text = "Hide"
            ImageXView1.Toolbar.Activated = True

        End If

    End Sub

   
End Class

Imports PegasusImaging.WinForms.TwainPro4

Public Class FormMain
    Inherits System.Windows.Forms.Form

    Private _nImageCount As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        ' Before the call to InitializeComponent, we have to unlock
        UnlockPICTwainPro.PS_Unlock(123456789, 123456789, 123456789, 123456789)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        PicTwainPro1.hParentWnd = Me.Handle.ToInt32()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            PicTwainPro1.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonShowCaps As System.Windows.Forms.Button
    Friend WithEvents buttonCloseSession As System.Windows.Forms.Button
    Friend WithEvents buttonStartSession As System.Windows.Forms.Button
    Friend WithEvents buttonOpenSession As System.Windows.Forms.Button
    Friend WithEvents buttonShowCaption As System.Windows.Forms.Button
    Friend WithEvents buttonShowLayout As System.Windows.Forms.Button
    Friend WithEvents buttonSelectSource As System.Windows.Forms.Button
    Friend WithEvents panelMain As System.Windows.Forms.Panel
    Friend WithEvents panelCaption As System.Windows.Forms.Panel
    Friend WithEvents groupBoxCaption As System.Windows.Forms.GroupBox
    Friend WithEvents comboBoxVAlign As System.Windows.Forms.ComboBox
    Friend WithEvents comboBoxHAlign As System.Windows.Forms.ComboBox
    Friend WithEvents textBoxCaption As System.Windows.Forms.TextBox
    Friend WithEvents textBoxCapHeight As System.Windows.Forms.TextBox
    Friend WithEvents textBoxCapWidth As System.Windows.Forms.TextBox
    Friend WithEvents textBoxCapTop As System.Windows.Forms.TextBox
    Friend WithEvents textBoxCapLeft As System.Windows.Forms.TextBox
    Friend WithEvents labelVAlign As System.Windows.Forms.Label
    Friend WithEvents labelHAlign As System.Windows.Forms.Label
    Friend WithEvents labelCaption As System.Windows.Forms.Label
    Friend WithEvents labelCapHeight As System.Windows.Forms.Label
    Friend WithEvents labelCapWidth As System.Windows.Forms.Label
    Friend WithEvents labelCapTop As System.Windows.Forms.Label
    Friend WithEvents labelCapLeft As System.Windows.Forms.Label
    Friend WithEvents checkBoxShadowText As System.Windows.Forms.CheckBox
    Friend WithEvents checkBoxClipCaption As System.Windows.Forms.CheckBox
    Friend WithEvents panelLayout As System.Windows.Forms.Panel
    Friend WithEvents groupBoxLayout As System.Windows.Forms.GroupBox
    Friend WithEvents textBoxIB As System.Windows.Forms.TextBox
    Friend WithEvents textBoxIR As System.Windows.Forms.TextBox
    Friend WithEvents textBoxIT As System.Windows.Forms.TextBox
    Friend WithEvents textBoxIL As System.Windows.Forms.TextBox
    Friend WithEvents buttonLayoutUpdate As System.Windows.Forms.Button
    Friend WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents panelCaps As System.Windows.Forms.Panel
    Friend WithEvents groupBoxCaps As System.Windows.Forms.GroupBox
    Friend WithEvents labelMax As System.Windows.Forms.Label
    Friend WithEvents labelMin As System.Windows.Forms.Label
    Friend WithEvents buttonUpdate As System.Windows.Forms.Button
    Friend WithEvents textBoxCurrent As System.Windows.Forms.TextBox
    Friend WithEvents labelCurrent As System.Windows.Forms.Label
    Friend WithEvents listBoxCaps As System.Windows.Forms.ListBox
    Friend WithEvents labelDefault As System.Windows.Forms.Label
    Friend WithEvents comboBoxCaps As System.Windows.Forms.ComboBox
    Friend WithEvents panelStart As System.Windows.Forms.Panel
    Friend WithEvents checkBoxSaveAsBMP As System.Windows.Forms.CheckBox
    Friend WithEvents checkBoxShowUI As System.Windows.Forms.CheckBox
    Friend WithEvents labelStatus As System.Windows.Forms.Label

    Friend WithEvents PicTwainPro1 As PegasusImaging.WinForms.TwainPro4.PICTwainPro
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents mnu_File As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Quit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_About As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonShowCaps = New System.Windows.Forms.Button()
        Me.buttonCloseSession = New System.Windows.Forms.Button()
        Me.buttonStartSession = New System.Windows.Forms.Button()
        Me.buttonOpenSession = New System.Windows.Forms.Button()
        Me.buttonShowCaption = New System.Windows.Forms.Button()
        Me.buttonShowLayout = New System.Windows.Forms.Button()
        Me.buttonSelectSource = New System.Windows.Forms.Button()
        Me.panelMain = New System.Windows.Forms.Panel()
        Me.panelCaption = New System.Windows.Forms.Panel()
        Me.groupBoxCaption = New System.Windows.Forms.GroupBox()
        Me.comboBoxVAlign = New System.Windows.Forms.ComboBox()
        Me.comboBoxHAlign = New System.Windows.Forms.ComboBox()
        Me.textBoxCaption = New System.Windows.Forms.TextBox()
        Me.textBoxCapHeight = New System.Windows.Forms.TextBox()
        Me.textBoxCapWidth = New System.Windows.Forms.TextBox()
        Me.textBoxCapTop = New System.Windows.Forms.TextBox()
        Me.textBoxCapLeft = New System.Windows.Forms.TextBox()
        Me.labelVAlign = New System.Windows.Forms.Label()
        Me.labelHAlign = New System.Windows.Forms.Label()
        Me.labelCaption = New System.Windows.Forms.Label()
        Me.labelCapHeight = New System.Windows.Forms.Label()
        Me.labelCapWidth = New System.Windows.Forms.Label()
        Me.labelCapTop = New System.Windows.Forms.Label()
        Me.labelCapLeft = New System.Windows.Forms.Label()
        Me.checkBoxShadowText = New System.Windows.Forms.CheckBox()
        Me.checkBoxClipCaption = New System.Windows.Forms.CheckBox()
        Me.panelLayout = New System.Windows.Forms.Panel()
        Me.groupBoxLayout = New System.Windows.Forms.GroupBox()
        Me.textBoxIB = New System.Windows.Forms.TextBox()
        Me.textBoxIR = New System.Windows.Forms.TextBox()
        Me.textBoxIT = New System.Windows.Forms.TextBox()
        Me.textBoxIL = New System.Windows.Forms.TextBox()
        Me.buttonLayoutUpdate = New System.Windows.Forms.Button()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.panelCaps = New System.Windows.Forms.Panel()
        Me.groupBoxCaps = New System.Windows.Forms.GroupBox()
        Me.labelMax = New System.Windows.Forms.Label()
        Me.labelMin = New System.Windows.Forms.Label()
        Me.buttonUpdate = New System.Windows.Forms.Button()
        Me.textBoxCurrent = New System.Windows.Forms.TextBox()
        Me.labelCurrent = New System.Windows.Forms.Label()
        Me.listBoxCaps = New System.Windows.Forms.ListBox()
        Me.labelDefault = New System.Windows.Forms.Label()
        Me.comboBoxCaps = New System.Windows.Forms.ComboBox()
        Me.panelStart = New System.Windows.Forms.Panel()
        Me.checkBoxSaveAsBMP = New System.Windows.Forms.CheckBox()
        Me.checkBoxShowUI = New System.Windows.Forms.CheckBox()
        Me.labelStatus = New System.Windows.Forms.Label()
        Me.PicTwainPro1 = New PegasusImaging.WinForms.TwainPro4.PICTwainPro()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnu_File = New System.Windows.Forms.MenuItem()
        Me.mnu_Quit = New System.Windows.Forms.MenuItem()
        Me.mnu_About = New System.Windows.Forms.MenuItem()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.panelMain.SuspendLayout()
        Me.panelCaption.SuspendLayout()
        Me.groupBoxCaption.SuspendLayout()
        Me.panelLayout.SuspendLayout()
        Me.groupBoxLayout.SuspendLayout()
        Me.panelCaps.SuspendLayout()
        Me.groupBoxCaps.SuspendLayout()
        Me.panelStart.SuspendLayout()
        Me.SuspendLayout()
        '
        'buttonShowCaps
        '
        Me.buttonShowCaps.Enabled = False
        Me.buttonShowCaps.Location = New System.Drawing.Point(8, 264)
        Me.buttonShowCaps.Name = "buttonShowCaps"
        Me.buttonShowCaps.Size = New System.Drawing.Size(128, 23)
        Me.buttonShowCaps.TabIndex = 13
        Me.buttonShowCaps.Text = "Show Capabilities"
        '
        'buttonCloseSession
        '
        Me.buttonCloseSession.Location = New System.Drawing.Point(8, 232)
        Me.buttonCloseSession.Name = "buttonCloseSession"
        Me.buttonCloseSession.Size = New System.Drawing.Size(128, 23)
        Me.buttonCloseSession.TabIndex = 12
        Me.buttonCloseSession.Text = "Close Session"
        '
        'buttonStartSession
        '
        Me.buttonStartSession.Location = New System.Drawing.Point(8, 200)
        Me.buttonStartSession.Name = "buttonStartSession"
        Me.buttonStartSession.Size = New System.Drawing.Size(128, 23)
        Me.buttonStartSession.TabIndex = 11
        Me.buttonStartSession.Text = "Start Session\Acquire"
        '
        'buttonOpenSession
        '
        Me.buttonOpenSession.Location = New System.Drawing.Point(8, 168)
        Me.buttonOpenSession.Name = "buttonOpenSession"
        Me.buttonOpenSession.Size = New System.Drawing.Size(128, 23)
        Me.buttonOpenSession.TabIndex = 10
        Me.buttonOpenSession.Text = "Open Session"
        '
        'buttonShowCaption
        '
        Me.buttonShowCaption.Location = New System.Drawing.Point(8, 328)
        Me.buttonShowCaption.Name = "buttonShowCaption"
        Me.buttonShowCaption.Size = New System.Drawing.Size(128, 23)
        Me.buttonShowCaption.TabIndex = 15
        Me.buttonShowCaption.Text = "Show Caption"
        '
        'buttonShowLayout
        '
        Me.buttonShowLayout.Enabled = False
        Me.buttonShowLayout.Location = New System.Drawing.Point(8, 296)
        Me.buttonShowLayout.Name = "buttonShowLayout"
        Me.buttonShowLayout.Size = New System.Drawing.Size(128, 23)
        Me.buttonShowLayout.TabIndex = 14
        Me.buttonShowLayout.Text = "Show Layout"
        '
        'buttonSelectSource
        '
        Me.buttonSelectSource.Location = New System.Drawing.Point(8, 136)
        Me.buttonSelectSource.Name = "buttonSelectSource"
        Me.buttonSelectSource.Size = New System.Drawing.Size(128, 23)
        Me.buttonSelectSource.TabIndex = 16
        Me.buttonSelectSource.Text = "Select Source"
        '
        'panelMain
        '
        Me.panelMain.Controls.AddRange(New System.Windows.Forms.Control() {Me.panelCaption, Me.panelLayout, Me.panelCaps, Me.panelStart})
        Me.panelMain.Location = New System.Drawing.Point(143, 128)
        Me.panelMain.Name = "panelMain"
        Me.panelMain.Size = New System.Drawing.Size(312, 224)
        Me.panelMain.TabIndex = 18
        '
        'panelCaption
        '
        Me.panelCaption.BackColor = System.Drawing.SystemColors.Control
        Me.panelCaption.Controls.AddRange(New System.Windows.Forms.Control() {Me.groupBoxCaption})
        Me.panelCaption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelCaption.Name = "panelCaption"
        Me.panelCaption.Size = New System.Drawing.Size(312, 224)
        Me.panelCaption.TabIndex = 2
        Me.panelCaption.Visible = False
        '
        'groupBoxCaption
        '
        Me.groupBoxCaption.Controls.AddRange(New System.Windows.Forms.Control() {Me.comboBoxVAlign, Me.comboBoxHAlign, Me.textBoxCaption, Me.textBoxCapHeight, Me.textBoxCapWidth, Me.textBoxCapTop, Me.textBoxCapLeft, Me.labelVAlign, Me.labelHAlign, Me.labelCaption, Me.labelCapHeight, Me.labelCapWidth, Me.labelCapTop, Me.labelCapLeft, Me.checkBoxShadowText, Me.checkBoxClipCaption})
        Me.groupBoxCaption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupBoxCaption.Name = "groupBoxCaption"
        Me.groupBoxCaption.Size = New System.Drawing.Size(312, 224)
        Me.groupBoxCaption.TabIndex = 0
        Me.groupBoxCaption.TabStop = False
        Me.groupBoxCaption.Text = "Caption"
        '
        'comboBoxVAlign
        '
        Me.comboBoxVAlign.Location = New System.Drawing.Point(184, 104)
        Me.comboBoxVAlign.Name = "comboBoxVAlign"
        Me.comboBoxVAlign.Size = New System.Drawing.Size(120, 21)
        Me.comboBoxVAlign.TabIndex = 15
        Me.comboBoxVAlign.Text = "comboBox2"
        '
        'comboBoxHAlign
        '
        Me.comboBoxHAlign.Location = New System.Drawing.Point(184, 56)
        Me.comboBoxHAlign.Name = "comboBoxHAlign"
        Me.comboBoxHAlign.Size = New System.Drawing.Size(120, 21)
        Me.comboBoxHAlign.TabIndex = 14
        Me.comboBoxHAlign.Text = "comboBox1"
        '
        'textBoxCaption
        '
        Me.textBoxCaption.Location = New System.Drawing.Point(112, 134)
        Me.textBoxCaption.Name = "textBoxCaption"
        Me.textBoxCaption.Size = New System.Drawing.Size(192, 20)
        Me.textBoxCaption.TabIndex = 13
        Me.textBoxCaption.Text = "TwainPRO 4"
        '
        'textBoxCapHeight
        '
        Me.textBoxCapHeight.Location = New System.Drawing.Point(112, 110)
        Me.textBoxCapHeight.Name = "textBoxCapHeight"
        Me.textBoxCapHeight.Size = New System.Drawing.Size(48, 20)
        Me.textBoxCapHeight.TabIndex = 12
        Me.textBoxCapHeight.Text = "0"
        Me.textBoxCapHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'textBoxCapWidth
        '
        Me.textBoxCapWidth.Location = New System.Drawing.Point(112, 86)
        Me.textBoxCapWidth.Name = "textBoxCapWidth"
        Me.textBoxCapWidth.Size = New System.Drawing.Size(48, 20)
        Me.textBoxCapWidth.TabIndex = 11
        Me.textBoxCapWidth.Text = "0"
        Me.textBoxCapWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'textBoxCapTop
        '
        Me.textBoxCapTop.Location = New System.Drawing.Point(112, 62)
        Me.textBoxCapTop.Name = "textBoxCapTop"
        Me.textBoxCapTop.Size = New System.Drawing.Size(48, 20)
        Me.textBoxCapTop.TabIndex = 10
        Me.textBoxCapTop.Text = "0"
        Me.textBoxCapTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'textBoxCapLeft
        '
        Me.textBoxCapLeft.Location = New System.Drawing.Point(112, 38)
        Me.textBoxCapLeft.Name = "textBoxCapLeft"
        Me.textBoxCapLeft.Size = New System.Drawing.Size(48, 20)
        Me.textBoxCapLeft.TabIndex = 9
        Me.textBoxCapLeft.Text = "0"
        Me.textBoxCapLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'labelVAlign
        '
        Me.labelVAlign.Location = New System.Drawing.Point(184, 88)
        Me.labelVAlign.Name = "labelVAlign"
        Me.labelVAlign.Size = New System.Drawing.Size(40, 16)
        Me.labelVAlign.TabIndex = 8
        Me.labelVAlign.Text = "VAlign"
        '
        'labelHAlign
        '
        Me.labelHAlign.Location = New System.Drawing.Point(184, 40)
        Me.labelHAlign.Name = "labelHAlign"
        Me.labelHAlign.Size = New System.Drawing.Size(40, 16)
        Me.labelHAlign.TabIndex = 7
        Me.labelHAlign.Text = "HAlign"
        '
        'labelCaption
        '
        Me.labelCaption.Location = New System.Drawing.Point(16, 136)
        Me.labelCaption.Name = "labelCaption"
        Me.labelCaption.Size = New System.Drawing.Size(48, 16)
        Me.labelCaption.TabIndex = 6
        Me.labelCaption.Text = "Caption:"
        '
        'labelCapHeight
        '
        Me.labelCapHeight.Location = New System.Drawing.Point(16, 112)
        Me.labelCapHeight.Name = "labelCapHeight"
        Me.labelCapHeight.Size = New System.Drawing.Size(88, 16)
        Me.labelCapHeight.TabIndex = 5
        Me.labelCapHeight.Text = "Caption Height:"
        '
        'labelCapWidth
        '
        Me.labelCapWidth.Location = New System.Drawing.Point(16, 88)
        Me.labelCapWidth.Name = "labelCapWidth"
        Me.labelCapWidth.Size = New System.Drawing.Size(80, 16)
        Me.labelCapWidth.TabIndex = 4
        Me.labelCapWidth.Text = "Caption Width:"
        '
        'labelCapTop
        '
        Me.labelCapTop.Location = New System.Drawing.Point(16, 64)
        Me.labelCapTop.Name = "labelCapTop"
        Me.labelCapTop.Size = New System.Drawing.Size(72, 16)
        Me.labelCapTop.TabIndex = 3
        Me.labelCapTop.Text = "Caption Top:"
        '
        'labelCapLeft
        '
        Me.labelCapLeft.Location = New System.Drawing.Point(16, 40)
        Me.labelCapLeft.Name = "labelCapLeft"
        Me.labelCapLeft.Size = New System.Drawing.Size(72, 16)
        Me.labelCapLeft.TabIndex = 2
        Me.labelCapLeft.Text = "Caption Left:"
        '
        'checkBoxShadowText
        '
        Me.checkBoxShadowText.Location = New System.Drawing.Point(128, 16)
        Me.checkBoxShadowText.Name = "checkBoxShadowText"
        Me.checkBoxShadowText.Size = New System.Drawing.Size(96, 16)
        Me.checkBoxShadowText.TabIndex = 1
        Me.checkBoxShadowText.Text = "Shadow Text"
        '
        'checkBoxClipCaption
        '
        Me.checkBoxClipCaption.Location = New System.Drawing.Point(16, 16)
        Me.checkBoxClipCaption.Name = "checkBoxClipCaption"
        Me.checkBoxClipCaption.Size = New System.Drawing.Size(88, 16)
        Me.checkBoxClipCaption.TabIndex = 0
        Me.checkBoxClipCaption.Text = "Clip Caption"
        '
        'panelLayout
        '
        Me.panelLayout.BackColor = System.Drawing.SystemColors.Control
        Me.panelLayout.Controls.AddRange(New System.Windows.Forms.Control() {Me.groupBoxLayout})
        Me.panelLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelLayout.Name = "panelLayout"
        Me.panelLayout.Size = New System.Drawing.Size(312, 224)
        Me.panelLayout.TabIndex = 0
        Me.panelLayout.Visible = False
        '
        'groupBoxLayout
        '
        Me.groupBoxLayout.Controls.AddRange(New System.Windows.Forms.Control() {Me.textBoxIB, Me.textBoxIR, Me.textBoxIT, Me.textBoxIL, Me.buttonLayoutUpdate, Me.label4, Me.label3, Me.label2, Me.label1})
        Me.groupBoxLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupBoxLayout.Name = "groupBoxLayout"
        Me.groupBoxLayout.Size = New System.Drawing.Size(312, 224)
        Me.groupBoxLayout.TabIndex = 0
        Me.groupBoxLayout.TabStop = False
        Me.groupBoxLayout.Text = "Layout"
        '
        'textBoxIB
        '
        Me.textBoxIB.Location = New System.Drawing.Point(156, 126)
        Me.textBoxIB.Name = "textBoxIB"
        Me.textBoxIB.Size = New System.Drawing.Size(88, 20)
        Me.textBoxIB.TabIndex = 8
        Me.textBoxIB.Text = ""
        '
        'textBoxIR
        '
        Me.textBoxIR.Location = New System.Drawing.Point(156, 94)
        Me.textBoxIR.Name = "textBoxIR"
        Me.textBoxIR.Size = New System.Drawing.Size(88, 20)
        Me.textBoxIR.TabIndex = 7
        Me.textBoxIR.Text = ""
        '
        'textBoxIT
        '
        Me.textBoxIT.Location = New System.Drawing.Point(156, 62)
        Me.textBoxIT.Name = "textBoxIT"
        Me.textBoxIT.Size = New System.Drawing.Size(88, 20)
        Me.textBoxIT.TabIndex = 6
        Me.textBoxIT.Text = ""
        '
        'textBoxIL
        '
        Me.textBoxIL.Location = New System.Drawing.Point(156, 30)
        Me.textBoxIL.Name = "textBoxIL"
        Me.textBoxIL.Size = New System.Drawing.Size(88, 20)
        Me.textBoxIL.TabIndex = 5
        Me.textBoxIL.Text = ""
        '
        'buttonLayoutUpdate
        '
        Me.buttonLayoutUpdate.Location = New System.Drawing.Point(112, 160)
        Me.buttonLayoutUpdate.Name = "buttonLayoutUpdate"
        Me.buttonLayoutUpdate.Size = New System.Drawing.Size(96, 24)
        Me.buttonLayoutUpdate.TabIndex = 4
        Me.buttonLayoutUpdate.Text = "Update"
        '
        'label4
        '
        Me.label4.Location = New System.Drawing.Point(68, 128)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(80, 16)
        Me.label4.TabIndex = 3
        Me.label4.Text = "Image Bottom:"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(68, 96)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(72, 16)
        Me.label3.TabIndex = 2
        Me.label3.Text = "Image Right:"
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(68, 64)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(64, 16)
        Me.label2.TabIndex = 1
        Me.label2.Text = "Image Top:"
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(68, 32)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(64, 16)
        Me.label1.TabIndex = 0
        Me.label1.Text = "Image Left:"
        '
        'panelCaps
        '
        Me.panelCaps.BackColor = System.Drawing.SystemColors.Control
        Me.panelCaps.Controls.AddRange(New System.Windows.Forms.Control() {Me.groupBoxCaps})
        Me.panelCaps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelCaps.Name = "panelCaps"
        Me.panelCaps.Size = New System.Drawing.Size(312, 224)
        Me.panelCaps.TabIndex = 1
        Me.panelCaps.Visible = False
        '
        'groupBoxCaps
        '
        Me.groupBoxCaps.Controls.AddRange(New System.Windows.Forms.Control() {Me.labelMax, Me.labelMin, Me.buttonUpdate, Me.textBoxCurrent, Me.labelCurrent, Me.listBoxCaps, Me.labelDefault, Me.comboBoxCaps})
        Me.groupBoxCaps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupBoxCaps.Name = "groupBoxCaps"
        Me.groupBoxCaps.Size = New System.Drawing.Size(312, 224)
        Me.groupBoxCaps.TabIndex = 0
        Me.groupBoxCaps.TabStop = False
        Me.groupBoxCaps.Text = "Capabilities"
        '
        'labelMax
        '
        Me.labelMax.Location = New System.Drawing.Point(32, 104)
        Me.labelMax.Name = "labelMax"
        Me.labelMax.Size = New System.Drawing.Size(256, 16)
        Me.labelMax.TabIndex = 7
        Me.labelMax.Text = "labelMax"
        '
        'labelMin
        '
        Me.labelMin.Location = New System.Drawing.Point(32, 80)
        Me.labelMin.Name = "labelMin"
        Me.labelMin.Size = New System.Drawing.Size(256, 16)
        Me.labelMin.TabIndex = 6
        Me.labelMin.Text = "labelMin"
        '
        'buttonUpdate
        '
        Me.buttonUpdate.Location = New System.Drawing.Point(208, 168)
        Me.buttonUpdate.Name = "buttonUpdate"
        Me.buttonUpdate.Size = New System.Drawing.Size(80, 24)
        Me.buttonUpdate.TabIndex = 5
        Me.buttonUpdate.Text = "Update"
        '
        'textBoxCurrent
        '
        Me.textBoxCurrent.Location = New System.Drawing.Point(96, 170)
        Me.textBoxCurrent.Name = "textBoxCurrent"
        Me.textBoxCurrent.Size = New System.Drawing.Size(96, 20)
        Me.textBoxCurrent.TabIndex = 4
        Me.textBoxCurrent.Text = ""
        '
        'labelCurrent
        '
        Me.labelCurrent.Location = New System.Drawing.Point(40, 172)
        Me.labelCurrent.Name = "labelCurrent"
        Me.labelCurrent.Size = New System.Drawing.Size(48, 16)
        Me.labelCurrent.TabIndex = 3
        Me.labelCurrent.Text = "Current:"
        '
        'listBoxCaps
        '
        Me.listBoxCaps.Location = New System.Drawing.Point(32, 56)
        Me.listBoxCaps.Name = "listBoxCaps"
        Me.listBoxCaps.Size = New System.Drawing.Size(256, 95)
        Me.listBoxCaps.TabIndex = 2
        '
        'labelDefault
        '
        Me.labelDefault.Location = New System.Drawing.Point(32, 56)
        Me.labelDefault.Name = "labelDefault"
        Me.labelDefault.Size = New System.Drawing.Size(256, 16)
        Me.labelDefault.TabIndex = 1
        Me.labelDefault.Text = "labelDefault"
        '
        'comboBoxCaps
        '
        Me.comboBoxCaps.Items.AddRange(New Object() {"AutoBright", "Brightness", "Contrast", "Gamma", "Physical Width", "Physical Height", "Pixel Type", "Rotation", "Units", "X Native Resolution", "Y Native Resolution", "X Resolution", "Y Resolution", "X Scaling", "Y Scaling", "Device Online", "Indicators", "Lamp State", "UIControllable", "Transfer Count", "Auto Feed", "Auto Scan", "Clear Page", "Duplex", "Duplex Enabled", "Feeder Enabled", "Feeder Loaded", "Feed Page", "Paper Detectable"})
        Me.comboBoxCaps.Location = New System.Drawing.Point(32, 24)
        Me.comboBoxCaps.Name = "comboBoxCaps"
        Me.comboBoxCaps.Size = New System.Drawing.Size(256, 21)
        Me.comboBoxCaps.TabIndex = 0
        Me.comboBoxCaps.Text = "comboBox1"
        '
        'panelStart
        '
        Me.panelStart.Controls.AddRange(New System.Windows.Forms.Control() {Me.checkBoxSaveAsBMP, Me.checkBoxShowUI})
        Me.panelStart.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelStart.Name = "panelStart"
        Me.panelStart.Size = New System.Drawing.Size(312, 224)
        Me.panelStart.TabIndex = 3
        '
        'checkBoxSaveAsBMP
        '
        Me.checkBoxSaveAsBMP.Location = New System.Drawing.Point(8, 40)
        Me.checkBoxSaveAsBMP.Name = "checkBoxSaveAsBMP"
        Me.checkBoxSaveAsBMP.Size = New System.Drawing.Size(128, 16)
        Me.checkBoxSaveAsBMP.TabIndex = 1
        Me.checkBoxSaveAsBMP.Text = "Save as imag-n.bmp"
        '
        'checkBoxShowUI
        '
        Me.checkBoxShowUI.Location = New System.Drawing.Point(8, 8)
        Me.checkBoxShowUI.Name = "checkBoxShowUI"
        Me.checkBoxShowUI.Size = New System.Drawing.Size(96, 16)
        Me.checkBoxShowUI.TabIndex = 0
        Me.checkBoxShowUI.Text = "Show UI"
        Me.checkBoxShowUI.TextAlign = System.Drawing.ContentAlignment.TopLeft
        '
        'labelStatus
        '
        Me.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelStatus.Location = New System.Drawing.Point(8, 360)
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.Size = New System.Drawing.Size(448, 16)
        Me.labelStatus.TabIndex = 17
        '
        'PicTwainPro1
        '
        Me.PicTwainPro1.Caption = ""
        Me.PicTwainPro1.CaptionHeight = 0
        Me.PicTwainPro1.CaptionLeft = 0
        Me.PicTwainPro1.CaptionTop = 0
        Me.PicTwainPro1.CaptionWidth = 0
        Me.PicTwainPro1.ClipCaption = False
        Me.PicTwainPro1.CloseOnCancel = True
        Me.PicTwainPro1.Debug = False
        Me.PicTwainPro1.EnableExtCaps = True
        Me.PicTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic
        Me.PicTwainPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicTwainPro1.ForeColor = System.Drawing.Color.FromArgb(CType(82, Byte), CType(73, Byte), CType(86, Byte))
        Me.PicTwainPro1.FTPPassword = ""
        Me.PicTwainPro1.FTPUserName = ""
        Me.PicTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft
        Me.PicTwainPro1.Manufacturer = ""
        Me.PicTwainPro1.MaxImages = -1
        Me.PicTwainPro1.PICPassword = ""
        Me.PicTwainPro1.ProductFamily = ""
        Me.PicTwainPro1.ProductName = ""
        Me.PicTwainPro1.ProxyServer = ""
        Me.PicTwainPro1.SaveJPEGChromFactor = 36
        Me.PicTwainPro1.SaveJPEGLumFactor = 32
        Me.PicTwainPro1.SaveJPEGProgressive = False
        Me.PicTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411
        Me.PicTwainPro1.SaveMultiPage = False
        Me.PicTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed
        Me.PicTwainPro1.ShadowText = True
        Me.PicTwainPro1.ShowUI = True
        Me.PicTwainPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE
        Me.PicTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop
        Me.PicTwainPro1.VersionInfo = ""
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_File, Me.mnu_About})
        '
        'mnu_File
        '
        Me.mnu_File.Index = 0
        Me.mnu_File.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_Quit})
        Me.mnu_File.Text = "File"
        '
        'mnu_Quit
        '
        Me.mnu_Quit.Index = 0
        Me.mnu_Quit.Text = "Quit"
        '
        'mnu_About
        '
        Me.mnu_About.Index = 1
        Me.mnu_About.Text = "About"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Selecting the device with or without the scanner's UI via the ShowUI property.", "2)Querying and setting the various TWAIN capabilities.", "3)Scanning a specific size page via the SetImageLayout method.", "4)Saving the scanned image."})
        Me.ListBox1.Location = New System.Drawing.Point(8, 16)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(440, 95)
        Me.ListBox1.TabIndex = 19
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(462, 392)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.ListBox1, Me.buttonCloseSession, Me.buttonStartSession, Me.buttonOpenSession, Me.buttonShowCaption, Me.buttonShowLayout, Me.buttonSelectSource, Me.panelMain, Me.labelStatus, Me.buttonShowCaps})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Twain PRO Sample"
        Me.panelMain.ResumeLayout(False)
        Me.panelCaption.ResumeLayout(False)
        Me.groupBoxCaption.ResumeLayout(False)
        Me.panelLayout.ResumeLayout(False)
        Me.groupBoxLayout.ResumeLayout(False)
        Me.panelCaps.ResumeLayout(False)
        Me.groupBoxCaps.ResumeLayout(False)
        Me.panelStart.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub buttonOpenSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonOpenSession.Click


        Try
            labelStatus.Text = ""
            PicTwainPro1.OpenSession()
            buttonShowCaps.Enabled = True
            buttonShowLayout.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()

        End Try
    End Sub

    Private Sub buttonStartSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonStartSession.Click

        labelStatus.Text = ""
        buttonShowCaps.Enabled = False
        buttonShowLayout.Enabled = False
        panelCaps.Visible = False
        panelLayout.Visible = False
        UpdateProps()
        _nImageCount = 0
        Try
            PicTwainPro1.StartSession()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub

    Private Sub buttonCloseSession_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCloseSession.Click
        Try
            labelStatus.Text = ""
            PicTwainPro1.CloseSession()
            buttonShowCaps.Enabled = False
            buttonShowLayout.Enabled = False
            panelCaps.Visible = False
            panelLayout.Visible = False
        Catch

        End Try
    End Sub

    Private Sub buttonShowCaps_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonShowCaps.Click
        If (panelCaps.Visible = False) Then
            buttonShowCaption.Text = "Show Caption"
            buttonShowLayout.Text = "Show Layout"
            buttonShowCaps.Text = "Hide Capabilities"
            panelCaption.Visible = False
            panelLayout.Visible = False
            panelCaps.Visible = True
            panelCaps.BringToFront()
            comboBoxCaps.SelectedIndex = 0
        Else
            buttonShowCaps.Text = "Show Capabilities"
            panelCaps.Visible = False
        End If
    End Sub

    Private Sub buttonShowLayout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonShowLayout.Click
        labelStatus.Text = ""
        If (panelLayout.Visible = False) Then
            buttonShowCaption.Text = "Show Caption"
            buttonShowCaps.Text = "Show Capabilities"
            buttonShowLayout.Text = "Hide Layout"
            panelCaption.Visible = False
            panelCaps.Visible = False

            ' TODO: Fix this once everything becomes nice again
            Dim l As Single = -1
            Dim t As Object = Nothing, r As Object = Nothing, b As Object = Nothing

            PicTwainPro1.GetImageLayout(l, t, r, b)

            textBoxIL.Text = l.ToString()
            textBoxIT.Text = t.ToString()
            textBoxIR.Text = r.ToString()
            textBoxIB.Text = b.ToString()

            panelLayout.Visible = True
            panelLayout.BringToFront()
        Else
            buttonShowLayout.Text = "Show Layout"
            panelLayout.Visible = False
        End If
    End Sub

    Private Sub buttonShowCaption_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonShowCaption.Click
        If (panelCaption.Visible = False) Then
            buttonShowLayout.Text = "Show Layout"
            buttonShowCaps.Text = "Show Capabilities"
            buttonShowCaption.Text = "Hide Caption"
            panelCaps.Visible = False
            panelLayout.Visible = False
            panelCaption.Visible = True
            panelCaption.BringToFront()
        Else
            buttonShowCaption.Text = "Show Caption"
            panelCaption.Visible = False
        End If
    End Sub

    Private Sub buttonSelectSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSelectSource.Click
        Try
            labelStatus.Text = ""
            PicTwainPro1.CloseSession()
            buttonShowCaps.Enabled = False
            buttonShowLayout.Enabled = False
            PicTwainPro1.SelectSource()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()

        End Try
    End Sub

    Private Sub UpdateProps()
        ' Copy values from fields to TwainPRO control
        PicTwainPro1.ShowUI = checkBoxShowUI.Checked
        PicTwainPro1.Caption = textBoxCaption.Text
        PicTwainPro1.ClipCaption = checkBoxClipCaption.Checked
        PicTwainPro1.ShadowText = checkBoxShadowText.Checked

        PicTwainPro1.CaptionLeft = Int32.Parse(textBoxCapLeft.Text)
        PicTwainPro1.CaptionTop = Int32.Parse(textBoxCapTop.Text)
        PicTwainPro1.CaptionWidth = Int32.Parse(textBoxCapWidth.Text)
        PicTwainPro1.CaptionHeight = Int32.Parse(textBoxCapHeight.Text)

        '			picTwainPRO1.HAlign = (tagenumHAlign) comboBoxHAlign.SelectedIndex;
        '			picTwainPRO1.VAlign = (tagenumVAlign) comboBoxVAlign.SelectedIndex;
    End Sub

    Private Sub comboBoxCaps_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles comboBoxCaps.SelectedIndexChanged
        ' Only process when session is open and FrmCaps can only be visible
        ' after OpenSession is called
        If (panelCaps.Visible = False) Then
            Return
        End If

        ' Reset all the controls in FrmCaps
        labelDefault.Visible = False
        labelMin.Visible = False
        labelMax.Visible = False
        listBoxCaps.Visible = False
        listBoxCaps.Items.Clear()

        textBoxCurrent.Text = ""
        buttonUpdate.Enabled = False
        PicTwainPro1.Capability = comboBoxCaps.SelectedIndex

        ' Check for support and stop if none
        If (PicTwainPro1.CapSupported = False) Then
            labelDefault.Text = "Not Supported"
            labelDefault.Visible = True
            Return
        End If

        ' What data type is the Cap?  We have to check the data type
        ' to know which properties are valid
        Select Case (PicTwainPro1.CapType)
            ' Type ONEVALUE only returns a single value
        Case tagenumCapType.TWCAP_ONEVALUE
                textBoxCurrent.Text = PicTwainPro1.CapValue.ToString()
                textBoxCurrent.Visible = True
                Exit Select

                ' Type ENUM returns a list of legal values as well as current and
                ' default values.  A list of constants is returned and the CapDesc
                ' property can be used to find out what the constants mean
            Case tagenumCapType.TWCAP_ENUM
                textBoxCurrent.Text = PicTwainPro1.CapValue.ToString()
                textBoxCurrent.Visible = True

                labelDefault.Text = "Default = " + PicTwainPro1.CapDefault.ToString()
                labelDefault.Visible = True

                listBoxCaps.Items.Add("Legal Values:")

                Dim nIndex As Integer
                For nIndex = 0 To PicTwainPro1.CapNumItems - 1 Step 1
                    Dim fResult As Single = PicTwainPro1.CapItem_Get(nIndex)
                    Dim strResult As String = PicTwainPro1.CapDesc_Get(nIndex)

                    listBoxCaps.Items.Add(fResult.ToString() + " - " + strResult.ToString())
                Next
                listBoxCaps.Visible = True
                Exit Select

                ' Type ARRAY returns a list of values, but no current or default values
                ' This is a less common type that many sources don't use
            Case tagenumCapType.TWCAP_ARRAY
                listBoxCaps.Items.Add("Legal Values:")
                Dim nIndex As Integer
                For nIndex = 0 To PicTwainPro1.CapNumItems - 1 Step 1

                    Dim fResult As Single = PicTwainPro1.CapItem_Get(nIndex)
                    listBoxCaps.Items.Add(fResult)
                Next
                Exit Select

                ' Returns a range of values as well as current and default values
            Case tagenumCapType.TWCAP_RANGE
                textBoxCurrent.Text = PicTwainPro1.CapValue.ToString()
                textBoxCurrent.Visible = True

                labelDefault.Text = "Default = " + PicTwainPro1.CapDefault.ToString()
                labelDefault.Visible = True

                labelMin.Text = "MinValue = " + PicTwainPro1.CapMin.ToString()
                labelMin.Visible = True

                labelMax.Text = "MaxValue = " + PicTwainPro1.CapMax.ToString()
                labelMax.Visible = True
                Exit Select
        End Select

        buttonUpdate.Enabled = True
    End Sub

    Private Sub buttonUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonUpdate.Click
        Try
            labelStatus.Text = ""

            ' In this sample we only set the current value.  Other data types can
            ' be used to provide the Source with a new list of legal values to
            ' present the user when the UI is enabled, however this requires strict
            ' Twain compliance by the Source which is not found in many Twain drivers
            PicTwainPro1.CapTypeOut = PegasusImaging.WinForms.TwainPro4.tagenumCapType.TWCAP_ONEVALUE
            PicTwainPro1.CapValueOut = System.Single.Parse(textBoxCurrent.Text)
            PicTwainPro1.SetCapOut()

            ' Force an update of the Caps frame with the new values
            comboBoxCaps.SelectedIndex = comboBoxCaps.SelectedIndex

        Catch exception As FormatException
            MessageBox.Show("You must enter a real number for the new current value.")
            GetError()
        End Try
    End Sub

    Private Sub buttonLayoutUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonLayoutUpdate.Click
        Try
            labelStatus.Text = ""
            Dim l As Single = System.Single.Parse(textBoxIL.Text)
            Dim t As Single = System.Single.Parse(textBoxIT.Text)
            Dim r As Single = System.Single.Parse(textBoxIR.Text)
            Dim b As Single = System.Single.Parse(textBoxIB.Text)

            PicTwainPro1.SetImageLayout(l, t, r, b)
        Catch exception As FormatException
            MessageBox.Show("You must enter a real number for each field.")
            GetError()
        End Try
    End Sub

    Private Sub FormMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PicTwainPro1.Debug = True
    End Sub

    Private Sub picTwainPRO1_PostScandotNet(ByVal classPtr As Integer, ByVal objID As Integer, ByVal Cancel As Boolean) Handles PicTwainPro1.PostScandotNet
        If (checkBoxSaveAsBMP.Checked) Then
            Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
            Dim strPath As String = System.IO.Path.Combine(strCurrentDir, "image-" + _nImageCount.ToString() + ".bmp")
            PicTwainPro1.SaveFile(strPath)
        End If

        _nImageCount += 1

        Dim viewer As FormViewer = New FormViewer()
        viewer.PrimaryTP = PicTwainPro1
        viewer.AddImage()

        Application.DoEvents()
    End Sub

    ' Update the status bar with the current error code and description
    Sub GetError()
        If (PicTwainPro1.ErrorCode <> 0) Then

            labelStatus.Text = "Data Source Error " & PicTwainPro1.ErrorCode & ": " & PicTwainPro1.ErrorString

        End If
    End Sub

    Private Sub mnu_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Quit.Click
        Application.Exit()
    End Sub

    Private Sub mnu_About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_About.Click
        PicTwainPro1.About()
    End Sub


End Class

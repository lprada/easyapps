//****************************************************************'
//* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
//* This sample code is provided to Pegasus licensees "as is"    *'
//* with no restrictions on use or modification. No warranty for *'
//* use of this sample code is provided by Pegasus.              *'
//****************************************************************'

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.TwainPro4;

namespace TwainBasicsCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonGetW;
		private System.Windows.Forms.Button buttonGetWO;
		private System.Windows.Forms.PictureBox pictureBox;
		private PegasusImaging.WinForms.TwainPro4.PICTwainPro picTwainPro1;
		private System.Windows.Forms.ListBox lstInfo;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		

	
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			// Before the call to InitializeComponent, we have to unlock
			UnlockPICTwainPro.PS_Unlock (123456789, 123456789, 123456789, 123456789);

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		   picTwainPro1.hParentWnd = this.Handle.ToInt32();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
				picTwainPro1.Dispose ();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonGetW = new System.Windows.Forms.Button();
			this.buttonGetWO = new System.Windows.Forms.Button();
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.picTwainPro1 = new PegasusImaging.WinForms.TwainPro4.PICTwainPro();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// buttonGetW
			// 
			this.buttonGetW.Location = new System.Drawing.Point(80, 472);
			this.buttonGetW.Name = "buttonGetW";
			this.buttonGetW.Size = new System.Drawing.Size(192, 32);
			this.buttonGetW.TabIndex = 1;
			this.buttonGetW.Text = "Get Image With Device Dialog";
			this.buttonGetW.Click += new System.EventHandler(this.buttonGetW_Click);
			// 
			// buttonGetWO
			// 
			this.buttonGetWO.Location = new System.Drawing.Point(296, 472);
			this.buttonGetWO.Name = "buttonGetWO";
			this.buttonGetWO.Size = new System.Drawing.Size(192, 32);
			this.buttonGetWO.TabIndex = 2;
			this.buttonGetWO.Text = "Get Image Without Device Dialog";
			this.buttonGetWO.Click += new System.EventHandler(this.buttonGetWO_Click);
			// 
			// pictureBox
			// 
			this.pictureBox.BackColor = System.Drawing.SystemColors.ControlDark;
			this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox.Location = new System.Drawing.Point(16, 80);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(536, 376);
			this.pictureBox.TabIndex = 3;
			this.pictureBox.TabStop = false;
			// 
			// picTwainPro1
			// 
			this.picTwainPro1.Caption = "";
			this.picTwainPro1.CaptionHeight = 0;
			this.picTwainPro1.CaptionLeft = 0;
			this.picTwainPro1.CaptionTop = 0;
			this.picTwainPro1.CaptionWidth = 0;
			this.picTwainPro1.ClipCaption = false;
			this.picTwainPro1.CloseOnCancel = true;
			this.picTwainPro1.Debug = false;
			this.picTwainPro1.EnableExtCaps = true;
			this.picTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic;
			this.picTwainPro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picTwainPro1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(61)), ((System.Byte)(85)), ((System.Byte)(58)));
			this.picTwainPro1.FTPPassword = "";
			this.picTwainPro1.FTPUserName = "";
			this.picTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft;
			this.picTwainPro1.Manufacturer = "";
			this.picTwainPro1.MaxImages = -1;
			this.picTwainPro1.PICPassword = "";
			this.picTwainPro1.ProductFamily = "";
			this.picTwainPro1.ProductName = "";
			this.picTwainPro1.ProxyServer = "";
			this.picTwainPro1.SaveJPEGChromFactor = 36;
			this.picTwainPro1.SaveJPEGLumFactor = 32;
			this.picTwainPro1.SaveJPEGProgressive = false;
			this.picTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411;
			this.picTwainPro1.SaveMultiPage = false;
			this.picTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed;
			this.picTwainPro1.ShadowText = true;
			this.picTwainPro1.ShowUI = true;
			this.picTwainPro1.SN = "PEXHJ700AA-PEG0Q000195";
			this.picTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE;
			this.picTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop;
			this.picTwainPro1.VersionInfo = "";
			this.picTwainPro1.PostScandotNet += new PegasusImaging.WinForms.TwainPro4.PostScanDelegate(this.picTwainPro1_PostScandotNet);
			// 
			// lstInfo
			// 
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Aquiring an image with or without the scanner\'s UI."});
			this.lstInfo.Location = new System.Drawing.Point(16, 8);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(536, 56);
			this.lstInfo.TabIndex = 4;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click_1);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(568, 517);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstInfo,
																		  this.pictureBox,
																		  this.buttonGetWO,
																		  this.buttonGetW});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "FormMain";
			this.Text = "TwainPRO - Basic Sample";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread()]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void buttonGetW_Click(object sender, System.EventArgs e)
		{
			// Run with Driver Dialog Box
			try
			{
				picTwainPro1.SelectSource ();
				picTwainPro1.ShowUI = true;
				picTwainPro1.StartSession ();
			}
			catch(Exception ex)
			{	
				MessageBox.Show(ex.Message);
			}
		}

		private void buttonGetWO_Click(object sender, System.EventArgs e)
		{
			  // Run with Driver Dialog Box
			try
			{
				picTwainPro1.SelectSource ();
				picTwainPro1.ShowUI = false;
				picTwainPro1.StartSession ();
			}
			catch(Exception ex)
			{	
				MessageBox.Show(ex.Message);
			}
		}


		

		private void picTwainPro1_PostScandotNet(int classPtr, int objID, bool Cancel)
		{
			
			pictureBox.Image = picTwainPro1.Picture;
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
		
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			picTwainPro1.About();
		}

		private void mnu_Quit_Click_1(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

						
	}
}

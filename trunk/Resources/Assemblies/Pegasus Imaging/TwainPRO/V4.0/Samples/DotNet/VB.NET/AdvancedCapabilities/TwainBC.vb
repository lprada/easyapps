'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.TwainPro4

Public Class Form1
    Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        'This call is required!
        PicTwainPro1.hParentWnd = Me.Handle.ToInt32()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNumBCs As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lstBCTypes As System.Windows.Forms.ListBox
    Friend WithEvents lstBCStrings As System.Windows.Forms.ListBox
    Friend WithEvents cmdSelSource As System.Windows.Forms.Button
    Friend WithEvents cmdScan As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnu_File As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Quit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_About As System.Windows.Forms.MenuItem
    Friend WithEvents PicTwainPro1 As PegasusImaging.WinForms.TwainPro4.PICTwainPro
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblNumBCs = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lstBCTypes = New System.Windows.Forms.ListBox()
        Me.lstBCStrings = New System.Windows.Forms.ListBox()
        Me.cmdSelSource = New System.Windows.Forms.Button()
        Me.cmdScan = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnu_File = New System.Windows.Forms.MenuItem()
        Me.mnu_Quit = New System.Windows.Forms.MenuItem()
        Me.mnu_About = New System.Windows.Forms.MenuItem()
        Me.PicTwainPro1 = New PegasusImaging.WinForms.TwainPro4.PICTwainPro()
        Me.SuspendLayout()
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Setting an advanced TWAIN capability via the TWCAP_USECAPADVANCED capability.", "", "Note: There are many other advanced capabilities in addition to the ICAP_BARCODED" & _
        "ETECTIONENABLED capability ", "used in this sample.  Please see the Advanced Capabilities Constants section of t" & _
        "he TwainPRO Help File."})
        Me.lstInfo.Location = New System.Drawing.Point(16, 16)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(648, 93)
        Me.lstInfo.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(160, 136)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 32)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Num Barcodes:"
        '
        'lblNumBCs
        '
        Me.lblNumBCs.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblNumBCs.Location = New System.Drawing.Point(328, 136)
        Me.lblNumBCs.Name = "lblNumBCs"
        Me.lblNumBCs.Size = New System.Drawing.Size(104, 24)
        Me.lblNumBCs.TabIndex = 2
        Me.lblNumBCs.Text = "0"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(160, 200)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 24)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "BarCode Types"
        '
        'Label3
        '
        Me.Label3.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.Label3.Location = New System.Drawing.Point(328, 200)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 24)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "BarCode Strings"
        '
        'lstBCTypes
        '
        Me.lstBCTypes.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left)
        Me.lstBCTypes.Location = New System.Drawing.Point(152, 241)
        Me.lstBCTypes.Name = "lstBCTypes"
        Me.lstBCTypes.Size = New System.Drawing.Size(136, 56)
        Me.lstBCTypes.TabIndex = 5
        '
        'lstBCStrings
        '
        Me.lstBCStrings.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstBCStrings.Location = New System.Drawing.Point(320, 241)
        Me.lstBCStrings.Name = "lstBCStrings"
        Me.lstBCStrings.Size = New System.Drawing.Size(160, 56)
        Me.lstBCStrings.TabIndex = 6
        '
        'cmdSelSource
        '
        Me.cmdSelSource.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.cmdSelSource.Location = New System.Drawing.Point(168, 313)
        Me.cmdSelSource.Name = "cmdSelSource"
        Me.cmdSelSource.Size = New System.Drawing.Size(104, 40)
        Me.cmdSelSource.TabIndex = 7
        Me.cmdSelSource.Text = "Select Source"
        '
        'cmdScan
        '
        Me.cmdScan.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.cmdScan.Location = New System.Drawing.Point(336, 313)
        Me.cmdScan.Name = "cmdScan"
        Me.cmdScan.Size = New System.Drawing.Size(120, 40)
        Me.cmdScan.TabIndex = 8
        Me.cmdScan.Text = "Scan"
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStatus.Location = New System.Drawing.Point(72, 368)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(472, 24)
        Me.lblStatus.TabIndex = 9
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_File, Me.mnu_About})
        '
        'mnu_File
        '
        Me.mnu_File.Index = 0
        Me.mnu_File.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_Quit})
        Me.mnu_File.Text = "File"
        '
        'mnu_Quit
        '
        Me.mnu_Quit.Index = 0
        Me.mnu_Quit.Text = "Quit"
        '
        'mnu_About
        '
        Me.mnu_About.Index = 1
        Me.mnu_About.Text = "About"
        '
        'PicTwainPro1
        '
        Me.PicTwainPro1.Caption = ""
        Me.PicTwainPro1.CaptionHeight = 0
        Me.PicTwainPro1.CaptionLeft = 0
        Me.PicTwainPro1.CaptionTop = 0
        Me.PicTwainPro1.CaptionWidth = 0
        Me.PicTwainPro1.ClipCaption = False
        Me.PicTwainPro1.CloseOnCancel = True
        Me.PicTwainPro1.Debug = False
        Me.PicTwainPro1.EnableExtCaps = True
        Me.PicTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic
        Me.PicTwainPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicTwainPro1.ForeColor = System.Drawing.Color.FromArgb(CType(86, Byte), CType(69, Byte), CType(61, Byte))
        Me.PicTwainPro1.FTPPassword = ""
        Me.PicTwainPro1.FTPUserName = ""
        Me.PicTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft
        Me.PicTwainPro1.Manufacturer = ""
        Me.PicTwainPro1.MaxImages = -1
        Me.PicTwainPro1.PICPassword = ""
        Me.PicTwainPro1.ProductFamily = ""
        Me.PicTwainPro1.ProductName = ""
        Me.PicTwainPro1.ProxyServer = ""
        Me.PicTwainPro1.SaveJPEGChromFactor = 36
        Me.PicTwainPro1.SaveJPEGLumFactor = 32
        Me.PicTwainPro1.SaveJPEGProgressive = False
        Me.PicTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411
        Me.PicTwainPro1.SaveMultiPage = False
        Me.PicTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed
        Me.PicTwainPro1.ShadowText = True
        Me.PicTwainPro1.ShowUI = True
        Me.PicTwainPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE
        Me.PicTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop
        Me.PicTwainPro1.VersionInfo = ""
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(682, 401)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblStatus, Me.cmdScan, Me.cmdSelSource, Me.lstBCStrings, Me.lstBCTypes, Me.Label3, Me.Label2, Me.lblNumBCs, Me.Label1, Me.lstInfo})
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "TwainBC"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private gbBarcodeSupported As Boolean

    Private Sub InitBarcode()

        Try
            'Enable the barcode capability
            PicTwainPro1.Capability = tagenumCapability.TWCAP_USECAPADVANCED
            PicTwainPro1.CapAdvanced = tagenumAdvancedCapability.ICAP_BARCODEDETECTIONENABLED
            If (PicTwainPro1.CapSupported) Then

                gbBarcodeSupported = True
                'Turn on barcode detection
                PicTwainPro1.CapValueOut = 1
                PicTwainPro1.SetCapOut()

                PicTwainPro1.CapExtInfoCount = 4
                PicTwainPro1.Capability = tagenumCapability.TWCAP_USECAPADVANCEDFOREXTINFO

                PicTwainPro1.CapExtInfoIndex = 0
                PicTwainPro1.CapAdvanced = tagenumExtendedInfo.TWEI_BARCODECOUNT

                PicTwainPro1.CapExtInfoIndex = 1
                PicTwainPro1.CapAdvanced = tagenumExtendedInfo.TWEI_BARCODETYPE

                PicTwainPro1.CapExtInfoIndex = 2
                PicTwainPro1.CapAdvanced = tagenumExtendedInfo.TWEI_BARCODETEXTLENGTH

                PicTwainPro1.CapExtInfoIndex = 3
                PicTwainPro1.CapAdvanced = tagenumExtendedInfo.TWEI_BARCODETEXT

            Else
                gbBarcodeSupported = False
            End If

            Exit Sub


        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PicTwainPro1.Debug = True

    End Sub

    


    Private Sub cmdSelSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelSource.Click

        Try

            PicTwainPro1.SelectSource()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub


    Private Sub cmdScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdScan.Click

        lblStatus.Text = ""

        Try
            PicTwainPro1.OpenSession()
            InitBarcode()


            PicTwainPro1.StartSession()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try
    End Sub

    ' Update the status bar with the current error code and description
    Sub GetError()
        If (PicTwainPro1.ErrorCode <> 0) Then

            lblStatus.Text = "Data Source Error " & PicTwainPro1.ErrorCode & ": " & PicTwainPro1.ErrorString
        End If
    End Sub

    Private Sub mnu_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Quit.Click
        Application.Exit()
    End Sub

    Private Sub mnu_About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_About.Click
        PicTwainPro1.About()
    End Sub

    Private Sub PicTwainPro1_PostScandotNet(ByVal classPtr As Integer, ByVal objID As Integer, ByVal Cancel As Boolean) Handles PicTwainPro1.PostScandotNet

        Dim nIndex As Long
        Dim alBCTypes As New ArrayList()
        Dim alBCStrings As New ArrayList()

        Try


            If (gbBarcodeSupported) Then
                'Get the Extended Image Info
                PicTwainPro1.GetExtInfo()
                'Now the ExtInfo structure has been filled

                ' Get the Barcode Count
                PicTwainPro1.CapExtInfoIndex = 0
                If (PicTwainPro1.CapSupported) Then
                    lblNumBCs.Text = PicTwainPro1.CapValue
                End If

                ' Get the Barcode Type
                PicTwainPro1.CapExtInfoIndex = 1
                If (PicTwainPro1.CapSupported) Then
                    For nIndex = 0 To PicTwainPro1.CapNumItems - 1
                        Select Case PicTwainPro1.CapItem_Get(nIndex)
                            Case tagenumExtInfoConstants.TWBT_3OF9
                                alBCTypes.Add("TWBT_3OF9")
                                ' lstBCTypes.AddItem("TWBT_3OF9")

                            Case tagenumExtInfoConstants.TWBT_2OF5INTERLEAVED
                                alBCTypes.Add("TWBT_2OF5INTERLEAVED")

                            Case tagenumExtInfoConstants.TWBT_2OF5NONINTERLEAVED
                                alBCTypes.Add("TWBT_2OF5NONINTERLEAVED")

                            Case tagenumExtInfoConstants.TWBT_CODE93
                                alBCTypes.Add("TWBT_CODE93")

                            Case tagenumExtInfoConstants.TWBT_CODE128
                                alBCTypes.Add("TWBT_CODE128")

                            Case tagenumExtInfoConstants.TWBT_UCC128
                                alBCTypes.Add("TWBT_UCC128")

                            Case tagenumExtInfoConstants.TWBT_CODABAR
                                alBCTypes.Add("TWBT_CODABAR")

                            Case tagenumExtInfoConstants.TWBT_UPCA
                                alBCTypes.Add("TWBT_UPCA")

                            Case tagenumExtInfoConstants.TWBT_UPCE
                                alBCTypes.Add("TWBT_UPCE")
                        End Select
                    Next
                End If

                lstBCTypes.DataSource = alBCTypes


                'Get the Barcode Strings
                PicTwainPro1.CapExtInfoIndex = 3
                If (PicTwainPro1.CapSupported) Then
                    For nIndex = 0 To PicTwainPro1.CapNumItems - 1
                        alBCStrings.Add(PicTwainPro1.CapItemString_Get(nIndex))
                    Next
                End If

                lstBCStrings.DataSource = alBCStrings

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            GetError()
        End Try

    End Sub
End Class

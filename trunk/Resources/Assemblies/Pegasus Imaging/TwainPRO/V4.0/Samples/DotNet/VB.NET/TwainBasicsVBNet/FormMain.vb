Public Class FormMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        ' Before the call to InitializeComponent, we have to unlock
        'UnlockPICTwainPro.PS_Unlock(1234,1234,1234,1234)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        PicTwainPro1.hParentWnd = Me.Handle.ToInt32()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            PicTwainPro1.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonGetWO As System.Windows.Forms.Button
    Friend WithEvents buttonGetW As System.Windows.Forms.Button
    Friend WithEvents TwainProMain As PegasusImaging.WinForms.TwainPro4.PICTwainPro

    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PicTwainPro1 As PegasusImaging.WinForms.TwainPro4.PICTwainPro
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Quit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_About As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonGetWO = New System.Windows.Forms.Button()
        Me.buttonGetW = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PicTwainPro1 = New PegasusImaging.WinForms.TwainPro4.PICTwainPro()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnu_Quit = New System.Windows.Forms.MenuItem()
        Me.mnu_About = New System.Windows.Forms.MenuItem()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'buttonGetWO
        '
        Me.buttonGetWO.Location = New System.Drawing.Point(296, 480)
        Me.buttonGetWO.Name = "buttonGetWO"
        Me.buttonGetWO.Size = New System.Drawing.Size(192, 32)
        Me.buttonGetWO.TabIndex = 5
        Me.buttonGetWO.Text = "Get Image Without Device Dialog"
        '
        'buttonGetW
        '
        Me.buttonGetW.Location = New System.Drawing.Point(80, 480)
        Me.buttonGetW.Name = "buttonGetW"
        Me.buttonGetW.Size = New System.Drawing.Size(192, 32)
        Me.buttonGetW.TabIndex = 4
        Me.buttonGetW.Text = "Get Image With Device Dialog"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(16, 80)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(544, 352)
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'PicTwainPro1
        '
        Me.PicTwainPro1.Caption = ""
        Me.PicTwainPro1.CaptionHeight = 0
        Me.PicTwainPro1.CaptionLeft = 0
        Me.PicTwainPro1.CaptionTop = 0
        Me.PicTwainPro1.CaptionWidth = 0
        Me.PicTwainPro1.ClipCaption = False
        Me.PicTwainPro1.CloseOnCancel = True
        Me.PicTwainPro1.Debug = False
        Me.PicTwainPro1.EnableExtCaps = True
        Me.PicTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic
        Me.PicTwainPro1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicTwainPro1.ForeColor = System.Drawing.Color.FromArgb(CType(73, Byte), CType(86, Byte), CType(69, Byte))
        Me.PicTwainPro1.FTPPassword = ""
        Me.PicTwainPro1.FTPUserName = ""
        Me.PicTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft
        Me.PicTwainPro1.Manufacturer = ""
        Me.PicTwainPro1.MaxImages = -1
        Me.PicTwainPro1.PICPassword = ""
        Me.PicTwainPro1.ProductFamily = ""
        Me.PicTwainPro1.ProductName = ""
        Me.PicTwainPro1.ProxyServer = ""
        Me.PicTwainPro1.SaveJPEGChromFactor = 36
        Me.PicTwainPro1.SaveJPEGLumFactor = 32
        Me.PicTwainPro1.SaveJPEGProgressive = False
        Me.PicTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411
        Me.PicTwainPro1.SaveMultiPage = False
        Me.PicTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed
        Me.PicTwainPro1.ShadowText = True
        Me.PicTwainPro1.ShowUI = True
        Me.PicTwainPro1.SN = "PEXHJ700AA-PEG0Q000195"
        Me.PicTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE
        Me.PicTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop
        Me.PicTwainPro1.VersionInfo = ""
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.mnu_About})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_Quit})
        Me.MenuItem1.Text = "File"
        '
        'mnu_Quit
        '
        Me.mnu_Quit.Index = 0
        Me.mnu_Quit.Text = "Quit"
        '
        'mnu_About
        '
        Me.mnu_About.Index = 1
        Me.mnu_About.Text = "About"
        '
        'lstInfo
        '
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Aquiring an image with or without the scanner's UI."})
        Me.lstInfo.Location = New System.Drawing.Point(8, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(544, 43)
        Me.lstInfo.TabIndex = 8
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(568, 525)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstInfo, Me.PictureBox1, Me.buttonGetWO, Me.buttonGetW})
        Me.Menu = Me.MainMenu1
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TwainPRO - Basics Sample"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub buttonGetW_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonGetW.Click
        ' Run with Driver Dialog Box
        Try
            PicTwainPro1.SelectSource()
            PicTwainPro1.ShowUI = True
            PicTwainPro1.StartSession()
        Catch exception As System.Runtime.InteropServices.SEHException
            MessageBox.Show(exception.Message)
        End Try
    End Sub

    Private Sub buttonGetWO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonGetWO.Click
        ' Run without Driver Dialog Box
        Try
            PicTwainPro1.SelectSource()
            PicTwainPro1.ShowUI = False
            PicTwainPro1.StartSession()
        Catch exception As System.Runtime.InteropServices.SEHException
            MessageBox.Show(exception.Message)
        End Try
    End Sub

    Private Sub PicTwainPro1_PostScandotNet(ByVal classPtr As Integer, ByVal objID As Integer, ByVal Cancel As Boolean) Handles PicTwainPro1.PostScandotNet
        PictureBox1.Image = PicTwainPro1.Picture
    End Sub

    Private Sub mnu_Quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Quit.Click
        Application.Exit()
    End Sub

    Private Sub mnu_About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_About.Click
        PicTwainPro1.About()
    End Sub

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

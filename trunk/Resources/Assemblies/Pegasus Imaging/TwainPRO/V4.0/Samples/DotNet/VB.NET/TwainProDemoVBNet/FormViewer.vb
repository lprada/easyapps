Imports PegasusImaging.WinForms.ImagXpress8

Public Class FormViewer
    Inherits System.Windows.Forms.Form

    Private _tp As PegasusImaging.WinForms.TwainPro4.PICTwainPro

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.


    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.AlignHorizontal = PegasusImaging.WinForms.ImagXpress8.AlignHorizontal.Center
        Me.ImageXView1.AlignVertical = PegasusImaging.WinForms.ImagXpress8.AlignVertical.Center
        Me.ImageXView1.AllowUpdate = True
        Me.ImageXView1.Antialias = True
        Me.ImageXView1.AspectX = 1
        Me.ImageXView1.AspectY = 1
        Me.ImageXView1.AutoImageDispose = False
        Me.ImageXView1.BorderType = PegasusImaging.WinForms.ImagXpress8.BorderType.Inset
        Me.ImageXView1.Brightness = 0
        Me.ImageXView1.ClipRectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.ImageXView1.Contrast = 0
        Me.ImageXView1.Dithered = True
        Me.ImageXView1.GrayMode = PegasusImaging.WinForms.ImagXpress8.GrayMode.Standard
        Me.ImageXView1.Location = New System.Drawing.Point(16, 8)
        Me.ImageXView1.Maximum = New System.Drawing.Size(0, 0)
        Me.ImageXView1.Minimum = New System.Drawing.Size(1, 1)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Progressive = False
        Me.ImageXView1.Size = New System.Drawing.Size(440, 344)
        Me.ImageXView1.Smoothing = True
        Me.ImageXView1.TabIndex = 0
        Me.ImageXView1.ZoomFactor = 1.0!
        '
        'FormViewer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(464, 357)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.ImageXView1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormViewer"
        Me.Text = "FormViewer"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Sub AddImage()
        ImageXView1.AutoScroll = True
        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(_tp.hDIB))

        If (Me.Visible = False) Then
            Me.ShowDialog()
        End If
    End Sub

    Public Property PrimaryTP() As PegasusImaging.WinForms.TwainPro4.PICTwainPro
        Get
            Return _tp
        End Get
        Set(ByVal Value As PegasusImaging.WinForms.TwainPro4.PICTwainPro)
            _tp = Value
        End Set
    End Property

    Private Sub FormViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

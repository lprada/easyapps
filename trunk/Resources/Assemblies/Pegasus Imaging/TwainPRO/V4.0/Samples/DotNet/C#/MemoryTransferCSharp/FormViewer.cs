/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress8;

namespace TwainProMemCSharp
{
	/// <summary>
	/// Summary description for FormViewer.
	/// </summary>
	public class FormViewer : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button buttonZoomIn;
		private System.Windows.Forms.Button buttonMirror;
		private System.Windows.Forms.Button buttonFlip;
		private System.Windows.Forms.Button buttonInvert;
		private System.Windows.Forms.Button buttonDeskew;
		private System.Windows.Forms.Button buttonDespeckle;
		private System.Windows.Forms.Button buttonZoomOut;
		private System.Windows.Forms.Button buttonFit;

		private System.Double dZoomFactor;

		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.Processor processor1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormViewer()
		{

			InitializeComponent();

			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{	
				// Don't forget to dispose IX
				//

				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				
				if (processor1 != null)
				{
					processor1.Dispose();
					processor1 = null;
				}
				if (imageXView1 != null)
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonZoomIn = new System.Windows.Forms.Button();
			this.buttonMirror = new System.Windows.Forms.Button();
			this.buttonFlip = new System.Windows.Forms.Button();
			this.buttonInvert = new System.Windows.Forms.Button();
			this.buttonDeskew = new System.Windows.Forms.Button();
			this.buttonDespeckle = new System.Windows.Forms.Button();
			this.buttonZoomOut = new System.Windows.Forms.Button();
			this.buttonFit = new System.Windows.Forms.Button();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.processor1 = new PegasusImaging.WinForms.ImagXpress8.Processor();
			this.SuspendLayout();
			// 
			// buttonZoomIn
			// 
			this.buttonZoomIn.Name = "buttonZoomIn";
			this.buttonZoomIn.Size = new System.Drawing.Size(64, 24);
			this.buttonZoomIn.TabIndex = 1;
			this.buttonZoomIn.Text = "Zoom In";
			this.buttonZoomIn.Click += new System.EventHandler(this.buttonZoomIn_Click);
			// 
			// buttonMirror
			// 
			this.buttonMirror.Location = new System.Drawing.Point(456, 0);
			this.buttonMirror.Name = "buttonMirror";
			this.buttonMirror.Size = new System.Drawing.Size(64, 24);
			this.buttonMirror.TabIndex = 2;
			this.buttonMirror.Text = "Mirror";
			this.buttonMirror.Click += new System.EventHandler(this.buttonMirror_Click);
			// 
			// buttonFlip
			// 
			this.buttonFlip.Location = new System.Drawing.Point(392, 0);
			this.buttonFlip.Name = "buttonFlip";
			this.buttonFlip.Size = new System.Drawing.Size(64, 24);
			this.buttonFlip.TabIndex = 3;
			this.buttonFlip.Text = "Flip";
			this.buttonFlip.Click += new System.EventHandler(this.buttonFlip_Click);
			// 
			// buttonInvert
			// 
			this.buttonInvert.Location = new System.Drawing.Point(328, 0);
			this.buttonInvert.Name = "buttonInvert";
			this.buttonInvert.Size = new System.Drawing.Size(64, 24);
			this.buttonInvert.TabIndex = 4;
			this.buttonInvert.Text = "Invert";
			this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
			// 
			// buttonDeskew
			// 
			this.buttonDeskew.Location = new System.Drawing.Point(264, 0);
			this.buttonDeskew.Name = "buttonDeskew";
			this.buttonDeskew.Size = new System.Drawing.Size(64, 24);
			this.buttonDeskew.TabIndex = 5;
			this.buttonDeskew.Text = "Deskew";
			this.buttonDeskew.Click += new System.EventHandler(this.buttonDeskew_Click);
			// 
			// buttonDespeckle
			// 
			this.buttonDespeckle.Location = new System.Drawing.Point(192, 0);
			this.buttonDespeckle.Name = "buttonDespeckle";
			this.buttonDespeckle.Size = new System.Drawing.Size(72, 24);
			this.buttonDespeckle.TabIndex = 6;
			this.buttonDespeckle.Text = "Despeckle";
			this.buttonDespeckle.Click += new System.EventHandler(this.buttonDespeckle_Click);
			// 
			// buttonZoomOut
			// 
			this.buttonZoomOut.Location = new System.Drawing.Point(128, 0);
			this.buttonZoomOut.Name = "buttonZoomOut";
			this.buttonZoomOut.Size = new System.Drawing.Size(64, 24);
			this.buttonZoomOut.TabIndex = 7;
			this.buttonZoomOut.Text = "Zoom Out";
			this.buttonZoomOut.Click += new System.EventHandler(this.buttonZoomOut_Click);
			// 
			// buttonFit
			// 
			this.buttonFit.Location = new System.Drawing.Point(64, 0);
			this.buttonFit.Name = "buttonFit";
			this.buttonFit.Size = new System.Drawing.Size(64, 24);
			this.buttonFit.TabIndex = 8;
			this.buttonFit.Text = "Fit";
			this.buttonFit.Click += new System.EventHandler(this.buttonFit_Click);
			// 
			// imageXView1
			// 
			this.imageXView1.Location = new System.Drawing.Point(16, 40);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(512, 344);
			this.imageXView1.TabIndex = 9;
			// 
			// processor1
			// 
			this.processor1.BackgroundColor = System.Drawing.Color.Black;
			this.processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast;
			this.processor1.ProgressPercent = 10;
			this.processor1.Redeyes = null;
			// 
			// FormViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(538, 429);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.imageXView1,
																		  this.buttonFit,
																		  this.buttonZoomOut,
																		  this.buttonDespeckle,
																		  this.buttonDeskew,
																		  this.buttonInvert,
																		  this.buttonFlip,
																		  this.buttonMirror,
																		  this.buttonZoomIn});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormViewer";
			this.ShowInTaskbar = false;
			this.Text = "FormViewer";
			this.Load += new System.EventHandler(this.FormViewer_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonZoomIn_Click(object sender, System.EventArgs e)
		{
		
			dZoomFactor += 0.05;
			try 
			{
				imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonFit_Click(object sender, System.EventArgs e)
		{
			
			try 
			{
				imageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest);
				dZoomFactor = imageXView1.ZoomFactor;

			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
			
		}

		private void buttonZoomOut_Click(object sender, System.EventArgs e)
		{
			dZoomFactor -= 0.05;
			try 
			{
				imageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor);
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonDespeckle_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Despeckle();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonDeskew_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Deskew();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonInvert_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Negate();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonFlip_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Flip();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		private void buttonMirror_Click(object sender, System.EventArgs e)
		{
			try 
			{
				processor1.Image = imageXView1.Image.Copy();
				processor1.Mirror();
				imageXView1.Image = processor1.Image;

			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
			{
				MessageBox.Show(eX.Message + " " + eX.Source);
			}
		}

		public void ShowEx (PegasusImaging.WinForms.ImagXpress8.ImageX im, bool bIsHDib)
		{
			if (bIsHDib)
			{
				imageXView1.Image = im;
			}
			else	
			{
				imageXView1.Image = im;
			}
			
			dZoomFactor = Convert.ToDouble(imageXView1.ZoomFactor);

			if (this.Visible == false)
			{
				this.Show ();
			}
			else
			{
				this.Invalidate ();
			}   
		
		}

		private void FormViewer_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}

/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using PegasusImaging.WinForms.ImagXpress8;

namespace TwainProMemCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		[DllImport ("kernel32.dll")]
		private static extern int GlobalFree (int hMem);

		private System.String strCurrentDir;

		private System.Windows.Forms.Label labelCompressTypes;
		private System.Windows.Forms.Label lblCompressTypes;
		private System.Windows.Forms.Button buttonScan;
		private System.Windows.Forms.Button buttonSelectSource;
		private System.Windows.Forms.ComboBox comboBoxCompression;
		private System.Windows.Forms.CheckBox checkBoxUseViewer;
		private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.Label labelJpegQual;
		private System.Windows.Forms.Label labelJpegQualVal;
		private System.Windows.Forms.HScrollBar hScrollBarJpegQual;
		private PegasusImaging.WinForms.TwainPro4.PICTwainPro twainProNet;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		internal System.Windows.Forms.ListBox lstStatus;
		internal System.Windows.Forms.Label lblLoadStatus;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.Label lblerror;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.MainMenu mainmenu;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuQuit;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private System.Windows.Forms.MenuItem mnuAbout;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			// Before the call to InitializeComponent, we have to unlock
			UnlockPICTwainPro.PS_Unlock (123456789, 123456789, 123456789, 123456789);
			
			// call the unlock function with dummy values
			// PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345, 12345, 12345, 12345);


			InitializeComponent();

			// Initialize TwainPRO to receive events
			twainProNet.hParentWnd = this.Handle.ToInt32 ();

			// Let TwainPRO know that you wish to use Buffered Memory Mode
			twainProNet.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_MEMORY;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				// Don't forget to dispose IX
				//

				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}
				if (imageXView1 != null)
				{
					imageXView1.Dispose();
					imageXView1 = null;
				}
			    if (components != null) 
				{
					components.Dispose();
				}
				if (twainProNet != null)
				{
					twainProNet.Dispose ();
				}
				}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelCompressTypes = new System.Windows.Forms.Label();
			this.checkBoxUseViewer = new System.Windows.Forms.CheckBox();
			this.hScrollBarJpegQual = new System.Windows.Forms.HScrollBar();
			this.comboBoxCompression = new System.Windows.Forms.ComboBox();
			this.buttonScan = new System.Windows.Forms.Button();
			this.buttonSelectSource = new System.Windows.Forms.Button();
			this.labelJpegQual = new System.Windows.Forms.Label();
			this.labelJpegQualVal = new System.Windows.Forms.Label();
			this.lblCompressTypes = new System.Windows.Forms.Label();
			this.labelStatus = new System.Windows.Forms.Label();
			this.twainProNet = new PegasusImaging.WinForms.TwainPro4.PICTwainPro();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lblerror = new System.Windows.Forms.Label();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.mainmenu = new System.Windows.Forms.MainMenu();
			this.mnuFile = new System.Windows.Forms.MenuItem();
			this.mnuQuit = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// labelCompressTypes
			// 
			this.labelCompressTypes.Location = new System.Drawing.Point(8, 176);
			this.labelCompressTypes.Name = "labelCompressTypes";
			this.labelCompressTypes.Size = new System.Drawing.Size(112, 16);
			this.labelCompressTypes.TabIndex = 4;
			this.labelCompressTypes.Text = "Compression Types:";
			// 
			// checkBoxUseViewer
			// 
			this.checkBoxUseViewer.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.checkBoxUseViewer.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxUseViewer.Checked = true;
			this.checkBoxUseViewer.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxUseViewer.Cursor = System.Windows.Forms.Cursors.Default;
			this.checkBoxUseViewer.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBoxUseViewer.ForeColor = System.Drawing.SystemColors.ControlText;
			this.checkBoxUseViewer.Location = new System.Drawing.Point(24, 304);
			this.checkBoxUseViewer.Name = "checkBoxUseViewer";
			this.checkBoxUseViewer.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.checkBoxUseViewer.Size = new System.Drawing.Size(129, 17);
			this.checkBoxUseViewer.TabIndex = 17;
			this.checkBoxUseViewer.Text = "Use Viewer";
			// 
			// hScrollBarJpegQual
			// 
			this.hScrollBarJpegQual.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.hScrollBarJpegQual.Cursor = System.Windows.Forms.Cursors.Default;
			this.hScrollBarJpegQual.LargeChange = 1;
			this.hScrollBarJpegQual.Location = new System.Drawing.Point(24, 276);
			this.hScrollBarJpegQual.Name = "hScrollBarJpegQual";
			this.hScrollBarJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.hScrollBarJpegQual.Size = new System.Drawing.Size(129, 17);
			this.hScrollBarJpegQual.TabIndex = 14;
			this.hScrollBarJpegQual.TabStop = true;
			this.hScrollBarJpegQual.Value = 80;
			this.hScrollBarJpegQual.ValueChanged += new System.EventHandler(this.hScrollBarJPEGQual_ValueChanged);
			// 
			// comboBoxCompression
			// 
			this.comboBoxCompression.BackColor = System.Drawing.SystemColors.Window;
			this.comboBoxCompression.Cursor = System.Windows.Forms.Cursors.Default;
			this.comboBoxCompression.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxCompression.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBoxCompression.ForeColor = System.Drawing.SystemColors.WindowText;
			this.comboBoxCompression.Location = new System.Drawing.Point(24, 208);
			this.comboBoxCompression.Name = "comboBoxCompression";
			this.comboBoxCompression.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.comboBoxCompression.Size = new System.Drawing.Size(129, 22);
			this.comboBoxCompression.TabIndex = 12;
			this.comboBoxCompression.SelectedIndexChanged += new System.EventHandler(this.comboBoxCompression_SelectedIndexChanged);
			// 
			// buttonScan
			// 
			this.buttonScan.BackColor = System.Drawing.SystemColors.Control;
			this.buttonScan.Cursor = System.Windows.Forms.Cursors.Default;
			this.buttonScan.Enabled = false;
			this.buttonScan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonScan.ForeColor = System.Drawing.SystemColors.ControlText;
			this.buttonScan.Location = new System.Drawing.Point(16, 120);
			this.buttonScan.Name = "buttonScan";
			this.buttonScan.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.buttonScan.Size = new System.Drawing.Size(121, 41);
			this.buttonScan.TabIndex = 10;
			this.buttonScan.Text = "Scan";
			this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
			// 
			// buttonSelectSource
			// 
			this.buttonSelectSource.BackColor = System.Drawing.SystemColors.Control;
			this.buttonSelectSource.Cursor = System.Windows.Forms.Cursors.Default;
			this.buttonSelectSource.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonSelectSource.ForeColor = System.Drawing.SystemColors.ControlText;
			this.buttonSelectSource.Location = new System.Drawing.Point(16, 64);
			this.buttonSelectSource.Name = "buttonSelectSource";
			this.buttonSelectSource.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.buttonSelectSource.Size = new System.Drawing.Size(121, 41);
			this.buttonSelectSource.TabIndex = 9;
			this.buttonSelectSource.Text = "Select Source";
			this.buttonSelectSource.Click += new System.EventHandler(this.buttonSelectSource_Click);
			// 
			// labelJpegQual
			// 
			this.labelJpegQual.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.labelJpegQual.BackColor = System.Drawing.SystemColors.Control;
			this.labelJpegQual.Cursor = System.Windows.Forms.Cursors.Default;
			this.labelJpegQual.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelJpegQual.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelJpegQual.Location = new System.Drawing.Point(24, 248);
			this.labelJpegQual.Name = "labelJpegQual";
			this.labelJpegQual.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.labelJpegQual.Size = new System.Drawing.Size(80, 17);
			this.labelJpegQual.TabIndex = 16;
			this.labelJpegQual.Text = "JPEG Quality:";
			// 
			// labelJpegQualVal
			// 
			this.labelJpegQualVal.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.labelJpegQualVal.BackColor = System.Drawing.SystemColors.Control;
			this.labelJpegQualVal.Cursor = System.Windows.Forms.Cursors.Default;
			this.labelJpegQualVal.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelJpegQualVal.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelJpegQualVal.Location = new System.Drawing.Point(128, 248);
			this.labelJpegQualVal.Name = "labelJpegQualVal";
			this.labelJpegQualVal.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.labelJpegQualVal.Size = new System.Drawing.Size(25, 17);
			this.labelJpegQualVal.TabIndex = 15;
			this.labelJpegQualVal.Text = "80";
			// 
			// lblCompressTypes
			// 
			this.lblCompressTypes.BackColor = System.Drawing.SystemColors.Control;
			this.lblCompressTypes.Cursor = System.Windows.Forms.Cursors.Default;
			this.lblCompressTypes.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCompressTypes.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lblCompressTypes.Location = new System.Drawing.Point(24, 176);
			this.lblCompressTypes.Name = "lblCompressTypes";
			this.lblCompressTypes.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblCompressTypes.Size = new System.Drawing.Size(121, 17);
			this.lblCompressTypes.TabIndex = 13;
			this.lblCompressTypes.Text = "Compression Types:";
			// 
			// labelStatus
			// 
			this.labelStatus.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.labelStatus.BackColor = System.Drawing.SystemColors.Control;
			this.labelStatus.Cursor = System.Windows.Forms.Cursors.Default;
			this.labelStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelStatus.ForeColor = System.Drawing.SystemColors.ControlText;
			this.labelStatus.Location = new System.Drawing.Point(28, 340);
			this.labelStatus.Name = "labelStatus";
			this.labelStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.labelStatus.Size = new System.Drawing.Size(444, 33);
			this.labelStatus.TabIndex = 11;
			this.labelStatus.Text = "Select and Scan";
			// 
			// twainProNet
			// 
			this.twainProNet.Caption = "";
			this.twainProNet.CaptionHeight = 0;
			this.twainProNet.CaptionLeft = 0;
			this.twainProNet.CaptionTop = 0;
			this.twainProNet.CaptionWidth = 0;
			this.twainProNet.ClipCaption = false;
			this.twainProNet.CloseOnCancel = true;
			this.twainProNet.Debug = false;
			this.twainProNet.EnableExtCaps = true;
			this.twainProNet.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic;
			this.twainProNet.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.twainProNet.ForeColor = System.Drawing.Color.Black;
			this.twainProNet.FTPPassword = "";
			this.twainProNet.FTPUserName = "";
			this.twainProNet.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft;
			this.twainProNet.Manufacturer = "";
			this.twainProNet.MaxImages = -1;
			this.twainProNet.PICPassword = "";
			this.twainProNet.ProductFamily = "";
			this.twainProNet.ProductName = "";
			this.twainProNet.ProxyServer = "";
			this.twainProNet.SaveJPEGChromFactor = 36;
			this.twainProNet.SaveJPEGLumFactor = 32;
			this.twainProNet.SaveJPEGProgressive = false;
			this.twainProNet.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411;
			this.twainProNet.SaveMultiPage = false;
			this.twainProNet.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed;
			this.twainProNet.ShadowText = true;
			this.twainProNet.ShowUI = true;
			this.twainProNet.SN = "PEXHJ700AA-PEG0Q000195";
			this.twainProNet.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE;
			this.twainProNet.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop;
			this.twainProNet.VersionInfo = "";
			this.twainProNet.PreScandotNet += new PegasusImaging.WinForms.TwainPro4.PreScanDelegate(this.twainProNet_PreScandotNet_1);
			this.twainProNet.PostScandotNet += new PegasusImaging.WinForms.TwainPro4.PostScanDelegate(this.twainProNet_PostScandotNet_1);
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.Location = new System.Drawing.Point(168, 64);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(304, 264);
			this.imageXView1.TabIndex = 18;
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(480, 112);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(224, 82);
			this.lstStatus.TabIndex = 24;
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(480, 72);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(192, 24);
			this.lblLoadStatus.TabIndex = 23;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblLastError.Location = new System.Drawing.Point(480, 244);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(216, 16);
			this.lblLastError.TabIndex = 22;
			this.lblLastError.Text = "Last Error:";
			// 
			// lblerror
			// 
			this.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblerror.Location = new System.Drawing.Point(488, 284);
			this.lblerror.Name = "lblerror";
			this.lblerror.Size = new System.Drawing.Size(216, 88);
			this.lblerror.TabIndex = 21;
			// 
			// listBox1
			// 
			this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.listBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.listBox1.Items.AddRange(new object[] {
														  "This sample demonstrates the following functionality:",
														  "1)Using the Memory Transfer mode and ICAP_Compression capability."});
			this.listBox1.Location = new System.Drawing.Point(16, 12);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(680, 43);
			this.listBox1.TabIndex = 25;
			// 
			// mainmenu
			// 
			this.mainmenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnuFile,
																					 this.mnuToolbar,
																					 this.mnuAbout});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuQuit});
			this.mnuFile.Text = "File";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 0;
			this.mnuQuit.Text = "Quit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 1;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuToolbarShow});
			this.mnuToolbar.Text = "Toolbar";
			// 
			// mnuToolbarShow
			// 
			this.mnuToolbarShow.Index = 0;
			this.mnuToolbarShow.Text = "Show";
			this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 2;
			this.mnuAbout.Text = "About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click_1);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(714, 385);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.listBox1,
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.lblLastError,
																		  this.lblerror,
																		  this.imageXView1,
																		  this.hScrollBarJpegQual,
																		  this.comboBoxCompression,
																		  this.buttonScan,
																		  this.buttonSelectSource,
																		  this.labelJpegQual,
																		  this.labelJpegQualVal,
																		  this.lblCompressTypes,
																		  this.labelStatus,
																		  this.checkBoxUseViewer});
			this.MaximizeBox = false;
			this.Menu = this.mainmenu;
			this.MinimizeBox = false;
			this.Name = "FormMain";
			this.Text = "TwainPRO Memory and Compression Sample";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Pegasus Imaging Sample Application Standard Functions

		System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";

    
		private string GetFileName(string FullName) 
		{
			return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length 
				- (FullName.LastIndexOf("\\") + 1)));
		}
    
		private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ ((ErrorException.Source + "") 
				+ "\n"))));
		}
    
		private static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
		}
    
		private string PegasusOpenFile(string sTitle, string sFileFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFileFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}
    
		private string PegasusSaveFile(string sTitle, string sFilter) 
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}

		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void buttonSelectSource_Click(object sender, System.EventArgs e)
		{
			labelStatus.Text = "Select Scanner";
			try
			{
				twainProNet.SelectSource ();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			UpdateCompressionTypes ();
			buttonScan.Enabled = true;
		}

		private void UpdateCompressionTypes ()
		{
			labelStatus.Text = "Querying Compression Capabilities";
			Application.DoEvents ();
			
			twainProNet.OpenSession ();
			twainProNet.Capability = PegasusImaging.WinForms.TwainPro4.tagenumCapability.TWCAP_USECAPADVANCED;
			twainProNet.CapAdvanced = (int) PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_COMPRESSION;

			comboBoxCompression.Items.Clear ();    
			comboBoxCompression.Items.Add ("TWCP_NONE");
			comboBoxCompression.SelectedIndex = 0;

			if (twainProNet.CapSupported)
			{
				if (twainProNet.CapType == PegasusImaging.WinForms.TwainPro4.tagenumCapType.TWCAP_ENUM)
				{
					for (int nIndex = 0; nIndex < twainProNet.CapNumItems; nIndex++)
					{
						float fResult = twainProNet.CapItem_Get (nIndex);

						if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_NONE)
						{
							// Already handled
						}
						else if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_PACKBITS)
						{
							int nPosition = comboBoxCompression.Items.Add ("TWCP_PACKBITS");
						}
						else if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31D)
						{
							comboBoxCompression.Items.Add ("TWCP_GROUP31D");
						}
						else if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31DEOL)
						{
							comboBoxCompression.Items.Add ("TWCP_GROUP31DEOL");
						}
						else if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP32D)
						{
							comboBoxCompression.Items.Add ("TWCP_GROUP32D");
						}
						else if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP4)
						{
							comboBoxCompression.Items.Add ("TWCP_GROUP4");
						}
						else if (fResult == (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_JPEG)
						{
							comboBoxCompression.Items.Add ("TWCP_JPEG");
						}
					}
				}
			}

			twainProNet.CloseSession ();
			labelStatus.Text = "Select and Scan";
		}

		private void hScrollBarJPEGQual_ValueChanged(object sender, System.EventArgs e)
		{
			labelJpegQualVal.Text = hScrollBarJpegQual.Value.ToString ();
		}

		private void buttonScan_Click(object sender, System.EventArgs e)
		{
			try
			{
				labelStatus.Text = "Opening Scanner";
				Application.DoEvents ();
				twainProNet.OpenSession ();

				if (twainProNet.TransferMode == PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_MEMORY)
				{
					twainProNet.CapTypeOut = PegasusImaging.WinForms.TwainPro4.tagenumCapType.TWCAP_ONEVALUE;
					twainProNet.Capability = PegasusImaging.WinForms.TwainPro4.tagenumCapability.TWCAP_USECAPADVANCED;
					twainProNet.CapAdvanced = (int) PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_COMPRESSION;
				
					// Only do the following if the Capability is supported and
					// there is more than one compression type to set. Otherwise,
					// the data source may say they support this capability but
					// might return an error if you try to set to TWCP_NONE and
					// it is the only type. All data sources must support TWCP_NONE.
					if (twainProNet.CapSupported && comboBoxCompression.Items.Count > 1)
					{
						labelStatus.Text = "Setting Compression Mode";
					
						string strCompressType = comboBoxCompression.SelectedItem.ToString();
					
						// we have to figure out how to get back to the enumeration value
						// Microsoft removed the ItemData property from the combo box control
						// so this is definitely not an elegant solution, but it works...
						switch (strCompressType)
						{
							case "TWCP_NONE":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_NONE;
								break;
						
							case "TWCP_PACKBITS":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_PACKBITS;
								break;
						
							case "TWCP_GROUP31D":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31D;
								break;
						
							case "TWCP_GROUP31DEOL":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP31DEOL;
								break;
						
							case "TWCP_GROUP32D":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP32D;
								break;
						
							case "TWCP_GROUP4":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_GROUP4;
								break;
						
							case "TWCP_JPEG":
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_JPEG;
								break;
						}
						twainProNet.SetCapOut ();
					
						if (strCompressType == "TWCP_JPEG")
						{
							// Set the JPEG Quality factor
							twainProNet.CapAdvanced = (int) PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_JPEGQUALITY;
							if (twainProNet.CapSupported)
							{
								twainProNet.CapValueOut = (float) hScrollBarJpegQual.Value;
								twainProNet.SetCapOut ();
							}

							// You could also negotiate the ICAP_JPEGPIXELTYPE capability.
							// I am leaving this to the DataSource
						}
						else if (strCompressType != "TWCP_NONE")
						{
							// The following is done to ensure the TIFF file can be read correctly by the Imaging for Windows
							// Apparently Imaging for Windows must ignore the baseline tiff tag, PhotometricInterpretation.
							// ImagXpress does not need the following as it correctly reads in the PhotometricInterpreation tag
							// This will ensure that the TIFF file created has 0 as white and 1 as black
						                
							// We will set the ICAP_PIXELFLAVOR first, although for CCITT compression types
							// ICAP_PIXELFLAVORCODES can override ICAP_PIXELFLAVOR
							twainProNet.CapAdvanced = (int) PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_PIXELFLAVOR;
							if (twainProNet.CapSupported)
							{
								twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWPF_VANILLA;
								twainProNet.SetCapOut ();
							}	
						
							if (strCompressType != "TWCP_PACKBITS")
							{
								twainProNet.CapAdvanced = (int) PegasusImaging.WinForms.TwainPro4.tagenumAdvancedCapability.ICAP_PIXELFLAVORCODES;
								if (twainProNet.CapSupported)
								{
									if (twainProNet.CapValue != (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWPF_VANILLA)
									{
										twainProNet.CapValueOut = (float) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWPF_VANILLA;
										twainProNet.SetCapOut ();	// Set it only if we have to
									}
								}
							}
						}
					}
				}
				else
				{
					labelStatus.Text = "TwainPRO forced to Native Transfer Mode. Compression unavailable.";
				}
    
				twainProNet.StartSession ();
				labelStatus.Text = "Select and Scan";
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		

		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}
		
		private void FormMain_Load(object sender, System.EventArgs e)
		{		

			//**The UnlockRuntime function must be called to distribute the runtime**
			//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);


			try 
			{
				//this is where events are assigned. This happens before the file gets loaded.
				PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
				PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
		
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}

			// Let TwainPRO know that you wish to use Buffered Memory Mode
			twainProNet.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_MEMORY;
		}

		private void twainProNet_PostScandotNet_1(int classPtr, int objID, bool Cancel)
		{
			labelStatus.Text = "";
			
			// Need to check the Compression type to see if we are transferring a DIB
			// or we are transferring compressed data.
			
			if (twainProNet.ICompression == (int) PegasusImaging.WinForms.TwainPro4.tagenumCapAdvConstants.TWCP_NONE)
			{
				int hImage = twainProNet.hDIB;
				if (hImage != 0)
				{
					try
					{
						imageXView1.Image  = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)hImage);
						if (checkBoxUseViewer.Checked == true)
						{
							FormViewer formViewer = new FormViewer ();
							formViewer.ShowEx (imageXView1.Image , true);
						}
					}
					catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
					{
						PegasusError(ex, lblerror);
					}
				}
			}
			else
			{
				int hImage = twainProNet.hImage;
				if (hImage != 0)
				{
					try
					{
						imageXView1.Image  = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr)hImage);
						if (checkBoxUseViewer.Checked == true)
						{
							FormViewer formViewer = new FormViewer ();
							formViewer.ShowEx (imageXView1.Image, false);
						}
					}
					catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
					{
						PegasusError(ex, lblerror);
					}
					GlobalFree (hImage);
				}
			}
			
			Application.DoEvents ();    
			Cancel = false;
		}

		private void twainProNet_PreScandotNet_1(int classPtr, int objID, bool Cancel)
		{
			labelStatus.Text = "Scanning...";
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void mnuAbout_Click_1(object sender, System.EventArgs e)
		{
			twainProNet.About();
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			if ((imageXView1.Toolbar.Activated == true)) 
			{
				mnuToolbarShow.Text = "Show";
				imageXView1.Toolbar.Activated = false;
			}
			else 
			{
				mnuToolbarShow.Text = "Hide";
				imageXView1.Toolbar.Activated = true;
			}
		
		}

		private void comboBoxCompression_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (comboBoxCompression.SelectedItem.ToString() == "TWCP_JPEG")
			{
				hScrollBarJpegQual.Enabled = true;
			}
			else
			{
				hScrollBarJpegQual.Enabled = false;
			}
		}
			
	}
}

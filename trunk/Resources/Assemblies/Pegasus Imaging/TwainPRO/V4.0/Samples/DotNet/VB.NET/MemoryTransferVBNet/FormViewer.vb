'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'


Imports PegasusImaging.WinForms.ImagXpress8

Public Class FormViewer
    Inherits System.Windows.Forms.Form

    Dim dZoomFactor As Double

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            ' Don't forget to dispose IX
            '
            If (Not (ImagXpress1) Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If (Not (Processor1) Is Nothing) Then
                Processor1.Dispose()
                Processor1 = Nothing
            End If
            If (Not (ImageXView1) Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If


            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents buttonZoomIn As System.Windows.Forms.Button
    Friend WithEvents buttonFit As System.Windows.Forms.Button
    Friend WithEvents buttonZoomOut As System.Windows.Forms.Button
    Friend WithEvents buttonDespeckle As System.Windows.Forms.Button
    Friend WithEvents Processor1 As PegasusImaging.WinForms.ImagXpress8.Processor
    Friend WithEvents buttonDeskew As System.Windows.Forms.Button
    Friend WithEvents menuInvert As System.Windows.Forms.Button
    Friend WithEvents buttonFlip As System.Windows.Forms.Button
    Friend WithEvents buttonMirror As System.Windows.Forms.Button

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonZoomIn = New System.Windows.Forms.Button()
        Me.buttonZoomOut = New System.Windows.Forms.Button()
        Me.buttonFit = New System.Windows.Forms.Button()
        Me.buttonDespeckle = New System.Windows.Forms.Button()
        Me.buttonFlip = New System.Windows.Forms.Button()
        Me.menuInvert = New System.Windows.Forms.Button()
        Me.buttonDeskew = New System.Windows.Forms.Button()
        Me.buttonMirror = New System.Windows.Forms.Button()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.Processor1 = New PegasusImaging.WinForms.ImagXpress8.Processor()
        Me.SuspendLayout()
        '
        'buttonZoomIn
        '
        Me.buttonZoomIn.Name = "buttonZoomIn"
        Me.buttonZoomIn.Size = New System.Drawing.Size(72, 24)
        Me.buttonZoomIn.TabIndex = 1
        Me.buttonZoomIn.Text = "Zoom In"
        '
        'buttonZoomOut
        '
        Me.buttonZoomOut.Location = New System.Drawing.Point(72, 0)
        Me.buttonZoomOut.Name = "buttonZoomOut"
        Me.buttonZoomOut.Size = New System.Drawing.Size(72, 24)
        Me.buttonZoomOut.TabIndex = 2
        Me.buttonZoomOut.Text = "Zoom Out"
        '
        'buttonFit
        '
        Me.buttonFit.Location = New System.Drawing.Point(144, 0)
        Me.buttonFit.Name = "buttonFit"
        Me.buttonFit.Size = New System.Drawing.Size(72, 24)
        Me.buttonFit.TabIndex = 3
        Me.buttonFit.Text = "Fit"
        '
        'buttonDespeckle
        '
        Me.buttonDespeckle.Location = New System.Drawing.Point(216, 0)
        Me.buttonDespeckle.Name = "buttonDespeckle"
        Me.buttonDespeckle.Size = New System.Drawing.Size(72, 24)
        Me.buttonDespeckle.TabIndex = 4
        Me.buttonDespeckle.Text = "Despeckle"
        '
        'buttonFlip
        '
        Me.buttonFlip.Location = New System.Drawing.Point(432, 0)
        Me.buttonFlip.Name = "buttonFlip"
        Me.buttonFlip.Size = New System.Drawing.Size(72, 24)
        Me.buttonFlip.TabIndex = 5
        Me.buttonFlip.Text = "Flip"
        '
        'menuInvert
        '
        Me.menuInvert.Location = New System.Drawing.Point(360, 0)
        Me.menuInvert.Name = "menuInvert"
        Me.menuInvert.Size = New System.Drawing.Size(72, 24)
        Me.menuInvert.TabIndex = 6
        Me.menuInvert.Text = "Invert"
        '
        'buttonDeskew
        '
        Me.buttonDeskew.Location = New System.Drawing.Point(288, 0)
        Me.buttonDeskew.Name = "buttonDeskew"
        Me.buttonDeskew.Size = New System.Drawing.Size(72, 24)
        Me.buttonDeskew.TabIndex = 7
        Me.buttonDeskew.Text = "Deskew"
        '
        'buttonMirror
        '
        Me.buttonMirror.Location = New System.Drawing.Point(504, 0)
        Me.buttonMirror.Name = "buttonMirror"
        Me.buttonMirror.Size = New System.Drawing.Size(72, 24)
        Me.buttonMirror.TabIndex = 8
        Me.buttonMirror.Text = "Mirror"
        '
        'ImageXView1
        '
        Me.ImageXView1.Location = New System.Drawing.Point(16, 32)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(536, 312)
        Me.ImageXView1.TabIndex = 9
        '
        'Processor1
        '
        Me.Processor1.BackgroundColor = System.Drawing.Color.Black
        Me.Processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast
        Me.Processor1.ProgressPercent = 10
        Me.Processor1.Redeyes = Nothing
        '
        'FormViewer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(576, 373)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.ImageXView1, Me.buttonMirror, Me.buttonDeskew, Me.menuInvert, Me.buttonFlip, Me.buttonDespeckle, Me.buttonFit, Me.buttonZoomOut, Me.buttonZoomIn})
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FormViewer"
        Me.Text = "FormViewer"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Sub ShowEx(ByVal im As PegasusImaging.WinForms.ImagXpress8.ImageX, ByVal bIsHDIB As Boolean)
        If bIsHDIB Then
            ImageXView1.Image = im
        Else
            ImageXView1.Image = im
        End If
        dZoomFactor = Convert.ToDouble(ImageXView1.ZoomFactor)
        If (Me.Visible = False) Then
            Me.Show()
        Else
            Me.Invalidate()
        End If


    End Sub

    Private Sub FormViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub buttonZoomIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonZoomIn.Click
        dZoomFactor = dZoomFactor + 0.05
        Try
            ImageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor)
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try
    End Sub

    Private Sub buttonFit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonFit.Click
        Try
            ImageXView1.ZoomToFit(PegasusImaging.WinForms.ImagXpress8.ZoomToFitType.FitBest)
            dZoomFactor = ImageXView1.ZoomFactor
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try
    End Sub


    Private Sub buttonZoomOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonZoomOut.Click
        dZoomFactor = (dZoomFactor - 0.05)
        Try
            ImageXView1.ZoomFactor = Convert.ToSingle(dZoomFactor)
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try
    End Sub

    Private Sub buttonDespeckle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonDespeckle.Click
        Try
            Processor1.Image = ImageXView1.Image.Copy
            Processor1.Despeckle()
            ImageXView1.Image = Processor1.Image
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try


    End Sub

    Private Sub buttonDeskew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonDeskew.Click
        Try
            Processor1.Image = ImageXView1.Image.Copy
            Processor1.Deskew()
            ImageXView1.Image = Processor1.Image
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try


    End Sub


    Private Sub menuInvert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuInvert.Click
        Try
            Processor1.Image = ImageXView1.Image.Copy
            Processor1.Negate()
            ImageXView1.Image = Processor1.Image
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try


    End Sub

    Private Sub buttonFlip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonFlip.Click
        Try
            Processor1.Image = ImageXView1.Image.Copy
            Processor1.Flip()
            ImageXView1.Image = Processor1.Image
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try


    End Sub

    Private Sub buttonMirror_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonMirror.Click
        Try
            Processor1.Image = ImageXView1.Image.Copy
            Processor1.Mirror()
            ImageXView1.Image = Processor1.Image
        Catch eX As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show((eX.Message + (" " + eX.Source)))
        End Try

    End Sub
End Class

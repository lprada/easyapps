// ****************************************************************'
// * Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
// * This sample code is provided to Pegasus licensees "as is"    *'
// * with no restrictions on use or modification. No warranty for *'
// * use of this sample code is provided by Pegasus.              *'
// ****************************************************************'
using PegasusImaging.WinForms.TwainPro4;
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace TwainBC
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private PegasusImaging.WinForms.TwainPro4.PICTwainPro picTwainPro1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem mnu_File;
		private System.Windows.Forms.MenuItem mnu_Quit;
		private System.Windows.Forms.MenuItem mnu_About;
		internal System.Windows.Forms.ListBox lstInfo;
		internal System.Windows.Forms.Button cmdScan;
		internal System.Windows.Forms.ListBox lstBCStrings;
		internal System.Windows.Forms.ListBox lstBCTypes;
		internal System.Windows.Forms.Label Label3;
		internal System.Windows.Forms.Label Label2;
		internal System.Windows.Forms.Label lblNumBCs;
		internal System.Windows.Forms.Label Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		internal System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.Button cmdSelSource;

		private bool gbBarcodeSupported;

		public Form1()
		{
			//call the unlock function with dummy values
			//UnlockPICTwainPRO.PS_Unlock(1234, 1234, 1234, 1234) 

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			
			// This call is required!
			picTwainPro1.hParentWnd = this.Handle.ToInt32();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				if (picTwainPro1 != null)
				{
					picTwainPro1.Dispose();
					picTwainPro1 = null;
				}

				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.picTwainPro1 = new PegasusImaging.WinForms.TwainPro4.PICTwainPro();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnu_File = new System.Windows.Forms.MenuItem();
			this.mnu_Quit = new System.Windows.Forms.MenuItem();
			this.mnu_About = new System.Windows.Forms.MenuItem();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.cmdScan = new System.Windows.Forms.Button();
			this.lstBCStrings = new System.Windows.Forms.ListBox();
			this.lstBCTypes = new System.Windows.Forms.ListBox();
			this.Label3 = new System.Windows.Forms.Label();
			this.Label2 = new System.Windows.Forms.Label();
			this.lblNumBCs = new System.Windows.Forms.Label();
			this.Label1 = new System.Windows.Forms.Label();
			this.lblStatus = new System.Windows.Forms.Label();
			this.cmdSelSource = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// picTwainPro1
			// 
			this.picTwainPro1.Caption = "";
			this.picTwainPro1.CaptionHeight = 0;
			this.picTwainPro1.CaptionLeft = 0;
			this.picTwainPro1.CaptionTop = 0;
			this.picTwainPro1.CaptionWidth = 0;
			this.picTwainPro1.ClipCaption = false;
			this.picTwainPro1.CloseOnCancel = true;
			this.picTwainPro1.Debug = false;
			this.picTwainPro1.EnableExtCaps = true;
			this.picTwainPro1.EvalMode = PegasusImaging.WinForms.TwainPro4.tagenumEvaluationMode.EVAL_Automatic;
			this.picTwainPro1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.picTwainPro1.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(86)), ((System.Byte)(69)), ((System.Byte)(61)));
			this.picTwainPro1.FTPPassword = "";
			this.picTwainPro1.FTPUserName = "";
			this.picTwainPro1.HAlign = PegasusImaging.WinForms.TwainPro4.tagenumHAlign.HAlignLeft;
			this.picTwainPro1.Manufacturer = "";
			this.picTwainPro1.MaxImages = -1;
			this.picTwainPro1.PICPassword = "";
			this.picTwainPro1.ProductFamily = "";
			this.picTwainPro1.ProductName = "";
			this.picTwainPro1.ProxyServer = "";
			this.picTwainPro1.SaveJPEGChromFactor = 36;
			this.picTwainPro1.SaveJPEGLumFactor = 32;
			this.picTwainPro1.SaveJPEGProgressive = false;
			this.picTwainPro1.SaveJPEGSubSampling = PegasusImaging.WinForms.TwainPro4.tagenumSubSample.SS_411;
			this.picTwainPro1.SaveMultiPage = false;
			this.picTwainPro1.SaveTIFFCompression = PegasusImaging.WinForms.TwainPro4.tagenumSaveTIFFCompressionType.TWTIFF_Uncompressed;
			this.picTwainPro1.ShadowText = true;
			this.picTwainPro1.ShowUI = true;
			this.picTwainPro1.SN = "PEXHJ700AA-PEG0Q000195";
			this.picTwainPro1.TransferMode = PegasusImaging.WinForms.TwainPro4.tagenumTransferMode.TWSX_NATIVE;
			this.picTwainPro1.VAlign = PegasusImaging.WinForms.TwainPro4.tagenumVAlign.VAlignTop;
			this.picTwainPro1.VersionInfo = "";
			this.picTwainPro1.PostScandotNet += new PegasusImaging.WinForms.TwainPro4.PostScanDelegate(this.picTwainPro1_PostScandotNet);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnu_File,
																					  this.mnu_About});
			// 
			// mnu_File
			// 
			this.mnu_File.Index = 0;
			this.mnu_File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_Quit});
			this.mnu_File.Text = "File";
			// 
			// mnu_Quit
			// 
			this.mnu_Quit.Index = 0;
			this.mnu_Quit.Text = "Quit";
			this.mnu_Quit.Click += new System.EventHandler(this.mnu_Quit_Click);
			// 
			// mnu_About
			// 
			this.mnu_About.Index = 1;
			this.mnu_About.Text = "About";
			this.mnu_About.Click += new System.EventHandler(this.mnu_About_Click);
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Setting an advanced TWAIN capability via the TWCAP_USECAPADVANCED capability.",
														 "",
														 "Note: There are many other advanced capabilities in addition to the ICAP_BARCODED" +
														 "ETECTIONENABLED capability ",
														 "used in this sample.  Please see the Advanced Capabilities Constants section of t" +
														 "he TwainPRO Help File."});
			this.lstInfo.Location = new System.Drawing.Point(8, 16);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(648, 95);
			this.lstInfo.TabIndex = 1;
			// 
			// cmdScan
			// 
			this.cmdScan.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.cmdScan.Location = new System.Drawing.Point(357, 296);
			this.cmdScan.Name = "cmdScan";
			this.cmdScan.Size = new System.Drawing.Size(120, 40);
			this.cmdScan.TabIndex = 16;
			this.cmdScan.Text = "Scan";
			this.cmdScan.Click += new System.EventHandler(this.cmdScan_Click);
			// 
			// lstBCStrings
			// 
			this.lstBCStrings.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstBCStrings.Location = new System.Drawing.Point(336, 232);
			this.lstBCStrings.Name = "lstBCStrings";
			this.lstBCStrings.Size = new System.Drawing.Size(160, 56);
			this.lstBCStrings.TabIndex = 14;
			// 
			// lstBCTypes
			// 
			this.lstBCTypes.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.lstBCTypes.Location = new System.Drawing.Point(160, 232);
			this.lstBCTypes.Name = "lstBCTypes";
			this.lstBCTypes.Size = new System.Drawing.Size(144, 56);
			this.lstBCTypes.TabIndex = 13;
			// 
			// Label3
			// 
			this.Label3.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Label3.Location = new System.Drawing.Point(336, 192);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(104, 24);
			this.Label3.TabIndex = 12;
			this.Label3.Text = "BarCode Strings";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(168, 192);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(104, 24);
			this.Label2.TabIndex = 11;
			this.Label2.Text = "BarCode Types";
			// 
			// lblNumBCs
			// 
			this.lblNumBCs.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblNumBCs.Location = new System.Drawing.Point(336, 144);
			this.lblNumBCs.Name = "lblNumBCs";
			this.lblNumBCs.Size = new System.Drawing.Size(104, 24);
			this.lblNumBCs.TabIndex = 10;
			this.lblNumBCs.Text = "0";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(168, 144);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(112, 32);
			this.Label1.TabIndex = 9;
			this.Label1.Text = "Num Barcodes:";
			// 
			// lblStatus
			// 
			this.lblStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblStatus.Location = new System.Drawing.Point(72, 344);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(480, 16);
			this.lblStatus.TabIndex = 17;
			// 
			// cmdSelSource
			// 
			this.cmdSelSource.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.cmdSelSource.Location = new System.Drawing.Point(168, 296);
			this.cmdSelSource.Name = "cmdSelSource";
			this.cmdSelSource.Size = new System.Drawing.Size(128, 40);
			this.cmdSelSource.TabIndex = 18;
			this.cmdSelSource.Text = "Select Source";
			this.cmdSelSource.Click += new System.EventHandler(this.cmdSelSource_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(674, 369);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.cmdSelSource,
																		  this.lblStatus,
																		  this.cmdScan,
																		  this.lstBCStrings,
																		  this.lstBCTypes,
																		  this.Label3,
																		  this.Label2,
																		  this.lblNumBCs,
																		  this.Label1,
																		  this.lstInfo});
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "TwainBC";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void InitBarcode() 
		{
			try
			{
				// Enable the barcode capability
				picTwainPro1.Capability = tagenumCapability.TWCAP_USECAPADVANCED;
				picTwainPro1.CapAdvanced = (int)tagenumAdvancedCapability.ICAP_BARCODEDETECTIONENABLED;
				if (picTwainPro1.CapSupported) 
				{
					gbBarcodeSupported = true;
					picTwainPro1.CapValueOut = 1;
					picTwainPro1.SetCapOut();
					picTwainPro1.CapExtInfoCount = 4;
					picTwainPro1.Capability = tagenumCapability.TWCAP_USECAPADVANCEDFOREXTINFO;
					picTwainPro1.CapExtInfoIndex = 0;
					picTwainPro1.CapAdvanced = (int)tagenumExtendedInfo.TWEI_BARCODECOUNT;
					picTwainPro1.CapExtInfoIndex = 1;
					picTwainPro1.CapAdvanced = (int)tagenumExtendedInfo.TWEI_BARCODETYPE;
					picTwainPro1.CapExtInfoIndex = 2;
					picTwainPro1.CapAdvanced = (int)tagenumExtendedInfo.TWEI_BARCODETEXTLENGTH;
					picTwainPro1.CapExtInfoIndex = 3;
					picTwainPro1.CapAdvanced = (int)tagenumExtendedInfo.TWEI_BARCODETEXT;
				}
				else 
				{
					gbBarcodeSupported = false;
				}
			}
			catch(Exception ex)
			{		
				MessageBox.Show(ex.Message);		
				GetError();
			}

		}
    	
    
		private void Form1_Load(object sender, System.EventArgs e) 
		{	
			

			picTwainPro1.Debug = true;
		}
    
	
    	

		private void cmdScan_Click(object sender, System.EventArgs e)
		{
			lblStatus.Text = "";
			try
			{
				picTwainPro1.OpenSession();
				InitBarcode();
			
				picTwainPro1.StartSession ();
			}
			catch (System.Runtime.InteropServices.SEHException exception)
			{
				MessageBox.Show (exception.Message);
				GetError();
			}
		}

		private void cmdSelSource_Click(object sender, System.EventArgs e)
		{
			lblStatus.Text = "";
			try
			{

				picTwainPro1.SelectSource();
				return;
			}
			catch(Exception ex)
			{		
				MessageBox.Show(ex.Message);	
				GetError();
			}
		}

		//  Update the status bar with the current error code and description
		private void GetError() 
		{
			if ((picTwainPro1.ErrorCode != 0)) 
			{
				lblStatus.Text = ("Data Source Error " 
					+ (picTwainPro1.ErrorCode + (": " + picTwainPro1.ErrorString)));
			}
		}

		private void mnu_Quit_Click(object sender, System.EventArgs e)
		{
			 Application.Exit();
		}

		private void mnu_About_Click(object sender, System.EventArgs e)
		{
			picTwainPro1.About();
		}

		private void picTwainPro1_PostScandotNet(int classPtr, int objID, bool Cancel)
		{
						
				int nIndex;
				ArrayList alBCTypes = new ArrayList();
				ArrayList alBCStrings = new ArrayList();

				try
				{
					if (gbBarcodeSupported) 
					{
						// Get the Extended Image Info
						picTwainPro1.GetExtInfo();
						// Now the ExtInfo structure has been filled
						//  Get the Barcode Count
						picTwainPro1.CapExtInfoIndex = 0;
						if (picTwainPro1.CapSupported) 
						{
							lblNumBCs.Text = picTwainPro1.CapValue.ToString();
						}
						//  Get the Barcode Type
						picTwainPro1.CapExtInfoIndex = 1;
						if (picTwainPro1.CapSupported) 
						{
							for (nIndex = 0; (nIndex 
								<= (picTwainPro1.CapNumItems - 1)); nIndex++) 
							{
								switch ((int)picTwainPro1.CapItem_Get(nIndex)) 
								{
									case (int)tagenumExtInfoConstants.TWBT_3OF9:
										alBCTypes.Add("TWBT_3OF9");
										//  lstBCTypes.AddItem("TWBT_3OF9")
										break;
									case (int)tagenumExtInfoConstants.TWBT_2OF5INTERLEAVED:
										alBCTypes.Add("TWBT_2OF5INTERLEAVED");
										break;
									case (int)tagenumExtInfoConstants.TWBT_2OF5NONINTERLEAVED:
										alBCTypes.Add("TWBT_2OF5NONINTERLEAVED");
										break;
									case (int)tagenumExtInfoConstants.TWBT_CODE93:
										alBCTypes.Add("TWBT_CODE93");
										break;
									case (int)tagenumExtInfoConstants.TWBT_CODE128:
										alBCTypes.Add("TWBT_CODE128");
										break;
									case (int)tagenumExtInfoConstants.TWBT_UCC128:
										alBCTypes.Add("TWBT_UCC128");
										break;
									case (int)tagenumExtInfoConstants.TWBT_CODABAR:
										alBCTypes.Add("TWBT_CODABAR");
										break;
									case (int)tagenumExtInfoConstants.TWBT_UPCA:
										alBCTypes.Add("TWBT_UPCA");
										break;
									case (int)tagenumExtInfoConstants.TWBT_UPCE:
										alBCTypes.Add("TWBT_UPCE");
										break;
								}
							}
						}
						lstBCTypes.DataSource = alBCTypes;
						// Get the Barcode Strings
						picTwainPro1.CapExtInfoIndex = 3;
						if (picTwainPro1.CapSupported) 
						{
							for (nIndex = 0; (nIndex 
								<= (picTwainPro1.CapNumItems - 1)); nIndex++) 
							{
								alBCStrings.Add(picTwainPro1.CapItemString_Get(nIndex));
							}
						}
						lstBCStrings.DataSource = alBCStrings;
					}
				}
				catch(Exception ex)
				{		
					MessageBox.Show(ex.Message);
					GetError();
				}
			}
		}
		
	
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress8;

namespace TwainProDemoCSharp
{
	/// <summary>
	/// Summary description for FormViewer.
	/// </summary>
	public class FormViewer : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;


		private PegasusImaging.WinForms.TwainPro4.PICTwainPro _tp;

		public FormViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.AlignHorizontal = PegasusImaging.WinForms.ImagXpress8.AlignHorizontal.Center;
			this.imageXView1.AlignVertical = PegasusImaging.WinForms.ImagXpress8.AlignVertical.Center;
			this.imageXView1.AllowUpdate = true;
			this.imageXView1.Antialias = true;
			this.imageXView1.AspectX = 1;
			this.imageXView1.AspectY = 1;
			this.imageXView1.AutoImageDispose = false;
			this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.CropImage;
			this.imageXView1.BorderType = PegasusImaging.WinForms.ImagXpress8.BorderType.Inset;
			this.imageXView1.Brightness = 0;
			this.imageXView1.ClipRectangle = new System.Drawing.Rectangle(0, 0, 0, 0);
			this.imageXView1.Contrast = 0;
			this.imageXView1.Dithered = true;
			this.imageXView1.GrayMode = PegasusImaging.WinForms.ImagXpress8.GrayMode.Standard;
			this.imageXView1.Location = new System.Drawing.Point(16, 16);
			this.imageXView1.Maximum = new System.Drawing.Size(0, 0);
			this.imageXView1.Minimum = new System.Drawing.Size(1, 1);
			this.imageXView1.Name = "imageXView1";
			
			this.imageXView1.Progressive = false;
			this.imageXView1.Size = new System.Drawing.Size(440, 336);
			this.imageXView1.Smoothing = true;
			this.imageXView1.TabIndex = 0;
			this.imageXView1.ZoomFactor = 1F;
			// 
			// FormViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(466, 368);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.imageXView1});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormViewer";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Viewer";
			this.Load += new System.EventHandler(this.FormViewer_Load);
			this.ResumeLayout(false);

		}
		#endregion

		public PegasusImaging.WinForms.TwainPro4.PICTwainPro PrimaryTP
		{
			set { _tp = value; }
		}

		public void AddImage ()
       {

			imageXView1.AutoScroll = true;
			imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib((System.IntPtr) _tp.hDIB);

			if (this.Visible == false)
			{
				this.ShowDialog ();	
			}
		}

		private void FormViewer_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page
{
    Byte[] data;
    PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
    PegasusImaging.WinForms.ImagXpress8.ImageX img = new PegasusImaging.WinForms.ImagXpress8.ImageX();
    PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress barcodeXpress1 = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress();    
    System.IO.Stream stream = new System.IO.MemoryStream();
    PegasusImaging.WinForms.ImagXpress8.Processor proc1;

    protected void Page_Load(object sender, EventArgs e)
    {
        //***Must call the UnlockRuntime method to unlock the control
	//The unlock codes shown below are for formatting purposes only and will not work!
        //PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234);
        //imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);
        //barcodeXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

        try
        {
            barcodeXpress1.Licensing.LicenseEdition = PegasusImaging.WinForms.BarcodeXpress5.LicenseChoice.ProfessionalPlus2DWriteEdition;
            barcodeXpress1.reader.Area = new System.Drawing.Rectangle(0, 0, 0, 0);
            barcodeXpress1.reader.ReturnPossibleBarcodes = false;
            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UnknownBarcode };
        }
        catch (PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
        {
            Response.Write(ex.Message);
        }
        catch (System.Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void AnalyzeButton_Click(object sender, EventArgs e)
    {
        try
        {
            for (int j = 0; j < BarcodeTypes.Items.Count; j++)
            {
                if (BarcodeTypes.Items[j].Selected == true)
                {
                    switch (BarcodeTypes.Items[j].Text)
                    {
                        case "1D":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UnknownBarcode };
                            break;
                        case "PDF-417":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode };
                            break;
                        case "QR":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.QRCodeBarcode };
                            break;
                        case "Data Matrix":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode };
                            break;
                        case "Aus Post":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.AustralianPost4StateBarcode };
                            break;
                        case "Royal Post":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.RoyalPost4StateBarcode };
                            break;
                        case "PostNet":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PostNetBarcode };
                            break;
                        case "Patch":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode };
                            break;
                        case "One Code":
                            barcodeXpress1.reader.BarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[] { PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.IntelligentMailBarcode };
                            break;
                    }
                }
            }

            ResultsListBox.Items.Clear();

            img = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(Server.MapPath(Image1.ImageUrl));

            proc1 = new PegasusImaging.WinForms.ImagXpress8.Processor();
            if (img.ImageXData.BitsPerPixel != 1)
            {
                proc1.Image = img.Copy();
                proc1.ColorDepth(1, 0, 0);
                img = proc1.Image;
            }

            PegasusImaging.WinForms.BarcodeXpress5.Result[] result = barcodeXpress1.reader.Analyze(img.ToHdib(false).ToInt32());

            for (int i = 0; i < result.Length; i++)
            {
                string area = result[i].Area.ToString();
                string value = result[i].BarcodeValue.ToString();

                string confidence = result[i].Confidence.ToString();
                string type = result[i].BarcodeName.ToString();

                ResultsListBox.Items.Add("Barcode #" + i.ToString());
                ResultsListBox.Items.Add("Type: " + type);
                ResultsListBox.Items.Add("Location: " + area);
                ResultsListBox.Items.Add("Value: " + value);
                ResultsListBox.Items.Add("Confidence: " + confidence);
                ResultsListBox.Items.Add("--------------------------");
            }

            if (result.Length == 0)
            {
                ResultsListBox.Items.Add("No barcodes found.");
            }
        }
        catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
        {
            Response.Write(ex.Message);
        }
        catch (PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
        {
            Response.Write(ex.Message);
        }
        catch (System.Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void UploadButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (FileUpload1.PostedFile != null)
            {
                AnalyzeButton.Enabled = true;

                data = new byte[FileUpload1.FileBytes.Length];
                data = FileUpload1.FileBytes;

                stream.Write(data, 0, data.Length);
                stream.Seek(0, System.IO.SeekOrigin.Begin);

                img = PegasusImaging.WinForms.ImagXpress8.ImageX.FromStream(stream);

                PegasusImaging.WinForms.ImagXpress8.SaveOptions so = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
                so.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Jpeg;

                img.Save(Server.MapPath("input.jpg"), so);

                Image1.ImageUrl = "input.jpg";

                stream.Close();
            }
        }
        catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
        {
            Response.Write(ex.Message);
        }
    }
}

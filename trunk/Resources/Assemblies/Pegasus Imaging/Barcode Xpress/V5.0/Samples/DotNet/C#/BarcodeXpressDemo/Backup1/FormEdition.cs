/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.BarcodeXpress5;
using PegasusImaging.WinForms.ScanFix5;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for FormEdition.
	/// </summary>
	public class FormEdition : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ComboBox cboEdition;
		private System.Windows.Forms.Button cmdOK;
		private System.Windows.Forms.Label Label1;
		private System.Windows.Forms.Label Label3;
        private System.Windows.Forms.Label Label4;
		private System.Windows.Forms.Label Label2;
		private System.Windows.Forms.Label label6;

		private PegasusImaging.WinForms.BarcodeXpress5.LicenseChoice _barcodeEdition;

		public FormEdition()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			cboEdition.Items.Add ("Standard");
			cboEdition.Items.Add ("Professional");
			cboEdition.Items.Add ("Professional Plus 2D Write");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				//Dispose objects here
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEdition));
            this.cboEdition = new System.Windows.Forms.ComboBox();
            this.cmdOK = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cboEdition
            // 
            this.cboEdition.BackColor = System.Drawing.SystemColors.Window;
            this.cboEdition.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboEdition.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEdition.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cboEdition.Location = new System.Drawing.Point(140, 226);
            this.cboEdition.Name = "cboEdition";
            this.cboEdition.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboEdition.Size = new System.Drawing.Size(185, 22);
            this.cboEdition.TabIndex = 9;
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = System.Drawing.SystemColors.Control;
            this.cmdOK.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdOK.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdOK.Location = new System.Drawing.Point(140, 262);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdOK.Size = new System.Drawing.Size(120, 24);
            this.cmdOK.TabIndex = 8;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(-15, -22);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(305, 22);
            this.Label1.TabIndex = 15;
            this.Label1.Text = "BarcodeXpress is available in four Editions:";
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(12, 89);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(400, 94);
            this.Label3.TabIndex = 14;
            this.Label3.Text = resources.GetString("Label3.Text");
            // 
            // Label4
            // 
            this.Label4.BackColor = System.Drawing.SystemColors.Control;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.Location = new System.Drawing.Point(12, 9);
            this.Label4.Name = "Label4";
            this.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label4.Size = new System.Drawing.Size(400, 80);
            this.Label4.TabIndex = 13;
            this.Label4.Text = resources.GetString("Label4.Text");
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.SystemColors.Control;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(49, 229);
            this.Label2.Name = "Label2";
            this.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label2.Size = new System.Drawing.Size(85, 14);
            this.Label2.TabIndex = 10;
            this.Label2.Text = "Barcode Edition:";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(12, 183);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(400, 40);
            this.label6.TabIndex = 16;
            this.label6.Text = "Professional Plus 2D Write includes the functionality of the Professional Edition" +
                " as well as functionality for writing 2D Barcodes.";
            // 
            // FormEdition
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(421, 302);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.cboEdition);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormEdition";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Barcode Xpress Edition";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			switch (cboEdition.SelectedIndex)
            {
				case 0:
				{
					_barcodeEdition = LicenseChoice.StandardEdition;
					break;
				}

				case 1:
				{
					_barcodeEdition = LicenseChoice.ProfessionalEdition;
					break;
				}
				case 2:
				{
					_barcodeEdition = LicenseChoice.ProfessionalPlus2DWriteEdition;
					break;
				}
			}
			this.Close ();
        }

		public LicenseChoice BarcodeEdition
		{
			get { return _barcodeEdition; }
			set 
			{ 
				_barcodeEdition = value; 
				switch (_barcodeEdition)
				{
					case LicenseChoice.ProfessionalEdition:
					{
						cboEdition.SelectedIndex = 1;
						break;
					}
					case LicenseChoice.StandardEdition:
					{
						cboEdition.SelectedIndex = 0;
						break;
					}
					case LicenseChoice.ProfessionalPlus2DWriteEdition:
					{
						cboEdition.SelectedIndex = 2;
						break;
					}
				}
			}
		}
	}
}

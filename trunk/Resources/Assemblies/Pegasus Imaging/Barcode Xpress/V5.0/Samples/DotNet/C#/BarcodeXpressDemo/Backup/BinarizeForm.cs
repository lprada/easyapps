/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for BinarizeForm.
	/// </summary>
	public class BinarizeForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label ConBarLabel;
		private System.Windows.Forms.Label ConLabel;
		private System.Windows.Forms.Label LowLabel;
		private System.Windows.Forms.Label HighLabel;
		private System.Windows.Forms.ListBox DescriptionListBox;
		private System.Windows.Forms.GroupBox BinGroupBox;
		private System.Windows.Forms.RadioButton ColorDepthRadioButton;
		private System.Windows.Forms.RadioButton BinarizeRadioButton;
		private System.Windows.Forms.RadioButton AutoRadioButton;
		private System.Windows.Forms.Button OKButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PegasusImaging.WinForms.ImagXpress8.ImageX image;

		private System.Windows.Forms.GroupBox BinarizeGroupBox;
		private System.Windows.Forms.TrackBar HighBar;
		private System.Windows.Forms.TrackBar LowBar;
		private System.Windows.Forms.TrackBar ContrastBar;
		private System.Windows.Forms.Label LowThreshLabel;
		private System.Windows.Forms.Label HighThreshLabel;
		private PegasusImaging.WinForms.ImagXpress8.Processor proc1;
		private System.Windows.Forms.ComboBox BlurBox;
		private System.Windows.Forms.Label BlurLabel;

		private PegasusImaging.WinForms.ScanFix5.ScanFix scanFix1;

		public BinarizeForm(PegasusImaging.WinForms.ImagXpress8.ImageX img, PegasusImaging.WinForms.ScanFix5.ScanFix SF)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			image = img;

			if (SF.License.LicenseEdition == PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.LiteEdition)
			{
				BinarizeGroupBox.Enabled = false;
				AutoRadioButton.Enabled = false;
				BinarizeRadioButton.Enabled = false;
			}
			else
			{
				AutoRadioButton.Enabled = true;
				BinarizeRadioButton.Enabled = true;
			}

			scanFix1 = SF;

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BinarizeForm));
            this.BinarizeGroupBox = new System.Windows.Forms.GroupBox();
            this.BlurLabel = new System.Windows.Forms.Label();
            this.BlurBox = new System.Windows.Forms.ComboBox();
            this.ConLabel = new System.Windows.Forms.Label();
            this.LowLabel = new System.Windows.Forms.Label();
            this.HighLabel = new System.Windows.Forms.Label();
            this.ConBarLabel = new System.Windows.Forms.Label();
            this.LowThreshLabel = new System.Windows.Forms.Label();
            this.HighThreshLabel = new System.Windows.Forms.Label();
            this.ContrastBar = new System.Windows.Forms.TrackBar();
            this.LowBar = new System.Windows.Forms.TrackBar();
            this.HighBar = new System.Windows.Forms.TrackBar();
            this.DescriptionListBox = new System.Windows.Forms.ListBox();
            this.BinGroupBox = new System.Windows.Forms.GroupBox();
            this.AutoRadioButton = new System.Windows.Forms.RadioButton();
            this.BinarizeRadioButton = new System.Windows.Forms.RadioButton();
            this.ColorDepthRadioButton = new System.Windows.Forms.RadioButton();
            this.OKButton = new System.Windows.Forms.Button();
            this.BinarizeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContrastBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LowBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBar)).BeginInit();
            this.BinGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // BinarizeGroupBox
            // 
            this.BinarizeGroupBox.Controls.Add(this.BlurLabel);
            this.BinarizeGroupBox.Controls.Add(this.BlurBox);
            this.BinarizeGroupBox.Controls.Add(this.ConLabel);
            this.BinarizeGroupBox.Controls.Add(this.LowLabel);
            this.BinarizeGroupBox.Controls.Add(this.HighLabel);
            this.BinarizeGroupBox.Controls.Add(this.ConBarLabel);
            this.BinarizeGroupBox.Controls.Add(this.LowThreshLabel);
            this.BinarizeGroupBox.Controls.Add(this.HighThreshLabel);
            this.BinarizeGroupBox.Controls.Add(this.ContrastBar);
            this.BinarizeGroupBox.Controls.Add(this.LowBar);
            this.BinarizeGroupBox.Controls.Add(this.HighBar);
            this.BinarizeGroupBox.Location = new System.Drawing.Point(56, 312);
            this.BinarizeGroupBox.Name = "BinarizeGroupBox";
            this.BinarizeGroupBox.Size = new System.Drawing.Size(408, 304);
            this.BinarizeGroupBox.TabIndex = 0;
            this.BinarizeGroupBox.TabStop = false;
            this.BinarizeGroupBox.Text = "Binarize Method Options";
            // 
            // BlurLabel
            // 
            this.BlurLabel.Location = new System.Drawing.Point(48, 272);
            this.BlurLabel.Name = "BlurLabel";
            this.BlurLabel.Size = new System.Drawing.Size(72, 16);
            this.BlurLabel.TabIndex = 10;
            this.BlurLabel.Text = "PreBlurType:";
            // 
            // BlurBox
            // 
            this.BlurBox.Items.AddRange(new object[] {
            "None",
            "Gaussian",
            "Smart"});
            this.BlurBox.Location = new System.Drawing.Point(144, 272);
            this.BlurBox.Name = "BlurBox";
            this.BlurBox.Size = new System.Drawing.Size(152, 21);
            this.BlurBox.TabIndex = 9;
            // 
            // ConLabel
            // 
            this.ConLabel.Location = new System.Drawing.Point(366, 208);
            this.ConLabel.Name = "ConLabel";
            this.ConLabel.Size = new System.Drawing.Size(32, 16);
            this.ConLabel.TabIndex = 8;
            this.ConLabel.Text = "0";
            // 
            // LowLabel
            // 
            this.LowLabel.Location = new System.Drawing.Point(360, 136);
            this.LowLabel.Name = "LowLabel";
            this.LowLabel.Size = new System.Drawing.Size(40, 16);
            this.LowLabel.TabIndex = 7;
            this.LowLabel.Text = "150";
            // 
            // HighLabel
            // 
            this.HighLabel.Location = new System.Drawing.Point(360, 56);
            this.HighLabel.Name = "HighLabel";
            this.HighLabel.Size = new System.Drawing.Size(40, 16);
            this.HighLabel.TabIndex = 6;
            this.HighLabel.Text = "170";
            // 
            // ConBarLabel
            // 
            this.ConBarLabel.Location = new System.Drawing.Point(160, 192);
            this.ConBarLabel.Name = "ConBarLabel";
            this.ConBarLabel.Size = new System.Drawing.Size(56, 16);
            this.ConBarLabel.TabIndex = 5;
            this.ConBarLabel.Text = "Contrast";
            // 
            // LowThreshLabel
            // 
            this.LowThreshLabel.Location = new System.Drawing.Point(152, 104);
            this.LowThreshLabel.Name = "LowThreshLabel";
            this.LowThreshLabel.Size = new System.Drawing.Size(96, 16);
            this.LowThreshLabel.TabIndex = 4;
            this.LowThreshLabel.Text = "Low Threshold";
            // 
            // HighThreshLabel
            // 
            this.HighThreshLabel.Location = new System.Drawing.Point(144, 24);
            this.HighThreshLabel.Name = "HighThreshLabel";
            this.HighThreshLabel.Size = new System.Drawing.Size(96, 16);
            this.HighThreshLabel.TabIndex = 3;
            this.HighThreshLabel.Text = "High Threshold";
            // 
            // ContrastBar
            // 
            this.ContrastBar.Location = new System.Drawing.Point(24, 208);
            this.ContrastBar.Maximum = 255;
            this.ContrastBar.Name = "ContrastBar";
            this.ContrastBar.Size = new System.Drawing.Size(336, 45);
            this.ContrastBar.TabIndex = 2;
            this.ContrastBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.ContrastBar.Scroll += new System.EventHandler(this.ContrastBar_Scroll);
            // 
            // LowBar
            // 
            this.LowBar.LargeChange = 1;
            this.LowBar.Location = new System.Drawing.Point(16, 128);
            this.LowBar.Maximum = 255;
            this.LowBar.Name = "LowBar";
            this.LowBar.Size = new System.Drawing.Size(336, 45);
            this.LowBar.TabIndex = 1;
            this.LowBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.LowBar.Value = 150;
            this.LowBar.Scroll += new System.EventHandler(this.LowBar_Scroll);
            // 
            // HighBar
            // 
            this.HighBar.LargeChange = 1;
            this.HighBar.Location = new System.Drawing.Point(16, 48);
            this.HighBar.Maximum = 255;
            this.HighBar.Name = "HighBar";
            this.HighBar.Size = new System.Drawing.Size(336, 45);
            this.HighBar.TabIndex = 0;
            this.HighBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.HighBar.Value = 170;
            this.HighBar.Scroll += new System.EventHandler(this.HighBar_Scroll);
            // 
            // DescriptionListBox
            // 
            this.DescriptionListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionListBox.Items.AddRange(new object[] {
            "The image you\'re opening is not 1-bit (B&W or Bitonal), BarcodeXpress only works " +
                "",
            "with 1-bit images.",
            "",
            "Please binarize the image using one of the following options:",
            "",
            "ColorDepth - simple thresholding method of ImagXpress (All Editions)",
            "",
            "Binarize - Controllable, more powerful method of ScanFix Bitonal Edition ",
            "and ImagXpress Document or Photo Edition. ",
            "(The Binarize used in this Demo is for ScanFix).",
            "",
            "AutoBinarize - Automatic setting of ScanFix\'s Binarize method ",
            "(ScanFix Bitonal Edition only)."});
            this.DescriptionListBox.Location = new System.Drawing.Point(8, 8);
            this.DescriptionListBox.Name = "DescriptionListBox";
            this.DescriptionListBox.Size = new System.Drawing.Size(488, 173);
            this.DescriptionListBox.TabIndex = 1;
            // 
            // BinGroupBox
            // 
            this.BinGroupBox.Controls.Add(this.AutoRadioButton);
            this.BinGroupBox.Controls.Add(this.BinarizeRadioButton);
            this.BinGroupBox.Controls.Add(this.ColorDepthRadioButton);
            this.BinGroupBox.Location = new System.Drawing.Point(144, 200);
            this.BinGroupBox.Name = "BinGroupBox";
            this.BinGroupBox.Size = new System.Drawing.Size(232, 96);
            this.BinGroupBox.TabIndex = 2;
            this.BinGroupBox.TabStop = false;
            this.BinGroupBox.Text = "Binarization Options";
            // 
            // AutoRadioButton
            // 
            this.AutoRadioButton.Location = new System.Drawing.Point(24, 72);
            this.AutoRadioButton.Name = "AutoRadioButton";
            this.AutoRadioButton.Size = new System.Drawing.Size(104, 16);
            this.AutoRadioButton.TabIndex = 2;
            this.AutoRadioButton.Text = "AutoBinarize";
            this.AutoRadioButton.CheckedChanged += new System.EventHandler(this.AutoRadioButton_CheckedChanged);
            // 
            // BinarizeRadioButton
            // 
            this.BinarizeRadioButton.Location = new System.Drawing.Point(24, 48);
            this.BinarizeRadioButton.Name = "BinarizeRadioButton";
            this.BinarizeRadioButton.Size = new System.Drawing.Size(128, 16);
            this.BinarizeRadioButton.TabIndex = 1;
            this.BinarizeRadioButton.Text = "Binarize";
            this.BinarizeRadioButton.CheckedChanged += new System.EventHandler(this.BinarizeRadioButton_CheckedChanged);
            // 
            // ColorDepthRadioButton
            // 
            this.ColorDepthRadioButton.Checked = true;
            this.ColorDepthRadioButton.Location = new System.Drawing.Point(24, 24);
            this.ColorDepthRadioButton.Name = "ColorDepthRadioButton";
            this.ColorDepthRadioButton.Size = new System.Drawing.Size(168, 16);
            this.ColorDepthRadioButton.TabIndex = 0;
            this.ColorDepthRadioButton.TabStop = true;
            this.ColorDepthRadioButton.Text = "ColorDepth";
            this.ColorDepthRadioButton.CheckedChanged += new System.EventHandler(this.ColorDepthRadioButton_CheckedChanged);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(200, 624);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(88, 24);
            this.OKButton.TabIndex = 3;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // BinarizeForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(504, 656);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.BinGroupBox);
            this.Controls.Add(this.DescriptionListBox);
            this.Controls.Add(this.BinarizeGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BinarizeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Binarize Options";
            this.Load += new System.EventHandler(this.BinarizeForm_Load);
            this.BinarizeGroupBox.ResumeLayout(false);
            this.BinarizeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContrastBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LowBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HighBar)).EndInit();
            this.BinGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void BinarizeForm_Load(object sender, System.EventArgs e)
		{
			BlurBox.SelectedIndex = 0;

			BinarizeGroupBox.Enabled = false;
		}

		private PegasusImaging.WinForms.ScanFix5.BinarizeOptions binOpts;

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			if (ColorDepthRadioButton.Checked==true)
			{
				proc1 = new PegasusImaging.WinForms.ImagXpress8.Processor(image.Copy());
				proc1.ColorDepth(1,0,0);
				image = proc1.Image;
			}
			else if (BinarizeRadioButton.Checked == true)
			{
				binOpts = new PegasusImaging.WinForms.ScanFix5.BinarizeOptions();
				binOpts.LceFactor = ContrastBar.Value;
				binOpts.LowThreshold = LowBar.Value;
				binOpts.HighThreshold = HighBar.Value;
				binOpts.PreBlurType = (PegasusImaging.WinForms.ScanFix5.BinarizeBlur)BlurBox.SelectedIndex;

                int DIB = image.ToHdib(false).ToInt32();

				scanFix1.FromHdib(new System.IntPtr(DIB));
				scanFix1.Binarize(binOpts);
				image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB));
			}
			else
			{
                int DIB = image.ToHdib(false).ToInt32();
                scanFix1.FromHdib(new System.IntPtr(DIB));
				scanFix1.AutoBinarize();
				image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(false));
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB));
			}
			Close();
		}

		private void LowBar_Scroll(object sender, System.EventArgs e)
		{
			if (LowBar.Value >= HighBar.Value)
			{
				LowBar.Value = HighBar.Value - 1;
				int valueNow = HighBar.Value - 1;
				LowLabel.Text = valueNow.ToString();
			}
			else
			{
				LowLabel.Text = LowBar.Value.ToString();
			}
		}

		private void HighBar_Scroll(object sender, System.EventArgs e)
		{
			if (HighBar.Value <= LowBar.Value)
			{
				HighBar.Value = LowBar.Value + 1;
				int valueNow = LowBar.Value + 1;
				HighLabel.Text = valueNow.ToString();
			}
			else
			{
				HighLabel.Text = HighBar.Value.ToString();
			}
		}

		private void ContrastBar_Scroll(object sender, System.EventArgs e)
		{
			ConLabel.Text = ContrastBar.Value.ToString();
		}

		private void ColorDepthRadioButton_CheckedChanged(object sender, System.EventArgs e)
		{
			BinarizeGroupBox.Enabled = false;
		}

		private void BinarizeRadioButton_CheckedChanged(object sender, System.EventArgs e)
		{
			BinarizeGroupBox.Enabled = true;
		}

		private void AutoRadioButton_CheckedChanged(object sender, System.EventArgs e)
		{
			BinarizeGroupBox.Enabled = false;
		}


	}
}

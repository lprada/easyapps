'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Imports PegasusImaging.WinForms.ScanFix5
Imports PegasusImaging.WinForms.BarcodeXpress5

Public Class FormOptions
    Inherits System.Windows.Forms.Form

    ' property variables
    Private _nBarcodeTypes As System.Array
    Private _barcodeInkColor As InkColorInfo
    Private _nMaxBarcodes As System.Int32
    Friend WithEvents QRRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents OneCodeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents RoyalRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents AusRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents AusGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents CustomStatesRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents CustomTableNRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents CustomTableCRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents CustomNoneRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents Code93ExtCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Code39ExtCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DiagRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ScanLabel2 As System.Windows.Forms.Label
    Friend WithEvents ScanBox As System.Windows.Forms.NumericUpDown
    Friend WithEvents ScanLabel As System.Windows.Forms.Label
    Friend WithEvents AppendCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ScanLabel3 As System.Windows.Forms.Label
    Private _barcodeOrientation As OrientationInfo
    Public cancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tabControl As System.Windows.Forms.TabControl
    Friend WithEvents tabPageType As System.Windows.Forms.TabPage
    Friend WithEvents frm1DTypes As System.Windows.Forms.GroupBox
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents _chkBCType_0 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_1 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_2 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_3 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_4 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_5 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_6 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_7 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_8 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_9 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_10 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_11 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_12 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_13 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_14 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_15 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_16 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_17 As System.Windows.Forms.CheckBox
    Friend WithEvents _chkBCType_18 As System.Windows.Forms.CheckBox
    Friend WithEvents cmdAll As System.Windows.Forms.Button
    Friend WithEvents fraType As System.Windows.Forms.GroupBox
    Friend WithEvents optTypeDataMatrix As System.Windows.Forms.RadioButton
    Friend WithEvents optType1D As System.Windows.Forms.RadioButton
    Friend WithEvents optTypePFD417 As System.Windows.Forms.RadioButton
    Friend WithEvents optTypePatchCode As System.Windows.Forms.RadioButton
    Friend WithEvents tabPageOrientation As System.Windows.Forms.TabPage
    Friend WithEvents fraOrient As System.Windows.Forms.Panel
    Friend WithEvents _optOrient_2 As System.Windows.Forms.RadioButton
    Friend WithEvents _optOrient_1 As System.Windows.Forms.RadioButton
    Friend WithEvents _optOrient_0 As System.Windows.Forms.RadioButton
    Friend WithEvents tabPageMisc As System.Windows.Forms.TabPage
    Friend WithEvents fraMisc As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMax As System.Windows.Forms.TextBox
    Friend WithEvents cboColor As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents buttonOK As System.Windows.Forms.Button
    Friend WithEvents buttonCancel As System.Windows.Forms.Button
    Friend WithEvents optTypePostNet As System.Windows.Forms.RadioButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonOK = New System.Windows.Forms.Button
        Me.tabControl = New System.Windows.Forms.TabControl
        Me.tabPageType = New System.Windows.Forms.TabPage
        Me.AusGroupBox = New System.Windows.Forms.GroupBox
        Me.CustomStatesRadioButton = New System.Windows.Forms.RadioButton
        Me.CustomTableNRadioButton = New System.Windows.Forms.RadioButton
        Me.CustomTableCRadioButton = New System.Windows.Forms.RadioButton
        Me.CustomNoneRadioButton = New System.Windows.Forms.RadioButton
        Me.frm1DTypes = New System.Windows.Forms.GroupBox
        Me.Code93ExtCheckBox = New System.Windows.Forms.CheckBox
        Me.Code39ExtCheckBox = New System.Windows.Forms.CheckBox
        Me.cmdClear = New System.Windows.Forms.Button
        Me._chkBCType_0 = New System.Windows.Forms.CheckBox
        Me._chkBCType_1 = New System.Windows.Forms.CheckBox
        Me._chkBCType_2 = New System.Windows.Forms.CheckBox
        Me._chkBCType_3 = New System.Windows.Forms.CheckBox
        Me._chkBCType_4 = New System.Windows.Forms.CheckBox
        Me._chkBCType_5 = New System.Windows.Forms.CheckBox
        Me._chkBCType_6 = New System.Windows.Forms.CheckBox
        Me._chkBCType_7 = New System.Windows.Forms.CheckBox
        Me._chkBCType_8 = New System.Windows.Forms.CheckBox
        Me._chkBCType_9 = New System.Windows.Forms.CheckBox
        Me._chkBCType_10 = New System.Windows.Forms.CheckBox
        Me._chkBCType_11 = New System.Windows.Forms.CheckBox
        Me._chkBCType_12 = New System.Windows.Forms.CheckBox
        Me._chkBCType_13 = New System.Windows.Forms.CheckBox
        Me._chkBCType_14 = New System.Windows.Forms.CheckBox
        Me._chkBCType_15 = New System.Windows.Forms.CheckBox
        Me._chkBCType_16 = New System.Windows.Forms.CheckBox
        Me._chkBCType_17 = New System.Windows.Forms.CheckBox
        Me._chkBCType_18 = New System.Windows.Forms.CheckBox
        Me.cmdAll = New System.Windows.Forms.Button
        Me.fraType = New System.Windows.Forms.GroupBox
        Me.QRRadioButton = New System.Windows.Forms.RadioButton
        Me.OneCodeRadioButton = New System.Windows.Forms.RadioButton
        Me.RoyalRadioButton = New System.Windows.Forms.RadioButton
        Me.AusRadioButton = New System.Windows.Forms.RadioButton
        Me.optTypePostNet = New System.Windows.Forms.RadioButton
        Me.optTypeDataMatrix = New System.Windows.Forms.RadioButton
        Me.optType1D = New System.Windows.Forms.RadioButton
        Me.optTypePFD417 = New System.Windows.Forms.RadioButton
        Me.optTypePatchCode = New System.Windows.Forms.RadioButton
        Me.tabPageOrientation = New System.Windows.Forms.TabPage
        Me.fraOrient = New System.Windows.Forms.Panel
        Me.DiagRadioButton = New System.Windows.Forms.RadioButton
        Me._optOrient_2 = New System.Windows.Forms.RadioButton
        Me._optOrient_1 = New System.Windows.Forms.RadioButton
        Me._optOrient_0 = New System.Windows.Forms.RadioButton
        Me.tabPageMisc = New System.Windows.Forms.TabPage
        Me.fraMisc = New System.Windows.Forms.Panel
        Me.ScanLabel3 = New System.Windows.Forms.Label
        Me.ScanLabel2 = New System.Windows.Forms.Label
        Me.ScanBox = New System.Windows.Forms.NumericUpDown
        Me.ScanLabel = New System.Windows.Forms.Label
        Me.AppendCheckBox = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtMax = New System.Windows.Forms.TextBox
        Me.cboColor = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.buttonCancel = New System.Windows.Forms.Button
        Me.tabControl.SuspendLayout()
        Me.tabPageType.SuspendLayout()
        Me.AusGroupBox.SuspendLayout()
        Me.frm1DTypes.SuspendLayout()
        Me.fraType.SuspendLayout()
        Me.tabPageOrientation.SuspendLayout()
        Me.fraOrient.SuspendLayout()
        Me.tabPageMisc.SuspendLayout()
        Me.fraMisc.SuspendLayout()
        CType(Me.ScanBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'buttonOK
        '
        Me.buttonOK.Location = New System.Drawing.Point(163, 361)
        Me.buttonOK.Name = "buttonOK"
        Me.buttonOK.Size = New System.Drawing.Size(105, 24)
        Me.buttonOK.TabIndex = 31
        Me.buttonOK.Text = "OK"
        '
        'tabControl
        '
        Me.tabControl.Controls.Add(Me.tabPageType)
        Me.tabControl.Controls.Add(Me.tabPageOrientation)
        Me.tabControl.Controls.Add(Me.tabPageMisc)
        Me.tabControl.Location = New System.Drawing.Point(7, 4)
        Me.tabControl.Name = "tabControl"
        Me.tabControl.SelectedIndex = 0
        Me.tabControl.Size = New System.Drawing.Size(500, 351)
        Me.tabControl.TabIndex = 29
        '
        'tabPageType
        '
        Me.tabPageType.Controls.Add(Me.AusGroupBox)
        Me.tabPageType.Controls.Add(Me.frm1DTypes)
        Me.tabPageType.Controls.Add(Me.fraType)
        Me.tabPageType.Location = New System.Drawing.Point(4, 22)
        Me.tabPageType.Name = "tabPageType"
        Me.tabPageType.Size = New System.Drawing.Size(492, 325)
        Me.tabPageType.TabIndex = 0
        Me.tabPageType.Text = "Barcode Type"
        '
        'AusGroupBox
        '
        Me.AusGroupBox.Controls.Add(Me.CustomStatesRadioButton)
        Me.AusGroupBox.Controls.Add(Me.CustomTableNRadioButton)
        Me.AusGroupBox.Controls.Add(Me.CustomTableCRadioButton)
        Me.AusGroupBox.Controls.Add(Me.CustomNoneRadioButton)
        Me.AusGroupBox.Enabled = False
        Me.AusGroupBox.Location = New System.Drawing.Point(309, 12)
        Me.AusGroupBox.Name = "AusGroupBox"
        Me.AusGroupBox.Size = New System.Drawing.Size(170, 115)
        Me.AusGroupBox.TabIndex = 28
        Me.AusGroupBox.TabStop = False
        Me.AusGroupBox.Text = "Aus Post Custom Info Section"
        '
        'CustomStatesRadioButton
        '
        Me.CustomStatesRadioButton.AutoSize = True
        Me.CustomStatesRadioButton.Location = New System.Drawing.Point(19, 87)
        Me.CustomStatesRadioButton.Name = "CustomStatesRadioButton"
        Me.CustomStatesRadioButton.Size = New System.Drawing.Size(71, 17)
        Me.CustomStatesRadioButton.TabIndex = 3
        Me.CustomStatesRadioButton.Text = "BarStates"
        Me.CustomStatesRadioButton.UseVisualStyleBackColor = True
        '
        'CustomTableNRadioButton
        '
        Me.CustomTableNRadioButton.AutoSize = True
        Me.CustomTableNRadioButton.Location = New System.Drawing.Point(19, 65)
        Me.CustomTableNRadioButton.Name = "CustomTableNRadioButton"
        Me.CustomTableNRadioButton.Size = New System.Drawing.Size(60, 17)
        Me.CustomTableNRadioButton.TabIndex = 2
        Me.CustomTableNRadioButton.Text = "TableN"
        Me.CustomTableNRadioButton.UseVisualStyleBackColor = True
        '
        'CustomTableCRadioButton
        '
        Me.CustomTableCRadioButton.AutoSize = True
        Me.CustomTableCRadioButton.Location = New System.Drawing.Point(19, 43)
        Me.CustomTableCRadioButton.Name = "CustomTableCRadioButton"
        Me.CustomTableCRadioButton.Size = New System.Drawing.Size(59, 17)
        Me.CustomTableCRadioButton.TabIndex = 1
        Me.CustomTableCRadioButton.Text = "TableC"
        Me.CustomTableCRadioButton.UseVisualStyleBackColor = True
        '
        'CustomNoneRadioButton
        '
        Me.CustomNoneRadioButton.AutoSize = True
        Me.CustomNoneRadioButton.Checked = True
        Me.CustomNoneRadioButton.Location = New System.Drawing.Point(19, 20)
        Me.CustomNoneRadioButton.Name = "CustomNoneRadioButton"
        Me.CustomNoneRadioButton.Size = New System.Drawing.Size(51, 17)
        Me.CustomNoneRadioButton.TabIndex = 0
        Me.CustomNoneRadioButton.TabStop = True
        Me.CustomNoneRadioButton.Text = "None"
        Me.CustomNoneRadioButton.UseVisualStyleBackColor = True
        '
        'frm1DTypes
        '
        Me.frm1DTypes.BackColor = System.Drawing.SystemColors.Control
        Me.frm1DTypes.Controls.Add(Me.Code93ExtCheckBox)
        Me.frm1DTypes.Controls.Add(Me.Code39ExtCheckBox)
        Me.frm1DTypes.Controls.Add(Me.cmdClear)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_0)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_1)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_2)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_3)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_4)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_5)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_6)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_7)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_8)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_9)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_10)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_11)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_12)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_13)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_14)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_15)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_16)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_17)
        Me.frm1DTypes.Controls.Add(Me._chkBCType_18)
        Me.frm1DTypes.Controls.Add(Me.cmdAll)
        Me.frm1DTypes.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.frm1DTypes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.frm1DTypes.Location = New System.Drawing.Point(16, 134)
        Me.frm1DTypes.Name = "frm1DTypes"
        Me.frm1DTypes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.frm1DTypes.Size = New System.Drawing.Size(463, 185)
        Me.frm1DTypes.TabIndex = 27
        Me.frm1DTypes.TabStop = False
        Me.frm1DTypes.Text = "1D Barcode Types: "
        '
        'Code93ExtCheckBox
        '
        Me.Code93ExtCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.Code93ExtCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.Code93ExtCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Code93ExtCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Code93ExtCheckBox.Location = New System.Drawing.Point(337, 40)
        Me.Code93ExtCheckBox.Name = "Code93ExtCheckBox"
        Me.Code93ExtCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Code93ExtCheckBox.Size = New System.Drawing.Size(105, 18)
        Me.Code93ExtCheckBox.TabIndex = 46
        Me.Code93ExtCheckBox.Text = "Code 93 Ext"
        Me.Code93ExtCheckBox.UseVisualStyleBackColor = False
        '
        'Code39ExtCheckBox
        '
        Me.Code39ExtCheckBox.BackColor = System.Drawing.SystemColors.Control
        Me.Code39ExtCheckBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.Code39ExtCheckBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Code39ExtCheckBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Code39ExtCheckBox.Location = New System.Drawing.Point(337, 16)
        Me.Code39ExtCheckBox.Name = "Code39ExtCheckBox"
        Me.Code39ExtCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Code39ExtCheckBox.Size = New System.Drawing.Size(105, 18)
        Me.Code39ExtCheckBox.TabIndex = 45
        Me.Code39ExtCheckBox.Text = "Code 39 Ext"
        Me.Code39ExtCheckBox.UseVisualStyleBackColor = False
        '
        'cmdClear
        '
        Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClear.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClear.Location = New System.Drawing.Point(248, 155)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClear.Size = New System.Drawing.Size(97, 22)
        Me.cmdClear.TabIndex = 44
        Me.cmdClear.Text = "&Clear"
        Me.cmdClear.UseVisualStyleBackColor = False
        '
        '_chkBCType_0
        '
        Me._chkBCType_0.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_0.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_0.Location = New System.Drawing.Point(9, 16)
        Me._chkBCType_0.Name = "_chkBCType_0"
        Me._chkBCType_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_0.Size = New System.Drawing.Size(121, 18)
        Me._chkBCType_0.TabIndex = 43
        Me._chkBCType_0.Text = "Code 2 of 5"
        Me._chkBCType_0.UseVisualStyleBackColor = False
        '
        '_chkBCType_1
        '
        Me._chkBCType_1.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_1.Location = New System.Drawing.Point(9, 31)
        Me._chkBCType_1.Name = "_chkBCType_1"
        Me._chkBCType_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_1.Size = New System.Drawing.Size(121, 34)
        Me._chkBCType_1.TabIndex = 42
        Me._chkBCType_1.Text = "Interleaved 2 of 5"
        Me._chkBCType_1.UseVisualStyleBackColor = False
        '
        '_chkBCType_2
        '
        Me._chkBCType_2.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_2.Location = New System.Drawing.Point(9, 55)
        Me._chkBCType_2.Name = "_chkBCType_2"
        Me._chkBCType_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_2.Size = New System.Drawing.Size(121, 34)
        Me._chkBCType_2.TabIndex = 41
        Me._chkBCType_2.Text = "Airline 2 of 5"
        Me._chkBCType_2.UseVisualStyleBackColor = False
        '
        '_chkBCType_3
        '
        Me._chkBCType_3.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_3.Location = New System.Drawing.Point(9, 79)
        Me._chkBCType_3.Name = "_chkBCType_3"
        Me._chkBCType_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_3.Size = New System.Drawing.Size(121, 34)
        Me._chkBCType_3.TabIndex = 40
        Me._chkBCType_3.Text = "DataLogic 2 of 5"
        Me._chkBCType_3.UseVisualStyleBackColor = False
        '
        '_chkBCType_4
        '
        Me._chkBCType_4.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_4.Location = New System.Drawing.Point(9, 106)
        Me._chkBCType_4.Name = "_chkBCType_4"
        Me._chkBCType_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_4.Size = New System.Drawing.Size(121, 29)
        Me._chkBCType_4.TabIndex = 39
        Me._chkBCType_4.Text = "Invert 2 of 5"
        Me._chkBCType_4.UseVisualStyleBackColor = False
        '
        '_chkBCType_5
        '
        Me._chkBCType_5.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_5.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_5.Location = New System.Drawing.Point(9, 134)
        Me._chkBCType_5.Name = "_chkBCType_5"
        Me._chkBCType_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_5.Size = New System.Drawing.Size(121, 23)
        Me._chkBCType_5.TabIndex = 38
        Me._chkBCType_5.Text = "BCD Matrix"
        Me._chkBCType_5.UseVisualStyleBackColor = False
        '
        '_chkBCType_6
        '
        Me._chkBCType_6.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_6.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_6.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_6.Location = New System.Drawing.Point(9, 155)
        Me._chkBCType_6.Name = "_chkBCType_6"
        Me._chkBCType_6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_6.Size = New System.Drawing.Size(121, 29)
        Me._chkBCType_6.TabIndex = 37
        Me._chkBCType_6.Text = "Matrix 2 of 5"
        Me._chkBCType_6.UseVisualStyleBackColor = False
        '
        '_chkBCType_7
        '
        Me._chkBCType_7.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_7.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_7.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_7.Location = New System.Drawing.Point(136, 16)
        Me._chkBCType_7.Name = "_chkBCType_7"
        Me._chkBCType_7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_7.Size = New System.Drawing.Size(105, 17)
        Me._chkBCType_7.TabIndex = 36
        Me._chkBCType_7.Text = "Code 32"
        Me._chkBCType_7.UseVisualStyleBackColor = False
        '
        '_chkBCType_8
        '
        Me._chkBCType_8.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_8.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_8.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_8.Location = New System.Drawing.Point(136, 40)
        Me._chkBCType_8.Name = "_chkBCType_8"
        Me._chkBCType_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_8.Size = New System.Drawing.Size(105, 17)
        Me._chkBCType_8.TabIndex = 35
        Me._chkBCType_8.Text = "Code 39"
        Me._chkBCType_8.UseVisualStyleBackColor = False
        '
        '_chkBCType_9
        '
        Me._chkBCType_9.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_9.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_9.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_9.Location = New System.Drawing.Point(136, 60)
        Me._chkBCType_9.Name = "_chkBCType_9"
        Me._chkBCType_9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_9.Size = New System.Drawing.Size(105, 25)
        Me._chkBCType_9.TabIndex = 34
        Me._chkBCType_9.Text = "CODABAR"
        Me._chkBCType_9.UseVisualStyleBackColor = False
        '
        '_chkBCType_10
        '
        Me._chkBCType_10.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_10.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_10.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_10.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_10.Location = New System.Drawing.Point(136, 88)
        Me._chkBCType_10.Name = "_chkBCType_10"
        Me._chkBCType_10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_10.Size = New System.Drawing.Size(105, 22)
        Me._chkBCType_10.TabIndex = 33
        Me._chkBCType_10.Text = "Code 93"
        Me._chkBCType_10.UseVisualStyleBackColor = False
        '
        '_chkBCType_11
        '
        Me._chkBCType_11.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_11.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_11.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_11.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_11.Location = New System.Drawing.Point(136, 106)
        Me._chkBCType_11.Name = "_chkBCType_11"
        Me._chkBCType_11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_11.Size = New System.Drawing.Size(105, 31)
        Me._chkBCType_11.TabIndex = 32
        Me._chkBCType_11.Text = "Code 128"
        Me._chkBCType_11.UseVisualStyleBackColor = False
        '
        '_chkBCType_12
        '
        Me._chkBCType_12.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_12.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_12.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_12.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_12.Location = New System.Drawing.Point(136, 134)
        Me._chkBCType_12.Name = "_chkBCType_12"
        Me._chkBCType_12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_12.Size = New System.Drawing.Size(105, 23)
        Me._chkBCType_12.TabIndex = 31
        Me._chkBCType_12.Text = "EAN 13"
        Me._chkBCType_12.UseVisualStyleBackColor = False
        '
        '_chkBCType_13
        '
        Me._chkBCType_13.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_13.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_13.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_13.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_13.Location = New System.Drawing.Point(136, 155)
        Me._chkBCType_13.Name = "_chkBCType_13"
        Me._chkBCType_13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_13.Size = New System.Drawing.Size(105, 22)
        Me._chkBCType_13.TabIndex = 30
        Me._chkBCType_13.Text = "EAN 8"
        Me._chkBCType_13.UseVisualStyleBackColor = False
        '
        '_chkBCType_14
        '
        Me._chkBCType_14.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_14.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_14.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_14.Location = New System.Drawing.Point(248, 16)
        Me._chkBCType_14.Name = "_chkBCType_14"
        Me._chkBCType_14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_14.Size = New System.Drawing.Size(105, 18)
        Me._chkBCType_14.TabIndex = 29
        Me._chkBCType_14.Text = "UPC-A"
        Me._chkBCType_14.UseVisualStyleBackColor = False
        '
        '_chkBCType_15
        '
        Me._chkBCType_15.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_15.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_15.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_15.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_15.Location = New System.Drawing.Point(248, 40)
        Me._chkBCType_15.Name = "_chkBCType_15"
        Me._chkBCType_15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_15.Size = New System.Drawing.Size(105, 17)
        Me._chkBCType_15.TabIndex = 28
        Me._chkBCType_15.Text = "UPC-E"
        Me._chkBCType_15.UseVisualStyleBackColor = False
        '
        '_chkBCType_16
        '
        Me._chkBCType_16.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_16.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_16.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_16.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_16.Location = New System.Drawing.Point(248, 64)
        Me._chkBCType_16.Name = "_chkBCType_16"
        Me._chkBCType_16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_16.Size = New System.Drawing.Size(105, 17)
        Me._chkBCType_16.TabIndex = 27
        Me._chkBCType_16.Text = "ADD 5"
        Me._chkBCType_16.UseVisualStyleBackColor = False
        '
        '_chkBCType_17
        '
        Me._chkBCType_17.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_17.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_17.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_17.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_17.Location = New System.Drawing.Point(248, 88)
        Me._chkBCType_17.Name = "_chkBCType_17"
        Me._chkBCType_17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_17.Size = New System.Drawing.Size(105, 17)
        Me._chkBCType_17.TabIndex = 26
        Me._chkBCType_17.Text = "Add 2"
        Me._chkBCType_17.UseVisualStyleBackColor = False
        '
        '_chkBCType_18
        '
        Me._chkBCType_18.BackColor = System.Drawing.SystemColors.Control
        Me._chkBCType_18.Cursor = System.Windows.Forms.Cursors.Default
        Me._chkBCType_18.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._chkBCType_18.ForeColor = System.Drawing.SystemColors.ControlText
        Me._chkBCType_18.Location = New System.Drawing.Point(248, 112)
        Me._chkBCType_18.Name = "_chkBCType_18"
        Me._chkBCType_18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._chkBCType_18.Size = New System.Drawing.Size(105, 17)
        Me._chkBCType_18.TabIndex = 25
        Me._chkBCType_18.Text = "UCC/EAN 128"
        Me._chkBCType_18.UseVisualStyleBackColor = False
        '
        'cmdAll
        '
        Me.cmdAll.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdAll.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAll.Location = New System.Drawing.Point(248, 131)
        Me.cmdAll.Name = "cmdAll"
        Me.cmdAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdAll.Size = New System.Drawing.Size(97, 22)
        Me.cmdAll.TabIndex = 24
        Me.cmdAll.Text = "&All (unknown)"
        Me.cmdAll.UseVisualStyleBackColor = False
        '
        'fraType
        '
        Me.fraType.BackColor = System.Drawing.SystemColors.Control
        Me.fraType.Controls.Add(Me.QRRadioButton)
        Me.fraType.Controls.Add(Me.OneCodeRadioButton)
        Me.fraType.Controls.Add(Me.RoyalRadioButton)
        Me.fraType.Controls.Add(Me.AusRadioButton)
        Me.fraType.Controls.Add(Me.optTypePostNet)
        Me.fraType.Controls.Add(Me.optTypeDataMatrix)
        Me.fraType.Controls.Add(Me.optType1D)
        Me.fraType.Controls.Add(Me.optTypePFD417)
        Me.fraType.Controls.Add(Me.optTypePatchCode)
        Me.fraType.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraType.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraType.Location = New System.Drawing.Point(16, 16)
        Me.fraType.Name = "fraType"
        Me.fraType.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraType.Size = New System.Drawing.Size(282, 112)
        Me.fraType.TabIndex = 26
        Me.fraType.TabStop = False
        Me.fraType.Text = "Barcode Type: "
        '
        'QRRadioButton
        '
        Me.QRRadioButton.Location = New System.Drawing.Point(112, 85)
        Me.QRRadioButton.Name = "QRRadioButton"
        Me.QRRadioButton.Size = New System.Drawing.Size(80, 16)
        Me.QRRadioButton.TabIndex = 50
        Me.QRRadioButton.Text = "QR"
        '
        'OneCodeRadioButton
        '
        Me.OneCodeRadioButton.BackColor = System.Drawing.SystemColors.Control
        Me.OneCodeRadioButton.Cursor = System.Windows.Forms.Cursors.Default
        Me.OneCodeRadioButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OneCodeRadioButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.OneCodeRadioButton.Location = New System.Drawing.Point(112, 62)
        Me.OneCodeRadioButton.Name = "OneCodeRadioButton"
        Me.OneCodeRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.OneCodeRadioButton.Size = New System.Drawing.Size(89, 17)
        Me.OneCodeRadioButton.TabIndex = 49
        Me.OneCodeRadioButton.TabStop = True
        Me.OneCodeRadioButton.Text = "IntelligentMail"
        Me.OneCodeRadioButton.UseVisualStyleBackColor = False
        '
        'RoyalRadioButton
        '
        Me.RoyalRadioButton.Location = New System.Drawing.Point(16, 84)
        Me.RoyalRadioButton.Name = "RoyalRadioButton"
        Me.RoyalRadioButton.Size = New System.Drawing.Size(80, 16)
        Me.RoyalRadioButton.TabIndex = 48
        Me.RoyalRadioButton.Text = "Royal Post"
        '
        'AusRadioButton
        '
        Me.AusRadioButton.BackColor = System.Drawing.SystemColors.Control
        Me.AusRadioButton.Cursor = System.Windows.Forms.Cursors.Default
        Me.AusRadioButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AusRadioButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AusRadioButton.Location = New System.Drawing.Point(16, 61)
        Me.AusRadioButton.Name = "AusRadioButton"
        Me.AusRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AusRadioButton.Size = New System.Drawing.Size(125, 17)
        Me.AusRadioButton.TabIndex = 47
        Me.AusRadioButton.TabStop = True
        Me.AusRadioButton.Text = "Australian Post"
        Me.AusRadioButton.UseVisualStyleBackColor = False
        '
        'optTypePostNet
        '
        Me.optTypePostNet.Location = New System.Drawing.Point(16, 39)
        Me.optTypePostNet.Name = "optTypePostNet"
        Me.optTypePostNet.Size = New System.Drawing.Size(80, 16)
        Me.optTypePostNet.TabIndex = 46
        Me.optTypePostNet.Text = "Post&Net"
        '
        'optTypeDataMatrix
        '
        Me.optTypeDataMatrix.BackColor = System.Drawing.SystemColors.Control
        Me.optTypeDataMatrix.Cursor = System.Windows.Forms.Cursors.Default
        Me.optTypeDataMatrix.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optTypeDataMatrix.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optTypeDataMatrix.Location = New System.Drawing.Point(112, 39)
        Me.optTypeDataMatrix.Name = "optTypeDataMatrix"
        Me.optTypeDataMatrix.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optTypeDataMatrix.Size = New System.Drawing.Size(79, 17)
        Me.optTypeDataMatrix.TabIndex = 45
        Me.optTypeDataMatrix.TabStop = True
        Me.optTypeDataMatrix.Text = "&Data Matrix"
        Me.optTypeDataMatrix.UseVisualStyleBackColor = False
        '
        'optType1D
        '
        Me.optType1D.BackColor = System.Drawing.SystemColors.Control
        Me.optType1D.Cursor = System.Windows.Forms.Cursors.Default
        Me.optType1D.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optType1D.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optType1D.Location = New System.Drawing.Point(16, 16)
        Me.optType1D.Name = "optType1D"
        Me.optType1D.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optType1D.Size = New System.Drawing.Size(89, 17)
        Me.optType1D.TabIndex = 22
        Me.optType1D.TabStop = True
        Me.optType1D.Text = "&1D (standard)"
        Me.optType1D.UseVisualStyleBackColor = False
        '
        'optTypePFD417
        '
        Me.optTypePFD417.BackColor = System.Drawing.SystemColors.Control
        Me.optTypePFD417.Cursor = System.Windows.Forms.Cursors.Default
        Me.optTypePFD417.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optTypePFD417.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optTypePFD417.Location = New System.Drawing.Point(205, 16)
        Me.optTypePFD417.Name = "optTypePFD417"
        Me.optTypePFD417.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optTypePFD417.Size = New System.Drawing.Size(72, 17)
        Me.optTypePFD417.TabIndex = 21
        Me.optTypePFD417.TabStop = True
        Me.optTypePFD417.Text = "PDF-417"
        Me.optTypePFD417.UseVisualStyleBackColor = False
        '
        'optTypePatchCode
        '
        Me.optTypePatchCode.BackColor = System.Drawing.SystemColors.Control
        Me.optTypePatchCode.Cursor = System.Windows.Forms.Cursors.Default
        Me.optTypePatchCode.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optTypePatchCode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optTypePatchCode.Location = New System.Drawing.Point(112, 16)
        Me.optTypePatchCode.Name = "optTypePatchCode"
        Me.optTypePatchCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optTypePatchCode.Size = New System.Drawing.Size(104, 16)
        Me.optTypePatchCode.TabIndex = 20
        Me.optTypePatchCode.TabStop = True
        Me.optTypePatchCode.Text = "&Patch Code"
        Me.optTypePatchCode.UseVisualStyleBackColor = False
        '
        'tabPageOrientation
        '
        Me.tabPageOrientation.Controls.Add(Me.fraOrient)
        Me.tabPageOrientation.Location = New System.Drawing.Point(4, 22)
        Me.tabPageOrientation.Name = "tabPageOrientation"
        Me.tabPageOrientation.Size = New System.Drawing.Size(492, 325)
        Me.tabPageOrientation.TabIndex = 1
        Me.tabPageOrientation.Text = "Orientation"
        Me.tabPageOrientation.Visible = False
        '
        'fraOrient
        '
        Me.fraOrient.BackColor = System.Drawing.SystemColors.Control
        Me.fraOrient.Controls.Add(Me.DiagRadioButton)
        Me.fraOrient.Controls.Add(Me._optOrient_2)
        Me.fraOrient.Controls.Add(Me._optOrient_1)
        Me.fraOrient.Controls.Add(Me._optOrient_0)
        Me.fraOrient.Cursor = System.Windows.Forms.Cursors.Default
        Me.fraOrient.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraOrient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraOrient.Location = New System.Drawing.Point(49, 51)
        Me.fraOrient.Name = "fraOrient"
        Me.fraOrient.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraOrient.Size = New System.Drawing.Size(374, 168)
        Me.fraOrient.TabIndex = 11
        '
        'DiagRadioButton
        '
        Me.DiagRadioButton.BackColor = System.Drawing.SystemColors.Control
        Me.DiagRadioButton.Cursor = System.Windows.Forms.Cursors.Default
        Me.DiagRadioButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiagRadioButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me.DiagRadioButton.Location = New System.Drawing.Point(104, 109)
        Me.DiagRadioButton.Name = "DiagRadioButton"
        Me.DiagRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DiagRadioButton.Size = New System.Drawing.Size(267, 17)
        Me.DiagRadioButton.TabIndex = 14
        Me.DiagRadioButton.TabStop = True
        Me.DiagRadioButton.Text = "Vertical, Horizontal, 45 Degree Angled Barcodes"
        Me.DiagRadioButton.UseVisualStyleBackColor = False
        '
        '_optOrient_2
        '
        Me._optOrient_2.BackColor = System.Drawing.SystemColors.Control
        Me._optOrient_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._optOrient_2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._optOrient_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me._optOrient_2.Location = New System.Drawing.Point(104, 86)
        Me._optOrient_2.Name = "_optOrient_2"
        Me._optOrient_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optOrient_2.Size = New System.Drawing.Size(177, 17)
        Me._optOrient_2.TabIndex = 13
        Me._optOrient_2.TabStop = True
        Me._optOrient_2.Text = "Vertical and Horizontal Barcodes"
        Me._optOrient_2.UseVisualStyleBackColor = False
        '
        '_optOrient_1
        '
        Me._optOrient_1.BackColor = System.Drawing.SystemColors.Control
        Me._optOrient_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._optOrient_1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._optOrient_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me._optOrient_1.Location = New System.Drawing.Point(104, 63)
        Me._optOrient_1.Name = "_optOrient_1"
        Me._optOrient_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optOrient_1.Size = New System.Drawing.Size(121, 17)
        Me._optOrient_1.TabIndex = 12
        Me._optOrient_1.TabStop = True
        Me._optOrient_1.Text = "Horizontal Barcodes"
        Me._optOrient_1.UseVisualStyleBackColor = False
        '
        '_optOrient_0
        '
        Me._optOrient_0.BackColor = System.Drawing.SystemColors.Control
        Me._optOrient_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._optOrient_0.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._optOrient_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me._optOrient_0.Location = New System.Drawing.Point(104, 40)
        Me._optOrient_0.Name = "_optOrient_0"
        Me._optOrient_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optOrient_0.Size = New System.Drawing.Size(113, 17)
        Me._optOrient_0.TabIndex = 11
        Me._optOrient_0.TabStop = True
        Me._optOrient_0.Text = "Vertical Barcodes"
        Me._optOrient_0.UseVisualStyleBackColor = False
        '
        'tabPageMisc
        '
        Me.tabPageMisc.Controls.Add(Me.fraMisc)
        Me.tabPageMisc.Location = New System.Drawing.Point(4, 22)
        Me.tabPageMisc.Name = "tabPageMisc"
        Me.tabPageMisc.Size = New System.Drawing.Size(492, 325)
        Me.tabPageMisc.TabIndex = 2
        Me.tabPageMisc.Text = "Miscellaneous"
        Me.tabPageMisc.Visible = False
        '
        'fraMisc
        '
        Me.fraMisc.BackColor = System.Drawing.SystemColors.Control
        Me.fraMisc.Controls.Add(Me.ScanLabel3)
        Me.fraMisc.Controls.Add(Me.ScanLabel2)
        Me.fraMisc.Controls.Add(Me.ScanBox)
        Me.fraMisc.Controls.Add(Me.ScanLabel)
        Me.fraMisc.Controls.Add(Me.AppendCheckBox)
        Me.fraMisc.Controls.Add(Me.Label3)
        Me.fraMisc.Controls.Add(Me.txtMax)
        Me.fraMisc.Controls.Add(Me.cboColor)
        Me.fraMisc.Controls.Add(Me.Label1)
        Me.fraMisc.Cursor = System.Windows.Forms.Cursors.Default
        Me.fraMisc.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraMisc.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraMisc.Location = New System.Drawing.Point(32, 51)
        Me.fraMisc.Name = "fraMisc"
        Me.fraMisc.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMisc.Size = New System.Drawing.Size(438, 169)
        Me.fraMisc.TabIndex = 15
        '
        'ScanLabel3
        '
        Me.ScanLabel3.AutoSize = True
        Me.ScanLabel3.Location = New System.Drawing.Point(210, 19)
        Me.ScanLabel3.Name = "ScanLabel3"
        Me.ScanLabel3.Size = New System.Drawing.Size(37, 14)
        Me.ScanLabel3.TabIndex = 22
        Me.ScanLabel3.Text = "(1-10)"
        '
        'ScanLabel2
        '
        Me.ScanLabel2.AutoSize = True
        Me.ScanLabel2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ScanLabel2.Location = New System.Drawing.Point(264, 19)
        Me.ScanLabel2.Name = "ScanLabel2"
        Me.ScanLabel2.Size = New System.Drawing.Size(151, 14)
        Me.ScanLabel2.TabIndex = 21
        Me.ScanLabel2.Text = "(Only applies to 1D barcodes)"
        '
        'ScanBox
        '
        Me.ScanBox.Location = New System.Drawing.Point(154, 17)
        Me.ScanBox.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.ScanBox.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.ScanBox.Name = "ScanBox"
        Me.ScanBox.Size = New System.Drawing.Size(50, 20)
        Me.ScanBox.TabIndex = 20
        Me.ScanBox.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'ScanLabel
        '
        Me.ScanLabel.AutoSize = True
        Me.ScanLabel.Location = New System.Drawing.Point(70, 19)
        Me.ScanLabel.Name = "ScanLabel"
        Me.ScanLabel.Size = New System.Drawing.Size(77, 14)
        Me.ScanLabel.TabIndex = 19
        Me.ScanLabel.Text = "ScanDistance:"
        '
        'AppendCheckBox
        '
        Me.AppendCheckBox.AutoSize = True
        Me.AppendCheckBox.Location = New System.Drawing.Point(156, 117)
        Me.AppendCheckBox.Name = "AppendCheckBox"
        Me.AppendCheckBox.Size = New System.Drawing.Size(118, 18)
        Me.AppendCheckBox.TabIndex = 16
        Me.AppendCheckBox.Text = "Append CheckSum"
        Me.AppendCheckBox.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(11, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(145, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Max. Barcodes Per Image: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtMax
        '
        Me.txtMax.AcceptsReturn = True
        Me.txtMax.BackColor = System.Drawing.SystemColors.Window
        Me.txtMax.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMax.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMax.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMax.Location = New System.Drawing.Point(156, 80)
        Me.txtMax.MaxLength = 0
        Me.txtMax.Name = "txtMax"
        Me.txtMax.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMax.Size = New System.Drawing.Size(73, 20)
        Me.txtMax.TabIndex = 18
        '
        'cboColor
        '
        Me.cboColor.BackColor = System.Drawing.SystemColors.Window
        Me.cboColor.Cursor = System.Windows.Forms.Cursors.Default
        Me.cboColor.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboColor.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cboColor.Location = New System.Drawing.Point(156, 48)
        Me.cboColor.Name = "cboColor"
        Me.cboColor.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cboColor.Size = New System.Drawing.Size(129, 22)
        Me.cboColor.TabIndex = 16
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(11, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(145, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Barcode Color: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'buttonCancel
        '
        Me.buttonCancel.Location = New System.Drawing.Point(291, 361)
        Me.buttonCancel.Name = "buttonCancel"
        Me.buttonCancel.Size = New System.Drawing.Size(105, 24)
        Me.buttonCancel.TabIndex = 30
        Me.buttonCancel.Text = "Cancel"
        '
        'FormOptions
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(519, 393)
        Me.Controls.Add(Me.buttonOK)
        Me.Controls.Add(Me.tabControl)
        Me.Controls.Add(Me.buttonCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Options..."
        Me.tabControl.ResumeLayout(False)
        Me.tabPageType.ResumeLayout(False)
        Me.AusGroupBox.ResumeLayout(False)
        Me.AusGroupBox.PerformLayout()
        Me.frm1DTypes.ResumeLayout(False)
        Me.fraType.ResumeLayout(False)
        Me.tabPageOrientation.ResumeLayout(False)
        Me.fraOrient.ResumeLayout(False)
        Me.tabPageMisc.ResumeLayout(False)
        Me.fraMisc.ResumeLayout(False)
        Me.fraMisc.PerformLayout()
        CType(Me.ScanBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FormOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboColor.Items.Add("Unknown Color")
        cboColor.Items.Add("White")
        cboColor.Items.Add("Black")
        Select Case (_barcodeInkColor)
            Case InkColorInfo.Black
                cboColor.SelectedIndex = 2
                Exit Select

            Case InkColorInfo.White
                cboColor.SelectedIndex = 1
                Exit Select

            Case InkColorInfo.Unknown
                cboColor.SelectedIndex = 0
                Exit Select
        End Select
        txtMax.Text = _nMaxBarcodes.ToString()

        _optOrient_0.Checked = False
        _optOrient_1.Checked = False
        _optOrient_2.Checked = False

        Select Case (_barcodeOrientation)
            Case OrientationInfo.HorizontalVertical
                _optOrient_2.Checked = True
                Exit Select

            Case OrientationInfo.Vertical
                _optOrient_0.Checked = True
                Exit Select

            Case OrientationInfo.Horizontal
                _optOrient_1.Checked = True

            Case OrientationInfo.HorizontalVerticalDiagonal
                DiagRadioButton.Checked = True
                Exit Select
        End Select

        If (_chkBCType_0.Enabled = True And _chkBCType_1.Enabled = True And _chkBCType_2.Enabled = True And _chkBCType_3.Enabled = True And _chkBCType_4.Enabled = True And _chkBCType_5.Enabled = True And _chkBCType_6.Enabled = True And _chkBCType_7.Enabled = True And _chkBCType_8.Enabled = True And _chkBCType_9.Enabled = True And _chkBCType_10.Enabled = True And _chkBCType_11.Enabled = True And _chkBCType_12.Enabled = True And _chkBCType_13.Enabled = True And _chkBCType_14.Enabled = True And _chkBCType_15.Enabled = True And _chkBCType_16.Enabled = True And _chkBCType_17.Enabled = True And _chkBCType_18.Enabled = True) Then
            optType1D.Checked = True
        End If


        'If (optType1D.Checked = True) Then
        '    cmdAll_Click(sender, e)
        'End If
    End Sub

    Private Sub cmdAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAll.Click
        _chkBCType_0.Checked = True
        _chkBCType_1.Checked = True
        _chkBCType_2.Checked = True
        _chkBCType_3.Checked = True
        _chkBCType_4.Checked = True
        _chkBCType_5.Checked = True
        _chkBCType_6.Checked = True
        _chkBCType_7.Checked = True
        _chkBCType_8.Checked = True
        _chkBCType_9.Checked = True
        _chkBCType_10.Checked = True
        _chkBCType_11.Checked = True
        _chkBCType_12.Checked = True
        _chkBCType_13.Checked = True
        _chkBCType_14.Checked = True
        _chkBCType_15.Checked = True
        _chkBCType_16.Checked = True
        _chkBCType_17.Checked = True
        _chkBCType_18.Checked = True
        Code39ExtCheckBox.Checked = True
        Code93ExtCheckBox.Checked = True
    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        _chkBCType_0.Checked = False
        _chkBCType_1.Checked = False
        _chkBCType_2.Checked = False
        _chkBCType_3.Checked = False
        _chkBCType_4.Checked = False
        _chkBCType_5.Checked = False
        _chkBCType_6.Checked = False
        _chkBCType_7.Checked = False
        _chkBCType_8.Checked = False
        _chkBCType_9.Checked = False
        _chkBCType_10.Checked = False
        _chkBCType_11.Checked = False
        _chkBCType_12.Checked = False
        _chkBCType_13.Checked = False
        _chkBCType_14.Checked = False
        _chkBCType_15.Checked = False
        _chkBCType_16.Checked = False
        _chkBCType_17.Checked = False
        _chkBCType_18.Checked = False
        Code39ExtCheckBox.Checked = False
        Code93ExtCheckBox.Checked = False
    End Sub

    Private Function GetBarcodes() As System.Array
        Dim currentCount As Integer = 0

        If (optType1D.Checked = True) Then

            If (_chkBCType_0.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_1.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_2.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_3.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_4.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_5.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_6.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_7.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_8.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_9.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_10.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_11.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_12.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_13.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_14.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_15.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_16.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_17.Checked) Then
                currentCount += 1
            End If
            If (_chkBCType_18.Checked) Then
                currentCount += 1
            End If
            If (Code39ExtCheckBox.Checked) Then
                currentCount += 1
            End If
            If (Code93ExtCheckBox.Checked) Then
                currentCount += 1
            End If
        End If
        If (optTypePatchCode.Checked) Then
            currentCount += 1
        End If
        If (optTypeDataMatrix.Checked) Then
            currentCount += 1
        End If
        If (optTypePFD417.Checked) Then
            currentCount += 1
        End If
        If (optTypePostNet.Checked) Then
            currentCount += 1
        End If
        If (QRRadioButton.Checked = True) Then
            currentCount += 1
        End If
        If (RoyalRadioButton.Checked = True) Then
            currentCount += 1
        End If
        If (AusRadioButton.Checked = True) Then
            currentCount += 1
        End If
        If (OneCodeRadioButton.Checked = True) Then
            currentCount += 1
        End If

        Dim currentBarcodeTypes As System.Array = New BarcodeType(currentCount) {}
        If (currentCount > 0) Then
            currentCount = 0
            If (optType1D.Checked = True) Then

                If (_chkBCType_0.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Industry2of5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_1.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Interleaved2of5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_2.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.IATA2of5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_3.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataLogic2of5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_4.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Invert2of5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_5.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.BCDMatrixBarcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_6.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Matrix2of5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_7.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code32Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_8.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_9.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.CodabarBarcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_10.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_11.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_12.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN13Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_13.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN8Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_14.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCABarcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_15.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCEBarcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_16.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Add5Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_17.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Add2Barcode, currentCount)
                    currentCount += 1
                End If
                If (_chkBCType_18.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN128Barcode, currentCount)
                    currentCount += 1
                End If
                If (Code93ExtCheckBox.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93ExtendedBarcode, currentCount)
                    currentCount += 1
                End If
                If (Code39ExtCheckBox.Checked) Then
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode, currentCount)
                    currentCount += 1
                End If
            End If
            If (optTypePFD417.Checked) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, currentCount)
                currentCount += 1
            End If
            If (optTypePatchCode.Checked) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode, currentCount)
                currentCount += 1
            End If
            If (optTypeDataMatrix.Checked) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, currentCount)
                currentCount += 1
            End If
            If (optTypePostNet.Checked) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PostNetBarcode, currentCount)
                currentCount += 1
            End If
            If (QRRadioButton.Checked = True) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.QRCodeBarcode, currentCount)
            End If
            If (AusRadioButton.Checked = True) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.AustralianPost4StateBarcode, currentCount)
            End If
            If (RoyalRadioButton.Checked = True) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.RoyalPost4StateBarcode, currentCount)
            End If
            If (OneCodeRadioButton.Checked = True) Then
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.IntelligentMailBarcode, currentCount)
            End If

        End If
        Return currentBarcodeTypes
        'end barcode types
    End Function

    Private Sub buttonOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonOK.Click

        Me._nBarcodeTypes = GetBarcodes()

        If (_optOrient_0.Checked) Then
            _barcodeOrientation = OrientationInfo.Vertical
        End If

        If (_optOrient_1.Checked) Then
            _barcodeOrientation = OrientationInfo.Horizontal
        End If

        If (_optOrient_2.Checked) Then
            _barcodeOrientation = OrientationInfo.HorizontalVertical
        End If

        If (DiagRadioButton.Checked) Then
            _barcodeOrientation = OrientationInfo.HorizontalVerticalDiagonal
        End If

        Select Case (cboColor.SelectedIndex)
            Case 0
                _barcodeInkColor = InkColorInfo.Unknown
                Exit Select
            Case 1
                _barcodeInkColor = InkColorInfo.White
                Exit Select
            Case 2
                _barcodeInkColor = InkColorInfo.Black
                Exit Select
        End Select

        _nMaxBarcodes = Int32.Parse(txtMax.Text)
        Close()
    End Sub

    Private Sub buttonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCancel.Click
        cancel = True
        Me.Close()
    End Sub

    Property BarcodeTypes() As System.Array
        Get
            Return _nBarcodeTypes
        End Get

        Set(ByVal Value As System.Array)
            _nBarcodeTypes = Value
        End Set
    End Property

    Property BarcodeInkColor() As InkColorInfo
        Get
            Return _barcodeInkColor
        End Get
        Set(ByVal Value As InkColorInfo)
            _barcodeInkColor = Value
        End Set
    End Property

    Property MaxBarcodes() As System.Int32
        Get
            Return _nMaxBarcodes
        End Get
        Set(ByVal Value As System.Int32)
            _nMaxBarcodes = Value
        End Set
    End Property

    Property BarcodeOrientation() As OrientationInfo
        Get
            Return _barcodeOrientation
        End Get
        Set(ByVal Value As OrientationInfo)
            _barcodeOrientation = Value
        End Set
    End Property

    Private Sub optType1D_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optType1D.CheckedChanged
        Dim onOff As Boolean

        onOff = optType1D.Checked()
        _chkBCType_0.Enabled = onOff
        _chkBCType_1.Enabled = onOff
        _chkBCType_2.Enabled = onOff
        _chkBCType_3.Enabled = onOff
        _chkBCType_4.Enabled = onOff
        _chkBCType_5.Enabled = onOff
        _chkBCType_6.Enabled = onOff
        _chkBCType_7.Enabled = onOff
        _chkBCType_8.Enabled = onOff
        _chkBCType_9.Enabled = onOff
        _chkBCType_10.Enabled = onOff
        _chkBCType_11.Enabled = onOff
        _chkBCType_12.Enabled = onOff
        _chkBCType_13.Enabled = onOff
        _chkBCType_14.Enabled = onOff
        _chkBCType_15.Enabled = onOff
        _chkBCType_16.Enabled = onOff
        _chkBCType_17.Enabled = onOff
        _chkBCType_18.Enabled = onOff
        Code39ExtCheckBox.Enabled = onOff
        Code93ExtCheckBox.Enabled = onOff
    End Sub

    Private Sub QRRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QRRadioButton.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub

    Private Sub optTypePatchCode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTypePatchCode.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub

    Private Sub optTypePFD417_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTypePFD417.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub

    Private Sub OneCodeRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OneCodeRadioButton.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub

    Private Sub optTypePostNet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTypePostNet.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub

    Private Sub AusRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AusRadioButton.CheckedChanged
        optType1D_CheckedChanged(sender, e)

        If (AusRadioButton.Checked = True) Then
            AusGroupBox.Enabled = True
        Else
            AusGroupBox.Enabled = False
        End If

    End Sub

    Private Sub RoyalRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RoyalRadioButton.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub

    Private Sub optTypeDataMatrix_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTypeDataMatrix.CheckedChanged
        optType1D_CheckedChanged(sender, e)
    End Sub
End Class
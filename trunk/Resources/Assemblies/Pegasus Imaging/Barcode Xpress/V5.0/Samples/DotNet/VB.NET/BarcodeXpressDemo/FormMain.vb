'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Option Explicit On
Option Strict On

Imports System.Runtime.InteropServices
Imports PegasusImaging.WinForms.BarcodeXpress5
Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.ScanFix5

Public Class FormMain
    Inherits System.Windows.Forms.Form

    ' Windows API functions
    <DllImport("kernel32.dll")> _
    Public Shared Function GetSystemDirectory(ByVal sb As System.Text.StringBuilder, ByVal nSize As Int32) As Int32
        ' you have to declare a body even though nothing goes here
    End Function

    <DllImport("kernel32.dll")> _
    Public Shared Function GetWindowsDirectory(ByVal sb As System.Text.StringBuilder, ByVal nSize As Int32) As Int32
        ' you have to declare a body even though nothing goes here
    End Function

    Declare Function QueryPerformanceCounter Lib "kernel32" (ByRef lpPerformanceCount As Long) As Boolean
    Declare Function QueryPerformanceFrequency Lib "kernel32" (ByRef lpFrequency As Long) As Boolean

    Friend WithEvents BarcodeXpress1 As PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents BarcodeXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ScanFixMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents PreMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents PicImagXpress As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents PicImagXpressTemp As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents WriteMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter

    Public Shared sfCancelled As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()


        '***Must call the UnlockRuntime method to unlock the control
	'The unlock codes shown below are for formatting purposes only and will not work!
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        'PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234)


        'This call is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (PicImagXpress.Image Is Nothing) Then
                PicImagXpress.Image.Dispose()
                PicImagXpress.Image = Nothing
            End If
            If Not (PicImagXpressTemp.Image Is Nothing) Then
                PicImagXpressTemp.Image.Dispose()
                PicImagXpressTemp.Image = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (BarcodeXpress1 Is Nothing) Then
                BarcodeXpress1.Dispose()
                BarcodeXpress1 = Nothing
            End If
            If Not (ScanFix1 Is Nothing) Then
                ScanFix1.Dispose()
                ScanFix1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents groupBoxMain As System.Windows.Forms.GroupBox
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileClose As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileBar6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileExit As System.Windows.Forms.MenuItem
    Friend WithEvents groupBoxResults As System.Windows.Forms.GroupBox
    Friend WithEvents richTextBox As System.Windows.Forms.RichTextBox
    Friend WithEvents mnuView As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAnalyze As System.Windows.Forms.MenuItem
    Friend WithEvents mnuViewOptions As System.Windows.Forms.MenuItem
    Friend WithEvents mainMenu As System.Windows.Forms.MainMenu
    Friend WithEvents mnuPreProcess As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEdition As System.Windows.Forms.MenuItem
    Friend WithEvents openFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents statusBarMain As System.Windows.Forms.StatusBar
    Friend WithEvents statusBarPanelStat As System.Windows.Forms.StatusBarPanel
    Friend WithEvents statusBarPanelTime As System.Windows.Forms.StatusBarPanel
    Friend results() As PegasusImaging.WinForms.BarcodeXpress5.Result
    Friend WithEvents ScanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Friend WithEvents mnuBarcodeWriting As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMain))
        Me.groupBoxMain = New System.Windows.Forms.GroupBox
        Me.PicImagXpress = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.PicImagXpressTemp = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuFileOpen = New System.Windows.Forms.MenuItem
        Me.mnuFileClose = New System.Windows.Forms.MenuItem
        Me.mnuFileBar6 = New System.Windows.Forms.MenuItem
        Me.mnuFileExit = New System.Windows.Forms.MenuItem
        Me.groupBoxResults = New System.Windows.Forms.GroupBox
        Me.richTextBox = New System.Windows.Forms.RichTextBox
        Me.mnuView = New System.Windows.Forms.MenuItem
        Me.mnuAnalyze = New System.Windows.Forms.MenuItem
        Me.mnuViewOptions = New System.Windows.Forms.MenuItem
        Me.mainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuPreProcess = New System.Windows.Forms.MenuItem
        Me.PreMenuItem = New System.Windows.Forms.MenuItem
        Me.mnuBarcodeWriting = New System.Windows.Forms.MenuItem
        Me.WriteMenuItem = New System.Windows.Forms.MenuItem
        Me.mnuEdition = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.BarcodeXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.ScanFixMenuItem = New System.Windows.Forms.MenuItem
        Me.openFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.statusBarMain = New System.Windows.Forms.StatusBar
        Me.statusBarPanelStat = New System.Windows.Forms.StatusBarPanel
        Me.statusBarPanelTime = New System.Windows.Forms.StatusBarPanel
        Me.ScanFix1 = New PegasusImaging.WinForms.ScanFix5.ScanFix(Me.components)
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Me.BarcodeXpress1 = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.groupBoxMain.SuspendLayout()
        Me.PicImagXpress.SuspendLayout()
        Me.groupBoxResults.SuspendLayout()
        CType(Me.statusBarPanelStat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.statusBarPanelTime, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'groupBoxMain
        '
        Me.groupBoxMain.Controls.Add(Me.PicImagXpress)
        Me.groupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupBoxMain.Location = New System.Drawing.Point(163, 0)
        Me.groupBoxMain.Name = "groupBoxMain"
        Me.groupBoxMain.Size = New System.Drawing.Size(565, 515)
        Me.groupBoxMain.TabIndex = 5
        Me.groupBoxMain.TabStop = False
        Me.groupBoxMain.Text = "groupBox1"
        '
        'PicImagXpress
        '
        Me.PicImagXpress.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.PicImagXpress.AutoScroll = True
        Me.PicImagXpress.Controls.Add(Me.PicImagXpressTemp)
        Me.PicImagXpress.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PicImagXpress.Location = New System.Drawing.Point(3, 16)
        Me.PicImagXpress.Name = "PicImagXpress"
        Me.PicImagXpress.Size = New System.Drawing.Size(559, 496)
        Me.PicImagXpress.TabIndex = 6
        '
        'PicImagXpressTemp
        '
        Me.PicImagXpressTemp.Location = New System.Drawing.Point(101, 55)
        Me.PicImagXpressTemp.Name = "PicImagXpressTemp"
        Me.PicImagXpressTemp.Size = New System.Drawing.Size(80, 46)
        Me.PicImagXpressTemp.TabIndex = 5
        Me.PicImagXpressTemp.Visible = False
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFileOpen, Me.mnuFileClose, Me.mnuFileBar6, Me.mnuFileExit})
        Me.mnuFile.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuFile.Text = "&File"
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Index = 0
        Me.mnuFileOpen.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuFileOpen.Shortcut = System.Windows.Forms.Shortcut.CtrlO
        Me.mnuFileOpen.Text = "&Open"
        '
        'mnuFileClose
        '
        Me.mnuFileClose.Index = 1
        Me.mnuFileClose.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuFileClose.Text = "&Close"
        '
        'mnuFileBar6
        '
        Me.mnuFileBar6.Index = 2
        Me.mnuFileBar6.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuFileBar6.Text = "-"
        '
        'mnuFileExit
        '
        Me.mnuFileExit.Index = 3
        Me.mnuFileExit.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuFileExit.Text = "E&xit"
        '
        'groupBoxResults
        '
        Me.groupBoxResults.Controls.Add(Me.richTextBox)
        Me.groupBoxResults.Dock = System.Windows.Forms.DockStyle.Left
        Me.groupBoxResults.Location = New System.Drawing.Point(0, 0)
        Me.groupBoxResults.Name = "groupBoxResults"
        Me.groupBoxResults.Size = New System.Drawing.Size(160, 515)
        Me.groupBoxResults.TabIndex = 0
        Me.groupBoxResults.TabStop = False
        Me.groupBoxResults.Text = "groupBoxResults"
        '
        'richTextBox
        '
        Me.richTextBox.BackColor = System.Drawing.SystemColors.Control
        Me.richTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.richTextBox.Location = New System.Drawing.Point(3, 16)
        Me.richTextBox.Name = "richTextBox"
        Me.richTextBox.ReadOnly = True
        Me.richTextBox.Size = New System.Drawing.Size(154, 496)
        Me.richTextBox.TabIndex = 0
        Me.richTextBox.Text = ""
        '
        'mnuView
        '
        Me.mnuView.Index = 1
        Me.mnuView.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAnalyze, Me.mnuViewOptions})
        Me.mnuView.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuView.Text = "&Recognition"
        '
        'mnuAnalyze
        '
        Me.mnuAnalyze.Enabled = False
        Me.mnuAnalyze.Index = 0
        Me.mnuAnalyze.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuAnalyze.Text = "&Analyze Barcode"
        '
        'mnuViewOptions
        '
        Me.mnuViewOptions.Index = 1
        Me.mnuViewOptions.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuViewOptions.Text = "&Options..."
        '
        'mainMenu
        '
        Me.mainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuView, Me.mnuPreProcess, Me.mnuBarcodeWriting, Me.mnuEdition, Me.MenuItem2})
        '
        'mnuPreProcess
        '
        Me.mnuPreProcess.Index = 2
        Me.mnuPreProcess.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.PreMenuItem})
        Me.mnuPreProcess.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuPreProcess.Text = "&Enhance Image"
        '
        'PreMenuItem
        '
        Me.PreMenuItem.Index = 0
        Me.PreMenuItem.Text = "&PreProcessing"
        '
        'mnuBarcodeWriting
        '
        Me.mnuBarcodeWriting.Index = 3
        Me.mnuBarcodeWriting.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.WriteMenuItem})
        Me.mnuBarcodeWriting.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuBarcodeWriting.Text = "Barcode &Writing"
        '
        'WriteMenuItem
        '
        Me.WriteMenuItem.Index = 0
        Me.WriteMenuItem.Text = "Write &Barcodes"
        '
        'mnuEdition
        '
        Me.mnuEdition.Index = 4
        Me.mnuEdition.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        Me.mnuEdition.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuEdition.Text = "&Edition"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Set BarcodeXpress Edition"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 5
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.BarcodeXpressMenuItem, Me.ImagXpressMenuItem, Me.ScanFixMenuItem})
        Me.MenuItem2.Text = "&About"
        '
        'BarcodeXpressMenuItem
        '
        Me.BarcodeXpressMenuItem.Index = 0
        Me.BarcodeXpressMenuItem.Text = "&BarcodeXpress"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 1
        Me.ImagXpressMenuItem.Text = "&ImagXpress"
        '
        'ScanFixMenuItem
        '
        Me.ScanFixMenuItem.Index = 2
        Me.ScanFixMenuItem.Text = "&ScanFix"
        '
        'statusBarMain
        '
        Me.statusBarMain.Location = New System.Drawing.Point(0, 515)
        Me.statusBarMain.Name = "statusBarMain"
        Me.statusBarMain.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusBarPanelStat, Me.statusBarPanelTime})
        Me.statusBarMain.ShowPanels = True
        Me.statusBarMain.Size = New System.Drawing.Size(728, 22)
        Me.statusBarMain.SizingGrip = False
        Me.statusBarMain.TabIndex = 9
        Me.statusBarMain.Text = "hello"
        '
        'statusBarPanelStat
        '
        Me.statusBarPanelStat.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusBarPanelStat.Name = "statusBarPanelStat"
        Me.statusBarPanelStat.Text = "statusBarPanel"
        Me.statusBarPanelStat.Width = 718
        '
        'statusBarPanelTime
        '
        Me.statusBarPanelTime.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.statusBarPanelTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.statusBarPanelTime.Name = "statusBarPanelTime"
        Me.statusBarPanelTime.Width = 10
        '
        'ScanFix1
        '
        Me.ScanFix1.Debug = False
        Me.ScanFix1.DebugLogFile = "C:\ScanFix5.log"
        Me.ScanFix1.ErrorLevel = PegasusImaging.WinForms.ScanFix5.ErrorLevel.Production
        Me.ScanFix1.ResolutionUnits = System.Drawing.GraphicsUnit.Inch
        '
        'BarcodeXpress1
        '
        Me.BarcodeXpress1.ErrorLevel = PegasusImaging.WinForms.BarcodeXpress5.ErrorLevelInfo.Production
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(160, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 515)
        Me.Splitter1.TabIndex = 10
        Me.Splitter1.TabStop = False
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(728, 537)
        Me.Controls.Add(Me.groupBoxMain)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.groupBoxResults)
        Me.Controls.Add(Me.statusBarMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.mainMenu
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pegasus Imaging Corporation - BarcodeXpress Barcode Demo"
        Me.groupBoxMain.ResumeLayout(False)
        Me.PicImagXpress.ResumeLayout(False)
        Me.groupBoxResults.ResumeLayout(False)
        CType(Me.statusBarPanelStat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.statusBarPanelTime, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim proc As PegasusImaging.WinForms.ImagXpress8.Processor
    Dim counter As Integer
    Dim cancelNotPressed As Boolean

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '***Must call the UnlockRuntime method to Unlock the control
        'The unlock codes shown below are for formatting purposes only and will not work!
        'BarcodeXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)
        'ScanFix1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        'ImagXpress1.License.UnlockRuntime(1234,1234,1234,1234)


        proc = New PegasusImaging.WinForms.ImagXpress8.Processor()

        ScanFix1.License.LicenseEdition = PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.BitonalEdition

        BarcodeXpress1.Licensing.LicenseEdition = LicenseChoice.ProfessionalPlus2DWriteEdition

        LoadNewDoc()

        Dim strProdName As String = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductName
        strProdName += " " + System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion

        If (BarcodeXpress1.Licensing.LicenseEdition = LicenseChoice.ProfessionalEdition) Then
            Me.Text = strProdName + " - Professional Edition"
        ElseIf (BarcodeXpress1.Licensing.LicenseEdition = LicenseChoice.StandardEdition) Then
            Me.Text = strProdName + " - Standard Edition"
        ElseIf (BarcodeXpress1.Licensing.LicenseEdition = LicenseChoice.ProfessionalPlus2DWriteEdition) Then
            Me.Text = strProdName + " - Professional Plus 2D Write Edition"
        End If

        Me.Show()

    End Sub

    Public Sub LoadNewDoc()
        statusBarPanelStat.Text = "Use File | Open to open an image for analysis"
        groupBoxResults.Text = "Barcode Analysis"
        groupBoxMain.Text = "Use File | Open to open an image for analysis"
    End Sub

    Private Sub mnuFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileExit.Click
        System.Environment.Exit(0)
    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileOpen.Click
        openFileDialog.Title = "Open a 1-Bit Black and White Image File"
        openFileDialog.InitialDirectory = Application.ExecutablePath & "\..\..\..\..\..\..\..\..\Common\Images"
        openFileDialog.ShowDialog(Me)

        Dim strFileName As String = openFileDialog.FileName
        statusBarPanelStat.Text = ""

        If (System.IO.File.Exists(strFileName)) Then

            Try
                PicImagXpressTemp.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strFileName)

                PicImagXpress.Image = PicImagXpressTemp.Image

                If (PicImagXpress.Image.ImageXData.Format = ImageXFormat.Pdf Or PicImagXpress.Image.ImageXData.Format = ImageXFormat.Tiff) Then

                    If (PicImagXpress.Image.PageCount > 1) Then

                        Dim multi As MultiPageForm = New MultiPageForm()
                        multi.CountBox.Text = PicImagXpress.Image.PageCount.ToString()
                        multi.ShowDialog()

                        PicImagXpress.Image.Page = Int32.Parse(multi.PageBox.Text)
                    End If
                End If

                If (PicImagXpress.Image.ImageXData.BitsPerPixel <> 1) Then


                    Dim bin As BinarizeForm = New BinarizeForm(PicImagXpress.Image.Copy(), ScanFix1)

                    bin.ShowDialog()

                    PicImagXpressTemp.Image = bin.image

                    PicImagXpress.Image = PicImagXpressTemp.Image.Copy()

                End If

                If Not (PicImagXpress.Image.Resolution.Dimensions.Width = PicImagXpress.Image.Resolution.Dimensions.Height) Then

                    proc.Image = PicImagXpress.Image.Copy()
                    If (PicImagXpress.Image.Resolution.Dimensions.Width > PicImagXpress.Image.Resolution.Dimensions.Height) Then

                        proc.Resize(New System.Drawing.Size(PicImagXpress.Image.ImageXData.Width, CType(PicImagXpress.Image.ImageXData.Height * PicImagXpress.Image.Resolution.Dimensions.Width / PicImagXpress.Image.Resolution.Dimensions.Height, Int32)), PegasusImaging.WinForms.ImagXpress8.ResizeType.Quality)
                        PicImagXpress.Image = proc.Image
                        PicImagXpress.Image.Resolution.Dimensions = New System.Drawing.Size(CType(PicImagXpress.Image.Resolution.Dimensions.Width, Int32), CType(PicImagXpress.Image.Resolution.Dimensions.Width, Int32))

                    Else

                        proc.Resize(New System.Drawing.Size(CType(PicImagXpress.Image.ImageXData.Width * PicImagXpress.Image.Resolution.Dimensions.Height / PicImagXpress.Image.Resolution.Dimensions.Width, Int32), PicImagXpress.Image.ImageXData.Height), PegasusImaging.WinForms.ImagXpress8.ResizeType.Quality)
                        PicImagXpress.Image = proc.Image
                        PicImagXpress.Image.Resolution.Dimensions = New System.Drawing.Size(CType(PicImagXpress.Image.Resolution.Dimensions.Height, Int32), CType(PicImagXpress.Image.Resolution.Dimensions.Height, Int32))
                    End If

                    MessageBox.Show("The image does not have symmetric resolution, the image will be resized with respect to it's new symmetric resolution (the larger of it's current vertical or horizontal resolution) otherwise barcode detection will be difficult.", "Asymmetric resolution", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                MessageBox.Show(ex.Message)
            End Try

            PicImagXpressTemp.Image = PicImagXpress.Image.Copy()

            PicImagXpress.Visible = True
            mnuAnalyze.Enabled = True
            statusBarPanelStat.Text = "Click the Analyze button to detect barcodes in the image."
        End If

    End Sub

    Private Sub mnuFileClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFileClose.Click
        If Not (PicImagXpress.Image Is Nothing) Then
            PicImagXpress.Image = Nothing
        Else
            Dim img As PegasusImaging.WinForms.ImagXpress8.ImageX
            img = New PegasusImaging.WinForms.ImagXpress8.ImageX(1, 1, 8, System.Drawing.Color.FromArgb(192, 192, 192))
            PicImagXpress.Image = img
        End If

        LoadNewDoc()

        richTextBox.Text = ""
        mnuAnalyze.Enabled = False

        statusBarPanelStat.Text = ""
    End Sub

    Private Sub checkType(ByVal Form As FormOptions)


        Form._chkBCType_0.Checked = BarcodeSearch(BarcodeType.Industry2of5Barcode)
        Form._chkBCType_1.Checked = BarcodeSearch(BarcodeType.Interleaved2of5Barcode)
        Form._chkBCType_2.Checked = BarcodeSearch(BarcodeType.IATA2of5Barcode)
        Form._chkBCType_3.Checked = BarcodeSearch(BarcodeType.DataLogic2of5Barcode)
        Form._chkBCType_4.Checked = BarcodeSearch(BarcodeType.Invert2of5Barcode)
        Form._chkBCType_5.Checked = BarcodeSearch(BarcodeType.BCDMatrixBarcode)
        Form._chkBCType_6.Checked = BarcodeSearch(BarcodeType.Matrix2of5Barcode)
        Form._chkBCType_7.Checked = BarcodeSearch(BarcodeType.Code32Barcode)
        Form._chkBCType_8.Checked = BarcodeSearch(BarcodeType.Code39Barcode)
        Form._chkBCType_9.Checked = BarcodeSearch(BarcodeType.CodabarBarcode)
        Form._chkBCType_10.Checked = BarcodeSearch(BarcodeType.Code93Barcode)
        Form._chkBCType_11.Checked = BarcodeSearch(BarcodeType.Code128Barcode)
        Form._chkBCType_12.Checked = BarcodeSearch(BarcodeType.EAN13Barcode)
        Form._chkBCType_13.Checked = BarcodeSearch(BarcodeType.EAN8Barcode)
        Form._chkBCType_14.Checked = BarcodeSearch(BarcodeType.UPCABarcode)
        Form._chkBCType_15.Checked = BarcodeSearch(BarcodeType.UPCEBarcode)
        Form._chkBCType_16.Checked = BarcodeSearch(BarcodeType.Add5Barcode)
        Form._chkBCType_17.Checked = BarcodeSearch(BarcodeType.Add2Barcode)
        Form._chkBCType_18.Checked = BarcodeSearch(BarcodeType.EAN128Barcode)
        Form.Code39ExtCheckBox.Checked = BarcodeSearch(BarcodeType.Code39ExtendedBarcode)
        Form.Code93ExtCheckBox.Checked = BarcodeSearch(BarcodeType.Code93ExtendedBarcode)

        Form.optTypePFD417.Checked = BarcodeSearch(BarcodeType.PDF417Barcode)
        Form.optTypePatchCode.Checked = BarcodeSearch(BarcodeType.PatchCodeBarcode)
        Form.optTypeDataMatrix.Checked = BarcodeSearch(BarcodeType.DataMatrixBarcode)
        Form.optTypePostNet.Checked = BarcodeSearch(BarcodeType.PostNetBarcode)
        Form.QRRadioButton.Checked = BarcodeSearch(BarcodeType.QRCodeBarcode)
        Form.AusRadioButton.Checked = BarcodeSearch(BarcodeType.AustralianPost4StateBarcode)
        Form.RoyalRadioButton.Checked = BarcodeSearch(BarcodeType.RoyalPost4StateBarcode)
        Form.OneCodeRadioButton.Checked = BarcodeSearch(BarcodeType.IntelligentMailBarcode)


    End Sub

    Function BarcodeSearch(ByVal barcodeTypeChosen As PegasusImaging.WinForms.BarcodeXpress5.BarcodeType) As Boolean
        If (BarcodeXpress1.reader.BarcodeTypes.Length > 0) Then
            Dim i As Int32
            For i = 0 To BarcodeXpress1.reader.BarcodeTypes.Length - 1

                Dim types As System.Array = BarcodeXpress1.reader.BarcodeTypes
                If (CType(types.GetValue(i), PegasusImaging.WinForms.BarcodeXpress5.BarcodeType) = barcodeTypeChosen) Then
                    Return True
                End If
            Next i

            Return False
        End If

        Return False
    End Function

    Private Sub Analyze()
        Try
            richTextBox.Text = ""
            statusBarPanelStat.Text = ""

            statusBarPanelStat.Text = "Detecting Barcodes..."
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim startTime As System.DateTime = System.DateTime.Now

            ' Do the magic here.

            ' Use the copy before the writing was placed on it.
            PicImagXpress.Image = PicImagXpressTemp.Image.Copy()
            ' Make a copy of the Dib before we write the value on it.
            PicImagXpressTemp.Image = PicImagXpress.Image.Copy()

            Dim currentArea As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
            BarcodeXpress1.reader.Area = currentArea
            BarcodeXpress1.reader.ReturnPossibleBarcodes = True

            If PicImagXpress.Image.ImageXData.BitsPerPixel <> 0 Then
                proc = New PegasusImaging.WinForms.ImagXpress8.Processor()
                proc.Image = PicImagXpress.Image
                proc.ColorDepth(1, 0, 0)
                PicImagXpress.Image = proc.Image
            End If

            Dim DIB As Int32

            DIB = PicImagXpress.Image.ToHdib(False).ToInt32()

            Dim start As Long = 0
            Dim stoptime As Long = 0
            Dim freq As Long = 0

            QueryPerformanceCounter(start)

            QueryPerformanceFrequency(freq)

            results = BarcodeXpress1.reader.Analyze(DIB)

            QueryPerformanceCounter(stoptime)

            Dim timeElapsed As Double = (CType((stoptime - start), Double) / CType(freq, Double))

            Dim endTime As System.DateTime = System.DateTime.Now

            Me.Cursor = System.Windows.Forms.Cursors.Default

            If (results.Length > 0) Then
                statusBarPanelStat.Text = "Barcode Detection Successful"
                Dim strResult As String = Chr(13) + "Analyze Time:" + Chr(13) + timeElapsed.ToString() + " sec." + Chr(13)
                strResult += results.Length.ToString() + " barcodes(s) found." + Chr(13) + Chr(13)

                proc = New PegasusImaging.WinForms.ImagXpress8.Processor()
                proc.Image = PicImagXpress.Image.Copy()
                proc.ColorDepth(24, 0, 0)

                PicImagXpress.Image = proc.Image

                Dim img As PegasusImaging.WinForms.ImagXpress8.ImageX = PicImagXpress.Image.Copy()

                Dim i As Integer
                Dim g As System.Drawing.Graphics = img.GetGraphics() 'PicImagXpress.Image.GetGraphics()
                For i = 0 To results.Length - 1 Step 1
                    Dim curResult As PegasusImaging.WinForms.BarcodeXpress5.Result = CType(results.GetValue(i), PegasusImaging.WinForms.BarcodeXpress5.Result)

                    If (results(i).BarcodeType = BarcodeType.UnknownBarcode) Then
                        strResult += "Symbol #" + i.ToString() + Chr(13) + "Type = " + "Unknown Barcode" + Chr(13)
                    Else
                        strResult += "Symbol #" + i.ToString() + Chr(13) + "Type = " + curResult.BarcodeName + Chr(13)
                    End If

                    strResult += "X - " + curResult.Area.X.ToString() + " pixels" + Chr(13) + "Y - " + curResult.Area.Y.ToString() + " pixels" + Chr(13)
                    strResult += "W - " + curResult.Area.Width.ToString() + " pixels" + Chr(13) + "H - " + curResult.Area.Height.ToString() + " pixels" + Chr(13)
                    strResult += "Value:" + Chr(13) + curResult.BarcodeValue + Chr(13) + Chr(13)


                    Dim pen As System.Drawing.Pen = New System.Drawing.Pen(System.Drawing.Color.Black)

                    Dim point1 As Point = New Point(curResult.Area.X, curResult.Area.Y)
                    Dim point2 As Point = New Point(curResult.Area.X + curResult.Area.Width, curResult.Area.Y)
                    Dim point3 As Point = New Point(curResult.Area.X + curResult.Area.Width, curResult.Area.Y + curResult.Area.Height)
                    Dim point4 As Point = New Point(curResult.Area.X, curResult.Area.Y + curResult.Area.Height)

                    g.DrawLines(pen, New Point() {point1, point2, point3, point4, point1})

                    Dim strMsg As String = "Symbol #" + i.ToString() + " - " + curResult.BarcodeValue
                    Dim graphics As System.Drawing.Graphics = System.Drawing.Graphics.FromHwnd(Me.Handle)
                    Dim size As SizeF = graphics.MeasureString(strMsg, PicImagXpress.Font)

                    Dim font As System.Drawing.Font = New System.Drawing.Font("Arial", 7)

                    Dim brush As System.Drawing.Brush = System.Drawing.Brushes.White
                    g.FillRectangle(brush, curResult.Area.X + 2, point1.Y - font.Height, curResult.Area.Width, font.Height)


                    If (results(i).BarcodeValue = Nothing) Then
                        g.DrawString("Symbol #" + i.ToString() + " - ", font, System.Drawing.Brushes.Black, curResult.Area.X + 2, point1.Y - font.Height)
                    Else
                        g.DrawString("Symbol #" + i.ToString() + " - " + curResult.BarcodeValue.ToString(), font, System.Drawing.Brushes.Black, curResult.Area.X + 2, point1.Y - font.Height)
                    End If


                    Select Case (curResult.BarcodeType)
                        Case PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.QRCodeBarcode
                            If Not (results(i).BarcodeValue.Length = results(i).BarcodeData.Length) Then
                                BinaryData(curResult)
                            End If
                    End Select

                Next

                img.ReleaseGraphics()


                PicImagXpress.Image = img

                strResult += Chr(10)
                richTextBox.Text = strResult
            Else
                statusBarPanelStat.Text = "Barcode Detection Failed"
                richTextBox.Text = "Could not detect any barcodes. See Help for reasons why the barcode detection might have failed."
            End If

            System.Runtime.InteropServices.Marshal.FreeHGlobal(New System.IntPtr(DIB))

            Dim DIB2 As Int32 = PicImagXpressTemp.Image.ToHdib(False).ToInt32()
            System.Runtime.InteropServices.Marshal.FreeHGlobal(New System.IntPtr(DIB2))

        Catch ex As PegasusImaging.WinForms.BarcodeXpress5.InvalidLicenseException
            MessageBox.Show(ex.Message)
        Catch ex As PegasusImaging.WinForms.BarcodeXpress5.BarcodeException
            MessageBox.Show(ex.Message)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            MessageBox.Show(ex.Message)
        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuAnalyze_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAnalyze.Click
        If (counter = 0) Then

            mnuViewOptions_Click(sender, e)
            If (cancelNotPressed = True) Then

                Analyze()
            End If
            counter = 1

        Else

            Analyze()
        End If


    End Sub

    Private Sub BinaryData(ByVal result As PegasusImaging.WinForms.BarcodeXpress5.Result)
        Dim binForm As BinaryForm = New BinaryForm(result)
        binForm.ShowDialog()

        If Not (binForm Is Nothing) Then
            binForm.Dispose()
            binForm = Nothing
        End If

    End Sub

    Private Sub mnuViewOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewOptions.Click

        Try

            counter = 1

            ' Force the user to at least look at the options screen.
            Dim form As FormOptions = New FormOptions()

            ' set the requried properties before we show
            form.BarcodeInkColor = BarcodeXpress1.reader.InkColor
            form.BarcodeTypes = BarcodeXpress1.reader.BarcodeTypes
            form.MaxBarcodes = BarcodeXpress1.reader.MaximumBarcodes
            form.BarcodeOrientation = BarcodeXpress1.reader.Orientation
            form.AppendCheckBox.Checked = BarcodeXpress1.reader.AppendCheckSum
            form.ScanBox.Value = BarcodeXpress1.reader.ScanDistance

            checkType(form)

            form.ShowDialog(Me)

            If (form.cancel = False) Then

                cancelNotPressed = True

                ' we have returned, so reset the properties on the ScanXpress control
                BarcodeXpress1.reader.InkColor = form.BarcodeInkColor
                BarcodeXpress1.reader.BarcodeTypes = form.BarcodeTypes
                BarcodeXpress1.reader.MaximumBarcodes = CShort(form.MaxBarcodes)
                BarcodeXpress1.reader.Orientation = form.BarcodeOrientation
                BarcodeXpress1.reader.AppendCheckSum = form.AppendCheckBox.Checked
                BarcodeXpress1.reader.ScanDistance = CType(form.ScanBox.Value, Int32)

                If (form.CustomStatesRadioButton.Checked = True) Then

                    BarcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.BarStates

                ElseIf (form.CustomTableCRadioButton.Checked = True) Then

                    BarcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.TableC

                ElseIf (form.CustomTableNRadioButton.Checked = True) Then

                    BarcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.TableN

                Else

                    BarcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.NoCustomDecode
                End If
            End If

        Catch ex As PegasusImaging.WinForms.BarcodeXpress5.BarcodeException

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        Dim Form As FormEdition = New FormEdition()
        Form.BarcodeEdition = BarcodeXpress1.Licensing.LicenseEdition()
        Form.ShowDialog(Me)

        BarcodeXpress1.Licensing.LicenseEdition = Form.BarcodeEdition
        Select Case (Form.BarcodeEdition)
            Case LicenseChoice.ProfessionalEdition
                Me.Text = "Pegasus Software BarcodeXpress Barcode v5.0 Demo - Professional Edition"
                Exit Select

            Case LicenseChoice.StandardEdition
                Me.Text = "Pegasus Software BarcodeXpress Barcode v5.0 Demo - Standard Edition"
                Exit Select

            Case LicenseChoice.ProfessionalPlus2DWriteEdition
                Me.Text = "Pegasus Software BarcodeXpress Barcode v5.0 Demo - Professional Plus 2D Write Edition"
                Exit Select
        End Select
    End Sub

    Private Sub PreMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreMenuItem.Click
        Try

            If (PicImagXpress.Image Is Nothing) Then
                MessageBox.Show("Please load an image before setting the preprocess options.", "BarcodeXpress Barcode Demo", System.Windows.Forms.MessageBoxButtons.OK)
                Exit Sub
            End If

            ' Use the copy before the writing was placed on it.
            PicImagXpress.Image = PicImagXpressTemp.Image.Copy()

            Dim preprocess As ProcessScreen
            preprocess = New ProcessScreen(PicImagXpress, PicImagXpressTemp, ScanFix1)

            preprocess.ShowDialog()

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException

            MessageBox.Show(ex.Message)

        Catch ex As PegasusImaging.WinForms.ScanFix5.ScanFixException

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BarcodeXpressMenuItem.Click
        BarcodeXpress1.AboutBox()
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub ScanFixMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ScanFixMenuItem.Click
        ScanFix1.AboutBox()
    End Sub

    Private Sub WriteMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WriteMenuItem.Click
        Dim Form As FormBarcodeWriting = New FormBarcodeWriting(BarcodeXpress1)
        Form.ShowDialog(Me)
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BinaryForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BinaryForm))
        Me.ValueTextBox = New System.Windows.Forms.TextBox
        Me.HexTextBox = New System.Windows.Forms.TextBox
        Me.ValueLabel = New System.Windows.Forms.Label
        Me.HexLabel = New System.Windows.Forms.Label
        Me.OKButton = New System.Windows.Forms.Button
        Me.BarcodeFound = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'ValueTextBox
        '
        Me.ValueTextBox.Location = New System.Drawing.Point(14, 149)
        Me.ValueTextBox.Multiline = True
        Me.ValueTextBox.Name = "ValueTextBox"
        Me.ValueTextBox.ReadOnly = True
        Me.ValueTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.ValueTextBox.Size = New System.Drawing.Size(444, 228)
        Me.ValueTextBox.TabIndex = 0
        '
        'HexTextBox
        '
        Me.HexTextBox.Location = New System.Drawing.Point(14, 417)
        Me.HexTextBox.Multiline = True
        Me.HexTextBox.Name = "HexTextBox"
        Me.HexTextBox.ReadOnly = True
        Me.HexTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.HexTextBox.Size = New System.Drawing.Size(444, 228)
        Me.HexTextBox.TabIndex = 1
        '
        'ValueLabel
        '
        Me.ValueLabel.Location = New System.Drawing.Point(12, 128)
        Me.ValueLabel.Name = "ValueLabel"
        Me.ValueLabel.Size = New System.Drawing.Size(111, 18)
        Me.ValueLabel.TabIndex = 2
        Me.ValueLabel.Text = "ASCII Character:"
        '
        'HexLabel
        '
        Me.HexLabel.Location = New System.Drawing.Point(12, 401)
        Me.HexLabel.Name = "HexLabel"
        Me.HexLabel.Size = New System.Drawing.Size(65, 13)
        Me.HexLabel.TabIndex = 3
        Me.HexLabel.Text = "Hex Value:"
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(154, 659)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(149, 34)
        Me.OKButton.TabIndex = 4
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'BarcodeFound
        '
        Me.BarcodeFound.AutoSize = True
        Me.BarcodeFound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BarcodeFound.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarcodeFound.Location = New System.Drawing.Point(15, 15)
        Me.BarcodeFound.Name = "BarcodeFound"
        Me.BarcodeFound.Size = New System.Drawing.Size(2, 15)
        Me.BarcodeFound.TabIndex = 5
        '
        'BinaryForm
        '
        Me.AcceptButton = Me.OKButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(471, 702)
        Me.Controls.Add(Me.BarcodeFound)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.HexLabel)
        Me.Controls.Add(Me.ValueLabel)
        Me.Controls.Add(Me.HexTextBox)
        Me.Controls.Add(Me.ValueTextBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "BinaryForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BinaryForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ValueTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HexTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ValueLabel As System.Windows.Forms.Label
    Friend WithEvents HexLabel As System.Windows.Forms.Label
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents BarcodeFound As System.Windows.Forms.Label
End Class

'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Public Class BinarizeForm
    Public image As PegasusImaging.WinForms.ImagXpress8.ImageX
    Dim scanfix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Dim binOpts As PegasusImaging.WinForms.ScanFix5.BinarizeOptions
    Dim proc1 As PegasusImaging.WinForms.ImagXpress8.Processor

    Public Sub New(ByVal img As PegasusImaging.WinForms.ImagXpress8.ImageX, ByVal SF As PegasusImaging.WinForms.ScanFix5.ScanFix)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Image = img

        If (SF.License.LicenseEdition = PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.LiteEdition) Then

            BinarizeGroupBox.Enabled = False
            AutoRadioButton.Enabled = False
            BinarizeRadioButton.Enabled = False

        Else

            AutoRadioButton.Enabled = True
            BinarizeRadioButton.Enabled = True
        End If

        scanFix1 = SF

    End Sub

    Private Sub BinarizeForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BlurBox.SelectedIndex = 0
        BinGroupBox.Enabled = False
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        If (ColorDepthRadioButton.Checked = True) Then

            proc1 = New PegasusImaging.WinForms.ImagXpress8.Processor(Image.Copy())
            proc1.ColorDepth(1, 0, 0)
            Image = proc1.Image

        ElseIf (BinarizeRadioButton.Checked = True) Then

            binOpts = New PegasusImaging.WinForms.ScanFix5.BinarizeOptions()
            binOpts.LceFactor = ConBar.Value
            binOpts.LowThreshold = LowBar.Value
            binOpts.HighThreshold = HighBar.Value
            binOpts.PreBlurType = CType(BlurBox.SelectedIndex, PegasusImaging.WinForms.ScanFix5.BinarizeBlur)

            Dim DIB As Int32 = Image.ToHdib(False).ToInt32()

            scanFix1.FromHdib(New System.IntPtr(DIB))
            scanFix1.Binarize(binOpts)
            Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(False))
            System.Runtime.InteropServices.Marshal.FreeHGlobal(New System.IntPtr(DIB))

        Else

            Dim DIB As Int32 = Image.ToHdib(False).ToInt32()
            scanFix1.FromHdib(New System.IntPtr(DIB))
            scanFix1.AutoBinarize()
            Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(scanFix1.ToHdib(False))
            System.Runtime.InteropServices.Marshal.FreeHGlobal(New System.IntPtr(DIB))
        End If
        Close()
    End Sub

    Private Sub ColorDepthRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ColorDepthRadioButton.CheckedChanged
        BinGroupBox.Enabled = False
    End Sub

    Private Sub BinarizeRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BinarizeRadioButton.CheckedChanged
        BinGroupBox.Enabled = True
    End Sub

    Private Sub AutoRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AutoRadioButton.CheckedChanged
        BinGroupBox.Enabled = False
    End Sub

    Private Sub HighBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HighBar.Scroll
        If (HighBar.Value <= LowBar.Value) Then

            HighBar.Value = LowBar.Value + 1
            Dim valueNow As Int32 = LowBar.Value + 1
            HighValue.Text = valueNow.ToString()

        Else
            HighValue.Text = HighBar.Value.ToString()
        End If
    End Sub

    Private Sub LowBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LowBar.Scroll
        If (LowBar.Value >= HighBar.Value) Then

            LowBar.Value = HighBar.Value - 1
            Dim valueNow As Int32 = HighBar.Value - 1
            LowValue.Text = valueNow.ToString()

        Else

            LowValue.Text = LowBar.Value.ToString()
        End If
    End Sub

    Private Sub ConBar_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConBar.Scroll
        ConValue.Text = ConBar.Value.ToString()
    End Sub
End Class
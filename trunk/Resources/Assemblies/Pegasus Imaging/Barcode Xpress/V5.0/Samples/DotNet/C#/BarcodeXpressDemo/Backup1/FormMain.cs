/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.ScanFix5;
using PegasusImaging.WinForms.BarcodeXpress5;
using System.IO;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		// Windows API functions
		[DllImport ("kernel32")]
		static extern Int32 GetSystemDirectory (System.Text.StringBuilder sb, Int32 nSize);

		[DllImport ("kernel32")]
		static extern Int32 GetWindowsDirectory (System.Text.StringBuilder sb, Int32 nSize);

        [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);
        
        [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long lpFrequency);

		private System.Windows.Forms.StatusBar statusBarMain;
		private System.Windows.Forms.StatusBarPanel statusBarPanelStat;
		private System.Windows.Forms.StatusBarPanel statusBarPanelTime;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.GroupBox groupBoxResults;
        private System.Windows.Forms.RichTextBox richTextBox;
		private System.ComponentModel.IContainer components;
        private GroupBox groupBoxMain;
        private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;

        private PegasusImaging.WinForms.BarcodeXpress5.Result[] results;
        private ScanFix scanFix1;
        private ImageXView imageXViewTemp;
        private ImageXView imageXView;
        private PegasusImaging.WinForms.ImagXpress8.Processor proc;

        private int counter;
        private bool cancelNotPressed;
        private BarcodeXpress barcodeXpress1;
        private HelpProvider helpProvider1;

        private string openPath = "";
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem openToolStripMenuItem;
        private ToolStripMenuItem closeToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem recognitionToolStripMenuItem;
        private ToolStripMenuItem analyzeBarcodesToolStripMenuItem;
        private ToolStripMenuItem optionsToolStripMenuItem;
        private ToolStripMenuItem enhanceImageToolStripMenuItem;
        private ToolStripMenuItem preProcessingToolStripMenuItem;
        private ToolStripMenuItem barcodeWritingToolStripMenuItem;
        private ToolStripMenuItem writeBarcodesToolStripMenuItem;
        private ToolStripMenuItem eToolStripMenuItem;
        private ToolStripMenuItem setBarcodeXpressEditionToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem1;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private ToolStripMenuItem barcodeXpressToolStripMenuItem;
        private ToolStripMenuItem imagXpressToolStripMenuItem;
        private ToolStripMenuItem scanFixToolStripMenuItem;
        private ToolStripMenuItem viewToolStripMenuItem;
        private ToolStripMenuItem toolbarToolStripMenuItem;
        private ToolStripMenuItem statusBarToolStripMenuItem;
        private ToolStrip toolStrip1;
        private ToolStripButton OpenButton;
        private ToolStripButton CloseButton;
        private ToolStripButton PreButton;
        private ToolStripButton AnalyzeButton;
        private ToolStripButton OptionsButton;
        private ToolStripButton WriteButton;
        private ToolStripButton HelpFileButton;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripButton AboutButton;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripSeparator toolStripSeparator4;
        private Splitter splitter1;
        private string helpPath = "";

		public FormMain()
		{


            //***Must call the UnlockRuntime method to unlock the control
	    //The unlock codes shown below are for formatting purposes only and will not work!
            //PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234);
            //PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234);


			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (imageXViewTemp.Image != null)
                {
                    imageXViewTemp.Image.Dispose();
                    imageXViewTemp.Image = null;
                }
                if (imageXViewTemp != null)
                {
                    imageXViewTemp.Dispose();
                    imageXViewTemp = null;
                }
                if (imageXView.Image != null)
                {
                    imageXView.Image.Dispose();
                    imageXView.Image = null;
                }
                if (imageXView != null)
                {
                    imageXView.Dispose();
                    imageXView = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (barcodeXpress1 != null)
                {
                    barcodeXpress1.Dispose();
                    barcodeXpress1 = null;
                }
                if (scanFix1 != null)
                {
                    scanFix1.Dispose();
                    scanFix1 = null;
                }
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.statusBarMain = new System.Windows.Forms.StatusBar();
            this.statusBarPanelStat = new System.Windows.Forms.StatusBarPanel();
            this.statusBarPanelTime = new System.Windows.Forms.StatusBarPanel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxResults = new System.Windows.Forms.GroupBox();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBoxMain = new System.Windows.Forms.GroupBox();
            this.imageXView = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.imageXViewTemp = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
            this.scanFix1 = new PegasusImaging.WinForms.ScanFix5.ScanFix(this.components);
            this.barcodeXpress1 = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recognitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analyzeBarcodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enhanceImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preProcessingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barcodeWritingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeBarcodesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setBarcodeXpressEditionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barcodeXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scanFixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OpenButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.CloseButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.PreButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.AnalyzeButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.OptionsButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.WriteButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.HelpFileButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.AboutButton = new System.Windows.Forms.ToolStripButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelStat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelTime)).BeginInit();
            this.groupBoxResults.SuspendLayout();
            this.groupBoxMain.SuspendLayout();
            this.imageXView.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusBarMain
            // 
            this.statusBarMain.Location = new System.Drawing.Point(0, 577);
            this.statusBarMain.Name = "statusBarMain";
            this.statusBarMain.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanelStat,
            this.statusBarPanelTime});
            this.statusBarMain.ShowPanels = true;
            this.statusBarMain.Size = new System.Drawing.Size(850, 26);
            this.statusBarMain.SizingGrip = false;
            this.statusBarMain.TabIndex = 0;
            this.statusBarMain.Text = "hello";
            // 
            // statusBarPanelStat
            // 
            this.statusBarPanelStat.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanelStat.Name = "statusBarPanelStat";
            this.statusBarPanelStat.Text = "statusBarPanel";
            this.statusBarPanelStat.Width = 840;
            // 
            // statusBarPanelTime
            // 
            this.statusBarPanelTime.Alignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.statusBarPanelTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.statusBarPanelTime.Name = "statusBarPanelTime";
            this.statusBarPanelTime.Width = 10;
            // 
            // groupBoxResults
            // 
            this.groupBoxResults.Controls.Add(this.richTextBox);
            this.groupBoxResults.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBoxResults.Location = new System.Drawing.Point(0, 77);
            this.groupBoxResults.Name = "groupBoxResults";
            this.groupBoxResults.Size = new System.Drawing.Size(187, 500);
            this.groupBoxResults.TabIndex = 0;
            this.groupBoxResults.TabStop = false;
            this.groupBoxResults.Text = "groupBoxResults";
            // 
            // richTextBox
            // 
            this.richTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.Location = new System.Drawing.Point(3, 16);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(181, 481);
            this.richTextBox.TabIndex = 0;
            this.richTextBox.Text = "";
            // 
            // groupBoxMain
            // 
            this.groupBoxMain.Controls.Add(this.imageXView);
            this.groupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxMain.Location = new System.Drawing.Point(187, 77);
            this.groupBoxMain.Name = "groupBoxMain";
            this.groupBoxMain.Size = new System.Drawing.Size(663, 500);
            this.groupBoxMain.TabIndex = 5;
            this.groupBoxMain.TabStop = false;
            this.groupBoxMain.Text = "groupBox1";
            // 
            // imageXView
            // 
            this.imageXView.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.imageXView.AutoScroll = true;
            this.imageXView.Controls.Add(this.imageXViewTemp);
            this.imageXView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageXView.Location = new System.Drawing.Point(3, 16);
            this.imageXView.Name = "imageXView";
            this.imageXView.Size = new System.Drawing.Size(657, 481);
            this.imageXView.TabIndex = 4;
            // 
            // imageXViewTemp
            // 
            this.imageXViewTemp.Location = new System.Drawing.Point(369, 238);
            this.imageXViewTemp.Name = "imageXViewTemp";
            this.imageXViewTemp.Size = new System.Drawing.Size(126, 102);
            this.imageXViewTemp.TabIndex = 5;
            this.imageXViewTemp.Visible = false;
            // 
            // scanFix1
            // 
            this.scanFix1.Debug = false;
            this.scanFix1.DebugLogFile = "C:\\Documents and Settings\\jargento.JPG.COM\\My Documents\\ScanFix5.log";
            this.scanFix1.ErrorLevel = PegasusImaging.WinForms.ScanFix5.ErrorLevel.Production;
            this.scanFix1.ResolutionUnits = System.Drawing.GraphicsUnit.Inch;
            // 
            // barcodeXpress1
            // 
            this.barcodeXpress1.Debug = false;
            this.barcodeXpress1.DebugLogFile = "C:\\Documents and Settings\\jargento.JPG.COM\\My Documents\\BarcodeXpress5.log";
            this.barcodeXpress1.ErrorLevel = PegasusImaging.WinForms.BarcodeXpress5.ErrorLevelInfo.Production;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.recognitionToolStripMenuItem,
            this.enhanceImageToolStripMenuItem,
            this.barcodeWritingToolStripMenuItem,
            this.eToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(850, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // toolbarToolStripMenuItem
            // 
            this.toolbarToolStripMenuItem.Checked = true;
            this.toolbarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolbarToolStripMenuItem.Name = "toolbarToolStripMenuItem";
            this.toolbarToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.toolbarToolStripMenuItem.Text = "&Toolbar";
            this.toolbarToolStripMenuItem.Click += new System.EventHandler(this.toolbarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.statusBarToolStripMenuItem.Text = "&Statusbar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.statusBarToolStripMenuItem_Click);
            // 
            // recognitionToolStripMenuItem
            // 
            this.recognitionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analyzeBarcodesToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.recognitionToolStripMenuItem.Name = "recognitionToolStripMenuItem";
            this.recognitionToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.recognitionToolStripMenuItem.Text = "&Recognition";
            // 
            // analyzeBarcodesToolStripMenuItem
            // 
            this.analyzeBarcodesToolStripMenuItem.Enabled = false;
            this.analyzeBarcodesToolStripMenuItem.Name = "analyzeBarcodesToolStripMenuItem";
            this.analyzeBarcodesToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.analyzeBarcodesToolStripMenuItem.Text = "&Analyze Barcode(s)";
            this.analyzeBarcodesToolStripMenuItem.Click += new System.EventHandler(this.analyzeBarcodesToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.optionsToolStripMenuItem.Text = "Reading &Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // enhanceImageToolStripMenuItem
            // 
            this.enhanceImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preProcessingToolStripMenuItem});
            this.enhanceImageToolStripMenuItem.Name = "enhanceImageToolStripMenuItem";
            this.enhanceImageToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.enhanceImageToolStripMenuItem.Text = "&Enhance Image";
            // 
            // preProcessingToolStripMenuItem
            // 
            this.preProcessingToolStripMenuItem.Name = "preProcessingToolStripMenuItem";
            this.preProcessingToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.preProcessingToolStripMenuItem.Text = "&PreProcessing";
            this.preProcessingToolStripMenuItem.Click += new System.EventHandler(this.preProcessingToolStripMenuItem_Click);
            // 
            // barcodeWritingToolStripMenuItem
            // 
            this.barcodeWritingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeBarcodesToolStripMenuItem});
            this.barcodeWritingToolStripMenuItem.Name = "barcodeWritingToolStripMenuItem";
            this.barcodeWritingToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.barcodeWritingToolStripMenuItem.Text = "&Barcode Writing";
            // 
            // writeBarcodesToolStripMenuItem
            // 
            this.writeBarcodesToolStripMenuItem.Name = "writeBarcodesToolStripMenuItem";
            this.writeBarcodesToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.writeBarcodesToolStripMenuItem.Text = "&Write Barcodes";
            this.writeBarcodesToolStripMenuItem.Click += new System.EventHandler(this.writeBarcodesToolStripMenuItem_Click);
            // 
            // eToolStripMenuItem
            // 
            this.eToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setBarcodeXpressEditionToolStripMenuItem});
            this.eToolStripMenuItem.Name = "eToolStripMenuItem";
            this.eToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.eToolStripMenuItem.Text = "&Edition";
            // 
            // setBarcodeXpressEditionToolStripMenuItem
            // 
            this.setBarcodeXpressEditionToolStripMenuItem.Name = "setBarcodeXpressEditionToolStripMenuItem";
            this.setBarcodeXpressEditionToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.setBarcodeXpressEditionToolStripMenuItem.Text = "&Set Barcode Xpress Edition";
            this.setBarcodeXpressEditionToolStripMenuItem.Click += new System.EventHandler(this.setBarcodeXpressEditionToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.helpToolStripMenuItem1.Text = "H&elp...";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.barcodeXpressToolStripMenuItem,
            this.imagXpressToolStripMenuItem,
            this.scanFixToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // barcodeXpressToolStripMenuItem
            // 
            this.barcodeXpressToolStripMenuItem.Name = "barcodeXpressToolStripMenuItem";
            this.barcodeXpressToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.barcodeXpressToolStripMenuItem.Text = "&Barcode Xpress";
            this.barcodeXpressToolStripMenuItem.Click += new System.EventHandler(this.barcodeXpressToolStripMenuItem_Click);
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.imagXpressToolStripMenuItem.Text = "Imag&Xpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click);
            // 
            // scanFixToolStripMenuItem
            // 
            this.scanFixToolStripMenuItem.Name = "scanFixToolStripMenuItem";
            this.scanFixToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.scanFixToolStripMenuItem.Text = "&ScanFix";
            this.scanFixToolStripMenuItem.Click += new System.EventHandler(this.scanFixToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenButton,
            this.toolStripSeparator2,
            this.CloseButton,
            this.toolStripSeparator3,
            this.PreButton,
            this.toolStripSeparator7,
            this.AnalyzeButton,
            this.toolStripSeparator8,
            this.OptionsButton,
            this.toolStripSeparator6,
            this.WriteButton,
            this.toolStripSeparator5,
            this.HelpFileButton,
            this.toolStripSeparator4,
            this.AboutButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(850, 53);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OpenButton
            // 
            this.OpenButton.AutoSize = false;
            this.OpenButton.Image = ((System.Drawing.Image)(resources.GetObject("OpenButton.Image")));
            this.OpenButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(50, 50);
            this.OpenButton.Text = "Open";
            this.OpenButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.OpenButton.ToolTipText = "Open Image";
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 53);
            // 
            // CloseButton
            // 
            this.CloseButton.AutoSize = false;
            this.CloseButton.Image = ((System.Drawing.Image)(resources.GetObject("CloseButton.Image")));
            this.CloseButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(50, 50);
            this.CloseButton.Text = "Close";
            this.CloseButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.CloseButton.ToolTipText = "Close Image";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 53);
            // 
            // PreButton
            // 
            this.PreButton.AutoSize = false;
            this.PreButton.Image = ((System.Drawing.Image)(resources.GetObject("PreButton.Image")));
            this.PreButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PreButton.Name = "PreButton";
            this.PreButton.Size = new System.Drawing.Size(80, 50);
            this.PreButton.Text = "PreProcessing";
            this.PreButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PreButton.Click += new System.EventHandler(this.PreButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 53);
            // 
            // AnalyzeButton
            // 
            this.AnalyzeButton.AutoSize = false;
            this.AnalyzeButton.Enabled = false;
            this.AnalyzeButton.Image = ((System.Drawing.Image)(resources.GetObject("AnalyzeButton.Image")));
            this.AnalyzeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AnalyzeButton.Name = "AnalyzeButton";
            this.AnalyzeButton.Size = new System.Drawing.Size(50, 50);
            this.AnalyzeButton.Text = "Analyze";
            this.AnalyzeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AnalyzeButton.Click += new System.EventHandler(this.AnalyzeButton_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 53);
            // 
            // OptionsButton
            // 
            this.OptionsButton.AutoSize = false;
            this.OptionsButton.Image = ((System.Drawing.Image)(resources.GetObject("OptionsButton.Image")));
            this.OptionsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptionsButton.Name = "OptionsButton";
            this.OptionsButton.Size = new System.Drawing.Size(90, 50);
            this.OptionsButton.Text = "Reading Options";
            this.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.OptionsButton.Click += new System.EventHandler(this.OptionsButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 53);
            // 
            // WriteButton
            // 
            this.WriteButton.AutoSize = false;
            this.WriteButton.Image = ((System.Drawing.Image)(resources.GetObject("WriteButton.Image")));
            this.WriteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WriteButton.Name = "WriteButton";
            this.WriteButton.Size = new System.Drawing.Size(85, 50);
            this.WriteButton.Text = "Barcode Writing";
            this.WriteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.WriteButton.ToolTipText = "Write barcodes";
            this.WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 53);
            // 
            // HelpFileButton
            // 
            this.HelpFileButton.AutoSize = false;
            this.HelpFileButton.Image = ((System.Drawing.Image)(resources.GetObject("HelpFileButton.Image")));
            this.HelpFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.HelpFileButton.Name = "HelpFileButton";
            this.HelpFileButton.Size = new System.Drawing.Size(50, 50);
            this.HelpFileButton.Text = "Help";
            this.HelpFileButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.HelpFileButton.ToolTipText = "Help";
            this.HelpFileButton.Click += new System.EventHandler(this.HelpFileButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 53);
            // 
            // AboutButton
            // 
            this.AboutButton.AutoSize = false;
            this.AboutButton.Image = ((System.Drawing.Image)(resources.GetObject("AboutButton.Image")));
            this.AboutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(50, 50);
            this.AboutButton.Text = "About";
            this.AboutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AboutButton.ToolTipText = "About BarcodeXpress";
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(187, 77);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 500);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(850, 603);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBoxMain);
            this.Controls.Add(this.groupBoxResults);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusBarMain);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Menu = this.mainMenu;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Barcode Xpress Demo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelStat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanelTime)).EndInit();
            this.groupBoxResults.ResumeLayout(false);
            this.groupBoxMain.ResumeLayout(false);
            this.imageXView.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
            Application.EnableVisualStyles();

			FormSplash splash = new FormSplash ();
			splash.ShowDialog ();

            if (splash != null)
            {
                splash.Dispose();
                splash = null;
            }

			Application.Run(new FormMain());
        }


		private void FormMain_Load(object sender, System.EventArgs e)
		{

            try
            {
                //***Must call the UnlockRuntime method to unlock the control
		        //The unlock codes shown below are for formatting purposes only and will not work!
                //barcodeXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);
                //scanFix1.License.UnlockRuntime(1234,1234,1234,1234);
                //imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);

                LoadNewDoc();

                openPath = Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
               
                if (System.IO.Path.GetFileName(Application.StartupPath) == "BarcodeXpressLicensedDemo")
                {
                    openPath = Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
                }

                barcodeXpress1.Licensing.LicenseEdition = PegasusImaging.WinForms.BarcodeXpress5.LicenseChoice.ProfessionalPlus2DWriteEdition;

                string strProdName = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).ProductName;
                strProdName += " " + System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion;

                if (barcodeXpress1.Licensing.LicenseEdition == LicenseChoice.ProfessionalEdition)
                {
                    this.Text = "Barcode Xpress 5.0 - Professional Edition";
                }
                else if (barcodeXpress1.Licensing.LicenseEdition == LicenseChoice.StandardEdition)
                {
                    this.Text = "Barcode Xpress 5.0 - Standard Edition";
                }
                else if (barcodeXpress1.Licensing.LicenseEdition == LicenseChoice.ProfessionalPlus2DWriteEdition)
                {
                    this.Text = "Barcode Xpress 5.0 - Professional Plus 2D Write Edition";
                }

                this.Show();
            }
            catch (PegasusImaging.WinForms.ScanFix5.ScanFixException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
            {
                MessageBox.Show(ex.Message);
            }
		}

        private void checkType(FormOptions form)
        {
            form._chkBCType_0.Checked = BarcodeSearch(BarcodeType.Industry2of5Barcode);
            form._chkBCType_1.Checked = BarcodeSearch(BarcodeType.Interleaved2of5Barcode);
            form._chkBCType_2.Checked = BarcodeSearch(BarcodeType.IATA2of5Barcode);
            form._chkBCType_3.Checked = BarcodeSearch(BarcodeType.DataLogic2of5Barcode);
            form._chkBCType_4.Checked = BarcodeSearch(BarcodeType.Invert2of5Barcode);
            form._chkBCType_5.Checked = BarcodeSearch(BarcodeType.BCDMatrixBarcode);
            form._chkBCType_6.Checked = BarcodeSearch(BarcodeType.Matrix2of5Barcode);
            form._chkBCType_7.Checked = BarcodeSearch(BarcodeType.Code32Barcode);
            form._chkBCType_8.Checked = BarcodeSearch(BarcodeType.Code39Barcode);
            form._chkBCType_9.Checked = BarcodeSearch(BarcodeType.CodabarBarcode);
            form._chkBCType_10.Checked = BarcodeSearch(BarcodeType.Code93Barcode);
            form._chkBCType_11.Checked = BarcodeSearch(BarcodeType.Code128Barcode);
            form._chkBCType_12.Checked = BarcodeSearch(BarcodeType.EAN13Barcode);
            form._chkBCType_13.Checked = BarcodeSearch(BarcodeType.EAN8Barcode);
            form._chkBCType_14.Checked = BarcodeSearch(BarcodeType.UPCABarcode);
            form._chkBCType_15.Checked = BarcodeSearch(BarcodeType.UPCEBarcode);
            form._chkBCType_16.Checked = BarcodeSearch(BarcodeType.Add5Barcode);
            form._chkBCType_17.Checked = BarcodeSearch(BarcodeType.Add2Barcode);
            form._chkBCType_18.Checked = BarcodeSearch(BarcodeType.EAN128Barcode);
            form.Code39ExtCheckBox.Checked = BarcodeSearch(BarcodeType.Code39ExtendedBarcode);
            form.Code93ExtCheckBox.Checked = BarcodeSearch(BarcodeType.Code93ExtendedBarcode);

            form.optTypePFD417.Checked = BarcodeSearch(BarcodeType.PDF417Barcode);
            form.optTypePatchCode.Checked = BarcodeSearch(BarcodeType.PatchCodeBarcode);
            form.optTypeDataMatrix.Checked = BarcodeSearch(BarcodeType.DataMatrixBarcode);
            form.optTypePostNet.Checked = BarcodeSearch(BarcodeType.PostNetBarcode);
            form.QRRadioButton.Checked = BarcodeSearch(BarcodeType.QRCodeBarcode);
            form.AusRadioButton.Checked = BarcodeSearch(BarcodeType.AustralianPost4StateBarcode);
            form.RoyalRadioButton.Checked = BarcodeSearch(BarcodeType.RoyalPost4StateBarcode);
            form.OneCodeRadioButton.Checked = BarcodeSearch(BarcodeType.IntelligentMailBarcode);            
        }

        private bool BarcodeSearch(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType barcodeTypeChosen)
        {
            if (barcodeXpress1.reader.BarcodeTypes.Length > 0)
            {
                for (int i = 0; i < barcodeXpress1.reader.BarcodeTypes.Length; i++)
                {
                    System.Array types = barcodeXpress1.reader.BarcodeTypes;
                    if ((PegasusImaging.WinForms.BarcodeXpress5.BarcodeType)types.GetValue(i) == barcodeTypeChosen)
                    {
                        return true;
                    }
                }

                return false;
            }

            return false;
        }

        private void Analyze()
        {
            try
            {              
                richTextBox.Text = "";
                statusBarPanelStat.Text = "";

                statusBarPanelStat.Text = "Detecting Barcodes...";
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                System.DateTime startTime = System.DateTime.Now;


                // copy the DIB before analyze info added
                imageXView.Image = imageXViewTemp.Image.Copy();
                imageXViewTemp.Image = imageXView.Image.Copy();

                System.Drawing.Rectangle currentArea = new System.Drawing.Rectangle(0, 0, 0, 0);
                barcodeXpress1.reader.Area = currentArea;
                barcodeXpress1.reader.ReturnPossibleBarcodes = true;

                if (imageXView.Image.ImageXData.BitsPerPixel != 0)
                {
                    PegasusImaging.WinForms.ImagXpress8.Processor proc = new PegasusImaging.WinForms.ImagXpress8.Processor();
                    proc.Image = imageXView.Image.Copy();
                    proc.ColorDepth(1, 0, 0);
                    imageXView.Image = proc.Image;
                }

                int DIB;
                DIB = imageXView.Image.ToHdib(false).ToInt32();


                long start = 0;
                long stop = 0;
                long freq = 0;

                QueryPerformanceCounter(out start);

                QueryPerformanceFrequency(out freq);	

                results = barcodeXpress1.reader.Analyze(DIB);

                QueryPerformanceCounter(out stop);

                double timeElapsed = ((double)(stop - start) / (double)freq);

                this.Cursor = System.Windows.Forms.Cursors.Default;

                if (results.Length > 0)
                {
                    statusBarPanelStat.Text = "Barcode Detection Successful";

                    

                    string strResult = "\nAnalyze Time: \r" + timeElapsed.ToString().Substring(0,7) + " sec.\r";
                    strResult += results.Length.ToString() + " barcode(s) found.\r\r";


                    proc = new PegasusImaging.WinForms.ImagXpress8.Processor();
                    proc.Image = imageXView.Image.Copy();
                    proc.ColorDepth(24, 0, 0);
                    imageXView.Image = proc.Image;

                    PegasusImaging.WinForms.ImagXpress8.ImageX img = imageXView.Image.Copy();

                    System.Drawing.Graphics g = img.GetGraphics();

                    for (int i = 0; i < results.Length; i++)
                    {
                        PegasusImaging.WinForms.BarcodeXpress5.Result curResult = (PegasusImaging.WinForms.BarcodeXpress5.Result)results.GetValue(i);

                        if (results[i].BarcodeType == BarcodeType.UnknownBarcode)
                        {
                            strResult += "Symbol #" + i.ToString() + "\n" + "Type = " + "Unknown Barcode" + "\n";
                        }
                        else
                        {
                            strResult += "Symbol #" + i.ToString() + "\n" + "Type = " + curResult.BarcodeName + "\n";
                        }

                        strResult += "X - " + curResult.Area.X.ToString() + " pixels" + "\n" + "Y - " + curResult.Area.Y.ToString() + " pixels" + "\n";
                        strResult += "W - " + curResult.Area.Width.ToString() + " pixels" + "\n" + "H - " + curResult.Area.Height.ToString() + " pixels" + "\n";
                        strResult += "Value:" + "\n" + curResult.BarcodeValue + "\n" + "Confidence: " + curResult.Confidence.ToString() + "\n\n";


                        System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.Black);

                        Point point1 = new Point(curResult.Area.X, curResult.Area.Y);
                        Point point2 = new Point(curResult.Area.X + curResult.Area.Width, curResult.Area.Y);
                        Point point3 = new Point(curResult.Area.X + curResult.Area.Width, curResult.Area.Y + curResult.Area.Height);
                        Point point4 = new Point(curResult.Area.X, curResult.Area.Y + curResult.Area.Height);

                        g.DrawLines(pen, new Point[] { point1, point2, point3, point4, point1 });

                        String strMsg = "Symbol #" + i.ToString() + " - " + curResult.BarcodeValue;
                        System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(this.Handle);
                        SizeF size = graphics.MeasureString(strMsg, imageXView.Font);


                        System.Drawing.Font font = new Font("Arial", 7);

                        System.Drawing.Brush brush = System.Drawing.Brushes.White;
                        g.FillRectangle(brush, curResult.Area.X + 2, point1.Y - font.Height, curResult.Area.Width, font.Height);


                        if (results[i].BarcodeValue == null)
                        {
                            g.DrawString("Symbol #" + i.ToString() + " - ", font, System.Drawing.Brushes.Black, curResult.Area.X + 2, point1.Y - font.Height);
                        }
                        else
                        {
                            g.DrawString("Symbol #" + i.ToString() + " - " + curResult.BarcodeValue.ToString(), font, System.Drawing.Brushes.Black, curResult.Area.X + 2, point1.Y - font.Height);
                        }

                        

                        switch (curResult.BarcodeType)
                        {
                            case PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode:
                            case PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode:
                            case PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.QRCodeBarcode:

                                if (results[i].BarcodeValue != null)
                                {
                                    if (results[i].BarcodeValue.Length != results[i].BarcodeData.Length)
                                    {
                                        BinaryData(curResult);
                                    }
                                }
                                break;
                        }

                    }

                    img.ReleaseGraphics();

                    imageXView.Image = img;

                    strResult += "\n";
                    richTextBox.Text = strResult;

                }
                else
                {
                    statusBarPanelStat.Text = "Barcode Detection Failed";
                    richTextBox.Text = "Could not detect any barcodes. See Help for reasons why the barcode detection might have failed.";
                }


                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB));

                int DIB2 = imageXViewTemp.Image.ToHdib(false).ToInt32();
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB2));
            }
            catch (PegasusImaging.WinForms.BarcodeXpress5.InvalidLicenseException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BinaryData(PegasusImaging.WinForms.BarcodeXpress5.Result result)
        {
            BinaryForm binForm = new BinaryForm(result);
            binForm.ShowDialog();

            if (binForm != null)
            {
                binForm.Dispose();
                binForm = null;
            }
        }

		private void LoadNewDoc ()
		{
			this.statusBarPanelStat.Text = "Use File | Open to open an image for analysis";
			groupBoxResults.Text = "Barcode Analysis";
			groupBoxMain.Text = "Use File | Open to open an image for analysis";
		}

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Open a 1-Bit Black and White Image File";
            openFileDialog.InitialDirectory = openPath;
            openFileDialog.Filter = "All Files (*.*) | *.*";
            openFileDialog.ShowDialog(this);

            string strFileName = openFileDialog.FileName;
            statusBarPanelStat.Text = "";

            richTextBox.Text = "";

            try
            {

                if (System.IO.File.Exists(strFileName))
                {
                    imageXViewTemp.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strFileName);

                    PegasusImaging.WinForms.ImagXpress8.Processor proc = new PegasusImaging.WinForms.ImagXpress8.Processor();

                    imageXView.Image = imageXViewTemp.Image;

                    if (imageXView.Image.ImageXData.Format == PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff
                        | imageXView.Image.ImageXData.Format == PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Pdf)
                    {
                        if (imageXView.Image.PageCount > 1)
                        {
                            MultiPageForm multiFrm = new MultiPageForm(imageXView.Image.PageCount);
                            multiFrm.ShowDialog();

                            imageXView.Image.Page = Convert.ToInt32(multiFrm.PageBox.Text);
                        }
                    }

                    if (imageXView.Image.ImageXData.BitsPerPixel != 1)
                    {

                        BinarizeForm binarizeForm = new BinarizeForm(imageXView.Image.Copy(), scanFix1);

                        binarizeForm.ShowDialog();

                        imageXViewTemp.Image = binarizeForm.image;

                        imageXView.Image = imageXViewTemp.Image.Copy();

                    }

                    if (imageXView.Image.Resolution.Dimensions.Width != imageXView.Image.Resolution.Dimensions.Height)
                    {
                        proc.Image = imageXView.Image.Copy();
                        if (imageXView.Image.Resolution.Dimensions.Width > imageXView.Image.Resolution.Dimensions.Height)
                        {
                            proc.Resize(new System.Drawing.Size(imageXView.Image.ImageXData.Width,
                                        (int)(imageXView.Image.ImageXData.Height * imageXView.Image.Resolution.Dimensions.Width / imageXView.Image.Resolution.Dimensions.Height)),
                                        PegasusImaging.WinForms.ImagXpress8.ResizeType.Quality);
                            imageXView.Image = proc.Image;
                            imageXView.Image.Resolution.Dimensions = new System.Drawing.Size((int)imageXView.Image.Resolution.Dimensions.Width, (int)imageXView.Image.Resolution.Dimensions.Width);
                        }
                        else
                        {
                            proc.Resize(new System.Drawing.Size((int)(imageXView.Image.ImageXData.Width * imageXView.Image.Resolution.Dimensions.Height / imageXView.Image.Resolution.Dimensions.Width),
                                        imageXView.Image.ImageXData.Height), PegasusImaging.WinForms.ImagXpress8.ResizeType.Quality);
                            imageXView.Image = proc.Image;
                            imageXView.Image.Resolution.Dimensions = new System.Drawing.Size((int)imageXView.Image.Resolution.Dimensions.Height, (int)imageXView.Image.Resolution.Dimensions.Height);
                        }

                        MessageBox.Show(@"The image does not have symmetric resolution, the image will be resized with respect to it's new symmetric resolution (the larger of it's current vertical or horizontal resolution) otherwise barcode detection will be difficult.", "Asymmetric resolution", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    imageXViewTemp.Image = imageXView.Image.Copy();

                    groupBoxMain.Text = openFileDialog.FileName;

                    imageXView.Visible = true;

                    AnalyzeButton.Enabled = true;
                    analyzeBarcodesToolStripMenuItem.Enabled = true;
                    statusBarPanelStat.Text = "Click the Analyze button to detect barcodes in the image.";
                }
            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                groupBoxMain.Text = "Use File | Open to open an image for analysis";
                statusBarPanelStat.Text = "An error occurred loading the image.  Please make sure that the image is a 1-bit image.";
                MessageBox.Show(ex.Message);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (imageXView.Image != null)
                    imageXView.Image = null;
                else
                    imageXView.Image = new PegasusImaging.WinForms.ImagXpress8.ImageX(1, 1, 8, System.Drawing.Color.FromArgb(192, 192, 192));

                LoadNewDoc();

                richTextBox.Text = "";
                analyzeBarcodesToolStripMenuItem.Enabled = false;
                AnalyzeButton.Enabled = false;
                imageXView.Refresh();
                statusBarPanelStat.Text = "";
                groupBoxMain.Text = "Use File | Open to open an image for analysis";
            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void analyzeBarcodesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (counter == 0)
            {
                optionsToolStripMenuItem_Click(sender, e);
                if (cancelNotPressed == true)
                {
                    Analyze();
                }
                counter = 1;
            }
            else
            {
                Analyze();
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                counter = 1;
                // Force the user to at least look at the options screen.
                FormOptions form = new FormOptions();

                // set the requried properties before we show
                form.BarcodeInkColor = barcodeXpress1.reader.InkColor;
                form.BarcodeTypes = barcodeXpress1.reader.BarcodeTypes;
                form.MaxBarcodes = barcodeXpress1.reader.MaximumBarcodes;
                form.BarcodeOrientation = barcodeXpress1.reader.Orientation;
                form.CheckSumCheckBox.Checked = barcodeXpress1.reader.AppendCheckSum;
                form.ScanBox.Value = barcodeXpress1.reader.ScanDistance;

                checkType(form);

                form.ShowDialog(this);

                if (form.Cancel == false)
                {
                    cancelNotPressed = true;
                    // we have returned, so set the properties on the ScanXpress control
                    barcodeXpress1.reader.InkColor = form.BarcodeInkColor;
                    barcodeXpress1.reader.BarcodeTypes = form.BarcodeTypes;
                    barcodeXpress1.reader.MaximumBarcodes = (short)form.MaxBarcodes;
                    barcodeXpress1.reader.Orientation = form.BarcodeOrientation;
                    barcodeXpress1.reader.AppendCheckSum = form.CheckSumCheckBox.Checked;
                    barcodeXpress1.reader.ScanDistance = (int)form.ScanBox.Value;

                    if (form.CustomStatesRadioButton.Checked == true)
                    {
                        barcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.BarStates;
                    }
                    else if (form.CustomTableCRadioButton.Checked == true)
                    {
                        barcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.TableC;
                    }
                    else if (form.CustomTableNRadioButton.Checked == true)
                    {
                        barcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.TableN;
                    }
                    else
                    {
                        barcodeXpress1.reader.AustralianPostCustomDecodeMode = AustralianPostCustomDecodeType.NoCustomDecode;
                    }
                }
            }
            catch (PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void preProcessingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (imageXView.Image == null)
                {
                    MessageBox.Show("Please load an image before setting the preprocess options.", "BarcodeXpress Demo", System.Windows.Forms.MessageBoxButtons.OK);
                    return;
                }

                // copy the DIB before analyze info added
                imageXView.Image = imageXViewTemp.Image.Copy();

                ProcessScreen processFrm = new ProcessScreen(imageXView, imageXViewTemp, scanFix1);

                processFrm.ShowDialog();

            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (PegasusImaging.WinForms.ScanFix5.ScanFixException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void writeBarcodesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormBarcodeWriting form = new FormBarcodeWriting(barcodeXpress1);
            form.ShowDialog(this);
        }

        private void setBarcodeXpressEditionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEdition form = new FormEdition();
            form.BarcodeEdition = barcodeXpress1.Licensing.LicenseEdition;
            form.ShowDialog(this);

            barcodeXpress1.Licensing.LicenseEdition = form.BarcodeEdition;

            switch (form.BarcodeEdition)
            {
                case LicenseChoice.ProfessionalEdition:
                    this.Text = "Barcode Xpress v5.0 Demo - Professional Edition";
                    break;
                case LicenseChoice.StandardEdition:
                    this.Text = "Barcode Xpress v5.0 Demo - Standard Edition";
                    break;
                case LicenseChoice.ProfessionalPlus2DWriteEdition:
                    this.Text = "Barcode Xpress v5.0 Demo - Professional Plus 2D Write Edition";
                    break;
            }
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            helpPath = Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\Documentation\\Help\\Demos\\DotNet\\PegasusImaging.WinForms.BarcodeXpress5.Demo.chm";

            if (System.IO.Path.GetFileName(Application.StartupPath) == "BarcodeXpressLicensedDemo")
            {
                helpPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\Documentation\\Help\\Demos\\DotNet\\PegasusImaging.WinForms.BarcodeXpress5.Demo.chm";
            }

            helpPath = System.IO.Path.GetFullPath(helpPath);

            Help.ShowHelp(this, helpPath);
        }

        private void barcodeXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            barcodeXpress1.AboutBox();
        }

        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void scanFixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scanFix1.AboutBox();
        }

        private void statusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (statusBarMain.Visible == true)
            {
                statusBarMain.Visible = false;
                statusBarToolStripMenuItem.Checked = false;
            }
            else
            {
                statusBarMain.Visible = true;
                statusBarToolStripMenuItem.Checked = true;
            }
        }

        private void toolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (toolStrip1.Visible == true)
            {
                toolStrip1.Visible = false;
                toolbarToolStripMenuItem.Checked = false;
            }
            else
            {
                toolStrip1.Visible = true;
                toolbarToolStripMenuItem.Checked = true;
            }
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            openToolStripMenuItem_Click(sender, e);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            closeToolStripMenuItem_Click(sender, e);
        }

        private void PreButton_Click(object sender, EventArgs e)
        {
            preProcessingToolStripMenuItem_Click(sender, e);
        }

        private void AnalyzeButton_Click(object sender, EventArgs e)
        {
            analyzeBarcodesToolStripMenuItem_Click(sender, e);
        }

        private void OptionsButton_Click(object sender, EventArgs e)
        {
            optionsToolStripMenuItem_Click(sender, e);
        }

        private void WriteButton_Click(object sender, EventArgs e)
        {
            writeBarcodesToolStripMenuItem_Click(sender, e);
        }

        private void HelpFileButton_Click(object sender, EventArgs e)
        {
            helpToolStripMenuItem1_Click(sender, e);
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            barcodeXpressToolStripMenuItem_Click(sender, e);
        }
	}
}

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MultiPageForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MultiPageForm))
        Me.OKButton = New System.Windows.Forms.Button
        Me.DescriptionListBox = New System.Windows.Forms.ListBox
        Me.PageBox = New System.Windows.Forms.TextBox
        Me.CountBox = New System.Windows.Forms.TextBox
        Me.FirstLabel = New System.Windows.Forms.Label
        Me.MidLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(188, 115)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(91, 23)
        Me.OKButton.TabIndex = 0
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'DescriptionListBox
        '
        Me.DescriptionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionListBox.FormattingEnabled = True
        Me.DescriptionListBox.Items.AddRange(New Object() {"The file you're opening is a multi-page file.  Please indicate which ", "page you want to open below."})
        Me.DescriptionListBox.Location = New System.Drawing.Point(12, 12)
        Me.DescriptionListBox.Name = "DescriptionListBox"
        Me.DescriptionListBox.Size = New System.Drawing.Size(449, 43)
        Me.DescriptionListBox.TabIndex = 1
        '
        'PageBox
        '
        Me.PageBox.Location = New System.Drawing.Point(169, 72)
        Me.PageBox.Name = "PageBox"
        Me.PageBox.Size = New System.Drawing.Size(52, 20)
        Me.PageBox.TabIndex = 2
        Me.PageBox.Text = "1"
        '
        'CountBox
        '
        Me.CountBox.Location = New System.Drawing.Point(319, 72)
        Me.CountBox.Name = "CountBox"
        Me.CountBox.Size = New System.Drawing.Size(57, 20)
        Me.CountBox.TabIndex = 3
        '
        'FirstLabel
        '
        Me.FirstLabel.AutoSize = True
        Me.FirstLabel.Location = New System.Drawing.Point(79, 75)
        Me.FirstLabel.Name = "FirstLabel"
        Me.FirstLabel.Size = New System.Drawing.Size(71, 13)
        Me.FirstLabel.TabIndex = 4
        Me.FirstLabel.Text = "Open Page #"
        '
        'MidLabel
        '
        Me.MidLabel.AutoSize = True
        Me.MidLabel.Location = New System.Drawing.Point(263, 75)
        Me.MidLabel.Name = "MidLabel"
        Me.MidLabel.Size = New System.Drawing.Size(16, 13)
        Me.MidLabel.TabIndex = 5
        Me.MidLabel.Text = "of"
        '
        'MultiPageForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(470, 150)
        Me.Controls.Add(Me.MidLabel)
        Me.Controls.Add(Me.FirstLabel)
        Me.Controls.Add(Me.CountBox)
        Me.Controls.Add(Me.PageBox)
        Me.Controls.Add(Me.DescriptionListBox)
        Me.Controls.Add(Me.OKButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MultiPageForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MultiPageForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents DescriptionListBox As System.Windows.Forms.ListBox
    Friend WithEvents PageBox As System.Windows.Forms.TextBox
    Friend WithEvents CountBox As System.Windows.Forms.TextBox
    Friend WithEvents FirstLabel As System.Windows.Forms.Label
    Friend WithEvents MidLabel As System.Windows.Forms.Label
End Class

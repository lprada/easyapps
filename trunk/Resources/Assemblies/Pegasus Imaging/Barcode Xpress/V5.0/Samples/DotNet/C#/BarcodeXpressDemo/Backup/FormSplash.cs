/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for FormSplash.
	/// </summary>
	public class FormSplash : System.Windows.Forms.Form
	{
		public System.Windows.Forms.ToolTip ToolTip1;
		public System.Windows.Forms.GroupBox fraMainFrame;
		public System.Windows.Forms.Button cmdOK;
		public System.Windows.Forms.Label lblDisclaimer;
		public System.Windows.Forms.Label lblProductName;
		public System.Windows.Forms.Label lblPlatform;
		public System.Windows.Forms.Label lblVersion;
		public System.Windows.Forms.Label lblURL;
		public System.Windows.Forms.Label lblCompany;
		public System.Windows.Forms.Label lblCopyright;
		private System.Windows.Forms.Label DemoInfo;
		private System.Windows.Forms.PictureBox Logo;
		private System.ComponentModel.IContainer components;

		public FormSplash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSplash));
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.fraMainFrame = new System.Windows.Forms.GroupBox();
            this.DemoInfo = new System.Windows.Forms.Label();
            this.cmdOK = new System.Windows.Forms.Button();
            this.lblDisclaimer = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblPlatform = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblURL = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.fraMainFrame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // fraMainFrame
            // 
            this.fraMainFrame.BackColor = System.Drawing.SystemColors.Control;
            this.fraMainFrame.Controls.Add(this.DemoInfo);
            this.fraMainFrame.Controls.Add(this.cmdOK);
            this.fraMainFrame.Controls.Add(this.lblDisclaimer);
            this.fraMainFrame.Controls.Add(this.lblProductName);
            this.fraMainFrame.Controls.Add(this.lblPlatform);
            this.fraMainFrame.Controls.Add(this.lblVersion);
            this.fraMainFrame.Controls.Add(this.lblURL);
            this.fraMainFrame.Controls.Add(this.lblCompany);
            this.fraMainFrame.Controls.Add(this.lblCopyright);
            this.fraMainFrame.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fraMainFrame.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fraMainFrame.Location = new System.Drawing.Point(0, 99);
            this.fraMainFrame.Name = "fraMainFrame";
            this.fraMainFrame.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraMainFrame.Size = new System.Drawing.Size(728, 432);
            this.fraMainFrame.TabIndex = 1;
            this.fraMainFrame.TabStop = false;
            // 
            // DemoInfo
            // 
            this.DemoInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DemoInfo.Location = new System.Drawing.Point(65, 72);
            this.DemoInfo.Name = "DemoInfo";
            this.DemoInfo.Size = new System.Drawing.Size(584, 88);
            this.DemoInfo.TabIndex = 18;
            this.DemoInfo.Text = resources.GetString("DemoInfo.Text");
            this.DemoInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = System.Drawing.SystemColors.Control;
            this.cmdOK.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdOK.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdOK.Location = new System.Drawing.Point(561, 392);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdOK.Size = new System.Drawing.Size(89, 25);
            this.cmdOK.TabIndex = 5;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = false;
            // 
            // lblDisclaimer
            // 
            this.lblDisclaimer.BackColor = System.Drawing.SystemColors.Control;
            this.lblDisclaimer.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblDisclaimer.Font = new System.Drawing.Font("Arial", 9.6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisclaimer.ForeColor = System.Drawing.Color.Blue;
            this.lblDisclaimer.Location = new System.Drawing.Point(65, 184);
            this.lblDisclaimer.Name = "lblDisclaimer";
            this.lblDisclaimer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDisclaimer.Size = new System.Drawing.Size(584, 72);
            this.lblDisclaimer.TabIndex = 17;
            this.lblDisclaimer.Text = "PLEASE NOTE: This sample uses the ImagXpress product to perform image editing and" +
                "/or conversion.  ImagXpress Standard Edition is included in the BarcodeXpress5 t" +
                "oolkit.";
            this.lblDisclaimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductName
            // 
            this.lblProductName.BackColor = System.Drawing.SystemColors.Control;
            this.lblProductName.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblProductName.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblProductName.Location = new System.Drawing.Point(113, 24);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblProductName.Size = new System.Drawing.Size(481, 29);
            this.lblProductName.TabIndex = 16;
            this.lblProductName.Tag = "Product";
            this.lblProductName.Text = "Barcode Xpress 5.0 Demo";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPlatform
            // 
            this.lblPlatform.AccessibleDescription = "0";
            this.lblPlatform.AutoSize = true;
            this.lblPlatform.BackColor = System.Drawing.SystemColors.Control;
            this.lblPlatform.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblPlatform.Font = new System.Drawing.Font("Arial", 15.6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlatform.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPlatform.Location = new System.Drawing.Point(305, 288);
            this.lblPlatform.Name = "lblPlatform";
            this.lblPlatform.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblPlatform.Size = new System.Drawing.Size(113, 24);
            this.lblPlatform.TabIndex = 15;
            this.lblPlatform.Tag = "Platform";
            this.lblPlatform.Text = "For Win32";
            this.lblPlatform.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblVersion
            // 
            this.lblVersion.AccessibleDescription = "0";
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.SystemColors.Control;
            this.lblVersion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblVersion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblVersion.Location = new System.Drawing.Point(288, 324);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblVersion.Size = new System.Drawing.Size(152, 19);
            this.lblVersion.TabIndex = 14;
            this.lblVersion.Tag = "Version";
            this.lblVersion.Text = "Demo Version 5.00";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblURL
            // 
            this.lblURL.AccessibleDescription = "0";
            this.lblURL.AutoSize = true;
            this.lblURL.BackColor = System.Drawing.SystemColors.Control;
            this.lblURL.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblURL.Font = new System.Drawing.Font("Arial", 8.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblURL.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblURL.Location = new System.Drawing.Point(282, 385);
            this.lblURL.Name = "lblURL";
            this.lblURL.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblURL.Size = new System.Drawing.Size(158, 15);
            this.lblURL.TabIndex = 6;
            this.lblURL.Tag = "Warning";
            this.lblURL.Text = "www.pegasusimaging.com";
            this.lblURL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCompany
            // 
            this.lblCompany.AccessibleDescription = "0";
            this.lblCompany.BackColor = System.Drawing.SystemColors.Control;
            this.lblCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCompany.Font = new System.Drawing.Font("Arial", 8.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCompany.Location = new System.Drawing.Point(273, 368);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCompany.Size = new System.Drawing.Size(177, 17);
            this.lblCompany.TabIndex = 4;
            this.lblCompany.Tag = "Company";
            this.lblCompany.Text = "Pegasus Imaging Corp.";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCopyright
            // 
            this.lblCopyright.AccessibleDescription = "0";
            this.lblCopyright.BackColor = System.Drawing.SystemColors.Control;
            this.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCopyright.Font = new System.Drawing.Font("Arial", 8.4F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCopyright.Location = new System.Drawing.Point(273, 352);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCopyright.Size = new System.Drawing.Size(177, 17);
            this.lblCopyright.TabIndex = 3;
            this.lblCopyright.Tag = "Copyright";
            this.lblCopyright.Text = "Copyright 1999-2008";
            this.lblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Logo
            // 
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(0, 0);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(728, 104);
            this.Logo.TabIndex = 20;
            this.Logo.TabStop = false;
            // 
            // FormSplash
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(728, 536);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.fraMainFrame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSplash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Barcode Xpress 5.0 Demo";
            this.fraMainFrame.ResumeLayout(false);
            this.fraMainFrame.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion


    }
}

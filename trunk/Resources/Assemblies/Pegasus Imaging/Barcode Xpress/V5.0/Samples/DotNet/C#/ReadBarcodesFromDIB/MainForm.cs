/****************************************************************
 * Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.BarcodeXpress5;
using System.Runtime.InteropServices;

namespace DIBBC
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
    {
		private System.Windows.Forms.OpenFileDialog cd;
		private System.Windows.Forms.MainMenu MainMenu;
		private System.Windows.Forms.MenuItem FileMenu;
		private System.Windows.Forms.MenuItem OpenMenuItem;
		private System.Windows.Forms.MenuItem ExitMenuItem;
		private System.Windows.Forms.Button cmdread;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton readdata;
		private System.Windows.Forms.RadioButton readpdf;
		private System.Windows.Forms.RadioButton readpatch;
		private System.Windows.Forms.RadioButton read1d;
		private System.Windows.Forms.RadioButton readpost;
        private System.Windows.Forms.Label DescriptionLabel;
		private System.Windows.Forms.MenuItem AboutMenu;
        private System.Windows.Forms.MenuItem ImagXpressMenuItem;
        private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
        private RadioButton AusRadioButton;
        private RadioButton RoyalRadioButton;
        private RadioButton OneCodeRadioButton;
        private RadioButton QRRadioButton;
        private MenuItem BarcodeXpressMenuItem;
        private BarcodeXpress barcodeXpress1;
        private IContainer components;

		public MainForm()
		{

			// You need to get unlock codes from Pegasus Imaging Corporation
			// in order to distribute your application.  The keys shown below
			// are for illustration purposes only.
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234);

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }
                if (imageXView1 != null)
                {
                    imageXView1.Dispose();
                    imageXView1 = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (barcodeXpress1 != null)
                {
                    barcodeXpress1.Dispose();
                    barcodeXpress1 = null;
                }
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cd = new System.Windows.Forms.OpenFileDialog();
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.OpenMenuItem = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.BarcodeXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.cmdread = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AusRadioButton = new System.Windows.Forms.RadioButton();
            this.RoyalRadioButton = new System.Windows.Forms.RadioButton();
            this.OneCodeRadioButton = new System.Windows.Forms.RadioButton();
            this.QRRadioButton = new System.Windows.Forms.RadioButton();
            this.readpost = new System.Windows.Forms.RadioButton();
            this.readdata = new System.Windows.Forms.RadioButton();
            this.readpdf = new System.Windows.Forms.RadioButton();
            this.readpatch = new System.Windows.Forms.RadioButton();
            this.read1d = new System.Windows.Forms.RadioButton();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.barcodeXpress1 = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.OpenMenuItem,
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // OpenMenuItem
            // 
            this.OpenMenuItem.Index = 0;
            this.OpenMenuItem.Text = "&Open";
            this.OpenMenuItem.Click += new System.EventHandler(this.OpenMenuItem_Click);
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 1;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 1;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ImagXpressMenuItem,
            this.BarcodeXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 0;
            this.ImagXpressMenuItem.Text = "Imag&Xpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // BarcodeXpressMenuItem
            // 
            this.BarcodeXpressMenuItem.Index = 1;
            this.BarcodeXpressMenuItem.Text = "&BarcodeXpress";
            this.BarcodeXpressMenuItem.Click += new System.EventHandler(this.BarcodeXpressMenuItem_Click);
            // 
            // cmdread
            // 
            this.cmdread.Location = new System.Drawing.Point(156, 592);
            this.cmdread.Name = "cmdread";
            this.cmdread.Size = new System.Drawing.Size(184, 32);
            this.cmdread.TabIndex = 1;
            this.cmdread.Text = "Detect Barcodes";
            this.cmdread.Click += new System.EventHandler(this.cmdread_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AusRadioButton);
            this.groupBox1.Controls.Add(this.RoyalRadioButton);
            this.groupBox1.Controls.Add(this.OneCodeRadioButton);
            this.groupBox1.Controls.Add(this.QRRadioButton);
            this.groupBox1.Controls.Add(this.readpost);
            this.groupBox1.Controls.Add(this.readdata);
            this.groupBox1.Controls.Add(this.readpdf);
            this.groupBox1.Controls.Add(this.readpatch);
            this.groupBox1.Controls.Add(this.read1d);
            this.groupBox1.Location = new System.Drawing.Point(9, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(488, 96);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Barcode Types";
            // 
            // AusRadioButton
            // 
            this.AusRadioButton.Location = new System.Drawing.Point(368, 64);
            this.AusRadioButton.Name = "AusRadioButton";
            this.AusRadioButton.Size = new System.Drawing.Size(99, 16);
            this.AusRadioButton.TabIndex = 9;
            this.AusRadioButton.Text = "Australian Post";
            // 
            // RoyalRadioButton
            // 
            this.RoyalRadioButton.Location = new System.Drawing.Point(283, 64);
            this.RoyalRadioButton.Name = "RoyalRadioButton";
            this.RoyalRadioButton.Size = new System.Drawing.Size(79, 16);
            this.RoyalRadioButton.TabIndex = 8;
            this.RoyalRadioButton.Text = "Royal Post";
            // 
            // OneCodeRadioButton
            // 
            this.OneCodeRadioButton.Location = new System.Drawing.Point(283, 28);
            this.OneCodeRadioButton.Name = "OneCodeRadioButton";
            this.OneCodeRadioButton.Size = new System.Drawing.Size(106, 16);
            this.OneCodeRadioButton.TabIndex = 7;
            this.OneCodeRadioButton.Text = "IntelligentMail";
            // 
            // QRRadioButton
            // 
            this.QRRadioButton.Location = new System.Drawing.Point(203, 64);
            this.QRRadioButton.Name = "QRRadioButton";
            this.QRRadioButton.Size = new System.Drawing.Size(69, 16);
            this.QRRadioButton.TabIndex = 6;
            this.QRRadioButton.Text = "QR";
            // 
            // readpost
            // 
            this.readpost.Location = new System.Drawing.Point(203, 28);
            this.readpost.Name = "readpost";
            this.readpost.Size = new System.Drawing.Size(128, 16);
            this.readpost.TabIndex = 5;
            this.readpost.Text = "PostNet";
            // 
            // readdata
            // 
            this.readdata.Location = new System.Drawing.Point(110, 64);
            this.readdata.Name = "readdata";
            this.readdata.Size = new System.Drawing.Size(104, 16);
            this.readdata.TabIndex = 4;
            this.readdata.Text = "Data Matrix";
            // 
            // readpdf
            // 
            this.readpdf.Location = new System.Drawing.Point(110, 28);
            this.readpdf.Name = "readpdf";
            this.readpdf.Size = new System.Drawing.Size(120, 16);
            this.readpdf.TabIndex = 2;
            this.readpdf.Text = "PDF-417";
            // 
            // readpatch
            // 
            this.readpatch.Location = new System.Drawing.Point(16, 64);
            this.readpatch.Name = "readpatch";
            this.readpatch.Size = new System.Drawing.Size(104, 16);
            this.readpatch.TabIndex = 1;
            this.readpatch.Text = "Patch Code";
            // 
            // read1d
            // 
            this.read1d.Checked = true;
            this.read1d.Location = new System.Drawing.Point(16, 24);
            this.read1d.Name = "read1d";
            this.read1d.Size = new System.Drawing.Size(104, 24);
            this.read1d.TabIndex = 0;
            this.read1d.TabStop = true;
            this.read1d.Text = "1D-Standard";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DescriptionLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLabel.ForeColor = System.Drawing.Color.Blue;
            this.DescriptionLabel.Location = new System.Drawing.Point(9, 9);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(488, 48);
            this.DescriptionLabel.TabIndex = 3;
            this.DescriptionLabel.Text = "Use the File | Open command to load a 1-bit monochrome image file containing one " +
                "or more barcodes, then click the \"Detect Barcodes\" button.";
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(9, 162);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(488, 424);
            this.imageXView1.TabIndex = 5;
            // 
            // barcodeXpress1
            // 
            this.barcodeXpress1.ErrorLevel = PegasusImaging.WinForms.BarcodeXpress5.ErrorLevelInfo.Production;
            // 
            // MainForm
            // 
            this.AcceptButton = this.cmdread;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(509, 636);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmdread);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Read Barcode From DIB";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.Run(new MainForm());
		}

		private System.Array SetBarcodeType()
		{
			int currentcount = 0;

			if (read1d.Checked)
				currentcount++;
			if (readpatch.Checked)
				currentcount++;
			if (readpdf.Checked)
				currentcount++;
			if (readdata.Checked)
				currentcount++;
			if (readpost.Checked)
				currentcount++;
            if (QRRadioButton.Checked)
                currentcount++;
            if (AusRadioButton.Checked)
                currentcount++;
            if (RoyalRadioButton.Checked)
                currentcount++;
            if (OneCodeRadioButton.Checked)
                currentcount++;

			System.Array currentBarcodeTypes = new BarcodeType[currentcount];

			currentcount = 0;
			if (read1d.Checked)
				currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UnknownBarcode,currentcount++);
			if (readpatch.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode, currentcount++);
			if (readpdf.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, currentcount++);
			if (readdata.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, currentcount++);
			if (readpost.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PostNetBarcode, currentcount++);
            if (QRRadioButton.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.QRCodeBarcode, currentcount++);
            if (AusRadioButton.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.AustralianPost4StateBarcode, currentcount++);
            if (RoyalRadioButton.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.RoyalPost4StateBarcode, currentcount++);
            if (OneCodeRadioButton.Checked)
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.IntelligentMailBarcode, currentcount++);

			return currentBarcodeTypes;
		}


		private void cmdread_Click(object sender, System.EventArgs e)
		{
			try
			{

                if (imageXView1.Image == null)
                {
                    MessageBox.Show("No file loaded");
                    return;
                }

			System.Array BarcodeTypes;
            System.Int32 DIB;

			BarcodeTypes = SetBarcodeType();

            PegasusImaging.WinForms.BarcodeXpress5.Result[] results;


			barcodeXpress1.reader.BarcodeTypes = BarcodeTypes;

			System.Drawing.Rectangle currentArea = new System.Drawing.Rectangle(0, 0, 0, 0);
			barcodeXpress1.reader.Area = currentArea;
            DIB = imageXView1.Image.ToHdib(false).ToInt32();
            results = barcodeXpress1.reader.Analyze(DIB);

			this.Cursor = System.Windows.Forms.Cursors.Default;

			if (results.Length > 0)
			{
				string strResult;
				strResult = "";

				for (int i = 0; i < results.Length; i++)
				{
                    PegasusImaging.WinForms.BarcodeXpress5.Result curResult = (PegasusImaging.WinForms.BarcodeXpress5.Result)results.GetValue(i);
					strResult += "Symbol #" + i + "\r" + "Type = " + curResult.BarcodeName + "\r";
					strResult += "Value=" + curResult.BarcodeValue + "\r";
					string strMsg = "Symbol #" + i + " - " + curResult.BarcodeData;

				}
                
				strResult += "\n";
				MessageBox.Show(strResult, "Barcode Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
                MessageBox.Show("No Barcodes Found", "Barcode Result", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

           //Must free the DIB that was passed into the Analyze method
           Marshal.FreeHGlobal(new System.IntPtr(DIB));

			}
            catch (PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
			{
				string strResult;
				strResult = "Error: ";
				strResult += ex.Message;
				MessageBox.Show(strResult);
			}

        }


        private void BarcodeXpressMenuItem_Click(object sender, EventArgs e)
        {
            barcodeXpress1.AboutBox();
        }

        private void ImagXpressMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OpenMenuItem_Click(object sender, EventArgs e)
        {
            cd.Filter = "TIFF Files (*.TIF)|*.TIF|All Files (*.*)|*.*";
            cd.Title = "Open a 1-Bit Black and White Image File";
            cd.InitialDirectory = Application.ExecutablePath + "\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images";
            cd.ShowDialog(this);
            try
            {
                imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(cd.FileName);
            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                MessageBox.Show("Error Opening File.  ImagXpress Error: " + ex.Message);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {


            //***Must call the UnlockRuntime method to unlock the control
	    //The unlock codes shown below are for formatting purposes only and will not work!
            //barcodeXpress1.Licensing.UnlockRuntime(1234,1234,1234,1234);
            //imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);

            Application.EnableVisualStyles();
        }
	}
}


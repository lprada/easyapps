/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BarcodeDemo
{
    public partial class BinaryForm : Form
    {
        private char[] barcodeCharArray;
        private byte[] barcodeByteArray;
        private System.Drawing.Rectangle area;
        private string name;

        public BinaryForm(PegasusImaging.WinForms.BarcodeXpress5.Result result)
        {
            InitializeComponent();

            barcodeCharArray = result.BarcodeData;
            barcodeByteArray = result.BarcodeDataAsByte;
            area = result.Area;
            name = result.BarcodeName;
        }

        private void BinaryForm_Load(object sender, EventArgs e)
        {
            BarcodeFound.Text = "";
            BarcodeFound.Text = "Type = " + name + "\n";
            BarcodeFound.Text += "X - " + area.X + " pixels\nY - " + area.Y + " pixels\n";
            BarcodeFound.Text += "W - " + area.Width + " pixels\nH - " + area.Height + " pixels\n";


            string hexDump = "";
            for (int j = 0; j < barcodeByteArray.Length; j++)
            {
                if (barcodeByteArray[j] < 16)
                {
                    hexDump += Convert.ToChar(48) + barcodeByteArray[j].ToString("X") + Convert.ToChar(32);
                }
                else
                {
                    hexDump += barcodeByteArray[j].ToString("X") + Convert.ToChar(32);
                }
            }

            HexTextBox.Text = hexDump;

            byte[] copyBarcodeData = new byte[barcodeByteArray.Length];

            System.Array.Copy(barcodeByteArray, copyBarcodeData, barcodeByteArray.Length);

            char[] copyBarcodeChar = new char[barcodeCharArray.Length];
            int g = 0;
            for (int k = 0; k < barcodeCharArray.Length; k++)
            {
                if (barcodeByteArray[k] != 0)
                {
                    copyBarcodeChar[g] = Convert.ToChar(copyBarcodeData[k]);
                    g = g + 1;
                }
            }

            string binaryBarcodeData = new string(copyBarcodeChar);

            ValueTextBox.Text = binaryBarcodeData;

        }

        private void OKBinaryButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
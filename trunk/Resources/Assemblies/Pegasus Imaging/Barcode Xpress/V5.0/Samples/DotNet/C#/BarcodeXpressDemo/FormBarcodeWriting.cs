/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for FormBarcodeWriting.
	/// </summary>
	public class FormBarcodeWriting : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button buttonWrite;
		private System.Windows.Forms.Label labelHeightVal;
		private System.Windows.Forms.Label labelUPCFontVal;
		private System.Windows.Forms.Label labelValueFontVal;
		private System.Windows.Forms.Label labelUPCGapVal;
		private System.Windows.Forms.Label labelValueGapVal;
		private System.Windows.Forms.Label labelUPCNotchVal;
		private System.Windows.Forms.Label labelMinBarVal;
		private System.Windows.Forms.TextBox textBoxBCValue;
		private System.Windows.Forms.HScrollBar hScrollBarUPCGap;
		private System.Windows.Forms.HScrollBar hScrollBarValueFont;
		private System.Windows.Forms.HScrollBar hScrollBarUPCFont;
		private System.Windows.Forms.HScrollBar hScrollBarValueGap;
		private System.Windows.Forms.HScrollBar hScrollBarBCHeight;
		private System.Windows.Forms.HScrollBar hScrollBarMinBar;
		private System.Windows.Forms.HScrollBar hScrollBarUPCNotch;
		private System.Windows.Forms.Label labelUPCGap;
		private System.Windows.Forms.Label labelValueFont;
		private System.Windows.Forms.Label labelUPCFont;
		private System.Windows.Forms.Label labelUPCNotch;
		private System.Windows.Forms.Label labelValueGap;
		private System.Windows.Forms.Label labelBCHeight;
		private System.Windows.Forms.Label labelMinBar;
		private System.Windows.Forms.Label labelBCValue;
		private System.Windows.Forms.Button buttonPrint;
		private System.Windows.Forms.Button buttonCreate;
		private System.Windows.Forms.Label labelResult;
		private System.Windows.Forms.Label labelInfo;
		private System.Windows.Forms.ComboBox comboBoxStyle;
		private System.Windows.Forms.Label labelStyle;
		private System.Windows.Forms.Label labelStatus;
		private PegasusImaging.WinForms.BarcodeXpress5.Result[] results;
		internal System.Windows.Forms.ComboBox cmbBarcodeType;
		internal System.Windows.Forms.ComboBox cmbColumnsAndRows;
		internal System.Windows.Forms.HScrollBar hscHeight;
		internal System.Windows.Forms.HScrollBar hscWidth;
		internal System.Windows.Forms.HScrollBar hscRows;
		internal System.Windows.Forms.HScrollBar hscColumns;
		internal System.Windows.Forms.HScrollBar hscError;
		internal System.Windows.Forms.TextBox TextBox1;
		internal System.Windows.Forms.Label lblHeightVal;
		internal System.Windows.Forms.Label lblWidthVal;
		internal System.Windows.Forms.Label lblRowsVal;
		internal System.Windows.Forms.Label lblColumnsVal;
		internal System.Windows.Forms.Label lblErrorVal;
		internal System.Windows.Forms.Label lblHeight;
		internal System.Windows.Forms.Label lblWidth;
		internal System.Windows.Forms.Label lblRows;
		internal System.Windows.Forms.Label lblColumns;
		internal System.Windows.Forms.Label lblError;
		internal System.Windows.Forms.Label lblValue;
		internal System.Windows.Forms.Label lblStyle;
		internal System.Windows.Forms.Label lblInfo;
		internal System.Windows.Forms.Label lblResult;
        internal System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.Button closeForm;
		internal System.Windows.Forms.Button buttonPrintBarcode2D;
		internal System.Windows.Forms.Button buttonWriteBarcode2D;
        internal System.Windows.Forms.Button buttonCreateBarcode2D;

        private PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress barcodeXpress;

        private string savedImageFileName;
        private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
        private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView2;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormBarcodeWriting(PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress barcodeObject)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            barcodeXpress = barcodeObject;

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{                
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBarcodeWriting));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.buttonWrite = new System.Windows.Forms.Button();
            this.labelHeightVal = new System.Windows.Forms.Label();
            this.labelUPCFontVal = new System.Windows.Forms.Label();
            this.labelValueFontVal = new System.Windows.Forms.Label();
            this.labelUPCGapVal = new System.Windows.Forms.Label();
            this.labelValueGapVal = new System.Windows.Forms.Label();
            this.labelUPCNotchVal = new System.Windows.Forms.Label();
            this.labelMinBarVal = new System.Windows.Forms.Label();
            this.textBoxBCValue = new System.Windows.Forms.TextBox();
            this.hScrollBarUPCGap = new System.Windows.Forms.HScrollBar();
            this.hScrollBarValueFont = new System.Windows.Forms.HScrollBar();
            this.hScrollBarUPCFont = new System.Windows.Forms.HScrollBar();
            this.hScrollBarValueGap = new System.Windows.Forms.HScrollBar();
            this.hScrollBarBCHeight = new System.Windows.Forms.HScrollBar();
            this.hScrollBarMinBar = new System.Windows.Forms.HScrollBar();
            this.hScrollBarUPCNotch = new System.Windows.Forms.HScrollBar();
            this.labelUPCGap = new System.Windows.Forms.Label();
            this.labelValueFont = new System.Windows.Forms.Label();
            this.labelUPCFont = new System.Windows.Forms.Label();
            this.labelUPCNotch = new System.Windows.Forms.Label();
            this.labelValueGap = new System.Windows.Forms.Label();
            this.labelBCHeight = new System.Windows.Forms.Label();
            this.labelMinBar = new System.Windows.Forms.Label();
            this.labelBCValue = new System.Windows.Forms.Label();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.labelInfo = new System.Windows.Forms.Label();
            this.comboBoxStyle = new System.Windows.Forms.ComboBox();
            this.labelStyle = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.imageXView2 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.cmbBarcodeType = new System.Windows.Forms.ComboBox();
            this.cmbColumnsAndRows = new System.Windows.Forms.ComboBox();
            this.hscHeight = new System.Windows.Forms.HScrollBar();
            this.hscWidth = new System.Windows.Forms.HScrollBar();
            this.hscRows = new System.Windows.Forms.HScrollBar();
            this.hscColumns = new System.Windows.Forms.HScrollBar();
            this.hscError = new System.Windows.Forms.HScrollBar();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.lblHeightVal = new System.Windows.Forms.Label();
            this.lblWidthVal = new System.Windows.Forms.Label();
            this.lblRowsVal = new System.Windows.Forms.Label();
            this.lblColumnsVal = new System.Windows.Forms.Label();
            this.lblErrorVal = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblRows = new System.Windows.Forms.Label();
            this.lblColumns = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.lblStyle = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.buttonPrintBarcode2D = new System.Windows.Forms.Button();
            this.buttonWriteBarcode2D = new System.Windows.Forms.Button();
            this.buttonCreateBarcode2D = new System.Windows.Forms.Button();
            this.closeForm = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(16, 8);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(808, 544);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.imageXView1);
            this.tabPage1.Controls.Add(this.buttonWrite);
            this.tabPage1.Controls.Add(this.labelHeightVal);
            this.tabPage1.Controls.Add(this.labelUPCFontVal);
            this.tabPage1.Controls.Add(this.labelValueFontVal);
            this.tabPage1.Controls.Add(this.labelUPCGapVal);
            this.tabPage1.Controls.Add(this.labelValueGapVal);
            this.tabPage1.Controls.Add(this.labelUPCNotchVal);
            this.tabPage1.Controls.Add(this.labelMinBarVal);
            this.tabPage1.Controls.Add(this.textBoxBCValue);
            this.tabPage1.Controls.Add(this.hScrollBarUPCGap);
            this.tabPage1.Controls.Add(this.hScrollBarValueFont);
            this.tabPage1.Controls.Add(this.hScrollBarUPCFont);
            this.tabPage1.Controls.Add(this.hScrollBarValueGap);
            this.tabPage1.Controls.Add(this.hScrollBarBCHeight);
            this.tabPage1.Controls.Add(this.hScrollBarMinBar);
            this.tabPage1.Controls.Add(this.hScrollBarUPCNotch);
            this.tabPage1.Controls.Add(this.labelUPCGap);
            this.tabPage1.Controls.Add(this.labelValueFont);
            this.tabPage1.Controls.Add(this.labelUPCFont);
            this.tabPage1.Controls.Add(this.labelUPCNotch);
            this.tabPage1.Controls.Add(this.labelValueGap);
            this.tabPage1.Controls.Add(this.labelBCHeight);
            this.tabPage1.Controls.Add(this.labelMinBar);
            this.tabPage1.Controls.Add(this.labelBCValue);
            this.tabPage1.Controls.Add(this.buttonPrint);
            this.tabPage1.Controls.Add(this.buttonCreate);
            this.tabPage1.Controls.Add(this.labelResult);
            this.tabPage1.Controls.Add(this.labelInfo);
            this.tabPage1.Controls.Add(this.comboBoxStyle);
            this.tabPage1.Controls.Add(this.labelStyle);
            this.tabPage1.Controls.Add(this.labelStatus);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(800, 518);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "1D Barcode Writing";
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(342, 260);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(442, 196);
            this.imageXView1.TabIndex = 129;
            // 
            // buttonWrite
            // 
            this.buttonWrite.Location = new System.Drawing.Point(480, 16);
            this.buttonWrite.Name = "buttonWrite";
            this.buttonWrite.Size = new System.Drawing.Size(112, 24);
            this.buttonWrite.TabIndex = 128;
            this.buttonWrite.Text = "Write To File";
            this.buttonWrite.Click += new System.EventHandler(this.buttonWrite_Click);
            // 
            // labelHeightVal
            // 
            this.labelHeightVal.Location = new System.Drawing.Point(680, 80);
            this.labelHeightVal.Name = "labelHeightVal";
            this.labelHeightVal.Size = new System.Drawing.Size(40, 16);
            this.labelHeightVal.TabIndex = 126;
            // 
            // labelUPCFontVal
            // 
            this.labelUPCFontVal.Location = new System.Drawing.Point(680, 224);
            this.labelUPCFontVal.Name = "labelUPCFontVal";
            this.labelUPCFontVal.Size = new System.Drawing.Size(40, 16);
            this.labelUPCFontVal.TabIndex = 125;
            // 
            // labelValueFontVal
            // 
            this.labelValueFontVal.Location = new System.Drawing.Point(680, 200);
            this.labelValueFontVal.Name = "labelValueFontVal";
            this.labelValueFontVal.Size = new System.Drawing.Size(40, 16);
            this.labelValueFontVal.TabIndex = 124;
            // 
            // labelUPCGapVal
            // 
            this.labelUPCGapVal.Location = new System.Drawing.Point(680, 176);
            this.labelUPCGapVal.Name = "labelUPCGapVal";
            this.labelUPCGapVal.Size = new System.Drawing.Size(40, 16);
            this.labelUPCGapVal.TabIndex = 123;
            // 
            // labelValueGapVal
            // 
            this.labelValueGapVal.Location = new System.Drawing.Point(680, 152);
            this.labelValueGapVal.Name = "labelValueGapVal";
            this.labelValueGapVal.Size = new System.Drawing.Size(40, 16);
            this.labelValueGapVal.TabIndex = 122;
            // 
            // labelUPCNotchVal
            // 
            this.labelUPCNotchVal.Location = new System.Drawing.Point(680, 128);
            this.labelUPCNotchVal.Name = "labelUPCNotchVal";
            this.labelUPCNotchVal.Size = new System.Drawing.Size(40, 16);
            this.labelUPCNotchVal.TabIndex = 121;
            // 
            // labelMinBarVal
            // 
            this.labelMinBarVal.Location = new System.Drawing.Point(680, 104);
            this.labelMinBarVal.Name = "labelMinBarVal";
            this.labelMinBarVal.Size = new System.Drawing.Size(40, 16);
            this.labelMinBarVal.TabIndex = 120;
            // 
            // textBoxBCValue
            // 
            this.textBoxBCValue.Location = new System.Drawing.Point(488, 48);
            this.textBoxBCValue.Name = "textBoxBCValue";
            this.textBoxBCValue.Size = new System.Drawing.Size(184, 20);
            this.textBoxBCValue.TabIndex = 119;
            this.textBoxBCValue.Text = "textBox1";
            // 
            // hScrollBarUPCGap
            // 
            this.hScrollBarUPCGap.Location = new System.Drawing.Point(488, 176);
            this.hScrollBarUPCGap.Maximum = 200;
            this.hScrollBarUPCGap.Minimum = -200;
            this.hScrollBarUPCGap.Name = "hScrollBarUPCGap";
            this.hScrollBarUPCGap.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarUPCGap.TabIndex = 118;
            this.hScrollBarUPCGap.ValueChanged += new System.EventHandler(this.hScrollBarUPCGap_ValueChanged);
            // 
            // hScrollBarValueFont
            // 
            this.hScrollBarValueFont.Location = new System.Drawing.Point(488, 200);
            this.hScrollBarValueFont.Maximum = 150;
            this.hScrollBarValueFont.Minimum = 12;
            this.hScrollBarValueFont.Name = "hScrollBarValueFont";
            this.hScrollBarValueFont.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarValueFont.TabIndex = 117;
            this.hScrollBarValueFont.Value = 12;
            this.hScrollBarValueFont.ValueChanged += new System.EventHandler(this.hScrollBarValueFont_ValueChanged);
            // 
            // hScrollBarUPCFont
            // 
            this.hScrollBarUPCFont.Location = new System.Drawing.Point(488, 224);
            this.hScrollBarUPCFont.Minimum = 6;
            this.hScrollBarUPCFont.Name = "hScrollBarUPCFont";
            this.hScrollBarUPCFont.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarUPCFont.TabIndex = 116;
            this.hScrollBarUPCFont.Value = 8;
            this.hScrollBarUPCFont.ValueChanged += new System.EventHandler(this.hScrollBarUPCFont_ValueChanged);
            // 
            // hScrollBarValueGap
            // 
            this.hScrollBarValueGap.Location = new System.Drawing.Point(488, 152);
            this.hScrollBarValueGap.Maximum = 200;
            this.hScrollBarValueGap.Minimum = -200;
            this.hScrollBarValueGap.Name = "hScrollBarValueGap";
            this.hScrollBarValueGap.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarValueGap.TabIndex = 115;
            this.hScrollBarValueGap.ValueChanged += new System.EventHandler(this.hScrollBarValueGap_ValueChanged);
            // 
            // hScrollBarBCHeight
            // 
            this.hScrollBarBCHeight.Location = new System.Drawing.Point(488, 80);
            this.hScrollBarBCHeight.Maximum = 1200;
            this.hScrollBarBCHeight.Minimum = 100;
            this.hScrollBarBCHeight.Name = "hScrollBarBCHeight";
            this.hScrollBarBCHeight.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarBCHeight.TabIndex = 114;
            this.hScrollBarBCHeight.Value = 300;
            this.hScrollBarBCHeight.ValueChanged += new System.EventHandler(this.hScrollBarBCHeight_ValueChanged);
            // 
            // hScrollBarMinBar
            // 
            this.hScrollBarMinBar.Location = new System.Drawing.Point(488, 104);
            this.hScrollBarMinBar.Maximum = 15;
            this.hScrollBarMinBar.Minimum = 2;
            this.hScrollBarMinBar.Name = "hScrollBarMinBar";
            this.hScrollBarMinBar.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarMinBar.TabIndex = 113;
            this.hScrollBarMinBar.Value = 2;
            this.hScrollBarMinBar.ValueChanged += new System.EventHandler(this.hScrollBarMinBar_ValueChanged);
            // 
            // hScrollBarUPCNotch
            // 
            this.hScrollBarUPCNotch.Location = new System.Drawing.Point(488, 128);
            this.hScrollBarUPCNotch.Maximum = 50;
            this.hScrollBarUPCNotch.Name = "hScrollBarUPCNotch";
            this.hScrollBarUPCNotch.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarUPCNotch.TabIndex = 112;
            this.hScrollBarUPCNotch.ValueChanged += new System.EventHandler(this.hScrollBarUPCNotch_ValueChanged);
            // 
            // labelUPCGap
            // 
            this.labelUPCGap.Location = new System.Drawing.Point(376, 176);
            this.labelUPCGap.Name = "labelUPCGap";
            this.labelUPCGap.Size = new System.Drawing.Size(112, 16);
            this.labelUPCGap.TabIndex = 111;
            this.labelUPCGap.Text = "UPC Value Gap:";
            // 
            // labelValueFont
            // 
            this.labelValueFont.Location = new System.Drawing.Point(376, 200);
            this.labelValueFont.Name = "labelValueFont";
            this.labelValueFont.Size = new System.Drawing.Size(112, 16);
            this.labelValueFont.TabIndex = 110;
            this.labelValueFont.Text = "Value Font Size:";
            // 
            // labelUPCFont
            // 
            this.labelUPCFont.Location = new System.Drawing.Point(376, 224);
            this.labelUPCFont.Name = "labelUPCFont";
            this.labelUPCFont.Size = new System.Drawing.Size(120, 16);
            this.labelUPCFont.TabIndex = 109;
            this.labelUPCFont.Text = "UPC Small Font Size:";
            // 
            // labelUPCNotch
            // 
            this.labelUPCNotch.Location = new System.Drawing.Point(376, 128);
            this.labelUPCNotch.Name = "labelUPCNotch";
            this.labelUPCNotch.Size = new System.Drawing.Size(112, 16);
            this.labelUPCNotch.TabIndex = 108;
            this.labelUPCNotch.Text = "UPC Notch %:";
            // 
            // labelValueGap
            // 
            this.labelValueGap.Location = new System.Drawing.Point(376, 152);
            this.labelValueGap.Name = "labelValueGap";
            this.labelValueGap.Size = new System.Drawing.Size(112, 16);
            this.labelValueGap.TabIndex = 107;
            this.labelValueGap.Text = "Value Gap:";
            // 
            // labelBCHeight
            // 
            this.labelBCHeight.Location = new System.Drawing.Point(376, 80);
            this.labelBCHeight.Name = "labelBCHeight";
            this.labelBCHeight.Size = new System.Drawing.Size(112, 16);
            this.labelBCHeight.TabIndex = 106;
            this.labelBCHeight.Text = "Barcode Height:";
            // 
            // labelMinBar
            // 
            this.labelMinBar.Location = new System.Drawing.Point(376, 104);
            this.labelMinBar.Name = "labelMinBar";
            this.labelMinBar.Size = new System.Drawing.Size(112, 16);
            this.labelMinBar.TabIndex = 105;
            this.labelMinBar.Text = "Min. Bar Width:";
            // 
            // labelBCValue
            // 
            this.labelBCValue.Location = new System.Drawing.Point(376, 56);
            this.labelBCValue.Name = "labelBCValue";
            this.labelBCValue.Size = new System.Drawing.Size(96, 16);
            this.labelBCValue.TabIndex = 104;
            this.labelBCValue.Text = "Barcode Value:";
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(616, 16);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(112, 24);
            this.buttonPrint.TabIndex = 103;
            this.buttonPrint.Text = "Print Barcode";
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(344, 16);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(112, 24);
            this.buttonCreate.TabIndex = 102;
            this.buttonCreate.Text = "Create Barcode";
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // labelResult
            // 
            this.labelResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelResult.Location = new System.Drawing.Point(16, 160);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(304, 296);
            this.labelResult.TabIndex = 101;
            this.labelResult.Text = "label3";
            // 
            // labelInfo
            // 
            this.labelInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelInfo.Location = new System.Drawing.Point(16, 72);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(304, 80);
            this.labelInfo.TabIndex = 100;
            this.labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), an" +
                "d the following symbols: space, minus (-), plus (+), period (.), dollar sign ($)" +
                ", slash (/), and percent (%).";
            // 
            // comboBoxStyle
            // 
            this.comboBoxStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStyle.Location = new System.Drawing.Point(104, 40);
            this.comboBoxStyle.Name = "comboBoxStyle";
            this.comboBoxStyle.Size = new System.Drawing.Size(216, 21);
            this.comboBoxStyle.TabIndex = 99;
            this.comboBoxStyle.SelectedIndexChanged += new System.EventHandler(this.comboBoxStyle_SelectedIndexChanged);
            // 
            // labelStyle
            // 
            this.labelStyle.Location = new System.Drawing.Point(16, 48);
            this.labelStyle.Name = "labelStyle";
            this.labelStyle.Size = new System.Drawing.Size(80, 16);
            this.labelStyle.TabIndex = 98;
            this.labelStyle.Text = "Barcode Style:";
            // 
            // labelStatus
            // 
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStatus.Location = new System.Drawing.Point(16, 464);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(768, 48);
            this.labelStatus.TabIndex = 97;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.imageXView2);
            this.tabPage2.Controls.Add(this.cmbBarcodeType);
            this.tabPage2.Controls.Add(this.cmbColumnsAndRows);
            this.tabPage2.Controls.Add(this.hscHeight);
            this.tabPage2.Controls.Add(this.hscWidth);
            this.tabPage2.Controls.Add(this.hscRows);
            this.tabPage2.Controls.Add(this.hscColumns);
            this.tabPage2.Controls.Add(this.hscError);
            this.tabPage2.Controls.Add(this.TextBox1);
            this.tabPage2.Controls.Add(this.lblHeightVal);
            this.tabPage2.Controls.Add(this.lblWidthVal);
            this.tabPage2.Controls.Add(this.lblRowsVal);
            this.tabPage2.Controls.Add(this.lblColumnsVal);
            this.tabPage2.Controls.Add(this.lblErrorVal);
            this.tabPage2.Controls.Add(this.lblHeight);
            this.tabPage2.Controls.Add(this.lblWidth);
            this.tabPage2.Controls.Add(this.lblRows);
            this.tabPage2.Controls.Add(this.lblColumns);
            this.tabPage2.Controls.Add(this.lblError);
            this.tabPage2.Controls.Add(this.lblValue);
            this.tabPage2.Controls.Add(this.lblStyle);
            this.tabPage2.Controls.Add(this.lblInfo);
            this.tabPage2.Controls.Add(this.lblResult);
            this.tabPage2.Controls.Add(this.lblStatus);
            this.tabPage2.Controls.Add(this.buttonPrintBarcode2D);
            this.tabPage2.Controls.Add(this.buttonWriteBarcode2D);
            this.tabPage2.Controls.Add(this.buttonCreateBarcode2D);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(800, 518);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "2D Barcode Writing";
            // 
            // imageXView2
            // 
            this.imageXView2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.imageXView2.AutoScroll = true;
            this.imageXView2.Location = new System.Drawing.Point(328, 257);
            this.imageXView2.Name = "imageXView2";
            this.imageXView2.Size = new System.Drawing.Size(455, 198);
            this.imageXView2.TabIndex = 84;
            // 
            // cmbBarcodeType
            // 
            this.cmbBarcodeType.Items.AddRange(new object[] {
            "PDF417",
            "DataMatrix"});
            this.cmbBarcodeType.Location = new System.Drawing.Point(16, 40);
            this.cmbBarcodeType.Name = "cmbBarcodeType";
            this.cmbBarcodeType.Size = new System.Drawing.Size(312, 21);
            this.cmbBarcodeType.TabIndex = 83;
            this.cmbBarcodeType.Text = "PDF417";
            this.cmbBarcodeType.SelectedIndexChanged += new System.EventHandler(this.cmbBarcodeType_SelectedIndexChanged);
            // 
            // cmbColumnsAndRows
            // 
            this.cmbColumnsAndRows.Location = new System.Drawing.Point(448, 160);
            this.cmbColumnsAndRows.Name = "cmbColumnsAndRows";
            this.cmbColumnsAndRows.Size = new System.Drawing.Size(192, 21);
            this.cmbColumnsAndRows.TabIndex = 82;
            this.cmbColumnsAndRows.Text = "ComboBox1";
            this.cmbColumnsAndRows.Visible = false;
            // 
            // hscHeight
            // 
            this.hscHeight.LargeChange = 1;
            this.hscHeight.Location = new System.Drawing.Point(448, 224);
            this.hscHeight.Maximum = 5;
            this.hscHeight.Minimum = 3;
            this.hscHeight.Name = "hscHeight";
            this.hscHeight.Size = new System.Drawing.Size(192, 16);
            this.hscHeight.TabIndex = 81;
            this.hscHeight.Value = 4;
            this.hscHeight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscHeight_Scroll);
            // 
            // hscWidth
            // 
            this.hscWidth.LargeChange = 1;
            this.hscWidth.Location = new System.Drawing.Point(448, 200);
            this.hscWidth.Maximum = 10;
            this.hscWidth.Minimum = 2;
            this.hscWidth.Name = "hscWidth";
            this.hscWidth.Size = new System.Drawing.Size(192, 16);
            this.hscWidth.TabIndex = 80;
            this.hscWidth.Value = 3;
            this.hscWidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscWidth_Scroll);
            // 
            // hscRows
            // 
            this.hscRows.LargeChange = 1;
            this.hscRows.Location = new System.Drawing.Point(448, 152);
            this.hscRows.Maximum = 90;
            this.hscRows.Minimum = 2;
            this.hscRows.Name = "hscRows";
            this.hscRows.Size = new System.Drawing.Size(192, 16);
            this.hscRows.TabIndex = 79;
            this.hscRows.Value = 2;
            this.hscRows.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscRows_Scroll);
            // 
            // hscColumns
            // 
            this.hscColumns.LargeChange = 1;
            this.hscColumns.Location = new System.Drawing.Point(448, 176);
            this.hscColumns.Maximum = 30;
            this.hscColumns.Name = "hscColumns";
            this.hscColumns.Size = new System.Drawing.Size(192, 16);
            this.hscColumns.TabIndex = 78;
            this.hscColumns.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscColumns_Scroll);
            // 
            // hscError
            // 
            this.hscError.LargeChange = 1;
            this.hscError.Location = new System.Drawing.Point(448, 128);
            this.hscError.Maximum = 8;
            this.hscError.Minimum = -1;
            this.hscError.Name = "hscError";
            this.hscError.Size = new System.Drawing.Size(192, 16);
            this.hscError.TabIndex = 77;
            this.hscError.Value = -1;
            this.hscError.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscError_Scroll);
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(448, 104);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(192, 20);
            this.TextBox1.TabIndex = 76;
            this.TextBox1.Text = "PDF417";
            // 
            // lblHeightVal
            // 
            this.lblHeightVal.Location = new System.Drawing.Point(648, 224);
            this.lblHeightVal.Name = "lblHeightVal";
            this.lblHeightVal.Size = new System.Drawing.Size(88, 16);
            this.lblHeightVal.TabIndex = 75;
            this.lblHeightVal.Text = "4";
            // 
            // lblWidthVal
            // 
            this.lblWidthVal.Location = new System.Drawing.Point(648, 200);
            this.lblWidthVal.Name = "lblWidthVal";
            this.lblWidthVal.Size = new System.Drawing.Size(88, 16);
            this.lblWidthVal.TabIndex = 74;
            this.lblWidthVal.Text = "3";
            // 
            // lblRowsVal
            // 
            this.lblRowsVal.Location = new System.Drawing.Point(648, 152);
            this.lblRowsVal.Name = "lblRowsVal";
            this.lblRowsVal.Size = new System.Drawing.Size(88, 16);
            this.lblRowsVal.TabIndex = 73;
            this.lblRowsVal.Text = "-1";
            // 
            // lblColumnsVal
            // 
            this.lblColumnsVal.Location = new System.Drawing.Point(648, 176);
            this.lblColumnsVal.Name = "lblColumnsVal";
            this.lblColumnsVal.Size = new System.Drawing.Size(88, 16);
            this.lblColumnsVal.TabIndex = 72;
            this.lblColumnsVal.Text = "-1";
            // 
            // lblErrorVal
            // 
            this.lblErrorVal.Location = new System.Drawing.Point(648, 128);
            this.lblErrorVal.Name = "lblErrorVal";
            this.lblErrorVal.Size = new System.Drawing.Size(80, 16);
            this.lblErrorVal.TabIndex = 71;
            this.lblErrorVal.Text = "-1";
            // 
            // lblHeight
            // 
            this.lblHeight.Location = new System.Drawing.Point(352, 224);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(88, 16);
            this.lblHeight.TabIndex = 70;
            this.lblHeight.Text = "Height:";
            // 
            // lblWidth
            // 
            this.lblWidth.Location = new System.Drawing.Point(352, 200);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(88, 16);
            this.lblWidth.TabIndex = 69;
            this.lblWidth.Text = "Width:";
            // 
            // lblRows
            // 
            this.lblRows.Location = new System.Drawing.Point(352, 152);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(88, 16);
            this.lblRows.TabIndex = 68;
            this.lblRows.Text = "Rows:";
            // 
            // lblColumns
            // 
            this.lblColumns.Location = new System.Drawing.Point(352, 176);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(88, 16);
            this.lblColumns.TabIndex = 67;
            this.lblColumns.Text = "Columns:";
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(352, 128);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(88, 16);
            this.lblError.TabIndex = 66;
            this.lblError.Text = "Error Correction:";
            // 
            // lblValue
            // 
            this.lblValue.Location = new System.Drawing.Point(352, 104);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(88, 16);
            this.lblValue.TabIndex = 65;
            this.lblValue.Text = "Barcode Value:";
            // 
            // lblStyle
            // 
            this.lblStyle.Location = new System.Drawing.Point(8, 16);
            this.lblStyle.Name = "lblStyle";
            this.lblStyle.Size = new System.Drawing.Size(144, 16);
            this.lblStyle.TabIndex = 64;
            this.lblStyle.Text = "Barcode Style:";
            // 
            // lblInfo
            // 
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Location = new System.Drawing.Point(16, 72);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(304, 80);
            this.lblInfo.TabIndex = 63;
            this.lblInfo.Text = "PDF417 - Supports ASCII and binary characters.";
            // 
            // lblResult
            // 
            this.lblResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResult.Location = new System.Drawing.Point(16, 160);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(304, 296);
            this.lblResult.TabIndex = 62;
            // 
            // lblStatus
            // 
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblStatus.Location = new System.Drawing.Point(16, 464);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(768, 48);
            this.lblStatus.TabIndex = 61;
            // 
            // buttonPrintBarcode2D
            // 
            this.buttonPrintBarcode2D.Location = new System.Drawing.Point(656, 48);
            this.buttonPrintBarcode2D.Name = "buttonPrintBarcode2D";
            this.buttonPrintBarcode2D.Size = new System.Drawing.Size(128, 32);
            this.buttonPrintBarcode2D.TabIndex = 60;
            this.buttonPrintBarcode2D.Text = "Print Barcode";
            this.buttonPrintBarcode2D.Click += new System.EventHandler(this.buttonPrintBarcode2D_Click);
            // 
            // buttonWriteBarcode2D
            // 
            this.buttonWriteBarcode2D.Location = new System.Drawing.Point(504, 48);
            this.buttonWriteBarcode2D.Name = "buttonWriteBarcode2D";
            this.buttonWriteBarcode2D.Size = new System.Drawing.Size(128, 32);
            this.buttonWriteBarcode2D.TabIndex = 59;
            this.buttonWriteBarcode2D.Text = "Write To File";
            this.buttonWriteBarcode2D.Click += new System.EventHandler(this.buttonWriteBarcode2D_Click);
            // 
            // buttonCreateBarcode2D
            // 
            this.buttonCreateBarcode2D.Location = new System.Drawing.Point(352, 48);
            this.buttonCreateBarcode2D.Name = "buttonCreateBarcode2D";
            this.buttonCreateBarcode2D.Size = new System.Drawing.Size(128, 32);
            this.buttonCreateBarcode2D.TabIndex = 58;
            this.buttonCreateBarcode2D.Text = "Create Barcode";
            this.buttonCreateBarcode2D.Click += new System.EventHandler(this.buttonCreateBarcode2D_Click);
            // 
            // closeForm
            // 
            this.closeForm.Location = new System.Drawing.Point(16, 560);
            this.closeForm.Name = "closeForm";
            this.closeForm.Size = new System.Drawing.Size(104, 24);
            this.closeForm.TabIndex = 1;
            this.closeForm.Text = "Close";
            this.closeForm.Click += new System.EventHandler(this.closeForm_Click);
            // 
            // FormBarcodeWriting
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(840, 589);
            this.Controls.Add(this.closeForm);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormBarcodeWriting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Barcode Writing";
            this.Load += new System.EventHandler(this.FormBarcodeWriting_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void closeForm_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void comboBoxStyle_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			set1DStyle();
			labelResult.Text = "";
			imageXView1.Image = null;
			buttonPrint.Enabled = false;
			buttonWrite.Enabled = false;

		}

		private void set1DStyle()
		{
			switch (comboBoxStyle.SelectedIndex)
			{
				case 0:
				{
					labelInfo.Text = @"Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 1:
				{
					labelInfo.Text = @"Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39 Ext";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 2:
				{
					labelInfo.Text = @"Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 3:
				{
					labelInfo.Text = @"Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39 Ext";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 4:
				{
					labelInfo.Text = @"Industry 2 of 5 - Allowable characters - digits 0-9.";
					textBoxBCValue.Text = "1234321";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Industry2of5Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Industry2of5Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 5:
				{
					labelInfo.Text = @"Interleaved 2 of 5 - Allowable characters - digits 0-9.";
					textBoxBCValue.Text = "1234321";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Interleaved2of5Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Interleaved2of5Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 6:
				{
					labelInfo.Text = @"Codabar - Allowable characters - digits 0-9, six symbols including: minus (-), plus (+), period (.), dollar sign ($), slash (/), and colon (:), and the following start/stop characters A, B, C and D.";
					textBoxBCValue.Text = "C5467D";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.CodabarBarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.CodabarBarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 7:
				{
					labelInfo.Text = @"Code 128A - Allowable characters - Digits, uppercase characters, standard ASCII symbols.";
					textBoxBCValue.Text = "CODE 128A";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}
				case 8:
				{
					labelInfo.Text = @"Code 128B - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols.";
					textBoxBCValue.Text = "CODE 128B";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 9:
				{
					labelInfo.Text = @"Code 128C - Allowable characters - Digits 0-9.";
					textBoxBCValue.Text = "987654321";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 10:
				{
					labelInfo.Text = @"Code 128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols.";
					textBoxBCValue.Text = "CODE 128";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 11:
				{
					labelInfo.Text = @"UPC-A - Allowable characters - Digits 0-9; You must have 11 or 12 digits.";
					textBoxBCValue.Text = "69792911003";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCABarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCABarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 12:
				{
					labelInfo.Text = @"UPC-E - Allowable characters - Digits 0-9; You must have 6, 10, 11 or 12 digits.";
					textBoxBCValue.Text = "654123";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCEBarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCEBarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 13:
				{
					labelInfo.Text = @"EAN-8 - Allowable characters - Digits 0-9; You must have 7 or 8 digits.";
					textBoxBCValue.Text = "1234567";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN8Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN8Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 14:
				{
					labelInfo.Text = @"EAN-13 - Allowable characters - Digits 0-9; You must have 12 or 13 digits.";
					textBoxBCValue.Text = "123456765432";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN13Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN13Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 15:
				{
					labelInfo.Text = @"EAN-128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols.";
					textBoxBCValue.Text = "EAN-128";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN128Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN128Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 16:
				{
                    labelInfo.Text = @"Code 93 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 93";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93Barcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93Barcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 17:
				{
					labelInfo.Text = @"Code 93 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 93 Ext";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93ExtendedBarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93ExtendedBarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 18:
				{
					labelInfo.Text = @"Patch code - (1)=Patch I; (2)=Patch II; (3)=Patch III; (4)=Patch IV; (5)=Patch VI; (6)=Patch T.";
					if( hScrollBarMinBar.Value < 5 )
					{
						hScrollBarMinBar.Value = 5;
					}
					textBoxBCValue.Text = "1";
					barcodeXpress.writer.BarcodeType = PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode;
					System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
					currentBarcodeTypes.SetValue( PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode, 0 );
					barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}
			}
		}

			private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
			{
				set1DStyle();
			}

		private void FormBarcodeWriting_Load(object sender, System.EventArgs e)
		{        
			comboBoxStyle.Items.Add ("Code 39 Narrow");
			comboBoxStyle.Items.Add ("Code 39 Extended Narrow");
			comboBoxStyle.Items.Add ("Code 39 Wide");
			comboBoxStyle.Items.Add ("Code 39 Extended Wide");
			comboBoxStyle.Items.Add ("Industrial 2 of 5");
			comboBoxStyle.Items.Add ("Interleaved 2 of 5");
			comboBoxStyle.Items.Add ("Codabar Rationalized");
			comboBoxStyle.Items.Add ("Code 128A");
			comboBoxStyle.Items.Add ("Code 128B");
			comboBoxStyle.Items.Add ("Code 128C");
			comboBoxStyle.Items.Add ("Code 128");
			comboBoxStyle.Items.Add ("UPC-A");
			comboBoxStyle.Items.Add ("UPC-E");
			comboBoxStyle.Items.Add ("EAN-8");
			comboBoxStyle.Items.Add ("EAN-13");
			comboBoxStyle.Items.Add ("EAN-128");
			comboBoxStyle.Items.Add ("Code 93");
			comboBoxStyle.Items.Add ("Code 93 Extended");
			comboBoxStyle.Items.Add ("Patch Code");
			comboBoxStyle.SelectedIndex = 0;

			hScrollBarBCHeight.Value = barcodeXpress.writer.MinimumBarHeight;
			hScrollBarMinBar.Value = barcodeXpress.writer.MinimumBarWidth;
			hScrollBarUPCNotch.Value = barcodeXpress.writer.TextNotchPercent;
			hScrollBarValueGap.Value = barcodeXpress.writer.VerticalTextGap;
			hScrollBarUPCGap.Value = barcodeXpress.writer.HorizontalTextGap;
			hScrollBarValueFont.Value = (System.Int32) barcodeXpress.writer.TextFont.Size;
			hScrollBarUPCFont.Value = (System.Int32) barcodeXpress.writer.OutsideTextFont.Size;

			//Setup the values of the Data Matrix Combo Box
			cmbColumnsAndRows.Items.Add("Auto");
			cmbColumnsAndRows.Items[0] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.AutoRowsAndColumns;

			cmbColumnsAndRows.Items.Add("8 x 18");
			cmbColumnsAndRows.Items[1] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows8Columns18;

			cmbColumnsAndRows.Items.Add("8 x 32");
			cmbColumnsAndRows.Items[2] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows8Columns32;

			cmbColumnsAndRows.Items.Add("10 x 10");
			cmbColumnsAndRows.Items[3] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows10Columns10;

			cmbColumnsAndRows.Items.Add("12 x 12");
			cmbColumnsAndRows.Items[4] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns12;

			cmbColumnsAndRows.Items.Add("12 x 26");
			cmbColumnsAndRows.Items[5] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns26;

			cmbColumnsAndRows.Items.Add("12 x 36");
			cmbColumnsAndRows.Items[6] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns36;

			cmbColumnsAndRows.Items.Add("14 x 14");
			cmbColumnsAndRows.Items[7] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows14Columns14;

			cmbColumnsAndRows.Items.Add("16 x 16");
			cmbColumnsAndRows.Items[8] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns16;

			cmbColumnsAndRows.Items.Add("16 x 36");
			cmbColumnsAndRows.Items[9] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns36;

			cmbColumnsAndRows.Items.Add("16 x 48");
			cmbColumnsAndRows.Items[10] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns48;

			cmbColumnsAndRows.Items.Add("18 x 18");
			cmbColumnsAndRows.Items[11] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows18Columns18;

			cmbColumnsAndRows.Items.Add("20 x 20");
			cmbColumnsAndRows.Items[12] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows20Columns20;

			cmbColumnsAndRows.Items.Add("22 x 22");
			cmbColumnsAndRows.Items[13] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows22Columns22;

			cmbColumnsAndRows.Items.Add("24 x 24");
			cmbColumnsAndRows.Items[14] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows24Columns24;

			cmbColumnsAndRows.Items.Add("26 x 26");
			cmbColumnsAndRows.Items[15] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows26Columns26;

			cmbColumnsAndRows.Items.Add("32 x 32");
			cmbColumnsAndRows.Items[16] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows32Columns32;

			cmbColumnsAndRows.Items.Add("36 x 36");
			cmbColumnsAndRows.Items[17] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows36Columns36;

			cmbColumnsAndRows.Items.Add("40 x 40");
			cmbColumnsAndRows.Items[18] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows40Columns40;

			cmbColumnsAndRows.Items.Add("44 x 44");
			cmbColumnsAndRows.Items[19] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows44Columns44;

			cmbColumnsAndRows.Items.Add("48 x 48");
			cmbColumnsAndRows.Items[20] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows48Columns48;

			cmbColumnsAndRows.Items.Add("52 x 52");
			cmbColumnsAndRows.Items[21] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows52Columns52;

			cmbColumnsAndRows.Items.Add("64 x 64");
			cmbColumnsAndRows.Items[22] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows64Columns64;

			cmbColumnsAndRows.Items.Add("72 x 72");
			cmbColumnsAndRows.Items[23] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows72Columns72;

			cmbColumnsAndRows.Items.Add("80 x 80");
			cmbColumnsAndRows.Items[24] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows80Columns80;

			cmbColumnsAndRows.Items.Add("88 x 88");
			cmbColumnsAndRows.Items[25] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows88Columns88;

			cmbColumnsAndRows.Items.Add("96 x 96");
			cmbColumnsAndRows.Items[26] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows96Columns96;

			cmbColumnsAndRows.Items.Add("104 x 104");
			cmbColumnsAndRows.Items[27] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows104Columns104;

			cmbColumnsAndRows.Items.Add("120 x 120");
			cmbColumnsAndRows.Items[28] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows120Columns120;

			cmbColumnsAndRows.Items.Add("132 x 132");
			cmbColumnsAndRows.Items[29] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows132Columns132;

			cmbColumnsAndRows.Items.Add("144 x 144");
			cmbColumnsAndRows.Items[30] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows144Columns144;

			cmbColumnsAndRows.SelectedIndex = 0;

			lblColumnsVal.Text = "-1";
			lblRowsVal.Text = "-1";

			//Set the Barcode type combo box
			cmbBarcodeType.SelectedIndex = 0;

			buttonPrintBarcode2D.Enabled = false;
			buttonWriteBarcode2D.Enabled = false;

		}

		private void buttonCreate_Click(object sender, System.EventArgs e)
		{
			barcodeXpress.writer.BarcodeValue = textBoxBCValue.Text;
			labelStatus.Text = "";

			try
			{
                int DIB2 = barcodeXpress.writer.Create();

				imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(new IntPtr(DIB2));

				// read the barcode
				System.Drawing.Rectangle currentArea = new System.Drawing.Rectangle(0, 0, 0, 0);
				barcodeXpress.reader.Area = currentArea;

                int DIB = imageXView1.Image.ToHdib(false).ToInt32();
                results = barcodeXpress.reader.Analyze(DIB);

				if (results.Length > 0 )
				{
					string strResult = results.Length + " barcode(s) found.\n\n";
					for (int i = 0; i < results.Length; i++)
					{
						PegasusImaging.WinForms.BarcodeXpress5.Result curResult = (PegasusImaging.WinForms.BarcodeXpress5.Result)results.GetValue(i);;
						strResult += "Symbol #" + i + "\nType: " + curResult.BarcodeName + "\n";
						strResult += "X - " + curResult.Area.X + " pixels\nY - " + curResult.Area.Y + " pixels\n";
						strResult += "W - " + curResult.Area.Width + " pixels\nH - " + curResult.Area.Height + " pixels\n";
						strResult += "Value:\n" + curResult.BarcodeValue + "\n\n";
						if(curResult.ValidCheckSum)
						{
							strResult += "Checksum OK";
						}
					}

					strResult += "\n";
					labelResult.Text = strResult;
					labelStatus.Text = "Barcode detection was successful.";
					buttonPrint.Enabled = true;
					buttonWrite.Enabled = true;
				}
				else
				{
					labelStatus.Text = "Could not detect any barcodes.";
					buttonPrint.Enabled = false;
					buttonWrite.Enabled = false;
				}

                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB));
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB2));
			}
			catch( PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex )
			{
				string strResult;
				strResult = "Error Creating Barcode: ";
				strResult += ex.Message;
				labelResult.Text = strResult;
				labelStatus.Text = strResult;
			}
		}

		private void buttonWrite_Click(object sender, System.EventArgs e)
		{
            System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();

            dlg.Filter = "TIF (*.TIF)|.TIF";
            dlg.FilterIndex = 0;
            dlg.ShowDialog();

            string strMsg;

            try
            {
                //string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
                //string strImageFile = System.IO.Path.Combine(strCurrentDir, @"..\..\barcode.TIF");

                if (dlg.FileName != "")
                {
                    savedImageFileName = dlg.FileName;
                    PegasusImaging.WinForms.ImagXpress8.SaveOptions so = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
                    so.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff;
                    so.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4;

                    imageXView1.Image.Save(dlg.FileName, so);

                    strMsg = "Image saved as " + dlg.FileName;//strImageFile;
                    labelStatus.Text = strMsg;
                }

            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                strMsg = "Error Writing To File.  ImagXpress Error: ";
                strMsg += ex.Message;
                labelStatus.Text = strMsg;
            }
		}

		private void buttonPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// Print the Image
				PrintDocument doc = new PrintDocument ();
				doc.PrintPage += new PrintPageEventHandler (this.OnPrintPage);
				doc.Print ();
			}
			catch( Exception ex )
			{
				labelStatus.Text = ex.Message;
			}
		}

		private void OnPrintPage (Object sender, PrintPageEventArgs args)
		{
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(savedImageFileName);

            PegasusImaging.WinForms.ImagXpress8.Processor proc = new PegasusImaging.WinForms.ImagXpress8.Processor();

            proc.Image = imageXView1.Image.Copy();
            proc.ColorDepth(24, 0, 0);
            imageXView1.Image = proc.Image;

            System.Drawing.Graphics g = imageXView1.Image.GetGraphics();
            System.Drawing.Image img = (System.Drawing.Image)imageXView1.Image.ToBitmap(false);
            args.Graphics.PageUnit = GraphicsUnit.Inch;

            args.Graphics.DrawImage(img, (Single)((8.5 - img.Width / img.HorizontalResolution) / 2), (Single)((11 - img.Height / img.VerticalResolution) / 2), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution);

            imageXView1.Image.ReleaseGraphics();
		}

		private void hScrollBarBCHeight_ValueChanged(object sender, System.EventArgs e)
		{
			labelHeightVal.Text = hScrollBarBCHeight.Value.ToString ();
			barcodeXpress.writer.MinimumBarHeight = hScrollBarBCHeight.Value;
		}

		private void hScrollBarMinBar_ValueChanged(object sender, System.EventArgs e)
		{
			labelMinBarVal.Text = hScrollBarMinBar.Value.ToString ();
			barcodeXpress.writer.MinimumBarWidth = (short) hScrollBarMinBar.Value;
		}

		private void hScrollBarUPCNotch_ValueChanged(object sender, System.EventArgs e)
		{
			labelUPCNotchVal.Text = hScrollBarUPCNotch.Value.ToString ();
			barcodeXpress.writer.TextNotchPercent = hScrollBarUPCNotch.Value;
		}

		private void hScrollBarValueGap_ValueChanged(object sender, System.EventArgs e)
		{
			labelValueGapVal.Text = hScrollBarValueGap.Value.ToString ();
			barcodeXpress.writer.VerticalTextGap = hScrollBarValueGap.Value;
		}

		private void hScrollBarUPCGap_ValueChanged(object sender, System.EventArgs e)
		{
			labelUPCGapVal.Text = hScrollBarUPCGap.Value.ToString ();
			barcodeXpress.writer.HorizontalTextGap = hScrollBarUPCGap.Value;
		}

		private void hScrollBarValueFont_ValueChanged(object sender, System.EventArgs e)
		{
			labelValueFontVal.Text = hScrollBarValueFont.Value.ToString ();
			System.Drawing.Font font = barcodeXpress.writer.TextFont;
			barcodeXpress.writer.TextFont = new System.Drawing.Font (font.FontFamily.GetName(0), hScrollBarValueFont.Value);
		}

		private void hScrollBarUPCFont_ValueChanged(object sender, System.EventArgs e)
		{
			this.labelUPCFontVal.Text = hScrollBarUPCFont.Value.ToString ();

			System.Drawing.Font font = barcodeXpress.writer.OutsideTextFont;
			barcodeXpress.writer.OutsideTextFont = new System.Drawing.Font (font.FontFamily.GetName(0), hScrollBarUPCFont.Value);
		}

		private void buttonCreateBarcode2D_Click(object sender, System.EventArgs e)
		{
			lblStatus.Text = "";

			try
			{

				//Set the rows columns and Height
				if (cmbBarcodeType.SelectedIndex == 0)
				{
					UpdatePDF();
				}
				else
				{
					UpdateDM();
				}

				// read the barcode
				System.Drawing.Rectangle currentArea = new System.Drawing.Rectangle(0, 0, 0, 0);
				barcodeXpress.reader.Area = currentArea;

                int DIB = imageXView2.Image.ToHdib(false).ToInt32();
                results = barcodeXpress.reader.Analyze(DIB);

				if (results.Length > 0 )
				{
					string strResult = results.Length + " barcode(s) found.\n\n";
					for (int i = 0; i < results.Length; i++)
					{
						PegasusImaging.WinForms.BarcodeXpress5.Result curResult = (PegasusImaging.WinForms.BarcodeXpress5.Result)results.GetValue(i);;
						strResult += "Symbol #" + i + "\nType: " + curResult.BarcodeName + "\n";
						strResult += "X - " + curResult.Area.X + " pixels\nY - " + curResult.Area.Y + " pixels\n";
						strResult += "W - " + curResult.Area.Width + " pixels\nH - " + curResult.Area.Height + " pixels\n";
						strResult += "Value:\n" + curResult.BarcodeValue + "\n\n";
						if(curResult.ValidCheckSum)
						{
							strResult += "Checksum OK";
						}
					}

					strResult += "\n";
					lblResult.Text = strResult;
					lblStatus.Text = "Barcode detection was successful.";
					buttonPrintBarcode2D.Enabled = true;
					buttonWriteBarcode2D.Enabled = true;
				}
				else
				{
					lblStatus.Text = "Could not detect any barcodes.";
					buttonPrintBarcode2D.Enabled = false;
					buttonWriteBarcode2D.Enabled = false;
				}
                System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB));
			}
			catch(PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
			{
				string strResult;
				strResult = "Error Creating Barcode: ";
				strResult += ex.Message;
				lblResult.Text = strResult;
				lblStatus.Text = strResult;

			}
		}

		private void buttonWriteBarcode2D_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();

			dlg.Filter = "TIF (*.TIF)|.TIF";
			dlg.FilterIndex = 0;
			dlg.ShowDialog();

			string strMsg;

            try
            {
                if (dlg.FileName != "")
                {
                    savedImageFileName = dlg.FileName;
                    PegasusImaging.WinForms.ImagXpress8.SaveOptions so = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
                    so.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff;
                    so.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4;

                    imageXView2.Image.Save(dlg.FileName, so);

                    strMsg = "Image saved as " + dlg.FileName;//strImageFile;
                    labelStatus.Text = strMsg;
                }

            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                strMsg = "Error Writing To File.  ImagXpress Error: ";
                strMsg += ex.Message;
            }
		}

		private void buttonPrintBarcode2D_Click(object sender, System.EventArgs e)
		{
			try
			{
				// Print the Image
				PrintDocument doc = new PrintDocument ();
				doc.PrintPage += new PrintPageEventHandler (this.OnPrintPage);
				doc.Print ();
				lblStatus.Text = "Barcode printed.";
			}
			catch( Exception ex )
			{
				lblStatus.Text = ex.Message;
			}
		}

		private void OnPrint2DPage (Object sender, PrintPageEventArgs args)
		{
            imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(savedImageFileName);

            PegasusImaging.WinForms.ImagXpress8.Processor proc = new PegasusImaging.WinForms.ImagXpress8.Processor();

            proc.Image = imageXView1.Image.Copy();
            proc.ColorDepth(24, 0, 0);
            imageXView2.Image = proc.Image;

            System.Drawing.Graphics g = imageXView1.Image.GetGraphics();
            System.Drawing.Image img = (System.Drawing.Image)imageXView2.Image.ToBitmap(false);
            args.Graphics.PageUnit = GraphicsUnit.Inch;

            args.Graphics.DrawImage(img, (Single)((8.5 - img.Width / img.HorizontalResolution) / 2), (Single)((11 - img.Height / img.VerticalResolution) / 2), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution);

            imageXView2.Image.ReleaseGraphics();
		}

		private void hscError_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblErrorVal.Text = hscError.Value.ToString();
		}

		private void hscColumns_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{

			if(hscColumns.Value < 1)
			{
				lblColumnsVal.Text = Convert.ToString(-1);
			}
			else
			{
				lblColumnsVal.Text = hscColumns.Value.ToString();
			}

		}

		private void hscRows_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			if(hscRows.Value < 3)
			{
				lblRowsVal.Text = Convert.ToString(-1);
			}
			else
			{
				lblRowsVal.Text = hscRows.Value.ToString();
			}
		}

		private void hscWidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblWidthVal.Text = hscWidth.Value.ToString();
		}

		private void hscHeight_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblHeightVal.Text = hscHeight.Value.ToString();
		}

		private void cmbBarcodeType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbBarcodeType.SelectedIndex == 0)
			{
				SetPDF417RowsAndColumns();
				lblInfo.Text = "PDF417 - Supports ASCII and binary characters.";
				TextBox1.Text = "PDF417";
                lblHeightVal.Visible = true;
                hscHeight.Visible = true;
                lblHeight.Visible = true;
                lblError.Visible = true;
                lblErrorVal.Visible = true;
                hscError.Visible = true;
			}
			else
			{
				SetDataMatrixRowsAndColumns();
				lblInfo.Text = "DataMatrix - Supports ASCII and binary characters.";
				TextBox1.Text = "DataMatrix";
                lblHeightVal.Visible = false;
                hscHeight.Visible = false;
                lblHeight.Visible = false;
                lblError.Visible = false;
                lblErrorVal.Visible = false;
                hscError.Visible = false;
			}

			lblResult.Text = "";
            imageXView2.Image = null;
			buttonPrintBarcode2D.Enabled = false;
			buttonWriteBarcode2D.Enabled = false;
		}

		private void SetPDF417RowsAndColumns()
		{

			cmbColumnsAndRows.Visible = false;
			hscColumns.Visible = true;
			hscRows.Visible = true;
			if(hscColumns.Value < 1)
			{
				lblColumnsVal.Text = "-1";
			}
			else
			{
				lblColumnsVal.Text = hscColumns.Value.ToString();
			}
			if(hscRows.Value < 3)
			{
				lblRowsVal.Text = "-1";
			}
			else
			{
				lblRowsVal.Text = hscRows.Value.ToString();
			}
			lblColumnsVal.Visible = true;
			lblRowsVal.Visible = true;

		}

		private void SetDataMatrixRowsAndColumns()
		{

			cmbColumnsAndRows.Visible = true;
			hscColumns.Visible = false;
			hscRows.Visible = false;
			lblColumnsVal.Visible = false;
			lblRowsVal.Visible = false;

		}

		private void UpdatePDF()
		{

			System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
			currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, 0);
			barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;

			PegasusImaging.WinForms.BarcodeXpress5.WriterPDF417 pdfWriter = new PegasusImaging.WinForms.BarcodeXpress5.WriterPDF417(barcodeXpress);
			if(hscRows.Value  < 3)
			{
				pdfWriter.Rows = -1;
			}
			else
			{
				pdfWriter.Rows = hscRows.Value;
			}
			if(hscColumns.Value < 1)
			{
				pdfWriter.Columns = -1;
			}
			else
			{
				pdfWriter.Columns = hscColumns.Value;
			}
			pdfWriter.ErrorCorrectionLevel = (PegasusImaging.WinForms.BarcodeXpress5.PDF417ErrorCorrectionLevel)(hscError.Value);
			pdfWriter.MinimumBarWidth = hscWidth.Value;
			pdfWriter.MinimumBarHeight = hscHeight.Value;
			pdfWriter.AutoSize = true;
			pdfWriter.BarcodeValue = TextBox1.Text;

			lblStatus.Text = "Making Barcode";
			lblResult.Text = "";
			Application.DoEvents();

			imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(new IntPtr(pdfWriter.Create()));

		}

		private void UpdateDM()
		{
			System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
			currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, 0);
			barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes;

			PegasusImaging.WinForms.BarcodeXpress5.WriterDataMatrix dmWriter = new PegasusImaging.WinForms.BarcodeXpress5.WriterDataMatrix(barcodeXpress);
			dmWriter.RowsAndColumns = (PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes)(cmbColumnsAndRows.Items[cmbColumnsAndRows.SelectedIndex]);
			dmWriter.MinimumBarWidth = Convert.ToInt32(lblWidthVal.Text);
			dmWriter.MinimumBarHeight = Convert.ToInt32(lblHeightVal.Text);
			dmWriter.BarcodeValue = TextBox1.Text;
			dmWriter.AutoSize = true;

			lblStatus.Text = "Making Barcode";
			lblResult.Text = "";
			Application.DoEvents();

			imageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(new IntPtr(dmWriter.Create()));
        }


	}


}

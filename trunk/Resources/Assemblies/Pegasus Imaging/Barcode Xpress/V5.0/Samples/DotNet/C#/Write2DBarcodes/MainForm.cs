/****************************************************************
 * Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Drawing.Printing;
using System.Runtime.InteropServices;

namespace Write2DBarcodes
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		internal System.Windows.Forms.ComboBox cmbBarcodeType;
		internal System.Windows.Forms.ComboBox cmbColumnsAndRows;
		internal System.Windows.Forms.HScrollBar hscHeight;
		internal System.Windows.Forms.HScrollBar hscWidth;
		internal System.Windows.Forms.HScrollBar hscRows;
		internal System.Windows.Forms.HScrollBar hscColumns;
		internal System.Windows.Forms.HScrollBar hscError;
		internal System.Windows.Forms.TextBox TextBox1;
		internal System.Windows.Forms.Label lblHeightVal;
		internal System.Windows.Forms.Label lblWidthVal;
		internal System.Windows.Forms.Label lblRowsVal;
		internal System.Windows.Forms.Label lblColumnsVal;
		internal System.Windows.Forms.Label lblErrorVal;
		internal System.Windows.Forms.Label lblHeight;
		internal System.Windows.Forms.Label lblWidth;
		internal System.Windows.Forms.Label lblRows;
		internal System.Windows.Forms.Label lblColumns;
		internal System.Windows.Forms.Label lblError;
		internal System.Windows.Forms.Label lblValue;
		internal System.Windows.Forms.Label lblStyle;
		internal System.Windows.Forms.Label lblInfo;
		internal System.Windows.Forms.Label lblResult;
		internal System.Windows.Forms.Label lblStatus;
		internal System.Windows.Forms.ListBox ListBox1;
		internal System.Windows.Forms.Button buttonPrint;
		internal System.Windows.Forms.Button buttonWrite;
		internal System.Windows.Forms.Button buttonCreate;
		internal System.Windows.Forms.MainMenu MainMenu;
		internal System.Windows.Forms.MenuItem FileMenu;
		internal System.Windows.Forms.MenuItem ExitMenuItem;
		internal System.Windows.Forms.MenuItem AboutMenu;
        internal System.Windows.Forms.MenuItem AboutMenuItem;
        private PegasusImaging.WinForms.BarcodeXpress5.Result[] results;
        private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
        private PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress barcodeXpress1;
        private MenuItem ImagXpressMenuItem;
        private IContainer components;

        private string savedImageFileName;

		public MainForm()
		{


            //***Must call the UnlockRuntime method to unlock the control
	    //The unlock codes shown below are for formatting purposes only and will not work!
            //PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234);


			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }
                if (imageXView1 != null)
                {
                    imageXView1.Dispose();
                    imageXView1 = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (barcodeXpress1 != null)
                {
                    barcodeXpress1.Dispose();
                    barcodeXpress1 = null;
                }
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbBarcodeType = new System.Windows.Forms.ComboBox();
            this.cmbColumnsAndRows = new System.Windows.Forms.ComboBox();
            this.hscHeight = new System.Windows.Forms.HScrollBar();
            this.hscWidth = new System.Windows.Forms.HScrollBar();
            this.hscRows = new System.Windows.Forms.HScrollBar();
            this.hscColumns = new System.Windows.Forms.HScrollBar();
            this.hscError = new System.Windows.Forms.HScrollBar();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.lblHeightVal = new System.Windows.Forms.Label();
            this.lblWidthVal = new System.Windows.Forms.Label();
            this.lblRowsVal = new System.Windows.Forms.Label();
            this.lblColumnsVal = new System.Windows.Forms.Label();
            this.lblErrorVal = new System.Windows.Forms.Label();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblWidth = new System.Windows.Forms.Label();
            this.lblRows = new System.Windows.Forms.Label();
            this.lblColumns = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.lblStyle = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.ListBox1 = new System.Windows.Forms.ListBox();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonWrite = new System.Windows.Forms.Button();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.AboutMenuItem = new System.Windows.Forms.MenuItem();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.barcodeXpress1 = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // cmbBarcodeType
            // 
            this.cmbBarcodeType.Items.AddRange(new object[] {
            "PDF417",
            "DataMatrix"});
            this.cmbBarcodeType.Location = new System.Drawing.Point(24, 112);
            this.cmbBarcodeType.Name = "cmbBarcodeType";
            this.cmbBarcodeType.Size = new System.Drawing.Size(312, 21);
            this.cmbBarcodeType.TabIndex = 56;
            this.cmbBarcodeType.Text = "PDF417";
            this.cmbBarcodeType.SelectedIndexChanged += new System.EventHandler(this.cmbBarcodeType_SelectedIndexChanged);
            // 
            // cmbColumnsAndRows
            // 
            this.cmbColumnsAndRows.Location = new System.Drawing.Point(456, 232);
            this.cmbColumnsAndRows.Name = "cmbColumnsAndRows";
            this.cmbColumnsAndRows.Size = new System.Drawing.Size(192, 21);
            this.cmbColumnsAndRows.TabIndex = 55;
            this.cmbColumnsAndRows.Text = "ComboBox1";
            this.cmbColumnsAndRows.Visible = false;
            // 
            // hscHeight
            // 
            this.hscHeight.LargeChange = 1;
            this.hscHeight.Location = new System.Drawing.Point(456, 296);
            this.hscHeight.Maximum = 5;
            this.hscHeight.Minimum = 3;
            this.hscHeight.Name = "hscHeight";
            this.hscHeight.Size = new System.Drawing.Size(192, 16);
            this.hscHeight.TabIndex = 54;
            this.hscHeight.Value = 4;
            this.hscHeight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscHeight_Scroll);
            // 
            // hscWidth
            // 
            this.hscWidth.LargeChange = 1;
            this.hscWidth.Location = new System.Drawing.Point(456, 272);
            this.hscWidth.Maximum = 10;
            this.hscWidth.Minimum = 2;
            this.hscWidth.Name = "hscWidth";
            this.hscWidth.Size = new System.Drawing.Size(192, 16);
            this.hscWidth.TabIndex = 53;
            this.hscWidth.Value = 3;
            this.hscWidth.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscWidth_Scroll);
            // 
            // hscRows
            // 
            this.hscRows.LargeChange = 1;
            this.hscRows.Location = new System.Drawing.Point(456, 224);
            this.hscRows.Maximum = 90;
            this.hscRows.Minimum = 2;
            this.hscRows.Name = "hscRows";
            this.hscRows.Size = new System.Drawing.Size(192, 16);
            this.hscRows.TabIndex = 52;
            this.hscRows.Value = 2;
            this.hscRows.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscRows_Scroll);
            // 
            // hscColumns
            // 
            this.hscColumns.LargeChange = 1;
            this.hscColumns.Location = new System.Drawing.Point(456, 248);
            this.hscColumns.Maximum = 30;
            this.hscColumns.Name = "hscColumns";
            this.hscColumns.Size = new System.Drawing.Size(192, 16);
            this.hscColumns.TabIndex = 51;
            this.hscColumns.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscColumns_Scroll);
            // 
            // hscError
            // 
            this.hscError.LargeChange = 1;
            this.hscError.Location = new System.Drawing.Point(456, 200);
            this.hscError.Maximum = 8;
            this.hscError.Minimum = -1;
            this.hscError.Name = "hscError";
            this.hscError.Size = new System.Drawing.Size(192, 16);
            this.hscError.TabIndex = 50;
            this.hscError.Value = -1;
            this.hscError.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hscError_Scroll);
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(456, 176);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(192, 20);
            this.TextBox1.TabIndex = 49;
            this.TextBox1.Text = "PDF417";
            // 
            // lblHeightVal
            // 
            this.lblHeightVal.Location = new System.Drawing.Point(656, 296);
            this.lblHeightVal.Name = "lblHeightVal";
            this.lblHeightVal.Size = new System.Drawing.Size(88, 16);
            this.lblHeightVal.TabIndex = 48;
            this.lblHeightVal.Text = "4";
            // 
            // lblWidthVal
            // 
            this.lblWidthVal.Location = new System.Drawing.Point(656, 272);
            this.lblWidthVal.Name = "lblWidthVal";
            this.lblWidthVal.Size = new System.Drawing.Size(88, 16);
            this.lblWidthVal.TabIndex = 47;
            this.lblWidthVal.Text = "3";
            // 
            // lblRowsVal
            // 
            this.lblRowsVal.Location = new System.Drawing.Point(656, 224);
            this.lblRowsVal.Name = "lblRowsVal";
            this.lblRowsVal.Size = new System.Drawing.Size(88, 16);
            this.lblRowsVal.TabIndex = 46;
            this.lblRowsVal.Text = "-1";
            // 
            // lblColumnsVal
            // 
            this.lblColumnsVal.Location = new System.Drawing.Point(656, 248);
            this.lblColumnsVal.Name = "lblColumnsVal";
            this.lblColumnsVal.Size = new System.Drawing.Size(88, 16);
            this.lblColumnsVal.TabIndex = 45;
            this.lblColumnsVal.Text = "-1";
            // 
            // lblErrorVal
            // 
            this.lblErrorVal.Location = new System.Drawing.Point(656, 200);
            this.lblErrorVal.Name = "lblErrorVal";
            this.lblErrorVal.Size = new System.Drawing.Size(80, 16);
            this.lblErrorVal.TabIndex = 44;
            this.lblErrorVal.Text = "-1";
            // 
            // lblHeight
            // 
            this.lblHeight.Location = new System.Drawing.Point(360, 296);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(88, 16);
            this.lblHeight.TabIndex = 43;
            this.lblHeight.Text = "Height:";
            // 
            // lblWidth
            // 
            this.lblWidth.Location = new System.Drawing.Point(360, 272);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(88, 16);
            this.lblWidth.TabIndex = 42;
            this.lblWidth.Text = "Width:";
            // 
            // lblRows
            // 
            this.lblRows.Location = new System.Drawing.Point(360, 224);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(88, 16);
            this.lblRows.TabIndex = 41;
            this.lblRows.Text = "Rows:";
            // 
            // lblColumns
            // 
            this.lblColumns.Location = new System.Drawing.Point(360, 248);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(88, 16);
            this.lblColumns.TabIndex = 40;
            this.lblColumns.Text = "Columns:";
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(360, 200);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(88, 16);
            this.lblError.TabIndex = 39;
            this.lblError.Text = "Error Correction:";
            // 
            // lblValue
            // 
            this.lblValue.Location = new System.Drawing.Point(360, 176);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(88, 16);
            this.lblValue.TabIndex = 38;
            this.lblValue.Text = "Barcode Value:";
            // 
            // lblStyle
            // 
            this.lblStyle.Location = new System.Drawing.Point(16, 88);
            this.lblStyle.Name = "lblStyle";
            this.lblStyle.Size = new System.Drawing.Size(144, 16);
            this.lblStyle.TabIndex = 37;
            this.lblStyle.Text = "Barcode Style:";
            // 
            // lblInfo
            // 
            this.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblInfo.Location = new System.Drawing.Point(24, 144);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(312, 80);
            this.lblInfo.TabIndex = 36;
            this.lblInfo.Text = "PDF417 - Supports ASCII and binary characters.";
            // 
            // lblResult
            // 
            this.lblResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResult.Location = new System.Drawing.Point(24, 232);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(312, 392);
            this.lblResult.TabIndex = 35;
            // 
            // lblStatus
            // 
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblStatus.Location = new System.Drawing.Point(24, 632);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(768, 40);
            this.lblStatus.TabIndex = 34;
            // 
            // ListBox1
            // 
            this.ListBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListBox1.Items.AddRange(new object[] {
            "This sample demonstrates creating 2D Barcodes with the toolkit."});
            this.ListBox1.Location = new System.Drawing.Point(24, 8);
            this.ListBox1.Name = "ListBox1";
            this.ListBox1.Size = new System.Drawing.Size(768, 69);
            this.ListBox1.TabIndex = 33;
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(664, 120);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(128, 32);
            this.buttonPrint.TabIndex = 32;
            this.buttonPrint.Text = "Print Barcode";
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonWrite
            // 
            this.buttonWrite.Location = new System.Drawing.Point(512, 120);
            this.buttonWrite.Name = "buttonWrite";
            this.buttonWrite.Size = new System.Drawing.Size(128, 32);
            this.buttonWrite.TabIndex = 31;
            this.buttonWrite.Text = "Write To File";
            this.buttonWrite.Click += new System.EventHandler(this.buttonWrite_Click);
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(360, 120);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(128, 32);
            this.buttonCreate.TabIndex = 30;
            this.buttonCreate.Text = "Create Barcode";
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 0;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 1;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.AboutMenuItem,
            this.ImagXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // AboutMenuItem
            // 
            this.AboutMenuItem.Index = 0;
            this.AboutMenuItem.Text = "&BarcodeXpress";
            this.AboutMenuItem.Click += new System.EventHandler(this.AboutMenuItem_Click);
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(349, 323);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(455, 289);
            this.imageXView1.TabIndex = 57;
            // 
            // barcodeXpress1
            // 
            this.barcodeXpress1.Debug = false;
            this.barcodeXpress1.DebugLogFile = "C:\\Documents and Settings\\jargento.JPG.COM\\My Documents\\BarcodeXpress5.log";
            this.barcodeXpress1.ErrorLevel = PegasusImaging.WinForms.BarcodeXpress5.ErrorLevelInfo.Production;
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 1;
            this.ImagXpressMenuItem.Text = "&ImagXpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(816, 683);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.cmbBarcodeType);
            this.Controls.Add(this.cmbColumnsAndRows);
            this.Controls.Add(this.hscHeight);
            this.Controls.Add(this.hscWidth);
            this.Controls.Add(this.hscRows);
            this.Controls.Add(this.hscColumns);
            this.Controls.Add(this.hscError);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.lblHeightVal);
            this.Controls.Add(this.lblWidthVal);
            this.Controls.Add(this.lblRowsVal);
            this.Controls.Add(this.lblColumnsVal);
            this.Controls.Add(this.lblErrorVal);
            this.Controls.Add(this.lblHeight);
            this.Controls.Add(this.lblWidth);
            this.Controls.Add(this.lblRows);
            this.Controls.Add(this.lblColumns);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblStyle);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.ListBox1);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.buttonWrite);
            this.Controls.Add(this.buttonCreate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Write 2D Barcodes";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
        }

		private void buttonWrite_Click(object sender, System.EventArgs e)
		{

			System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();
			dlg.Filter = "TIF (*.TIF)|.TIF";
			dlg.FilterIndex = 0;
			dlg.ShowDialog();

			string strMsg;

            try
            {
                //string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
                //string strImageFile = System.IO.Path.Combine(strCurrentDir, @"..\..\barcode.TIF");

                if (dlg.FileName != "")
                {
                    savedImageFileName = dlg.FileName;
                    PegasusImaging.WinForms.ImagXpress8.SaveOptions so = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
                    so.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff;
                    so.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4;

                    imageXView1.Image.Save(dlg.FileName, so);

                    strMsg = "Image saved as " + dlg.FileName;//strImageFile;
                    lblStatus.Text = strMsg;
                }

            }
            catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex)
            {
                strMsg = "Error Writing To File.  ImagXpress Error: ";
                strMsg += ex.Message;
                lblStatus.Text = strMsg;
            }
		}

		private void buttonPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// Print the Image
				PrintDocument doc = new PrintDocument ();
				doc.PrintPage += new PrintPageEventHandler (this.OnPrintPage);
				doc.Print ();
				lblStatus.Text = "Barcode printed.";
			}
			catch( Exception ex )
			{
				lblStatus.Text = ex.Message;
			}
		}

		private void OnPrintPage (Object sender, PrintPageEventArgs args)
		{
            imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(savedImageFileName);

            PegasusImaging.WinForms.ImagXpress8.Processor proc = new PegasusImaging.WinForms.ImagXpress8.Processor();

            proc.Image = imageXView1.Image.Copy();
            proc.ColorDepth(24, 0, 0);
            imageXView1.Image = proc.Image;

            System.Drawing.Graphics g = imageXView1.Image.GetGraphics();
            System.Drawing.Image img = (System.Drawing.Image)imageXView1.Image.ToBitmap(false);
            args.Graphics.PageUnit = GraphicsUnit.Inch;

            args.Graphics.DrawImage(img, (Single)((8.5 - img.Width / img.HorizontalResolution) / 2), (Single)((11 - img.Height / img.VerticalResolution) / 2), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution);

            imageXView1.Image.ReleaseGraphics();
		}

		private void buttonCreate_Click(object sender, System.EventArgs e)
		{
			lblStatus.Text = "";
            System.Int32 DIB;
			try
			{

				//Set the rows columns and Height
				if (cmbBarcodeType.SelectedIndex == 0)
				{
					UpdatePDF();
				}
				else
				{
					UpdateDM();
				}

				// read the barcode
				System.Drawing.Rectangle currentArea = new System.Drawing.Rectangle(0, 0, 0, 0);
                DIB = imageXView1.Image.ToHdib(false).ToInt32();
				barcodeXpress1.reader.Area = currentArea;
                results = barcodeXpress1.reader.Analyze(imageXView1.Image.ToHdib(false).ToInt32());

                
				if (results.Length > 0 )
				{
					string strResult = results.Length + " barcode(s) found.\n\n";
					for (int i = 0; i < results.Length; i++)
					{
						PegasusImaging.WinForms.BarcodeXpress5.Result curResult = (PegasusImaging.WinForms.BarcodeXpress5.Result)results.GetValue(i);;
						strResult += "Symbol #" + i + "\nType: " + curResult.BarcodeName + "\n";
						strResult += "X - " + curResult.Area.X + " pixels\nY - " + curResult.Area.Y + " pixels\n";
						strResult += "W - " + curResult.Area.Width + " pixels\nH - " + curResult.Area.Height + " pixels\n";
						strResult += "Value:\n" + curResult.BarcodeValue + "\n\n";
						if(curResult.ValidCheckSum)
						{
							strResult += "Checksum OK";
						}
					}

					strResult += "\n";
					lblResult.Text = strResult;
					lblStatus.Text = "Barcode detection was successful.";
					buttonPrint.Enabled = true;
					buttonWrite.Enabled = true;
				}
				else
				{
					lblStatus.Text = "Could not detect any barcodes.";
					buttonPrint.Enabled = false;
					buttonWrite.Enabled = false;
				}


              //Must free the value passed to the Analyze method
              Marshal.FreeHGlobal(new System.IntPtr(DIB));
			}
			catch(PegasusImaging.WinForms.BarcodeXpress5.BarcodeException ex)
			{
				string strResult;
				strResult = "Error Creating Barcode: ";
				strResult += ex.Message;
				lblResult.Text = strResult;
				lblStatus.Text = strResult;

			}


		}

		private void hscError_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblErrorVal.Text = hscError.Value.ToString();
		}

		private void hscColumns_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblColumnsVal.Text = hscColumns.Value.ToString();

			if(hscColumns.Value < 1)
			{
				lblColumnsVal.Text = "-1";
			}
			else
			{
				lblColumnsVal.Text = hscColumns.Value.ToString();
			}

		}

		private void hscRows_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			if(hscRows.Value < 3)
			{
				lblRowsVal.Text = "-1";
			}
			else
			{
				lblRowsVal.Text = hscRows.Value.ToString();
			}
		}

		private void hscWidth_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblWidthVal.Text = hscWidth.Value.ToString();
		}

		private void hscHeight_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			lblHeightVal.Text = hscHeight.Value.ToString();
		}

		private void cmbBarcodeType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		    if (cmbBarcodeType.SelectedIndex == 0)
			{
				SetPDF417RowsAndColumns();
				lblInfo.Text = "PDF417 - Supports ASCII and binary characters.";
				TextBox1.Text = "PDF417";
                lblHeightVal.Visible = true;
                hscHeight.Visible = true;
                lblHeight.Visible = true;
                lblError.Visible = true;
                lblErrorVal.Visible = true;
                hscError.Visible = true;
			}
			else
			{
				SetDataMatrixRowsAndColumns();
				lblInfo.Text = "DataMatrix - Supports ASCII and binary characters.";
				TextBox1.Text = "DataMatrix";
                lblHeightVal.Visible = false;
                hscHeight.Visible = false;
                lblHeight.Visible = false;
                lblError.Visible = false;
                lblErrorVal.Visible = false;
                hscError.Visible = false;
			}

        lblResult.Text = "";
        imageXView1.Image = null;
        buttonPrint.Enabled = false;
        buttonWrite.Enabled = false;
		}

		private void SetPDF417RowsAndColumns()
		{

			cmbColumnsAndRows.Visible = false;
			hscColumns.Visible = true;
			hscRows.Visible = true;
			if(hscColumns.Value < 1)
			{
				lblColumnsVal.Text = "-1";
			}
			else
			{
				lblColumnsVal.Text = hscColumns.Value.ToString();
			}
			if(hscRows.Value < 3)
			{
				lblRowsVal.Text = "-1";
			}
			else
			{
				lblRowsVal.Text = hscRows.Value.ToString();
			}
			lblColumnsVal.Visible = true;
			lblRowsVal.Visible = true;

		}

		private void SetDataMatrixRowsAndColumns()
		{

			cmbColumnsAndRows.Visible = true;
			hscColumns.Visible = false;
			hscRows.Visible = false;
			lblColumnsVal.Visible = false;
			lblRowsVal.Visible = false;

        }

		private void UpdatePDF()
		{

			System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
			currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, 0);
			barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes;

			PegasusImaging.WinForms.BarcodeXpress5.WriterPDF417 pdfWriter = new PegasusImaging.WinForms.BarcodeXpress5.WriterPDF417(barcodeXpress1);
			if(hscRows.Value  < 3)
			{
				pdfWriter.Rows = -1;
			}
			else
			{
				pdfWriter.Rows = hscRows.Value;
			}
			if(hscColumns.Value < 1)
			{
				pdfWriter.Columns = -1;
			}
			else
			{
				pdfWriter.Columns = hscColumns.Value;
			}
			pdfWriter.ErrorCorrectionLevel = (PegasusImaging.WinForms.BarcodeXpress5.PDF417ErrorCorrectionLevel)(hscError.Value);
			pdfWriter.MinimumBarWidth = hscWidth.Value;
			pdfWriter.MinimumBarHeight = hscHeight.Value;
			pdfWriter.AutoSize = true;
			pdfWriter.BarcodeValue = TextBox1.Text;

			lblStatus.Text = "Making Barcode";
			lblResult.Text = "";
			Application.DoEvents();

            imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(new IntPtr(pdfWriter.Create())); ;

		}

		private void UpdateDM()
		{
			System.Array currentBarcodeTypes = new PegasusImaging.WinForms.BarcodeXpress5.BarcodeType[1];
			currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, 0);
			barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes;

			PegasusImaging.WinForms.BarcodeXpress5.WriterDataMatrix dmWriter = new PegasusImaging.WinForms.BarcodeXpress5.WriterDataMatrix(barcodeXpress1);
			dmWriter.RowsAndColumns = (PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes)(cmbColumnsAndRows.Items[cmbColumnsAndRows.SelectedIndex]);
			dmWriter.MinimumBarWidth = Convert.ToInt32(lblWidthVal.Text);
			dmWriter.MinimumBarHeight = Convert.ToInt32(lblHeightVal.Text);
			dmWriter.BarcodeValue = TextBox1.Text;
			dmWriter.AutoSize = true;

			lblStatus.Text = "Making Barcode";
			lblResult.Text = "";
			Application.DoEvents();

            imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(new IntPtr(dmWriter.Create())); ;
		}

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            barcodeXpress1.AboutBox();
        }

        private void ImagXpressMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Application.EnableVisualStyles();
            // You need to get unlock codes from Pegasus Imaging Corporation
            // in order to distribute your application.  The key shown below
            // is for illustration purposes only.
            barcodeXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

            barcodeXpress1.Licensing.LicenseEdition = PegasusImaging.WinForms.BarcodeXpress5.LicenseChoice.ProfessionalPlus2DWriteEdition;

            //Setup the values of the Data Matrix Combo Box
            cmbColumnsAndRows.Items.Add("Auto");
            cmbColumnsAndRows.Items[0] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.AutoRowsAndColumns;

            cmbColumnsAndRows.Items.Add("8 x 18");
            cmbColumnsAndRows.Items[1] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows8Columns18;

            cmbColumnsAndRows.Items.Add("8 x 32");
            cmbColumnsAndRows.Items[2] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows8Columns32;

            cmbColumnsAndRows.Items.Add("10 x 10");
            cmbColumnsAndRows.Items[3] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows10Columns10;

            cmbColumnsAndRows.Items.Add("12 x 12");
            cmbColumnsAndRows.Items[4] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns12;

            cmbColumnsAndRows.Items.Add("12 x 26");
            cmbColumnsAndRows.Items[5] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns26;

            cmbColumnsAndRows.Items.Add("12 x 36");
            cmbColumnsAndRows.Items[6] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns36;

            cmbColumnsAndRows.Items.Add("14 x 14");
            cmbColumnsAndRows.Items[7] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows14Columns14;

            cmbColumnsAndRows.Items.Add("16 x 16");
            cmbColumnsAndRows.Items[8] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns16;

            cmbColumnsAndRows.Items.Add("16 x 36");
            cmbColumnsAndRows.Items[9] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns36;

            cmbColumnsAndRows.Items.Add("16 x 48");
            cmbColumnsAndRows.Items[10] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns48;

            cmbColumnsAndRows.Items.Add("18 x 18");
            cmbColumnsAndRows.Items[11] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows18Columns18;

            cmbColumnsAndRows.Items.Add("20 x 20");
            cmbColumnsAndRows.Items[12] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows20Columns20;

            cmbColumnsAndRows.Items.Add("22 x 22");
            cmbColumnsAndRows.Items[13] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows22Columns22;

            cmbColumnsAndRows.Items.Add("24 x 24");
            cmbColumnsAndRows.Items[14] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows24Columns24;

            cmbColumnsAndRows.Items.Add("26 x 26");
            cmbColumnsAndRows.Items[15] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows26Columns26;

            cmbColumnsAndRows.Items.Add("32 x 32");
            cmbColumnsAndRows.Items[16] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows32Columns32;

            cmbColumnsAndRows.Items.Add("36 x 36");
            cmbColumnsAndRows.Items[17] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows36Columns36;

            cmbColumnsAndRows.Items.Add("40 x 40");
            cmbColumnsAndRows.Items[18] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows40Columns40;

            cmbColumnsAndRows.Items.Add("44 x 44");
            cmbColumnsAndRows.Items[19] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows44Columns44;

            cmbColumnsAndRows.Items.Add("48 x 48");
            cmbColumnsAndRows.Items[20] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows48Columns48;

            cmbColumnsAndRows.Items.Add("52 x 52");
            cmbColumnsAndRows.Items[21] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows52Columns52;

            cmbColumnsAndRows.Items.Add("64 x 64");
            cmbColumnsAndRows.Items[22] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows64Columns64;

            cmbColumnsAndRows.Items.Add("72 x 72");
            cmbColumnsAndRows.Items[23] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows72Columns72;

            cmbColumnsAndRows.Items.Add("80 x 80");
            cmbColumnsAndRows.Items[24] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows80Columns80;

            cmbColumnsAndRows.Items.Add("88 x 88");
            cmbColumnsAndRows.Items[25] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows88Columns88;

            cmbColumnsAndRows.Items.Add("96 x 96");
            cmbColumnsAndRows.Items[26] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows96Columns96;

            cmbColumnsAndRows.Items.Add("104 x 104");
            cmbColumnsAndRows.Items[27] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows104Columns104;

            cmbColumnsAndRows.Items.Add("120 x 120");
            cmbColumnsAndRows.Items[28] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows120Columns120;

            cmbColumnsAndRows.Items.Add("132 x 132");
            cmbColumnsAndRows.Items[29] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows132Columns132;

            cmbColumnsAndRows.Items.Add("144 x 144");
            cmbColumnsAndRows.Items[30] = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows144Columns144;

            cmbColumnsAndRows.SelectedIndex = 0;

            lblColumnsVal.Text = "-1";
            lblRowsVal.Text = "-1";

            //Set the Barcode type combo box
            cmbBarcodeType.SelectedIndex = 0;

            buttonPrint.Enabled = false;
            buttonWrite.Enabled = false;
        }


	}
}

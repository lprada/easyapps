namespace BarcodeDemo
{
    partial class BinaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BinaryForm));
            this.BarcodeFound = new System.Windows.Forms.Label();
            this.HexTextBox = new System.Windows.Forms.TextBox();
            this.ValueLabel = new System.Windows.Forms.Label();
            this.HexLabel = new System.Windows.Forms.Label();
            this.ValueTextBox = new System.Windows.Forms.TextBox();
            this.OKBinaryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BarcodeFound
            // 
            this.BarcodeFound.AutoSize = true;
            this.BarcodeFound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BarcodeFound.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BarcodeFound.Location = new System.Drawing.Point(14, 17);
            this.BarcodeFound.Name = "BarcodeFound";
            this.BarcodeFound.Size = new System.Drawing.Size(2, 15);
            this.BarcodeFound.TabIndex = 0;
            // 
            // HexTextBox
            // 
            this.HexTextBox.Location = new System.Drawing.Point(14, 406);
            this.HexTextBox.Multiline = true;
            this.HexTextBox.Name = "HexTextBox";
            this.HexTextBox.ReadOnly = true;
            this.HexTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.HexTextBox.Size = new System.Drawing.Size(437, 237);
            this.HexTextBox.TabIndex = 1;
            // 
            // ValueLabel
            // 
            this.ValueLabel.Location = new System.Drawing.Point(11, 119);
            this.ValueLabel.Name = "ValueLabel";
            this.ValueLabel.Size = new System.Drawing.Size(87, 13);
            this.ValueLabel.TabIndex = 2;
            this.ValueLabel.Text = "ASCII Character:";
            // 
            // HexLabel
            // 
            this.HexLabel.Location = new System.Drawing.Point(14, 390);
            this.HexLabel.Name = "HexLabel";
            this.HexLabel.Size = new System.Drawing.Size(70, 13);
            this.HexLabel.TabIndex = 4;
            this.HexLabel.Text = "Hex Value:";
            // 
            // ValueTextBox
            // 
            this.ValueTextBox.Location = new System.Drawing.Point(14, 135);
            this.ValueTextBox.Multiline = true;
            this.ValueTextBox.Name = "ValueTextBox";
            this.ValueTextBox.ReadOnly = true;
            this.ValueTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ValueTextBox.Size = new System.Drawing.Size(437, 237);
            this.ValueTextBox.TabIndex = 3;
            // 
            // OKBinaryButton
            // 
            this.OKBinaryButton.Location = new System.Drawing.Point(168, 659);
            this.OKBinaryButton.Name = "OKBinaryButton";
            this.OKBinaryButton.Size = new System.Drawing.Size(118, 27);
            this.OKBinaryButton.TabIndex = 5;
            this.OKBinaryButton.Text = "OK";
            this.OKBinaryButton.UseVisualStyleBackColor = true;
            this.OKBinaryButton.Click += new System.EventHandler(this.OKBinaryButton_Click);
            // 
            // BinaryForm
            // 
            this.AcceptButton = this.OKBinaryButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 698);
            this.Controls.Add(this.OKBinaryButton);
            this.Controls.Add(this.HexLabel);
            this.Controls.Add(this.ValueTextBox);
            this.Controls.Add(this.ValueLabel);
            this.Controls.Add(this.HexTextBox);
            this.Controls.Add(this.BarcodeFound);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BinaryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "2D Barcode Data";
            this.Load += new System.EventHandler(this.BinaryForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BarcodeFound;
        private System.Windows.Forms.TextBox HexTextBox;
        private System.Windows.Forms.Label ValueLabel;
        private System.Windows.Forms.Label HexLabel;
        private System.Windows.Forms.TextBox ValueTextBox;
        private System.Windows.Forms.Button OKBinaryButton;
    }
}
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BinarizeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BinarizeForm))
        Me.OKButton = New System.Windows.Forms.Button
        Me.DescriptionListBox = New System.Windows.Forms.ListBox
        Me.BinGroupBox = New System.Windows.Forms.GroupBox
        Me.BinarizeGroupBox = New System.Windows.Forms.GroupBox
        Me.BlurBox = New System.Windows.Forms.ComboBox
        Me.BlurLabel = New System.Windows.Forms.Label
        Me.HighBar = New System.Windows.Forms.TrackBar
        Me.LowBar = New System.Windows.Forms.TrackBar
        Me.ConBar = New System.Windows.Forms.TrackBar
        Me.ConValue = New System.Windows.Forms.Label
        Me.LowValue = New System.Windows.Forms.Label
        Me.HighValue = New System.Windows.Forms.Label
        Me.HighLabel = New System.Windows.Forms.Label
        Me.LowLabel = New System.Windows.Forms.Label
        Me.ConLabel = New System.Windows.Forms.Label
        Me.ColorDepthRadioButton = New System.Windows.Forms.RadioButton
        Me.BinarizeRadioButton = New System.Windows.Forms.RadioButton
        Me.AutoRadioButton = New System.Windows.Forms.RadioButton
        Me.BinGroupBox.SuspendLayout()
        Me.BinarizeGroupBox.SuspendLayout()
        CType(Me.HighBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LowBar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(227, 627)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(94, 30)
        Me.OKButton.TabIndex = 0
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'DescriptionListBox
        '
        Me.DescriptionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionListBox.FormattingEnabled = True
        Me.DescriptionListBox.Items.AddRange(New Object() {"The image you're opening is not 1-bit (B&W or Bitonal), BarcodeXpress only works " & _
                        "", "with 1-bit images.", "", "Please binarize the image using one of the following options:", "", "ColorDepth - simple thresholding method of ImagXpress (All Editions)", "", "Binarize - Controllable, more powerful method of ScanFix Bitonal Edition ", "and ImagXpress Document or Photo Edition. ", "(The Binarize used in this Demo is for ScanFix).", "", "AutoBinarize - Automatic setting of ScanFix's Binarize method ", "(ScanFix Bitonal Edition only)."})
        Me.DescriptionListBox.Location = New System.Drawing.Point(12, 12)
        Me.DescriptionListBox.Name = "DescriptionListBox"
        Me.DescriptionListBox.Size = New System.Drawing.Size(547, 186)
        Me.DescriptionListBox.TabIndex = 1
        '
        'BinGroupBox
        '
        Me.BinGroupBox.Controls.Add(Me.ConLabel)
        Me.BinGroupBox.Controls.Add(Me.LowLabel)
        Me.BinGroupBox.Controls.Add(Me.HighLabel)
        Me.BinGroupBox.Controls.Add(Me.HighValue)
        Me.BinGroupBox.Controls.Add(Me.LowValue)
        Me.BinGroupBox.Controls.Add(Me.ConValue)
        Me.BinGroupBox.Controls.Add(Me.ConBar)
        Me.BinGroupBox.Controls.Add(Me.LowBar)
        Me.BinGroupBox.Controls.Add(Me.HighBar)
        Me.BinGroupBox.Controls.Add(Me.BlurLabel)
        Me.BinGroupBox.Controls.Add(Me.BlurBox)
        Me.BinGroupBox.Location = New System.Drawing.Point(51, 348)
        Me.BinGroupBox.Name = "BinGroupBox"
        Me.BinGroupBox.Size = New System.Drawing.Size(466, 262)
        Me.BinGroupBox.TabIndex = 2
        Me.BinGroupBox.TabStop = False
        Me.BinGroupBox.Text = "Binarize Method Options"
        '
        'BinarizeGroupBox
        '
        Me.BinarizeGroupBox.Controls.Add(Me.AutoRadioButton)
        Me.BinarizeGroupBox.Controls.Add(Me.BinarizeRadioButton)
        Me.BinarizeGroupBox.Controls.Add(Me.ColorDepthRadioButton)
        Me.BinarizeGroupBox.Location = New System.Drawing.Point(191, 215)
        Me.BinarizeGroupBox.Name = "BinarizeGroupBox"
        Me.BinarizeGroupBox.Size = New System.Drawing.Size(200, 127)
        Me.BinarizeGroupBox.TabIndex = 0
        Me.BinarizeGroupBox.TabStop = False
        Me.BinarizeGroupBox.Text = "Binarization Options"
        '
        'BlurBox
        '
        Me.BlurBox.FormattingEnabled = True
        Me.BlurBox.Items.AddRange(New Object() {"None", "Gaussian", "Smart"})
        Me.BlurBox.Location = New System.Drawing.Point(176, 230)
        Me.BlurBox.Name = "BlurBox"
        Me.BlurBox.Size = New System.Drawing.Size(121, 21)
        Me.BlurBox.TabIndex = 0
        '
        'BlurLabel
        '
        Me.BlurLabel.AutoSize = True
        Me.BlurLabel.Location = New System.Drawing.Point(102, 233)
        Me.BlurLabel.Name = "BlurLabel"
        Me.BlurLabel.Size = New System.Drawing.Size(68, 13)
        Me.BlurLabel.TabIndex = 1
        Me.BlurLabel.Text = "PreBlurType:"
        '
        'HighBar
        '
        Me.HighBar.Location = New System.Drawing.Point(69, 33)
        Me.HighBar.Maximum = 255
        Me.HighBar.Name = "HighBar"
        Me.HighBar.Size = New System.Drawing.Size(296, 45)
        Me.HighBar.TabIndex = 2
        Me.HighBar.TickStyle = System.Windows.Forms.TickStyle.None
        Me.HighBar.Value = 170
        '
        'LowBar
        '
        Me.LowBar.Location = New System.Drawing.Point(69, 97)
        Me.LowBar.Maximum = 255
        Me.LowBar.Name = "LowBar"
        Me.LowBar.Size = New System.Drawing.Size(296, 45)
        Me.LowBar.TabIndex = 3
        Me.LowBar.TickStyle = System.Windows.Forms.TickStyle.None
        Me.LowBar.Value = 150
        '
        'ConBar
        '
        Me.ConBar.Location = New System.Drawing.Point(69, 161)
        Me.ConBar.Maximum = 255
        Me.ConBar.Name = "ConBar"
        Me.ConBar.Size = New System.Drawing.Size(296, 45)
        Me.ConBar.TabIndex = 4
        Me.ConBar.TickStyle = System.Windows.Forms.TickStyle.None
        '
        'ConValue
        '
        Me.ConValue.AutoSize = True
        Me.ConValue.Location = New System.Drawing.Point(371, 177)
        Me.ConValue.Name = "ConValue"
        Me.ConValue.Size = New System.Drawing.Size(13, 13)
        Me.ConValue.TabIndex = 5
        Me.ConValue.Text = "0"
        '
        'LowValue
        '
        Me.LowValue.AutoSize = True
        Me.LowValue.Location = New System.Drawing.Point(371, 106)
        Me.LowValue.Name = "LowValue"
        Me.LowValue.Size = New System.Drawing.Size(25, 13)
        Me.LowValue.TabIndex = 6
        Me.LowValue.Text = "150"
        '
        'HighValue
        '
        Me.HighValue.AutoSize = True
        Me.HighValue.Location = New System.Drawing.Point(371, 43)
        Me.HighValue.Name = "HighValue"
        Me.HighValue.Size = New System.Drawing.Size(25, 13)
        Me.HighValue.TabIndex = 7
        Me.HighValue.Text = "170"
        '
        'HighLabel
        '
        Me.HighLabel.AutoSize = True
        Me.HighLabel.Location = New System.Drawing.Point(185, 17)
        Me.HighLabel.Name = "HighLabel"
        Me.HighLabel.Size = New System.Drawing.Size(79, 13)
        Me.HighLabel.TabIndex = 8
        Me.HighLabel.Text = "High Threshold"
        '
        'LowLabel
        '
        Me.LowLabel.AutoSize = True
        Me.LowLabel.Location = New System.Drawing.Point(193, 81)
        Me.LowLabel.Name = "LowLabel"
        Me.LowLabel.Size = New System.Drawing.Size(77, 13)
        Me.LowLabel.TabIndex = 9
        Me.LowLabel.Text = "Low Threshold"
        '
        'ConLabel
        '
        Me.ConLabel.AutoSize = True
        Me.ConLabel.Location = New System.Drawing.Point(199, 145)
        Me.ConLabel.Name = "ConLabel"
        Me.ConLabel.Size = New System.Drawing.Size(46, 13)
        Me.ConLabel.TabIndex = 10
        Me.ConLabel.Text = "Contrast"
        '
        'ColorDepthRadioButton
        '
        Me.ColorDepthRadioButton.AutoSize = True
        Me.ColorDepthRadioButton.Checked = True
        Me.ColorDepthRadioButton.Location = New System.Drawing.Point(51, 28)
        Me.ColorDepthRadioButton.Name = "ColorDepthRadioButton"
        Me.ColorDepthRadioButton.Size = New System.Drawing.Size(78, 17)
        Me.ColorDepthRadioButton.TabIndex = 0
        Me.ColorDepthRadioButton.TabStop = True
        Me.ColorDepthRadioButton.Text = "ColorDepth"
        Me.ColorDepthRadioButton.UseVisualStyleBackColor = True
        '
        'BinarizeRadioButton
        '
        Me.BinarizeRadioButton.AutoSize = True
        Me.BinarizeRadioButton.Location = New System.Drawing.Point(51, 63)
        Me.BinarizeRadioButton.Name = "BinarizeRadioButton"
        Me.BinarizeRadioButton.Size = New System.Drawing.Size(62, 17)
        Me.BinarizeRadioButton.TabIndex = 1
        Me.BinarizeRadioButton.Text = "Binarize"
        Me.BinarizeRadioButton.UseVisualStyleBackColor = True
        '
        'AutoRadioButton
        '
        Me.AutoRadioButton.AutoSize = True
        Me.AutoRadioButton.Location = New System.Drawing.Point(51, 98)
        Me.AutoRadioButton.Name = "AutoRadioButton"
        Me.AutoRadioButton.Size = New System.Drawing.Size(84, 17)
        Me.AutoRadioButton.TabIndex = 2
        Me.AutoRadioButton.Text = "AutoBinarize"
        Me.AutoRadioButton.UseVisualStyleBackColor = True
        '
        'BinarizeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(564, 669)
        Me.Controls.Add(Me.BinarizeGroupBox)
        Me.Controls.Add(Me.BinGroupBox)
        Me.Controls.Add(Me.DescriptionListBox)
        Me.Controls.Add(Me.OKButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "BinarizeForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BinarizeForm"
        Me.BinGroupBox.ResumeLayout(False)
        Me.BinGroupBox.PerformLayout()
        Me.BinarizeGroupBox.ResumeLayout(False)
        Me.BinarizeGroupBox.PerformLayout()
        CType(Me.HighBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LowBar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents DescriptionListBox As System.Windows.Forms.ListBox
    Friend WithEvents BinGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents BinarizeGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents ConLabel As System.Windows.Forms.Label
    Friend WithEvents LowLabel As System.Windows.Forms.Label
    Friend WithEvents HighLabel As System.Windows.Forms.Label
    Friend WithEvents HighValue As System.Windows.Forms.Label
    Friend WithEvents LowValue As System.Windows.Forms.Label
    Friend WithEvents ConValue As System.Windows.Forms.Label
    Friend WithEvents ConBar As System.Windows.Forms.TrackBar
    Friend WithEvents LowBar As System.Windows.Forms.TrackBar
    Friend WithEvents HighBar As System.Windows.Forms.TrackBar
    Friend WithEvents BlurLabel As System.Windows.Forms.Label
    Friend WithEvents BlurBox As System.Windows.Forms.ComboBox
    Friend WithEvents AutoRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BinarizeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents ColorDepthRadioButton As System.Windows.Forms.RadioButton
End Class

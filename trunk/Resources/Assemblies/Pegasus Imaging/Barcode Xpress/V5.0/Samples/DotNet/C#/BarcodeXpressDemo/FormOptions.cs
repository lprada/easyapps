/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ScanFix5;
using PegasusImaging.WinForms.BarcodeXpress5;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for FormOptions.
	/// </summary>
	public class FormOptions : System.Windows.Forms.Form
	{
        public bool Cancel;

		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabPageType;
        private System.Windows.Forms.GroupBox fraType;
        public RadioButton optTypeDataMatrix;
        public RadioButton optType1D;
        public RadioButton optTypePFD417;
        public RadioButton optTypePatchCode;
		private System.Windows.Forms.GroupBox frm1DTypes;
        private System.Windows.Forms.Button cmdClear;
        public CheckBox _chkBCType_0;
        public CheckBox _chkBCType_1;
        public CheckBox _chkBCType_2;
        public CheckBox _chkBCType_3;
        public CheckBox _chkBCType_4;
        public CheckBox _chkBCType_5;
        public CheckBox _chkBCType_6;
        public CheckBox _chkBCType_7;
        public CheckBox _chkBCType_8;
        public CheckBox _chkBCType_9;
        public CheckBox _chkBCType_10;
        public CheckBox _chkBCType_11;
        public CheckBox _chkBCType_12;
        public CheckBox _chkBCType_13;
        public CheckBox _chkBCType_14;
        public CheckBox _chkBCType_15;
        public CheckBox _chkBCType_16;
        public CheckBox _chkBCType_17;
        public CheckBox _chkBCType_18;
		private System.Windows.Forms.Button cmdAll;
		private System.Windows.Forms.TabPage tabPageOrientation;
		private System.Windows.Forms.TabPage tabPageMisc;
		private System.Windows.Forms.Panel fraOrient;
		private System.Windows.Forms.RadioButton _optOrient_2;
		private System.Windows.Forms.RadioButton _optOrient_1;
		private System.Windows.Forms.RadioButton _optOrient_0;
		private System.Windows.Forms.Panel fraMisc;
		private System.Windows.Forms.TextBox txtMax;
		private System.Windows.Forms.ComboBox cboColor;
		private System.Windows.Forms.Label Label3;
		private System.Windows.Forms.Label Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		// property variables
		private System.Array _nBarcodeTypes;
		private InkColorInfo _barcodeInkColor;
		private System.Int32 _nMaxBarcodes;
		private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        public RadioButton optTypePostNet;
        public RadioButton RoyalRadioButton;
        public RadioButton AusRadioButton;
        public RadioButton QRRadioButton;
        public RadioButton OneCodeRadioButton;
		private OrientationInfo _barcodeOrientation;
        private GroupBox AusGroupBox;
        public RadioButton CustomStatesRadioButton;
        public RadioButton CustomTableNRadioButton;
        public RadioButton CustomTableCRadioButton;
        public RadioButton CustomNoneRadioButton;
        private Label ChecksumLabel;
        public CheckBox CheckSumCheckBox;
        public CheckBox Code39ExtCheckBox;
        public CheckBox Code93ExtCheckBox;
        private Label OrientationLabel;
        private RadioButton DiagRadioButton;
        private Label ScanLabel;
        public NumericUpDown ScanBox;
        private Label DistanceLabel2;
        private Label ScanLabel3;

		public FormOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageType = new System.Windows.Forms.TabPage();
            this.AusGroupBox = new System.Windows.Forms.GroupBox();
            this.CustomStatesRadioButton = new System.Windows.Forms.RadioButton();
            this.CustomTableNRadioButton = new System.Windows.Forms.RadioButton();
            this.CustomTableCRadioButton = new System.Windows.Forms.RadioButton();
            this.CustomNoneRadioButton = new System.Windows.Forms.RadioButton();
            this.frm1DTypes = new System.Windows.Forms.GroupBox();
            this.Code39ExtCheckBox = new System.Windows.Forms.CheckBox();
            this.Code93ExtCheckBox = new System.Windows.Forms.CheckBox();
            this.cmdClear = new System.Windows.Forms.Button();
            this._chkBCType_0 = new System.Windows.Forms.CheckBox();
            this._chkBCType_1 = new System.Windows.Forms.CheckBox();
            this._chkBCType_2 = new System.Windows.Forms.CheckBox();
            this._chkBCType_3 = new System.Windows.Forms.CheckBox();
            this._chkBCType_4 = new System.Windows.Forms.CheckBox();
            this._chkBCType_5 = new System.Windows.Forms.CheckBox();
            this._chkBCType_6 = new System.Windows.Forms.CheckBox();
            this._chkBCType_7 = new System.Windows.Forms.CheckBox();
            this._chkBCType_8 = new System.Windows.Forms.CheckBox();
            this._chkBCType_9 = new System.Windows.Forms.CheckBox();
            this._chkBCType_10 = new System.Windows.Forms.CheckBox();
            this._chkBCType_11 = new System.Windows.Forms.CheckBox();
            this._chkBCType_12 = new System.Windows.Forms.CheckBox();
            this._chkBCType_13 = new System.Windows.Forms.CheckBox();
            this._chkBCType_14 = new System.Windows.Forms.CheckBox();
            this._chkBCType_15 = new System.Windows.Forms.CheckBox();
            this._chkBCType_16 = new System.Windows.Forms.CheckBox();
            this._chkBCType_17 = new System.Windows.Forms.CheckBox();
            this._chkBCType_18 = new System.Windows.Forms.CheckBox();
            this.cmdAll = new System.Windows.Forms.Button();
            this.fraType = new System.Windows.Forms.GroupBox();
            this.OneCodeRadioButton = new System.Windows.Forms.RadioButton();
            this.RoyalRadioButton = new System.Windows.Forms.RadioButton();
            this.AusRadioButton = new System.Windows.Forms.RadioButton();
            this.QRRadioButton = new System.Windows.Forms.RadioButton();
            this.optTypePostNet = new System.Windows.Forms.RadioButton();
            this.optTypeDataMatrix = new System.Windows.Forms.RadioButton();
            this.optType1D = new System.Windows.Forms.RadioButton();
            this.optTypePFD417 = new System.Windows.Forms.RadioButton();
            this.optTypePatchCode = new System.Windows.Forms.RadioButton();
            this.tabPageOrientation = new System.Windows.Forms.TabPage();
            this.fraOrient = new System.Windows.Forms.Panel();
            this.DiagRadioButton = new System.Windows.Forms.RadioButton();
            this.OrientationLabel = new System.Windows.Forms.Label();
            this._optOrient_2 = new System.Windows.Forms.RadioButton();
            this._optOrient_1 = new System.Windows.Forms.RadioButton();
            this._optOrient_0 = new System.Windows.Forms.RadioButton();
            this.tabPageMisc = new System.Windows.Forms.TabPage();
            this.fraMisc = new System.Windows.Forms.Panel();
            this.ScanLabel3 = new System.Windows.Forms.Label();
            this.DistanceLabel2 = new System.Windows.Forms.Label();
            this.ScanLabel = new System.Windows.Forms.Label();
            this.ScanBox = new System.Windows.Forms.NumericUpDown();
            this.ChecksumLabel = new System.Windows.Forms.Label();
            this.CheckSumCheckBox = new System.Windows.Forms.CheckBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtMax = new System.Windows.Forms.TextBox();
            this.cboColor = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.tabPageType.SuspendLayout();
            this.AusGroupBox.SuspendLayout();
            this.frm1DTypes.SuspendLayout();
            this.fraType.SuspendLayout();
            this.tabPageOrientation.SuspendLayout();
            this.fraOrient.SuspendLayout();
            this.tabPageMisc.SuspendLayout();
            this.fraMisc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScanBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageType);
            this.tabControl.Controls.Add(this.tabPageOrientation);
            this.tabControl.Controls.Add(this.tabPageMisc);
            this.tabControl.Location = new System.Drawing.Point(8, 8);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(522, 378);
            this.tabControl.TabIndex = 1;
            // 
            // tabPageType
            // 
            this.tabPageType.Controls.Add(this.AusGroupBox);
            this.tabPageType.Controls.Add(this.frm1DTypes);
            this.tabPageType.Controls.Add(this.fraType);
            this.tabPageType.Location = new System.Drawing.Point(4, 22);
            this.tabPageType.Name = "tabPageType";
            this.tabPageType.Size = new System.Drawing.Size(514, 352);
            this.tabPageType.TabIndex = 0;
            this.tabPageType.Text = "Barcode Type";
            // 
            // AusGroupBox
            // 
            this.AusGroupBox.Controls.Add(this.CustomStatesRadioButton);
            this.AusGroupBox.Controls.Add(this.CustomTableNRadioButton);
            this.AusGroupBox.Controls.Add(this.CustomTableCRadioButton);
            this.AusGroupBox.Controls.Add(this.CustomNoneRadioButton);
            this.AusGroupBox.Enabled = false;
            this.AusGroupBox.Location = new System.Drawing.Point(328, 8);
            this.AusGroupBox.Name = "AusGroupBox";
            this.AusGroupBox.Size = new System.Drawing.Size(161, 122);
            this.AusGroupBox.TabIndex = 3;
            this.AusGroupBox.TabStop = false;
            this.AusGroupBox.Text = "Aus Post Custom Info Section";
            // 
            // CustomStatesRadioButton
            // 
            this.CustomStatesRadioButton.AutoSize = true;
            this.CustomStatesRadioButton.Location = new System.Drawing.Point(15, 94);
            this.CustomStatesRadioButton.Name = "CustomStatesRadioButton";
            this.CustomStatesRadioButton.Size = new System.Drawing.Size(71, 17);
            this.CustomStatesRadioButton.TabIndex = 3;
            this.CustomStatesRadioButton.Text = "BarStates";
            this.CustomStatesRadioButton.UseVisualStyleBackColor = true;
            // 
            // CustomTableNRadioButton
            // 
            this.CustomTableNRadioButton.AutoSize = true;
            this.CustomTableNRadioButton.Location = new System.Drawing.Point(15, 72);
            this.CustomTableNRadioButton.Name = "CustomTableNRadioButton";
            this.CustomTableNRadioButton.Size = new System.Drawing.Size(60, 17);
            this.CustomTableNRadioButton.TabIndex = 2;
            this.CustomTableNRadioButton.Text = "TableN";
            this.CustomTableNRadioButton.UseVisualStyleBackColor = true;
            // 
            // CustomTableCRadioButton
            // 
            this.CustomTableCRadioButton.AutoSize = true;
            this.CustomTableCRadioButton.Location = new System.Drawing.Point(15, 49);
            this.CustomTableCRadioButton.Name = "CustomTableCRadioButton";
            this.CustomTableCRadioButton.Size = new System.Drawing.Size(59, 17);
            this.CustomTableCRadioButton.TabIndex = 1;
            this.CustomTableCRadioButton.Text = "TableC";
            this.CustomTableCRadioButton.UseVisualStyleBackColor = true;
            // 
            // CustomNoneRadioButton
            // 
            this.CustomNoneRadioButton.AutoSize = true;
            this.CustomNoneRadioButton.Checked = true;
            this.CustomNoneRadioButton.Location = new System.Drawing.Point(15, 26);
            this.CustomNoneRadioButton.Name = "CustomNoneRadioButton";
            this.CustomNoneRadioButton.Size = new System.Drawing.Size(51, 17);
            this.CustomNoneRadioButton.TabIndex = 0;
            this.CustomNoneRadioButton.TabStop = true;
            this.CustomNoneRadioButton.Text = "None";
            this.CustomNoneRadioButton.UseVisualStyleBackColor = true;
            // 
            // frm1DTypes
            // 
            this.frm1DTypes.BackColor = System.Drawing.SystemColors.Control;
            this.frm1DTypes.Controls.Add(this.Code39ExtCheckBox);
            this.frm1DTypes.Controls.Add(this.Code93ExtCheckBox);
            this.frm1DTypes.Controls.Add(this.cmdClear);
            this.frm1DTypes.Controls.Add(this._chkBCType_0);
            this.frm1DTypes.Controls.Add(this._chkBCType_1);
            this.frm1DTypes.Controls.Add(this._chkBCType_2);
            this.frm1DTypes.Controls.Add(this._chkBCType_3);
            this.frm1DTypes.Controls.Add(this._chkBCType_4);
            this.frm1DTypes.Controls.Add(this._chkBCType_5);
            this.frm1DTypes.Controls.Add(this._chkBCType_6);
            this.frm1DTypes.Controls.Add(this._chkBCType_7);
            this.frm1DTypes.Controls.Add(this._chkBCType_8);
            this.frm1DTypes.Controls.Add(this._chkBCType_9);
            this.frm1DTypes.Controls.Add(this._chkBCType_10);
            this.frm1DTypes.Controls.Add(this._chkBCType_11);
            this.frm1DTypes.Controls.Add(this._chkBCType_12);
            this.frm1DTypes.Controls.Add(this._chkBCType_13);
            this.frm1DTypes.Controls.Add(this._chkBCType_14);
            this.frm1DTypes.Controls.Add(this._chkBCType_15);
            this.frm1DTypes.Controls.Add(this._chkBCType_16);
            this.frm1DTypes.Controls.Add(this._chkBCType_17);
            this.frm1DTypes.Controls.Add(this._chkBCType_18);
            this.frm1DTypes.Controls.Add(this.cmdAll);
            this.frm1DTypes.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frm1DTypes.ForeColor = System.Drawing.SystemColors.ControlText;
            this.frm1DTypes.Location = new System.Drawing.Point(16, 126);
            this.frm1DTypes.Name = "frm1DTypes";
            this.frm1DTypes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.frm1DTypes.Size = new System.Drawing.Size(423, 223);
            this.frm1DTypes.TabIndex = 2;
            this.frm1DTypes.TabStop = false;
            this.frm1DTypes.Text = "1D Barcode Types: ";
            // 
            // Code39ExtCheckBox
            // 
            this.Code39ExtCheckBox.AutoSize = true;
            this.Code39ExtCheckBox.Location = new System.Drawing.Point(318, 65);
            this.Code39ExtCheckBox.Name = "Code39ExtCheckBox";
            this.Code39ExtCheckBox.Size = new System.Drawing.Size(84, 18);
            this.Code39ExtCheckBox.TabIndex = 23;
            this.Code39ExtCheckBox.Text = "Code 39 Ext";
            this.Code39ExtCheckBox.UseVisualStyleBackColor = true;
            // 
            // Code93ExtCheckBox
            // 
            this.Code93ExtCheckBox.AutoSize = true;
            this.Code93ExtCheckBox.Location = new System.Drawing.Point(318, 40);
            this.Code93ExtCheckBox.Name = "Code93ExtCheckBox";
            this.Code93ExtCheckBox.Size = new System.Drawing.Size(84, 18);
            this.Code93ExtCheckBox.TabIndex = 22;
            this.Code93ExtCheckBox.Text = "Code 93 Ext";
            this.Code93ExtCheckBox.UseVisualStyleBackColor = true;
            // 
            // cmdClear
            // 
            this.cmdClear.BackColor = System.Drawing.SystemColors.Control;
            this.cmdClear.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdClear.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdClear.Location = new System.Drawing.Point(126, 189);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdClear.Size = new System.Drawing.Size(97, 23);
            this.cmdClear.TabIndex = 21;
            this.cmdClear.Text = "&Clear";
            this.cmdClear.UseVisualStyleBackColor = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // _chkBCType_0
            // 
            this._chkBCType_0.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_0.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_0.Location = new System.Drawing.Point(8, 31);
            this._chkBCType_0.Name = "_chkBCType_0";
            this._chkBCType_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_0.Size = new System.Drawing.Size(94, 25);
            this._chkBCType_0.TabIndex = 1;
            this._chkBCType_0.Text = "Code 2 of 5";
            this._chkBCType_0.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_1
            // 
            this._chkBCType_1.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_1.Location = new System.Drawing.Point(117, 81);
            this._chkBCType_1.Name = "_chkBCType_1";
            this._chkBCType_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_1.Size = new System.Drawing.Size(112, 28);
            this._chkBCType_1.TabIndex = 4;
            this._chkBCType_1.Text = "Interleaved 2 of 5";
            this._chkBCType_1.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_2
            // 
            this._chkBCType_2.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_2.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_2.Location = new System.Drawing.Point(6, 59);
            this._chkBCType_2.Name = "_chkBCType_2";
            this._chkBCType_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_2.Size = new System.Drawing.Size(105, 21);
            this._chkBCType_2.TabIndex = 7;
            this._chkBCType_2.Text = "Airline 2 of 5";
            this._chkBCType_2.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_3
            // 
            this._chkBCType_3.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_3.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_3.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_3.Location = new System.Drawing.Point(8, 84);
            this._chkBCType_3.Name = "_chkBCType_3";
            this._chkBCType_3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_3.Size = new System.Drawing.Size(105, 22);
            this._chkBCType_3.TabIndex = 10;
            this._chkBCType_3.Text = "DataLogic 2 of 5";
            this._chkBCType_3.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_4
            // 
            this._chkBCType_4.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_4.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_4.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_4.Location = new System.Drawing.Point(8, 113);
            this._chkBCType_4.Name = "_chkBCType_4";
            this._chkBCType_4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_4.Size = new System.Drawing.Size(94, 17);
            this._chkBCType_4.TabIndex = 13;
            this._chkBCType_4.Text = "Invert 2 of 5";
            this._chkBCType_4.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_5
            // 
            this._chkBCType_5.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_5.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_5.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_5.Location = new System.Drawing.Point(8, 136);
            this._chkBCType_5.Name = "_chkBCType_5";
            this._chkBCType_5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_5.Size = new System.Drawing.Size(94, 19);
            this._chkBCType_5.TabIndex = 16;
            this._chkBCType_5.Text = "BCD Matrix";
            this._chkBCType_5.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_6
            // 
            this._chkBCType_6.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_6.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_6.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_6.Location = new System.Drawing.Point(8, 161);
            this._chkBCType_6.Name = "_chkBCType_6";
            this._chkBCType_6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_6.Size = new System.Drawing.Size(94, 17);
            this._chkBCType_6.TabIndex = 18;
            this._chkBCType_6.Text = "Matrix 2 of 5";
            this._chkBCType_6.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_7
            // 
            this._chkBCType_7.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_7.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_7.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_7.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_7.Location = new System.Drawing.Point(117, 36);
            this._chkBCType_7.Name = "_chkBCType_7";
            this._chkBCType_7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_7.Size = new System.Drawing.Size(112, 22);
            this._chkBCType_7.TabIndex = 2;
            this._chkBCType_7.Text = "Code 32";
            this._chkBCType_7.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_8
            // 
            this._chkBCType_8.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_8.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_8.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_8.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_8.Location = new System.Drawing.Point(235, 159);
            this._chkBCType_8.Name = "_chkBCType_8";
            this._chkBCType_8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_8.Size = new System.Drawing.Size(112, 17);
            this._chkBCType_8.TabIndex = 5;
            this._chkBCType_8.Text = "Code 39";
            this._chkBCType_8.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_9
            // 
            this._chkBCType_9.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_9.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_9.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_9.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_9.Location = new System.Drawing.Point(8, 184);
            this._chkBCType_9.Name = "_chkBCType_9";
            this._chkBCType_9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_9.Size = new System.Drawing.Size(112, 28);
            this._chkBCType_9.TabIndex = 8;
            this._chkBCType_9.Text = "CODABAR";
            this._chkBCType_9.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_10
            // 
            this._chkBCType_10.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_10.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_10.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_10.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_10.Location = new System.Drawing.Point(117, 59);
            this._chkBCType_10.Name = "_chkBCType_10";
            this._chkBCType_10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_10.Size = new System.Drawing.Size(112, 27);
            this._chkBCType_10.TabIndex = 11;
            this._chkBCType_10.Text = "Code 93";
            this._chkBCType_10.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_11
            // 
            this._chkBCType_11.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_11.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_11.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_11.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_11.Location = new System.Drawing.Point(117, 106);
            this._chkBCType_11.Name = "_chkBCType_11";
            this._chkBCType_11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_11.Size = new System.Drawing.Size(112, 28);
            this._chkBCType_11.TabIndex = 14;
            this._chkBCType_11.Text = "Code 128";
            this._chkBCType_11.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_12
            // 
            this._chkBCType_12.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_12.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_12.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_12.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_12.Location = new System.Drawing.Point(117, 130);
            this._chkBCType_12.Name = "_chkBCType_12";
            this._chkBCType_12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_12.Size = new System.Drawing.Size(112, 29);
            this._chkBCType_12.TabIndex = 17;
            this._chkBCType_12.Text = "EAN 13";
            this._chkBCType_12.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_13
            // 
            this._chkBCType_13.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_13.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_13.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_13.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_13.Location = new System.Drawing.Point(117, 153);
            this._chkBCType_13.Name = "_chkBCType_13";
            this._chkBCType_13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_13.Size = new System.Drawing.Size(112, 28);
            this._chkBCType_13.TabIndex = 19;
            this._chkBCType_13.Text = "EAN 8";
            this._chkBCType_13.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_14
            // 
            this._chkBCType_14.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_14.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_14.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_14.Location = new System.Drawing.Point(235, 36);
            this._chkBCType_14.Name = "_chkBCType_14";
            this._chkBCType_14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_14.Size = new System.Drawing.Size(96, 25);
            this._chkBCType_14.TabIndex = 3;
            this._chkBCType_14.Text = "UPC-A";
            this._chkBCType_14.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_15
            // 
            this._chkBCType_15.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_15.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_15.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_15.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_15.Location = new System.Drawing.Point(235, 62);
            this._chkBCType_15.Name = "_chkBCType_15";
            this._chkBCType_15.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_15.Size = new System.Drawing.Size(96, 24);
            this._chkBCType_15.TabIndex = 6;
            this._chkBCType_15.Text = "UPC-E";
            this._chkBCType_15.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_16
            // 
            this._chkBCType_16.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_16.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_16.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_16.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_16.Location = new System.Drawing.Point(235, 89);
            this._chkBCType_16.Name = "_chkBCType_16";
            this._chkBCType_16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_16.Size = new System.Drawing.Size(96, 22);
            this._chkBCType_16.TabIndex = 9;
            this._chkBCType_16.Text = "ADD 5";
            this._chkBCType_16.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_17
            // 
            this._chkBCType_17.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_17.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_17.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_17.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_17.Location = new System.Drawing.Point(235, 113);
            this._chkBCType_17.Name = "_chkBCType_17";
            this._chkBCType_17.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_17.Size = new System.Drawing.Size(96, 17);
            this._chkBCType_17.TabIndex = 12;
            this._chkBCType_17.Text = "Add 2";
            this._chkBCType_17.UseVisualStyleBackColor = false;
            // 
            // _chkBCType_18
            // 
            this._chkBCType_18.BackColor = System.Drawing.SystemColors.Control;
            this._chkBCType_18.Cursor = System.Windows.Forms.Cursors.Default;
            this._chkBCType_18.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._chkBCType_18.ForeColor = System.Drawing.SystemColors.ControlText;
            this._chkBCType_18.Location = new System.Drawing.Point(235, 136);
            this._chkBCType_18.Name = "_chkBCType_18";
            this._chkBCType_18.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._chkBCType_18.Size = new System.Drawing.Size(96, 17);
            this._chkBCType_18.TabIndex = 15;
            this._chkBCType_18.Text = "UCC/EAN 128";
            this._chkBCType_18.UseVisualStyleBackColor = false;
            // 
            // cmdAll
            // 
            this.cmdAll.BackColor = System.Drawing.SystemColors.Control;
            this.cmdAll.Cursor = System.Windows.Forms.Cursors.Default;
            this.cmdAll.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdAll.Location = new System.Drawing.Point(234, 189);
            this.cmdAll.Name = "cmdAll";
            this.cmdAll.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmdAll.Size = new System.Drawing.Size(97, 23);
            this.cmdAll.TabIndex = 20;
            this.cmdAll.Text = "&All (unknown)";
            this.cmdAll.UseVisualStyleBackColor = false;
            this.cmdAll.Click += new System.EventHandler(this.cmdAll_Click);
            // 
            // fraType
            // 
            this.fraType.BackColor = System.Drawing.SystemColors.Control;
            this.fraType.Controls.Add(this.OneCodeRadioButton);
            this.fraType.Controls.Add(this.RoyalRadioButton);
            this.fraType.Controls.Add(this.AusRadioButton);
            this.fraType.Controls.Add(this.QRRadioButton);
            this.fraType.Controls.Add(this.optTypePostNet);
            this.fraType.Controls.Add(this.optTypeDataMatrix);
            this.fraType.Controls.Add(this.optType1D);
            this.fraType.Controls.Add(this.optTypePFD417);
            this.fraType.Controls.Add(this.optTypePatchCode);
            this.fraType.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fraType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fraType.Location = new System.Drawing.Point(16, 8);
            this.fraType.Name = "fraType";
            this.fraType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraType.Size = new System.Drawing.Size(292, 112);
            this.fraType.TabIndex = 1;
            this.fraType.TabStop = false;
            this.fraType.Text = "Barcode Type: ";
            // 
            // OneCodeRadioButton
            // 
            this.OneCodeRadioButton.BackColor = System.Drawing.SystemColors.Control;
            this.OneCodeRadioButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.OneCodeRadioButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OneCodeRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.OneCodeRadioButton.Location = new System.Drawing.Point(136, 63);
            this.OneCodeRadioButton.Name = "OneCodeRadioButton";
            this.OneCodeRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OneCodeRadioButton.Size = new System.Drawing.Size(96, 17);
            this.OneCodeRadioButton.TabIndex = 9;
            this.OneCodeRadioButton.TabStop = true;
            this.OneCodeRadioButton.Text = "IntelligentMail";
            this.OneCodeRadioButton.UseVisualStyleBackColor = false;
            this.OneCodeRadioButton.CheckedChanged += new System.EventHandler(this.OneCodeRadioButton_CheckedChanged);
            // 
            // RoyalRadioButton
            // 
            this.RoyalRadioButton.BackColor = System.Drawing.SystemColors.Control;
            this.RoyalRadioButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.RoyalRadioButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoyalRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.RoyalRadioButton.Location = new System.Drawing.Point(8, 86);
            this.RoyalRadioButton.Name = "RoyalRadioButton";
            this.RoyalRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RoyalRadioButton.Size = new System.Drawing.Size(87, 17);
            this.RoyalRadioButton.TabIndex = 8;
            this.RoyalRadioButton.TabStop = true;
            this.RoyalRadioButton.Text = "Royal Post";
            this.RoyalRadioButton.UseVisualStyleBackColor = false;
            this.RoyalRadioButton.CheckedChanged += new System.EventHandler(this.RoyalRadioButton_CheckedChanged);
            // 
            // AusRadioButton
            // 
            this.AusRadioButton.BackColor = System.Drawing.SystemColors.Control;
            this.AusRadioButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.AusRadioButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AusRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AusRadioButton.Location = new System.Drawing.Point(8, 63);
            this.AusRadioButton.Name = "AusRadioButton";
            this.AusRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AusRadioButton.Size = new System.Drawing.Size(112, 17);
            this.AusRadioButton.TabIndex = 7;
            this.AusRadioButton.TabStop = true;
            this.AusRadioButton.Text = "Australian Post";
            this.AusRadioButton.UseVisualStyleBackColor = false;
            this.AusRadioButton.CheckedChanged += new System.EventHandler(this.AusRadioButton_CheckedChanged);
            // 
            // QRRadioButton
            // 
            this.QRRadioButton.BackColor = System.Drawing.SystemColors.Control;
            this.QRRadioButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.QRRadioButton.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QRRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.QRRadioButton.Location = new System.Drawing.Point(243, 17);
            this.QRRadioButton.Name = "QRRadioButton";
            this.QRRadioButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.QRRadioButton.Size = new System.Drawing.Size(43, 17);
            this.QRRadioButton.TabIndex = 6;
            this.QRRadioButton.TabStop = true;
            this.QRRadioButton.Text = "QR";
            this.QRRadioButton.UseVisualStyleBackColor = false;
            this.QRRadioButton.CheckedChanged += new System.EventHandler(this.QRRadioButton_CheckedChanged);
            // 
            // optTypePostNet
            // 
            this.optTypePostNet.BackColor = System.Drawing.SystemColors.Control;
            this.optTypePostNet.Cursor = System.Windows.Forms.Cursors.Default;
            this.optTypePostNet.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTypePostNet.ForeColor = System.Drawing.SystemColors.ControlText;
            this.optTypePostNet.Location = new System.Drawing.Point(8, 40);
            this.optTypePostNet.Name = "optTypePostNet";
            this.optTypePostNet.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optTypePostNet.Size = new System.Drawing.Size(112, 17);
            this.optTypePostNet.TabIndex = 4;
            this.optTypePostNet.TabStop = true;
            this.optTypePostNet.Text = "Post&Net";
            this.optTypePostNet.UseVisualStyleBackColor = false;
            this.optTypePostNet.CheckedChanged += new System.EventHandler(this.optTypePostNet_CheckedChanged);
            // 
            // optTypeDataMatrix
            // 
            this.optTypeDataMatrix.BackColor = System.Drawing.SystemColors.Control;
            this.optTypeDataMatrix.Cursor = System.Windows.Forms.Cursors.Default;
            this.optTypeDataMatrix.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTypeDataMatrix.ForeColor = System.Drawing.SystemColors.ControlText;
            this.optTypeDataMatrix.Location = new System.Drawing.Point(136, 40);
            this.optTypeDataMatrix.Name = "optTypeDataMatrix";
            this.optTypeDataMatrix.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optTypeDataMatrix.Size = new System.Drawing.Size(112, 17);
            this.optTypeDataMatrix.TabIndex = 5;
            this.optTypeDataMatrix.TabStop = true;
            this.optTypeDataMatrix.Text = "&Data Matrix";
            this.optTypeDataMatrix.UseVisualStyleBackColor = false;
            this.optTypeDataMatrix.CheckedChanged += new System.EventHandler(this.optTypeDataMatrix_CheckedChanged);
            // 
            // optType1D
            // 
            this.optType1D.BackColor = System.Drawing.SystemColors.Control;
            this.optType1D.Cursor = System.Windows.Forms.Cursors.Default;
            this.optType1D.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optType1D.ForeColor = System.Drawing.SystemColors.ControlText;
            this.optType1D.Location = new System.Drawing.Point(8, 16);
            this.optType1D.Name = "optType1D";
            this.optType1D.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optType1D.Size = new System.Drawing.Size(112, 17);
            this.optType1D.TabIndex = 1;
            this.optType1D.TabStop = true;
            this.optType1D.Text = "&1D (standard)";
            this.optType1D.UseVisualStyleBackColor = false;
            this.optType1D.CheckedChanged += new System.EventHandler(this.optType1D_CheckedChanged);
            // 
            // optTypePFD417
            // 
            this.optTypePFD417.BackColor = System.Drawing.SystemColors.Control;
            this.optTypePFD417.Cursor = System.Windows.Forms.Cursors.Default;
            this.optTypePFD417.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTypePFD417.ForeColor = System.Drawing.SystemColors.ControlText;
            this.optTypePFD417.Location = new System.Drawing.Point(136, 86);
            this.optTypePFD417.Name = "optTypePFD417";
            this.optTypePFD417.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optTypePFD417.Size = new System.Drawing.Size(96, 17);
            this.optTypePFD417.TabIndex = 3;
            this.optTypePFD417.TabStop = true;
            this.optTypePFD417.Text = "PDF-&417";
            this.optTypePFD417.UseVisualStyleBackColor = false;
            this.optTypePFD417.CheckedChanged += new System.EventHandler(this.optTypePFD417_CheckedChanged);
            // 
            // optTypePatchCode
            // 
            this.optTypePatchCode.BackColor = System.Drawing.SystemColors.Control;
            this.optTypePatchCode.Cursor = System.Windows.Forms.Cursors.Default;
            this.optTypePatchCode.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTypePatchCode.ForeColor = System.Drawing.SystemColors.ControlText;
            this.optTypePatchCode.Location = new System.Drawing.Point(136, 16);
            this.optTypePatchCode.Name = "optTypePatchCode";
            this.optTypePatchCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.optTypePatchCode.Size = new System.Drawing.Size(112, 17);
            this.optTypePatchCode.TabIndex = 2;
            this.optTypePatchCode.TabStop = true;
            this.optTypePatchCode.Text = "&Patch Code";
            this.optTypePatchCode.UseVisualStyleBackColor = false;
            this.optTypePatchCode.CheckedChanged += new System.EventHandler(this.optTypePatchCode_CheckedChanged);
            // 
            // tabPageOrientation
            // 
            this.tabPageOrientation.Controls.Add(this.fraOrient);
            this.tabPageOrientation.Location = new System.Drawing.Point(4, 22);
            this.tabPageOrientation.Name = "tabPageOrientation";
            this.tabPageOrientation.Size = new System.Drawing.Size(514, 352);
            this.tabPageOrientation.TabIndex = 1;
            this.tabPageOrientation.Text = "Orientation";
            // 
            // fraOrient
            // 
            this.fraOrient.BackColor = System.Drawing.SystemColors.Control;
            this.fraOrient.Controls.Add(this.DiagRadioButton);
            this.fraOrient.Controls.Add(this.OrientationLabel);
            this.fraOrient.Controls.Add(this._optOrient_2);
            this.fraOrient.Controls.Add(this._optOrient_1);
            this.fraOrient.Controls.Add(this._optOrient_0);
            this.fraOrient.Cursor = System.Windows.Forms.Cursors.Default;
            this.fraOrient.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fraOrient.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fraOrient.Location = new System.Drawing.Point(36, 51);
            this.fraOrient.Name = "fraOrient";
            this.fraOrient.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraOrient.Size = new System.Drawing.Size(381, 168);
            this.fraOrient.TabIndex = 11;
            // 
            // DiagRadioButton
            // 
            this.DiagRadioButton.AutoSize = true;
            this.DiagRadioButton.Location = new System.Drawing.Point(104, 135);
            this.DiagRadioButton.Name = "DiagRadioButton";
            this.DiagRadioButton.Size = new System.Drawing.Size(259, 18);
            this.DiagRadioButton.TabIndex = 13;
            this.DiagRadioButton.TabStop = true;
            this.DiagRadioButton.Text = "Vertical, Horizontal, 45 Degree Angled Barcodes";
            this.DiagRadioButton.UseVisualStyleBackColor = true;
            // 
            // OrientationLabel
            // 
            this.OrientationLabel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrientationLabel.Location = new System.Drawing.Point(16, 14);
            this.OrientationLabel.Name = "OrientationLabel";
            this.OrientationLabel.Size = new System.Drawing.Size(171, 23);
            this.OrientationLabel.TabIndex = 12;
            this.OrientationLabel.Text = "Only applies to 1D barcodes:";
            // 
            // _optOrient_2
            // 
            this._optOrient_2.BackColor = System.Drawing.SystemColors.Control;
            this._optOrient_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._optOrient_2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._optOrient_2.ForeColor = System.Drawing.SystemColors.ControlText;
            this._optOrient_2.Location = new System.Drawing.Point(104, 104);
            this._optOrient_2.Name = "_optOrient_2";
            this._optOrient_2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optOrient_2.Size = new System.Drawing.Size(205, 17);
            this._optOrient_2.TabIndex = 3;
            this._optOrient_2.TabStop = true;
            this._optOrient_2.Text = "Vertical and Horizontal Barcodes";
            this._optOrient_2.UseVisualStyleBackColor = false;
            // 
            // _optOrient_1
            // 
            this._optOrient_1.BackColor = System.Drawing.SystemColors.Control;
            this._optOrient_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._optOrient_1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._optOrient_1.ForeColor = System.Drawing.SystemColors.ControlText;
            this._optOrient_1.Location = new System.Drawing.Point(104, 72);
            this._optOrient_1.Name = "_optOrient_1";
            this._optOrient_1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optOrient_1.Size = new System.Drawing.Size(121, 17);
            this._optOrient_1.TabIndex = 2;
            this._optOrient_1.TabStop = true;
            this._optOrient_1.Text = "Horizontal Barcodes";
            this._optOrient_1.UseVisualStyleBackColor = false;
            // 
            // _optOrient_0
            // 
            this._optOrient_0.BackColor = System.Drawing.SystemColors.Control;
            this._optOrient_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._optOrient_0.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._optOrient_0.ForeColor = System.Drawing.SystemColors.ControlText;
            this._optOrient_0.Location = new System.Drawing.Point(104, 40);
            this._optOrient_0.Name = "_optOrient_0";
            this._optOrient_0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._optOrient_0.Size = new System.Drawing.Size(113, 17);
            this._optOrient_0.TabIndex = 1;
            this._optOrient_0.TabStop = true;
            this._optOrient_0.Text = "Vertical Barcodes";
            this._optOrient_0.UseVisualStyleBackColor = false;
            // 
            // tabPageMisc
            // 
            this.tabPageMisc.Controls.Add(this.fraMisc);
            this.tabPageMisc.Location = new System.Drawing.Point(4, 22);
            this.tabPageMisc.Name = "tabPageMisc";
            this.tabPageMisc.Size = new System.Drawing.Size(514, 352);
            this.tabPageMisc.TabIndex = 2;
            this.tabPageMisc.Text = "Miscellaneous";
            // 
            // fraMisc
            // 
            this.fraMisc.BackColor = System.Drawing.SystemColors.Control;
            this.fraMisc.Controls.Add(this.ScanLabel3);
            this.fraMisc.Controls.Add(this.DistanceLabel2);
            this.fraMisc.Controls.Add(this.ScanLabel);
            this.fraMisc.Controls.Add(this.ScanBox);
            this.fraMisc.Controls.Add(this.ChecksumLabel);
            this.fraMisc.Controls.Add(this.CheckSumCheckBox);
            this.fraMisc.Controls.Add(this.Label3);
            this.fraMisc.Controls.Add(this.txtMax);
            this.fraMisc.Controls.Add(this.cboColor);
            this.fraMisc.Controls.Add(this.Label1);
            this.fraMisc.Cursor = System.Windows.Forms.Cursors.Default;
            this.fraMisc.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fraMisc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fraMisc.Location = new System.Drawing.Point(32, 51);
            this.fraMisc.Name = "fraMisc";
            this.fraMisc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fraMisc.Size = new System.Drawing.Size(452, 169);
            this.fraMisc.TabIndex = 15;
            // 
            // ScanLabel3
            // 
            this.ScanLabel3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScanLabel3.Location = new System.Drawing.Point(283, 14);
            this.ScanLabel3.Name = "ScanLabel3";
            this.ScanLabel3.Size = new System.Drawing.Size(155, 23);
            this.ScanLabel3.TabIndex = 13;
            this.ScanLabel3.Text = "(Only applies to 1D barcodes:)";
            // 
            // DistanceLabel2
            // 
            this.DistanceLabel2.AutoSize = true;
            this.DistanceLabel2.Location = new System.Drawing.Point(240, 14);
            this.DistanceLabel2.Name = "DistanceLabel2";
            this.DistanceLabel2.Size = new System.Drawing.Size(37, 14);
            this.DistanceLabel2.TabIndex = 9;
            this.DistanceLabel2.Text = "(1-10)";
            // 
            // ScanLabel
            // 
            this.ScanLabel.AutoSize = true;
            this.ScanLabel.Location = new System.Drawing.Point(86, 14);
            this.ScanLabel.Name = "ScanLabel";
            this.ScanLabel.Size = new System.Drawing.Size(77, 14);
            this.ScanLabel.TabIndex = 8;
            this.ScanLabel.Text = "ScanDistance:";
            // 
            // ScanBox
            // 
            this.ScanBox.Location = new System.Drawing.Point(177, 12);
            this.ScanBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.ScanBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ScanBox.Name = "ScanBox";
            this.ScanBox.Size = new System.Drawing.Size(57, 20);
            this.ScanBox.TabIndex = 7;
            this.ScanBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // ChecksumLabel
            // 
            this.ChecksumLabel.AutoSize = true;
            this.ChecksumLabel.Location = new System.Drawing.Point(198, 123);
            this.ChecksumLabel.Name = "ChecksumLabel";
            this.ChecksumLabel.Size = new System.Drawing.Size(96, 14);
            this.ChecksumLabel.TabIndex = 6;
            this.ChecksumLabel.Text = "AppendCheckSum";
            // 
            // CheckSumCheckBox
            // 
            this.CheckSumCheckBox.AutoSize = true;
            this.CheckSumCheckBox.Location = new System.Drawing.Point(177, 123);
            this.CheckSumCheckBox.Name = "CheckSumCheckBox";
            this.CheckSumCheckBox.Size = new System.Drawing.Size(15, 14);
            this.CheckSumCheckBox.TabIndex = 5;
            this.CheckSumCheckBox.UseVisualStyleBackColor = true;
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.SystemColors.Control;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(32, 80);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label3.Size = new System.Drawing.Size(145, 13);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "Max. Barcodes Per Image: ";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtMax
            // 
            this.txtMax.AcceptsReturn = true;
            this.txtMax.BackColor = System.Drawing.SystemColors.Window;
            this.txtMax.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMax.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMax.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtMax.Location = new System.Drawing.Point(177, 80);
            this.txtMax.MaxLength = 0;
            this.txtMax.Name = "txtMax";
            this.txtMax.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMax.Size = new System.Drawing.Size(73, 20);
            this.txtMax.TabIndex = 4;
            // 
            // cboColor
            // 
            this.cboColor.BackColor = System.Drawing.SystemColors.Window;
            this.cboColor.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboColor.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboColor.ForeColor = System.Drawing.SystemColors.WindowText;
            this.cboColor.Location = new System.Drawing.Point(177, 48);
            this.cboColor.Name = "cboColor";
            this.cboColor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboColor.Size = new System.Drawing.Size(129, 22);
            this.cboColor.TabIndex = 2;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(32, 48);
            this.Label1.Name = "Label1";
            this.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label1.Size = new System.Drawing.Size(145, 13);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Barcode Color: ";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(215, 392);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(120, 24);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(87, 392);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(120, 24);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.button2_Click);
            // 
            // FormOptions
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(542, 422);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormOptions";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Options...";
            this.Load += new System.EventHandler(this.FormOptions_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageType.ResumeLayout(false);
            this.AusGroupBox.ResumeLayout(false);
            this.AusGroupBox.PerformLayout();
            this.frm1DTypes.ResumeLayout(false);
            this.frm1DTypes.PerformLayout();
            this.fraType.ResumeLayout(false);
            this.tabPageOrientation.ResumeLayout(false);
            this.fraOrient.ResumeLayout(false);
            this.fraOrient.PerformLayout();
            this.tabPageMisc.ResumeLayout(false);
            this.fraMisc.ResumeLayout(false);
            this.fraMisc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScanBox)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public System.Array BarcodeTypes
		{
			get {return _nBarcodeTypes;}
			set {_nBarcodeTypes = value;}
		}

		public InkColorInfo BarcodeInkColor
		{
			get { return _barcodeInkColor; }
			set { _barcodeInkColor = value; }
		}

		public System.Int32 MaxBarcodes
		{
			get { return _nMaxBarcodes; }
			set { _nMaxBarcodes = value; }
		}

		public OrientationInfo BarcodeOrientation
		{
			get { return _barcodeOrientation; }
			set { _barcodeOrientation = value; } 
		}

		private void FormOptions_Load(object sender, System.EventArgs e)
		{		
			cboColor.Items.Add ("Unknown Color");
			cboColor.Items.Add ("White");
			cboColor.Items.Add ("Black");
			switch (_barcodeInkColor)
			{
				case InkColorInfo.Black:
				{
					cboColor.SelectedIndex = 2;
					break;
				}

				case InkColorInfo.White:
				{
					cboColor.SelectedIndex = 1;
					break;
				}

				case InkColorInfo.Unknown:
				{
					cboColor.SelectedIndex = 0;
					break;
				}
			}
			txtMax.Text = _nMaxBarcodes.ToString ();
			
			_optOrient_0.Checked = false;
			_optOrient_1.Checked = false;
			_optOrient_2.Checked = false;

			switch (_barcodeOrientation)
			{
				case OrientationInfo.HorizontalVertical:
				{
					_optOrient_2.Checked = true;
					break;
				}

				case OrientationInfo.Vertical:
				{
					_optOrient_0.Checked = true;
					break;
				}
					
				case OrientationInfo.Horizontal:
				{
					_optOrient_1.Checked = true;
					break;
				}
                case OrientationInfo.HorizontalVerticalDiagonal:
                {
                    DiagRadioButton.Checked = true;
                    break;
                }
			}

            if (_chkBCType_0.Enabled == true & _chkBCType_1.Enabled == true & _chkBCType_2.Enabled == true & _chkBCType_3.Enabled == true & _chkBCType_4.Enabled == true & _chkBCType_5.Enabled == true & _chkBCType_6.Enabled == true & _chkBCType_7.Enabled == true & _chkBCType_8.Enabled == true & _chkBCType_9.Enabled == true & _chkBCType_10.Enabled == true & _chkBCType_11.Enabled == true & _chkBCType_12.Enabled == true & _chkBCType_13.Enabled == true & _chkBCType_14.Enabled == true & _chkBCType_15.Enabled == true & _chkBCType_16.Enabled == true & _chkBCType_17.Enabled == true & _chkBCType_18.Enabled == true)
            {
                optType1D.Checked = true;
            }
		}



		private System.Array GetBarcodes()
		{
			// Set the barcode type
			int currentCount = 0;

            if (optType1D.Checked == true)
            {
                if (_chkBCType_0.Checked)
                    currentCount++;
                if (_chkBCType_1.Checked)
                    currentCount++;
                if (_chkBCType_2.Checked)
                    currentCount++;
                if (_chkBCType_3.Checked)
                    currentCount++;
                if (_chkBCType_4.Checked)
                    currentCount++;
                if (_chkBCType_5.Checked)
                    currentCount++;
                if (_chkBCType_6.Checked)
                    currentCount++;
                if (_chkBCType_7.Checked)
                    currentCount++;
                if (_chkBCType_8.Checked)
                    currentCount++;
                if (_chkBCType_9.Checked)
                    currentCount++;
                if (_chkBCType_10.Checked)
                    currentCount++;
                if (_chkBCType_11.Checked)
                    currentCount++;
                if (_chkBCType_12.Checked)
                    currentCount++;
                if (_chkBCType_13.Checked)
                    currentCount++;
                if (_chkBCType_14.Checked)
                    currentCount++;
                if (_chkBCType_15.Checked)
                    currentCount++;
                if (_chkBCType_16.Checked)
                    currentCount++;
                if (_chkBCType_17.Checked)
                    currentCount++;
                if (_chkBCType_18.Checked)
                    currentCount++;
                if (Code39ExtCheckBox.Checked)
                    currentCount++;
                if (Code93ExtCheckBox.Checked)
                    currentCount++;
            }
			if (optTypePFD417.Checked)
				currentCount++;
			if (optTypePatchCode.Checked)
				currentCount++;
			if (optTypeDataMatrix.Checked)
				currentCount++;
			if (optTypePostNet.Checked)
				currentCount++;
            if (QRRadioButton.Checked)
                currentCount++;
            if (AusRadioButton.Checked)
                currentCount++;
            if (RoyalRadioButton.Checked)
                currentCount++;
            if (OneCodeRadioButton.Checked)
                currentCount++;

			System.Array currentBarcodeTypes = new BarcodeType[currentCount];
			if (currentCount > 0)
			{
				currentCount = 0;
                if (optType1D.Checked == true)
                {
                    if (_chkBCType_0.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Industry2of5Barcode, currentCount++);
                    if (_chkBCType_1.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Interleaved2of5Barcode, currentCount++);
                    if (_chkBCType_2.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.IATA2of5Barcode, currentCount++);
                    if (_chkBCType_3.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataLogic2of5Barcode, currentCount++);
                    if (_chkBCType_4.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Invert2of5Barcode, currentCount++);
                    if (_chkBCType_5.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.BCDMatrixBarcode, currentCount++);
                    if (_chkBCType_6.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Matrix2of5Barcode, currentCount++);
                    if (_chkBCType_7.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code32Barcode, currentCount++);
                    if (_chkBCType_8.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode, currentCount++);
                    if (_chkBCType_9.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.CodabarBarcode, currentCount++);
                    if (_chkBCType_10.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93Barcode, currentCount++);
                    if (_chkBCType_11.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, currentCount++);
                    if (_chkBCType_12.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN13Barcode, currentCount++);
                    if (_chkBCType_13.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN8Barcode, currentCount++);
                    if (_chkBCType_14.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCABarcode, currentCount++);
                    if (_chkBCType_15.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCEBarcode, currentCount++);
                    if (_chkBCType_16.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Add5Barcode, currentCount++);
                    if (_chkBCType_17.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Add2Barcode, currentCount++);
                    if (_chkBCType_18.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN128Barcode, currentCount++);
                    if (Code39ExtCheckBox.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode, currentCount++);
                    if (Code93ExtCheckBox.Checked)
                        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93ExtendedBarcode, currentCount++);


                }
				if (optTypePFD417.Checked)
					currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, currentCount++);
				if (optTypePatchCode.Checked)
					currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode, currentCount++);
				if (optTypeDataMatrix.Checked)
					currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, currentCount++);
				if (optTypePostNet.Checked)
					currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PostNetBarcode, currentCount++);
                if (QRRadioButton.Checked)
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.QRCodeBarcode, currentCount++);
                if (AusRadioButton.Checked)
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.AustralianPost4StateBarcode, currentCount++);
                if (RoyalRadioButton.Checked)
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.RoyalPost4StateBarcode, currentCount++);
                if (OneCodeRadioButton.Checked)
                    currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.IntelligentMailBarcode, currentCount++);

			}
			return currentBarcodeTypes;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			this._nBarcodeTypes = GetBarcodes();
		
			
			if(_optOrient_0.Checked)
				_barcodeOrientation = OrientationInfo.Vertical;

			if (_optOrient_1.Checked)
				_barcodeOrientation = OrientationInfo.Horizontal;

			if (_optOrient_2.Checked)
				_barcodeOrientation = OrientationInfo.HorizontalVertical;

            if (DiagRadioButton.Checked)
                _barcodeOrientation = OrientationInfo.HorizontalVerticalDiagonal;
    
			switch (cboColor.SelectedIndex)
			{
				case 0:
				{
					_barcodeInkColor = InkColorInfo.Unknown;
					break;
				}

				case 1:
				{	
					_barcodeInkColor = InkColorInfo.White;
					break;
				}

				case 2:
				{
					_barcodeInkColor = InkColorInfo.Black;
					break;
				}
			}

			_nMaxBarcodes = Int32.Parse (txtMax.Text);
			this.Close ();
		}

		private void cmdAll_Click(object sender, System.EventArgs e)
		{
			_chkBCType_0.Checked = true;
			_chkBCType_1.Checked = true;
			_chkBCType_2.Checked = true;
			_chkBCType_3.Checked = true;
			_chkBCType_4.Checked = true;
			_chkBCType_5.Checked = true;
			_chkBCType_6.Checked = true;
			_chkBCType_7.Checked = true;
			_chkBCType_8.Checked = true;
			_chkBCType_9.Checked = true;
			_chkBCType_10.Checked = true;
			_chkBCType_11.Checked = true;
			_chkBCType_12.Checked = true;
			_chkBCType_13.Checked = true;
			_chkBCType_14.Checked = true;
			_chkBCType_15.Checked = true;
			_chkBCType_16.Checked = true;
			_chkBCType_17.Checked = true;
			_chkBCType_18.Checked = true;
            Code39ExtCheckBox.Checked = true;
            Code93ExtCheckBox.Checked = true;
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			_chkBCType_0.Checked = false;
			_chkBCType_1.Checked = false;
			_chkBCType_2.Checked = false;
			_chkBCType_3.Checked = false;
			_chkBCType_4.Checked = false;
			_chkBCType_5.Checked = false;
			_chkBCType_6.Checked = false;
			_chkBCType_7.Checked = false;
			_chkBCType_8.Checked = false;
			_chkBCType_9.Checked = false;
			_chkBCType_10.Checked = false;
			_chkBCType_11.Checked = false;
			_chkBCType_12.Checked = false;
			_chkBCType_13.Checked = false;
			_chkBCType_14.Checked = false;
			_chkBCType_15.Checked = false;
			_chkBCType_16.Checked = false;
			_chkBCType_17.Checked = false;
			_chkBCType_18.Checked = false;
            Code39ExtCheckBox.Checked = false;
            Code93ExtCheckBox.Checked = false;
		}

		private void optType1D_CheckedChanged(object sender, System.EventArgs e)
		{
			bool onOff;

			onOff = optType1D.Checked;
			_chkBCType_0.Enabled = onOff;
			_chkBCType_1.Enabled = onOff;
			_chkBCType_2.Enabled = onOff;
			_chkBCType_3.Enabled = onOff;
			_chkBCType_4.Enabled = onOff;
			_chkBCType_5.Enabled = onOff;
			_chkBCType_6.Enabled = onOff;
			_chkBCType_7.Enabled = onOff;
			_chkBCType_8.Enabled = onOff;
			_chkBCType_9.Enabled = onOff;
			_chkBCType_10.Enabled = onOff;
			_chkBCType_11.Enabled = onOff;
			_chkBCType_12.Enabled = onOff;
			_chkBCType_13.Enabled = onOff;
			_chkBCType_14.Enabled = onOff;
			_chkBCType_15.Enabled = onOff;
			_chkBCType_16.Enabled = onOff;
			_chkBCType_17.Enabled = onOff;
			_chkBCType_18.Enabled = onOff;
            Code39ExtCheckBox.Enabled = onOff;
            Code93ExtCheckBox.Enabled = onOff;

        }

        private void RoyalRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void optTypePostNet_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void optTypePFD417_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void OneCodeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void optTypeDataMatrix_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void optTypePatchCode_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void QRRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);
        }

        private void AusRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            optType1D_CheckedChanged(sender, e);

            if (AusRadioButton.Checked == true)
            {
                AusGroupBox.Enabled = true;
            }
            else
            {
                AusGroupBox.Enabled = false;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Cancel = true;
            Close();
        }
	}
}

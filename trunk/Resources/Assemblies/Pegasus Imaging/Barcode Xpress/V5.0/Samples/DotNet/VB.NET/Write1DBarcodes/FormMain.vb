'****************************************************************
'* Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Option Explicit On
Option Strict On

Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.BarcodeXpress5
Imports System.Drawing.Printing
Imports System.Runtime.InteropServices

Public Class FormMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '***Must call the UnlockControl method to unlock the control
	'The unlock codes shown below are for formatting purposes only and will not work!
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (ImageXView1.Image Is Nothing) Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (BarcodeXpress1 Is Nothing) Then
                BarcodeXpress1.Dispose()
                BarcodeXpress1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents hScrollBarBCHeight As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarMinBar As System.Windows.Forms.HScrollBar
    Friend WithEvents labelUPCGap As System.Windows.Forms.Label
    Friend WithEvents labelValueFont As System.Windows.Forms.Label
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents labelMinBarVal As System.Windows.Forms.Label
    Friend WithEvents hScrollBarValueGap As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarUPCNotch As System.Windows.Forms.HScrollBar
    Friend WithEvents labelUPCFont As System.Windows.Forms.Label
    Friend WithEvents labelUPCNotch As System.Windows.Forms.Label
    Friend WithEvents labelValueGap As System.Windows.Forms.Label
    Friend WithEvents labelBCHeight As System.Windows.Forms.Label
    Friend WithEvents labelMinBar As System.Windows.Forms.Label
    Friend WithEvents labelBCValue As System.Windows.Forms.Label
    Friend WithEvents comboBoxStyle As System.Windows.Forms.ComboBox
    Friend WithEvents labelStyle As System.Windows.Forms.Label
    Friend WithEvents mainMenu As System.Windows.Forms.MainMenu
    Friend WithEvents picImagXpress As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents labelHeightVal As System.Windows.Forms.Label
    Friend WithEvents labelUPCFontVal As System.Windows.Forms.Label
    Friend WithEvents labelValueFontVal As System.Windows.Forms.Label
    Friend WithEvents labelUPCGapVal As System.Windows.Forms.Label
    Friend WithEvents labelValueGapVal As System.Windows.Forms.Label
    Friend WithEvents labelUPCNotchVal As System.Windows.Forms.Label
    Friend WithEvents textBoxBCValue As System.Windows.Forms.TextBox
    Friend WithEvents hScrollBarUPCGap As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarValueFont As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarUPCFont As System.Windows.Forms.HScrollBar
    Friend WithEvents buttonPrint As System.Windows.Forms.Button
    Friend WithEvents buttonCreate As System.Windows.Forms.Button
    Friend WithEvents labelResult As System.Windows.Forms.Label
    Friend WithEvents labelInfo As System.Windows.Forms.Label
    Friend WithEvents labelStatus As System.Windows.Forms.Label
    Friend results() As PegasusImaging.WinForms.barcodexpress5.Result
    Friend WithEvents buttonWrite As System.Windows.Forms.Button
    Friend WithEvents AboutMenu As System.Windows.Forms.MenuItem
    Friend WithEvents BarcodeXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents BarcodeXpress1 As PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.hScrollBarBCHeight = New System.Windows.Forms.HScrollBar
        Me.hScrollBarMinBar = New System.Windows.Forms.HScrollBar
        Me.labelUPCGap = New System.Windows.Forms.Label
        Me.labelValueFont = New System.Windows.Forms.Label
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem
        Me.FileMenu = New System.Windows.Forms.MenuItem
        Me.labelMinBarVal = New System.Windows.Forms.Label
        Me.hScrollBarValueGap = New System.Windows.Forms.HScrollBar
        Me.hScrollBarUPCNotch = New System.Windows.Forms.HScrollBar
        Me.labelUPCFont = New System.Windows.Forms.Label
        Me.labelUPCNotch = New System.Windows.Forms.Label
        Me.labelValueGap = New System.Windows.Forms.Label
        Me.labelBCHeight = New System.Windows.Forms.Label
        Me.labelMinBar = New System.Windows.Forms.Label
        Me.labelBCValue = New System.Windows.Forms.Label
        Me.comboBoxStyle = New System.Windows.Forms.ComboBox
        Me.labelStyle = New System.Windows.Forms.Label
        Me.mainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.AboutMenu = New System.Windows.Forms.MenuItem
        Me.BarcodeXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.labelHeightVal = New System.Windows.Forms.Label
        Me.labelUPCFontVal = New System.Windows.Forms.Label
        Me.labelValueFontVal = New System.Windows.Forms.Label
        Me.labelUPCGapVal = New System.Windows.Forms.Label
        Me.labelValueGapVal = New System.Windows.Forms.Label
        Me.labelUPCNotchVal = New System.Windows.Forms.Label
        Me.textBoxBCValue = New System.Windows.Forms.TextBox
        Me.hScrollBarUPCGap = New System.Windows.Forms.HScrollBar
        Me.hScrollBarValueFont = New System.Windows.Forms.HScrollBar
        Me.hScrollBarUPCFont = New System.Windows.Forms.HScrollBar
        Me.buttonPrint = New System.Windows.Forms.Button
        Me.buttonCreate = New System.Windows.Forms.Button
        Me.labelResult = New System.Windows.Forms.Label
        Me.labelInfo = New System.Windows.Forms.Label
        Me.labelStatus = New System.Windows.Forms.Label
        Me.buttonWrite = New System.Windows.Forms.Button
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.BarcodeXpress1 = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.SuspendLayout()
        '
        'hScrollBarBCHeight
        '
        Me.hScrollBarBCHeight.Location = New System.Drawing.Point(472, 152)
        Me.hScrollBarBCHeight.Maximum = 1200
        Me.hScrollBarBCHeight.Minimum = 100
        Me.hScrollBarBCHeight.Name = "hScrollBarBCHeight"
        Me.hScrollBarBCHeight.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarBCHeight.TabIndex = 51
        Me.hScrollBarBCHeight.Value = 300
        '
        'hScrollBarMinBar
        '
        Me.hScrollBarMinBar.Location = New System.Drawing.Point(472, 176)
        Me.hScrollBarMinBar.Maximum = 15
        Me.hScrollBarMinBar.Minimum = 2
        Me.hScrollBarMinBar.Name = "hScrollBarMinBar"
        Me.hScrollBarMinBar.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarMinBar.TabIndex = 50
        Me.hScrollBarMinBar.Value = 2
        '
        'labelUPCGap
        '
        Me.labelUPCGap.Location = New System.Drawing.Point(360, 248)
        Me.labelUPCGap.Name = "labelUPCGap"
        Me.labelUPCGap.Size = New System.Drawing.Size(112, 16)
        Me.labelUPCGap.TabIndex = 48
        Me.labelUPCGap.Text = "UPC Value Gap:"
        '
        'labelValueFont
        '
        Me.labelValueFont.Location = New System.Drawing.Point(360, 272)
        Me.labelValueFont.Name = "labelValueFont"
        Me.labelValueFont.Size = New System.Drawing.Size(112, 16)
        Me.labelValueFont.TabIndex = 47
        Me.labelValueFont.Text = "Value Font Size:"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 0
        Me.ExitMenuItem.Text = "E&xit"
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ExitMenuItem})
        Me.FileMenu.Text = "&File"
        '
        'labelMinBarVal
        '
        Me.labelMinBarVal.Location = New System.Drawing.Point(664, 176)
        Me.labelMinBarVal.Name = "labelMinBarVal"
        Me.labelMinBarVal.Size = New System.Drawing.Size(40, 16)
        Me.labelMinBarVal.TabIndex = 57
        '
        'hScrollBarValueGap
        '
        Me.hScrollBarValueGap.Location = New System.Drawing.Point(472, 224)
        Me.hScrollBarValueGap.Maximum = 200
        Me.hScrollBarValueGap.Minimum = -200
        Me.hScrollBarValueGap.Name = "hScrollBarValueGap"
        Me.hScrollBarValueGap.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarValueGap.TabIndex = 52
        '
        'hScrollBarUPCNotch
        '
        Me.hScrollBarUPCNotch.Location = New System.Drawing.Point(472, 200)
        Me.hScrollBarUPCNotch.Maximum = 50
        Me.hScrollBarUPCNotch.Name = "hScrollBarUPCNotch"
        Me.hScrollBarUPCNotch.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarUPCNotch.TabIndex = 49
        '
        'labelUPCFont
        '
        Me.labelUPCFont.Location = New System.Drawing.Point(360, 296)
        Me.labelUPCFont.Name = "labelUPCFont"
        Me.labelUPCFont.Size = New System.Drawing.Size(117, 16)
        Me.labelUPCFont.TabIndex = 46
        Me.labelUPCFont.Text = "UPC Small Font Size:"
        '
        'labelUPCNotch
        '
        Me.labelUPCNotch.Location = New System.Drawing.Point(360, 200)
        Me.labelUPCNotch.Name = "labelUPCNotch"
        Me.labelUPCNotch.Size = New System.Drawing.Size(112, 16)
        Me.labelUPCNotch.TabIndex = 45
        Me.labelUPCNotch.Text = "UPC Notch %:"
        '
        'labelValueGap
        '
        Me.labelValueGap.Location = New System.Drawing.Point(360, 224)
        Me.labelValueGap.Name = "labelValueGap"
        Me.labelValueGap.Size = New System.Drawing.Size(112, 16)
        Me.labelValueGap.TabIndex = 44
        Me.labelValueGap.Text = "Value Gap:"
        '
        'labelBCHeight
        '
        Me.labelBCHeight.Location = New System.Drawing.Point(360, 152)
        Me.labelBCHeight.Name = "labelBCHeight"
        Me.labelBCHeight.Size = New System.Drawing.Size(112, 16)
        Me.labelBCHeight.TabIndex = 43
        Me.labelBCHeight.Text = "Barcode Height:"
        '
        'labelMinBar
        '
        Me.labelMinBar.Location = New System.Drawing.Point(360, 176)
        Me.labelMinBar.Name = "labelMinBar"
        Me.labelMinBar.Size = New System.Drawing.Size(112, 16)
        Me.labelMinBar.TabIndex = 42
        Me.labelMinBar.Text = "Min. Bar Width:"
        '
        'labelBCValue
        '
        Me.labelBCValue.Location = New System.Drawing.Point(360, 128)
        Me.labelBCValue.Name = "labelBCValue"
        Me.labelBCValue.Size = New System.Drawing.Size(96, 16)
        Me.labelBCValue.TabIndex = 41
        Me.labelBCValue.Text = "Barcode Value:"
        '
        'comboBoxStyle
        '
        Me.comboBoxStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxStyle.Location = New System.Drawing.Point(104, 96)
        Me.comboBoxStyle.Name = "comboBoxStyle"
        Me.comboBoxStyle.Size = New System.Drawing.Size(216, 21)
        Me.comboBoxStyle.TabIndex = 36
        '
        'labelStyle
        '
        Me.labelStyle.Location = New System.Drawing.Point(16, 96)
        Me.labelStyle.Name = "labelStyle"
        Me.labelStyle.Size = New System.Drawing.Size(80, 16)
        Me.labelStyle.TabIndex = 35
        Me.labelStyle.Text = "Barcode Style:"
        '
        'mainMenu
        '
        Me.mainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.AboutMenu})
        '
        'AboutMenu
        '
        Me.AboutMenu.Index = 1
        Me.AboutMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.BarcodeXpressMenuItem, Me.ImagXpressMenuItem})
        Me.AboutMenu.Text = "&About"
        '
        'BarcodeXpressMenuItem
        '
        Me.BarcodeXpressMenuItem.Index = 0
        Me.BarcodeXpressMenuItem.Text = "&BarcodeXpress"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 1
        Me.ImagXpressMenuItem.Text = "&ImagXpress"
        '
        'labelHeightVal
        '
        Me.labelHeightVal.Location = New System.Drawing.Point(664, 152)
        Me.labelHeightVal.Name = "labelHeightVal"
        Me.labelHeightVal.Size = New System.Drawing.Size(40, 16)
        Me.labelHeightVal.TabIndex = 63
        '
        'labelUPCFontVal
        '
        Me.labelUPCFontVal.Location = New System.Drawing.Point(664, 296)
        Me.labelUPCFontVal.Name = "labelUPCFontVal"
        Me.labelUPCFontVal.Size = New System.Drawing.Size(40, 16)
        Me.labelUPCFontVal.TabIndex = 62
        '
        'labelValueFontVal
        '
        Me.labelValueFontVal.Location = New System.Drawing.Point(664, 272)
        Me.labelValueFontVal.Name = "labelValueFontVal"
        Me.labelValueFontVal.Size = New System.Drawing.Size(40, 16)
        Me.labelValueFontVal.TabIndex = 61
        '
        'labelUPCGapVal
        '
        Me.labelUPCGapVal.Location = New System.Drawing.Point(664, 248)
        Me.labelUPCGapVal.Name = "labelUPCGapVal"
        Me.labelUPCGapVal.Size = New System.Drawing.Size(40, 16)
        Me.labelUPCGapVal.TabIndex = 60
        '
        'labelValueGapVal
        '
        Me.labelValueGapVal.Location = New System.Drawing.Point(664, 224)
        Me.labelValueGapVal.Name = "labelValueGapVal"
        Me.labelValueGapVal.Size = New System.Drawing.Size(40, 16)
        Me.labelValueGapVal.TabIndex = 59
        '
        'labelUPCNotchVal
        '
        Me.labelUPCNotchVal.Location = New System.Drawing.Point(664, 200)
        Me.labelUPCNotchVal.Name = "labelUPCNotchVal"
        Me.labelUPCNotchVal.Size = New System.Drawing.Size(40, 16)
        Me.labelUPCNotchVal.TabIndex = 58
        '
        'textBoxBCValue
        '
        Me.textBoxBCValue.Location = New System.Drawing.Point(472, 128)
        Me.textBoxBCValue.Name = "textBoxBCValue"
        Me.textBoxBCValue.Size = New System.Drawing.Size(184, 20)
        Me.textBoxBCValue.TabIndex = 56
        Me.textBoxBCValue.Text = "textBox1"
        '
        'hScrollBarUPCGap
        '
        Me.hScrollBarUPCGap.Location = New System.Drawing.Point(472, 248)
        Me.hScrollBarUPCGap.Maximum = 200
        Me.hScrollBarUPCGap.Minimum = -200
        Me.hScrollBarUPCGap.Name = "hScrollBarUPCGap"
        Me.hScrollBarUPCGap.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarUPCGap.TabIndex = 55
        '
        'hScrollBarValueFont
        '
        Me.hScrollBarValueFont.Location = New System.Drawing.Point(472, 272)
        Me.hScrollBarValueFont.Maximum = 150
        Me.hScrollBarValueFont.Minimum = 8
        Me.hScrollBarValueFont.Name = "hScrollBarValueFont"
        Me.hScrollBarValueFont.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarValueFont.TabIndex = 54
        Me.hScrollBarValueFont.Value = 12
        '
        'hScrollBarUPCFont
        '
        Me.hScrollBarUPCFont.Location = New System.Drawing.Point(472, 296)
        Me.hScrollBarUPCFont.Minimum = 6
        Me.hScrollBarUPCFont.Name = "hScrollBarUPCFont"
        Me.hScrollBarUPCFont.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarUPCFont.TabIndex = 53
        Me.hScrollBarUPCFont.Value = 8
        '
        'buttonPrint
        '
        Me.buttonPrint.Location = New System.Drawing.Point(600, 96)
        Me.buttonPrint.Name = "buttonPrint"
        Me.buttonPrint.Size = New System.Drawing.Size(112, 24)
        Me.buttonPrint.TabIndex = 40
        Me.buttonPrint.Text = "Print Barcode"
        '
        'buttonCreate
        '
        Me.buttonCreate.Location = New System.Drawing.Point(344, 96)
        Me.buttonCreate.Name = "buttonCreate"
        Me.buttonCreate.Size = New System.Drawing.Size(112, 24)
        Me.buttonCreate.TabIndex = 39
        Me.buttonCreate.Text = "Create Barcode"
        '
        'labelResult
        '
        Me.labelResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelResult.Location = New System.Drawing.Point(16, 224)
        Me.labelResult.Name = "labelResult"
        Me.labelResult.Size = New System.Drawing.Size(304, 240)
        Me.labelResult.TabIndex = 38
        Me.labelResult.Text = "label3"
        '
        'labelInfo
        '
        Me.labelInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelInfo.Location = New System.Drawing.Point(16, 128)
        Me.labelInfo.Name = "labelInfo"
        Me.labelInfo.Size = New System.Drawing.Size(304, 80)
        Me.labelInfo.TabIndex = 37
        Me.labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), an" & _
            "d the following symbols: space, minus (-), plus (+), period (.), dollar sign ($)" & _
            ", slash (/), and percent (%)."
        '
        'labelStatus
        '
        Me.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelStatus.Location = New System.Drawing.Point(16, 472)
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.Size = New System.Drawing.Size(696, 40)
        Me.labelStatus.TabIndex = 34
        '
        'buttonWrite
        '
        Me.buttonWrite.Location = New System.Drawing.Point(472, 96)
        Me.buttonWrite.Name = "buttonWrite"
        Me.buttonWrite.Size = New System.Drawing.Size(112, 24)
        Me.buttonWrite.TabIndex = 66
        Me.buttonWrite.Text = "Write to File"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates creating 1D Barcodes with the toolkit."})
        Me.ListBox1.Location = New System.Drawing.Point(16, 8)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(696, 69)
        Me.ListBox1.TabIndex = 67
        '
        'BarcodeXpress1
        '
        Me.BarcodeXpress1.Debug = False
        Me.BarcodeXpress1.DebugLogFile = "C:\Documents and Settings\jargento.JPG.COM\My Documents\BarcodeXpress5.log"
        Me.BarcodeXpress1.ErrorLevel = PegasusImaging.WinForms.BarcodeXpress5.ErrorLevelInfo.Production
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(331, 322)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(380, 141)
        Me.ImageXView1.TabIndex = 68
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(726, 523)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.hScrollBarUPCFont)
        Me.Controls.Add(Me.buttonWrite)
        Me.Controls.Add(Me.labelUPCGap)
        Me.Controls.Add(Me.labelValueFont)
        Me.Controls.Add(Me.labelMinBarVal)
        Me.Controls.Add(Me.hScrollBarValueGap)
        Me.Controls.Add(Me.hScrollBarUPCNotch)
        Me.Controls.Add(Me.labelUPCFont)
        Me.Controls.Add(Me.labelUPCNotch)
        Me.Controls.Add(Me.labelValueGap)
        Me.Controls.Add(Me.labelBCHeight)
        Me.Controls.Add(Me.labelMinBar)
        Me.Controls.Add(Me.labelBCValue)
        Me.Controls.Add(Me.comboBoxStyle)
        Me.Controls.Add(Me.labelStyle)
        Me.Controls.Add(Me.labelHeightVal)
        Me.Controls.Add(Me.labelUPCFontVal)
        Me.Controls.Add(Me.labelValueFontVal)
        Me.Controls.Add(Me.labelUPCGapVal)
        Me.Controls.Add(Me.labelValueGapVal)
        Me.Controls.Add(Me.labelUPCNotchVal)
        Me.Controls.Add(Me.textBoxBCValue)
        Me.Controls.Add(Me.hScrollBarUPCGap)
        Me.Controls.Add(Me.hScrollBarValueFont)
        Me.Controls.Add(Me.buttonPrint)
        Me.Controls.Add(Me.buttonCreate)
        Me.Controls.Add(Me.labelResult)
        Me.Controls.Add(Me.labelInfo)
        Me.Controls.Add(Me.labelStatus)
        Me.Controls.Add(Me.hScrollBarBCHeight)
        Me.Controls.Add(Me.hScrollBarMinBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.mainMenu
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Write 1D Barcodes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub FormMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '***Must call the UnlockRuntime method to unlock the control
	'The unlock codes shown below are for formatting purposes only and will not work!
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        'BarcodeXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

        comboBoxStyle.Items.Add("Code 39 Narrow")
        comboBoxStyle.Items.Add("Code 39 Extended Narrow")
        comboBoxStyle.Items.Add("Code 39 Wide")
        comboBoxStyle.Items.Add("Code 39 Extended Wide")
        comboBoxStyle.Items.Add("Industrial 2 of 5")
        comboBoxStyle.Items.Add("Interleaved 2 of 5")
        comboBoxStyle.Items.Add("Codabar Rationalized")
        comboBoxStyle.Items.Add("Code 128A")
        comboBoxStyle.Items.Add("Code 128B")
        comboBoxStyle.Items.Add("Code 128C")
        comboBoxStyle.Items.Add("Code 128")
        comboBoxStyle.Items.Add("UPC-A")
        comboBoxStyle.Items.Add("UPC-E")
        comboBoxStyle.Items.Add("EAN-8")
        comboBoxStyle.Items.Add("EAN-13")
        comboBoxStyle.Items.Add("EAN-128")
        comboBoxStyle.Items.Add("Code 93")
        comboBoxStyle.Items.Add("Code 93 Extended")
        comboBoxStyle.Items.Add("Patch Code")
        comboBoxStyle.SelectedIndex = 0

        hScrollBarBCHeight.Value = barcodeXpress1.writer.MinimumBarHeight
        hScrollBarMinBar.Value = barcodeXpress1.writer.MinimumBarWidth
        hScrollBarUPCNotch.Value = barcodeXpress1.writer.TextNotchPercent
        hScrollBarValueGap.Value = barcodeXpress1.writer.VerticalTextGap
        hScrollBarUPCGap.Value = barcodeXpress1.writer.HorizontalTextGap
        hScrollBarValueFont.Value = CInt(barcodeXpress1.writer.TextFont.Size)
        hScrollBarUPCFont.Value = CInt(BarcodeXpress1.writer.OutsideTextFont.Size)

        Application.EnableVisualStyles()
    End Sub


    Private Sub comboBoxStyle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles comboBoxStyle.SelectedIndexChanged
        Select Case (comboBoxStyle.SelectedIndex)
            Case 0
                labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code39Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code39Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 1
                labelInfo.Text = "Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39 Ext"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code39ExtendedBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code39ExtendedBarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 2
                labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code39Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code39Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 3
                labelInfo.Text = "Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39 Ext"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code39ExtendedBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code39ExtendedBarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 4
                labelInfo.Text = "Industry 2 of 5 - Allowable characters - digits 0-9."
                textBoxBCValue.Text = "1234321"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Industry2of5Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Industry2of5Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 5
                labelInfo.Text = "Interleaved 2 of 5 - Allowable characters - digits 0-9."
                textBoxBCValue.Text = "1234321"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Interleaved2of5Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Interleaved2of5Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 6
                labelInfo.Text = "Codabar - Allowable characters - digits 0-9, six symbols including: minus (-), plus (+), period (.), dollar sign ($), slash (/), and colon (:), and the following start/stop characters A, B, C and D."
                textBoxBCValue.Text = "C5467D"
                barcodeXpress1.writer.BarcodeType = BarcodeType.CodabarBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.CodabarBarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 7
                labelInfo.Text = "Code 128A - Allowable characters - Digits, uppercase characters, standard ASCII symbols."
                textBoxBCValue.Text = "CODE 128A"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 8
                labelInfo.Text = "Code 128B - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols."
                textBoxBCValue.Text = "CODE 128B"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 9
                labelInfo.Text = "Code 128C - Allowable characters - Digits 0-9."
                textBoxBCValue.Text = "987654321"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 10
                labelInfo.Text = "Code 128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols."
                textBoxBCValue.Text = "CODE 128"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 11
                labelInfo.Text = "UPC-A - Allowable characters - Digits 0-9 You must have 11 or 12 digits."
                textBoxBCValue.Text = "69792911003"
                barcodeXpress1.writer.BarcodeType = BarcodeType.UPCABarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.UPCABarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 12
                labelInfo.Text = "UPC-E - Allowable characters - Digits 0-9 You must have 6, 10, 11 or 12 digits."
                textBoxBCValue.Text = "654123"
                barcodeXpress1.writer.BarcodeType = BarcodeType.UPCEBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.UPCEBarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 13
                labelInfo.Text = "EAN-8 - Allowable characters - Digits 0-9 You must have 7 or 8 digits."
                textBoxBCValue.Text = "1234567"
                barcodeXpress1.writer.BarcodeType = BarcodeType.EAN8Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.EAN8Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 14
                labelInfo.Text = "EAN-13 - Allowable characters - Digits 0-9 You must have 12 or 13 digits."
                textBoxBCValue.Text = "123456765432"
                barcodeXpress1.writer.BarcodeType = BarcodeType.EAN13Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.EAN13Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 15
                labelInfo.Text = "EAN-128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols."
                textBoxBCValue.Text = "EAN-128"
                barcodeXpress1.writer.BarcodeType = BarcodeType.EAN128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.EAN128Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 16
                labelInfo.Text = "Code 93 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 93"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code93Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code93Barcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 17
                labelInfo.Text = "Code 93 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 93 Ext"
                barcodeXpress1.writer.BarcodeType = BarcodeType.Code93ExtendedBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.Code93ExtendedBarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 18
                labelInfo.Text = "Patch code - (1)=Patch I (2)=Patch II (3)=Patch III (4)=Patch IV (5)=Patch VI (6)=Patch T."
                textBoxBCValue.Text = "1"
                If hScrollBarMinBar.Value < 5 Then
                    hScrollBarMinBar.Value = 5
                End If
                barcodeXpress1.writer.BarcodeType = BarcodeType.PatchCodeBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.barcodexpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.barcodexpress5.BarcodeType.PatchCodeBarcode, 0)
                barcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select
        End Select

        labelResult.Text = ""
        ImageXView1.Image = Nothing
        buttonPrint.Enabled = False
        buttonWrite.Enabled = False
    End Sub
    Private Sub buttonCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCreate.Click
        barcodeXpress1.writer.BarcodeValue = textBoxBCValue.Text
        labelStatus.Text = ""

        Try
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New IntPtr(BarcodeXpress1.writer.Create()))

            ' read the barcode
            Dim DIB As System.Int32
            Dim currentArea As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
            BarcodeXpress1.reader.Area = currentArea
            DIB = ImageXView1.Image.ToHdib(False).ToInt32()
            results = BarcodeXpress1.reader.Analyze(DIB)

            If (results.Length > 0) Then
                Dim strResult As String = results.Length & " barcode(s) found." & vbNewLine & vbNewLine
                Dim i As Integer
                For i = 0 To results.Length - 1 Step 1
                    Dim curResult As PegasusImaging.WinForms.barcodexpress5.Result = CType(results.GetValue(i), PegasusImaging.WinForms.BarcodeXpress5.Result)
                    strResult = strResult & "Symbol #" & i & vbNewLine & "Type: " & curResult.BarcodeName & vbNewLine
                    strResult += "X - " + curResult.Area.X.ToString() + " pixels" + vbNewLine + "Y - " + curResult.Area.Y.ToString() + " pixels" + vbNewLine
                    strResult += "W - " + curResult.Area.Width.ToString() + " pixels" + vbNewLine + "H - " + curResult.Area.Height.ToString() + " pixels" + vbNewLine
                    strResult += "Value:" + vbNewLine + curResult.BarcodeValue + vbNewLine + vbNewLine
                    If curResult.ValidCheckSum Then
                        strResult += "Checksum OK"
                    End If
                Next

                strResult += vbNewLine
                labelResult.Text = strResult
                labelStatus.Text = "Barcode detection was successful."
                buttonPrint.Enabled = True
                buttonWrite.Enabled = True
            Else
                labelStatus.Text = "Could not detect any barcodes."
                buttonPrint.Enabled = False
                buttonWrite.Enabled = False
            End If

            'Must free the DIB passed into the Analyze method
            Marshal.FreeHGlobal(New System.IntPtr(DIB))
        Catch ex As PegasusImaging.WinForms.BarcodeXpress5.BarcodeException
            Dim strResult As String
            strResult = "Error Creating Barcode: "
            strResult += ex.Message
            labelResult.Text = strResult
            labelStatus.Text = strResult

        End Try
    End Sub

    Private Sub buttonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrint.Click
        Try
            ' Print the Image
            Dim doc As PrintDocument = New PrintDocument()
            AddHandler doc.PrintPage, AddressOf OnPrintPage
            doc.Print()
            labelStatus.Text = "Barcode printed."
        Catch ex As Exception
            labelStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub OnPrintPage(ByVal sender As System.Object, ByVal args As PrintPageEventArgs)
        ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(Application.StartupPath + "\barcode.tif")

        Dim proc As PegasusImaging.WinForms.ImagXpress8.Processor = New PegasusImaging.WinForms.ImagXpress8.Processor()

        proc.Image = ImageXView1.Image.Copy()
        proc.ColorDepth(24, 0, 0)
        ImageXView1.Image = proc.Image

        Dim g As System.Drawing.Graphics = ImageXView1.Image.GetGraphics()
        Dim img As System.Drawing.Image = CType(ImageXView1.Image.ToBitmap(False), System.Drawing.Image)
        args.Graphics.PageUnit = GraphicsUnit.Inch

        args.Graphics.DrawImage(img, CType((8.5 - img.Width / img.HorizontalResolution) / 2, Single), CType((11 - img.Height / img.VerticalResolution) / 2, Single), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution)

        ImageXView1.Image.ReleaseGraphics()

    End Sub

    Private Sub menuItemFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        System.Environment.Exit(0)
    End Sub

    Private Sub hScrollBarBCHeight_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarBCHeight.ValueChanged
        labelHeightVal.Text = hScrollBarBCHeight.Value.ToString()
        barcodeXpress1.writer.MinimumBarHeight = hScrollBarBCHeight.Value
    End Sub

    Private Sub hScrollBarMinBar_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarMinBar.ValueChanged
        labelMinBarVal.Text = hScrollBarMinBar.Value.ToString()
        barcodeXpress1.writer.MinimumBarWidth = CShort(hScrollBarMinBar.Value)
    End Sub

    Private Sub hScrollBarValueGap_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarValueGap.ValueChanged
        labelValueGapVal.Text = hScrollBarValueGap.Value.ToString()
        barcodeXpress1.writer.VerticalTextGap = hScrollBarValueGap.Value
    End Sub

    Private Sub hScrollBarUPCNotch_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarUPCNotch.ValueChanged
        labelUPCNotchVal.Text = hScrollBarUPCNotch.Value.ToString()
        barcodeXpress1.writer.TextNotchPercent = hScrollBarUPCNotch.Value
    End Sub

    Private Sub hScrollBarUPCGap_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarUPCGap.ValueChanged
        labelUPCGapVal.Text = hScrollBarUPCGap.Value.ToString()
        barcodeXpress1.writer.HorizontalTextGap = hScrollBarUPCGap.Value
    End Sub

    Private Sub hScrollBarValueFont_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarValueFont.ValueChanged
        labelValueFontVal.Text = hScrollBarValueFont.Value.ToString()
        Dim Font As System.Drawing.Font = barcodeXpress1.writer.TextFont
        BarcodeXpress1.writer.TextFont = New System.Drawing.Font(Font.FontFamily.GetName(0), hScrollBarValueFont.Value)
    End Sub

    Private Sub hScrollBarUPCFont_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hScrollBarUPCFont.ValueChanged
        labelUPCFontVal.Text = hScrollBarUPCFont.Value.ToString()
        Dim Font As System.Drawing.Font = BarcodeXpress1.writer.OutsideTextFont
        BarcodeXpress1.writer.OutsideTextFont = New System.Drawing.Font(Font.FontFamily.GetName(0), hScrollBarUPCFont.Value)
    End Sub

    Private Sub buttonWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonWrite.Click

        Dim strMsg As String
        ' Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
        ' Dim strImageFile As String = System.IO.Path.Combine(strCurrentDir, "..\2Dbarcode.TIF")
        Dim so As PegasusImaging.WinForms.ImagXpress8.SaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
        Try

            so.Format = ImageXFormat.Tiff
            so.Tiff.Compression = Compression.Group4

            ImageXView1.Image.Save(Application.StartupPath + "\barcode.tif", so)
            strMsg = "Image saved as " + Application.StartupPath + "\barcode.tif"
            labelStatus.Text = strMsg

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            labelStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BarcodeXpressMenuItem.Click
        BarcodeXpress1.AboutBox()
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub
End Class

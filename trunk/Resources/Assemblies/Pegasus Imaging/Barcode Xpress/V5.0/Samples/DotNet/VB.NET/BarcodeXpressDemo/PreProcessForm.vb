'****************************************************************'
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Imports PegasusImaging.WinForms.ScanFix5
Public Class ProcessScreen
    Inherits System.Windows.Forms.Form


    Dim regOpts As PegasusImaging.WinForms.ScanFix5.ImageRegistrationOptions
    Friend WithEvents BitonalTabRoll As System.Windows.Forms.TabControl
    Friend WithEvents tabblob As System.Windows.Forms.TabPage
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents scrollden As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollm As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollmin As System.Windows.Forms.HScrollBar
    Friend WithEvents scrolh As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollw As System.Windows.Forms.HScrollBar
    Friend WithEvents scrolly As System.Windows.Forms.HScrollBar
    Friend WithEvents scrollx As System.Windows.Forms.HScrollBar
    Friend WithEvents tabborder As System.Windows.Forms.TabPage
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents CheckBox18 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox17 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox16 As System.Windows.Forms.CheckBox
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar33 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar34 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar35 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar36 As System.Windows.Forms.HScrollBar
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar38 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar39 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar40 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar41 As System.Windows.Forms.HScrollBar
    Friend WithEvents tabdotshade As System.Windows.Forms.TabPage
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar18 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar19 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar20 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar21 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar22 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar23 As System.Windows.Forms.HScrollBar
    Friend WithEvents tabsmooth As System.Windows.Forms.TabPage
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar32 As System.Windows.Forms.HScrollBar
    Friend WithEvents tabnegpage As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar24 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar25 As System.Windows.Forms.HScrollBar
    Friend WithEvents tabcomb As System.Windows.Forms.TabPage
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar2 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar14 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar15 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar16 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar17 As System.Windows.Forms.HScrollBar
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents HScrollBar9 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar10 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar11 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar12 As System.Windows.Forms.HScrollBar
    Friend WithEvents HScrollBar13 As System.Windows.Forms.HScrollBar
    Friend WithEvents tabinverse As System.Windows.Forms.TabPage
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents minblack As System.Windows.Forms.HScrollBar
    Friend WithEvents minheight As System.Windows.Forms.HScrollBar
    Friend WithEvents minwidth As System.Windows.Forms.HScrollBar
    Friend WithEvents TabRoll As System.Windows.Forms.TabControl
    Friend WithEvents LitePage As System.Windows.Forms.TabPage
    Friend WithEvents BitonalPage As System.Windows.Forms.TabPage
    Friend WithEvents PreviewButton As System.Windows.Forms.Button
    Friend WithEvents ProcessButton As System.Windows.Forms.Button
    Friend WithEvents CloseButton As System.Windows.Forms.Button
    Friend WithEvents EditionNote As System.Windows.Forms.Label
    Dim regResults As PegasusImaging.WinForms.ScanFix5.ImageRegistrationResults
    Friend WithEvents BlobCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents InverseCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CombCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents NegCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents DotCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents BorderCheckBox As System.Windows.Forms.CheckBox

    Dim scanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Dim PreviewFlag As Boolean
    Public PreProcessedFlag As Boolean = False
    Dim mainImage As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents chkDeskew As System.Windows.Forms.CheckBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents chkNegate As System.Windows.Forms.CheckBox
    Friend WithEvents txtRotAngle As System.Windows.Forms.TextBox
    Friend WithEvents chkRotAngle As System.Windows.Forms.CheckBox
    Friend WithEvents chkRot180 As System.Windows.Forms.CheckBox
    Friend WithEvents chkRot90 As System.Windows.Forms.CheckBox
    Friend WithEvents txtShear As System.Windows.Forms.TextBox
    Friend WithEvents chkShear As System.Windows.Forms.CheckBox
    Friend WithEvents chkSmoothZoom As System.Windows.Forms.CheckBox
    Friend WithEvents chkFlip As System.Windows.Forms.CheckBox
    Friend WithEvents chkMirror As System.Windows.Forms.CheckBox
    Friend WithEvents chkRemoveLines As System.Windows.Forms.CheckBox
    Public WithEvents Label17 As System.Windows.Forms.Label
    Public WithEvents chkDespeckle As System.Windows.Forms.CheckBox
    Public WithEvents txtSpeckleW As System.Windows.Forms.TextBox
    Public WithEvents txtSpeckleH As System.Windows.Forms.TextBox
    Public WithEvents Frame2 As System.Windows.Forms.GroupBox
    Public WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Public WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Public WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Public WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Public WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Public WithEvents Frame1 As System.Windows.Forms.GroupBox
    Public WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Public WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Public WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Public WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Public WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Public WithEvents Label18 As System.Windows.Forms.Label
    Public WithEvents chkDilate1 As System.Windows.Forms.CheckBox
    Public WithEvents optDilate1_0 As System.Windows.Forms.RadioButton
    Public WithEvents optDilate1_1 As System.Windows.Forms.RadioButton
    Public WithEvents optDilate1_2 As System.Windows.Forms.RadioButton
    Public WithEvents optDilate1_3 As System.Windows.Forms.RadioButton
    Public WithEvents optErode1_3_3 As System.Windows.Forms.RadioButton
    Public WithEvents optErode1_3_2 As System.Windows.Forms.RadioButton
    Public WithEvents optErode1_3_1 As System.Windows.Forms.RadioButton
    Public WithEvents optErode1_3_0 As System.Windows.Forms.RadioButton
    Public WithEvents chkErode1 As System.Windows.Forms.CheckBox
    Friend WithEvents LengthTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LengthLabel As System.Windows.Forms.Label
    Friend WithEvents SmoothObjectsCheckBox As System.Windows.Forms.CheckBox
    Dim mainTestImage As PegasusImaging.WinForms.ImagXpress8.ImageXView


#Region " Windows Form Designer generated code "

    Public Sub New(ByVal mainImg As PegasusImaging.WinForms.ImagXpress8.ImageXView, ByVal mainTestImg As PegasusImaging.WinForms.ImagXpress8.ImageXView, ByVal SF As PegasusImaging.WinForms.ScanFix5.ScanFix)

        MyBase.New()



        '***Must call the UnlockControl method before the InitializeComponent statement
        'The unlock codes shown below are for formatting purposes only and will not work!
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234, 1234, 1234, 1234)
        'PegasusImaging.WinForms.ScanFix5.License.UnlockControl(1234, 1234, 1234, 1234)


        'This call is required by the Windows Form Designer.
        InitializeComponent()

        mainImage = mainImg
        mainTestImage = mainTestImg

        scanFix1 = SF

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            If Not InputImgView.Image Is Nothing Then
                InputImgView.Image.Dispose()
                InputImgView.Image = Nothing
            End If

            If Not OutputImgView.Image Is Nothing Then
                OutputImgView.Image.Dispose()
                OutputImgView.Image = Nothing
            End If
            If Not (InputImgView Is Nothing) Then
                InputImgView.Dispose()
                InputImgView = Nothing
            End If

            If Not (OutputImgView Is Nothing) Then
                OutputImgView.Dispose()
                OutputImgView = Nothing
            End If

            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If

            If Not (scanFix1 Is Nothing) Then
                scanFix1.Dispose()
                scanFix1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If

        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents lblinput As System.Windows.Forms.Label
    Friend WithEvents lblout As System.Windows.Forms.Label
    'Friend WithEvents ScanFix1 As PegasusImaging.WinForms.ScanFix5.ScanFix
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents InputImgView As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents OutputImgView As PegasusImaging.WinForms.ImagXpress8.ImageXView
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProcessScreen))
        Me.lblinput = New System.Windows.Forms.Label
        Me.lblout = New System.Windows.Forms.Label
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Me.InputImgView = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.OutputImgView = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.tabinverse = New System.Windows.Forms.TabPage
        Me.InverseCheckBox = New System.Windows.Forms.CheckBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.minblack = New System.Windows.Forms.HScrollBar
        Me.minheight = New System.Windows.Forms.HScrollBar
        Me.minwidth = New System.Windows.Forms.HScrollBar
        Me.tabcomb = New System.Windows.Forms.TabPage
        Me.CombCheckBox = New System.Windows.Forms.CheckBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label43 = New System.Windows.Forms.Label
        Me.Label49 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label52 = New System.Windows.Forms.Label
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label54 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.Label56 = New System.Windows.Forms.Label
        Me.HScrollBar2 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar14 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar15 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar16 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar17 = New System.Windows.Forms.HScrollBar
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label42 = New System.Windows.Forms.Label
        Me.Label44 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.Label46 = New System.Windows.Forms.Label
        Me.Label47 = New System.Windows.Forms.Label
        Me.Label48 = New System.Windows.Forms.Label
        Me.HScrollBar9 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar10 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar11 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar12 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar13 = New System.Windows.Forms.HScrollBar
        Me.tabnegpage = New System.Windows.Forms.TabPage
        Me.NegCheckBox = New System.Windows.Forms.CheckBox
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.Label69 = New System.Windows.Forms.Label
        Me.Label70 = New System.Windows.Forms.Label
        Me.Label71 = New System.Windows.Forms.Label
        Me.Label72 = New System.Windows.Forms.Label
        Me.HScrollBar24 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar25 = New System.Windows.Forms.HScrollBar
        Me.tabsmooth = New System.Windows.Forms.TabPage
        Me.SmoothObjectsCheckBox = New System.Windows.Forms.CheckBox
        Me.Label86 = New System.Windows.Forms.Label
        Me.Label91 = New System.Windows.Forms.Label
        Me.HScrollBar32 = New System.Windows.Forms.HScrollBar
        Me.tabdotshade = New System.Windows.Forms.TabPage
        Me.DotCheckBox = New System.Windows.Forms.CheckBox
        Me.Label57 = New System.Windows.Forms.Label
        Me.Label58 = New System.Windows.Forms.Label
        Me.Label59 = New System.Windows.Forms.Label
        Me.Label60 = New System.Windows.Forms.Label
        Me.Label61 = New System.Windows.Forms.Label
        Me.Label62 = New System.Windows.Forms.Label
        Me.Label63 = New System.Windows.Forms.Label
        Me.Label64 = New System.Windows.Forms.Label
        Me.Label65 = New System.Windows.Forms.Label
        Me.Label66 = New System.Windows.Forms.Label
        Me.Label67 = New System.Windows.Forms.Label
        Me.Label68 = New System.Windows.Forms.Label
        Me.HScrollBar18 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar19 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar20 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar21 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar22 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar23 = New System.Windows.Forms.HScrollBar
        Me.tabborder = New System.Windows.Forms.TabPage
        Me.BorderCheckBox = New System.Windows.Forms.CheckBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label106 = New System.Windows.Forms.Label
        Me.CheckBox18 = New System.Windows.Forms.CheckBox
        Me.CheckBox17 = New System.Windows.Forms.CheckBox
        Me.CheckBox16 = New System.Windows.Forms.CheckBox
        Me.Label87 = New System.Windows.Forms.Label
        Me.Label88 = New System.Windows.Forms.Label
        Me.Label89 = New System.Windows.Forms.Label
        Me.Label90 = New System.Windows.Forms.Label
        Me.Label92 = New System.Windows.Forms.Label
        Me.Label93 = New System.Windows.Forms.Label
        Me.Label94 = New System.Windows.Forms.Label
        Me.Label95 = New System.Windows.Forms.Label
        Me.HScrollBar33 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar34 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar35 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar36 = New System.Windows.Forms.HScrollBar
        Me.Label97 = New System.Windows.Forms.Label
        Me.Label98 = New System.Windows.Forms.Label
        Me.Label99 = New System.Windows.Forms.Label
        Me.Label100 = New System.Windows.Forms.Label
        Me.Label102 = New System.Windows.Forms.Label
        Me.Label103 = New System.Windows.Forms.Label
        Me.Label104 = New System.Windows.Forms.Label
        Me.Label105 = New System.Windows.Forms.Label
        Me.HScrollBar38 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar39 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar40 = New System.Windows.Forms.HScrollBar
        Me.HScrollBar41 = New System.Windows.Forms.HScrollBar
        Me.tabblob = New System.Windows.Forms.TabPage
        Me.BlobCheckBox = New System.Windows.Forms.CheckBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.scrollden = New System.Windows.Forms.HScrollBar
        Me.scrollm = New System.Windows.Forms.HScrollBar
        Me.scrollmin = New System.Windows.Forms.HScrollBar
        Me.scrolh = New System.Windows.Forms.HScrollBar
        Me.scrollw = New System.Windows.Forms.HScrollBar
        Me.scrolly = New System.Windows.Forms.HScrollBar
        Me.scrollx = New System.Windows.Forms.HScrollBar
        Me.BitonalTabRoll = New System.Windows.Forms.TabControl
        Me.TabRoll = New System.Windows.Forms.TabControl
        Me.LitePage = New System.Windows.Forms.TabPage
        Me.LengthLabel = New System.Windows.Forms.Label
        Me.LengthTextBox = New System.Windows.Forms.TextBox
        Me.chkDeskew = New System.Windows.Forms.CheckBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.chkNegate = New System.Windows.Forms.CheckBox
        Me.txtRotAngle = New System.Windows.Forms.TextBox
        Me.chkRotAngle = New System.Windows.Forms.CheckBox
        Me.chkRot180 = New System.Windows.Forms.CheckBox
        Me.chkRot90 = New System.Windows.Forms.CheckBox
        Me.txtShear = New System.Windows.Forms.TextBox
        Me.chkShear = New System.Windows.Forms.CheckBox
        Me.chkSmoothZoom = New System.Windows.Forms.CheckBox
        Me.chkFlip = New System.Windows.Forms.CheckBox
        Me.chkMirror = New System.Windows.Forms.CheckBox
        Me.chkRemoveLines = New System.Windows.Forms.CheckBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.chkDespeckle = New System.Windows.Forms.CheckBox
        Me.txtSpeckleW = New System.Windows.Forms.TextBox
        Me.txtSpeckleH = New System.Windows.Forms.TextBox
        Me.Frame2 = New System.Windows.Forms.GroupBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.RadioButton4 = New System.Windows.Forms.RadioButton
        Me.Frame1 = New System.Windows.Forms.GroupBox
        Me.RadioButton5 = New System.Windows.Forms.RadioButton
        Me.RadioButton6 = New System.Windows.Forms.RadioButton
        Me.RadioButton7 = New System.Windows.Forms.RadioButton
        Me.RadioButton8 = New System.Windows.Forms.RadioButton
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.BitonalPage = New System.Windows.Forms.TabPage
        Me.PreviewButton = New System.Windows.Forms.Button
        Me.ProcessButton = New System.Windows.Forms.Button
        Me.CloseButton = New System.Windows.Forms.Button
        Me.EditionNote = New System.Windows.Forms.Label
        Me.chkDilate1 = New System.Windows.Forms.CheckBox
        Me.optDilate1_0 = New System.Windows.Forms.RadioButton
        Me.optDilate1_1 = New System.Windows.Forms.RadioButton
        Me.optDilate1_2 = New System.Windows.Forms.RadioButton
        Me.optDilate1_3 = New System.Windows.Forms.RadioButton
        Me.optErode1_3_3 = New System.Windows.Forms.RadioButton
        Me.optErode1_3_2 = New System.Windows.Forms.RadioButton
        Me.optErode1_3_1 = New System.Windows.Forms.RadioButton
        Me.optErode1_3_0 = New System.Windows.Forms.RadioButton
        Me.chkErode1 = New System.Windows.Forms.CheckBox
        Me.tabinverse.SuspendLayout()
        Me.tabcomb.SuspendLayout()
        Me.tabnegpage.SuspendLayout()
        Me.tabsmooth.SuspendLayout()
        Me.tabdotshade.SuspendLayout()
        Me.tabborder.SuspendLayout()
        Me.tabblob.SuspendLayout()
        Me.BitonalTabRoll.SuspendLayout()
        Me.TabRoll.SuspendLayout()
        Me.LitePage.SuspendLayout()
        Me.Frame2.SuspendLayout()
        Me.Frame1.SuspendLayout()
        Me.BitonalPage.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblinput
        '
        Me.lblinput.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblinput.Location = New System.Drawing.Point(137, 236)
        Me.lblinput.Name = "lblinput"
        Me.lblinput.Size = New System.Drawing.Size(128, 16)
        Me.lblinput.TabIndex = 3
        Me.lblinput.Text = "Input Image"
        Me.lblinput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblout
        '
        Me.lblout.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblout.Location = New System.Drawing.Point(541, 236)
        Me.lblout.Name = "lblout"
        Me.lblout.Size = New System.Drawing.Size(184, 16)
        Me.lblout.TabIndex = 4
        Me.lblout.Text = "Output Image"
        Me.lblout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'InputImgView
        '
        Me.InputImgView.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.InputImgView.AutoScroll = True
        Me.InputImgView.Location = New System.Drawing.Point(8, 12)
        Me.InputImgView.Name = "InputImgView"
        Me.InputImgView.Size = New System.Drawing.Size(405, 221)
        Me.InputImgView.TabIndex = 8
        '
        'OutputImgView
        '
        Me.OutputImgView.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.OutputImgView.AutoScroll = True
        Me.OutputImgView.Location = New System.Drawing.Point(424, 12)
        Me.OutputImgView.Name = "OutputImgView"
        Me.OutputImgView.Size = New System.Drawing.Size(409, 221)
        Me.OutputImgView.TabIndex = 9
        '
        'tabinverse
        '
        Me.tabinverse.BackColor = System.Drawing.SystemColors.Control
        Me.tabinverse.Controls.Add(Me.InverseCheckBox)
        Me.tabinverse.Controls.Add(Me.Label24)
        Me.tabinverse.Controls.Add(Me.Label23)
        Me.tabinverse.Controls.Add(Me.Label22)
        Me.tabinverse.Controls.Add(Me.Label21)
        Me.tabinverse.Controls.Add(Me.Label20)
        Me.tabinverse.Controls.Add(Me.Label19)
        Me.tabinverse.Controls.Add(Me.minblack)
        Me.tabinverse.Controls.Add(Me.minheight)
        Me.tabinverse.Controls.Add(Me.minwidth)
        Me.tabinverse.Location = New System.Drawing.Point(4, 22)
        Me.tabinverse.Name = "tabinverse"
        Me.tabinverse.Size = New System.Drawing.Size(799, 466)
        Me.tabinverse.TabIndex = 3
        Me.tabinverse.Text = "Inverse Text Correction"
        '
        'InverseCheckBox
        '
        Me.InverseCheckBox.AutoSize = True
        Me.InverseCheckBox.Location = New System.Drawing.Point(350, 54)
        Me.InverseCheckBox.Name = "InverseCheckBox"
        Me.InverseCheckBox.Size = New System.Drawing.Size(127, 17)
        Me.InverseCheckBox.TabIndex = 23
        Me.InverseCheckBox.Text = "Correct Inverse Text"
        Me.InverseCheckBox.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(467, 239)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(56, 16)
        Me.Label24.TabIndex = 8
        Me.Label24.Text = "Label24"
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(467, 175)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(56, 16)
        Me.Label23.TabIndex = 7
        Me.Label23.Text = "Label23"
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(467, 111)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 6
        Me.Label22.Text = "Label22"
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(243, 239)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(200, 16)
        Me.Label21.TabIndex = 5
        Me.Label21.Text = "Minimum Black On Edges:"
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(235, 175)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(192, 16)
        Me.Label20.TabIndex = 4
        Me.Label20.Text = "Minimum Area Height:"
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(235, 111)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(184, 16)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Minimum Area Width:"
        '
        'minblack
        '
        Me.minblack.LargeChange = 1
        Me.minblack.Location = New System.Drawing.Point(235, 271)
        Me.minblack.Minimum = 1
        Me.minblack.Name = "minblack"
        Me.minblack.Size = New System.Drawing.Size(312, 16)
        Me.minblack.TabIndex = 2
        Me.minblack.Value = 1
        '
        'minheight
        '
        Me.minheight.LargeChange = 1
        Me.minheight.Location = New System.Drawing.Point(235, 207)
        Me.minheight.Maximum = 9999
        Me.minheight.Minimum = 1
        Me.minheight.Name = "minheight"
        Me.minheight.Size = New System.Drawing.Size(312, 16)
        Me.minheight.TabIndex = 1
        Me.minheight.Value = 1
        '
        'minwidth
        '
        Me.minwidth.LargeChange = 1
        Me.minwidth.Location = New System.Drawing.Point(235, 151)
        Me.minwidth.Maximum = 9999
        Me.minwidth.Minimum = 1
        Me.minwidth.Name = "minwidth"
        Me.minwidth.Size = New System.Drawing.Size(312, 16)
        Me.minwidth.TabIndex = 0
        Me.minwidth.Value = 1
        '
        'tabcomb
        '
        Me.tabcomb.BackColor = System.Drawing.SystemColors.Control
        Me.tabcomb.Controls.Add(Me.CombCheckBox)
        Me.tabcomb.Controls.Add(Me.Label25)
        Me.tabcomb.Controls.Add(Me.Label43)
        Me.tabcomb.Controls.Add(Me.Label49)
        Me.tabcomb.Controls.Add(Me.Label50)
        Me.tabcomb.Controls.Add(Me.Label51)
        Me.tabcomb.Controls.Add(Me.Label52)
        Me.tabcomb.Controls.Add(Me.Label53)
        Me.tabcomb.Controls.Add(Me.Label54)
        Me.tabcomb.Controls.Add(Me.Label55)
        Me.tabcomb.Controls.Add(Me.Label56)
        Me.tabcomb.Controls.Add(Me.HScrollBar2)
        Me.tabcomb.Controls.Add(Me.HScrollBar14)
        Me.tabcomb.Controls.Add(Me.HScrollBar15)
        Me.tabcomb.Controls.Add(Me.HScrollBar16)
        Me.tabcomb.Controls.Add(Me.HScrollBar17)
        Me.tabcomb.Controls.Add(Me.Label32)
        Me.tabcomb.Controls.Add(Me.Label39)
        Me.tabcomb.Controls.Add(Me.Label40)
        Me.tabcomb.Controls.Add(Me.Label41)
        Me.tabcomb.Controls.Add(Me.Label42)
        Me.tabcomb.Controls.Add(Me.Label44)
        Me.tabcomb.Controls.Add(Me.Label45)
        Me.tabcomb.Controls.Add(Me.Label46)
        Me.tabcomb.Controls.Add(Me.Label47)
        Me.tabcomb.Controls.Add(Me.Label48)
        Me.tabcomb.Controls.Add(Me.HScrollBar9)
        Me.tabcomb.Controls.Add(Me.HScrollBar10)
        Me.tabcomb.Controls.Add(Me.HScrollBar11)
        Me.tabcomb.Controls.Add(Me.HScrollBar12)
        Me.tabcomb.Controls.Add(Me.HScrollBar13)
        Me.tabcomb.Location = New System.Drawing.Point(4, 22)
        Me.tabcomb.Name = "tabcomb"
        Me.tabcomb.Size = New System.Drawing.Size(799, 466)
        Me.tabcomb.TabIndex = 5
        Me.tabcomb.Text = "Comb Removal"
        '
        'CombCheckBox
        '
        Me.CombCheckBox.AutoSize = True
        Me.CombCheckBox.Location = New System.Drawing.Point(335, 64)
        Me.CombCheckBox.Name = "CombCheckBox"
        Me.CombCheckBox.Size = New System.Drawing.Size(100, 17)
        Me.CombCheckBox.TabIndex = 76
        Me.CombCheckBox.Text = "Remove Combs"
        Me.CombCheckBox.UseVisualStyleBackColor = True
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(550, 267)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(64, 16)
        Me.Label25.TabIndex = 75
        '
        'Label43
        '
        Me.Label43.Location = New System.Drawing.Point(550, 227)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(72, 16)
        Me.Label43.TabIndex = 74
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(550, 195)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(64, 16)
        Me.Label49.TabIndex = 73
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(550, 155)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(80, 16)
        Me.Label50.TabIndex = 72
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(550, 115)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(72, 16)
        Me.Label51.TabIndex = 71
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(414, 267)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(128, 16)
        Me.Label52.TabIndex = 70
        Me.Label52.Text = "Vertical Line Thickness"
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(414, 227)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(136, 16)
        Me.Label53.TabIndex = 69
        Me.Label53.Text = "Horizontal Line Thickness"
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(414, 195)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(96, 16)
        Me.Label54.TabIndex = 68
        Me.Label54.Text = "Min Confidence"
        '
        'Label55
        '
        Me.Label55.Location = New System.Drawing.Point(414, 155)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(96, 16)
        Me.Label55.TabIndex = 67
        Me.Label55.Text = "Min Comb Length"
        '
        'Label56
        '
        Me.Label56.Location = New System.Drawing.Point(414, 115)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(80, 16)
        Me.Label56.TabIndex = 66
        Me.Label56.Text = "Comb Spacing"
        '
        'HScrollBar2
        '
        Me.HScrollBar2.LargeChange = 1
        Me.HScrollBar2.Location = New System.Drawing.Point(414, 283)
        Me.HScrollBar2.Maximum = 500
        Me.HScrollBar2.Minimum = 1
        Me.HScrollBar2.Name = "HScrollBar2"
        Me.HScrollBar2.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar2.TabIndex = 65
        Me.HScrollBar2.Value = 4
        '
        'HScrollBar14
        '
        Me.HScrollBar14.LargeChange = 1
        Me.HScrollBar14.Location = New System.Drawing.Point(414, 243)
        Me.HScrollBar14.Maximum = 500
        Me.HScrollBar14.Minimum = 1
        Me.HScrollBar14.Name = "HScrollBar14"
        Me.HScrollBar14.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar14.TabIndex = 64
        Me.HScrollBar14.Value = 4
        '
        'HScrollBar15
        '
        Me.HScrollBar15.LargeChange = 1
        Me.HScrollBar15.Location = New System.Drawing.Point(414, 211)
        Me.HScrollBar15.Minimum = 1
        Me.HScrollBar15.Name = "HScrollBar15"
        Me.HScrollBar15.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar15.TabIndex = 63
        Me.HScrollBar15.Value = 50
        '
        'HScrollBar16
        '
        Me.HScrollBar16.LargeChange = 1
        Me.HScrollBar16.Location = New System.Drawing.Point(414, 171)
        Me.HScrollBar16.Maximum = 500
        Me.HScrollBar16.Minimum = 10
        Me.HScrollBar16.Name = "HScrollBar16"
        Me.HScrollBar16.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar16.TabIndex = 62
        Me.HScrollBar16.Value = 50
        '
        'HScrollBar17
        '
        Me.HScrollBar17.LargeChange = 1
        Me.HScrollBar17.Location = New System.Drawing.Point(414, 131)
        Me.HScrollBar17.Maximum = 500
        Me.HScrollBar17.Minimum = 1
        Me.HScrollBar17.Name = "HScrollBar17"
        Me.HScrollBar17.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar17.TabIndex = 61
        Me.HScrollBar17.Value = 25
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(302, 267)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(64, 16)
        Me.Label32.TabIndex = 60
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(302, 227)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(72, 16)
        Me.Label39.TabIndex = 59
        '
        'Label40
        '
        Me.Label40.Location = New System.Drawing.Point(302, 195)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(64, 16)
        Me.Label40.TabIndex = 58
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(302, 155)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(80, 16)
        Me.Label41.TabIndex = 57
        '
        'Label42
        '
        Me.Label42.Location = New System.Drawing.Point(302, 115)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(72, 16)
        Me.Label42.TabIndex = 56
        '
        'Label44
        '
        Me.Label44.Location = New System.Drawing.Point(166, 267)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(116, 16)
        Me.Label44.TabIndex = 54
        Me.Label44.Text = "Comb Height"
        '
        'Label45
        '
        Me.Label45.Location = New System.Drawing.Point(166, 227)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(88, 16)
        Me.Label45.TabIndex = 53
        Me.Label45.Text = "Area Height"
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(166, 195)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(96, 16)
        Me.Label46.TabIndex = 52
        Me.Label46.Text = "Area Width"
        '
        'Label47
        '
        Me.Label47.Location = New System.Drawing.Point(166, 155)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(64, 16)
        Me.Label47.TabIndex = 51
        Me.Label47.Text = "Area Y"
        '
        'Label48
        '
        Me.Label48.Location = New System.Drawing.Point(166, 115)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(64, 16)
        Me.Label48.TabIndex = 50
        Me.Label48.Text = "Area X"
        '
        'HScrollBar9
        '
        Me.HScrollBar9.LargeChange = 1
        Me.HScrollBar9.Location = New System.Drawing.Point(166, 283)
        Me.HScrollBar9.Maximum = 500
        Me.HScrollBar9.Minimum = 4
        Me.HScrollBar9.Name = "HScrollBar9"
        Me.HScrollBar9.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar9.TabIndex = 48
        Me.HScrollBar9.Value = 20
        '
        'HScrollBar10
        '
        Me.HScrollBar10.LargeChange = 1
        Me.HScrollBar10.Location = New System.Drawing.Point(166, 243)
        Me.HScrollBar10.Maximum = 9999
        Me.HScrollBar10.Name = "HScrollBar10"
        Me.HScrollBar10.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar10.TabIndex = 47
        '
        'HScrollBar11
        '
        Me.HScrollBar11.LargeChange = 1
        Me.HScrollBar11.Location = New System.Drawing.Point(166, 211)
        Me.HScrollBar11.Maximum = 9999
        Me.HScrollBar11.Name = "HScrollBar11"
        Me.HScrollBar11.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar11.TabIndex = 46
        '
        'HScrollBar12
        '
        Me.HScrollBar12.LargeChange = 1
        Me.HScrollBar12.Location = New System.Drawing.Point(166, 171)
        Me.HScrollBar12.Maximum = 9999
        Me.HScrollBar12.Name = "HScrollBar12"
        Me.HScrollBar12.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar12.TabIndex = 45
        '
        'HScrollBar13
        '
        Me.HScrollBar13.LargeChange = 1
        Me.HScrollBar13.Location = New System.Drawing.Point(166, 131)
        Me.HScrollBar13.Maximum = 9999
        Me.HScrollBar13.Name = "HScrollBar13"
        Me.HScrollBar13.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar13.TabIndex = 44
        '
        'tabnegpage
        '
        Me.tabnegpage.BackColor = System.Drawing.SystemColors.Control
        Me.tabnegpage.Controls.Add(Me.NegCheckBox)
        Me.tabnegpage.Controls.Add(Me.CheckBox3)
        Me.tabnegpage.Controls.Add(Me.Label69)
        Me.tabnegpage.Controls.Add(Me.Label70)
        Me.tabnegpage.Controls.Add(Me.Label71)
        Me.tabnegpage.Controls.Add(Me.Label72)
        Me.tabnegpage.Controls.Add(Me.HScrollBar24)
        Me.tabnegpage.Controls.Add(Me.HScrollBar25)
        Me.tabnegpage.Location = New System.Drawing.Point(4, 22)
        Me.tabnegpage.Name = "tabnegpage"
        Me.tabnegpage.Size = New System.Drawing.Size(799, 466)
        Me.tabnegpage.TabIndex = 7
        Me.tabnegpage.Text = "Negative Page Correction"
        '
        'NegCheckBox
        '
        Me.NegCheckBox.AutoSize = True
        Me.NegCheckBox.Location = New System.Drawing.Point(359, 75)
        Me.NegCheckBox.Name = "NegCheckBox"
        Me.NegCheckBox.Size = New System.Drawing.Size(135, 17)
        Me.NegCheckBox.TabIndex = 52
        Me.NegCheckBox.Text = "Correct Negative Page"
        Me.NegCheckBox.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(186, 160)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(169, 24)
        Me.CheckBox3.TabIndex = 51
        Me.CheckBox3.Text = "Apply Negative Correction"
        '
        'Label69
        '
        Me.Label69.Location = New System.Drawing.Point(527, 234)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(80, 16)
        Me.Label69.TabIndex = 50
        '
        'Label70
        '
        Me.Label70.Location = New System.Drawing.Point(527, 202)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(72, 16)
        Me.Label70.TabIndex = 49
        '
        'Label71
        '
        Me.Label71.Location = New System.Drawing.Point(183, 234)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(64, 16)
        Me.Label71.TabIndex = 48
        Me.Label71.Text = "Quality"
        '
        'Label72
        '
        Me.Label72.Location = New System.Drawing.Point(183, 202)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(112, 16)
        Me.Label72.TabIndex = 47
        Me.Label72.Text = "Minimum Confidence"
        '
        'HScrollBar24
        '
        Me.HScrollBar24.LargeChange = 1
        Me.HScrollBar24.Location = New System.Drawing.Point(319, 234)
        Me.HScrollBar24.Minimum = 1
        Me.HScrollBar24.Name = "HScrollBar24"
        Me.HScrollBar24.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar24.TabIndex = 46
        Me.HScrollBar24.Value = 80
        '
        'HScrollBar25
        '
        Me.HScrollBar25.LargeChange = 1
        Me.HScrollBar25.Location = New System.Drawing.Point(319, 202)
        Me.HScrollBar25.Name = "HScrollBar25"
        Me.HScrollBar25.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar25.TabIndex = 45
        Me.HScrollBar25.Value = 50
        '
        'tabsmooth
        '
        Me.tabsmooth.BackColor = System.Drawing.SystemColors.Control
        Me.tabsmooth.Controls.Add(Me.SmoothObjectsCheckBox)
        Me.tabsmooth.Controls.Add(Me.Label86)
        Me.tabsmooth.Controls.Add(Me.Label91)
        Me.tabsmooth.Controls.Add(Me.HScrollBar32)
        Me.tabsmooth.Location = New System.Drawing.Point(4, 22)
        Me.tabsmooth.Name = "tabsmooth"
        Me.tabsmooth.Size = New System.Drawing.Size(799, 466)
        Me.tabsmooth.TabIndex = 11
        Me.tabsmooth.Text = "Smoothing"
        '
        'SmoothObjectsCheckBox
        '
        Me.SmoothObjectsCheckBox.AutoSize = True
        Me.SmoothObjectsCheckBox.Location = New System.Drawing.Point(355, 115)
        Me.SmoothObjectsCheckBox.Name = "SmoothObjectsCheckBox"
        Me.SmoothObjectsCheckBox.Size = New System.Drawing.Size(102, 17)
        Me.SmoothObjectsCheckBox.TabIndex = 14
        Me.SmoothObjectsCheckBox.Text = "Smooth Objects"
        Me.SmoothObjectsCheckBox.UseVisualStyleBackColor = True
        '
        'Label86
        '
        Me.Label86.Location = New System.Drawing.Point(427, 179)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(72, 16)
        Me.Label86.TabIndex = 9
        '
        'Label91
        '
        Me.Label91.Location = New System.Drawing.Point(291, 179)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(96, 16)
        Me.Label91.TabIndex = 8
        Me.Label91.Text = "Smoothing Size"
        '
        'HScrollBar32
        '
        Me.HScrollBar32.Location = New System.Drawing.Point(283, 219)
        Me.HScrollBar32.Minimum = 1
        Me.HScrollBar32.Name = "HScrollBar32"
        Me.HScrollBar32.Size = New System.Drawing.Size(240, 16)
        Me.HScrollBar32.TabIndex = 7
        Me.HScrollBar32.Value = 1
        '
        'tabdotshade
        '
        Me.tabdotshade.BackColor = System.Drawing.SystemColors.Control
        Me.tabdotshade.Controls.Add(Me.DotCheckBox)
        Me.tabdotshade.Controls.Add(Me.Label57)
        Me.tabdotshade.Controls.Add(Me.Label58)
        Me.tabdotshade.Controls.Add(Me.Label59)
        Me.tabdotshade.Controls.Add(Me.Label60)
        Me.tabdotshade.Controls.Add(Me.Label61)
        Me.tabdotshade.Controls.Add(Me.Label62)
        Me.tabdotshade.Controls.Add(Me.Label63)
        Me.tabdotshade.Controls.Add(Me.Label64)
        Me.tabdotshade.Controls.Add(Me.Label65)
        Me.tabdotshade.Controls.Add(Me.Label66)
        Me.tabdotshade.Controls.Add(Me.Label67)
        Me.tabdotshade.Controls.Add(Me.Label68)
        Me.tabdotshade.Controls.Add(Me.HScrollBar18)
        Me.tabdotshade.Controls.Add(Me.HScrollBar19)
        Me.tabdotshade.Controls.Add(Me.HScrollBar20)
        Me.tabdotshade.Controls.Add(Me.HScrollBar21)
        Me.tabdotshade.Controls.Add(Me.HScrollBar22)
        Me.tabdotshade.Controls.Add(Me.HScrollBar23)
        Me.tabdotshade.Location = New System.Drawing.Point(4, 22)
        Me.tabdotshade.Name = "tabdotshade"
        Me.tabdotshade.Size = New System.Drawing.Size(799, 466)
        Me.tabdotshade.TabIndex = 6
        Me.tabdotshade.Text = "Dot Shading Removal"
        '
        'DotCheckBox
        '
        Me.DotCheckBox.AutoSize = True
        Me.DotCheckBox.Location = New System.Drawing.Point(354, 55)
        Me.DotCheckBox.Name = "DotCheckBox"
        Me.DotCheckBox.Size = New System.Drawing.Size(126, 17)
        Me.DotCheckBox.TabIndex = 62
        Me.DotCheckBox.Text = "Remove Dot Shading"
        Me.DotCheckBox.UseVisualStyleBackColor = True
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(536, 285)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(72, 16)
        Me.Label57.TabIndex = 61
        '
        'Label58
        '
        Me.Label58.Location = New System.Drawing.Point(536, 253)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(64, 16)
        Me.Label58.TabIndex = 60
        '
        'Label59
        '
        Me.Label59.Location = New System.Drawing.Point(536, 221)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(72, 16)
        Me.Label59.TabIndex = 59
        '
        'Label60
        '
        Me.Label60.Location = New System.Drawing.Point(536, 197)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(64, 16)
        Me.Label60.TabIndex = 58
        '
        'Label61
        '
        Me.Label61.Location = New System.Drawing.Point(536, 165)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(80, 16)
        Me.Label61.TabIndex = 57
        '
        'Label62
        '
        Me.Label62.Location = New System.Drawing.Point(536, 125)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(72, 16)
        Me.Label62.TabIndex = 56
        '
        'Label63
        '
        Me.Label63.Location = New System.Drawing.Point(192, 285)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(104, 16)
        Me.Label63.TabIndex = 55
        Me.Label63.Text = "Max Dot Size"
        '
        'Label64
        '
        Me.Label64.Location = New System.Drawing.Point(192, 253)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(116, 16)
        Me.Label64.TabIndex = 54
        Me.Label64.Text = "Dot Shading Density"
        '
        'Label65
        '
        Me.Label65.Location = New System.Drawing.Point(173, 221)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(147, 26)
        Me.Label65.TabIndex = 53
        Me.Label65.Text = "Horizontal Size Adjustment"
        '
        'Label66
        '
        Me.Label66.Location = New System.Drawing.Point(192, 189)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(128, 16)
        Me.Label66.TabIndex = 52
        Me.Label66.Text = "Vertical Size Adjustment"
        '
        'Label67
        '
        Me.Label67.Location = New System.Drawing.Point(192, 157)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(96, 16)
        Me.Label67.TabIndex = 51
        Me.Label67.Text = "Min Area Height"
        '
        'Label68
        '
        Me.Label68.Location = New System.Drawing.Point(192, 125)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(88, 16)
        Me.Label68.TabIndex = 50
        Me.Label68.Text = "Min Area Width"
        '
        'HScrollBar18
        '
        Me.HScrollBar18.LargeChange = 1
        Me.HScrollBar18.Location = New System.Drawing.Point(328, 285)
        Me.HScrollBar18.Maximum = 20
        Me.HScrollBar18.Name = "HScrollBar18"
        Me.HScrollBar18.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar18.TabIndex = 49
        Me.HScrollBar18.Value = 5
        '
        'HScrollBar19
        '
        Me.HScrollBar19.LargeChange = 1
        Me.HScrollBar19.Location = New System.Drawing.Point(328, 253)
        Me.HScrollBar19.Maximum = 50
        Me.HScrollBar19.Minimum = -50
        Me.HScrollBar19.Name = "HScrollBar19"
        Me.HScrollBar19.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar19.TabIndex = 48
        '
        'HScrollBar20
        '
        Me.HScrollBar20.LargeChange = 1
        Me.HScrollBar20.Location = New System.Drawing.Point(328, 221)
        Me.HScrollBar20.Minimum = -100
        Me.HScrollBar20.Name = "HScrollBar20"
        Me.HScrollBar20.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar20.TabIndex = 47
        '
        'HScrollBar21
        '
        Me.HScrollBar21.LargeChange = 1
        Me.HScrollBar21.Location = New System.Drawing.Point(328, 189)
        Me.HScrollBar21.Minimum = -100
        Me.HScrollBar21.Name = "HScrollBar21"
        Me.HScrollBar21.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar21.TabIndex = 46
        '
        'HScrollBar22
        '
        Me.HScrollBar22.LargeChange = 1
        Me.HScrollBar22.Location = New System.Drawing.Point(328, 157)
        Me.HScrollBar22.Maximum = 9999
        Me.HScrollBar22.Name = "HScrollBar22"
        Me.HScrollBar22.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar22.TabIndex = 45
        Me.HScrollBar22.Value = 50
        '
        'HScrollBar23
        '
        Me.HScrollBar23.LargeChange = 1
        Me.HScrollBar23.Location = New System.Drawing.Point(328, 125)
        Me.HScrollBar23.Maximum = 9999
        Me.HScrollBar23.Name = "HScrollBar23"
        Me.HScrollBar23.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar23.TabIndex = 44
        Me.HScrollBar23.Value = 300
        '
        'tabborder
        '
        Me.tabborder.BackColor = System.Drawing.SystemColors.Control
        Me.tabborder.Controls.Add(Me.BorderCheckBox)
        Me.tabborder.Controls.Add(Me.ComboBox2)
        Me.tabborder.Controls.Add(Me.Label106)
        Me.tabborder.Controls.Add(Me.CheckBox18)
        Me.tabborder.Controls.Add(Me.CheckBox17)
        Me.tabborder.Controls.Add(Me.CheckBox16)
        Me.tabborder.Controls.Add(Me.Label87)
        Me.tabborder.Controls.Add(Me.Label88)
        Me.tabborder.Controls.Add(Me.Label89)
        Me.tabborder.Controls.Add(Me.Label90)
        Me.tabborder.Controls.Add(Me.Label92)
        Me.tabborder.Controls.Add(Me.Label93)
        Me.tabborder.Controls.Add(Me.Label94)
        Me.tabborder.Controls.Add(Me.Label95)
        Me.tabborder.Controls.Add(Me.HScrollBar33)
        Me.tabborder.Controls.Add(Me.HScrollBar34)
        Me.tabborder.Controls.Add(Me.HScrollBar35)
        Me.tabborder.Controls.Add(Me.HScrollBar36)
        Me.tabborder.Controls.Add(Me.Label97)
        Me.tabborder.Controls.Add(Me.Label98)
        Me.tabborder.Controls.Add(Me.Label99)
        Me.tabborder.Controls.Add(Me.Label100)
        Me.tabborder.Controls.Add(Me.Label102)
        Me.tabborder.Controls.Add(Me.Label103)
        Me.tabborder.Controls.Add(Me.Label104)
        Me.tabborder.Controls.Add(Me.Label105)
        Me.tabborder.Controls.Add(Me.HScrollBar38)
        Me.tabborder.Controls.Add(Me.HScrollBar39)
        Me.tabborder.Controls.Add(Me.HScrollBar40)
        Me.tabborder.Controls.Add(Me.HScrollBar41)
        Me.tabborder.Location = New System.Drawing.Point(4, 22)
        Me.tabborder.Name = "tabborder"
        Me.tabborder.Size = New System.Drawing.Size(799, 466)
        Me.tabborder.TabIndex = 12
        Me.tabborder.Text = "Border Removal"
        '
        'BorderCheckBox
        '
        Me.BorderCheckBox.AutoSize = True
        Me.BorderCheckBox.Location = New System.Drawing.Point(361, 55)
        Me.BorderCheckBox.Name = "BorderCheckBox"
        Me.BorderCheckBox.Size = New System.Drawing.Size(100, 17)
        Me.BorderCheckBox.TabIndex = 112
        Me.BorderCheckBox.Text = "Remove Border"
        Me.BorderCheckBox.UseVisualStyleBackColor = True
        '
        'ComboBox2
        '
        Me.ComboBox2.Items.AddRange(New Object() {"White", "Black"})
        Me.ComboBox2.Location = New System.Drawing.Point(99, 226)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(80, 21)
        Me.ComboBox2.TabIndex = 111
        '
        'Label106
        '
        Me.Label106.Location = New System.Drawing.Point(99, 210)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(72, 16)
        Me.Label106.TabIndex = 110
        Me.Label106.Text = "Pad Color"
        '
        'CheckBox18
        '
        Me.CheckBox18.Checked = True
        Me.CheckBox18.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox18.Location = New System.Drawing.Point(99, 178)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(88, 16)
        Me.CheckBox18.TabIndex = 109
        Me.CheckBox18.Text = "Deskew"
        '
        'CheckBox17
        '
        Me.CheckBox17.Location = New System.Drawing.Point(99, 154)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(104, 16)
        Me.CheckBox17.TabIndex = 108
        Me.CheckBox17.Text = "Replace Border"
        '
        'CheckBox16
        '
        Me.CheckBox16.Checked = True
        Me.CheckBox16.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox16.Location = New System.Drawing.Point(99, 130)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(88, 16)
        Me.CheckBox16.TabIndex = 107
        Me.CheckBox16.Text = "Crop Border"
        '
        'Label87
        '
        Me.Label87.Location = New System.Drawing.Point(595, 258)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(72, 16)
        Me.Label87.TabIndex = 105
        '
        'Label88
        '
        Me.Label88.Location = New System.Drawing.Point(595, 226)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(64, 16)
        Me.Label88.TabIndex = 104
        '
        'Label89
        '
        Me.Label89.Location = New System.Drawing.Point(595, 186)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(80, 16)
        Me.Label89.TabIndex = 103
        '
        'Label90
        '
        Me.Label90.Location = New System.Drawing.Point(595, 146)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(72, 16)
        Me.Label90.TabIndex = 102
        '
        'Label92
        '
        Me.Label92.Location = New System.Drawing.Point(459, 258)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(136, 16)
        Me.Label92.TabIndex = 100
        Me.Label92.Text = "Max Page Height"
        '
        'Label93
        '
        Me.Label93.Location = New System.Drawing.Point(459, 226)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(96, 16)
        Me.Label93.TabIndex = 99
        Me.Label93.Text = "Max Page Width"
        '
        'Label94
        '
        Me.Label94.Location = New System.Drawing.Point(459, 186)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(96, 16)
        Me.Label94.TabIndex = 98
        Me.Label94.Text = "Min Page Height"
        '
        'Label95
        '
        Me.Label95.Location = New System.Drawing.Point(459, 146)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(96, 16)
        Me.Label95.TabIndex = 97
        Me.Label95.Text = "Min Page Width"
        '
        'HScrollBar33
        '
        Me.HScrollBar33.LargeChange = 1
        Me.HScrollBar33.Location = New System.Drawing.Point(459, 274)
        Me.HScrollBar33.Maximum = 9999
        Me.HScrollBar33.Name = "HScrollBar33"
        Me.HScrollBar33.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar33.TabIndex = 95
        '
        'HScrollBar34
        '
        Me.HScrollBar34.LargeChange = 1
        Me.HScrollBar34.Location = New System.Drawing.Point(459, 242)
        Me.HScrollBar34.Maximum = 9999
        Me.HScrollBar34.Name = "HScrollBar34"
        Me.HScrollBar34.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar34.TabIndex = 94
        '
        'HScrollBar35
        '
        Me.HScrollBar35.LargeChange = 1
        Me.HScrollBar35.Location = New System.Drawing.Point(459, 202)
        Me.HScrollBar35.Maximum = 9999
        Me.HScrollBar35.Name = "HScrollBar35"
        Me.HScrollBar35.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar35.TabIndex = 93
        '
        'HScrollBar36
        '
        Me.HScrollBar36.LargeChange = 1
        Me.HScrollBar36.Location = New System.Drawing.Point(459, 162)
        Me.HScrollBar36.Maximum = 9999
        Me.HScrollBar36.Name = "HScrollBar36"
        Me.HScrollBar36.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar36.TabIndex = 92
        '
        'Label97
        '
        Me.Label97.Location = New System.Drawing.Point(347, 258)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(72, 16)
        Me.Label97.TabIndex = 90
        '
        'Label98
        '
        Me.Label98.Location = New System.Drawing.Point(347, 226)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(64, 16)
        Me.Label98.TabIndex = 89
        '
        'Label99
        '
        Me.Label99.Location = New System.Drawing.Point(347, 186)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(80, 16)
        Me.Label99.TabIndex = 88
        '
        'Label100
        '
        Me.Label100.Location = New System.Drawing.Point(347, 146)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(72, 16)
        Me.Label100.TabIndex = 87
        '
        'Label102
        '
        Me.Label102.Location = New System.Drawing.Point(211, 258)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(104, 16)
        Me.Label102.TabIndex = 85
        Me.Label102.Text = "Min Confidence"
        '
        'Label103
        '
        Me.Label103.Location = New System.Drawing.Point(211, 226)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(96, 16)
        Me.Label103.TabIndex = 84
        Me.Label103.Text = "Quality"
        '
        'Label104
        '
        Me.Label104.Location = New System.Drawing.Point(211, 186)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(104, 16)
        Me.Label104.TabIndex = 83
        Me.Label104.Text = "Page Speck Size"
        '
        'Label105
        '
        Me.Label105.Location = New System.Drawing.Point(211, 146)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(104, 16)
        Me.Label105.TabIndex = 82
        Me.Label105.Text = "Border Speck Size"
        '
        'HScrollBar38
        '
        Me.HScrollBar38.LargeChange = 1
        Me.HScrollBar38.Location = New System.Drawing.Point(211, 274)
        Me.HScrollBar38.Minimum = 1
        Me.HScrollBar38.Name = "HScrollBar38"
        Me.HScrollBar38.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar38.TabIndex = 80
        Me.HScrollBar38.Value = 50
        '
        'HScrollBar39
        '
        Me.HScrollBar39.LargeChange = 1
        Me.HScrollBar39.Location = New System.Drawing.Point(211, 242)
        Me.HScrollBar39.Minimum = 1
        Me.HScrollBar39.Name = "HScrollBar39"
        Me.HScrollBar39.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar39.TabIndex = 79
        Me.HScrollBar39.Value = 80
        '
        'HScrollBar40
        '
        Me.HScrollBar40.LargeChange = 1
        Me.HScrollBar40.Location = New System.Drawing.Point(211, 202)
        Me.HScrollBar40.Name = "HScrollBar40"
        Me.HScrollBar40.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar40.TabIndex = 78
        Me.HScrollBar40.Value = 2
        '
        'HScrollBar41
        '
        Me.HScrollBar41.LargeChange = 1
        Me.HScrollBar41.Location = New System.Drawing.Point(211, 162)
        Me.HScrollBar41.Name = "HScrollBar41"
        Me.HScrollBar41.Size = New System.Drawing.Size(192, 16)
        Me.HScrollBar41.TabIndex = 77
        Me.HScrollBar41.Value = 3
        '
        'tabblob
        '
        Me.tabblob.BackColor = System.Drawing.SystemColors.Control
        Me.tabblob.Controls.Add(Me.BlobCheckBox)
        Me.tabblob.Controls.Add(Me.Label14)
        Me.tabblob.Controls.Add(Me.Label13)
        Me.tabblob.Controls.Add(Me.Label12)
        Me.tabblob.Controls.Add(Me.Label11)
        Me.tabblob.Controls.Add(Me.Label10)
        Me.tabblob.Controls.Add(Me.Label9)
        Me.tabblob.Controls.Add(Me.Label8)
        Me.tabblob.Controls.Add(Me.Label7)
        Me.tabblob.Controls.Add(Me.Label6)
        Me.tabblob.Controls.Add(Me.Label5)
        Me.tabblob.Controls.Add(Me.Label4)
        Me.tabblob.Controls.Add(Me.Label3)
        Me.tabblob.Controls.Add(Me.Label2)
        Me.tabblob.Controls.Add(Me.Label1)
        Me.tabblob.Controls.Add(Me.scrollden)
        Me.tabblob.Controls.Add(Me.scrollm)
        Me.tabblob.Controls.Add(Me.scrollmin)
        Me.tabblob.Controls.Add(Me.scrolh)
        Me.tabblob.Controls.Add(Me.scrollw)
        Me.tabblob.Controls.Add(Me.scrolly)
        Me.tabblob.Controls.Add(Me.scrollx)
        Me.tabblob.Location = New System.Drawing.Point(4, 22)
        Me.tabblob.Name = "tabblob"
        Me.tabblob.Size = New System.Drawing.Size(799, 466)
        Me.tabblob.TabIndex = 2
        Me.tabblob.Text = "Blob Remove"
        '
        'BlobCheckBox
        '
        Me.BlobCheckBox.AutoSize = True
        Me.BlobCheckBox.Location = New System.Drawing.Point(337, 39)
        Me.BlobCheckBox.Name = "BlobCheckBox"
        Me.BlobCheckBox.Size = New System.Drawing.Size(93, 17)
        Me.BlobCheckBox.TabIndex = 22
        Me.BlobCheckBox.Text = "Remove Blobs"
        Me.BlobCheckBox.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(519, 300)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(88, 16)
        Me.Label14.TabIndex = 21
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(519, 268)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 16)
        Me.Label13.TabIndex = 20
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(519, 236)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 16)
        Me.Label12.TabIndex = 19
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(519, 204)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 16)
        Me.Label11.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(519, 172)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(64, 16)
        Me.Label10.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(519, 148)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(519, 108)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 16)
        Me.Label8.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(175, 300)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 16)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Minimum Density"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(175, 268)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 16)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Max Pixel Count"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(175, 236)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Min Pixel Count"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(175, 204)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Area Height"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(175, 172)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Area Width"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(175, 140)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Area Y"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(175, 108)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 16)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Area X"
        '
        'scrollden
        '
        Me.scrollden.LargeChange = 1
        Me.scrollden.Location = New System.Drawing.Point(311, 300)
        Me.scrollden.Name = "scrollden"
        Me.scrollden.Size = New System.Drawing.Size(192, 16)
        Me.scrollden.TabIndex = 7
        Me.scrollden.Value = 50
        '
        'scrollm
        '
        Me.scrollm.LargeChange = 1
        Me.scrollm.Location = New System.Drawing.Point(311, 268)
        Me.scrollm.Maximum = 9999
        Me.scrollm.Name = "scrollm"
        Me.scrollm.Size = New System.Drawing.Size(192, 16)
        Me.scrollm.TabIndex = 6
        Me.scrollm.Value = 9999
        '
        'scrollmin
        '
        Me.scrollmin.LargeChange = 1
        Me.scrollmin.Location = New System.Drawing.Point(311, 236)
        Me.scrollmin.Maximum = 9999
        Me.scrollmin.Minimum = 1
        Me.scrollmin.Name = "scrollmin"
        Me.scrollmin.Size = New System.Drawing.Size(192, 16)
        Me.scrollmin.TabIndex = 5
        Me.scrollmin.Value = 300
        '
        'scrolh
        '
        Me.scrolh.LargeChange = 1
        Me.scrolh.Location = New System.Drawing.Point(311, 204)
        Me.scrolh.Maximum = 9999
        Me.scrolh.Name = "scrolh"
        Me.scrolh.Size = New System.Drawing.Size(192, 16)
        Me.scrolh.TabIndex = 4
        '
        'scrollw
        '
        Me.scrollw.LargeChange = 1
        Me.scrollw.Location = New System.Drawing.Point(311, 172)
        Me.scrollw.Maximum = 9999
        Me.scrollw.Name = "scrollw"
        Me.scrollw.Size = New System.Drawing.Size(192, 16)
        Me.scrollw.TabIndex = 3
        '
        'scrolly
        '
        Me.scrolly.LargeChange = 1
        Me.scrolly.Location = New System.Drawing.Point(311, 140)
        Me.scrolly.Maximum = 9999
        Me.scrolly.Name = "scrolly"
        Me.scrolly.Size = New System.Drawing.Size(192, 16)
        Me.scrolly.TabIndex = 2
        '
        'scrollx
        '
        Me.scrollx.LargeChange = 1
        Me.scrollx.Location = New System.Drawing.Point(311, 108)
        Me.scrollx.Maximum = 9999
        Me.scrollx.Name = "scrollx"
        Me.scrollx.Size = New System.Drawing.Size(192, 16)
        Me.scrollx.TabIndex = 1
        '
        'BitonalTabRoll
        '
        Me.BitonalTabRoll.Controls.Add(Me.tabblob)
        Me.BitonalTabRoll.Controls.Add(Me.tabborder)
        Me.BitonalTabRoll.Controls.Add(Me.tabdotshade)
        Me.BitonalTabRoll.Controls.Add(Me.tabsmooth)
        Me.BitonalTabRoll.Controls.Add(Me.tabnegpage)
        Me.BitonalTabRoll.Controls.Add(Me.tabcomb)
        Me.BitonalTabRoll.Controls.Add(Me.tabinverse)
        Me.BitonalTabRoll.Location = New System.Drawing.Point(6, 11)
        Me.BitonalTabRoll.Multiline = True
        Me.BitonalTabRoll.Name = "BitonalTabRoll"
        Me.BitonalTabRoll.SelectedIndex = 0
        Me.BitonalTabRoll.Size = New System.Drawing.Size(807, 492)
        Me.BitonalTabRoll.TabIndex = 13
        '
        'TabRoll
        '
        Me.TabRoll.Controls.Add(Me.LitePage)
        Me.TabRoll.Controls.Add(Me.BitonalPage)
        Me.TabRoll.Location = New System.Drawing.Point(12, 255)
        Me.TabRoll.Name = "TabRoll"
        Me.TabRoll.SelectedIndex = 0
        Me.TabRoll.Size = New System.Drawing.Size(825, 532)
        Me.TabRoll.TabIndex = 0
        '
        'LitePage
        '
        Me.LitePage.BackColor = System.Drawing.SystemColors.Control
        Me.LitePage.Controls.Add(Me.LengthLabel)
        Me.LitePage.Controls.Add(Me.LengthTextBox)
        Me.LitePage.Controls.Add(Me.chkDeskew)
        Me.LitePage.Controls.Add(Me.Label15)
        Me.LitePage.Controls.Add(Me.Label16)
        Me.LitePage.Controls.Add(Me.chkNegate)
        Me.LitePage.Controls.Add(Me.txtRotAngle)
        Me.LitePage.Controls.Add(Me.chkRotAngle)
        Me.LitePage.Controls.Add(Me.chkRot180)
        Me.LitePage.Controls.Add(Me.chkRot90)
        Me.LitePage.Controls.Add(Me.txtShear)
        Me.LitePage.Controls.Add(Me.chkShear)
        Me.LitePage.Controls.Add(Me.chkSmoothZoom)
        Me.LitePage.Controls.Add(Me.chkFlip)
        Me.LitePage.Controls.Add(Me.chkMirror)
        Me.LitePage.Controls.Add(Me.chkRemoveLines)
        Me.LitePage.Controls.Add(Me.Label17)
        Me.LitePage.Controls.Add(Me.chkDespeckle)
        Me.LitePage.Controls.Add(Me.txtSpeckleW)
        Me.LitePage.Controls.Add(Me.txtSpeckleH)
        Me.LitePage.Controls.Add(Me.Frame2)
        Me.LitePage.Controls.Add(Me.Frame1)
        Me.LitePage.Controls.Add(Me.Label18)
        Me.LitePage.Location = New System.Drawing.Point(4, 22)
        Me.LitePage.Name = "LitePage"
        Me.LitePage.Padding = New System.Windows.Forms.Padding(3)
        Me.LitePage.Size = New System.Drawing.Size(817, 506)
        Me.LitePage.TabIndex = 1
        Me.LitePage.Text = "ScanFix Xpress Lite Edition"
        '
        'LengthLabel
        '
        Me.LengthLabel.AutoSize = True
        Me.LengthLabel.Location = New System.Drawing.Point(434, 175)
        Me.LengthLabel.Name = "LengthLabel"
        Me.LengthLabel.Size = New System.Drawing.Size(34, 13)
        Me.LengthLabel.TabIndex = 161
        Me.LengthLabel.Text = "pixels"
        '
        'LengthTextBox
        '
        Me.LengthTextBox.AcceptsReturn = True
        Me.LengthTextBox.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.LengthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.LengthTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LengthTextBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LengthTextBox.Location = New System.Drawing.Point(371, 170)
        Me.LengthTextBox.MaxLength = 0
        Me.LengthTextBox.Name = "LengthTextBox"
        Me.LengthTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LengthTextBox.Size = New System.Drawing.Size(40, 20)
        Me.LengthTextBox.TabIndex = 160
        Me.LengthTextBox.Text = "50"
        '
        'chkDeskew
        '
        Me.chkDeskew.BackColor = System.Drawing.SystemColors.Control
        Me.chkDeskew.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDeskew.Location = New System.Drawing.Point(171, 226)
        Me.chkDeskew.Name = "chkDeskew"
        Me.chkDeskew.Size = New System.Drawing.Size(104, 24)
        Me.chkDeskew.TabIndex = 159
        Me.chkDeskew.Text = "Deskew"
        Me.chkDeskew.UseVisualStyleBackColor = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(547, 330)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(89, 14)
        Me.Label15.TabIndex = 157
        Me.Label15.Text = "degrees (0-360)."
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(315, 354)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(83, 14)
        Me.Label16.TabIndex = 152
        Me.Label16.Text = "degrees (0-15)."
        '
        'chkNegate
        '
        Me.chkNegate.BackColor = System.Drawing.SystemColors.Control
        Me.chkNegate.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkNegate.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNegate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkNegate.Location = New System.Drawing.Point(451, 346)
        Me.chkNegate.Name = "chkNegate"
        Me.chkNegate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkNegate.Size = New System.Drawing.Size(114, 17)
        Me.chkNegate.TabIndex = 158
        Me.chkNegate.Text = "Negate the image"
        Me.chkNegate.UseVisualStyleBackColor = False
        '
        'txtRotAngle
        '
        Me.txtRotAngle.AcceptsReturn = True
        Me.txtRotAngle.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtRotAngle.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRotAngle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRotAngle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtRotAngle.Location = New System.Drawing.Point(515, 322)
        Me.txtRotAngle.MaxLength = 0
        Me.txtRotAngle.Name = "txtRotAngle"
        Me.txtRotAngle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRotAngle.Size = New System.Drawing.Size(26, 20)
        Me.txtRotAngle.TabIndex = 156
        Me.txtRotAngle.Text = "0"
        '
        'chkRotAngle
        '
        Me.chkRotAngle.BackColor = System.Drawing.SystemColors.Control
        Me.chkRotAngle.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRotAngle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRotAngle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRotAngle.Location = New System.Drawing.Point(451, 322)
        Me.chkRotAngle.Name = "chkRotAngle"
        Me.chkRotAngle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRotAngle.Size = New System.Drawing.Size(64, 17)
        Me.chkRotAngle.TabIndex = 155
        Me.chkRotAngle.Text = "Rotate"
        Me.chkRotAngle.UseVisualStyleBackColor = False
        '
        'chkRot180
        '
        Me.chkRot180.BackColor = System.Drawing.SystemColors.Control
        Me.chkRot180.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRot180.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRot180.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRot180.Location = New System.Drawing.Point(451, 290)
        Me.chkRot180.Name = "chkRot180"
        Me.chkRot180.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRot180.Size = New System.Drawing.Size(120, 25)
        Me.chkRot180.TabIndex = 154
        Me.chkRot180.Text = "Rotate 180 degrees"
        Me.chkRot180.UseVisualStyleBackColor = False
        '
        'chkRot90
        '
        Me.chkRot90.BackColor = System.Drawing.SystemColors.Control
        Me.chkRot90.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkRot90.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRot90.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRot90.Location = New System.Drawing.Point(451, 266)
        Me.chkRot90.Name = "chkRot90"
        Me.chkRot90.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkRot90.Size = New System.Drawing.Size(137, 21)
        Me.chkRot90.TabIndex = 153
        Me.chkRot90.Text = "Rotate 90 degrees"
        Me.chkRot90.UseVisualStyleBackColor = False
        '
        'txtShear
        '
        Me.txtShear.AcceptsReturn = True
        Me.txtShear.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtShear.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtShear.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtShear.Location = New System.Drawing.Point(283, 346)
        Me.txtShear.MaxLength = 0
        Me.txtShear.Name = "txtShear"
        Me.txtShear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtShear.Size = New System.Drawing.Size(26, 20)
        Me.txtShear.TabIndex = 151
        Me.txtShear.Text = "0"
        '
        'chkShear
        '
        Me.chkShear.BackColor = System.Drawing.SystemColors.Control
        Me.chkShear.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkShear.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkShear.Location = New System.Drawing.Point(171, 346)
        Me.chkShear.Name = "chkShear"
        Me.chkShear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkShear.Size = New System.Drawing.Size(105, 17)
        Me.chkShear.TabIndex = 150
        Me.chkShear.Text = "Shear the image"
        Me.chkShear.UseVisualStyleBackColor = False
        '
        'chkSmoothZoom
        '
        Me.chkSmoothZoom.BackColor = System.Drawing.SystemColors.Control
        Me.chkSmoothZoom.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkSmoothZoom.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSmoothZoom.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkSmoothZoom.Location = New System.Drawing.Point(171, 322)
        Me.chkSmoothZoom.Name = "chkSmoothZoom"
        Me.chkSmoothZoom.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkSmoothZoom.Size = New System.Drawing.Size(169, 17)
        Me.chkSmoothZoom.TabIndex = 149
        Me.chkSmoothZoom.Text = "Smooth and Zoom the image"
        Me.chkSmoothZoom.UseVisualStyleBackColor = False
        '
        'chkFlip
        '
        Me.chkFlip.BackColor = System.Drawing.SystemColors.Control
        Me.chkFlip.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkFlip.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFlip.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkFlip.Location = New System.Drawing.Point(171, 290)
        Me.chkFlip.Name = "chkFlip"
        Me.chkFlip.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkFlip.Size = New System.Drawing.Size(105, 25)
        Me.chkFlip.TabIndex = 148
        Me.chkFlip.Text = "Flip the image"
        Me.chkFlip.UseVisualStyleBackColor = False
        '
        'chkMirror
        '
        Me.chkMirror.BackColor = System.Drawing.SystemColors.Control
        Me.chkMirror.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkMirror.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMirror.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkMirror.Location = New System.Drawing.Point(171, 266)
        Me.chkMirror.Name = "chkMirror"
        Me.chkMirror.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkMirror.Size = New System.Drawing.Size(105, 21)
        Me.chkMirror.TabIndex = 147
        Me.chkMirror.Text = "Mirror the image"
        Me.chkMirror.UseVisualStyleBackColor = False
        '
        'chkRemoveLines
        '
        Me.chkRemoveLines.BackColor = System.Drawing.SystemColors.Control
        Me.chkRemoveLines.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkRemoveLines.Location = New System.Drawing.Point(171, 170)
        Me.chkRemoveLines.Name = "chkRemoveLines"
        Me.chkRemoveLines.Size = New System.Drawing.Size(195, 24)
        Me.chkRemoveLines.TabIndex = 146
        Me.chkRemoveLines.Text = "Remove lines of minimum length"
        Me.chkRemoveLines.UseVisualStyleBackColor = False
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.SystemColors.Control
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(443, 202)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label17.Size = New System.Drawing.Size(160, 14)
        Me.Label17.TabIndex = 143
        Me.Label17.Text = "pixels and a height smaller than"
        '
        'chkDespeckle
        '
        Me.chkDespeckle.BackColor = System.Drawing.SystemColors.Control
        Me.chkDespeckle.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDespeckle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDespeckle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDespeckle.Location = New System.Drawing.Point(171, 200)
        Me.chkDespeckle.Name = "chkDespeckle"
        Me.chkDespeckle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDespeckle.Size = New System.Drawing.Size(240, 16)
        Me.chkDespeckle.TabIndex = 141
        Me.chkDespeckle.Text = "Delete all speckles with a width smaller than"
        Me.chkDespeckle.UseVisualStyleBackColor = False
        '
        'txtSpeckleW
        '
        Me.txtSpeckleW.AcceptsReturn = True
        Me.txtSpeckleW.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtSpeckleW.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpeckleW.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpeckleW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtSpeckleW.Location = New System.Drawing.Point(411, 197)
        Me.txtSpeckleW.MaxLength = 0
        Me.txtSpeckleW.Name = "txtSpeckleW"
        Me.txtSpeckleW.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpeckleW.Size = New System.Drawing.Size(26, 20)
        Me.txtSpeckleW.TabIndex = 142
        Me.txtSpeckleW.Text = "2"
        '
        'txtSpeckleH
        '
        Me.txtSpeckleH.AcceptsReturn = True
        Me.txtSpeckleH.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtSpeckleH.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpeckleH.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpeckleH.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtSpeckleH.Location = New System.Drawing.Point(603, 197)
        Me.txtSpeckleH.MaxLength = 0
        Me.txtSpeckleH.Name = "txtSpeckleH"
        Me.txtSpeckleH.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpeckleH.Size = New System.Drawing.Size(26, 20)
        Me.txtSpeckleH.TabIndex = 144
        Me.txtSpeckleH.Text = "2"
        '
        'Frame2
        '
        Me.Frame2.BackColor = System.Drawing.Color.Transparent
        Me.Frame2.Controls.Add(Me.CheckBox1)
        Me.Frame2.Controls.Add(Me.RadioButton1)
        Me.Frame2.Controls.Add(Me.RadioButton2)
        Me.Frame2.Controls.Add(Me.RadioButton3)
        Me.Frame2.Controls.Add(Me.RadioButton4)
        Me.Frame2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame2.Location = New System.Drawing.Point(74, 121)
        Me.Frame2.Name = "Frame2"
        Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame2.Size = New System.Drawing.Size(677, 43)
        Me.Frame2.TabIndex = 140
        Me.Frame2.TabStop = False
        '
        'CheckBox1
        '
        Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CheckBox1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CheckBox1.Location = New System.Drawing.Point(6, 13)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBox1.Size = New System.Drawing.Size(108, 17)
        Me.CheckBox1.TabIndex = 14
        Me.CheckBox1.Text = "Dilate the image"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton1.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton1.Location = New System.Drawing.Point(120, 12)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton1.Size = New System.Drawing.Size(112, 22)
        Me.RadioButton1.TabIndex = 13
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Vertical (up side)"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton2.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton2.Location = New System.Drawing.Point(238, 10)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton2.Size = New System.Drawing.Size(127, 28)
        Me.RadioButton2.TabIndex = 12
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Horizontal (left side)"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton3
        '
        Me.RadioButton3.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton3.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton3.Location = New System.Drawing.Point(366, 14)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton3.Size = New System.Drawing.Size(136, 24)
        Me.RadioButton3.TabIndex = 11
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Diagonal (up/left side)"
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'RadioButton4
        '
        Me.RadioButton4.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton4.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton4.Location = New System.Drawing.Point(505, 14)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton4.Size = New System.Drawing.Size(159, 20)
        Me.RadioButton4.TabIndex = 10
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "All Directions (up/left side)"
        Me.RadioButton4.UseVisualStyleBackColor = False
        '
        'Frame1
        '
        Me.Frame1.BackColor = System.Drawing.SystemColors.Control
        Me.Frame1.Controls.Add(Me.RadioButton5)
        Me.Frame1.Controls.Add(Me.RadioButton6)
        Me.Frame1.Controls.Add(Me.RadioButton7)
        Me.Frame1.Controls.Add(Me.RadioButton8)
        Me.Frame1.Controls.Add(Me.CheckBox2)
        Me.Frame1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Frame1.Location = New System.Drawing.Point(71, 69)
        Me.Frame1.Name = "Frame1"
        Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Frame1.Size = New System.Drawing.Size(677, 43)
        Me.Frame1.TabIndex = 139
        Me.Frame1.TabStop = False
        '
        'RadioButton5
        '
        Me.RadioButton5.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton5.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton5.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton5.Location = New System.Drawing.Point(508, 13)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton5.Size = New System.Drawing.Size(159, 23)
        Me.RadioButton5.TabIndex = 8
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "All Directions (up/left side)"
        Me.RadioButton5.UseVisualStyleBackColor = False
        '
        'RadioButton6
        '
        Me.RadioButton6.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton6.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton6.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton6.Location = New System.Drawing.Point(366, 14)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton6.Size = New System.Drawing.Size(159, 21)
        Me.RadioButton6.TabIndex = 7
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "Diagonal (up/left side)"
        Me.RadioButton6.UseVisualStyleBackColor = False
        '
        'RadioButton7
        '
        Me.RadioButton7.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton7.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton7.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton7.Location = New System.Drawing.Point(238, 11)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton7.Size = New System.Drawing.Size(137, 27)
        Me.RadioButton7.TabIndex = 6
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "Horizontal (left side)"
        Me.RadioButton7.UseVisualStyleBackColor = False
        '
        'RadioButton8
        '
        Me.RadioButton8.BackColor = System.Drawing.Color.Transparent
        Me.RadioButton8.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadioButton8.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RadioButton8.Location = New System.Drawing.Point(120, 15)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RadioButton8.Size = New System.Drawing.Size(121, 23)
        Me.RadioButton8.TabIndex = 5
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "Vertical (up side)"
        Me.RadioButton8.UseVisualStyleBackColor = False
        '
        'CheckBox2
        '
        Me.CheckBox2.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.CheckBox2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CheckBox2.Location = New System.Drawing.Point(6, 15)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBox2.Size = New System.Drawing.Size(136, 17)
        Me.CheckBox2.TabIndex = 4
        Me.CheckBox2.Text = "Erode the image"
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.SystemColors.Control
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(635, 202)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label18.Size = New System.Drawing.Size(38, 14)
        Me.Label18.TabIndex = 145
        Me.Label18.Text = "pixels."
        '
        'BitonalPage
        '
        Me.BitonalPage.BackColor = System.Drawing.SystemColors.Control
        Me.BitonalPage.Controls.Add(Me.BitonalTabRoll)
        Me.BitonalPage.Location = New System.Drawing.Point(4, 22)
        Me.BitonalPage.Name = "BitonalPage"
        Me.BitonalPage.Padding = New System.Windows.Forms.Padding(3)
        Me.BitonalPage.Size = New System.Drawing.Size(817, 506)
        Me.BitonalPage.TabIndex = 0
        Me.BitonalPage.Text = "ScanFix Xpress Bitonal Edition"
        '
        'PreviewButton
        '
        Me.PreviewButton.Location = New System.Drawing.Point(12, 802)
        Me.PreviewButton.Name = "PreviewButton"
        Me.PreviewButton.Size = New System.Drawing.Size(96, 23)
        Me.PreviewButton.TabIndex = 10
        Me.PreviewButton.Text = "Preview"
        Me.PreviewButton.UseVisualStyleBackColor = True
        '
        'ProcessButton
        '
        Me.ProcessButton.Location = New System.Drawing.Point(117, 802)
        Me.ProcessButton.Name = "ProcessButton"
        Me.ProcessButton.Size = New System.Drawing.Size(96, 23)
        Me.ProcessButton.TabIndex = 11
        Me.ProcessButton.Text = "Process"
        Me.ProcessButton.UseVisualStyleBackColor = True
        '
        'CloseButton
        '
        Me.CloseButton.Location = New System.Drawing.Point(219, 802)
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(96, 23)
        Me.CloseButton.TabIndex = 12
        Me.CloseButton.Text = "Close"
        Me.CloseButton.UseVisualStyleBackColor = True
        '
        'EditionNote
        '
        Me.EditionNote.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.EditionNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditionNote.Location = New System.Drawing.Point(475, 790)
        Me.EditionNote.Name = "EditionNote"
        Me.EditionNote.Size = New System.Drawing.Size(340, 39)
        Me.EditionNote.TabIndex = 13
        Me.EditionNote.Text = "NOTE: You can only use the Bitonal tab if you're registered for ScanFix Bitonal E" & _
            "dition."
        '
        'chkDilate1
        '
        Me.chkDilate1.BackColor = System.Drawing.SystemColors.Control
        Me.chkDilate1.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDilate1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDilate1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDilate1.Location = New System.Drawing.Point(5, 9)
        Me.chkDilate1.Name = "chkDilate1"
        Me.chkDilate1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDilate1.Size = New System.Drawing.Size(97, 17)
        Me.chkDilate1.TabIndex = 14
        Me.chkDilate1.Text = "Dilate the image"
        Me.chkDilate1.UseVisualStyleBackColor = False
        '
        'optDilate1_0
        '
        Me.optDilate1_0.BackColor = System.Drawing.SystemColors.Control
        Me.optDilate1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optDilate1_0.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optDilate1_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optDilate1_0.Location = New System.Drawing.Point(108, 10)
        Me.optDilate1_0.Name = "optDilate1_0"
        Me.optDilate1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optDilate1_0.Size = New System.Drawing.Size(104, 16)
        Me.optDilate1_0.TabIndex = 13
        Me.optDilate1_0.TabStop = True
        Me.optDilate1_0.Text = "Vertical (up side)"
        Me.optDilate1_0.UseVisualStyleBackColor = False
        '
        'optDilate1_1
        '
        Me.optDilate1_1.BackColor = System.Drawing.SystemColors.Control
        Me.optDilate1_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optDilate1_1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optDilate1_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optDilate1_1.Location = New System.Drawing.Point(214, 10)
        Me.optDilate1_1.Name = "optDilate1_1"
        Me.optDilate1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optDilate1_1.Size = New System.Drawing.Size(118, 15)
        Me.optDilate1_1.TabIndex = 12
        Me.optDilate1_1.TabStop = True
        Me.optDilate1_1.Text = "Horizontal (left side)"
        Me.optDilate1_1.UseVisualStyleBackColor = False
        '
        'optDilate1_2
        '
        Me.optDilate1_2.BackColor = System.Drawing.SystemColors.Control
        Me.optDilate1_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.optDilate1_2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optDilate1_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optDilate1_2.Location = New System.Drawing.Point(332, 10)
        Me.optDilate1_2.Name = "optDilate1_2"
        Me.optDilate1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optDilate1_2.Size = New System.Drawing.Size(129, 15)
        Me.optDilate1_2.TabIndex = 11
        Me.optDilate1_2.TabStop = True
        Me.optDilate1_2.Text = "Diagonal (up/left side)"
        Me.optDilate1_2.UseVisualStyleBackColor = False
        '
        'optDilate1_3
        '
        Me.optDilate1_3.BackColor = System.Drawing.SystemColors.Control
        Me.optDilate1_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.optDilate1_3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optDilate1_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optDilate1_3.Location = New System.Drawing.Point(462, 10)
        Me.optDilate1_3.Name = "optDilate1_3"
        Me.optDilate1_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optDilate1_3.Size = New System.Drawing.Size(149, 14)
        Me.optDilate1_3.TabIndex = 10
        Me.optDilate1_3.TabStop = True
        Me.optDilate1_3.Text = "All Directions (up/left side)"
        Me.optDilate1_3.UseVisualStyleBackColor = False
        '
        'optErode1_3_3
        '
        Me.optErode1_3_3.BackColor = System.Drawing.SystemColors.Control
        Me.optErode1_3_3.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErode1_3_3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optErode1_3_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErode1_3_3.Location = New System.Drawing.Point(462, 10)
        Me.optErode1_3_3.Name = "optErode1_3_3"
        Me.optErode1_3_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErode1_3_3.Size = New System.Drawing.Size(149, 14)
        Me.optErode1_3_3.TabIndex = 8
        Me.optErode1_3_3.TabStop = True
        Me.optErode1_3_3.Text = "All Directions (up/left side)"
        Me.optErode1_3_3.UseVisualStyleBackColor = False
        '
        'optErode1_3_2
        '
        Me.optErode1_3_2.BackColor = System.Drawing.SystemColors.Control
        Me.optErode1_3_2.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErode1_3_2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optErode1_3_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErode1_3_2.Location = New System.Drawing.Point(332, 10)
        Me.optErode1_3_2.Name = "optErode1_3_2"
        Me.optErode1_3_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErode1_3_2.Size = New System.Drawing.Size(129, 15)
        Me.optErode1_3_2.TabIndex = 7
        Me.optErode1_3_2.TabStop = True
        Me.optErode1_3_2.Text = "Diagonal (up/left side)"
        Me.optErode1_3_2.UseVisualStyleBackColor = False
        '
        'optErode1_3_1
        '
        Me.optErode1_3_1.BackColor = System.Drawing.SystemColors.Control
        Me.optErode1_3_1.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErode1_3_1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optErode1_3_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErode1_3_1.Location = New System.Drawing.Point(214, 10)
        Me.optErode1_3_1.Name = "optErode1_3_1"
        Me.optErode1_3_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErode1_3_1.Size = New System.Drawing.Size(118, 15)
        Me.optErode1_3_1.TabIndex = 6
        Me.optErode1_3_1.TabStop = True
        Me.optErode1_3_1.Text = "Horizontal (left side)"
        Me.optErode1_3_1.UseVisualStyleBackColor = False
        '
        'optErode1_3_0
        '
        Me.optErode1_3_0.BackColor = System.Drawing.SystemColors.Control
        Me.optErode1_3_0.Cursor = System.Windows.Forms.Cursors.Default
        Me.optErode1_3_0.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optErode1_3_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optErode1_3_0.Location = New System.Drawing.Point(108, 10)
        Me.optErode1_3_0.Name = "optErode1_3_0"
        Me.optErode1_3_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.optErode1_3_0.Size = New System.Drawing.Size(104, 16)
        Me.optErode1_3_0.TabIndex = 5
        Me.optErode1_3_0.TabStop = True
        Me.optErode1_3_0.Text = "Vertical (up side)"
        Me.optErode1_3_0.UseVisualStyleBackColor = False
        '
        'chkErode1
        '
        Me.chkErode1.BackColor = System.Drawing.SystemColors.Control
        Me.chkErode1.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkErode1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkErode1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkErode1.Location = New System.Drawing.Point(5, 9)
        Me.chkErode1.Name = "chkErode1"
        Me.chkErode1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkErode1.Size = New System.Drawing.Size(99, 17)
        Me.chkErode1.TabIndex = 4
        Me.chkErode1.Text = "Erode the image"
        Me.chkErode1.UseVisualStyleBackColor = False
        '
        'ProcessScreen
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(839, 837)
        Me.Controls.Add(Me.ProcessButton)
        Me.Controls.Add(Me.PreviewButton)
        Me.Controls.Add(Me.EditionNote)
        Me.Controls.Add(Me.CloseButton)
        Me.Controls.Add(Me.TabRoll)
        Me.Controls.Add(Me.OutputImgView)
        Me.Controls.Add(Me.InputImgView)
        Me.Controls.Add(Me.lblout)
        Me.Controls.Add(Me.lblinput)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "ProcessScreen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PreProcess Setup - Image Transformation"
        Me.tabinverse.ResumeLayout(False)
        Me.tabinverse.PerformLayout()
        Me.tabcomb.ResumeLayout(False)
        Me.tabcomb.PerformLayout()
        Me.tabnegpage.ResumeLayout(False)
        Me.tabnegpage.PerformLayout()
        Me.tabsmooth.ResumeLayout(False)
        Me.tabsmooth.PerformLayout()
        Me.tabdotshade.ResumeLayout(False)
        Me.tabdotshade.PerformLayout()
        Me.tabborder.ResumeLayout(False)
        Me.tabborder.PerformLayout()
        Me.tabblob.ResumeLayout(False)
        Me.tabblob.PerformLayout()
        Me.BitonalTabRoll.ResumeLayout(False)
        Me.TabRoll.ResumeLayout(False)
        Me.LitePage.ResumeLayout(False)
        Me.LitePage.PerformLayout()
        Me.Frame2.ResumeLayout(False)
        Me.Frame1.ResumeLayout(False)
        Me.BitonalPage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub MainScreen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        '***Must call the UnlockRuntime method before the application begins
	'The unlock codes shown below are for formatting purposes only and will not work!
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        'ScanFix1.License.UnlockRuntime(1234, 1234, 1234, 1234)


        ComboBox2.SelectedIndex = 0

        scanFix1 = New PegasusImaging.WinForms.ScanFix5.ScanFix()

        scanFix1.License.LicenseEdition = PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.BitonalEdition

    End Sub

    Private Sub scrollx_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrollx.ValueChanged
        If scrollx.Value > scrollw.Value Then
            scrollx.Value = scrollw.Value
        End If
        If scrollx.Value > InputImgView.Image.ImageXData.Width Then
            scrollx.Value = InputImgView.Image.ImageXData.Width
        End If
        Label8.Text = scrollx.Value
    End Sub

    Private Sub scrolly_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrolly.ValueChanged
        If scrolly.Value > scrolh.Value Then
            scrolly.Value = scrolh.Value
        End If
        If scrolly.Value > InputImgView.Image.ImageXData.Height Then
            scrolly.Value = InputImgView.Image.ImageXData.Height
        End If
        Label9.Text = scrolly.Value
    End Sub

    Private Sub scrollw_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrollw.ValueChanged
        If scrollw.Value < scrollx.Value Then
            scrollw.Value = scrollx.Value
        End If
        If scrollw.Value > InputImgView.Image.ImageXData.Width Then
            scrollw.Value = InputImgView.Image.ImageXData.Width - 1
        End If
        Label10.Text = scrollw.Value
    End Sub

    Private Sub scrolh_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrolh.ValueChanged
        If scrolh.Value < scrollx.Value Then
            scrolh.Value = scrollx.Value
        End If
        If scrolh.Value > InputImgView.Image.ImageXData.Height Then
            scrolh.Value = InputImgView.Image.ImageXData.Height - 1
        End If
        Label11.Text = scrolh.Value
    End Sub

    Private Sub scrollmin_Changed(ByVal sender As System.Object, ByVal e As EventArgs) Handles scrollmin.ValueChanged
        If scrollmin.Value > scrollm.Value Then
            scrollmin.Value = scrollm.Value
        End If
        Label12.Text = scrollmin.Value
    End Sub

    Private Sub scrollm_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles scrollm.Scroll
        If scrollm.Value < scrollmin.Value Then
            scrollm.Value = scrollmin.Value
        End If
        Label13.Text = scrollm.Value
    End Sub

    Private Sub scrollden_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles scrollden.Scroll
        Label14.Text = scrollden.Value
    End Sub

    Private Sub tabblob_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabblob.Paint
        Label8.Text = scrollx.Value
        Label9.Text = scrolly.Value
        Label10.Text = scrollw.Value
        Label11.Text = scrolh.Value
        Label12.Text = scrollmin.Value
        Label13.Text = scrollm.Value
        Label14.Text = scrollden.Value
    End Sub
    Private Sub minwidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles minwidth.Scroll
        Label22.Text = minwidth.Value
    End Sub

    Private Sub minheight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles minheight.Scroll
        Label23.Text = minheight.Value
    End Sub

    Private Sub minblack_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles minblack.Scroll
        Label24.Text = minblack.Value
    End Sub

    Private Sub tabinverse_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabinverse.Paint
        Label22.Text = minwidth.Value
        Label23.Text = minheight.Value
        Label24.Text = minblack.Value
    End Sub

    Private Sub HScrollBar13_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar13.Scroll
        Label42.Text = HScrollBar13.Value
    End Sub

    Private Sub HScrollBar12_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar12.Scroll
        Label41.Text = HScrollBar12.Value
    End Sub

    Private Sub HScrollBar11_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar11.Scroll
        Label40.Text = HScrollBar11.Value
    End Sub

    Private Sub HScrollBar10_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar10.Scroll
        Label39.Text = HScrollBar10.Value
    End Sub

    Private Sub HScrollBar9_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar9.Scroll
        Label32.Text = HScrollBar9.Value
    End Sub

    Private Sub HScrollBar17_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar17.Scroll
        Label51.Text = HScrollBar17.Value
    End Sub

    Private Sub HScrollBar16_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar16.Scroll
        Label50.Text = HScrollBar16.Value
    End Sub

    Private Sub HScrollBar15_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar15.Scroll
        Label49.Text = HScrollBar15.Value
    End Sub

    Private Sub HScrollBar14_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar14.Scroll
        Label43.Text = HScrollBar14.Value
    End Sub

    Private Sub HScrollBar2_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar2.Scroll
        Label25.Text = HScrollBar2.Value
    End Sub

    Private Sub tabcomb_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabcomb.Paint
        Label42.Text = HScrollBar13.Value
        Label41.Text = HScrollBar12.Value
        Label40.Text = HScrollBar11.Value
        Label39.Text = HScrollBar10.Value
        Label32.Text = HScrollBar9.Value
        Label51.Text = HScrollBar17.Value
        Label50.Text = HScrollBar16.Value
        Label49.Text = HScrollBar15.Value
        Label43.Text = HScrollBar14.Value
        Label25.Text = HScrollBar2.Value
    End Sub

    Private Sub HScrollBar23_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar23.Scroll
        Label62.Text = HScrollBar23.Value
    End Sub

    Private Sub HScrollBar22_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar22.Scroll
        Label61.Text = HScrollBar22.Value
    End Sub

    Private Sub HScrollBar21_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar21.Scroll
        Label60.Text = HScrollBar21.Value
    End Sub

    Private Sub HScrollBar20_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar20.Scroll
        Label59.Text = HScrollBar20.Value
    End Sub

    Private Sub HScrollBar19_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar19.Scroll
        Label58.Text = HScrollBar19.Value
    End Sub

    Private Sub HScrollBar18_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar18.Scroll
        Label57.Text = HScrollBar18.Value
    End Sub

    Private Sub tabdotshade_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabdotshade.Paint
        Label62.Text = HScrollBar23.Value
        Label61.Text = HScrollBar22.Value
        Label60.Text = HScrollBar21.Value
        Label59.Text = HScrollBar20.Value
        Label58.Text = HScrollBar19.Value
        Label57.Text = HScrollBar18.Value
    End Sub

    Private Sub tabnegpage_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabnegpage.Paint
        Label69.Text = HScrollBar24.Value
        Label70.Text = HScrollBar25.Value
    End Sub

    Private Sub HScrollBar25_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar25.Scroll
        Label70.Text = HScrollBar25.Value
    End Sub

    Private Sub HScrollBar24_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar24.Scroll
        Label69.Text = HScrollBar24.Value
    End Sub

    Private Sub HScrollBar41_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar41.Scroll
        Label100.Text = HScrollBar41.Value
    End Sub

    Private Sub HScrollBar40_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar40.Scroll
        Label99.Text = HScrollBar40.Value
    End Sub

    Private Sub HScrollBar39_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar39.Scroll
        Label98.Text = HScrollBar39.Value
    End Sub

    Private Sub HScrollBar38_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar38.Scroll
        Label97.Text = HScrollBar38.Value
    End Sub

    Private Sub HScrollBar36_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar36.Scroll
        Label90.Text = HScrollBar36.Value
    End Sub

    Private Sub HScrollBar35_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar35.Scroll
        Label89.Text = HScrollBar35.Value
    End Sub

    Private Sub HScrollBar34_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar34.Scroll
        Label88.Text = HScrollBar34.Value
    End Sub

    Private Sub HScrollBar33_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar33.Scroll
        Label87.Text = HScrollBar33.Value
    End Sub

    Private Sub tabborder_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabborder.Paint
        Label100.Text = HScrollBar41.Value
        Label99.Text = HScrollBar40.Value
        Label98.Text = HScrollBar39.Value
        Label97.Text = HScrollBar38.Value
        Label90.Text = HScrollBar36.Value
        Label89.Text = HScrollBar35.Value
        Label88.Text = HScrollBar34.Value
        Label87.Text = HScrollBar33.Value
    End Sub

    Private Sub tabsmooth_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles tabsmooth.Paint
        Label86.Text = HScrollBar32.Value
    End Sub

    Private Sub HScrollBar32_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles HScrollBar32.Scroll
        Label86.Text = HScrollBar32.Value
    End Sub

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Close()
    End Sub

    Private Sub PreviewButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewButton.Click
        PreviewFlag = True
        ProcessButton_Click(sender, e)
    End Sub

    Dim comb As PegasusImaging.WinForms.ScanFix5.CombRemovalOptions
    Dim dot As PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions
    Dim invtext As PegasusImaging.WinForms.ScanFix5.InverseTextOptions
    Dim neg As PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions
    Dim blob As PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions
    Dim border As PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions

    Private Sub ProcessButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcessButton.Click
        PreProcessedFlag = True

        Dim proc As PegasusImaging.WinForms.ImagXpress8.Processor = New PegasusImaging.WinForms.ImagXpress8.Processor(mainImage.Image)

        If Not (mainImage.Image.ImageXData.BitsPerPixel = 0) Then

            proc.ColorDepth(1, 0, 0)
        End If


        scanFix1.FromHdib(InputImgView.Image.ToHdib(False))


        If (chkMirror.Checked = True) Then

            scanFix1.Mirror()
        End If

        If (chkFlip.Checked = True) Then

            scanFix1.Flip()
        End If
        If (chkSmoothZoom.Checked = True) Then

            scanFix1.SmoothZoom()
        End If
        If (chkShear.Checked = True) Then

            scanFix1.Shear(Int32.Parse(txtShear.Text))
        End If
        If (chkRot90.Checked = True) Then

            scanFix1.Rotate(90)
        End If
        If (chkRot180.Checked = True) Then

            scanFix1.Rotate(180)
        End If
        If (chkNegate.Checked = True) Then

            scanFix1.Negate()
        End If
        If (chkRotAngle.Checked = True) Then

            scanFix1.Rotate(Int32.Parse(txtRotAngle.Text))
        End If

        If (CheckBox2.Checked = True) Then

            Dim ErodeOpts As PegasusImaging.WinForms.ScanFix5.ErodeOptions = New ErodeOptions()
            ErodeOpts.Amount = 2
            Dim ErodeDir As PegasusImaging.WinForms.ScanFix5.EnhancementDirections = New EnhancementDirections()

            If (RadioButton8.Checked = True) Then

                ErodeDir = EnhancementDirections.Down
            End If
            If (RadioButton7.Checked = True) Then

                ErodeDir = EnhancementDirections.Right
            End If
            If (RadioButton6.Checked = True) Then

                ErodeDir = CType(EnhancementDirections.Down.GetHashCode() + EnhancementDirections.Right.GetHashCode(), EnhancementDirections)
                ErodeOpts.OnlyDiagonal = True
            End If
            If (RadioButton5.Checked = True) Then

                ErodeDir = EnhancementDirections.All


                ErodeOpts.Direction = ErodeDir
                scanFix1.Erode(ErodeOpts)

            End If

        End If


        If (CheckBox1.Checked = True) Then

            Dim DilateOpts As DilateOptions = New DilateOptions()
            DilateOpts.Amount = 2
            Dim DilateDir As EnhancementDirections = New EnhancementDirections()


            If (RadioButton1.Checked = True) Then

                DilateDir = EnhancementDirections.Down
            End If
            If (RadioButton2.Checked = True) Then

                DilateDir = EnhancementDirections.Right
            End If
            If (RadioButton3.Checked = True) Then

                DilateDir = CType(EnhancementDirections.Down.GetHashCode() + EnhancementDirections.Right.GetHashCode(), EnhancementDirections)
                DilateOpts.OnlyDiagonal = True
            End If
            If (RadioButton4.Checked = True) Then

                DilateDir = EnhancementDirections.All


                DilateOpts.Direction = DilateDir
                scanFix1.Dilate(DilateOpts)
            End If

        End If


        If (chkRemoveLines.Checked = True) Then

            Dim LineOpts As LineRemovalOptions = New LineRemovalOptions()
            LineOpts.MinimumLength = Int32.Parse(LengthTextBox.Text)
            scanFix1.RemoveLines(LineOpts)

        End If

        If (chkDespeckle.Checked = True) Then

            Dim DespeckOpts As DespeckleOptions = New DespeckleOptions()
            DespeckOpts.SpeckWidth = Int32.Parse(txtSpeckleW.Text)
            DespeckOpts.SpeckHeight = Int32.Parse(txtSpeckleH.Text)
            scanFix1.Despeckle(DespeckOpts)

        End If
        If (chkDeskew.Checked = True) Then

            scanFix1.Deskew()

        End If

        If (CombCheckBox.Checked = True) Then

            comb = New PegasusImaging.WinForms.ScanFix5.CombRemovalOptions()

            Dim rec As System.Drawing.Rectangle = New System.Drawing.Rectangle()
            rec.X = HScrollBar13.Value
            rec.Y = HScrollBar12.Value
            rec.Width = HScrollBar11.Value
            rec.Height = HScrollBar10.Value
            comb.Area = rec
            comb.CombHeight = HScrollBar9.Value
            comb.CombSpacing = HScrollBar17.Value
            comb.MinimumCombLength = HScrollBar16.Value
            comb.MinimumConfidence = HScrollBar15.Value
            comb.HorizontalLineThickness = HScrollBar14.Value
            comb.VerticalLineThickness = HScrollBar2.Value

            Try

                scanFix1.RemoveCombs(comb)

            Catch ex As PegasusImaging.WinForms.ScanFix5.ScanFixException

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            scanFix1.RemoveCombs(comb)
        End If

        If (NegCheckBox.Checked = True) Then

            neg = New PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions()

            neg.ApplyCorrection = CheckBox3.Checked
            neg.MinimumConfidence = HScrollBar25.Value
            neg.Quality = HScrollBar24.Value

            scanFix1.CorrectNegativePage(neg)

        End If


        If (SmoothObjectsCheckBox.Checked = True) Then

            scanFix1.SmoothObjects(HScrollBar32.Value)

        End If

        If (InverseCheckBox.Checked = True) Then

            invtext = New PegasusImaging.WinForms.ScanFix5.InverseTextOptions()

            invtext.MinimumAreaHeight = minheight.Value
            invtext.MinimumAreaWidth = minwidth.Value
            invtext.MinimumBlankOnEdges = minblack.Value

            scanFix1.CorrectInverseText(invtext)

        End If

        If (BlobCheckBox.Checked = True) Then

            blob = New PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions()

            blob.Area = New System.Drawing.Rectangle(scrollx.Value, scrolly.Value, scrollw.Value, scrolh.Value)
            blob.MaximumPixelCount = scrollm.Value
            blob.MinimumPixelCount = scrollmin.Value
            blob.MinimumDensity = scrollden.Value

            scanFix1.RemoveBlobs(blob)

        End If

        If (DotCheckBox.Checked = True) Then

            dot = New PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions()

            dot.DensityAdjustment = HScrollBar19.Value
            dot.MaximumDotSize = HScrollBar18.Value
            dot.HorizontalSizeAdjustment = HScrollBar20.Value
            dot.VerticalSizeAdjustment = HScrollBar21.Value
            dot.MinimumAreaHeight = HScrollBar22.Value
            dot.MinimumAreaWidth = HScrollBar23.Value

            scanFix1.RemoveDotShading(dot)
        End If

        If (BorderCheckBox.Checked = True) Then

            border = New PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions()

            border.BorderSpeckSize = HScrollBar41.Value
            border.CropBorder = CheckBox16.Checked
            border.DeskewBorder = CheckBox18.Checked
            border.MaximumPageHeight = HScrollBar33.Value
            border.MaximumPageWidth = HScrollBar34.Value
            border.MinimumConfidence = HScrollBar38.Value
            border.MinimumPageHeight = HScrollBar35.Value
            border.MinimumPageWidth = HScrollBar36.Value
            border.PageSpeckSize = HScrollBar40.Value
            border.Quality = HScrollBar39.Value
            border.ReplaceBorder = CheckBox17.Checked

            If (ComboBox2.SelectedIndex = 1) Then

                border.PadColor = System.Drawing.Color.FromArgb(0, 0, 0)

            Else

                border.PadColor = System.Drawing.Color.FromArgb(255, 255, 255)


                scanFix1.RemoveBorder(border)

            End If

        End If

        Dim DIB As Int32 = scanFix1.ToHdib(True).ToInt32()

        If (PreviewFlag = False) Then

            mainImage.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(DIB))
            mainTestImage.Image = mainImage.Image

        Else

            OutputImgView.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New System.IntPtr(DIB))
            PreviewFlag = False

        End If


        System.Runtime.InteropServices.Marshal.FreeHGlobal(New System.IntPtr(DIB))

    End Sub

    Private Sub ProcessScreen_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If ((mainImage.Rubberband.Dimensions.Height <= 0) Or (mainImage.Rubberband.Dimensions.Width <= 0)) Then

            InputImgView.Image = mainImage.Image

        Else

            InputImgView.Image = mainImage.Rubberband.Copy()
        End If
    End Sub
End Class
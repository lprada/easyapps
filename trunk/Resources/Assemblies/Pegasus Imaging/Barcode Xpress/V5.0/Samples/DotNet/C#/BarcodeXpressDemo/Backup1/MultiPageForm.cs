/****************************************************************
 * Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for MultiPageForm.
	/// </summary>
	public class MultiPageForm : System.Windows.Forms.Form
	{
		public System.Windows.Forms.TextBox PageBox;
        private System.Windows.Forms.TextBox CountBox;
		private System.Windows.Forms.Label MidLabel;
		private System.Windows.Forms.Label FirstLabel;
		private System.Windows.Forms.Button OKButton;
        private ListBox DescriptionListBox;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MultiPageForm(int count)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			CountBox.Text = count.ToString();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultiPageForm));
            this.PageBox = new System.Windows.Forms.TextBox();
            this.MidLabel = new System.Windows.Forms.Label();
            this.CountBox = new System.Windows.Forms.TextBox();
            this.FirstLabel = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.DescriptionListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // PageBox
            // 
            this.PageBox.Location = new System.Drawing.Point(152, 56);
            this.PageBox.Name = "PageBox";
            this.PageBox.Size = new System.Drawing.Size(80, 20);
            this.PageBox.TabIndex = 0;
            this.PageBox.Text = "1";
            // 
            // MidLabel
            // 
            this.MidLabel.Location = new System.Drawing.Point(264, 64);
            this.MidLabel.Name = "MidLabel";
            this.MidLabel.Size = new System.Drawing.Size(40, 16);
            this.MidLabel.TabIndex = 1;
            this.MidLabel.Text = "of ";
            // 
            // CountBox
            // 
            this.CountBox.Enabled = false;
            this.CountBox.Location = new System.Drawing.Point(320, 56);
            this.CountBox.Name = "CountBox";
            this.CountBox.Size = new System.Drawing.Size(56, 20);
            this.CountBox.TabIndex = 2;
            // 
            // FirstLabel
            // 
            this.FirstLabel.Location = new System.Drawing.Point(56, 64);
            this.FirstLabel.Name = "FirstLabel";
            this.FirstLabel.Size = new System.Drawing.Size(88, 16);
            this.FirstLabel.TabIndex = 3;
            this.FirstLabel.Text = "Open Page #";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(192, 104);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(88, 24);
            this.OKButton.TabIndex = 5;
            this.OKButton.Text = "OK";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // DescriptionListBox
            // 
            this.DescriptionListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionListBox.Items.AddRange(new object[] {
            "The file you\'re opening is a multi-page file.  Please indicate which ",
            "page you want to open below."});
            this.DescriptionListBox.Location = new System.Drawing.Point(8, 8);
            this.DescriptionListBox.Name = "DescriptionListBox";
            this.DescriptionListBox.Size = new System.Drawing.Size(456, 30);
            this.DescriptionListBox.TabIndex = 4;
            // 
            // MultiPageForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(474, 136);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.DescriptionListBox);
            this.Controls.Add(this.FirstLabel);
            this.Controls.Add(this.CountBox);
            this.Controls.Add(this.MidLabel);
            this.Controls.Add(this.PageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MultiPageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MultiPage File";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}

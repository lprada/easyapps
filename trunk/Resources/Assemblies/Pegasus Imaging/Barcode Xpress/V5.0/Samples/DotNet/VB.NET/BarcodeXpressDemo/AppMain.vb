'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Module AppMain
    Sub Main()
        Application.EnableVisualStyles()
        Dim form As FormSplash = New FormSplash()
        form.ShowDialog()

        Application.Run(New FormMain())
    End Sub
End Module

'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Public Class BinaryForm

    Public Sub New(ByVal result As PegasusImaging.WinForms.BarcodeXpress5.Result)
        InitializeComponent()

        barcodeCharArray = result.BarcodeData
        barcodeByteArray = result.BarcodeDataAsByte
        area = result.Area
        Name = result.BarcodeName
    End Sub

    Dim barcodeCharArray As Char()
    Dim barcodeByteArray As Byte()
    Dim area As System.Drawing.Rectangle
    Dim barcodename As String

    Private Sub BinaryForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BarcodeFound.Text = ""
        BarcodeFound.Text = "Type = " + Name + Chr(10)
        BarcodeFound.Text += "X - " + area.X.ToString() + " pixels" + Chr(10) + "Y - " + area.Y.ToString() + " pixels" + Chr(10)
        BarcodeFound.Text += "W - " + area.Width.ToString() + " pixels" + Chr(10) + "H - " + area.Height.ToString() + " pixels" + Chr(10)


        Dim hexDump As String = ""
        Dim j As Integer
        For j = 0 To barcodeByteArray.Length - 1

            If (barcodeByteArray(j) < 16) Then
                hexDump += Convert.ToChar(48) + barcodeByteArray(j).ToString("X") + Convert.ToChar(32)
            Else
                hexDump += barcodeByteArray(j).ToString("X") + Convert.ToChar(32)
            End If
        Next

        HexTextBox.Text = hexDump

        Dim copyBarcodeData(barcodeByteArray.Length) As Byte '= New Byte

        System.Array.Copy(barcodeByteArray, copyBarcodeData, barcodeByteArray.Length)

        Dim copyBarcodeChar(barcodeCharArray.Length) As Char '= New Char()
        Dim g As Integer = 0
        Dim k As Integer
        For k = 0 To barcodeCharArray.Length - 1
            If Not (barcodeByteArray(k) = 0) Then
                copyBarcodeChar(g) = Convert.ToChar(copyBarcodeData(k))
                g = g + 1
            End If
        Next

        Dim binaryBarcodeData As String = New String(copyBarcodeChar)

        ValueTextBox.Text = binaryBarcodeData

    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Close()
    End Sub
End Class
﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Read Barcodes ASP.NET Example - BarcodeXpress 5.0</title>
</head>
<body bgcolor="#ccffff">
    <form id="form1" runat="server">
    <div>
    
    <p align="center">    &nbsp;<asp:TextBox ID="TextBox1" runat="server" Font-Bold="True" Font-Size="X-Large" Font-Underline="True" Width="282px">Read Barcodes Example</asp:TextBox>&nbsp;</p>
        <p align="center">
            <asp:Image ID="Image1" runat="server" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px" Height="500px" ImageAlign="Middle" Width="500px" ImageUrl="~/original.jpg" />

        </p>

        <asp:Panel ID="Panel1" runat="server" Height="185px" Width="733px">
            &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" Width="594px" />
            &nbsp; &nbsp;<asp:Button ID="UploadButton" runat="server" OnClick="UploadButton_Click" Text="Upload Image" />
            &nbsp; &nbsp; &nbsp;
            <asp:Button ID="AnalyzeButton" runat="server" Height="29px" Text="Analyze Barcodes" OnClick="AnalyzeButton_Click" /><br />
            <br />
            <asp:RadioButtonList ID="BarcodeTypes" runat="server" Height="45px" RepeatDirection="Horizontal"
                Width="741px">
                <asp:ListItem Selected="True">1D</asp:ListItem>
                <asp:ListItem>PDF-417</asp:ListItem>
                <asp:ListItem>Data Matrix</asp:ListItem>
                <asp:ListItem>QR</asp:ListItem>
                <asp:ListItem>Aus Post</asp:ListItem>
                <asp:ListItem>IntelligentMail</asp:ListItem>
                <asp:ListItem>Royal Post</asp:ListItem>
                <asp:ListItem>PostNet</asp:ListItem>
                <asp:ListItem>Patch</asp:ListItem>
            </asp:RadioButtonList><br />
            <asp:ListBox ID="ResultsListBox" runat="server" Height="204px" Width="736px"></asp:ListBox></asp:Panel>
        &nbsp; &nbsp; &nbsp;
    
    </div>
    </form>
</body>
</html>

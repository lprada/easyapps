/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.ScanFix5;

namespace BarcodeDemo
{
	/// <summary>
	/// Summary description for ProcessScreen.
	/// </summary>
	public class ProcessScreen : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label InputImgLabel;
		private System.Windows.Forms.Label OutputImgLabel;
		private System.Windows.Forms.Button PreviewButton;
		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button CloseButton;
        private ImageXView InputImgView;
		private ImageXView OutputImgView;
		private ImageXView mainImage;
		private ImageXView mainTestImage;

		public bool PreprocessedFlag = false;
		private System.Windows.Forms.CheckBox ErodeCheckBox;
		private System.Windows.Forms.RadioButton DiagonalRadioButton1;
		private System.Windows.Forms.RadioButton VerticalRadioButton1;
		private System.Windows.Forms.RadioButton HorizontalRadioButton1;
		private System.Windows.Forms.RadioButton AllRadioButton1;
		private System.Windows.Forms.Label DeleteLabel2;
		private System.Windows.Forms.TextBox HeightTextBox;
		private System.Windows.Forms.Label DeleteLabel1;
		private System.Windows.Forms.TextBox WidthTextBox;
		private System.Windows.Forms.Label DegreesLabel2;
		private System.Windows.Forms.Label LengthLabel;
		private System.Windows.Forms.TextBox ShearTextBox;
		private System.Windows.Forms.TextBox LengthTextBox;
		private System.Windows.Forms.Label DegreesLabel;
		private System.Windows.Forms.TextBox RotateTextBox;
		private System.Windows.Forms.CheckBox ShearCheckBox;
		private System.Windows.Forms.CheckBox RemoveCheckBox;
		private System.Windows.Forms.CheckBox NegateCheckBox;
		private System.Windows.Forms.CheckBox Rotate180CheckBox;
		private System.Windows.Forms.CheckBox MirrorCheckBox;
		private System.Windows.Forms.CheckBox RotateCheckBox;
		private System.Windows.Forms.CheckBox FlipCheckBox;
		private System.Windows.Forms.CheckBox Rotate90CheckBox;
		private System.Windows.Forms.CheckBox SmoothCheckBox;
		private System.Windows.Forms.CheckBox DeskewCheckBox;
		private System.Windows.Forms.CheckBox DeleteCheckBox;
		private System.Windows.Forms.CheckBox DilateCheckBox;
		private System.Windows.Forms.RadioButton VerticalRadioButton2;
		private System.Windows.Forms.RadioButton HorizontalRadioButton2;
		private System.Windows.Forms.RadioButton DiagonalRadioButton2;
		private System.Windows.Forms.RadioButton AllRadioButton2;
		private System.Windows.Forms.GroupBox LitePanel;
		private System.Windows.Forms.TabControl TabRoll;
		private System.Windows.Forms.TabPage Page1;
		private System.Windows.Forms.TabPage Page2;
		private System.Windows.Forms.HScrollBar AreaXBar;
		private System.Windows.Forms.HScrollBar AreaYBar;
		private System.Windows.Forms.HScrollBar AreaHBar;
		private System.Windows.Forms.HScrollBar AreaWBar;
		private System.Windows.Forms.Label AreaXLabel;
		private System.Windows.Forms.Label AreaYLabel;
		private System.Windows.Forms.Label AreaWLabel;
		private System.Windows.Forms.Label AreaHLabel;
		private System.Windows.Forms.Label CombHLabel;
		private System.Windows.Forms.HScrollBar LengthBar;
		private System.Windows.Forms.HScrollBar SpaceBar;
		private System.Windows.Forms.HScrollBar HBar;
		private System.Windows.Forms.HScrollBar ConfidenceBar;
		private System.Windows.Forms.HScrollBar VBar;
		private System.Windows.Forms.Label CombSLabel;
		private System.Windows.Forms.Label MinLLabel;
		private System.Windows.Forms.Label ConLabel;
		private System.Windows.Forms.Label HThickLabel;
		private System.Windows.Forms.Label VThickLabel;
		private System.Windows.Forms.Label AreaXValue;
		private System.Windows.Forms.Label VThickValue;
		private System.Windows.Forms.Label HThickValue;
		private System.Windows.Forms.Label ConValue;
		private System.Windows.Forms.Label LengthValue;
		private System.Windows.Forms.Label SpaceValue;
		private System.Windows.Forms.Label AreaYValue;
		private System.Windows.Forms.Label AreaWValue;
		private System.Windows.Forms.Label AreaHValue;
		private System.Windows.Forms.Label CombHValue;
		private System.Windows.Forms.GroupBox ErodeGroupBox;
		private System.Windows.Forms.GroupBox DilatePanel;
		private System.Windows.Forms.HScrollBar CombHBar;
		private System.Windows.Forms.CheckBox CombCheckBox;
		private System.Windows.Forms.TabPage NegPage;
		private System.Windows.Forms.CheckBox NegCheckBox;
		private System.Windows.Forms.CheckBox ApplyCheckBox;
		private System.Windows.Forms.Label ConfidLabel;
		private System.Windows.Forms.Label QualityLabel;
		private System.Windows.Forms.Label NegConValue;
		private System.Windows.Forms.Label QualityValue;
		private System.Windows.Forms.HScrollBar ConBar;
		private System.Windows.Forms.HScrollBar QualityBar;
		private System.Windows.Forms.TabPage SmoothPage;
		private System.Windows.Forms.HScrollBar SmoothBar;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label SmoothValue;
		private System.Windows.Forms.TabPage CombtPage;
		private System.Windows.Forms.TabPage InversePage;
		private System.Windows.Forms.HScrollBar WidthBar;
		private System.Windows.Forms.HScrollBar HeightBar;
		private System.Windows.Forms.HScrollBar BlackBar;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label WidthLabel;
		private System.Windows.Forms.Label WidthBox;
		private System.Windows.Forms.Label HeightBox;
		private System.Windows.Forms.Label BlackBox;
		private System.Windows.Forms.Label BlackLabel;
		private System.Windows.Forms.CheckBox InverseCheckBox;
		private System.Windows.Forms.TabPage BlobPage;
		private System.Windows.Forms.CheckBox BlobCheckBox;
		private System.Windows.Forms.Label DensityLabel;
		private System.Windows.Forms.Label MaxLabel;
		private System.Windows.Forms.Label CountLabel;
		private System.Windows.Forms.HScrollBar BlobAreaXBar;
		private System.Windows.Forms.HScrollBar AreaHeightBar;
		private System.Windows.Forms.HScrollBar MaxCountBar;
		private System.Windows.Forms.HScrollBar MinCountBar;
		private System.Windows.Forms.HScrollBar AreaWidthBar;
		private System.Windows.Forms.HScrollBar BlobAreaYBar;
		private System.Windows.Forms.HScrollBar DensityBar;
		private System.Windows.Forms.Label BlobHLabel;
		private System.Windows.Forms.Label BlobWLabel;
		private System.Windows.Forms.Label BlobYLabel;
		private System.Windows.Forms.Label BlobXLabel;
		private System.Windows.Forms.Label BlobWLabel2;
		private System.Windows.Forms.Label BlobHLabel2;
		private System.Windows.Forms.Label BlobYLabel2;
		private System.Windows.Forms.Label BlobXLabel2;
		private System.Windows.Forms.Label BlobMinLabel;
		private System.Windows.Forms.Label BlobDLabel;
		private System.Windows.Forms.Label BlobMaxLabel;
		private System.Windows.Forms.TabPage DotPage;
		internal System.Windows.Forms.Label DotSizeLabel;
		internal System.Windows.Forms.Label DLabel;
		internal System.Windows.Forms.Label HAdjLabel;
		internal System.Windows.Forms.Label VAdjLabel;
		internal System.Windows.Forms.Label DotAreaHeightLabel;
		internal System.Windows.Forms.Label DotWidthLabel;
		internal System.Windows.Forms.HScrollBar DotSizeBar;
		internal System.Windows.Forms.HScrollBar DotDensityBar;
		internal System.Windows.Forms.HScrollBar HAdjBar;
		internal System.Windows.Forms.HScrollBar VAdjBar;
		internal System.Windows.Forms.HScrollBar DotHBar;
		internal System.Windows.Forms.HScrollBar DotWBar;
		internal System.Windows.Forms.Label MaxDValue;
		internal System.Windows.Forms.Label DotDValue;
		internal System.Windows.Forms.Label HAdjValue;
		internal System.Windows.Forms.Label VAdjValue;
		internal System.Windows.Forms.Label DotHValue;
		internal System.Windows.Forms.Label DotWValue;
		private System.Windows.Forms.TabPage CropPage;
		private System.Windows.Forms.Label ColorLabel;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox cmbpad;
		private System.Windows.Forms.CheckBox chkdeskew;
		private System.Windows.Forms.CheckBox chkreplace;
		private System.Windows.Forms.CheckBox chkcrop;
		private System.Windows.Forms.Label SpeckConLabel;
		private System.Windows.Forms.Label SpeckQualityLabel;
		private System.Windows.Forms.Label PSpeckLabel;
		private System.Windows.Forms.Label SpeckLabel;
		private System.Windows.Forms.HScrollBar MinConBar;
		private System.Windows.Forms.HScrollBar SpeckQualityBar;
		private System.Windows.Forms.HScrollBar PSpeckBar;
		private System.Windows.Forms.HScrollBar BSpeckBar;
		private System.Windows.Forms.Label SpeckConValue;
		private System.Windows.Forms.Label SpeckQValue;
		private System.Windows.Forms.Label PSpeckValue;
		private System.Windows.Forms.Label BSpeckValue;
		private System.Windows.Forms.Label SpeckWValue;
		private System.Windows.Forms.Label SpeckWidthLabel;
		private System.Windows.Forms.HScrollBar SpeckWidthBar;
		private System.Windows.Forms.CheckBox BorderCheckBox;
		private System.Windows.Forms.Label MaxPageHeightValue;
		private System.Windows.Forms.Label MaxPageWidthValue;
		private System.Windows.Forms.Label MinPageHeightValue;
		private System.Windows.Forms.HScrollBar MaxWidthBar;
		private System.Windows.Forms.HScrollBar MinHeightBar;
		private System.Windows.Forms.HScrollBar MaxHeightBar;
		private System.Windows.Forms.TabControl BitonalTabRoll;
		private System.Windows.Forms.CheckBox SmoothObjectsCheckBox;
		private System.Windows.Forms.CheckBox DotCheckBox;
		private bool PreviewFlag = false;
		private System.Windows.Forms.Label EditionNote;
        private RadioButton VUpDilate;
        private RadioButton HLeftDilate;
        private RadioButton DiagUpLeftDilate;
        private RadioButton DiagLeftDownDilate;
        private RadioButton DiagUpRightDilate;
        private RadioButton DiagUpLeftErode;
        private RadioButton VUpErode;
        private RadioButton HLeftErode;
        private RadioButton DiagUpRightErode;
        private RadioButton DiagLeftDownErode;
        private NumericUpDown DilateBox;
        private Label DilateLabel;
        private Label DilateLabel2;
        private Label ErodeLabel2;
        private NumericUpDown ErodeBox;
        private Label ErodeLabel;

		private PegasusImaging.WinForms.ScanFix5.ScanFix scanFix1;
		
		public ProcessScreen(ImageXView mainImg, ImageXView mainTestImg, ScanFix SF)
		{
			InitializeComponent();
			
			mainImage = mainImg;
			mainTestImage = mainTestImg;

			scanFix1 = SF;

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(InputImgView != null)
				{
					InputImgView.Dispose();
					InputImgView = null;
				}

				if(OutputImgView != null)
				{
					OutputImgView.Dispose();
					OutputImgView = null;
				}

				if(mainImage != null)
				{
					mainImage.Dispose();
					mainImage = null;
				}

				if(mainTestImage != null)
				{
					mainTestImage.Dispose();
					mainTestImage = null;
				}
				
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessScreen));
            this.InputImgView = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.OutputImgView = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
            this.InputImgLabel = new System.Windows.Forms.Label();
            this.OutputImgLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.OKButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.ErodeGroupBox = new System.Windows.Forms.GroupBox();
            this.ErodeLabel2 = new System.Windows.Forms.Label();
            this.ErodeBox = new System.Windows.Forms.NumericUpDown();
            this.ErodeLabel = new System.Windows.Forms.Label();
            this.DiagUpRightErode = new System.Windows.Forms.RadioButton();
            this.DiagLeftDownErode = new System.Windows.Forms.RadioButton();
            this.DiagUpLeftErode = new System.Windows.Forms.RadioButton();
            this.VUpErode = new System.Windows.Forms.RadioButton();
            this.HLeftErode = new System.Windows.Forms.RadioButton();
            this.ErodeCheckBox = new System.Windows.Forms.CheckBox();
            this.DiagonalRadioButton1 = new System.Windows.Forms.RadioButton();
            this.VerticalRadioButton1 = new System.Windows.Forms.RadioButton();
            this.HorizontalRadioButton1 = new System.Windows.Forms.RadioButton();
            this.AllRadioButton1 = new System.Windows.Forms.RadioButton();
            this.DeleteLabel2 = new System.Windows.Forms.Label();
            this.HeightTextBox = new System.Windows.Forms.TextBox();
            this.DeleteLabel1 = new System.Windows.Forms.Label();
            this.WidthTextBox = new System.Windows.Forms.TextBox();
            this.DegreesLabel2 = new System.Windows.Forms.Label();
            this.LengthLabel = new System.Windows.Forms.Label();
            this.ShearTextBox = new System.Windows.Forms.TextBox();
            this.LengthTextBox = new System.Windows.Forms.TextBox();
            this.DegreesLabel = new System.Windows.Forms.Label();
            this.RotateTextBox = new System.Windows.Forms.TextBox();
            this.ShearCheckBox = new System.Windows.Forms.CheckBox();
            this.RemoveCheckBox = new System.Windows.Forms.CheckBox();
            this.NegateCheckBox = new System.Windows.Forms.CheckBox();
            this.Rotate180CheckBox = new System.Windows.Forms.CheckBox();
            this.MirrorCheckBox = new System.Windows.Forms.CheckBox();
            this.RotateCheckBox = new System.Windows.Forms.CheckBox();
            this.FlipCheckBox = new System.Windows.Forms.CheckBox();
            this.Rotate90CheckBox = new System.Windows.Forms.CheckBox();
            this.SmoothCheckBox = new System.Windows.Forms.CheckBox();
            this.DeskewCheckBox = new System.Windows.Forms.CheckBox();
            this.DeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.DilatePanel = new System.Windows.Forms.GroupBox();
            this.DilateLabel2 = new System.Windows.Forms.Label();
            this.DilateLabel = new System.Windows.Forms.Label();
            this.DilateBox = new System.Windows.Forms.NumericUpDown();
            this.DiagUpRightDilate = new System.Windows.Forms.RadioButton();
            this.DiagLeftDownDilate = new System.Windows.Forms.RadioButton();
            this.VUpDilate = new System.Windows.Forms.RadioButton();
            this.HLeftDilate = new System.Windows.Forms.RadioButton();
            this.DiagUpLeftDilate = new System.Windows.Forms.RadioButton();
            this.DilateCheckBox = new System.Windows.Forms.CheckBox();
            this.VerticalRadioButton2 = new System.Windows.Forms.RadioButton();
            this.HorizontalRadioButton2 = new System.Windows.Forms.RadioButton();
            this.DiagonalRadioButton2 = new System.Windows.Forms.RadioButton();
            this.AllRadioButton2 = new System.Windows.Forms.RadioButton();
            this.LitePanel = new System.Windows.Forms.GroupBox();
            this.TabRoll = new System.Windows.Forms.TabControl();
            this.Page1 = new System.Windows.Forms.TabPage();
            this.Page2 = new System.Windows.Forms.TabPage();
            this.BitonalTabRoll = new System.Windows.Forms.TabControl();
            this.CombtPage = new System.Windows.Forms.TabPage();
            this.SpaceValue = new System.Windows.Forms.Label();
            this.AreaXBar = new System.Windows.Forms.HScrollBar();
            this.CombHValue = new System.Windows.Forms.Label();
            this.AreaHValue = new System.Windows.Forms.Label();
            this.AreaHBar = new System.Windows.Forms.HScrollBar();
            this.LengthBar = new System.Windows.Forms.HScrollBar();
            this.AreaWLabel = new System.Windows.Forms.Label();
            this.AreaWValue = new System.Windows.Forms.Label();
            this.AreaYValue = new System.Windows.Forms.Label();
            this.AreaXValue = new System.Windows.Forms.Label();
            this.VThickLabel = new System.Windows.Forms.Label();
            this.HThickLabel = new System.Windows.Forms.Label();
            this.ConLabel = new System.Windows.Forms.Label();
            this.SpaceBar = new System.Windows.Forms.HScrollBar();
            this.MinLLabel = new System.Windows.Forms.Label();
            this.CombSLabel = new System.Windows.Forms.Label();
            this.CombHLabel = new System.Windows.Forms.Label();
            this.AreaHLabel = new System.Windows.Forms.Label();
            this.AreaWBar = new System.Windows.Forms.HScrollBar();
            this.AreaYLabel = new System.Windows.Forms.Label();
            this.CombCheckBox = new System.Windows.Forms.CheckBox();
            this.AreaXLabel = new System.Windows.Forms.Label();
            this.VBar = new System.Windows.Forms.HScrollBar();
            this.VThickValue = new System.Windows.Forms.Label();
            this.AreaYBar = new System.Windows.Forms.HScrollBar();
            this.CombHBar = new System.Windows.Forms.HScrollBar();
            this.HBar = new System.Windows.Forms.HScrollBar();
            this.HThickValue = new System.Windows.Forms.Label();
            this.ConfidenceBar = new System.Windows.Forms.HScrollBar();
            this.ConValue = new System.Windows.Forms.Label();
            this.LengthValue = new System.Windows.Forms.Label();
            this.NegPage = new System.Windows.Forms.TabPage();
            this.QualityValue = new System.Windows.Forms.Label();
            this.NegConValue = new System.Windows.Forms.Label();
            this.QualityLabel = new System.Windows.Forms.Label();
            this.ConfidLabel = new System.Windows.Forms.Label();
            this.QualityBar = new System.Windows.Forms.HScrollBar();
            this.ConBar = new System.Windows.Forms.HScrollBar();
            this.ApplyCheckBox = new System.Windows.Forms.CheckBox();
            this.NegCheckBox = new System.Windows.Forms.CheckBox();
            this.SmoothPage = new System.Windows.Forms.TabPage();
            this.SmoothObjectsCheckBox = new System.Windows.Forms.CheckBox();
            this.SmoothValue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SmoothBar = new System.Windows.Forms.HScrollBar();
            this.InversePage = new System.Windows.Forms.TabPage();
            this.InverseCheckBox = new System.Windows.Forms.CheckBox();
            this.WidthLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BlackLabel = new System.Windows.Forms.Label();
            this.BlackBox = new System.Windows.Forms.Label();
            this.HeightBox = new System.Windows.Forms.Label();
            this.WidthBox = new System.Windows.Forms.Label();
            this.BlackBar = new System.Windows.Forms.HScrollBar();
            this.HeightBar = new System.Windows.Forms.HScrollBar();
            this.WidthBar = new System.Windows.Forms.HScrollBar();
            this.BlobPage = new System.Windows.Forms.TabPage();
            this.BlobMinLabel = new System.Windows.Forms.Label();
            this.BlobAreaXBar = new System.Windows.Forms.HScrollBar();
            this.BlobHLabel = new System.Windows.Forms.Label();
            this.AreaHeightBar = new System.Windows.Forms.HScrollBar();
            this.MaxCountBar = new System.Windows.Forms.HScrollBar();
            this.BlobWLabel2 = new System.Windows.Forms.Label();
            this.BlobWLabel = new System.Windows.Forms.Label();
            this.BlobYLabel = new System.Windows.Forms.Label();
            this.BlobXLabel = new System.Windows.Forms.Label();
            this.DensityLabel = new System.Windows.Forms.Label();
            this.MinCountBar = new System.Windows.Forms.HScrollBar();
            this.MaxLabel = new System.Windows.Forms.Label();
            this.CountLabel = new System.Windows.Forms.Label();
            this.BlobHLabel2 = new System.Windows.Forms.Label();
            this.AreaWidthBar = new System.Windows.Forms.HScrollBar();
            this.BlobYLabel2 = new System.Windows.Forms.Label();
            this.BlobXLabel2 = new System.Windows.Forms.Label();
            this.BlobAreaYBar = new System.Windows.Forms.HScrollBar();
            this.DensityBar = new System.Windows.Forms.HScrollBar();
            this.BlobDLabel = new System.Windows.Forms.Label();
            this.BlobMaxLabel = new System.Windows.Forms.Label();
            this.BlobCheckBox = new System.Windows.Forms.CheckBox();
            this.DotPage = new System.Windows.Forms.TabPage();
            this.DotCheckBox = new System.Windows.Forms.CheckBox();
            this.MaxDValue = new System.Windows.Forms.Label();
            this.DotDValue = new System.Windows.Forms.Label();
            this.HAdjValue = new System.Windows.Forms.Label();
            this.VAdjValue = new System.Windows.Forms.Label();
            this.DotHValue = new System.Windows.Forms.Label();
            this.DotWValue = new System.Windows.Forms.Label();
            this.DotSizeLabel = new System.Windows.Forms.Label();
            this.DLabel = new System.Windows.Forms.Label();
            this.HAdjLabel = new System.Windows.Forms.Label();
            this.VAdjLabel = new System.Windows.Forms.Label();
            this.DotAreaHeightLabel = new System.Windows.Forms.Label();
            this.DotWidthLabel = new System.Windows.Forms.Label();
            this.DotSizeBar = new System.Windows.Forms.HScrollBar();
            this.DotDensityBar = new System.Windows.Forms.HScrollBar();
            this.HAdjBar = new System.Windows.Forms.HScrollBar();
            this.VAdjBar = new System.Windows.Forms.HScrollBar();
            this.DotHBar = new System.Windows.Forms.HScrollBar();
            this.DotWBar = new System.Windows.Forms.HScrollBar();
            this.CropPage = new System.Windows.Forms.TabPage();
            this.BorderCheckBox = new System.Windows.Forms.CheckBox();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.MaxPageHeightValue = new System.Windows.Forms.Label();
            this.MaxPageWidthValue = new System.Windows.Forms.Label();
            this.MinPageHeightValue = new System.Windows.Forms.Label();
            this.SpeckWValue = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.SpeckWidthLabel = new System.Windows.Forms.Label();
            this.MaxHeightBar = new System.Windows.Forms.HScrollBar();
            this.MaxWidthBar = new System.Windows.Forms.HScrollBar();
            this.MinHeightBar = new System.Windows.Forms.HScrollBar();
            this.SpeckWidthBar = new System.Windows.Forms.HScrollBar();
            this.SpeckConValue = new System.Windows.Forms.Label();
            this.SpeckQValue = new System.Windows.Forms.Label();
            this.PSpeckValue = new System.Windows.Forms.Label();
            this.BSpeckValue = new System.Windows.Forms.Label();
            this.SpeckConLabel = new System.Windows.Forms.Label();
            this.SpeckQualityLabel = new System.Windows.Forms.Label();
            this.PSpeckLabel = new System.Windows.Forms.Label();
            this.SpeckLabel = new System.Windows.Forms.Label();
            this.MinConBar = new System.Windows.Forms.HScrollBar();
            this.SpeckQualityBar = new System.Windows.Forms.HScrollBar();
            this.PSpeckBar = new System.Windows.Forms.HScrollBar();
            this.BSpeckBar = new System.Windows.Forms.HScrollBar();
            this.cmbpad = new System.Windows.Forms.ComboBox();
            this.chkdeskew = new System.Windows.Forms.CheckBox();
            this.chkreplace = new System.Windows.Forms.CheckBox();
            this.chkcrop = new System.Windows.Forms.CheckBox();
            this.EditionNote = new System.Windows.Forms.Label();
            this.ErodeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ErodeBox)).BeginInit();
            this.DilatePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DilateBox)).BeginInit();
            this.LitePanel.SuspendLayout();
            this.TabRoll.SuspendLayout();
            this.Page1.SuspendLayout();
            this.Page2.SuspendLayout();
            this.BitonalTabRoll.SuspendLayout();
            this.CombtPage.SuspendLayout();
            this.NegPage.SuspendLayout();
            this.SmoothPage.SuspendLayout();
            this.InversePage.SuspendLayout();
            this.BlobPage.SuspendLayout();
            this.DotPage.SuspendLayout();
            this.CropPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // InputImgView
            // 
            this.InputImgView.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.InputImgView.AutoScroll = true;
            this.InputImgView.Location = new System.Drawing.Point(8, 8);
            this.InputImgView.Name = "InputImgView";
            this.InputImgView.Size = new System.Drawing.Size(360, 224);
            this.InputImgView.TabIndex = 0;
            // 
            // OutputImgView
            // 
            this.OutputImgView.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit;
            this.OutputImgView.AutoScroll = true;
            this.OutputImgView.Location = new System.Drawing.Point(392, 8);
            this.OutputImgView.Name = "OutputImgView";
            this.OutputImgView.Size = new System.Drawing.Size(360, 224);
            this.OutputImgView.TabIndex = 1;
            // 
            // InputImgLabel
            // 
            this.InputImgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InputImgLabel.Location = new System.Drawing.Point(112, 235);
            this.InputImgLabel.Name = "InputImgLabel";
            this.InputImgLabel.Size = new System.Drawing.Size(120, 26);
            this.InputImgLabel.TabIndex = 2;
            this.InputImgLabel.Text = "Original";
            this.InputImgLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OutputImgLabel
            // 
            this.OutputImgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputImgLabel.Location = new System.Drawing.Point(468, 240);
            this.OutputImgLabel.Name = "OutputImgLabel";
            this.OutputImgLabel.Size = new System.Drawing.Size(200, 16);
            this.OutputImgLabel.TabIndex = 3;
            this.OutputImgLabel.Text = "Preview Transformation";
            this.OutputImgLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(192, 672);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 2;
            this.CloseButton.Text = "Close";
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(104, 672);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 1;
            this.OKButton.Text = "Process";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Location = new System.Drawing.Point(16, 672);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(75, 23);
            this.PreviewButton.TabIndex = 0;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // ErodeGroupBox
            // 
            this.ErodeGroupBox.Controls.Add(this.ErodeLabel2);
            this.ErodeGroupBox.Controls.Add(this.ErodeBox);
            this.ErodeGroupBox.Controls.Add(this.ErodeLabel);
            this.ErodeGroupBox.Controls.Add(this.DiagUpRightErode);
            this.ErodeGroupBox.Controls.Add(this.DiagLeftDownErode);
            this.ErodeGroupBox.Controls.Add(this.DiagUpLeftErode);
            this.ErodeGroupBox.Controls.Add(this.VUpErode);
            this.ErodeGroupBox.Controls.Add(this.HLeftErode);
            this.ErodeGroupBox.Controls.Add(this.ErodeCheckBox);
            this.ErodeGroupBox.Controls.Add(this.DiagonalRadioButton1);
            this.ErodeGroupBox.Controls.Add(this.VerticalRadioButton1);
            this.ErodeGroupBox.Controls.Add(this.HorizontalRadioButton1);
            this.ErodeGroupBox.Controls.Add(this.AllRadioButton1);
            this.ErodeGroupBox.Location = new System.Drawing.Point(366, 146);
            this.ErodeGroupBox.Name = "ErodeGroupBox";
            this.ErodeGroupBox.Size = new System.Drawing.Size(357, 208);
            this.ErodeGroupBox.TabIndex = 27;
            this.ErodeGroupBox.TabStop = false;
            // 
            // ErodeLabel2
            // 
            this.ErodeLabel2.Location = new System.Drawing.Point(112, 182);
            this.ErodeLabel2.Name = "ErodeLabel2";
            this.ErodeLabel2.Size = new System.Drawing.Size(36, 24);
            this.ErodeLabel2.TabIndex = 39;
            this.ErodeLabel2.Text = "pixels.";
            // 
            // ErodeBox
            // 
            this.ErodeBox.Location = new System.Drawing.Point(72, 178);
            this.ErodeBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.ErodeBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ErodeBox.Name = "ErodeBox";
            this.ErodeBox.Size = new System.Drawing.Size(34, 20);
            this.ErodeBox.TabIndex = 37;
            this.ErodeBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // ErodeLabel
            // 
            this.ErodeLabel.Location = new System.Drawing.Point(15, 182);
            this.ErodeLabel.Name = "ErodeLabel";
            this.ErodeLabel.Size = new System.Drawing.Size(59, 24);
            this.ErodeLabel.TabIndex = 38;
            this.ErodeLabel.Text = "Erode by:";
            // 
            // DiagUpRightErode
            // 
            this.DiagUpRightErode.Location = new System.Drawing.Point(190, 131);
            this.DiagUpRightErode.Name = "DiagUpRightErode";
            this.DiagUpRightErode.Size = new System.Drawing.Size(161, 24);
            this.DiagUpRightErode.TabIndex = 31;
            this.DiagUpRightErode.Text = "Diagonal (up/right direction)";
            // 
            // DiagLeftDownErode
            // 
            this.DiagLeftDownErode.Location = new System.Drawing.Point(10, 131);
            this.DiagLeftDownErode.Name = "DiagLeftDownErode";
            this.DiagLeftDownErode.Size = new System.Drawing.Size(174, 24);
            this.DiagLeftDownErode.TabIndex = 30;
            this.DiagLeftDownErode.Text = "Diagonal (left/down direction)";
            // 
            // DiagUpLeftErode
            // 
            this.DiagUpLeftErode.Location = new System.Drawing.Point(190, 102);
            this.DiagUpLeftErode.Name = "DiagUpLeftErode";
            this.DiagUpLeftErode.Size = new System.Drawing.Size(161, 24);
            this.DiagUpLeftErode.TabIndex = 29;
            this.DiagUpLeftErode.Text = "Diagonal (up/left direction)";
            // 
            // VUpErode
            // 
            this.VUpErode.Location = new System.Drawing.Point(190, 42);
            this.VUpErode.Name = "VUpErode";
            this.VUpErode.Size = new System.Drawing.Size(132, 24);
            this.VUpErode.TabIndex = 27;
            this.VUpErode.Text = "Vertical (up direction)";
            // 
            // HLeftErode
            // 
            this.HLeftErode.Location = new System.Drawing.Point(190, 72);
            this.HLeftErode.Name = "HLeftErode";
            this.HLeftErode.Size = new System.Drawing.Size(161, 24);
            this.HLeftErode.TabIndex = 28;
            this.HLeftErode.Text = "Horizontal (left direction)";
            // 
            // ErodeCheckBox
            // 
            this.ErodeCheckBox.Location = new System.Drawing.Point(139, 14);
            this.ErodeCheckBox.Name = "ErodeCheckBox";
            this.ErodeCheckBox.Size = new System.Drawing.Size(112, 24);
            this.ErodeCheckBox.TabIndex = 23;
            this.ErodeCheckBox.Text = "Erode the image";
            // 
            // DiagonalRadioButton1
            // 
            this.DiagonalRadioButton1.Location = new System.Drawing.Point(10, 102);
            this.DiagonalRadioButton1.Name = "DiagonalRadioButton1";
            this.DiagonalRadioButton1.Size = new System.Drawing.Size(187, 24);
            this.DiagonalRadioButton1.TabIndex = 26;
            this.DiagonalRadioButton1.Text = "Diagonal (down/right direction)";
            // 
            // VerticalRadioButton1
            // 
            this.VerticalRadioButton1.Location = new System.Drawing.Point(10, 42);
            this.VerticalRadioButton1.Name = "VerticalRadioButton1";
            this.VerticalRadioButton1.Size = new System.Drawing.Size(144, 24);
            this.VerticalRadioButton1.TabIndex = 24;
            this.VerticalRadioButton1.Text = "Vertical (down direction)";
            // 
            // HorizontalRadioButton1
            // 
            this.HorizontalRadioButton1.Location = new System.Drawing.Point(10, 72);
            this.HorizontalRadioButton1.Name = "HorizontalRadioButton1";
            this.HorizontalRadioButton1.Size = new System.Drawing.Size(174, 24);
            this.HorizontalRadioButton1.TabIndex = 26;
            this.HorizontalRadioButton1.Text = "Horizontal (right direction)";
            // 
            // AllRadioButton1
            // 
            this.AllRadioButton1.Location = new System.Drawing.Point(139, 155);
            this.AllRadioButton1.Name = "AllRadioButton1";
            this.AllRadioButton1.Size = new System.Drawing.Size(88, 24);
            this.AllRadioButton1.TabIndex = 26;
            this.AllRadioButton1.Text = "All Directions";
            // 
            // DeleteLabel2
            // 
            this.DeleteLabel2.Location = new System.Drawing.Point(512, 24);
            this.DeleteLabel2.Name = "DeleteLabel2";
            this.DeleteLabel2.Size = new System.Drawing.Size(40, 23);
            this.DeleteLabel2.TabIndex = 26;
            this.DeleteLabel2.Text = "pixels.";
            // 
            // HeightTextBox
            // 
            this.HeightTextBox.Location = new System.Drawing.Point(480, 24);
            this.HeightTextBox.Name = "HeightTextBox";
            this.HeightTextBox.Size = new System.Drawing.Size(24, 20);
            this.HeightTextBox.TabIndex = 25;
            this.HeightTextBox.Text = "2";
            this.HeightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HeightTextBox_KeyPress);
            this.HeightTextBox.TextChanged += new System.EventHandler(this.HeightTextBox_TextChanged);
            // 
            // DeleteLabel1
            // 
            this.DeleteLabel1.Location = new System.Drawing.Point(304, 24);
            this.DeleteLabel1.Name = "DeleteLabel1";
            this.DeleteLabel1.Size = new System.Drawing.Size(168, 16);
            this.DeleteLabel1.TabIndex = 24;
            this.DeleteLabel1.Text = "pixels and a height smaller than";
            // 
            // WidthTextBox
            // 
            this.WidthTextBox.Location = new System.Drawing.Point(272, 24);
            this.WidthTextBox.Name = "WidthTextBox";
            this.WidthTextBox.Size = new System.Drawing.Size(24, 20);
            this.WidthTextBox.TabIndex = 23;
            this.WidthTextBox.Text = "2";
            this.WidthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WidthTextBox_KeyPress);
            this.WidthTextBox.TextChanged += new System.EventHandler(this.WidthTextBox_TextChanged);
            // 
            // DegreesLabel2
            // 
            this.DegreesLabel2.Location = new System.Drawing.Point(536, 88);
            this.DegreesLabel2.Name = "DegreesLabel2";
            this.DegreesLabel2.Size = new System.Drawing.Size(100, 16);
            this.DegreesLabel2.TabIndex = 22;
            this.DegreesLabel2.Text = "degrees (-15 - 15)";
            // 
            // LengthLabel
            // 
            this.LengthLabel.Location = new System.Drawing.Point(568, 56);
            this.LengthLabel.Name = "LengthLabel";
            this.LengthLabel.Size = new System.Drawing.Size(100, 16);
            this.LengthLabel.TabIndex = 21;
            this.LengthLabel.Text = "Minimum Length";
            // 
            // ShearTextBox
            // 
            this.ShearTextBox.Location = new System.Drawing.Point(496, 88);
            this.ShearTextBox.Name = "ShearTextBox";
            this.ShearTextBox.Size = new System.Drawing.Size(24, 20);
            this.ShearTextBox.TabIndex = 20;
            this.ShearTextBox.Text = "0";
            this.ShearTextBox.Leave += new System.EventHandler(this.ShearTextBox_Leave);
            // 
            // LengthTextBox
            // 
            this.LengthTextBox.Location = new System.Drawing.Point(488, 56);
            this.LengthTextBox.Name = "LengthTextBox";
            this.LengthTextBox.Size = new System.Drawing.Size(64, 20);
            this.LengthTextBox.TabIndex = 19;
            this.LengthTextBox.Text = "50";
            this.LengthTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.LengthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LengthTextBox_KeyPress);
            this.LengthTextBox.TextChanged += new System.EventHandler(this.LengthTextBox_TextChanged);
            // 
            // DegreesLabel
            // 
            this.DegreesLabel.Location = new System.Drawing.Point(328, 120);
            this.DegreesLabel.Name = "DegreesLabel";
            this.DegreesLabel.Size = new System.Drawing.Size(112, 16);
            this.DegreesLabel.TabIndex = 18;
            this.DegreesLabel.Text = "degrees (-360 - 360)";
            // 
            // RotateTextBox
            // 
            this.RotateTextBox.Location = new System.Drawing.Point(288, 120);
            this.RotateTextBox.Name = "RotateTextBox";
            this.RotateTextBox.Size = new System.Drawing.Size(32, 20);
            this.RotateTextBox.TabIndex = 17;
            this.RotateTextBox.Text = "0";
            this.RotateTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RotateTextBox_KeyPress);
            this.RotateTextBox.TextChanged += new System.EventHandler(this.RotateTextBox_TextChanged);
            // 
            // ShearCheckBox
            // 
            this.ShearCheckBox.Location = new System.Drawing.Point(376, 88);
            this.ShearCheckBox.Name = "ShearCheckBox";
            this.ShearCheckBox.Size = new System.Drawing.Size(112, 29);
            this.ShearCheckBox.TabIndex = 16;
            this.ShearCheckBox.Text = "Shear the image";
            // 
            // RemoveCheckBox
            // 
            this.RemoveCheckBox.Location = new System.Drawing.Point(376, 64);
            this.RemoveCheckBox.Name = "RemoveCheckBox";
            this.RemoveCheckBox.Size = new System.Drawing.Size(104, 16);
            this.RemoveCheckBox.TabIndex = 15;
            this.RemoveCheckBox.Text = "Remove Lines";
            // 
            // NegateCheckBox
            // 
            this.NegateCheckBox.Location = new System.Drawing.Point(16, 128);
            this.NegateCheckBox.Name = "NegateCheckBox";
            this.NegateCheckBox.Size = new System.Drawing.Size(120, 16);
            this.NegateCheckBox.TabIndex = 14;
            this.NegateCheckBox.Text = "AutoNegate";
            // 
            // Rotate180CheckBox
            // 
            this.Rotate180CheckBox.Location = new System.Drawing.Point(216, 56);
            this.Rotate180CheckBox.Name = "Rotate180CheckBox";
            this.Rotate180CheckBox.Size = new System.Drawing.Size(128, 16);
            this.Rotate180CheckBox.TabIndex = 13;
            this.Rotate180CheckBox.Text = "Rotate 180 degrees";
            // 
            // MirrorCheckBox
            // 
            this.MirrorCheckBox.Location = new System.Drawing.Point(216, 80);
            this.MirrorCheckBox.Name = "MirrorCheckBox";
            this.MirrorCheckBox.Size = new System.Drawing.Size(120, 16);
            this.MirrorCheckBox.TabIndex = 12;
            this.MirrorCheckBox.Text = "Mirror the image";
            // 
            // RotateCheckBox
            // 
            this.RotateCheckBox.Location = new System.Drawing.Point(216, 120);
            this.RotateCheckBox.Name = "RotateCheckBox";
            this.RotateCheckBox.Size = new System.Drawing.Size(64, 16);
            this.RotateCheckBox.TabIndex = 11;
            this.RotateCheckBox.Text = "Rotate";
            // 
            // FlipCheckBox
            // 
            this.FlipCheckBox.Location = new System.Drawing.Point(568, 24);
            this.FlipCheckBox.Name = "FlipCheckBox";
            this.FlipCheckBox.Size = new System.Drawing.Size(104, 16);
            this.FlipCheckBox.TabIndex = 10;
            this.FlipCheckBox.Text = "Flip the image";
            // 
            // Rotate90CheckBox
            // 
            this.Rotate90CheckBox.Location = new System.Drawing.Point(16, 104);
            this.Rotate90CheckBox.Name = "Rotate90CheckBox";
            this.Rotate90CheckBox.Size = new System.Drawing.Size(136, 16);
            this.Rotate90CheckBox.TabIndex = 9;
            this.Rotate90CheckBox.Text = "Rotate 90 degrees";
            // 
            // SmoothCheckBox
            // 
            this.SmoothCheckBox.Location = new System.Drawing.Point(16, 80);
            this.SmoothCheckBox.Name = "SmoothCheckBox";
            this.SmoothCheckBox.Size = new System.Drawing.Size(184, 16);
            this.SmoothCheckBox.TabIndex = 6;
            this.SmoothCheckBox.Text = "Smooth and Zoom the image";
            // 
            // DeskewCheckBox
            // 
            this.DeskewCheckBox.Location = new System.Drawing.Point(16, 56);
            this.DeskewCheckBox.Name = "DeskewCheckBox";
            this.DeskewCheckBox.Size = new System.Drawing.Size(104, 16);
            this.DeskewCheckBox.TabIndex = 5;
            this.DeskewCheckBox.Text = "Deskew";
            // 
            // DeleteCheckBox
            // 
            this.DeleteCheckBox.Location = new System.Drawing.Point(16, 24);
            this.DeleteCheckBox.Name = "DeleteCheckBox";
            this.DeleteCheckBox.Size = new System.Drawing.Size(248, 24);
            this.DeleteCheckBox.TabIndex = 4;
            this.DeleteCheckBox.Text = "Delete all speckles with a width smaller than";
            // 
            // DilatePanel
            // 
            this.DilatePanel.Controls.Add(this.DilateLabel2);
            this.DilatePanel.Controls.Add(this.DilateLabel);
            this.DilatePanel.Controls.Add(this.DilateBox);
            this.DilatePanel.Controls.Add(this.DiagUpRightDilate);
            this.DilatePanel.Controls.Add(this.DiagLeftDownDilate);
            this.DilatePanel.Controls.Add(this.VUpDilate);
            this.DilatePanel.Controls.Add(this.HLeftDilate);
            this.DilatePanel.Controls.Add(this.DiagUpLeftDilate);
            this.DilatePanel.Controls.Add(this.DilateCheckBox);
            this.DilatePanel.Controls.Add(this.VerticalRadioButton2);
            this.DilatePanel.Controls.Add(this.HorizontalRadioButton2);
            this.DilatePanel.Controls.Add(this.DiagonalRadioButton2);
            this.DilatePanel.Controls.Add(this.AllRadioButton2);
            this.DilatePanel.Location = new System.Drawing.Point(8, 146);
            this.DilatePanel.Name = "DilatePanel";
            this.DilatePanel.Size = new System.Drawing.Size(352, 208);
            this.DilatePanel.TabIndex = 3;
            this.DilatePanel.TabStop = false;
            // 
            // DilateLabel2
            // 
            this.DilateLabel2.Location = new System.Drawing.Point(108, 182);
            this.DilateLabel2.Name = "DilateLabel2";
            this.DilateLabel2.Size = new System.Drawing.Size(36, 24);
            this.DilateLabel2.TabIndex = 35;
            this.DilateLabel2.Text = "pixels.";
            // 
            // DilateLabel
            // 
            this.DilateLabel.Location = new System.Drawing.Point(11, 182);
            this.DilateLabel.Name = "DilateLabel";
            this.DilateLabel.Size = new System.Drawing.Size(51, 24);
            this.DilateLabel.TabIndex = 34;
            this.DilateLabel.Text = "Dilate by:";
            // 
            // DilateBox
            // 
            this.DilateBox.Location = new System.Drawing.Point(68, 178);
            this.DilateBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.DilateBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.DilateBox.Name = "DilateBox";
            this.DilateBox.Size = new System.Drawing.Size(34, 20);
            this.DilateBox.TabIndex = 32;
            this.DilateBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // DiagUpRightDilate
            // 
            this.DiagUpRightDilate.Location = new System.Drawing.Point(179, 131);
            this.DiagUpRightDilate.Name = "DiagUpRightDilate";
            this.DiagUpRightDilate.Size = new System.Drawing.Size(170, 24);
            this.DiagUpRightDilate.TabIndex = 33;
            this.DiagUpRightDilate.Text = "Diagonal (up/right direction)";
            // 
            // DiagLeftDownDilate
            // 
            this.DiagLeftDownDilate.Location = new System.Drawing.Point(6, 131);
            this.DiagLeftDownDilate.Name = "DiagLeftDownDilate";
            this.DiagLeftDownDilate.Size = new System.Drawing.Size(170, 24);
            this.DiagLeftDownDilate.TabIndex = 32;
            this.DiagLeftDownDilate.Text = "Diagonal (left/down direction)";
            // 
            // VUpDilate
            // 
            this.VUpDilate.Location = new System.Drawing.Point(182, 42);
            this.VUpDilate.Name = "VUpDilate";
            this.VUpDilate.Size = new System.Drawing.Size(138, 24);
            this.VUpDilate.TabIndex = 28;
            this.VUpDilate.Text = "Vertical (up direction)";
            // 
            // HLeftDilate
            // 
            this.HLeftDilate.Location = new System.Drawing.Point(182, 72);
            this.HLeftDilate.Name = "HLeftDilate";
            this.HLeftDilate.Size = new System.Drawing.Size(149, 24);
            this.HLeftDilate.TabIndex = 31;
            this.HLeftDilate.Text = "Horizontal (left direction)";
            // 
            // DiagUpLeftDilate
            // 
            this.DiagUpLeftDilate.Location = new System.Drawing.Point(182, 101);
            this.DiagUpLeftDilate.Name = "DiagUpLeftDilate";
            this.DiagUpLeftDilate.Size = new System.Drawing.Size(162, 24);
            this.DiagUpLeftDilate.TabIndex = 30;
            this.DiagUpLeftDilate.Text = "Diagonal (up/left direction)";
            // 
            // DilateCheckBox
            // 
            this.DilateCheckBox.Location = new System.Drawing.Point(123, 14);
            this.DilateCheckBox.Name = "DilateCheckBox";
            this.DilateCheckBox.Size = new System.Drawing.Size(120, 22);
            this.DilateCheckBox.TabIndex = 23;
            this.DilateCheckBox.Text = "Dilate the image";
            // 
            // VerticalRadioButton2
            // 
            this.VerticalRadioButton2.Location = new System.Drawing.Point(6, 42);
            this.VerticalRadioButton2.Name = "VerticalRadioButton2";
            this.VerticalRadioButton2.Size = new System.Drawing.Size(149, 24);
            this.VerticalRadioButton2.TabIndex = 25;
            this.VerticalRadioButton2.Text = "Vertical (down direction)";
            // 
            // HorizontalRadioButton2
            // 
            this.HorizontalRadioButton2.Location = new System.Drawing.Point(6, 72);
            this.HorizontalRadioButton2.Name = "HorizontalRadioButton2";
            this.HorizontalRadioButton2.Size = new System.Drawing.Size(170, 24);
            this.HorizontalRadioButton2.TabIndex = 27;
            this.HorizontalRadioButton2.Text = "Horizontal (right direction)";
            // 
            // DiagonalRadioButton2
            // 
            this.DiagonalRadioButton2.Location = new System.Drawing.Point(6, 101);
            this.DiagonalRadioButton2.Name = "DiagonalRadioButton2";
            this.DiagonalRadioButton2.Size = new System.Drawing.Size(186, 24);
            this.DiagonalRadioButton2.TabIndex = 27;
            this.DiagonalRadioButton2.Text = "Diagonal (down/right direction)";
            // 
            // AllRadioButton2
            // 
            this.AllRadioButton2.Location = new System.Drawing.Point(135, 155);
            this.AllRadioButton2.Name = "AllRadioButton2";
            this.AllRadioButton2.Size = new System.Drawing.Size(92, 24);
            this.AllRadioButton2.TabIndex = 27;
            this.AllRadioButton2.Text = "All Directions";
            // 
            // LitePanel
            // 
            this.LitePanel.Controls.Add(this.ErodeGroupBox);
            this.LitePanel.Controls.Add(this.DeleteLabel2);
            this.LitePanel.Controls.Add(this.HeightTextBox);
            this.LitePanel.Controls.Add(this.DeleteLabel1);
            this.LitePanel.Controls.Add(this.WidthTextBox);
            this.LitePanel.Controls.Add(this.DegreesLabel2);
            this.LitePanel.Controls.Add(this.LengthLabel);
            this.LitePanel.Controls.Add(this.ShearTextBox);
            this.LitePanel.Controls.Add(this.LengthTextBox);
            this.LitePanel.Controls.Add(this.DegreesLabel);
            this.LitePanel.Controls.Add(this.RotateTextBox);
            this.LitePanel.Controls.Add(this.ShearCheckBox);
            this.LitePanel.Controls.Add(this.RemoveCheckBox);
            this.LitePanel.Controls.Add(this.NegateCheckBox);
            this.LitePanel.Controls.Add(this.Rotate180CheckBox);
            this.LitePanel.Controls.Add(this.MirrorCheckBox);
            this.LitePanel.Controls.Add(this.RotateCheckBox);
            this.LitePanel.Controls.Add(this.FlipCheckBox);
            this.LitePanel.Controls.Add(this.Rotate90CheckBox);
            this.LitePanel.Controls.Add(this.SmoothCheckBox);
            this.LitePanel.Controls.Add(this.DeskewCheckBox);
            this.LitePanel.Controls.Add(this.DeleteCheckBox);
            this.LitePanel.Controls.Add(this.DilatePanel);
            this.LitePanel.Location = new System.Drawing.Point(4, 3);
            this.LitePanel.Name = "LitePanel";
            this.LitePanel.Size = new System.Drawing.Size(729, 360);
            this.LitePanel.TabIndex = 4;
            this.LitePanel.TabStop = false;
            // 
            // TabRoll
            // 
            this.TabRoll.Controls.Add(this.Page1);
            this.TabRoll.Controls.Add(this.Page2);
            this.TabRoll.Location = new System.Drawing.Point(8, 264);
            this.TabRoll.Name = "TabRoll";
            this.TabRoll.SelectedIndex = 0;
            this.TabRoll.Size = new System.Drawing.Size(744, 392);
            this.TabRoll.TabIndex = 5;
            // 
            // Page1
            // 
            this.Page1.Controls.Add(this.LitePanel);
            this.Page1.Location = new System.Drawing.Point(4, 22);
            this.Page1.Name = "Page1";
            this.Page1.Size = new System.Drawing.Size(736, 366);
            this.Page1.TabIndex = 0;
            this.Page1.Text = "ScanFix Lite Edition";
            // 
            // Page2
            // 
            this.Page2.Controls.Add(this.BitonalTabRoll);
            this.Page2.Location = new System.Drawing.Point(4, 22);
            this.Page2.Name = "Page2";
            this.Page2.Size = new System.Drawing.Size(736, 366);
            this.Page2.TabIndex = 1;
            this.Page2.Text = "ScanFix Bitonal Edition";
            // 
            // BitonalTabRoll
            // 
            this.BitonalTabRoll.Controls.Add(this.CombtPage);
            this.BitonalTabRoll.Controls.Add(this.NegPage);
            this.BitonalTabRoll.Controls.Add(this.SmoothPage);
            this.BitonalTabRoll.Controls.Add(this.InversePage);
            this.BitonalTabRoll.Controls.Add(this.BlobPage);
            this.BitonalTabRoll.Controls.Add(this.DotPage);
            this.BitonalTabRoll.Controls.Add(this.CropPage);
            this.BitonalTabRoll.Location = new System.Drawing.Point(16, 8);
            this.BitonalTabRoll.Name = "BitonalTabRoll";
            this.BitonalTabRoll.SelectedIndex = 0;
            this.BitonalTabRoll.Size = new System.Drawing.Size(712, 352);
            this.BitonalTabRoll.TabIndex = 32;
            // 
            // CombtPage
            // 
            this.CombtPage.Controls.Add(this.SpaceValue);
            this.CombtPage.Controls.Add(this.AreaXBar);
            this.CombtPage.Controls.Add(this.CombHValue);
            this.CombtPage.Controls.Add(this.AreaHValue);
            this.CombtPage.Controls.Add(this.AreaHBar);
            this.CombtPage.Controls.Add(this.LengthBar);
            this.CombtPage.Controls.Add(this.AreaWLabel);
            this.CombtPage.Controls.Add(this.AreaWValue);
            this.CombtPage.Controls.Add(this.AreaYValue);
            this.CombtPage.Controls.Add(this.AreaXValue);
            this.CombtPage.Controls.Add(this.VThickLabel);
            this.CombtPage.Controls.Add(this.HThickLabel);
            this.CombtPage.Controls.Add(this.ConLabel);
            this.CombtPage.Controls.Add(this.SpaceBar);
            this.CombtPage.Controls.Add(this.MinLLabel);
            this.CombtPage.Controls.Add(this.CombSLabel);
            this.CombtPage.Controls.Add(this.CombHLabel);
            this.CombtPage.Controls.Add(this.AreaHLabel);
            this.CombtPage.Controls.Add(this.AreaWBar);
            this.CombtPage.Controls.Add(this.AreaYLabel);
            this.CombtPage.Controls.Add(this.CombCheckBox);
            this.CombtPage.Controls.Add(this.AreaXLabel);
            this.CombtPage.Controls.Add(this.VBar);
            this.CombtPage.Controls.Add(this.VThickValue);
            this.CombtPage.Controls.Add(this.AreaYBar);
            this.CombtPage.Controls.Add(this.CombHBar);
            this.CombtPage.Controls.Add(this.HBar);
            this.CombtPage.Controls.Add(this.HThickValue);
            this.CombtPage.Controls.Add(this.ConfidenceBar);
            this.CombtPage.Controls.Add(this.ConValue);
            this.CombtPage.Controls.Add(this.LengthValue);
            this.CombtPage.Location = new System.Drawing.Point(4, 22);
            this.CombtPage.Name = "CombtPage";
            this.CombtPage.Size = new System.Drawing.Size(704, 326);
            this.CombtPage.TabIndex = 0;
            this.CombtPage.Text = "Comb Removal";
            // 
            // SpaceValue
            // 
            this.SpaceValue.Location = new System.Drawing.Point(512, 64);
            this.SpaceValue.Name = "SpaceValue";
            this.SpaceValue.Size = new System.Drawing.Size(48, 16);
            this.SpaceValue.TabIndex = 26;
            this.SpaceValue.Tag = "";
            this.SpaceValue.Text = "25";
            // 
            // AreaXBar
            // 
            this.AreaXBar.LargeChange = 1;
            this.AreaXBar.Location = new System.Drawing.Point(136, 80);
            this.AreaXBar.Maximum = 9999;
            this.AreaXBar.Name = "AreaXBar";
            this.AreaXBar.Size = new System.Drawing.Size(200, 16);
            this.AreaXBar.TabIndex = 1;
            this.AreaXBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.AreaXBar_Scroll);
            // 
            // CombHValue
            // 
            this.CombHValue.Location = new System.Drawing.Point(288, 256);
            this.CombHValue.Name = "CombHValue";
            this.CombHValue.Size = new System.Drawing.Size(48, 16);
            this.CombHValue.TabIndex = 25;
            this.CombHValue.Text = "20";
            // 
            // AreaHValue
            // 
            this.AreaHValue.Location = new System.Drawing.Point(288, 208);
            this.AreaHValue.Name = "AreaHValue";
            this.AreaHValue.Size = new System.Drawing.Size(48, 16);
            this.AreaHValue.TabIndex = 24;
            this.AreaHValue.Text = "0";
            // 
            // AreaHBar
            // 
            this.AreaHBar.LargeChange = 1;
            this.AreaHBar.Location = new System.Drawing.Point(136, 224);
            this.AreaHBar.Maximum = 9999;
            this.AreaHBar.Name = "AreaHBar";
            this.AreaHBar.Size = new System.Drawing.Size(200, 16);
            this.AreaHBar.TabIndex = 4;
            this.AreaHBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.AreaHBar_Scroll);
            // 
            // LengthBar
            // 
            this.LengthBar.LargeChange = 1;
            this.LengthBar.Location = new System.Drawing.Point(360, 128);
            this.LengthBar.Maximum = 500;
            this.LengthBar.Minimum = 10;
            this.LengthBar.Name = "LengthBar";
            this.LengthBar.Size = new System.Drawing.Size(200, 16);
            this.LengthBar.TabIndex = 6;
            this.LengthBar.Value = 50;
            this.LengthBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.LengthBar_Scroll);
            // 
            // AreaWLabel
            // 
            this.AreaWLabel.Location = new System.Drawing.Point(136, 160);
            this.AreaWLabel.Name = "AreaWLabel";
            this.AreaWLabel.Size = new System.Drawing.Size(72, 16);
            this.AreaWLabel.TabIndex = 13;
            this.AreaWLabel.Text = "Area Width";
            // 
            // AreaWValue
            // 
            this.AreaWValue.Location = new System.Drawing.Point(288, 160);
            this.AreaWValue.Name = "AreaWValue";
            this.AreaWValue.Size = new System.Drawing.Size(48, 16);
            this.AreaWValue.TabIndex = 23;
            this.AreaWValue.Text = "0";
            // 
            // AreaYValue
            // 
            this.AreaYValue.Location = new System.Drawing.Point(288, 112);
            this.AreaYValue.Name = "AreaYValue";
            this.AreaYValue.Size = new System.Drawing.Size(48, 16);
            this.AreaYValue.TabIndex = 22;
            this.AreaYValue.Text = "0";
            // 
            // AreaXValue
            // 
            this.AreaXValue.Location = new System.Drawing.Point(288, 64);
            this.AreaXValue.Name = "AreaXValue";
            this.AreaXValue.Size = new System.Drawing.Size(48, 16);
            this.AreaXValue.TabIndex = 21;
            this.AreaXValue.Text = "0";
            // 
            // VThickLabel
            // 
            this.VThickLabel.Location = new System.Drawing.Point(360, 256);
            this.VThickLabel.Name = "VThickLabel";
            this.VThickLabel.Size = new System.Drawing.Size(128, 16);
            this.VThickLabel.TabIndex = 20;
            this.VThickLabel.Text = "Vertical Line Thickness";
            // 
            // HThickLabel
            // 
            this.HThickLabel.Location = new System.Drawing.Point(360, 208);
            this.HThickLabel.Name = "HThickLabel";
            this.HThickLabel.Size = new System.Drawing.Size(152, 16);
            this.HThickLabel.TabIndex = 19;
            this.HThickLabel.Text = "Horizontal Line Thickness";
            // 
            // ConLabel
            // 
            this.ConLabel.Location = new System.Drawing.Point(360, 160);
            this.ConLabel.Name = "ConLabel";
            this.ConLabel.Size = new System.Drawing.Size(112, 16);
            this.ConLabel.TabIndex = 18;
            this.ConLabel.Text = "Minimum Confidence";
            // 
            // SpaceBar
            // 
            this.SpaceBar.LargeChange = 1;
            this.SpaceBar.Location = new System.Drawing.Point(360, 80);
            this.SpaceBar.Maximum = 500;
            this.SpaceBar.Minimum = 1;
            this.SpaceBar.Name = "SpaceBar";
            this.SpaceBar.Size = new System.Drawing.Size(200, 16);
            this.SpaceBar.TabIndex = 5;
            this.SpaceBar.Value = 25;
            this.SpaceBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.SpaceBar_Scroll);
            // 
            // MinLLabel
            // 
            this.MinLLabel.Location = new System.Drawing.Point(360, 112);
            this.MinLLabel.Name = "MinLLabel";
            this.MinLLabel.Size = new System.Drawing.Size(128, 16);
            this.MinLLabel.TabIndex = 17;
            this.MinLLabel.Text = "Minimum Comb Length";
            // 
            // CombSLabel
            // 
            this.CombSLabel.Location = new System.Drawing.Point(360, 64);
            this.CombSLabel.Name = "CombSLabel";
            this.CombSLabel.Size = new System.Drawing.Size(88, 16);
            this.CombSLabel.TabIndex = 16;
            this.CombSLabel.Text = "Comb Spacing";
            // 
            // CombHLabel
            // 
            this.CombHLabel.Location = new System.Drawing.Point(136, 256);
            this.CombHLabel.Name = "CombHLabel";
            this.CombHLabel.Size = new System.Drawing.Size(72, 16);
            this.CombHLabel.TabIndex = 15;
            this.CombHLabel.Text = "Comb Height";
            // 
            // AreaHLabel
            // 
            this.AreaHLabel.Location = new System.Drawing.Point(136, 208);
            this.AreaHLabel.Name = "AreaHLabel";
            this.AreaHLabel.Size = new System.Drawing.Size(72, 16);
            this.AreaHLabel.TabIndex = 14;
            this.AreaHLabel.Text = "Area Height";
            // 
            // AreaWBar
            // 
            this.AreaWBar.LargeChange = 1;
            this.AreaWBar.Location = new System.Drawing.Point(136, 176);
            this.AreaWBar.Maximum = 9999;
            this.AreaWBar.Name = "AreaWBar";
            this.AreaWBar.Size = new System.Drawing.Size(200, 16);
            this.AreaWBar.TabIndex = 3;
            this.AreaWBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.AreaWBar_Scroll);
            // 
            // AreaYLabel
            // 
            this.AreaYLabel.Location = new System.Drawing.Point(136, 112);
            this.AreaYLabel.Name = "AreaYLabel";
            this.AreaYLabel.Size = new System.Drawing.Size(56, 16);
            this.AreaYLabel.TabIndex = 12;
            this.AreaYLabel.Text = "Area Y";
            // 
            // CombCheckBox
            // 
            this.CombCheckBox.Location = new System.Drawing.Point(304, 32);
            this.CombCheckBox.Name = "CombCheckBox";
            this.CombCheckBox.Size = new System.Drawing.Size(128, 16);
            this.CombCheckBox.TabIndex = 0;
            this.CombCheckBox.Text = "Remove Combs";
            // 
            // AreaXLabel
            // 
            this.AreaXLabel.Location = new System.Drawing.Point(136, 64);
            this.AreaXLabel.Name = "AreaXLabel";
            this.AreaXLabel.Size = new System.Drawing.Size(56, 16);
            this.AreaXLabel.TabIndex = 11;
            this.AreaXLabel.Text = "Area X";
            // 
            // VBar
            // 
            this.VBar.LargeChange = 1;
            this.VBar.Location = new System.Drawing.Point(360, 272);
            this.VBar.Maximum = 500;
            this.VBar.Minimum = 1;
            this.VBar.Name = "VBar";
            this.VBar.Size = new System.Drawing.Size(200, 16);
            this.VBar.TabIndex = 10;
            this.VBar.Value = 4;
            this.VBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VBar_Scroll);
            // 
            // VThickValue
            // 
            this.VThickValue.Location = new System.Drawing.Point(512, 256);
            this.VThickValue.Name = "VThickValue";
            this.VThickValue.Size = new System.Drawing.Size(48, 16);
            this.VThickValue.TabIndex = 30;
            this.VThickValue.Text = "4";
            // 
            // AreaYBar
            // 
            this.AreaYBar.LargeChange = 1;
            this.AreaYBar.Location = new System.Drawing.Point(136, 128);
            this.AreaYBar.Maximum = 9999;
            this.AreaYBar.Name = "AreaYBar";
            this.AreaYBar.Size = new System.Drawing.Size(200, 16);
            this.AreaYBar.TabIndex = 2;
            this.AreaYBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.AreaYBar_Scroll);
            // 
            // CombHBar
            // 
            this.CombHBar.LargeChange = 1;
            this.CombHBar.Location = new System.Drawing.Point(136, 272);
            this.CombHBar.Maximum = 500;
            this.CombHBar.Minimum = 4;
            this.CombHBar.Name = "CombHBar";
            this.CombHBar.Size = new System.Drawing.Size(200, 16);
            this.CombHBar.TabIndex = 9;
            this.CombHBar.Value = 20;
            this.CombHBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.CombHBar_Scroll);
            // 
            // HBar
            // 
            this.HBar.LargeChange = 1;
            this.HBar.Location = new System.Drawing.Point(360, 224);
            this.HBar.Maximum = 500;
            this.HBar.Minimum = 1;
            this.HBar.Name = "HBar";
            this.HBar.Size = new System.Drawing.Size(200, 16);
            this.HBar.TabIndex = 8;
            this.HBar.Value = 4;
            this.HBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HBar_Scroll);
            // 
            // HThickValue
            // 
            this.HThickValue.Location = new System.Drawing.Point(512, 208);
            this.HThickValue.Name = "HThickValue";
            this.HThickValue.Size = new System.Drawing.Size(48, 16);
            this.HThickValue.TabIndex = 29;
            this.HThickValue.Text = "4";
            // 
            // ConfidenceBar
            // 
            this.ConfidenceBar.LargeChange = 1;
            this.ConfidenceBar.Location = new System.Drawing.Point(360, 176);
            this.ConfidenceBar.Minimum = 1;
            this.ConfidenceBar.Name = "ConfidenceBar";
            this.ConfidenceBar.Size = new System.Drawing.Size(200, 16);
            this.ConfidenceBar.TabIndex = 7;
            this.ConfidenceBar.Value = 50;
            this.ConfidenceBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ConfidenceBar_Scroll);
            // 
            // ConValue
            // 
            this.ConValue.Location = new System.Drawing.Point(512, 160);
            this.ConValue.Name = "ConValue";
            this.ConValue.Size = new System.Drawing.Size(48, 16);
            this.ConValue.TabIndex = 28;
            this.ConValue.Text = "50";
            // 
            // LengthValue
            // 
            this.LengthValue.Location = new System.Drawing.Point(512, 112);
            this.LengthValue.Name = "LengthValue";
            this.LengthValue.Size = new System.Drawing.Size(48, 16);
            this.LengthValue.TabIndex = 27;
            this.LengthValue.Text = "50";
            // 
            // NegPage
            // 
            this.NegPage.Controls.Add(this.QualityValue);
            this.NegPage.Controls.Add(this.NegConValue);
            this.NegPage.Controls.Add(this.QualityLabel);
            this.NegPage.Controls.Add(this.ConfidLabel);
            this.NegPage.Controls.Add(this.QualityBar);
            this.NegPage.Controls.Add(this.ConBar);
            this.NegPage.Controls.Add(this.ApplyCheckBox);
            this.NegPage.Controls.Add(this.NegCheckBox);
            this.NegPage.Location = new System.Drawing.Point(4, 22);
            this.NegPage.Name = "NegPage";
            this.NegPage.Size = new System.Drawing.Size(704, 326);
            this.NegPage.TabIndex = 1;
            this.NegPage.Text = "Negative Page Correction";
            // 
            // QualityValue
            // 
            this.QualityValue.Location = new System.Drawing.Point(432, 176);
            this.QualityValue.Name = "QualityValue";
            this.QualityValue.Size = new System.Drawing.Size(40, 16);
            this.QualityValue.TabIndex = 7;
            this.QualityValue.Text = "80";
            // 
            // NegConValue
            // 
            this.NegConValue.Location = new System.Drawing.Point(432, 136);
            this.NegConValue.Name = "NegConValue";
            this.NegConValue.Size = new System.Drawing.Size(40, 24);
            this.NegConValue.TabIndex = 6;
            this.NegConValue.Text = "50";
            // 
            // QualityLabel
            // 
            this.QualityLabel.Location = new System.Drawing.Point(96, 176);
            this.QualityLabel.Name = "QualityLabel";
            this.QualityLabel.Size = new System.Drawing.Size(128, 16);
            this.QualityLabel.TabIndex = 5;
            this.QualityLabel.Text = "Quality";
            // 
            // ConfidLabel
            // 
            this.ConfidLabel.Location = new System.Drawing.Point(96, 136);
            this.ConfidLabel.Name = "ConfidLabel";
            this.ConfidLabel.Size = new System.Drawing.Size(128, 16);
            this.ConfidLabel.TabIndex = 4;
            this.ConfidLabel.Text = "Minimum Confidence";
            // 
            // QualityBar
            // 
            this.QualityBar.LargeChange = 1;
            this.QualityBar.Location = new System.Drawing.Point(240, 176);
            this.QualityBar.Minimum = 1;
            this.QualityBar.Name = "QualityBar";
            this.QualityBar.Size = new System.Drawing.Size(176, 17);
            this.QualityBar.TabIndex = 3;
            this.QualityBar.Value = 80;
            this.QualityBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.QualityBar_Scroll);
            // 
            // ConBar
            // 
            this.ConBar.LargeChange = 1;
            this.ConBar.Location = new System.Drawing.Point(240, 136);
            this.ConBar.Name = "ConBar";
            this.ConBar.Size = new System.Drawing.Size(176, 17);
            this.ConBar.TabIndex = 2;
            this.ConBar.Value = 50;
            this.ConBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ConBar_Scroll);
            // 
            // ApplyCheckBox
            // 
            this.ApplyCheckBox.Location = new System.Drawing.Point(488, 160);
            this.ApplyCheckBox.Name = "ApplyCheckBox";
            this.ApplyCheckBox.Size = new System.Drawing.Size(112, 16);
            this.ApplyCheckBox.TabIndex = 1;
            this.ApplyCheckBox.Text = "Apply Correction";
            // 
            // NegCheckBox
            // 
            this.NegCheckBox.Location = new System.Drawing.Point(256, 72);
            this.NegCheckBox.Name = "NegCheckBox";
            this.NegCheckBox.Size = new System.Drawing.Size(168, 24);
            this.NegCheckBox.TabIndex = 0;
            this.NegCheckBox.Text = "Correct Negative Page";
            // 
            // SmoothPage
            // 
            this.SmoothPage.Controls.Add(this.SmoothObjectsCheckBox);
            this.SmoothPage.Controls.Add(this.SmoothValue);
            this.SmoothPage.Controls.Add(this.label1);
            this.SmoothPage.Controls.Add(this.SmoothBar);
            this.SmoothPage.Location = new System.Drawing.Point(4, 22);
            this.SmoothPage.Name = "SmoothPage";
            this.SmoothPage.Size = new System.Drawing.Size(704, 326);
            this.SmoothPage.TabIndex = 2;
            this.SmoothPage.Text = "SmoothObjects";
            // 
            // SmoothObjectsCheckBox
            // 
            this.SmoothObjectsCheckBox.Location = new System.Drawing.Point(272, 104);
            this.SmoothObjectsCheckBox.Name = "SmoothObjectsCheckBox";
            this.SmoothObjectsCheckBox.Size = new System.Drawing.Size(112, 16);
            this.SmoothObjectsCheckBox.TabIndex = 3;
            this.SmoothObjectsCheckBox.Text = "Smooth Objects";
            // 
            // SmoothValue
            // 
            this.SmoothValue.Location = new System.Drawing.Point(496, 152);
            this.SmoothValue.Name = "SmoothValue";
            this.SmoothValue.Size = new System.Drawing.Size(32, 16);
            this.SmoothValue.TabIndex = 2;
            this.SmoothValue.Text = "1";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(160, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Smoothing Size";
            // 
            // SmoothBar
            // 
            this.SmoothBar.LargeChange = 1;
            this.SmoothBar.Location = new System.Drawing.Point(312, 152);
            this.SmoothBar.Minimum = 1;
            this.SmoothBar.Name = "SmoothBar";
            this.SmoothBar.Size = new System.Drawing.Size(168, 16);
            this.SmoothBar.TabIndex = 0;
            this.SmoothBar.Value = 1;
            this.SmoothBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.SmoothBar_Scroll);
            // 
            // InversePage
            // 
            this.InversePage.Controls.Add(this.InverseCheckBox);
            this.InversePage.Controls.Add(this.WidthLabel);
            this.InversePage.Controls.Add(this.label6);
            this.InversePage.Controls.Add(this.BlackLabel);
            this.InversePage.Controls.Add(this.BlackBox);
            this.InversePage.Controls.Add(this.HeightBox);
            this.InversePage.Controls.Add(this.WidthBox);
            this.InversePage.Controls.Add(this.BlackBar);
            this.InversePage.Controls.Add(this.HeightBar);
            this.InversePage.Controls.Add(this.WidthBar);
            this.InversePage.Location = new System.Drawing.Point(4, 22);
            this.InversePage.Name = "InversePage";
            this.InversePage.Size = new System.Drawing.Size(704, 326);
            this.InversePage.TabIndex = 3;
            this.InversePage.Text = "Inverse Text Correction";
            // 
            // InverseCheckBox
            // 
            this.InverseCheckBox.Location = new System.Drawing.Point(264, 56);
            this.InverseCheckBox.Name = "InverseCheckBox";
            this.InverseCheckBox.Size = new System.Drawing.Size(152, 24);
            this.InverseCheckBox.TabIndex = 9;
            this.InverseCheckBox.Text = "Correct Inverse Text";
            // 
            // WidthLabel
            // 
            this.WidthLabel.Location = new System.Drawing.Point(168, 104);
            this.WidthLabel.Name = "WidthLabel";
            this.WidthLabel.Size = new System.Drawing.Size(112, 16);
            this.WidthLabel.TabIndex = 8;
            this.WidthLabel.Text = "Minimum Area Width";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(160, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Minimum Area Height";
            // 
            // BlackLabel
            // 
            this.BlackLabel.Location = new System.Drawing.Point(136, 208);
            this.BlackLabel.Name = "BlackLabel";
            this.BlackLabel.Size = new System.Drawing.Size(144, 16);
            this.BlackLabel.TabIndex = 6;
            this.BlackLabel.Text = "Minimum Black On Edges";
            // 
            // BlackBox
            // 
            this.BlackBox.Location = new System.Drawing.Point(424, 192);
            this.BlackBox.Name = "BlackBox";
            this.BlackBox.Size = new System.Drawing.Size(56, 16);
            this.BlackBox.TabIndex = 5;
            this.BlackBox.Text = "1";
            // 
            // HeightBox
            // 
            this.HeightBox.Location = new System.Drawing.Point(424, 144);
            this.HeightBox.Name = "HeightBox";
            this.HeightBox.Size = new System.Drawing.Size(56, 16);
            this.HeightBox.TabIndex = 4;
            this.HeightBox.Text = "1";
            // 
            // WidthBox
            // 
            this.WidthBox.Location = new System.Drawing.Point(424, 88);
            this.WidthBox.Name = "WidthBox";
            this.WidthBox.Size = new System.Drawing.Size(56, 16);
            this.WidthBox.TabIndex = 3;
            this.WidthBox.Text = "1";
            // 
            // BlackBar
            // 
            this.BlackBar.LargeChange = 1;
            this.BlackBar.Location = new System.Drawing.Point(288, 208);
            this.BlackBar.Minimum = 1;
            this.BlackBar.Name = "BlackBar";
            this.BlackBar.Size = new System.Drawing.Size(192, 16);
            this.BlackBar.TabIndex = 2;
            this.BlackBar.Value = 1;
            this.BlackBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.BlackBar_Scroll);
            // 
            // HeightBar
            // 
            this.HeightBar.LargeChange = 1;
            this.HeightBar.Location = new System.Drawing.Point(288, 160);
            this.HeightBar.Maximum = 9999;
            this.HeightBar.Name = "HeightBar";
            this.HeightBar.Size = new System.Drawing.Size(192, 16);
            this.HeightBar.TabIndex = 1;
            this.HeightBar.Value = 1;
            this.HeightBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HeightBar_Scroll);
            // 
            // WidthBar
            // 
            this.WidthBar.LargeChange = 1;
            this.WidthBar.Location = new System.Drawing.Point(288, 104);
            this.WidthBar.Maximum = 9999;
            this.WidthBar.Name = "WidthBar";
            this.WidthBar.Size = new System.Drawing.Size(192, 16);
            this.WidthBar.TabIndex = 0;
            this.WidthBar.Value = 1;
            this.WidthBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.WidthBar_Scroll);
            // 
            // BlobPage
            // 
            this.BlobPage.Controls.Add(this.BlobMinLabel);
            this.BlobPage.Controls.Add(this.BlobAreaXBar);
            this.BlobPage.Controls.Add(this.BlobHLabel);
            this.BlobPage.Controls.Add(this.AreaHeightBar);
            this.BlobPage.Controls.Add(this.MaxCountBar);
            this.BlobPage.Controls.Add(this.BlobWLabel2);
            this.BlobPage.Controls.Add(this.BlobWLabel);
            this.BlobPage.Controls.Add(this.BlobYLabel);
            this.BlobPage.Controls.Add(this.BlobXLabel);
            this.BlobPage.Controls.Add(this.DensityLabel);
            this.BlobPage.Controls.Add(this.MinCountBar);
            this.BlobPage.Controls.Add(this.MaxLabel);
            this.BlobPage.Controls.Add(this.CountLabel);
            this.BlobPage.Controls.Add(this.BlobHLabel2);
            this.BlobPage.Controls.Add(this.AreaWidthBar);
            this.BlobPage.Controls.Add(this.BlobYLabel2);
            this.BlobPage.Controls.Add(this.BlobXLabel2);
            this.BlobPage.Controls.Add(this.BlobAreaYBar);
            this.BlobPage.Controls.Add(this.DensityBar);
            this.BlobPage.Controls.Add(this.BlobDLabel);
            this.BlobPage.Controls.Add(this.BlobMaxLabel);
            this.BlobPage.Controls.Add(this.BlobCheckBox);
            this.BlobPage.Location = new System.Drawing.Point(4, 22);
            this.BlobPage.Name = "BlobPage";
            this.BlobPage.Size = new System.Drawing.Size(704, 326);
            this.BlobPage.TabIndex = 4;
            this.BlobPage.Text = "Blob Removal";
            // 
            // BlobMinLabel
            // 
            this.BlobMinLabel.Location = new System.Drawing.Point(496, 64);
            this.BlobMinLabel.Name = "BlobMinLabel";
            this.BlobMinLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobMinLabel.TabIndex = 56;
            this.BlobMinLabel.Tag = "";
            this.BlobMinLabel.Text = "300";
            // 
            // BlobAreaXBar
            // 
            this.BlobAreaXBar.LargeChange = 1;
            this.BlobAreaXBar.Location = new System.Drawing.Point(120, 80);
            this.BlobAreaXBar.Maximum = 9999;
            this.BlobAreaXBar.Name = "BlobAreaXBar";
            this.BlobAreaXBar.Size = new System.Drawing.Size(200, 16);
            this.BlobAreaXBar.TabIndex = 31;
            this.BlobAreaXBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.BlobAreaXBar_Scroll);
            // 
            // BlobHLabel
            // 
            this.BlobHLabel.Location = new System.Drawing.Point(272, 208);
            this.BlobHLabel.Name = "BlobHLabel";
            this.BlobHLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobHLabel.TabIndex = 54;
            this.BlobHLabel.Text = "0";
            // 
            // AreaHeightBar
            // 
            this.AreaHeightBar.LargeChange = 1;
            this.AreaHeightBar.Location = new System.Drawing.Point(120, 224);
            this.AreaHeightBar.Maximum = 9999;
            this.AreaHeightBar.Name = "AreaHeightBar";
            this.AreaHeightBar.Size = new System.Drawing.Size(200, 16);
            this.AreaHeightBar.TabIndex = 34;
            this.AreaHeightBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.AreaHeightBar_Scroll);
            // 
            // MaxCountBar
            // 
            this.MaxCountBar.LargeChange = 1;
            this.MaxCountBar.Location = new System.Drawing.Point(344, 128);
            this.MaxCountBar.Maximum = 9999;
            this.MaxCountBar.Name = "MaxCountBar";
            this.MaxCountBar.Size = new System.Drawing.Size(200, 16);
            this.MaxCountBar.TabIndex = 36;
            this.MaxCountBar.Value = 9999;
            this.MaxCountBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MaxCountBar_Scroll);
            // 
            // BlobWLabel2
            // 
            this.BlobWLabel2.Location = new System.Drawing.Point(120, 160);
            this.BlobWLabel2.Name = "BlobWLabel2";
            this.BlobWLabel2.Size = new System.Drawing.Size(72, 16);
            this.BlobWLabel2.TabIndex = 43;
            this.BlobWLabel2.Text = "Area Width";
            // 
            // BlobWLabel
            // 
            this.BlobWLabel.Location = new System.Drawing.Point(272, 160);
            this.BlobWLabel.Name = "BlobWLabel";
            this.BlobWLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobWLabel.TabIndex = 53;
            this.BlobWLabel.Text = "0";
            // 
            // BlobYLabel
            // 
            this.BlobYLabel.Location = new System.Drawing.Point(272, 112);
            this.BlobYLabel.Name = "BlobYLabel";
            this.BlobYLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobYLabel.TabIndex = 52;
            this.BlobYLabel.Text = "0";
            // 
            // BlobXLabel
            // 
            this.BlobXLabel.Location = new System.Drawing.Point(272, 64);
            this.BlobXLabel.Name = "BlobXLabel";
            this.BlobXLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobXLabel.TabIndex = 51;
            this.BlobXLabel.Text = "0";
            // 
            // DensityLabel
            // 
            this.DensityLabel.Location = new System.Drawing.Point(344, 160);
            this.DensityLabel.Name = "DensityLabel";
            this.DensityLabel.Size = new System.Drawing.Size(112, 16);
            this.DensityLabel.TabIndex = 48;
            this.DensityLabel.Text = "Minimum Density";
            // 
            // MinCountBar
            // 
            this.MinCountBar.LargeChange = 1;
            this.MinCountBar.Location = new System.Drawing.Point(344, 80);
            this.MinCountBar.Maximum = 9999;
            this.MinCountBar.Minimum = 1;
            this.MinCountBar.Name = "MinCountBar";
            this.MinCountBar.Size = new System.Drawing.Size(200, 16);
            this.MinCountBar.TabIndex = 35;
            this.MinCountBar.Value = 300;
            this.MinCountBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MinCountBar_Scroll);
            // 
            // MaxLabel
            // 
            this.MaxLabel.Location = new System.Drawing.Point(344, 112);
            this.MaxLabel.Name = "MaxLabel";
            this.MaxLabel.Size = new System.Drawing.Size(128, 16);
            this.MaxLabel.TabIndex = 47;
            this.MaxLabel.Text = "Maximum Pixel Count";
            // 
            // CountLabel
            // 
            this.CountLabel.Location = new System.Drawing.Point(344, 64);
            this.CountLabel.Name = "CountLabel";
            this.CountLabel.Size = new System.Drawing.Size(120, 16);
            this.CountLabel.TabIndex = 46;
            this.CountLabel.Text = "Minimum Pixel Count";
            // 
            // BlobHLabel2
            // 
            this.BlobHLabel2.Location = new System.Drawing.Point(120, 208);
            this.BlobHLabel2.Name = "BlobHLabel2";
            this.BlobHLabel2.Size = new System.Drawing.Size(72, 16);
            this.BlobHLabel2.TabIndex = 44;
            this.BlobHLabel2.Text = "Area Height";
            // 
            // AreaWidthBar
            // 
            this.AreaWidthBar.LargeChange = 1;
            this.AreaWidthBar.Location = new System.Drawing.Point(120, 176);
            this.AreaWidthBar.Maximum = 9999;
            this.AreaWidthBar.Name = "AreaWidthBar";
            this.AreaWidthBar.Size = new System.Drawing.Size(200, 16);
            this.AreaWidthBar.TabIndex = 33;
            this.AreaWidthBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.AreaWidthBar_Scroll);
            // 
            // BlobYLabel2
            // 
            this.BlobYLabel2.Location = new System.Drawing.Point(120, 112);
            this.BlobYLabel2.Name = "BlobYLabel2";
            this.BlobYLabel2.Size = new System.Drawing.Size(56, 16);
            this.BlobYLabel2.TabIndex = 42;
            this.BlobYLabel2.Text = "Area Y";
            // 
            // BlobXLabel2
            // 
            this.BlobXLabel2.Location = new System.Drawing.Point(120, 64);
            this.BlobXLabel2.Name = "BlobXLabel2";
            this.BlobXLabel2.Size = new System.Drawing.Size(56, 16);
            this.BlobXLabel2.TabIndex = 41;
            this.BlobXLabel2.Text = "Area X";
            // 
            // BlobAreaYBar
            // 
            this.BlobAreaYBar.LargeChange = 1;
            this.BlobAreaYBar.Location = new System.Drawing.Point(120, 128);
            this.BlobAreaYBar.Maximum = 9999;
            this.BlobAreaYBar.Name = "BlobAreaYBar";
            this.BlobAreaYBar.Size = new System.Drawing.Size(200, 16);
            this.BlobAreaYBar.TabIndex = 32;
            this.BlobAreaYBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.BlobAreaYBar_Scroll);
            // 
            // DensityBar
            // 
            this.DensityBar.LargeChange = 1;
            this.DensityBar.Location = new System.Drawing.Point(344, 176);
            this.DensityBar.Name = "DensityBar";
            this.DensityBar.Size = new System.Drawing.Size(200, 16);
            this.DensityBar.TabIndex = 37;
            this.DensityBar.Value = 50;
            this.DensityBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DensityBar_Scroll);
            // 
            // BlobDLabel
            // 
            this.BlobDLabel.Location = new System.Drawing.Point(496, 160);
            this.BlobDLabel.Name = "BlobDLabel";
            this.BlobDLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobDLabel.TabIndex = 58;
            this.BlobDLabel.Text = "50";
            // 
            // BlobMaxLabel
            // 
            this.BlobMaxLabel.Location = new System.Drawing.Point(496, 112);
            this.BlobMaxLabel.Name = "BlobMaxLabel";
            this.BlobMaxLabel.Size = new System.Drawing.Size(48, 16);
            this.BlobMaxLabel.TabIndex = 57;
            this.BlobMaxLabel.Text = "9999";
            // 
            // BlobCheckBox
            // 
            this.BlobCheckBox.Location = new System.Drawing.Point(288, 24);
            this.BlobCheckBox.Name = "BlobCheckBox";
            this.BlobCheckBox.Size = new System.Drawing.Size(120, 16);
            this.BlobCheckBox.TabIndex = 0;
            this.BlobCheckBox.Text = "Remove Blobs";
            // 
            // DotPage
            // 
            this.DotPage.Controls.Add(this.DotCheckBox);
            this.DotPage.Controls.Add(this.MaxDValue);
            this.DotPage.Controls.Add(this.DotDValue);
            this.DotPage.Controls.Add(this.HAdjValue);
            this.DotPage.Controls.Add(this.VAdjValue);
            this.DotPage.Controls.Add(this.DotHValue);
            this.DotPage.Controls.Add(this.DotWValue);
            this.DotPage.Controls.Add(this.DotSizeLabel);
            this.DotPage.Controls.Add(this.DLabel);
            this.DotPage.Controls.Add(this.HAdjLabel);
            this.DotPage.Controls.Add(this.VAdjLabel);
            this.DotPage.Controls.Add(this.DotAreaHeightLabel);
            this.DotPage.Controls.Add(this.DotWidthLabel);
            this.DotPage.Controls.Add(this.DotSizeBar);
            this.DotPage.Controls.Add(this.DotDensityBar);
            this.DotPage.Controls.Add(this.HAdjBar);
            this.DotPage.Controls.Add(this.VAdjBar);
            this.DotPage.Controls.Add(this.DotHBar);
            this.DotPage.Controls.Add(this.DotWBar);
            this.DotPage.Location = new System.Drawing.Point(4, 22);
            this.DotPage.Name = "DotPage";
            this.DotPage.Size = new System.Drawing.Size(704, 326);
            this.DotPage.TabIndex = 5;
            this.DotPage.Text = "Dot Shading Removal";
            // 
            // DotCheckBox
            // 
            this.DotCheckBox.Location = new System.Drawing.Point(272, 24);
            this.DotCheckBox.Name = "DotCheckBox";
            this.DotCheckBox.Size = new System.Drawing.Size(144, 16);
            this.DotCheckBox.TabIndex = 99;
            this.DotCheckBox.Text = "Remove Dot Shading";
            // 
            // MaxDValue
            // 
            this.MaxDValue.Location = new System.Drawing.Point(468, 235);
            this.MaxDValue.Name = "MaxDValue";
            this.MaxDValue.Size = new System.Drawing.Size(72, 16);
            this.MaxDValue.TabIndex = 98;
            this.MaxDValue.Text = "5";
            // 
            // DotDValue
            // 
            this.DotDValue.Location = new System.Drawing.Point(468, 203);
            this.DotDValue.Name = "DotDValue";
            this.DotDValue.Size = new System.Drawing.Size(64, 16);
            this.DotDValue.TabIndex = 97;
            this.DotDValue.Text = "0";
            // 
            // HAdjValue
            // 
            this.HAdjValue.Location = new System.Drawing.Point(468, 171);
            this.HAdjValue.Name = "HAdjValue";
            this.HAdjValue.Size = new System.Drawing.Size(72, 16);
            this.HAdjValue.TabIndex = 96;
            this.HAdjValue.Text = "0";
            // 
            // VAdjValue
            // 
            this.VAdjValue.Location = new System.Drawing.Point(468, 147);
            this.VAdjValue.Name = "VAdjValue";
            this.VAdjValue.Size = new System.Drawing.Size(64, 16);
            this.VAdjValue.TabIndex = 95;
            this.VAdjValue.Text = "0";
            // 
            // DotHValue
            // 
            this.DotHValue.Location = new System.Drawing.Point(468, 115);
            this.DotHValue.Name = "DotHValue";
            this.DotHValue.Size = new System.Drawing.Size(80, 16);
            this.DotHValue.TabIndex = 94;
            this.DotHValue.Text = "50";
            // 
            // DotWValue
            // 
            this.DotWValue.Location = new System.Drawing.Point(468, 75);
            this.DotWValue.Name = "DotWValue";
            this.DotWValue.Size = new System.Drawing.Size(72, 16);
            this.DotWValue.TabIndex = 93;
            this.DotWValue.Text = "300";
            // 
            // DotSizeLabel
            // 
            this.DotSizeLabel.Location = new System.Drawing.Point(104, 235);
            this.DotSizeLabel.Name = "DotSizeLabel";
            this.DotSizeLabel.Size = new System.Drawing.Size(104, 16);
            this.DotSizeLabel.TabIndex = 92;
            this.DotSizeLabel.Text = "Maximum Dot Size";
            // 
            // DLabel
            // 
            this.DLabel.Location = new System.Drawing.Point(104, 203);
            this.DLabel.Name = "DLabel";
            this.DLabel.Size = new System.Drawing.Size(116, 16);
            this.DLabel.TabIndex = 91;
            this.DLabel.Text = "Dot Shading Density";
            // 
            // HAdjLabel
            // 
            this.HAdjLabel.Location = new System.Drawing.Point(104, 171);
            this.HAdjLabel.Name = "HAdjLabel";
            this.HAdjLabel.Size = new System.Drawing.Size(140, 26);
            this.HAdjLabel.TabIndex = 90;
            this.HAdjLabel.Text = "Horizontal Size Adjustment";
            // 
            // VAdjLabel
            // 
            this.VAdjLabel.Location = new System.Drawing.Point(104, 139);
            this.VAdjLabel.Name = "VAdjLabel";
            this.VAdjLabel.Size = new System.Drawing.Size(128, 16);
            this.VAdjLabel.TabIndex = 89;
            this.VAdjLabel.Text = "Vertical Size Adjustment";
            // 
            // DotAreaHeightLabel
            // 
            this.DotAreaHeightLabel.Location = new System.Drawing.Point(104, 107);
            this.DotAreaHeightLabel.Name = "DotAreaHeightLabel";
            this.DotAreaHeightLabel.Size = new System.Drawing.Size(116, 16);
            this.DotAreaHeightLabel.TabIndex = 88;
            this.DotAreaHeightLabel.Text = "Minimum Area Height";
            // 
            // DotWidthLabel
            // 
            this.DotWidthLabel.Location = new System.Drawing.Point(104, 75);
            this.DotWidthLabel.Name = "DotWidthLabel";
            this.DotWidthLabel.Size = new System.Drawing.Size(116, 16);
            this.DotWidthLabel.TabIndex = 87;
            this.DotWidthLabel.Text = "Minimum Area Width";
            // 
            // DotSizeBar
            // 
            this.DotSizeBar.LargeChange = 1;
            this.DotSizeBar.Location = new System.Drawing.Point(260, 235);
            this.DotSizeBar.Maximum = 20;
            this.DotSizeBar.Name = "DotSizeBar";
            this.DotSizeBar.Size = new System.Drawing.Size(192, 16);
            this.DotSizeBar.TabIndex = 86;
            this.DotSizeBar.Value = 5;
            this.DotSizeBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DotSizeBar_Scroll);
            // 
            // DotDensityBar
            // 
            this.DotDensityBar.LargeChange = 1;
            this.DotDensityBar.Location = new System.Drawing.Point(260, 203);
            this.DotDensityBar.Maximum = 50;
            this.DotDensityBar.Minimum = -50;
            this.DotDensityBar.Name = "DotDensityBar";
            this.DotDensityBar.Size = new System.Drawing.Size(192, 16);
            this.DotDensityBar.TabIndex = 85;
            this.DotDensityBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DotDensityBar_Scroll);
            // 
            // HAdjBar
            // 
            this.HAdjBar.LargeChange = 1;
            this.HAdjBar.Location = new System.Drawing.Point(260, 171);
            this.HAdjBar.Minimum = -100;
            this.HAdjBar.Name = "HAdjBar";
            this.HAdjBar.Size = new System.Drawing.Size(192, 16);
            this.HAdjBar.TabIndex = 84;
            this.HAdjBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.HAdjBar_Scroll);
            // 
            // VAdjBar
            // 
            this.VAdjBar.LargeChange = 1;
            this.VAdjBar.Location = new System.Drawing.Point(260, 139);
            this.VAdjBar.Minimum = -100;
            this.VAdjBar.Name = "VAdjBar";
            this.VAdjBar.Size = new System.Drawing.Size(192, 16);
            this.VAdjBar.TabIndex = 83;
            this.VAdjBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VAdjBar_Scroll);
            // 
            // DotHBar
            // 
            this.DotHBar.LargeChange = 1;
            this.DotHBar.Location = new System.Drawing.Point(260, 107);
            this.DotHBar.Maximum = 9999;
            this.DotHBar.Name = "DotHBar";
            this.DotHBar.Size = new System.Drawing.Size(192, 16);
            this.DotHBar.TabIndex = 82;
            this.DotHBar.Value = 50;
            this.DotHBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DotHBar_Scroll);
            // 
            // DotWBar
            // 
            this.DotWBar.LargeChange = 1;
            this.DotWBar.Location = new System.Drawing.Point(260, 75);
            this.DotWBar.Maximum = 9999;
            this.DotWBar.Name = "DotWBar";
            this.DotWBar.Size = new System.Drawing.Size(192, 16);
            this.DotWBar.TabIndex = 81;
            this.DotWBar.Value = 300;
            this.DotWBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DotWBar_Scroll);
            // 
            // CropPage
            // 
            this.CropPage.Controls.Add(this.BorderCheckBox);
            this.CropPage.Controls.Add(this.ColorLabel);
            this.CropPage.Controls.Add(this.MaxPageHeightValue);
            this.CropPage.Controls.Add(this.MaxPageWidthValue);
            this.CropPage.Controls.Add(this.MinPageHeightValue);
            this.CropPage.Controls.Add(this.SpeckWValue);
            this.CropPage.Controls.Add(this.label12);
            this.CropPage.Controls.Add(this.label11);
            this.CropPage.Controls.Add(this.label10);
            this.CropPage.Controls.Add(this.SpeckWidthLabel);
            this.CropPage.Controls.Add(this.MaxHeightBar);
            this.CropPage.Controls.Add(this.MaxWidthBar);
            this.CropPage.Controls.Add(this.MinHeightBar);
            this.CropPage.Controls.Add(this.SpeckWidthBar);
            this.CropPage.Controls.Add(this.SpeckConValue);
            this.CropPage.Controls.Add(this.SpeckQValue);
            this.CropPage.Controls.Add(this.PSpeckValue);
            this.CropPage.Controls.Add(this.BSpeckValue);
            this.CropPage.Controls.Add(this.SpeckConLabel);
            this.CropPage.Controls.Add(this.SpeckQualityLabel);
            this.CropPage.Controls.Add(this.PSpeckLabel);
            this.CropPage.Controls.Add(this.SpeckLabel);
            this.CropPage.Controls.Add(this.MinConBar);
            this.CropPage.Controls.Add(this.SpeckQualityBar);
            this.CropPage.Controls.Add(this.PSpeckBar);
            this.CropPage.Controls.Add(this.BSpeckBar);
            this.CropPage.Controls.Add(this.cmbpad);
            this.CropPage.Controls.Add(this.chkdeskew);
            this.CropPage.Controls.Add(this.chkreplace);
            this.CropPage.Controls.Add(this.chkcrop);
            this.CropPage.Location = new System.Drawing.Point(4, 22);
            this.CropPage.Name = "CropPage";
            this.CropPage.Size = new System.Drawing.Size(704, 326);
            this.CropPage.TabIndex = 6;
            this.CropPage.Text = "Border Removal";
            // 
            // BorderCheckBox
            // 
            this.BorderCheckBox.Location = new System.Drawing.Point(312, 24);
            this.BorderCheckBox.Name = "BorderCheckBox";
            this.BorderCheckBox.Size = new System.Drawing.Size(136, 24);
            this.BorderCheckBox.TabIndex = 60;
            this.BorderCheckBox.Text = "Border Removal";
            // 
            // ColorLabel
            // 
            this.ColorLabel.Location = new System.Drawing.Point(24, 216);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(56, 16);
            this.ColorLabel.TabIndex = 59;
            this.ColorLabel.Text = "Pad Color";
            // 
            // MaxPageHeightValue
            // 
            this.MaxPageHeightValue.Location = new System.Drawing.Point(600, 224);
            this.MaxPageHeightValue.Name = "MaxPageHeightValue";
            this.MaxPageHeightValue.Size = new System.Drawing.Size(40, 16);
            this.MaxPageHeightValue.TabIndex = 58;
            this.MaxPageHeightValue.Text = "0";
            // 
            // MaxPageWidthValue
            // 
            this.MaxPageWidthValue.Location = new System.Drawing.Point(600, 176);
            this.MaxPageWidthValue.Name = "MaxPageWidthValue";
            this.MaxPageWidthValue.Size = new System.Drawing.Size(40, 16);
            this.MaxPageWidthValue.TabIndex = 57;
            this.MaxPageWidthValue.Text = "0";
            // 
            // MinPageHeightValue
            // 
            this.MinPageHeightValue.Location = new System.Drawing.Point(600, 128);
            this.MinPageHeightValue.Name = "MinPageHeightValue";
            this.MinPageHeightValue.Size = new System.Drawing.Size(40, 16);
            this.MinPageHeightValue.TabIndex = 56;
            this.MinPageHeightValue.Text = "0";
            // 
            // SpeckWValue
            // 
            this.SpeckWValue.Location = new System.Drawing.Point(600, 80);
            this.SpeckWValue.Name = "SpeckWValue";
            this.SpeckWValue.Size = new System.Drawing.Size(40, 16);
            this.SpeckWValue.TabIndex = 55;
            this.SpeckWValue.Text = "0";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(448, 224);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(120, 16);
            this.label12.TabIndex = 54;
            this.label12.Text = "Maximum Page Height";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(448, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 16);
            this.label11.TabIndex = 53;
            this.label11.Text = "Maximum Page Width";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(448, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 16);
            this.label10.TabIndex = 52;
            this.label10.Text = "Minimum Page Height";
            // 
            // SpeckWidthLabel
            // 
            this.SpeckWidthLabel.Location = new System.Drawing.Point(448, 80);
            this.SpeckWidthLabel.Name = "SpeckWidthLabel";
            this.SpeckWidthLabel.Size = new System.Drawing.Size(112, 16);
            this.SpeckWidthLabel.TabIndex = 51;
            this.SpeckWidthLabel.Text = "Minimum Page Width";
            // 
            // MaxHeightBar
            // 
            this.MaxHeightBar.LargeChange = 1;
            this.MaxHeightBar.Location = new System.Drawing.Point(448, 240);
            this.MaxHeightBar.Maximum = 9999;
            this.MaxHeightBar.Name = "MaxHeightBar";
            this.MaxHeightBar.Size = new System.Drawing.Size(192, 16);
            this.MaxHeightBar.TabIndex = 50;
            this.MaxHeightBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MaxHeightBar_Scroll);
            // 
            // MaxWidthBar
            // 
            this.MaxWidthBar.LargeChange = 1;
            this.MaxWidthBar.Location = new System.Drawing.Point(448, 192);
            this.MaxWidthBar.Maximum = 9999;
            this.MaxWidthBar.Name = "MaxWidthBar";
            this.MaxWidthBar.Size = new System.Drawing.Size(192, 16);
            this.MaxWidthBar.TabIndex = 49;
            this.MaxWidthBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MaxWidthBar_Scroll);
            // 
            // MinHeightBar
            // 
            this.MinHeightBar.LargeChange = 1;
            this.MinHeightBar.Location = new System.Drawing.Point(448, 144);
            this.MinHeightBar.Maximum = 9999;
            this.MinHeightBar.Name = "MinHeightBar";
            this.MinHeightBar.Size = new System.Drawing.Size(192, 16);
            this.MinHeightBar.TabIndex = 48;
            this.MinHeightBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MinHeightBar_Scroll);
            // 
            // SpeckWidthBar
            // 
            this.SpeckWidthBar.LargeChange = 1;
            this.SpeckWidthBar.Location = new System.Drawing.Point(448, 96);
            this.SpeckWidthBar.Maximum = 9999;
            this.SpeckWidthBar.Name = "SpeckWidthBar";
            this.SpeckWidthBar.Size = new System.Drawing.Size(192, 16);
            this.SpeckWidthBar.TabIndex = 47;
            this.SpeckWidthBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.SpeckWidthBar_Scroll);
            // 
            // SpeckConValue
            // 
            this.SpeckConValue.Location = new System.Drawing.Point(344, 224);
            this.SpeckConValue.Name = "SpeckConValue";
            this.SpeckConValue.Size = new System.Drawing.Size(48, 16);
            this.SpeckConValue.TabIndex = 46;
            this.SpeckConValue.Text = "50";
            // 
            // SpeckQValue
            // 
            this.SpeckQValue.Location = new System.Drawing.Point(344, 176);
            this.SpeckQValue.Name = "SpeckQValue";
            this.SpeckQValue.Size = new System.Drawing.Size(48, 16);
            this.SpeckQValue.TabIndex = 45;
            this.SpeckQValue.Text = "80";
            // 
            // PSpeckValue
            // 
            this.PSpeckValue.Location = new System.Drawing.Point(344, 128);
            this.PSpeckValue.Name = "PSpeckValue";
            this.PSpeckValue.Size = new System.Drawing.Size(48, 16);
            this.PSpeckValue.TabIndex = 44;
            this.PSpeckValue.Text = "2";
            // 
            // BSpeckValue
            // 
            this.BSpeckValue.Location = new System.Drawing.Point(352, 80);
            this.BSpeckValue.Name = "BSpeckValue";
            this.BSpeckValue.Size = new System.Drawing.Size(40, 16);
            this.BSpeckValue.TabIndex = 43;
            this.BSpeckValue.Text = "3";
            // 
            // SpeckConLabel
            // 
            this.SpeckConLabel.Location = new System.Drawing.Point(192, 224);
            this.SpeckConLabel.Name = "SpeckConLabel";
            this.SpeckConLabel.Size = new System.Drawing.Size(120, 16);
            this.SpeckConLabel.TabIndex = 42;
            this.SpeckConLabel.Text = "Minimum Confidence";
            // 
            // SpeckQualityLabel
            // 
            this.SpeckQualityLabel.Location = new System.Drawing.Point(192, 176);
            this.SpeckQualityLabel.Name = "SpeckQualityLabel";
            this.SpeckQualityLabel.Size = new System.Drawing.Size(56, 16);
            this.SpeckQualityLabel.TabIndex = 41;
            this.SpeckQualityLabel.Text = "Quality";
            // 
            // PSpeckLabel
            // 
            this.PSpeckLabel.Location = new System.Drawing.Point(192, 128);
            this.PSpeckLabel.Name = "PSpeckLabel";
            this.PSpeckLabel.Size = new System.Drawing.Size(96, 16);
            this.PSpeckLabel.TabIndex = 40;
            this.PSpeckLabel.Text = "Page Speck Size";
            // 
            // SpeckLabel
            // 
            this.SpeckLabel.Location = new System.Drawing.Point(192, 80);
            this.SpeckLabel.Name = "SpeckLabel";
            this.SpeckLabel.Size = new System.Drawing.Size(104, 16);
            this.SpeckLabel.TabIndex = 39;
            this.SpeckLabel.Text = "Border Speck Size";
            // 
            // MinConBar
            // 
            this.MinConBar.LargeChange = 1;
            this.MinConBar.Location = new System.Drawing.Point(192, 240);
            this.MinConBar.Minimum = 1;
            this.MinConBar.Name = "MinConBar";
            this.MinConBar.Size = new System.Drawing.Size(200, 16);
            this.MinConBar.TabIndex = 38;
            this.MinConBar.Value = 50;
            this.MinConBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MinConBar_Scroll);
            // 
            // SpeckQualityBar
            // 
            this.SpeckQualityBar.LargeChange = 1;
            this.SpeckQualityBar.Location = new System.Drawing.Point(192, 192);
            this.SpeckQualityBar.Minimum = 1;
            this.SpeckQualityBar.Name = "SpeckQualityBar";
            this.SpeckQualityBar.Size = new System.Drawing.Size(200, 16);
            this.SpeckQualityBar.TabIndex = 37;
            this.SpeckQualityBar.Value = 80;
            this.SpeckQualityBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.SpeckQualityBar_Scroll);
            // 
            // PSpeckBar
            // 
            this.PSpeckBar.LargeChange = 1;
            this.PSpeckBar.Location = new System.Drawing.Point(192, 144);
            this.PSpeckBar.Name = "PSpeckBar";
            this.PSpeckBar.Size = new System.Drawing.Size(200, 16);
            this.PSpeckBar.TabIndex = 36;
            this.PSpeckBar.Value = 2;
            this.PSpeckBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.PSpeckBar_Scroll);
            // 
            // BSpeckBar
            // 
            this.BSpeckBar.LargeChange = 1;
            this.BSpeckBar.Location = new System.Drawing.Point(192, 96);
            this.BSpeckBar.Name = "BSpeckBar";
            this.BSpeckBar.Size = new System.Drawing.Size(200, 16);
            this.BSpeckBar.TabIndex = 35;
            this.BSpeckBar.Value = 3;
            this.BSpeckBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.BSpeckBar_Scroll);
            // 
            // cmbpad
            // 
            this.cmbpad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbpad.Items.AddRange(new object[] {
            "White",
            "Black"});
            this.cmbpad.Location = new System.Drawing.Point(88, 216);
            this.cmbpad.Name = "cmbpad";
            this.cmbpad.Size = new System.Drawing.Size(80, 21);
            this.cmbpad.TabIndex = 34;
            // 
            // chkdeskew
            // 
            this.chkdeskew.Checked = true;
            this.chkdeskew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkdeskew.Location = new System.Drawing.Point(24, 184);
            this.chkdeskew.Name = "chkdeskew";
            this.chkdeskew.Size = new System.Drawing.Size(64, 16);
            this.chkdeskew.TabIndex = 33;
            this.chkdeskew.Text = "Deskew Border";
            // 
            // chkreplace
            // 
            this.chkreplace.Location = new System.Drawing.Point(24, 152);
            this.chkreplace.Name = "chkreplace";
            this.chkreplace.Size = new System.Drawing.Size(104, 16);
            this.chkreplace.TabIndex = 32;
            this.chkreplace.Text = "Replace Border";
            // 
            // chkcrop
            // 
            this.chkcrop.Checked = true;
            this.chkcrop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkcrop.Location = new System.Drawing.Point(24, 120);
            this.chkcrop.Name = "chkcrop";
            this.chkcrop.Size = new System.Drawing.Size(88, 16);
            this.chkcrop.TabIndex = 31;
            this.chkcrop.Text = "Crop Border";
            // 
            // EditionNote
            // 
            this.EditionNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EditionNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditionNote.Location = new System.Drawing.Point(304, 664);
            this.EditionNote.Name = "EditionNote";
            this.EditionNote.Size = new System.Drawing.Size(441, 32);
            this.EditionNote.TabIndex = 31;
            this.EditionNote.Text = "NOTE: Features in the Bitonal tab are enabled for demonstration purposes only and" +
                " require licensing of ScanFix Bitonal for application use.";
            // 
            // ProcessScreen
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(762, 704);
            this.Controls.Add(this.OutputImgLabel);
            this.Controls.Add(this.InputImgLabel);
            this.Controls.Add(this.OutputImgView);
            this.Controls.Add(this.InputImgView);
            this.Controls.Add(this.TabRoll);
            this.Controls.Add(this.PreviewButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.EditionNote);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ProcessScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PreProcess Setup - Image Transformation";
            this.Activated += new System.EventHandler(this.ProcessScreen_Activated);
            this.Load += new System.EventHandler(this.ProcessScreen_Load);
            this.ErodeGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ErodeBox)).EndInit();
            this.DilatePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DilateBox)).EndInit();
            this.LitePanel.ResumeLayout(false);
            this.LitePanel.PerformLayout();
            this.TabRoll.ResumeLayout(false);
            this.Page1.ResumeLayout(false);
            this.Page2.ResumeLayout(false);
            this.BitonalTabRoll.ResumeLayout(false);
            this.CombtPage.ResumeLayout(false);
            this.NegPage.ResumeLayout(false);
            this.SmoothPage.ResumeLayout(false);
            this.InversePage.ResumeLayout(false);
            this.BlobPage.ResumeLayout(false);
            this.DotPage.ResumeLayout(false);
            this.CropPage.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private PegasusImaging.WinForms.ScanFix5.CombRemovalOptions comb;
		private PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions dot;
		private PegasusImaging.WinForms.ScanFix5.InverseTextOptions text;
		private PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions neg;
		private PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions blob;
		private PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions border;

		private void ProcessScreen_Load(object sender, System.EventArgs e)
		{

			VerticalRadioButton1.Checked = true;
			HorizontalRadioButton1.Checked = false;
			DiagonalRadioButton1.Checked = false;
			AllRadioButton1.Checked = false;

			VerticalRadioButton2.Checked = true;
			HorizontalRadioButton2.Checked = false;
			DiagonalRadioButton2.Checked = false;
			AllRadioButton2.Checked = false;
			cmbpad.SelectedIndex = 0;


			
			if (scanFix1.License.LicenseEdition == PegasusImaging.WinForms.ScanFix5.License.LicenseChoice.LiteEdition)
			{
				BitonalTabRoll.Enabled = false;
			}

		}

		private void CloseButton_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			PreprocessedFlag = true;

			Processor proc = new Processor(mainImage.Image);

			if(mainImage.Image.ImageXData.BitsPerPixel != 0)
			{
				proc.ColorDepth(1,0,0);
			}

			scanFix1.FromHdib(InputImgView.Image.ToHdib(false));

			if(MirrorCheckBox.Checked == true)
			{
				scanFix1.Mirror();
			}
			if(FlipCheckBox.Checked == true)
			{
				scanFix1.Flip();
			}
			if(SmoothCheckBox.Checked == true)
			{
				scanFix1.SmoothZoom();
			}
			if(ShearCheckBox.Checked == true)
			{
				scanFix1.Shear(Int32.Parse(ShearTextBox.Text));
			}
			if(Rotate90CheckBox.Checked == true)
			{
				scanFix1.Rotate(90);
			}
			if(Rotate180CheckBox.Checked == true)
			{
				scanFix1.Rotate(180);
			}
			if(NegateCheckBox.Checked == true)
			{
				scanFix1.Negate();
			}
			if(RotateCheckBox.Checked == true)
			{
				scanFix1.Rotate(Int32.Parse(RotateTextBox.Text));
			}

			if(ErodeCheckBox.Checked==true)
			{
				ErodeOptions ErodeOpts = new ErodeOptions();
				ErodeOpts.Amount = (int)ErodeBox.Value;
				EnhancementDirections ErodeDir = new EnhancementDirections();

				if (VerticalRadioButton1.Checked == true)
				{
					ErodeDir = EnhancementDirections.Down;
				}
                if (VUpErode.Checked == true)
                {
                    ErodeDir = EnhancementDirections.Up;
                }
				if (HorizontalRadioButton1.Checked == true)
				{
					ErodeDir = EnhancementDirections.Right;
				}
                if (HLeftErode.Checked == true)
                {
                    ErodeDir = EnhancementDirections.Left;
                }
				if(DiagonalRadioButton1.Checked == true)
				{
					ErodeDir = (EnhancementDirections) (EnhancementDirections.Down.GetHashCode() + EnhancementDirections.Right.GetHashCode());
					ErodeOpts.OnlyDiagonal = true;
				}
                if (DiagLeftDownErode.Checked == true)
                {
                    ErodeDir = (EnhancementDirections)(EnhancementDirections.Left.GetHashCode() + EnhancementDirections.Down.GetHashCode());
                    ErodeOpts.OnlyDiagonal = true;
                }
                if (DiagUpLeftErode.Checked == true)
                {
                    ErodeDir = (EnhancementDirections)(EnhancementDirections.Up.GetHashCode() + EnhancementDirections.Left.GetHashCode());
                    ErodeOpts.OnlyDiagonal = true;
                }
                if (DiagUpRightErode.Checked == true)
                {
                    ErodeDir = (EnhancementDirections)(EnhancementDirections.Up.GetHashCode() + EnhancementDirections.Right.GetHashCode());
                    ErodeOpts.OnlyDiagonal = true;
                }
				if(AllRadioButton1.Checked == true)
				{
					ErodeDir = EnhancementDirections.All;
				}

				ErodeOpts.Direction = ErodeDir;
				scanFix1.Erode(ErodeOpts);
			}

			if(DilateCheckBox.Checked==true)
			{
				DilateOptions DilateOpts = new DilateOptions();
				DilateOpts.Amount = (int)DilateBox.Value;
				EnhancementDirections DilateDir = new EnhancementDirections();
				

				if (VerticalRadioButton2.Checked == true)
				{
					DilateDir = EnhancementDirections.Down;
				}
                if (VUpDilate.Checked == true)
                {
                    DilateDir = EnhancementDirections.Up;
                }
				if (HorizontalRadioButton2.Checked == true)
				{
					DilateDir = EnhancementDirections.Right;
				}
                if (HLeftDilate.Checked == true)
                {
                    DilateDir = EnhancementDirections.Left;
                }
				if(DiagonalRadioButton2.Checked == true)
				{
					DilateDir = (EnhancementDirections) (EnhancementDirections.Down.GetHashCode() + EnhancementDirections.Right.GetHashCode());
					DilateOpts.OnlyDiagonal = true;
				}
                if (DiagUpLeftDilate.Checked == true)
                {
                    DilateDir = (EnhancementDirections)(EnhancementDirections.Up.GetHashCode() + EnhancementDirections.Left.GetHashCode());
                    DilateOpts.OnlyDiagonal = true;
                }
                if (DiagUpRightDilate.Checked == true)
                {
                    DilateDir = (EnhancementDirections)(EnhancementDirections.Up.GetHashCode() + EnhancementDirections.Right.GetHashCode());
                    DilateOpts.OnlyDiagonal = true;
                }
                if (DiagLeftDownDilate.Checked == true)
                {
                    DilateDir = (EnhancementDirections)(EnhancementDirections.Down.GetHashCode() + EnhancementDirections.Left.GetHashCode());
                    DilateOpts.OnlyDiagonal = true;
                }
				if(AllRadioButton2.Checked == true)
				{
					DilateDir = EnhancementDirections.All;
				}
                

				DilateOpts.Direction = DilateDir;
				scanFix1.Dilate(DilateOpts);
			}

			if(RemoveCheckBox.Checked == true)
			{
				LineRemovalOptions LineOpts = new LineRemovalOptions();
				LineOpts.MinimumLength = Int32.Parse(LengthTextBox.Text);
				scanFix1.RemoveLines(LineOpts);
			}

			if(DeleteCheckBox.Checked == true)
			{
				DespeckleOptions DespeckOpts = new DespeckleOptions();
				DespeckOpts.SpeckWidth = Int32.Parse(WidthTextBox.Text);
				DespeckOpts.SpeckHeight = Int32.Parse(HeightTextBox.Text);
				scanFix1.Despeckle(DespeckOpts);
			}

			if(DeskewCheckBox.Checked == true)
			{	
				scanFix1.Deskew();
			}

			if (CombCheckBox.Checked == true)
			{
				comb = new PegasusImaging.WinForms.ScanFix5.CombRemovalOptions();
				
				comb.Area = new System.Drawing.Rectangle(AreaXBar.Value, AreaYBar.Value, AreaWBar.Value, AreaHBar.Value);
				comb.CombHeight = CombHBar.Value;
				comb.CombSpacing = SpaceBar.Value;
				comb.HorizontalLineThickness = HBar.Value;
				comb.VerticalLineThickness = VBar.Value;
				comb.MinimumCombLength = LengthBar.Value;
				comb.MinimumConfidence = ConfidenceBar.Value;

				scanFix1.RemoveCombs(comb);
			}

			if (NegCheckBox.Checked == true)
			{
				neg = new PegasusImaging.WinForms.ScanFix5.NegativeCorrectionOptions();

				neg.ApplyCorrection =ApplyCheckBox.Checked ;
				neg.MinimumConfidence = ConBar.Value;
				neg.Quality = QualityBar.Value;

				scanFix1.CorrectNegativePage(neg);
			}

			if (SmoothObjectsCheckBox.Checked == true)
			{
				scanFix1.SmoothObjects(SmoothBar.Value);
			}

			if (InverseCheckBox.Checked == true)
			{
				text = new PegasusImaging.WinForms.ScanFix5.InverseTextOptions();

				text.MinimumAreaHeight=HeightBar.Value;
				text.MinimumAreaWidth = WidthBar.Value;
				text.MinimumBlankOnEdges = BlackBar.Value;

				scanFix1.CorrectInverseText(text);
			}

			if (BlobCheckBox.Checked == true)
			{
				blob = new PegasusImaging.WinForms.ScanFix5.BlobRemovalOptions();

				blob.Area = new System.Drawing.Rectangle(BlobAreaXBar.Value, BlobAreaYBar.Value, AreaWidthBar.Value, AreaHeightBar.Value);
				blob.MaximumPixelCount = MaxCountBar.Value;
				blob.MinimumPixelCount = MinCountBar.Value;
				blob.MinimumDensity = DensityBar.Value;

				scanFix1.RemoveBlobs(blob);
			}

			if (DotCheckBox.Checked == true)
			{
				dot = new PegasusImaging.WinForms.ScanFix5.DotShadingRemovalOptions();

				dot.DensityAdjustment = DotDensityBar.Value;
				dot.MaximumDotSize = DotSizeBar.Value;
				dot.HorizontalSizeAdjustment = HAdjBar.Value;
				dot.VerticalSizeAdjustment = VAdjBar.Value;
				dot.MinimumAreaHeight = DotHBar.Value;
				dot.MinimumAreaWidth = DotWBar.Value;

				scanFix1.RemoveDotShading(dot);
			}

			if (BorderCheckBox.Checked == true)
			{
				border = new PegasusImaging.WinForms.ScanFix5.BorderRemovalOptions();

				border.BorderSpeckSize =BSpeckBar.Value;
				border.CropBorder = chkcrop.Checked;
				border.DeskewBorder = chkdeskew.Checked;
				border.MaximumPageHeight = MaxHeightBar.Value;
				border.MaximumPageWidth = MaxWidthBar.Value;
				border.MinimumConfidence = MinConBar.Value;
				border.MinimumPageHeight = MinHeightBar.Value;
				border.MinimumPageWidth = SpeckWidthBar.Value;
				border.PageSpeckSize = PSpeckBar.Value;
				border.Quality = SpeckQualityBar.Value;
				border.ReplaceBorder = chkreplace.Checked;

				if (cmbpad.SelectedIndex == 1)
				{
					border.PadColor = System.Drawing.Color.FromArgb(0,0,0);
				}
				else
				{
					border.PadColor = System.Drawing.Color.FromArgb(255,255,255);
				}

				scanFix1.RemoveBorder(border);
			}

            int DIB = scanFix1.ToHdib(true).ToInt32();

			if(PreviewFlag == false)
			{
				mainImage.Image = ImageX.FromHdib(new System.IntPtr(DIB));
				mainTestImage.Image = mainImage.Image;
			}
			else
			{
                OutputImgView.Image = ImageX.FromHdib(new System.IntPtr(DIB));
				PreviewFlag = false;
			}

          

            System.Runtime.InteropServices.Marshal.FreeHGlobal(new System.IntPtr(DIB));
		}

		private void PreviewButton_Click(object sender, System.EventArgs e)
		{
			PreviewFlag = true;
			OKButton_Click(sender,e);
		}

		private void ProcessScreen_Activated(object sender, System.EventArgs e)
		{
			if((mainImage.Rubberband.Dimensions.Height <= 0) | (mainImage.Rubberband.Dimensions.Width <= 0))
			{
				InputImgView.Image = mainImage.Image;
			}
			else
			{
				InputImgView.Image = mainImage.Rubberband.Copy();
			}
		}

		private void RotateTextBox_TextChanged(object sender, System.EventArgs e)
		{
			try
			{
				int rotate_value = 0;

				if(RotateTextBox.Text.Length > 1)
				{
					rotate_value = Int32.Parse(RotateTextBox.Text);
				}

				if(rotate_value < -360)
				{
					RotateTextBox.Text = "-360";
				}
				if(rotate_value > 360)
				{
					RotateTextBox.Text = "360";
				}
			}
			catch
			{
				RotateTextBox.Text = "0";
			}
		}

		private void RotateTextBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if(((e.KeyChar < '0') | (e.KeyChar > '9')) & (e.KeyChar != '-'))
			{
				e.Handled = true;
			}
		}

        //private void ShearTextBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        //{
        //    if(((e.KeyChar < '0') | (e.KeyChar > '9')) & (e.KeyChar != '-'))
        //    {
        //        e.Handled = true;
        //    }
        //}

		private void HeightTextBox_TextChanged(object sender, System.EventArgs e)
		{
			if(Int32.Parse(HeightTextBox.Text) < 1)
			{
				HeightTextBox.Text = "1";
			}
			if(Int32.Parse(HeightTextBox.Text) > 15)
			{
				HeightTextBox.Text = "15";
			}
		}

		private void HeightTextBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if((e.KeyChar < '0') | (e.KeyChar > '9'))
			{
				e.Handled = true;
			}
		}

		private void WidthTextBox_TextChanged(object sender, System.EventArgs e)
		{
			if(Int32.Parse(WidthTextBox.Text) < 1)
			{
				WidthTextBox.Text = "1";
			}
			if(Int32.Parse(WidthTextBox.Text) > 15)
			{
				WidthTextBox.Text = "15";
			}
		}

		private void WidthTextBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if((e.KeyChar < '0') | (e.KeyChar > '9'))
			{
				e.Handled = true;
			}
		}

		private void LengthTextBox_TextChanged(object sender, System.EventArgs e)
		{
			if(Int32.Parse(LengthTextBox.Text) < 10)
			{
				LengthTextBox.Text = "10";
			}
			if(Int32.Parse(LengthTextBox.Text) > 20000)
			{
				LengthTextBox.Text = "20000";
			}
		}

		private void LengthTextBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if((e.KeyChar < '0') | (e.KeyChar > '9'))
			{
				e.Handled = true;
			}
		}

		private void AreaXBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			AreaXValue.Text = AreaXBar.Value.ToString();
		}

		private void AreaYBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			AreaYValue.Text = AreaYBar.Value.ToString();
		}

		private void AreaWBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			AreaWValue.Text = AreaWBar.Value.ToString();
		}

		private void AreaHBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			AreaHValue.Text = AreaHBar.Value.ToString();
		}

		private void CombHBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			CombHValue.Text = CombHBar.Value.ToString();
		}

		private void SpaceBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			SpaceValue.Text = SpaceBar.Value.ToString();
		}

		private void LengthBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			LengthValue.Text = LengthBar.Value.ToString();
		}

		private void ConfidenceBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			ConValue.Text = ConfidenceBar.Value.ToString();
		}

		private void HBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HThickValue.Text = HBar.Value.ToString();
		}

		private void VBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VThickValue.Text = VBar.Value.ToString();
		}

		private void ConBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			NegConValue.Text = ConBar.Value.ToString();
		}

		private void QualityBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			QualityValue.Text = QualityBar.Value.ToString();
		}

		private void SmoothBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			SmoothValue.Text = SmoothBar.Value.ToString();
		}

		private void WidthBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			WidthBox.Text = WidthBar.Value.ToString();
		}

		private void HeightBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HeightBox.Text = HeightBar.Value.ToString();
		}

		private void BlackBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlackBox.Text = BlackBar.Value.ToString();
		}

		private void BlobAreaXBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobXLabel.Text = BlobAreaXBar.Value.ToString();
		}

		private void BlobAreaYBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobYLabel.Text = BlobAreaYBar.Value.ToString();
		}

		private void AreaWidthBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobWLabel.Text = AreaWidthBar.Value.ToString();
		}

		private void AreaHeightBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobHLabel.Text = AreaHeightBar.Value.ToString();
		}

		private void MinCountBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobMinLabel.Text = MinCountBar.Value.ToString();
		}

		private void MaxCountBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobMaxLabel.Text = MaxCountBar.Value.ToString();
		}

		private void DensityBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BlobDLabel.Text = DensityBar.Value.ToString();
		}

		private void DotWBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			DotWValue.Text = DotWBar.Value.ToString();
		}

		private void DotHBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			DotHValue.Text = DotHBar.Value.ToString();
		}

		private void VAdjBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			VAdjValue.Text = VAdjBar.Value.ToString();
		}

		private void HAdjBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			HAdjValue.Text = HAdjBar.Value.ToString();
		}

		private void DotDensityBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			DotDValue.Text = DotDensityBar.Value.ToString();
		}

		private void DotSizeBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			MaxDValue.Text = DotSizeBar.Value.ToString();
		}

		private void BSpeckBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			BSpeckValue.Text = BSpeckBar.Value.ToString();
		}

		private void PSpeckBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			PSpeckValue.Text = PSpeckBar.Value.ToString();
		}

		private void SpeckQualityBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			SpeckQValue.Text = SpeckQualityBar.Value.ToString();
		}

		private void MinConBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			SpeckConValue.Text = MinConBar.Value.ToString();
		}

		private void SpeckWidthBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			SpeckWValue.Text = SpeckWidthBar.Value.ToString();
		}

		private void MinHeightBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			MinPageHeightValue.Text = MinHeightBar.Value.ToString();
		}

		private void MaxWidthBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			MaxPageWidthValue.Text = MaxWidthBar.Value.ToString();
		}

		private void MaxHeightBar_Scroll(object sender, System.Windows.Forms.ScrollEventArgs e)
		{
			MaxPageHeightValue.Text = MaxHeightBar.Value.ToString();
		}

        private void ShearTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                int shear_value = 0;

                if (ShearTextBox.Text.Length > 1)
                {
                    shear_value = Int32.Parse(ShearTextBox.Text);
                }

                if (Int32.Parse(ShearTextBox.Text) < -15)
                {
                    ShearTextBox.Text = "-15";
                }
                if (Int32.Parse(ShearTextBox.Text) > 15)
                {
                    ShearTextBox.Text = "15";
                }
            }
            catch
            {
                ShearTextBox.Text = "0";
            }
        }
	}
}
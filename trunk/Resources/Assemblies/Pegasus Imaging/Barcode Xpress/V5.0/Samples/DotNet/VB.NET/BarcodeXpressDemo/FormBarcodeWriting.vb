'****************************************************************
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.BarcodeXpress5
Imports System.Drawing.Printing
Public Class FormBarcodeWriting
    Inherits System.Windows.Forms.Form

    Dim barcodeXpress As PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal barcodeObject As PegasusImaging.WinForms.BarcodeXpress5.BarcodeXpress)
        MyBase.New()


        barcodeXpress = barcodeObject

        'This call is required by the Windows Form Designer.
        InitializeComponent()



        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend results() As PegasusImaging.WinForms.BarcodeXpress5.Result
    Friend WithEvents CloseForm As System.Windows.Forms.Button
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents hScrollBarUPCFont As System.Windows.Forms.HScrollBar
    Friend WithEvents buttonWrite As System.Windows.Forms.Button
    Friend WithEvents labelUPCGap As System.Windows.Forms.Label
    Friend WithEvents labelValueFont As System.Windows.Forms.Label
    Friend WithEvents labelMinBarVal As System.Windows.Forms.Label
    Friend WithEvents hScrollBarValueGap As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarUPCNotch As System.Windows.Forms.HScrollBar
    Friend WithEvents labelUPCFont As System.Windows.Forms.Label
    Friend WithEvents labelUPCNotch As System.Windows.Forms.Label
    Friend WithEvents labelValueGap As System.Windows.Forms.Label
    Friend WithEvents labelBCHeight As System.Windows.Forms.Label
    Friend WithEvents labelMinBar As System.Windows.Forms.Label
    Friend WithEvents labelBCValue As System.Windows.Forms.Label
    Friend WithEvents comboBoxStyle As System.Windows.Forms.ComboBox
    Friend WithEvents labelStyle As System.Windows.Forms.Label
    Friend WithEvents labelHeightVal As System.Windows.Forms.Label
    Friend WithEvents labelUPCFontVal As System.Windows.Forms.Label
    Friend WithEvents labelValueFontVal As System.Windows.Forms.Label
    Friend WithEvents labelUPCGapVal As System.Windows.Forms.Label
    Friend WithEvents labelValueGapVal As System.Windows.Forms.Label
    Friend WithEvents labelUPCNotchVal As System.Windows.Forms.Label
    Friend WithEvents textBoxBCValue As System.Windows.Forms.TextBox
    Friend WithEvents hScrollBarUPCGap As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarValueFont As System.Windows.Forms.HScrollBar
    Friend WithEvents buttonPrint As System.Windows.Forms.Button
    Friend WithEvents buttonCreate As System.Windows.Forms.Button
    Friend WithEvents labelResult As System.Windows.Forms.Label
    Friend WithEvents labelInfo As System.Windows.Forms.Label
    Friend WithEvents labelStatus As System.Windows.Forms.Label
    Friend WithEvents hScrollBarBCHeight As System.Windows.Forms.HScrollBar
    Friend WithEvents hScrollBarMinBar As System.Windows.Forms.HScrollBar
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents cmbBarcodeType As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumnsAndRows As System.Windows.Forms.ComboBox
    Friend WithEvents hscHeight As System.Windows.Forms.HScrollBar
    Friend WithEvents hscWidth As System.Windows.Forms.HScrollBar
    Friend WithEvents hscRows As System.Windows.Forms.HScrollBar
    Friend WithEvents hscColumns As System.Windows.Forms.HScrollBar
    Friend WithEvents hscError As System.Windows.Forms.HScrollBar
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents lblHeightVal As System.Windows.Forms.Label
    Friend WithEvents lblWidthVal As System.Windows.Forms.Label
    Friend WithEvents lblRowsVal As System.Windows.Forms.Label
    Friend WithEvents lblColumnsVal As System.Windows.Forms.Label
    Friend WithEvents lblErrorVal As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents lblRows As System.Windows.Forms.Label
    Friend WithEvents lblColumns As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents lblStyle As System.Windows.Forms.Label
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents buttonPrint2D As System.Windows.Forms.Button
    Friend WithEvents buttonWrite2D As System.Windows.Forms.Button
    Friend WithEvents buttonCreate2D As System.Windows.Forms.Button
    Friend WithEvents PicImagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents picimagxpress1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBarcodeWriting))
        Me.CloseForm = New System.Windows.Forms.Button
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.picimagxpress1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.hScrollBarUPCFont = New System.Windows.Forms.HScrollBar
        Me.buttonWrite = New System.Windows.Forms.Button
        Me.labelUPCGap = New System.Windows.Forms.Label
        Me.labelValueFont = New System.Windows.Forms.Label
        Me.labelMinBarVal = New System.Windows.Forms.Label
        Me.hScrollBarValueGap = New System.Windows.Forms.HScrollBar
        Me.hScrollBarUPCNotch = New System.Windows.Forms.HScrollBar
        Me.labelUPCFont = New System.Windows.Forms.Label
        Me.labelUPCNotch = New System.Windows.Forms.Label
        Me.labelValueGap = New System.Windows.Forms.Label
        Me.labelBCHeight = New System.Windows.Forms.Label
        Me.labelMinBar = New System.Windows.Forms.Label
        Me.labelBCValue = New System.Windows.Forms.Label
        Me.comboBoxStyle = New System.Windows.Forms.ComboBox
        Me.labelStyle = New System.Windows.Forms.Label
        Me.labelHeightVal = New System.Windows.Forms.Label
        Me.labelUPCFontVal = New System.Windows.Forms.Label
        Me.labelValueFontVal = New System.Windows.Forms.Label
        Me.labelUPCGapVal = New System.Windows.Forms.Label
        Me.labelValueGapVal = New System.Windows.Forms.Label
        Me.labelUPCNotchVal = New System.Windows.Forms.Label
        Me.textBoxBCValue = New System.Windows.Forms.TextBox
        Me.hScrollBarUPCGap = New System.Windows.Forms.HScrollBar
        Me.hScrollBarValueFont = New System.Windows.Forms.HScrollBar
        Me.buttonPrint = New System.Windows.Forms.Button
        Me.buttonCreate = New System.Windows.Forms.Button
        Me.labelResult = New System.Windows.Forms.Label
        Me.labelInfo = New System.Windows.Forms.Label
        Me.labelStatus = New System.Windows.Forms.Label
        Me.hScrollBarBCHeight = New System.Windows.Forms.HScrollBar
        Me.hScrollBarMinBar = New System.Windows.Forms.HScrollBar
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.PicImagXpress2 = New PegasusImaging.WinForms.ImagXpress8.ImageXView
        Me.cmbBarcodeType = New System.Windows.Forms.ComboBox
        Me.cmbColumnsAndRows = New System.Windows.Forms.ComboBox
        Me.hscHeight = New System.Windows.Forms.HScrollBar
        Me.hscWidth = New System.Windows.Forms.HScrollBar
        Me.hscRows = New System.Windows.Forms.HScrollBar
        Me.hscColumns = New System.Windows.Forms.HScrollBar
        Me.hscError = New System.Windows.Forms.HScrollBar
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.lblHeightVal = New System.Windows.Forms.Label
        Me.lblWidthVal = New System.Windows.Forms.Label
        Me.lblRowsVal = New System.Windows.Forms.Label
        Me.lblColumnsVal = New System.Windows.Forms.Label
        Me.lblErrorVal = New System.Windows.Forms.Label
        Me.lblHeight = New System.Windows.Forms.Label
        Me.lblWidth = New System.Windows.Forms.Label
        Me.lblRows = New System.Windows.Forms.Label
        Me.lblColumns = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.lblValue = New System.Windows.Forms.Label
        Me.lblStyle = New System.Windows.Forms.Label
        Me.lblInfo = New System.Windows.Forms.Label
        Me.lblResult = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.buttonPrint2D = New System.Windows.Forms.Button
        Me.buttonWrite2D = New System.Windows.Forms.Button
        Me.buttonCreate2D = New System.Windows.Forms.Button
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CloseForm
        '
        Me.CloseForm.Location = New System.Drawing.Point(8, 560)
        Me.CloseForm.Name = "CloseForm"
        Me.CloseForm.Size = New System.Drawing.Size(104, 24)
        Me.CloseForm.TabIndex = 1
        Me.CloseForm.Text = "Close"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.picimagxpress1)
        Me.TabPage1.Controls.Add(Me.hScrollBarUPCFont)
        Me.TabPage1.Controls.Add(Me.buttonWrite)
        Me.TabPage1.Controls.Add(Me.labelUPCGap)
        Me.TabPage1.Controls.Add(Me.labelValueFont)
        Me.TabPage1.Controls.Add(Me.labelMinBarVal)
        Me.TabPage1.Controls.Add(Me.hScrollBarValueGap)
        Me.TabPage1.Controls.Add(Me.hScrollBarUPCNotch)
        Me.TabPage1.Controls.Add(Me.labelUPCFont)
        Me.TabPage1.Controls.Add(Me.labelUPCNotch)
        Me.TabPage1.Controls.Add(Me.labelValueGap)
        Me.TabPage1.Controls.Add(Me.labelBCHeight)
        Me.TabPage1.Controls.Add(Me.labelMinBar)
        Me.TabPage1.Controls.Add(Me.labelBCValue)
        Me.TabPage1.Controls.Add(Me.comboBoxStyle)
        Me.TabPage1.Controls.Add(Me.labelStyle)
        Me.TabPage1.Controls.Add(Me.labelHeightVal)
        Me.TabPage1.Controls.Add(Me.labelUPCFontVal)
        Me.TabPage1.Controls.Add(Me.labelValueFontVal)
        Me.TabPage1.Controls.Add(Me.labelUPCGapVal)
        Me.TabPage1.Controls.Add(Me.labelValueGapVal)
        Me.TabPage1.Controls.Add(Me.labelUPCNotchVal)
        Me.TabPage1.Controls.Add(Me.textBoxBCValue)
        Me.TabPage1.Controls.Add(Me.hScrollBarUPCGap)
        Me.TabPage1.Controls.Add(Me.hScrollBarValueFont)
        Me.TabPage1.Controls.Add(Me.buttonPrint)
        Me.TabPage1.Controls.Add(Me.buttonCreate)
        Me.TabPage1.Controls.Add(Me.labelResult)
        Me.TabPage1.Controls.Add(Me.labelInfo)
        Me.TabPage1.Controls.Add(Me.labelStatus)
        Me.TabPage1.Controls.Add(Me.hScrollBarBCHeight)
        Me.TabPage1.Controls.Add(Me.hScrollBarMinBar)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(800, 518)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "1D Barcode Writing"
        '
        'picimagxpress1
        '
        Me.picimagxpress1.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.picimagxpress1.AutoScroll = True
        Me.picimagxpress1.Location = New System.Drawing.Point(328, 264)
        Me.picimagxpress1.Name = "picimagxpress1"
        Me.picimagxpress1.Size = New System.Drawing.Size(464, 200)
        Me.picimagxpress1.TabIndex = 99
        '
        'hScrollBarUPCFont
        '
        Me.hScrollBarUPCFont.Location = New System.Drawing.Point(472, 216)
        Me.hScrollBarUPCFont.Minimum = 6
        Me.hScrollBarUPCFont.Name = "hScrollBarUPCFont"
        Me.hScrollBarUPCFont.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarUPCFont.TabIndex = 86
        Me.hScrollBarUPCFont.Value = 8
        '
        'buttonWrite
        '
        Me.buttonWrite.Location = New System.Drawing.Point(472, 16)
        Me.buttonWrite.Name = "buttonWrite"
        Me.buttonWrite.Size = New System.Drawing.Size(112, 24)
        Me.buttonWrite.TabIndex = 98
        Me.buttonWrite.Text = "Write to File"
        '
        'labelUPCGap
        '
        Me.labelUPCGap.Location = New System.Drawing.Point(360, 168)
        Me.labelUPCGap.Name = "labelUPCGap"
        Me.labelUPCGap.Size = New System.Drawing.Size(112, 16)
        Me.labelUPCGap.TabIndex = 81
        Me.labelUPCGap.Text = "UPC Value Gap:"
        '
        'labelValueFont
        '
        Me.labelValueFont.Location = New System.Drawing.Point(360, 192)
        Me.labelValueFont.Name = "labelValueFont"
        Me.labelValueFont.Size = New System.Drawing.Size(112, 16)
        Me.labelValueFont.TabIndex = 80
        Me.labelValueFont.Text = "Value Font Size:"
        '
        'labelMinBarVal
        '
        Me.labelMinBarVal.Location = New System.Drawing.Point(664, 96)
        Me.labelMinBarVal.Name = "labelMinBarVal"
        Me.labelMinBarVal.Size = New System.Drawing.Size(40, 16)
        Me.labelMinBarVal.TabIndex = 90
        '
        'hScrollBarValueGap
        '
        Me.hScrollBarValueGap.Location = New System.Drawing.Point(472, 144)
        Me.hScrollBarValueGap.Maximum = 200
        Me.hScrollBarValueGap.Minimum = -200
        Me.hScrollBarValueGap.Name = "hScrollBarValueGap"
        Me.hScrollBarValueGap.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarValueGap.TabIndex = 85
        '
        'hScrollBarUPCNotch
        '
        Me.hScrollBarUPCNotch.Location = New System.Drawing.Point(472, 120)
        Me.hScrollBarUPCNotch.Maximum = 50
        Me.hScrollBarUPCNotch.Name = "hScrollBarUPCNotch"
        Me.hScrollBarUPCNotch.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarUPCNotch.TabIndex = 82
        '
        'labelUPCFont
        '
        Me.labelUPCFont.Location = New System.Drawing.Point(360, 216)
        Me.labelUPCFont.Name = "labelUPCFont"
        Me.labelUPCFont.Size = New System.Drawing.Size(117, 16)
        Me.labelUPCFont.TabIndex = 79
        Me.labelUPCFont.Text = "UPC Small Font Size:"
        '
        'labelUPCNotch
        '
        Me.labelUPCNotch.Location = New System.Drawing.Point(360, 120)
        Me.labelUPCNotch.Name = "labelUPCNotch"
        Me.labelUPCNotch.Size = New System.Drawing.Size(112, 16)
        Me.labelUPCNotch.TabIndex = 78
        Me.labelUPCNotch.Text = "UPC Notch %:"
        '
        'labelValueGap
        '
        Me.labelValueGap.Location = New System.Drawing.Point(360, 144)
        Me.labelValueGap.Name = "labelValueGap"
        Me.labelValueGap.Size = New System.Drawing.Size(112, 16)
        Me.labelValueGap.TabIndex = 77
        Me.labelValueGap.Text = "Value Gap:"
        '
        'labelBCHeight
        '
        Me.labelBCHeight.Location = New System.Drawing.Point(360, 72)
        Me.labelBCHeight.Name = "labelBCHeight"
        Me.labelBCHeight.Size = New System.Drawing.Size(112, 16)
        Me.labelBCHeight.TabIndex = 76
        Me.labelBCHeight.Text = "Barcode Height:"
        '
        'labelMinBar
        '
        Me.labelMinBar.Location = New System.Drawing.Point(360, 96)
        Me.labelMinBar.Name = "labelMinBar"
        Me.labelMinBar.Size = New System.Drawing.Size(112, 16)
        Me.labelMinBar.TabIndex = 75
        Me.labelMinBar.Text = "Min. Bar Width:"
        '
        'labelBCValue
        '
        Me.labelBCValue.Location = New System.Drawing.Point(360, 48)
        Me.labelBCValue.Name = "labelBCValue"
        Me.labelBCValue.Size = New System.Drawing.Size(96, 16)
        Me.labelBCValue.TabIndex = 74
        Me.labelBCValue.Text = "Barcode Value:"
        '
        'comboBoxStyle
        '
        Me.comboBoxStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboBoxStyle.ItemHeight = 13
        Me.comboBoxStyle.Location = New System.Drawing.Point(104, 16)
        Me.comboBoxStyle.Name = "comboBoxStyle"
        Me.comboBoxStyle.Size = New System.Drawing.Size(216, 21)
        Me.comboBoxStyle.TabIndex = 69
        '
        'labelStyle
        '
        Me.labelStyle.Location = New System.Drawing.Point(16, 16)
        Me.labelStyle.Name = "labelStyle"
        Me.labelStyle.Size = New System.Drawing.Size(80, 16)
        Me.labelStyle.TabIndex = 68
        Me.labelStyle.Text = "Barcode Style:"
        '
        'labelHeightVal
        '
        Me.labelHeightVal.Location = New System.Drawing.Point(664, 72)
        Me.labelHeightVal.Name = "labelHeightVal"
        Me.labelHeightVal.Size = New System.Drawing.Size(40, 16)
        Me.labelHeightVal.TabIndex = 96
        '
        'labelUPCFontVal
        '
        Me.labelUPCFontVal.Location = New System.Drawing.Point(664, 216)
        Me.labelUPCFontVal.Name = "labelUPCFontVal"
        Me.labelUPCFontVal.Size = New System.Drawing.Size(40, 16)
        Me.labelUPCFontVal.TabIndex = 95
        '
        'labelValueFontVal
        '
        Me.labelValueFontVal.Location = New System.Drawing.Point(664, 192)
        Me.labelValueFontVal.Name = "labelValueFontVal"
        Me.labelValueFontVal.Size = New System.Drawing.Size(40, 16)
        Me.labelValueFontVal.TabIndex = 94
        '
        'labelUPCGapVal
        '
        Me.labelUPCGapVal.Location = New System.Drawing.Point(664, 168)
        Me.labelUPCGapVal.Name = "labelUPCGapVal"
        Me.labelUPCGapVal.Size = New System.Drawing.Size(40, 16)
        Me.labelUPCGapVal.TabIndex = 93
        '
        'labelValueGapVal
        '
        Me.labelValueGapVal.Location = New System.Drawing.Point(664, 144)
        Me.labelValueGapVal.Name = "labelValueGapVal"
        Me.labelValueGapVal.Size = New System.Drawing.Size(40, 16)
        Me.labelValueGapVal.TabIndex = 92
        '
        'labelUPCNotchVal
        '
        Me.labelUPCNotchVal.Location = New System.Drawing.Point(664, 120)
        Me.labelUPCNotchVal.Name = "labelUPCNotchVal"
        Me.labelUPCNotchVal.Size = New System.Drawing.Size(40, 16)
        Me.labelUPCNotchVal.TabIndex = 91
        '
        'textBoxBCValue
        '
        Me.textBoxBCValue.Location = New System.Drawing.Point(472, 48)
        Me.textBoxBCValue.Name = "textBoxBCValue"
        Me.textBoxBCValue.Size = New System.Drawing.Size(184, 20)
        Me.textBoxBCValue.TabIndex = 89
        Me.textBoxBCValue.Text = "textBox1"
        '
        'hScrollBarUPCGap
        '
        Me.hScrollBarUPCGap.Location = New System.Drawing.Point(472, 168)
        Me.hScrollBarUPCGap.Maximum = 200
        Me.hScrollBarUPCGap.Minimum = -200
        Me.hScrollBarUPCGap.Name = "hScrollBarUPCGap"
        Me.hScrollBarUPCGap.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarUPCGap.TabIndex = 88
        '
        'hScrollBarValueFont
        '
        Me.hScrollBarValueFont.Location = New System.Drawing.Point(472, 192)
        Me.hScrollBarValueFont.Maximum = 150
        Me.hScrollBarValueFont.Minimum = 8
        Me.hScrollBarValueFont.Name = "hScrollBarValueFont"
        Me.hScrollBarValueFont.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarValueFont.TabIndex = 87
        Me.hScrollBarValueFont.Value = 12
        '
        'buttonPrint
        '
        Me.buttonPrint.Location = New System.Drawing.Point(600, 16)
        Me.buttonPrint.Name = "buttonPrint"
        Me.buttonPrint.Size = New System.Drawing.Size(112, 24)
        Me.buttonPrint.TabIndex = 73
        Me.buttonPrint.Text = "Print Barcode"
        '
        'buttonCreate
        '
        Me.buttonCreate.Location = New System.Drawing.Point(344, 16)
        Me.buttonCreate.Name = "buttonCreate"
        Me.buttonCreate.Size = New System.Drawing.Size(112, 24)
        Me.buttonCreate.TabIndex = 72
        Me.buttonCreate.Text = "Create Barcode"
        '
        'labelResult
        '
        Me.labelResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelResult.Location = New System.Drawing.Point(16, 144)
        Me.labelResult.Name = "labelResult"
        Me.labelResult.Size = New System.Drawing.Size(304, 320)
        Me.labelResult.TabIndex = 71
        Me.labelResult.Text = "label3"
        '
        'labelInfo
        '
        Me.labelInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelInfo.Location = New System.Drawing.Point(16, 48)
        Me.labelInfo.Name = "labelInfo"
        Me.labelInfo.Size = New System.Drawing.Size(304, 80)
        Me.labelInfo.TabIndex = 70
        Me.labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), an" & _
            "d the following symbols: space, minus (-), plus (+), period (.), dollar sign ($)" & _
            ", slash (/), and percent (%)."
        '
        'labelStatus
        '
        Me.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelStatus.Location = New System.Drawing.Point(16, 472)
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.Size = New System.Drawing.Size(768, 40)
        Me.labelStatus.TabIndex = 67
        '
        'hScrollBarBCHeight
        '
        Me.hScrollBarBCHeight.Location = New System.Drawing.Point(472, 72)
        Me.hScrollBarBCHeight.Maximum = 1200
        Me.hScrollBarBCHeight.Minimum = 100
        Me.hScrollBarBCHeight.Name = "hScrollBarBCHeight"
        Me.hScrollBarBCHeight.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarBCHeight.TabIndex = 84
        Me.hScrollBarBCHeight.Value = 300
        '
        'hScrollBarMinBar
        '
        Me.hScrollBarMinBar.Location = New System.Drawing.Point(472, 96)
        Me.hScrollBarMinBar.Maximum = 15
        Me.hScrollBarMinBar.Minimum = 2
        Me.hScrollBarMinBar.Name = "hScrollBarMinBar"
        Me.hScrollBarMinBar.Size = New System.Drawing.Size(184, 16)
        Me.hScrollBarMinBar.TabIndex = 83
        Me.hScrollBarMinBar.Value = 2
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.PicImagXpress2)
        Me.TabPage2.Controls.Add(Me.cmbBarcodeType)
        Me.TabPage2.Controls.Add(Me.cmbColumnsAndRows)
        Me.TabPage2.Controls.Add(Me.hscHeight)
        Me.TabPage2.Controls.Add(Me.hscWidth)
        Me.TabPage2.Controls.Add(Me.hscRows)
        Me.TabPage2.Controls.Add(Me.hscColumns)
        Me.TabPage2.Controls.Add(Me.hscError)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.lblHeightVal)
        Me.TabPage2.Controls.Add(Me.lblWidthVal)
        Me.TabPage2.Controls.Add(Me.lblRowsVal)
        Me.TabPage2.Controls.Add(Me.lblColumnsVal)
        Me.TabPage2.Controls.Add(Me.lblErrorVal)
        Me.TabPage2.Controls.Add(Me.lblHeight)
        Me.TabPage2.Controls.Add(Me.lblWidth)
        Me.TabPage2.Controls.Add(Me.lblRows)
        Me.TabPage2.Controls.Add(Me.lblColumns)
        Me.TabPage2.Controls.Add(Me.lblError)
        Me.TabPage2.Controls.Add(Me.lblValue)
        Me.TabPage2.Controls.Add(Me.lblStyle)
        Me.TabPage2.Controls.Add(Me.lblInfo)
        Me.TabPage2.Controls.Add(Me.lblResult)
        Me.TabPage2.Controls.Add(Me.lblStatus)
        Me.TabPage2.Controls.Add(Me.buttonPrint2D)
        Me.TabPage2.Controls.Add(Me.buttonWrite2D)
        Me.TabPage2.Controls.Add(Me.buttonCreate2D)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(800, 518)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "2D Barcode Writing"
        '
        'PicImagXpress2
        '
        Me.PicImagXpress2.AutoResize = PegasusImaging.WinForms.ImagXpress8.AutoResizeType.BestFit
        Me.PicImagXpress2.AutoScroll = True
        Me.PicImagXpress2.Location = New System.Drawing.Point(336, 264)
        Me.PicImagXpress2.Name = "PicImagXpress2"
        Me.PicImagXpress2.Size = New System.Drawing.Size(456, 200)
        Me.PicImagXpress2.TabIndex = 57
        '
        'cmbBarcodeType
        '
        Me.cmbBarcodeType.ItemHeight = 13
        Me.cmbBarcodeType.Items.AddRange(New Object() {"PDF417", "DataMatrix"})
        Me.cmbBarcodeType.Location = New System.Drawing.Point(16, 32)
        Me.cmbBarcodeType.Name = "cmbBarcodeType"
        Me.cmbBarcodeType.Size = New System.Drawing.Size(312, 21)
        Me.cmbBarcodeType.TabIndex = 56
        Me.cmbBarcodeType.Text = "PDF417"
        '
        'cmbColumnsAndRows
        '
        Me.cmbColumnsAndRows.ItemHeight = 13
        Me.cmbColumnsAndRows.Location = New System.Drawing.Point(448, 152)
        Me.cmbColumnsAndRows.Name = "cmbColumnsAndRows"
        Me.cmbColumnsAndRows.Size = New System.Drawing.Size(192, 21)
        Me.cmbColumnsAndRows.TabIndex = 55
        Me.cmbColumnsAndRows.Text = "ComboBox1"
        Me.cmbColumnsAndRows.Visible = False
        '
        'hscHeight
        '
        Me.hscHeight.LargeChange = 1
        Me.hscHeight.Location = New System.Drawing.Point(448, 216)
        Me.hscHeight.Maximum = 5
        Me.hscHeight.Minimum = 3
        Me.hscHeight.Name = "hscHeight"
        Me.hscHeight.Size = New System.Drawing.Size(192, 16)
        Me.hscHeight.TabIndex = 54
        Me.hscHeight.Value = 4
        '
        'hscWidth
        '
        Me.hscWidth.LargeChange = 1
        Me.hscWidth.Location = New System.Drawing.Point(448, 192)
        Me.hscWidth.Maximum = 10
        Me.hscWidth.Minimum = 2
        Me.hscWidth.Name = "hscWidth"
        Me.hscWidth.Size = New System.Drawing.Size(192, 16)
        Me.hscWidth.TabIndex = 53
        Me.hscWidth.Value = 3
        '
        'hscRows
        '
        Me.hscRows.LargeChange = 1
        Me.hscRows.Location = New System.Drawing.Point(448, 144)
        Me.hscRows.Maximum = 90
        Me.hscRows.Minimum = 2
        Me.hscRows.Name = "hscRows"
        Me.hscRows.Size = New System.Drawing.Size(192, 16)
        Me.hscRows.TabIndex = 52
        Me.hscRows.Value = 2
        '
        'hscColumns
        '
        Me.hscColumns.LargeChange = 1
        Me.hscColumns.Location = New System.Drawing.Point(448, 168)
        Me.hscColumns.Maximum = 30
        Me.hscColumns.Name = "hscColumns"
        Me.hscColumns.Size = New System.Drawing.Size(192, 16)
        Me.hscColumns.TabIndex = 51
        '
        'hscError
        '
        Me.hscError.LargeChange = 1
        Me.hscError.Location = New System.Drawing.Point(448, 120)
        Me.hscError.Maximum = 8
        Me.hscError.Minimum = -1
        Me.hscError.Name = "hscError"
        Me.hscError.Size = New System.Drawing.Size(192, 16)
        Me.hscError.TabIndex = 50
        Me.hscError.Value = -1
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(448, 96)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(192, 20)
        Me.TextBox1.TabIndex = 49
        Me.TextBox1.Text = "PDF417"
        '
        'lblHeightVal
        '
        Me.lblHeightVal.Location = New System.Drawing.Point(648, 216)
        Me.lblHeightVal.Name = "lblHeightVal"
        Me.lblHeightVal.Size = New System.Drawing.Size(88, 16)
        Me.lblHeightVal.TabIndex = 48
        Me.lblHeightVal.Text = "4"
        '
        'lblWidthVal
        '
        Me.lblWidthVal.Location = New System.Drawing.Point(648, 192)
        Me.lblWidthVal.Name = "lblWidthVal"
        Me.lblWidthVal.Size = New System.Drawing.Size(88, 16)
        Me.lblWidthVal.TabIndex = 47
        Me.lblWidthVal.Text = "3"
        '
        'lblRowsVal
        '
        Me.lblRowsVal.Location = New System.Drawing.Point(648, 144)
        Me.lblRowsVal.Name = "lblRowsVal"
        Me.lblRowsVal.Size = New System.Drawing.Size(88, 16)
        Me.lblRowsVal.TabIndex = 46
        Me.lblRowsVal.Text = "-1"
        '
        'lblColumnsVal
        '
        Me.lblColumnsVal.Location = New System.Drawing.Point(648, 168)
        Me.lblColumnsVal.Name = "lblColumnsVal"
        Me.lblColumnsVal.Size = New System.Drawing.Size(88, 16)
        Me.lblColumnsVal.TabIndex = 45
        Me.lblColumnsVal.Text = "-1"
        '
        'lblErrorVal
        '
        Me.lblErrorVal.Location = New System.Drawing.Point(648, 120)
        Me.lblErrorVal.Name = "lblErrorVal"
        Me.lblErrorVal.Size = New System.Drawing.Size(80, 16)
        Me.lblErrorVal.TabIndex = 44
        Me.lblErrorVal.Text = "-1"
        '
        'lblHeight
        '
        Me.lblHeight.Location = New System.Drawing.Point(352, 216)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(88, 16)
        Me.lblHeight.TabIndex = 43
        Me.lblHeight.Text = "Height:"
        '
        'lblWidth
        '
        Me.lblWidth.Location = New System.Drawing.Point(352, 192)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(88, 16)
        Me.lblWidth.TabIndex = 42
        Me.lblWidth.Text = "Width:"
        '
        'lblRows
        '
        Me.lblRows.Location = New System.Drawing.Point(352, 144)
        Me.lblRows.Name = "lblRows"
        Me.lblRows.Size = New System.Drawing.Size(88, 16)
        Me.lblRows.TabIndex = 41
        Me.lblRows.Text = "Rows:"
        '
        'lblColumns
        '
        Me.lblColumns.Location = New System.Drawing.Point(352, 168)
        Me.lblColumns.Name = "lblColumns"
        Me.lblColumns.Size = New System.Drawing.Size(88, 16)
        Me.lblColumns.TabIndex = 40
        Me.lblColumns.Text = "Columns:"
        '
        'lblError
        '
        Me.lblError.Location = New System.Drawing.Point(352, 120)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(88, 16)
        Me.lblError.TabIndex = 39
        Me.lblError.Text = "Error Correction:"
        '
        'lblValue
        '
        Me.lblValue.Location = New System.Drawing.Point(352, 96)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(88, 16)
        Me.lblValue.TabIndex = 38
        Me.lblValue.Text = "Barcode Value:"
        '
        'lblStyle
        '
        Me.lblStyle.Location = New System.Drawing.Point(8, 8)
        Me.lblStyle.Name = "lblStyle"
        Me.lblStyle.Size = New System.Drawing.Size(144, 16)
        Me.lblStyle.TabIndex = 37
        Me.lblStyle.Text = "Barcode Style:"
        '
        'lblInfo
        '
        Me.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblInfo.Location = New System.Drawing.Point(16, 64)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(312, 80)
        Me.lblInfo.TabIndex = 35
        Me.lblInfo.Text = "PDF417 - Supports ASCII and binary characters."
        '
        'lblResult
        '
        Me.lblResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblResult.Location = New System.Drawing.Point(16, 152)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(312, 312)
        Me.lblResult.TabIndex = 34
        '
        'lblStatus
        '
        Me.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStatus.Location = New System.Drawing.Point(16, 472)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(768, 40)
        Me.lblStatus.TabIndex = 33
        '
        'buttonPrint2D
        '
        Me.buttonPrint2D.Location = New System.Drawing.Point(656, 40)
        Me.buttonPrint2D.Name = "buttonPrint2D"
        Me.buttonPrint2D.Size = New System.Drawing.Size(128, 32)
        Me.buttonPrint2D.TabIndex = 32
        Me.buttonPrint2D.Text = "Print Barcode"
        '
        'buttonWrite2D
        '
        Me.buttonWrite2D.Location = New System.Drawing.Point(504, 40)
        Me.buttonWrite2D.Name = "buttonWrite2D"
        Me.buttonWrite2D.Size = New System.Drawing.Size(128, 32)
        Me.buttonWrite2D.TabIndex = 31
        Me.buttonWrite2D.Text = "Write To File"
        '
        'buttonCreate2D
        '
        Me.buttonCreate2D.Location = New System.Drawing.Point(352, 40)
        Me.buttonCreate2D.Name = "buttonCreate2D"
        Me.buttonCreate2D.Size = New System.Drawing.Size(128, 32)
        Me.buttonCreate2D.TabIndex = 30
        Me.buttonCreate2D.Text = "Create Barcode"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.ItemSize = New System.Drawing.Size(105, 18)
        Me.TabControl1.Location = New System.Drawing.Point(8, 8)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(808, 544)
        Me.TabControl1.TabIndex = 0
        '
        'PrintDocument1
        '
        '
        'FormBarcodeWriting
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(832, 589)
        Me.Controls.Add(Me.CloseForm)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FormBarcodeWriting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FormBarcodeWriting"
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub comboBoxStyle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboBoxStyle.SelectedIndexChanged

        Set1DStyle()

        labelResult.Text = ""
        picimagxpress1.Image = Nothing
        buttonPrint.Enabled = False
        buttonWrite.Enabled = False
    End Sub

    Private Sub buttonCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCreate.Click
        barcodeXpress.writer.BarcodeValue = textBoxBCValue.Text
        labelStatus.Text = ""

        Try
            picimagxpress1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New IntPtr(barcodeXpress.writer.Create()))

            ' read the barcode
            Dim currentArea As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
            barcodeXpress.reader.Area = currentArea
            results = barcodeXpress.reader.Analyze(picimagxpress1.Image.ToHdib(False).ToInt32())

            If (results.Length > 0) Then
                Dim strResult As String = results.Length & " barcode(s) found." & vbNewLine & vbNewLine
                Dim i As Integer
                For i = 0 To results.Length - 1 Step 1
                    Dim curResult As PegasusImaging.WinForms.BarcodeXpress5.Result = CType(results.GetValue(i), PegasusImaging.WinForms.BarcodeXpress5.Result)
                    strResult = strResult & "Symbol #" & i & vbNewLine & "Type: " & curResult.BarcodeName & vbNewLine
                    strResult += "X - " + curResult.Area.X.ToString() + " pixels" + vbNewLine + "Y - " + curResult.Area.Y.ToString() + " pixels" + vbNewLine
                    strResult += "W - " + curResult.Area.Width.ToString() + " pixels" + vbNewLine + "H - " + curResult.Area.Height.ToString() + " pixels" + vbNewLine
                    strResult += "Value:" + vbNewLine + curResult.BarcodeValue + vbNewLine + vbNewLine
                    If curResult.ValidCheckSum Then
                        strResult += "Checksum OK"
                    End If
                Next

                strResult += vbNewLine
                labelResult.Text = strResult
                labelStatus.Text = "Barcode detection was successful."
                buttonPrint.Enabled = True
                buttonWrite.Enabled = True
            Else
                labelStatus.Text = "Could not detect any barcodes."
                buttonPrint.Enabled = False
                buttonWrite.Enabled = False
            End If
        Catch ex As PegasusImaging.WinForms.BarcodeXpress5.BarcodeException
            Dim strResult As String
            strResult = "Error Creating Barcode: "
            strResult += ex.Message
            labelResult.Text = strResult
            labelStatus.Text = strResult

        End Try
    End Sub

    Private Sub buttonWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonWrite.Click
        Dim strMsg As String
        'Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
        'Dim strImageFile As String = System.IO.Path.Combine(strCurrentDir, "..\barcode.TIF")
        Dim so As PegasusImaging.WinForms.ImagXpress8.SaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()

        Try
            so.Format = ImageXFormat.Tiff
            so.Tiff.Compression = Compression.Group4

            picimagxpress1.Image.Save(Application.StartupPath + "\barcode.tif", so)


            strMsg = "Image saved as " + Application.StartupPath + "\barcode.tif"
            labelStatus.Text = strMsg

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            labelStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub buttonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrint.Click
        Try
            PrintDocument1.Print()
            labelStatus.Text = "Barcode printed."
        Catch ex As Exception
            labelStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub OnPrint2DPage(ByVal sender As System.Object, ByVal args As PrintPageEventArgs)

        PicImagXpress2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(Application.StartupPath + "\barcode.tif")

        Dim proc As PegasusImaging.WinForms.ImagXpress8.Processor = New PegasusImaging.WinForms.ImagXpress8.Processor()
        Dim img2 As PegasusImaging.WinForms.ImagXpress8.ImageX

        img2 = PicImagXpress2.Image

        proc.Image = PicImagXpress2.Image.Copy()
        proc.ColorDepth(24, 0, 0)
        PicImagXpress2.Image = proc.Image

        Dim g As System.Drawing.Graphics = PicImagXpress2.Image.GetGraphics()
        Dim img As System.Drawing.Image = CType(PicImagXpress2.Image.ToBitmap(False), System.Drawing.Image)
        args.Graphics.PageUnit = GraphicsUnit.Inch


        args.Graphics.DrawImage(img, CType((8.5 - img.Width / img.HorizontalResolution) / 2, Single), CType((11 - img.Height / img.VerticalResolution) / 2, Single), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution)

        PicImagXpress2.Image.ReleaseGraphics()
        PicImagXpress2.Image = img2

    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        picimagxpress1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(Application.StartupPath + "\barcode.tif")

        Dim proc As PegasusImaging.WinForms.ImagXpress8.Processor = New PegasusImaging.WinForms.ImagXpress8.Processor()

        proc.Image = picimagxpress1.Image.Copy()
        proc.ColorDepth(24, 0, 0)
        picimagxpress1.Image = proc.Image

        Dim g As System.Drawing.Graphics = picimagxpress1.Image.GetGraphics()
        Dim img As System.Drawing.Image = CType(picimagxpress1.Image.ToBitmap(False), System.Drawing.Image)
        e.Graphics.PageUnit = GraphicsUnit.Inch

        e.Graphics.DrawImage(img, CType((8.5 - img.Width / img.HorizontalResolution) / 2, Single), CType((11 - img.Height / img.VerticalResolution) / 2, Single), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution)

        picimagxpress1.Image.ReleaseGraphics()

    End Sub

    Private Sub FormBarcodeWriting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        comboBoxStyle.Items.Add("Code 39 Narrow")
        comboBoxStyle.Items.Add("Code 39 Extended Narrow")
        comboBoxStyle.Items.Add("Code 39 Wide")
        comboBoxStyle.Items.Add("Code 39 Extended Wide")
        comboBoxStyle.Items.Add("Industrial 2 of 5")
        comboBoxStyle.Items.Add("Interleaved 2 of 5")
        comboBoxStyle.Items.Add("Codabar Rationalized")
        comboBoxStyle.Items.Add("Code 128A")
        comboBoxStyle.Items.Add("Code 128B")
        comboBoxStyle.Items.Add("Code 128C")
        comboBoxStyle.Items.Add("Code 128")
        comboBoxStyle.Items.Add("UPC-A")
        comboBoxStyle.Items.Add("UPC-E")
        comboBoxStyle.Items.Add("EAN-8")
        comboBoxStyle.Items.Add("EAN-13")
        comboBoxStyle.Items.Add("EAN-128")
        comboBoxStyle.Items.Add("Code 93")
        comboBoxStyle.Items.Add("Code 93 Extended")
        comboBoxStyle.Items.Add("Patch Code")
        comboBoxStyle.SelectedIndex = 0

        hScrollBarBCHeight.Value = barcodeXpress.writer.MinimumBarHeight
        hScrollBarMinBar.Value = barcodeXpress.writer.MinimumBarWidth
        hScrollBarUPCNotch.Value = barcodeXpress.writer.TextNotchPercent
        hScrollBarValueGap.Value = barcodeXpress.writer.VerticalTextGap
        hScrollBarUPCGap.Value = barcodeXpress.writer.HorizontalTextGap
        hScrollBarValueFont.Value = CInt(barcodeXpress.writer.TextFont.Size)
        hScrollBarUPCFont.Value = CInt(barcodeXpress.writer.OutsideTextFont.Size)

        'Setup the values of the Data Matrix Combo Box
        cmbColumnsAndRows.Items.Add("Auto")
        cmbColumnsAndRows.Items.Item(0) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.AutoRowsAndColumns

        cmbColumnsAndRows.Items.Add("8 x 18")
        cmbColumnsAndRows.Items.Item(1) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows8Columns18

        cmbColumnsAndRows.Items.Add("8 x 32")
        cmbColumnsAndRows.Items.Item(2) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows8Columns32

        cmbColumnsAndRows.Items.Add("10 x 10")
        cmbColumnsAndRows.Items.Item(3) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows10Columns10

        cmbColumnsAndRows.Items.Add("12 x 12")
        cmbColumnsAndRows.Items.Item(4) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns12

        cmbColumnsAndRows.Items.Add("12 x 26")
        cmbColumnsAndRows.Items.Item(5) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns26

        cmbColumnsAndRows.Items.Add("12 x 36")
        cmbColumnsAndRows.Items.Item(6) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows12Columns36

        cmbColumnsAndRows.Items.Add("14 x 14")
        cmbColumnsAndRows.Items.Item(7) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows14Columns14

        cmbColumnsAndRows.Items.Add("16 x 16")
        cmbColumnsAndRows.Items.Item(8) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns16

        cmbColumnsAndRows.Items.Add("16 x 36")
        cmbColumnsAndRows.Items.Item(9) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns36

        cmbColumnsAndRows.Items.Add("16 x 48")
        cmbColumnsAndRows.Items.Item(10) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows16Columns48

        cmbColumnsAndRows.Items.Add("18 x 18")
        cmbColumnsAndRows.Items.Item(11) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows18Columns18

        cmbColumnsAndRows.Items.Add("20 x 20")
        cmbColumnsAndRows.Items.Item(12) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows20Columns20

        cmbColumnsAndRows.Items.Add("22 x 22")
        cmbColumnsAndRows.Items.Item(13) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows22Columns22

        cmbColumnsAndRows.Items.Add("24 x 24")
        cmbColumnsAndRows.Items.Item(14) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows24Columns24

        cmbColumnsAndRows.Items.Add("26 x 26")
        cmbColumnsAndRows.Items.Item(15) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows26Columns26

        cmbColumnsAndRows.Items.Add("32 x 32")
        cmbColumnsAndRows.Items.Item(16) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows32Columns32

        cmbColumnsAndRows.Items.Add("36 x 36")
        cmbColumnsAndRows.Items.Item(17) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows36Columns36

        cmbColumnsAndRows.Items.Add("40 x 40")
        cmbColumnsAndRows.Items.Item(18) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows40Columns40

        cmbColumnsAndRows.Items.Add("44 x 44")
        cmbColumnsAndRows.Items.Item(19) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows44Columns44

        cmbColumnsAndRows.Items.Add("48 x 48")
        cmbColumnsAndRows.Items.Item(20) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows48Columns48

        cmbColumnsAndRows.Items.Add("52 x 52")
        cmbColumnsAndRows.Items.Item(21) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows52Columns52

        cmbColumnsAndRows.Items.Add("64 x 64")
        cmbColumnsAndRows.Items.Item(22) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows64Columns64

        cmbColumnsAndRows.Items.Add("72 x 72")
        cmbColumnsAndRows.Items.Item(23) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows72Columns72

        cmbColumnsAndRows.Items.Add("80 x 80")
        cmbColumnsAndRows.Items.Item(24) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows80Columns80

        cmbColumnsAndRows.Items.Add("88 x 88")
        cmbColumnsAndRows.Items.Item(25) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows88Columns88

        cmbColumnsAndRows.Items.Add("96 x 96")
        cmbColumnsAndRows.Items.Item(26) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows96Columns96

        cmbColumnsAndRows.Items.Add("104 x 104")
        cmbColumnsAndRows.Items.Item(27) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows104Columns104

        cmbColumnsAndRows.Items.Add("120 x 120")
        cmbColumnsAndRows.Items.Item(28) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows120Columns120

        cmbColumnsAndRows.Items.Add("132 x 132")
        cmbColumnsAndRows.Items.Item(29) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows132Columns132

        cmbColumnsAndRows.Items.Add("144 x 144")
        cmbColumnsAndRows.Items.Item(30) = PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes.Rows144Columns144

        cmbColumnsAndRows.SelectedIndex = 0

        lblColumnsVal.Text = "-1"
        lblRowsVal.Text = "-1"

        'Set the Barcode type combo box
        cmbBarcodeType.SelectedIndex = 0

    End Sub

    Private Sub hScrollBarBCHeight_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarBCHeight.ValueChanged
        labelHeightVal.Text = hScrollBarBCHeight.Value.ToString()
        barcodeXpress.writer.MinimumBarHeight = hScrollBarBCHeight.Value
    End Sub

    Private Sub hScrollBarMinBar_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarMinBar.ValueChanged
        labelMinBarVal.Text = hScrollBarMinBar.Value.ToString()
        barcodeXpress.writer.MinimumBarWidth = CShort(hScrollBarMinBar.Value)
    End Sub

    Private Sub hScrollBarValueGap_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarValueGap.ValueChanged
        labelValueGapVal.Text = hScrollBarValueGap.Value.ToString()
        barcodeXpress.writer.VerticalTextGap = hScrollBarValueGap.Value
    End Sub

    Private Sub hScrollBarUPCNotch_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarUPCNotch.ValueChanged
        labelUPCNotchVal.Text = hScrollBarUPCNotch.Value.ToString()
        barcodeXpress.writer.TextNotchPercent = hScrollBarUPCNotch.Value
    End Sub

    Private Sub hScrollBarUPCGap_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarUPCGap.ValueChanged
        labelUPCGapVal.Text = hScrollBarUPCGap.Value.ToString()
        barcodeXpress.writer.HorizontalTextGap = hScrollBarUPCGap.Value
    End Sub

    Private Sub hScrollBarValueFont_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarValueFont.ValueChanged
        labelValueFontVal.Text = hScrollBarValueFont.Value.ToString()
        Dim Font As System.Drawing.Font = barcodeXpress.writer.TextFont
        barcodeXpress.writer.TextFont = New System.Drawing.Font(Font.FontFamily.GetName(0), hScrollBarValueFont.Value)
    End Sub

    Private Sub hScrollBarUPCFont_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hScrollBarUPCFont.ValueChanged
        labelUPCFontVal.Text = hScrollBarUPCFont.Value.ToString()
        Dim Font As System.Drawing.Font = barcodeXpress.writer.OutsideTextFont
        barcodeXpress.writer.OutsideTextFont = New System.Drawing.Font(Font.FontFamily.GetName(0), hScrollBarUPCFont.Value)
    End Sub

    Private Sub CloseForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseForm.Click
        Me.Close()
    End Sub

    Private Sub cmbBarcodeType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBarcodeType.SelectedIndexChanged
        If (cmbBarcodeType.SelectedIndex = 0) Then
            SetPDF417RowsAndColumns()
            lblInfo.Text = "PDF417 - Supports ASCII and binary characters."
            TextBox1.Text = "PDF417"
            lblHeightVal.Visible = True
            hscHeight.Visible = True
            lblHeight.Visible = True
            lblError.Visible = True
            lblErrorVal.Visible = True
            hscError.Visible = True
        Else
            SetDataMatrixRowsAndColumns()
            lblInfo.Text = "DataMatrix - Supports ASCII and binary characters."
            TextBox1.Text = "DataMatrix"
            lblHeightVal.Visible = False
            hscHeight.Visible = False
            lblHeight.Visible = False
            lblError.Visible = False
            lblErrorVal.Visible = False
            hscError.Visible = False
        End If

        lblResult.Text = ""
        PicImagXpress2.Image = Nothing
        buttonPrint2D.Enabled = False
        buttonWrite2D.Enabled = False
    End Sub

    Private Sub SetPDF417RowsAndColumns()

        cmbColumnsAndRows.Visible = False
        hscColumns.Visible = True
        hscRows.Visible = True
        If hscColumns.Value < 1 Then
            lblColumnsVal.Text = "-1"
        Else
            lblColumnsVal.Text = hscColumns.Value.ToString
        End If
        If hscRows.Value < 3 Then
            lblRowsVal.Text = "-1"
        Else
            lblRowsVal.Text = hscRows.Value.ToString
        End If
        lblColumnsVal.Visible = True
        lblRowsVal.Visible = True

    End Sub
    Private Sub SetDataMatrixRowsAndColumns()

        cmbColumnsAndRows.Visible = True
        hscColumns.Visible = False
        hscRows.Visible = False
        lblColumnsVal.Visible = False
        lblRowsVal.Visible = False

    End Sub

    Private Sub buttonCreate2D_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCreate2D.Click
        lblStatus.Text = ""

        Try

            'Set the rows columns and Height
            If (cmbBarcodeType.SelectedIndex = 0) Then
                UpdatePDF()
            Else
                UpdateDM()
            End If

            ' read the barcode
            Dim currentArea As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
            barcodeXpress.reader.Area = currentArea
            results = barcodeXpress.reader.Analyze(PicImagXpress2.Image.ToHdib(False).ToInt32())

            If (results.Length > 0) Then
                Dim strResult As String = results.Length & " barcode(s) found." & vbNewLine & vbNewLine
                Dim i As Integer
                For i = 0 To results.Length - 1 Step 1

                    Dim curResult As PegasusImaging.WinForms.BarcodeXpress5.Result = CType(results.GetValue(i), PegasusImaging.WinForms.BarcodeXpress5.Result)
                    strResult = strResult & "Symbol #" & i & vbNewLine & "Type: " & curResult.BarcodeName & vbNewLine
                    strResult += "X - " + curResult.Area.X.ToString() + " pixels" + vbNewLine + "Y - " + curResult.Area.Y.ToString() + " pixels" + vbNewLine
                    strResult += "W - " + curResult.Area.Width.ToString() + " pixels" + vbNewLine + "H - " + curResult.Area.Height.ToString() + " pixels" + vbNewLine
                    strResult += "Value:" + vbNewLine + curResult.BarcodeValue + vbNewLine + vbNewLine
                    If curResult.ValidCheckSum Then
                        strResult += "Checksum OK"
                    End If

                Next

                strResult += vbNewLine
                lblResult.Text = strResult
                lblStatus.Text = "Barcode detection was successful."
                buttonPrint2D.Enabled = True
                buttonWrite2D.Enabled = True
            Else
                lblStatus.Text = "Could not detect any barcodes."
                buttonPrint2D.Enabled = False
                buttonWrite2D.Enabled = False
            End If
        Catch ex As PegasusImaging.WinForms.BarcodeXpress5.BarcodeException
            Dim strResult As String
            strResult = "Error Creating Barcode: "
            strResult += ex.Message
            lblResult.Text = strResult
            lblStatus.Text = strResult

        End Try

    End Sub

    Private Sub UpdatePDF()

        Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PDF417Barcode, 0)
        barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes

        Dim pdfWriter As New PegasusImaging.WinForms.BarcodeXpress5.WriterPDF417(barcodeXpress)
        If hscRows.Value = 2 Then
            pdfWriter.Rows = -1
        Else
            pdfWriter.Rows = hscRows.Value
        End If
        If hscColumns.Value < 1 Then
            pdfWriter.Columns = -1
        Else
            pdfWriter.Columns = hscColumns.Value
        End If
        pdfWriter.ErrorCorrectionLevel = CType(hscError.Value, PegasusImaging.WinForms.BarcodeXpress5.PDF417ErrorCorrectionLevel)
        pdfWriter.MinimumBarWidth = hscWidth.Value
        pdfWriter.MinimumBarHeight = hscHeight.Value
        pdfWriter.AutoSize = True
        pdfWriter.BarcodeValue = TextBox1.Text

        lblStatus.Text = "Making Barcode"
        lblResult.Text = ""
        Application.DoEvents()

        PicImagXpress2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New IntPtr(pdfWriter.Create()))

    End Sub

    Private Sub UpdateDM()

        Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
        currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.DataMatrixBarcode, 0)
        barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes

        Dim dmWriter As New PegasusImaging.WinForms.BarcodeXpress5.WriterDataMatrix(barcodeXpress)
        dmWriter.RowsAndColumns = CType(cmbColumnsAndRows.Items.Item(cmbColumnsAndRows.SelectedIndex), PegasusImaging.WinForms.BarcodeXpress5.DataMatrixSizes)
        dmWriter.MinimumBarWidth = hscWidth.Value
        dmWriter.MinimumBarHeight = hscHeight.Value
        dmWriter.BarcodeValue = TextBox1.Text
        dmWriter.AutoSize = True

        lblStatus.Text = "Making Barcode"
        lblResult.Text = ""
        Application.DoEvents()

        PicImagXpress2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromHdib(New IntPtr(dmWriter.Create()))

    End Sub

    Private Sub buttonWrite2D_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonWrite2D.Click
        Dim strMsg As String
        ' Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
        ' Dim strImageFile As String = System.IO.Path.Combine(strCurrentDir, "..\2Dbarcode.TIF")
        Dim so As PegasusImaging.WinForms.ImagXpress8.SaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
        Try

            so.Format = ImageXFormat.Tiff
            so.Tiff.Compression = Compression.Group4

            PicImagXpress2.Image.Save(Application.StartupPath + "\barcode.tif", so)
            strMsg = "Image saved as " + Application.StartupPath + "\barcode.tif"
            lblStatus.Text = strMsg

        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            lblStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub buttonPrint2D_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrint2D.Click
        Try
            ' Print the Image
            Dim doc As PrintDocument = New PrintDocument()
            AddHandler doc.PrintPage, AddressOf OnPrint2DPage
            doc.Print()
            lblStatus.Text = "Barcode printed."
        Catch ex As Exception
            lblStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub hscError_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscError.Scroll
        lblErrorVal.Text = hscError.Value.ToString()
    End Sub

    Private Sub hscColumns_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscColumns.Scroll

        If hscColumns.Value < 1 Then
            lblColumnsVal.Text = "-1"
        Else
            lblColumnsVal.Text = hscColumns.Value.ToString
        End If


    End Sub

    Private Sub hscRows_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscRows.Scroll

        If hscRows.Value < 3 Then
            lblRowsVal.Text = "-1"
        Else
            lblRowsVal.Text = hscRows.Value.ToString()
        End If

    End Sub

    Private Sub hscWidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscWidth.Scroll
        lblWidthVal.Text = hscWidth.Value.ToString
    End Sub

    Private Sub hscHeight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscHeight.Scroll
        lblHeightVal.Text = hscHeight.Value.ToString
    End Sub

    Private Sub TabControl1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.Click
        Set1DStyle()
    End Sub

    Private Sub Set1DStyle()
        Select Case (comboBoxStyle.SelectedIndex)
            Case 0
                labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code39Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 1
                labelInfo.Text = "Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39 Ext"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code39ExtendedBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 2
                labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code39Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 3
                labelInfo.Text = "Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 39 Ext"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code39ExtendedBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code39ExtendedBarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 4
                labelInfo.Text = "Industry 2 of 5 - Allowable characters - digits 0-9."
                textBoxBCValue.Text = "1234321"
                barcodeXpress.writer.BarcodeType = BarcodeType.Industry2of5Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Industry2of5Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 5
                labelInfo.Text = "Interleaved 2 of 5 - Allowable characters - digits 0-9."
                textBoxBCValue.Text = "1234321"
                barcodeXpress.writer.BarcodeType = BarcodeType.Interleaved2of5Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Interleaved2of5Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 6
                labelInfo.Text = "Codabar - Allowable characters - digits 0-9, six symbols including: minus (-), plus (+), period (.), dollar sign ($), slash (/), and colon (:), and the following start/stop characters A, B, C and D."
                textBoxBCValue.Text = "C5467D"
                barcodeXpress.writer.BarcodeType = BarcodeType.CodabarBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.CodabarBarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 7
                labelInfo.Text = "Code 128A - Allowable characters - Digits, uppercase characters, standard ASCII symbols."
                textBoxBCValue.Text = "CODE 128A"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 8
                labelInfo.Text = "Code 128B - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols."
                textBoxBCValue.Text = "CODE 128B"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 9
                labelInfo.Text = "Code 128C - Allowable characters - Digits 0-9."
                textBoxBCValue.Text = "987654321"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 10
                labelInfo.Text = "Code 128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols."
                textBoxBCValue.Text = "CODE 128"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code128Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 11
                labelInfo.Text = "UPC-A - Allowable characters - Digits 0-9 You must have 11 or 12 digits."
                textBoxBCValue.Text = "69792911003"
                barcodeXpress.writer.BarcodeType = BarcodeType.UPCABarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCABarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 12
                labelInfo.Text = "UPC-E - Allowable characters - Digits 0-9 You must have 6, 10, 11 or 12 digits."
                textBoxBCValue.Text = "654123"
                barcodeXpress.writer.BarcodeType = BarcodeType.UPCEBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.UPCEBarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 13
                labelInfo.Text = "EAN-8 - Allowable characters - Digits 0-9 You must have 7 or 8 digits."
                textBoxBCValue.Text = "1234567"
                barcodeXpress.writer.BarcodeType = BarcodeType.EAN8Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN8Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 14
                labelInfo.Text = "EAN-13 - Allowable characters - Digits 0-9 You must have 12 or 13 digits."
                textBoxBCValue.Text = "123456765432"
                barcodeXpress.writer.BarcodeType = BarcodeType.EAN13Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN13Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 15
                labelInfo.Text = "EAN-128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols."
                textBoxBCValue.Text = "EAN-128"
                barcodeXpress.writer.BarcodeType = BarcodeType.EAN128Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.EAN128Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 16
                labelInfo.Text = "Code 93 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 93"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code93Barcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93Barcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 17
                labelInfo.Text = "Code 93 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%)."
                textBoxBCValue.Text = "CODE 93 Ext"
                barcodeXpress.writer.BarcodeType = BarcodeType.Code93ExtendedBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.Code93ExtendedBarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select

            Case 18
                labelInfo.Text = "Patch code - (1)=Patch I (2)=Patch II (3)=Patch III (4)=Patch IV (5)=Patch VI (6)=Patch T."
                textBoxBCValue.Text = "1"
                If hScrollBarMinBar.Value < 5 Then
                    hScrollBarMinBar.Value = 5
                End If
                barcodeXpress.writer.BarcodeType = BarcodeType.PatchCodeBarcode
                Dim currentBarcodeTypes As System.Array = New PegasusImaging.WinForms.BarcodeXpress5.BarcodeType(1) {}
                currentBarcodeTypes.SetValue(PegasusImaging.WinForms.BarcodeXpress5.BarcodeType.PatchCodeBarcode, 0)
                barcodeXpress.reader.BarcodeTypes = currentBarcodeTypes
                Exit Select
        End Select
    End Sub

    Private Sub TabPage1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage1.Click

    End Sub


End Class

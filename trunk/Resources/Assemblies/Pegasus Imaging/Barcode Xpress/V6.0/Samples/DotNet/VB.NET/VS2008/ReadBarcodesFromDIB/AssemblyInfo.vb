'****************************************************************
'* Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Read Barcode from DIB")> 
<Assembly: AssemblyDescription("VB.NET Read Barcode from DIB Sample")> 
<Assembly: AssemblyCompany("Pegasus Imaging Corporation")> 
<Assembly: AssemblyProduct("BarcodeXpress")> 
<Assembly: AssemblyCopyright("Copyright � 2007. All rights reserved.")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("CE2BB35A-7B7B-4618-8453-E070F4E0F5A0")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 

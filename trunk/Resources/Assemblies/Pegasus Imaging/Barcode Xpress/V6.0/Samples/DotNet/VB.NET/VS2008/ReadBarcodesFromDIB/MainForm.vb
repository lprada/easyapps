'****************************************************************
'* Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Imports Accusoft.BarcodeXpressSdk
Imports System.Windows.Forms
Imports System.Runtime.InteropServices

Public Class MainForm
    Inherits System.Windows.Forms.Form

    <DllImport("kernel32.dll")> _
    Private Shared Function GlobalFree(ByVal hMem As IntPtr) As IntPtr
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()


        '***Must call the UnlockControl method to unlock the control
	'The unlock codes shown below are for formatting purposes only and will not work!
        'Accusoft.ImagXpressSdk.Licensing.UnlockControl(1234, 1234, 1234, 1234)


        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            If Not (ImageXView1.Image Is Nothing) Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (BarcodeXpress1 Is Nothing) Then
                BarcodeXpress1.Dispose()
                BarcodeXpress1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents OpenMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents readpost As System.Windows.Forms.RadioButton
    Friend WithEvents readdata As System.Windows.Forms.RadioButton
    Friend WithEvents readpdf As System.Windows.Forms.RadioButton
    Friend WithEvents readpatch As System.Windows.Forms.RadioButton
    Friend WithEvents read1d As System.Windows.Forms.RadioButton
    Friend WithEvents cmdread As System.Windows.Forms.Button
    Friend WithEvents cd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents MainMenu As System.Windows.Forms.MainMenu
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents RoyalRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents OneCodeRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents AusRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents QRRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BarcodeXpress1 As Accusoft.BarcodeXpressSdk.BarcodeXpress
    Friend WithEvents AztecRadioButton As System.Windows.Forms.RadioButton
    Friend WithEvents BarcodeXpressMenuItem As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.FileMenu = New System.Windows.Forms.MenuItem
        Me.OpenMenuItem = New System.Windows.Forms.MenuItem
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem
        Me.label2 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.groupBox1 = New System.Windows.Forms.GroupBox
        Me.RoyalRadioButton = New System.Windows.Forms.RadioButton
        Me.OneCodeRadioButton = New System.Windows.Forms.RadioButton
        Me.AusRadioButton = New System.Windows.Forms.RadioButton
        Me.QRRadioButton = New System.Windows.Forms.RadioButton
        Me.readpost = New System.Windows.Forms.RadioButton
        Me.readdata = New System.Windows.Forms.RadioButton
        Me.readpdf = New System.Windows.Forms.RadioButton
        Me.readpatch = New System.Windows.Forms.RadioButton
        Me.read1d = New System.Windows.Forms.RadioButton
        Me.cmdread = New System.Windows.Forms.Button
        Me.cd = New System.Windows.Forms.OpenFileDialog
        Me.MainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.BarcodeXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.Label4 = New System.Windows.Forms.Label
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView
        Me.BarcodeXpress1 = New Accusoft.BarcodeXpressSdk.BarcodeXpress
        Me.AztecRadioButton = New System.Windows.Forms.RadioButton
        Me.groupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.OpenMenuItem, Me.ExitMenuItem})
        Me.FileMenu.Text = "&File"
        '
        'OpenMenuItem
        '
        Me.OpenMenuItem.Index = 0
        Me.OpenMenuItem.Text = "Open"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 1
        Me.ExitMenuItem.Text = "Exit"
        '
        'label2
        '
        Me.label2.BackColor = System.Drawing.Color.Aqua
        Me.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(-102, -184)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(488, 79)
        Me.label2.TabIndex = 8
        Me.label2.Text = resources.GetString("label2.Text")
        '
        'label1
        '
        Me.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.ForeColor = System.Drawing.Color.Blue
        Me.label1.Location = New System.Drawing.Point(-102, -104)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(488, 55)
        Me.label1.TabIndex = 7
        Me.label1.Text = "Use the File | Open command to load a 1-bit monochrome image file containing one " & _
            "or more barcodes, then click the ""Detect Barcodes"" button."
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.AztecRadioButton)
        Me.groupBox1.Controls.Add(Me.RoyalRadioButton)
        Me.groupBox1.Controls.Add(Me.OneCodeRadioButton)
        Me.groupBox1.Controls.Add(Me.AusRadioButton)
        Me.groupBox1.Controls.Add(Me.QRRadioButton)
        Me.groupBox1.Controls.Add(Me.readpost)
        Me.groupBox1.Controls.Add(Me.readdata)
        Me.groupBox1.Controls.Add(Me.readpdf)
        Me.groupBox1.Controls.Add(Me.readpatch)
        Me.groupBox1.Controls.Add(Me.read1d)
        Me.groupBox1.Location = New System.Drawing.Point(12, 70)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(488, 96)
        Me.groupBox1.TabIndex = 6
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Barcode Types"
        '
        'RoyalRadioButton
        '
        Me.RoyalRadioButton.AutoSize = True
        Me.RoyalRadioButton.Location = New System.Drawing.Point(389, 32)
        Me.RoyalRadioButton.Name = "RoyalRadioButton"
        Me.RoyalRadioButton.Size = New System.Drawing.Size(76, 17)
        Me.RoyalRadioButton.TabIndex = 9
        Me.RoyalRadioButton.TabStop = True
        Me.RoyalRadioButton.Text = "Royal Post"
        Me.RoyalRadioButton.UseVisualStyleBackColor = True
        '
        'OneCodeRadioButton
        '
        Me.OneCodeRadioButton.AutoSize = True
        Me.OneCodeRadioButton.Location = New System.Drawing.Point(288, 63)
        Me.OneCodeRadioButton.Name = "OneCodeRadioButton"
        Me.OneCodeRadioButton.Size = New System.Drawing.Size(89, 17)
        Me.OneCodeRadioButton.TabIndex = 8
        Me.OneCodeRadioButton.TabStop = True
        Me.OneCodeRadioButton.Text = "IntelligentMail"
        Me.OneCodeRadioButton.UseVisualStyleBackColor = True
        '
        'AusRadioButton
        '
        Me.AusRadioButton.AutoSize = True
        Me.AusRadioButton.Location = New System.Drawing.Point(288, 32)
        Me.AusRadioButton.Name = "AusRadioButton"
        Me.AusRadioButton.Size = New System.Drawing.Size(95, 17)
        Me.AusRadioButton.TabIndex = 7
        Me.AusRadioButton.TabStop = True
        Me.AusRadioButton.Text = "Australian Post"
        Me.AusRadioButton.UseVisualStyleBackColor = True
        '
        'QRRadioButton
        '
        Me.QRRadioButton.AutoSize = True
        Me.QRRadioButton.Location = New System.Drawing.Point(212, 63)
        Me.QRRadioButton.Name = "QRRadioButton"
        Me.QRRadioButton.Size = New System.Drawing.Size(41, 17)
        Me.QRRadioButton.TabIndex = 6
        Me.QRRadioButton.TabStop = True
        Me.QRRadioButton.Text = "QR"
        Me.QRRadioButton.UseVisualStyleBackColor = True
        '
        'readpost
        '
        Me.readpost.Location = New System.Drawing.Point(212, 32)
        Me.readpost.Name = "readpost"
        Me.readpost.Size = New System.Drawing.Size(128, 16)
        Me.readpost.TabIndex = 5
        Me.readpost.Text = "PostNet"
        '
        'readdata
        '
        Me.readdata.Location = New System.Drawing.Point(126, 64)
        Me.readdata.Name = "readdata"
        Me.readdata.Size = New System.Drawing.Size(104, 16)
        Me.readdata.TabIndex = 4
        Me.readdata.Text = "Data Matrix"
        '
        'readpdf
        '
        Me.readpdf.Location = New System.Drawing.Point(126, 32)
        Me.readpdf.Name = "readpdf"
        Me.readpdf.Size = New System.Drawing.Size(120, 16)
        Me.readpdf.TabIndex = 2
        Me.readpdf.Text = "PDF-417"
        '
        'readpatch
        '
        Me.readpatch.Location = New System.Drawing.Point(16, 64)
        Me.readpatch.Name = "readpatch"
        Me.readpatch.Size = New System.Drawing.Size(104, 16)
        Me.readpatch.TabIndex = 1
        Me.readpatch.Text = "Patch Code"
        '
        'read1d
        '
        Me.read1d.Checked = True
        Me.read1d.Location = New System.Drawing.Point(16, 28)
        Me.read1d.Name = "read1d"
        Me.read1d.Size = New System.Drawing.Size(104, 24)
        Me.read1d.TabIndex = 0
        Me.read1d.TabStop = True
        Me.read1d.Text = "1D-Standard"
        '
        'cmdread
        '
        Me.cmdread.Location = New System.Drawing.Point(156, 544)
        Me.cmdread.Name = "cmdread"
        Me.cmdread.Size = New System.Drawing.Size(184, 32)
        Me.cmdread.TabIndex = 5
        Me.cmdread.Text = "Detect Barcodes"
        '
        'MainMenu
        '
        Me.MainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.MenuItem4})
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.BarcodeXpressMenuItem, Me.ImagXpressMenuItem})
        Me.MenuItem4.Text = "&About"
        '
        'BarcodeXpressMenuItem
        '
        Me.BarcodeXpressMenuItem.Index = 0
        Me.BarcodeXpressMenuItem.Text = "&BarcodeXpress"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 1
        Me.ImagXpressMenuItem.Text = "&ImagXpress"
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Blue
        Me.Label4.Location = New System.Drawing.Point(12, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(488, 48)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Use the File | Open command to load a 1-bit monochrome image file containing one " & _
            "or more barcodes, then click the ""Detect Barcodes"" button."
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(14, 182)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(486, 353)
        Me.ImageXView1.TabIndex = 11
        '
        'BarcodeXpress1
        '
        Me.BarcodeXpress1.ErrorLevel = Accusoft.BarcodeXpressSdk.ErrorLevelInfo.Production
        '
        'AztecRadioButton
        '
        Me.AztecRadioButton.AutoSize = True
        Me.AztecRadioButton.Location = New System.Drawing.Point(389, 63)
        Me.AztecRadioButton.Name = "AztecRadioButton"
        Me.AztecRadioButton.Size = New System.Drawing.Size(52, 17)
        Me.AztecRadioButton.TabIndex = 10
        Me.AztecRadioButton.TabStop = True
        Me.AztecRadioButton.Text = "Aztec"
        Me.AztecRadioButton.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(510, 588)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.groupBox1)
        Me.Controls.Add(Me.cmdread)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Read Barcode From DIB"
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Function SetBarcodeType() As System.Array
        Dim currentcount As Integer = 0
        If (read1d.Checked) Then
            currentcount = currentcount + 1
        End If
        If (readpatch.Checked) Then
            currentcount = currentcount + 1
        End If
        If (readpdf.Checked) Then
            currentcount = currentcount + 1
        End If
        If (readdata.Checked) Then
            currentcount = currentcount + 1
        End If
        If (readpost.Checked) Then
            currentcount = currentcount + 1
        End If
        If (QRRadioButton.Checked = True) Then
            currentcount = currentcount + 1
        End If
        If (RoyalRadioButton.Checked = True) Then
            currentcount = currentcount + 1
        End If
        If (AusRadioButton.Checked = True) Then
            currentcount = currentcount + 1
        End If
        If (OneCodeRadioButton.Checked = True) Then
            currentcount = currentcount + 1
        End If
        If (AztecRadioButton.Checked = True) Then
            currentcount = currentcount + 1
        End If

        Dim currentBarcodeTypes As System.Array = New BarcodeType(currentcount) {}

        currentcount = 0
        If (read1d.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.UnknownBarcode, currentcount)
            currentcount = currentcount + 1
        End If

        If (readpatch.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.PatchCodeBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (readpdf.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.PDF417Barcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (readdata.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.DataMatrixBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (readpost.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.PostNetBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (QRRadioButton.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.QRCodeBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (RoyalRadioButton.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.RoyalPost4StateBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (AusRadioButton.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.AustralianPost4StateBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (OneCodeRadioButton.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.IntelligentMailBarcode, currentcount)
            currentcount = currentcount + 1
        End If
        If (AztecRadioButton.Checked) Then
            currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.AztecBarcode, currentcount)
            currentcount = currentcount + 1
        End If

        Return currentBarcodeTypes

    End Function

    Private Sub cmdread_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdread.Click

        Try

            If ImageXView1.Image Is Nothing Then
                MessageBox.Show("No file loaded")
                Return
            End If

            Dim BarcodeTypes As System.Array
            Dim DIB As System.IntPtr

            BarcodeTypes = SetBarcodeType()
            Dim results As Accusoft.BarcodeXpressSdk.Result()

            BarcodeXpress1.reader.BarcodeTypes = BarcodeTypes
            Dim currentArea As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
            BarcodeXpress1.reader.Area = currentArea
            DIB = ImageXView1.Image.ToHdib(False)
            results = BarcodeXpress1.reader.Analyze(DIB)

            Me.Cursor = System.Windows.Forms.Cursors.Default

            If (results.Length > 0) Then
                Dim strResult As String
                strResult = ""

                Dim i As Integer
                For i = 0 To results.Length - 1 Step 1
                    Dim curResult As Accusoft.BarcodeXpressSdk.Result = CType(results.GetValue(i), Accusoft.BarcodeXpressSdk.Result)
                    strResult += "Symbol #" + i.ToString() + Chr(13) + "Type = " + curResult.BarcodeName + Chr(13)
                    strResult += "Value=" + curResult.BarcodeValue + Chr(13)
                Next

                strResult += Chr(13)
                MessageBox.Show(strResult)
            Else
                MessageBox.Show("No Barcodes Found")
            End If

            'Must free the DIB passed into the Analyze method
            GlobalFree(DIB)
        Catch ex As Accusoft.BarcodeXpressSdk.BarcodeException
            Dim strResult As String
            strResult = "Error: "
            strResult += ex.Message
            MessageBox.Show(strResult)

        End Try

    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        '***Must call the UnlockRuntime method to unlock the control
        'The unlock codes shown below are for formatting purposes only and will not work!
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        'BarcodeXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)


        BarcodeXpress1.Licensing.LicenseEdition = LicenseChoice.ProfessionalEdition

        Application.EnableVisualStyles()
    End Sub

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub OpenMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenMenuItem.Click
        cd.Filter = "TIFF Files (*.TIF)|*.TIF|All Files (*.*)|*.*"
        cd.Title = "Open a 1-Bit Black and White Image File"
        cd.InitialDirectory = Application.ExecutablePath & "\..\..\..\..\..\..\..\..\Common\Images"
        cd.ShowDialog(Me)

        Try
            If Not (cd.FileName = "") Then
                ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromFile(ImagXpress1, cd.FileName)
            End If
        Catch ex As Exception
            Dim strResult As String
            strResult = "Error Opening File.  ImagXpress Error: "
            strResult += ex.Message
            MessageBox.Show(strResult)
        End Try
    End Sub

    Private Sub BarcodeXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BarcodeXpressMenuItem.Click
        BarcodeXpress1.AboutBox()
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub
End Class

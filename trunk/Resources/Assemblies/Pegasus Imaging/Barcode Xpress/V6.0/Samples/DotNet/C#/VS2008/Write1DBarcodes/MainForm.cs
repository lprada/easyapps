/****************************************************************
 * Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using Accusoft.BarcodeXpressSdk;
using System.Drawing.Printing;

namespace CreateBC
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalFree(IntPtr hMem);

		private System.Windows.Forms.Label labelStatus;
		private System.Windows.Forms.ComboBox comboBoxStyle;
		private System.Windows.Forms.Label labelInfo;
		private System.Windows.Forms.Label labelResult;
		private System.Windows.Forms.Button buttonCreate;
		private System.Windows.Forms.Button buttonPrint;
		private System.Windows.Forms.Label labelBCValue;
		private System.Windows.Forms.Label labelMinBar;
		private System.Windows.Forms.Label labelBCHeight;
		private System.Windows.Forms.Label labelValueGap;
		private System.Windows.Forms.Label labelUPCNotch;
		private System.Windows.Forms.Label labelUPCFont;
		private System.Windows.Forms.Label labelValueFont;
		private System.Windows.Forms.Label labelUPCGap;
		private System.Windows.Forms.HScrollBar hScrollBarUPCNotch;
		private System.Windows.Forms.HScrollBar hScrollBarBCHeight;
		private System.Windows.Forms.HScrollBar hScrollBarValueGap;
		private System.Windows.Forms.HScrollBar hScrollBarUPCFont;
		private System.Windows.Forms.HScrollBar hScrollBarValueFont;
		private System.Windows.Forms.HScrollBar hScrollBarUPCGap;
		private System.Windows.Forms.HScrollBar hScrollBarMinBar;
		private System.Windows.Forms.Label labelMinBarVal;
		private System.Windows.Forms.Label labelUPCNotchVal;
		private System.Windows.Forms.Label labelValueGapVal;
		private System.Windows.Forms.Label labelUPCGapVal;
		private System.Windows.Forms.Label labelValueFontVal;
		private System.Windows.Forms.Label labelUPCFontVal;
		private System.Windows.Forms.Label labelHeightVal;
		private System.Windows.Forms.Label labelStyle;
		private System.Windows.Forms.TextBox textBoxBCValue;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.MenuItem FileMenu;
        private System.Windows.Forms.MenuItem ExitMenuItem;
		private Accusoft.BarcodeXpressSdk.Result[] results;
		private System.Windows.Forms.Button buttonWrite;
        private System.Windows.Forms.MenuItem AboutMenu;
        private System.Windows.Forms.MenuItem BarcodeXpressMenuItem;
		internal System.Windows.Forms.ListBox ListBox1;
        private Accusoft.BarcodeXpressSdk.BarcodeXpress BarcodeXpress6;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private MenuItem ImagXpressMenuItem;
        
        private string savedImageFileName;
		
        private System.ComponentModel.IContainer components = null;

		public MainForm()
		{


            //***Must call the UnlockRuntime method to unlock the control
	    //The unlock codes shown below are for formatting purposes only and will not work!
            //Accusoft.ImagXpressSdk.Licensing.UnlockControl(1234, 1234, 1234, 1234);

			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }
                if (imageXView1 != null)
                {
                    imageXView1.Dispose();
                    imageXView1 = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (BarcodeXpress6 != null)
                {
                    BarcodeXpress6.Dispose();
                    BarcodeXpress6 = null;
                }
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelStyle = new System.Windows.Forms.Label();
            this.comboBoxStyle = new System.Windows.Forms.ComboBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.labelBCValue = new System.Windows.Forms.Label();
            this.labelMinBar = new System.Windows.Forms.Label();
            this.labelBCHeight = new System.Windows.Forms.Label();
            this.labelValueGap = new System.Windows.Forms.Label();
            this.labelUPCNotch = new System.Windows.Forms.Label();
            this.labelUPCFont = new System.Windows.Forms.Label();
            this.labelValueFont = new System.Windows.Forms.Label();
            this.labelUPCGap = new System.Windows.Forms.Label();
            this.hScrollBarUPCNotch = new System.Windows.Forms.HScrollBar();
            this.hScrollBarMinBar = new System.Windows.Forms.HScrollBar();
            this.hScrollBarBCHeight = new System.Windows.Forms.HScrollBar();
            this.hScrollBarValueGap = new System.Windows.Forms.HScrollBar();
            this.hScrollBarUPCFont = new System.Windows.Forms.HScrollBar();
            this.hScrollBarValueFont = new System.Windows.Forms.HScrollBar();
            this.hScrollBarUPCGap = new System.Windows.Forms.HScrollBar();
            this.textBoxBCValue = new System.Windows.Forms.TextBox();
            this.labelMinBarVal = new System.Windows.Forms.Label();
            this.labelUPCNotchVal = new System.Windows.Forms.Label();
            this.labelValueGapVal = new System.Windows.Forms.Label();
            this.labelUPCGapVal = new System.Windows.Forms.Label();
            this.labelValueFontVal = new System.Windows.Forms.Label();
            this.labelUPCFontVal = new System.Windows.Forms.Label();
            this.labelHeightVal = new System.Windows.Forms.Label();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.BarcodeXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.buttonWrite = new System.Windows.Forms.Button();
            this.ListBox1 = new System.Windows.Forms.ListBox();
            this.BarcodeXpress6 = new Accusoft.BarcodeXpressSdk.BarcodeXpress();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress();
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView();
            this.SuspendLayout();
            // 
            // labelStatus
            // 
            this.labelStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStatus.Location = new System.Drawing.Point(8, 488);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(712, 40);
            this.labelStatus.TabIndex = 1;
            // 
            // labelStyle
            // 
            this.labelStyle.Location = new System.Drawing.Point(8, 104);
            this.labelStyle.Name = "labelStyle";
            this.labelStyle.Size = new System.Drawing.Size(80, 16);
            this.labelStyle.TabIndex = 3;
            this.labelStyle.Text = "Barcode Style:";
            // 
            // comboBoxStyle
            // 
            this.comboBoxStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStyle.Location = new System.Drawing.Point(96, 96);
            this.comboBoxStyle.Name = "comboBoxStyle";
            this.comboBoxStyle.Size = new System.Drawing.Size(216, 21);
            this.comboBoxStyle.TabIndex = 4;
            this.comboBoxStyle.SelectedIndexChanged += new System.EventHandler(this.comboBoxStyle_SelectedIndexChanged);
            // 
            // labelInfo
            // 
            this.labelInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelInfo.Location = new System.Drawing.Point(8, 128);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(304, 80);
            this.labelInfo.TabIndex = 5;
            this.labelInfo.Text = "Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), an" +
                "d the following symbols: space, minus (-), plus (+), period (.), dollar sign ($)" +
                ", slash (/), and percent (%).";
            // 
            // labelResult
            // 
            this.labelResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelResult.Location = new System.Drawing.Point(8, 224);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(304, 256);
            this.labelResult.TabIndex = 6;
            this.labelResult.Text = "label3";
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(336, 72);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(112, 24);
            this.buttonCreate.TabIndex = 7;
            this.buttonCreate.Text = "Create Barcode";
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(608, 72);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(112, 24);
            this.buttonPrint.TabIndex = 8;
            this.buttonPrint.Text = "Print Barcode";
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // labelBCValue
            // 
            this.labelBCValue.Location = new System.Drawing.Point(368, 112);
            this.labelBCValue.Name = "labelBCValue";
            this.labelBCValue.Size = new System.Drawing.Size(96, 16);
            this.labelBCValue.TabIndex = 9;
            this.labelBCValue.Text = "Barcode Value:";
            // 
            // labelMinBar
            // 
            this.labelMinBar.Location = new System.Drawing.Point(368, 160);
            this.labelMinBar.Name = "labelMinBar";
            this.labelMinBar.Size = new System.Drawing.Size(112, 16);
            this.labelMinBar.TabIndex = 10;
            this.labelMinBar.Text = "Min. Bar Width:";
            // 
            // labelBCHeight
            // 
            this.labelBCHeight.Location = new System.Drawing.Point(368, 136);
            this.labelBCHeight.Name = "labelBCHeight";
            this.labelBCHeight.Size = new System.Drawing.Size(112, 16);
            this.labelBCHeight.TabIndex = 11;
            this.labelBCHeight.Text = "Barcode Height:";
            // 
            // labelValueGap
            // 
            this.labelValueGap.Location = new System.Drawing.Point(368, 208);
            this.labelValueGap.Name = "labelValueGap";
            this.labelValueGap.Size = new System.Drawing.Size(112, 16);
            this.labelValueGap.TabIndex = 12;
            this.labelValueGap.Text = "Value Gap:";
            // 
            // labelUPCNotch
            // 
            this.labelUPCNotch.Location = new System.Drawing.Point(368, 184);
            this.labelUPCNotch.Name = "labelUPCNotch";
            this.labelUPCNotch.Size = new System.Drawing.Size(112, 16);
            this.labelUPCNotch.TabIndex = 13;
            this.labelUPCNotch.Text = "UPC Notch %:";
            // 
            // labelUPCFont
            // 
            this.labelUPCFont.Location = new System.Drawing.Point(368, 280);
            this.labelUPCFont.Name = "labelUPCFont";
            this.labelUPCFont.Size = new System.Drawing.Size(120, 16);
            this.labelUPCFont.TabIndex = 14;
            this.labelUPCFont.Text = "UPC Small Font Size:";
            // 
            // labelValueFont
            // 
            this.labelValueFont.Location = new System.Drawing.Point(368, 256);
            this.labelValueFont.Name = "labelValueFont";
            this.labelValueFont.Size = new System.Drawing.Size(112, 16);
            this.labelValueFont.TabIndex = 15;
            this.labelValueFont.Text = "Value Font Size:";
            // 
            // labelUPCGap
            // 
            this.labelUPCGap.Location = new System.Drawing.Point(368, 232);
            this.labelUPCGap.Name = "labelUPCGap";
            this.labelUPCGap.Size = new System.Drawing.Size(112, 16);
            this.labelUPCGap.TabIndex = 16;
            this.labelUPCGap.Text = "UPC Value Gap:";
            // 
            // hScrollBarUPCNotch
            // 
            this.hScrollBarUPCNotch.Location = new System.Drawing.Point(480, 184);
            this.hScrollBarUPCNotch.Maximum = 50;
            this.hScrollBarUPCNotch.Name = "hScrollBarUPCNotch";
            this.hScrollBarUPCNotch.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarUPCNotch.TabIndex = 18;
            this.hScrollBarUPCNotch.ValueChanged += new System.EventHandler(this.hScrollBarUPCNotch_ValueChanged);
            // 
            // hScrollBarMinBar
            // 
            this.hScrollBarMinBar.Location = new System.Drawing.Point(480, 160);
            this.hScrollBarMinBar.Maximum = 15;
            this.hScrollBarMinBar.Minimum = 2;
            this.hScrollBarMinBar.Name = "hScrollBarMinBar";
            this.hScrollBarMinBar.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarMinBar.TabIndex = 19;
            this.hScrollBarMinBar.Value = 2;
            this.hScrollBarMinBar.ValueChanged += new System.EventHandler(this.hScrollBarMinBar_ValueChanged);
            // 
            // hScrollBarBCHeight
            // 
            this.hScrollBarBCHeight.Location = new System.Drawing.Point(480, 136);
            this.hScrollBarBCHeight.Maximum = 1200;
            this.hScrollBarBCHeight.Minimum = 100;
            this.hScrollBarBCHeight.Name = "hScrollBarBCHeight";
            this.hScrollBarBCHeight.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarBCHeight.TabIndex = 20;
            this.hScrollBarBCHeight.Value = 300;
            this.hScrollBarBCHeight.ValueChanged += new System.EventHandler(this.hScrollBarBCHeight_ValueChanged);
            // 
            // hScrollBarValueGap
            // 
            this.hScrollBarValueGap.Location = new System.Drawing.Point(480, 208);
            this.hScrollBarValueGap.Maximum = 200;
            this.hScrollBarValueGap.Minimum = -200;
            this.hScrollBarValueGap.Name = "hScrollBarValueGap";
            this.hScrollBarValueGap.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarValueGap.TabIndex = 21;
            this.hScrollBarValueGap.ValueChanged += new System.EventHandler(this.hScrollBarValueGap_ValueChanged);
            // 
            // hScrollBarUPCFont
            // 
            this.hScrollBarUPCFont.Location = new System.Drawing.Point(480, 280);
            this.hScrollBarUPCFont.Minimum = 6;
            this.hScrollBarUPCFont.Name = "hScrollBarUPCFont";
            this.hScrollBarUPCFont.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarUPCFont.TabIndex = 22;
            this.hScrollBarUPCFont.Value = 8;
            this.hScrollBarUPCFont.ValueChanged += new System.EventHandler(this.hScrollBarUPCFont_ValueChanged);
            // 
            // hScrollBarValueFont
            // 
            this.hScrollBarValueFont.Location = new System.Drawing.Point(480, 256);
            this.hScrollBarValueFont.Maximum = 150;
            this.hScrollBarValueFont.Minimum = 12;
            this.hScrollBarValueFont.Name = "hScrollBarValueFont";
            this.hScrollBarValueFont.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarValueFont.TabIndex = 23;
            this.hScrollBarValueFont.Value = 12;
            this.hScrollBarValueFont.ValueChanged += new System.EventHandler(this.hScrollBarValueFont_ValueChanged);
            // 
            // hScrollBarUPCGap
            // 
            this.hScrollBarUPCGap.Location = new System.Drawing.Point(480, 232);
            this.hScrollBarUPCGap.Maximum = 200;
            this.hScrollBarUPCGap.Minimum = -200;
            this.hScrollBarUPCGap.Name = "hScrollBarUPCGap";
            this.hScrollBarUPCGap.Size = new System.Drawing.Size(184, 16);
            this.hScrollBarUPCGap.TabIndex = 24;
            this.hScrollBarUPCGap.ValueChanged += new System.EventHandler(this.hScrollBarUPCGap_ValueChanged);
            // 
            // textBoxBCValue
            // 
            this.textBoxBCValue.Location = new System.Drawing.Point(480, 104);
            this.textBoxBCValue.Name = "textBoxBCValue";
            this.textBoxBCValue.Size = new System.Drawing.Size(184, 20);
            this.textBoxBCValue.TabIndex = 25;
            this.textBoxBCValue.Text = "textBox1";
            // 
            // labelMinBarVal
            // 
            this.labelMinBarVal.Location = new System.Drawing.Point(672, 160);
            this.labelMinBarVal.Name = "labelMinBarVal";
            this.labelMinBarVal.Size = new System.Drawing.Size(40, 16);
            this.labelMinBarVal.TabIndex = 26;
            // 
            // labelUPCNotchVal
            // 
            this.labelUPCNotchVal.Location = new System.Drawing.Point(672, 184);
            this.labelUPCNotchVal.Name = "labelUPCNotchVal";
            this.labelUPCNotchVal.Size = new System.Drawing.Size(40, 16);
            this.labelUPCNotchVal.TabIndex = 27;
            // 
            // labelValueGapVal
            // 
            this.labelValueGapVal.Location = new System.Drawing.Point(672, 208);
            this.labelValueGapVal.Name = "labelValueGapVal";
            this.labelValueGapVal.Size = new System.Drawing.Size(40, 16);
            this.labelValueGapVal.TabIndex = 28;
            // 
            // labelUPCGapVal
            // 
            this.labelUPCGapVal.Location = new System.Drawing.Point(672, 232);
            this.labelUPCGapVal.Name = "labelUPCGapVal";
            this.labelUPCGapVal.Size = new System.Drawing.Size(40, 16);
            this.labelUPCGapVal.TabIndex = 29;
            // 
            // labelValueFontVal
            // 
            this.labelValueFontVal.Location = new System.Drawing.Point(672, 256);
            this.labelValueFontVal.Name = "labelValueFontVal";
            this.labelValueFontVal.Size = new System.Drawing.Size(40, 16);
            this.labelValueFontVal.TabIndex = 30;
            // 
            // labelUPCFontVal
            // 
            this.labelUPCFontVal.Location = new System.Drawing.Point(672, 280);
            this.labelUPCFontVal.Name = "labelUPCFontVal";
            this.labelUPCFontVal.Size = new System.Drawing.Size(40, 16);
            this.labelUPCFontVal.TabIndex = 31;
            // 
            // labelHeightVal
            // 
            this.labelHeightVal.Location = new System.Drawing.Point(672, 136);
            this.labelHeightVal.Name = "labelHeightVal";
            this.labelHeightVal.Size = new System.Drawing.Size(40, 16);
            this.labelHeightVal.TabIndex = 32;
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 0;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.menuItemFileExit_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 1;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.BarcodeXpressMenuItem,
            this.ImagXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // BarcodeXpressMenuItem
            // 
            this.BarcodeXpressMenuItem.Index = 0;
            this.BarcodeXpressMenuItem.Text = "&BarcodeXpress";
            this.BarcodeXpressMenuItem.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 1;
            this.ImagXpressMenuItem.Text = "&ImagXpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // buttonWrite
            // 
            this.buttonWrite.Location = new System.Drawing.Point(472, 72);
            this.buttonWrite.Name = "buttonWrite";
            this.buttonWrite.Size = new System.Drawing.Size(112, 24);
            this.buttonWrite.TabIndex = 96;
            this.buttonWrite.Text = "Write To File";
            this.buttonWrite.Click += new System.EventHandler(this.buttonWrite_Click);
            // 
            // ListBox1
            // 
            this.ListBox1.Items.AddRange(new object[] {
            "This sample demonstrates creating 1D Barcodes with the toolkit."});
            this.ListBox1.Location = new System.Drawing.Point(16, 8);
            this.ListBox1.Name = "ListBox1";
            this.ListBox1.Size = new System.Drawing.Size(696, 56);
            this.ListBox1.TabIndex = 97;
            // 
            // BarcodeXpress5
            // 
            this.BarcodeXpress6.Debug = false;
            this.BarcodeXpress6.DebugLogFile = "C:\\Documents and Settings\\jargento.JPG.COM\\My Documents\\BarcodeXpress5.log";
            this.BarcodeXpress6.ErrorLevel = Accusoft.BarcodeXpressSdk.ErrorLevelInfo.Production;
            // 
            // imageXView1
            // 
            this.imageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(318, 299);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(401, 186);
            this.imageXView1.TabIndex = 98;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(728, 539);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.ListBox1);
            this.Controls.Add(this.buttonWrite);
            this.Controls.Add(this.labelHeightVal);
            this.Controls.Add(this.labelUPCFontVal);
            this.Controls.Add(this.labelValueFontVal);
            this.Controls.Add(this.labelUPCGapVal);
            this.Controls.Add(this.labelValueGapVal);
            this.Controls.Add(this.labelUPCNotchVal);
            this.Controls.Add(this.labelMinBarVal);
            this.Controls.Add(this.textBoxBCValue);
            this.Controls.Add(this.hScrollBarUPCGap);
            this.Controls.Add(this.hScrollBarValueFont);
            this.Controls.Add(this.hScrollBarUPCFont);
            this.Controls.Add(this.hScrollBarValueGap);
            this.Controls.Add(this.hScrollBarBCHeight);
            this.Controls.Add(this.hScrollBarMinBar);
            this.Controls.Add(this.hScrollBarUPCNotch);
            this.Controls.Add(this.labelUPCGap);
            this.Controls.Add(this.labelValueFont);
            this.Controls.Add(this.labelUPCFont);
            this.Controls.Add(this.labelUPCNotch);
            this.Controls.Add(this.labelValueGap);
            this.Controls.Add(this.labelBCHeight);
            this.Controls.Add(this.labelMinBar);
            this.Controls.Add(this.labelBCValue);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.comboBoxStyle);
            this.Controls.Add(this.labelStyle);
            this.Controls.Add(this.labelStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Write 1D Barcodes";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.Run(new MainForm());
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{

			//***Must call the UnlockRuntime method to unlock the control
			//The unlock codes shown below are for formatting purposes only and will not work!
	               //imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);

			comboBoxStyle.Items.Add ("Code 39 Narrow");
			comboBoxStyle.Items.Add ("Code 39 Extended Narrow");
			comboBoxStyle.Items.Add ("Code 39 Wide");
			comboBoxStyle.Items.Add ("Code 39 Extended Wide");
			comboBoxStyle.Items.Add ("Industrial 2 of 5");
			comboBoxStyle.Items.Add ("Interleaved 2 of 5");
			comboBoxStyle.Items.Add ("Codabar Rationalized");
			comboBoxStyle.Items.Add ("Code 128A");
			comboBoxStyle.Items.Add ("Code 128B");
			comboBoxStyle.Items.Add ("Code 128C");
			comboBoxStyle.Items.Add ("Code 128");
			comboBoxStyle.Items.Add ("UPC-A");
			comboBoxStyle.Items.Add ("UPC-E");
			comboBoxStyle.Items.Add ("EAN-8");
			comboBoxStyle.Items.Add ("EAN-13");
			comboBoxStyle.Items.Add ("EAN-128");
			comboBoxStyle.Items.Add ("Code 93");
			comboBoxStyle.Items.Add ("Code 93 Extended");
			comboBoxStyle.Items.Add ("Patch Code");
			comboBoxStyle.SelectedIndex = 0;

			hScrollBarBCHeight.Value = BarcodeXpress6.writer.MinimumBarHeight;
			hScrollBarMinBar.Value = BarcodeXpress6.writer.MinimumBarWidth;
			hScrollBarUPCNotch.Value = BarcodeXpress6.writer.TextNotchPercent;
			hScrollBarValueGap.Value = BarcodeXpress6.writer.VerticalTextGap;
			hScrollBarUPCGap.Value = BarcodeXpress6.writer.HorizontalTextGap;
			hScrollBarValueFont.Value = (System.Int32) BarcodeXpress6.writer.TextFont.Size;
			hScrollBarUPCFont.Value = (System.Int32) BarcodeXpress6.writer.OutsideTextFont.Size;
		}

		private void comboBoxStyle_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			switch (comboBoxStyle.SelectedIndex)
			{
				case 0:
				{
					//BarcodeXpress5.writer.BarcodeFont = peBarcodeStyle.BARSTYLE_Code39_Narrow;
					labelInfo.Text = @"Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code39Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code39Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 1:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code39_Extended_Narrow;
					labelInfo.Text = @"Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39 Ext";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code39ExtendedBarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code39ExtendedBarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 2:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code39_Wide;
					labelInfo.Text = @"Code 39 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code39Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code39Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 3:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code39_Extended_Wide;
					labelInfo.Text = @"Code 39 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 39 Ext";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code39ExtendedBarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code39ExtendedBarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 4:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Industrial_2_of_5;
					labelInfo.Text = @"Industry 2 of 5 - Allowable characters - digits 0-9.";
					textBoxBCValue.Text = "1234321";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Industry2of5Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Industry2of5Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 5:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Interleaved_2_of_5;
					labelInfo.Text = @"Interleaved 2 of 5 - Allowable characters - digits 0-9.";
					textBoxBCValue.Text = "1234321";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Interleaved2of5Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Interleaved2of5Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 6:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Codabar_Rationalized;
					labelInfo.Text = @"Codabar - Allowable characters - digits 0-9, six symbols including: minus (-), plus (+), period (.), dollar sign ($), slash (/), and colon (:), and the following start/stop characters A, B, C and D.";
					textBoxBCValue.Text = "C5467D";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.CodabarBarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.CodabarBarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 7:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code128A;
					labelInfo.Text = @"Code 128A - Allowable characters - Digits, uppercase characters, standard ASCII symbols.";
					textBoxBCValue.Text = "CODE 128A";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}
				case 8:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code128B;
					labelInfo.Text = @"Code 128B - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols.";
					textBoxBCValue.Text = "CODE 128B";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 9:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code128C;
					labelInfo.Text = @"Code 128C - Allowable characters - Digits 0-9.";
					textBoxBCValue.Text = "987654321";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 10:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code128;
					labelInfo.Text = @"Code 128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols.";
					textBoxBCValue.Text = "CODE 128";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code128Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 11:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_UPC_A;
					labelInfo.Text = @"UPC-A - Allowable characters - Digits 0-9; You must have 11 or 12 digits.";
					textBoxBCValue.Text = "69792911003";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.UPCABarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.UPCABarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 12:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_UPC_E;
					labelInfo.Text = @"UPC-E - Allowable characters - Digits 0-9; You must have 6, 10, 11 or 12 digits.";
					textBoxBCValue.Text = "654123";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.UPCEBarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.UPCEBarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 13:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_EAN_8;
					labelInfo.Text = @"EAN-8 - Allowable characters - Digits 0-9; You must have 7 or 8 digits.";
					textBoxBCValue.Text = "1234567";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.EAN8Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.EAN8Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 14:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_EAN_13;
					labelInfo.Text = @"EAN-13 - Allowable characters - Digits 0-9; You must have 12 or 13 digits.";
					textBoxBCValue.Text = "123456765432";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.EAN13Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.EAN13Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 15:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_EAN_128;
					labelInfo.Text = @"EAN-128 - Allowable characters - Digits, upper and lower case characters, standard ASCII symbols.";
					textBoxBCValue.Text = "EAN-128";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.EAN128Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.EAN128Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 16:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code93;
					labelInfo.Text = @"Code 93 - Allowable characters - digits 0-9, the letters A-Z (uppercase only), and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 93";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code93Barcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code93Barcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 17:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Code93_Extended;
					labelInfo.Text = @"Code 93 Ext - Allowable characters - digits 0-9, the letters A-Z, a-z, and the following symbols: space, minus (-), plus (+), period (.), dollar sign ($), slash (/), and percent (%).";
					textBoxBCValue.Text = "CODE 93 Ext";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.Code93ExtendedBarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.Code93ExtendedBarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}

				case 18:
				{
					//BarcodeXpress5.MakeBarcodeStyle = peBarcodeStyle.BARSTYLE_Patchcode;
					labelInfo.Text = @"Patch code - (1)=Patch I; (2)=Patch II; (3)=Patch III; (4)=Patch IV; (5)=Patch VI; (6)=Patch T.";
					if( hScrollBarMinBar.Value < 5 )
					{
						hScrollBarMinBar.Value = 5;
					}
					textBoxBCValue.Text = "1";
					BarcodeXpress6.writer.BarcodeType = Accusoft.BarcodeXpressSdk.BarcodeType.PatchCodeBarcode;
					System.Array currentBarcodeTypes = new Accusoft.BarcodeXpressSdk.BarcodeType[1];
					currentBarcodeTypes.SetValue( Accusoft.BarcodeXpressSdk.BarcodeType.PatchCodeBarcode, 0 );
					BarcodeXpress6.reader.BarcodeTypes = currentBarcodeTypes;
					break;
				}
			}

			labelResult.Text = "";
			imageXView1.Image = null;
			buttonPrint.Enabled = false;
			buttonWrite.Enabled = false;
		}

		private void buttonCreate_Click(object sender, System.EventArgs e)
		{
			BarcodeXpress6.writer.BarcodeValue = textBoxBCValue.Text;
			labelStatus.Text = "";
            System.IntPtr DIB;

			try
			{
                //imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(new IntPtr(BarcodeXpress6.writer.Create()));
                imageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress1, BarcodeXpress6.writer.Create());
                DIB = imageXView1.Image.ToHdib(false);
				// read the barcode
				System.Drawing.Rectangle currentArea = new System.Drawing.Rectangle(0, 0, 0, 0);
				BarcodeXpress6.reader.Area = currentArea;
                results = BarcodeXpress6.reader.Analyze(DIB);

				if (results.Length > 0 )
				{
					string strResult = results.Length + " barcode(s) found.\n\n";
					for (int i = 0; i < results.Length; i++)
					{

						Accusoft.BarcodeXpressSdk.Result curResult = (Accusoft.BarcodeXpressSdk.Result)results.GetValue(i);;
						strResult += "Symbol #" + i + "\nType: " + curResult.BarcodeName + "\n";
						strResult += "X - " + curResult.Area.X + " pixels\nY - " + curResult.Area.Y + " pixels\n";
						strResult += "W - " + curResult.Area.Width + " pixels\nH - " + curResult.Area.Height + " pixels\n";
						strResult += "Value:\n" + curResult.BarcodeValue + "\n\n";
						if(curResult.ValidCheckSum)
						{
							strResult += "Checksum OK";
						}
					}

					strResult += "\n";
					labelResult.Text = strResult;
					labelStatus.Text = "Barcode detection was successful.";
					buttonPrint.Enabled = true;
					buttonWrite.Enabled = true;
				}
				else
				{
					labelStatus.Text = "Could not detect any barcodes.";
					buttonPrint.Enabled = false;
					buttonWrite.Enabled = false;
				}


                //Must free the value passed to the Analyze method
                GlobalFree(DIB);

			}
			catch( Accusoft.BarcodeXpressSdk.BarcodeException ex )
			{
				string strResult;
				strResult = "Error Creating Barcode: ";
				strResult += ex.Message;
				labelResult.Text = strResult;
				labelStatus.Text = strResult;
			}

		}

		private void hScrollBarBCHeight_ValueChanged(object sender, System.EventArgs e)
		{
			labelHeightVal.Text = hScrollBarBCHeight.Value.ToString ();
			BarcodeXpress6.writer.MinimumBarHeight = hScrollBarBCHeight.Value;
		}

		private void hScrollBarMinBar_ValueChanged(object sender, System.EventArgs e)
		{
			labelMinBarVal.Text = hScrollBarMinBar.Value.ToString ();
			BarcodeXpress6.writer.MinimumBarWidth = (short) hScrollBarMinBar.Value;
		}

		private void hScrollBarUPCNotch_ValueChanged(object sender, System.EventArgs e)
		{
			labelUPCNotchVal.Text = hScrollBarUPCNotch.Value.ToString ();
			BarcodeXpress6.writer.TextNotchPercent = hScrollBarUPCNotch.Value;
		}

		private void hScrollBarValueGap_ValueChanged(object sender, System.EventArgs e)
		{
			labelValueGapVal.Text = hScrollBarValueGap.Value.ToString ();
			BarcodeXpress6.writer.VerticalTextGap = hScrollBarValueGap.Value;
		}

		private void hScrollBarUPCGap_ValueChanged(object sender, System.EventArgs e)
		{
			labelUPCGapVal.Text = hScrollBarUPCGap.Value.ToString ();
			BarcodeXpress6.writer.HorizontalTextGap = hScrollBarUPCGap.Value;
		}

		private void hScrollBarValueFont_ValueChanged(object sender, System.EventArgs e)
		{
			labelValueFontVal.Text = hScrollBarValueFont.Value.ToString ();
			System.Drawing.Font font = BarcodeXpress6.writer.TextFont;
			BarcodeXpress6.writer.TextFont = new System.Drawing.Font (font.FontFamily.GetName(0), hScrollBarValueFont.Value);
		}

		private void hScrollBarUPCFont_ValueChanged(object sender, System.EventArgs e)
		{
			this.labelUPCFontVal.Text = hScrollBarUPCFont.Value.ToString ();

			System.Drawing.Font font = BarcodeXpress6.writer.OutsideTextFont;
			BarcodeXpress6.writer.OutsideTextFont = new System.Drawing.Font (font.FontFamily.GetName(0), hScrollBarUPCFont.Value);
		}

		private void menuItemFileExit_Click(object sender, System.EventArgs e)
		{
			System.Environment.Exit (0);
		}

		private void buttonPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// Print the Image
				PrintDocument doc = new PrintDocument ();
				doc.PrintPage += new PrintPageEventHandler (this.OnPrintPage);
				doc.Print ();
				labelStatus.Text = "Barcode printed.";
			}
			catch( Exception ex )
			{
				labelStatus.Text = ex.Message;
			}
		}

        private void OnPrintPage(Object sender, PrintPageEventArgs args)
        {

            Accusoft.ImagXpressSdk.Processor proc = new Accusoft.ImagXpressSdk.Processor(imagXpress1);

            proc.Image = imageXView1.Image.Copy();
            proc.ColorDepth(24, 0, 0);
            imageXView1.Image = proc.Image;

            System.Drawing.Graphics g = imageXView1.Image.GetGraphics();
            System.Drawing.Image img = (System.Drawing.Image)imageXView1.Image.ToBitmap(false);
            args.Graphics.PageUnit = GraphicsUnit.Inch;

            args.Graphics.DrawImage(img, (Single)((8.5 - img.Width / img.HorizontalResolution) / 2), (Single)((11 - img.Height / img.VerticalResolution) / 2), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution);

            imageXView1.Image.ReleaseGraphics();
        }

		private void buttonWrite_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();

			dlg.Filter = "TIF (*.TIF)|.TIF";
			dlg.FilterIndex = 0;
			dlg.ShowDialog();

			string strMsg;

            try
            {
                //string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
                //string strImageFile = System.IO.Path.Combine(strCurrentDir, @"..\..\barcode.TIF");

                if (dlg.FileName != "")
                {
                    savedImageFileName = dlg.FileName;
                    Accusoft.ImagXpressSdk.SaveOptions so = new Accusoft.ImagXpressSdk.SaveOptions();
                    so.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff;
                    so.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4;

                    imageXView1.Image.Save(dlg.FileName, so);

                    strMsg = "Image saved as " + dlg.FileName;//strImageFile;
                    labelStatus.Text = strMsg;
                }

            }
            catch (Accusoft.ImagXpressSdk.ImagXpressException ex)
            {
                strMsg = "Error Writing To File.  ImagXpress Error: ";
                strMsg += ex.Message;
                labelStatus.Text = strMsg;
            }
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			BarcodeXpress6.AboutBox();
		}

        private void ImagXpressMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }
	}
}

'****************************************************************
'* Copyright 2007 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
'****************************************************************/
Option Explicit On
Option Strict On

Imports System.Drawing.Printing
Imports Accusoft.ImagXpressSdk
Imports Accusoft.BarcodeXpressSdk
Imports System.Runtime.InteropServices

Public Class Write2DBarcodes
    Inherits System.Windows.Forms.Form

    <DllImport("kernel32.dll")> _
    Private Shared Function GlobalFree(ByVal hMem As IntPtr) As IntPtr
    End Function

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()


        '***Must call the UnlockControl method to unlock the control
	'The unlock codes shown below are for formatting purposes only and will not work!
        'Accusoft.ImagXpressSdk.Licensing.UnlockControl(1234, 1234, 1234, 1234)


        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (ImageXView1.Image Is Nothing) Then
                ImageXView1.Image.Dispose()
                ImageXView1.Image = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (BarcodeXpress1 Is Nothing) Then
                BarcodeXpress1.Dispose()
                BarcodeXpress1 = Nothing
            End If
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents AboutMenu As System.Windows.Forms.MenuItem
    Friend WithEvents BarcodeXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend results() As Accusoft.BarcodeXpressSdk.Result
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents buttonCreate As System.Windows.Forms.Button
    Friend WithEvents buttonWrite As System.Windows.Forms.Button
    Friend WithEvents buttonPrint As System.Windows.Forms.Button
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblColumns As System.Windows.Forms.Label
    Friend WithEvents lblRows As System.Windows.Forms.Label
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblStyle As System.Windows.Forms.Label
    Friend WithEvents lblErrorVal As System.Windows.Forms.Label
    Friend WithEvents lblRowsVal As System.Windows.Forms.Label
    Friend WithEvents lblWidthVal As System.Windows.Forms.Label
    Friend WithEvents lblHeightVal As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents hscError As System.Windows.Forms.HScrollBar
    Friend WithEvents hscColumns As System.Windows.Forms.HScrollBar
    Friend WithEvents hscRows As System.Windows.Forms.HScrollBar
    Friend WithEvents hscWidth As System.Windows.Forms.HScrollBar
    Friend WithEvents hscHeight As System.Windows.Forms.HScrollBar
    Friend WithEvents cmbBarcodeType As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumnsAndRows As System.Windows.Forms.ComboBox
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents BarcodeXpress1 As Accusoft.BarcodeXpressSdk.BarcodeXpress
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents lblColumnsVal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.buttonCreate = New System.Windows.Forms.Button
        Me.buttonWrite = New System.Windows.Forms.Button
        Me.buttonPrint = New System.Windows.Forms.Button
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblResult = New System.Windows.Forms.Label
        Me.lblInfo = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.FileMenu = New System.Windows.Forms.MenuItem
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem
        Me.AboutMenu = New System.Windows.Forms.MenuItem
        Me.BarcodeXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.lblStyle = New System.Windows.Forms.Label
        Me.lblValue = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.lblColumns = New System.Windows.Forms.Label
        Me.lblRows = New System.Windows.Forms.Label
        Me.lblWidth = New System.Windows.Forms.Label
        Me.lblHeight = New System.Windows.Forms.Label
        Me.lblErrorVal = New System.Windows.Forms.Label
        Me.lblColumnsVal = New System.Windows.Forms.Label
        Me.lblRowsVal = New System.Windows.Forms.Label
        Me.lblWidthVal = New System.Windows.Forms.Label
        Me.lblHeightVal = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.hscError = New System.Windows.Forms.HScrollBar
        Me.hscColumns = New System.Windows.Forms.HScrollBar
        Me.hscRows = New System.Windows.Forms.HScrollBar
        Me.hscWidth = New System.Windows.Forms.HScrollBar
        Me.hscHeight = New System.Windows.Forms.HScrollBar
        Me.cmbColumnsAndRows = New System.Windows.Forms.ComboBox
        Me.cmbBarcodeType = New System.Windows.Forms.ComboBox
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.BarcodeXpress1 = New Accusoft.BarcodeXpressSdk.BarcodeXpress
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView
        Me.SuspendLayout()
        '
        'buttonCreate
        '
        Me.buttonCreate.Location = New System.Drawing.Point(360, 120)
        Me.buttonCreate.Name = "buttonCreate"
        Me.buttonCreate.Size = New System.Drawing.Size(128, 32)
        Me.buttonCreate.TabIndex = 1
        Me.buttonCreate.Text = "Create Barcode"
        '
        'buttonWrite
        '
        Me.buttonWrite.Location = New System.Drawing.Point(512, 120)
        Me.buttonWrite.Name = "buttonWrite"
        Me.buttonWrite.Size = New System.Drawing.Size(128, 32)
        Me.buttonWrite.TabIndex = 2
        Me.buttonWrite.Text = "Write To File"
        '
        'buttonPrint
        '
        Me.buttonPrint.Location = New System.Drawing.Point(664, 120)
        Me.buttonPrint.Name = "buttonPrint"
        Me.buttonPrint.Size = New System.Drawing.Size(128, 32)
        Me.buttonPrint.TabIndex = 3
        Me.buttonPrint.Text = "Print Barcode"
        '
        'ListBox1
        '
        Me.ListBox1.Items.AddRange(New Object() {"This sample demonstrates creating 2D Barcodes with the toolkit."})
        Me.ListBox1.Location = New System.Drawing.Point(24, 8)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(768, 69)
        Me.ListBox1.TabIndex = 4
        '
        'lblStatus
        '
        Me.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStatus.Location = New System.Drawing.Point(24, 632)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(768, 40)
        Me.lblStatus.TabIndex = 5
        '
        'lblResult
        '
        Me.lblResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblResult.Location = New System.Drawing.Point(24, 232)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(312, 392)
        Me.lblResult.TabIndex = 6
        '
        'lblInfo
        '
        Me.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblInfo.Location = New System.Drawing.Point(24, 144)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(312, 80)
        Me.lblInfo.TabIndex = 7
        Me.lblInfo.Text = "PDF417"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.AboutMenu})
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ExitMenuItem})
        Me.FileMenu.Text = "&File"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 0
        Me.ExitMenuItem.Text = "E&xit"
        '
        'AboutMenu
        '
        Me.AboutMenu.Index = 1
        Me.AboutMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.BarcodeXpressMenuItem, Me.ImagXpressMenuItem})
        Me.AboutMenu.Text = "&About"
        '
        'BarcodeXpressMenuItem
        '
        Me.BarcodeXpressMenuItem.Index = 0
        Me.BarcodeXpressMenuItem.Text = "&BarcodeXpress"
        '
        'lblStyle
        '
        Me.lblStyle.Location = New System.Drawing.Point(16, 88)
        Me.lblStyle.Name = "lblStyle"
        Me.lblStyle.Size = New System.Drawing.Size(144, 16)
        Me.lblStyle.TabIndex = 9
        Me.lblStyle.Text = "Barcode Style:"
        '
        'lblValue
        '
        Me.lblValue.Location = New System.Drawing.Point(360, 176)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(88, 16)
        Me.lblValue.TabIndex = 10
        Me.lblValue.Text = "Barcode Value:"
        '
        'lblError
        '
        Me.lblError.Location = New System.Drawing.Point(360, 200)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(88, 16)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error Correction:"
        '
        'lblColumns
        '
        Me.lblColumns.Location = New System.Drawing.Point(360, 248)
        Me.lblColumns.Name = "lblColumns"
        Me.lblColumns.Size = New System.Drawing.Size(88, 16)
        Me.lblColumns.TabIndex = 12
        Me.lblColumns.Text = "Columns:"
        '
        'lblRows
        '
        Me.lblRows.Location = New System.Drawing.Point(360, 224)
        Me.lblRows.Name = "lblRows"
        Me.lblRows.Size = New System.Drawing.Size(88, 16)
        Me.lblRows.TabIndex = 13
        Me.lblRows.Text = "Rows:"
        '
        'lblWidth
        '
        Me.lblWidth.Location = New System.Drawing.Point(360, 272)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(88, 16)
        Me.lblWidth.TabIndex = 14
        Me.lblWidth.Text = "Width:"
        '
        'lblHeight
        '
        Me.lblHeight.Location = New System.Drawing.Point(360, 296)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(88, 16)
        Me.lblHeight.TabIndex = 15
        Me.lblHeight.Text = "Height:"
        '
        'lblErrorVal
        '
        Me.lblErrorVal.Location = New System.Drawing.Point(656, 200)
        Me.lblErrorVal.Name = "lblErrorVal"
        Me.lblErrorVal.Size = New System.Drawing.Size(80, 16)
        Me.lblErrorVal.TabIndex = 16
        Me.lblErrorVal.Text = "-1"
        '
        'lblColumnsVal
        '
        Me.lblColumnsVal.Location = New System.Drawing.Point(656, 248)
        Me.lblColumnsVal.Name = "lblColumnsVal"
        Me.lblColumnsVal.Size = New System.Drawing.Size(88, 16)
        Me.lblColumnsVal.TabIndex = 17
        Me.lblColumnsVal.Text = "-1"
        '
        'lblRowsVal
        '
        Me.lblRowsVal.Location = New System.Drawing.Point(656, 224)
        Me.lblRowsVal.Name = "lblRowsVal"
        Me.lblRowsVal.Size = New System.Drawing.Size(88, 16)
        Me.lblRowsVal.TabIndex = 18
        Me.lblRowsVal.Text = "-1"
        '
        'lblWidthVal
        '
        Me.lblWidthVal.Location = New System.Drawing.Point(656, 272)
        Me.lblWidthVal.Name = "lblWidthVal"
        Me.lblWidthVal.Size = New System.Drawing.Size(88, 16)
        Me.lblWidthVal.TabIndex = 19
        Me.lblWidthVal.Text = "3"
        '
        'lblHeightVal
        '
        Me.lblHeightVal.Location = New System.Drawing.Point(656, 296)
        Me.lblHeightVal.Name = "lblHeightVal"
        Me.lblHeightVal.Size = New System.Drawing.Size(88, 16)
        Me.lblHeightVal.TabIndex = 20
        Me.lblHeightVal.Text = "4"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(456, 176)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(192, 20)
        Me.TextBox1.TabIndex = 21
        Me.TextBox1.Text = "PDF417"
        '
        'hscError
        '
        Me.hscError.LargeChange = 1
        Me.hscError.Location = New System.Drawing.Point(456, 200)
        Me.hscError.Maximum = 8
        Me.hscError.Minimum = -1
        Me.hscError.Name = "hscError"
        Me.hscError.Size = New System.Drawing.Size(192, 16)
        Me.hscError.TabIndex = 22
        Me.hscError.Value = -1
        '
        'hscColumns
        '
        Me.hscColumns.LargeChange = 1
        Me.hscColumns.Location = New System.Drawing.Point(456, 248)
        Me.hscColumns.Maximum = 30
        Me.hscColumns.Name = "hscColumns"
        Me.hscColumns.Size = New System.Drawing.Size(192, 16)
        Me.hscColumns.TabIndex = 23
        '
        'hscRows
        '
        Me.hscRows.LargeChange = 1
        Me.hscRows.Location = New System.Drawing.Point(456, 224)
        Me.hscRows.Maximum = 90
        Me.hscRows.Minimum = 2
        Me.hscRows.Name = "hscRows"
        Me.hscRows.Size = New System.Drawing.Size(192, 16)
        Me.hscRows.TabIndex = 24
        Me.hscRows.Value = 2
        '
        'hscWidth
        '
        Me.hscWidth.LargeChange = 1
        Me.hscWidth.Location = New System.Drawing.Point(456, 272)
        Me.hscWidth.Maximum = 10
        Me.hscWidth.Minimum = 2
        Me.hscWidth.Name = "hscWidth"
        Me.hscWidth.Size = New System.Drawing.Size(192, 16)
        Me.hscWidth.TabIndex = 25
        Me.hscWidth.Value = 3
        '
        'hscHeight
        '
        Me.hscHeight.LargeChange = 1
        Me.hscHeight.Location = New System.Drawing.Point(456, 296)
        Me.hscHeight.Maximum = 5
        Me.hscHeight.Minimum = 3
        Me.hscHeight.Name = "hscHeight"
        Me.hscHeight.Size = New System.Drawing.Size(192, 16)
        Me.hscHeight.TabIndex = 26
        Me.hscHeight.Value = 4
        '
        'cmbColumnsAndRows
        '
        Me.cmbColumnsAndRows.Location = New System.Drawing.Point(456, 232)
        Me.cmbColumnsAndRows.Name = "cmbColumnsAndRows"
        Me.cmbColumnsAndRows.Size = New System.Drawing.Size(192, 21)
        Me.cmbColumnsAndRows.TabIndex = 27
        Me.cmbColumnsAndRows.Text = "ComboBox1"
        Me.cmbColumnsAndRows.Visible = False
        '
        'cmbBarcodeType
        '
        Me.cmbBarcodeType.Items.AddRange(New Object() {"PDF417", "DataMatrix"})
        Me.cmbBarcodeType.Location = New System.Drawing.Point(24, 112)
        Me.cmbBarcodeType.Name = "cmbBarcodeType"
        Me.cmbBarcodeType.Size = New System.Drawing.Size(312, 21)
        Me.cmbBarcodeType.TabIndex = 29
        Me.cmbBarcodeType.Text = "PDF417"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 1
        Me.ImagXpressMenuItem.Text = "&ImagXpress"
        '
        'BarcodeXpress1
        '
        Me.BarcodeXpress1.Debug = False
        Me.BarcodeXpress1.DebugLogFile = "C:\Documents and Settings\jargento.JPG.COM\My Documents\BarcodeXpress5.log"
        Me.BarcodeXpress1.ErrorLevel = Accusoft.BarcodeXpressSdk.ErrorLevelInfo.Production
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(345, 325)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(461, 298)
        Me.ImageXView1.TabIndex = 30
        '
        'Write2DBarcodes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(818, 683)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.cmbBarcodeType)
        Me.Controls.Add(Me.cmbColumnsAndRows)
        Me.Controls.Add(Me.hscHeight)
        Me.Controls.Add(Me.hscWidth)
        Me.Controls.Add(Me.hscRows)
        Me.Controls.Add(Me.hscColumns)
        Me.Controls.Add(Me.hscError)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.lblHeightVal)
        Me.Controls.Add(Me.lblWidthVal)
        Me.Controls.Add(Me.lblRowsVal)
        Me.Controls.Add(Me.lblColumnsVal)
        Me.Controls.Add(Me.lblErrorVal)
        Me.Controls.Add(Me.lblHeight)
        Me.Controls.Add(Me.lblWidth)
        Me.Controls.Add(Me.lblRows)
        Me.Controls.Add(Me.lblColumns)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.lblValue)
        Me.Controls.Add(Me.lblStyle)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.lblResult)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.buttonPrint)
        Me.Controls.Add(Me.buttonWrite)
        Me.Controls.Add(Me.buttonCreate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Menu = Me.MainMenu1
        Me.Name = "Write2DBarcodes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Write2DBarcodes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Write2DBarcodes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '***Must call the UnlockRuntime method to unlock the control
        'The unlock codes shown below are for formatting purposes only and will not work!
        'ImagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234)
        'barcodexpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

        Application.EnableVisualStyles()

        BarcodeXpress1.Licensing.LicenseEdition = Accusoft.BarcodeXpressSdk.LicenseChoice.ProfessionalEdition

        'Setup the values of the Data Matrix Combo Box
        cmbColumnsAndRows.Items.Add("Auto")
        cmbColumnsAndRows.Items.Item(0) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.AutoRowsAndColumns

        cmbColumnsAndRows.Items.Add("8 x 18")
        cmbColumnsAndRows.Items.Item(1) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows8Columns18

        cmbColumnsAndRows.Items.Add("8 x 32")
        cmbColumnsAndRows.Items.Item(2) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows8Columns32

        cmbColumnsAndRows.Items.Add("10 x 10")
        cmbColumnsAndRows.Items.Item(3) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows10Columns10

        cmbColumnsAndRows.Items.Add("12 x 12")
        cmbColumnsAndRows.Items.Item(4) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows12Columns12

        cmbColumnsAndRows.Items.Add("12 x 26")
        cmbColumnsAndRows.Items.Item(5) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows12Columns26

        cmbColumnsAndRows.Items.Add("12 x 36")
        cmbColumnsAndRows.Items.Item(6) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows12Columns36

        cmbColumnsAndRows.Items.Add("14 x 14")
        cmbColumnsAndRows.Items.Item(7) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows14Columns14

        cmbColumnsAndRows.Items.Add("16 x 16")
        cmbColumnsAndRows.Items.Item(8) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows16Columns16

        cmbColumnsAndRows.Items.Add("16 x 36")
        cmbColumnsAndRows.Items.Item(9) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows16Columns36

        cmbColumnsAndRows.Items.Add("16 x 48")
        cmbColumnsAndRows.Items.Item(10) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows16Columns48

        cmbColumnsAndRows.Items.Add("18 x 18")
        cmbColumnsAndRows.Items.Item(11) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows18Columns18

        cmbColumnsAndRows.Items.Add("20 x 20")
        cmbColumnsAndRows.Items.Item(12) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows20Columns20

        cmbColumnsAndRows.Items.Add("22 x 22")
        cmbColumnsAndRows.Items.Item(13) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows22Columns22

        cmbColumnsAndRows.Items.Add("24 x 24")
        cmbColumnsAndRows.Items.Item(14) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows24Columns24

        cmbColumnsAndRows.Items.Add("26 x 26")
        cmbColumnsAndRows.Items.Item(15) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows26Columns26

        cmbColumnsAndRows.Items.Add("32 x 32")
        cmbColumnsAndRows.Items.Item(16) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows32Columns32

        cmbColumnsAndRows.Items.Add("36 x 36")
        cmbColumnsAndRows.Items.Item(17) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows36Columns36

        cmbColumnsAndRows.Items.Add("40 x 40")
        cmbColumnsAndRows.Items.Item(18) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows40Columns40

        cmbColumnsAndRows.Items.Add("44 x 44")
        cmbColumnsAndRows.Items.Item(19) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows44Columns44

        cmbColumnsAndRows.Items.Add("48 x 48")
        cmbColumnsAndRows.Items.Item(20) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows48Columns48

        cmbColumnsAndRows.Items.Add("52 x 52")
        cmbColumnsAndRows.Items.Item(21) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows52Columns52

        cmbColumnsAndRows.Items.Add("64 x 64")
        cmbColumnsAndRows.Items.Item(22) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows64Columns64

        cmbColumnsAndRows.Items.Add("72 x 72")
        cmbColumnsAndRows.Items.Item(23) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows72Columns72

        cmbColumnsAndRows.Items.Add("80 x 80")
        cmbColumnsAndRows.Items.Item(24) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows80Columns80

        cmbColumnsAndRows.Items.Add("88 x 88")
        cmbColumnsAndRows.Items.Item(25) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows88Columns88

        cmbColumnsAndRows.Items.Add("96 x 96")
        cmbColumnsAndRows.Items.Item(26) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows96Columns96

        cmbColumnsAndRows.Items.Add("104 x 104")
        cmbColumnsAndRows.Items.Item(27) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows104Columns104

        cmbColumnsAndRows.Items.Add("120 x 120")
        cmbColumnsAndRows.Items.Item(28) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows120Columns120

        cmbColumnsAndRows.Items.Add("132 x 132")
        cmbColumnsAndRows.Items.Item(29) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows132Columns132

        cmbColumnsAndRows.Items.Add("144 x 144")
        cmbColumnsAndRows.Items.Item(30) = Accusoft.BarcodeXpressSdk.DataMatrixSizes.Rows144Columns144

        cmbColumnsAndRows.SelectedIndex = 0

        lblColumnsVal.Text = "-1"
        lblRowsVal.Text = "-1"

        'Set the Barcode type combo box
        cmbBarcodeType.SelectedIndex = 0
    End Sub

    Private Sub buttonCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCreate.Click
        lblStatus.Text = ""
        Dim DIB As System.IntPtr
        Try

            'Set the rows columns and Height
            If (cmbBarcodeType.SelectedIndex = 0) Then
                UpdatePDF()
            Else
                UpdateDM()
            End If

            ' read the barcode
            Dim currentArea As System.Drawing.Rectangle = New System.Drawing.Rectangle(0, 0, 0, 0)
            BarcodeXpress1.reader.Area = currentArea
            DIB = ImageXView1.Image.ToHdib(False)
            results = BarcodeXpress1.reader.Analyze(DIB)

            If (results.Length > 0) Then
                Dim strResult As String = results.Length & " barcode(s) found." & vbNewLine & vbNewLine
                Dim i As Integer
                For i = 0 To results.Length - 1 Step 1

                    Dim curResult As Accusoft.BarcodeXpressSdk.Result = CType(results.GetValue(i), Accusoft.BarcodeXpressSdk.Result)
                    strResult = strResult & "Symbol #" & i & vbNewLine & "Type: " & curResult.BarcodeName & vbNewLine
                    strResult += "X - " + curResult.Area.X.ToString() + " pixels" + vbNewLine + "Y - " + curResult.Area.Y.ToString() + " pixels" + vbNewLine
                    strResult += "W - " + curResult.Area.Width.ToString() + " pixels" + vbNewLine + "H - " + curResult.Area.Height.ToString() + " pixels" + vbNewLine
                    strResult += "Value:" + vbNewLine + curResult.BarcodeValue + vbNewLine + vbNewLine
                    If curResult.ValidCheckSum Then
                        strResult += "Checksum OK"
                    End If

                Next

                strResult += vbNewLine
                lblResult.Text = strResult
                lblStatus.Text = "Barcode detection was successful."
                buttonPrint.Enabled = True
                buttonWrite.Enabled = True
            Else
                lblStatus.Text = "Could not detect any barcodes."
                buttonPrint.Enabled = False
                buttonWrite.Enabled = False
            End If

            'Must free the DIB passed into the Analyze method
            GlobalFree(DIB)
        Catch ex As Accusoft.BarcodeXpressSdk.BarcodeException
            Dim strResult As String
            strResult = "Error Creating Barcode: "
            strResult += ex.Message
            lblResult.Text = strResult
            lblStatus.Text = strResult

        End Try

    End Sub

    Private Sub UpdatePDF()

        Dim currentBarcodeTypes As System.Array = New Accusoft.BarcodeXpressSdk.BarcodeType(1) {}
        currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.PDF417Barcode, 0)
        BarcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes

        Dim pdfWriter As New Accusoft.BarcodeXpressSdk.WriterPDF417(BarcodeXpress1)
        If hscRows.Value < 3 Then
            pdfWriter.Rows = -1
        Else
            pdfWriter.Rows = hscRows.Value
        End If
        If hscColumns.Value < 1 Then
            pdfWriter.Columns = -1
        Else
            pdfWriter.Columns = hscColumns.Value
        End If
        pdfWriter.ErrorCorrectionLevel = CType(hscError.Value, Accusoft.BarcodeXpressSdk.PDF417ErrorCorrectionLevel)
        pdfWriter.MinimumBarWidth = hscWidth.Value
        pdfWriter.MinimumBarHeight = hscHeight.Value
        pdfWriter.AutoSize = True
        pdfWriter.BarcodeValue = TextBox1.Text

        lblStatus.Text = "Making Barcode"
        lblResult.Text = ""
        Application.DoEvents()

        ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(ImagXpress1, pdfWriter.Create())

    End Sub

    Private Sub UpdateDM()

        Dim currentBarcodeTypes As System.Array = New Accusoft.BarcodeXpressSdk.BarcodeType(1) {}
        currentBarcodeTypes.SetValue(Accusoft.BarcodeXpressSdk.BarcodeType.DataMatrixBarcode, 0)
        BarcodeXpress1.reader.BarcodeTypes = currentBarcodeTypes

        Dim dmWriter As New Accusoft.BarcodeXpressSdk.WriterDataMatrix(BarcodeXpress1)
        dmWriter.RowsAndColumns = CType(cmbColumnsAndRows.Items.Item(cmbColumnsAndRows.SelectedIndex), Accusoft.BarcodeXpressSdk.DataMatrixSizes)
        dmWriter.MinimumBarWidth = hscWidth.Value
        dmWriter.MinimumBarHeight = hscHeight.Value
        dmWriter.BarcodeValue = TextBox1.Text
        dmWriter.AutoSize = True

        lblStatus.Text = "Making Barcode"
        lblResult.Text = ""
        Application.DoEvents()

        ImageXView1.Image = Accusoft.ImagXpressSdk.ImageX.FromHdib(ImagXpress1, dmWriter.Create())

    End Sub

    Private Sub buttonWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonWrite.Click

        Dim strMsg As String
        ' Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
        ' Dim strImageFile As String = System.IO.Path.Combine(strCurrentDir, "..\2Dbarcode.TIF")
        Dim so As Accusoft.ImagXpressSdk.SaveOptions = New Accusoft.ImagXpressSdk.SaveOptions()
        Try

            so.Format = ImageXFormat.Tiff
            so.Tiff.Compression = Compression.Group4

            ImageXView1.Image.Save(Application.StartupPath + "\barcode.tif", so)
            strMsg = "Image saved as " + Application.StartupPath + "\barcode.tif"
            lblStatus.Text = strMsg

        Catch ex As Accusoft.ImagXpressSdk.ImagXpressException
            lblStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub buttonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonPrint.Click
        Try
            ' Print the Image
            Dim doc As PrintDocument = New PrintDocument()
            AddHandler doc.PrintPage, AddressOf OnPrintPage
            doc.Print()
            lblStatus.Text = "Barcode printed."
        Catch ex As Exception
            lblStatus.Text = ex.Message
        End Try
    End Sub

    Private Sub hscError_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscError.Scroll
        lblErrorVal.Text = hscError.Value.ToString()
    End Sub

    Private Sub OnPrintPage(ByVal sender As System.Object, ByVal args As PrintPageEventArgs)

        Dim proc As Accusoft.ImagXpressSdk.Processor = New Accusoft.ImagXpressSdk.Processor(ImagXpress1)

        proc.Image = ImageXView1.Image.Copy()
        proc.ColorDepth(24, 0, 0)
        ImageXView1.Image = proc.Image

        Dim g As System.Drawing.Graphics = ImageXView1.Image.GetGraphics()
        Dim img As System.Drawing.Image = CType(ImageXView1.Image.ToBitmap(False), System.Drawing.Image)
        args.Graphics.PageUnit = GraphicsUnit.Inch

        args.Graphics.DrawImage(img, CType((8.5 - img.Width / img.HorizontalResolution) / 2, Single), CType((11 - img.Height / img.VerticalResolution) / 2, Single), img.Width / img.HorizontalResolution, img.Height / img.VerticalResolution)

        ImageXView1.Image.ReleaseGraphics()

    End Sub

    Private Sub hscColumns_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscColumns.Scroll
        If hscColumns.Value < 1 Then
            lblColumnsVal.Text = "-1"
        Else
            lblColumnsVal.Text = hscColumns.Value.ToString
        End If

    End Sub

    Private Sub hscRows_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscRows.Scroll

        If hscRows.Value < 3 Then
            lblRowsVal.Text = "-1"
        Else
            lblRowsVal.Text = hscRows.Value.ToString()
        End If

    End Sub

    Private Sub hscWidth_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscWidth.Scroll
        lblWidthVal.Text = hscWidth.Value.ToString
    End Sub

    Private Sub hscHeight_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles hscHeight.Scroll
        lblHeightVal.Text = hscHeight.Value.ToString
    End Sub

    Private Sub cmbBarcodeType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBarcodeType.SelectedIndexChanged
        If (cmbBarcodeType.SelectedIndex = 0) Then
            SetPDF417RowsAndColumns()
            lblInfo.Text = "PDF417 - Supports ASCII and binary characters."
            TextBox1.Text = "PDF417"
            lblHeightVal.Visible = True
            hscHeight.Visible = True
            lblHeight.Visible = True
            lblError.Visible = True
            lblErrorVal.Visible = True
            hscError.Visible = True
        Else
            SetDataMatrixRowsAndColumns()
            lblInfo.Text = "DataMatrix - Supports ASCII and binary characters."
            TextBox1.Text = "DataMatrix"
            lblHeightVal.Visible = False
            hscHeight.Visible = False
            lblHeight.Visible = False
            lblError.Visible = False
            lblErrorVal.Visible = False
            hscError.Visible = False
        End If

        lblResult.Text = ""
        ImageXView1.Image = Nothing
        buttonPrint.Enabled = False
        buttonWrite.Enabled = False
    End Sub

    Private Sub SetPDF417RowsAndColumns()

        cmbColumnsAndRows.Visible = False
        hscColumns.Visible = True
        hscRows.Visible = True
        If hscColumns.Value < 1 Then
            lblColumnsVal.Text = "-1"
        Else
            lblColumnsVal.Text = hscColumns.Value.ToString
        End If
        If hscRows.Value < 3 Then
            lblRowsVal.Text = "-1"
        Else
            lblRowsVal.Text = hscRows.Value.ToString
        End If
        lblColumnsVal.Visible = True
        lblRowsVal.Visible = True

    End Sub
    Private Sub SetDataMatrixRowsAndColumns()

        cmbColumnsAndRows.Visible = True
        hscColumns.Visible = False
        hscRows.Visible = False
        lblColumnsVal.Visible = False
        lblRowsVal.Visible = False

    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub BarcodeXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BarcodeXpressMenuItem.Click
        BarcodeXpress1.AboutBox()
    End Sub

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        Application.Exit()
    End Sub
End Class

'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MoveForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MoveForm))
        Me.MoveTextBox = New System.Windows.Forms.TextBox
        Me.MoveLabel = New System.Windows.Forms.Label
        Me.OKButton = New System.Windows.Forms.Button
        Me.CancelMoveButton = New System.Windows.Forms.Button
        Me.ToLabel = New System.Windows.Forms.Label
        Me.ToTextBox = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'MoveTextBox
        '
        Me.MoveTextBox.Location = New System.Drawing.Point(126, 21)
        Me.MoveTextBox.Name = "MoveTextBox"
        Me.MoveTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MoveTextBox.TabIndex = 1
        Me.MoveTextBox.Text = "1"
        '
        'MoveLabel
        '
        Me.MoveLabel.AutoSize = True
        Me.MoveLabel.Location = New System.Drawing.Point(12, 21)
        Me.MoveLabel.Name = "MoveLabel"
        Me.MoveLabel.Size = New System.Drawing.Size(102, 13)
        Me.MoveLabel.TabIndex = 0
        Me.MoveLabel.Text = "Move Page Number"
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(15, 85)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 23)
        Me.OKButton.TabIndex = 4
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'CancelMoveButton
        '
        Me.CancelMoveButton.Location = New System.Drawing.Point(157, 85)
        Me.CancelMoveButton.Name = "CancelMoveButton"
        Me.CancelMoveButton.Size = New System.Drawing.Size(75, 23)
        Me.CancelMoveButton.TabIndex = 5
        Me.CancelMoveButton.Text = "Cancel"
        Me.CancelMoveButton.UseVisualStyleBackColor = True
        '
        'ToLabel
        '
        Me.ToLabel.AutoSize = True
        Me.ToLabel.Location = New System.Drawing.Point(12, 47)
        Me.ToLabel.Name = "ToLabel"
        Me.ToLabel.Size = New System.Drawing.Size(88, 13)
        Me.ToLabel.TabIndex = 2
        Me.ToLabel.Text = "To Page Number"
        '
        'ToTextBox
        '
        Me.ToTextBox.Location = New System.Drawing.Point(126, 47)
        Me.ToTextBox.Name = "ToTextBox"
        Me.ToTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ToTextBox.TabIndex = 3
        Me.ToTextBox.Text = "1"
        '
        'MoveForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(244, 120)
        Me.Controls.Add(Me.ToLabel)
        Me.Controls.Add(Me.ToTextBox)
        Me.Controls.Add(Me.CancelMoveButton)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.MoveLabel)
        Me.Controls.Add(Me.MoveTextBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MoveForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Move Page"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MoveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MoveLabel As System.Windows.Forms.Label
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents CancelMoveButton As System.Windows.Forms.Button
    Friend WithEvents ToLabel As System.Windows.Forms.Label
    Friend WithEvents ToTextBox As System.Windows.Forms.TextBox
End Class

/// Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( )
        {
            Application.EnableVisualStyles( );
            Application.SetCompatibleTextRenderingDefault( false );
            Application.Run( new ManipulatePdfAnnotationsForm( ) );
        }
    }
}
/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using System.Runtime.InteropServices;
using Accusoft.ImagXpressSdk;
using Accusoft.PdfXpressSdk;
using Accusoft.NotateXpressSdk;
using System.IO;
using System.Collections.Generic;

namespace PDFDemo
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class FormMain : System.Windows.Forms.Form
    {
        private System.Windows.Forms.MainMenu mainMenu;
        private System.ComponentModel.IContainer components;
        private ToolStripMenuItem helpToolStripMenuItem1;
        private ToolStripButton OpenButton;
        private ToolStripSeparator OpenCloseSeparator;
        private ToolStripButton PreviousButton;
        private ToolStripButton NextButton;
        private ToolStripTextBox PageBox;
        private ToolStripSeparator PageFindSeparator;
        private ToolStripButton AboutButton;
        private ToolStrip toolStrip;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem newToolStripMenuItem;
        private ToolStripMenuItem openToolStripMenuItem;
        private ToolStripMenuItem exportAnnotationsToXFDFToolStripMenuItem;
        private ToolStripMenuItem saveMenuItem;
        private ToolStripMenuItem saveAsToolStripMenuItem;
        private ToolStripMenuItem closeToolStripMenuItem;
        private ToolStripSeparator CloseExitSeparator;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem optionsToolStripMenuItem;
        private ToolStripMenuItem addImageToolStripMenuItem;
        private ToolStripMenuItem createThumbnailToolStripMenuItem;
        private ToolStripMenuItem deleteThumbnailToolStripMenuItem;
        private ToolStripMenuItem editBookmarksToolStripMenuItem;
        private ToolStripMenuItem insertPageToolStripMenuItem;
        private ToolStripMenuItem metaDataToolStripMenuItem;
        private ToolStripMenuItem movePageToolStripMenuItem;
        private ToolStripMenuItem setRenderOptionsToolStripMenuItem;
        private ToolStripMenuItem viewToolStripMenuItem;
        private ToolStripMenuItem toolbarToolStripMenuItem;
        private ToolStripMenuItem statusBarToolStripMenuItem;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private ToolStripMenuItem imagXpressToolStripMenuItem;
        private ToolStripMenuItem pdfXpressToolStripMenuItem;
        private MenuStrip menuStrip;
        private ToolStripMenuItem notateXpressToolStripMenuItem;
        private ToolStripComboBox zoomComboBox;
        private ToolStripMenuItem deletePageToolStripMenuItem;
        private ToolStripLabel AnnotationLabel;
        private ToolStripButton PointerTool;
        private ToolStripButton noteTool;
        private ToolStripButton highlightTool;
        private ToolStripButton TextTool;
        private ToolStripButton ArrowTool;
        private ToolStripButton LineTool;
        private ToolStripButton RectangleTool;
        private ToolStripButton OvalTool;
        private ToolStripButton PolyLineTool;
        private ToolStripButton PolygonTool;
        private ToolStripButton PencilTool;
        private ToolStripMenuItem importAnnotationsToXFDFToolStripMenuItem;
        private ToolStripLabel PrePageLabel;
        private ToolStripLabel CountBox;
        private HelpProvider helpProvider1;
        private StatusStrip statusBarMain;
        private Panel panelLeft;
        private Panel panelBookmarks;
        private GroupBox groupBoxBookmarks;
        private TreeView treeViewBookmarks;
        private Panel panelText;
        private GroupBox groupBoxFind;
        private Label FindLabelInstructions;
        private CheckBox WholeCheckBox;
        private Label wordsAfterLabel;
        private NumericUpDown afterWordsBox;
        private Label wordsBeforelabel;
        private NumericUpDown beforeWordsBox;
        private Label FindLabel;
        private Button NextTextButton;
        private Button PreviousTextButton;
        private TextBox FindBox;
        private GroupBox groupBoxText;
        private RichTextBox textSearchResultsBox;
        private Panel panelMain;
        private GroupBox groupBoxMain;
        private ImageXView imageXView;
        private ToolStripStatusLabel StatusBarLabel;
        private ToolStripSeparator ImagePageSeparator;
        private ToolStripSeparator PageBookmarkSeparator;
        private ToolStripSeparator MetadataRenderSeparator;
        private ToolStripSeparator RenderThumbnailSeparator;
        private ToolStripButton[] AnnotationToolStrip;
        private ToolStripSeparator AnnotationAboutSeparator;
        private Splitter splitter;
        private ToolStripSeparator ThumbnailWatermarkSeparator;
        private ToolStripMenuItem addWatermarkToolStripMenuItem;
        private ToolStripMenuItem removeWatermarkToolStripMenuItem;
        private PdfXpress pdfXpress1;
        private ToolStripMenuItem savePdfAMenuItem;

        public FormMain()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        private List<TextMatch[]> matches;

        private XfdfOptions xfdfOptions;
        private ImageFitSettings destinationFit;
        private Document document;
        private RenderOptions rendOpts;

        private int pageNumber, pageCount;
        private int wordChosen;
        public int destinationX, destinationY, destinationW, destinationH;

        private string openPath = "", iccPath = "", savePath = "", strFileName = "", 
                       strTempFileName = "", strTempSavedFileName = "";

        private List<string> strTempSavedFileNames = new List<string>();

        private TreeNode nodeSelected;

        private ArrayList bookmarks;
        private Accusoft.NotateXpressSdk.NotateXpress notateXpress1;

        private Accusoft.PdfXpressSdk.SaveOptions so;

        private bool useXFDF;
        private Accusoft.NotateXpressSdk.LoadOptions lo;

        private int map;

        private OpenOptions oo;

        private bool underLine, strikeOut;

        private List<byte[]> layerData;

        private byte[] xfdfOriginal;

        private List<Rectangle[]> highlights;

        Helper helper = new Helper();

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (notateXpress1 != null)
                {
                    if (notateXpress1.Layers != null)
                    {
                        for (int i = 0; i < notateXpress1.Layers.Count; i++)
                        {
                            if (notateXpress1.Layers[i] != null)
                            {
                                if (notateXpress1.Layers[i].Elements != null)
                                {
                                    notateXpress1.Layers[i].Elements.Clear();
                                    notateXpress1.Layers[i].Elements.Dispose();
                                }
                            }
                        }

                        notateXpress1.Layers.Dispose();
                    }

                    notateXpress1.ClientDisconnect();
                    notateXpress1.Dispose();
                    notateXpress1 = null;
                }
                if (imageXView != null)
                {
                    if (imageXView.Image != null)
                    {
                        imageXView.Image.Dispose();
                        imageXView.Image = null;
                    }
                    imageXView.Dispose();
                    imageXView = null;
                }
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (document != null)
                {
                    document.Dispose();
                }
                if (bookmarks != null)
                {
                    for (int i = 0; i < bookmarks.Count; i++)
                    {
                        if (bookmarks[i] != null)
                        {
                            ((BookmarkContext)bookmarks[i]).Dispose();
                            bookmarks[i] = null;
                        }
                    }
                }
                if (pdfXpress1 != null)
                {
                    if (pdfXpress1.Documents != null)
                    {
                        for (int i = 0; i < pdfXpress1.Documents.Count; i++)
                        {
                            if (pdfXpress1.Documents[i] != null)
                                pdfXpress1.Documents[i].Dispose();
                        }

                        pdfXpress1.Documents.Dispose();
                    }
                    pdfXpress1.Dispose();
                    pdfXpress1 = null;
                }

                helper.SafelyDeleteFile(strTempFileName);   

                helper.SafelyDeleteFiles(strTempSavedFileNames.ToArray());             

                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenButton = new System.Windows.Forms.ToolStripButton();
            this.OpenCloseSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.PreviousButton = new System.Windows.Forms.ToolStripButton();
            this.NextButton = new System.Windows.Forms.ToolStripButton();
            this.PageBox = new System.Windows.Forms.ToolStripTextBox();
            this.PageFindSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.AboutButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.PrePageLabel = new System.Windows.Forms.ToolStripLabel();
            this.CountBox = new System.Windows.Forms.ToolStripLabel();
            this.zoomComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.AnnotationLabel = new System.Windows.Forms.ToolStripLabel();
            this.PointerTool = new System.Windows.Forms.ToolStripButton();
            this.noteTool = new System.Windows.Forms.ToolStripButton();
            this.highlightTool = new System.Windows.Forms.ToolStripButton();
            this.TextTool = new System.Windows.Forms.ToolStripButton();
            this.ArrowTool = new System.Windows.Forms.ToolStripButton();
            this.LineTool = new System.Windows.Forms.ToolStripButton();
            this.RectangleTool = new System.Windows.Forms.ToolStripButton();
            this.OvalTool = new System.Windows.Forms.ToolStripButton();
            this.PolyLineTool = new System.Windows.Forms.ToolStripButton();
            this.PolygonTool = new System.Windows.Forms.ToolStripButton();
            this.PencilTool = new System.Windows.Forms.ToolStripButton();
            this.AnnotationAboutSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.notateXpress1 = new Accusoft.NotateXpressSdk.NotateXpress(this.components);
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAnnotationsToXFDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importAnnotationsToXFDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePdfAMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseExitSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImagePageSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.insertPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PageBookmarkSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.editBookmarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metaDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MetadataRenderSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.setRenderOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RenderThumbnailSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.createThumbnailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteThumbnailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ThumbnailWatermarkSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.addWatermarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeWatermarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pdfXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.statusBarMain = new System.Windows.Forms.StatusStrip();
            this.StatusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelBookmarks = new System.Windows.Forms.Panel();
            this.groupBoxBookmarks = new System.Windows.Forms.GroupBox();
            this.treeViewBookmarks = new System.Windows.Forms.TreeView();
            this.panelText = new System.Windows.Forms.Panel();
            this.groupBoxFind = new System.Windows.Forms.GroupBox();
            this.FindLabelInstructions = new System.Windows.Forms.Label();
            this.WholeCheckBox = new System.Windows.Forms.CheckBox();
            this.wordsAfterLabel = new System.Windows.Forms.Label();
            this.afterWordsBox = new System.Windows.Forms.NumericUpDown();
            this.wordsBeforelabel = new System.Windows.Forms.Label();
            this.beforeWordsBox = new System.Windows.Forms.NumericUpDown();
            this.FindLabel = new System.Windows.Forms.Label();
            this.NextTextButton = new System.Windows.Forms.Button();
            this.PreviousTextButton = new System.Windows.Forms.Button();
            this.FindBox = new System.Windows.Forms.TextBox();
            this.groupBoxText = new System.Windows.Forms.GroupBox();
            this.textSearchResultsBox = new System.Windows.Forms.RichTextBox();
            this.panelMain = new System.Windows.Forms.Panel();
            this.groupBoxMain = new System.Windows.Forms.GroupBox();
            this.imageXView = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.splitter = new System.Windows.Forms.Splitter();
            this.pdfXpress1 = new Accusoft.PdfXpressSdk.PdfXpress();
            this.toolStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.statusBarMain.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelBookmarks.SuspendLayout();
            this.groupBoxBookmarks.SuspendLayout();
            this.panelText.SuspendLayout();
            this.groupBoxFind.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.afterWordsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beforeWordsBox)).BeginInit();
            this.groupBoxText.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.groupBoxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // OpenButton
            // 
            this.OpenButton.AutoSize = false;
            this.OpenButton.Image = ((System.Drawing.Image)(resources.GetObject("OpenButton.Image")));
            this.OpenButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(50, 50);
            this.OpenButton.Text = "Open";
            this.OpenButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.OpenButton.ToolTipText = "Open Image";
            this.OpenButton.Click += new System.EventHandler(this.OpenButton_Click);
            // 
            // OpenCloseSeparator
            // 
            this.OpenCloseSeparator.Name = "OpenCloseSeparator";
            this.OpenCloseSeparator.Size = new System.Drawing.Size(6, 53);
            // 
            // PreviousButton
            // 
            this.PreviousButton.Enabled = false;
            this.PreviousButton.Image = ((System.Drawing.Image)(resources.GetObject("PreviousButton.Image")));
            this.PreviousButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PreviousButton.Name = "PreviousButton";
            this.PreviousButton.Size = new System.Drawing.Size(85, 50);
            this.PreviousButton.Text = "Previous Page";
            this.PreviousButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PreviousButton.Click += new System.EventHandler(this.PreviousButton_Click);
            // 
            // NextButton
            // 
            this.NextButton.Enabled = false;
            this.NextButton.Image = ((System.Drawing.Image)(resources.GetObject("NextButton.Image")));
            this.NextButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(64, 50);
            this.NextButton.Text = "Next Page";
            this.NextButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // PageBox
            // 
            this.PageBox.Enabled = false;
            this.PageBox.Name = "PageBox";
            this.PageBox.Size = new System.Drawing.Size(45, 53);
            this.PageBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PageBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PageBox_KeyUp);
            // 
            // PageFindSeparator
            // 
            this.PageFindSeparator.Name = "PageFindSeparator";
            this.PageFindSeparator.Size = new System.Drawing.Size(6, 53);
            // 
            // AboutButton
            // 
            this.AboutButton.AutoSize = false;
            this.AboutButton.Image = ((System.Drawing.Image)(resources.GetObject("AboutButton.Image")));
            this.AboutButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(50, 50);
            this.AboutButton.Text = "About";
            this.AboutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AboutButton.ToolTipText = "About PDF Xpress";
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.AutoSize = false;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenButton,
            this.OpenCloseSeparator,
            this.PreviousButton,
            this.NextButton,
            this.PrePageLabel,
            this.PageBox,
            this.CountBox,
            this.zoomComboBox,
            this.PageFindSeparator,
            this.AnnotationLabel,
            this.PointerTool,
            this.noteTool,
            this.highlightTool,
            this.TextTool,
            this.ArrowTool,
            this.LineTool,
            this.RectangleTool,
            this.OvalTool,
            this.PolyLineTool,
            this.PolygonTool,
            this.PencilTool,
            this.AnnotationAboutSeparator,
            this.AboutButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1161, 53);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // PrePageLabel
            // 
            this.PrePageLabel.Name = "PrePageLabel";
            this.PrePageLabel.Size = new System.Drawing.Size(36, 50);
            this.PrePageLabel.Text = "Page:";
            // 
            // CountBox
            // 
            this.CountBox.Name = "CountBox";
            this.CountBox.Size = new System.Drawing.Size(0, 50);
            // 
            // zoomComboBox
            // 
            this.zoomComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.zoomComboBox.Enabled = false;
            this.zoomComboBox.Items.AddRange(new object[] {
            "Zoom to Best Fit",
            "Zoom to 1:1 Ratio",
            "Zoom to Fit Width",
            "Zoom to Fit Height"});
            this.zoomComboBox.Name = "zoomComboBox";
            this.zoomComboBox.Size = new System.Drawing.Size(121, 53);
            this.zoomComboBox.SelectedIndexChanged += new System.EventHandler(this.zoomComboBox_SelectedIndexChanged);
            // 
            // AnnotationLabel
            // 
            this.AnnotationLabel.Name = "AnnotationLabel";
            this.AnnotationLabel.Size = new System.Drawing.Size(72, 50);
            this.AnnotationLabel.Text = "Annotations";
            // 
            // PointerTool
            // 
            this.PointerTool.AutoSize = false;
            this.PointerTool.Checked = true;
            this.PointerTool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PointerTool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PointerTool.Enabled = false;
            this.PointerTool.Image = ((System.Drawing.Image)(resources.GetObject("PointerTool.Image")));
            this.PointerTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PointerTool.Name = "PointerTool";
            this.PointerTool.Size = new System.Drawing.Size(55, 45);
            this.PointerTool.Text = "Pointer Tool";
            this.PointerTool.Click += new System.EventHandler(this.pointerTool_Click);
            // 
            // noteTool
            // 
            this.noteTool.AutoSize = false;
            this.noteTool.CheckOnClick = true;
            this.noteTool.Enabled = false;
            this.noteTool.Image = ((System.Drawing.Image)(resources.GetObject("noteTool.Image")));
            this.noteTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.noteTool.Name = "noteTool";
            this.noteTool.Size = new System.Drawing.Size(55, 45);
            this.noteTool.Text = "Note";
            this.noteTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.noteTool.Click += new System.EventHandler(this.noteTool_Click);
            // 
            // highlightTool
            // 
            this.highlightTool.AutoSize = false;
            this.highlightTool.CheckOnClick = true;
            this.highlightTool.Enabled = false;
            this.highlightTool.Image = ((System.Drawing.Image)(resources.GetObject("highlightTool.Image")));
            this.highlightTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.highlightTool.Name = "highlightTool";
            this.highlightTool.Size = new System.Drawing.Size(55, 45);
            this.highlightTool.Text = "Highlight";
            this.highlightTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.highlightTool.Click += new System.EventHandler(this.highlightTool_Click);
            // 
            // TextTool
            // 
            this.TextTool.AutoSize = false;
            this.TextTool.CheckOnClick = true;
            this.TextTool.Enabled = false;
            this.TextTool.Image = ((System.Drawing.Image)(resources.GetObject("TextTool.Image")));
            this.TextTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TextTool.Name = "TextTool";
            this.TextTool.Size = new System.Drawing.Size(55, 45);
            this.TextTool.Text = "Text";
            this.TextTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.TextTool.Click += new System.EventHandler(this.TextTool_Click);
            // 
            // ArrowTool
            // 
            this.ArrowTool.AutoSize = false;
            this.ArrowTool.CheckOnClick = true;
            this.ArrowTool.Enabled = false;
            this.ArrowTool.Image = ((System.Drawing.Image)(resources.GetObject("ArrowTool.Image")));
            this.ArrowTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ArrowTool.Name = "ArrowTool";
            this.ArrowTool.Size = new System.Drawing.Size(55, 45);
            this.ArrowTool.Text = "Arrow";
            this.ArrowTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ArrowTool.Click += new System.EventHandler(this.ArrowTool_Click);
            // 
            // LineTool
            // 
            this.LineTool.AutoSize = false;
            this.LineTool.CheckOnClick = true;
            this.LineTool.Enabled = false;
            this.LineTool.Image = ((System.Drawing.Image)(resources.GetObject("LineTool.Image")));
            this.LineTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LineTool.Name = "LineTool";
            this.LineTool.Size = new System.Drawing.Size(55, 45);
            this.LineTool.Text = "Line";
            this.LineTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.LineTool.Click += new System.EventHandler(this.LineTool_Click);
            // 
            // RectangleTool
            // 
            this.RectangleTool.AutoSize = false;
            this.RectangleTool.CheckOnClick = true;
            this.RectangleTool.Enabled = false;
            this.RectangleTool.Image = ((System.Drawing.Image)(resources.GetObject("RectangleTool.Image")));
            this.RectangleTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RectangleTool.Name = "RectangleTool";
            this.RectangleTool.Size = new System.Drawing.Size(55, 45);
            this.RectangleTool.Text = "Rectangle";
            this.RectangleTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.RectangleTool.Click += new System.EventHandler(this.RectangleTool_Click);
            // 
            // OvalTool
            // 
            this.OvalTool.AutoSize = false;
            this.OvalTool.CheckOnClick = true;
            this.OvalTool.Enabled = false;
            this.OvalTool.Image = ((System.Drawing.Image)(resources.GetObject("OvalTool.Image")));
            this.OvalTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OvalTool.Name = "OvalTool";
            this.OvalTool.Size = new System.Drawing.Size(55, 45);
            this.OvalTool.Text = "Ellipse";
            this.OvalTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.OvalTool.Click += new System.EventHandler(this.OvalTool_Click);
            // 
            // PolyLineTool
            // 
            this.PolyLineTool.AutoSize = false;
            this.PolyLineTool.CheckOnClick = true;
            this.PolyLineTool.Enabled = false;
            this.PolyLineTool.Image = ((System.Drawing.Image)(resources.GetObject("PolyLineTool.Image")));
            this.PolyLineTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PolyLineTool.Name = "PolyLineTool";
            this.PolyLineTool.Size = new System.Drawing.Size(55, 45);
            this.PolyLineTool.Text = "Polyline";
            this.PolyLineTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PolyLineTool.Click += new System.EventHandler(this.PolyLineTool_Click);
            // 
            // PolygonTool
            // 
            this.PolygonTool.AutoSize = false;
            this.PolygonTool.CheckOnClick = true;
            this.PolygonTool.Enabled = false;
            this.PolygonTool.Image = ((System.Drawing.Image)(resources.GetObject("PolygonTool.Image")));
            this.PolygonTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PolygonTool.Name = "PolygonTool";
            this.PolygonTool.Size = new System.Drawing.Size(55, 45);
            this.PolygonTool.Text = "Polygon";
            this.PolygonTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PolygonTool.Click += new System.EventHandler(this.PolygonTool_Click);
            // 
            // PencilTool
            // 
            this.PencilTool.AutoSize = false;
            this.PencilTool.CheckOnClick = true;
            this.PencilTool.Enabled = false;
            this.PencilTool.Image = ((System.Drawing.Image)(resources.GetObject("PencilTool.Image")));
            this.PencilTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PencilTool.Name = "PencilTool";
            this.PencilTool.Size = new System.Drawing.Size(55, 45);
            this.PencilTool.Text = "Pencil";
            this.PencilTool.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PencilTool.Click += new System.EventHandler(this.PencilTool_Click);
            // 
            // AnnotationAboutSeparator
            // 
            this.AnnotationAboutSeparator.Name = "AnnotationAboutSeparator";
            this.AnnotationAboutSeparator.Size = new System.Drawing.Size(6, 53);
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            this.notateXpress1.AnnotationAdded += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
            this.notateXpress1.AnnotationDeleted += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
            this.notateXpress1.AnnotationMoved += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationMovedEventHandler(this.notateXpress1_AnnotationMoved);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exportAnnotationsToXFDFToolStripMenuItem,
            this.importAnnotationsToXFDFToolStripMenuItem,
            this.saveMenuItem,
            this.savePdfAMenuItem,
            this.saveAsToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.CloseExitSeparator,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.newToolStripMenuItem.Text = "&New PDF Document";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.openToolStripMenuItem.Text = "&Open PDF Document";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exportAnnotationsToXFDFToolStripMenuItem
            // 
            this.exportAnnotationsToXFDFToolStripMenuItem.Enabled = false;
            this.exportAnnotationsToXFDFToolStripMenuItem.Name = "exportAnnotationsToXFDFToolStripMenuItem";
            this.exportAnnotationsToXFDFToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.exportAnnotationsToXFDFToolStripMenuItem.Text = "&Export Annotations to XFDF";
            this.exportAnnotationsToXFDFToolStripMenuItem.Click += new System.EventHandler(this.exportAnnotationsToXFDFToolStripMenuItem_Click);
            // 
            // importAnnotationsToXFDFToolStripMenuItem
            // 
            this.importAnnotationsToXFDFToolStripMenuItem.Enabled = false;
            this.importAnnotationsToXFDFToolStripMenuItem.Name = "importAnnotationsToXFDFToolStripMenuItem";
            this.importAnnotationsToXFDFToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.importAnnotationsToXFDFToolStripMenuItem.Text = "I&mport Annotations from XFDF";
            this.importAnnotationsToXFDFToolStripMenuItem.Click += new System.EventHandler(this.importAnnotationsToXFDFToolStripMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Enabled = false;
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(314, 22);
            this.saveMenuItem.Text = "&Save PDF Document";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // savePdfAMenuItem
            // 
            this.savePdfAMenuItem.Enabled = false;
            this.savePdfAMenuItem.Name = "savePdfAMenuItem";
            this.savePdfAMenuItem.Size = new System.Drawing.Size(314, 22);
            this.savePdfAMenuItem.Text = "Save PDF Document as &Image-only PDF/A-1b";
            this.savePdfAMenuItem.Click += new System.EventHandler(this.savePdfAMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.saveAsToolStripMenuItem.Text = "Save PDF Document &As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Enabled = false;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.closeToolStripMenuItem.Text = "&Close PDF Document";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // CloseExitSeparator
            // 
            this.CloseExitSeparator.Name = "CloseExitSeparator";
            this.CloseExitSeparator.Size = new System.Drawing.Size(311, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(314, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addImageToolStripMenuItem,
            this.ImagePageSeparator,
            this.insertPageToolStripMenuItem,
            this.deletePageToolStripMenuItem,
            this.movePageToolStripMenuItem,
            this.PageBookmarkSeparator,
            this.editBookmarksToolStripMenuItem,
            this.metaDataToolStripMenuItem,
            this.MetadataRenderSeparator,
            this.setRenderOptionsToolStripMenuItem,
            this.RenderThumbnailSeparator,
            this.createThumbnailToolStripMenuItem,
            this.deleteThumbnailToolStripMenuItem,
            this.ThumbnailWatermarkSeparator,
            this.addWatermarkToolStripMenuItem,
            this.removeWatermarkToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.optionsToolStripMenuItem.Text = "&Tools";
            // 
            // addImageToolStripMenuItem
            // 
            this.addImageToolStripMenuItem.Enabled = false;
            this.addImageToolStripMenuItem.Name = "addImageToolStripMenuItem";
            this.addImageToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.addImageToolStripMenuItem.Text = "&Add Image";
            this.addImageToolStripMenuItem.Click += new System.EventHandler(this.addImageToolStripMenuItem_Click);
            // 
            // ImagePageSeparator
            // 
            this.ImagePageSeparator.Name = "ImagePageSeparator";
            this.ImagePageSeparator.Size = new System.Drawing.Size(175, 6);
            // 
            // insertPageToolStripMenuItem
            // 
            this.insertPageToolStripMenuItem.Enabled = false;
            this.insertPageToolStripMenuItem.Name = "insertPageToolStripMenuItem";
            this.insertPageToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.insertPageToolStripMenuItem.Text = "&Insert Page";
            this.insertPageToolStripMenuItem.Click += new System.EventHandler(this.insertPageToolStripMenuItem_Click);
            // 
            // deletePageToolStripMenuItem
            // 
            this.deletePageToolStripMenuItem.Enabled = false;
            this.deletePageToolStripMenuItem.Name = "deletePageToolStripMenuItem";
            this.deletePageToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.deletePageToolStripMenuItem.Text = "&Delete Page";
            this.deletePageToolStripMenuItem.Click += new System.EventHandler(this.deletePageToolStripMenuItem_Click);
            // 
            // movePageToolStripMenuItem
            // 
            this.movePageToolStripMenuItem.Enabled = false;
            this.movePageToolStripMenuItem.Name = "movePageToolStripMenuItem";
            this.movePageToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.movePageToolStripMenuItem.Text = "M&ove Page";
            this.movePageToolStripMenuItem.Click += new System.EventHandler(this.movePageToolStripMenuItem_Click);
            // 
            // PageBookmarkSeparator
            // 
            this.PageBookmarkSeparator.Name = "PageBookmarkSeparator";
            this.PageBookmarkSeparator.Size = new System.Drawing.Size(175, 6);
            // 
            // editBookmarksToolStripMenuItem
            // 
            this.editBookmarksToolStripMenuItem.Enabled = false;
            this.editBookmarksToolStripMenuItem.Name = "editBookmarksToolStripMenuItem";
            this.editBookmarksToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.editBookmarksToolStripMenuItem.Text = "Edit &Bookmarks";
            this.editBookmarksToolStripMenuItem.Click += new System.EventHandler(this.editBookmarksToolStripMenuItem_Click);
            // 
            // metaDataToolStripMenuItem
            // 
            this.metaDataToolStripMenuItem.Enabled = false;
            this.metaDataToolStripMenuItem.Name = "metaDataToolStripMenuItem";
            this.metaDataToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.metaDataToolStripMenuItem.Text = "Edit &MetaData";
            this.metaDataToolStripMenuItem.Click += new System.EventHandler(this.metaDataToolStripMenuItem_Click);
            // 
            // MetadataRenderSeparator
            // 
            this.MetadataRenderSeparator.Name = "MetadataRenderSeparator";
            this.MetadataRenderSeparator.Size = new System.Drawing.Size(175, 6);
            // 
            // setRenderOptionsToolStripMenuItem
            // 
            this.setRenderOptionsToolStripMenuItem.Enabled = false;
            this.setRenderOptionsToolStripMenuItem.Name = "setRenderOptionsToolStripMenuItem";
            this.setRenderOptionsToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.setRenderOptionsToolStripMenuItem.Text = "Set &Render Options";
            this.setRenderOptionsToolStripMenuItem.Click += new System.EventHandler(this.setRenderOptionsToolStripMenuItem_Click);
            // 
            // RenderThumbnailSeparator
            // 
            this.RenderThumbnailSeparator.Name = "RenderThumbnailSeparator";
            this.RenderThumbnailSeparator.Size = new System.Drawing.Size(175, 6);
            // 
            // createThumbnailToolStripMenuItem
            // 
            this.createThumbnailToolStripMenuItem.Enabled = false;
            this.createThumbnailToolStripMenuItem.Name = "createThumbnailToolStripMenuItem";
            this.createThumbnailToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.createThumbnailToolStripMenuItem.Text = "&Create Thumbnail";
            this.createThumbnailToolStripMenuItem.Click += new System.EventHandler(this.createThumbnailToolStripMenuItem_Click);
            // 
            // deleteThumbnailToolStripMenuItem
            // 
            this.deleteThumbnailToolStripMenuItem.Enabled = false;
            this.deleteThumbnailToolStripMenuItem.Name = "deleteThumbnailToolStripMenuItem";
            this.deleteThumbnailToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.deleteThumbnailToolStripMenuItem.Text = "Delete &Thumbnail";
            this.deleteThumbnailToolStripMenuItem.Click += new System.EventHandler(this.deleteThumbnailToolStripMenuItem_Click);
            // 
            // ThumbnailWatermarkSeparator
            // 
            this.ThumbnailWatermarkSeparator.Name = "ThumbnailWatermarkSeparator";
            this.ThumbnailWatermarkSeparator.Size = new System.Drawing.Size(175, 6);
            // 
            // addWatermarkToolStripMenuItem
            // 
            this.addWatermarkToolStripMenuItem.Enabled = false;
            this.addWatermarkToolStripMenuItem.Name = "addWatermarkToolStripMenuItem";
            this.addWatermarkToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.addWatermarkToolStripMenuItem.Text = "Add &Watermark";
            this.addWatermarkToolStripMenuItem.Click += new System.EventHandler(this.addWatermarkToolStripMenuItem_Click);
            // 
            // removeWatermarkToolStripMenuItem
            // 
            this.removeWatermarkToolStripMenuItem.Enabled = false;
            this.removeWatermarkToolStripMenuItem.Name = "removeWatermarkToolStripMenuItem";
            this.removeWatermarkToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.removeWatermarkToolStripMenuItem.Text = "Remove Watermar&k";
            this.removeWatermarkToolStripMenuItem.Click += new System.EventHandler(this.removeWatermarkToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // toolbarToolStripMenuItem
            // 
            this.toolbarToolStripMenuItem.Checked = true;
            this.toolbarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolbarToolStripMenuItem.Name = "toolbarToolStripMenuItem";
            this.toolbarToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.toolbarToolStripMenuItem.Text = "&Toolbar";
            this.toolbarToolStripMenuItem.Click += new System.EventHandler(this.toolbarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.statusBarToolStripMenuItem.Text = "&Statusbar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.statusBarToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imagXpressToolStripMenuItem,
            this.notateXpressToolStripMenuItem,
            this.pdfXpressToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.imagXpressToolStripMenuItem.Text = "Imag&Xpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click);
            // 
            // notateXpressToolStripMenuItem
            // 
            this.notateXpressToolStripMenuItem.Name = "notateXpressToolStripMenuItem";
            this.notateXpressToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.notateXpressToolStripMenuItem.Text = "&NotateXpress";
            this.notateXpressToolStripMenuItem.Click += new System.EventHandler(this.notateXpressToolStripMenuItem_Click);
            // 
            // pdfXpressToolStripMenuItem
            // 
            this.pdfXpressToolStripMenuItem.Name = "pdfXpressToolStripMenuItem";
            this.pdfXpressToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pdfXpressToolStripMenuItem.Text = "&PDF Xpress";
            this.pdfXpressToolStripMenuItem.Click += new System.EventHandler(this.pdfXpressToolStripMenuItem_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1161, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // statusBarMain
            // 
            this.statusBarMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBarLabel});
            this.statusBarMain.Location = new System.Drawing.Point(0, 581);
            this.statusBarMain.Name = "statusBarMain";
            this.statusBarMain.Size = new System.Drawing.Size(1161, 22);
            this.statusBarMain.TabIndex = 10;
            this.statusBarMain.Text = "statusStrip1";
            // 
            // StatusBarLabel
            // 
            this.StatusBarLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBarLabel.Name = "StatusBarLabel";
            this.StatusBarLabel.Size = new System.Drawing.Size(130, 17);
            this.StatusBarLabel.Text = "toolStripStatusLabel1";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelBookmarks);
            this.panelLeft.Controls.Add(this.panelText);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 77);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(325, 504);
            this.panelLeft.TabIndex = 11;
            // 
            // panelBookmarks
            // 
            this.panelBookmarks.Controls.Add(this.groupBoxBookmarks);
            this.panelBookmarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBookmarks.Location = new System.Drawing.Point(0, 238);
            this.panelBookmarks.Name = "panelBookmarks";
            this.panelBookmarks.Size = new System.Drawing.Size(325, 266);
            this.panelBookmarks.TabIndex = 3;
            // 
            // groupBoxBookmarks
            // 
            this.groupBoxBookmarks.Controls.Add(this.treeViewBookmarks);
            this.groupBoxBookmarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxBookmarks.Location = new System.Drawing.Point(0, 0);
            this.groupBoxBookmarks.Name = "groupBoxBookmarks";
            this.groupBoxBookmarks.Size = new System.Drawing.Size(325, 266);
            this.groupBoxBookmarks.TabIndex = 0;
            this.groupBoxBookmarks.TabStop = false;
            this.groupBoxBookmarks.Text = "Bookmarks";
            // 
            // treeViewBookmarks
            // 
            this.treeViewBookmarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewBookmarks.Location = new System.Drawing.Point(3, 16);
            this.treeViewBookmarks.Name = "treeViewBookmarks";
            this.treeViewBookmarks.Size = new System.Drawing.Size(319, 247);
            this.treeViewBookmarks.TabIndex = 0;
            this.treeViewBookmarks.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // panelText
            // 
            this.panelText.Controls.Add(this.groupBoxFind);
            this.panelText.Controls.Add(this.groupBoxText);
            this.panelText.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelText.Location = new System.Drawing.Point(0, 0);
            this.panelText.Name = "panelText";
            this.panelText.Size = new System.Drawing.Size(325, 238);
            this.panelText.TabIndex = 3;
            // 
            // groupBoxFind
            // 
            this.groupBoxFind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFind.Controls.Add(this.FindLabelInstructions);
            this.groupBoxFind.Controls.Add(this.WholeCheckBox);
            this.groupBoxFind.Controls.Add(this.wordsAfterLabel);
            this.groupBoxFind.Controls.Add(this.afterWordsBox);
            this.groupBoxFind.Controls.Add(this.wordsBeforelabel);
            this.groupBoxFind.Controls.Add(this.beforeWordsBox);
            this.groupBoxFind.Controls.Add(this.FindLabel);
            this.groupBoxFind.Controls.Add(this.NextTextButton);
            this.groupBoxFind.Controls.Add(this.PreviousTextButton);
            this.groupBoxFind.Controls.Add(this.FindBox);
            this.groupBoxFind.Location = new System.Drawing.Point(6, 7);
            this.groupBoxFind.Name = "groupBoxFind";
            this.groupBoxFind.Size = new System.Drawing.Size(315, 126);
            this.groupBoxFind.TabIndex = 2;
            this.groupBoxFind.TabStop = false;
            this.groupBoxFind.Text = "Find Text";
            // 
            // FindLabelInstructions
            // 
            this.FindLabelInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FindLabelInstructions.Location = new System.Drawing.Point(6, 24);
            this.FindLabelInstructions.Name = "FindLabelInstructions";
            this.FindLabelInstructions.Size = new System.Drawing.Size(167, 13);
            this.FindLabelInstructions.TabIndex = 17;
            this.FindLabelInstructions.Text = "Press Enter to Find Text";
            // 
            // WholeCheckBox
            // 
            this.WholeCheckBox.AutoSize = true;
            this.WholeCheckBox.Checked = true;
            this.WholeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.WholeCheckBox.Location = new System.Drawing.Point(90, 67);
            this.WholeCheckBox.Name = "WholeCheckBox";
            this.WholeCheckBox.Size = new System.Drawing.Size(150, 17);
            this.WholeCheckBox.TabIndex = 9;
            this.WholeCheckBox.Text = "Return Whole Words Only";
            this.WholeCheckBox.UseVisualStyleBackColor = true;
            // 
            // wordsAfterLabel
            // 
            this.wordsAfterLabel.AutoSize = true;
            this.wordsAfterLabel.Location = new System.Drawing.Point(237, 97);
            this.wordsAfterLabel.Name = "wordsAfterLabel";
            this.wordsAfterLabel.Size = new System.Drawing.Size(62, 13);
            this.wordsAfterLabel.TabIndex = 16;
            this.wordsAfterLabel.Text = "words after.";
            // 
            // afterWordsBox
            // 
            this.afterWordsBox.Location = new System.Drawing.Point(191, 95);
            this.afterWordsBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.afterWordsBox.Name = "afterWordsBox";
            this.afterWordsBox.Size = new System.Drawing.Size(40, 20);
            this.afterWordsBox.TabIndex = 15;
            this.afterWordsBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.afterWordsBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.afterWordsBox_KeyPress);
            // 
            // wordsBeforelabel
            // 
            this.wordsBeforelabel.AutoSize = true;
            this.wordsBeforelabel.Location = new System.Drawing.Point(96, 97);
            this.wordsBeforelabel.Name = "wordsBeforelabel";
            this.wordsBeforelabel.Size = new System.Drawing.Size(89, 13);
            this.wordsBeforelabel.TabIndex = 14;
            this.wordsBeforelabel.Text = "words before and";
            // 
            // beforeWordsBox
            // 
            this.beforeWordsBox.Location = new System.Drawing.Point(50, 95);
            this.beforeWordsBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.beforeWordsBox.Name = "beforeWordsBox";
            this.beforeWordsBox.Size = new System.Drawing.Size(40, 20);
            this.beforeWordsBox.TabIndex = 13;
            this.beforeWordsBox.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.beforeWordsBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.beforeWordsBox_KeyPress);
            // 
            // FindLabel
            // 
            this.FindLabel.AutoSize = true;
            this.FindLabel.Location = new System.Drawing.Point(17, 97);
            this.FindLabel.Name = "FindLabel";
            this.FindLabel.Size = new System.Drawing.Size(27, 13);
            this.FindLabel.TabIndex = 12;
            this.FindLabel.Text = "Find";
            // 
            // NextTextButton
            // 
            this.NextTextButton.Enabled = false;
            this.NextTextButton.Image = ((System.Drawing.Image)(resources.GetObject("NextTextButton.Image")));
            this.NextTextButton.Location = new System.Drawing.Point(258, 38);
            this.NextTextButton.Name = "NextTextButton";
            this.NextTextButton.Size = new System.Drawing.Size(51, 23);
            this.NextTextButton.TabIndex = 11;
            this.NextTextButton.Text = ">";
            this.NextTextButton.UseVisualStyleBackColor = true;
            this.NextTextButton.Click += new System.EventHandler(this.NextTextButton_Click);
            // 
            // PreviousTextButton
            // 
            this.PreviousTextButton.Enabled = false;
            this.PreviousTextButton.Image = ((System.Drawing.Image)(resources.GetObject("PreviousTextButton.Image")));
            this.PreviousTextButton.Location = new System.Drawing.Point(201, 38);
            this.PreviousTextButton.Name = "PreviousTextButton";
            this.PreviousTextButton.Size = new System.Drawing.Size(51, 23);
            this.PreviousTextButton.TabIndex = 10;
            this.PreviousTextButton.Text = "<";
            this.PreviousTextButton.UseVisualStyleBackColor = true;
            this.PreviousTextButton.Click += new System.EventHandler(this.PreviousTextButton_Click);
            // 
            // FindBox
            // 
            this.FindBox.Enabled = false;
            this.FindBox.Location = new System.Drawing.Point(6, 40);
            this.FindBox.Name = "FindBox";
            this.FindBox.Size = new System.Drawing.Size(185, 20);
            this.FindBox.TabIndex = 9;
            this.FindBox.Text = "Find Text";
            this.FindBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FindBox.Click += new System.EventHandler(this.FindBox_Click);
            this.FindBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FindBox_KeyPress);
            // 
            // groupBoxText
            // 
            this.groupBoxText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxText.Controls.Add(this.textSearchResultsBox);
            this.groupBoxText.Location = new System.Drawing.Point(3, 139);
            this.groupBoxText.Name = "groupBoxText";
            this.groupBoxText.Size = new System.Drawing.Size(319, 89);
            this.groupBoxText.TabIndex = 1;
            this.groupBoxText.TabStop = false;
            this.groupBoxText.Text = "Text Search Results";
            // 
            // textSearchResultsBox
            // 
            this.textSearchResultsBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textSearchResultsBox.Location = new System.Drawing.Point(3, 16);
            this.textSearchResultsBox.Name = "textSearchResultsBox";
            this.textSearchResultsBox.ReadOnly = true;
            this.textSearchResultsBox.Size = new System.Drawing.Size(313, 70);
            this.textSearchResultsBox.TabIndex = 18;
            this.textSearchResultsBox.Text = "";
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.groupBoxMain);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(329, 77);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(832, 504);
            this.panelMain.TabIndex = 12;
            // 
            // groupBoxMain
            // 
            this.groupBoxMain.Controls.Add(this.imageXView);
            this.groupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxMain.Location = new System.Drawing.Point(0, 0);
            this.groupBoxMain.Name = "groupBoxMain";
            this.groupBoxMain.Size = new System.Drawing.Size(832, 504);
            this.groupBoxMain.TabIndex = 0;
            this.groupBoxMain.TabStop = false;
            // 
            // imageXView
            // 
            this.imageXView.AutoScroll = true;
            this.imageXView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageXView.Location = new System.Drawing.Point(3, 16);
            this.imageXView.Name = "imageXView";
            this.imageXView.Size = new System.Drawing.Size(826, 485);
            this.imageXView.TabIndex = 0;
            this.imageXView.ZoomFactorChanged += new Accusoft.ImagXpressSdk.ImageXView.ZoomFactorChangedEventHandler(this.imageXView_ZoomFactorChanged);
            // 
            // splitter
            // 
            this.splitter.Location = new System.Drawing.Point(325, 77);
            this.splitter.MinSize = 325;
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(4, 504);
            this.splitter.TabIndex = 13;
            this.splitter.TabStop = false;
            // 
            // pdfXpress1
            // 
            this.pdfXpress1.Debug = false;
            this.pdfXpress1.DebugLogFile = "";
            this.pdfXpress1.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1161, 603);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.statusBarMain);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Menu = this.mainMenu;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PDF Xpress Demo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusBarMain.ResumeLayout(false);
            this.statusBarMain.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.panelBookmarks.ResumeLayout(false);
            this.groupBoxBookmarks.ResumeLayout(false);
            this.panelText.ResumeLayout(false);
            this.groupBoxFind.ResumeLayout(false);
            this.groupBoxFind.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.afterWordsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beforeWordsBox)).EndInit();
            this.groupBoxText.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.groupBoxMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();

            using (SplashForm splash = new SplashForm())
            {
                //want to keep main form on primary screen
                Screen screen = Screen.PrimaryScreen;

                splash.StartPosition = FormStartPosition.Manual;

                //equivalent StartPosition of CenterScreen
                splash.Location = new Point((screen.WorkingArea.Width - splash.Width) / 2, 
                                            (screen.WorkingArea.Height - splash.Height) / 2);
        
                splash.ShowDialog();
            }

            Application.Run(new FormMain());
        }

        private void FormMain_Load(object sender, System.EventArgs e)
        {
            try
            {
                //***Must call the UnlockRuntime method to unlock the control
                //The unlock codes shown below are for formatting purposes only and will not work!
                //imagXpress1.Licensing.UnlockRuntime(1234,1234,1234,1234);
                //pdfXpress1.Licensing.UnlockRuntime(1234,1234,1234,1234);

                pdfXpress1.Licensing.LicenseEdition = LicenseChoice.ProfessionalEdition;

                zoomComboBox.SelectedIndex = 0;

                imageXView.AutoResize = AutoResizeType.BestFit;

                LoadNewDoc();

                string fontPath, cmapPath;

                if (Application.StartupPath.Contains("PdfXpressLicensedDemo") == true)
                {
                    openPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
                    fontPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\Font\\";
                    cmapPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\CMap\\";
                    iccPath  = Application.StartupPath  + 
                         "..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\SampleICCProfile\\sRGB_IEC61966-2-1_withBPC.icc";
                }
                else
                {
                    openPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
                    fontPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\Font\\";
                    cmapPath = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\CMap\\";
                    iccPath  = Application.StartupPath  +
                        "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\SampleICCProfile\\sRGB_IEC61966-2-1_withBPC.icc";
                }

                if (Directory.Exists(fontPath) == true && Directory.Exists(cmapPath) == true)
                {
                    pdfXpress1.Initialize(fontPath, cmapPath);
                }
                else
                {
                    pdfXpress1.Initialize();
                }

                notateXpress1.ClientWindow = imageXView.Handle;

                oo = new OpenOptions();
                oo.Filename = strFileName;
                oo.Password = "";
                oo.Repair = true;

                rendOpts = new RenderOptions();
                rendOpts.ResolutionX = 300;
                rendOpts.ResolutionY = 300;
                rendOpts.ProduceDibSection = false;
                rendOpts.RenderAnnotations = false;

                useXFDF = true;

                destinationX = (int).25 * 72;
                destinationY = (int).25 * 72;

                bookmarks = new ArrayList();

                so = new Accusoft.PdfXpressSdk.SaveOptions();
                so.Overwrite = true;
                so.Linearized = true;

                document = new Document(pdfXpress1);

                lo = new Accusoft.NotateXpressSdk.LoadOptions();
                lo.AnnType = AnnotationType.Xfdf;

                xfdfOptions = new XfdfOptions();
                xfdfOptions.WhichEncodingName = "UTF-8";
                xfdfOptions.WhichAnnotation = XfdfOptions.AllAnnotations;
                xfdfOptions.WhichPage = XfdfOptions.AllPages;

                layerData = new List<byte[]>();

                AnnotationToolStrip = new ToolStripButton[] 
                    { PointerTool, highlightTool, noteTool, TextTool, 
                      ArrowTool, LineTool, RectangleTool, OvalTool, PencilTool, PolyLineTool, 
                      PolygonTool };
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadNewDoc()
        {
            statusBarMain.Items[0].Text = "Use File | Open to open a PDF document";
            groupBoxMain.Text = "Use File | Open to open a PDF document";
        }

        private void InitializeBookmarkTree()
        {
            ClearBookmarks();

            treeViewBookmarks.BeginUpdate();
            treeViewBookmarks.Nodes.Clear();

            using (BookmarkContext bookmarkRoot = document.GetBookmarkRoot())
            {
                ChildBookmarkTree(bookmarkRoot, treeViewBookmarks.Nodes);
            }

            treeViewBookmarks.EndUpdate();
        }

        private void ChildBookmarkTree(BookmarkContext parentBookmark, TreeNodeCollection parentNodes)
        {
            BookmarkContext bookmark = document.GetBookmarkChild(parentBookmark);
            if (bookmark != null)
            {
                bookmarks.Add(bookmark);

                TreeNode node = null;
                
                if (bookmark.ActionPageNumber == -1)
                {
                    node = new TreeNode(bookmark.Title);
                    node.Name = bookmark.ActionPageNumber.ToString();
                }
                else
                {
                    node = new TreeNode(bookmark.Title + " (page " + (bookmark.ActionPageNumber + 1) + ")");
                    node.Name = bookmark.ActionPageNumber.ToString();
                }

                BookmarkContent bookmarkContent = new BookmarkContent();

                bookmarkContent.actionPageNumber = bookmark.ActionPageNumber;
                node.Tag = bookmarkContent;
                parentNodes.Add(node);

                ChildBookmarkTree(bookmark, node.Nodes);
                SiblingBookmarkTree(bookmark, parentNodes);
            }
        }

        private void SiblingBookmarkTree(BookmarkContext parentBookmark, TreeNodeCollection parentNodes)
        {
            BookmarkContext bookmark = document.GetBookmarkSibling(parentBookmark);
            if (bookmark != null)
            {
                bookmarks.Add(bookmark);

                TreeNode node = null;

                if (bookmark.ActionPageNumber == -1)
                {
                    node = new TreeNode(bookmark.Title);
                    node.Name = bookmark.ActionPageNumber.ToString();
                }
                else
                {
                    node = new TreeNode(bookmark.Title + " (page " + (bookmark.ActionPageNumber + 1) + ")");
                    node.Name = bookmark.ActionPageNumber.ToString();
                }

                BookmarkContent bookmarkContent = new BookmarkContent();

                bookmarkContent.actionPageNumber = bookmark.ActionPageNumber;
                node.Tag = bookmarkContent;
                parentNodes.Add(node);
                ChildBookmarkTree(bookmark, node.Nodes);
                SiblingBookmarkTree(bookmark, parentNodes);
            }
        }

        private class BookmarkContent
        {
            public int actionPageNumber;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Title = "Open a PDF Document";
                    dlg.InitialDirectory = Path.GetFullPath(openPath);

                    dlg.Filter = "PDF Document (*.PDF)|*.PDF|All Files (*.*) | *.*";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        strFileName = dlg.FileName;
                        statusBarMain.Items[0].Text = "";

                        if (File.Exists(strFileName))
                        {
                            openPath = Path.GetDirectoryName(dlg.FileName);

                            oo.Filename = strFileName;

                            //resource cleanup
                            if (document != null)
                                document.Dispose();

                            bool passwordNeeded = false;
                            int passwordCounter = 0;

                            do
                            {
                                try
                                {
                                    document = new Document(pdfXpress1, oo);

                                    passwordNeeded = false;
                                }
                                catch (Accusoft.PdfXpressSdk.InvalidPasswordException)
                                {
                                    if (passwordCounter == 0)
                                    {
                                        passwordNeeded = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("The password you entered is not correct.", "Error",
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                    passwordCounter++;

                                    using (PasswordForm passForm = new PasswordForm())
                                    {
                                        if (passForm.ShowDialog() == DialogResult.OK)
                                        {
                                            oo.Password = passForm.Password;
                                        }
                                        else
                                        {
                                            return;
                                        }
                                    }
                                }
                            } while (passwordNeeded == true);

                            pageNumber = 0;

                            ImageLoad();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void ResetTextSearchArrays()
        {
            if (highlights != null)
            {
                highlights.Clear();
            }

            if (matches != null)
            {
                matches.Clear();
            }

            highlights = new List<Rectangle[]>(pageCount);
            matches = new List<TextMatch[]>(pageCount);

            for (int i = 0; i < pageCount; i++)
            {
                highlights.Add(new Rectangle[0]);
                matches.Add(new TextMatch[0]);
            }
        }

        private void ImageLoad()
        {
            pageCount = document.PageCount;

            if (pageCount > 1)
            {
                NextButton.Enabled = true;

                PageBox.Enabled = true;
            }

            pageNumber = 0;

            ResetTextSearchArrays();

            PageBox.Text = ((int)(pageNumber + 1)).ToString();

            destinationW = (int)(document.GetInfo(pageNumber).MediaWidth - .5 * 72);
            destinationH = (int)(document.GetInfo(pageNumber).MediaHeight - .5 * 72);

            so.Filename = strFileName;

            UpdateUI_PDF(true);

            CountBox.Text = "of " + pageCount.ToString();
            
            InitializeBookmarkTree();

            layerData.Clear();

            for (int i = 0; i < pageCount; i++)
            {
                layerData.Add(new byte[1]);
            }

            xfdfOriginal = document.ExportXfdf(xfdfOptions);

            double renderToUse;

            if (rendOpts.ResolutionX >= rendOpts.ResolutionY)
            {
                renderToUse = rendOpts.ResolutionX;
            }
            else
            {
                renderToUse = rendOpts.ResolutionY;
            }

            notateXpress1.ToolbarDefaults.NoteToolbarDefaults.TextFont = new Font(System.Drawing.FontFamily.GenericMonospace,
                    (float)renderToUse / 6, FontStyle.Bold);

            notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = new Font(System.Drawing.FontFamily.GenericMonospace,
                (float)renderToUse / 6, FontStyle.Bold);

            UpdatePage();
        }

        private void UpdatePage()
        {
            imageXView.ToolSet(Tool.None, MouseButtons.Left, Keys.None);

            using (Bitmap bp = document.RenderPageToBitmap(pageNumber, rendOpts))
            {
                imageXView.AllowUpdate = false;

                //resource cleanup
                if (imageXView.Image != null)
                {
                    imageXView.Image.Dispose();
                    imageXView.Image = null;
                }

                imageXView.Image = ImageX.FromBitmap(imagXpress1, bp);
                imageXView.Image.Resolution.Units = GraphicsUnit.Inch;
                imageXView.Image.Resolution.Dimensions = new SizeF((float)rendOpts.ResolutionX, (float)rendOpts.ResolutionY);

                UpdatePageInfo();

                for (int i = 0; i < AnnotationToolStrip.Length; i++)
                {
                    if (AnnotationToolStrip[i].Name != "PointerTool")
                    {
                        AnnotationToolStrip[i].Checked = false;
                    }
                    else
                    {
                        AnnotationToolStrip[i].Checked = true;
                    }
                }

                notateXpress1.Layers.Clear();

                if (useXFDF == true)
                {
                    try
                    {
                        notateXpress1.PageNumber = pageNumber + 1;

                        notateXpress1.Layers.FromByteArray(document.ExportXfdf(xfdfOptions), lo);

                        switch (document.GetInfo(pageNumber).Rotated)
                        {
                            case Accusoft.PdfXpressSdk.RotateAngle.Rotate0:
                                {
                                    map = (int)Accusoft.NotateXpressSdk.Mapping.None;
                                    break;
                                }
                            case Accusoft.PdfXpressSdk.RotateAngle.Rotate180:
                                {
                                    map = (int)Accusoft.NotateXpressSdk.Mapping.Pdf180;
                                    break;
                                }
                            case Accusoft.PdfXpressSdk.RotateAngle.Rotate270:
                                {
                                    map = (int)Accusoft.NotateXpressSdk.Mapping.Pdf270;
                                    break;
                                }
                            case Accusoft.PdfXpressSdk.RotateAngle.Rotate90:
                                {
                                    map = (int)Accusoft.NotateXpressSdk.Mapping.Pdf90;
                                    break;
                                }
                            default:
                                {
                                    map = (int)Accusoft.NotateXpressSdk.Mapping.User;
                                    break;
                                }
                        }

                        Accusoft.NotateXpressSdk.SaveOptions notateSaveOptions =
                            new Accusoft.NotateXpressSdk.SaveOptions();

                        notateSaveOptions.Mapping = (Mapping)map;
                        notateSaveOptions.AnnType = AnnotationType.Xfdf;

                        notateXpress1.Layers.SetSaveOptions(notateSaveOptions);

                        notateXpress1.Layers.Clear();

                        notateXpress1.PageNumber = pageNumber + 1;

                        notateXpress1.Layers.FromByteArray(document.ExportXfdf(xfdfOptions), lo);

                    }
                    catch (PageNotFoundException) { };
                }
                else
                {
                    try
                    {
                        if (layerData[pageNumber].Length > 1)
                        {
                            notateXpress1.PageNumber = pageNumber + 1;
                            notateXpress1.Layers.FromByteArray(layerData[pageNumber], lo);
                        }
                    }
                    catch (PageNotFoundException) { };
                }

                notateXpress1.Layers.Add(new Layer());

                notateXpress1.Layers[0].Select();

                imageXView.AllowUpdate = true;
            }

            if (pageNumber == 0)
            {
                PreviousButton.Enabled = false;
            }
            else
            {
                PreviousButton.Enabled = true;
            }

            if (pageCount > 1)
            {
                if (pageNumber == pageCount - 1)
                {
                    NextButton.Enabled = false;
                }
                else
                {
                    NextButton.Enabled = true;
                }
            }

            PageBox.Text = ((int)(pageNumber + 1)).ToString();
        }

        private void UpdatePageInfo()
        {
            PageInfo info = document.GetInfo(pageNumber);

            statusBarMain.Items[0].Text = "Clip Box: {X: " + ((double)(info.ClipX / 72)).ToString("F2")
                + ", Y: " + ((double)(info.ClipY / 72)).ToString("F2")
                + ", W: " + ((double)(info.ClipWidth / 72)).ToString("F2") + ", H: "
                + ((double)(info.ClipHeight / 72)).ToString("F2") + "} inches,"
                + " Media Box: {X: " + ((double)(info.MediaX / 72)).ToString("F2")
                + ", Y: " + ((double)(info.MediaY / 72)).ToString("F2") + ", W: "
                + ((double)(info.MediaWidth / 72)).ToString("F2") + ", H: " +
                ((double)(info.MediaHeight / 72)).ToString("F2") + "} inches,"
                + " Rotation: " + info.Rotated.ToString();
        }

        private void PageBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    try
                    {
                        int pageChosen = Convert.ToInt32(PageBox.Text) - 1;

                        if (pageChosen >= 0 && pageChosen < pageCount)
                        {
                            statusBarMain.Items[0].Text = "";

                            SaveAnnotations();

                            pageNumber = pageChosen;

                            UpdatePage();
                        }
                        else
                        {
                            PageBox.Text = ((int)(pageNumber + 1)).ToString();
                            statusBarMain.Items[0].Text = "Please enter a valid Page Number.";
                        }
                    }
                    catch (FormatException)
                    {
                        PageBox.Text = ((int)(pageNumber + 1)).ToString();
                        statusBarMain.Items[0].Text = "Please enter a valid Page Number.";
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void PreviousButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAnnotations();

                pageNumber = pageNumber - 1;

                UpdatePage();
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAnnotations();

                pageNumber += 1;

                UpdatePage();
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateUI_PDF(bool onOff)
        {
            treeViewBookmarks.Nodes.Clear();

            statusBarMain.Items[0].Text = "";

            imageXView.ToolSet(Tool.None, MouseButtons.Left, Keys.None);

            addImageToolStripMenuItem.Enabled = onOff;
            addWatermarkToolStripMenuItem.Enabled = onOff;
            closeToolStripMenuItem.Enabled = onOff;
            createThumbnailToolStripMenuItem.Enabled = onOff;
            deletePageToolStripMenuItem.Enabled = onOff;
            deleteThumbnailToolStripMenuItem.Enabled = onOff;
            editBookmarksToolStripMenuItem.Enabled = onOff;
            exportAnnotationsToXFDFToolStripMenuItem.Enabled = onOff;
            importAnnotationsToXFDFToolStripMenuItem.Enabled = onOff;
            insertPageToolStripMenuItem.Enabled = onOff;
            metaDataToolStripMenuItem.Enabled = onOff;
            movePageToolStripMenuItem.Enabled = onOff;
            removeWatermarkToolStripMenuItem.Enabled = onOff;
            saveAsToolStripMenuItem.Enabled = onOff;
            saveMenuItem.Enabled = onOff;
            setRenderOptionsToolStripMenuItem.Enabled = onOff;
            savePdfAMenuItem.Enabled = onOff;

            for (int i = 0; i < AnnotationToolStrip.Length; i++)
            {
                AnnotationToolStrip[i].Enabled = onOff;
            }

            NextButton.Enabled = false;
            PreviousButton.Enabled = false;

            PreviousTextButton.Enabled = false;
            NextTextButton.Enabled = false;

            FindBox.Enabled = onOff;
            FindBox.Text = "Find Text";

            zoomComboBox.Enabled = onOff;

            textSearchResultsBox.Text = "";


            if (onOff == true)
            {
                statusBarMain.Items[0].Text = "";

                groupBoxMain.Text = strFileName;

                for (int i = 0; i < AnnotationToolStrip.Length; i++)
                {
                    if (AnnotationToolStrip[i].Name != "PointerTool")
                    {
                        AnnotationToolStrip[i].Checked = false;
                    }
                }

                groupBoxMain.Text = strFileName;


                PageBox.Text = "1";
                CountBox.Text = " of 1";
            }
            else
            {
                statusBarMain.Items[0].Text = "Use File | Open to open a PDF Document";

                groupBoxMain.Text = "Use File | Open to open a PDF Document";

                if (highlights != null)
                {
                    highlights.Clear();
                }
                if (matches != null)
                {
                    matches.Clear();
                }

                for (int i = 0; i < AnnotationToolStrip.Length; i++)
                {
                    if (AnnotationToolStrip[i].Name != "pointerTool")
                    {
                        AnnotationToolStrip[i].Checked = false;
                    }
                    else
                    {
                        AnnotationToolStrip[i].Checked = true;
                    }
                }

                PageBox.Enabled = false;
                PageBox.Text = "";
                CountBox.Text = "";
            }

        }
        
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //cleanup any temp files created
                helper.SafelyDeleteFile(strTempFileName);

                helper.SafelyDeleteFiles(strTempSavedFileNames.ToArray());

                if (imageXView.Image != null)
                {
                    imageXView.Image.Dispose();
                    imageXView.Image = null;
                }

                oo.Password = "";
                oo.Filename = "";

                document.Dispose();

                LoadNewDoc();
                
                ClearBookmarks();

                UpdateUI_PDF(false);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearBookmarks()
        {
            if (bookmarks != null)
            {
                for (int i = 0; i < bookmarks.Count; i++)
                {
                    if (bookmarks[i] != null)
                    {
                        ((BookmarkContext)bookmarks[i]).Dispose();
                        bookmarks[i] = null;
                    }
                }
            }

            bookmarks.Clear();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void statusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (statusBarMain.Visible == true)
                {
                    statusBarMain.Visible = false;
                    statusBarToolStripMenuItem.Checked = false;
                }
                else
                {
                    statusBarMain.Visible = true;
                    statusBarToolStripMenuItem.Checked = true;
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void toolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (toolStrip.Visible == true)
                {
                    toolStrip.Visible = false;
                    for (int i = 0; i < AnnotationToolStrip.Length; i++)
                    {
                        AnnotationToolStrip[i].Visible = false;
                    }
                    toolbarToolStripMenuItem.Checked = false;
                }
                else
                {
                    toolStrip.Visible = true;
                    for (int i = 0; i < AnnotationToolStrip.Length; i++)
                    {
                        AnnotationToolStrip[i].Visible = true;
                    }
                    toolbarToolStripMenuItem.Checked = true;
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            try
            {
                openToolStripMenuItem_Click(sender, e);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            try
            {
                closeToolStripMenuItem_Click(sender, e);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void pdfXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                pdfXpress1.AboutBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FindBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (FindBox.Text.Length > 0)
                    {
                        ResetTextSearchArrays();

                        wordChosen = 0;

                        PreviousTextButton.Enabled = false;
                        NextTextButton.Enabled = false;

                        textSearchResultsBox.Text = "";

                        //create a new TextFinderOptions object
                        TextFinderOptions findOptions = new TextFinderOptions();
                        findOptions.IgnoreTextAnnotations = false;
                        findOptions.IgnoreTaggedPdfArtifacts = false;

                        for (int pageIndex = 0; pageIndex < pageCount; pageIndex++)
                        {
                            //create a new TextFinder
                            using (TextFinder finder = document.GetTextFinder(pageIndex, findOptions))
                            {
                                //extract the text
                                string foundText = finder.GetText();

                                if (foundText.Length > 0)
                                {
                                    int beginOffset = 0;
                                    for (int i = 0; i < foundText.Length; i++)
                                    {
                                        if (foundText.Substring(0, 1) == " ")
                                        {
                                            foundText = foundText.Substring(1, foundText.Length - (i + 1));
                                            beginOffset = beginOffset + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }

                                    //creat an array for offsets
                                    ArrayList offset = new ArrayList();
                                    
                                    for (int i = 0; i < foundText.Length - FindBox.Text.Length; i++)
                                    {
                                        //search for the word inside the text
                                        int offsetFound = foundText.IndexOf(FindBox.Text, i, FindBox.Text.Length, StringComparison.OrdinalIgnoreCase);

                                        if (offsetFound != -1)
                                        {
                                            //record the location of the offset
                                            offset.Add(offsetFound);
                                        }
                                    }

                                    if (offset.Count > 0)
                                    {
                                        //create a new TextMatchOptions object
                                        TextMatchOptions matchOptions = new TextMatchOptions();

                                        //set the sizes of the highlight and matched arrays based on the offset count
                                        highlights[pageIndex] = new Rectangle[offset.Count];
                                        matches[pageIndex] = new TextMatch[offset.Count];

                                        for (int k = 0; k < offset.Count; k++)
                                        {
                                            //set the TextMatchOptions object
                                            matchOptions = new TextMatchOptions(beginOffset + (int)offset[k],
                                                beginOffset + (int)offset[k] + FindBox.Text.Length,
                                                Convert.ToInt32(beforeWordsBox.Text),
                                                Convert.ToInt32(afterWordsBox.Text), WholeCheckBox.Checked);

                                            //set the TextMatch objects using GetTextMatch()
                                            ((TextMatch[])matches[pageIndex])[k] = finder.GetTextMatch(matchOptions);

                                            //record the top left and bottom right location of the highlight, convert from PDF units to pixels based on
                                            //resolution rendered at
                                            ((Rectangle[])highlights[pageIndex])[k] = new Rectangle(
                                                (int)(((TextMatch[])matches[pageIndex])[k].BoundingQuadrilaterals[0].TopLeft.X
                                                * rendOpts.ResolutionX / 72),
                                                (int)((document.GetInfo(pageNumber).ClipHeight -
                                                ((TextMatch[])matches[pageIndex])[k].BoundingQuadrilaterals[0].TopLeft.Y)
                                                * rendOpts.ResolutionY / 72),
                                                (int)((((TextMatch[])matches[pageIndex])[k].BoundingQuadrilaterals[0].BottomRight.X -
                                                ((TextMatch[])matches[pageIndex])[k].BoundingQuadrilaterals[0].TopLeft.X)
                                                * rendOpts.ResolutionX / 72),
                                                (int)(((document.GetInfo(pageNumber).MediaHeight -
                                                ((TextMatch[])matches[pageIndex])[k].BoundingQuadrilaterals[0].BottomRight.Y) -
                                                (document.GetInfo(pageNumber).MediaHeight -
                                                ((TextMatch[])matches[pageIndex])[k].BoundingQuadrilaterals[0].TopLeft.Y))
                                                * (rendOpts.ResolutionY / 72)));
                                        }
                                    }
                                }
                            }
                        }

                        //if more then one match found on the page then enable the Next button
                        if (highlights[pageNumber].Length > 1)
                        {
                            NextTextButton.Enabled = true;
                        }

                        wordChosen = 0;

                        bool noResults = true;

                        for (int i = pageNumber ; i < matches.Count; i++)
                        {
                            if (matches[i].Length != 0)
                            {
                                noResults = false;
                                break;
                            }
                        }

                        if (noResults == false)
                        {
                            HighlightWord();
                        }
                        else
                        {
                            MessageBox.Show("The specified text was not found.", "Text Search",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please enter what text to look for.");
                    }
                }
                else
                {
                    PreviousTextButton.Enabled = false;
                    NextTextButton.Enabled = false;
                }
            }
            catch (PdfXpressException)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show("An error occured in the Text Finder, make sure this is a PDF that is text-searchable and not image-only.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void Highlight(int layerIndex)
        {
            RectangleTool border = new RectangleTool();
            border.BackStyle = BackStyle.Translucent;
            border.PenWidth = 0;
            border.FillColor = Color.Yellow;
            border.BoundingRectangle = ((Rectangle[])highlights[pageNumber])[wordChosen];

            if (notateXpress1.Layers[layerIndex] != null)
            {
                if (notateXpress1.Layers[layerIndex].Elements != null)
                {
                    notateXpress1.Layers[layerIndex].Elements.Dispose();

                    notateXpress1.Layers[layerIndex].Elements.Clear();
                    notateXpress1.Layers[layerIndex].Elements.Add(border);

                    notateXpress1.Layers[layerIndex].Visible = true;
                }
            }
        }

        private void HighlightWord()
        {
            SaveAnnotations();

            int numberOfWordsFound = 0;

            int numberOfMatchesOnCurrentPage = ((TextMatch[])matches[pageNumber]).Length;

            if (wordChosen > numberOfMatchesOnCurrentPage - 1 && numberOfMatchesOnCurrentPage > 0)
            {
                pageNumber += 1;
                wordChosen = 0;

                for (int i = pageNumber; i < pageCount; i++)
                {
                    if (((TextMatch[])matches[i]).Length != 0)
                    {
                        pageNumber = i;
                        break;
                    }
                }
            }

            if (wordChosen < 0)
            {
                pageNumber -= 1;

                for (int i = pageNumber; i > -1; i--)
                {
                    numberOfWordsFound = ((TextMatch[])matches[i]).Length;

                    if (numberOfWordsFound != 0)
                    {
                        wordChosen = numberOfWordsFound - 1;
                        pageNumber = i;
                        break;
                    }
                }
            }            

            UpdatePage();

            imageXView.AllowUpdate = false;

            if (notateXpress1.Layers != null)
            {
                if (notateXpress1.Layers.Count > 1)
                {
                    Highlight(1);
                }
                else
                {
                    Highlight(0);
                }
            }

            if (imageXView.AutoResize == AutoResizeType.CropImage)
            {
                imageXView.ZoomFactor = 1.0;
                imageXView.ScrollPosition = new Point(((Rectangle[])highlights[pageNumber])[wordChosen].Location.X
                                                + ((Rectangle[])highlights[pageNumber])[wordChosen].Width / 2
                                                - imageXView.Width / 2,
                                                ((Rectangle[])highlights[pageNumber])[wordChosen].Location.Y +
                                                ((Rectangle[])highlights[pageNumber])[wordChosen].Height / 2
                                                - imageXView.Height / 2);
            }

            imageXView.AllowUpdate = true;

            Application.DoEvents();

            string wordsBefore = ((TextMatch[])matches[pageNumber])[wordChosen].WordsBefore;
            string wordsAfter = ((TextMatch[])matches[pageNumber])[wordChosen].WordsAfter;
            string wordMatched = ((TextMatch[])matches[pageNumber])[wordChosen].MatchedText;

            textSearchResultsBox.Clear();
            textSearchResultsBox.Text = wordsBefore + wordMatched + wordsAfter;
            textSearchResultsBox.Select(wordsBefore.Length, wordMatched.Length);
            textSearchResultsBox.SelectionBackColor = Color.LightBlue;

            NextTextButton.Enabled = false;
            PreviousTextButton.Enabled = false;

            for (int i = pageNumber; i < pageCount; i++)
            {
                numberOfWordsFound = ((Rectangle[])highlights[i]).Length;

                //if any of the next pages have any # of the search words, enable the next text button
                if (numberOfWordsFound > 0 && i > pageNumber)
                {
                    NextTextButton.Enabled = true;
                    break;
                }
                //if on the last page and on the last word, disable the next text button
                else if (i == pageCount - 1 && wordChosen == numberOfWordsFound - 1)
                {
                    NextTextButton.Enabled = false;
                    break;
                }
                //if on the current page and not on the last word yet, enable the next text button
                else if (numberOfWordsFound > 0 && i == pageNumber && wordChosen < numberOfWordsFound - 1)
                {
                    NextTextButton.Enabled = true;
                    break;
                }
            }

            for (int i = pageNumber; i > -1; i--)
            {
                numberOfWordsFound = ((Rectangle[])highlights[i]).Length;

                //if any of the previous pages have any # of the search words, enable the previous text button
                if (numberOfWordsFound > 0 && i < pageNumber)
                {
                    PreviousTextButton.Enabled = true;
                    break;
                }
                //if on the first page and on the first word, disable the previous text button
                else if (i == 0 && wordChosen == 0)
                {
                    PreviousTextButton.Enabled = false;
                    break;
                }
                //if on the current page and not on the first word yet, enable the previous text button
                else if (numberOfWordsFound > 0 && i == pageNumber && wordChosen > 0)
                {
                    PreviousTextButton.Enabled = true;
                }
            }
        }

        private void NextTextButton_Click(object sender, EventArgs e)
        {
            try
            {
                wordChosen = wordChosen + 1;

                HighlightWord();
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void PreviousTextButton_Click(object sender, EventArgs e)
        {
            try
            {
                wordChosen = wordChosen - 1;

                HighlightWord();
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void ZoomInButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageXView.AutoResize = AutoResizeType.CropImage;

                if (imageXView.ZoomFactor < 10)
                {
                    imageXView.ZoomFactor = imageXView.ZoomFactor * 1.1;
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void ZoomOutButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageXView.AutoResize = AutoResizeType.CropImage;

                if (imageXView.ZoomFactor > .05)
                {
                    imageXView.ZoomFactor = imageXView.ZoomFactor * .95;
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void ZoomFitButton_Click(object sender, EventArgs e)
        {
            try
            {
                imageXView.ZoomToFit(ZoomToFitType.FitBest);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void setRenderOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RendForm();
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void addImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (ImageForm imgFrm = new ImageForm())
                {
                    imgFrm.DesXBox.Maximum = (decimal)document.GetInfo(pageNumber).MediaWidth;
                    imgFrm.DesWBox.Maximum = imgFrm.DesXBox.Maximum;
                    imgFrm.DesYBox.Maximum = (decimal)document.GetInfo(pageNumber).MediaHeight;
                    imgFrm.DesHBox.Maximum = imgFrm.DesYBox.Maximum;
                    imgFrm.DestintationX = destinationX;
                    imgFrm.DestintationY = destinationY;
                    imgFrm.DestintationW = destinationW;
                    imgFrm.DestintationH = destinationH;

                    if (imgFrm.ShowDialog() == DialogResult.OK)
                    {
                        switch (imgFrm.Settings)
                        {
                            case 0:
                                {
                                    destinationFit = ImageFitSettings.None;
                                    break;
                                }
                            case 1:
                                {
                                    destinationFit = ImageFitSettings.Stretch;
                                    break;
                                }
                            case 2:
                                {
                                    destinationFit = ImageFitSettings.Shrink;
                                    break;
                                }
                            case 3:
                                {
                                    destinationFit = ImageFitSettings.Grow;
                                    break;
                                }
                            case 4:
                                {
                                    destinationFit = ImageFitSettings.GravityLeft;
                                    break;
                                }
                            case 5:
                                {
                                    destinationFit = ImageFitSettings.GravityTop;
                                    break;
                                }
                            case 6:
                                {
                                    destinationFit = ImageFitSettings.GravityRight;
                                    break;
                                }
                            case 7:
                                {
                                    destinationFit = ImageFitSettings.GravityBottom;
                                    break;
                                }
                        }

                        SaveAnnotations();

                        document.AddImage(pageNumber, imgFrm.DestintationX, document.GetInfo(pageNumber).MediaHeight
                                          - imgFrm.DestintationH - imgFrm.DestintationY, destinationW, imgFrm.DestintationH,
                                          destinationFit, imgFrm.Image, 0);
                        UpdatePage();
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    dlg.Title = "Save the PDF Document As";
                    
                    if (savePath == "")
                    {
                        savePath = openPath;
                    }

                    dlg.InitialDirectory = Path.GetFullPath(savePath);
                    dlg.FilterIndex = 0;
                    dlg.Filter = "PDF Document (*.PDF)|*.PDF";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        if (dlg.FileName != "")
                        {
                            savePath = Path.GetDirectoryName(dlg.FileName);

                            Save(dlg.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Focus();

                Save(strFileName);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void Save(string filename)
        {
            so.Filename = filename;

            SaveAnnotations();

            document.Save(so);
        }

        private void SaveAnnotations()
        {
            Accusoft.NotateXpressSdk.SaveOptions so = 
                new Accusoft.NotateXpressSdk.SaveOptions();
            so.AnnType = AnnotationType.Xfdf;
            so.Mapping = (Mapping)map;

            if (notateXpress1.Layers.Count > 1)
            {
                notateXpress1.Layers[1].Elements.Clear();
            }

            byte[] xfdfDataCurrent = notateXpress1.Layers.SaveToByteArray(so);

            XfdfOptions xo = new XfdfOptions();
            xo.WhichAnnotation = XfdfOptions.AllAnnotations;
            xo.WhichPage = pageNumber;
            xo.WhichEncodingName = "UTF-8";

            if (useXFDF == false)
            {
                xo.CanDeleteAnnotations = false;
            }

            document.ImportXfdf(xo, xfdfDataCurrent);

            if (useXFDF == false)
            {
                document.ImportXfdf(xo, xfdfOriginal);
            }
        }

        private void createThumbnailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Title = "Open an Image to use as a Thumbnail";
                    dlg.Filter = "TIF (*.TIF)|*.TIF|BMP (*.BMP)|*.BMP|JPG (*.JPG)|*.JPG|All Files (*.*) | *.*";
                    dlg.InitialDirectory = Path.GetFullPath(openPath);

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        document.CreateThumbnail(pageNumber, dlg.FileName, 0);

                        document.Save(so);

                        MessageBox.Show("The PDF " + strFileName + " will now have the chosen thumbnail on page " + ((int)(pageNumber + 1)).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }       

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                PageOptions opts = new PageOptions();
                opts.MediaHeight = 792;
                opts.MediaWidth = 612;

                //resource cleanup
                if (document != null)
                    document.Dispose();

                document = new Document(pdfXpress1);

                //cleanup any temp files created
                helper.SafelyDeleteFile(strTempFileName);

                helper.SafelyDeleteFiles(strTempSavedFileNames.ToArray());
              
                pageNumber = 0;
                pageCount = 1;
                
                ClearBookmarks();

                document.CreatePage(-1, opts);

                useXFDF = true;
 
                strTempFileName = Path.GetTempFileName();

                strTempSavedFileNames.Add(strTempFileName);

                strFileName = strTempFileName;

                UpdateUI_PDF(true);

                UpdatePage();
                
                destinationW = (int)(document.GetInfo(pageNumber).MediaWidth - 0.5 * 72);
                destinationH = (int)(document.GetInfo(pageNumber).MediaHeight - 0.5 * 72);
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e.X >= e.Node.Bounds.X)
                {
                    if (e.Node.Name != "")
                    {
                        if (Int32.Parse(e.Node.Name) > -1)
                        {
                            pageNumber = Int32.Parse(e.Node.Name);

                            nodeSelected = e.Node;

                            UpdatePage();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void insertPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (InsertForm insertForm = new InsertForm())
                {
                    if (insertForm.ShowDialog() == DialogResult.OK)
                    {
                        InsertPagesOptions ipo = new InsertPagesOptions();

                        ipo.IncludeAllDocumentData = insertForm.IncludeAllData;
                        ipo.IncludeArticleThreads = insertForm.IncludeArticles;
                        ipo.IncludeSourceBookmarks = insertForm.IncludeBookmarks;
                        ipo.InsertAtPageNumber = insertForm.InsertPageNumber - 1;

                        using (Document doc = new Document(pdfXpress1, insertForm.SourceFile))
                        {
                            ipo.SourceDocument = doc;

                            PageRange pr = new PageRange();

                            pr.UseAllPages = insertForm.UseAllPages;

                            if (pr.UseAllPages == false)
                            {
                                pr.PageCount = insertForm.PageCount;
                                pr.StartPageNumber = insertForm.StartPage - 1;
                            }                            

                            PageList pl = new PageList();

                            pl.Add(pr);

                            ipo.PageList = pl;

                            SaveAnnotations();

                            document.InsertPages(ipo);
                        }

                        ImageLoad();
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void movePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (MoveForm moveForm = new MoveForm())
                {
                    if (moveForm.ShowDialog() == DialogResult.OK)
                    {
                        SaveAnnotations();

                        if (moveForm.MoveTo == 1)
                        {
                            document.MovePage(moveForm.MoveFrom - 1, 0);
                        }
                        else
                        {
                            document.MovePage(moveForm.MoveFrom - 1, moveForm.MoveTo);
                        }

                        ImageLoad();
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteThumbnailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                document.DeleteThumbnail(pageNumber);

                document.Save(so);

                MessageBox.Show("The PDF " + strFileName + " will have its thumbnail on page " + ((int)(pageNumber + 1)).ToString()
                    + " deleted.");
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void metaDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (MetaDataForm metaForm = new MetaDataForm())
                {
                    metaForm.Author = document.DocumentMetadata.Author;
                    metaForm.CreationDate = document.DocumentMetadata.CreationDate;
                    metaForm.Creator = document.DocumentMetadata.Creator;
                    metaForm.Keywords = document.DocumentMetadata.Keywords;
                    metaForm.ModificationDate = document.DocumentMetadata.ModificationDate;
                    metaForm.Producer = document.DocumentMetadata.Producer;
                    metaForm.Title = document.DocumentMetadata.Title;
                    metaForm.Subject = document.DocumentMetadata.Subject;

                    if (metaForm.ShowDialog() == DialogResult.OK)
                    {
                        document.DocumentMetadata.Author = metaForm.Author;
                        document.DocumentMetadata.Creator = metaForm.Creator;
                        document.DocumentMetadata.Keywords = metaForm.Keywords;
                        document.DocumentMetadata.Producer = metaForm.Producer;
                        document.DocumentMetadata.Title = metaForm.Title;
                        document.DocumentMetadata.Subject = metaForm.Subject;
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void editBookmarksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BookmarkContext bookmarkUsed = null;

            try
            {
                using (BookmarkForm bookForm = new BookmarkForm(pageCount, treeViewBookmarks))
                {
                    if (bookForm.ShowDialog() == DialogResult.OK)
                    {
                        for (int j = 0; j < bookmarks.Count; j++)
                        {
                            if ((BookmarkContext)bookmarks[j] != null)
                            {
                                if (((BookmarkContext)bookmarks[j]).ActionPageNumber.ToString()
                                    == treeViewBookmarks.Nodes[treeViewBookmarks.Nodes.Count - 1].Name
                                    && ((BookmarkContext)bookmarks[j]).Title + " (page " +
                                    ((int)(((BookmarkContext)bookmarks[j]).ActionPageNumber + 1)).ToString() + ")"
                                    == treeViewBookmarks.Nodes[treeViewBookmarks.Nodes.Count - 1].Text)
                                {
                                    bookmarkUsed = (BookmarkContext)bookmarks[j];
                                    break;
                                }
                            }
                        }

                        for (int i = 0; i < bookForm.BookmarkAddedPageNumber.Count; i++)
                        {
                            if (bookmarkUsed != null)
                            {
                                document.AddBookmarkSibling(bookmarkUsed,
                                    (string)bookForm.BookmarkAddedTitle[i], (int)bookForm.BookmarkAddedPageNumber[i]);

                                //bookmarks.Add(document.GetBookmarkSibling(bookmarkUsed));
                            }
                            else
                            {
                                bookmarkUsed = document.GetBookmarkRoot();
                                document.AddBookmarkChild(bookmarkUsed,
                                        (string)bookForm.BookmarkAddedTitle[i], (int)bookForm.BookmarkAddedPageNumber[i]);
                                bookmarks.Add(document.GetBookmarkChild(bookmarkUsed));

                                bookmarkUsed = document.GetBookmarkChild(bookmarkUsed);

                                ImageLoad();
                            }
                        }

                        for (int i = 0; i < bookForm.BookmarksDeletedTitle.Count; i++)
                        {
                            for (int j = 0; j < bookmarks.Count; j++)
                            {
                                if ((BookmarkContext)bookmarks[j] != null)
                                {
                                    if (((BookmarkContext)bookmarks[j]).ActionPageNumber.ToString()
                                        == bookForm.BookmarksDeletedPageNumber[i].ToString()
                                        && ((BookmarkContext)bookmarks[j]).Title + " (page " +
                                        ((int)(((BookmarkContext)bookmarks[j]).ActionPageNumber + 1)).ToString() + ")"
                                        == bookForm.BookmarksDeletedTitle[i].ToString())
                                    {
                                        document.RemoveBookmark((BookmarkContext)bookmarks[j]);

                                        InitializeBookmarkTree();

                                        break;
                                    }
                                }
                            }
                        }

                        ImageLoad();
                    }
                }
            }
            catch (Exception ex)
            {
                imageXView.AllowUpdate = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void RendForm()
        {
            using (RenderForm rendForm = new RenderForm(rendOpts))
            {
                rendForm.Annotations = rendOpts.RenderAnnotations;
                rendForm.ReduceBitonal = rendOpts.ReduceToBitonal;
                rendForm.ResolutionX = rendOpts.ResolutionX;
                rendForm.ResolutionY = rendOpts.ResolutionY;
                rendForm.SelectionX = rendOpts.SelectionX;
                rendForm.SelectionY = rendOpts.SelectionY;
                rendForm.SelectionW = rendOpts.SelectionWidth;
                rendForm.SelectionH = rendOpts.SelectionHeight;

                if (rendForm.ShowDialog() == DialogResult.OK)
                {
                    rendOpts.ProduceDibSection = false;
                    rendOpts.RenderAnnotations = rendForm.Annotations;
                    rendOpts.ReduceToBitonal = rendForm.ReduceBitonal;
                    rendOpts.ResolutionX = rendForm.ResolutionX;
                    rendOpts.ResolutionY = rendForm.ResolutionY;
                    rendOpts.SelectionX = rendForm.SelectionX;
                    rendOpts.SelectionY = rendForm.SelectionY;
                    rendOpts.SelectionWidth = rendForm.SelectionW;
                    rendOpts.SelectionHeight = rendForm.SelectionH;

                    //resource cleanup
                    if (document != null)
                        document.Dispose();

                    document = new Document(pdfXpress1, oo);

                    useXFDF = rendForm.XFDF;

                    ImageLoad();
                }
            }
        }

        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                imagXpress1.AboutBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            pdfXpressToolStripMenuItem_Click(sender, e);
        }

        private void FindBox_Click(object sender, EventArgs e)
        {
            FindBox.SelectAll();
        }

        private void RectangleTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.RectangleTool, "RectangleTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OvalTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.EllipseTool, "OvalTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PolyLineTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.PolyLineTool, "PolyLineTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PolygonTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.PolygonTool, "PolygonTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PencilTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.FreehandTool, "PencilTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LineTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.LineTool, "LineTool");

                notateXpress1.Layers[0].Toolbar.LineToolbarDefaults.EndPointStyle = EndPointStyle.Plain;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ArrowTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.LineTool, "ArrowTool");

                notateXpress1.Layers[0].Toolbar.LineToolbarDefaults.EndPointStyle = EndPointStyle.Arrow;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.TextTool, "TextTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CrossOutTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.TextTool, "CrossOutTool");

                imageXView.ToolSet(Tool.Select, MouseButtons.Left, Keys.None);

                strikeOut = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void underlineTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.NoTool, "underlineTool");

                imageXView.ToolSet(Tool.Select, MouseButtons.Left, Keys.None);

                underLine = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void highlightTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.BlockHighlightTool, "highlightTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void noteTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.NoteTool, "noteTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AnnotationHelper(AnnotationTool tool, string name)
        {
            strikeOut = false;
            underLine = false;

            imageXView.ToolSet(Tool.None, MouseButtons.Left, Keys.None);

            notateXpress1.Layers[0].Toolbar.Selected = tool;

            for (int i = 0; i < AnnotationToolStrip.Length; i++)
            {
                if (name == AnnotationToolStrip[i].Name)
                {
                    AnnotationToolStrip[i].Checked = true;
                }
                else
                {
                    AnnotationToolStrip[i].Checked = false;
                }
            }
        }

        private void pointerTool_Click(object sender, EventArgs e)
        {
            try
            {
                AnnotationHelper(AnnotationTool.PointerTool, "pointerTool");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exportAnnotationsToXFDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    dlg.Title = "Export Annotations to XFDF";
                    dlg.FilterIndex = 0;
                    dlg.Filter = "XFDF (*.xfdf)|*.xfdf";
                    dlg.InitialDirectory = Path.GetFullPath(openPath);

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        SaveAnnotations();

                        using (TextWriter tw = new StreamWriter(dlg.FileName))
                        {
                            tw.WriteLine(Encoding.UTF8.GetString(document.ExportXfdf(xfdfOptions)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }        

        private float GetUserSpaceResolution(Document whichDocument, Int32 forPageIndex)
        {
            // PDF pages whose user space resolution is not 
            // the typical 72.0 will not be handled correctly.
            //
            return 72.0f;
        }

        /// <summary>
        /// Converts an image coordinate, expressed as a pixel at device resolution,
        /// as a PDF user space coordinate.
        /// </summary>
        /// <remarks>
        ///     PDF Xpress reports the Media and Crop (Clip) boxes after the
        ///     default page matrix has been applied. An accurate conversion may not
        ///     possible for PDF pages whose original Crop box and Media box are
        ///     not located at the user space origin.
        /// </remarks>
        /// <param name="imageLocation">A DIB coordinate in the image, expressed in
        /// pixels at device resolution.</param>
        /// <returns>The equivalent PDF user space coordinate on the current page.</returns>
        private PointF ImageCursorLocationToUserspaceLocation(Point imageLocation)
        {
            // Express image location as an offset from the image coordinate system
            // origin in userspace units.
            //
            float userspaceDpi = GetUserSpaceResolution(document, pageNumber);

            imageXView.Image.Resolution.Units = GraphicsUnit.Inch;

            SizeF imageOriginToLocation = new SizeF
                                                (imageLocation.X / imageXView.Image.Resolution.Dimensions.Width * userspaceDpi
                                                , imageLocation.Y / imageXView.Image.Resolution.Dimensions.Height * userspaceDpi
                                                );

            // The image origin corresponds to a different corner of the
            // page crop box depending on the intrinsic PDF page rotation.
            // Identify the corner of the image corresponding to the 
            // bottom-left corner of the crop box.
            //
            PointF userspaceLocation = new PointF();

            PageInfo info = document.GetInfo(pageNumber);

            switch (info.Rotated)
            {
                case Accusoft.PdfXpressSdk.RotateAngle.Rotate0:
                    // Bottom-left corner of the crop box corresponds to 
                    // the bottom-left corner of the image. 
                    //
                    userspaceLocation = new PointF
                                                ((float)(info.ClipX - info.MediaX + imageOriginToLocation.Width)
                                                , (float)(info.ClipY - info.MediaY + info.ClipHeight - imageOriginToLocation.Height)
                                                );
                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate90:
                    // Bottom-left corner of the crop box corresponds to
                    // the top-left corner of the image.
                    //
                    userspaceLocation = new PointF
                                                (imageOriginToLocation.Height - (float)info.MediaY
                                                , imageOriginToLocation.Width - (float)info.MediaX
                                                );
                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate180:
                    // Bottom-left corner of the crop box corresponds to 
                    // the top-right corner of the image.
                    //
                    userspaceLocation = new PointF
                                                ((float)(info.ClipX - info.MediaX + info.ClipWidth - imageOriginToLocation.Width)
                                                , (float)(info.ClipY - info.MediaY + imageOriginToLocation.Height)
                                                );
                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate270:
                    // Bottom-left corner of the crop box corresponds to 
                    // the bottom-right corner of the image.
                    //
                    userspaceLocation = new PointF
                                                ((float)(info.ClipY - info.MediaY + info.ClipHeight - imageOriginToLocation.Height)
                                                , (float)(info.ClipX - info.MediaX + info.ClipWidth - imageOriginToLocation.Width)
                                                );
                    break;
            }

            return userspaceLocation;
        }

        private Point underlineStartLocation = new Point();
        private Point strikeoutStartLocation = new Point();

        private void imageXView_ToolEvent(object sender, ToolEventArgs e)
        {
            try
            {
                if (e.Tool == Tool.Select)
                {
                    if (underLine == true)
                    {
                        switch (e.Action)
                        {
                            case ToolAction.Begin:
                                {
                                    // Capture the first of two locations
                                    //
                                    underlineStartLocation = imageXView.ImageCursorPosition;
                                    break;
                                }
                            case ToolAction.End:
                                {
                                    SaveAnnotations();

                                    // Express a new PDF text annotation as a single XFDF text annotation.
                                    // for the current page index.
                                    //
                                    CreateXfdfUnderlineAnnotationOptions underlineOptions
                                                                        = new CreateXfdfUnderlineAnnotationOptions();

                                    underlineOptions.Page = pageNumber;

                                    PointF bottomLeftLocation = ImageCursorLocationToUserspaceLocation
                                                                            (underlineStartLocation);

                                    PointF topRightLocation = ImageCursorLocationToUserspaceLocation
                                                                            (imageXView.ImageCursorPosition);

                                    underlineOptions.Rectangle = new RectangleF
                                                                            (bottomLeftLocation.X
                                                                            , bottomLeftLocation.Y
                                                                            , topRightLocation.X - bottomLeftLocation.X
                                                                            , topRightLocation.Y - bottomLeftLocation.Y
                                                                            );

                                    underlineOptions.RotateAngle = document.GetInfo(pageNumber).Rotated;

                                    byte[] utf16XfdfPacket = XfdfSnippets.CreateXfdfUnderlineAnnotation(underlineOptions);

                                    // Add the annotation to the current PDF page
                                    //
                                    XfdfOptions xfdfOptions = new XfdfOptions();
                                    xfdfOptions.CanCreateAnnotations = true;
                                    xfdfOptions.CanModifyAnnotations = false;
                                    xfdfOptions.CanDeleteAnnotations = false;
                                    xfdfOptions.WhichPage = pageNumber;

                                    document.ImportXfdf(xfdfOptions, utf16XfdfPacket);

                                    UpdatePage();

                                    AnnotationHelper(AnnotationTool.NoTool, "underlineTool");

                                    imageXView.ToolSet(Tool.Select, MouseButtons.Left, Keys.None);

                                    underLine = true;

                                    break;
                                }
                        }
                    }
                    else if (strikeOut == true)
                    {
                        switch (e.Action)
                        {
                            case ToolAction.Begin:
                                {
                                    // Capture the first of two locations
                                    //
                                    strikeoutStartLocation = imageXView.ImageCursorPosition;
                                    break;
                                }
                            case ToolAction.End:
                                {
                                    SaveAnnotations();

                                    // Express a new PDF text annotation as a single XFDF text annotation.
                                    // for the current page index.
                                    //
                                    CreateXfdfStrikeoutAnnotationOptions strikeOutOptions
                                                                        = new CreateXfdfStrikeoutAnnotationOptions();

                                    strikeOutOptions.Page = pageNumber;

                                    PointF bottomLeftLocation = ImageCursorLocationToUserspaceLocation
                                                                            (strikeoutStartLocation);

                                    PointF topRightLocation = ImageCursorLocationToUserspaceLocation
                                                                            (imageXView.ImageCursorPosition);

                                    strikeOutOptions.Rectangle = new RectangleF
                                                                            (bottomLeftLocation.X
                                                                            , bottomLeftLocation.Y
                                                                            , topRightLocation.X - bottomLeftLocation.X
                                                                            , topRightLocation.Y - bottomLeftLocation.Y
                                                                            );

                                    strikeOutOptions.RotateAngle = document.GetInfo(pageNumber).Rotated;

                                    byte[] utf16XfdfPacket = XfdfSnippets.CreateXfdfStrikeoutAnnotation(strikeOutOptions);

                                    // Add the annotation to the current PDF page
                                    //
                                    XfdfOptions xfdfOptions = new XfdfOptions();
                                    xfdfOptions.CanCreateAnnotations = true;
                                    xfdfOptions.CanModifyAnnotations = false;
                                    xfdfOptions.CanDeleteAnnotations = false;
                                    xfdfOptions.WhichPage = pageNumber;

                                    document.ImportXfdf(xfdfOptions, utf16XfdfPacket);

                                    UpdatePage();

                                    AnnotationHelper(AnnotationTool.NoTool, "CrossOutTool");

                                    imageXView.ToolSet(Tool.Select, MouseButtons.Left, Keys.None);

                                    strikeOut = true;

                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void importAnnotationsToXFDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Title = "Import Annotations from XFDF";
                    dlg.FilterIndex = 0;
                    dlg.Filter = "XFDF (*.xfdf)|*.xfdf";
                    dlg.InitialDirectory = Path.GetFullPath(openPath);

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        using (FileStream fs = new FileStream(dlg.FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            byte[] data = new byte[(int)fs.Length];
                            fs.Read(data, 0, data.Length);

                            document.ImportXfdf(xfdfOptions, data);

                            UpdatePage();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.AboutBox();
        }
     
        private void zoomComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (zoomComboBox.SelectedIndex)
            {
                case 0:
                    {
                        imageXView.AutoResize = AutoResizeType.BestFit;
                        break;
                    }
                case 1:
                    {
                        imageXView.AutoResize = AutoResizeType.CropImage;
                        imageXView.ZoomFactor = 1.0;
                        break;
                    }
                case 2:
                    {
                        imageXView.AutoResize = AutoResizeType.FitWidth;
                        break;
                    }
                case 3:
                    {
                        imageXView.AutoResize = AutoResizeType.FitHeight;
                        break;
                    }
            }
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            zoomComboBox_SelectedIndexChanged(sender, e);
        }

        private void AnnotationUpdate()
        {
            try
            {
                if (useXFDF == false)
                {
                    Accusoft.NotateXpressSdk.SaveOptions so = new Accusoft.NotateXpressSdk.SaveOptions();
                    so.AnnType = AnnotationType.Xfdf;

                    if (layerData.Count > pageNumber)
                    {
                        layerData.RemoveAt(pageNumber);
                    }

                    notateXpress1.PageNumber = pageNumber + 1;
                    layerData.Insert(pageNumber, notateXpress1.Layers.SaveToByteArray(so));
                }

                notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;

                for (int i = 0; i < AnnotationToolStrip.Length; i++)
                {
                    if ("PointerTool" != AnnotationToolStrip[i].Name)
                    {
                        AnnotationToolStrip[i].Checked = false;
                    }
                }

                PointerTool.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_AnnotationAdded(object sender, AnnotationAddedEventArgs e)
        {
            AnnotationUpdate();
        }

        private void notateXpress1_AnnotationDeleted(object sender, AnnotationDeletedEventArgs e)
        {
            AnnotationUpdate();
        }

        private void notateXpress1_AnnotationMoved(object sender, AnnotationMovedEventArgs e)
        {
            AnnotationUpdate();
        }

        private void imageXView_ZoomFactorChanged(object sender, ZoomFactorChangedEventArgs e)
        {
            switch (imageXView.AutoResize)
            {
                case AutoResizeType.BestFit:
                    {
                        zoomComboBox.SelectedIndex = 0;
                        break;
                    }
                case AutoResizeType.CropImage:
                    {
                        zoomComboBox.SelectedIndex = 1;
                        break;
                    }
                case AutoResizeType.FitWidth:
                    {
                        zoomComboBox.SelectedIndex = 2;
                        break;
                    }
                case AutoResizeType.FitHeight:
                    {
                        zoomComboBox.SelectedIndex = 3;
                        break;
                    }
            }
        }

        private void deletePageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (DeleteForm deleteForm = new DeleteForm())
                {
                    if (deleteForm.ShowDialog() == DialogResult.OK)
                    {
                        SaveAnnotations();

                        document.DeletePage(deleteForm.PageNumber);

                        ImageLoad();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void beforeWordsBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void afterWordsBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void removeWatermarkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (RemoveWatermarkForm watermarkForm = new RemoveWatermarkForm())
                {
                    helper.CenterScreen(watermarkForm);

                    watermarkForm.PageCount = pageCount;
                    watermarkForm.ToPageNumber = pageCount;

                    if (watermarkForm.ShowDialog() == DialogResult.OK)
                    {
                        int fromPageNumber = watermarkForm.FromPageNumber - 1;
                        int toPageNumber = watermarkForm.ToPageNumber - 1;

                        for (int i = fromPageNumber; i <= toPageNumber; i++)
                        {
                            document.DeleteWatermark(i);
                        }

                        SaveAnnotations();

                        UpdatePage();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addWatermarkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAnnotations();

            if (strTempFileName != "")
            {
                strTempSavedFileName = strTempFileName + ".pdf";

                strTempSavedFileNames.Add(strTempSavedFileName);

                so.Filename = strTempSavedFileName;
                so.Linearized = true;
                so.Overwrite = true;

                document.Save(so);
            }

            Document sourceDoc = null;

            try
            {
                using (AddWatermarkForm watermarkForm = new AddWatermarkForm())
                {
                    helper.CenterScreen(watermarkForm);

                    if (strTempSavedFileName != "")
                    {
                        watermarkForm.FileName = strTempSavedFileName;
                    }
                    else
                    {
                        watermarkForm.FileName = strFileName;
                    }

                    if (watermarkForm.ShowDialog() == DialogResult.OK)
                    {
                        PageArtifactOptions po = new PageArtifactOptions();

                        po.HorizontalAlignment = watermarkForm.HorzAlign;
                        po.HorizontalOffset = watermarkForm.HorzOffset / 100;
                        po.Opacity = watermarkForm.OpacityArtifact / 100;
                        po.Rotation = watermarkForm.Rotation;
                        po.ScaleFactor = watermarkForm.ScaleFactor / 100;
                        po.VerticalAlignment = (Accusoft.PdfXpressSdk.VerticalAlignment)
                                           watermarkForm.VertAlign;
                        po.VerticalOffset = watermarkForm.VertOffset / 100;
                        po.Zorder = watermarkForm.ZOrder;
                        po.FixedPrint = watermarkForm.FixedPrint;
                        po.ShowOnPrint = watermarkForm.ShowOnPrint;
                        po.ShowOnView = watermarkForm.ShowOnView;

                        PageList pageListing = new PageList();
                        PageRange range = new PageRange(0, pageCount);

                        pageListing.Add(range);

                        po.PageList = pageListing;

                        if (watermarkForm.ImageFileName != "")
                        {
                            sourceDoc = new Document(pdfXpress1, watermarkForm.ImageFileName);

                            po.SourcePageNumber = watermarkForm.PageNumber - 1;
                            po.SourceDocument = sourceDoc;

                            po.ArtifactType = TypeOfArtifact.SourceWatermark;
                        }
                        else
                        {
                            po.Text = watermarkForm.TextArtifact;
                            po.TextAlignment = watermarkForm.TextAlign;
                            po.TextColor = watermarkForm.TextColor;
                            po.TextSize = watermarkForm.TextSize;

                            po.ArtifactType = TypeOfArtifact.TextWatermark;
                        }

                        document.AddWatermark(po);

                        UpdatePage();

                        strTempFileName = strFileName + ".pdf";

                        if (helper.SafelyDeleteFile(strTempFileName) == true)
                        {
                            File.Copy(strFileName, strTempFileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //cleanup
                if (sourceDoc != null)
                {
                    sourceDoc.Dispose();
                    sourceDoc = null;
                }
            }
        }

        private void ConvertDocumentToPdfA(Compression compressValue)
        {
            RenderOptions renderPdfAOpts = new RenderOptions();
            renderPdfAOpts.ProduceDibSection = false;
            renderPdfAOpts.RenderAnnotations = true;
            renderPdfAOpts.ResolutionX = rendOpts.ResolutionX;
            renderPdfAOpts.ResolutionY = rendOpts.ResolutionY;

            Accusoft.ImagXpressSdk.SaveOptions imageSO = new Accusoft.ImagXpressSdk.SaveOptions();

            using (Document imageDoc = new Document(pdfXpress1))
            {
                for (int i = 0; i < pageCount; i++)
                {
                    PageInfo info = document.GetInfo(i);

                    switch (compressValue)
                    {
                        case Compression.JBIG2:
                            {
                                renderPdfAOpts.ReduceToBitonal = true;
                                imageSO.Format = ImageXFormat.Jbig2;
                                imageSO.Jbig2.LoosenessCompression = 1;
                                break;
                            }
                        case Compression.JPEG:
                            {
                                imageSO.Format = ImageXFormat.Jpeg;
                                imageSO.Jpeg.Chrominance = 40;
                                imageSO.Jpeg.Luminance = 40;
                                break;
                            }
                        case Compression.G4:
                            {
                                renderPdfAOpts.ReduceToBitonal = true;
                                imageSO.Format = ImageXFormat.Tiff;
                                imageSO.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4;
                                break;
                            }
                    }

                    using (Bitmap bp = document.RenderPageToBitmap(i, renderPdfAOpts))
                    {
                        using (ImageX img = ImageX.FromBitmap(imagXpress1, bp))
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                img.SaveStream(ms, imageSO);

                                ms.Seek(0, SeekOrigin.Begin);

                                PageOptions po = new PageOptions();
                                po.MediaWidth = info.MediaWidth;
                                po.MediaHeight = info.MediaHeight;

                                imageDoc.CreatePage(i - 1, po);

                                imageDoc.AddImage(i, 0, 0, info.MediaWidth, info.MediaHeight, ImageFitSettings.None, ms.ToArray(), 0);
                            }
                        }
                    }
                }

                ConvertOptions co = new ConvertOptions();
                co.PdfType = PdfType.pdfA1b;
                co.IccProfileFilename = iccPath;

                imageDoc.ConvertToPdfA(co);

                so.Filename = savePath;
                so.Overwrite = true;
                so.Linearized = true;

                imageDoc.Save(so);
            }
        }

        private void savePdfAMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    dlg.Title = "Save the PDF Document As PDF/A-1b";

                    if (savePath == "")
                    {
                        savePath = openPath;
                    }

                    dlg.InitialDirectory = Path.GetFullPath(Path.GetDirectoryName(savePath));
                    dlg.FilterIndex = 0;
                    dlg.Filter = "PDFA-1b Document (*.PDF)|*.PDF";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        using (CompressForm compressForm = new CompressForm())
                        {
                            helper.CenterScreen(compressForm);

                            if (compressForm.ShowDialog() == DialogResult.OK)
                            {
                                savePath = dlg.FileName;

                                SaveAnnotations();

                                ConvertDocumentToPdfA(compressForm.CompressionSelected);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CompressForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.G4RadioButton = New System.Windows.Forms.RadioButton
        Me.JpegRadioButton = New System.Windows.Forms.RadioButton
        Me.Jbig2RadioButton = New System.Windows.Forms.RadioButton
        Me.button2 = New System.Windows.Forms.Button
        Me.button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'G4RadioButton
        '
        Me.G4RadioButton.AutoSize = True
        Me.G4RadioButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.G4RadioButton.Location = New System.Drawing.Point(12, 63)
        Me.G4RadioButton.Name = "G4RadioButton"
        Me.G4RadioButton.Size = New System.Drawing.Size(174, 17)
        Me.G4RadioButton.TabIndex = 5
        Me.G4RadioButton.TabStop = True
        Me.G4RadioButton.Text = "CCITT G4 (works well for B&&W)"
        Me.G4RadioButton.UseVisualStyleBackColor = True
        '
        'JpegRadioButton
        '
        Me.JpegRadioButton.AutoSize = True
        Me.JpegRadioButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JpegRadioButton.Location = New System.Drawing.Point(12, 40)
        Me.JpegRadioButton.Name = "JpegRadioButton"
        Me.JpegRadioButton.Size = New System.Drawing.Size(201, 17)
        Me.JpegRadioButton.TabIndex = 4
        Me.JpegRadioButton.TabStop = True
        Me.JpegRadioButton.Text = "JPEG (works best for color but lossy)"
        Me.JpegRadioButton.UseVisualStyleBackColor = True
        '
        'Jbig2RadioButton
        '
        Me.Jbig2RadioButton.AutoSize = True
        Me.Jbig2RadioButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Jbig2RadioButton.Location = New System.Drawing.Point(12, 17)
        Me.Jbig2RadioButton.Name = "Jbig2RadioButton"
        Me.Jbig2RadioButton.Size = New System.Drawing.Size(159, 17)
        Me.Jbig2RadioButton.TabIndex = 3
        Me.Jbig2RadioButton.TabStop = True
        Me.Jbig2RadioButton.Text = "JBIG2 (works best for B&&W)"
        Me.Jbig2RadioButton.UseVisualStyleBackColor = True
        '
        'button2
        '
        Me.button2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button2.Location = New System.Drawing.Point(192, 92)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(59, 33)
        Me.button2.TabIndex = 7
        Me.button2.Text = "Cancel"
        Me.button2.UseVisualStyleBackColor = True
        '
        'button1
        '
        Me.button1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button1.Location = New System.Drawing.Point(12, 92)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(59, 33)
        Me.button1.TabIndex = 6
        Me.button1.Text = "OK"
        Me.button1.UseVisualStyleBackColor = True
        '
        'CompressForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(263, 137)
        Me.Controls.Add(Me.button2)
        Me.Controls.Add(Me.button1)
        Me.Controls.Add(Me.G4RadioButton)
        Me.Controls.Add(Me.JpegRadioButton)
        Me.Controls.Add(Me.Jbig2RadioButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CompressForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Compression Options"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents G4RadioButton As System.Windows.Forms.RadioButton
    Private WithEvents JpegRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents Jbig2RadioButton As System.Windows.Forms.RadioButton
    Private WithEvents button2 As System.Windows.Forms.Button
    Private WithEvents button1 As System.Windows.Forms.Button

End Class

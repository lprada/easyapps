'****************************************************************'
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not (pdfXpress Is Nothing) Then
                pdfXpress.Dispose()
                pdfXpress = Nothing
            End If
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AddFromFileButton = New System.Windows.Forms.Button
        Me.AddFromByteArrayButton = New System.Windows.Forms.Button
        Me.AddFromHandleButton = New System.Windows.Forms.Button
        Me.AddFromDataInfoButton = New System.Windows.Forms.Button
        Me.TopListBox = New System.Windows.Forms.ListBox
        Me.MenuStrip = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PDFXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OpenDialog = New System.Windows.Forms.OpenFileDialog
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.pdfXpress = New Accusoft.PdfXpressSdk.PdfXpress
        Me.MenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'AddFromFileButton
        '
        Me.AddFromFileButton.Location = New System.Drawing.Point(109, 102)
        Me.AddFromFileButton.Name = "AddFromFileButton"
        Me.AddFromFileButton.Size = New System.Drawing.Size(207, 32)
        Me.AddFromFileButton.TabIndex = 0
        Me.AddFromFileButton.Text = "Add Image From File"
        Me.AddFromFileButton.UseVisualStyleBackColor = True
        '
        'AddFromByteArrayButton
        '
        Me.AddFromByteArrayButton.Location = New System.Drawing.Point(109, 150)
        Me.AddFromByteArrayButton.Name = "AddFromByteArrayButton"
        Me.AddFromByteArrayButton.Size = New System.Drawing.Size(207, 32)
        Me.AddFromByteArrayButton.TabIndex = 1
        Me.AddFromByteArrayButton.Text = "Add Image From Byte Array"
        Me.AddFromByteArrayButton.UseVisualStyleBackColor = True
        '
        'AddFromHandleButton
        '
        Me.AddFromHandleButton.Location = New System.Drawing.Point(109, 201)
        Me.AddFromHandleButton.Name = "AddFromHandleButton"
        Me.AddFromHandleButton.Size = New System.Drawing.Size(207, 32)
        Me.AddFromHandleButton.TabIndex = 2
        Me.AddFromHandleButton.Text = "Add Image From Handle"
        Me.AddFromHandleButton.UseVisualStyleBackColor = True
        '
        'AddFromDataInfoButton
        '
        Me.AddFromDataInfoButton.Location = New System.Drawing.Point(109, 252)
        Me.AddFromDataInfoButton.Name = "AddFromDataInfoButton"
        Me.AddFromDataInfoButton.Size = New System.Drawing.Size(207, 32)
        Me.AddFromDataInfoButton.TabIndex = 3
        Me.AddFromDataInfoButton.Text = "Add Image From DataInfo"
        Me.AddFromDataInfoButton.UseVisualStyleBackColor = True
        '
        'TopListBox
        '
        Me.TopListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TopListBox.FormattingEnabled = True
        Me.TopListBox.Items.AddRange(New Object() {"This sample demonstrates how an image file can be used to create ", "a single-page PDF.  Click the Add Image button to select an image and ", "specify the location to save the generated PDF."})
        Me.TopListBox.Location = New System.Drawing.Point(7, 27)
        Me.TopListBox.Name = "TopListBox"
        Me.TopListBox.Size = New System.Drawing.Size(420, 56)
        Me.TopListBox.TabIndex = 4
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(431, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PDFXpressToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'PDFXpressToolStripMenuItem
        '
        Me.PDFXpressToolStripMenuItem.Name = "PDFXpressToolStripMenuItem"
        Me.PDFXpressToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PDFXpressToolStripMenuItem.Text = "&PDF Xpress"
        '
        'OpenDialog
        '
        Me.OpenDialog.FileName = "OpenFileDialog1"
        '
        'pdfXpress
        '
        Me.pdfXpress.Debug = False
        Me.pdfXpress.DebugLogFile = ""
        Me.pdfXpress.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(431, 301)
        Me.Controls.Add(Me.TopListBox)
        Me.Controls.Add(Me.AddFromDataInfoButton)
        Me.Controls.Add(Me.AddFromHandleButton)
        Me.Controls.Add(Me.AddFromByteArrayButton)
        Me.Controls.Add(Me.AddFromFileButton)
        Me.Controls.Add(Me.MenuStrip)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.MenuStrip
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Image"
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents AddFromFileButton As System.Windows.Forms.Button
    Friend WithEvents AddFromByteArrayButton As System.Windows.Forms.Button
    Friend WithEvents AddFromHandleButton As System.Windows.Forms.Button
    Friend WithEvents AddFromDataInfoButton As System.Windows.Forms.Button
    Friend WithEvents TopListBox As System.Windows.Forms.ListBox
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PDFXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents pdfXpress As Accusoft.PdfXpressSdk.PdfXpress

End Class

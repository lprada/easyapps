'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Public Class FormSplash
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private WithEvents Logo As System.Windows.Forms.PictureBox
    Public WithEvents cmdPurchase As System.Windows.Forms.Button
    Public WithEvents lblProductName As System.Windows.Forms.Label
    Public WithEvents cmdDownload As System.Windows.Forms.Button
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Private WithEvents DescriptionLabel As System.Windows.Forms.Label
    Public WithEvents cmdOK As System.Windows.Forms.Button
    Public WithEvents lblURL As System.Windows.Forms.Label
    Public WithEvents lblCompany As System.Windows.Forms.Label
    Public WithEvents fraMainFrame As System.Windows.Forms.GroupBox
    Public WithEvents lblCopyright As System.Windows.Forms.Label

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormSplash))
        Me.Logo = New System.Windows.Forms.PictureBox
        Me.cmdPurchase = New System.Windows.Forms.Button
        Me.lblProductName = New System.Windows.Forms.Label
        Me.cmdDownload = New System.Windows.Forms.Button
        Me.label3 = New System.Windows.Forms.Label
        Me.label2 = New System.Windows.Forms.Label
        Me.label1 = New System.Windows.Forms.Label
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.DescriptionLabel = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.lblURL = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.fraMainFrame = New System.Windows.Forms.GroupBox
        Me.lblCopyright = New System.Windows.Forms.Label
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraMainFrame.SuspendLayout()
        Me.SuspendLayout()
        '
        'Logo
        '
        Me.Logo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Logo.BackColor = System.Drawing.Color.Transparent
        Me.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Logo.Image = CType(resources.GetObject("Logo.Image"), System.Drawing.Image)
        Me.Logo.Location = New System.Drawing.Point(12, 32)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(225, 173)
        Me.Logo.TabIndex = 20
        Me.Logo.TabStop = False
        '
        'cmdPurchase
        '
        Me.cmdPurchase.BackColor = System.Drawing.Color.White
        Me.cmdPurchase.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdPurchase.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdPurchase.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdPurchase.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdPurchase.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdPurchase.ForeColor = System.Drawing.Color.White
        Me.cmdPurchase.Image = CType(resources.GetObject("cmdPurchase.Image"), System.Drawing.Image)
        Me.cmdPurchase.Location = New System.Drawing.Point(291, 314)
        Me.cmdPurchase.Name = "cmdPurchase"
        Me.cmdPurchase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdPurchase.Size = New System.Drawing.Size(272, 59)
        Me.cmdPurchase.TabIndex = 13
        Me.cmdPurchase.UseVisualStyleBackColor = False
        '
        'lblProductName
        '
        Me.lblProductName.BackColor = System.Drawing.Color.White
        Me.lblProductName.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProductName.Font = New System.Drawing.Font("Calibri", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProductName.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProductName.Location = New System.Drawing.Point(231, 32)
        Me.lblProductName.Name = "lblProductName"
        Me.lblProductName.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProductName.Size = New System.Drawing.Size(481, 78)
        Me.lblProductName.TabIndex = 0
        Me.lblProductName.Tag = "Product"
        Me.lblProductName.Text = "Welcome to the PDF Xpress v4 Demo"
        Me.lblProductName.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdDownload
        '
        Me.cmdDownload.BackColor = System.Drawing.Color.White
        Me.cmdDownload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.cmdDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdDownload.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdDownload.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.cmdDownload.FlatAppearance.BorderSize = 0
        Me.cmdDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmdDownload.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDownload.ForeColor = System.Drawing.Color.White
        Me.cmdDownload.Image = CType(resources.GetObject("cmdDownload.Image"), System.Drawing.Image)
        Me.cmdDownload.Location = New System.Drawing.Point(291, 250)
        Me.cmdDownload.Name = "cmdDownload"
        Me.cmdDownload.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdDownload.Size = New System.Drawing.Size(272, 58)
        Me.cmdDownload.TabIndex = 12
        Me.cmdDownload.UseVisualStyleBackColor = False
        '
        'label3
        '
        Me.label3.BackColor = System.Drawing.Color.White
        Me.label3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(301, 208)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(301, 26)
        Me.label3.TabIndex = 10
        Me.label3.Text = "�  Searching and Highlighting text"
        '
        'label2
        '
        Me.label2.BackColor = System.Drawing.Color.White
        Me.label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(301, 181)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(301, 26)
        Me.label2.TabIndex = 9
        Me.label2.Text = "�  Merging and Splitting PDF files"
        '
        'label1
        '
        Me.label1.BackColor = System.Drawing.Color.White
        Me.label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(301, 154)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(353, 26)
        Me.label1.TabIndex = 8
        Me.label1.Text = "�  PDF Annotations, Bookmarks, and Metadata"
        '
        'DescriptionLabel
        '
        Me.DescriptionLabel.BackColor = System.Drawing.Color.White
        Me.DescriptionLabel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionLabel.Location = New System.Drawing.Point(234, 110)
        Me.DescriptionLabel.Name = "DescriptionLabel"
        Me.DescriptionLabel.Size = New System.Drawing.Size(443, 42)
        Me.DescriptionLabel.TabIndex = 1
        Me.DescriptionLabel.Text = "Quickly build applications to create PDFs, including:"
        Me.DescriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cmdOK.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.Location = New System.Drawing.Point(612, 390)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdOK.Size = New System.Drawing.Size(82, 29)
        Me.cmdOK.TabIndex = 7
        Me.cmdOK.Text = "Continue"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'lblURL
        '
        Me.lblURL.AccessibleDescription = "0"
        Me.lblURL.AutoSize = True
        Me.lblURL.BackColor = System.Drawing.Color.White
        Me.lblURL.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblURL.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblURL.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblURL.Location = New System.Drawing.Point(23, 409)
        Me.lblURL.Name = "lblURL"
        Me.lblURL.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblURL.Size = New System.Drawing.Size(105, 14)
        Me.lblURL.TabIndex = 6
        Me.lblURL.Tag = "Warning"
        Me.lblURL.Text = "www.accusoft.com"
        Me.lblURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCompany
        '
        Me.lblCompany.AccessibleDescription = "0"
        Me.lblCompany.BackColor = System.Drawing.Color.White
        Me.lblCompany.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCompany.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCompany.Location = New System.Drawing.Point(23, 392)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCompany.Size = New System.Drawing.Size(177, 17)
        Me.lblCompany.TabIndex = 5
        Me.lblCompany.Tag = "Company"
        Me.lblCompany.Text = "Accusoft Pegasus"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fraMainFrame
        '
        Me.fraMainFrame.BackColor = System.Drawing.Color.White
        Me.fraMainFrame.Controls.Add(Me.Logo)
        Me.fraMainFrame.Controls.Add(Me.cmdPurchase)
        Me.fraMainFrame.Controls.Add(Me.lblProductName)
        Me.fraMainFrame.Controls.Add(Me.cmdDownload)
        Me.fraMainFrame.Controls.Add(Me.label3)
        Me.fraMainFrame.Controls.Add(Me.label2)
        Me.fraMainFrame.Controls.Add(Me.label1)
        Me.fraMainFrame.Controls.Add(Me.DescriptionLabel)
        Me.fraMainFrame.Controls.Add(Me.cmdOK)
        Me.fraMainFrame.Controls.Add(Me.lblURL)
        Me.fraMainFrame.Controls.Add(Me.lblCompany)
        Me.fraMainFrame.Controls.Add(Me.lblCopyright)
        Me.fraMainFrame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.fraMainFrame.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.fraMainFrame.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraMainFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraMainFrame.Location = New System.Drawing.Point(0, 0)
        Me.fraMainFrame.Name = "fraMainFrame"
        Me.fraMainFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMainFrame.Size = New System.Drawing.Size(731, 435)
        Me.fraMainFrame.TabIndex = 1
        Me.fraMainFrame.TabStop = False
        '
        'lblCopyright
        '
        Me.lblCopyright.AccessibleDescription = "0"
        Me.lblCopyright.BackColor = System.Drawing.Color.White
        Me.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCopyright.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyright.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCopyright.Location = New System.Drawing.Point(23, 375)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCopyright.Size = New System.Drawing.Size(177, 17)
        Me.lblCopyright.TabIndex = 4
        Me.lblCopyright.Tag = "Copyright"
        Me.lblCopyright.Text = "Copyright � 1997-2010"
        Me.lblCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FormSplash
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(731, 435)
        Me.Controls.Add(Me.fraMainFrame)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormSplash"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PDF Xpress v4 Demo"
        Me.TopMost = True
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraMainFrame.ResumeLayout(False)
        Me.fraMainFrame.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPurchase.Click
        Dim theUrl As String = "http://www.pegasusimaging.com/pdfxpresspricing.htm"
        System.Diagnostics.Process.Start(theUrl)
    End Sub

    Private Sub cmdDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDownload.Click
        Dim theUrl As String = "http://www.pegasusimaging.com/pdfxpressdownload.htm"
        System.Diagnostics.Process.Start(theUrl)
    End Sub
End Class

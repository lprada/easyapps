/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace PDFDemo
{
    public enum Compression
    {
        JPEG = 0,
        JBIG2 = 1,
        G4 = 2,
    }

    class Helper
    {
        public Helper()
        {

        }
      
        public bool SafelyDeleteFile(string fileName)
        {
            //check if filename is not 0 length
            if (fileName != "")
            {
                //check if file exists
                if (File.Exists(fileName) == true)
                {
                    FileAttributes fileAttr = File.GetAttributes(fileName);

                    //if file is read-only change it to be normal
                    if (fileAttr == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(fileName, FileAttributes.Normal);
                    }

                    try
                    {
                        //check if file is being used by another process by setting the FileShare to not allow
                        //any other process to work, if it is being used by another process this should throw 
                        //an IOException
                        using (FileStream fs = new FileStream(fileName, FileMode.Open,
                                                              FileAccess.Read, FileShare.None)) { };

                        File.Delete(fileName);
                    }
                    catch (IOException)
                    {
                        //can't delete but this is expected if file is used by another process
                        return false;
                    }
                    catch (Exception ex)
                    {
                        //something else went wrong
                        MessageBox.Show(ex.Message);

                        return false;
                    }
                }
            }

            return true;
        }

        public void SafelyDeleteFiles(string[] fileNames)
        {
            foreach (string fileName in fileNames)
            {
                SafelyDeleteFile(fileName);
            }
        }

        public void CenterScreen(Form form)
        {
            //want to keep main form on primary screen
            Screen screen = Screen.PrimaryScreen;

            form.StartPosition = FormStartPosition.Manual;

            //equivalent StartPosition of CenterScreen
            form.Location = new Point((screen.WorkingArea.Width - form.Width) / 2, 
                                        (screen.WorkingArea.Height - form.Height) / 2);
        }
    }
}

namespace ManipulatePdfAnnotations
{
    partial class ManipulatePdfAnnotationsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                // Close open document
                //
                this.ClosePdfDocumentCommand( );

                this._imagxview.Dispose( );
                this._pdfxpress.Dispose( );
                this._imagexpress.Dispose( );

                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManipulatePdfAnnotationsForm));
            this._mainStatusStrip = new System.Windows.Forms.StatusStrip();
            this._pageSizeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._messageTextBox = new System.Windows.Forms.TextBox();
            this._imagexpress = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this._notatexpress = new Accusoft.NotateXpressSdk.NotateXpress(this.components);
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this._imagxview = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this._descriptionLabel = new System.Windows.Forms.Label();
            this._mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this._fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this._saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this._exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._commentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._importCommentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._exportToDataFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._gotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._firstPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._previousPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._nextPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._lastPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this._zoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._zoomToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._actualSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._fitWidthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._fitHeightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.showZoomToolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this._toolbarsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._commentMarkupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._navigationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._zoomToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this._showAllToolbarsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._hideAllToolbarsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._commentsMarkupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._stickyNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.highlightTextToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineTextToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crossOutTextToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxTextToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrowToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectangleToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ovalToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.polygonLineToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.polygonToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pencilToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._showCommentMarkupToolbarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this._helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._aboutImagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._aboutNotateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._aboutPDFXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._zoomToolStrip = new System.Windows.Forms.ToolStrip();
            this._currentZoomPercentageToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._zoomToToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._navigationToolStrip = new System.Windows.Forms.ToolStrip();
            this._previousPageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._nextPageToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._currentPageToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this._pageCountToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this._commentMarkupToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this._stickyNoteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._highlightTextToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._underlineTextToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._crossOutTextTool = new System.Windows.Forms.ToolStripButton();
            this._textBoxToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._arrowToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._lineToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._rectangleToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._ovalToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._polygonLineToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._polygonToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._pencilToolStripButton = new System.Windows.Forms.ToolStripButton();
            this._pdfxpress = new Accusoft.PdfXpressSdk.PdfXpress();
            this._mainStatusStrip.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this._mainMenuStrip.SuspendLayout();
            this._zoomToolStrip.SuspendLayout();
            this._navigationToolStrip.SuspendLayout();
            this._commentMarkupToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _mainStatusStrip
            // 
            this._mainStatusStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._mainStatusStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this._mainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._pageSizeToolStripStatusLabel});
            this._mainStatusStrip.Location = new System.Drawing.Point(0, 0);
            this._mainStatusStrip.Name = "_mainStatusStrip";
            this._mainStatusStrip.Size = new System.Drawing.Size(627, 22);
            this._mainStatusStrip.TabIndex = 1;
            this._mainStatusStrip.Text = "statusStrip1";
            // 
            // _pageSizeToolStripStatusLabel
            // 
            this._pageSizeToolStripStatusLabel.Name = "_pageSizeToolStripStatusLabel";
            this._pageSizeToolStripStatusLabel.Size = new System.Drawing.Size(76, 17);
            this._pageSizeToolStripStatusLabel.Text = "0.00 x 0.00 in.";
            // 
            // _messageTextBox
            // 
            this._messageTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._messageTextBox.Location = new System.Drawing.Point(0, 432);
            this._messageTextBox.Multiline = true;
            this._messageTextBox.Name = "_messageTextBox";
            this._messageTextBox.ReadOnly = true;
            this._messageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._messageTextBox.Size = new System.Drawing.Size(627, 58);
            this._messageTextBox.TabIndex = 2;
            // 
            // _notatexpress
            // 
            this._notatexpress.AllowPaint = true;
            this._notatexpress.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal;
            this._notatexpress.ImagXpressLoad = true;
            this._notatexpress.ImagXpressSave = true;
            this._notatexpress.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit;
            this._notatexpress.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal;
            this._notatexpress.MultiLineEdit = false;
            this._notatexpress.RecalibrateXdpi = -1;
            this._notatexpress.RecalibrateYdpi = -1;
            this._notatexpress.ToolTipTimeEdit = 0;
            this._notatexpress.ToolTipTimeInteractive = 0;
            this._notatexpress.MappingInput += new Accusoft.NotateXpressSdk.NotateXpress.MappingInputEventHandler(this._notatexpress_MappingInput);
            this._notatexpress.MappingOutput += new Accusoft.NotateXpressSdk.NotateXpress.MappingOutputEventHandler(this._notatexpress_MappingOutput);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this._mainStatusStrip);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this._imagxview);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitter1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this._messageTextBox);
            this.toolStripContainer1.ContentPanel.Controls.Add(this._descriptionLabel);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(627, 490);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.LeftToolStripPanelVisible = false;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(627, 586);
            this.toolStripContainer1.TabIndex = 3;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this._mainMenuStrip);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this._zoomToolStrip);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this._navigationToolStrip);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this._commentMarkupToolStrip);
            // 
            // _imagxview
            // 
            this._imagxview.AutoScroll = true;
            this._imagxview.Dock = System.Windows.Forms.DockStyle.Fill;
            this._imagxview.Location = new System.Drawing.Point(0, 30);
            this._imagxview.Name = "_imagxview";
            this._imagxview.Size = new System.Drawing.Size(627, 399);
            this._imagxview.Smoothing = false;
            this._imagxview.TabIndex = 5;
            this._imagxview.PaintImage += new Accusoft.ImagXpressSdk.ImageXView.PaintImageEventHandler(this._imagxview_PaintImage);
            this._imagxview.ZoomFactorChanged += new Accusoft.ImagXpressSdk.ImageXView.ZoomFactorChangedEventHandler(this._imagxview_ZoomFactorChanged);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 429);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(627, 3);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // _descriptionLabel
            // 
            this._descriptionLabel.BackColor = System.Drawing.SystemColors.Info;
            this._descriptionLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._descriptionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this._descriptionLabel.Location = new System.Drawing.Point(0, 0);
            this._descriptionLabel.Name = "_descriptionLabel";
            this._descriptionLabel.Size = new System.Drawing.Size(627, 30);
            this._descriptionLabel.TabIndex = 5;
            this._descriptionLabel.Text = "This sample demonstrates using NotateXpress to manipulate PDF Annotations and Ima" +
                "gXpress to delegate rendering activities to PDF Xpress using a virtual image.";
            // 
            // _mainMenuStrip
            // 
            this._mainMenuStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._mainMenuStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this._mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._fileToolStripMenuItem,
            this._commentsToolStripMenuItem,
            this._viewToolStripMenuItem,
            this._toolsToolStripMenuItem,
            this._helpToolStripMenuItem});
            this._mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this._mainMenuStrip.Name = "_mainMenuStrip";
            this._mainMenuStrip.Size = new System.Drawing.Size(627, 24);
            this._mainMenuStrip.TabIndex = 5;
            this._mainMenuStrip.Text = "menuStrip1";
            // 
            // _fileToolStripMenuItem
            // 
            this._fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._newToolStripMenuItem,
            this._toolStripSeparator1,
            this._openToolStripMenuItem,
            this._toolStripSeparator2,
            this._saveToolStripMenuItem,
            this._saveAsToolStripMenuItem,
            this._toolStripSeparator3,
            this._closeToolStripMenuItem,
            this._toolStripSeparator4,
            this._exitToolStripMenuItem});
            this._fileToolStripMenuItem.Name = "_fileToolStripMenuItem";
            this._fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this._fileToolStripMenuItem.Text = "&File";
            // 
            // _newToolStripMenuItem
            // 
            this._newToolStripMenuItem.Name = "_newToolStripMenuItem";
            this._newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._newToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this._newToolStripMenuItem.Text = "&New";
            this._newToolStripMenuItem.Click += new System.EventHandler(this._newToolStripMenuItem_Click);
            // 
            // _toolStripSeparator1
            // 
            this._toolStripSeparator1.Name = "_toolStripSeparator1";
            this._toolStripSeparator1.Size = new System.Drawing.Size(192, 6);
            // 
            // _openToolStripMenuItem
            // 
            this._openToolStripMenuItem.Name = "_openToolStripMenuItem";
            this._openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._openToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this._openToolStripMenuItem.Text = "&Open...";
            this._openToolStripMenuItem.Click += new System.EventHandler(this._openToolStripMenuItem_Click);
            // 
            // _toolStripSeparator2
            // 
            this._toolStripSeparator2.Name = "_toolStripSeparator2";
            this._toolStripSeparator2.Size = new System.Drawing.Size(192, 6);
            // 
            // _saveToolStripMenuItem
            // 
            this._saveToolStripMenuItem.Name = "_saveToolStripMenuItem";
            this._saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._saveToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this._saveToolStripMenuItem.Text = "&Save";
            this._saveToolStripMenuItem.Click += new System.EventHandler(this._saveToolStripMenuItem_Click);
            // 
            // _saveAsToolStripMenuItem
            // 
            this._saveAsToolStripMenuItem.Name = "_saveAsToolStripMenuItem";
            this._saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this._saveAsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this._saveAsToolStripMenuItem.Text = "Save &As...";
            this._saveAsToolStripMenuItem.Click += new System.EventHandler(this._saveAsToolStripMenuItem_Click);
            // 
            // _toolStripSeparator3
            // 
            this._toolStripSeparator3.Name = "_toolStripSeparator3";
            this._toolStripSeparator3.Size = new System.Drawing.Size(192, 6);
            // 
            // _closeToolStripMenuItem
            // 
            this._closeToolStripMenuItem.Name = "_closeToolStripMenuItem";
            this._closeToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this._closeToolStripMenuItem.Text = "&Close";
            this._closeToolStripMenuItem.Click += new System.EventHandler(this._closeToolStripMenuItem_Click);
            // 
            // _toolStripSeparator4
            // 
            this._toolStripSeparator4.Name = "_toolStripSeparator4";
            this._toolStripSeparator4.Size = new System.Drawing.Size(192, 6);
            // 
            // _exitToolStripMenuItem
            // 
            this._exitToolStripMenuItem.Name = "_exitToolStripMenuItem";
            this._exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this._exitToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this._exitToolStripMenuItem.Text = "E&xit";
            this._exitToolStripMenuItem.Click += new System.EventHandler(this._exitToolStripMenuItem_Click);
            // 
            // _commentsToolStripMenuItem
            // 
            this._commentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._importCommentsToolStripMenuItem,
            this._exportToDataFileToolStripMenuItem});
            this._commentsToolStripMenuItem.Name = "_commentsToolStripMenuItem";
            this._commentsToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this._commentsToolStripMenuItem.Text = "&Comments";
            // 
            // _importCommentsToolStripMenuItem
            // 
            this._importCommentsToolStripMenuItem.Name = "_importCommentsToolStripMenuItem";
            this._importCommentsToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this._importCommentsToolStripMenuItem.Text = "Import comments...";
            this._importCommentsToolStripMenuItem.Click += new System.EventHandler(this._importCommentsToolStripMenuItem_Click);
            // 
            // _exportToDataFileToolStripMenuItem
            // 
            this._exportToDataFileToolStripMenuItem.Name = "_exportToDataFileToolStripMenuItem";
            this._exportToDataFileToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this._exportToDataFileToolStripMenuItem.Text = "Export to &Data File...";
            this._exportToDataFileToolStripMenuItem.Click += new System.EventHandler(this._exportToDataFileToolStripMenuItem_Click);
            // 
            // _viewToolStripMenuItem
            // 
            this._viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._gotoToolStripMenuItem,
            this.toolStripSeparator4,
            this._toolbarsToolStripMenuItem});
            this._viewToolStripMenuItem.Name = "_viewToolStripMenuItem";
            this._viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this._viewToolStripMenuItem.Text = "&View";
            // 
            // _gotoToolStripMenuItem
            // 
            this._gotoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._firstPageToolStripMenuItem,
            this._previousPageToolStripMenuItem,
            this._nextPageToolStripMenuItem,
            this._lastPageToolStripMenuItem,
            this.pageToolStripMenuItem,
            this._toolStripSeparator5,
            this._zoomToolStripMenuItem});
            this._gotoToolStripMenuItem.Name = "_gotoToolStripMenuItem";
            this._gotoToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this._gotoToolStripMenuItem.Text = "&Goto";
            // 
            // _firstPageToolStripMenuItem
            // 
            this._firstPageToolStripMenuItem.Name = "_firstPageToolStripMenuItem";
            this._firstPageToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this._firstPageToolStripMenuItem.Text = "&First Page";
            this._firstPageToolStripMenuItem.Click += new System.EventHandler(this._firstPageToolStripMenuItem_Click);
            // 
            // _previousPageToolStripMenuItem
            // 
            this._previousPageToolStripMenuItem.Name = "_previousPageToolStripMenuItem";
            this._previousPageToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this._previousPageToolStripMenuItem.Text = "P&revious Page";
            this._previousPageToolStripMenuItem.Click += new System.EventHandler(this._previousPageToolStripMenuItem_Click);
            // 
            // _nextPageToolStripMenuItem
            // 
            this._nextPageToolStripMenuItem.Name = "_nextPageToolStripMenuItem";
            this._nextPageToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this._nextPageToolStripMenuItem.Text = "&Next Page";
            this._nextPageToolStripMenuItem.Click += new System.EventHandler(this._nextPageToolStripMenuItem_Click);
            // 
            // _lastPageToolStripMenuItem
            // 
            this._lastPageToolStripMenuItem.Name = "_lastPageToolStripMenuItem";
            this._lastPageToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this._lastPageToolStripMenuItem.Text = "&Last Page";
            this._lastPageToolStripMenuItem.Click += new System.EventHandler(this._lastPageToolStripMenuItem_Click);
            // 
            // pageToolStripMenuItem
            // 
            this.pageToolStripMenuItem.Name = "pageToolStripMenuItem";
            this.pageToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.pageToolStripMenuItem.Text = "&Page...";
            this.pageToolStripMenuItem.Click += new System.EventHandler(this._pageToolStripMenuItem_Click);
            // 
            // _toolStripSeparator5
            // 
            this._toolStripSeparator5.Name = "_toolStripSeparator5";
            this._toolStripSeparator5.Size = new System.Drawing.Size(145, 6);
            // 
            // _zoomToolStripMenuItem
            // 
            this._zoomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._zoomToToolStripMenuItem,
            this._actualSizeToolStripMenuItem,
            this._fitWidthToolStripMenuItem,
            this._fitHeightToolStripMenuItem,
            this.toolStripSeparator8,
            this.showZoomToolbarToolStripMenuItem});
            this._zoomToolStripMenuItem.Name = "_zoomToolStripMenuItem";
            this._zoomToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this._zoomToolStripMenuItem.Text = "&Zoom";
            // 
            // _zoomToToolStripMenuItem
            // 
            this._zoomToToolStripMenuItem.Name = "_zoomToToolStripMenuItem";
            this._zoomToToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this._zoomToToolStripMenuItem.Text = "&Zoom To...";
            this._zoomToToolStripMenuItem.Click += new System.EventHandler(this._zoomToToolStripMenuItem_Click);
            // 
            // _actualSizeToolStripMenuItem
            // 
            this._actualSizeToolStripMenuItem.Name = "_actualSizeToolStripMenuItem";
            this._actualSizeToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this._actualSizeToolStripMenuItem.Text = "Act&ual Size";
            this._actualSizeToolStripMenuItem.Click += new System.EventHandler(this._actualSizeToolStripMenuItem_Click);
            // 
            // _fitWidthToolStripMenuItem
            // 
            this._fitWidthToolStripMenuItem.CheckOnClick = true;
            this._fitWidthToolStripMenuItem.Name = "_fitWidthToolStripMenuItem";
            this._fitWidthToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this._fitWidthToolStripMenuItem.Text = "Fit &Width";
            this._fitWidthToolStripMenuItem.Click += new System.EventHandler(this._fitWidthToolStripMenuItem_Click);
            // 
            // _fitHeightToolStripMenuItem
            // 
            this._fitHeightToolStripMenuItem.Name = "_fitHeightToolStripMenuItem";
            this._fitHeightToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this._fitHeightToolStripMenuItem.Text = "Fit &Height";
            this._fitHeightToolStripMenuItem.Click += new System.EventHandler(this._fitHeightToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(179, 6);
            // 
            // showZoomToolbarToolStripMenuItem
            // 
            this.showZoomToolbarToolStripMenuItem.Checked = true;
            this.showZoomToolbarToolStripMenuItem.CheckOnClick = true;
            this.showZoomToolbarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showZoomToolbarToolStripMenuItem.Name = "showZoomToolbarToolStripMenuItem";
            this.showZoomToolbarToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.showZoomToolbarToolStripMenuItem.Text = "Show Zoom Tool&bar";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(117, 6);
            // 
            // _toolbarsToolStripMenuItem
            // 
            this._toolbarsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._commentMarkupToolStripMenuItem,
            this._navigationToolStripMenuItem,
            this._zoomToolStripMenuItem2,
            this.toolStripSeparator7,
            this._showAllToolbarsToolStripMenuItem,
            this._hideAllToolbarsToolStripMenuItem});
            this._toolbarsToolStripMenuItem.Name = "_toolbarsToolStripMenuItem";
            this._toolbarsToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this._toolbarsToolStripMenuItem.Text = "&Toolbars";
            // 
            // _commentMarkupToolStripMenuItem
            // 
            this._commentMarkupToolStripMenuItem.Checked = true;
            this._commentMarkupToolStripMenuItem.CheckOnClick = true;
            this._commentMarkupToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._commentMarkupToolStripMenuItem.Name = "_commentMarkupToolStripMenuItem";
            this._commentMarkupToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this._commentMarkupToolStripMenuItem.Text = "&Comment && Markup";
            this._commentMarkupToolStripMenuItem.Click += new System.EventHandler(this._commentMarkupToolStripMenuItem_Click);
            // 
            // _navigationToolStripMenuItem
            // 
            this._navigationToolStripMenuItem.Checked = true;
            this._navigationToolStripMenuItem.CheckOnClick = true;
            this._navigationToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this._navigationToolStripMenuItem.Name = "_navigationToolStripMenuItem";
            this._navigationToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this._navigationToolStripMenuItem.Text = "&Navigation";
            // 
            // _zoomToolStripMenuItem2
            // 
            this._zoomToolStripMenuItem2.Checked = true;
            this._zoomToolStripMenuItem2.CheckOnClick = true;
            this._zoomToolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this._zoomToolStripMenuItem2.Name = "_zoomToolStripMenuItem2";
            this._zoomToolStripMenuItem2.Size = new System.Drawing.Size(185, 22);
            this._zoomToolStripMenuItem2.Text = "&Zoom";
            this._zoomToolStripMenuItem2.Click += new System.EventHandler(this._zoomToolStripMenuItem2_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(182, 6);
            // 
            // _showAllToolbarsToolStripMenuItem
            // 
            this._showAllToolbarsToolStripMenuItem.Name = "_showAllToolbarsToolStripMenuItem";
            this._showAllToolbarsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this._showAllToolbarsToolStripMenuItem.Text = "Show All Toolbars";
            this._showAllToolbarsToolStripMenuItem.Click += new System.EventHandler(this._showAllToolbarsToolStripMenuItem_Click);
            // 
            // _hideAllToolbarsToolStripMenuItem
            // 
            this._hideAllToolbarsToolStripMenuItem.Name = "_hideAllToolbarsToolStripMenuItem";
            this._hideAllToolbarsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this._hideAllToolbarsToolStripMenuItem.Text = "Hide All Toolbars";
            this._hideAllToolbarsToolStripMenuItem.Click += new System.EventHandler(this._hideAllToolbarsToolStripMenuItem_Click);
            // 
            // _toolsToolStripMenuItem
            // 
            this._toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._commentsMarkupToolStripMenuItem});
            this._toolsToolStripMenuItem.Name = "_toolsToolStripMenuItem";
            this._toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this._toolsToolStripMenuItem.Text = "&Tools";
            // 
            // _commentsMarkupToolStripMenuItem
            // 
            this._commentsMarkupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._stickyNoteToolStripMenuItem,
            this.highlightTextToolToolStripMenuItem,
            this.underlineTextToolToolStripMenuItem,
            this.crossOutTextToolToolStripMenuItem,
            this.textBoxTextToolToolStripMenuItem,
            this.arrowToolToolStripMenuItem,
            this.lineToolToolStripMenuItem,
            this.rectangleToolToolStripMenuItem,
            this.ovalToolToolStripMenuItem,
            this.polygonLineToolToolStripMenuItem,
            this.polygonToolToolStripMenuItem,
            this.pencilToolToolStripMenuItem,
            this.toolStripSeparator3,
            this._showCommentMarkupToolbarToolStripMenuItem2});
            this._commentsMarkupToolStripMenuItem.Name = "_commentsMarkupToolStripMenuItem";
            this._commentsMarkupToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this._commentsMarkupToolStripMenuItem.Text = "&Comments && Markup";
            // 
            // _stickyNoteToolStripMenuItem
            // 
            this._stickyNoteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("_stickyNoteToolStripMenuItem.Image")));
            this._stickyNoteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this._stickyNoteToolStripMenuItem.Name = "_stickyNoteToolStripMenuItem";
            this._stickyNoteToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this._stickyNoteToolStripMenuItem.Text = "Sticky &Note";
            this._stickyNoteToolStripMenuItem.Click += new System.EventHandler(this._stickyNoteToolStripMenuItem_Click);
            // 
            // highlightTextToolToolStripMenuItem
            // 
            this.highlightTextToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("highlightTextToolToolStripMenuItem.Image")));
            this.highlightTextToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.highlightTextToolToolStripMenuItem.Name = "highlightTextToolToolStripMenuItem";
            this.highlightTextToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.highlightTextToolToolStripMenuItem.Text = "&Highlight Text Tool";
            this.highlightTextToolToolStripMenuItem.Click += new System.EventHandler(this.highlightTextToolToolStripMenuItem_Click);
            // 
            // underlineTextToolToolStripMenuItem
            // 
            this.underlineTextToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("underlineTextToolToolStripMenuItem.Image")));
            this.underlineTextToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.underlineTextToolToolStripMenuItem.Name = "underlineTextToolToolStripMenuItem";
            this.underlineTextToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.underlineTextToolToolStripMenuItem.Text = "Underline Text Tool";
            this.underlineTextToolToolStripMenuItem.Click += new System.EventHandler(this._underlineTextToolToolStripMenuItem_Click);
            // 
            // crossOutTextToolToolStripMenuItem
            // 
            this.crossOutTextToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("crossOutTextToolToolStripMenuItem.Image")));
            this.crossOutTextToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.crossOutTextToolToolStripMenuItem.Name = "crossOutTextToolToolStripMenuItem";
            this.crossOutTextToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.crossOutTextToolToolStripMenuItem.Text = "Cross Out Text Tool";
            this.crossOutTextToolToolStripMenuItem.Click += new System.EventHandler(this._crossOutTextToolToolStripMenuItem_Click);
            // 
            // textBoxTextToolToolStripMenuItem
            // 
            this.textBoxTextToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("textBoxTextToolToolStripMenuItem.Image")));
            this.textBoxTextToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.textBoxTextToolToolStripMenuItem.Name = "textBoxTextToolToolStripMenuItem";
            this.textBoxTextToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.textBoxTextToolToolStripMenuItem.Text = "Te&xt Box Text Tool";
            this.textBoxTextToolToolStripMenuItem.Click += new System.EventHandler(this._textBoxTextToolToolStripMenuItem_Click);
            // 
            // arrowToolToolStripMenuItem
            // 
            this.arrowToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("arrowToolToolStripMenuItem.Image")));
            this.arrowToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.arrowToolToolStripMenuItem.Name = "arrowToolToolStripMenuItem";
            this.arrowToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.arrowToolToolStripMenuItem.Text = "&Arrow Tool";
            this.arrowToolToolStripMenuItem.Click += new System.EventHandler(this._arrowToolToolStripMenuItem_Click);
            // 
            // lineToolToolStripMenuItem
            // 
            this.lineToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("lineToolToolStripMenuItem.Image")));
            this.lineToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.lineToolToolStripMenuItem.Name = "lineToolToolStripMenuItem";
            this.lineToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.lineToolToolStripMenuItem.Text = "&Line Tool";
            this.lineToolToolStripMenuItem.Click += new System.EventHandler(this._lineToolToolStripMenuItem_Click);
            // 
            // rectangleToolToolStripMenuItem
            // 
            this.rectangleToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rectangleToolToolStripMenuItem.Image")));
            this.rectangleToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.rectangleToolToolStripMenuItem.Name = "rectangleToolToolStripMenuItem";
            this.rectangleToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.rectangleToolToolStripMenuItem.Text = "Rectan&gle Tool";
            this.rectangleToolToolStripMenuItem.Click += new System.EventHandler(this._rectangleToolToolStripMenuItem_Click);
            // 
            // ovalToolToolStripMenuItem
            // 
            this.ovalToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ovalToolToolStripMenuItem.Image")));
            this.ovalToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.ovalToolToolStripMenuItem.Name = "ovalToolToolStripMenuItem";
            this.ovalToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.ovalToolToolStripMenuItem.Text = "O&val Tool";
            this.ovalToolToolStripMenuItem.Click += new System.EventHandler(this._ovalToolToolStripMenuItem_Click);
            // 
            // polygonLineToolToolStripMenuItem
            // 
            this.polygonLineToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("polygonLineToolToolStripMenuItem.Image")));
            this.polygonLineToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.polygonLineToolToolStripMenuItem.Name = "polygonLineToolToolStripMenuItem";
            this.polygonLineToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.polygonLineToolToolStripMenuItem.Text = "Polygon Line Tool";
            this.polygonLineToolToolStripMenuItem.Click += new System.EventHandler(this._polygonLineToolToolStripMenuItem_Click);
            // 
            // polygonToolToolStripMenuItem
            // 
            this.polygonToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("polygonToolToolStripMenuItem.Image")));
            this.polygonToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.polygonToolToolStripMenuItem.Name = "polygonToolToolStripMenuItem";
            this.polygonToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.polygonToolToolStripMenuItem.Text = "Pol&ygon Tool";
            this.polygonToolToolStripMenuItem.Click += new System.EventHandler(this._polygonToolToolStripMenuItem_Click);
            // 
            // pencilToolToolStripMenuItem
            // 
            this.pencilToolToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pencilToolToolStripMenuItem.Image")));
            this.pencilToolToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.pencilToolToolStripMenuItem.Name = "pencilToolToolStripMenuItem";
            this.pencilToolToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.pencilToolToolStripMenuItem.Text = "Penci&l Tool";
            this.pencilToolToolStripMenuItem.Click += new System.EventHandler(this._pencilToolToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(258, 6);
            // 
            // _showCommentMarkupToolbarToolStripMenuItem2
            // 
            this._showCommentMarkupToolbarToolStripMenuItem2.Checked = true;
            this._showCommentMarkupToolbarToolStripMenuItem2.CheckOnClick = true;
            this._showCommentMarkupToolbarToolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this._showCommentMarkupToolbarToolStripMenuItem2.Name = "_showCommentMarkupToolbarToolStripMenuItem2";
            this._showCommentMarkupToolbarToolStripMenuItem2.Size = new System.Drawing.Size(261, 22);
            this._showCommentMarkupToolbarToolStripMenuItem2.Text = "Show Comment && Markup Tool&bar";
            this._showCommentMarkupToolbarToolStripMenuItem2.Click += new System.EventHandler(this._showCommentMarkupToolbarToolStripMenuItem2_Click);
            // 
            // _helpToolStripMenuItem
            // 
            this._helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._aboutImagXpressToolStripMenuItem,
            this._aboutNotateXpressToolStripMenuItem,
            this._aboutPDFXpressToolStripMenuItem});
            this._helpToolStripMenuItem.Name = "_helpToolStripMenuItem";
            this._helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this._helpToolStripMenuItem.Text = "&Help";
            // 
            // _aboutImagXpressToolStripMenuItem
            // 
            this._aboutImagXpressToolStripMenuItem.Name = "_aboutImagXpressToolStripMenuItem";
            this._aboutImagXpressToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this._aboutImagXpressToolStripMenuItem.Text = "About &ImagXpress...";
            this._aboutImagXpressToolStripMenuItem.Click += new System.EventHandler(this._aboutImagXpressToolStripMenuItem_Click);
            // 
            // _aboutNotateXpressToolStripMenuItem
            // 
            this._aboutNotateXpressToolStripMenuItem.Name = "_aboutNotateXpressToolStripMenuItem";
            this._aboutNotateXpressToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this._aboutNotateXpressToolStripMenuItem.Text = "About &NotateXpress...";
            this._aboutNotateXpressToolStripMenuItem.Click += new System.EventHandler(this._aboutNotateXpressToolStripMenuItem_Click);
            // 
            // _aboutPDFXpressToolStripMenuItem
            // 
            this._aboutPDFXpressToolStripMenuItem.Name = "_aboutPDFXpressToolStripMenuItem";
            this._aboutPDFXpressToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this._aboutPDFXpressToolStripMenuItem.Text = "About &PDF Xpress...";
            this._aboutPDFXpressToolStripMenuItem.Click += new System.EventHandler(this._aboutPDFXpressToolStripMenuItem_Click);
            // 
            // _zoomToolStrip
            // 
            this._zoomToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._zoomToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._currentZoomPercentageToolStripTextBox,
            this._zoomToToolStripButton});
            this._zoomToolStrip.Location = new System.Drawing.Point(3, 24);
            this._zoomToolStrip.Name = "_zoomToolStrip";
            this._zoomToolStrip.Size = new System.Drawing.Size(92, 25);
            this._zoomToolStrip.TabIndex = 8;
            this._zoomToolStrip.Text = "Zoom Toolbar";
            // 
            // _currentZoomPercentageToolStripTextBox
            // 
            this._currentZoomPercentageToolStripTextBox.Name = "_currentZoomPercentageToolStripTextBox";
            this._currentZoomPercentageToolStripTextBox.Size = new System.Drawing.Size(55, 25);
            this._currentZoomPercentageToolStripTextBox.Text = "100 %";
            this._currentZoomPercentageToolStripTextBox.TextChanged += new System.EventHandler(this._currentZoomPercentageToolStripTextBox_TextChanged);
            // 
            // _zoomToToolStripButton
            // 
            this._zoomToToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._zoomToToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_zoomToToolStripButton.Image")));
            this._zoomToToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._zoomToToolStripButton.Name = "_zoomToToolStripButton";
            this._zoomToToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._zoomToToolStripButton.Text = "toolStripButton1";
            this._zoomToToolStripButton.Click += new System.EventHandler(this._zoomToToolStripButton_Click);
            // 
            // _navigationToolStrip
            // 
            this._navigationToolStrip.AccessibleDescription = "Go To Toolbar";
            this._navigationToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._navigationToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._previousPageToolStripButton,
            this._nextPageToolStripButton,
            this._currentPageToolStripTextBox,
            this._pageCountToolStripLabel});
            this._navigationToolStrip.Location = new System.Drawing.Point(95, 24);
            this._navigationToolStrip.Name = "_navigationToolStrip";
            this._navigationToolStrip.Size = new System.Drawing.Size(137, 25);
            this._navigationToolStrip.TabIndex = 4;
            this._navigationToolStrip.Text = "Navigation Toolbar";
            this._navigationToolStrip.VisibleChanged += new System.EventHandler(this._navigationToolStrip_VisibleChanged);
            // 
            // _previousPageToolStripButton
            // 
            this._previousPageToolStripButton.AccessibleDescription = "Previous Page";
            this._previousPageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._previousPageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_previousPageToolStripButton.Image")));
            this._previousPageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._previousPageToolStripButton.Name = "_previousPageToolStripButton";
            this._previousPageToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._previousPageToolStripButton.Text = "Previous Page";
            this._previousPageToolStripButton.Click += new System.EventHandler(this._previousPageToolStripButton_Click);
            // 
            // _nextPageToolStripButton
            // 
            this._nextPageToolStripButton.AccessibleDescription = "Next Page";
            this._nextPageToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._nextPageToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_nextPageToolStripButton.Image")));
            this._nextPageToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._nextPageToolStripButton.Name = "_nextPageToolStripButton";
            this._nextPageToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._nextPageToolStripButton.Text = "Next Page";
            this._nextPageToolStripButton.ToolTipText = "Next Page";
            this._nextPageToolStripButton.Click += new System.EventHandler(this._nextPageToolStripButton_Click);
            // 
            // _currentPageToolStripTextBox
            // 
            this._currentPageToolStripTextBox.Name = "_currentPageToolStripTextBox";
            this._currentPageToolStripTextBox.Size = new System.Drawing.Size(50, 25);
            this._currentPageToolStripTextBox.Text = "1";
            this._currentPageToolStripTextBox.TextChanged += new System.EventHandler(this._currentPageToolStripTextBox_TextChanged);
            // 
            // _pageCountToolStripLabel
            // 
            this._pageCountToolStripLabel.Name = "_pageCountToolStripLabel";
            this._pageCountToolStripLabel.Size = new System.Drawing.Size(27, 22);
            this._pageCountToolStripLabel.Text = "/ 16";
            // 
            // _commentMarkupToolStrip
            // 
            this._commentMarkupToolStrip.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._commentMarkupToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this._commentMarkupToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this._stickyNoteToolStripButton,
            this._highlightTextToolStripButton,
            this._underlineTextToolStripButton,
            this._crossOutTextTool,
            this._textBoxToolStripButton,
            this._arrowToolStripButton,
            this._lineToolStripButton,
            this._rectangleToolStripButton,
            this._ovalToolStripButton,
            this._polygonLineToolStripButton,
            this._polygonToolStripButton,
            this._pencilToolStripButton});
            this._commentMarkupToolStrip.Location = new System.Drawing.Point(3, 49);
            this._commentMarkupToolStrip.Name = "_commentMarkupToolStrip";
            this._commentMarkupToolStrip.Size = new System.Drawing.Size(311, 25);
            this._commentMarkupToolStrip.TabIndex = 3;
            this._commentMarkupToolStrip.Text = "toolStrip1";
            this._commentMarkupToolStrip.VisibleChanged += new System.EventHandler(this._commentMarkupToolStrip_VisibleChanged);
            this._commentMarkupToolStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this._textEditToolStrip_ItemClicked);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Checked = true;
            this.toolStripButton2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "_pointerToolStripButton";
            this.toolStripButton2.Click += new System.EventHandler(this._pointerToolStripButton_Click);
            // 
            // _stickyNoteToolStripButton
            // 
            this._stickyNoteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._stickyNoteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_stickyNoteToolStripButton.Image")));
            this._stickyNoteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._stickyNoteToolStripButton.Name = "_stickyNoteToolStripButton";
            this._stickyNoteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._stickyNoteToolStripButton.Text = "Sticky Note Tool";
            this._stickyNoteToolStripButton.Click += new System.EventHandler(this._stickyNoteToolStripButton_Click);
            // 
            // _highlightTextToolStripButton
            // 
            this._highlightTextToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._highlightTextToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_highlightTextToolStripButton.Image")));
            this._highlightTextToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._highlightTextToolStripButton.Name = "_highlightTextToolStripButton";
            this._highlightTextToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._highlightTextToolStripButton.Text = "Highlight Text Tool";
            this._highlightTextToolStripButton.Click += new System.EventHandler(this._highlightTextToolStripButton_Click);
            // 
            // _underlineTextToolStripButton
            // 
            this._underlineTextToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._underlineTextToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_underlineTextToolStripButton.Image")));
            this._underlineTextToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._underlineTextToolStripButton.Name = "_underlineTextToolStripButton";
            this._underlineTextToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._underlineTextToolStripButton.Text = "Underline Text Tool";
            this._underlineTextToolStripButton.Click += new System.EventHandler(this._underlineTextToolStripButton_Click);
            // 
            // _crossOutTextTool
            // 
            this._crossOutTextTool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._crossOutTextTool.Image = ((System.Drawing.Image)(resources.GetObject("_crossOutTextTool.Image")));
            this._crossOutTextTool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._crossOutTextTool.Name = "_crossOutTextTool";
            this._crossOutTextTool.Size = new System.Drawing.Size(23, 22);
            this._crossOutTextTool.Text = "Cross-out Text Tool";
            this._crossOutTextTool.Click += new System.EventHandler(this._crossOutTextTool_Click);
            // 
            // _textBoxToolStripButton
            // 
            this._textBoxToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._textBoxToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_textBoxToolStripButton.Image")));
            this._textBoxToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._textBoxToolStripButton.Name = "_textBoxToolStripButton";
            this._textBoxToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._textBoxToolStripButton.Text = "Text Box Tool";
            this._textBoxToolStripButton.Click += new System.EventHandler(this._textBoxToolStripButton_Click);
            // 
            // _arrowToolStripButton
            // 
            this._arrowToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._arrowToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_arrowToolStripButton.Image")));
            this._arrowToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._arrowToolStripButton.Name = "_arrowToolStripButton";
            this._arrowToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._arrowToolStripButton.Text = "Arrow Tool";
            this._arrowToolStripButton.Click += new System.EventHandler(this._arrowToolStripButton_Click);
            // 
            // _lineToolStripButton
            // 
            this._lineToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._lineToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_lineToolStripButton.Image")));
            this._lineToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._lineToolStripButton.Name = "_lineToolStripButton";
            this._lineToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._lineToolStripButton.Text = "Line Tool";
            this._lineToolStripButton.Click += new System.EventHandler(this._lineToolStripButton_Click);
            // 
            // _rectangleToolStripButton
            // 
            this._rectangleToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._rectangleToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_rectangleToolStripButton.Image")));
            this._rectangleToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._rectangleToolStripButton.Name = "_rectangleToolStripButton";
            this._rectangleToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._rectangleToolStripButton.Text = "Rectangle Tool";
            this._rectangleToolStripButton.Click += new System.EventHandler(this._rectangleToolStripButton_Click);
            // 
            // _ovalToolStripButton
            // 
            this._ovalToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ovalToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_ovalToolStripButton.Image")));
            this._ovalToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ovalToolStripButton.Name = "_ovalToolStripButton";
            this._ovalToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._ovalToolStripButton.Text = "Ellipse Tool";
            this._ovalToolStripButton.Click += new System.EventHandler(this._ovalToolStripButton_Click);
            // 
            // _polygonLineToolStripButton
            // 
            this._polygonLineToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._polygonLineToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_polygonLineToolStripButton.Image")));
            this._polygonLineToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._polygonLineToolStripButton.Name = "_polygonLineToolStripButton";
            this._polygonLineToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._polygonLineToolStripButton.Text = "Polyline Line";
            this._polygonLineToolStripButton.Click += new System.EventHandler(this._polygonLineToolStripButton_Click);
            // 
            // _polygonToolStripButton
            // 
            this._polygonToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._polygonToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_polygonToolStripButton.Image")));
            this._polygonToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._polygonToolStripButton.Name = "_polygonToolStripButton";
            this._polygonToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._polygonToolStripButton.Text = "Polygon Tool";
            this._polygonToolStripButton.Click += new System.EventHandler(this._polygonToolStripButton_Click);
            // 
            // _pencilToolStripButton
            // 
            this._pencilToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._pencilToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("_pencilToolStripButton.Image")));
            this._pencilToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._pencilToolStripButton.Name = "_pencilToolStripButton";
            this._pencilToolStripButton.Size = new System.Drawing.Size(23, 22);
            this._pencilToolStripButton.Text = "Pencil Tool";
            this._pencilToolStripButton.Click += new System.EventHandler(this._pencilToolStripButton_Click);
            // 
            // _pdfxpress
            // 
            this._pdfxpress.Debug = false;
            this._pdfxpress.DebugLogFile = "";
            this._pdfxpress.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production;
            // 
            // ManipulatePdfAnnotationsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 586);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(338, 370);
            this.Name = "ManipulatePdfAnnotationsForm";
            this.Text = "Manipulate PDF Annotations";
            this._mainStatusStrip.ResumeLayout(false);
            this._mainStatusStrip.PerformLayout();
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this._mainMenuStrip.ResumeLayout(false);
            this._mainMenuStrip.PerformLayout();
            this._zoomToolStrip.ResumeLayout(false);
            this._zoomToolStrip.PerformLayout();
            this._navigationToolStrip.ResumeLayout(false);
            this._navigationToolStrip.PerformLayout();
            this._commentMarkupToolStrip.ResumeLayout(false);
            this._commentMarkupToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.StatusStrip _mainStatusStrip;
        private System.Windows.Forms.TextBox _messageTextBox;
        private Accusoft.ImagXpressSdk.ImagXpress _imagexpress;
        private Accusoft.NotateXpressSdk.NotateXpress _notatexpress;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip _mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem _fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem _saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem _closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem _exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _aboutImagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _aboutNotateXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _aboutPDFXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _viewToolStripMenuItem;
        private System.Windows.Forms.ToolStrip _navigationToolStrip;
        private System.Windows.Forms.ToolStripButton _previousPageToolStripButton;
        private System.Windows.Forms.ToolStripButton _nextPageToolStripButton;
        private System.Windows.Forms.ToolStripTextBox _currentPageToolStripTextBox;
        private System.Windows.Forms.ToolStripLabel _pageCountToolStripLabel;
        private System.Windows.Forms.ToolStrip _commentMarkupToolStrip;
        private System.Windows.Forms.ToolStripButton _stickyNoteToolStripButton;
        private System.Windows.Forms.ToolStripButton _highlightTextToolStripButton;
        private System.Windows.Forms.ToolStripButton _underlineTextToolStripButton;
        private System.Windows.Forms.ToolStripButton _crossOutTextTool;
        private System.Windows.Forms.ToolStripButton _textBoxToolStripButton;
        private System.Windows.Forms.ToolStripButton _arrowToolStripButton;
        private System.Windows.Forms.ToolStripButton _lineToolStripButton;
        private System.Windows.Forms.ToolStripButton _rectangleToolStripButton;
        private System.Windows.Forms.ToolStripButton _ovalToolStripButton;
        private System.Windows.Forms.ToolStripButton _polygonLineToolStripButton;
        private System.Windows.Forms.ToolStripButton _polygonToolStripButton;
        private System.Windows.Forms.ToolStripButton _pencilToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem _toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _commentsMarkupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _stickyNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem highlightTextToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineTextToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crossOutTextToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textBoxTextToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrowToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rectangleToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ovalToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem polygonLineToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem polygonToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pencilToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _toolbarsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _commentMarkupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem _showCommentMarkupToolbarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem _navigationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _zoomToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem _showAllToolbarsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _hideAllToolbarsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _gotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _firstPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _previousPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _nextPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _lastPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem _zoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _zoomToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _actualSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _fitWidthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _fitHeightToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem showZoomToolbarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStrip _zoomToolStrip;
        private System.Windows.Forms.ToolStripTextBox _currentZoomPercentageToolStripTextBox;
        private Accusoft.PdfXpressSdk.PdfXpress _pdfxpress;
        private Accusoft.ImagXpressSdk.ImageXView _imagxview;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label _descriptionLabel;
        private System.Windows.Forms.ToolStripStatusLabel _pageSizeToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem _commentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _exportToDataFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _importCommentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton _zoomToToolStripButton;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}


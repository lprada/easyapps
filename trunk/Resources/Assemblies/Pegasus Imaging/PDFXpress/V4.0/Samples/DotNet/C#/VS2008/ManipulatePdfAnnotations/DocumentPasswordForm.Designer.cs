/// Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    partial class DocumentPasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._descriptionLabel = new System.Windows.Forms.Label( );
            this._passwordLabel = new System.Windows.Forms.Label( );
            this._iconPictureBox = new System.Windows.Forms.PictureBox( );
            this._passwordTestBox = new System.Windows.Forms.TextBox( );
            this._okButton = new System.Windows.Forms.Button( );
            this._cancelButton = new System.Windows.Forms.Button( );
            ( ( System.ComponentModel.ISupportInitialize )( this._iconPictureBox ) ).BeginInit( );
            this.SuspendLayout( );
            // 
            // _descriptionLabel
            // 
            this._descriptionLabel.Location = new System.Drawing.Point( 50 , 12 );
            this._descriptionLabel.Name = "_descriptionLabel";
            this._descriptionLabel.Size = new System.Drawing.Size( 346 , 32 );
            this._descriptionLabel.TabIndex = 0;
            this._descriptionLabel.Text = "\'{0}\' is protected. Please enter a Document Open Pasword.";
            // 
            // _passwordLabel
            // 
            this._passwordLabel.Location = new System.Drawing.Point( 99 , 47 );
            this._passwordLabel.Name = "_passwordLabel";
            this._passwordLabel.Size = new System.Drawing.Size( 211 , 17 );
            this._passwordLabel.TabIndex = 1;
            this._passwordLabel.Text = "&Enter Password:";
            // 
            // _iconPictureBox
            // 
            this._iconPictureBox.Location = new System.Drawing.Point( 12 , 12 );
            this._iconPictureBox.Name = "_iconPictureBox";
            this._iconPictureBox.Size = new System.Drawing.Size( 32 , 32 );
            this._iconPictureBox.TabIndex = 2;
            this._iconPictureBox.TabStop = false;
            // 
            // _passwordTestBox
            // 
            this._passwordTestBox.Location = new System.Drawing.Point( 102 , 67 );
            this._passwordTestBox.Name = "_passwordTestBox";
            this._passwordTestBox.PasswordChar = '*';
            this._passwordTestBox.Size = new System.Drawing.Size( 208 , 20 );
            this._passwordTestBox.TabIndex = 3;
            // 
            // _okButton
            // 
            this._okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._okButton.Location = new System.Drawing.Point( 235 , 98 );
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size( 75 , 23 );
            this._okButton.TabIndex = 4;
            this._okButton.Text = "&OK";
            this._okButton.UseVisualStyleBackColor = true;
            // 
            // _cancelButton
            // 
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point( 321 , 98 );
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size( 75 , 23 );
            this._cancelButton.TabIndex = 5;
            this._cancelButton.Text = "&Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            // 
            // DocumentPasswordForm
            // 
            this.AcceptButton = this._okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F , 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelButton;
            this.ClientSize = new System.Drawing.Size( 408 , 133 );
            this.Controls.Add( this._cancelButton );
            this.Controls.Add( this._okButton );
            this.Controls.Add( this._passwordTestBox );
            this.Controls.Add( this._iconPictureBox );
            this.Controls.Add( this._passwordLabel );
            this.Controls.Add( this._descriptionLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DocumentPasswordForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Password";
            ( ( System.ComponentModel.ISupportInitialize )( this._iconPictureBox ) ).EndInit( );
            this.ResumeLayout( false );
            this.PerformLayout( );

        }

        #endregion

        private System.Windows.Forms.Label _descriptionLabel;
        private System.Windows.Forms.Label _passwordLabel;
        private System.Windows.Forms.PictureBox _iconPictureBox;
        private System.Windows.Forms.TextBox _passwordTestBox;
        private System.Windows.Forms.Button _okButton;
        private System.Windows.Forms.Button _cancelButton;
    }
}
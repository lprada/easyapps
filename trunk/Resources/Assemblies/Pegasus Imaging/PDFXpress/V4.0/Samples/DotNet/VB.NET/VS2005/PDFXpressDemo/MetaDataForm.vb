'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Public Class MetaDataForm

    Public Property Author() As String
        Get
            Return AuthorTextBox.Text
        End Get
        Set(ByVal value As String)
            AuthorTextBox.Text = value
        End Set
    End Property

    Public Property Subject() As String
        Get
            Return SubjectTextBox.Text
        End Get
        Set(ByVal value As String)
            SubjectTextBox.Text = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return TitleTextBox.Text
        End Get
        Set(ByVal value As String)
            TitleTextBox.Text = value
        End Set
    End Property

    Public Property Keywords() As String
        Get
            Return KeywordsTextBox.Text
        End Get
        Set(ByVal value As String)
            KeywordsTextBox.Text = value
        End Set
    End Property
    Public Property Creator() As String
        Get
            Return CreatorTextBox.Text
        End Get
        Set(ByVal value As String)
            CreatorTextBox.Text = value
        End Set
    End Property

    Public Property CreationDate() As String
        Get
            Return CreationDateTextBox.Text
        End Get
        Set(ByVal value As String)
            CreationDateTextBox.Text = value
        End Set
    End Property
    Public Property Producer() As String
        Get
            Return ProducerTextBox.Text
        End Get
        Set(ByVal value As String)
            ProducerTextBox.Text = value
        End Set
    End Property
    Public Property ModificationDate() As String
        Get
            Return ModificationDateTextBox.Text
        End Get
        Set(ByVal value As String)
            ModificationDateTextBox.Text = value
        End Set
    End Property
    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.OK

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CancelMetaButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelMetaButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.Cancel

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
/// Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;

    public partial class DocumentPasswordForm : Form
    {
        public static Int32 DisplayFilenameMaximumLength = 28;
        private String _filename = "" ;
        private void RefreshDescriptionLabel( )
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo( this._filename ) ;
            String prettyFilename;
            if ( fileInfo.Name.Length > DocumentPasswordForm.DisplayFilenameMaximumLength )
            {
                prettyFilename = String.Format( "{0}..."
                                              , fileInfo.Name.Substring( 0 , DocumentPasswordForm.DisplayFilenameMaximumLength - 3 )
                                              ) ;
                                
            }
            else
            {
                prettyFilename = fileInfo.Name;
            }
            this._descriptionLabel.Text = String.Format( "'{0}' is protected. Please enter a Document Open Pasword."
                                                  , prettyFilename
                                                  ) ;
        }
        internal String Filename
        {
            get { return this._filename; }
            set { this._filename = value; this.RefreshDescriptionLabel( ); }
        }
        internal String Password
        {
            get
            {
                return this._passwordTestBox.Text;
            }
        }
        public DocumentPasswordForm( )
        {
            InitializeComponent( );
            this._iconPictureBox.Image = System.Drawing.SystemIcons.Warning.ToBitmap( );
        }
    }
}
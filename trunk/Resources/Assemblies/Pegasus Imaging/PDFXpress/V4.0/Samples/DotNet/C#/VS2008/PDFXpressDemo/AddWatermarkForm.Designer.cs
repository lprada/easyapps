/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class AddWatermarkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (pdfXpress1 != null)
            {
                pdfXpress1.Dispose();
                pdfXpress1 = null;
            }
            if (imageXView1.Image != null)
            {
                imageXView1.Image.Dispose();
                imageXView1.Image = null;
            }
            if (imagXpress1 != null)
            {
                imagXpress1.Dispose();
                imagXpress1 = null;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddWatermarkForm));
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.RotationBox = new System.Windows.Forms.NumericUpDown();
            this.RotationLabel = new System.Windows.Forms.Label();
            this.DegreesLabel = new System.Windows.Forms.Label();
            this.OpacityBox = new System.Windows.Forms.NumericUpDown();
            this.OpacityLabel = new System.Windows.Forms.Label();
            this.OpacityPercentLabel = new System.Windows.Forms.Label();
            this.HorizComboBox = new System.Windows.Forms.ComboBox();
            this.HorzAlignLabel = new System.Windows.Forms.Label();
            this.VerticalLabel = new System.Windows.Forms.Label();
            this.VertComboBox = new System.Windows.Forms.ComboBox();
            this.ScaleLabel = new System.Windows.Forms.Label();
            this.ScaleBox = new System.Windows.Forms.NumericUpDown();
            this.PercentScaleLabel = new System.Windows.Forms.Label();
            this.PrintCheckBox = new System.Windows.Forms.CheckBox();
            this.ViewCheckBox = new System.Windows.Forms.CheckBox();
            this.FixedCheckBox = new System.Windows.Forms.CheckBox();
            this.HorzOffsetLabel = new System.Windows.Forms.Label();
            this.HorzOffsetBox = new System.Windows.Forms.NumericUpDown();
            this.HorzOffsetPercentLabel = new System.Windows.Forms.Label();
            this.VertOffsetPercentLabel = new System.Windows.Forms.Label();
            this.VertOffsetLabel = new System.Windows.Forms.Label();
            this.VertOffsetBox = new System.Windows.Forms.NumericUpDown();
            this.WatermarkTextBox = new System.Windows.Forms.TextBox();
            this.TextRadioButton = new System.Windows.Forms.RadioButton();
            this.ImageRadioButton = new System.Windows.Forms.RadioButton();
            this.ImageTextBox = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.OKButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.textAlignLabel = new System.Windows.Forms.Label();
            this.TextAlignComboBox = new System.Windows.Forms.ComboBox();
            this.ColorButton = new System.Windows.Forms.Button();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.TextSizeLabel = new System.Windows.Forms.Label();
            this.TextSizeBox = new System.Windows.Forms.NumericUpDown();
            this.PointsLabel = new System.Windows.Forms.Label();
            this.SourceGroupBox = new System.Windows.Forms.GroupBox();
            this.SourceLabel = new System.Windows.Forms.Label();
            this.SourcePageBox = new System.Windows.Forms.NumericUpDown();
            this.zOrderLabel = new System.Windows.Forms.Label();
            this.zOrderComboBox = new System.Windows.Forms.ComboBox();
            this.PreviewLabel = new System.Windows.Forms.Label();
            this.PageBox = new System.Windows.Forms.NumericUpDown();
            this.pdfXpress1 = new Accusoft.PdfXpressSdk.PdfXpress();
            ((System.ComponentModel.ISupportInitialize)(this.RotationBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpacityBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScaleBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HorzOffsetBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VertOffsetBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSizeBox)).BeginInit();
            this.SourceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SourcePageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit;
            this.imageXView1.AutoScroll = true;
            this.imageXView1.Location = new System.Drawing.Point(380, 46);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(416, 484);
            this.imageXView1.TabIndex = 4;
            // 
            // RotationBox
            // 
            this.RotationBox.Location = new System.Drawing.Point(124, 407);
            this.RotationBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.RotationBox.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.RotationBox.Name = "RotationBox";
            this.RotationBox.Size = new System.Drawing.Size(50, 20);
            this.RotationBox.TabIndex = 24;
            this.RotationBox.Leave += new System.EventHandler(this.RotationBox_Leave);
            // 
            // RotationLabel
            // 
            this.RotationLabel.AutoSize = true;
            this.RotationLabel.Location = new System.Drawing.Point(65, 411);
            this.RotationLabel.Name = "RotationLabel";
            this.RotationLabel.Size = new System.Drawing.Size(50, 13);
            this.RotationLabel.TabIndex = 23;
            this.RotationLabel.Text = "Rotation:";
            // 
            // DegreesLabel
            // 
            this.DegreesLabel.AutoSize = true;
            this.DegreesLabel.Location = new System.Drawing.Point(180, 411);
            this.DegreesLabel.Name = "DegreesLabel";
            this.DegreesLabel.Size = new System.Drawing.Size(51, 13);
            this.DegreesLabel.TabIndex = 25;
            this.DegreesLabel.Text = "(degrees)";
            // 
            // OpacityBox
            // 
            this.OpacityBox.Location = new System.Drawing.Point(124, 381);
            this.OpacityBox.Name = "OpacityBox";
            this.OpacityBox.Size = new System.Drawing.Size(50, 20);
            this.OpacityBox.TabIndex = 21;
            this.OpacityBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.OpacityBox.Leave += new System.EventHandler(this.OpacityBox_Leave);
            // 
            // OpacityLabel
            // 
            this.OpacityLabel.AutoSize = true;
            this.OpacityLabel.Location = new System.Drawing.Point(69, 385);
            this.OpacityLabel.Name = "OpacityLabel";
            this.OpacityLabel.Size = new System.Drawing.Size(46, 13);
            this.OpacityLabel.TabIndex = 20;
            this.OpacityLabel.Text = "Opacity:";
            // 
            // OpacityPercentLabel
            // 
            this.OpacityPercentLabel.AutoSize = true;
            this.OpacityPercentLabel.Location = new System.Drawing.Point(180, 385);
            this.OpacityPercentLabel.Name = "OpacityPercentLabel";
            this.OpacityPercentLabel.Size = new System.Drawing.Size(49, 13);
            this.OpacityPercentLabel.TabIndex = 22;
            this.OpacityPercentLabel.Text = "(percent)";
            // 
            // HorizComboBox
            // 
            this.HorizComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HorizComboBox.FormattingEnabled = true;
            this.HorizComboBox.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.HorizComboBox.Location = new System.Drawing.Point(124, 246);
            this.HorizComboBox.Name = "HorizComboBox";
            this.HorizComboBox.Size = new System.Drawing.Size(89, 21);
            this.HorizComboBox.TabIndex = 6;
            this.HorizComboBox.Leave += new System.EventHandler(this.HorizComboBox_Leave);
            // 
            // HorzAlignLabel
            // 
            this.HorzAlignLabel.AutoSize = true;
            this.HorzAlignLabel.Location = new System.Drawing.Point(9, 250);
            this.HorzAlignLabel.Name = "HorzAlignLabel";
            this.HorzAlignLabel.Size = new System.Drawing.Size(106, 13);
            this.HorzAlignLabel.TabIndex = 5;
            this.HorzAlignLabel.Text = "Horizontal Alignment:";
            // 
            // VerticalLabel
            // 
            this.VerticalLabel.AutoSize = true;
            this.VerticalLabel.Location = new System.Drawing.Point(21, 279);
            this.VerticalLabel.Name = "VerticalLabel";
            this.VerticalLabel.Size = new System.Drawing.Size(94, 13);
            this.VerticalLabel.TabIndex = 9;
            this.VerticalLabel.Text = "Vertical Alignment:";
            // 
            // VertComboBox
            // 
            this.VertComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VertComboBox.FormattingEnabled = true;
            this.VertComboBox.Items.AddRange(new object[] {
            "Top",
            "Middle",
            "Bottom"});
            this.VertComboBox.Location = new System.Drawing.Point(124, 276);
            this.VertComboBox.Name = "VertComboBox";
            this.VertComboBox.Size = new System.Drawing.Size(89, 21);
            this.VertComboBox.TabIndex = 10;
            this.VertComboBox.Leave += new System.EventHandler(this.VertComboBox_Leave);
            // 
            // ScaleLabel
            // 
            this.ScaleLabel.AutoSize = true;
            this.ScaleLabel.Location = new System.Drawing.Point(45, 359);
            this.ScaleLabel.Name = "ScaleLabel";
            this.ScaleLabel.Size = new System.Drawing.Size(70, 13);
            this.ScaleLabel.TabIndex = 17;
            this.ScaleLabel.Text = "Scale Factor:";
            // 
            // ScaleBox
            // 
            this.ScaleBox.Location = new System.Drawing.Point(124, 355);
            this.ScaleBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.ScaleBox.Name = "ScaleBox";
            this.ScaleBox.Size = new System.Drawing.Size(50, 20);
            this.ScaleBox.TabIndex = 18;
            this.ScaleBox.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.ScaleBox.Leave += new System.EventHandler(this.ScaleBox_Leave);
            // 
            // PercentScaleLabel
            // 
            this.PercentScaleLabel.AutoSize = true;
            this.PercentScaleLabel.Location = new System.Drawing.Point(180, 359);
            this.PercentScaleLabel.Name = "PercentScaleLabel";
            this.PercentScaleLabel.Size = new System.Drawing.Size(49, 13);
            this.PercentScaleLabel.TabIndex = 19;
            this.PercentScaleLabel.Text = "(percent)";
            // 
            // PrintCheckBox
            // 
            this.PrintCheckBox.AutoSize = true;
            this.PrintCheckBox.Checked = true;
            this.PrintCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PrintCheckBox.Location = new System.Drawing.Point(17, 463);
            this.PrintCheckBox.Name = "PrintCheckBox";
            this.PrintCheckBox.Size = new System.Drawing.Size(106, 17);
            this.PrintCheckBox.TabIndex = 26;
            this.PrintCheckBox.Text = "Show on Printing";
            this.PrintCheckBox.UseVisualStyleBackColor = true;
            // 
            // ViewCheckBox
            // 
            this.ViewCheckBox.AutoSize = true;
            this.ViewCheckBox.Checked = true;
            this.ViewCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ViewCheckBox.Location = new System.Drawing.Point(17, 486);
            this.ViewCheckBox.Name = "ViewCheckBox";
            this.ViewCheckBox.Size = new System.Drawing.Size(116, 17);
            this.ViewCheckBox.TabIndex = 27;
            this.ViewCheckBox.Text = "Shown On Viewing";
            this.ViewCheckBox.UseVisualStyleBackColor = true;
            // 
            // FixedCheckBox
            // 
            this.FixedCheckBox.AutoSize = true;
            this.FixedCheckBox.Location = new System.Drawing.Point(17, 509);
            this.FixedCheckBox.Name = "FixedCheckBox";
            this.FixedCheckBox.Size = new System.Drawing.Size(264, 17);
            this.FixedCheckBox.TabIndex = 28;
            this.FixedCheckBox.Text = "Maintain size and position regardless of target PDF";
            this.FixedCheckBox.UseVisualStyleBackColor = true;
            // 
            // HorzOffsetLabel
            // 
            this.HorzOffsetLabel.AutoSize = true;
            this.HorzOffsetLabel.Location = new System.Drawing.Point(27, 308);
            this.HorzOffsetLabel.Name = "HorzOffsetLabel";
            this.HorzOffsetLabel.Size = new System.Drawing.Size(88, 13);
            this.HorzOffsetLabel.TabIndex = 11;
            this.HorzOffsetLabel.Text = "Horizontal Offset:";
            // 
            // HorzOffsetBox
            // 
            this.HorzOffsetBox.Location = new System.Drawing.Point(124, 304);
            this.HorzOffsetBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.HorzOffsetBox.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.HorzOffsetBox.Name = "HorzOffsetBox";
            this.HorzOffsetBox.Size = new System.Drawing.Size(50, 20);
            this.HorzOffsetBox.TabIndex = 12;
            this.HorzOffsetBox.Leave += new System.EventHandler(this.HorzOffsetBox_Leave);
            // 
            // HorzOffsetPercentLabel
            // 
            this.HorzOffsetPercentLabel.AutoSize = true;
            this.HorzOffsetPercentLabel.Location = new System.Drawing.Point(180, 308);
            this.HorzOffsetPercentLabel.Name = "HorzOffsetPercentLabel";
            this.HorzOffsetPercentLabel.Size = new System.Drawing.Size(49, 13);
            this.HorzOffsetPercentLabel.TabIndex = 13;
            this.HorzOffsetPercentLabel.Text = "(percent)";
            // 
            // VertOffsetPercentLabel
            // 
            this.VertOffsetPercentLabel.AutoSize = true;
            this.VertOffsetPercentLabel.Location = new System.Drawing.Point(180, 333);
            this.VertOffsetPercentLabel.Name = "VertOffsetPercentLabel";
            this.VertOffsetPercentLabel.Size = new System.Drawing.Size(49, 13);
            this.VertOffsetPercentLabel.TabIndex = 16;
            this.VertOffsetPercentLabel.Text = "(percent)";
            // 
            // VertOffsetLabel
            // 
            this.VertOffsetLabel.AutoSize = true;
            this.VertOffsetLabel.Location = new System.Drawing.Point(39, 333);
            this.VertOffsetLabel.Name = "VertOffsetLabel";
            this.VertOffsetLabel.Size = new System.Drawing.Size(76, 13);
            this.VertOffsetLabel.TabIndex = 14;
            this.VertOffsetLabel.Text = "Vertical Offset:";
            // 
            // VertOffsetBox
            // 
            this.VertOffsetBox.Location = new System.Drawing.Point(124, 329);
            this.VertOffsetBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.VertOffsetBox.Minimum = new decimal(new int[] {
            999,
            0,
            0,
            -2147483648});
            this.VertOffsetBox.Name = "VertOffsetBox";
            this.VertOffsetBox.Size = new System.Drawing.Size(50, 20);
            this.VertOffsetBox.TabIndex = 15;
            this.VertOffsetBox.Leave += new System.EventHandler(this.VertOffsetBox_Leave);
            // 
            // WatermarkTextBox
            // 
            this.WatermarkTextBox.AcceptsReturn = true;
            this.WatermarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WatermarkTextBox.Location = new System.Drawing.Point(60, 12);
            this.WatermarkTextBox.Multiline = true;
            this.WatermarkTextBox.Name = "WatermarkTextBox";
            this.WatermarkTextBox.Size = new System.Drawing.Size(271, 65);
            this.WatermarkTextBox.TabIndex = 1;
            this.WatermarkTextBox.Text = "Watermark";
            this.WatermarkTextBox.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // TextRadioButton
            // 
            this.TextRadioButton.AutoSize = true;
            this.TextRadioButton.Checked = true;
            this.TextRadioButton.Location = new System.Drawing.Point(8, 30);
            this.TextRadioButton.Name = "TextRadioButton";
            this.TextRadioButton.Size = new System.Drawing.Size(46, 17);
            this.TextRadioButton.TabIndex = 0;
            this.TextRadioButton.TabStop = true;
            this.TextRadioButton.Text = "Text";
            this.TextRadioButton.UseVisualStyleBackColor = true;
            this.TextRadioButton.CheckedChanged += new System.EventHandler(this.TextRadioButton_CheckedChanged);
            // 
            // ImageRadioButton
            // 
            this.ImageRadioButton.AutoSize = true;
            this.ImageRadioButton.Location = new System.Drawing.Point(10, 162);
            this.ImageRadioButton.Name = "ImageRadioButton";
            this.ImageRadioButton.Size = new System.Drawing.Size(54, 17);
            this.ImageRadioButton.TabIndex = 9;
            this.ImageRadioButton.Text = "Image";
            this.ImageRadioButton.UseVisualStyleBackColor = true;
            this.ImageRadioButton.CheckedChanged += new System.EventHandler(this.ImageRadioButton_CheckedChanged);
            // 
            // ImageTextBox
            // 
            this.ImageTextBox.Enabled = false;
            this.ImageTextBox.Location = new System.Drawing.Point(66, 159);
            this.ImageTextBox.Name = "ImageTextBox";
            this.ImageTextBox.Size = new System.Drawing.Size(198, 20);
            this.ImageTextBox.TabIndex = 10;
            // 
            // BrowseButton
            // 
            this.BrowseButton.Enabled = false;
            this.BrowseButton.Location = new System.Drawing.Point(273, 157);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(65, 23);
            this.BrowseButton.TabIndex = 11;
            this.BrowseButton.Text = "Browse...";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(585, 536);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 29;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CButton
            // 
            this.CButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CButton.Location = new System.Drawing.Point(724, 536);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(75, 23);
            this.CButton.TabIndex = 0;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // textAlignLabel
            // 
            this.textAlignLabel.AutoSize = true;
            this.textAlignLabel.Location = new System.Drawing.Point(58, 96);
            this.textAlignLabel.Name = "textAlignLabel";
            this.textAlignLabel.Size = new System.Drawing.Size(80, 13);
            this.textAlignLabel.TabIndex = 2;
            this.textAlignLabel.Text = "Text Alignment:";
            // 
            // TextAlignComboBox
            // 
            this.TextAlignComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TextAlignComboBox.FormattingEnabled = true;
            this.TextAlignComboBox.Items.AddRange(new object[] {
            "Left",
            "Center",
            "Right"});
            this.TextAlignComboBox.Location = new System.Drawing.Point(152, 93);
            this.TextAlignComboBox.Name = "TextAlignComboBox";
            this.TextAlignComboBox.Size = new System.Drawing.Size(89, 21);
            this.TextAlignComboBox.TabIndex = 3;
            this.TextAlignComboBox.Leave += new System.EventHandler(this.TextAlignComboBox_Leave);
            // 
            // ColorButton
            // 
            this.ColorButton.BackColor = System.Drawing.Color.Black;
            this.ColorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ColorButton.Location = new System.Drawing.Point(152, 125);
            this.ColorButton.Name = "ColorButton";
            this.ColorButton.Size = new System.Drawing.Size(22, 23);
            this.ColorButton.TabIndex = 5;
            this.ColorButton.UseVisualStyleBackColor = false;
            this.ColorButton.Click += new System.EventHandler(this.ColorButton_Click);
            // 
            // ColorLabel
            // 
            this.ColorLabel.AutoSize = true;
            this.ColorLabel.Location = new System.Drawing.Point(80, 130);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(58, 13);
            this.ColorLabel.TabIndex = 4;
            this.ColorLabel.Text = "Text Color:";
            // 
            // TextSizeLabel
            // 
            this.TextSizeLabel.AutoSize = true;
            this.TextSizeLabel.Location = new System.Drawing.Point(194, 130);
            this.TextSizeLabel.Name = "TextSizeLabel";
            this.TextSizeLabel.Size = new System.Drawing.Size(54, 13);
            this.TextSizeLabel.TabIndex = 6;
            this.TextSizeLabel.Text = "Text Size:";
            // 
            // TextSizeBox
            // 
            this.TextSizeBox.Location = new System.Drawing.Point(256, 128);
            this.TextSizeBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.TextSizeBox.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.TextSizeBox.Name = "TextSizeBox";
            this.TextSizeBox.Size = new System.Drawing.Size(50, 20);
            this.TextSizeBox.TabIndex = 7;
            this.TextSizeBox.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.TextSizeBox.Leave += new System.EventHandler(this.TextSizeBox_Leave);
            // 
            // PointsLabel
            // 
            this.PointsLabel.AutoSize = true;
            this.PointsLabel.Location = new System.Drawing.Point(312, 130);
            this.PointsLabel.Name = "PointsLabel";
            this.PointsLabel.Size = new System.Drawing.Size(41, 13);
            this.PointsLabel.TabIndex = 8;
            this.PointsLabel.Text = "(points)";
            // 
            // SourceGroupBox
            // 
            this.SourceGroupBox.Controls.Add(this.SourceLabel);
            this.SourceGroupBox.Controls.Add(this.SourcePageBox);
            this.SourceGroupBox.Controls.Add(this.PointsLabel);
            this.SourceGroupBox.Controls.Add(this.TextSizeLabel);
            this.SourceGroupBox.Controls.Add(this.TextSizeBox);
            this.SourceGroupBox.Controls.Add(this.ColorLabel);
            this.SourceGroupBox.Controls.Add(this.ColorButton);
            this.SourceGroupBox.Controls.Add(this.textAlignLabel);
            this.SourceGroupBox.Controls.Add(this.TextAlignComboBox);
            this.SourceGroupBox.Controls.Add(this.BrowseButton);
            this.SourceGroupBox.Controls.Add(this.ImageTextBox);
            this.SourceGroupBox.Controls.Add(this.ImageRadioButton);
            this.SourceGroupBox.Controls.Add(this.TextRadioButton);
            this.SourceGroupBox.Controls.Add(this.WatermarkTextBox);
            this.SourceGroupBox.Location = new System.Drawing.Point(12, 5);
            this.SourceGroupBox.Name = "SourceGroupBox";
            this.SourceGroupBox.Size = new System.Drawing.Size(362, 230);
            this.SourceGroupBox.TabIndex = 1;
            this.SourceGroupBox.TabStop = false;
            this.SourceGroupBox.Text = "Source";
            // 
            // SourceLabel
            // 
            this.SourceLabel.AutoSize = true;
            this.SourceLabel.Location = new System.Drawing.Point(63, 199);
            this.SourceLabel.Name = "SourceLabel";
            this.SourceLabel.Size = new System.Drawing.Size(75, 13);
            this.SourceLabel.TabIndex = 12;
            this.SourceLabel.Text = "Page Number:";
            // 
            // SourcePageBox
            // 
            this.SourcePageBox.Enabled = false;
            this.SourcePageBox.Location = new System.Drawing.Point(143, 197);
            this.SourcePageBox.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.SourcePageBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SourcePageBox.Name = "SourcePageBox";
            this.SourcePageBox.Size = new System.Drawing.Size(50, 20);
            this.SourcePageBox.TabIndex = 13;
            this.SourcePageBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SourcePageBox.Leave += new System.EventHandler(this.sourcePageBox_Leave);
            // 
            // zOrderLabel
            // 
            this.zOrderLabel.AutoSize = true;
            this.zOrderLabel.Location = new System.Drawing.Point(233, 250);
            this.zOrderLabel.Name = "zOrderLabel";
            this.zOrderLabel.Size = new System.Drawing.Size(43, 13);
            this.zOrderLabel.TabIndex = 7;
            this.zOrderLabel.Text = "ZOrder:";
            // 
            // zOrderComboBox
            // 
            this.zOrderComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.zOrderComboBox.FormattingEnabled = true;
            this.zOrderComboBox.Items.AddRange(new object[] {
            "Back",
            "Front"});
            this.zOrderComboBox.Location = new System.Drawing.Point(285, 246);
            this.zOrderComboBox.Name = "zOrderComboBox";
            this.zOrderComboBox.Size = new System.Drawing.Size(89, 21);
            this.zOrderComboBox.TabIndex = 8;
            this.zOrderComboBox.Leave += new System.EventHandler(this.zOrderComboBox_Leave);
            // 
            // PreviewLabel
            // 
            this.PreviewLabel.AutoSize = true;
            this.PreviewLabel.Location = new System.Drawing.Point(380, 24);
            this.PreviewLabel.Name = "PreviewLabel";
            this.PreviewLabel.Size = new System.Drawing.Size(35, 13);
            this.PreviewLabel.TabIndex = 2;
            this.PreviewLabel.Text = "Page:";
            // 
            // PageBox
            // 
            this.PageBox.Location = new System.Drawing.Point(421, 20);
            this.PageBox.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.PageBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PageBox.Name = "PageBox";
            this.PageBox.Size = new System.Drawing.Size(50, 20);
            this.PageBox.TabIndex = 3;
            this.PageBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.PageBox.ValueChanged += new System.EventHandler(this.pageBox_ValueChanged);
            // 
            // pdfXpress1
            // 
            this.pdfXpress1.Debug = false;
            this.pdfXpress1.DebugLogFile = "";
            this.pdfXpress1.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production;
            // 
            // AddWatermarkForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 568);
            this.Controls.Add(this.PreviewLabel);
            this.Controls.Add(this.PageBox);
            this.Controls.Add(this.zOrderLabel);
            this.Controls.Add(this.zOrderComboBox);
            this.Controls.Add(this.SourceGroupBox);
            this.Controls.Add(this.CButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.VertOffsetPercentLabel);
            this.Controls.Add(this.VertOffsetLabel);
            this.Controls.Add(this.VertOffsetBox);
            this.Controls.Add(this.HorzOffsetPercentLabel);
            this.Controls.Add(this.HorzOffsetLabel);
            this.Controls.Add(this.HorzOffsetBox);
            this.Controls.Add(this.FixedCheckBox);
            this.Controls.Add(this.ViewCheckBox);
            this.Controls.Add(this.PrintCheckBox);
            this.Controls.Add(this.PercentScaleLabel);
            this.Controls.Add(this.ScaleLabel);
            this.Controls.Add(this.ScaleBox);
            this.Controls.Add(this.VerticalLabel);
            this.Controls.Add(this.VertComboBox);
            this.Controls.Add(this.HorzAlignLabel);
            this.Controls.Add(this.HorizComboBox);
            this.Controls.Add(this.OpacityPercentLabel);
            this.Controls.Add(this.OpacityLabel);
            this.Controls.Add(this.OpacityBox);
            this.Controls.Add(this.DegreesLabel);
            this.Controls.Add(this.RotationLabel);
            this.Controls.Add(this.RotationBox);
            this.Controls.Add(this.imageXView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddWatermarkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Add Watermark";
            this.Load += new System.EventHandler(this.AddWatermarkForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RotationBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OpacityBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScaleBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HorzOffsetBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VertOffsetBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextSizeBox)).EndInit();
            this.SourceGroupBox.ResumeLayout(false);
            this.SourceGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SourcePageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private System.Windows.Forms.NumericUpDown RotationBox;
        private System.Windows.Forms.Label RotationLabel;
        private System.Windows.Forms.Label DegreesLabel;
        private System.Windows.Forms.NumericUpDown OpacityBox;
        private System.Windows.Forms.Label OpacityLabel;
        private System.Windows.Forms.Label OpacityPercentLabel;
        private System.Windows.Forms.ComboBox HorizComboBox;
        private System.Windows.Forms.Label HorzAlignLabel;
        private System.Windows.Forms.Label VerticalLabel;
        private System.Windows.Forms.ComboBox VertComboBox;
        private System.Windows.Forms.Label ScaleLabel;
        private System.Windows.Forms.NumericUpDown ScaleBox;
        private System.Windows.Forms.Label PercentScaleLabel;
        private System.Windows.Forms.CheckBox PrintCheckBox;
        private System.Windows.Forms.CheckBox ViewCheckBox;
        private System.Windows.Forms.CheckBox FixedCheckBox;
        private System.Windows.Forms.Label HorzOffsetLabel;
        private System.Windows.Forms.NumericUpDown HorzOffsetBox;
        private System.Windows.Forms.Label HorzOffsetPercentLabel;
        private System.Windows.Forms.Label VertOffsetPercentLabel;
        private System.Windows.Forms.Label VertOffsetLabel;
        private System.Windows.Forms.NumericUpDown VertOffsetBox;
        private System.Windows.Forms.TextBox WatermarkTextBox;
        private System.Windows.Forms.RadioButton TextRadioButton;
        private System.Windows.Forms.RadioButton ImageRadioButton;
        private System.Windows.Forms.TextBox ImageTextBox;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CButton;
        private System.Windows.Forms.Label textAlignLabel;
        private System.Windows.Forms.ComboBox TextAlignComboBox;
        private System.Windows.Forms.Button ColorButton;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.Label TextSizeLabel;
        private System.Windows.Forms.NumericUpDown TextSizeBox;
        private System.Windows.Forms.Label PointsLabel;
        private System.Windows.Forms.GroupBox SourceGroupBox;
        private System.Windows.Forms.Label SourceLabel;
        private System.Windows.Forms.NumericUpDown SourcePageBox;
        private System.Windows.Forms.Label zOrderLabel;
        private System.Windows.Forms.ComboBox zOrderComboBox;
        private System.Windows.Forms.Label PreviewLabel;
        private System.Windows.Forms.NumericUpDown PageBox;
        private Accusoft.PdfXpressSdk.PdfXpress pdfXpress1;
    }
}
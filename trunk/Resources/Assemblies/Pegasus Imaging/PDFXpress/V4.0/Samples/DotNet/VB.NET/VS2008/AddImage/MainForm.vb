'****************************************************************'
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Option Explicit On

Imports Accusoft.PdfXpressSdk

Public Class MainForm

    Dim inputFilename As String
    Dim outputFilename As String

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub PDFXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PDFXpressToolStripMenuItem.Click
        PdfXpress.AboutBox()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        PdfXpress = New PdfXpress()

        PdfXpress.Initialize(Nothing, Nothing)

        ' TODO: Change the 4 runtime unlock codes below to those purchased once a license agreement is in place.
        ' To purchase runtime licensing for this control, please contact sales@accusoft.com.
        'PdfXpress.Licensing.UnlockRuntime(1234, 1234, 1234, 1234)

        Application.EnableVisualStyles()
    End Sub

    Function GetInputFile() As DialogResult
        Try
            OpenDialog.Filter = "All Image files (*.*)|*.*"

            OpenDialog.InitialDirectory = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\")

            OpenDialog.FilterIndex = 1
            OpenDialog.RestoreDirectory = True
            OpenDialog.Title = "Choose an Image"
            OpenDialog.FileName = ""

            If (OpenDialog.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                If Not (OpenDialog.FileName = "") Then
                    inputFilename = OpenDialog.FileName
                End If

                Return Windows.Forms.DialogResult.OK
            Else
                Return Windows.Forms.DialogResult.Cancel
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Function GetOutputFile() As DialogResult
        Try
            Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
            Dim strImageDir As String = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\..\Common\Images")
            SaveDialog.Filter = "PDF file (*.pdf)|*.pdf"
            SaveDialog.FilterIndex = 1
            SaveDialog.RestoreDirectory = True
            SaveDialog.Title = "Save PDF Document"
            SaveDialog.FileName = ""
            SaveDialog.InitialDirectory = strImageDir

            If SaveDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                If Not (SaveDialog.FileName = "") Then
                    outputFilename = SaveDialog.FileName
                End If
                Return Windows.Forms.DialogResult.OK
            Else
                Return Windows.Forms.DialogResult.Cancel
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub AddButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddFromFileButton.Click
        Try
            ' Get the paths for the input image and output PDF file

            If GetInputFile() = Windows.Forms.DialogResult.OK Then

                If GetOutputFile() = Windows.Forms.DialogResult.OK Then

                    ' Create the output PDF document
                    Using outputDocument As Document = New Document(pdfXpress)

                        ' Create the output page
                        Dim PageOptions As PageOptions = New PageOptions()
                        outputDocument.CreatePage(-1, PageOptions) ' we want to create page 0, so we specify the page "before"

                        ' Create the location of the image on the page
                        Dim destinationX As Double = 0.25 * 72.0 ' Quarter inch margins around image
                        Dim destinationY As Double = 0.25 * 72.0
                        Dim destinationWidth As Double = PageOptions.MediaWidth - 0.5 * 72.0
                        Dim destinationHeight As Double = PageOptions.MediaHeight - 0.5 * 72.0

                        ' Define the image fit
                        Dim destinationFit As ImageFitSettings = ImageFitSettings.Shrink

                        ' Add the image to the output page
                        outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight, destinationFit, inputFilename, 0)

                        ' Save the output PDF document
                        Dim SaveOptions As SaveOptions = New SaveOptions()
                        SaveOptions.Filename = outputFilename
                        outputDocument.Save(SaveOptions)
                    End Using
                End If

            End If
        Catch ex As PdfXpressLibraryException
            MessageBox.Show(ex.Message)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AddButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddFromByteArrayButton.Click
        Try
            ' Get the paths for the input image and output PDF file

            If GetInputFile() = Windows.Forms.DialogResult.OK Then

                If GetOutputFile() = Windows.Forms.DialogResult.OK Then

                    Dim fs As System.IO.FileStream = New System.IO.FileStream(inputFilename, System.IO.FileMode.Open)

                    Dim byteData(fs.Length) As Byte
                    fs.Read(byteData, 0, byteData.Length)
                    fs.Close()

                    ' Create the output PDF document
                    Using outputDocument As Document = New Document(pdfXpress)

                        ' Create the output page
                        Dim PageOptions As PageOptions = New PageOptions()
                        outputDocument.CreatePage(-1, PageOptions) ' we want to create page 0, so we specify the page "before"

                        ' Create the location of the image on the page
                        Dim destinationX As Double = 0.25 * 72.0 ' Quarter inch margins around image
                        Dim destinationY As Double = 0.25 * 72.0
                        Dim destinationWidth As Double = PageOptions.MediaWidth - 0.5 * 72.0
                        Dim destinationHeight As Double = PageOptions.MediaHeight - 0.5 * 72.0

                        ' Define the image fit
                        Dim destinationFit As ImageFitSettings = ImageFitSettings.Shrink

                        ' Add the image to the output page
                        outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight, destinationFit, byteData, 0)

                        ' Save the output PDF document
                        Dim SaveOptions As SaveOptions = New SaveOptions()
                        SaveOptions.Filename = outputFilename
                        outputDocument.Save(SaveOptions)
                    End Using
                End If
            End If
        Catch ex As PdfXpressLibraryException
            MessageBox.Show(ex.Message)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AddButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddFromHandleButton.Click
        Try
            Dim dataHandle As IntPtr = New IntPtr()
            ' Get the paths for the input image and output PDF file

            If GetInputFile() = Windows.Forms.DialogResult.OK Then

                If GetOutputFile() = Windows.Forms.DialogResult.OK Then

                    Dim fs As System.IO.FileStream = New System.IO.FileStream(inputFilename, System.IO.FileMode.Open)

                    Dim byteData(fs.Length) As Byte
                    fs.Read(byteData, 0, byteData.Length)
                    fs.Close()

                    dataHandle = System.Runtime.InteropServices.Marshal.AllocHGlobal(byteData.Length)

                    System.Runtime.InteropServices.Marshal.Copy(byteData, 0, dataHandle, byteData.Length)

                    ' Create the output PDF document
                    Using outputDocument As Document = New Document(pdfXpress)

                        ' Create the output page
                        Dim PageOptions As PageOptions = New PageOptions()
                        outputDocument.CreatePage(-1, PageOptions) ' we want to create page 0, so we specify the page "before"

                        ' Create the location of the image on the page
                        Dim destinationX As Double = 0.25 * 72.0 ' Quarter inch margins around image
                        Dim destinationY As Double = 0.25 * 72.0
                        Dim destinationWidth As Double = PageOptions.MediaWidth - 0.5 * 72.0
                        Dim destinationHeight As Double = PageOptions.MediaHeight - 0.5 * 72.0

                        ' Define the image fit
                        Dim destinationFit As ImageFitSettings = ImageFitSettings.Shrink

                        ' Add the image to the output page
                        outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight, destinationFit, dataHandle, 0)

                        System.Runtime.InteropServices.Marshal.FreeHGlobal(dataHandle)

                        ' Save the output PDF document
                        Dim SaveOptions As SaveOptions = New SaveOptions()
                        SaveOptions.Filename = outputFilename
                        outputDocument.Save(SaveOptions)
                    End Using
                End If
            End If
        Catch ex As PdfXpressLibraryException
            MessageBox.Show(ex.Message)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AddButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddFromDataInfoButton.Click
        Try
            ' Get the paths for the input image and output PDF file        
            If GetOutputFile() = Windows.Forms.DialogResult.OK Then

                pdfXpress.Documents.Add(Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\pdfxpress3.pdf")

                Dim opts As RenderOptions = New RenderOptions()

                Dim dataInfo As ImageDataInfo = PdfXpress.Documents(0).RenderPageToImageData(0, opts)

                ' Create the output PDF document
                Using outputDocument As Document = New Document(pdfXpress)

                    ' Create the output page
                    Dim PageOptions As PageOptions = New PageOptions()
                    outputDocument.CreatePage(-1, PageOptions) ' we want to create page 0, so we specify the page "before"

                    ' Create the location of the image on the page
                    Dim destinationX As Double = 0.25 * 72.0 ' Quarter inch margins around image
                    Dim destinationY As Double = 0.25 * 72.0
                    Dim destinationWidth As Double = PageOptions.MediaWidth - 0.5 * 72.0
                    Dim destinationHeight As Double = PageOptions.MediaHeight - 0.5 * 72.0

                    ' Define the image fit
                    Dim destinationFit As ImageFitSettings = ImageFitSettings.Shrink

                    ' Add the image to the output page
                    outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight, destinationFit, dataInfo)

                    ' Save the output PDF document
                    Dim SaveOptions As SaveOptions = New SaveOptions()
                    SaveOptions.Filename = outputFilename
                    outputDocument.Save(SaveOptions)
                End Using
            End If
        Catch ex As PdfXpressLibraryException
            MessageBox.Show(ex.Message)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class

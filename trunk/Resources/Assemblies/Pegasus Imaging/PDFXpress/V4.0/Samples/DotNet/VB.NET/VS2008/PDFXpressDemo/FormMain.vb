'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On
Option Strict On

Imports System.Runtime.InteropServices
Imports Accusoft.ImagXpressSdk
Imports Accusoft.PdfXpressSdk
Imports Accusoft.NotateXpressSdk
Imports System.IO
Imports System.Text

Public Class FormMain
    Inherits System.Windows.Forms.Form

    Friend WithEvents ToolbarMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents StatusBarMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents PreviousTextButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents NextTextButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBoxLeft As System.Windows.Forms.GroupBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents GroupBoxText As System.Windows.Forms.GroupBox
    Friend WithEvents contextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContextLabel As System.Windows.Forms.Label
    Friend WithEvents afterBox As System.Windows.Forms.TextBox
    Friend WithEvents AfterLabel As System.Windows.Forms.Label
    Friend WithEvents beforeBox As System.Windows.Forms.TextBox
    Friend WithEvents BeforeLabel As System.Windows.Forms.Label
    Friend WithEvents ToolStripDropDownButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents wholeMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WordsBeforeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents beforeWordsBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents WordsAfterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents afterWordsBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents deleteThumbnailToolStripMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents editBookmarksToolStripMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents insertPageToolStripMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents metaDataToolStripMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents movePageToolStripMenuItem As System.Windows.Forms.MenuItem

    Dim pdfxpress1 As PdfXpress
    Dim document As Document
    Dim rendOpts As RenderOptions
    Dim destinationFit As ImageFitSettings
    Dim matched() As TextMatch
    Dim bookmarks As ArrayList
    Dim so As Accusoft.PdfXpressSdk.SaveOptions

    Dim destinationX, destinationY, destinationW, destinationH As Integer
    Dim pageNumber, pageCount As Integer
    Dim wordChosen As Integer
    Dim counter As Integer
    Dim desX As Integer
    Dim desY As Integer
    Dim desW As Integer
    Dim desH As Integer

    Dim openPath As String
    Dim destinationFile As String
    Dim strFileName As String

    Dim highlight() As Rectangle

    Dim cancelNotPressed As Boolean
    Friend WithEvents ImageXView As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents NotateXpress1 As Accusoft.NotateXpressSdk.NotateXpress

    Dim useXFDF As Boolean

    Dim xfdfData() As Byte

    Dim xfdfOptions As XfdfOptions

    Dim lo As Accusoft.NotateXpressSdk.LoadOptions

    Dim nodeSelected As TreeNode

    ' Windows API functions
    <DllImport("kernel32.dll")> _
    Public Shared Function GetSystemDirectory(ByVal sb As System.Text.StringBuilder, ByVal nSize As Int32) As Int32
        ' you have to declare a body even though nothing goes here
    End Function

    <DllImport("kernel32.dll")> _
    Public Shared Function GetWindowsDirectory(ByVal sb As System.Text.StringBuilder, ByVal nSize As Int32) As Int32
        ' you have to declare a body even though nothing goes here
    End Function

    Declare Function QueryPerformanceCounter Lib "kernel32" (ByRef lpPerformanceCount As Long) As Boolean
    Declare Function QueryPerformanceFrequency Lib "kernel32" (ByRef lpFrequency As Long) As Boolean

    Friend WithEvents AboutMenu As System.Windows.Forms.MenuItem
    Friend WithEvents PDFXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents NewMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents SaveMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents RenderMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents groupBoxMain As System.Windows.Forms.GroupBox
    Friend WithEvents OpenButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CloseButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PreviousButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents NextButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents PrePageBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents PageBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents CountBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FindBox As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ZoomInButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ZoomOutButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ZoomFitButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents AboutButton As System.Windows.Forms.ToolStripButton

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            Try
                If Not (NotateXpress1 Is Nothing) Then
                    If Not (NotateXpress1.Layers Is Nothing) Then
                        Dim i As Integer
                        For i = 0 To NotateXpress1.Layers.Count - 1
                            If Not (NotateXpress1.Layers(i) Is Nothing) Then
                                If Not (NotateXpress1.Layers(i).Elements Is Nothing) Then
                                    NotateXpress1.Layers(i).Elements.Clear()
                                    NotateXpress1.Layers(i).Elements.Dispose()
                                End If
                            End If
                        Next i

                        NotateXpress1.Layers.Dispose()
                    End If

                    NotateXpress1.ClientDisconnect()
                    NotateXpress1.Dispose()
                    NotateXpress1 = Nothing
                End If
                If Not (ImageXView Is Nothing) Then
                    If Not (ImageXView.Image Is Nothing) Then
                        ImageXView.Image.Dispose()
                        ImageXView.Image = Nothing
                    End If
                    ImageXView.Dispose()
                    ImageXView = Nothing
                End If
                If Not (ImagXpress1 Is Nothing) Then
                    ImagXpress1.Dispose()
                    ImagXpress1 = Nothing
                End If
                If Not (document Is Nothing) Then
                    document.Dispose()
                End If
                If Not (bookmarks Is Nothing) Then
                    Dim i As Integer
                    For i = 0 To bookmarks.Count - 1
                        If Not (bookmarks(i) Is Nothing) Then
                            CType(bookmarks(i), BookmarkContext).Dispose()
                            bookmarks(i) = Nothing
                        End If
                    Next
                End If
                If Not (pdfxpress1 Is Nothing) Then
                    If Not (pdfxpress1.Documents Is Nothing) Then
                        Dim i As Integer
                        For i = 0 To pdfxpress1.Documents.Count - 1
                            If Not (pdfxpress1.Documents(i) Is Nothing) Then
                                pdfxpress1.Documents(i).Dispose()
                            End If
                        Next i
                    End If
                    pdfxpress1.Dispose()
                    pdfxpress1 = Nothing
                End If
                If Not (openFileDialog Is Nothing) Then
                    openFileDialog.Dispose()
                    openFileDialog = Nothing
                End If
            Catch ex As Exception

            End Try
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents FileMenu As System.Windows.Forms.MenuItem
    Friend WithEvents OpenMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents CloseMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFileBar6 As System.Windows.Forms.MenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents OptionsMenu As System.Windows.Forms.MenuItem
    Friend WithEvents AddImageMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ThumbMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents mainMenu As System.Windows.Forms.MainMenu
    Friend WithEvents ViewMenu As System.Windows.Forms.MenuItem
    Friend WithEvents openFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents statusBarMain As System.Windows.Forms.StatusBar
    Friend WithEvents statusBarPanelStat As System.Windows.Forms.StatusBarPanel
    Friend WithEvents statusBarPanelTime As System.Windows.Forms.StatusBarPanel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMain))
        Me.FileMenu = New System.Windows.Forms.MenuItem
        Me.NewMenuItem = New System.Windows.Forms.MenuItem
        Me.OpenMenuItem = New System.Windows.Forms.MenuItem
        Me.SaveMenuItem = New System.Windows.Forms.MenuItem
        Me.CloseMenuItem = New System.Windows.Forms.MenuItem
        Me.mnuFileBar6 = New System.Windows.Forms.MenuItem
        Me.ExitMenuItem = New System.Windows.Forms.MenuItem
        Me.OptionsMenu = New System.Windows.Forms.MenuItem
        Me.AddImageMenuItem = New System.Windows.Forms.MenuItem
        Me.ThumbMenuItem = New System.Windows.Forms.MenuItem
        Me.deleteThumbnailToolStripMenuItem = New System.Windows.Forms.MenuItem
        Me.editBookmarksToolStripMenuItem = New System.Windows.Forms.MenuItem
        Me.insertPageToolStripMenuItem = New System.Windows.Forms.MenuItem
        Me.metaDataToolStripMenuItem = New System.Windows.Forms.MenuItem
        Me.movePageToolStripMenuItem = New System.Windows.Forms.MenuItem
        Me.RenderMenuItem = New System.Windows.Forms.MenuItem
        Me.mainMenu = New System.Windows.Forms.MainMenu(Me.components)
        Me.ViewMenu = New System.Windows.Forms.MenuItem
        Me.ToolbarMenuItem = New System.Windows.Forms.MenuItem
        Me.StatusBarMenuItem = New System.Windows.Forms.MenuItem
        Me.AboutMenu = New System.Windows.Forms.MenuItem
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.PDFXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.openFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.statusBarMain = New System.Windows.Forms.StatusBar
        Me.statusBarPanelStat = New System.Windows.Forms.StatusBarPanel
        Me.statusBarPanelTime = New System.Windows.Forms.StatusBarPanel
        Me.ToolStrip = New System.Windows.Forms.ToolStrip
        Me.OpenButton = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripSeparator
        Me.CloseButton = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripSeparator
        Me.PreviousButton = New System.Windows.Forms.ToolStripButton
        Me.NextButton = New System.Windows.Forms.ToolStripButton
        Me.PrePageBox = New System.Windows.Forms.ToolStripTextBox
        Me.PageBox = New System.Windows.Forms.ToolStripTextBox
        Me.CountBox = New System.Windows.Forms.ToolStripTextBox
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.FindBox = New System.Windows.Forms.ToolStripTextBox
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton
        Me.wholeMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.WordsBeforeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.beforeWordsBox = New System.Windows.Forms.ToolStripTextBox
        Me.WordsAfterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.afterWordsBox = New System.Windows.Forms.ToolStripTextBox
        Me.PreviousTextButton = New System.Windows.Forms.ToolStripButton
        Me.NextTextButton = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ZoomInButton = New System.Windows.Forms.ToolStripButton
        Me.ZoomOutButton = New System.Windows.Forms.ToolStripButton
        Me.ZoomFitButton = New System.Windows.Forms.ToolStripButton
        Me.AboutButton = New System.Windows.Forms.ToolStripButton
        Me.groupBoxMain = New System.Windows.Forms.GroupBox
        Me.ImageXView = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.GroupBoxLeft = New System.Windows.Forms.GroupBox
        Me.TreeView1 = New System.Windows.Forms.TreeView
        Me.GroupBoxText = New System.Windows.Forms.GroupBox
        Me.contextBox = New System.Windows.Forms.TextBox
        Me.ContextLabel = New System.Windows.Forms.Label
        Me.afterBox = New System.Windows.Forms.TextBox
        Me.AfterLabel = New System.Windows.Forms.Label
        Me.beforeBox = New System.Windows.Forms.TextBox
        Me.BeforeLabel = New System.Windows.Forms.Label
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.NotateXpress1 = New Accusoft.NotateXpressSdk.NotateXpress(Me.components)
        CType(Me.statusBarPanelStat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.statusBarPanelTime, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip.SuspendLayout()
        Me.groupBoxMain.SuspendLayout()
        Me.GroupBoxLeft.SuspendLayout()
        Me.GroupBoxText.SuspendLayout()
        Me.SuspendLayout()
        '
        'FileMenu
        '
        Me.FileMenu.Index = 0
        Me.FileMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.NewMenuItem, Me.OpenMenuItem, Me.SaveMenuItem, Me.CloseMenuItem, Me.mnuFileBar6, Me.ExitMenuItem})
        Me.FileMenu.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.FileMenu.Text = "&File"
        '
        'NewMenuItem
        '
        Me.NewMenuItem.Index = 0
        Me.NewMenuItem.Text = "&New PDF Document"
        '
        'OpenMenuItem
        '
        Me.OpenMenuItem.Index = 1
        Me.OpenMenuItem.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.OpenMenuItem.Text = "&Open PDF Document"
        '
        'SaveMenuItem
        '
        Me.SaveMenuItem.Enabled = False
        Me.SaveMenuItem.Index = 2
        Me.SaveMenuItem.Text = "&Save PDF Document"
        '
        'CloseMenuItem
        '
        Me.CloseMenuItem.Enabled = False
        Me.CloseMenuItem.Index = 3
        Me.CloseMenuItem.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.CloseMenuItem.Text = "&Close PDF Document"
        '
        'mnuFileBar6
        '
        Me.mnuFileBar6.Index = 4
        Me.mnuFileBar6.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.mnuFileBar6.Text = "-"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Index = 5
        Me.ExitMenuItem.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.ExitMenuItem.Text = "E&xit"
        '
        'OptionsMenu
        '
        Me.OptionsMenu.Index = 1
        Me.OptionsMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.AddImageMenuItem, Me.ThumbMenuItem, Me.deleteThumbnailToolStripMenuItem, Me.editBookmarksToolStripMenuItem, Me.insertPageToolStripMenuItem, Me.metaDataToolStripMenuItem, Me.movePageToolStripMenuItem, Me.RenderMenuItem})
        Me.OptionsMenu.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.OptionsMenu.Text = "&Options"
        '
        'AddImageMenuItem
        '
        Me.AddImageMenuItem.Enabled = False
        Me.AddImageMenuItem.Index = 0
        Me.AddImageMenuItem.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.AddImageMenuItem.Text = "&Add Image"
        '
        'ThumbMenuItem
        '
        Me.ThumbMenuItem.Enabled = False
        Me.ThumbMenuItem.Index = 1
        Me.ThumbMenuItem.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.ThumbMenuItem.Text = "&Create Thumbnail"
        '
        'deleteThumbnailToolStripMenuItem
        '
        Me.deleteThumbnailToolStripMenuItem.Enabled = False
        Me.deleteThumbnailToolStripMenuItem.Index = 2
        Me.deleteThumbnailToolStripMenuItem.Text = "&Delete Thumbnail"
        '
        'editBookmarksToolStripMenuItem
        '
        Me.editBookmarksToolStripMenuItem.Enabled = False
        Me.editBookmarksToolStripMenuItem.Index = 3
        Me.editBookmarksToolStripMenuItem.Text = "&Edit Bookmarks"
        '
        'insertPageToolStripMenuItem
        '
        Me.insertPageToolStripMenuItem.Enabled = False
        Me.insertPageToolStripMenuItem.Index = 4
        Me.insertPageToolStripMenuItem.Text = "&Insert Page"
        '
        'metaDataToolStripMenuItem
        '
        Me.metaDataToolStripMenuItem.Enabled = False
        Me.metaDataToolStripMenuItem.Index = 5
        Me.metaDataToolStripMenuItem.Text = "&Metadata"
        '
        'movePageToolStripMenuItem
        '
        Me.movePageToolStripMenuItem.Enabled = False
        Me.movePageToolStripMenuItem.Index = 6
        Me.movePageToolStripMenuItem.Text = "Move &Page"
        '
        'RenderMenuItem
        '
        Me.RenderMenuItem.Enabled = False
        Me.RenderMenuItem.Index = 7
        Me.RenderMenuItem.Text = "&Set Render Options"
        '
        'mainMenu
        '
        Me.mainMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.FileMenu, Me.OptionsMenu, Me.ViewMenu, Me.AboutMenu})
        '
        'ViewMenu
        '
        Me.ViewMenu.Index = 2
        Me.ViewMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ToolbarMenuItem, Me.StatusBarMenuItem})
        Me.ViewMenu.MergeType = System.Windows.Forms.MenuMerge.Remove
        Me.ViewMenu.Text = "&View"
        '
        'ToolbarMenuItem
        '
        Me.ToolbarMenuItem.Checked = True
        Me.ToolbarMenuItem.Index = 0
        Me.ToolbarMenuItem.Text = "&ToolBar"
        '
        'StatusBarMenuItem
        '
        Me.StatusBarMenuItem.Checked = True
        Me.StatusBarMenuItem.Index = 1
        Me.StatusBarMenuItem.Text = "&StatusBar"
        '
        'AboutMenu
        '
        Me.AboutMenu.Index = 3
        Me.AboutMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ImagXpressMenuItem, Me.PDFXpressMenuItem})
        Me.AboutMenu.Text = "&About"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 0
        Me.ImagXpressMenuItem.Text = "&ImagXpress"
        '
        'PDFXpressMenuItem
        '
        Me.PDFXpressMenuItem.Index = 1
        Me.PDFXpressMenuItem.Text = "&PDFXpress"
        '
        'statusBarMain
        '
        Me.statusBarMain.Location = New System.Drawing.Point(0, 515)
        Me.statusBarMain.Name = "statusBarMain"
        Me.statusBarMain.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusBarPanelStat, Me.statusBarPanelTime})
        Me.statusBarMain.ShowPanels = True
        Me.statusBarMain.Size = New System.Drawing.Size(885, 22)
        Me.statusBarMain.SizingGrip = False
        Me.statusBarMain.TabIndex = 3
        Me.statusBarMain.Text = "hello"
        '
        'statusBarPanelStat
        '
        Me.statusBarPanelStat.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusBarPanelStat.Name = "statusBarPanelStat"
        Me.statusBarPanelStat.Text = "statusBarPanel"
        Me.statusBarPanelStat.Width = 875
        '
        'statusBarPanelTime
        '
        Me.statusBarPanelTime.Alignment = System.Windows.Forms.HorizontalAlignment.Right
        Me.statusBarPanelTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
        Me.statusBarPanelTime.Name = "statusBarPanelTime"
        Me.statusBarPanelTime.Width = 10
        '
        'ToolStrip
        '
        Me.ToolStrip.AutoSize = False
        Me.ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenButton, Me.ToolStripButton6, Me.CloseButton, Me.ToolStripButton4, Me.PreviousButton, Me.NextButton, Me.PrePageBox, Me.PageBox, Me.CountBox, Me.ToolStripSeparator1, Me.FindBox, Me.ToolStripDropDownButton1, Me.PreviousTextButton, Me.NextTextButton, Me.ToolStripSeparator2, Me.ZoomInButton, Me.ZoomOutButton, Me.ZoomFitButton, Me.AboutButton})
        Me.ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(885, 50)
        Me.ToolStrip.TabIndex = 0
        Me.ToolStrip.Text = "ToolStrip1"
        '
        'OpenButton
        '
        Me.OpenButton.AutoSize = False
        Me.OpenButton.Image = CType(resources.GetObject("OpenButton.Image"), System.Drawing.Image)
        Me.OpenButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OpenButton.Name = "OpenButton"
        Me.OpenButton.Size = New System.Drawing.Size(45, 47)
        Me.OpenButton.Text = "Open"
        Me.OpenButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(6, 50)
        '
        'CloseButton
        '
        Me.CloseButton.AutoSize = False
        Me.CloseButton.Enabled = False
        Me.CloseButton.Image = CType(resources.GetObject("CloseButton.Image"), System.Drawing.Image)
        Me.CloseButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CloseButton.Name = "CloseButton"
        Me.CloseButton.Size = New System.Drawing.Size(50, 47)
        Me.CloseButton.Text = "Close"
        Me.CloseButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(6, 50)
        '
        'PreviousButton
        '
        Me.PreviousButton.Enabled = False
        Me.PreviousButton.Image = CType(resources.GetObject("PreviousButton.Image"), System.Drawing.Image)
        Me.PreviousButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PreviousButton.Name = "PreviousButton"
        Me.PreviousButton.Size = New System.Drawing.Size(85, 47)
        Me.PreviousButton.Text = "Previous Page"
        Me.PreviousButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'NextButton
        '
        Me.NextButton.Enabled = False
        Me.NextButton.Image = CType(resources.GetObject("NextButton.Image"), System.Drawing.Image)
        Me.NextButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NextButton.Name = "NextButton"
        Me.NextButton.Size = New System.Drawing.Size(64, 47)
        Me.NextButton.Text = "Next Page"
        Me.NextButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'PrePageBox
        '
        Me.PrePageBox.Enabled = False
        Me.PrePageBox.Name = "PrePageBox"
        Me.PrePageBox.Size = New System.Drawing.Size(50, 50)
        Me.PrePageBox.Text = "Page"
        Me.PrePageBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PageBox
        '
        Me.PageBox.Enabled = False
        Me.PageBox.Name = "PageBox"
        Me.PageBox.Size = New System.Drawing.Size(50, 50)
        Me.PageBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CountBox
        '
        Me.CountBox.Enabled = False
        Me.CountBox.Name = "CountBox"
        Me.CountBox.Size = New System.Drawing.Size(50, 50)
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 50)
        '
        'FindBox
        '
        Me.FindBox.Enabled = False
        Me.FindBox.Name = "FindBox"
        Me.FindBox.Size = New System.Drawing.Size(100, 50)
        Me.FindBox.Text = "Find Text"
        Me.FindBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.wholeMenuItem, Me.WordsBeforeToolStripMenuItem, Me.beforeWordsBox, Me.WordsAfterToolStripMenuItem, Me.afterWordsBox})
        Me.ToolStripDropDownButton1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(154, 47)
        Me.ToolStripDropDownButton1.Text = "Press Enter to Find Text"
        '
        'wholeMenuItem
        '
        Me.wholeMenuItem.Checked = True
        Me.wholeMenuItem.CheckOnClick = True
        Me.wholeMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.wholeMenuItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wholeMenuItem.Name = "wholeMenuItem"
        Me.wholeMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.wholeMenuItem.Text = "Return Whole Words Only"
        '
        'WordsBeforeToolStripMenuItem
        '
        Me.WordsBeforeToolStripMenuItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WordsBeforeToolStripMenuItem.Name = "WordsBeforeToolStripMenuItem"
        Me.WordsBeforeToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.WordsBeforeToolStripMenuItem.Text = "# Words Before"
        '
        'beforeWordsBox
        '
        Me.beforeWordsBox.Name = "beforeWordsBox"
        Me.beforeWordsBox.Size = New System.Drawing.Size(100, 23)
        Me.beforeWordsBox.Text = "5"
        Me.beforeWordsBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'WordsAfterToolStripMenuItem
        '
        Me.WordsAfterToolStripMenuItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WordsAfterToolStripMenuItem.Name = "WordsAfterToolStripMenuItem"
        Me.WordsAfterToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.WordsAfterToolStripMenuItem.Text = "# Words After"
        '
        'afterWordsBox
        '
        Me.afterWordsBox.Name = "afterWordsBox"
        Me.afterWordsBox.Size = New System.Drawing.Size(100, 23)
        Me.afterWordsBox.Text = "5"
        Me.afterWordsBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PreviousTextButton
        '
        Me.PreviousTextButton.Enabled = False
        Me.PreviousTextButton.Image = CType(resources.GetObject("PreviousTextButton.Image"), System.Drawing.Image)
        Me.PreviousTextButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PreviousTextButton.Name = "PreviousTextButton"
        Me.PreviousTextButton.Size = New System.Drawing.Size(56, 47)
        Me.PreviousTextButton.Text = "Previous"
        Me.PreviousTextButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'NextTextButton
        '
        Me.NextTextButton.Enabled = False
        Me.NextTextButton.Image = CType(resources.GetObject("NextTextButton.Image"), System.Drawing.Image)
        Me.NextTextButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NextTextButton.Name = "NextTextButton"
        Me.NextTextButton.Size = New System.Drawing.Size(35, 47)
        Me.NextTextButton.Text = "Next"
        Me.NextTextButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 50)
        '
        'ZoomInButton
        '
        Me.ZoomInButton.Enabled = False
        Me.ZoomInButton.Image = CType(resources.GetObject("ZoomInButton.Image"), System.Drawing.Image)
        Me.ZoomInButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ZoomInButton.Name = "ZoomInButton"
        Me.ZoomInButton.Size = New System.Drawing.Size(56, 47)
        Me.ZoomInButton.Text = "Zoom In"
        Me.ZoomInButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ZoomOutButton
        '
        Me.ZoomOutButton.Enabled = False
        Me.ZoomOutButton.Image = CType(resources.GetObject("ZoomOutButton.Image"), System.Drawing.Image)
        Me.ZoomOutButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ZoomOutButton.Name = "ZoomOutButton"
        Me.ZoomOutButton.Size = New System.Drawing.Size(66, 35)
        Me.ZoomOutButton.Text = "Zoom Out"
        Me.ZoomOutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ZoomFitButton
        '
        Me.ZoomFitButton.Enabled = False
        Me.ZoomFitButton.Image = CType(resources.GetObject("ZoomFitButton.Image"), System.Drawing.Image)
        Me.ZoomFitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ZoomFitButton.Name = "ZoomFitButton"
        Me.ZoomFitButton.Size = New System.Drawing.Size(59, 35)
        Me.ZoomFitButton.Text = "Zoom Fit"
        Me.ZoomFitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'AboutButton
        '
        Me.AboutButton.Image = CType(resources.GetObject("AboutButton.Image"), System.Drawing.Image)
        Me.AboutButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.AboutButton.Name = "AboutButton"
        Me.AboutButton.Size = New System.Drawing.Size(44, 35)
        Me.AboutButton.Text = "About"
        Me.AboutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'groupBoxMain
        '
        Me.groupBoxMain.Controls.Add(Me.ImageXView)
        Me.groupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupBoxMain.Location = New System.Drawing.Point(285, 50)
        Me.groupBoxMain.Name = "groupBoxMain"
        Me.groupBoxMain.Size = New System.Drawing.Size(600, 465)
        Me.groupBoxMain.TabIndex = 2
        Me.groupBoxMain.TabStop = False
        '
        'ImageXView
        '
        Me.ImageXView.AutoResize = Accusoft.ImagXpressSdk.AutoResizeType.BestFit
        Me.ImageXView.AutoScroll = True
        Me.ImageXView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ImageXView.Location = New System.Drawing.Point(3, 16)
        Me.ImageXView.Name = "ImageXView"
        Me.ImageXView.Size = New System.Drawing.Size(594, 446)
        Me.ImageXView.TabIndex = 13
        '
        'GroupBoxLeft
        '
        Me.GroupBoxLeft.Controls.Add(Me.TreeView1)
        Me.GroupBoxLeft.Controls.Add(Me.GroupBoxText)
        Me.GroupBoxLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBoxLeft.Location = New System.Drawing.Point(0, 50)
        Me.GroupBoxLeft.Name = "GroupBoxLeft"
        Me.GroupBoxLeft.Size = New System.Drawing.Size(282, 465)
        Me.GroupBoxLeft.TabIndex = 1
        Me.GroupBoxLeft.TabStop = False
        Me.GroupBoxLeft.Text = "BookMarks"
        '
        'TreeView1
        '
        Me.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeView1.Location = New System.Drawing.Point(3, 16)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(276, 260)
        Me.TreeView1.TabIndex = 0
        '
        'GroupBoxText
        '
        Me.GroupBoxText.Controls.Add(Me.contextBox)
        Me.GroupBoxText.Controls.Add(Me.ContextLabel)
        Me.GroupBoxText.Controls.Add(Me.afterBox)
        Me.GroupBoxText.Controls.Add(Me.AfterLabel)
        Me.GroupBoxText.Controls.Add(Me.beforeBox)
        Me.GroupBoxText.Controls.Add(Me.BeforeLabel)
        Me.GroupBoxText.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBoxText.Location = New System.Drawing.Point(3, 276)
        Me.GroupBoxText.Name = "GroupBoxText"
        Me.GroupBoxText.Size = New System.Drawing.Size(276, 186)
        Me.GroupBoxText.TabIndex = 1
        Me.GroupBoxText.TabStop = False
        Me.GroupBoxText.Text = "Text Match"
        '
        'contextBox
        '
        Me.contextBox.Location = New System.Drawing.Point(9, 140)
        Me.contextBox.Name = "contextBox"
        Me.contextBox.Size = New System.Drawing.Size(261, 20)
        Me.contextBox.TabIndex = 5
        '
        'ContextLabel
        '
        Me.ContextLabel.AutoSize = True
        Me.ContextLabel.Location = New System.Drawing.Point(9, 124)
        Me.ContextLabel.Name = "ContextLabel"
        Me.ContextLabel.Size = New System.Drawing.Size(184, 13)
        Me.ContextLabel.TabIndex = 4
        Me.ContextLabel.Text = "Search Word Matched Context String"
        '
        'afterBox
        '
        Me.afterBox.Location = New System.Drawing.Point(9, 94)
        Me.afterBox.Name = "afterBox"
        Me.afterBox.Size = New System.Drawing.Size(261, 20)
        Me.afterBox.TabIndex = 3
        '
        'AfterLabel
        '
        Me.AfterLabel.AutoSize = True
        Me.AfterLabel.Location = New System.Drawing.Point(9, 78)
        Me.AfterLabel.Name = "AfterLabel"
        Me.AfterLabel.Size = New System.Drawing.Size(162, 13)
        Me.AfterLabel.TabIndex = 2
        Me.AfterLabel.Text = "Words After Search Word Found"
        '
        'beforeBox
        '
        Me.beforeBox.Location = New System.Drawing.Point(9, 50)
        Me.beforeBox.Name = "beforeBox"
        Me.beforeBox.Size = New System.Drawing.Size(261, 20)
        Me.beforeBox.TabIndex = 1
        '
        'BeforeLabel
        '
        Me.BeforeLabel.AutoSize = True
        Me.BeforeLabel.Location = New System.Drawing.Point(9, 34)
        Me.BeforeLabel.Name = "BeforeLabel"
        Me.BeforeLabel.Size = New System.Drawing.Size(171, 13)
        Me.BeforeLabel.TabIndex = 0
        Me.BeforeLabel.Text = "Words Before Search Word Found"
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(282, 50)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 465)
        Me.Splitter1.TabIndex = 12
        Me.Splitter1.TabStop = False
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit
        Me.NotateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal
        Me.NotateXpress1.MultiLineEdit = False
        Me.NotateXpress1.RecalibrateXdpi = -1
        Me.NotateXpress1.RecalibrateYdpi = -1
        Me.NotateXpress1.ToolTipTimeEdit = 0
        Me.NotateXpress1.ToolTipTimeInteractive = 0
        '
        'FormMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(885, 537)
        Me.Controls.Add(Me.groupBoxMain)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.GroupBoxLeft)
        Me.Controls.Add(Me.ToolStrip)
        Me.Controls.Add(Me.statusBarMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.mainMenu
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PDF Xpress Demo"
        CType(Me.statusBarPanelStat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.statusBarPanelTime, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip.ResumeLayout(False)
        Me.ToolStrip.PerformLayout()
        Me.groupBoxMain.ResumeLayout(False)
        Me.GroupBoxLeft.ResumeLayout(False)
        Me.GroupBoxText.ResumeLayout(False)
        Me.GroupBoxText.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            '***Must call the UnlockRuntime method to unlock the control
            'The unlock codes shown below are for formatting purposes only and will not work!
            'imagXpress1.License.UnlockRuntime(1234, 1234, 1234, 1234);
            'pdfXpress.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

            pdfxpress1 = New PdfXpress()

            pdfxpress1.Licensing.LicenseEdition = LicenseChoice.ProfessionalEdition

            LoadNewDoc()

            openPath = Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\")


            If (Directory.Exists(Application.StartupPath & "..\..\..\..\..\..\..\..\..\PdfXpress\v2.0\Support\Font\") = True _
                    And Directory.Exists(Application.StartupPath & "..\..\..\..\..\..\..\..\..\PdfXpress\v2.0\Support\CMap\") = True) Then

                pdfxpress1.Initialize(Application.StartupPath & "..\..\..\..\..\..\..\..\..\PdfXpress\v2.0\Support\Font\", _
                    Application.StartupPath + "..\..\..\..\..\..\..\..\..\PdfXpress\v2.0\Support\CMap\")
            Else
                pdfxpress1.Initialize()
            End If

            NotateXpress1.ClientWindow = ImageXView.Handle

            NotateXpress1.Layers.Add(New Layer())
            NotateXpress1.Layers.Add(New Layer())

            counter = 0

            rendOpts = New RenderOptions()
            rendOpts.ResolutionX = 300
            rendOpts.ResolutionY = 300

            destinationX = CInt(0.25 * 72)
            destinationY = CInt(0.25 * 72)

            so = New Accusoft.PdfXpressSdk.SaveOptions()
            so.Overwrite = True

            Application.EnableVisualStyles()

            bookmarks = New ArrayList()

            document = New Document(pdfxpress1)

            lo = New Accusoft.NotateXpressSdk.LoadOptions()
            lo.AnnType = AnnotationType.Xfdf

            xfdfOptions = New XfdfOptions()
            xfdfOptions.WhichEncodingName = "UTF-8"
            xfdfOptions.WhichAnnotation = xfdfOptions.AllAnnotations
            xfdfOptions.WhichPage = xfdfOptions.AllPages

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub LoadNewDoc()
        statusBarPanelStat.Text = "Use File | Open to open a PDF document"
        groupBoxMain.Text = "Use File | Open to open a PDF document"
    End Sub

    Private Sub mnuFileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub mnuFileOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenMenuItem.Click
        Try
            openFileDialog.Title = "Open a PDF Document"
            openFileDialog.InitialDirectory = openPath

            openFileDialog.Filter = "PDF Document (*.PDF)|*.PDF|All Files (*.*) | *.*"

            If (openFileDialog.ShowDialog() = DialogResult.OK) Then

                strFileName = openFileDialog.FileName
                statusBarPanelStat.Text = ""

                If (File.Exists(strFileName)) Then

                    Dim oo As OpenOptions = New OpenOptions()
                    oo.Filename = strFileName
                    oo.Password = ""
                    oo.Repair = True

                    'resource cleanup
                    If Not (document Is Nothing) Then
                        document.Dispose()
                    End If

                    document = New Document(pdfxpress1, oo)

                    pageNumber = 0

                    If (counter = 0) Then
                        RendForm()

                        counter = 1
                    Else
                        ImageLoad()
                    End If
                End If
            End If
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ChildBookmarkTree(ByVal parentBookmark As BookmarkContext, ByVal parentNodes As TreeNodeCollection)
        Dim bookmark As BookmarkContext = document.GetBookmarkChild(parentBookmark)

        If Not (bookmark Is Nothing) Then

            bookmarks.Add(bookmark)

            Dim node As TreeNode = Nothing

            If (bookmark.ActionPageNumber = -1) Then
                node = New TreeNode(bookmark.Title)
                node.Name = bookmark.ActionPageNumber.ToString()
            Else
                node = New TreeNode(bookmark.Title & " (page " & (bookmark.ActionPageNumber + 1) & ")")
                node.Name = bookmark.ActionPageNumber.ToString()
            End If

            Dim BookmarkContent As BookmarkContent = New BookmarkContent()

            BookmarkContent.actionPageNumber = bookmark.ActionPageNumber
            node.Tag = BookmarkContent
            parentNodes.Add(node)

            ChildBookmarkTree(bookmark, node.Nodes)
            SiblingBookmarkTree(bookmark, parentNodes)
        End If
    End Sub

    Private Sub SiblingBookmarkTree(ByVal parentBookmark As BookmarkContext, ByVal parentNodes As TreeNodeCollection)
        Dim bookmark As BookmarkContext = document.GetBookmarkSibling(parentBookmark)

        If Not (bookmark Is Nothing) Then

            bookmarks.Add(bookmark)

            Dim node As TreeNode = Nothing

            If (bookmark.ActionPageNumber = -1) Then
                node = New TreeNode(bookmark.Title)
                node.Name = bookmark.ActionPageNumber.ToString()
            Else
                node = New TreeNode(bookmark.Title & " (page " & (bookmark.ActionPageNumber + 1) & ")")
                node.Name = bookmark.ActionPageNumber.ToString()
            End If

            Dim BookmarkContent As BookmarkContent = New BookmarkContent()

            BookmarkContent.actionPageNumber = bookmark.ActionPageNumber

            node.Tag = BookmarkContent
            parentNodes.Add(node)

            ChildBookmarkTree(bookmark, node.Nodes)
            SiblingBookmarkTree(bookmark, parentNodes)
        End If
    End Sub

    Private Class BookmarkContent
        Public actionPageNumber As Integer
    End Class

    Private Sub InitializeBookmarkTree()

        ClearBookmarks()

        TreeView1.BeginUpdate()
        TreeView1.Nodes.Clear()

        Using bookmarkRoot As BookmarkContext = document.GetBookmarkRoot()
            ChildBookmarkTree(bookmarkRoot, TreeView1.Nodes)
        End Using

        TreeView1.EndUpdate()
    End Sub

    Private Sub mnuFileClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseMenuItem.Click
        Try
            If Not (ImageXView.Image Is Nothing) Then
                ImageXView.Image.Dispose()
                ImageXView.Image = Nothing
            End If

            LoadNewDoc()

            document.Dispose()

            ImageXView.Refresh()
            statusBarPanelStat.Text = ""
            groupBoxMain.Text = "Use File | Open to open a PDF Document"
            PageBox.Enabled = False
            PageBox.Text = ""
            CountBox.Text = ""
            PreviousButton.Enabled = False
            NextButton.Enabled = False
            ZoomInButton.Enabled = False
            ZoomOutButton.Enabled = False
            ZoomFitButton.Enabled = False
            FindBox.Enabled = False
            CloseButton.Enabled = False

            AddImageMenuItem.Enabled = False
            ThumbMenuItem.Enabled = False
            RenderMenuItem.Enabled = False
            SaveMenuItem.Enabled = False
            FindBox.Text = ""
            PreviousTextButton.Enabled = False
            NextTextButton.Enabled = False
            editBookmarksToolStripMenuItem.Enabled = False
            metaDataToolStripMenuItem.Enabled = False
            deleteThumbnailToolStripMenuItem.Enabled = False
            insertPageToolStripMenuItem.Enabled = False
            movePageToolStripMenuItem.Enabled = False
            CloseMenuItem.Enabled = False

            ClearBookmarks()

            contextBox.Text = ""
            beforeBox.Text = ""
            afterBox.Text = ""

            TreeView1.Nodes.Clear()

        Catch ex As ImagXpressException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ClearBookmarks()
        If Not (bookmarks Is Nothing) Then
            Dim i As Integer
            For i = 0 To bookmarks.Count - 1
                If (bookmarks(i) Is Nothing) Then
                    CType(bookmarks(i), BookmarkContext).Dispose()
                    bookmarks(i) = Nothing
                End If
            Next i
        End If

        bookmarks.Clear()
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        Try
            ImagXpress1.AboutBox()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub PDFXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PDFXpressMenuItem.Click
        Try
            pdfxpress1.AboutBox()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewMenuItem.Click
        Try
            Dim opts As PageOptions = New PageOptions()
            opts.MediaHeight = 792
            opts.MediaWidth = 612

            pageNumber = 0

            'resource cleanup
            If Not (document Is Nothing) Then
                document.Dispose()
            End If

            document = New Document(pdfxpress1)

            pageNumber = 0
            pageCount = 1

            ClearBookmarks()

            document.CreatePage(-1, opts)

            destinationW = CInt(document.GetInfo(0).MediaWidth)
            destinationH = CInt(document.GetInfo(0).MediaHeight)

            UpdatePage()

            AddImageMenuItem.Enabled = True
            ThumbMenuItem.Enabled = True
            RenderMenuItem.Enabled = True
            SaveMenuItem.Enabled = True
            editBookmarksToolStripMenuItem.Enabled = True
            metaDataToolStripMenuItem.Enabled = True
            deleteThumbnailToolStripMenuItem.Enabled = True
            insertPageToolStripMenuItem.Enabled = True
            movePageToolStripMenuItem.Enabled = True
            CloseMenuItem.Enabled = True
            CloseButton.Enabled = True

            strFileName = Application.StartupPath + "\\new.pdf"

            groupBoxMain.Text = strFileName

            ZoomInButton.Enabled = True
            ZoomOutButton.Enabled = True
            ZoomFitButton.Enabled = True
            FindBox.Enabled = True

            PageBox.Text = "1"
            CountBox.Text = " of 1"

            contextBox.Text = ""
            beforeBox.Text = ""
            afterBox.Text = ""

        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveMenuItem.Click
        Try
            Using dlg As SaveFileDialog = New SaveFileDialog()
                dlg.Title = "Save PDF Document"
                dlg.InitialDirectory = openPath
                dlg.Filter = "PDF Document (*.PDF)|*.PDF"

                If (dlg.ShowDialog() = DialogResult.OK) Then
                    so.Filename = dlg.FileName
                    document.Save(so)
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolbarMenuItem.Click
        Try
            If (ToolStrip.Visible = True) Then
                ToolStrip.Visible = False
                ToolbarMenuItem.Checked = False
            Else
                ToolStrip.Visible = True
                ToolbarMenuItem.Checked = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusBarMenuItem.Click
        Try
            If (statusBarMain.Visible = True) Then
                statusBarMain.Visible = False
                StatusBarMenuItem.Checked = False
            Else
                statusBarMain.Visible = True
                StatusBarMenuItem.Checked = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenButton.Click
        Try
            mnuFileOpen_Click(sender, e)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Try
            mnuFileClose_Click(sender, e)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PreviousButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousButton.Click
        Try
            pageNumber -= 1

            UpdatePage()
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub NextButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextButton.Click
        Try
            pageNumber += 1

            UpdatePage()
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ZoomInButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomInButton.Click
        Try
            ImageXView.AutoResize = AutoResizeType.CropImage

            If (ImageXView.ZoomFactor < 10) Then
                ImageXView.ZoomFactor = ImageXView.ZoomFactor * 1.1
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ZoomOutButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomOutButton.Click
        Try
            ImageXView.AutoResize = AutoResizeType.CropImage

            If (ImageXView.ZoomFactor > 0.05) Then
                ImageXView.ZoomFactor = ImageXView.ZoomFactor * 0.95
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ZoomFitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomFitButton.Click
        Try
            ImageXView.ZoomToFit(ZoomToFitType.FitBest)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuViewOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ThumbMenuItem.Click
        Try
            Using dlg As OpenFileDialog = New OpenFileDialog()
                dlg.Title = "Choose an Image"
                dlg.Filter = "All Files (*.*) | *.*"
                dlg.InitialDirectory = openPath

                If (dlg.ShowDialog() = Windows.Forms.DialogResult.OK) Then

                    document.CreateThumbnail(pageNumber, dlg.FileName, 0)

                    document.Save(so)

                    MessageBox.Show("The PDF " + strFileName + " will now have the chosen thumbnail on page " + CType(pageNumber + 1, String))
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PageBox_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles PageBox.KeyUp
        Try
            If (e.KeyCode = Keys.Enter) Then
                Try
                    pageNumber = Convert.ToInt32(PageBox.Text) - 1

                    UpdatePage()
                Catch ex As FormatException
                    PageBox.Text = CInt(pageNumber + 1).ToString()
                    MessageBox.Show("Please enter a valid Page Number.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FindBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FindBox.KeyPress
        Try
            If (e.KeyChar = ChrW(Keys.Return)) Then
                If (FindBox.Text.Length > 0) Then

                    contextBox.Text = ""
                    beforeBox.Text = ""
                    afterBox.Text = ""

                    wordChosen = 0
                    PreviousTextButton.Enabled = False
                    NextTextButton.Enabled = False

                    'create a new TextFinderOptions object
                    Dim findOptions As TextFinderOptions = New TextFinderOptions()
                    findOptions.IgnoreTextAnnotations = False
                    findOptions.IgnoreTaggedPdfArtifacts = False

                    'create a new TextFinder
                    Using finder As TextFinder = document.GetTextFinder(pageNumber, findOptions)

                        'extract the text
                        Dim foundText As String = finder.GetText()

                        If (foundText.Length > 0) Then

                            Dim beginOffset As Int32 = 0
                            Dim i As Int32
                            For i = 0 To foundText.Length - 1

                                If (foundText.Substring(0, 1) = " ") Then
                                    foundText = foundText.Substring(1, foundText.Length - (i + 1))
                                    beginOffset = beginOffset + 1
                                Else
                                    GoTo nextLine
                                End If
                            Next i
nextLine:
                            'create an array for offsets
                            Dim offset As ArrayList = New ArrayList()

                            Dim j As Int32 = 0
                            Dim ii As Int32
                            For ii = 0 To foundText.Length - FindBox.Text.Length - 1

                                'search for the word inside the text
                                Dim offsetFound As Int32 = foundText.IndexOf(FindBox.Text, ii, FindBox.Text.Length, StringComparison.OrdinalIgnoreCase)

                                If Not (offsetFound = -1) Then
                                    'record the location of the offset
                                    offset.Add(offsetFound)
                                    j = j + 1
                                End If
                            Next ii

                            If (offset.Count > 0) Then
                                'create a new TextMatchOptions object
                                Dim matchOptions As TextMatchOptions

                                'set the sizes of the highlight and matched arrays based on the offset count
                                ReDim matched(offset.Count - 1)
                                ReDim highlight(offset.Count - 1)

                                Dim k As Int32
                                For k = 0 To offset.Count - 1

                                    'set the TextMatchOptions object
                                    matchOptions = New TextMatchOptions(beginOffset + CInt(offset(k)), _
                                    beginOffset + CInt(offset(k)) + FindBox.Text.Length, _
                                    Convert.ToInt32(beforeWordsBox.Text), _
                                    Convert.ToInt32(afterWordsBox.Text), wholeMenuItem.Checked)

                                    'set the TextMatch objects using GetTextMatch()
                                    matched(k) = finder.GetTextMatch(matchOptions)

                                    'record the top left and bottom right location of the highlight, convert from PDF units to pixels based on
                                    'resolution rendered at
                                    highlight(k) = New Rectangle(CInt((matched(k).BoundingQuadrilaterals(0).TopLeft.X * rendOpts.ResolutionX / 72)), _
                                    CInt((document.GetInfo(pageNumber).ClipHeight - matched(k).BoundingQuadrilaterals(0).TopLeft.Y) * rendOpts.ResolutionY / 72), _
                                    CInt((matched(k).BoundingQuadrilaterals(0).BottomRight.X - matched(k).BoundingQuadrilaterals(0).TopLeft.X) * rendOpts.ResolutionX / 72), _
                                    CInt(((document.GetInfo(pageNumber).MediaHeight - matched(k).BoundingQuadrilaterals(0).BottomRight.Y) - (document.GetInfo(pageNumber).MediaHeight - matched(k).BoundingQuadrilaterals(0).TopLeft.Y)) * (rendOpts.ResolutionY / 72)))
                                Next k

                                'if more then one match found on the page then enable the Next button
                                If (highlight.Length > 1) Then

                                    NextTextButton.Enabled = True
                                End If

                                wordChosen = 0

                                HighlightWord()

                            Else
                                MessageBox.Show("The specified text was not found.")
                            End If
                        Else
                            MessageBox.Show("Please enter what text to look for.")
                        End If
                    End Using
                Else
                    PreviousTextButton.Enabled = False
                    NextTextButton.Enabled = False
                End If
            End If
        Catch ex As PdfXpressException
            ImageXView.AllowUpdate = True
            MessageBox.Show("An error occured in the Text Finder, make sure this is a PDF that is text-searchable and not image-only.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub HighlightHelper(ByVal index As Integer)

        Dim border As RectangleTool = New RectangleTool()
        border.BackStyle = BackStyle.Translucent
        border.PenWidth = 0
        border.FillColor = Color.Yellow
        border.BoundingRectangle = highlight(wordChosen)

        If Not (NotateXpress1.Layers Is Nothing) Then
            If Not (NotateXpress1.Layers(index) Is Nothing) Then

                If Not (NotateXpress1.Layers(index).Elements Is Nothing) Then
                    NotateXpress1.Layers(index).Elements.Dispose()

                    NotateXpress1.Layers(index).Elements.Clear()
                    NotateXpress1.Layers(index).Elements.Add(border)

                    NotateXpress1.Layers(index).Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub HighlightWord()
        UpdatePage()

        ImageXView.AllowUpdate = False

        If Not (NotateXpress1.Layers Is Nothing) Then
            If (NotateXpress1.Layers.Count > 1) Then
                HighlightHelper(1)
            Else
                HighlightHelper(0)
            End If
        End If

        If (ImageXView.AutoResize = AutoResizeType.CropImage) Then
            ImageXView.ZoomFactor = 1.0
            ImageXView.ScrollPosition = New Point(highlight(wordChosen).Location.X + CInt(highlight(wordChosen).Width / 2) _
                                                 - CInt(ImageXView.Width / 2), highlight(wordChosen).Location.Y + _
                                                 CInt(highlight(wordChosen).Height / 2) - CInt(ImageXView.Height / 2))
        End If

        ImageXView.AllowUpdate = True

        Application.DoEvents()

        contextBox.Text = matched(wordChosen).MatchedText
        beforeBox.Text = matched(wordChosen).WordsBefore
        afterBox.Text = matched(wordChosen).WordsAfter

        NextTextButton.Enabled = False
        PreviousTextButton.Enabled = False

        If (wordChosen = highlight.Length - 1) Then
            NextTextButton.Enabled = False
        Else
            NextTextButton.Enabled = True
        End If
        If (wordChosen = 0) Then
            PreviousTextButton.Enabled = False
        Else
            PreviousTextButton.Enabled = True
        End If
    End Sub

    Private Sub AddImageMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddImageMenuItem.Click
        Try
            Using imgFrm As ImageForm = New ImageForm()

                imgFrm.DesXBox.Maximum = CInt(document.GetInfo(pageNumber).MediaWidth)
                imgFrm.DesWBox.Maximum = CInt(imgFrm.DesXBox.Maximum)
                imgFrm.DesYBox.Maximum = CInt(document.GetInfo(pageNumber).MediaHeight)
                imgFrm.DesHBox.Maximum = CInt(imgFrm.DesYBox.Maximum)
                imgFrm.DestinationX = desX
                imgFrm.DestinationY = desY
                imgFrm.DestinationW = desW
                imgFrm.DestinationH = desH

                If (imgFrm.ShowDialog() = DialogResult.OK) Then

                    Select Case (imgFrm.Settings)
                        Case 0
                            destinationFit = ImageFitSettings.None
                        Case 1
                            destinationFit = ImageFitSettings.Stretch
                        Case 2
                            destinationFit = ImageFitSettings.Shrink
                        Case 3
                            destinationFit = ImageFitSettings.Grow
                        Case 4
                            destinationFit = ImageFitSettings.GravityLeft
                        Case 5
                            destinationFit = ImageFitSettings.GravityTop
                        Case 6
                            destinationFit = ImageFitSettings.GravityRight
                        Case 7
                            destinationFit = ImageFitSettings.GravityBottom
                    End Select

                    desX = imgFrm.DestinationX
                    desY = imgFrm.DestinationY
                    desW = imgFrm.DestinationW
                    desH = imgFrm.DestinationH

                    document.AddImage(pageNumber, imgFrm.DestinationX, document.GetInfo(pageNumber).MediaHeight _
                                          - imgFrm.DestinationH - imgFrm.DestinationY, destinationW, imgFrm.DestinationH, _
                                          destinationFit, imgFrm.Image, 0)
                    UpdatePage()
                End If

            End Using

        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PreviousTextButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousTextButton.Click
        Try
            wordChosen = wordChosen - 1

            HighlightWord()
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        Try
            If Not (e.Node.Name = "") Then
                If (Int32.Parse(e.Node.Name) > -1) Then

                    pageNumber = Int32.Parse(e.Node.Name)

                    nodeSelected = e.Node

                    UpdatePage()
                End If
            End If
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FormMain_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            If Not (pdfxpress1 Is Nothing) Then
                pdfxpress1.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles editBookmarksToolStripMenuItem.Click
        Dim bookmarkUsed As BookmarkContext = Nothing

        Try

            Using bookForm As BookmarkForm = New BookmarkForm(pageCount, TreeView1)

                If (bookForm.ShowDialog() = DialogResult.OK) Then
                    Dim j As Integer

                    For j = 0 To bookmarks.Count - 1

                        If Not (CType(bookmarks(j), BookmarkContext) Is Nothing) Then

                            If ((CType(bookmarks(j), BookmarkContext).ActionPageNumber.ToString() _
                                = TreeView1.Nodes(TreeView1.Nodes.Count - 1).Name _
                               And (CType(bookmarks(j), BookmarkContext).Title + " (page " & _
                            CInt(CType(bookmarks(j), BookmarkContext).ActionPageNumber + 1).ToString() + ")" _
                                = TreeView1.Nodes(TreeView1.Nodes.Count - 1).Text))) Then
                                bookmarkUsed = CType(bookmarks(j), BookmarkContext)
                                Exit For
                            End If
                        End If
                    Next j

                    Dim i As Integer
                    For i = 0 To bookForm.BookmarksAddedPageNumberProperty.Count - 1

                        If Not (bookmarkUsed Is Nothing) Then

                            document.AddBookmarkSibling(bookmarkUsed, _
                                bookForm.BookmarksAddedTitleProperty(i).ToString(), CInt(bookForm.BookmarksAddedPageNumberProperty(i)))
                        Else

                            bookmarkUsed = document.GetBookmarkRoot()
                            document.AddBookmarkChild(bookmarkUsed, _
                                    bookForm.BookmarksAddedTitleProperty(i).ToString(), CInt(bookForm.BookmarksAddedPageNumberProperty(i)))
                            bookmarks.Add(document.GetBookmarkChild(bookmarkUsed))

                            ImageLoad()

                        End If

                    Next i

                    For i = 0 To bookForm.BookmarksDeletedTitleProperty.Count - 1
                        For j = 0 To bookmarks.Count - 1
                            If Not (CType(bookmarks(j), BookmarkContext) Is Nothing) Then
                                If ((CType(bookmarks(j), BookmarkContext)).ActionPageNumber.ToString() _
                                    = bookForm.BookmarksDeletedPageNumberProperty(i).ToString() _
                                    And (CType(bookmarks(j), BookmarkContext).Title & " (page " & _
                                   ((CType(bookmarks(j), BookmarkContext).ActionPageNumber + 1)).ToString() + ")" _
                                    = bookForm.BookmarksDeletedTitleProperty(i).ToString())) Then

                                    document.RemoveBookmark(CType(bookmarks(j), BookmarkContext))

                                    InitializeBookmarkTree()

                                    Exit For
                                End If
                            End If
                        Next j
                    Next i

                    ImageLoad()
                End If
            End Using

        Catch ex As Exception

            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deleteThumbnailToolStripMenuItem.Click
        Try
            document.DeleteThumbnail(pageNumber)
            document.Save(so)
            MessageBox.Show("The PDF " + strFileName + " will have its thumbnail on page " + CType(pageNumber + 1, String) + " deleted.")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MenuItem3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles insertPageToolStripMenuItem.Click
        Try
            Using insertForm As InsertForm = New InsertForm()
                If (insertForm.ShowDialog() = Windows.Forms.DialogResult.OK) Then

                    Dim ipo As InsertPagesOptions = New InsertPagesOptions()

                    ipo.IncludeAllDocumentData = insertForm.IncludeAllData
                    ipo.IncludeArticleThreads = insertForm.IncludeArticles
                    ipo.IncludeSourceBookmarks = insertForm.IncludeBookmarks
                    ipo.InsertAtPageNumber = insertForm.InsertPageNumber - 1

                    Using doc As Document = New Document(pdfxpress1, insertForm.SourceFile)

                        ipo.SourceDocument = doc

                        Dim pr As PageRange = New PageRange()

                        pr.PageCount = insertForm.PageCount
                        pr.StartPageNumber = insertForm.StartPage
                        pr.UseAllPages = insertForm.UseAllPages

                        Dim pl As PageList = New PageList()

                        pl.Add(pr)

                        ipo.PageList = pl

                        document.InsertPages(ipo)

                    End Using

                    ImageLoad()
                End If
            End Using
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ImageLoad()
        pageCount = document.PageCount

        If (pageCount > 1) Then
            NextButton.Enabled = True
            PageBox.Enabled = True
        End If

        CountBox.Text = "of " + pageCount.ToString()

        pageNumber = 0

        PageBox.Text = CType(pageNumber + 1, String)

        groupBoxMain.Text = openFileDialog.FileName

        ZoomInButton.Enabled = True
        ZoomOutButton.Enabled = True
        ZoomFitButton.Enabled = True
        FindBox.Enabled = True
        AddImageMenuItem.Enabled = True
        ThumbMenuItem.Enabled = True
        CloseButton.Enabled = True

        CloseMenuItem.Enabled = True
        RenderMenuItem.Enabled = True
        SaveMenuItem.Enabled = True
        FindBox.Text = "Find Text"
        PreviousTextButton.Enabled = False
        NextTextButton.Enabled = False
        metaDataToolStripMenuItem.Enabled = True
        deleteThumbnailToolStripMenuItem.Enabled = True
        insertPageToolStripMenuItem.Enabled = True
        movePageToolStripMenuItem.Enabled = True
        editBookmarksToolStripMenuItem.Enabled = True

        destinationW = CType(document.GetInfo(pageNumber).MediaWidth - 0.5 * 72, Integer)
        destinationH = CType(document.GetInfo(pageNumber).MediaHeight - 0.5 * 72, Integer)

        so.Filename = strFileName

        InitializeBookmarkTree()

        afterBox.Text = ""
        beforeBox.Text = ""
        contextBox.Text = ""

        If (useXFDF = True) Then
            xfdfData = document.ExportXfdf(xfdfOptions)
        End If

        UpdatePage()
    End Sub

    Private Sub RendForm()
        Using rendForm As RenderForm = New RenderForm()

            rendForm.Annotations = rendOpts.RenderAnnotations
            rendForm.ReduceBitonal = rendOpts.ReduceToBitonal
            rendForm.ResolutionX = rendOpts.ResolutionX
            rendForm.ResolutionY = rendOpts.ResolutionY
            rendForm.SelectionX = rendOpts.SelectionX
            rendForm.SelectionY = rendOpts.SelectionY
            rendForm.SelectionW = rendOpts.SelectionWidth
            rendForm.SelectionH = rendOpts.SelectionHeight

            If (rendForm.ShowDialog() = DialogResult.OK) Then

                rendOpts.RenderAnnotations = rendForm.Annotations
                rendOpts.ProduceDibSection = False
                rendOpts.ReduceToBitonal = rendForm.ReduceBitonal
                rendOpts.ResolutionX = rendForm.ResolutionX
                rendOpts.ResolutionY = rendForm.ResolutionY
                rendOpts.SelectionX = rendForm.SelectionX
                rendOpts.SelectionY = rendForm.SelectionY
                rendOpts.SelectionWidth = rendForm.SelectionW
                rendOpts.SelectionHeight = rendForm.SelectionH

                useXFDF = rendForm.XFDF

                If (useXFDF = True) Then
                    xfdfData = document.ExportXfdf(xfdfOptions)
                End If

                UpdatePage()

                ImageLoad()
            End If
        End Using
    End Sub

    Private Sub UpdatePage()
        Using bp As Bitmap = document.RenderPageToBitmap(pageNumber, rendOpts)

            ImageXView.AllowUpdate = False

            'resource cleanup
            If Not (ImageXView.Image Is Nothing) Then
                ImageXView.Image.Dispose()
                ImageXView.Image = Nothing
            End If

            ImageXView.Image = ImageX.FromBitmap(ImagXpress1, bp)

            ImageXView.Image.Resolution.Dimensions = New SizeF(CType(rendOpts.ResolutionX, Single), CType(rendOpts.ResolutionY, Single))

            If Not (NotateXpress1.Layers Is Nothing) Then
                Dim i As Integer
                For i = 0 To NotateXpress1.Layers.Count - 1
                    If Not (NotateXpress1.Layers(i) Is Nothing) Then
                        If Not (NotateXpress1.Layers(i).Elements Is Nothing) Then
                            NotateXpress1.Layers(i).Elements.Dispose()

                            NotateXpress1.Layers(i).Elements.Clear()
                        End If
                    End If
                Next i
            End If

            If (useXFDF = True) Then
                Try
                    NotateXpress1.PageNumber = pageNumber + 1
                    NotateXpress1.Layers.FromByteArray(xfdfData, lo)
                    NotateXpress1.Layers.Add(New Layer())
                Catch ex As PageNotFoundException

                End Try
            End If

            desW = CInt(document.GetInfo(pageNumber).MediaWidth)
            desH = CInt(document.GetInfo(pageNumber).MediaHeight)
        End Using

        ImageXView.AllowUpdate = True

        If (pageNumber = 0) Then
            PreviousButton.Enabled = False
        Else
            PreviousButton.Enabled = True
        End If

        If (pageCount > 1) Then
            If (pageNumber = pageCount - 1) Then
                NextButton.Enabled = False
            Else
                NextButton.Enabled = True
            End If
        End If

        PageBox.Text = Convert.ToString(pageNumber + 1)

        PreviousTextButton.Enabled = False
        NextTextButton.Enabled = False

        afterBox.Text = ""
        beforeBox.Text = ""
        contextBox.Text = ""
    End Sub

    Private Sub metaDataToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles metaDataToolStripMenuItem.Click
        Try
            Using metaForm As MetaDataForm = New MetaDataForm()

                metaForm.Author = document.DocumentMetadata.Author
                metaForm.CreationDate = document.DocumentMetadata.CreationDate
                metaForm.Creator = document.DocumentMetadata.Creator
                metaForm.Keywords = document.DocumentMetadata.Keywords
                metaForm.ModificationDate = document.DocumentMetadata.ModificationDate
                metaForm.Producer = document.DocumentMetadata.Producer
                metaForm.Title = document.DocumentMetadata.Title
                metaForm.Subject = document.DocumentMetadata.Subject

                If (metaForm.ShowDialog() = DialogResult.OK) Then
                    document.DocumentMetadata.Author = metaForm.Author
                    document.DocumentMetadata.Creator = metaForm.Creator
                    document.DocumentMetadata.Keywords = metaForm.Keywords
                    document.DocumentMetadata.Producer = metaForm.Producer
                    document.DocumentMetadata.Title = metaForm.Title
                    document.DocumentMetadata.Subject = metaForm.Subject
                    document.DocumentMetadata.Keywords = metaForm.Keywords
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub movePageToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles movePageToolStripMenuItem.Click
        Try
            Using moveForm As MoveForm = New MoveForm()
                If (moveForm.ShowDialog() = DialogResult.OK) Then

                    If (moveForm.MoveTo = 1) Then
                        document.MovePage(moveForm.MoveFrom - 1, 0)
                    Else
                        document.MovePage(moveForm.MoveFrom - 1, moveForm.MoveTo)
                    End If
                    ImageLoad()
                End If
            End Using
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RenderMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RenderMenuItem.Click
        Try
            counter = 1

            RendForm()
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FindBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FindBox.Click
        FindBox.SelectAll()
    End Sub

    Private Sub NextTextButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextTextButton.Click
        Try
            wordChosen = wordChosen + 1

            HighlightWord()
        Catch ex As Exception
            ImageXView.AllowUpdate = True
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class

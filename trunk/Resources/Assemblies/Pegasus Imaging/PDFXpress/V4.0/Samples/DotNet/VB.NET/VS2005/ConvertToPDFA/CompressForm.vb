'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
Option Explicit On

Imports System.Windows.Forms

Public Class CompressForm

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button1.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button2.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Public ReadOnly Property SelectedCompression() As ConvertToPDFA.Compression
        Get
            If Jbig2RadioButton.Checked = True Then
                Return ConvertToPDFA.Compression.JBIG2
            ElseIf JpegRadioButton.Checked = True Then
                Return ConvertToPDFA.Compression.JPEG
            ElseIf G4RadioButton.Checked = True Then
                Return ConvertToPDFA.Compression.G4
            End If
        End Get
    End Property
End Class

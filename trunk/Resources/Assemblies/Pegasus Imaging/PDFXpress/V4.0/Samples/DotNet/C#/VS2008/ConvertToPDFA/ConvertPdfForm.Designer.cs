namespace ConvertToPDFA
{
    partial class ConvertPDFForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToPDFAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePDFAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadPDFButton = new System.Windows.Forms.Button();
            this.loadLabel = new System.Windows.Forms.Label();
            this.convertToPDFAButton = new System.Windows.Forms.Button();
            this.savePDFAButton = new System.Windows.Forms.Button();
            this.saveLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.convertLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(438, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadPDFToolStripMenuItem,
            this.convertToPDFAToolStripMenuItem,
            this.savePDFAToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadPDFToolStripMenuItem
            // 
            this.loadPDFToolStripMenuItem.Name = "loadPDFToolStripMenuItem";
            this.loadPDFToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.loadPDFToolStripMenuItem.Text = "Load Document...";
            this.loadPDFToolStripMenuItem.Click += new System.EventHandler(this.loadPDFToolStripMenuItem_Click);
            // 
            // convertToPDFAToolStripMenuItem
            // 
            this.convertToPDFAToolStripMenuItem.Name = "convertToPDFAToolStripMenuItem";
            this.convertToPDFAToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.convertToPDFAToolStripMenuItem.Text = "Convert to PDF/A";
            this.convertToPDFAToolStripMenuItem.Click += new System.EventHandler(this.convertToPDFAToolStripMenuItem_Click);
            // 
            // savePDFAToolStripMenuItem
            // 
            this.savePDFAToolStripMenuItem.Name = "savePDFAToolStripMenuItem";
            this.savePDFAToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.savePDFAToolStripMenuItem.Text = "Save PDF/A As...";
            this.savePDFAToolStripMenuItem.Click += new System.EventHandler(this.savePDFAToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // loadPDFButton
            // 
            this.loadPDFButton.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadPDFButton.Location = new System.Drawing.Point(26, 89);
            this.loadPDFButton.Name = "loadPDFButton";
            this.loadPDFButton.Size = new System.Drawing.Size(123, 35);
            this.loadPDFButton.TabIndex = 1;
            this.loadPDFButton.Text = "Load Document...";
            this.loadPDFButton.UseVisualStyleBackColor = true;
            this.loadPDFButton.Click += new System.EventHandler(this.loadPDFButton_Click);
            // 
            // loadLabel
            // 
            this.loadLabel.AutoSize = true;
            this.loadLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadLabel.Location = new System.Drawing.Point(155, 100);
            this.loadLabel.Name = "loadLabel";
            this.loadLabel.Size = new System.Drawing.Size(159, 13);
            this.loadLabel.TabIndex = 2;
            this.loadLabel.Text = "Document not currently loaded.";
            // 
            // convertToPDFAButton
            // 
            this.convertToPDFAButton.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertToPDFAButton.Location = new System.Drawing.Point(26, 148);
            this.convertToPDFAButton.Name = "convertToPDFAButton";
            this.convertToPDFAButton.Size = new System.Drawing.Size(123, 35);
            this.convertToPDFAButton.TabIndex = 3;
            this.convertToPDFAButton.Text = "Convert to PDF/A";
            this.convertToPDFAButton.UseVisualStyleBackColor = true;
            this.convertToPDFAButton.Click += new System.EventHandler(this.convertToPDFAButton_Click);
            // 
            // savePDFAButton
            // 
            this.savePDFAButton.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savePDFAButton.Location = new System.Drawing.Point(26, 209);
            this.savePDFAButton.Name = "savePDFAButton";
            this.savePDFAButton.Size = new System.Drawing.Size(123, 35);
            this.savePDFAButton.TabIndex = 4;
            this.savePDFAButton.Text = "Save PDF/A As...";
            this.savePDFAButton.UseVisualStyleBackColor = true;
            this.savePDFAButton.Click += new System.EventHandler(this.savePDFAButton_Click);
            // 
            // saveLabel
            // 
            this.saveLabel.AutoSize = true;
            this.saveLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveLabel.Location = new System.Drawing.Point(155, 220);
            this.saveLabel.Name = "saveLabel";
            this.saveLabel.Size = new System.Drawing.Size(111, 13);
            this.saveLabel.TabIndex = 5;
            this.saveLabel.Text = "PDF/A not yet saved.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Info;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(427, 28);
            this.label3.TabIndex = 7;
            this.label3.Text = "Using PDF Xpress v4, developers can now render PDF pages to image-only PDF/A - 1b" +
                " \r\ndocuments. PDF Xpress will provide details of failed compliance rules during " +
                "conversion.";
            // 
            // convertLabel
            // 
            this.convertLabel.AutoSize = true;
            this.convertLabel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertLabel.Location = new System.Drawing.Point(155, 159);
            this.convertLabel.Name = "convertLabel";
            this.convertLabel.Size = new System.Drawing.Size(187, 13);
            this.convertLabel.TabIndex = 8;
            this.convertLabel.Text = "Please load document for conversion.";
            // 
            // ConvertPDFForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 266);
            this.Controls.Add(this.convertLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.saveLabel);
            this.Controls.Add(this.savePDFAButton);
            this.Controls.Add(this.convertToPDFAButton);
            this.Controls.Add(this.loadLabel);
            this.Controls.Add(this.loadPDFButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ConvertPDFForm";
            this.Text = "PDFXpress4 PDF/A conversion";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.convertPdfForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePDFAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button loadPDFButton;
        private System.Windows.Forms.Label loadLabel;
        private System.Windows.Forms.Button convertToPDFAButton;
        private System.Windows.Forms.Button savePDFAButton;
        private System.Windows.Forms.Label saveLabel;
        private System.Windows.Forms.ToolStripMenuItem convertToPDFAToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label convertLabel;
    }
}


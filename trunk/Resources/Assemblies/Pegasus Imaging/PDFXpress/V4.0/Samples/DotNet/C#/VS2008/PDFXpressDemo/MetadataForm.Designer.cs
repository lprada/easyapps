/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class MetaDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MetaDataForm));
            this.SubjectLabel = new System.Windows.Forms.Label();
            this.SubjectTextBox = new System.Windows.Forms.TextBox();
            this.AuthorTextBox = new System.Windows.Forms.TextBox();
            this.AuthorLabel = new System.Windows.Forms.Label();
            this.CreationDateTextBox = new System.Windows.Forms.TextBox();
            this.CreationDateLabel = new System.Windows.Forms.Label();
            this.CreatorTextBox = new System.Windows.Forms.TextBox();
            this.CreatorLabel = new System.Windows.Forms.Label();
            this.TitleTextBox = new System.Windows.Forms.TextBox();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.ModificationDatetextBox = new System.Windows.Forms.TextBox();
            this.ModificationDateLabel = new System.Windows.Forms.Label();
            this.ProducerTextBox = new System.Windows.Forms.TextBox();
            this.ProducerLabel = new System.Windows.Forms.Label();
            this.KeywordsTextBox = new System.Windows.Forms.TextBox();
            this.KeywordsLabel = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelMetaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SubjectLabel
            // 
            this.SubjectLabel.AutoSize = true;
            this.SubjectLabel.Location = new System.Drawing.Point(12, 19);
            this.SubjectLabel.Name = "SubjectLabel";
            this.SubjectLabel.Size = new System.Drawing.Size(43, 13);
            this.SubjectLabel.TabIndex = 0;
            this.SubjectLabel.Text = "Subject";
            // 
            // SubjectTextBox
            // 
            this.SubjectTextBox.Location = new System.Drawing.Point(116, 12);
            this.SubjectTextBox.Name = "SubjectTextBox";
            this.SubjectTextBox.Size = new System.Drawing.Size(366, 20);
            this.SubjectTextBox.TabIndex = 1;
            // 
            // AuthorTextBox
            // 
            this.AuthorTextBox.Location = new System.Drawing.Point(116, 38);
            this.AuthorTextBox.Name = "AuthorTextBox";
            this.AuthorTextBox.Size = new System.Drawing.Size(366, 20);
            this.AuthorTextBox.TabIndex = 3;
            // 
            // AuthorLabel
            // 
            this.AuthorLabel.AutoSize = true;
            this.AuthorLabel.Location = new System.Drawing.Point(12, 45);
            this.AuthorLabel.Name = "AuthorLabel";
            this.AuthorLabel.Size = new System.Drawing.Size(38, 13);
            this.AuthorLabel.TabIndex = 2;
            this.AuthorLabel.Text = "Author";
            // 
            // CreationDateTextBox
            // 
            this.CreationDateTextBox.Location = new System.Drawing.Point(116, 90);
            this.CreationDateTextBox.Name = "CreationDateTextBox";
            this.CreationDateTextBox.ReadOnly = true;
            this.CreationDateTextBox.Size = new System.Drawing.Size(366, 20);
            this.CreationDateTextBox.TabIndex = 7;
            // 
            // CreationDateLabel
            // 
            this.CreationDateLabel.AutoSize = true;
            this.CreationDateLabel.Location = new System.Drawing.Point(12, 97);
            this.CreationDateLabel.Name = "CreationDateLabel";
            this.CreationDateLabel.Size = new System.Drawing.Size(72, 13);
            this.CreationDateLabel.TabIndex = 6;
            this.CreationDateLabel.Text = "Creation Date";
            // 
            // CreatorTextBox
            // 
            this.CreatorTextBox.Location = new System.Drawing.Point(116, 64);
            this.CreatorTextBox.Name = "CreatorTextBox";
            this.CreatorTextBox.Size = new System.Drawing.Size(366, 20);
            this.CreatorTextBox.TabIndex = 5;
            // 
            // CreatorLabel
            // 
            this.CreatorLabel.AutoSize = true;
            this.CreatorLabel.Location = new System.Drawing.Point(12, 71);
            this.CreatorLabel.Name = "CreatorLabel";
            this.CreatorLabel.Size = new System.Drawing.Size(41, 13);
            this.CreatorLabel.TabIndex = 4;
            this.CreatorLabel.Text = "Creator";
            // 
            // TitleTextBox
            // 
            this.TitleTextBox.Location = new System.Drawing.Point(116, 194);
            this.TitleTextBox.Name = "TitleTextBox";
            this.TitleTextBox.Size = new System.Drawing.Size(366, 20);
            this.TitleTextBox.TabIndex = 15;
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Location = new System.Drawing.Point(12, 201);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(27, 13);
            this.TitleLabel.TabIndex = 14;
            this.TitleLabel.Text = "Title";
            // 
            // ModificationDatetextBox
            // 
            this.ModificationDatetextBox.Location = new System.Drawing.Point(116, 168);
            this.ModificationDatetextBox.Name = "ModificationDatetextBox";
            this.ModificationDatetextBox.ReadOnly = true;
            this.ModificationDatetextBox.Size = new System.Drawing.Size(366, 20);
            this.ModificationDatetextBox.TabIndex = 13;
            // 
            // ModificationDateLabel
            // 
            this.ModificationDateLabel.AutoSize = true;
            this.ModificationDateLabel.Location = new System.Drawing.Point(12, 175);
            this.ModificationDateLabel.Name = "ModificationDateLabel";
            this.ModificationDateLabel.Size = new System.Drawing.Size(90, 13);
            this.ModificationDateLabel.TabIndex = 12;
            this.ModificationDateLabel.Text = "Modification Date";
            // 
            // ProducerTextBox
            // 
            this.ProducerTextBox.Location = new System.Drawing.Point(116, 142);
            this.ProducerTextBox.Name = "ProducerTextBox";
            this.ProducerTextBox.Size = new System.Drawing.Size(366, 20);
            this.ProducerTextBox.TabIndex = 11;
            // 
            // ProducerLabel
            // 
            this.ProducerLabel.AutoSize = true;
            this.ProducerLabel.Location = new System.Drawing.Point(12, 149);
            this.ProducerLabel.Name = "ProducerLabel";
            this.ProducerLabel.Size = new System.Drawing.Size(50, 13);
            this.ProducerLabel.TabIndex = 10;
            this.ProducerLabel.Text = "Producer";
            // 
            // KeywordsTextBox
            // 
            this.KeywordsTextBox.Location = new System.Drawing.Point(116, 116);
            this.KeywordsTextBox.Name = "KeywordsTextBox";
            this.KeywordsTextBox.Size = new System.Drawing.Size(366, 20);
            this.KeywordsTextBox.TabIndex = 9;
            // 
            // KeywordsLabel
            // 
            this.KeywordsLabel.AutoSize = true;
            this.KeywordsLabel.Location = new System.Drawing.Point(12, 123);
            this.KeywordsLabel.Name = "KeywordsLabel";
            this.KeywordsLabel.Size = new System.Drawing.Size(53, 13);
            this.KeywordsLabel.TabIndex = 8;
            this.KeywordsLabel.Text = "Keywords";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(15, 234);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 16;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelMetaButton
            // 
            this.CancelMetaButton.Location = new System.Drawing.Point(401, 234);
            this.CancelMetaButton.Name = "CancelMetaButton";
            this.CancelMetaButton.Size = new System.Drawing.Size(75, 23);
            this.CancelMetaButton.TabIndex = 17;
            this.CancelMetaButton.Text = "Cancel";
            this.CancelMetaButton.UseVisualStyleBackColor = true;
            this.CancelMetaButton.Click += new System.EventHandler(this.CancelMetaButton_Click);
            // 
            // MetaDataForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 269);
            this.Controls.Add(this.CancelMetaButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.TitleTextBox);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.ModificationDatetextBox);
            this.Controls.Add(this.ModificationDateLabel);
            this.Controls.Add(this.ProducerTextBox);
            this.Controls.Add(this.ProducerLabel);
            this.Controls.Add(this.KeywordsTextBox);
            this.Controls.Add(this.KeywordsLabel);
            this.Controls.Add(this.CreationDateTextBox);
            this.Controls.Add(this.CreationDateLabel);
            this.Controls.Add(this.CreatorTextBox);
            this.Controls.Add(this.CreatorLabel);
            this.Controls.Add(this.AuthorTextBox);
            this.Controls.Add(this.AuthorLabel);
            this.Controls.Add(this.SubjectTextBox);
            this.Controls.Add(this.SubjectLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MetaDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Metadata";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SubjectLabel;
        private System.Windows.Forms.TextBox SubjectTextBox;
        private System.Windows.Forms.TextBox AuthorTextBox;
        private System.Windows.Forms.Label AuthorLabel;
        private System.Windows.Forms.TextBox CreationDateTextBox;
        private System.Windows.Forms.Label CreationDateLabel;
        private System.Windows.Forms.TextBox CreatorTextBox;
        private System.Windows.Forms.Label CreatorLabel;
        private System.Windows.Forms.TextBox TitleTextBox;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.TextBox ModificationDatetextBox;
        private System.Windows.Forms.Label ModificationDateLabel;
        private System.Windows.Forms.TextBox ProducerTextBox;
        private System.Windows.Forms.Label ProducerLabel;
        private System.Windows.Forms.TextBox KeywordsTextBox;
        private System.Windows.Forms.Label KeywordsLabel;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelMetaButton;
    }
}
/// Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;

    public partial class ZoomToForm : Form
    {
        
        internal struct DisplayValuePair< TDisplay , TValue >
        {
            public DisplayValuePair( TDisplay display , TValue value )
            {
                this._display = display;
                this._value = value;
            }
            private TDisplay _display;
            public TDisplay Display
            {
                get { return this._display; }
                set { this._display = value; }
            }
            private TValue _value ;
            public TValue Value
            {
                get { return this._value; }
                set { this._value = value; }
            }          
        }

        public ZoomToForm( )
        {
            InitializeComponent( );

            IList< DisplayValuePair< String , Magnification > > magnificationValueList
                = new DisplayValuePair<String , Magnification>[ ] 
                    { new DisplayValuePair< String , Magnification >( "Fit Page"    , Magnification.ZoomFitPage )
                    , new DisplayValuePair< String , Magnification >( "Fit Height"  , Magnification.ZoomFitHeight )
                    , new DisplayValuePair< String , Magnification >( "Fit Width"   , Magnification.ZoomFitWidth )
                    , new DisplayValuePair< String , Magnification >( "Actual size" , Magnification.ZoomActualSize )
                    , new DisplayValuePair< String , Magnification >( "10 %"        , Magnification.Zoom10Percent )
                    , new DisplayValuePair< String , Magnification >( "25 %"        , Magnification.Zoom25Percent )
                    , new DisplayValuePair< String , Magnification >( "50 %"        , Magnification.Zoom50Percent )
                    , new DisplayValuePair< String , Magnification >( "75 %"        , Magnification.Zoom75Percent )
                    , new DisplayValuePair< String , Magnification >( "100 %"       , Magnification.Zoom100Percent )
                    , new DisplayValuePair< String , Magnification >( "125 %"       , Magnification.Zoom125Percent )
                    , new DisplayValuePair< String , Magnification >( "175 %"       , Magnification.Zoom150Percent )
                    , new DisplayValuePair< String , Magnification >( "200 %"       , Magnification.Zoom200Percent )
                    , new DisplayValuePair< String , Magnification >( "400 %"       , Magnification.Zoom400Percent )
                    , new DisplayValuePair< String , Magnification >( "800 %"       , Magnification.Zoom800Percent )
                    , new DisplayValuePair< String , Magnification >( "1600 %"      , Magnification.Zoom1600Percent )
                    , new DisplayValuePair< String , Magnification >( "3200 %"      , Magnification.Zoom3200Percent )
                    , new DisplayValuePair< String , Magnification >( "6400 %"      , Magnification.Zoom6400Percent )
                    , new DisplayValuePair< String , Magnification >( "9000 %"      , Magnification.Zoom9000Percent )
                    } ;
            this._magnificationComboBox.DataSource      = magnificationValueList ;
            this._magnificationComboBox.DisplayMember   = "Display" ; 
            this._magnificationComboBox.ValueMember     = "Value" ;
        }

        public enum Magnification
        {
            ZoomFitPage = -3 ,
            ZoomFitHeight = -2 ,
            ZoomFitWidth = -1 ,
            ZoomActualSize = 100 ,
            Zoom10Percent = 10 ,
            Zoom25Percent = 25 ,
            Zoom50Percent = 50 ,
            Zoom75Percent = 75 ,
            Zoom100Percent = 100 ,
            Zoom125Percent = 125 ,
            Zoom150Percent = 150 ,
            Zoom175Percent = 175 ,
            Zoom200Percent = 200 ,
            Zoom400Percent = 400 ,
            Zoom800Percent = 800 ,
            Zoom1600Percent = 1600 ,
            Zoom3200Percent = 3200 ,
            Zoom6400Percent = 6400 ,
            Zoom9000Percent = 9000 ,
        };

        public Magnification ZoomTo
        {
            get { return ( Magnification )this._magnificationComboBox.SelectedValue ; }
            set { this._magnificationComboBox.SelectedValue = value; }
        }
    }
}
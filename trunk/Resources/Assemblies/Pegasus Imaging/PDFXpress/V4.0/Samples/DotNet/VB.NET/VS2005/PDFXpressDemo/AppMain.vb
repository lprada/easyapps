'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Module AppMain
    Sub Main()
        Application.EnableVisualStyles()

        Using formSplash As FormSplash = New FormSplash()
            formSplash.ShowDialog()
        End Using

        Application.Run(New FormMain())
    End Sub
End Module

'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Windows.Forms

Namespace ConvertToPDFA
    Enum ConversionSource
        FromTiffImage
        FromPDFDocument
    End Enum

    Public Enum Compression
        JPEG
        JBIG2
        G4
    End Enum

    Partial Public Class ConvertPDFForm
        Inherits Form
        Private pdfXpress As Accusoft.PdfXpressSdk.PdfXpress = Nothing
        Private originalImage As Accusoft.ImagXpressSdk.ImageX = Nothing
        Private imageOverTextPDFA As Accusoft.PdfXpressSdk.Document = Nothing
        Private imagXpress As Accusoft.ImagXpressSdk.ImagXpress = Nothing
        Private originalFormat As ConversionSource
        Private cmapFolder As String = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\PdfXpress\v4.0\Support\CMap\")
        Private fontFolder As String = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\PdfXpress\v4.0\Support\Font\")
        Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
        Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents LoadDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents convertToPDFAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents savePDFAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
        Friend WithEvents Label1 As System.Windows.Forms.Label
        Friend WithEvents Button1 As System.Windows.Forms.Button
        Friend WithEvents convertToPDFAButton As System.Windows.Forms.Button
        Friend WithEvents savePDFAButton As System.Windows.Forms.Button
        Friend WithEvents loadLabel As System.Windows.Forms.Label
        Friend WithEvents convertLabel As System.Windows.Forms.Label
        Friend WithEvents saveLabel As System.Windows.Forms.Label

        Public Sub New()
            InitializeComponent()
            pdfXpress = New Accusoft.PdfXpressSdk.PdfXpress()
            imagXpress = New Accusoft.ImagXpressSdk.ImagXpress()
            pdfXpress.Initialize(fontFolder, cmapFolder)
            SetSaveEnabled(False)
            SetConvertEnabled(False)
            'pdfXpress.Licensing.UnlockRuntime(1, 2, 3, 4);
        End Sub

        Private Sub SetConvertEnabled(ByVal enabled As Boolean)
            convertToPDFAButton.Enabled = enabled
            convertToPDFAToolStripMenuItem.Enabled = enabled
        End Sub

        Private Sub SetSaveEnabled(ByVal enabled As Boolean)
            savePDFAButton.Enabled = enabled
            savePDFAToolStripMenuItem.Enabled = enabled
        End Sub

        Private Sub exitGracefully()
            If imageOverTextPDFA IsNot Nothing Then
                imageOverTextPDFA.Dispose()
            End If
            pdfXpress.Documents.Clear()
            If pdfXpress IsNot Nothing Then
                pdfXpress.Dispose()
            End If
            Application.[Exit]()
        End Sub

        Private Sub loadUserSelectedDocument()
            Dim loadDialog As New System.Windows.Forms.OpenFileDialog()
            Dim pdfDocument As Accusoft.PdfXpressSdk.Document = Nothing

            loadDialog.Filter = "PDF documents (*.pdf)|*.pdf|Multi-page TIFF images (*.tif)|*.tif"
            loadDialog.FilterIndex = 1
            Try
                If loadDialog.ShowDialog() = DialogResult.OK Then
                    If loadDialog.FilterIndex = 1 Then
                        pdfDocument = New Accusoft.PdfXpressSdk.Document(pdfXpress, loadDialog.FileName)
                        loadLabel.Text = System.[String].Format("{0} loaded, {1} pages", loadDialog.FileName, pdfDocument.PageCount)
                        convertLabel.Text = "Document not yet converted."
                        pdfXpress.Documents.Clear()
                        pdfXpress.Documents.Add(pdfDocument)
                        SetConvertEnabled(True)
                        originalFormat = ConversionSource.FromPDFDocument
                    ElseIf loadDialog.FilterIndex = 2 Then
                        originalImage = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress, loadDialog.FileName)
                        loadLabel.Text = System.[String].Format("{0} loaded, {1} pages", loadDialog.FileName, originalImage.PageCount)
                        convertLabel.Text = "Document not yet converted."
                        SetConvertEnabled(True)
                        originalFormat = ConversionSource.FromTiffImage
                    End If
                End If
            Catch e As Exception
                loadLabel.Text = "Error loading PDF: " & e.Message
                SetConvertEnabled(False)
                If pdfDocument IsNot Nothing Then
                    pdfDocument.Dispose()
                End If
            End Try
        End Sub

        Private Sub convertUserSelectedDocument()
            Dim cf As New CompressForm()
            Dim compressionType As Compression
            If cf.ShowDialog() = Windows.Forms.DialogResult.OK Then
                If originalFormat = ConversionSource.FromPDFDocument Then
                    If pdfXpress.Documents.Count > 0 Then
                        compressionType = cf.SelectedCompression
                        Dim originalDocument As Accusoft.PdfXpressSdk.Document = pdfXpress.Documents(0)
                        Dim pdfaDocument As New Accusoft.PdfXpressSdk.Document(pdfXpress)

                        Dim renderOptions As New Accusoft.PdfXpressSdk.RenderOptions()
                        renderOptions.RenderAnnotations = False
                        renderOptions.ProduceDibSection = False
                        If compressionType = Compression.JPEG Then
                            renderOptions.ResolutionX = 100
                            renderOptions.ResolutionY = 100
                        ElseIf compressionType = Compression.JBIG2 Then
                            renderOptions.ResolutionX = 300
                            renderOptions.ResolutionY = 300
                            renderOptions.ReduceToBitonal = True
                        Else 'compressionType = Compression.G4
                            renderOptions.ResolutionX = 200
                            renderOptions.ResolutionY = 200
                            renderOptions.ReduceToBitonal = True
                        End If

                        Dim pageOpts As New Accusoft.PdfXpressSdk.PageOptions()
                        Dim pageInfo As Accusoft.PdfXpressSdk.PageInfo = Nothing

                        convertLabel.Text = "Conversion in progress..."
                        ' Display "Conversion in progress" immediately, in case conversion
                        ' takes a long time.
                        Application.DoEvents()

                        For page As Integer = 0 To originalDocument.PageCount - 1
                            ' Get bounds from the original page
                            pageInfo = originalDocument.GetInfo(page)

                            ' Set bounds from the original page
                            pageOpts.MediaHeight = pageInfo.MediaHeight
                            pageOpts.MediaWidth = pageInfo.MediaWidth

                            ' Create the new page in the image only document
                            pdfaDocument.CreatePage(page - 1, pageOpts)

                            ' Render the page in the original document
                            Dim dibPtr As System.IntPtr = New IntPtr(originalDocument.RenderPageToDib(page, renderOptions))

                            Dim pageImageX As Accusoft.ImagXpressSdk.ImageX = Nothing
                            pageImageX = Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress, dibPtr)

                            Dim buffer As Byte() = CompressDocumentImageXToBuffer(pageImageX, compressionType)

                            'Set the new page to the rendered image
                            pdfaDocument.AddImage(page, 0, 0, pageOpts.MediaWidth, pageOpts.MediaHeight, Accusoft.PdfXpressSdk.ImageFitSettings.None, _
                             buffer, 0)

                            If pageImageX IsNot Nothing Then
                                pageImageX.Dispose()
                            End If
                        Next

                        Dim conversionOptions As New Accusoft.PdfXpressSdk.ConvertOptions()
                        conversionOptions.PdfType = Accusoft.PdfXpressSdk.PdfType.pdfA1b
                        conversionOptions.IccProfileFilename = System.IO.Path.Combine(Application.StartupPath, _
                            "..\..\..\..\..\..\..\..\..\PdfXpress\v4.0\Support\SampleICCProfile\sRGB_IEC61966-2-1_withBPC.icc")
                        Try
                            pdfaDocument.ConvertToPdfA(conversionOptions)
                            convertLabel.Text = "Conversion successful."
                            imageOverTextPDFA = pdfaDocument

                            SetSaveEnabled(True)
                        Catch e As Accusoft.PdfXpressSdk.PdfXpressException
                            convertLabel.Text = "Conversion error: " & e.Message
                            If pdfaDocument IsNot Nothing Then
                                pdfaDocument.Dispose()
                            End If
                        End Try
                    End If
                    ' pdfXpress.Documents.Count > 0
                    ' originalFormat == PDF
                ElseIf originalFormat = ConversionSource.FromTiffImage Then
                    Dim pdfaDocument As New Accusoft.PdfXpressSdk.Document(pdfXpress)

                    ' ImagXpress uses a 1-based page index.
                    For page As Integer = 1 To originalImage.PageCount
                        Dim pageCreateOpts As New Accusoft.PdfXpressSdk.PageOptions()
                        originalImage.Page = page
                        originalImage.Resolution.Units = System.Drawing.GraphicsUnit.Inch

                        pageCreateOpts.MediaHeight = 72 * (originalImage.ImageXData.Height / originalImage.ImageXData.Resolution.Dimensions.Width)
                        pageCreateOpts.MediaWidth = 72 * (originalImage.ImageXData.Width / originalImage.ImageXData.Resolution.Dimensions.Height)

                        ' Compress the page image.
                        Dim buffer As Byte() = CompressDocumentImageXToBuffer(originalImage, compressionType)

                        ' We provide create page with the index *before* the index of the future page.
                        ' We offset an additional -1 for ImagXpress starting pages at 1 while PdfXpress
                        ' starts at 0.
                        pdfaDocument.CreatePage(page - 2, pageCreateOpts)

                        ' page-1 is the offset for ImagXpress using a 1-based index for pages 
                        ' while PdfXpress uses a 0-based index.
                        pdfaDocument.AddImage(page - 1, 0, 0, pageCreateOpts.MediaWidth, pageCreateOpts.MediaHeight, Accusoft.PdfXpressSdk.ImageFitSettings.None, _
                         buffer, 0)
                    Next
                    Dim conversionOptions As New Accusoft.PdfXpressSdk.ConvertOptions()
                    conversionOptions.PdfType = Accusoft.PdfXpressSdk.PdfType.pdfA1b
                    conversionOptions.IccProfileFilename = System.IO.Path.Combine(Application.StartupPath, _
                            "..\..\..\..\..\..\..\..\PdfXpress\v3.0\Support\SampleICCProfile\sRGB_IEC61966-2-1_withBPC.icc")
                    Try
                        pdfaDocument.ConvertToPdfA(conversionOptions)
                        convertLabel.Text = "Conversion successful."
                        imageOverTextPDFA = pdfaDocument

                        SetSaveEnabled(True)
                    Catch e As Accusoft.PdfXpressSdk.PdfXpressException
                        convertLabel.Text = "Conversion error: " & e.Message
                        If pdfaDocument IsNot Nothing Then
                            pdfaDocument.Dispose()
                        End If
                    End Try
                End If
            End If
        End Sub

        Private Shared Function CompressDocumentImageXToBuffer(ByVal pageImageX As Accusoft.ImagXpressSdk.ImageX, ByVal compressionType As Compression) As Byte()
            Using memStream As New System.IO.MemoryStream()
                memStream.Seek(0, System.IO.SeekOrigin.Begin)

                Dim saveOpts As New Accusoft.ImagXpressSdk.SaveOptions()
                Select Case compressionType
                    Case Compression.G4
                        saveOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff
                        saveOpts.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4
                        pageImageX.SaveStream(memStream, saveOpts)
                    Case Compression.JBIG2
                        saveOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jbig2
                        saveOpts.Jbig2.LoosenessCompression = 0
                        pageImageX.SaveStream(memStream, saveOpts)
                    Case Compression.JPEG
                        saveOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jpeg
                        pageImageX.SaveStream(memStream, saveOpts)
                    Case Else
                        Throw New InvalidEnumArgumentException()
                End Select
                memStream.Seek(0, System.IO.SeekOrigin.Begin)

                Dim buffer As Byte() = New Byte(CInt(memStream.Length) - 1) {}
                memStream.ToArray().CopyTo(buffer, 0)
                memStream.Close()
                Return buffer
            End Using
        End Function

        Private Sub saveConvertedDocument()
            Dim saveDialog As New System.Windows.Forms.SaveFileDialog()
            saveDialog.Filter = "PDF documents (*.pdf)|*.pdf"
            saveDialog.FilterIndex = 1
            If saveDialog.ShowDialog() = DialogResult.OK Then
                Try
                    Dim saveOpts As New Accusoft.PdfXpressSdk.SaveOptions()
                    saveOpts.Filename = saveDialog.FileName
                    imageOverTextPDFA.Save(saveOpts)

                    Dim trimmedName As String = saveDialog.FileName.Substring(saveDialog.FileName.LastIndexOf("\"c) + 1)
                    saveLabel.Text = System.[String].Format("{0} saved, {1} pages", trimmedName, imageOverTextPDFA.PageCount)
                Catch e As Exception
                    saveLabel.Text = "Error during save: " & e.Message
                End Try
            End If
        End Sub

        Private Sub aboutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AboutToolStripMenuItem.Click
            pdfXpress.AboutBox()
        End Sub

        Private Sub loadPDFButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
            loadUserSelectedDocument()
        End Sub

        Private Sub loadPDFToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LoadDocumentToolStripMenuItem.Click
            loadUserSelectedDocument()
        End Sub

        Private Sub convertToPDFAButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles convertToPDFAButton.Click
            convertUserSelectedDocument()
        End Sub

        Private Sub convertToPDFAToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles convertToPDFAToolStripMenuItem.Click
            convertUserSelectedDocument()
        End Sub

        Private Sub exitToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
            exitGracefully()
        End Sub

        Private Sub convertPdfForm_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
            exitGracefully()
        End Sub

        Private Sub savePDFAButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles savePDFAButton.Click
            saveConvertedDocument()
        End Sub

        Private Sub savePDFAToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles savePDFAToolStripMenuItem.Click
            saveConvertedDocument()
        End Sub

        Private Sub InitializeComponent()
            Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
            Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.LoadDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.convertToPDFAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.savePDFAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
            Me.Label1 = New System.Windows.Forms.Label
            Me.Button1 = New System.Windows.Forms.Button
            Me.convertToPDFAButton = New System.Windows.Forms.Button
            Me.savePDFAButton = New System.Windows.Forms.Button
            Me.loadLabel = New System.Windows.Forms.Label
            Me.convertLabel = New System.Windows.Forms.Label
            Me.saveLabel = New System.Windows.Forms.Label
            Me.MenuStrip1.SuspendLayout()
            Me.SuspendLayout()
            '
            'MenuStrip1
            '
            Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.HelpToolStripMenuItem})
            Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
            Me.MenuStrip1.Name = "MenuStrip1"
            Me.MenuStrip1.Size = New System.Drawing.Size(470, 24)
            Me.MenuStrip1.TabIndex = 0
            Me.MenuStrip1.Text = "MenuStrip1"
            '
            'FileToolStripMenuItem
            '
            Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadDocumentToolStripMenuItem, Me.convertToPDFAToolStripMenuItem, Me.savePDFAToolStripMenuItem, Me.ExitToolStripMenuItem})
            Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
            Me.FileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
            Me.FileToolStripMenuItem.Text = "File"
            '
            'LoadDocumentToolStripMenuItem
            '
            Me.LoadDocumentToolStripMenuItem.Name = "LoadDocumentToolStripMenuItem"
            Me.LoadDocumentToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
            Me.LoadDocumentToolStripMenuItem.Text = "Load Document..."
            '
            'convertToPDFAToolStripMenuItem
            '
            Me.convertToPDFAToolStripMenuItem.Name = "convertToPDFAToolStripMenuItem"
            Me.convertToPDFAToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
            Me.convertToPDFAToolStripMenuItem.Text = "Convert Document"
            '
            'savePDFAToolStripMenuItem
            '
            Me.savePDFAToolStripMenuItem.Name = "savePDFAToolStripMenuItem"
            Me.savePDFAToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
            Me.savePDFAToolStripMenuItem.Text = "Save Document..."
            '
            'ExitToolStripMenuItem
            '
            Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
            Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
            Me.ExitToolStripMenuItem.Text = "Exit"
            '
            'HelpToolStripMenuItem
            '
            Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
            Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
            Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(40, 20)
            Me.HelpToolStripMenuItem.Text = "Help"
            '
            'AboutToolStripMenuItem
            '
            Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
            Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
            Me.AboutToolStripMenuItem.Text = "About"
            '
            'Label1
            '
            Me.Label1.BackColor = System.Drawing.SystemColors.Info
            Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Location = New System.Drawing.Point(12, 35)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(446, 50)
            Me.Label1.TabIndex = 1
            Me.Label1.Text = "Using PDF Xpress v4, developers can now render PDF pages to image-only PDF/A - 1b" & _
                " " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "documents. PDF Xpress will provide details of failed compliance rules during " & _
                "conversion."
            '
            'Button1
            '
            Me.Button1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Button1.Location = New System.Drawing.Point(12, 101)
            Me.Button1.Name = "Button1"
            Me.Button1.Size = New System.Drawing.Size(124, 31)
            Me.Button1.TabIndex = 2
            Me.Button1.Text = "Load Document..."
            Me.Button1.UseVisualStyleBackColor = True
            '
            'convertToPDFAButton
            '
            Me.convertToPDFAButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.convertToPDFAButton.Location = New System.Drawing.Point(12, 150)
            Me.convertToPDFAButton.Name = "convertToPDFAButton"
            Me.convertToPDFAButton.Size = New System.Drawing.Size(124, 34)
            Me.convertToPDFAButton.TabIndex = 3
            Me.convertToPDFAButton.Text = "Convert To PDF/A"
            Me.convertToPDFAButton.UseVisualStyleBackColor = True
            '
            'savePDFAButton
            '
            Me.savePDFAButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.savePDFAButton.Location = New System.Drawing.Point(12, 202)
            Me.savePDFAButton.Name = "savePDFAButton"
            Me.savePDFAButton.Size = New System.Drawing.Size(124, 35)
            Me.savePDFAButton.TabIndex = 4
            Me.savePDFAButton.Text = "Save PDF/A"
            Me.savePDFAButton.UseVisualStyleBackColor = True
            '
            'loadLabel
            '
            Me.loadLabel.AutoSize = True
            Me.loadLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.loadLabel.Location = New System.Drawing.Point(142, 110)
            Me.loadLabel.Name = "loadLabel"
            Me.loadLabel.Size = New System.Drawing.Size(159, 13)
            Me.loadLabel.TabIndex = 5
            Me.loadLabel.Text = "Document not currently loaded."
            '
            'convertLabel
            '
            Me.convertLabel.AutoSize = True
            Me.convertLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.convertLabel.Location = New System.Drawing.Point(142, 161)
            Me.convertLabel.Name = "convertLabel"
            Me.convertLabel.Size = New System.Drawing.Size(187, 13)
            Me.convertLabel.TabIndex = 6
            Me.convertLabel.Text = "Please load document for conversion."
            '
            'saveLabel
            '
            Me.saveLabel.AutoSize = True
            Me.saveLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.saveLabel.Location = New System.Drawing.Point(142, 213)
            Me.saveLabel.Name = "saveLabel"
            Me.saveLabel.Size = New System.Drawing.Size(111, 13)
            Me.saveLabel.TabIndex = 7
            Me.saveLabel.Text = "PDF/A not yet saved."
            '
            'ConvertPDFForm
            '
            Me.ClientSize = New System.Drawing.Size(470, 257)
            Me.Controls.Add(Me.saveLabel)
            Me.Controls.Add(Me.convertLabel)
            Me.Controls.Add(Me.loadLabel)
            Me.Controls.Add(Me.savePDFAButton)
            Me.Controls.Add(Me.convertToPDFAButton)
            Me.Controls.Add(Me.Button1)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.MenuStrip1)
            Me.MainMenuStrip = Me.MenuStrip1
            Me.Name = "ConvertPDFForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
            Me.Text = "Convert to PDF/A Demo"
            Me.MenuStrip1.ResumeLayout(False)
            Me.MenuStrip1.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Private Sub ConvertPDFForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Application.EnableVisualStyles()
        End Sub
    End Class
End Namespace


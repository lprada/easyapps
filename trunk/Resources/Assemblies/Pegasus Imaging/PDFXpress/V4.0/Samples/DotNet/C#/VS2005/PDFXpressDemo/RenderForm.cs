/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accusoft.PdfXpressSdk;

namespace PDFDemo
{
    public partial class RenderForm : Form
    {
        private RenderOptions rendOptions;

        public DialogResult dialogResult;

        public bool ReduceBitonal
        {
            get
            {
                return ReduceCheckBox.Checked;
            }
            set
            {
                ReduceCheckBox.Checked = value;
            }
        }

        public bool XFDF
        {
            get
            {
                return XFDFCheckBox.Checked;
            }
        }

        public bool Annotations
        {
            get
            {
                return AnnotationCheckBox.Checked;
            }
            set
            {
                AnnotationCheckBox.Checked = value;
            }
        }

        public double ResolutionX
        {
            get
            {
                return Convert.ToDouble(HResBox.Text);
            }
            set
            {
                HResBox.Text = value.ToString();
            }
        }

        public double ResolutionY
        {
            get
            {
                return Convert.ToDouble(VResBox.Text);
            }
            set
            {
                VResBox.Text = value.ToString();
            }
        }

        public double SelectionX
        {
            get
            {
                return Convert.ToDouble(SelXBox.Text);
            }
            set
            {
                SelXBox.Text = value.ToString();
            }
        }

        public double SelectionY
        {
            get
            {
                return Convert.ToDouble(SelYBox.Text);
            }
            set
            {
                SelYBox.Text = value.ToString();
            }
        }
        public double SelectionW
        {
            get
            {
                return Convert.ToDouble(SelWBox.Text);
            }
            set
            {
                SelWBox.Text = value.ToString();
            }
        }

        public double SelectionH
        {
            get
            {
                return Convert.ToDouble(SelHBox.Text);
            }
            set
            {
                SelHBox.Text = value.ToString();
            }
        }

        public RenderForm(RenderOptions renderOptions)
        {
            rendOptions = renderOptions;

            InitializeComponent();
        }

        private void CancButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Cancel;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void XFDFCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (XFDFCheckBox.Checked == true)
            {
                AnnotationCheckBox.Checked = false;
            }
        }

        private void AnnotationCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (AnnotationCheckBox.Checked == true)
            {
                XFDFCheckBox.Checked = false;
            }
        } 
    }
}
'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Imports Accusoft.PdfXpressSdk

Public Class RenderForm

    Public Property ReduceBitonal() As Boolean
        Get
            Return ReduceCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            ReduceCheckBox.Checked = value
        End Set
    End Property
    Public ReadOnly Property XFDF() As Boolean
        Get
            Return XFDFCheckBox.Checked
        End Get
    End Property
    Public Property Annotations() As Boolean
        Get
            Return AnnotationCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            AnnotationCheckBox.Checked = value
        End Set
    End Property

    Public Property ResolutionX() As Double
        Get
            Return CType(HResBox.Text, Double)
        End Get
        Set(ByVal value As Double)
            HResBox.Text = value.ToString()
        End Set
    End Property

    Public Property ResolutionY() As Double
        Get
            Return CType(VResBox.Text, Double)
        End Get
        Set(ByVal value As Double)
            VResBox.Text = value.ToString()
        End Set
    End Property

    Public Property SelectionX() As Double
        Get
            Return CType(SelXBox.Text, Double)
        End Get
        Set(ByVal value As Double)
            SelXBox.Text = value.ToString()
        End Set
    End Property

    Public Property SelectionY() As Double
        Get
            Return CType(SelYBox.Text, Double)
        End Get
        Set(ByVal value As Double)
            SelYBox.Text = value.ToString()
        End Set
    End Property

    Public Property SelectionW() As Double
        Get
            Return CType(SelWBox.Text, Double)
        End Get
        Set(ByVal value As Double)
            SelWBox.Text = value.ToString()
        End Set
    End Property

    Public Property SelectionH() As Double
        Get
            Return CType(SelHBox.Text, Double)
        End Get
        Set(ByVal value As Double)
            SelHBox.Text = value.ToString()
        End Set
    End Property

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.Cancel

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.OK

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AnnotationCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnnotationCheckBox.CheckedChanged
        If (AnnotationCheckBox.Checked = True) Then
            XFDFCheckBox.Checked = False
        End If
    End Sub

    Private Sub XFDFCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles XFDFCheckBox.CheckedChanged
        If (XFDFCheckBox.Checked = True) Then
            AnnotationCheckBox.Checked = False
        End If
    End Sub
End Class
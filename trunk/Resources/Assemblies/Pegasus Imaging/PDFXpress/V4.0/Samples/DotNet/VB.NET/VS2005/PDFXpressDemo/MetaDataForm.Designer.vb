'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MetaDataForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MetaDataForm))
        Me.OKButton = New System.Windows.Forms.Button
        Me.CancelMetaButton = New System.Windows.Forms.Button
        Me.SubjectLabel = New System.Windows.Forms.Label
        Me.SubjectTextBox = New System.Windows.Forms.TextBox
        Me.AuthorTextBox = New System.Windows.Forms.TextBox
        Me.AuthorLabel = New System.Windows.Forms.Label
        Me.CreationDateTextBox = New System.Windows.Forms.TextBox
        Me.CreationDateLabel = New System.Windows.Forms.Label
        Me.CreatorTextBox = New System.Windows.Forms.TextBox
        Me.CreatorLabel = New System.Windows.Forms.Label
        Me.TitleTextBox = New System.Windows.Forms.TextBox
        Me.TitleLabel = New System.Windows.Forms.Label
        Me.ModificationDateTextBox = New System.Windows.Forms.TextBox
        Me.ModificationDateLabel = New System.Windows.Forms.Label
        Me.ProducerTextBox = New System.Windows.Forms.TextBox
        Me.ProducerLabel = New System.Windows.Forms.Label
        Me.KeywordsTextBox = New System.Windows.Forms.TextBox
        Me.KeywordsLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(9, 229)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 23)
        Me.OKButton.TabIndex = 16
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'CancelMetaButton
        '
        Me.CancelMetaButton.Location = New System.Drawing.Point(417, 226)
        Me.CancelMetaButton.Name = "CancelMetaButton"
        Me.CancelMetaButton.Size = New System.Drawing.Size(75, 23)
        Me.CancelMetaButton.TabIndex = 17
        Me.CancelMetaButton.Text = "Cancel"
        Me.CancelMetaButton.UseVisualStyleBackColor = True
        '
        'SubjectLabel
        '
        Me.SubjectLabel.AutoSize = True
        Me.SubjectLabel.Location = New System.Drawing.Point(12, 9)
        Me.SubjectLabel.Name = "SubjectLabel"
        Me.SubjectLabel.Size = New System.Drawing.Size(43, 13)
        Me.SubjectLabel.TabIndex = 0
        Me.SubjectLabel.Text = "Subject"
        '
        'SubjectTextBox
        '
        Me.SubjectTextBox.Location = New System.Drawing.Point(107, 6)
        Me.SubjectTextBox.Name = "SubjectTextBox"
        Me.SubjectTextBox.Size = New System.Drawing.Size(385, 20)
        Me.SubjectTextBox.TabIndex = 1
        '
        'AuthorTextBox
        '
        Me.AuthorTextBox.Location = New System.Drawing.Point(107, 32)
        Me.AuthorTextBox.Name = "AuthorTextBox"
        Me.AuthorTextBox.Size = New System.Drawing.Size(385, 20)
        Me.AuthorTextBox.TabIndex = 3
        '
        'AuthorLabel
        '
        Me.AuthorLabel.AutoSize = True
        Me.AuthorLabel.Location = New System.Drawing.Point(12, 35)
        Me.AuthorLabel.Name = "AuthorLabel"
        Me.AuthorLabel.Size = New System.Drawing.Size(38, 13)
        Me.AuthorLabel.TabIndex = 2
        Me.AuthorLabel.Text = "Author"
        '
        'CreationDateTextBox
        '
        Me.CreationDateTextBox.Location = New System.Drawing.Point(107, 84)
        Me.CreationDateTextBox.Name = "CreationDateTextBox"
        Me.CreationDateTextBox.ReadOnly = True
        Me.CreationDateTextBox.Size = New System.Drawing.Size(385, 20)
        Me.CreationDateTextBox.TabIndex = 7
        '
        'CreationDateLabel
        '
        Me.CreationDateLabel.AutoSize = True
        Me.CreationDateLabel.Location = New System.Drawing.Point(12, 87)
        Me.CreationDateLabel.Name = "CreationDateLabel"
        Me.CreationDateLabel.Size = New System.Drawing.Size(72, 13)
        Me.CreationDateLabel.TabIndex = 6
        Me.CreationDateLabel.Text = "Creation Date"
        '
        'CreatorTextBox
        '
        Me.CreatorTextBox.Location = New System.Drawing.Point(107, 58)
        Me.CreatorTextBox.Name = "CreatorTextBox"
        Me.CreatorTextBox.Size = New System.Drawing.Size(385, 20)
        Me.CreatorTextBox.TabIndex = 5
        '
        'CreatorLabel
        '
        Me.CreatorLabel.AutoSize = True
        Me.CreatorLabel.Location = New System.Drawing.Point(12, 61)
        Me.CreatorLabel.Name = "CreatorLabel"
        Me.CreatorLabel.Size = New System.Drawing.Size(41, 13)
        Me.CreatorLabel.TabIndex = 4
        Me.CreatorLabel.Text = "Creator"
        '
        'TitleTextBox
        '
        Me.TitleTextBox.Location = New System.Drawing.Point(107, 188)
        Me.TitleTextBox.Name = "TitleTextBox"
        Me.TitleTextBox.Size = New System.Drawing.Size(385, 20)
        Me.TitleTextBox.TabIndex = 15
        '
        'TitleLabel
        '
        Me.TitleLabel.AutoSize = True
        Me.TitleLabel.Location = New System.Drawing.Point(12, 191)
        Me.TitleLabel.Name = "TitleLabel"
        Me.TitleLabel.Size = New System.Drawing.Size(27, 13)
        Me.TitleLabel.TabIndex = 14
        Me.TitleLabel.Text = "Title"
        '
        'ModificationDateTextBox
        '
        Me.ModificationDateTextBox.Location = New System.Drawing.Point(107, 162)
        Me.ModificationDateTextBox.Name = "ModificationDateTextBox"
        Me.ModificationDateTextBox.ReadOnly = True
        Me.ModificationDateTextBox.Size = New System.Drawing.Size(385, 20)
        Me.ModificationDateTextBox.TabIndex = 13
        '
        'ModificationDateLabel
        '
        Me.ModificationDateLabel.AutoSize = True
        Me.ModificationDateLabel.Location = New System.Drawing.Point(12, 165)
        Me.ModificationDateLabel.Name = "ModificationDateLabel"
        Me.ModificationDateLabel.Size = New System.Drawing.Size(90, 13)
        Me.ModificationDateLabel.TabIndex = 12
        Me.ModificationDateLabel.Text = "Modification Date"
        '
        'ProducerTextBox
        '
        Me.ProducerTextBox.Location = New System.Drawing.Point(107, 136)
        Me.ProducerTextBox.Name = "ProducerTextBox"
        Me.ProducerTextBox.Size = New System.Drawing.Size(385, 20)
        Me.ProducerTextBox.TabIndex = 11
        '
        'ProducerLabel
        '
        Me.ProducerLabel.AutoSize = True
        Me.ProducerLabel.Location = New System.Drawing.Point(12, 139)
        Me.ProducerLabel.Name = "ProducerLabel"
        Me.ProducerLabel.Size = New System.Drawing.Size(50, 13)
        Me.ProducerLabel.TabIndex = 10
        Me.ProducerLabel.Text = "Producer"
        '
        'KeywordsTextBox
        '
        Me.KeywordsTextBox.Location = New System.Drawing.Point(107, 110)
        Me.KeywordsTextBox.Name = "KeywordsTextBox"
        Me.KeywordsTextBox.Size = New System.Drawing.Size(385, 20)
        Me.KeywordsTextBox.TabIndex = 9
        '
        'KeywordsLabel
        '
        Me.KeywordsLabel.AutoSize = True
        Me.KeywordsLabel.Location = New System.Drawing.Point(12, 113)
        Me.KeywordsLabel.Name = "KeywordsLabel"
        Me.KeywordsLabel.Size = New System.Drawing.Size(53, 13)
        Me.KeywordsLabel.TabIndex = 8
        Me.KeywordsLabel.Text = "Keywords"
        '
        'MetaDataForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(500, 261)
        Me.Controls.Add(Me.TitleTextBox)
        Me.Controls.Add(Me.TitleLabel)
        Me.Controls.Add(Me.ModificationDateTextBox)
        Me.Controls.Add(Me.ModificationDateLabel)
        Me.Controls.Add(Me.ProducerTextBox)
        Me.Controls.Add(Me.ProducerLabel)
        Me.Controls.Add(Me.KeywordsTextBox)
        Me.Controls.Add(Me.KeywordsLabel)
        Me.Controls.Add(Me.CreationDateTextBox)
        Me.Controls.Add(Me.CreationDateLabel)
        Me.Controls.Add(Me.CreatorTextBox)
        Me.Controls.Add(Me.CreatorLabel)
        Me.Controls.Add(Me.AuthorTextBox)
        Me.Controls.Add(Me.AuthorLabel)
        Me.Controls.Add(Me.SubjectTextBox)
        Me.Controls.Add(Me.SubjectLabel)
        Me.Controls.Add(Me.CancelMetaButton)
        Me.Controls.Add(Me.OKButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MetaDataForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Meta data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents CancelMetaButton As System.Windows.Forms.Button
    Friend WithEvents SubjectLabel As System.Windows.Forms.Label
    Friend WithEvents SubjectTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AuthorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AuthorLabel As System.Windows.Forms.Label
    Friend WithEvents CreationDateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreationDateLabel As System.Windows.Forms.Label
    Friend WithEvents CreatorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CreatorLabel As System.Windows.Forms.Label
    Friend WithEvents TitleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TitleLabel As System.Windows.Forms.Label
    Friend WithEvents ModificationDateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ModificationDateLabel As System.Windows.Forms.Label
    Friend WithEvents ProducerTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ProducerLabel As System.Windows.Forms.Label
    Friend WithEvents KeywordsTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KeywordsLabel As System.Windows.Forms.Label
End Class

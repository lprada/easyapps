/// Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;
    using System.Globalization;

    public partial class GoToPageForm : Form
    {
        public Int32 Page
        {
            get { return ( Int32 ) this._pageNumericUpDown.Value; }
            set { this._pageNumericUpDown.Value = value ; }
        }
        public Int32 PageCount
        {
            get { return ( Int32 )this._pageNumericUpDown.Maximum ; }
            set 
            { 
                this._pageNumericUpDown.Maximum = value ; 
                this.RefreshPageCountLabel( ); 
            }
        }
        private void RefreshPageCountLabel( )
        {
            this._ofPageCountLabel.Text = String.Format
                                            ( NumberFormatInfo.CurrentInfo
                                            , "of {0:d}"
                                            , ( Int32 )this._pageNumericUpDown.Maximum
                                            ) ;
        }
        public GoToPageForm( )
        {
            InitializeComponent( );
            this._pageNumericUpDown.Minimum = 1;
        }
    }
}
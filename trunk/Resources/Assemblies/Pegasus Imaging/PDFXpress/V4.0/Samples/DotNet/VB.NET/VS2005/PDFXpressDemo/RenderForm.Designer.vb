'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RenderForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RenderForm))
        Me.OKButton = New System.Windows.Forms.Button
        Me.CButton = New System.Windows.Forms.Button
        Me.HresLabel = New System.Windows.Forms.Label
        Me.HResBox = New System.Windows.Forms.TextBox
        Me.SelXBox = New System.Windows.Forms.TextBox
        Me.SelXLabel = New System.Windows.Forms.Label
        Me.VResBox = New System.Windows.Forms.TextBox
        Me.VResLabel = New System.Windows.Forms.Label
        Me.SelWBox = New System.Windows.Forms.TextBox
        Me.SelWLabel = New System.Windows.Forms.Label
        Me.SelHBox = New System.Windows.Forms.TextBox
        Me.SelHLabel = New System.Windows.Forms.Label
        Me.SelYBox = New System.Windows.Forms.TextBox
        Me.SelYLabel = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.ReduceCheckBox = New System.Windows.Forms.CheckBox
        Me.AnnotationCheckBox = New System.Windows.Forms.CheckBox
        Me.XFDFCheckBox = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(12, 234)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 23)
        Me.OKButton.TabIndex = 16
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'CButton
        '
        Me.CButton.Location = New System.Drawing.Point(226, 234)
        Me.CButton.Name = "CButton"
        Me.CButton.Size = New System.Drawing.Size(75, 23)
        Me.CButton.TabIndex = 17
        Me.CButton.Text = "Cancel"
        Me.CButton.UseVisualStyleBackColor = True
        '
        'HresLabel
        '
        Me.HresLabel.AutoSize = True
        Me.HresLabel.Location = New System.Drawing.Point(53, 18)
        Me.HresLabel.Name = "HresLabel"
        Me.HresLabel.Size = New System.Drawing.Size(107, 13)
        Me.HresLabel.TabIndex = 0
        Me.HresLabel.Text = "Horizontal Resolution"
        '
        'HResBox
        '
        Me.HResBox.Location = New System.Drawing.Point(166, 15)
        Me.HResBox.Name = "HResBox"
        Me.HResBox.Size = New System.Drawing.Size(60, 20)
        Me.HResBox.TabIndex = 1
        '
        'SelXBox
        '
        Me.SelXBox.Location = New System.Drawing.Point(166, 69)
        Me.SelXBox.Name = "SelXBox"
        Me.SelXBox.Size = New System.Drawing.Size(60, 20)
        Me.SelXBox.TabIndex = 7
        '
        'SelXLabel
        '
        Me.SelXLabel.AutoSize = True
        Me.SelXLabel.Location = New System.Drawing.Point(53, 69)
        Me.SelXLabel.Name = "SelXLabel"
        Me.SelXLabel.Size = New System.Drawing.Size(61, 13)
        Me.SelXLabel.TabIndex = 6
        Me.SelXLabel.Text = "Selection X"
        '
        'VResBox
        '
        Me.VResBox.Location = New System.Drawing.Point(166, 41)
        Me.VResBox.Name = "VResBox"
        Me.VResBox.Size = New System.Drawing.Size(60, 20)
        Me.VResBox.TabIndex = 4
        '
        'VResLabel
        '
        Me.VResLabel.AutoSize = True
        Me.VResLabel.Location = New System.Drawing.Point(53, 44)
        Me.VResLabel.Name = "VResLabel"
        Me.VResLabel.Size = New System.Drawing.Size(95, 13)
        Me.VResLabel.TabIndex = 3
        Me.VResLabel.Text = "Vertical Resolution"
        '
        'SelWBox
        '
        Me.SelWBox.Location = New System.Drawing.Point(166, 121)
        Me.SelWBox.Name = "SelWBox"
        Me.SelWBox.Size = New System.Drawing.Size(60, 20)
        Me.SelWBox.TabIndex = 11
        '
        'SelWLabel
        '
        Me.SelWLabel.AutoSize = True
        Me.SelWLabel.Location = New System.Drawing.Point(53, 121)
        Me.SelWLabel.Name = "SelWLabel"
        Me.SelWLabel.Size = New System.Drawing.Size(82, 13)
        Me.SelWLabel.TabIndex = 10
        Me.SelWLabel.Text = "Selection Width"
        '
        'SelHBox
        '
        Me.SelHBox.Location = New System.Drawing.Point(166, 147)
        Me.SelHBox.Name = "SelHBox"
        Me.SelHBox.Size = New System.Drawing.Size(60, 20)
        Me.SelHBox.TabIndex = 13
        '
        'SelHLabel
        '
        Me.SelHLabel.AutoSize = True
        Me.SelHLabel.Location = New System.Drawing.Point(53, 147)
        Me.SelHLabel.Name = "SelHLabel"
        Me.SelHLabel.Size = New System.Drawing.Size(85, 13)
        Me.SelHLabel.TabIndex = 12
        Me.SelHLabel.Text = "Selection Height"
        '
        'SelYBox
        '
        Me.SelYBox.Location = New System.Drawing.Point(166, 95)
        Me.SelYBox.Name = "SelYBox"
        Me.SelYBox.Size = New System.Drawing.Size(60, 20)
        Me.SelYBox.TabIndex = 9
        '
        'SelYLabel
        '
        Me.SelYLabel.AutoSize = True
        Me.SelYLabel.Location = New System.Drawing.Point(53, 95)
        Me.SelYLabel.Name = "SelYLabel"
        Me.SelYLabel.Size = New System.Drawing.Size(61, 13)
        Me.SelYLabel.TabIndex = 8
        Me.SelYLabel.Text = "Selection Y"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(235, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "(DPI)"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(235, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(31, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "(DPI)"
        '
        'ReduceCheckBox
        '
        Me.ReduceCheckBox.AutoSize = True
        Me.ReduceCheckBox.Location = New System.Drawing.Point(99, 179)
        Me.ReduceCheckBox.Name = "ReduceCheckBox"
        Me.ReduceCheckBox.Size = New System.Drawing.Size(115, 17)
        Me.ReduceCheckBox.TabIndex = 14
        Me.ReduceCheckBox.Text = "Reduce To Bitonal"
        Me.ReduceCheckBox.UseVisualStyleBackColor = True
        '
        'AnnotationCheckBox
        '
        Me.AnnotationCheckBox.AutoSize = True
        Me.AnnotationCheckBox.Location = New System.Drawing.Point(16, 202)
        Me.AnnotationCheckBox.Name = "AnnotationCheckBox"
        Me.AnnotationCheckBox.Size = New System.Drawing.Size(120, 17)
        Me.AnnotationCheckBox.TabIndex = 15
        Me.AnnotationCheckBox.Text = "Render Annotations"
        Me.AnnotationCheckBox.UseVisualStyleBackColor = True
        '
        'XFDFCheckBox
        '
        Me.XFDFCheckBox.AutoSize = True
        Me.XFDFCheckBox.Checked = True
        Me.XFDFCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.XFDFCheckBox.Location = New System.Drawing.Point(160, 202)
        Me.XFDFCheckBox.Name = "XFDFCheckBox"
        Me.XFDFCheckBox.Size = New System.Drawing.Size(157, 17)
        Me.XFDFCheckBox.TabIndex = 18
        Me.XFDFCheckBox.Text = "Add Annotations from XFDF"
        Me.XFDFCheckBox.UseVisualStyleBackColor = True
        '
        'RenderForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(322, 267)
        Me.Controls.Add(Me.XFDFCheckBox)
        Me.Controls.Add(Me.AnnotationCheckBox)
        Me.Controls.Add(Me.ReduceCheckBox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.SelWBox)
        Me.Controls.Add(Me.SelWLabel)
        Me.Controls.Add(Me.SelHBox)
        Me.Controls.Add(Me.SelHLabel)
        Me.Controls.Add(Me.SelYBox)
        Me.Controls.Add(Me.SelYLabel)
        Me.Controls.Add(Me.VResBox)
        Me.Controls.Add(Me.VResLabel)
        Me.Controls.Add(Me.SelXBox)
        Me.Controls.Add(Me.SelXLabel)
        Me.Controls.Add(Me.HResBox)
        Me.Controls.Add(Me.HresLabel)
        Me.Controls.Add(Me.CButton)
        Me.Controls.Add(Me.OKButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "RenderForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Render Options"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents CButton As System.Windows.Forms.Button
    Friend WithEvents HresLabel As System.Windows.Forms.Label
    Friend WithEvents HResBox As System.Windows.Forms.TextBox
    Friend WithEvents SelXBox As System.Windows.Forms.TextBox
    Friend WithEvents SelXLabel As System.Windows.Forms.Label
    Friend WithEvents VResBox As System.Windows.Forms.TextBox
    Friend WithEvents VResLabel As System.Windows.Forms.Label
    Friend WithEvents SelWBox As System.Windows.Forms.TextBox
    Friend WithEvents SelWLabel As System.Windows.Forms.Label
    Friend WithEvents SelHBox As System.Windows.Forms.TextBox
    Friend WithEvents SelHLabel As System.Windows.Forms.Label
    Friend WithEvents SelYBox As System.Windows.Forms.TextBox
    Friend WithEvents SelYLabel As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ReduceCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents AnnotationCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents XFDFCheckBox As System.Windows.Forms.CheckBox
End Class

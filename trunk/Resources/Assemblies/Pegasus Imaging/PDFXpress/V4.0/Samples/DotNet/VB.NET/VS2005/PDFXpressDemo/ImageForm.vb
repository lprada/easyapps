'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Imports Accusoft.PdfXpressSdk
Imports System.IO

Public Class ImageForm

    Public ReadOnly Property Settings() As Integer
        Get
            Return FitComboBox.SelectedIndex
        End Get
    End Property

    Public ReadOnly Property Image() As String
        Get
            Return ImageBox.Text
        End Get
    End Property

    Public Property DestinationX() As Integer
        Get
            Return CType(DesXBox.Value, Integer)
        End Get
        Set(ByVal value As Integer)
            DesXBox.Value = value
        End Set
    End Property

    Public Property DestinationY() As Integer
        Get
            Return CType(DesYBox.Value, Integer)
        End Get
        Set(ByVal value As Integer)
            DesYBox.Value = value
        End Set
    End Property

    Public Property DestinationW() As Integer
        Get
            Return CType(DesWBox.Value, Integer)
        End Get
        Set(ByVal value As Integer)
            DesWBox.Value = value
        End Set
    End Property

    Public Property DestinationH() As Integer
        Get
            Return CType(DesHBox.Value, Integer)
        End Get
        Set(ByVal value As Integer)
            DesHBox.Value = value
        End Set
    End Property

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CButton.Click
        Try
            DialogResult = DialogResult.Cancel

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Try
            If Not (ImageBox.Text = "") Then
                DialogResult = DialogResult.OK
                Close()
            Else
                MessageBox.Show("Please choose an image first.")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub imageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imageButton.Click
        Try
            OpenFileDialog1.Title = "Open an image"
            OpenFileDialog1.InitialDirectory = Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\")
            OpenFileDialog1.Filter = "All Files (*.*) | *.*"
            OpenFileDialog1.FileName = ImageBox.Text

            If (OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK) Then
                ImageBox.Text = OpenFileDialog1.FileName
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ImageForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FitComboBox.SelectedIndex = 0

        DesXBox.Value = DestinationX.ToString()
        DesYBox.Value = DestinationY.ToString()
        DesWBox.Value = DestinationW.ToString()
        DesHBox.Value = DestinationH.ToString()
    End Sub
End Class
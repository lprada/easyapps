'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImageForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not (OpenFileDialog1 Is Nothing) Then
                OpenFileDialog1.Dispose()
                OpenFileDialog1 = Nothing
            End If
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ImageForm))
        Me.OKButton = New System.Windows.Forms.Button
        Me.DesXBox = New System.Windows.Forms.NumericUpDown
        Me.DesWBox = New System.Windows.Forms.NumericUpDown
        Me.DesHBox = New System.Windows.Forms.NumericUpDown
        Me.DesYBox = New System.Windows.Forms.NumericUpDown
        Me.ImageBox = New System.Windows.Forms.TextBox
        Me.FitComboBox = New System.Windows.Forms.ComboBox
        Me.imageButton = New System.Windows.Forms.Button
        Me.CButton = New System.Windows.Forms.Button
        Me.TopLabel = New System.Windows.Forms.Label
        Me.DesXLabel = New System.Windows.Forms.Label
        Me.DesYLabel = New System.Windows.Forms.Label
        Me.DesHLabel = New System.Windows.Forms.Label
        Me.DesWLabel = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        CType(Me.DesXBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DesWBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DesHBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DesYBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(291, 231)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 23)
        Me.OKButton.TabIndex = 13
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'DesXBox
        '
        Me.DesXBox.Location = New System.Drawing.Point(224, 77)
        Me.DesXBox.Name = "DesXBox"
        Me.DesXBox.Size = New System.Drawing.Size(65, 20)
        Me.DesXBox.TabIndex = 4
        '
        'DesWBox
        '
        Me.DesWBox.Location = New System.Drawing.Point(224, 129)
        Me.DesWBox.Name = "DesWBox"
        Me.DesWBox.Size = New System.Drawing.Size(65, 20)
        Me.DesWBox.TabIndex = 8
        '
        'DesHBox
        '
        Me.DesHBox.Location = New System.Drawing.Point(224, 155)
        Me.DesHBox.Name = "DesHBox"
        Me.DesHBox.Size = New System.Drawing.Size(65, 20)
        Me.DesHBox.TabIndex = 10
        '
        'DesYBox
        '
        Me.DesYBox.Location = New System.Drawing.Point(224, 103)
        Me.DesYBox.Name = "DesYBox"
        Me.DesYBox.Size = New System.Drawing.Size(65, 20)
        Me.DesYBox.TabIndex = 6
        '
        'ImageBox
        '
        Me.ImageBox.Enabled = False
        Me.ImageBox.Location = New System.Drawing.Point(12, 40)
        Me.ImageBox.Name = "ImageBox"
        Me.ImageBox.Size = New System.Drawing.Size(440, 20)
        Me.ImageBox.TabIndex = 2
        '
        'FitComboBox
        '
        Me.FitComboBox.FormattingEnabled = True
        Me.FitComboBox.Items.AddRange(New Object() {"None", "Stretch", "Shrink", "Grow", "GravityLeft", "GravityTop", "GravityRight", "GravityBottom"})
        Me.FitComboBox.Location = New System.Drawing.Point(224, 193)
        Me.FitComboBox.Name = "FitComboBox"
        Me.FitComboBox.Size = New System.Drawing.Size(121, 21)
        Me.FitComboBox.TabIndex = 12
        '
        'imageButton
        '
        Me.imageButton.Location = New System.Drawing.Point(377, 11)
        Me.imageButton.Name = "imageButton"
        Me.imageButton.Size = New System.Drawing.Size(75, 23)
        Me.imageButton.TabIndex = 1
        Me.imageButton.Text = "Browse"
        Me.imageButton.UseVisualStyleBackColor = True
        '
        'CButton
        '
        Me.CButton.Location = New System.Drawing.Point(377, 231)
        Me.CButton.Name = "CButton"
        Me.CButton.Size = New System.Drawing.Size(75, 23)
        Me.CButton.TabIndex = 14
        Me.CButton.Text = "Cancel"
        Me.CButton.UseVisualStyleBackColor = True
        '
        'TopLabel
        '
        Me.TopLabel.AutoSize = True
        Me.TopLabel.Location = New System.Drawing.Point(12, 11)
        Me.TopLabel.Name = "TopLabel"
        Me.TopLabel.Size = New System.Drawing.Size(109, 13)
        Me.TopLabel.TabIndex = 0
        Me.TopLabel.Text = "Choose Image to Add"
        '
        'DesXLabel
        '
        Me.DesXLabel.AutoSize = True
        Me.DesXLabel.Location = New System.Drawing.Point(120, 79)
        Me.DesXLabel.Name = "DesXLabel"
        Me.DesXLabel.Size = New System.Drawing.Size(70, 13)
        Me.DesXLabel.TabIndex = 3
        Me.DesXLabel.Text = "Destination X"
        '
        'DesYLabel
        '
        Me.DesYLabel.AutoSize = True
        Me.DesYLabel.Location = New System.Drawing.Point(120, 105)
        Me.DesYLabel.Name = "DesYLabel"
        Me.DesYLabel.Size = New System.Drawing.Size(70, 13)
        Me.DesYLabel.TabIndex = 5
        Me.DesYLabel.Text = "Destination Y"
        '
        'DesHLabel
        '
        Me.DesHLabel.AutoSize = True
        Me.DesHLabel.Location = New System.Drawing.Point(120, 155)
        Me.DesHLabel.Name = "DesHLabel"
        Me.DesHLabel.Size = New System.Drawing.Size(94, 13)
        Me.DesHLabel.TabIndex = 9
        Me.DesHLabel.Text = "Destination Height"
        '
        'DesWLabel
        '
        Me.DesWLabel.AutoSize = True
        Me.DesWLabel.Location = New System.Drawing.Point(120, 131)
        Me.DesWLabel.Name = "DesWLabel"
        Me.DesWLabel.Size = New System.Drawing.Size(91, 13)
        Me.DesWLabel.TabIndex = 7
        Me.DesWLabel.Text = "Desintation Width"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(117, 196)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Image Fit Settings"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ImageForm
        '
        Me.AcceptButton = Me.OKButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(464, 266)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DesHLabel)
        Me.Controls.Add(Me.DesWLabel)
        Me.Controls.Add(Me.DesYLabel)
        Me.Controls.Add(Me.DesXLabel)
        Me.Controls.Add(Me.TopLabel)
        Me.Controls.Add(Me.CButton)
        Me.Controls.Add(Me.imageButton)
        Me.Controls.Add(Me.FitComboBox)
        Me.Controls.Add(Me.ImageBox)
        Me.Controls.Add(Me.DesYBox)
        Me.Controls.Add(Me.DesHBox)
        Me.Controls.Add(Me.DesWBox)
        Me.Controls.Add(Me.DesXBox)
        Me.Controls.Add(Me.OKButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "ImageForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Image"
        CType(Me.DesXBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DesWBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DesHBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DesYBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Public WithEvents ImageBox As System.Windows.Forms.TextBox
    Friend WithEvents imageButton As System.Windows.Forms.Button
    Friend WithEvents CButton As System.Windows.Forms.Button
    Friend WithEvents TopLabel As System.Windows.Forms.Label
    Friend WithEvents DesXLabel As System.Windows.Forms.Label
    Friend WithEvents DesYLabel As System.Windows.Forms.Label
    Friend WithEvents DesHLabel As System.Windows.Forms.Label
    Friend WithEvents DesWLabel As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Private WithEvents FitComboBox As System.Windows.Forms.ComboBox
    Public WithEvents DesXBox As System.Windows.Forms.NumericUpDown
    Public WithEvents DesWBox As System.Windows.Forms.NumericUpDown
    Public WithEvents DesHBox As System.Windows.Forms.NumericUpDown
    Public WithEvents DesYBox As System.Windows.Forms.NumericUpDown
End Class

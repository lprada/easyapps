/// Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    partial class GoToPageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._pageLabel = new System.Windows.Forms.Label( );
            this._pageNumericUpDown = new System.Windows.Forms.NumericUpDown( );
            this._ofPageCountLabel = new System.Windows.Forms.Label( );
            this._okButton = new System.Windows.Forms.Button( );
            this._cancelButton = new System.Windows.Forms.Button( );
            ( ( System.ComponentModel.ISupportInitialize )( this._pageNumericUpDown ) ).BeginInit( );
            this.SuspendLayout( );
            // 
            // _pageLabel
            // 
            this._pageLabel.AutoSize = true;
            this._pageLabel.Location = new System.Drawing.Point( 16 , 20 );
            this._pageLabel.Name = "_pageLabel";
            this._pageLabel.Size = new System.Drawing.Size( 35 , 13 );
            this._pageLabel.TabIndex = 0;
            this._pageLabel.Text = "Page:";
            // 
            // _pageNumericUpDown
            // 
            this._pageNumericUpDown.Location = new System.Drawing.Point( 57 , 18 );
            this._pageNumericUpDown.Name = "_pageNumericUpDown";
            this._pageNumericUpDown.Size = new System.Drawing.Size( 77 , 20 );
            this._pageNumericUpDown.TabIndex = 1;
            this._pageNumericUpDown.Value = new decimal( new int[ ] {
            1,
            0,
            0,
            0} );
            // 
            // _ofPageCountLabel
            // 
            this._ofPageCountLabel.Location = new System.Drawing.Point( 140 , 20 );
            this._ofPageCountLabel.Name = "_ofPageCountLabel";
            this._ofPageCountLabel.Size = new System.Drawing.Size( 78 , 18 );
            this._ofPageCountLabel.TabIndex = 2;
            this._ofPageCountLabel.Text = "of (0:d)";
            // 
            // _okButton
            // 
            this._okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._okButton.Location = new System.Drawing.Point( 59 , 47 );
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size( 75 , 23 );
            this._okButton.TabIndex = 3;
            this._okButton.Text = "&OK";
            this._okButton.UseVisualStyleBackColor = true;
            // 
            // _cancelButton
            // 
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point( 142 , 47 );
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size( 75 , 23 );
            this._cancelButton.TabIndex = 4;
            this._cancelButton.Text = "&Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            // 
            // GoToPageForm
            // 
            this.AcceptButton = this._okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F , 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._cancelButton;
            this.ClientSize = new System.Drawing.Size( 229 , 81 );
            this.Controls.Add( this._cancelButton );
            this.Controls.Add( this._okButton );
            this.Controls.Add( this._ofPageCountLabel );
            this.Controls.Add( this._pageNumericUpDown );
            this.Controls.Add( this._pageLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GoToPageForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Go To Page";
            ( ( System.ComponentModel.ISupportInitialize )( this._pageNumericUpDown ) ).EndInit( );
            this.ResumeLayout( false );
            this.PerformLayout( );

        }

        #endregion

        private System.Windows.Forms.Label _pageLabel;
        private System.Windows.Forms.NumericUpDown _pageNumericUpDown;
        private System.Windows.Forms.Label _ofPageCountLabel;
        private System.Windows.Forms.Button _okButton;
        private System.Windows.Forms.Button _cancelButton;
    }
}
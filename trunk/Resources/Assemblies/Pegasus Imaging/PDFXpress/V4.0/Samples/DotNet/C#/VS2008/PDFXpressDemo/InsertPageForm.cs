/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PDFDemo
{
    public partial class InsertForm : Form
    {
        public InsertForm()
        {
            InitializeComponent();
        }

        public bool IncludeAllData
        {
            get
            {
                return IncludeCheckBox.Checked;
            }
        }

        public bool IncludeBookmarks
        {
            get
            {
                return BookmarksCheckBox.Checked;
            }
        }

        public bool IncludeArticles
        {
            get
            {
                return ArticlesCheckBox.Checked;
            }
        }

        public int InsertPageNumber
        {
            get
            {
                return Int32.Parse(InsertTextBox.Text);
            }
        }

        public string SourceFile
        {
            get
            {
                return BrowseTextBox.Text;
            }
        }

        public bool UseAllPages
        {
            get
            {
                return UseCheckBox.Checked;
            }
        }

        public int StartPage
        {
            get
            {
                return Int32.Parse(StartTextBox.Text);
            }
        }

        public int PageCount
        {
            get
            {
                return Int32.Parse(PageCountTextBox.Text);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (BrowseTextBox.Text == "")
                {
                    MessageBox.Show("Please choose a Source Document first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult = DialogResult.OK;

                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelInsertButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Cancel;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openFileDialog = new OpenFileDialog())
                {
                    openFileDialog.Title = "Open a PDF Document";
                    openFileDialog.InitialDirectory = System.IO.Path.GetFullPath(
                        Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");
                    openFileDialog.Filter = "PDF Document (*.PDF)|*.PDF|All Files (*.*) | *.*";

                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        BrowseTextBox.Text = openFileDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void useCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (UseCheckBox.Checked == true)
                {
                    StartTextBox.Enabled = false;
                    PageCountTextBox.Enabled = false;
                }
                else
                {
                    StartTextBox.Enabled = true;
                    PageCountTextBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
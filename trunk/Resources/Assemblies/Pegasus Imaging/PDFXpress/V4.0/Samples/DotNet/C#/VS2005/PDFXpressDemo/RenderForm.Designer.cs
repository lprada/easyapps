/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class RenderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenderForm));
            this.HResLabel = new System.Windows.Forms.Label();
            this.HResBox = new System.Windows.Forms.TextBox();
            this.VResBox = new System.Windows.Forms.TextBox();
            this.VResLabel = new System.Windows.Forms.Label();
            this.SelYBox = new System.Windows.Forms.TextBox();
            this.SelYLabel = new System.Windows.Forms.Label();
            this.SelXBox = new System.Windows.Forms.TextBox();
            this.SelXLabel = new System.Windows.Forms.Label();
            this.SelHBox = new System.Windows.Forms.TextBox();
            this.SelHLabel = new System.Windows.Forms.Label();
            this.SelWBox = new System.Windows.Forms.TextBox();
            this.SelWLabel = new System.Windows.Forms.Label();
            this.ReduceCheckBox = new System.Windows.Forms.CheckBox();
            this.AnnotationCheckBox = new System.Windows.Forms.CheckBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.CancButton = new System.Windows.Forms.Button();
            this.DPILabel1 = new System.Windows.Forms.Label();
            this.DPILabel2 = new System.Windows.Forms.Label();
            this.XFDFCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // HResLabel
            // 
            this.HResLabel.AutoSize = true;
            this.HResLabel.Location = new System.Drawing.Point(62, 20);
            this.HResLabel.Name = "HResLabel";
            this.HResLabel.Size = new System.Drawing.Size(107, 13);
            this.HResLabel.TabIndex = 0;
            this.HResLabel.Text = "Horizontal Resolution";
            // 
            // HResBox
            // 
            this.HResBox.Location = new System.Drawing.Point(175, 17);
            this.HResBox.Name = "HResBox";
            this.HResBox.Size = new System.Drawing.Size(45, 20);
            this.HResBox.TabIndex = 1;
            this.HResBox.Text = "300";
            // 
            // VResBox
            // 
            this.VResBox.Location = new System.Drawing.Point(175, 43);
            this.VResBox.Name = "VResBox";
            this.VResBox.Size = new System.Drawing.Size(45, 20);
            this.VResBox.TabIndex = 4;
            this.VResBox.Text = "300";
            // 
            // VResLabel
            // 
            this.VResLabel.AutoSize = true;
            this.VResLabel.Location = new System.Drawing.Point(62, 46);
            this.VResLabel.Name = "VResLabel";
            this.VResLabel.Size = new System.Drawing.Size(95, 13);
            this.VResLabel.TabIndex = 3;
            this.VResLabel.Text = "Vertical Resolution";
            // 
            // SelYBox
            // 
            this.SelYBox.Location = new System.Drawing.Point(175, 95);
            this.SelYBox.Name = "SelYBox";
            this.SelYBox.Size = new System.Drawing.Size(45, 20);
            this.SelYBox.TabIndex = 9;
            this.SelYBox.Text = "0";
            // 
            // SelYLabel
            // 
            this.SelYLabel.AutoSize = true;
            this.SelYLabel.Location = new System.Drawing.Point(62, 98);
            this.SelYLabel.Name = "SelYLabel";
            this.SelYLabel.Size = new System.Drawing.Size(61, 13);
            this.SelYLabel.TabIndex = 8;
            this.SelYLabel.Text = "Selection Y";
            // 
            // SelXBox
            // 
            this.SelXBox.Location = new System.Drawing.Point(175, 69);
            this.SelXBox.Name = "SelXBox";
            this.SelXBox.Size = new System.Drawing.Size(45, 20);
            this.SelXBox.TabIndex = 7;
            this.SelXBox.Text = "0";
            // 
            // SelXLabel
            // 
            this.SelXLabel.AutoSize = true;
            this.SelXLabel.Location = new System.Drawing.Point(62, 72);
            this.SelXLabel.Name = "SelXLabel";
            this.SelXLabel.Size = new System.Drawing.Size(61, 13);
            this.SelXLabel.TabIndex = 6;
            this.SelXLabel.Text = "Selection X";
            // 
            // SelHBox
            // 
            this.SelHBox.Location = new System.Drawing.Point(175, 147);
            this.SelHBox.Name = "SelHBox";
            this.SelHBox.Size = new System.Drawing.Size(45, 20);
            this.SelHBox.TabIndex = 13;
            this.SelHBox.Text = "0";
            // 
            // SelHLabel
            // 
            this.SelHLabel.AutoSize = true;
            this.SelHLabel.Location = new System.Drawing.Point(62, 150);
            this.SelHLabel.Name = "SelHLabel";
            this.SelHLabel.Size = new System.Drawing.Size(85, 13);
            this.SelHLabel.TabIndex = 12;
            this.SelHLabel.Text = "Selection Height";
            // 
            // SelWBox
            // 
            this.SelWBox.Location = new System.Drawing.Point(175, 121);
            this.SelWBox.Name = "SelWBox";
            this.SelWBox.Size = new System.Drawing.Size(45, 20);
            this.SelWBox.TabIndex = 11;
            this.SelWBox.Text = "0";
            // 
            // SelWLabel
            // 
            this.SelWLabel.AutoSize = true;
            this.SelWLabel.Location = new System.Drawing.Point(62, 124);
            this.SelWLabel.Name = "SelWLabel";
            this.SelWLabel.Size = new System.Drawing.Size(82, 13);
            this.SelWLabel.TabIndex = 10;
            this.SelWLabel.Text = "Selection Width";
            // 
            // ReduceCheckBox
            // 
            this.ReduceCheckBox.AutoSize = true;
            this.ReduceCheckBox.Location = new System.Drawing.Point(105, 182);
            this.ReduceCheckBox.Name = "ReduceCheckBox";
            this.ReduceCheckBox.Size = new System.Drawing.Size(115, 17);
            this.ReduceCheckBox.TabIndex = 14;
            this.ReduceCheckBox.Text = "Reduce To Bitonal";
            this.ReduceCheckBox.UseVisualStyleBackColor = true;
            // 
            // AnnotationCheckBox
            // 
            this.AnnotationCheckBox.AutoSize = true;
            this.AnnotationCheckBox.Location = new System.Drawing.Point(18, 205);
            this.AnnotationCheckBox.Name = "AnnotationCheckBox";
            this.AnnotationCheckBox.Size = new System.Drawing.Size(120, 17);
            this.AnnotationCheckBox.TabIndex = 15;
            this.AnnotationCheckBox.Text = "Render Annotations";
            this.AnnotationCheckBox.UseVisualStyleBackColor = true;
            this.AnnotationCheckBox.CheckedChanged += new System.EventHandler(this.AnnotationCheckBox_CheckedChanged);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(18, 236);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 17;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // CancButton
            // 
            this.CancButton.Location = new System.Drawing.Point(230, 236);
            this.CancButton.Name = "CancButton";
            this.CancButton.Size = new System.Drawing.Size(75, 23);
            this.CancButton.TabIndex = 18;
            this.CancButton.Text = "Cancel";
            this.CancButton.UseVisualStyleBackColor = true;
            this.CancButton.Click += new System.EventHandler(this.CancButton_Click);
            // 
            // DPILabel1
            // 
            this.DPILabel1.AutoSize = true;
            this.DPILabel1.Location = new System.Drawing.Point(235, 20);
            this.DPILabel1.Name = "DPILabel1";
            this.DPILabel1.Size = new System.Drawing.Size(31, 13);
            this.DPILabel1.TabIndex = 2;
            this.DPILabel1.Text = "(DPI)";
            // 
            // DPILabel2
            // 
            this.DPILabel2.AutoSize = true;
            this.DPILabel2.Location = new System.Drawing.Point(235, 46);
            this.DPILabel2.Name = "DPILabel2";
            this.DPILabel2.Size = new System.Drawing.Size(31, 13);
            this.DPILabel2.TabIndex = 5;
            this.DPILabel2.Text = "(DPI)";
            // 
            // XFDFCheckBox
            // 
            this.XFDFCheckBox.AutoSize = true;
            this.XFDFCheckBox.Checked = true;
            this.XFDFCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.XFDFCheckBox.Location = new System.Drawing.Point(148, 205);
            this.XFDFCheckBox.Name = "XFDFCheckBox";
            this.XFDFCheckBox.Size = new System.Drawing.Size(157, 17);
            this.XFDFCheckBox.TabIndex = 16;
            this.XFDFCheckBox.Text = "Add Annotations from XFDF";
            this.XFDFCheckBox.UseVisualStyleBackColor = true;
            this.XFDFCheckBox.CheckedChanged += new System.EventHandler(this.XFDFCheckBox_CheckedChanged);
            // 
            // RenderForm
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 270);
            this.Controls.Add(this.XFDFCheckBox);
            this.Controls.Add(this.DPILabel2);
            this.Controls.Add(this.DPILabel1);
            this.Controls.Add(this.CancButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.AnnotationCheckBox);
            this.Controls.Add(this.ReduceCheckBox);
            this.Controls.Add(this.SelHBox);
            this.Controls.Add(this.SelHLabel);
            this.Controls.Add(this.SelWBox);
            this.Controls.Add(this.SelWLabel);
            this.Controls.Add(this.SelYBox);
            this.Controls.Add(this.SelYLabel);
            this.Controls.Add(this.SelXBox);
            this.Controls.Add(this.SelXLabel);
            this.Controls.Add(this.VResBox);
            this.Controls.Add(this.VResLabel);
            this.Controls.Add(this.HResBox);
            this.Controls.Add(this.HResLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RenderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Render Options";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HResLabel;
        private System.Windows.Forms.TextBox HResBox;
        private System.Windows.Forms.TextBox VResBox;
        private System.Windows.Forms.Label VResLabel;
        private System.Windows.Forms.TextBox SelYBox;
        private System.Windows.Forms.Label SelYLabel;
        private System.Windows.Forms.TextBox SelXBox;
        private System.Windows.Forms.Label SelXLabel;
        private System.Windows.Forms.TextBox SelHBox;
        private System.Windows.Forms.Label SelHLabel;
        private System.Windows.Forms.TextBox SelWBox;
        private System.Windows.Forms.Label SelWLabel;
        private System.Windows.Forms.CheckBox ReduceCheckBox;
        private System.Windows.Forms.CheckBox AnnotationCheckBox;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button CancButton;
        private System.Windows.Forms.Label DPILabel1;
        private System.Windows.Forms.Label DPILabel2;
        private System.Windows.Forms.CheckBox XFDFCheckBox;
    }
}
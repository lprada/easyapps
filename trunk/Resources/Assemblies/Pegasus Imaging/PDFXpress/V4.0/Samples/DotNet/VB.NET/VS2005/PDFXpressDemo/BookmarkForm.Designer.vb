'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BookmarkForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BookmarkForm))
        Me.DeleteButton = New System.Windows.Forms.Button
        Me.PageNumberTextBox = New System.Windows.Forms.TextBox
        Me.PageNumberLabel2 = New System.Windows.Forms.Label
        Me.TitleTextBox = New System.Windows.Forms.TextBox
        Me.TitleLabel2 = New System.Windows.Forms.Label
        Me.AddButton = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.TreeView1 = New System.Windows.Forms.TreeView
        Me.SuspendLayout()
        '
        'DeleteButton
        '
        Me.DeleteButton.Enabled = False
        Me.DeleteButton.Location = New System.Drawing.Point(12, 314)
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(168, 23)
        Me.DeleteButton.TabIndex = 1
        Me.DeleteButton.Text = "Delete Selected Bookmark"
        Me.DeleteButton.UseVisualStyleBackColor = True
        '
        'PageNumberTextBox
        '
        Me.PageNumberTextBox.Location = New System.Drawing.Point(286, 345)
        Me.PageNumberTextBox.Name = "PageNumberTextBox"
        Me.PageNumberTextBox.Size = New System.Drawing.Size(92, 20)
        Me.PageNumberTextBox.TabIndex = 5
        '
        'PageNumberLabel2
        '
        Me.PageNumberLabel2.AutoSize = True
        Me.PageNumberLabel2.Location = New System.Drawing.Point(208, 345)
        Me.PageNumberLabel2.Name = "PageNumberLabel2"
        Me.PageNumberLabel2.Size = New System.Drawing.Size(72, 13)
        Me.PageNumberLabel2.TabIndex = 4
        Me.PageNumberLabel2.Text = "Page Number"
        '
        'TitleTextBox
        '
        Me.TitleTextBox.Location = New System.Drawing.Point(286, 316)
        Me.TitleTextBox.Name = "TitleTextBox"
        Me.TitleTextBox.Size = New System.Drawing.Size(92, 20)
        Me.TitleTextBox.TabIndex = 3
        '
        'TitleLabel2
        '
        Me.TitleLabel2.AutoSize = True
        Me.TitleLabel2.Location = New System.Drawing.Point(208, 316)
        Me.TitleLabel2.Name = "TitleLabel2"
        Me.TitleLabel2.Size = New System.Drawing.Size(27, 13)
        Me.TitleLabel2.TabIndex = 2
        Me.TitleLabel2.Text = "Title"
        '
        'AddButton
        '
        Me.AddButton.Location = New System.Drawing.Point(259, 368)
        Me.AddButton.Name = "AddButton"
        Me.AddButton.Size = New System.Drawing.Size(119, 23)
        Me.AddButton.TabIndex = 6
        Me.AddButton.Text = "Add Bookmark"
        Me.AddButton.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(23, 402)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(90, 23)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(270, 402)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(90, 23)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(12, 12)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(366, 285)
        Me.TreeView1.TabIndex = 0
        '
        'BookmarkForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(388, 437)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PageNumberTextBox)
        Me.Controls.Add(Me.PageNumberLabel2)
        Me.Controls.Add(Me.TitleTextBox)
        Me.Controls.Add(Me.TitleLabel2)
        Me.Controls.Add(Me.AddButton)
        Me.Controls.Add(Me.DeleteButton)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "BookmarkForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bookmark"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DeleteButton As System.Windows.Forms.Button
    Friend WithEvents PageNumberTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PageNumberLabel2 As System.Windows.Forms.Label
    Friend WithEvents TitleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TitleLabel2 As System.Windows.Forms.Label
    Friend WithEvents AddButton As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
End Class

/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accusoft.PdfXpressSdk;
using Accusoft.ImagXpressSdk;
using System.IO;

namespace PDFDemo
{
    public partial class AddWatermarkForm : Form
    {
        private string fileName = "";
        private string imageFileName = "";
        private bool enableUpdates = false;
        private RenderOptions rendOpts = new RenderOptions();

        public AddWatermarkForm()
        {
            InitializeComponent();
        }

        Helper helper = new Helper();

        private Color textColor;

        public string FileName
        {
            set
            {
                fileName = value;
            }
        }

        public string ImageFileName
        {
            get
            {
                return imageFileName;
            }
        }

        public Accusoft.PdfXpressSdk.HorizontalAlignment HorzAlign
        {
            get
            {
                return (Accusoft.PdfXpressSdk.HorizontalAlignment)HorizComboBox.SelectedIndex;
            }
            set
            {
                HorizComboBox.SelectedIndex = (int)value;
            }
        }

        public Accusoft.PdfXpressSdk.VerticalAlignment VertAlign
        {
            get
            {
                return (Accusoft.PdfXpressSdk.VerticalAlignment)VertComboBox.SelectedIndex;
            }
            set
            {
                VertComboBox.SelectedIndex = (int)value;
            }
        }

        public double HorzOffset
        {
            get
            {
                return (double)HorzOffsetBox.Value;
            }
            set
            {
                HorzOffsetBox.Value = (decimal)value;
            }
        }

        public double VertOffset
        {
            get
            {
                return (double)VertOffsetBox.Value;
            }
            set
            {
                VertOffsetBox.Value = (decimal)value;
            }
        }

        public double OpacityArtifact
        {
            get
            {
                return (double)OpacityBox.Value;
            }
            set
            {
                OpacityBox.Value = (decimal)value;
            }
        }

        public double Rotation
        {
            get
            {
                return (double)RotationBox.Value;
            }
            set
            {
                RotationBox.Value = (decimal)value;
            }
        }

        public double ScaleFactor
        {
            get
            {
                return (double)ScaleBox.Value;
            }
            set
            {
                ScaleBox.Value = (decimal)value;
            }
        }

        public bool ShowOnView
        {
            get
            {
                return ViewCheckBox.Checked;
            }
            set
            {
                ViewCheckBox.Checked = value;
            }
        }

        public bool ShowOnPrint
        {
            get
            {
                return PrintCheckBox.Checked;
            }
            set
            {
                PrintCheckBox.Checked = value;
            }
        }

        public bool FixedPrint
        {
            get
            {
                return FixedCheckBox.Checked;
            }
            set
            {
                FixedCheckBox.Checked = value;
            }
        }

        public ZOrder ZOrder
        {
            get
            {
                return (ZOrder)zOrderComboBox.SelectedIndex;
            }
            set
            {
                zOrderComboBox.SelectedIndex = (int)value;
            }
        }

        public String TextArtifact
        {
            get
            {
                return WatermarkTextBox.Text;
            }
            set
            {
                WatermarkTextBox.Text = value;
            }
        }

        public Accusoft.PdfXpressSdk.HorizontalAlignment TextAlign
        {
            get
            {
                return (Accusoft.PdfXpressSdk.HorizontalAlignment)
                        TextAlignComboBox.SelectedIndex;
            }
            set
            {
                TextAlignComboBox.SelectedIndex = (int)value;
            }
        }

        public Color TextColor
        {
            get
            {
                return textColor;
            }
            set
            {
                textColor = value;
            }
        }

        public double TextSize
        {
            get
            {
                return (double)TextSizeBox.Value;
            }
            set
            {
                TextSizeBox.Value = (decimal)value;
            }
        }

        public int PageNumber
        {
            get
            {
                return (int)SourcePageBox.Value;
            }
            set
            {
                SourcePageBox.Value = value;
            }
        }

        private void UpdatePreview()
        {
            Document sourceDoc = null;

            if (enableUpdates == true)
            {
                string fileNameCopy = fileName + ".copy.pdf";

                if (helper.SafelyDeleteFile(fileNameCopy) == true)
                {
                    File.Copy(fileName, fileNameCopy);

                    try
                    {
                        using (Document doc = new Document(pdfXpress1, fileNameCopy))
                        {
                            PageBox.Maximum = doc.PageCount;

                            PageArtifactOptions po = new PageArtifactOptions();

                            po.HorizontalAlignment = (Accusoft.PdfXpressSdk.HorizontalAlignment)
                                                        HorizComboBox.SelectedIndex;
                            po.HorizontalOffset = (double)HorzOffsetBox.Value / 100;
                            po.Opacity = (double)OpacityBox.Value / 100;
                            po.Rotation = (double)RotationBox.Value;
                            po.ScaleFactor = (double)ScaleBox.Value / 100;
                            po.VerticalAlignment = (Accusoft.PdfXpressSdk.VerticalAlignment)
                                                VertComboBox.SelectedIndex;
                            po.VerticalOffset = (double)VertOffsetBox.Value / 100;
                            po.Zorder = (ZOrder)zOrderComboBox.SelectedIndex;

                            PageList pageListing = new PageList();
                            PageRange range = new PageRange((int)PageBox.Value - 1, 1);

                            pageListing.Add(range);

                            po.PageList = pageListing;

                            bool readyToAddWatermark = false;

                            if (ImageRadioButton.Checked == true && ImageFileName != "")
                            {
                                sourceDoc = new Document(pdfXpress1, imageFileName);

                                po.SourcePageNumber = (int)SourcePageBox.Value - 1;
                                po.SourceDocument = sourceDoc;

                                SourcePageBox.Maximum = sourceDoc.PageCount;

                                po.ArtifactType = TypeOfArtifact.SourceWatermark;

                                readyToAddWatermark = true;
                            }
                            else if (TextRadioButton.Checked == true && WatermarkTextBox.Text != "")
                            {
                                po.Text = WatermarkTextBox.Text;
                                po.TextAlignment = (Accusoft.PdfXpressSdk.HorizontalAlignment)
                                                    TextAlignComboBox.SelectedIndex;
                                po.TextColor = ColorButton.BackColor;
                                po.TextSize = (double)TextSizeBox.Value;

                                po.ArtifactType = TypeOfArtifact.TextWatermark;

                                readyToAddWatermark = true;
                            }

                            if (readyToAddWatermark == true)
                            {
                                doc.AddWatermark(po);
                            }

                            using (Bitmap bp = doc.RenderPageToBitmap((int)PageBox.Value - 1, rendOpts))
                            {
                                imageXView1.AllowUpdate = false;

                                if (imageXView1.Image != null)
                                {
                                    imageXView1.Image.Dispose();
                                    imageXView1.Image = null;
                                }

                                imageXView1.Image = ImageX.FromBitmap(imagXpress1, bp);

                                imageXView1.AllowUpdate = true;
                            }
                        }
                    }
                    finally
                    {
                        imageXView1.AllowUpdate = true;

                        //cleanup
                        helper.SafelyDeleteFile(fileNameCopy);

                        if (sourceDoc != null)
                        {
                            sourceDoc.Dispose();
                        }
                    }
                }
            }
        }

        private void AddWatermarkForm_Load(object sender, EventArgs e)
        {
            try
            {
                HorizComboBox.SelectedIndex = 0;
                VertComboBox.SelectedIndex = 0;
                TextAlignComboBox.SelectedIndex = 0;
                zOrderComboBox.SelectedIndex = 1;

                rendOpts.RenderAnnotations = true;
                rendOpts.ProduceDibSection = false;

                enableUpdates = true;

                //***Must call the UnlockRuntime method to unlock the control
                //The unlock codes shown below are for formatting purposes only and will not work!
                //pdfXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);
                //imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

                pdfXpress1.Initialize();

                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ImageRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ImageTextBox.Enabled = BrowseButton.Enabled = SourcePageBox.Enabled = ImageRadioButton.Checked;

                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                WatermarkTextBox.Enabled = ColorButton.Enabled = TextSizeBox.Enabled
                                = TextAlignComboBox.Enabled = TextRadioButton.Checked;

                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }

        private void ColorButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = textColor;

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        textColor = dlg.Color;
                        ColorButton.BackColor = textColor;

                        UpdatePreview();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            Close();
        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.FilterIndex = 0;
                    dlg.Filter = "PDF Files (*.pdf)|*.pdf|All Files (*.*)|*.*";
                    dlg.InitialDirectory = System.IO.Path.GetFullPath(Application.StartupPath
                                                              + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        imageFileName = ImageTextBox.Text = dlg.FileName;

                        SourcePageBox.Value = 1;

                        UpdatePreview();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextAlignComboBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextSizeBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void sourcePageBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void HorizComboBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void zOrderComboBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void VertComboBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void HorzOffsetBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void VertOffsetBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ScaleBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpacityBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RotationBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pageBox_Leave(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pageBox_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                UpdatePreview();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
'***************************************************************
' * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
' * This sample code is provided to Pegasus licensees "as is"    *
' * with no restrictions on use or modification. No warranty for *
' * use of this sample code is provided by Pegasus.              *
' ***************************************************************
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InsertForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.includeCheckBox = New System.Windows.Forms.CheckBox
        Me.bookmarksCheckBox = New System.Windows.Forms.CheckBox
        Me.articlesCheckBox = New System.Windows.Forms.CheckBox
        Me.InsertLabel = New System.Windows.Forms.Label
        Me.insertTextBox = New System.Windows.Forms.TextBox
        Me.browseTextBox = New System.Windows.Forms.TextBox
        Me.SourceLabel = New System.Windows.Forms.Label
        Me.useCheckBox = New System.Windows.Forms.CheckBox
        Me.browseButton = New System.Windows.Forms.Button
        Me.RangeGroupBox = New System.Windows.Forms.GroupBox
        Me.pageCountTextBox = New System.Windows.Forms.TextBox
        Me.CountLabel = New System.Windows.Forms.Label
        Me.startTextBox = New System.Windows.Forms.TextBox
        Me.StartLabel = New System.Windows.Forms.Label
        Me.OKButton = New System.Windows.Forms.Button
        Me.CancelInsertButton = New System.Windows.Forms.Button
        Me.RangeGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'includeCheckBox
        '
        Me.includeCheckBox.AutoSize = True
        Me.includeCheckBox.Location = New System.Drawing.Point(12, 12)
        Me.includeCheckBox.Name = "includeCheckBox"
        Me.includeCheckBox.Size = New System.Drawing.Size(150, 17)
        Me.includeCheckBox.TabIndex = 0
        Me.includeCheckBox.Text = "Include all Document data"
        Me.includeCheckBox.UseVisualStyleBackColor = True
        '
        'bookmarksCheckBox
        '
        Me.bookmarksCheckBox.AutoSize = True
        Me.bookmarksCheckBox.Location = New System.Drawing.Point(12, 35)
        Me.bookmarksCheckBox.Name = "bookmarksCheckBox"
        Me.bookmarksCheckBox.Size = New System.Drawing.Size(117, 17)
        Me.bookmarksCheckBox.TabIndex = 1
        Me.bookmarksCheckBox.Text = "Include Bookmarks"
        Me.bookmarksCheckBox.UseVisualStyleBackColor = True
        '
        'articlesCheckBox
        '
        Me.articlesCheckBox.AutoSize = True
        Me.articlesCheckBox.Location = New System.Drawing.Point(12, 63)
        Me.articlesCheckBox.Name = "articlesCheckBox"
        Me.articlesCheckBox.Size = New System.Drawing.Size(135, 17)
        Me.articlesCheckBox.TabIndex = 3
        Me.articlesCheckBox.Text = "Include Article Threads"
        Me.articlesCheckBox.UseVisualStyleBackColor = True
        '
        'InsertLabel
        '
        Me.InsertLabel.AutoSize = True
        Me.InsertLabel.Location = New System.Drawing.Point(165, 44)
        Me.InsertLabel.Name = "InsertLabel"
        Me.InsertLabel.Size = New System.Drawing.Size(113, 13)
        Me.InsertLabel.TabIndex = 2
        Me.InsertLabel.Text = "Insert at Page Number"
        '
        'insertTextBox
        '
        Me.insertTextBox.Location = New System.Drawing.Point(168, 60)
        Me.insertTextBox.Name = "insertTextBox"
        Me.insertTextBox.Size = New System.Drawing.Size(100, 20)
        Me.insertTextBox.TabIndex = 4
        Me.insertTextBox.Text = "1"
        '
        'browseTextBox
        '
        Me.browseTextBox.Location = New System.Drawing.Point(15, 114)
        Me.browseTextBox.Name = "browseTextBox"
        Me.browseTextBox.ReadOnly = True
        Me.browseTextBox.Size = New System.Drawing.Size(267, 20)
        Me.browseTextBox.TabIndex = 6
        '
        'SourceLabel
        '
        Me.SourceLabel.Location = New System.Drawing.Point(12, 98)
        Me.SourceLabel.Name = "SourceLabel"
        Me.SourceLabel.Size = New System.Drawing.Size(93, 13)
        Me.SourceLabel.TabIndex = 5
        Me.SourceLabel.Text = "Source Document"
        '
        'useCheckBox
        '
        Me.useCheckBox.AutoSize = True
        Me.useCheckBox.Checked = True
        Me.useCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.useCheckBox.Location = New System.Drawing.Point(74, 77)
        Me.useCheckBox.Name = "useCheckBox"
        Me.useCheckBox.Size = New System.Drawing.Size(90, 17)
        Me.useCheckBox.TabIndex = 4
        Me.useCheckBox.Text = "Use all pages"
        Me.useCheckBox.UseVisualStyleBackColor = True
        '
        'browseButton
        '
        Me.browseButton.Location = New System.Drawing.Point(288, 111)
        Me.browseButton.Name = "browseButton"
        Me.browseButton.Size = New System.Drawing.Size(75, 23)
        Me.browseButton.TabIndex = 7
        Me.browseButton.Text = "Browse"
        Me.browseButton.UseVisualStyleBackColor = True
        '
        'RangeGroupBox
        '
        Me.RangeGroupBox.Controls.Add(Me.pageCountTextBox)
        Me.RangeGroupBox.Controls.Add(Me.CountLabel)
        Me.RangeGroupBox.Controls.Add(Me.startTextBox)
        Me.RangeGroupBox.Controls.Add(Me.StartLabel)
        Me.RangeGroupBox.Controls.Add(Me.useCheckBox)
        Me.RangeGroupBox.Location = New System.Drawing.Point(12, 155)
        Me.RangeGroupBox.Name = "RangeGroupBox"
        Me.RangeGroupBox.Size = New System.Drawing.Size(221, 100)
        Me.RangeGroupBox.TabIndex = 8
        Me.RangeGroupBox.TabStop = False
        Me.RangeGroupBox.Text = "Page Range"
        '
        'pageCountTextBox
        '
        Me.pageCountTextBox.Location = New System.Drawing.Point(109, 48)
        Me.pageCountTextBox.Name = "pageCountTextBox"
        Me.pageCountTextBox.Size = New System.Drawing.Size(100, 20)
        Me.pageCountTextBox.TabIndex = 3
        Me.pageCountTextBox.Text = "1"
        '
        'CountLabel
        '
        Me.CountLabel.AutoSize = True
        Me.CountLabel.Location = New System.Drawing.Point(6, 48)
        Me.CountLabel.Name = "CountLabel"
        Me.CountLabel.Size = New System.Drawing.Size(63, 13)
        Me.CountLabel.TabIndex = 2
        Me.CountLabel.Text = "Page Count"
        '
        'startTextBox
        '
        Me.startTextBox.Location = New System.Drawing.Point(109, 22)
        Me.startTextBox.Name = "startTextBox"
        Me.startTextBox.Size = New System.Drawing.Size(100, 20)
        Me.startTextBox.TabIndex = 1
        Me.startTextBox.Text = "1"
        '
        'StartLabel
        '
        Me.StartLabel.AutoSize = True
        Me.StartLabel.Location = New System.Drawing.Point(6, 22)
        Me.StartLabel.Name = "StartLabel"
        Me.StartLabel.Size = New System.Drawing.Size(97, 13)
        Me.StartLabel.TabIndex = 0
        Me.StartLabel.Text = "Start Page Number"
        '
        'OKButton
        '
        Me.OKButton.Location = New System.Drawing.Point(40, 269)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(75, 23)
        Me.OKButton.TabIndex = 9
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'CancelInsertButton
        '
        Me.CancelInsertButton.Location = New System.Drawing.Point(246, 269)
        Me.CancelInsertButton.Name = "CancelInsertButton"
        Me.CancelInsertButton.Size = New System.Drawing.Size(75, 23)
        Me.CancelInsertButton.TabIndex = 10
        Me.CancelInsertButton.Text = "Cancel"
        Me.CancelInsertButton.UseVisualStyleBackColor = True
        '
        'InsertForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 304)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.CancelInsertButton)
        Me.Controls.Add(Me.RangeGroupBox)
        Me.Controls.Add(Me.browseButton)
        Me.Controls.Add(Me.SourceLabel)
        Me.Controls.Add(Me.browseTextBox)
        Me.Controls.Add(Me.insertTextBox)
        Me.Controls.Add(Me.InsertLabel)
        Me.Controls.Add(Me.articlesCheckBox)
        Me.Controls.Add(Me.bookmarksCheckBox)
        Me.Controls.Add(Me.includeCheckBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "InsertForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Insert Page"
        Me.RangeGroupBox.ResumeLayout(False)
        Me.RangeGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents includeCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents bookmarksCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents articlesCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents InsertLabel As System.Windows.Forms.Label
    Friend WithEvents insertTextBox As System.Windows.Forms.TextBox
    Friend WithEvents browseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SourceLabel As System.Windows.Forms.Label
    Friend WithEvents useCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents browseButton As System.Windows.Forms.Button
    Friend WithEvents RangeGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents pageCountTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CountLabel As System.Windows.Forms.Label
    Friend WithEvents startTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StartLabel As System.Windows.Forms.Label
    Friend WithEvents OKButton As System.Windows.Forms.Button
    Friend WithEvents CancelInsertButton As System.Windows.Forms.Button
End Class

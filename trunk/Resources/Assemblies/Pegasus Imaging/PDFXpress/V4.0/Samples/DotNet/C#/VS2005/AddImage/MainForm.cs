//****************************************************************'
//* Copyright 2010- Pegasus Imaging Corporation, Tampa Florida. *'
//* This sample code is provided to Pegasus licensees "as is"    *'
//* with no restrictions on use or modification. No warranty for *'
//* use of this sample code is provided by Pegasus.              *'
//****************************************************************'
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Accusoft.PdfXpressSdk;

namespace AddImage
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private System.String inputFilename;
		private System.String outputFilename;

		private System.Windows.Forms.Button AddFromFileButton;
		private System.Windows.Forms.OpenFileDialog OpenDialog;
        private System.Windows.Forms.SaveFileDialog SaveDialog;
		private System.Windows.Forms.MainMenu MainMenu;
		private System.Windows.Forms.MenuItem AboutMenu;
        private System.Windows.Forms.MenuItem PDFXpressMenuItem;
        private MenuItem FileMenu;
        private MenuItem ExitMenuItem;
        private ListBox DescriptionBox;
        private Button AddFromByteButton;
        private Button AddFromHandleButton;
        private Button AddFromDataButton;
        private PdfXpress pdfXpress;
        private IContainer components;

		public MainForm()
		{		
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();		
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (pdfXpress != null)
                {
                    pdfXpress.Dispose();
                    pdfXpress = null;
                }
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.AddFromFileButton = new System.Windows.Forms.Button();
            this.OpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.SaveDialog = new System.Windows.Forms.SaveFileDialog();
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.PDFXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.DescriptionBox = new System.Windows.Forms.ListBox();
            this.AddFromByteButton = new System.Windows.Forms.Button();
            this.AddFromHandleButton = new System.Windows.Forms.Button();
            this.AddFromDataButton = new System.Windows.Forms.Button();
            this.pdfXpress = new Accusoft.PdfXpressSdk.PdfXpress();
            this.SuspendLayout();
            // 
            // AddFromFileButton
            // 
            this.AddFromFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddFromFileButton.Location = new System.Drawing.Point(116, 89);
            this.AddFromFileButton.Name = "AddFromFileButton";
            this.AddFromFileButton.Size = new System.Drawing.Size(185, 29);
            this.AddFromFileButton.TabIndex = 1;
            this.AddFromFileButton.Text = "Add Image from File";
            this.AddFromFileButton.Click += new System.EventHandler(this.AddImageButton1_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 0;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 1;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.PDFXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // PDFXpressMenuItem
            // 
            this.PDFXpressMenuItem.Index = 0;
            this.PDFXpressMenuItem.Text = "&PDF Xpress";
            this.PDFXpressMenuItem.Click += new System.EventHandler(this.PDFXpressMenuItem_Click);
            // 
            // DescriptionBox
            // 
            this.DescriptionBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionBox.FormattingEnabled = true;
            this.DescriptionBox.Items.AddRange(new object[] {
            "This sample demonstrates how an image file can be used to create ",
            "a single-page PDF.  Click the Add Image button to select an image and ",
            "specify the location to save the generated PDF."});
            this.DescriptionBox.Location = new System.Drawing.Point(12, 12);
            this.DescriptionBox.Name = "DescriptionBox";
            this.DescriptionBox.Size = new System.Drawing.Size(414, 56);
            this.DescriptionBox.TabIndex = 0;
            // 
            // AddFromByteButton
            // 
            this.AddFromByteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddFromByteButton.Location = new System.Drawing.Point(116, 140);
            this.AddFromByteButton.Name = "AddFromByteButton";
            this.AddFromByteButton.Size = new System.Drawing.Size(185, 29);
            this.AddFromByteButton.TabIndex = 2;
            this.AddFromByteButton.Text = "Add Image from Byte Array";
            this.AddFromByteButton.Click += new System.EventHandler(this.AddImageButton2_Click);
            // 
            // AddFromHandleButton
            // 
            this.AddFromHandleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddFromHandleButton.Location = new System.Drawing.Point(116, 192);
            this.AddFromHandleButton.Name = "AddFromHandleButton";
            this.AddFromHandleButton.Size = new System.Drawing.Size(185, 29);
            this.AddFromHandleButton.TabIndex = 3;
            this.AddFromHandleButton.Text = "Add Image from Handle";
            this.AddFromHandleButton.Click += new System.EventHandler(this.AddImageButton3_Click);
            // 
            // AddFromDataButton
            // 
            this.AddFromDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddFromDataButton.Location = new System.Drawing.Point(116, 248);
            this.AddFromDataButton.Name = "AddFromDataButton";
            this.AddFromDataButton.Size = new System.Drawing.Size(185, 29);
            this.AddFromDataButton.TabIndex = 4;
            this.AddFromDataButton.Text = "Add Image from DataInfo";
            this.AddFromDataButton.Click += new System.EventHandler(this.AddImageButton4_Click);
            // 
            // pdfXpress
            // 
            this.pdfXpress.Debug = false;
            this.pdfXpress.DebugLogFile = "";
            this.pdfXpress.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(437, 310);
            this.Controls.Add(this.AddFromDataButton);
            this.Controls.Add(this.AddFromHandleButton);
            this.Controls.Add(this.AddFromByteButton);
            this.Controls.Add(this.DescriptionBox);
            this.Controls.Add(this.AddFromFileButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu;
            this.Name = "MainForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Image";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private DialogResult GetInputFile()
		{
            try
            {
                OpenDialog.Filter = "All Image files (*.*)|*.*";
               
                String strImageDir = System.IO.Path.GetFullPath(Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");
                OpenDialog.InitialDirectory = strImageDir;

                OpenDialog.FilterIndex = 1;
                OpenDialog.RestoreDirectory = true;
                OpenDialog.Title = "Choose an Image";

                if (OpenDialog.ShowDialog() == DialogResult.OK)
                {

                    if (OpenDialog.FileName != "")
                    {
                        inputFilename = OpenDialog.FileName;
                    }

                    return DialogResult.OK;
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DialogResult.Cancel;
            }
		}

        private DialogResult GetOutputFile()
		{
            try
            {
                SaveDialog.Filter = "PDF file (*.pdf)|*.pdf";
                SaveDialog.FilterIndex = 1;
                SaveDialog.RestoreDirectory = true;
                SaveDialog.Title = "Save PDF Document";

                if (SaveDialog.ShowDialog() == DialogResult.OK)
                {

                    if (SaveDialog.FileName != "")
                    {
                        outputFilename = SaveDialog.FileName;
                    }
                    return DialogResult.OK;
                }
                else
                {
                    return DialogResult.Cancel;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DialogResult.Cancel;
            }
		}

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                pdfXpress = new PdfXpress();

                pdfXpress.Initialize();

                // TODO: Change the 4 runtime unlock codes below to those purchased once a license agreement is in place.
                // To purchase runtime licensing for this control, please contact sales@accusoft.com.
                //pdfXpress.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

                Application.EnableVisualStyles();

            }
            catch (PdfXpressLibraryException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PDFXpressMenuItem_Click(object sender, EventArgs e)
        {
            pdfXpress.AboutBox();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AddImageButton1_Click(object sender, EventArgs e)
        {
            try
            {
                // Get the paths for the input image and output PDF file
                if (GetInputFile() == DialogResult.OK)
                {
                    if (GetOutputFile() == DialogResult.OK)
                    {

                        // Create the output PDF document
                        Document outputDocument = new Document(pdfXpress);

                        // Create the output page
                        PageOptions pageOptions = new PageOptions();
                        outputDocument.CreatePage(-1, pageOptions); // we want to create page 0, so we specify the page "before"

                        // Create the location of the image on the page
                        double destinationX = 0.25 * 72.0; // Quarter inch margins around image
                        double destinationY = 0.25 * 72.0;
                        double destinationWidth = pageOptions.MediaWidth - 0.5 * 72.0;
                        double destinationHeight = pageOptions.MediaHeight - 0.5 * 72.0;

                        // Define the image fit
                        ImageFitSettings destinationFit = ImageFitSettings.Shrink;

                        // Add the image to the output page
                        outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight,
                            destinationFit, inputFilename, 0);

                        // Save the output PDF document
                        SaveOptions saveOptions = new SaveOptions();
                        saveOptions.Filename = outputFilename;
                        outputDocument.Save(saveOptions);
                    }
                }
            }
            catch (PdfXpressLibraryException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }		
        }

        private void AddImageButton2_Click(object sender, EventArgs e)
        {
            try
            {
                // Get the paths for the input image and output PDF file
                if (GetInputFile() == DialogResult.OK)
                {
                    if (GetOutputFile() == DialogResult.OK)
                    {


                        System.IO.FileStream fs = new System.IO.FileStream(inputFilename, System.IO.FileMode.Open);

                        byte[] byteData = new byte[fs.Length];
                        fs.Read(byteData, 0, byteData.Length);
                        fs.Close();

                        // Create the output PDF document
                        Document outputDocument = new Document(pdfXpress);

                        // Create the output page
                        PageOptions pageOptions = new PageOptions();
                        outputDocument.CreatePage(-1, pageOptions); // we want to create page 0, so we specify the page "before"

                        // Create the location of the image on the page
                        double destinationX = 0.25 * 72.0; // Quarter inch margins around image
                        double destinationY = 0.25 * 72.0;
                        double destinationWidth = pageOptions.MediaWidth - 0.5 * 72.0;
                        double destinationHeight = pageOptions.MediaHeight - 0.5 * 72.0;

                        // Define the image fit
                        ImageFitSettings destinationFit = ImageFitSettings.Shrink;

                        // Add the image to the output page
                        outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight,
                            destinationFit, byteData, 0);

                        // Save the output PDF document
                        SaveOptions saveOptions = new SaveOptions();
                        saveOptions.Filename = outputFilename;
                        outputDocument.Save(saveOptions);
                    }
                }
            }
            catch (PdfXpressLibraryException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }	
        }

        private void AddImageButton3_Click(object sender, EventArgs e)
        {
            System.IntPtr dataHandle = new IntPtr();
            try
            {
                // Get the paths for the input image and output PDF file
                if (GetInputFile() == DialogResult.OK)
                {
                    if (GetOutputFile() == DialogResult.OK)
                    {


                        System.IO.FileStream fs = new System.IO.FileStream(inputFilename, System.IO.FileMode.Open);

                        byte[] byteData = new byte[fs.Length];
                        fs.Read(byteData, 0, byteData.Length);
                        fs.Close();

                        dataHandle = System.Runtime.InteropServices.Marshal.AllocHGlobal(byteData.Length);

                        System.Runtime.InteropServices.Marshal.Copy(byteData, 0, dataHandle, byteData.Length);

                        // Create the output PDF document
                        Document outputDocument = new Document(pdfXpress);

                        // Create the output page
                        PageOptions pageOptions = new PageOptions();
                        outputDocument.CreatePage(-1, pageOptions); // we want to create page 0, so we specify the page "before"

                        // Create the location of the image on the page
                        double destinationX = 0.25 * 72.0; // Quarter inch margins around image
                        double destinationY = 0.25 * 72.0;
                        double destinationWidth = pageOptions.MediaWidth - 0.5 * 72.0;
                        double destinationHeight = pageOptions.MediaHeight - 0.5 * 72.0;

                        // Define the image fit
                        ImageFitSettings destinationFit = ImageFitSettings.Shrink;

                        // Add the image to the output page
                        outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight,
                            destinationFit, dataHandle, 0);

                        // Save the output PDF document
                        SaveOptions saveOptions = new SaveOptions();
                        saveOptions.Filename = outputFilename;
                        outputDocument.Save(saveOptions);
                    }
                }
            }
            catch (PdfXpressLibraryException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.FreeHGlobal(dataHandle);
            }
        }

        private void AddImageButton4_Click(object sender, EventArgs e)
        {
            try
            {
                // Get the paths for the input image and output PDF file
                if (GetOutputFile() == DialogResult.OK)
                {
                    pdfXpress.Documents.Add(Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\pdfxpress3.pdf");

                    RenderOptions opts = new RenderOptions();

                    ImageDataInfo dataInfo = pdfXpress.Documents[0].RenderPageToImageData(0, opts);

                    // Create the output PDF document
                    Document outputDocument = new Document(pdfXpress);

                    // Create the output page
                    PageOptions pageOptions = new PageOptions();
                    outputDocument.CreatePage(-1, pageOptions); // we want to create page 0, so we specify the page "before"

                    // Create the location of the image on the page
                    double destinationX = 0.25 * 72.0; // Quarter inch margins around image
                    double destinationY = 0.25 * 72.0;
                    double destinationWidth = pageOptions.MediaWidth - 0.5 * 72.0;
                    double destinationHeight = pageOptions.MediaHeight - 0.5 * 72.0;

                    // Define the image fit
                    ImageFitSettings destinationFit = ImageFitSettings.Shrink;

                    // Add the image to the output page
                    outputDocument.AddImage(0, destinationX, destinationY, destinationWidth, destinationHeight,
                        destinationFit, dataInfo);

                    // Save the output PDF document
                    SaveOptions saveOptions = new SaveOptions();
                    saveOptions.Filename = outputFilename;
                    outputDocument.Save(saveOptions);
                }
            }
            catch (PdfXpressLibraryException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }         
        }
	}
}

/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace PDFDemo
{
	/// <summary>
	/// Summary description for FormSplash.
	/// </summary>
	public class SplashForm : System.Windows.Forms.Form
    {
		public System.Windows.Forms.GroupBox MainFrame;
        public System.Windows.Forms.Button ContinueButton;
        public System.Windows.Forms.Label ProductNameLabel;
		public System.Windows.Forms.Label UrlLabel;
		public System.Windows.Forms.Label CompanyLabel;
        public System.Windows.Forms.Label CopyrightLabel;
		private System.Windows.Forms.PictureBox Logo;
        private Label DescriptionLabel;
        private Label PDFLabel3;
        private Label PDFLabel2;
        private Label PDFLabel1;
        public Button PurchaseButton;
        public Button DownloadButton;
		private System.ComponentModel.IContainer components;

		public SplashForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
            this.MainFrame = new System.Windows.Forms.GroupBox();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.PurchaseButton = new System.Windows.Forms.Button();
            this.ProductNameLabel = new System.Windows.Forms.Label();
            this.DownloadButton = new System.Windows.Forms.Button();
            this.PDFLabel3 = new System.Windows.Forms.Label();
            this.PDFLabel2 = new System.Windows.Forms.Label();
            this.PDFLabel1 = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.UrlLabel = new System.Windows.Forms.Label();
            this.CompanyLabel = new System.Windows.Forms.Label();
            this.CopyrightLabel = new System.Windows.Forms.Label();
            this.MainFrame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // MainFrame
            // 
            this.MainFrame.BackColor = System.Drawing.Color.White;
            this.MainFrame.Controls.Add(this.Logo);
            this.MainFrame.Controls.Add(this.PurchaseButton);
            this.MainFrame.Controls.Add(this.ProductNameLabel);
            this.MainFrame.Controls.Add(this.DownloadButton);
            this.MainFrame.Controls.Add(this.PDFLabel3);
            this.MainFrame.Controls.Add(this.PDFLabel2);
            this.MainFrame.Controls.Add(this.PDFLabel1);
            this.MainFrame.Controls.Add(this.DescriptionLabel);
            this.MainFrame.Controls.Add(this.ContinueButton);
            this.MainFrame.Controls.Add(this.UrlLabel);
            this.MainFrame.Controls.Add(this.CompanyLabel);
            this.MainFrame.Controls.Add(this.CopyrightLabel);
            this.MainFrame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainFrame.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainFrame.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MainFrame.Location = new System.Drawing.Point(0, -10);
            this.MainFrame.Name = "MainFrame";
            this.MainFrame.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MainFrame.Size = new System.Drawing.Size(719, 442);
            this.MainFrame.TabIndex = 0;
            this.MainFrame.TabStop = false;
            // 
            // Logo
            // 
            this.Logo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(12, 32);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(213, 180);
            this.Logo.TabIndex = 20;
            this.Logo.TabStop = false;
            // 
            // PurchaseButton
            // 
            this.PurchaseButton.BackColor = System.Drawing.Color.White;
            this.PurchaseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PurchaseButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PurchaseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.PurchaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PurchaseButton.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PurchaseButton.ForeColor = System.Drawing.Color.White;
            this.PurchaseButton.Image = ((System.Drawing.Image)(resources.GetObject("PurchaseButton.Image")));
            this.PurchaseButton.Location = new System.Drawing.Point(291, 314);
            this.PurchaseButton.Name = "PurchaseButton";
            this.PurchaseButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PurchaseButton.Size = new System.Drawing.Size(272, 59);
            this.PurchaseButton.TabIndex = 13;
            this.PurchaseButton.UseVisualStyleBackColor = false;
            this.PurchaseButton.Click += new System.EventHandler(this.PurchaseButton_Click);
            // 
            // ProductNameLabel
            // 
            this.ProductNameLabel.BackColor = System.Drawing.Color.White;
            this.ProductNameLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.ProductNameLabel.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductNameLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ProductNameLabel.Location = new System.Drawing.Point(231, 32);
            this.ProductNameLabel.Name = "ProductNameLabel";
            this.ProductNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ProductNameLabel.Size = new System.Drawing.Size(481, 78);
            this.ProductNameLabel.TabIndex = 0;
            this.ProductNameLabel.Tag = "Product";
            this.ProductNameLabel.Text = "Welcome to the PDF Xpress v4 Demo";
            this.ProductNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DownloadButton
            // 
            this.DownloadButton.BackColor = System.Drawing.Color.White;
            this.DownloadButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DownloadButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DownloadButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.DownloadButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.DownloadButton.FlatAppearance.BorderSize = 0;
            this.DownloadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DownloadButton.Font = new System.Drawing.Font("Arial Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DownloadButton.ForeColor = System.Drawing.Color.White;
            this.DownloadButton.Image = ((System.Drawing.Image)(resources.GetObject("DownloadButton.Image")));
            this.DownloadButton.Location = new System.Drawing.Point(291, 250);
            this.DownloadButton.Name = "DownloadButton";
            this.DownloadButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DownloadButton.Size = new System.Drawing.Size(272, 58);
            this.DownloadButton.TabIndex = 12;
            this.DownloadButton.UseVisualStyleBackColor = false;
            this.DownloadButton.Click += new System.EventHandler(this.DownloadButton_Click);
            // 
            // PDFLabel3
            // 
            this.PDFLabel3.BackColor = System.Drawing.Color.White;
            this.PDFLabel3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PDFLabel3.Location = new System.Drawing.Point(301, 208);
            this.PDFLabel3.Name = "PDFLabel3";
            this.PDFLabel3.Size = new System.Drawing.Size(301, 26);
            this.PDFLabel3.TabIndex = 10;
            this.PDFLabel3.Text = "�  Searching and Highlighting text";
            // 
            // PDFLabel2
            // 
            this.PDFLabel2.BackColor = System.Drawing.Color.White;
            this.PDFLabel2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PDFLabel2.Location = new System.Drawing.Point(301, 181);
            this.PDFLabel2.Name = "PDFLabel2";
            this.PDFLabel2.Size = new System.Drawing.Size(301, 26);
            this.PDFLabel2.TabIndex = 9;
            this.PDFLabel2.Text = "�  Merging and Splitting PDF files";
            // 
            // PDFLabel1
            // 
            this.PDFLabel1.BackColor = System.Drawing.Color.White;
            this.PDFLabel1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PDFLabel1.Location = new System.Drawing.Point(301, 154);
            this.PDFLabel1.Name = "PDFLabel1";
            this.PDFLabel1.Size = new System.Drawing.Size(353, 26);
            this.PDFLabel1.TabIndex = 8;
            this.PDFLabel1.Text = "�  PDF Annotations, Bookmarks, and Metadata";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.BackColor = System.Drawing.Color.White;
            this.DescriptionLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLabel.Location = new System.Drawing.Point(234, 110);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(443, 42);
            this.DescriptionLabel.TabIndex = 1;
            this.DescriptionLabel.Text = "Quickly build applications to create PDFs, including:";
            this.DescriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContinueButton
            // 
            this.ContinueButton.BackColor = System.Drawing.SystemColors.Control;
            this.ContinueButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.ContinueButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ContinueButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ContinueButton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ContinueButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ContinueButton.Location = new System.Drawing.Point(612, 390);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ContinueButton.Size = new System.Drawing.Size(82, 29);
            this.ContinueButton.TabIndex = 7;
            this.ContinueButton.Text = "Continue";
            this.ContinueButton.UseVisualStyleBackColor = false;
            // 
            // UrlLabel
            // 
            this.UrlLabel.AccessibleDescription = "0";
            this.UrlLabel.AutoSize = true;
            this.UrlLabel.BackColor = System.Drawing.Color.White;
            this.UrlLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.UrlLabel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UrlLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UrlLabel.Location = new System.Drawing.Point(23, 409);
            this.UrlLabel.Name = "UrlLabel";
            this.UrlLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.UrlLabel.Size = new System.Drawing.Size(105, 14);
            this.UrlLabel.TabIndex = 6;
            this.UrlLabel.Tag = "Warning";
            this.UrlLabel.Text = "www.accusoft.com";
            this.UrlLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CompanyLabel
            // 
            this.CompanyLabel.AccessibleDescription = "0";
            this.CompanyLabel.BackColor = System.Drawing.Color.White;
            this.CompanyLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.CompanyLabel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CompanyLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CompanyLabel.Location = new System.Drawing.Point(23, 392);
            this.CompanyLabel.Name = "CompanyLabel";
            this.CompanyLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CompanyLabel.Size = new System.Drawing.Size(177, 17);
            this.CompanyLabel.TabIndex = 5;
            this.CompanyLabel.Tag = "Company";
            this.CompanyLabel.Text = "Accusoft Pegasus";
            this.CompanyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CopyrightLabel
            // 
            this.CopyrightLabel.AccessibleDescription = "0";
            this.CopyrightLabel.BackColor = System.Drawing.Color.White;
            this.CopyrightLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this.CopyrightLabel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CopyrightLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CopyrightLabel.Location = new System.Drawing.Point(23, 375);
            this.CopyrightLabel.Name = "CopyrightLabel";
            this.CopyrightLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CopyrightLabel.Size = new System.Drawing.Size(177, 17);
            this.CopyrightLabel.TabIndex = 4;
            this.CopyrightLabel.Tag = "Copyright";
            this.CopyrightLabel.Text = "Copyright � 1997-2010";
            this.CopyrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SplashForm
            // 
            this.AcceptButton = this.ContinueButton;
            this.AutoScaleBaseSize = new System.Drawing.Size(11, 24);
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.ContinueButton;
            this.ClientSize = new System.Drawing.Size(717, 429);
            this.Controls.Add(this.MainFrame);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PDF Xpress v4 Demo";
            this.MainFrame.ResumeLayout(false);
            this.MainFrame.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void PurchaseButton_Click(object sender, EventArgs e)
        {
            try
            {
                string theUrl = "http://www.pegasusimaging.com/pdfxpresspricing.htm";
                System.Diagnostics.Process.Start(theUrl);
            }
            catch (Win32Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DownloadButton_Click(object sender, EventArgs e)
        {
            try
            {
                string theUrl = "http://www.pegasusimaging.com/pdfxpressdownload.htm";
                System.Diagnostics.Process.Start(theUrl);
            }
            catch (Win32Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Drawing;
using System.Globalization;
using System.Text;
using Accusoft.PdfXpressSdk;

namespace PDFDemo
{
    internal class CreateXfdfTextAnnotationOptions
    {
        private float _userSpaceDpi = 72.0f;
        
        public float UserSpaceDpi
        {
            get { return this._userSpaceDpi; }
            set { this._userSpaceDpi = value; }
        }
        
        private Int32 _page = 0;
        
        public Int32 Page
        {
            get { return this._page; }
            set { this._page = value; }
        }
        
        private RectangleF _rectangle = new RectangleF(0f, 0f, 72f, 72f);
        
        public RectangleF Rectangle
        {
            get { return this._rectangle; }
            set { this._rectangle = value; }
        }
        
        private RotateAngle _rotateAngle = RotateAngle.Rotate0;
        
        public RotateAngle RotateAngle
        {
            get { return this._rotateAngle; }
            set { this._rotateAngle = value; }
        }
    }

    internal class CreateXfdfUnderlineAnnotationOptions
    {
        private float _userSpaceDpi = 72.0f;
        
        public float UserSpaceDpi
        {
            get { return this._userSpaceDpi; }
            set { this._userSpaceDpi = value; }
        }
        
        private Int32 _page = 0;
        
        public Int32 Page
        {
            get { return this._page; }
            set { this._page = value; }
        }
        
        private RectangleF _rectangle = new RectangleF(0f, 0f, 72f, 72f);
        
        public RectangleF Rectangle
        {
            get { return this._rectangle; }
            set { this._rectangle = value; }
        }
        
        private RotateAngle _rotateAngle = RotateAngle.Rotate0;
        
        public RotateAngle RotateAngle
        {
            get { return this._rotateAngle; }
            set { this._rotateAngle = value; }
        }
    }
    internal class CreateXfdfStrikeoutAnnotationOptions
    {
        private float _userSpaceDpi = 72.0f;
        
        public float UserSpaceDpi
        {
            get { return this._userSpaceDpi; }
            set { this._userSpaceDpi = value; }
        }
        
        private Int32 _page = 0;
        
        public Int32 Page
        {
            get { return this._page; }
            set { this._page = value; }
        }
        
        private RectangleF _rectangle = new RectangleF(0f, 0f, 72f, 72f);
        
        public RectangleF Rectangle
        {
            get { return this._rectangle; }
            set { this._rectangle = value; }
        }
        
        private RotateAngle _rotateAngle = RotateAngle.Rotate0;
        
        public RotateAngle RotateAngle
        {
            get { return this._rotateAngle; }
            set { this._rotateAngle = value; }
        }
    }
    internal class XfdfSnippets
    {
        /// <summary>
        /// Creates an XFDF packet to express a single underline annotation.
        /// </summary>
        /// <param name="options">Parameters used to create the XFDF text element.</param>
        /// <returns>An XFDF packet consisting of a single text annotation.</returns>
        public static byte[]
        CreateXfdfUnderlineAnnotation(CreateXfdfUnderlineAnnotationOptions options)
        {
            // Format an XFDF string to produce a
            // PDF Underline Annotation (with associated PDF Popup Annotation) 
            // on a page in the PDF document.
            //
            const String underlineAnnotationFormatString = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>"
                        + "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">"
                        + "<annots>"
                        + "<underline rect=\"{0},{1},{2},{3}\""
                        + " rotation=\"{18}\""
                        + " creationdate=\"{4}\""
                        + " name=\"{5}\""
                        + " opacity=\"{6}\""
                        + " color=\"{7}\""
                        + " flags=\"print\""
                        + " date=\"{8}\""
                        + " icon=\"{9}\""
                        + " title=\"{10}\""
                        + " subject=\"{11}\""
                        + " coords=\"{0},{1},{2},{1},{0},{3},{2},{3}\""
                        + " page=\"{12}\">"
                        + "<contents-richtext>"
                        + "<body xmlns=\"http://www.w3.org/1999/xhtml\""
                        + " xmlns:xfa=\"http://www.xfa.org/schema/xfa-data/1.0/\""
                        + " xfa:APIVersion=\"Acrobat:7.0.0\" xfa:spec=\"2.0.2\">"
                        + "<p>"
                        + "<span style=\"text-decoration:;font-size:10.0pt\">{13}</span>"
                        + "</p></body>"
                        + "</contents-richtext>"
                        + "<contents>{13}</contents>"
                        + "<popup rect=\"{14},{15},{16},{17}\""
                        + " flags=\"print,nozoom,norotate\""
                        + " open=\"yes\""
                        + " page=\"{12}\"/>"
                        + "</underline>"
                        + "</annots>"
                        + "<f href=\"~acroTmp.tmp\"/>"
                        + "<ids"
                        + " original=\"A4019D12C651A341A26067775AAA8351\""
                        + " modified=\"F7361C2072F4BB4CA928796B7B09A838\"/>"
                        + "</xfdf>";
            String xfdfNameString = Guid.NewGuid().ToString();
            String xfdfDateString = ToXfdfString(DateTime.Now);
            String xfdfColorString = ToXfdfString(Color.RoyalBlue);
            String xfdfRotatedString = ToXfdfString(options.RotateAngle);
            String xfdfString = String.Format
                        (NumberFormatInfo.CurrentInfo
                        , underlineAnnotationFormatString
                        , new object[] { options.Rectangle.Left        // underline rectangle left, userspace units
                                        , options.Rectangle.Bottom      // underline rectangle bottom, userspace units
                                        , options.Rectangle.Right       // underline rectangle right, userspace units
                                        , options.Rectangle.Top         // underline rectangle top, userspace units
                                        , xfdfDateString                // underline created on date
                                        , xfdfNameString                // underline name 
                                        , 0.5                           // underline opacity
                                        , xfdfColorString               // underline color
                                        , xfdfDateString                // underline last modified date
                                        , "Comment"                     // underline iconName
                                        , Environment.UserName          // underline title
                                        , "Underline"                   // underline subject
                                        , options.Page                  // underline page index
                                        , "Hello World!"                // underline message
                                        , options.Rectangle.Right       // popup rectangle left, userspace units
                                        , options.Rectangle.Bottom      // popup rectangle bottom, userspace units
                                        , options.Rectangle.Right  + 2.0 * options.UserSpaceDpi     // popup rectangle right, userspace units
                                        , options.Rectangle.Bottom + 1.0 * options.UserSpaceDpi     // popup rectangle top, userspace units
                                        , xfdfRotatedString             // rotate, multiples of 90
                                        });
            return UnicodeEncoding.Unicode.GetBytes(xfdfString);
        }
        /// <summary>
        /// Creates an XFDF packet to express a single text annotation.
        /// </summary>
        /// <param name="options">Parameters used to create the XFDF text element.</param>
        /// <returns>An XFDF packet consisting of a single text annotation.</returns>
        public static byte[]
        CreateXfdfStrikeoutAnnotation(CreateXfdfStrikeoutAnnotationOptions options)
        {
            // Format an XFDF string to produce a
            // PDF Strikeout Annotation (with associated PDF Popup Annotation) 
            // on a page in the PDF document.
            //
            const String strikeoutAnnotationFormatString = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>"
                        + "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">"
                        + "<annots>"
                        + "<strikeout rect=\"{0},{1},{2},{3}\""
                        + " rotation=\"{18}\""
                        + " creationdate=\"{4}\""
                        + " name=\"{5}\""
                        + " opacity=\"{6}\""
                        + " color=\"{7}\""
                        + " flags=\"print\""
                        + " date=\"{8}\""
                        + " icon=\"{9}\""
                        + " title=\"{10}\""
                        + " subject=\"{11}\""
                        + " coords=\"{0},{1},{2},{1},{0},{3},{2},{3}\""
                        + " page=\"{12}\">"
                        + "<contents-richtext>"
                        + "<body xmlns=\"http://www.w3.org/1999/xhtml\""
                        + " xmlns:xfa=\"http://www.xfa.org/schema/xfa-data/1.0/\""
                        + " xfa:APIVersion=\"Acrobat:7.0.0\" xfa:spec=\"2.0.2\">"
                        + "<p>"
                        + "<span style=\"text-decoration:;font-size:10.0pt\">{13}</span>"
                        + "</p></body>"
                        + "</contents-richtext>"
                        + "<contents>{13}</contents>"
                        + "<popup rect=\"{14},{15},{16},{17}\""
                        + " flags=\"print,nozoom,norotate\""
                        + " open=\"yes\""
                        + " page=\"{12}\"/>"
                        + "</strikeout>"
                        + "</annots>"
                        + "<f href=\"~acroTmp.tmp\"/>"
                        + "<ids"
                        + " original=\"A4019D12C651A341A26067775AAA8351\""
                        + " modified=\"F7361C2072F4BB4CA928796B7B09A838\"/>"
                        + "</xfdf>";
            String xfdfNameString = Guid.NewGuid().ToString();
            String xfdfDateString = ToXfdfString(DateTime.Now);
            String xfdfColorString = ToXfdfString(Color.Red);
            String xfdfRotatedString = ToXfdfString(options.RotateAngle);
            String xfdfString = String.Format
                        (NumberFormatInfo.CurrentInfo
                        , strikeoutAnnotationFormatString
                        , new object[] { options.Rectangle.Left        // strikeout rectangle left, userspace units
                                        , options.Rectangle.Bottom      // strikeout rectangle bottom, userspace units
                                        , options.Rectangle.Right       // strikeout rectangle right, userspace units
                                        , options.Rectangle.Top         // strikeout rectangle top, userspace units
                                        , xfdfDateString                // strikeout created on date
                                        , xfdfNameString                // strikeout name 
                                        , 0.5                           // strikeout opacity
                                        , xfdfColorString               // strikeout color
                                        , xfdfDateString                // strikeout last modified date
                                        , "Comment"                     // strikeout iconName
                                        , Environment.UserName          // strikeout title
                                        , "Cross-Out"                   // strikeout subject
                                        , options.Page                  // strikeout page index
                                        , "Hello World!"                // strikeout message
                                        , options.Rectangle.Right       // popup rectangle left, userspace units
                                        , options.Rectangle.Bottom      // popup rectangle bottom, userspace units
                                        , options.Rectangle.Right  + 2.0 * options.UserSpaceDpi     // popup rectangle right, userspace units
                                        , options.Rectangle.Bottom + 1.0 * options.UserSpaceDpi     // popup rectangle top, userspace units
                                        , xfdfRotatedString             // rotate, multiples of 90
                                        });
            return UnicodeEncoding.Unicode.GetBytes(xfdfString);
        }
        public static String ToXfdfString(RotateAngle angle)
        {
            switch (angle)
            {
                case RotateAngle.Rotate0:
                    return "0";
                case RotateAngle.Rotate90:
                    return "90";
                case RotateAngle.Rotate180:
                    return "180";
                case RotateAngle.Rotate270:
                    return "270";
                default:
                    return "";
            }
        }
        /// <summary>
        /// Express a Color object instance as an XFDF color string.
        /// </summary>
        /// <param name="color">A Color object instance.</param>
        /// <returns>An XFDF color string.</returns>
        public static String ToXfdfString(Color color)
        {
            String xfdfColorFormatString = "#{0:X2}{1:X2}{2:X2}";
            String xfdfColorString = String.Format
                                                        (NumberFormatInfo.CurrentInfo
                                                        , xfdfColorFormatString
                                                        , color.R
                                                        , color.G
                                                        , color.B
                                                        );
            return xfdfColorString;
        }
        /// <summary>
        /// Express a DateTime object instance as a PDF date string.
        /// Refer to the PDF Specification, Section 3.8.3 Dates.
        /// </summary>
        /// <param name="dateTime">A DateTime object instance.</param>
        /// <returns>A PDF Date string.</returns>
        public static String ToXfdfString(DateTime dateTime)
        {
            DateTime utcDateTime = dateTime.ToUniversalTime();
            TimeSpan utfOffsetTimeSpan = dateTime.Subtract(utcDateTime);
            String pdfDateFormatString =
                "D:{0:yyyy}{0:MM}{0:dd}{0:HH}{0:mm}{0:ss}{0:zz}'{1:D2}'";
            String xfdfDateString = String.Format
                                                        (DateTimeFormatInfo.CurrentInfo
                                                        , pdfDateFormatString
                                                        , dateTime
                                                        , utfOffsetTimeSpan.Minutes
                                                        );
            return xfdfDateString;
        }
    }
}

'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Public Class MoveForm

    Public ReadOnly Property MoveFrom() As Integer
        Get
            Return CType(MoveTextBox.Text, Integer)
        End Get
    End Property

    Public ReadOnly Property MoveTo() As Integer
        Get
            Return CType(ToTextBox.Text, Integer)
        End Get
    End Property

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.OK

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CancelMoveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelMoveButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.Cancel

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class ImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageForm));
            this.DesHLabel = new System.Windows.Forms.Label();
            this.DesWLabel = new System.Windows.Forms.Label();
            this.DesYLabel = new System.Windows.Forms.Label();
            this.DesXLabel = new System.Windows.Forms.Label();
            this.FitComboBox = new System.Windows.Forms.ComboBox();
            this.FitLabel = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.CButton = new System.Windows.Forms.Button();
            this.ImageBox = new System.Windows.Forms.TextBox();
            this.TopLabel = new System.Windows.Forms.Label();
            this.ImageButton = new System.Windows.Forms.Button();
            this.DesXBox = new System.Windows.Forms.NumericUpDown();
            this.DesYBox = new System.Windows.Forms.NumericUpDown();
            this.DesHBox = new System.Windows.Forms.NumericUpDown();
            this.DesWBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.DesXBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesHBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesWBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DesHLabel
            // 
            this.DesHLabel.AutoSize = true;
            this.DesHLabel.Location = new System.Drawing.Point(126, 148);
            this.DesHLabel.Name = "DesHLabel";
            this.DesHLabel.Size = new System.Drawing.Size(94, 13);
            this.DesHLabel.TabIndex = 9;
            this.DesHLabel.Text = "Destination Height";
            // 
            // DesWLabel
            // 
            this.DesWLabel.AutoSize = true;
            this.DesWLabel.Location = new System.Drawing.Point(126, 122);
            this.DesWLabel.Name = "DesWLabel";
            this.DesWLabel.Size = new System.Drawing.Size(91, 13);
            this.DesWLabel.TabIndex = 7;
            this.DesWLabel.Text = "Destination Width";
            // 
            // DesYLabel
            // 
            this.DesYLabel.AutoSize = true;
            this.DesYLabel.Location = new System.Drawing.Point(126, 96);
            this.DesYLabel.Name = "DesYLabel";
            this.DesYLabel.Size = new System.Drawing.Size(70, 13);
            this.DesYLabel.TabIndex = 5;
            this.DesYLabel.Text = "Destination Y";
            // 
            // DesXLabel
            // 
            this.DesXLabel.AutoSize = true;
            this.DesXLabel.Location = new System.Drawing.Point(126, 70);
            this.DesXLabel.Name = "DesXLabel";
            this.DesXLabel.Size = new System.Drawing.Size(70, 13);
            this.DesXLabel.TabIndex = 3;
            this.DesXLabel.Text = "Destination X";
            // 
            // FitComboBox
            // 
            this.FitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FitComboBox.FormattingEnabled = true;
            this.FitComboBox.Items.AddRange(new object[] {
            "None",
            "Stretch",
            "Shrink",
            "Grow",
            "GravityLeft",
            "GravityTop",
            "GravityRight",
            "GravityBottom"});
            this.FitComboBox.Location = new System.Drawing.Point(239, 172);
            this.FitComboBox.Name = "FitComboBox";
            this.FitComboBox.Size = new System.Drawing.Size(121, 21);
            this.FitComboBox.TabIndex = 12;
            // 
            // FitLabel
            // 
            this.FitLabel.AutoSize = true;
            this.FitLabel.Location = new System.Drawing.Point(126, 175);
            this.FitLabel.Name = "FitLabel";
            this.FitLabel.Size = new System.Drawing.Size(91, 13);
            this.FitLabel.TabIndex = 11;
            this.FitLabel.Text = "Image Fit Settings";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(299, 210);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 13;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CButton
            // 
            this.CButton.Location = new System.Drawing.Point(392, 210);
            this.CButton.Name = "CButton";
            this.CButton.Size = new System.Drawing.Size(75, 23);
            this.CButton.TabIndex = 14;
            this.CButton.Text = "Cancel";
            this.CButton.UseVisualStyleBackColor = true;
            this.CButton.Click += new System.EventHandler(this.CButton_Click);
            // 
            // ImageBox
            // 
            this.ImageBox.Enabled = false;
            this.ImageBox.Location = new System.Drawing.Point(12, 30);
            this.ImageBox.Name = "ImageBox";
            this.ImageBox.Size = new System.Drawing.Size(451, 20);
            this.ImageBox.TabIndex = 2;
            // 
            // TopLabel
            // 
            this.TopLabel.AutoSize = true;
            this.TopLabel.Location = new System.Drawing.Point(12, 9);
            this.TopLabel.Name = "TopLabel";
            this.TopLabel.Size = new System.Drawing.Size(115, 13);
            this.TopLabel.TabIndex = 0;
            this.TopLabel.Text = "Chooose Image to Add";
            // 
            // ImageButton
            // 
            this.ImageButton.Location = new System.Drawing.Point(388, 4);
            this.ImageButton.Name = "ImageButton";
            this.ImageButton.Size = new System.Drawing.Size(75, 23);
            this.ImageButton.TabIndex = 1;
            this.ImageButton.Text = "Browse";
            this.ImageButton.UseVisualStyleBackColor = true;
            this.ImageButton.Click += new System.EventHandler(this.imageButton_Click);
            // 
            // DesXBox
            // 
            this.DesXBox.Location = new System.Drawing.Point(239, 68);
            this.DesXBox.Name = "DesXBox";
            this.DesXBox.Size = new System.Drawing.Size(46, 20);
            this.DesXBox.TabIndex = 4;
            // 
            // DesYBox
            // 
            this.DesYBox.Location = new System.Drawing.Point(239, 94);
            this.DesYBox.Name = "DesYBox";
            this.DesYBox.Size = new System.Drawing.Size(46, 20);
            this.DesYBox.TabIndex = 6;
            // 
            // DesHBox
            // 
            this.DesHBox.Location = new System.Drawing.Point(239, 147);
            this.DesHBox.Name = "DesHBox";
            this.DesHBox.Size = new System.Drawing.Size(46, 20);
            this.DesHBox.TabIndex = 10;
            // 
            // DesWBox
            // 
            this.DesWBox.Location = new System.Drawing.Point(239, 121);
            this.DesWBox.Name = "DesWBox";
            this.DesWBox.Size = new System.Drawing.Size(46, 20);
            this.DesWBox.TabIndex = 8;
            // 
            // ImageForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 241);
            this.Controls.Add(this.DesHBox);
            this.Controls.Add(this.DesWBox);
            this.Controls.Add(this.DesYBox);
            this.Controls.Add(this.DesXBox);
            this.Controls.Add(this.ImageButton);
            this.Controls.Add(this.TopLabel);
            this.Controls.Add(this.ImageBox);
            this.Controls.Add(this.CButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.FitLabel);
            this.Controls.Add(this.FitComboBox);
            this.Controls.Add(this.DesHLabel);
            this.Controls.Add(this.DesWLabel);
            this.Controls.Add(this.DesYLabel);
            this.Controls.Add(this.DesXLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ImageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Image";
            this.Load += new System.EventHandler(this.ImageForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DesXBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesHBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesWBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DesHLabel;
        private System.Windows.Forms.Label DesWLabel;
        private System.Windows.Forms.Label DesYLabel;
        private System.Windows.Forms.Label DesXLabel;
        private System.Windows.Forms.Label FitLabel;
        public System.Windows.Forms.ComboBox FitComboBox;
        private System.Windows.Forms.Label TopLabel;
        private System.Windows.Forms.Button ImageButton;
        public System.Windows.Forms.TextBox ImageBox;
        public System.Windows.Forms.Button OKButton;
        public System.Windows.Forms.Button CButton;
        public System.Windows.Forms.NumericUpDown DesXBox;
        public System.Windows.Forms.NumericUpDown DesYBox;
        public System.Windows.Forms.NumericUpDown DesHBox;
        public System.Windows.Forms.NumericUpDown DesWBox;
    }
}
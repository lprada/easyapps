/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class InsertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InsertForm));
            this.IncludeCheckBox = new System.Windows.Forms.CheckBox();
            this.BookmarksCheckBox = new System.Windows.Forms.CheckBox();
            this.ArticlesCheckBox = new System.Windows.Forms.CheckBox();
            this.InsertTextBox = new System.Windows.Forms.TextBox();
            this.InsertLabel = new System.Windows.Forms.Label();
            this.BrowseTextBox = new System.Windows.Forms.TextBox();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.SourceLabel = new System.Windows.Forms.Label();
            this.RangeGroupBox = new System.Windows.Forms.GroupBox();
            this.UseCheckBox = new System.Windows.Forms.CheckBox();
            this.PageCountTextBox = new System.Windows.Forms.TextBox();
            this.CountLabel = new System.Windows.Forms.Label();
            this.StartTextBox = new System.Windows.Forms.TextBox();
            this.StartLabel = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelInsertButton = new System.Windows.Forms.Button();
            this.RangeGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // IncludeCheckBox
            // 
            this.IncludeCheckBox.AutoSize = true;
            this.IncludeCheckBox.Location = new System.Drawing.Point(12, 12);
            this.IncludeCheckBox.Name = "IncludeCheckBox";
            this.IncludeCheckBox.Size = new System.Drawing.Size(150, 17);
            this.IncludeCheckBox.TabIndex = 0;
            this.IncludeCheckBox.Text = "Include all Document data";
            this.IncludeCheckBox.UseVisualStyleBackColor = true;
            // 
            // BookmarksCheckBox
            // 
            this.BookmarksCheckBox.AutoSize = true;
            this.BookmarksCheckBox.Location = new System.Drawing.Point(12, 35);
            this.BookmarksCheckBox.Name = "BookmarksCheckBox";
            this.BookmarksCheckBox.Size = new System.Drawing.Size(117, 17);
            this.BookmarksCheckBox.TabIndex = 1;
            this.BookmarksCheckBox.Text = "Include Bookmarks";
            this.BookmarksCheckBox.UseVisualStyleBackColor = true;
            // 
            // ArticlesCheckBox
            // 
            this.ArticlesCheckBox.AutoSize = true;
            this.ArticlesCheckBox.Location = new System.Drawing.Point(12, 58);
            this.ArticlesCheckBox.Name = "ArticlesCheckBox";
            this.ArticlesCheckBox.Size = new System.Drawing.Size(135, 17);
            this.ArticlesCheckBox.TabIndex = 3;
            this.ArticlesCheckBox.Text = "Include Article Threads";
            this.ArticlesCheckBox.UseVisualStyleBackColor = true;
            // 
            // InsertTextBox
            // 
            this.InsertTextBox.Location = new System.Drawing.Point(158, 55);
            this.InsertTextBox.Name = "InsertTextBox";
            this.InsertTextBox.Size = new System.Drawing.Size(100, 20);
            this.InsertTextBox.TabIndex = 4;
            this.InsertTextBox.Text = "1";
            // 
            // InsertLabel
            // 
            this.InsertLabel.AutoSize = true;
            this.InsertLabel.Location = new System.Drawing.Point(155, 36);
            this.InsertLabel.Name = "InsertLabel";
            this.InsertLabel.Size = new System.Drawing.Size(113, 13);
            this.InsertLabel.TabIndex = 2;
            this.InsertLabel.Text = "Insert at Page Number";
            // 
            // BrowseTextBox
            // 
            this.BrowseTextBox.Location = new System.Drawing.Point(9, 105);
            this.BrowseTextBox.Name = "BrowseTextBox";
            this.BrowseTextBox.ReadOnly = true;
            this.BrowseTextBox.Size = new System.Drawing.Size(187, 20);
            this.BrowseTextBox.TabIndex = 6;
            // 
            // BrowseButton
            // 
            this.BrowseButton.Location = new System.Drawing.Point(202, 103);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(75, 23);
            this.BrowseButton.TabIndex = 7;
            this.BrowseButton.Text = "Browse";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // SourceLabel
            // 
            this.SourceLabel.Location = new System.Drawing.Point(9, 89);
            this.SourceLabel.Name = "SourceLabel";
            this.SourceLabel.Size = new System.Drawing.Size(106, 13);
            this.SourceLabel.TabIndex = 5;
            this.SourceLabel.Text = "Source Document";
            // 
            // RangeGroupBox
            // 
            this.RangeGroupBox.Controls.Add(this.UseCheckBox);
            this.RangeGroupBox.Controls.Add(this.PageCountTextBox);
            this.RangeGroupBox.Controls.Add(this.CountLabel);
            this.RangeGroupBox.Controls.Add(this.StartTextBox);
            this.RangeGroupBox.Controls.Add(this.StartLabel);
            this.RangeGroupBox.Location = new System.Drawing.Point(9, 142);
            this.RangeGroupBox.Name = "RangeGroupBox";
            this.RangeGroupBox.Size = new System.Drawing.Size(227, 112);
            this.RangeGroupBox.TabIndex = 8;
            this.RangeGroupBox.TabStop = false;
            this.RangeGroupBox.Text = "Page Range";
            // 
            // UseCheckBox
            // 
            this.UseCheckBox.AutoSize = true;
            this.UseCheckBox.Checked = true;
            this.UseCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UseCheckBox.Location = new System.Drawing.Point(65, 83);
            this.UseCheckBox.Name = "UseCheckBox";
            this.UseCheckBox.Size = new System.Drawing.Size(90, 17);
            this.UseCheckBox.TabIndex = 4;
            this.UseCheckBox.Text = "Use all pages";
            this.UseCheckBox.UseVisualStyleBackColor = true;
            this.UseCheckBox.CheckedChanged += new System.EventHandler(this.useCheckBox_CheckedChanged);
            // 
            // PageCountTextBox
            // 
            this.PageCountTextBox.Enabled = false;
            this.PageCountTextBox.Location = new System.Drawing.Point(111, 52);
            this.PageCountTextBox.Name = "PageCountTextBox";
            this.PageCountTextBox.Size = new System.Drawing.Size(100, 20);
            this.PageCountTextBox.TabIndex = 3;
            this.PageCountTextBox.Text = "1";
            // 
            // CountLabel
            // 
            this.CountLabel.AutoSize = true;
            this.CountLabel.Location = new System.Drawing.Point(6, 55);
            this.CountLabel.Name = "CountLabel";
            this.CountLabel.Size = new System.Drawing.Size(66, 13);
            this.CountLabel.TabIndex = 2;
            this.CountLabel.Text = "Page Count:";
            // 
            // StartTextBox
            // 
            this.StartTextBox.Enabled = false;
            this.StartTextBox.Location = new System.Drawing.Point(111, 26);
            this.StartTextBox.Name = "StartTextBox";
            this.StartTextBox.Size = new System.Drawing.Size(100, 20);
            this.StartTextBox.TabIndex = 1;
            this.StartTextBox.Text = "1";
            // 
            // StartLabel
            // 
            this.StartLabel.AutoSize = true;
            this.StartLabel.Location = new System.Drawing.Point(6, 29);
            this.StartLabel.Name = "StartLabel";
            this.StartLabel.Size = new System.Drawing.Size(100, 13);
            this.StartLabel.TabIndex = 0;
            this.StartLabel.Text = "Start Page Number:";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(18, 270);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 9;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelInsertButton
            // 
            this.CancelInsertButton.Location = new System.Drawing.Point(183, 270);
            this.CancelInsertButton.Name = "CancelInsertButton";
            this.CancelInsertButton.Size = new System.Drawing.Size(75, 23);
            this.CancelInsertButton.TabIndex = 10;
            this.CancelInsertButton.Text = "Cancel";
            this.CancelInsertButton.UseVisualStyleBackColor = true;
            this.CancelInsertButton.Click += new System.EventHandler(this.CancelInsertButton_Click);
            // 
            // InsertForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 302);
            this.Controls.Add(this.CancelInsertButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.RangeGroupBox);
            this.Controls.Add(this.SourceLabel);
            this.Controls.Add(this.BrowseButton);
            this.Controls.Add(this.BrowseTextBox);
            this.Controls.Add(this.InsertLabel);
            this.Controls.Add(this.InsertTextBox);
            this.Controls.Add(this.ArticlesCheckBox);
            this.Controls.Add(this.BookmarksCheckBox);
            this.Controls.Add(this.IncludeCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "InsertForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert Page";
            this.RangeGroupBox.ResumeLayout(false);
            this.RangeGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InsertTextBox;
        private System.Windows.Forms.Label InsertLabel;
        private System.Windows.Forms.TextBox BrowseTextBox;
        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.Label SourceLabel;
        private System.Windows.Forms.GroupBox RangeGroupBox;
        private System.Windows.Forms.CheckBox UseCheckBox;
        private System.Windows.Forms.TextBox PageCountTextBox;
        private System.Windows.Forms.Label CountLabel;
        private System.Windows.Forms.TextBox StartTextBox;
        private System.Windows.Forms.Label StartLabel;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelInsertButton;
        public System.Windows.Forms.CheckBox IncludeCheckBox;
        public System.Windows.Forms.CheckBox BookmarksCheckBox;
        public System.Windows.Forms.CheckBox ArticlesCheckBox;
    }
}
/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class MoveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MoveForm));
            this.MoveTextBox = new System.Windows.Forms.TextBox();
            this.MoveLabel2 = new System.Windows.Forms.Label();
            this.MoveLabel = new System.Windows.Forms.Label();
            this.ToTextBox = new System.Windows.Forms.TextBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelMoveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MoveTextBox
            // 
            this.MoveTextBox.Location = new System.Drawing.Point(120, 6);
            this.MoveTextBox.Name = "MoveTextBox";
            this.MoveTextBox.Size = new System.Drawing.Size(100, 20);
            this.MoveTextBox.TabIndex = 1;
            this.MoveTextBox.Text = "1";
            // 
            // MoveLabel2
            // 
            this.MoveLabel2.AutoSize = true;
            this.MoveLabel2.Location = new System.Drawing.Point(12, 9);
            this.MoveLabel2.Name = "MoveLabel2";
            this.MoveLabel2.Size = new System.Drawing.Size(102, 13);
            this.MoveLabel2.TabIndex = 0;
            this.MoveLabel2.Text = "Move Page Number";
            // 
            // MoveLabel
            // 
            this.MoveLabel.AutoSize = true;
            this.MoveLabel.Location = new System.Drawing.Point(12, 32);
            this.MoveLabel.Name = "MoveLabel";
            this.MoveLabel.Size = new System.Drawing.Size(88, 13);
            this.MoveLabel.TabIndex = 2;
            this.MoveLabel.Text = "To Page Number";
            // 
            // ToTextBox
            // 
            this.ToTextBox.Location = new System.Drawing.Point(120, 32);
            this.ToTextBox.Name = "ToTextBox";
            this.ToTextBox.Size = new System.Drawing.Size(100, 20);
            this.ToTextBox.TabIndex = 3;
            this.ToTextBox.Text = "1";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(15, 66);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelMoveButton
            // 
            this.CancelMoveButton.Location = new System.Drawing.Point(135, 66);
            this.CancelMoveButton.Name = "CancelMoveButton";
            this.CancelMoveButton.Size = new System.Drawing.Size(75, 23);
            this.CancelMoveButton.TabIndex = 5;
            this.CancelMoveButton.Text = "Cancel";
            this.CancelMoveButton.UseVisualStyleBackColor = true;
            this.CancelMoveButton.Click += new System.EventHandler(this.CancelMoveButton_Click);
            // 
            // MoveForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(229, 100);
            this.Controls.Add(this.CancelMoveButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.MoveLabel);
            this.Controls.Add(this.ToTextBox);
            this.Controls.Add(this.MoveLabel2);
            this.Controls.Add(this.MoveTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MoveForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Move Page";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MoveTextBox;
        private System.Windows.Forms.Label MoveLabel2;
        private System.Windows.Forms.Label MoveLabel;
        private System.Windows.Forms.TextBox ToTextBox;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelMoveButton;
    }
}
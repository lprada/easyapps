'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Imports Accusoft.PdfXpressSdk

Public Class BookmarkForm

    Dim bookmarksDeletedPageNumber As ArrayList
    Dim bookmarksDeletedTitle As ArrayList
    Dim bookmarksAddedTitle As ArrayList
    Dim bookmarksAddedPageNumber As ArrayList
    Dim pageNumber As Integer
    Dim pageName As String
    Dim pageCount As Integer

    Public ReadOnly Property BookmarksDeletedPageNumberProperty() As ArrayList
        Get
            Return bookmarksDeletedPageNumber
        End Get
    End Property
    Public ReadOnly Property BookmarksDeletedTitleProperty() As ArrayList
        Get
            Return bookmarksDeletedTitle
        End Get
    End Property
    Public ReadOnly Property BookmarksAddedTitleProperty() As ArrayList
        Get
            Return bookmarksAddedTitle
        End Get
    End Property
    Public ReadOnly Property BookmarksAddedPageNumberProperty() As ArrayList
        Get
            Return bookmarksAddedPageNumber
        End Get
    End Property

    Public Sub New(ByVal count As Integer, ByVal treeview As TreeView)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        TreeView1.BeginUpdate()

        If Not (treeview Is Nothing) Then
            Dim i As Integer
            For i = 0 To treeview.Nodes.Count - 1
                TreeView1.Nodes.Add(CType(treeview.Nodes(i).Clone(), TreeNode))
            Next i
        End If

        TreeView1.EndUpdate()

        pageCount = count

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Function Node(ByVal nodechosen As TreeNode) As Boolean
        If (nodechosen.IsSelected = True) Then

            bookmarksDeletedPageNumber.Add(nodechosen.Name)
            bookmarksDeletedTitle.Add(nodechosen.Text)

            Dim j As Integer

            For j = 0 To bookmarksAddedPageNumber.Count - 1
                If (nodechosen.Name = CInt(CInt(bookmarksAddedPageNumber(j)) + 1)).ToString() And _
                       nodechosen.Text = bookmarksAddedTitle(j).ToString() Then

                    bookmarksAddedTitle.RemoveAt(j)
                    bookmarksAddedPageNumber.RemoveAt(j)
                End If
            Next j

            TreeView1.Nodes.Remove(nodechosen)
            DeleteButton.Enabled = False

            Return True
        Else
            If Not (nodechosen.Nodes Is Nothing) Then
                Dim i As Integer
                For i = 0 To nodechosen.Nodes.Count - 1
                    If (Node(nodechosen.Nodes(i)) = True) Then
                        Return True
                    End If
                Next i
            End If
        End If

        Return False
    End Function

    Private Sub DeleteButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteButton.Click
        Try
            Dim j As Integer
            For j = 0 To TreeView1.Nodes.Count - 1
                If (Node(TreeView1.Nodes(j)) = True) Then
                    Exit For
                End If
            Next j
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddButton.Click
        Try
            If Not (PageNumberTextBox.Text = "" And TitleTextBox.Text = "") Then
                Try
                    Dim number As Integer = Int32.Parse(PageNumberTextBox.Text)

                    If (number <= pageCount) Then
                        bookmarksAddedPageNumber.Add(number - 1)
                        bookmarksAddedTitle.Add(TitleTextBox.Text)

                        Dim Node As TreeNode = New TreeNode()
                        Node.Text = TitleTextBox.Text
                        Node.Name = PageNumberTextBox.Text

                        TreeView1.Nodes.Add(Node)
                    Else
                        MessageBox.Show("Please specify a bookmark to add whose " _
                            & "PageNumber is less than or equal to the total page count of: " + pageCount.ToString())
                    End If
                Catch
                    MessageBox.Show("Please specify a number only for the Page Number of the bookmark you want to add.")
                End Try
            Else
                MessageBox.Show("Please specify a Page Number and Title of the bookmark you want to add.")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        DialogResult = Windows.Forms.DialogResult.OK

        Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        DialogResult = Windows.Forms.DialogResult.Cancel

        Close()
    End Sub

    Private Sub TreeView1_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeView1.Leave
        If (DeleteButton.Focused = False) Then
            DeleteButton.Enabled = False
        End If
    End Sub

    Private Sub TreeView1_NodeMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseClick
        Try            
            If (e.X >= e.Node.Bounds.X) Then
                If (Int32.Parse(e.Node.Name) > -1) Then
                    DeleteButton.Enabled = True

                    pageNumber = Int32.Parse(e.Node.Name)
                    pageName = e.Node.Text
                    Dim oldString As String
                    oldString = "(page " + (pageNumber + 1).ToString() + ")"
                    pageName = pageName.Replace(oldString, "").Trim()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BookmarkForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bookmarksDeletedTitle = New ArrayList()
        bookmarksDeletedPageNumber = New ArrayList()
        bookmarksAddedTitle = New ArrayList()
        bookmarksAddedPageNumber = New ArrayList()

        If (TreeView1.Nodes.Count = 0) Then
            DeleteButton.Enabled = False
        End If
    End Sub

    Private Sub DeleteButton_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DeleteButton.MouseUp
        DeleteButton.Enabled = False
    End Sub
End Class
/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PDFDemo
{
    public partial class MoveForm : Form
    {
        public MoveForm()
        {
            InitializeComponent();
        }

        public int MoveFrom
        {
            get
            {
                return Int32.Parse(MoveTextBox.Text);
            }
        }

        public int MoveTo
        {
            get
            {
                return Int32.Parse(ToTextBox.Text);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelMoveButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Cancel;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
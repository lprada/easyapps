/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accusoft.PdfXpressSdk;

namespace PDFDemo
{
    public partial class ImageForm : Form
    {
        public int Settings
        {
            get
            {
                return FitComboBox.SelectedIndex;
            }
        }

        public string Image
        {
            get
            {
                return ImageBox.Text;
            }
        }

        public int DestintationX
        {
            get
            {
                return (int)DesXBox.Value;
            }
            set
            {
                DesXBox.Value = (decimal)value;
            }
        }

        public int DestintationY
        {
            get
            {
                return (int)DesYBox.Value;
            }
            set
            {
                DesYBox.Value = (decimal)value;
            }
        }

        public int DestintationW
        {
            get
            {
                return (int)DesWBox.Value;
            }
            set
            {
                DesWBox.Value = (decimal)value;
            }
        }

        public int DestintationH
        {
            get
            {
                return (int)DesHBox.Value;
            }
            set
            {
                DesHBox.Value = (decimal)value;
            }
        }

        public ImageForm()
        {
            InitializeComponent();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Cancel;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ImageBox.Text != "")
                {
                    DialogResult = DialogResult.OK;

                    Close();
                }
                else
                {
                    MessageBox.Show("Please choose an image first.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void imageButton_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog openDialog = new OpenFileDialog())
                {
                    openDialog.Title = "Open an Image to add to the PDF";
                    openDialog.InitialDirectory = System.IO.Path.GetFullPath(Application.StartupPath
                                                              + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");

                    openDialog.Filter = "TIF (*.TIF)|*.TIF|BMP (*.BMP)|*.BMP|JPG (*.JPG)|*.JPG|All Files (*.*) | *.*";
                    openDialog.FileName = ImageBox.Text;

                    if (openDialog.ShowDialog() == DialogResult.OK)
                    {
                        ImageBox.Text = openDialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ImageForm_Load(object sender, EventArgs e)
        {
            FitComboBox.SelectedIndex = 0;
        }
    }
}
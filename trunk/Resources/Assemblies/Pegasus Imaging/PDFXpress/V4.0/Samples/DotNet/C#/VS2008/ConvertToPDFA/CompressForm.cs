/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ConvertToPDFA
{
    public partial class CompressForm : Form
    {
        public CompressForm()
        {
            InitializeComponent();
        }

        public Compression CompressionSelected
        {
            get
            {
                if (G4RadioButton.Checked == true)
                {
                    return Compression.JPEG;
                }
                else if (Jbig2RadioButton.Checked == true)
                {
                    return Compression.JBIG2;
                }
                else
                {
                    return Compression.G4;
                }
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            Close();
        }

        private void CancelFormButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            Close();
        }
    }
}
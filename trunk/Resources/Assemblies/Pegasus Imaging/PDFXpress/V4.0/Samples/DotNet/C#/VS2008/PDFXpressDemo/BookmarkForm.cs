/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Accusoft.PdfXpressSdk;

namespace PDFDemo
{
    public partial class BookmarkForm : Form
    {        
        private int pageNumber, pageCount;
        private string pageName;
        private ArrayList bookmarksDeletedTitle, bookmarksDeletedPageNumber, bookmarkAddedTitle, bookmarkAddedPageNumber;

        public ArrayList BookmarksDeletedPageNumber
        {
            get
            {
                return bookmarksDeletedPageNumber;
            }
        }

        public ArrayList BookmarksDeletedTitle
        {
            get
            {
                return bookmarksDeletedTitle;
            }
        }

        public ArrayList BookmarkAddedTitle
        {
            get
            {
                return bookmarkAddedTitle;
            }
        }

        public ArrayList BookmarkAddedPageNumber
        {
            get
            {
                return bookmarkAddedPageNumber;
            }
        }

        public BookmarkForm(int count, TreeView treeView)
        {
            InitializeComponent();

            TreeView1.BeginUpdate();

            if (treeView != null)
            {
                for (int i = 0; i < treeView.Nodes.Count; i++)
                {
                    TreeView1.Nodes.Add((TreeNode)treeView.Nodes[i].Clone());
                }
            }

            TreeView1.EndUpdate();

            pageCount = count;
        }

        private bool Node(TreeNode node)
        {
            if (node.IsSelected == true)
            {
                bookmarksDeletedPageNumber.Add(node.Name);
                bookmarksDeletedTitle.Add(node.Text);

                for (int j = 0; j < bookmarkAddedPageNumber.Count; j++)
                {
                    if (node.Name == ((int)((int)bookmarkAddedPageNumber[j]+1)).ToString() && node.Text == (string)bookmarkAddedTitle[j])
                    {
                        bookmarkAddedTitle.RemoveAt(j);
                        bookmarkAddedPageNumber.RemoveAt(j);
                    }
                }

                TreeView1.Nodes.Remove(node);
                DeleteButton.Enabled = false;

                return true;
            }
            else
            {
                if (node.Nodes != null)
                {
                    for (int i = 0; i < node.Nodes.Count; i++)
                    {
                        if (Node(node.Nodes[i]) == true)
                            return true;
                    }
                }
            }

            return false;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                for (int j = 0; j < TreeView1.Nodes.Count; j++)
                {
                    if (Node(TreeView1.Nodes[j]) == true)
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (PageNumberTextBox.Text != "" && TitleTextBox.Text != "")
                {
                    try
                    {
                        int number = Int32.Parse(PageNumberTextBox.Text);

                        if (number <= pageCount)
                        {
                            bookmarkAddedPageNumber.Add(number - 1);
                            bookmarkAddedTitle.Add(TitleTextBox.Text);

                            TreeNode node = new TreeNode();
                            node.Text = TitleTextBox.Text;
                            node.Name = PageNumberTextBox.Text;

                            TreeView1.Nodes.Add(node);
                        }
                        else
                        {
                            MessageBox.Show("Please specify a bookmark to add whose "
                                + "PageNumber is less than or equal to the total page count of: " + pageCount.ToString());
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Please specify a number only for the Page Number of the bookmark you want to add.");
                    }
                }
                else
                {
                    MessageBox.Show("Please specify a Page Number and Title of the bookmark you want to add.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            Close();
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                if (e.X >= e.Node.Bounds.X)
                {
                    DeleteButton.Enabled = true;

                    pageNumber = Int32.Parse(e.Node.Name);
                    pageName = e.Node.Text;
                    string oldString = "(page " + (pageNumber + 1).ToString() + ")";
                    pageName = pageName.Replace(oldString, "").Trim();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BookmarkForm_Load(object sender, EventArgs e)
        {
            bookmarksDeletedTitle = new ArrayList();
            bookmarksDeletedPageNumber = new ArrayList();
            bookmarkAddedTitle = new ArrayList();
            bookmarkAddedPageNumber = new ArrayList();

            if (TreeView1.Nodes.Count == 0)
                DeleteButton.Enabled = false;
        }

        private void treeView1_Leave(object sender, EventArgs e)
        {
            if (DeleteButton.Focused == false)
                DeleteButton.Enabled = false;
        }

        private void DeleteButton_MouseUp(object sender, MouseEventArgs e)
        {
            DeleteButton.Enabled = false;
        }
    }
}
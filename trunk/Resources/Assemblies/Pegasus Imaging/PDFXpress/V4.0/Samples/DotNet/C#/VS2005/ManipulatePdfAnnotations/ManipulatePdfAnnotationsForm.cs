/// Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Text;
    using System.Windows.Forms;
    using Accusoft.PdfXpressSdk;
    using Accusoft.ImagXpressSdk;
    using Accusoft.NotateXpressSdk;
    using System.Runtime.InteropServices;
    using System.Globalization;
    using System.Reflection;

    public partial class ManipulatePdfAnnotationsForm : Form
    {
        [DllImport( "gdi32.dll" , EntryPoint = "DeleteObject" )]
        public static extern IntPtr DeleteObject( IntPtr hDc );

        private String _pdfFilename             = @"" ;
        private Document _document              = null ;
        private PageInfo _pageInfo              = null ;
        private Int32 _pageIndex                = -1 ;
        private String _dialogTitle             = "";

        #region Constants
        private const Int32 FirstPage           = 0 ;
        private const Int32 RetryPasswordCount  = 3 ;
        private const Int32 AfterLast           = -1 ;
        private const Int32 Rgb24BitsPerPixel   = 24;
        private const String FontDirectory      = @"\..\..\..\..\..\..\..\Support\Font" ;
        private const String CmapDirectory      = @"\..\..\..\..\..\..\..\Support\CMap" ;
        private const String CommonDirectory    = @"\..\..\..\..\..\..\..\..\..\Common\Images";
        #endregion

        public ManipulatePdfAnnotationsForm( )
        {
            InitializeComponent( );

            // Save the original title of the application
            //
            this._dialogTitle = this.Text;

            // Must call the UnlockRuntime method to unlock the control.
            // Note: The unlock codes shown below are for demonstation purposes 
            // only and are not valid.
            //
            //this._pdfxpress.Licensing.UnlockRuntime( 1234 , 1234 , 1234 , 1234 );
            //this._imagexpress.Licensing.UnlockRuntime( 5678 , 5678 , 5678 , 5678 );         

            // Initalize PDF Xpress
            //
            if   ( Directory.Exists( Application.StartupPath + FontDirectory )
                && Directory.Exists( Application.StartupPath + CmapDirectory ) )
            {
                this._pdfxpress.Initialize( Application.StartupPath + FontDirectory
                                          , Application.StartupPath + CmapDirectory 
                                          ) ;
            }
            else
            {
                this._pdfxpress.Initialize( );
            }
        }
        #region Logging Routines
        private void LogMessage( String message )
        {
            String prettyMessage = String.Format
                                                ( NumberFormatInfo.CurrentInfo
                                                , "{0}: {1}\r\n"
                                                , System.DateTime.Now.ToString( )
                                                , message
                                                );
            this._messageTextBox.AppendText( prettyMessage );
        }
        #endregion
        #region Component Interoperability Routines
        private Accusoft.NotateXpressSdk.Mapping RotateAngleToMapping( Accusoft.PdfXpressSdk.RotateAngle rotateAngle )
        {
            switch ( rotateAngle )
            {
                case Accusoft.PdfXpressSdk.RotateAngle.Rotate0:
                    return Accusoft.NotateXpressSdk.Mapping.Pdf;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate90:
                    return Accusoft.NotateXpressSdk.Mapping.Pdf90;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate180:
                    return Accusoft.NotateXpressSdk.Mapping.Pdf180;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate270:
                    return Accusoft.NotateXpressSdk.Mapping.Pdf270;

                default:
                    return Accusoft.NotateXpressSdk.Mapping.User;
            }
        }
        private Accusoft.PdfXpressSdk.RotateAngle RotateMappingToRotateAngle( Accusoft.NotateXpressSdk.Mapping mapping )
        {
            switch ( mapping )
            {
                case Accusoft.NotateXpressSdk.Mapping.Pdf:
                    return Accusoft.PdfXpressSdk.RotateAngle.Rotate0;

                case Accusoft.NotateXpressSdk.Mapping.Pdf90:
                    return Accusoft.PdfXpressSdk.RotateAngle.Rotate90;

                case Accusoft.NotateXpressSdk.Mapping.Pdf180:
                    return Accusoft.PdfXpressSdk.RotateAngle.Rotate180;

                case Accusoft.NotateXpressSdk.Mapping.Pdf270:
                    return Accusoft.PdfXpressSdk.RotateAngle.Rotate270;

                default:
                    return Accusoft.PdfXpressSdk.RotateAngle.RotateOther;
            }
        }
        #region Underline Annotation Handler
        private ImageXView.ToolEventHandler _willAddUnderlineEventHandler;
        private Point _underlineBeginLocation = new Point();
        private void willAddUnderlineAnnotation( object sender , ToolEventArgs e )
        {
            if ( e.Tool == Tool.Select )
            {
                switch ( e.Action )
                {
                    case ToolAction.Begin:
                        // Capture the first of two locations
                        //
                        this._underlineBeginLocation        = this._imagxview.ImageCursorPosition;
                        break;

                    case ToolAction.End:
                        this.LogMessage( "Adding Underline Annotation..." );
                        try
                        {
                            // Save PDF annotations 
                            //
                            this.SavePageAnnotations( );

                            // Express a new PDF text annotation as a single XFDF text annotation.
                            // for the current page index.
                            //
                            CreateXfdfUnderlineAnnotationOptions underlineOptions
                                                                = new CreateXfdfUnderlineAnnotationOptions( );
                            underlineOptions.Page               = this._pageIndex;
                            PointF bottomLeftLocation           = this.ImageCursorLocationToUserspaceLocation
                                                                    ( this._underlineBeginLocation );
                            PointF topRightLocation             = this.ImageCursorLocationToUserspaceLocation
                                                                    ( this._imagxview.ImageCursorPosition ) ;
                            underlineOptions.Rectangle          = new RectangleF
                                                                    ( bottomLeftLocation.X
                                                                    , bottomLeftLocation.Y
                                                                    , topRightLocation.X - bottomLeftLocation.X
                                                                    , topRightLocation.Y - bottomLeftLocation.Y
                                                                    ) ;
                            underlineOptions.RotateAngle        = this._pageInfo.Rotated ;
                            byte[ ] utf16XfdfPacket             = XfdfSnippets.CreateXfdfUnderlineAnnotation
                                                                    ( underlineOptions );

                            // Add the annotation to the current PDF page
                            //
                            XfdfOptions xfdfOptions             = new XfdfOptions( ) ;
                            xfdfOptions.CanCreateAnnotations    = true ;
                            xfdfOptions.CanModifyAnnotations    = false ;
                            xfdfOptions.CanDeleteAnnotations    = false ;
                            xfdfOptions.WhichPage               = this._pageIndex ;
                            this._document.ImportXfdf           ( xfdfOptions
                                                                , utf16XfdfPacket
                                                                ) ;

                            // Reload PDF annotations into NotateXpress
                            //
                            this.ReloadPageAnnotations( ) ;
                        }
                        catch ( Exception ex )
                        {
                            this.LogMessage( ex.ToString( ) );
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// Use ImageXpress selection tool and events to define a rectangular area
        /// to add a new underline annotation.
        /// </summary>
        private void InstallWillAddUnderlineAnnotationHandler( )
        {
            if ( null == this._willAddUnderlineEventHandler )
            {
                this._willAddUnderlineEventHandler = new ImageXView.ToolEventHandler( this.willAddUnderlineAnnotation );
            }
            this._notatexpress.InteractMode = AnnotationMode.Interactive;
            this._imagxview.ToolSet( Tool.Select , MouseButtons.Left , Keys.None );
            this._imagxview.ToolEvent += this._willAddUnderlineEventHandler ;
        }
        /// <summary>
        /// Restore ImagXpress state.
        /// </summary>
        private void UninstallWillAddUnderlineAnnotationHandler( )
        {
            this._imagxview.ToolEvent -= this._willAddUnderlineEventHandler; // Uninstall event handler
            this._imagxview.ToolSet( Tool.None , MouseButtons.Left , Keys.None );
            this._notatexpress.InteractMode = AnnotationMode.Edit; // Make NXP responsive
            this._imagxview.Cursor = System.Windows.Forms.Cursors.Default ;
        }
        #endregion
        #region Strikeout Annotation Handler
        private ImageXView.ToolEventHandler _willAddStrikeoutEventHandler;
        private Point _StrikeoutBeginLocation = new Point();
        private void willAddStrikeoutAnnotation( object sender , ToolEventArgs e )
        {
            if ( e.Tool == Tool.Select )
            {
                switch ( e.Action )
                {
                    case ToolAction.Begin:
                        // Capture the first of two locations
                        //
                        this._StrikeoutBeginLocation        = this._imagxview.ImageCursorPosition;
                        break;

                    case ToolAction.End:
                        this.LogMessage( "Adding Strikeout Annotation..." );
                        try
                        {
                            // Save PDF annotations 
                            //
                            this.SavePageAnnotations( );

                            // Express a new PDF text annotation as a single XFDF text annotation.
                            // for the current page index.
                            //
                            CreateXfdfStrikeoutAnnotationOptions strikeoutOptions
                                                                = new CreateXfdfStrikeoutAnnotationOptions( );
                            strikeoutOptions.Page               = this._pageIndex;
                            PointF bottomLeftLocation           = this.ImageCursorLocationToUserspaceLocation
                                                                    ( this._StrikeoutBeginLocation );
                            PointF topRightLocation             = this.ImageCursorLocationToUserspaceLocation
                                                                    ( this._imagxview.ImageCursorPosition );
                            strikeoutOptions.Rectangle          = new RectangleF
                                                                    ( bottomLeftLocation.X
                                                                    , bottomLeftLocation.Y
                                                                    , topRightLocation.X - bottomLeftLocation.X
                                                                    , topRightLocation.Y - bottomLeftLocation.Y
                                                                    );
                            strikeoutOptions.RotateAngle        = this._pageInfo.Rotated;
                            byte[ ] utf16XfdfPacket             = XfdfSnippets.CreateXfdfStrikeoutAnnotation
                                                                    ( strikeoutOptions );

                            // Add the annotation to the current PDF page
                            //
                            XfdfOptions xfdfOptions             = new XfdfOptions( ) ;
                            xfdfOptions.CanCreateAnnotations    = true ;
                            xfdfOptions.CanModifyAnnotations    = false ;
                            xfdfOptions.CanDeleteAnnotations    = false ;
                            xfdfOptions.WhichPage               = this._pageIndex ;
                            this._document.ImportXfdf           ( xfdfOptions
                                                                , utf16XfdfPacket
                                                                ) ;

                            // Reload PDF annotations into NotateXpress
                            //
                            this.ReloadPageAnnotations( ) ;
                        }
                        catch ( Exception ex )
                        {
                            this.LogMessage( ex.ToString( ) );
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// Use ImageXpress selection tool and events to define a rectangular area
        /// to add a new Strikeout annotation.
        /// </summary>
        private void InstallWillAddStrikeoutAnnotationHandler( )
        {
            if ( null == this._willAddStrikeoutEventHandler )
            {
                this._willAddStrikeoutEventHandler = new ImageXView.ToolEventHandler( this.willAddStrikeoutAnnotation );
            }
            this._notatexpress.InteractMode = AnnotationMode.Interactive;
            this._imagxview.ToolSet( Tool.Select , MouseButtons.Left , Keys.None );
            this._imagxview.ToolEvent += this._willAddStrikeoutEventHandler ;
        }
        /// <summary>
        /// Restore ImagXpress state.
        /// </summary>
        private void UninstallWillAddStrikeoutAnnotationHandler( )
        {
            this._imagxview.ToolEvent -= this._willAddStrikeoutEventHandler; // Uninstall event handler
            this._imagxview.ToolSet( Tool.None , MouseButtons.Left , Keys.None );
            this._notatexpress.InteractMode = AnnotationMode.Edit; // Make NXP responsive
            this._imagxview.Cursor = System.Windows.Forms.Cursors.Default ;
        }
        #endregion
        private void DisableSpecialAnnotationHandlers( )
        {
            this.UninstallWillAddUnderlineAnnotationHandler( );
            this.UninstallWillAddStrikeoutAnnotationHandler( );
        }
        #endregion
        #region PDF Document State Routines
        /// <summary>
        /// Returns a page index guarenteed to be in the range [0, pageCount)
        /// </summary>
        /// <param name="document">The open PDF document.</param>
        /// <param name="pageIndex">The zero-based page index.</param>
        /// <exception cref="System.NullReferenceException"/>
        /// <returns>Returns a page index guarenteed to be in the range [0, pageCount)
        /// </returns>
        private Int32 GetSafePageIndex( Document document , Int32 pageIndex )
        {
            Int32 safePageIndex = System.Math.Min
                                    ( this._document.PageCount - 1
                                     , pageIndex
                                     ) ;
            safePageIndex       = System.Math.Max
                                    ( 0
                                    , safePageIndex
                                    ) ;
            return safePageIndex ; 
        }
        private void ViewPdfPage( String filename , String password , Int32 pageIndex )
        {
            // If document name is different, then close the open document
            if ( 0 != this._pdfFilename.CompareTo( filename ) )
            {
                // Close open document
                //
                this.ClosePdfDocumentCommand( );

                // Load PDF Document
                //
                this._document              = new Document
                                                ( this._pdfxpress
                                                , filename
                                                , password
                                                ) ;
                this._pageIndex             = -1 ;
            }
            this._pdfFilename               = filename ;
            this.ViewPdfPage                ( this._document , pageIndex ) ;
        }
        private void ViewPdfPage( Document document , Int32 pageIndex )
        {            
            // Coerce page index into a valid range
            // 
            Int32 validatedPageIndex    = this.GetSafePageIndex( document , pageIndex ) ;
            if ( validatedPageIndex == this._pageIndex )
            {
                return ;
            }

            this.SavePageAnnotations    ( ); // Retain changes made to current annotations to PDF
            this._pageInfo              = this._document.GetInfo( validatedPageIndex );

            float userspaceDpi          = this.GetUserSpaceResolution( document, validatedPageIndex ) ;
            String pageMessage          = String.Format
                                            ( NumberFormatInfo.CurrentInfo
                                            , "Viewing page {0} of {1}\r\nMedia Box: l={2:f6} r={3:f6} w={4:f6} h={5:f6}\r\nCrop Box: l={6:f6} r={7:f6} w={8:f6} h={9:f6}\r\nRotated: {10}"
                                            , validatedPageIndex + 1
                                            , document.PageCount
                                            , this._pageInfo.MediaX / userspaceDpi
                                            , this._pageInfo.MediaY / userspaceDpi
                                            , this._pageInfo.MediaWidth / userspaceDpi
                                            , this._pageInfo.MediaHeight / userspaceDpi
                                            , this._pageInfo.ClipX / userspaceDpi
                                            , this._pageInfo.ClipY / userspaceDpi
                                            , this._pageInfo.ClipWidth / userspaceDpi
                                            , this._pageInfo.ClipHeight / userspaceDpi
                                            , this._pageInfo.Rotated 
                                            ) ;
            this.LogMessage             ( pageMessage ) ;

            this._pageIndex             = validatedPageIndex ;

            // Delegate image data production to PDF Xpress since it supports a larger
            // variety of PDF content than ImagXpress.
            //
            // The ImagXpress virtual image divests from ImagXpress the role of
            // image data production. ImageXpress retains all other image viewing and
            // reporting roles.
            //
            // During the ImagXpress PaintImage event, direct PDF Xpress to render only
            // a specific selection of the PDF document, instead of the entire PDF page.
            //
            this.SetupVirtualImage      ( ) ; 

            this.ReloadPageAnnotations  ( ) ; // Set up NotateXpress to view current PDF page annotations.            
            this.RefreshZoomFactor      ( ) ;
            this.RefreshPageNumberLabel ( ) ;
            this.RefreshDialogCaption   ( ) ;
            this.RefreshPageCountLabel  ( ) ;
            this.RefreshPageSizeLabel   ( ) ;
        }
        /// <summary>
        /// Save NotateXpress Annotations to open PDF Document page.
        /// </summary>
        private void SavePageAnnotations( )
        {
            if ( null == this._document )
            {
                return ;
            }
            try
            {
                // Will export NotateXpress elements originating from 
                // XFDF back to XFDF
                //
                this._notatexpress.PageNumber   = this._pageIndex + 1 ;
                Accusoft.NotateXpressSdk.SaveOptions saveOptions
                                                = new Accusoft.NotateXpressSdk.SaveOptions( );
                saveOptions.AnnType             = AnnotationType.Xfdf;
                saveOptions.Mapping             = this.RotateAngleToMapping
                                                    ( this._pageInfo.Rotated ) ;
                byte[ ] xfdfPacket              = this._notatexpress.Layers.SaveToByteArray
                                                    ( saveOptions ) ;

                // Will import PDF Annotations on current page from XFDF
                //
                XfdfOptions xfdfOptions         = new XfdfOptions( ) ;
                xfdfOptions.WhichPage           = this._pageIndex ;
                xfdfOptions.WhichAnnotation     = XfdfOptions.AllAnnotations ;
                this._document.ImportXfdf       ( xfdfOptions 
                                                    , xfdfPacket 
                                                    ) ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        
        /// <summary>
        /// Load NotateXpress Annotations from open PDF Document page.
        /// </summary>
        private void ReloadPageAnnotations( )
        {
            if ( null == this._document )
            {
                return;
            }
            try
            {
                // Will Export PDF Annotations on current page to Xfdf
                XfdfOptions xfdfOptions         = new XfdfOptions( ) ;
                xfdfOptions.WhichPage           = this._pageIndex ;
                xfdfOptions.WhichAnnotation     = XfdfOptions.AllAnnotations ;
                xfdfOptions.WhichEncodingName   = "UTF-8";
                byte[ ] utf8Xfdf                = this._document.ExportXfdf
                                                    ( xfdfOptions );                

                // Will import Xfdf to NotateXpress elements
                this._notatexpress.PageNumber   = this._pageIndex + 1;
                Accusoft.NotateXpressSdk.LoadOptions loadOptions
                                                = new Accusoft.NotateXpressSdk.LoadOptions( );
                loadOptions.AnnType             = AnnotationType.Xfdf;
                using ( Layer xfdfLayer = this._notatexpress.Layers.FromByteArray
                         ( utf8Xfdf , loadOptions ) )
                {
                    this._notatexpress.InteractMode = AnnotationMode.Edit;
                    xfdfLayer.Select( ) ;
                }
            }
            catch ( Exception )
            {
                using ( Layer blankLayer = new Layer( ) )
                {
                    this._notatexpress.InteractMode = AnnotationMode.Edit;
                    this._notatexpress.Layers.Clear( );
                    this._notatexpress.Layers.Add( blankLayer );
                    blankLayer.Select( );
                }
            }
        }
        private void ConfigureNotateXpressToImportXfdfRotated
            ( Accusoft.NotateXpressSdk.NotateXpress notatexpress 
            , Accusoft.PdfXpressSdk.RotateAngle rotateAngle 
            )
        {
            Accusoft.NotateXpressSdk.SaveOptions saveOptions
                = new Accusoft.NotateXpressSdk.SaveOptions( );
            saveOptions.Mapping = this.RotateAngleToMapping( this._pageInfo.Rotated );
            this._notatexpress.Layers.SetSaveOptions( saveOptions );
        }
        #endregion    
        #region ImagXpress Virtual Image Routines
        #region Coordinate system conversions
        private void _notatexpress_MappingInput( object sender , MappingInputEventArgs e )
        {
            Point imageLocation             = this.UserspaceLocationToImageCursorLocation
                                                ( new PointF( ( float )e.X , ( float )e.Y ) );
            e.NewX                          = imageLocation.X ;
            e.NewY                          = imageLocation.Y ;
        }
        private void _notatexpress_MappingOutput( object sender , MappingOutputEventArgs e )
        {
            PointF userspaceLocation        = this.ImageCursorLocationToUserspaceLocation 
                                                ( new Point( e.X , e.Y ) ) ;
            e.NewX                          = userspaceLocation.X ;
            e.NewY                          = userspaceLocation.Y ;
        }
        /// <summary>
        /// Converts an image coordinate, expressed as a pixel at device resolution,
        /// as a PDF user space coordinate.
        /// </summary>
        /// <remarks>
        ///     PDF Xpress reports the Media and Crop (Clip) boxes after the
        ///     default page matrix has been applied. An accurate conversion may not
        ///     possible for PDF pages whose original Crop box and Media box are
        ///     not located at the user space origin.
        /// </remarks>
        /// <param name="imageLocation">A DIB coordinate in the image, expressed in
        /// pixels at device resolution.</param>
        /// <returns>The equivalent PDF user space coordinate on the current page.</returns>
        private PointF ImageCursorLocationToUserspaceLocation( Point imageLocation )
        {
            // Express image location as an offset from the image coordinate system
            // origin in userspace units.
            //
            float userspaceDpi              = this.GetUserSpaceResolution
                                                ( this._document
                                                , this._pageIndex
                                                ) ;
            this._imagxview.Image.Resolution.Units = GraphicsUnit.Inch ;
            SizeF imageOriginToLocation     = new SizeF
                                                ( imageLocation.X / this._imagxview.Image.Resolution.Dimensions.Width * userspaceDpi
                                                , imageLocation.Y / this._imagxview.Image.Resolution.Dimensions.Height * userspaceDpi
                                                ) ;

            // The image origin corresponds to a different corner of the
            // page crop box depending on the intrinsic PDF page rotation.
            // Identify the corner of the image corresponding to the 
            // bottom-left corner of the crop box.
            //
            PointF userspaceLocation        = new PointF( ) ;
            switch ( this._pageInfo.Rotated )
            {
                case Accusoft.PdfXpressSdk.RotateAngle.Rotate0:
                    // Bottom-left corner of the crop box corresponds to 
                    // the bottom-left corner of the image. 
                    //
                    userspaceLocation       = new PointF
                                                ( ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX + imageOriginToLocation.Width )
                                                , ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY + this._pageInfo.ClipHeight - imageOriginToLocation.Height )
                                                ) ;
                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate90:
                    // Bottom-left corner of the crop box corresponds to
                    // the top-left corner of the image.
                    //
                    userspaceLocation       = new PointF
                                                ( imageOriginToLocation.Height - ( float )this._pageInfo.MediaY
                                                , imageOriginToLocation.Width - ( float )this._pageInfo.MediaX
                                                ) ;
                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate180:
                    // Bottom-left corner of the crop box corresponds to 
                    // the top-right corner of the image.
                    //
                    userspaceLocation       = new PointF
                                                ( ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX + this._pageInfo.ClipWidth - imageOriginToLocation.Width )
                                                , ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY + imageOriginToLocation.Height )
                                                ) ;
                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate270:
                    // Bottom-left corner of the crop box corresponds to 
                    // the bottom-right corner of the image.
                    //
                    userspaceLocation       = new PointF
                                                ( ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY + this._pageInfo.ClipHeight - imageOriginToLocation.Height )
                                                , ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX + this._pageInfo.ClipWidth - imageOriginToLocation.Width )
                                                ) ;
                    break;
            }

            System.Diagnostics.Debug.Print  ( String.Format( "( {0:d} , {1:d} ) -> ( {2:f4} , {3:f4} )"
                                                , imageLocation.X
                                                , imageLocation.Y
                                                , userspaceLocation.X
                                                , userspaceLocation.Y
                                                ) );
            return userspaceLocation;
        }        
        /// <summary>
        /// Converts a PDF user space coordinate to an image coordinate, 
        /// expressed as a pixel at device resolution.
        /// </summary>
        /// <remarks>
        ///     PDF Xpress reports the Media and Crop (Clip) boxes after the
        ///     default page matrix has been applied. An accurate conversion may not
        ///     possible for PDF pages whose original Crop box and Media box are
        ///     not located at the user space origin.
        /// </remarks>
        /// <param name="userspaceLocation">A PDF user space coordinate in the image.</param>
        /// <returns>The equivalent image coordinate, expressed as a pixel at device resolution.</returns>
        private Point UserspaceLocationToImageCursorLocation( PointF userspaceLocation )
        {
            // The image cursor location is relative to the image origin.
            // Express the image origin as a user space coordinate.
            
            // User space resolution
            float userspaceDpi              = this.GetUserSpaceResolution
                                                ( this._document
                                                , this._pageIndex
                                                ) ;
            // Image resolution
            this._imagxview.Image.Resolution.Units = GraphicsUnit.Inch ;

            // The image origin corresponds to a different corner of the
            // page crop box depending on the intrinsic PDF page rotation.
            // Identify the corner of the image corresponding to the 
            // bottom-left corner of the crop box.
            //
            SizeF userspaceImageOriginToLocation = new SizeF( ) ;
            PointF userspaceImageOrigin     = new PointF( ) ;
            switch ( this._pageInfo.Rotated )
            {
                case Accusoft.PdfXpressSdk.RotateAngle.Rotate0:
                    // Top-left corner of the image corresponds to
                    // the top-left corner of the crop box
                    //
                    userspaceImageOrigin    = new PointF
                                                ( ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX )
                                                , ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY + this._pageInfo.ClipHeight )
                                                ) ;
                    // Calculate difference beteen image origin and specific location
                    // expressed as user space units
                    //
                    userspaceImageOriginToLocation.Width    = userspaceLocation.X - userspaceImageOrigin.X ;
                    userspaceImageOriginToLocation.Height   = userspaceImageOrigin.Y - userspaceLocation.Y ;

                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate90:
                    // Top-left corner of the image corresponds to
                    // the bottom-left corner of the crop box
                    //
                    userspaceImageOrigin    = new PointF
                                                ( ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY )
                                                , ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX )
                                                ) ;
                    // Calculate difference beteen image origin and specific location
                    // expressed as user space units
                    //
                    userspaceImageOriginToLocation.Width = userspaceLocation.Y - userspaceImageOrigin.Y ;
                    userspaceImageOriginToLocation.Height = userspaceLocation.X - userspaceImageOrigin.X ;

                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate180:
                    // Top-left corner of the image corresponds to
                    // the bottom-right corner of the crop box
                    //
                    userspaceImageOrigin    = new PointF
                                                ( ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX + this._pageInfo.ClipWidth )
                                                , ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY )
                                                ) ;
                    // Calculate difference beteen image origin and specific location
                    // expressed as user space units
                    //
                    userspaceImageOriginToLocation.Width = userspaceImageOrigin.X - userspaceLocation.X ;
                    userspaceImageOriginToLocation.Height = userspaceLocation.Y - userspaceImageOrigin.Y ;

                    break;

                case Accusoft.PdfXpressSdk.RotateAngle.Rotate270:
                    // Top-left corner of the image corresponds to
                    // the top-right corner of the crop box
                    //
                    userspaceImageOrigin    = new PointF
                                                ( ( float )( this._pageInfo.ClipY - this._pageInfo.MediaY + this._pageInfo.ClipHeight )
                                                , ( float )( this._pageInfo.ClipX - this._pageInfo.MediaX + this._pageInfo.ClipWidth )
                                                ) ;
                    // Calculate difference beteen image origin and specific location
                    // expressed as user space units
                    //
                    userspaceImageOriginToLocation.Width = userspaceImageOrigin.Y - userspaceLocation.Y ;
                    userspaceImageOriginToLocation.Height = userspaceImageOrigin.X - userspaceLocation.X ;

                    break;
            }

            Point imageLocation             = new Point
                                                ( ( int )( userspaceImageOriginToLocation.Width / userspaceDpi * this._imagxview.Image.Resolution.Dimensions.Width + 0.5 )
                                                , ( int )( userspaceImageOriginToLocation.Height / userspaceDpi * this._imagxview.Image.Resolution.Dimensions.Height + 0.5 )
                                                ) ;
            System.Diagnostics.Debug.Print ( String.Format( "( {0:d} , {1:d} ) <- ( {2:f4} , {3:f4} )"
                                                , imageLocation.X
                                                , imageLocation.Y
                                                , userspaceLocation.X
                                                , userspaceLocation.Y
                                                ) );
            return imageLocation;
        }
        #endregion
        /// <summary>
        /// Identifies the active screen graphics device resolution in DPI.
        /// </summary>
        /// <returns>The active screent graphics resolution in DPI.</returns>
        private PointF GetGrahicsResolution( )
        {            
            using ( System.Drawing.Graphics graphics = this.CreateGraphics( ) )
            {
                PointF dpi = new PointF( );
                if ( null != graphics )
                {
                    dpi.X = graphics.DpiX;
                    dpi.Y = graphics.DpiY;
                }
                return dpi ;
            }            
        }
        private float GetUserSpaceResolution( Document whichDocument , Int32 forPageIndex )
        {
            // PDF pages whose user space resolution is not 
            // the typical 72.0 will not be handled correctly.
            //
            return 72.0f;
        }
        /// <summary>
        /// Configures ImagXpress to delegate paint events to PDF Xpress.
        /// </summary>
        private void SetupVirtualImage( )
        {
            // Set up new ImagXpress Virtual image
            // for the current PDF page index.
            //
            PageInfo pageInfo               = this._document.GetInfo
                                                ( this._pageIndex ) ;

            // Identify the PDF user space resolution for the current
            // page index.
            //
            float userSpaceDpi              = this.GetUserSpaceResolution
                                                ( this._document
                                                , this._pageIndex 
                                                ) ;

            // The virtual image dimensions will be expressed as pixels 
            // whose size is governed by the current screen graphics 
            // resolution.
            //
            PointF deviceDpi                = this.GetGrahicsResolution( ) ;
            System.Int32 width              = ( Int32 )( this._pageInfo.ClipWidth
                                                / userSpaceDpi * deviceDpi.X + 0.5 ) ;
            System.Int32 height             = ( Int32 )( this._pageInfo.ClipHeight
                                                / userSpaceDpi * deviceDpi.Y + 0.5 ) ;
            this._imagxview.Image           = new ImageX
                                                    ( this._imagexpress
                                                    , width
                                                    , height
                                                    , Rgb24BitsPerPixel
                                                    , System.Drawing.SystemColors.Control
                                                    , new Resolution( deviceDpi.X
                                                                    , deviceDpi.Y
                                                                    , GraphicsUnit.Inch
                                                                    )
                                                    , true
                                                    ) ;
            // Attach NotateXpress to ImagXpress 
            //
            this._notatexpress.ClientWindow = this._imagxview.Handle;
            this._notatexpress.InteractMode = AnnotationMode.Edit;
            this._notatexpress.MultiLineEdit = true ;
            this.ConfigureNotateXpressToImportXfdfRotated
                                                ( this._notatexpress
                                                , this._pageInfo.Rotated
                                                ) ;
        }
        /// <summary>
        /// Directs PDF Xpress to render a selection of the current PDF page 
        /// for ImagXpress at the current scaling factor.
        /// </summary>
        /// <param name="sender">The caller.</param>
        /// <param name="e">The PaintImage arguments.</param>
        private void _imagxview_PaintImage( object sender , PaintImageEventArgs e )
        {
            System.Diagnostics.Debug.Print      ( "L( l={0} t={1} w={2} h={3} )"
                                                    , e.LogicalRectangle.Left
                                                    , e.LogicalRectangle.Top
                                                    , e.LogicalRectangle.Width
                                                    , e.LogicalRectangle.Height
                                                    ) ;
            System.Diagnostics.Debug.Print      ( "V( l={0} t={1} w={2} h={3} )"
                                                    , e.ViewRectangle.Left
                                                    , e.ViewRectangle.Top
                                                    , e.ViewRectangle.Width
                                                    , e.ViewRectangle.Height
                                                    ) ;
            System.Diagnostics.Debug.Print      ( "U( l={0} t={1} w={2} h={3} )"
                                                    , e.UpdateRectangle.Left
                                                    , e.UpdateRectangle.Top
                                                    , e.UpdateRectangle.Width
                                                    , e.UpdateRectangle.Height
                                                    ) ;
            try
            {
                Double userSpaceDpi                 = this.GetUserSpaceResolution
                                                        ( this._document
                                                        , this._pageIndex
                                                        );
                RenderOptions renderOptions         = new RenderOptions( );
                renderOptions.RenderAnnotations     = false;
                renderOptions.ResolutionX           = e.Graphics.DpiX * e.ZoomFactor;
                renderOptions.ResolutionY           = e.Graphics.DpiY * e.ZoomFactor;
                renderOptions.SelectionX            = ( e.LogicalRectangle.Left + e.UpdateRectangle.Left - e.ViewRectangle.Left )
                                                        / renderOptions.ResolutionX
                                                        * userSpaceDpi;
                if (e.LogicalRectangle.Height > e.UpdateRectangle.Top + e.UpdateRectangle.Height)
                {
                    renderOptions.SelectionY = (e.LogicalRectangle.Height - e.UpdateRectangle.Top - e.UpdateRectangle.Height)
                                                        / renderOptions.ResolutionY
                                                        * userSpaceDpi;
                }
                else
                {
                    renderOptions.SelectionY = this._pageInfo.MediaHeight / renderOptions.ResolutionY;
                }
                renderOptions.SelectionWidth            = e.UpdateRectangle.Width
                                                        / renderOptions.ResolutionX
                                                        * userSpaceDpi;
                renderOptions.SelectionHeight       = e.UpdateRectangle.Height
                                                        / renderOptions.ResolutionY
                                                        * userSpaceDpi;
                renderOptions.ProduceDibSection     = true;
                System.IntPtr hDib                  = new System.IntPtr
                                                        ( this._document.RenderPageToDib
                                                            ( this._pageIndex
                                                            , renderOptions
                                                            )
                                                        );
                e.Graphics.DrawImage                ( Image.FromHbitmap( hDib )
                                                        , e.UpdateRectangle.Left
                                                        , e.UpdateRectangle.Top
                                                        , e.UpdateRectangle.Width
                                                        , e.UpdateRectangle.Height
                                                        );
                DeleteObject                        ( hDib );
                System.Diagnostics.Debug.Print      ( "ResX={0}, ResY={1}, x={2}, y={3}, w={4}, h={5}"
                                                        , renderOptions.ResolutionX
                                                        , renderOptions.ResolutionY
                                                        , renderOptions.SelectionX
                                                        , renderOptions.SelectionY
                                                        , renderOptions.SelectionWidth
                                                        , renderOptions.SelectionHeight
                                                        );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }
        }
        #endregion
        #region UI State Refresh
        private void RefreshZoomFactor( )
        {
            this._currentZoomPercentageToolStripTextBox.Text = String.Format
                                                ( NumberFormatInfo.CurrentInfo
                                                , "{0:P0}"
                                                , this._imagxview.ZoomFactor
                                                );
            this._actualSizeToolStripMenuItem.Checked = ( this._imagxview.ZoomFactor == 1.0 );
            this._fitWidthToolStripMenuItem.Checked = ( this._imagxview.AutoResize == AutoResizeType.FitWidth );
            this._fitHeightToolStripMenuItem.Checked = ( this._imagxview.AutoResize == AutoResizeType.FitHeight );
        }
        private void RefreshDialogCaption( )
        {
            if ( !File.Exists( this._pdfFilename ) )
            {
                this.Text                   = this._dialogTitle ;
                return;
            }

            System.IO.FileInfo fileInfo     = new System.IO.FileInfo
                                                ( this._pdfFilename );
            String prettyString             = String.Format
                                                ( NumberFormatInfo.CurrentInfo
                                                , "{0} - {1}"
                                                , fileInfo.Name 
                                                , this._dialogTitle                                                
                                                ) ;
            this.Text                       = prettyString ;
        }
        private void RefreshPageCountLabel( )
        {
            if ( null == this._document )
            {
                System.Diagnostics.Debug.Assert( false , "Wasted cycle..");
                return ;
            }
            this._pageCountToolStripLabel.Text = String.Format
                                                ( NumberFormatInfo.CurrentInfo
                                                , "/ {0}"
                                                , this._document.PageCount 
                                                ) ;
        }
        private void RefreshPageNumberLabel( )
        {
            this._currentPageToolStripTextBox.Text = String.Format 
                                                ( NumberFormatInfo.CurrentInfo
                                                , "{0}"
                                                , this._pageIndex + 1
                                                ) ;
        }
        private void RefreshPageSizeLabel( )
        {
            float userSpaceResolution           = this.GetUserSpaceResolution
                                                    ( this._document 
                                                    , this._pageIndex 
                                                    ) ;
            this._pageSizeToolStripStatusLabel.Text = String.Format
                                                    ( NumberFormatInfo.CurrentInfo
                                                    , "{0:F2} x {1:F2} in."
                                                    , this._pageInfo.ClipWidth / userSpaceResolution
                                                    , this._pageInfo.ClipHeight / userSpaceResolution
                                                    ) ;
        }
        #endregion
        #region UI Event Handlers
        private void _stickyNoteToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateStickyNoteToolCommand( );
        }
        private void highlightTextToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateHighlightToolCommand( );
        }
        private void _underlineTextToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateUnderlineToolCommand( );
        }
        private void _crossOutTextToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateStrikeOutToolCommand( );
        }
        private void _textBoxTextToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateTextToolCommand( );
        }
        private void _arrowToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateArrowToolCommand( );
        }
        private void _lineToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateLineToolCommand( );
        }
        private void _rectangleToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateRectangleToolCommand( );
        }
        private void _ovalToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivateEllipseToolCommand( );
        }
        private void _polygonLineToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivatePolyLineToolCommand( );
        }
        private void _polygonToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivatePolygonToolCommand( );
        }
        private void _pencilToolToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ActivatePencilToolCommand( );
        }
        private void _showCommentMarkupToolbarToolStripMenuItem2_Click( object sender , EventArgs e )
        {
            this._commentMarkupToolStrip.Visible = this._showCommentMarkupToolbarToolStripMenuItem2.Checked;
        }
        private void _commentMarkupToolStrip_VisibleChanged( object sender , EventArgs e )
        {
            this._commentMarkupToolStripMenuItem.Checked = this._commentMarkupToolStrip.Visible;
            this._showCommentMarkupToolbarToolStripMenuItem2.Checked = this._commentMarkupToolStrip.Visible;
        }
        private void _pointerToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivatePointerToolCommand( );
        }
        private void _pageToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.GoToPageCommand( );
        }
        private void _zoomToToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ZoomToMagnificationCommand( );
        }
        private void _saveAsToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.SavePdfDocumentAsCommand( );
        }
        private void _exportToDataFileToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ExportCommentsToDataFileCommand( );
        }
        private void _importCommentsToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ImportCommentsFromDataFileCommand( );
        }
        private void _closeToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ClosePdfDocumentCommand( );
        }
        private void _imagxview_ZoomFactorChanged( object sender , ZoomFactorChangedEventArgs e )
        {
            LogMessage  ( String.Format( NumberFormatInfo.CurrentInfo
                        , "Magnification changed from {0:N} to {1:N}..."
                        , e.OldValue 
                        , e.NewValue 
                        ) );
            this.RefreshZoomFactor( );
        }
        private void _currentZoomPercentageToolStripTextBox_TextChanged( object sender , EventArgs e )
        {
            try
            {
                double newScaleFactor       = Double.Parse
                                                ( this._currentZoomPercentageToolStripTextBox.Text.Replace( '%' , ' ' )
                                                , NumberStyles.Float  
                                                , NumberFormatInfo.CurrentInfo
                                                ) ;
                this._imagxview.ZoomFactor = newScaleFactor / 100 ;
            }
            catch ( Exception ex )
            {
                this.LogMessage ( ex.ToString( ) );
            }
        }
        private void _currentPageToolStripTextBox_TextChanged( object sender , EventArgs e )
        {
            this.ViewPageCommand( );
        }
        private void _openToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.OpenPdfDocumentCommand( );
        }
        private void _nextPageToolStripButton_Click( object sender , EventArgs e )
        {
            this.ViewNextPageCommand( );
        }
        private void _newToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.NewPdfDocumentCommand( );
        }
        private void _previousPageToolStripButton_Click( object sender , EventArgs e )
        {
            this.ViewPreviousPageCommand( );
        }
        private void _saveToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.SavePdfDocumentCommand( );
        }
        private void _aboutPDFXpressToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ShowPdfXpressAboutBoxCommand();
        }
        private void _aboutNotateXpressToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ShowNotateXpressAboutBoxCommand( );
        }
        private void _aboutImagXpressToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ShowImagXpressAboutBoxCommand();
        }
        private void _firstPageToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ViewFirstPageCommand( );
        }
        private void _lastPageToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ViewLastPageCommand();
        }
        private void _previousPageToolStripMenuItem_Click( object sender , EventArgs e )
        {            
            this.ViewPreviousPageCommand();
        }
        private void _nextPageToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ViewNextPageCommand();
        }
        private void _zoomToToolStripButton_Click( object sender , EventArgs e )
        {
            this.ZoomToMagnificationCommand();
        }
        private void _actualSizeToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ZoomToActualSizeCommand();
        }
        private void _fitWidthToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ZoomToFitWidthCommand( );
        }
        private void _fitHeightToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ZoomToFitHeightCommand( );
        }
        private void _exitToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ExitApplicationCommand( );
        }
        private void _showAllToolbarsToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ShowAllToolbarsCommand( );
        }
        private void _showZoomToolbarToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.ShowHideZoomToolbarCommand( );
        }
        private void _hideAllToolbarsToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this.HideAllToolbarsCommand( );
        }
        private void _zooomToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this._zoomToolStrip.Visible = this._zoomToolStripMenuItem.Checked;
        }
        private void _zoomToolStrip_VisibleChanged( object sender , EventArgs e )
        {
            this._zoomToolStripMenuItem.Checked = this._zoomToolStrip.Visible;
        }
        private void _zoomToolStripMenuItem2_Click( object sender , EventArgs e )
        {
            this._zoomToolStrip.Visible = this._zoomToolStripMenuItem2.Checked;
        }
        private void _navigationToolStrip_VisibleChanged( object sender , EventArgs e )
        {
            this._navigationToolStripMenuItem.Checked = this._navigationToolStrip.Visible;
        }
        private void _navigationToolbarToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this._navigationToolStrip.Visible = this._navigationToolStripMenuItem.Checked;
        }
        private void _highlightTextToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateHighlightToolCommand( );
        }
        private void _ovalToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateEllipseToolCommand( );
        }
        private void _crossOutTextTool_Click( object sender , EventArgs e )
        {
            this.ActivateStrikeOutToolCommand( );
        }
        private void _underlineTextToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateUnderlineToolCommand( );
        }
        private void _textBoxToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateTextToolCommand( );
        }
        private void _arrowToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateArrowToolCommand( );
        }
        private void _lineToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateLineToolCommand( );
        }
        private void _rectangleToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateRectangleToolCommand( );
        }
        private void _polygonLineToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivatePolyLineToolCommand( );
        }
        private void _polygonToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivatePolygonToolCommand( );
        }
        private void _pencilToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivatePencilToolCommand( );
        }
        private void _replaceSelectedTextToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateUnderlineToolCommand( );
        }
        private void _stickyNoteToolStripButton_Click( object sender , EventArgs e )
        {
            this.ActivateStickyNoteToolCommand( );
        }
        private void _commentMarkupToolStripMenuItem_Click( object sender , EventArgs e )
        {
            this._commentMarkupToolStrip.Visible = this._commentMarkupToolStripMenuItem.Checked;
        }
        private void _textEditToolStrip_ItemClicked( object sender , ToolStripItemClickedEventArgs e )
        {
            // Keep only one annotation tool active at a time.
            ToolStrip toolstrip = ( ToolStrip )sender;
            foreach ( ToolStripButton item in toolstrip.Items )
            {
                item.Checked = ( e.ClickedItem == item );
            }
        }
        #endregion
        #region Commands
        #region About box Commands
        private void ShowPdfXpressAboutBoxCommand( )
        {
            try
            {
                LogMessage( "Show PDF Xpress About Box..." );
                this._pdfxpress.AboutBox( );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ShowNotateXpressAboutBoxCommand( )
        {
            try
            {
                LogMessage( "Show NotateXpress About Box..." );
                this._notatexpress.AboutBox( );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ShowImagXpressAboutBoxCommand()
        {
            try
            {
                LogMessage( "Show ImagXpress About Box..." );
                this._imagexpress.AboutBox( );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion 
        #region PDF Page Navigation Commands
        private void GoToPageCommand( )
        {
            this.LogMessage( "Go to page" );
            try
            {
                using ( GoToPageForm form = new GoToPageForm( ) )
                {
                    form.Page       = this._pageIndex + 1;
                    form.PageCount  = this._document.PageCount;
                    if ( DialogResult.OK == form.ShowDialog( this ) )
                    {
                        this.ViewPdfPage( this._document
                                        , form.Page - 1
                                        );
                    }
                }                
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ViewFirstPageCommand()
        {
            try
            {
                LogMessage              ( "View first page." );
                this.ViewPdfPage        ( this._document , FirstPage ) ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }
        }
        private void ViewLastPageCommand()
        {
            try
            {
                LogMessage              ( "View last page." );
                this.ViewPdfPage        ( this._document , this._document.PageCount - 1 ) ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            } 
        }
        private void ViewPreviousPageCommand()
        {
            try
            {
                LogMessage              ( "View previous page." );
                this.ViewPdfPage        ( this._document , this._pageIndex - 1 ) ;
            }  
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }  
        }
        private void ViewNextPageCommand()
        {
            try
            {
                LogMessage              ( "View next page." );
                this.ViewPdfPage        ( this._document , this._pageIndex + 1 ) ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }
        }
        private void ViewPageCommand( )
        {
            this.LogMessage( "View page" );
            try
            {
                Int32 newPageNumber = Int32.Parse
                                                ( this._currentPageToolStripTextBox.Text
                                                , NumberStyles.Integer
                                                , NumberFormatInfo.CurrentInfo
                                                );
                if ( 1 > newPageNumber )
                {
                    return; // page index out of range.
                }
                if ( this._document.PageCount < newPageNumber )
                {
                    return; // page index out of range.
                }
                if ( newPageNumber - 1 == this._pageIndex )
                {
                    return; // same page; no refresh
                }

                this.ViewPdfPage( this._document , newPageNumber - 1 );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion
        #region PDF View Magnification Commands
        private void ZoomToActualSizeCommand( )
        {
            try
            {
                LogMessage( "Zoom to actual size." );
                this._imagxview.ZoomFactor = 1.0;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            } 
        }
        private void ZoomToFitWidthCommand( )
        {
            try
            {
                LogMessage( "Zoom to fit page width." );
                this._imagxview.AutoResize = AutoResizeType.FitWidth ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            } 
        }
        private void ZoomToFitHeightCommand( )
        {
            try
            {
                LogMessage( "Zoom to fit page height." );
                this._imagxview.AutoResize = AutoResizeType.FitHeight ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }
        }
        private void ZoomToMagnificationCommand( )
        {
            this.LogMessage( "Zoom to magnification" );
            try
            {
                using ( ZoomToForm zoomToForm = new ZoomToForm( ) )
                {
                    switch ( this._imagxview.AutoResize )
                    {
                        case AutoResizeType.BestFit:
                            zoomToForm.ZoomTo = ZoomToForm.Magnification.ZoomFitPage;
                            break;
                        case AutoResizeType.FitWidth:
                            zoomToForm.ZoomTo = ZoomToForm.Magnification.ZoomFitWidth;
                            break;
                        case AutoResizeType.FitHeight:
                            zoomToForm.ZoomTo = ZoomToForm.Magnification.ZoomFitHeight;
                            break;
                        default:
                            zoomToForm.ZoomTo = ( ZoomToForm.Magnification )( this._imagxview.ZoomFactor * 100 );
                            break;
                    }
                    if ( DialogResult.OK == zoomToForm.ShowDialog( this ) )
                    {
                        switch ( zoomToForm.ZoomTo )
                        {
                            case ZoomToForm.Magnification.ZoomFitPage:
                                this._imagxview.AutoResize = AutoResizeType.BestFit;
                                break;
                            case ZoomToForm.Magnification.ZoomFitHeight:
                                this._imagxview.AutoResize = AutoResizeType.FitHeight;
                                break;
                            case ZoomToForm.Magnification.ZoomFitWidth:
                                this._imagxview.AutoResize = AutoResizeType.FitWidth;
                                break;
                            default:
                                this._imagxview.AutoResize = AutoResizeType.CropImage;
                                this._imagxview.ZoomFactor = ( Double )zoomToForm.ZoomTo / 100.0;
                                break;
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion
        #region Application Commands
        private void ExitApplicationCommand( )
        {
            try
            {
                LogMessage( "Exit application." );
                Application.Exit( );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }
        }
        #endregion
        #region Toolbar Commands
        private void ShowHideZoomToolbarCommand( )
        {
            try
            {
                if ( this.showZoomToolbarToolStripMenuItem.Checked )
                {
                    this.LogMessage( "Show Zoom toolbar" );
                    this._zoomToolStrip.Visible = true;
                }
                else
                {
                    this.LogMessage( "Hide Zoom toolbar" );
                    this._zoomToolStrip.Visible = false;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ShowAllToolbarsCommand( )
        {
            try
            {
                this.LogMessage( "Show all toolbars" );
                foreach ( ToolStrip toolstrip in this.toolStripContainer1.TopToolStripPanel.Controls )
                {
                    toolstrip.Visible = true;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }

        }
        private void HideAllToolbarsCommand( )
        {
            try
            {
                this.LogMessage( "Hide all toolbars" );
                foreach ( ToolStrip toolstrip in this.toolStripContainer1.TopToolStripPanel.Controls )
                {
                    if ( toolstrip == this._mainMenuStrip )
                    {
                        continue;
                    }
                    toolstrip.Visible = false;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion
        #region PDF Document Commands
        private void NewPdfDocumentCommand( )
        {
            this.LogMessage( "Create new PDF document" );
            try
            {
                // Close any open document.
                //
                this.ClosePdfDocumentCommand( );

                // Create a new empty PDF document.
                //
                this._document = new Document( this._pdfxpress );

                // Add a blank US-Letter page
                //
                PageOptions usLetterPageOptions = new PageOptions( );
                this._document.CreatePage( AfterLast , usLetterPageOptions );

                // Update View State
                //
                this._pdfFilename   = "NewDocument.pdf";
                this._pageIndex     = -1 ;
                this.ViewPdfPage( this._document , FirstPage );                                
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void OpenPdfDocumentCommand( )
        {
            this.LogMessage( "Open PDF document" );
            try
            {
                using ( System.Windows.Forms.OpenFileDialog openFileDialog
                            = new System.Windows.Forms.OpenFileDialog( ) )
                {
                    openFileDialog.InitialDirectory = Path.GetFullPath(Application.StartupPath + CommonDirectory);
                    openFileDialog.CheckFileExists = true;
                    openFileDialog.CheckPathExists = true;
                    openFileDialog.DefaultExt = ".pdf";
                    openFileDialog.Filter = "Adobe PDF Files (*.pdf)|*.pdf|All files (*.*)|*.*";
                    openFileDialog.FilterIndex = 1;
                    openFileDialog.Title = "Open";
                    openFileDialog.SupportMultiDottedExtensions = true;
                    if ( DialogResult.OK == openFileDialog.ShowDialog( this ) )
                    {
                        // Close any open PDF document
                        //
                        this.ClosePdfDocumentCommand( );

                        this.LogMessage( String.Format( NumberFormatInfo.CurrentInfo
                                                    , "Opening document \"{0}\"..."
                                                    , openFileDialog.FileName
                                                    ) );
                        String pdfPassword = @"";
                        Int32 pageIndex = FirstPage;
                        try
                        {
                            this.ViewPdfPage( openFileDialog.FileName
                                                    , pdfPassword
                                                    , pageIndex
                                                    );
                        }
                        catch ( Accusoft.PdfXpressSdk.InvalidPasswordException )
                        {
                            LogMessage( "Document requires a user or owner password to open." );

                            // If a password exception is encountered, then
                            // give the user a fixed-number of retries before failing.
                            //
                            for ( int i = 0
                                ; i < RetryPasswordCount
                                ; i++ )
                            {
                                try
                                {
                                    // Prompt the user for a password to open the PDF document
                                    //
                                    using ( DocumentPasswordForm passwordForm = new DocumentPasswordForm( ) )
                                    {
                                        passwordForm.Filename = openFileDialog.FileName;
                                        switch ( passwordForm.ShowDialog( this ) )
                                        {
                                            case DialogResult.OK:
                                                this.ViewPdfPage( openFileDialog.FileName
                                                                    , passwordForm.Password
                                                                    , pageIndex
                                                                    );
                                                break;

                                            default:
                                                return;
                                        }
                                    }
                                    // If opening the document does not raise an exception
                                    // then return.
                                    //
                                    if ( null != this._document )
                                    {
                                        return;
                                    }
                                }
                                catch ( Accusoft.PdfXpressSdk.InvalidPasswordException )
                                {
                                    LogMessage( "Could not open document with password provided." );
                                }
                            }
                        }
                    }
                }                
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void SavePdfDocumentCommand( )
        {
            this.LogMessage( "Save document" );
            try
            {                
                this.SavePageAnnotations( );

                Accusoft.PdfXpressSdk.SaveOptions saveOptions
                    = new Accusoft.PdfXpressSdk.SaveOptions( );
                saveOptions.Filename = this._pdfFilename;
                saveOptions.Overwrite = true ;
                this._document.Save( saveOptions ) ;
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) ) ;
            }
        }
        private void SavePdfDocumentAsCommand( )
        {
            try
            {
                this.LogMessage( "Save document as" );
                using ( System.Windows.Forms.SaveFileDialog saveFileDialog = new SaveFileDialog( ) )
                {
                    saveFileDialog.InitialDirectory = Path.GetFullPath(Application.StartupPath + CommonDirectory);
                    saveFileDialog.Title            = "Save As";
                    saveFileDialog.OverwritePrompt  = true;
                    saveFileDialog.DefaultExt       = ".pdf";
                    saveFileDialog.Filter           = "Adobe PDF Files (*.pdf)|*.pdf|All files (*.*)|*.*";
                    saveFileDialog.FilterIndex      = 4;
                    saveFileDialog.FileName         = this._pdfFilename;
                    if ( DialogResult.OK == saveFileDialog.ShowDialog( this ) )
                    {
                        this.SavePageAnnotations    ( );

                        Accusoft.PdfXpressSdk.SaveOptions saveOptions
                            = new Accusoft.PdfXpressSdk.SaveOptions( );
                        saveOptions.Filename        = saveFileDialog.FileName;
                        saveOptions.Overwrite       = true;
                        this._document.Save         ( saveOptions ) ;

                        // Update UI state
                        //
                        this._pdfFilename           = saveFileDialog.FileName ;
                        this.RefreshDialogCaption    ( );
                    }
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ClosePdfDocumentCommand( )
        {
            try
            {
                this.LogMessage( "Close document" );
                if ( null != this._document )
                {
                    this._document.Dispose( );
                    this._document = null;                    
                    this._pdfFilename = "";
                    this._pageIndex = 0;
                    this._pageInfo = null;
                    this._imagxview.Image.Dispose( );
                    this._notatexpress.Layers.Clear( );                    
                    this.RefreshDialogCaption( );
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion
        #region Import/Export Commands
        private void ExportCommentsToDataFileCommand( )
        {
            this.LogMessage( "Export comments to data file" );
            try
            {
                using ( SaveFileDialog saveFileDialog = new SaveFileDialog( ) )
                {
                    saveFileDialog.Title            = "Export Comments";
                    saveFileDialog.OverwritePrompt  = true;
                    saveFileDialog.DefaultExt       = ".xfdf";
                    saveFileDialog.Filter           = "XFDF (*.xfdf)|*.xfdf|All files (*.*)|*.*";
                    saveFileDialog.FilterIndex      = 0;
                    if ( DialogResult.OK == saveFileDialog.ShowDialog( this ) )
                    {
                        // Save the current annotation state
                        //
                        this.SavePageAnnotations    ( );

                        XfdfOptions xfdfOptions     = new XfdfOptions( );
                        byte[] xfdfData             = this._document.ExportXfdf( xfdfOptions  );
                        WriteLocalBufferToFile      ( saveFileDialog.FileName , xfdfData );
                    }
                }                
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ImportCommentsFromDataFileCommand( )
        {
            this.LogMessage( "Import comments from data file" );
            try
            {
                using ( OpenFileDialog openFileDialog = new OpenFileDialog( ) )
                {
                    openFileDialog.CheckFileExists  = true;
                    openFileDialog.CheckPathExists  = true;
                    openFileDialog.DefaultExt       = ".xfdf";
                    openFileDialog.Filter           = "XFDF (*.xfdf)|*.xfdf|All files (*.*)|*.*";
                    openFileDialog.FilterIndex      = 0;
                    openFileDialog.Title            = "Import Comments" ;
                    if ( DialogResult.OK == openFileDialog.ShowDialog( this ) )
                    {
                        // Will replace all annotations with the ones
                        // described in the source file.
                        byte[] xfdfData             = ReadFileToLocalBuffer( openFileDialog.FileName ) ;
                        XfdfOptions xfdfOptions     = new XfdfOptions( );
                        this._document.ImportXfdf( xfdfOptions , xfdfData );

                        // Display the annotations.
                        //
                        this.ReloadPageAnnotations( );
                    }
                }                 
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion
        #region Annotation Commands
        private void ActivateHighlightToolCommand( )
        {
            this.LogMessage( "Activate Highlight Tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );                
                this._notatexpress.InteractMode = AnnotationMode.Edit;
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {                    
                    currentLayer.Toolbar.Selected = AnnotationTool.BlockHighlightTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateEllipseToolCommand( )
        {
            this.LogMessage( "Activate Ellipse Tool" );
            try
            {                
                this.DisableSpecialAnnotationHandlers( ); 
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.EllipseTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateStrikeOutToolCommand( )
        {
            this.LogMessage( "Activate Strikeout tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                this.InstallWillAddStrikeoutAnnotationHandler( );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivatePointerToolCommand( )
        {
            this.LogMessage( "Activate Pointer tool" );
            try
            {                
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.PointerTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateTextToolCommand( )
        {
            this.LogMessage( "Activate Text tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                this._notatexpress.InteractMode = AnnotationMode.Edit;
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.TextTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateArrowToolCommand( )
        {
            this.LogMessage( "Activate Arrow tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.LineTool;
                    currentLayer.Toolbar.LineToolbarDefaults.EndPointStyle = EndPointStyle.Arrow;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateLineToolCommand( )
        {
            this.LogMessage( "Activate Line tool" );
            try
            {                
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.LineTool;
                    currentLayer.Toolbar.LineToolbarDefaults.EndPointStyle = EndPointStyle.Plain;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateRectangleToolCommand( )
        {
            this.LogMessage( "Activate Rectangle tool" );
            try
            {                
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.RectangleTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivatePolyLineToolCommand( )
        {
            this.LogMessage( "Activate PolyLine tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.PolyLineTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivatePolygonToolCommand( )
        {
            this.LogMessage( "Activate Polygon tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.PolygonTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivatePencilToolCommand( )
        {
            this.LogMessage( "Activate Pencil tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.FreehandTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateUnderlineToolCommand( )
        {
            this.LogMessage( "Activate Underline tool" );
            try
            {                
                this.DisableSpecialAnnotationHandlers( );
                this.InstallWillAddUnderlineAnnotationHandler( );
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        private void ActivateStickyNoteToolCommand( )
        {
            this.LogMessage( "Activate Sticky Text tool" );
            try
            {
                this.DisableSpecialAnnotationHandlers( );
                using ( Layer currentLayer = this._notatexpress.Layers.Selected( ) )
                {
                    currentLayer.Toolbar.Selected = AnnotationTool.NoteTool;
                }
            }
            catch ( Exception ex )
            {
                this.LogMessage( ex.ToString( ) );
            }
        }
        #endregion
        #region Miscellaneous
        /// <summary>
        /// Serialize a file to a byte array.
        /// </summary>
        /// <param name="filename">The path to the file to read.</param>
        /// <returns></returns>
        private static byte[ ] ReadFileToLocalBuffer( String filename )
        {
            FileInfo fileInfo           = new FileInfo( filename );
            byte[ ] buffer              = new byte[ fileInfo.Length ];
            using ( FileStream fileStream = new FileStream
                                            ( filename                                            
                                            , System.IO.FileMode.Open
                                            , FileAccess.Read 
                                            ) )
            {
                int bytesRead           = fileStream.Read
                                            ( buffer
                                            , 0
                                            , ( int )fileInfo.Length
                                            );
            }
            return                      buffer;
        }
        private static void WriteLocalBufferToFile( String filename , byte[] data )
        {
            FileInfo fileInfo           = new FileInfo( filename );
            FileMode fileMode           = FileMode.OpenOrCreate ;
            if ( fileInfo.Exists )
            {
                fileMode               |= FileMode.Truncate ;
            }
            using ( FileStream fileStream = new FileStream
                                            ( filename
                                            , fileMode
                                            , FileAccess.Write 
                                            ) )
            {
                fileStream.Write( data , 0 , data.Length );
            }
        }
        #endregion

        #endregion
    }
}
/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Accusoft.PdfXpressSdk;
using Accusoft.ImagXpressSdk;

namespace PDFToImage
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
    {
		private System.Windows.Forms.OpenFileDialog OpenDialog;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button buttonSelectPDF;
		private System.Windows.Forms.TextBox textBoxPDF;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSelectSaveLocation;
		private System.Windows.Forms.TextBox textBoxOutputLocation;
		private System.Windows.Forms.GroupBox SaveGroupBox;
		private System.Windows.Forms.RadioButton radioButtonJBIG2;
		private System.Windows.Forms.Button buttonConvert;
		private System.Windows.Forms.RadioButton radioButtonMultiPageTIFF;
        private System.Windows.Forms.RadioButton radioButtonJPEG;
		private System.Windows.Forms.RadioButton radioButtonSinglePageTIFF;
		private System.Windows.Forms.GroupBox groupBoxConversionOpts;
		private System.Windows.Forms.MainMenu MainMenu;
		private System.Windows.Forms.MenuItem AboutMenu;
        private System.Windows.Forms.MenuItem PDFXpressMenuItem;
        private MenuItem FileMenu;
        private MenuItem ExitMenuItem;
        private MenuItem ImagXpressMenuItem;
        private FolderBrowserDialog browseDialog;
        private IContainer components;

        private ProgressBar ProgressBar;
        private Label ProgLabel;
        private Label ProgLabel2;
        private ListBox lstDesc;
        private PdfXpress pdfXpress;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
      
        private ImageX img;

		public FormMain()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }
                if (pdfXpress != null)
                {
                    pdfXpress.Dispose();
                    pdfXpress = null;
                }
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.OpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxConversionOpts = new System.Windows.Forms.GroupBox();
            this.SaveGroupBox = new System.Windows.Forms.GroupBox();
            this.radioButtonSinglePageTIFF = new System.Windows.Forms.RadioButton();
            this.radioButtonMultiPageTIFF = new System.Windows.Forms.RadioButton();
            this.radioButtonJPEG = new System.Windows.Forms.RadioButton();
            this.radioButtonJBIG2 = new System.Windows.Forms.RadioButton();
            this.buttonSelectPDF = new System.Windows.Forms.Button();
            this.textBoxPDF = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSelectSaveLocation = new System.Windows.Forms.Button();
            this.textBoxOutputLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonConvert = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.ExitMenuItem = new System.Windows.Forms.MenuItem();
            this.AboutMenu = new System.Windows.Forms.MenuItem();
            this.ImagXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.PDFXpressMenuItem = new System.Windows.Forms.MenuItem();
            this.browseDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.ProgLabel = new System.Windows.Forms.Label();
            this.ProgLabel2 = new System.Windows.Forms.Label();
            this.lstDesc = new System.Windows.Forms.ListBox();
            this.pdfXpress = new Accusoft.PdfXpressSdk.PdfXpress();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.groupBoxConversionOpts.SuspendLayout();
            this.SaveGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxConversionOpts
            // 
            this.groupBoxConversionOpts.Controls.Add(this.SaveGroupBox);
            this.groupBoxConversionOpts.Controls.Add(this.buttonSelectPDF);
            this.groupBoxConversionOpts.Controls.Add(this.textBoxPDF);
            this.groupBoxConversionOpts.Controls.Add(this.label1);
            this.groupBoxConversionOpts.Controls.Add(this.buttonSelectSaveLocation);
            this.groupBoxConversionOpts.Controls.Add(this.textBoxOutputLocation);
            this.groupBoxConversionOpts.Controls.Add(this.label2);
            this.groupBoxConversionOpts.Location = new System.Drawing.Point(15, 76);
            this.groupBoxConversionOpts.Name = "groupBoxConversionOpts";
            this.groupBoxConversionOpts.Size = new System.Drawing.Size(650, 197);
            this.groupBoxConversionOpts.TabIndex = 1;
            this.groupBoxConversionOpts.TabStop = false;
            this.groupBoxConversionOpts.Text = "Conversion Options";
            // 
            // SaveGroupBox
            // 
            this.SaveGroupBox.Controls.Add(this.radioButtonSinglePageTIFF);
            this.SaveGroupBox.Controls.Add(this.radioButtonMultiPageTIFF);
            this.SaveGroupBox.Controls.Add(this.radioButtonJPEG);
            this.SaveGroupBox.Controls.Add(this.radioButtonJBIG2);
            this.SaveGroupBox.Location = new System.Drawing.Point(15, 114);
            this.SaveGroupBox.Name = "SaveGroupBox";
            this.SaveGroupBox.Size = new System.Drawing.Size(621, 68);
            this.SaveGroupBox.TabIndex = 6;
            this.SaveGroupBox.TabStop = false;
            this.SaveGroupBox.Text = "Save As";
            // 
            // radioButtonSinglePageTIFF
            // 
            this.radioButtonSinglePageTIFF.Checked = true;
            this.radioButtonSinglePageTIFF.Location = new System.Drawing.Point(460, 23);
            this.radioButtonSinglePageTIFF.Name = "radioButtonSinglePageTIFF";
            this.radioButtonSinglePageTIFF.Size = new System.Drawing.Size(139, 30);
            this.radioButtonSinglePageTIFF.TabIndex = 3;
            this.radioButtonSinglePageTIFF.TabStop = true;
            this.radioButtonSinglePageTIFF.Text = "Single-Page TIFF (G4)";
            // 
            // radioButtonMultiPageTIFF
            // 
            this.radioButtonMultiPageTIFF.Checked = true;
            this.radioButtonMultiPageTIFF.Location = new System.Drawing.Point(307, 23);
            this.radioButtonMultiPageTIFF.Name = "radioButtonMultiPageTIFF";
            this.radioButtonMultiPageTIFF.Size = new System.Drawing.Size(157, 30);
            this.radioButtonMultiPageTIFF.TabIndex = 2;
            this.radioButtonMultiPageTIFF.TabStop = true;
            this.radioButtonMultiPageTIFF.Text = "Multi-Page TIFF (G4)";
            // 
            // radioButtonJPEG
            // 
            this.radioButtonJPEG.Checked = true;
            this.radioButtonJPEG.Location = new System.Drawing.Point(161, 23);
            this.radioButtonJPEG.Name = "radioButtonJPEG";
            this.radioButtonJPEG.Size = new System.Drawing.Size(139, 30);
            this.radioButtonJPEG.TabIndex = 1;
            this.radioButtonJPEG.TabStop = true;
            this.radioButtonJPEG.Text = "JPEG (Single-page)";
            // 
            // radioButtonJBIG2
            // 
            this.radioButtonJBIG2.Checked = true;
            this.radioButtonJBIG2.Location = new System.Drawing.Point(18, 23);
            this.radioButtonJBIG2.Name = "radioButtonJBIG2";
            this.radioButtonJBIG2.Size = new System.Drawing.Size(139, 30);
            this.radioButtonJBIG2.TabIndex = 0;
            this.radioButtonJBIG2.TabStop = true;
            this.radioButtonJBIG2.Text = "JBIG2 (Single-page)";
            // 
            // buttonSelectPDF
            // 
            this.buttonSelectPDF.Location = new System.Drawing.Point(511, 38);
            this.buttonSelectPDF.Name = "buttonSelectPDF";
            this.buttonSelectPDF.Size = new System.Drawing.Size(125, 23);
            this.buttonSelectPDF.TabIndex = 2;
            this.buttonSelectPDF.Text = "Browse";
            this.buttonSelectPDF.Click += new System.EventHandler(this.buttonSelectPDF_Click);
            // 
            // textBoxPDF
            // 
            this.textBoxPDF.Location = new System.Drawing.Point(15, 38);
            this.textBoxPDF.Name = "textBoxPDF";
            this.textBoxPDF.ReadOnly = true;
            this.textBoxPDF.Size = new System.Drawing.Size(489, 20);
            this.textBoxPDF.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(490, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "PDF File to Convert:";
            // 
            // buttonSelectSaveLocation
            // 
            this.buttonSelectSaveLocation.Location = new System.Drawing.Point(511, 83);
            this.buttonSelectSaveLocation.Name = "buttonSelectSaveLocation";
            this.buttonSelectSaveLocation.Size = new System.Drawing.Size(125, 23);
            this.buttonSelectSaveLocation.TabIndex = 5;
            this.buttonSelectSaveLocation.Text = "Browse";
            this.buttonSelectSaveLocation.Click += new System.EventHandler(this.buttonSelectSaveLocation_Click);
            // 
            // textBoxOutputLocation
            // 
            this.textBoxOutputLocation.Location = new System.Drawing.Point(15, 83);
            this.textBoxOutputLocation.Name = "textBoxOutputLocation";
            this.textBoxOutputLocation.ReadOnly = true;
            this.textBoxOutputLocation.Size = new System.Drawing.Size(489, 20);
            this.textBoxOutputLocation.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Save Location:";
            // 
            // buttonConvert
            // 
            this.buttonConvert.Location = new System.Drawing.Point(528, 280);
            this.buttonConvert.Name = "buttonConvert";
            this.buttonConvert.Size = new System.Drawing.Size(132, 30);
            this.buttonConvert.TabIndex = 3;
            this.buttonConvert.Text = "Convert File";
            this.buttonConvert.Click += new System.EventHandler(this.buttonConvert_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu,
            this.AboutMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ExitMenuItem});
            this.FileMenu.Text = "&File";
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Index = 0;
            this.ExitMenuItem.Text = "E&xit";
            this.ExitMenuItem.Click += new System.EventHandler(this.ExitMenuItem_Click);
            // 
            // AboutMenu
            // 
            this.AboutMenu.Index = 1;
            this.AboutMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ImagXpressMenuItem,
            this.PDFXpressMenuItem});
            this.AboutMenu.Text = "&About";
            // 
            // ImagXpressMenuItem
            // 
            this.ImagXpressMenuItem.Index = 0;
            this.ImagXpressMenuItem.Text = "Imag&Xpress";
            this.ImagXpressMenuItem.Click += new System.EventHandler(this.ImagXpressMenuItem_Click);
            // 
            // PDFXpressMenuItem
            // 
            this.PDFXpressMenuItem.Index = 1;
            this.PDFXpressMenuItem.Text = "&PDFXpress";
            this.PDFXpressMenuItem.Click += new System.EventHandler(this.PDFXpressMenuItem_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.ForeColor = System.Drawing.Color.Lime;
            this.ProgressBar.Location = new System.Drawing.Point(12, 331);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(507, 30);
            this.ProgressBar.Step = 1;
            this.ProgressBar.TabIndex = 4;
            // 
            // ProgLabel
            // 
            this.ProgLabel.Location = new System.Drawing.Point(15, 287);
            this.ProgLabel.Name = "ProgLabel";
            this.ProgLabel.Size = new System.Drawing.Size(191, 23);
            this.ProgLabel.TabIndex = 2;
            // 
            // ProgLabel2
            // 
            this.ProgLabel2.Location = new System.Drawing.Point(555, 338);
            this.ProgLabel2.Name = "ProgLabel2";
            this.ProgLabel2.Size = new System.Drawing.Size(96, 23);
            this.ProgLabel2.TabIndex = 5;
            // 
            // lstDesc
            // 
            this.lstDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDesc.FormattingEnabled = true;
            this.lstDesc.Items.AddRange(new object[] {
            "This sample demonstates loading a PDF file into PDF Xpress and then rendering to " +
                "an image",
            "of the specified option in the SaveAs options."});
            this.lstDesc.Location = new System.Drawing.Point(9, 13);
            this.lstDesc.Name = "lstDesc";
            this.lstDesc.Size = new System.Drawing.Size(651, 43);
            this.lstDesc.TabIndex = 1;
            // 
            // pdfXpress
            // 
            this.pdfXpress.Debug = false;
            this.pdfXpress.DebugLogFile = "";
            this.pdfXpress.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(672, 377);
            this.Controls.Add(this.lstDesc);
            this.Controls.Add(this.ProgLabel2);
            this.Controls.Add(this.ProgLabel);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.buttonConvert);
            this.Controls.Add(this.groupBoxConversionOpts);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.MainMenu;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PDF To Image Converter";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBoxConversionOpts.ResumeLayout(false);
            this.groupBoxConversionOpts.PerformLayout();
            this.SaveGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void menuItemFileExit_Click(object sender, System.EventArgs e)
		{
			Application.Exit ();
		}

		private void menuItemFileAbout_Click(object sender, System.EventArgs e)
		{
			pdfXpress.AboutBox ();
		}

		private void buttonSelectPDF_Click(object sender, System.EventArgs e)
		{
            try
            {
                OpenDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                OpenDialog.Title = "Open PDF Document";
                System.String currentDir;
                currentDir = System.IO.Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images");
                OpenDialog.InitialDirectory = currentDir;
                OpenDialog.ShowDialog();

                if (OpenDialog.FileName != "")
                {
                    textBoxPDF.Text = OpenDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
		}
        
        private void buttonSelectSaveLocation_Click(object sender, System.EventArgs e)
        {
            browseDialog.ShowNewFolderButton = true;

            browseDialog.ShowDialog();

            if (browseDialog.SelectedPath != "")
            {
                textBoxOutputLocation.Text = browseDialog.SelectedPath;
            }
        }

		private void buttonConvert_Click(object sender, System.EventArgs e)
		{
			try
			{

                ProgressBar.Value = 0;
				if (System.IO.File.Exists (textBoxPDF.Text) && System.IO.Directory.Exists (textBoxOutputLocation.Text))
				{
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
					// make sure no documents are currently loaded
					pdfXpress.Documents.Clear();				    

					// load the PDF document
					System.Int32 index = pdfXpress.Documents.Add(textBoxPDF.Text);
			
					// initialize the variables we need to write out the files
					System.String outputFileBase = System.IO.Path.GetFileNameWithoutExtension (textBoxPDF.Text);
					System.Int32 currentPage = 0;
					
					// Please note: If the DIB is being passed to an ImagXpress control
					// then the ProduceDibSection property of the RenderOptions class should be set = False.
					RenderOptions renderOpts = new RenderOptions();
					renderOpts.ProduceDibSection = false;
                    renderOpts.ResolutionX = 300;
                    renderOpts.ResolutionY = 300;

                    Accusoft.ImagXpressSdk.SaveOptions so = new Accusoft.ImagXpressSdk.SaveOptions();

                    ProgressBar.Maximum = pdfXpress.Documents[index].PageCount;
                    ProgressBar.Step = 1;

					// render and save each page
                    System.Int32 pageIndex, pageShown;
					for (pageIndex = 0; pageIndex < pdfXpress.Documents[index].PageCount; pageIndex++)
					{                        
                        ProgressBar.PerformStep();
                        pageShown = pageIndex + 1;
                        ProgLabel.Text = "Converting Page: " + pageShown.ToString() + " of " + pdfXpress.Documents[index].PageCount.ToString();
                        ProgLabel2.Text = string.Format("{0:0.00}%", 100 * (float)ProgressBar.Value / (float)ProgressBar.Maximum);

						img = ImageX.FromBitmap(imagXpress1, pdfXpress.Documents[index].RenderPageToBitmap(pageIndex, renderOpts));
					
						// figure out how to save this file based on desired output type
						System.String newFilePathWithName = System.String.Empty;
						if (radioButtonJBIG2.Checked)
						{
							newFilePathWithName = System.IO.Path.Combine (textBoxOutputLocation.Text, outputFileBase + "_" + currentPage + ".jb2");
							so.Format = ImageXFormat.Jbig2;
						}
						else if (radioButtonJPEG.Checked)
						{
							newFilePathWithName = System.IO.Path.Combine (textBoxOutputLocation.Text, outputFileBase + "_" + currentPage + ".jpg");
                            so.Format = ImageXFormat.Jpeg;
						}
						else if (radioButtonMultiPageTIFF.Checked)
						{
							if (currentPage == 0)
							{
								so.Tiff.MultiPage = true;
							}
							
							newFilePathWithName = System.IO.Path.Combine (textBoxOutputLocation.Text, outputFileBase + ".tif");
							so.Format = ImageXFormat.Tiff;
                            so.Tiff.Compression = Compression.Group4;
						}
						else if (radioButtonSinglePageTIFF.Checked)
						{
                            so.Tiff.MultiPage = false;
							
							newFilePathWithName = System.IO.Path.Combine (textBoxOutputLocation.Text, outputFileBase + "_" + currentPage + ".tif");
							so.Format = ImageXFormat.Tiff;
                            so.Tiff.Compression = Compression.Group4;
						}

						img.Save( newFilePathWithName, so);
															
						// allow other events to keep processing
                        Application.DoEvents();

						// increment the currentPage, so the next file name gets set
						currentPage++;
					}

                    this.Cursor = System.Windows.Forms.Cursors.Default;
				}
				else
				{
					MessageBox.Show ("You must select a PDF to convert and output location for the conversion to occur");
				}
			}
			catch (System.Exception ex)
			{
                this.Cursor = System.Windows.Forms.Cursors.Default;

				MessageBox.Show (ex.Message);
			}
		}

        private void FormMain_Load(object sender, EventArgs e)
        {
            // TODO: Change the 4 runtime unlock codes below to those purchased once a license agreement is in place.
            //       To purchase runtime licensing for this control, please contact sales@accusoft.com.

            //imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);            
            //pdfXpress.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);


            pdfXpress.Initialize(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\Support\\Font", Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\Support\\CMap");

            Application.EnableVisualStyles();
        }

        private void ImagXpressMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void PDFXpressMenuItem_Click(object sender, EventArgs e)
        {
            pdfXpress.AboutBox();
        }

        private void ExitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();        
        }
	}
}

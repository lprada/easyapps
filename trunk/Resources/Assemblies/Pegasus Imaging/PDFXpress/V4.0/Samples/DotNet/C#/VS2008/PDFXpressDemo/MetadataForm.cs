/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PDFDemo
{
    public partial class MetaDataForm : Form
    {
        public MetaDataForm()
        {
            InitializeComponent();
        }

        public string Author
        {
            get
            {
                return AuthorTextBox.Text;
            }
            set
            {
                AuthorTextBox.Text = value;
            }
        }

        public string Subject
        {
            get
            {
                return SubjectTextBox.Text;
            }
            set
            {
                SubjectTextBox.Text = value;
            }
        }

        public string Title
        {
            get
            {
                return TitleTextBox.Text;
            }
            set
            {
                TitleTextBox.Text = value;
            }
        }

        public string Keywords
        {
            get
            {
                return KeywordsTextBox.Text;
            }
            set
            {
                KeywordsTextBox.Text = value;
            }
        }

        public string Creator
        {
            get
            {
                return CreatorTextBox.Text;
            }
            set
            {
                CreatorTextBox.Text = value;
            }
        }

        public string CreationDate
        {
            get
            {
                return CreationDateTextBox.Text;
            }
            set
            {
                CreationDateTextBox.Text = value;
            }
        }

        public string Producer
        {
            get
            {
                return ProducerTextBox.Text;
            }
            set
            {
                ProducerTextBox.Text = value;
            }
        }

        public string ModificationDate
        {
            get
            {
                return ModificationDatetextBox.Text;
            }
            set
            {
                ModificationDatetextBox.Text = value;
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.OK;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelMetaButton_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult = DialogResult.Cancel;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
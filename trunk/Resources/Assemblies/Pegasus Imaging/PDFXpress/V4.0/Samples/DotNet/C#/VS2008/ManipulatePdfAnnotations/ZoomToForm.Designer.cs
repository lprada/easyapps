/// Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
namespace ManipulatePdfAnnotations
{
    partial class ZoomToForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this._magnificationLabel = new System.Windows.Forms.Label( );
            this._okButton = new System.Windows.Forms.Button( );
            this._cancelButton = new System.Windows.Forms.Button( );
            this._magnificationComboBox = new System.Windows.Forms.ComboBox( );
            this.SuspendLayout( );
            // 
            // _magnificationLabel
            // 
            this._magnificationLabel.Location = new System.Drawing.Point( 12 , 24 );
            this._magnificationLabel.Name = "_magnificationLabel";
            this._magnificationLabel.Size = new System.Drawing.Size( 159 , 16 );
            this._magnificationLabel.TabIndex = 0;
            this._magnificationLabel.Text = "&Magnification:";
            // 
            // _okButton
            // 
            this._okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._okButton.Location = new System.Drawing.Point( 15 , 83 );
            this._okButton.Name = "_okButton";
            this._okButton.Size = new System.Drawing.Size( 75 , 23 );
            this._okButton.TabIndex = 1;
            this._okButton.Text = "&OK";
            this._okButton.UseVisualStyleBackColor = true;
            // 
            // _cancelButton
            // 
            this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._cancelButton.Location = new System.Drawing.Point( 96 , 83 );
            this._cancelButton.Name = "_cancelButton";
            this._cancelButton.Size = new System.Drawing.Size( 75 , 23 );
            this._cancelButton.TabIndex = 2;
            this._cancelButton.Text = "&Cancel";
            this._cancelButton.UseVisualStyleBackColor = true;
            // 
            // _magnificationComboBox
            // 
            this._magnificationComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._magnificationComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._magnificationComboBox.FormattingEnabled = true;
            this._magnificationComboBox.Location = new System.Drawing.Point( 15 , 43 );
            this._magnificationComboBox.Name = "_magnificationComboBox";
            this._magnificationComboBox.Size = new System.Drawing.Size( 156 , 21 );
            this._magnificationComboBox.TabIndex = 3;
            this._magnificationComboBox.Text = "Fit Page";
            // 
            // ZoomToForm
            // 
            this.AcceptButton = this._okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F , 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 185 , 118 );
            this.Controls.Add( this._magnificationComboBox );
            this.Controls.Add( this._cancelButton );
            this.Controls.Add( this._okButton );
            this.Controls.Add( this._magnificationLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZoomToForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Zoom To";
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Label _magnificationLabel;
        private System.Windows.Forms.Button _okButton;
        private System.Windows.Forms.Button _cancelButton;
        private System.Windows.Forms.ComboBox _magnificationComboBox;
    }
}
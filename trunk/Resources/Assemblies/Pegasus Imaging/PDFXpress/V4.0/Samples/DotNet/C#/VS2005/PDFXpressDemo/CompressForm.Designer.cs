/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
namespace PDFDemo
{
    partial class CompressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompressForm));
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelFormButton = new System.Windows.Forms.Button();
            this.OptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.G4RadioButton = new System.Windows.Forms.RadioButton();
            this.Jbig2RadioButton = new System.Windows.Forms.RadioButton();
            this.JpegRadioButton = new System.Windows.Forms.RadioButton();
            this.OptionsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(12, 132);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 1;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelFormButton
            // 
            this.CancelFormButton.Location = new System.Drawing.Point(169, 132);
            this.CancelFormButton.Name = "CancelFormButton";
            this.CancelFormButton.Size = new System.Drawing.Size(75, 23);
            this.CancelFormButton.TabIndex = 2;
            this.CancelFormButton.Text = "Cancel";
            this.CancelFormButton.UseVisualStyleBackColor = true;
            this.CancelFormButton.Click += new System.EventHandler(this.CancelFormButton_Click);
            // 
            // OptionsGroupBox
            // 
            this.OptionsGroupBox.Controls.Add(this.G4RadioButton);
            this.OptionsGroupBox.Controls.Add(this.Jbig2RadioButton);
            this.OptionsGroupBox.Controls.Add(this.JpegRadioButton);
            this.OptionsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.OptionsGroupBox.Name = "OptionsGroupBox";
            this.OptionsGroupBox.Size = new System.Drawing.Size(232, 104);
            this.OptionsGroupBox.TabIndex = 0;
            this.OptionsGroupBox.TabStop = false;
            this.OptionsGroupBox.Text = "Options";
            // 
            // G4RadioButton
            // 
            this.G4RadioButton.AutoSize = true;
            this.G4RadioButton.Location = new System.Drawing.Point(15, 75);
            this.G4RadioButton.Name = "G4RadioButton";
            this.G4RadioButton.Size = new System.Drawing.Size(173, 17);
            this.G4RadioButton.TabIndex = 2;
            this.G4RadioButton.Text = "CCITT G4 (works well for B&&W)";
            this.G4RadioButton.UseVisualStyleBackColor = true;
            // 
            // Jbig2RadioButton
            // 
            this.Jbig2RadioButton.AutoSize = true;
            this.Jbig2RadioButton.Location = new System.Drawing.Point(15, 47);
            this.Jbig2RadioButton.Name = "Jbig2RadioButton";
            this.Jbig2RadioButton.Size = new System.Drawing.Size(156, 17);
            this.Jbig2RadioButton.TabIndex = 1;
            this.Jbig2RadioButton.Text = "JBIG2 (works best for B&&W)";
            this.Jbig2RadioButton.UseVisualStyleBackColor = true;
            // 
            // JpegRadioButton
            // 
            this.JpegRadioButton.AutoSize = true;
            this.JpegRadioButton.Checked = true;
            this.JpegRadioButton.Location = new System.Drawing.Point(15, 19);
            this.JpegRadioButton.Name = "JpegRadioButton";
            this.JpegRadioButton.Size = new System.Drawing.Size(197, 17);
            this.JpegRadioButton.TabIndex = 0;
            this.JpegRadioButton.TabStop = true;
            this.JpegRadioButton.Text = "JPEG (works best for color but lossy)";
            this.JpegRadioButton.UseVisualStyleBackColor = true;
            // 
            // CompressForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 164);
            this.Controls.Add(this.OptionsGroupBox);
            this.Controls.Add(this.CancelFormButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CompressForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Compression Options";
            this.OptionsGroupBox.ResumeLayout(false);
            this.OptionsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelFormButton;
        private System.Windows.Forms.GroupBox OptionsGroupBox;
        private System.Windows.Forms.RadioButton Jbig2RadioButton;
        private System.Windows.Forms.RadioButton JpegRadioButton;
        private System.Windows.Forms.RadioButton G4RadioButton;
    }
}
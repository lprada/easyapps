/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PDFDemo
{
    public partial class RemoveWatermarkForm : Form
    {
        public RemoveWatermarkForm()
        {
            InitializeComponent();
        }

        private int pageCount;

        public int FromPageNumber
        {
            get
            {
                return Int32.Parse(FromTextBox.Text); 
            }
        }

        public int ToPageNumber
        {
            get
            {
                return Int32.Parse(ToTextBox.Text);
            }
            set
            {
                ToTextBox.Text = pageCount.ToString();
            }
        }

        public int PageCount
        {
            set
            {
                pageCount = value;   
            }
        }

        private void RemoveWatermarkForm_Load(object sender, EventArgs e)
        {
            try
            {
                OfLabel.Text = OfLabel.Text = String.Format("of {0}", pageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void CButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
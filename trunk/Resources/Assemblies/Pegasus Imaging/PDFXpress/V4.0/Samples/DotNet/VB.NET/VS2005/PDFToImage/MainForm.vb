'****************************************************************'
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Option Explicit On

Imports Accusoft.PdfXpressSdk
Imports Accusoft.ImagXpressSdk

Public Class MainForm

    Dim img As ImageX

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub PDFXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PDFXpressToolStripMenuItem.Click
        PdfXpress.AboutBox()
    End Sub

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' TODO: Change the 4 runtime unlock codes below to those purchased once a license agreement is in place.
        '       To purchase runtime licensing for this control, please contact sales@accusoft.com.

        'imagXpress1.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);            
        'pdfXpress.Licensing.UnlockRuntime(1234, 1234, 1234, 1234);

        pdfXpress.Initialize(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\Support\\Font", Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\Support\\CMap")

        Application.EnableVisualStyles()
    End Sub

   
    Private Sub BrowseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BrowseButton.Click
        Try
            OpenDialog.Filter = "PDF Files (*.pdf)|*.pdf"
            OpenDialog.Title = "Open PDF Document"
            OpenDialog.InitialDirectory = Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\"
            OpenDialog.FileName = ""
            OpenDialog.ShowDialog()

            If Not (OpenDialog.FileName = "") Then
                textBoxPDF.Text = OpenDialog.FileName
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BrowseButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BrowseButton2.Click
        browseDialog.ShowNewFolderButton = True

        browseDialog.ShowDialog()

        If Not (browseDialog.SelectedPath = "") Then
            textBoxOutputLocation.Text = browseDialog.SelectedPath
        End If
    End Sub

    Private Sub ConvertButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConvertButton.Click
        Try			

            ProgressBar.Value = 0
            If (System.IO.File.Exists(textBoxPDF.Text) And System.IO.Directory.Exists(textBoxOutputLocation.Text)) Then
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' make sure no documents are currently loaded
                pdfXpress.Documents.Clear()

                'load the PDF document
                Dim index As Integer = pdfXpress.Documents.Add(textBoxPDF.Text)

                ' initialize the variables we need to write out the files
                Dim outputFileBase As String = System.IO.Path.GetFileNameWithoutExtension(textBoxPDF.Text)
                Dim currentPage As Integer = 0

                ' Please note: If the DIB is being passed to an ImagXpress control
                ' then the ProduceDibSection property of the RenderOptions class should be set = False.
                Dim renderOpts As RenderOptions = New RenderOptions()
                renderOpts.ProduceDibSection = False
                renderOpts.ResolutionX = 300
                renderOpts.ResolutionY = 300

                Dim so As Accusoft.ImagXpressSdk.SaveOptions = New Accusoft.ImagXpressSdk.SaveOptions()

                ProgressBar.Maximum = pdfXpress.Documents(index).PageCount
                ProgressBar.Step = 1

                ' render and save each page
                Dim pageIndex, pageShown As Integer

                For pageIndex = 0 To pdfXpress.Documents(index).PageCount - 1

                    ProgressBar.PerformStep()
                    pageShown = pageIndex + 1
                    ProgLabel.Text = "Converting Page: " + pageShown.ToString() + " of " + pdfXpress.Documents(index).PageCount.ToString()
                    ProgLabel2.Text = String.Format("{0:0.00}%", 100 * CLng(ProgressBar.Value) / CLng(ProgressBar.Maximum))

                    img = ImageX.FromBitmap(ImagXpress1, pdfXpress.Documents(index).RenderPageToBitmap(pageIndex, renderOpts))

                    ' figure out how to save this file based on desired output type
                    Dim newFilePathWithName As String = System.String.Empty
                    If (radioButtonSinglePageJBIG2.Checked) Then
                        newFilePathWithName = System.IO.Path.Combine(textBoxOutputLocation.Text, outputFileBase + "_" + currentPage.ToString() + ".jb2")
                        so.Format = ImageXFormat.Jbig2
                    ElseIf (radioButtonSinglePageJPEG.Checked) Then
                        newFilePathWithName = System.IO.Path.Combine(textBoxOutputLocation.Text, outputFileBase + "_" + currentPage.ToString() + ".jpg")
                        so.Format = ImageXFormat.Jpeg
                    ElseIf (radioButtonMultiPageTIFF.Checked) Then
                        If (currentPage = 0) Then
                            so.Tiff.MultiPage = True
                        End If

                        newFilePathWithName = System.IO.Path.Combine(textBoxOutputLocation.Text, outputFileBase + ".tif")
                        so.Format = ImageXFormat.Tiff
                        so.Tiff.Compression = Compression.Group4
                    ElseIf (radioButtonSinglePageTIFF.Checked) Then
                        newFilePathWithName = System.IO.Path.Combine(textBoxOutputLocation.Text, outputFileBase + "_" + currentPage.ToString() + ".tif")
                        so.Format = ImageXFormat.Tiff
                        so.Tiff.Compression = Compression.Group4
                        so.Tiff.MultiPage = False
                    End If

                    img.Save(newFilePathWithName, so)

                    ' allow other events to keep processing
                    Application.DoEvents()

                    ' increment the currentPage, so the next file name gets set
                    currentPage = currentPage + 1

                Next pageIndex

            Me.Cursor = System.Windows.Forms.Cursors.Default

            Else
                MessageBox.Show("You must select a PDF to convert and output location for the conversion to occur")
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ImagXpressToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressToolStripMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub
End Class

'****************************************************************
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
'* This sample code is provided to Pegasus licensees "as is"    *
'* with no restrictions on use or modification. No warranty for *
'* use of this sample code is provided by Pegasus.              *
' ****************************************************************/
Option Explicit On

Imports System.IO

Public Class InsertForm
    Public ReadOnly Property IncludeAllData() As Boolean
        Get
            Return includeCheckBox.Checked
        End Get
    End Property

    Public ReadOnly Property IncludeBookmarks() As Boolean
        Get
            Return bookmarksCheckBox.Checked
        End Get
    End Property

    Public ReadOnly Property IncludeArticles() As Boolean
        Get
            Return articlesCheckBox.Checked
        End Get
    End Property

    Public ReadOnly Property InsertPageNumber() As Integer
        Get
            Return CType(insertTextBox.Text, Integer)
        End Get
    End Property

    Public ReadOnly Property SourceFile() As String
        Get
            Return browseTextBox.Text
        End Get
    End Property

    Public ReadOnly Property UseAllPages() As Boolean
        Get
            Return useCheckBox.Checked
        End Get
    End Property

    Public ReadOnly Property StartPage() As Integer
        Get
            Return CType(startTextBox.Text, Integer)
        End Get
    End Property

    Public ReadOnly Property PageCount() As Integer
        Get
            Return CType(pageCountTextBox.Text, Integer)
        End Get
    End Property

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Try
            If (browseTextBox.Text = "") Then
                MessageBox.Show("Please choose a Source Document first.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                DialogResult = Windows.Forms.DialogResult.OK

                Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub CancelInsertButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelInsertButton.Click
        Try
            DialogResult = Windows.Forms.DialogResult.Cancel

            Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub browseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles browseButton.Click
        Try
            Using openFileDialog As OpenFileDialog = New OpenFileDialog()
                openFileDialog.Title = "Open a PDF Document"
                openFileDialog.InitialDirectory = Path.GetFullPath(Application.StartupPath + "\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\")
                openFileDialog.Filter = "PDF Document (*.PDF)|*.PDF|All Files (*.*) | *.*"

                If (openFileDialog.ShowDialog() = DialogResult.OK) Then
                    browseTextBox.Text = openFileDialog.FileName
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
﻿/// Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. 
/// This sample code is provided to Pegasus licensees "as is" with 
/// no restrictions on use or modification. No warranty for use of 
/// this sample code is provided by Pegasus.
/// 
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "ManipulatePdfAnnotations" )]
[assembly: AssemblyDescription( "This sample demonstrates using NotateXpress to manipulate PDF Annotations and ImagXpress to delegate rendering activities to PDF Xpress using a virtual image." )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "Accusoft Pegasus" )]
[assembly: AssemblyProduct( "PDF Xpress 4" )]
[assembly: AssemblyCopyright( "Copyright © 2010 Pegasus Imaging Corporation" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible( false )]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid( "af3aa4b3-4dc0-4b6c-b621-fdc8fd0a2650" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

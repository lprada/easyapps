'****************************************************************'
'* Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not (ImagXpress1 Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (pdfXpress Is Nothing) Then
                pdfXpress.Dispose()
                pdfXpress = Nothing
            End If
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.groupBoxConversionOpts = New System.Windows.Forms.GroupBox
        Me.BrowseButton2 = New System.Windows.Forms.Button
        Me.BrowseButton = New System.Windows.Forms.Button
        Me.PDFLabel = New System.Windows.Forms.Label
        Me.SaveLabel = New System.Windows.Forms.Label
        Me.textBoxPDF = New System.Windows.Forms.TextBox
        Me.textBoxOutputLocation = New System.Windows.Forms.TextBox
        Me.SaveGroupBox = New System.Windows.Forms.GroupBox
        Me.radioButtonSinglePageJPEG = New System.Windows.Forms.RadioButton
        Me.radioButtonSinglePageJBIG2 = New System.Windows.Forms.RadioButton
        Me.radioButtonSinglePageTIFF = New System.Windows.Forms.RadioButton
        Me.radioButtonMultiPageTIFF = New System.Windows.Forms.RadioButton
        Me.MainMenu = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PDFXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ProgLabel = New System.Windows.Forms.Label
        Me.ProgLabel2 = New System.Windows.Forms.Label
        Me.ProgressBar = New System.Windows.Forms.ProgressBar
        Me.ConvertButton = New System.Windows.Forms.Button
        Me.OpenDialog = New System.Windows.Forms.OpenFileDialog
        Me.browseDialog = New System.Windows.Forms.FolderBrowserDialog
        Me.DescriptionListBox = New System.Windows.Forms.ListBox
        Me.pdfXpress = New Accusoft.PdfXpressSdk.PdfXpress
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.groupBoxConversionOpts.SuspendLayout()
        Me.SaveGroupBox.SuspendLayout()
        Me.MainMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupBoxConversionOpts
        '
        Me.groupBoxConversionOpts.Controls.Add(Me.BrowseButton2)
        Me.groupBoxConversionOpts.Controls.Add(Me.BrowseButton)
        Me.groupBoxConversionOpts.Controls.Add(Me.PDFLabel)
        Me.groupBoxConversionOpts.Controls.Add(Me.SaveLabel)
        Me.groupBoxConversionOpts.Controls.Add(Me.textBoxPDF)
        Me.groupBoxConversionOpts.Controls.Add(Me.textBoxOutputLocation)
        Me.groupBoxConversionOpts.Controls.Add(Me.SaveGroupBox)
        Me.groupBoxConversionOpts.Location = New System.Drawing.Point(8, 89)
        Me.groupBoxConversionOpts.Name = "groupBoxConversionOpts"
        Me.groupBoxConversionOpts.Size = New System.Drawing.Size(566, 208)
        Me.groupBoxConversionOpts.TabIndex = 2
        Me.groupBoxConversionOpts.TabStop = False
        Me.groupBoxConversionOpts.Text = "Conversion Options"
        '
        'BrowseButton2
        '
        Me.BrowseButton2.Location = New System.Drawing.Point(475, 93)
        Me.BrowseButton2.Name = "BrowseButton2"
        Me.BrowseButton2.Size = New System.Drawing.Size(75, 23)
        Me.BrowseButton2.TabIndex = 5
        Me.BrowseButton2.Text = "Browse"
        Me.BrowseButton2.UseVisualStyleBackColor = True
        '
        'BrowseButton
        '
        Me.BrowseButton.Location = New System.Drawing.Point(475, 40)
        Me.BrowseButton.Name = "BrowseButton"
        Me.BrowseButton.Size = New System.Drawing.Size(75, 23)
        Me.BrowseButton.TabIndex = 2
        Me.BrowseButton.Text = "Browse"
        Me.BrowseButton.UseVisualStyleBackColor = True
        '
        'PDFLabel
        '
        Me.PDFLabel.Location = New System.Drawing.Point(9, 24)
        Me.PDFLabel.Name = "PDFLabel"
        Me.PDFLabel.Size = New System.Drawing.Size(449, 13)
        Me.PDFLabel.TabIndex = 0
        Me.PDFLabel.Text = "PDF File to Convert"
        '
        'SaveLabel
        '
        Me.SaveLabel.Location = New System.Drawing.Point(12, 77)
        Me.SaveLabel.Name = "SaveLabel"
        Me.SaveLabel.Size = New System.Drawing.Size(76, 13)
        Me.SaveLabel.TabIndex = 3
        Me.SaveLabel.Text = "Save Location"
        '
        'textBoxPDF
        '
        Me.textBoxPDF.Enabled = False
        Me.textBoxPDF.Location = New System.Drawing.Point(12, 40)
        Me.textBoxPDF.Name = "textBoxPDF"
        Me.textBoxPDF.Size = New System.Drawing.Size(446, 20)
        Me.textBoxPDF.TabIndex = 1
        '
        'textBoxOutputLocation
        '
        Me.textBoxOutputLocation.Enabled = False
        Me.textBoxOutputLocation.Location = New System.Drawing.Point(12, 93)
        Me.textBoxOutputLocation.Name = "textBoxOutputLocation"
        Me.textBoxOutputLocation.Size = New System.Drawing.Size(446, 20)
        Me.textBoxOutputLocation.TabIndex = 4
        '
        'SaveGroupBox
        '
        Me.SaveGroupBox.Controls.Add(Me.radioButtonSinglePageJPEG)
        Me.SaveGroupBox.Controls.Add(Me.radioButtonSinglePageJBIG2)
        Me.SaveGroupBox.Controls.Add(Me.radioButtonSinglePageTIFF)
        Me.SaveGroupBox.Controls.Add(Me.radioButtonMultiPageTIFF)
        Me.SaveGroupBox.Location = New System.Drawing.Point(6, 131)
        Me.SaveGroupBox.Name = "SaveGroupBox"
        Me.SaveGroupBox.Size = New System.Drawing.Size(554, 61)
        Me.SaveGroupBox.TabIndex = 6
        Me.SaveGroupBox.TabStop = False
        Me.SaveGroupBox.Text = "Save As"
        '
        'radioButtonSinglePageJPEG
        '
        Me.radioButtonSinglePageJPEG.AutoSize = True
        Me.radioButtonSinglePageJPEG.Location = New System.Drawing.Point(139, 28)
        Me.radioButtonSinglePageJPEG.Name = "radioButtonSinglePageJPEG"
        Me.radioButtonSinglePageJPEG.Size = New System.Drawing.Size(115, 17)
        Me.radioButtonSinglePageJPEG.TabIndex = 5
        Me.radioButtonSinglePageJPEG.TabStop = True
        Me.radioButtonSinglePageJPEG.Text = "JPEG (single page)"
        Me.radioButtonSinglePageJPEG.UseVisualStyleBackColor = True
        '
        'radioButtonSinglePageJBIG2
        '
        Me.radioButtonSinglePageJBIG2.AutoSize = True
        Me.radioButtonSinglePageJBIG2.Location = New System.Drawing.Point(7, 28)
        Me.radioButtonSinglePageJBIG2.Name = "radioButtonSinglePageJBIG2"
        Me.radioButtonSinglePageJBIG2.Size = New System.Drawing.Size(117, 17)
        Me.radioButtonSinglePageJBIG2.TabIndex = 4
        Me.radioButtonSinglePageJBIG2.TabStop = True
        Me.radioButtonSinglePageJBIG2.Text = "JBIG2 (single page)"
        Me.radioButtonSinglePageJBIG2.UseVisualStyleBackColor = True
        '
        'radioButtonSinglePageTIFF
        '
        Me.radioButtonSinglePageTIFF.AutoSize = True
        Me.radioButtonSinglePageTIFF.Location = New System.Drawing.Point(414, 28)
        Me.radioButtonSinglePageTIFF.Name = "radioButtonSinglePageTIFF"
        Me.radioButtonSinglePageTIFF.Size = New System.Drawing.Size(130, 17)
        Me.radioButtonSinglePageTIFF.TabIndex = 3
        Me.radioButtonSinglePageTIFF.TabStop = True
        Me.radioButtonSinglePageTIFF.Text = "Single Page TIFF (G4)"
        Me.radioButtonSinglePageTIFF.UseVisualStyleBackColor = True
        '
        'radioButtonMultiPageTIFF
        '
        Me.radioButtonMultiPageTIFF.AutoSize = True
        Me.radioButtonMultiPageTIFF.Location = New System.Drawing.Point(270, 28)
        Me.radioButtonMultiPageTIFF.Name = "radioButtonMultiPageTIFF"
        Me.radioButtonMultiPageTIFF.Size = New System.Drawing.Size(123, 17)
        Me.radioButtonMultiPageTIFF.TabIndex = 2
        Me.radioButtonMultiPageTIFF.TabStop = True
        Me.radioButtonMultiPageTIFF.Text = "Multi-Page TIFF (G4)"
        Me.radioButtonMultiPageTIFF.UseVisualStyleBackColor = True
        '
        'MainMenu
        '
        Me.MainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MainMenu.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu.Name = "MainMenu"
        Me.MainMenu.Size = New System.Drawing.Size(586, 24)
        Me.MainMenu.TabIndex = 0
        Me.MainMenu.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImagXpressToolStripMenuItem, Me.PDFXpressToolStripMenuItem})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "&About"
        '
        'ImagXpressToolStripMenuItem
        '
        Me.ImagXpressToolStripMenuItem.Name = "ImagXpressToolStripMenuItem"
        Me.ImagXpressToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.ImagXpressToolStripMenuItem.Text = "Imag&Xpress"
        '
        'PDFXpressToolStripMenuItem
        '
        Me.PDFXpressToolStripMenuItem.Name = "PDFXpressToolStripMenuItem"
        Me.PDFXpressToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.PDFXpressToolStripMenuItem.Text = "&PDFXpress"
        '
        'ProgLabel
        '
        Me.ProgLabel.Location = New System.Drawing.Point(11, 310)
        Me.ProgLabel.Name = "ProgLabel"
        Me.ProgLabel.Size = New System.Drawing.Size(139, 28)
        Me.ProgLabel.TabIndex = 3
        '
        'ProgLabel2
        '
        Me.ProgLabel2.Location = New System.Drawing.Point(461, 350)
        Me.ProgLabel2.Name = "ProgLabel2"
        Me.ProgLabel2.Size = New System.Drawing.Size(113, 31)
        Me.ProgLabel2.TabIndex = 6
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(11, 350)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(444, 31)
        Me.ProgressBar.TabIndex = 5
        '
        'ConvertButton
        '
        Me.ConvertButton.Location = New System.Drawing.Point(458, 310)
        Me.ConvertButton.Name = "ConvertButton"
        Me.ConvertButton.Size = New System.Drawing.Size(116, 28)
        Me.ConvertButton.TabIndex = 4
        Me.ConvertButton.Text = "Convert File"
        Me.ConvertButton.UseVisualStyleBackColor = True
        '
        'OpenDialog
        '
        Me.OpenDialog.FileName = "OpenFileDialog1"
        '
        'DescriptionListBox
        '
        Me.DescriptionListBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescriptionListBox.FormattingEnabled = True
        Me.DescriptionListBox.Items.AddRange(New Object() {"This sample demonstates loading a PDF file into PDF Xpress and then rendering to " & _
                        "an image", "of the specified option in the SaveAs options."})
        Me.DescriptionListBox.Location = New System.Drawing.Point(8, 27)
        Me.DescriptionListBox.Name = "DescriptionListBox"
        Me.DescriptionListBox.Size = New System.Drawing.Size(566, 56)
        Me.DescriptionListBox.TabIndex = 1
        '
        'pdfXpress
        '
        Me.pdfXpress.Debug = False
        Me.pdfXpress.DebugLogFile = ""
        Me.pdfXpress.ErrorLevel = Accusoft.PdfXpressSdk.ErrorLevel.Production
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 387)
        Me.Controls.Add(Me.DescriptionListBox)
        Me.Controls.Add(Me.ConvertButton)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.ProgLabel)
        Me.Controls.Add(Me.ProgLabel2)
        Me.Controls.Add(Me.groupBoxConversionOpts)
        Me.Controls.Add(Me.MainMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PDF To Image"
        Me.groupBoxConversionOpts.ResumeLayout(False)
        Me.groupBoxConversionOpts.PerformLayout()
        Me.SaveGroupBox.ResumeLayout(False)
        Me.SaveGroupBox.PerformLayout()
        Me.MainMenu.ResumeLayout(False)
        Me.MainMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents groupBoxConversionOpts As System.Windows.Forms.GroupBox
    Friend WithEvents BrowseButton2 As System.Windows.Forms.Button
    Friend WithEvents BrowseButton As System.Windows.Forms.Button
    Friend WithEvents PDFLabel As System.Windows.Forms.Label
    Friend WithEvents SaveLabel As System.Windows.Forms.Label
    Friend WithEvents textBoxPDF As System.Windows.Forms.TextBox
    Friend WithEvents textBoxOutputLocation As System.Windows.Forms.TextBox
    Friend WithEvents SaveGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents radioButtonMultiPageTIFF As System.Windows.Forms.RadioButton
    Friend WithEvents MainMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents ProgLabel As System.Windows.Forms.Label
    Friend WithEvents ProgLabel2 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents ConvertButton As System.Windows.Forms.Button
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PDFXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents browseDialog As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DescriptionListBox As System.Windows.Forms.ListBox
    Friend WithEvents pdfXpress As Accusoft.PdfXpressSdk.PdfXpress
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents radioButtonSinglePageJPEG As System.Windows.Forms.RadioButton
    Friend WithEvents radioButtonSinglePageJBIG2 As System.Windows.Forms.RadioButton
    Friend WithEvents radioButtonSinglePageTIFF As System.Windows.Forms.RadioButton

End Class

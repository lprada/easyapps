/****************************************************************
 * Copyright 2010 - Pegasus Imaging Corporation, Tampa Florida. *
 * This sample code is provided to Pegasus licensees "as is"    *
 * with no restrictions on use or modification. No warranty for *
 * use of this sample code is provided by Pegasus.              *
 ****************************************************************/

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace ConvertToPDFA
{
    enum ConversionSource
    {
        FromTiffImage,
        FromPDFDocument
    }

    public enum Compression
    {
        JPEG,
        JBIG2,
        G4
    }

    public partial class ConvertPDFForm : Form
    {
        private Accusoft.PdfXpressSdk.PdfXpress pdfXpress = null;
        private Accusoft.ImagXpressSdk.ImageX originalImage = null;
        private Accusoft.PdfXpressSdk.Document imageOverTextPDFA = null;
        private Accusoft.ImagXpressSdk.ImagXpress imagXpress = null;
        private ConversionSource originalFormat;
        private static string fontFolder = System.IO.Path.Combine(Application.StartupPath,"..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\Font\\");
        private static string cmapFolder = System.IO.Path.Combine(Application.StartupPath,"..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\CMap\\");
        private static string iccPath = System.IO.Path.Combine(Application.StartupPath,
                                              "..\\..\\..\\..\\..\\..\\..\\..\\..\\PdfXpress\\v4.0\\Support\\SampleICCProfile\\sRGB_IEC61966-2-1_withBPC.icc");

        public ConvertPDFForm()
        {
            InitializeComponent();
            pdfXpress = new Accusoft.PdfXpressSdk.PdfXpress();
            imagXpress = new Accusoft.ImagXpressSdk.ImagXpress();
            pdfXpress.Initialize(fontFolder, cmapFolder);
            SetSaveEnabled(false);
            SetConvertEnabled(false);
            // pdfXpress.Licensing.UnlockRuntime(1,2,3,4);
            // imagXpress.Licensing.UnlockRuntime(1,2,3,4);
        }

        private void SetConvertEnabled(bool enabled)
        {
            convertToPDFAButton.Enabled = enabled;
            convertToPDFAToolStripMenuItem.Enabled = enabled;
        }

        private void SetSaveEnabled(bool enabled)
        {
            savePDFAButton.Enabled = enabled;
            savePDFAToolStripMenuItem.Enabled = enabled;
        }

        private void exitGracefully()
        {
            if (null != imageOverTextPDFA) imageOverTextPDFA.Dispose();
            pdfXpress.Documents.Clear();
            if (null != pdfXpress) pdfXpress.Dispose();
            Application.Exit();
        }

        private void loadUserSelectedDocument()
        {
            System.Windows.Forms.OpenFileDialog loadDialog = new System.Windows.Forms.OpenFileDialog();
            Accusoft.PdfXpressSdk.Document pdfDocument = null;

            loadDialog.Filter = "PDF documents (*.pdf)|*.pdf|Multi-page TIFF images (*.tif)|*.tif";
            loadDialog.FilterIndex = 1;
            loadDialog.InitialDirectory = System.IO.Path.GetFullPath(Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\");

            try
            {
                if (loadDialog.ShowDialog() == DialogResult.OK)
                {
                    if (loadDialog.FilterIndex == 1)
                    {
                        pdfDocument = new Accusoft.PdfXpressSdk.Document(pdfXpress, loadDialog.FileName);
                        loadLabel.Text =
                            System.String.Format("{0} loaded, {1} pages", loadDialog.FileName, pdfDocument.PageCount);
                        convertLabel.Text =
                            "Document not yet converted.";
                        pdfXpress.Documents.Clear();
                        pdfXpress.Documents.Add(pdfDocument);
                        SetConvertEnabled(true);
                        originalFormat = ConversionSource.FromPDFDocument;
                    }
                    else if (loadDialog.FilterIndex == 2)
                    {
                        originalImage = Accusoft.ImagXpressSdk.ImageX.FromFile(imagXpress,
                            loadDialog.FileName);
                        loadLabel.Text =
                            System.String.Format("{0} loaded, {1} pages", loadDialog.FileName, originalImage.PageCount);
                        convertLabel.Text =
                            "Document not yet converted.";
                        SetConvertEnabled(true);
                        originalFormat = ConversionSource.FromTiffImage;
                    }
                }
            }
            catch (Exception e)
            {
                loadLabel.Text = "Error loading PDF: " + e.Message;
                SetConvertEnabled(false);
                if (null != pdfDocument) pdfDocument.Dispose();
            }
        }

        private void convertUserSelectedDocument()
        {
            Compression compressionType = Compression.JPEG;
            CompressForm cf = new CompressForm();
            if (cf.ShowDialog() == DialogResult.OK)
            {
                compressionType = cf.CompressionSelected;
                if (originalFormat == ConversionSource.FromPDFDocument)
                {
                    if (pdfXpress.Documents.Count > 0)
                    {
                        Accusoft.PdfXpressSdk.Document originalDocument =
                            pdfXpress.Documents[0];
                        Accusoft.PdfXpressSdk.Document pdfaDocument =
                            new Accusoft.PdfXpressSdk.Document(pdfXpress);

                        Accusoft.PdfXpressSdk.RenderOptions renderOptions =
                            new Accusoft.PdfXpressSdk.RenderOptions();
                        renderOptions.RenderAnnotations = false;
                        renderOptions.ProduceDibSection = false;

                        if (compressionType == Compression.JBIG2)
                        {
                            renderOptions.ReduceToBitonal = true;
                            renderOptions.ResolutionX = 300;
                            renderOptions.ResolutionY = 300;
                        }
                        else if (compressionType == Compression.G4)
                        {
                            renderOptions.ReduceToBitonal = true;
                            renderOptions.ResolutionX = 200;
                            renderOptions.ResolutionY = 200;
                        }
                        else
                        {
                            renderOptions.ResolutionX = 100;
                            renderOptions.ResolutionY = 100;
                        }

                        Accusoft.PdfXpressSdk.PageOptions pageOpts =
                                new Accusoft.PdfXpressSdk.PageOptions();
                        Accusoft.PdfXpressSdk.PageInfo pageInfo = null;

                        convertLabel.Text = "Conversion in progress...";
                        // Display "Conversion in progress" immediately, in case conversion
                        // takes a long time.
                        Application.DoEvents();

                        for (int page = 0; page < originalDocument.PageCount; page++)
                        {
                            // Get bounds from the original page
                            pageInfo = originalDocument.GetInfo(page);

                            // Set bounds from the original page
                            pageOpts.MediaHeight = pageInfo.MediaHeight;
                            pageOpts.MediaWidth = pageInfo.MediaWidth;

                            // Create the new page in the image only document
                            pdfaDocument.CreatePage(page - 1, pageOpts);

                            // Render the page in the original document
                            System.IntPtr dibPtr = new IntPtr(originalDocument.RenderPageToDib(page, renderOptions));

                            Accusoft.ImagXpressSdk.ImageX pageImageX = 
                                Accusoft.ImagXpressSdk.ImageX.FromHdib(imagXpress, dibPtr);

                            byte[] buffer = CompressDocumentImageXToBuffer(pageImageX, compressionType);

                            //Set the new page to the rendered image
                            pdfaDocument.AddImage(page, 0, 0, pageOpts.MediaWidth, pageOpts.MediaHeight,
                               Accusoft.PdfXpressSdk.ImageFitSettings.None, buffer, 0);

                            if (pageImageX != null) pageImageX.Dispose();
                        }

                        Accusoft.PdfXpressSdk.ConvertOptions conversionOptions =
                            new Accusoft.PdfXpressSdk.ConvertOptions();
                        conversionOptions.PdfType = Accusoft.PdfXpressSdk.PdfType.pdfA1b;
                        conversionOptions.IccProfileFilename = iccPath;

                        try
                        {
                            pdfaDocument.ConvertToPdfA(conversionOptions);
                            convertLabel.Text = "Conversion successful.";
                            imageOverTextPDFA = pdfaDocument;

                            SetSaveEnabled(true);
                        }
                        catch (Accusoft.PdfXpressSdk.PdfXpressException e)
                        {
                            convertLabel.Text = "Conversion error: " + e.Message;
                            if (pdfaDocument != null) pdfaDocument.Dispose();
                        }
                    } // pdfXpress.Documents.Count > 0
                } // originalFormat == PDF
                else if (originalFormat == ConversionSource.FromTiffImage)
                {
                    Accusoft.PdfXpressSdk.Document pdfaDocument =
                        new Accusoft.PdfXpressSdk.Document(pdfXpress);

                    // ImagXpress uses a 1-based page index.
                    for (int page = 1; page <= originalImage.PageCount; page++)
                    {
                        Accusoft.PdfXpressSdk.PageOptions pageCreateOpts
                             = new Accusoft.PdfXpressSdk.PageOptions();
                        originalImage.Page = page;
                        originalImage.Resolution.Units = System.Drawing.GraphicsUnit.Inch;

                        pageCreateOpts.MediaHeight = 72 * (originalImage.ImageXData.Height
                                                        / originalImage.ImageXData.Resolution.Dimensions.Width);
                        pageCreateOpts.MediaWidth = 72 * (originalImage.ImageXData.Width
                                                        / originalImage.ImageXData.Resolution.Dimensions.Height);

                        // Compress the page image.
                        byte[] buffer = CompressDocumentImageXToBuffer(originalImage, compressionType);

                        // We provide create page with the index *before* the index of the future page.
                        // We offset an additional -1 for ImagXpress starting pages at 1 while PdfXpress
                        // starts at 0.
                        pdfaDocument.CreatePage(page - 2, pageCreateOpts);

                        // page-1 is the offset for ImagXpress using a 1-based index for pages 
                        // while PdfXpress uses a 0-based index.
                        pdfaDocument.AddImage(page - 1, 0, 0, pageCreateOpts.MediaWidth, pageCreateOpts.MediaHeight,
                            Accusoft.PdfXpressSdk.ImageFitSettings.None, buffer, 0);
                    }
                    Accusoft.PdfXpressSdk.ConvertOptions conversionOptions =
                        new Accusoft.PdfXpressSdk.ConvertOptions();
                    conversionOptions.PdfType = Accusoft.PdfXpressSdk.PdfType.pdfA1b;
                    conversionOptions.IccProfileFilename = iccPath;

                    try
                    {
                        pdfaDocument.ConvertToPdfA(conversionOptions);
                        convertLabel.Text = "Conversion successful.";
                        imageOverTextPDFA = pdfaDocument;

                        SetSaveEnabled(true);
                    }
                    catch (Accusoft.PdfXpressSdk.PdfXpressException e)
                    {
                        convertLabel.Text = "Conversion error: " + e.Message;
                        if (pdfaDocument != null) pdfaDocument.Dispose();
                    }
                }
            } // (cf.ShowDialog() == DialogResult.OK)
        }

        private static byte[] CompressDocumentImageXToBuffer(Accusoft.ImagXpressSdk.ImageX pageImageX,
            Compression compressionType)
        {
            using (System.IO.MemoryStream memStream = new System.IO.MemoryStream())
            {
                memStream.Seek(0, System.IO.SeekOrigin.Begin);
                Accusoft.ImagXpressSdk.SaveOptions saveOpts =
                              new Accusoft.ImagXpressSdk.SaveOptions();
                switch (compressionType)
                {
                    case Compression.G4:
                        saveOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Tiff;
                        saveOpts.Tiff.Compression = Accusoft.ImagXpressSdk.Compression.Group4;
                        pageImageX.SaveStream(memStream, saveOpts);
                        break;
                    case Compression.JBIG2:
                        saveOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jbig2;
                        saveOpts.Jbig2.LoosenessCompression = 0;
                        pageImageX.SaveStream(memStream, saveOpts);
                        break;
                    case Compression.JPEG:
                        saveOpts.Format = Accusoft.ImagXpressSdk.ImageXFormat.Jpeg;
                        pageImageX.SaveStream(memStream, saveOpts);
                        break;
                    default:
                        throw new InvalidEnumArgumentException("Invalid Compression Value");
                }
                memStream.Seek(0, System.IO.SeekOrigin.Begin);

                byte[] buffer = new byte[memStream.Length];
                memStream.ToArray().CopyTo(buffer, 0);
                memStream.Close();
                return buffer;
            }
        }

        private void saveConvertedDocument()
        {
            System.Windows.Forms.SaveFileDialog saveDialog = new System.Windows.Forms.SaveFileDialog();
            saveDialog.Filter = "PDF documents (*.pdf)|*.pdf";
            saveDialog.FilterIndex = 1;
            saveDialog.InitialDirectory = Application.StartupPath + "..\\..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Accusoft.PdfXpressSdk.SaveOptions saveOpts = new
                        Accusoft.PdfXpressSdk.SaveOptions();
                    saveOpts.Filename = saveDialog.FileName;
                    imageOverTextPDFA.Save(saveOpts);

                    string trimmedName = saveDialog.FileName.Substring(saveDialog.FileName.LastIndexOf('\\') + 1);
                    saveLabel.Text =
                        System.String.Format("{0} saved, {1} pages", trimmedName, imageOverTextPDFA.PageCount);
                }
                catch (Exception e)
                {
                    saveLabel.Text = "Error during save: " + e.Message;
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pdfXpress.AboutBox();
        }

        private void loadPDFButton_Click(object sender, EventArgs e)
        {
            loadUserSelectedDocument();
        }

        private void loadPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadUserSelectedDocument();
        }

        private void convertToPDFAButton_Click(object sender, EventArgs e)
        {
            convertUserSelectedDocument();
        }

        private void convertToPDFAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            convertUserSelectedDocument();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exitGracefully();
        }

        private void convertPdfForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            exitGracefully();
        }

        private void savePDFAButton_Click(object sender, EventArgs e)
        {
            saveConvertedDocument();
        }

        private void savePDFAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveConvertedDocument();
        }
    }
}
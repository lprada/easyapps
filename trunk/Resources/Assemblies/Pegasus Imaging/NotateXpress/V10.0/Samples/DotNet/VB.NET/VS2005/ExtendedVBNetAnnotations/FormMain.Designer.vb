﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.NotateXpress1 = New Accusoft.NotateXpressSdk.NotateXpress(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.Processor1 = New Accusoft.ImagXpressSdk.Processor(Me.components)
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem20 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem21 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem22 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem23 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem24 = New System.Windows.Forms.ToolStripMenuItem
        Me.brandToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem25 = New System.Windows.Forms.ToolStripMenuItem
        Me.displayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.lineScalingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.fontScalingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.editModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.interactiveModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.useMLEForTextEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.rotate90ClockwiseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.rotate90CounterClockwiseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.layersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.createToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.deleteCurrentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.getLayerNameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.setLayerNameToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.hideLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem26 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem27 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem28 = New System.Windows.Forms.ToolStripMenuItem
        Me.showLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem29 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem30 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem31 = New System.Windows.Forms.ToolStripMenuItem
        Me.switchToLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem32 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem33 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem34 = New System.Windows.Forms.ToolStripMenuItem
        Me.getOfLayersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.createAnnotationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem35 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem36 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem37 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem38 = New System.Windows.Forms.ToolStripMenuItem
        Me.imageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.redactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.selectedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.backgroundColorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.penColorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.penWidthToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.fontToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.allToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.backgroundColorToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.penColorToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.penWidthToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.fontToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.contextMenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.enabledToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.addCustomTextToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolbarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.notateXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.imagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.smallToolbarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.largeToolbarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.buttonToolToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.enableNoteToolToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem39 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem40 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem41 = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit
        Me.NotateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal
        Me.NotateXpress1.MultiLineEdit = False
        Me.NotateXpress1.RecalibrateXdpi = -1
        Me.NotateXpress1.RecalibrateYdpi = -1
        Me.NotateXpress1.ToolTipTimeEdit = 0
        Me.NotateXpress1.ToolTipTimeInteractive = 0
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 710)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ImageXView1
        '
        Me.ImageXView1.Location = New System.Drawing.Point(57, 65)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(901, 619)
        Me.ImageXView1.TabIndex = 2
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem8, Me.displayToolStripMenuItem, Me.layersToolStripMenuItem, Me.createAnnotationsToolStripMenuItem, Me.selectedToolStripMenuItem, Me.allToolStripMenuItem, Me.contextMenuToolStripMenuItem, Me.toolbarToolStripMenuItem, Me.ToolStripMenuItem39})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem9, Me.ToolStripMenuItem13, Me.ToolStripSeparator2, Me.ToolStripMenuItem17, Me.ToolStripMenuItem21, Me.brandToolStripMenuItem, Me.ToolStripMenuItem25})
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(37, 20)
        Me.ToolStripMenuItem8.Text = "&File"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem10, Me.ToolStripMenuItem11, Me.ToolStripMenuItem12})
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(206, 22)
        Me.ToolStripMenuItem9.Text = "&Open Image"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(277, 22)
        Me.ToolStripMenuItem10.Text = "No Annotations"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(277, 22)
        Me.ToolStripMenuItem11.Text = "Embedded Wang / Kodak Annotations"
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(277, 22)
        Me.ToolStripMenuItem12.Text = "Embedded NotateXpress Annotations"
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem14, Me.ToolStripMenuItem15, Me.ToolStripMenuItem16})
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(206, 22)
        Me.ToolStripMenuItem13.Text = "&Save Image"
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        Me.ToolStripMenuItem14.Size = New System.Drawing.Size(217, 22)
        Me.ToolStripMenuItem14.Text = "No Annotations"
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        Me.ToolStripMenuItem15.Size = New System.Drawing.Size(217, 22)
        Me.ToolStripMenuItem15.Text = "Wang / Kodak Annotations"
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(217, 22)
        Me.ToolStripMenuItem16.Text = "NotateXpress Annotations"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(203, 6)
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem18, Me.ToolStripMenuItem19, Me.ToolStripMenuItem20})
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(206, 22)
        Me.ToolStripMenuItem17.Text = "Open Annotation File"
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(222, 22)
        Me.ToolStripMenuItem18.Text = "NotateXpress (NXP)"
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(222, 22)
        Me.ToolStripMenuItem19.Text = "NotateXpress XML (NXXML)"
        '
        'ToolStripMenuItem20
        '
        Me.ToolStripMenuItem20.Name = "ToolStripMenuItem20"
        Me.ToolStripMenuItem20.Size = New System.Drawing.Size(222, 22)
        Me.ToolStripMenuItem20.Text = "Adobe PDF (XFDF)"
        '
        'ToolStripMenuItem21
        '
        Me.ToolStripMenuItem21.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem22, Me.ToolStripMenuItem23, Me.ToolStripMenuItem24})
        Me.ToolStripMenuItem21.Name = "ToolStripMenuItem21"
        Me.ToolStripMenuItem21.Size = New System.Drawing.Size(206, 22)
        Me.ToolStripMenuItem21.Text = "Save Annotation File"
        '
        'ToolStripMenuItem22
        '
        Me.ToolStripMenuItem22.Name = "ToolStripMenuItem22"
        Me.ToolStripMenuItem22.Size = New System.Drawing.Size(222, 22)
        Me.ToolStripMenuItem22.Text = "NotateXpress (NXP)"
        '
        'ToolStripMenuItem23
        '
        Me.ToolStripMenuItem23.Name = "ToolStripMenuItem23"
        Me.ToolStripMenuItem23.Size = New System.Drawing.Size(222, 22)
        Me.ToolStripMenuItem23.Text = "NotateXpress XML (NXXML)"
        '
        'ToolStripMenuItem24
        '
        Me.ToolStripMenuItem24.Name = "ToolStripMenuItem24"
        Me.ToolStripMenuItem24.Size = New System.Drawing.Size(222, 22)
        Me.ToolStripMenuItem24.Text = "Adobe PDF (XFDF)"
        '
        'brandToolStripMenuItem
        '
        Me.brandToolStripMenuItem.Name = "brandToolStripMenuItem"
        Me.brandToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.brandToolStripMenuItem.Text = "&Brand (Make Permanent)"
        '
        'ToolStripMenuItem25
        '
        Me.ToolStripMenuItem25.Name = "ToolStripMenuItem25"
        Me.ToolStripMenuItem25.Size = New System.Drawing.Size(206, 22)
        Me.ToolStripMenuItem25.Text = "E&xit"
        '
        'displayToolStripMenuItem
        '
        Me.displayToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lineScalingToolStripMenuItem, Me.fontScalingToolStripMenuItem, Me.ToolStripSeparator3, Me.editModeToolStripMenuItem, Me.interactiveModeToolStripMenuItem, Me.ToolStripSeparator4, Me.useMLEForTextEntryToolStripMenuItem, Me.ToolStripSeparator5, Me.rotate90ClockwiseToolStripMenuItem, Me.rotate90CounterClockwiseToolStripMenuItem})
        Me.displayToolStripMenuItem.Name = "displayToolStripMenuItem"
        Me.displayToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.displayToolStripMenuItem.Text = "&Display"
        '
        'lineScalingToolStripMenuItem
        '
        Me.lineScalingToolStripMenuItem.Name = "lineScalingToolStripMenuItem"
        Me.lineScalingToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.lineScalingToolStripMenuItem.Text = "Line Scaling"
        '
        'fontScalingToolStripMenuItem
        '
        Me.fontScalingToolStripMenuItem.Name = "fontScalingToolStripMenuItem"
        Me.fontScalingToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.fontScalingToolStripMenuItem.Text = "Font Scaling"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(218, 6)
        '
        'editModeToolStripMenuItem
        '
        Me.editModeToolStripMenuItem.Name = "editModeToolStripMenuItem"
        Me.editModeToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.editModeToolStripMenuItem.Text = "Edit Mode"
        '
        'interactiveModeToolStripMenuItem
        '
        Me.interactiveModeToolStripMenuItem.Name = "interactiveModeToolStripMenuItem"
        Me.interactiveModeToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.interactiveModeToolStripMenuItem.Text = "Interactive Mode"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(218, 6)
        '
        'useMLEForTextEntryToolStripMenuItem
        '
        Me.useMLEForTextEntryToolStripMenuItem.Name = "useMLEForTextEntryToolStripMenuItem"
        Me.useMLEForTextEntryToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.useMLEForTextEntryToolStripMenuItem.Text = "Use MLE for Text Entry"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(218, 6)
        '
        'rotate90ClockwiseToolStripMenuItem
        '
        Me.rotate90ClockwiseToolStripMenuItem.Name = "rotate90ClockwiseToolStripMenuItem"
        Me.rotate90ClockwiseToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.rotate90ClockwiseToolStripMenuItem.Text = "Rotate 90 clockwise"
        '
        'rotate90CounterClockwiseToolStripMenuItem
        '
        Me.rotate90CounterClockwiseToolStripMenuItem.Name = "rotate90CounterClockwiseToolStripMenuItem"
        Me.rotate90CounterClockwiseToolStripMenuItem.Size = New System.Drawing.Size(221, 22)
        Me.rotate90CounterClockwiseToolStripMenuItem.Text = "Rotate 90 counter clockwise"
        '
        'layersToolStripMenuItem
        '
        Me.layersToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.createToolStripMenuItem, Me.deleteCurrentToolStripMenuItem, Me.getLayerNameToolStripMenuItem, Me.setLayerNameToolStripMenuItem, Me.hideLayerToolStripMenuItem, Me.showLayerToolStripMenuItem, Me.switchToLayerToolStripMenuItem, Me.getOfLayersToolStripMenuItem})
        Me.layersToolStripMenuItem.Name = "layersToolStripMenuItem"
        Me.layersToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.layersToolStripMenuItem.Text = "&Layers"
        '
        'createToolStripMenuItem
        '
        Me.createToolStripMenuItem.Name = "createToolStripMenuItem"
        Me.createToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.createToolStripMenuItem.Text = "Create"
        '
        'deleteCurrentToolStripMenuItem
        '
        Me.deleteCurrentToolStripMenuItem.Name = "deleteCurrentToolStripMenuItem"
        Me.deleteCurrentToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.deleteCurrentToolStripMenuItem.Text = "Delete Current"
        '
        'getLayerNameToolStripMenuItem
        '
        Me.getLayerNameToolStripMenuItem.Name = "getLayerNameToolStripMenuItem"
        Me.getLayerNameToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.getLayerNameToolStripMenuItem.Text = "Get Layer Name"
        '
        'setLayerNameToolStripMenuItem
        '
        Me.setLayerNameToolStripMenuItem.Name = "setLayerNameToolStripMenuItem"
        Me.setLayerNameToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.setLayerNameToolStripMenuItem.Text = "Set Layer Name"
        '
        'hideLayerToolStripMenuItem
        '
        Me.hideLayerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem26, Me.ToolStripMenuItem27, Me.ToolStripMenuItem28})
        Me.hideLayerToolStripMenuItem.Name = "hideLayerToolStripMenuItem"
        Me.hideLayerToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.hideLayerToolStripMenuItem.Text = "Hide Layer"
        '
        'ToolStripMenuItem26
        '
        Me.ToolStripMenuItem26.Name = "ToolStripMenuItem26"
        Me.ToolStripMenuItem26.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem26.Text = "Hide 1"
        '
        'ToolStripMenuItem27
        '
        Me.ToolStripMenuItem27.Name = "ToolStripMenuItem27"
        Me.ToolStripMenuItem27.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem27.Text = "Hide 2"
        '
        'ToolStripMenuItem28
        '
        Me.ToolStripMenuItem28.Name = "ToolStripMenuItem28"
        Me.ToolStripMenuItem28.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem28.Text = "Hide 3"
        '
        'showLayerToolStripMenuItem
        '
        Me.showLayerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem29, Me.ToolStripMenuItem30, Me.ToolStripMenuItem31})
        Me.showLayerToolStripMenuItem.Name = "showLayerToolStripMenuItem"
        Me.showLayerToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.showLayerToolStripMenuItem.Text = "Show Layer"
        '
        'ToolStripMenuItem29
        '
        Me.ToolStripMenuItem29.Name = "ToolStripMenuItem29"
        Me.ToolStripMenuItem29.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem29.Text = "Show 1"
        '
        'ToolStripMenuItem30
        '
        Me.ToolStripMenuItem30.Name = "ToolStripMenuItem30"
        Me.ToolStripMenuItem30.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem30.Text = "Show 2"
        '
        'ToolStripMenuItem31
        '
        Me.ToolStripMenuItem31.Name = "ToolStripMenuItem31"
        Me.ToolStripMenuItem31.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem31.Text = "Show 3"
        '
        'switchToLayerToolStripMenuItem
        '
        Me.switchToLayerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem32, Me.ToolStripMenuItem33, Me.ToolStripMenuItem34})
        Me.switchToLayerToolStripMenuItem.Name = "switchToLayerToolStripMenuItem"
        Me.switchToLayerToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.switchToLayerToolStripMenuItem.Text = "Switch to Layer"
        '
        'ToolStripMenuItem32
        '
        Me.ToolStripMenuItem32.Name = "ToolStripMenuItem32"
        Me.ToolStripMenuItem32.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem32.Text = "Switch 1"
        '
        'ToolStripMenuItem33
        '
        Me.ToolStripMenuItem33.Name = "ToolStripMenuItem33"
        Me.ToolStripMenuItem33.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem33.Text = "Switch 2"
        '
        'ToolStripMenuItem34
        '
        Me.ToolStripMenuItem34.Name = "ToolStripMenuItem34"
        Me.ToolStripMenuItem34.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem34.Text = "Switch 3"
        '
        'getOfLayersToolStripMenuItem
        '
        Me.getOfLayersToolStripMenuItem.Name = "getOfLayersToolStripMenuItem"
        Me.getOfLayersToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.getOfLayersToolStripMenuItem.Text = "Get # of Layers"
        '
        'createAnnotationsToolStripMenuItem
        '
        Me.createAnnotationsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem35, Me.ToolStripMenuItem36, Me.ToolStripMenuItem37, Me.ToolStripMenuItem38, Me.imageToolStripMenuItem, Me.redactionToolStripMenuItem})
        Me.createAnnotationsToolStripMenuItem.Name = "createAnnotationsToolStripMenuItem"
        Me.createAnnotationsToolStripMenuItem.Size = New System.Drawing.Size(121, 20)
        Me.createAnnotationsToolStripMenuItem.Text = "&Create Annotations"
        '
        'ToolStripMenuItem35
        '
        Me.ToolStripMenuItem35.Name = "ToolStripMenuItem35"
        Me.ToolStripMenuItem35.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem35.Text = "Rectangle"
        '
        'ToolStripMenuItem36
        '
        Me.ToolStripMenuItem36.Name = "ToolStripMenuItem36"
        Me.ToolStripMenuItem36.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem36.Text = "Stamp"
        '
        'ToolStripMenuItem37
        '
        Me.ToolStripMenuItem37.Name = "ToolStripMenuItem37"
        Me.ToolStripMenuItem37.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem37.Text = "Text"
        '
        'ToolStripMenuItem38
        '
        Me.ToolStripMenuItem38.Name = "ToolStripMenuItem38"
        Me.ToolStripMenuItem38.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripMenuItem38.Text = "Ruler"
        '
        'imageToolStripMenuItem
        '
        Me.imageToolStripMenuItem.Name = "imageToolStripMenuItem"
        Me.imageToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.imageToolStripMenuItem.Text = "Image"
        '
        'redactionToolStripMenuItem
        '
        Me.redactionToolStripMenuItem.Name = "redactionToolStripMenuItem"
        Me.redactionToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.redactionToolStripMenuItem.Text = "Redaction"
        '
        'selectedToolStripMenuItem
        '
        Me.selectedToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.backgroundColorToolStripMenuItem, Me.penColorToolStripMenuItem, Me.penWidthToolStripMenuItem, Me.fontToolStripMenuItem})
        Me.selectedToolStripMenuItem.Name = "selectedToolStripMenuItem"
        Me.selectedToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.selectedToolStripMenuItem.Text = "&Selected"
        '
        'backgroundColorToolStripMenuItem
        '
        Me.backgroundColorToolStripMenuItem.Name = "backgroundColorToolStripMenuItem"
        Me.backgroundColorToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.backgroundColorToolStripMenuItem.Text = "Background Color"
        '
        'penColorToolStripMenuItem
        '
        Me.penColorToolStripMenuItem.Name = "penColorToolStripMenuItem"
        Me.penColorToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.penColorToolStripMenuItem.Text = "Pen Color"
        '
        'penWidthToolStripMenuItem
        '
        Me.penWidthToolStripMenuItem.Name = "penWidthToolStripMenuItem"
        Me.penWidthToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.penWidthToolStripMenuItem.Text = "Pen Width"
        '
        'fontToolStripMenuItem
        '
        Me.fontToolStripMenuItem.Name = "fontToolStripMenuItem"
        Me.fontToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.fontToolStripMenuItem.Text = "Font"
        '
        'allToolStripMenuItem
        '
        Me.allToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.backgroundColorToolStripMenuItem1, Me.penColorToolStripMenuItem1, Me.penWidthToolStripMenuItem1, Me.fontToolStripMenuItem1})
        Me.allToolStripMenuItem.Name = "allToolStripMenuItem"
        Me.allToolStripMenuItem.Size = New System.Drawing.Size(77, 20)
        Me.allToolStripMenuItem.Text = "Change &All"
        '
        'backgroundColorToolStripMenuItem1
        '
        Me.backgroundColorToolStripMenuItem1.Name = "backgroundColorToolStripMenuItem1"
        Me.backgroundColorToolStripMenuItem1.Size = New System.Drawing.Size(170, 22)
        Me.backgroundColorToolStripMenuItem1.Text = "Background Color"
        '
        'penColorToolStripMenuItem1
        '
        Me.penColorToolStripMenuItem1.Name = "penColorToolStripMenuItem1"
        Me.penColorToolStripMenuItem1.Size = New System.Drawing.Size(170, 22)
        Me.penColorToolStripMenuItem1.Text = "Pen Color"
        '
        'penWidthToolStripMenuItem1
        '
        Me.penWidthToolStripMenuItem1.Name = "penWidthToolStripMenuItem1"
        Me.penWidthToolStripMenuItem1.Size = New System.Drawing.Size(170, 22)
        Me.penWidthToolStripMenuItem1.Text = "Pen Width"
        '
        'fontToolStripMenuItem1
        '
        Me.fontToolStripMenuItem1.Name = "fontToolStripMenuItem1"
        Me.fontToolStripMenuItem1.Size = New System.Drawing.Size(170, 22)
        Me.fontToolStripMenuItem1.Text = "Font"
        '
        'contextMenuToolStripMenuItem
        '
        Me.contextMenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.enabledToolStripMenuItem, Me.addCustomTextToolStripMenuItem})
        Me.contextMenuToolStripMenuItem.Name = "contextMenuToolStripMenuItem"
        Me.contextMenuToolStripMenuItem.Size = New System.Drawing.Size(94, 20)
        Me.contextMenuToolStripMenuItem.Text = "Context &Menu"
        '
        'enabledToolStripMenuItem
        '
        Me.enabledToolStripMenuItem.Name = "enabledToolStripMenuItem"
        Me.enabledToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.enabledToolStripMenuItem.Text = "Enabled"
        '
        'addCustomTextToolStripMenuItem
        '
        Me.addCustomTextToolStripMenuItem.Name = "addCustomTextToolStripMenuItem"
        Me.addCustomTextToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.addCustomTextToolStripMenuItem.Text = "Add Custom Text"
        '
        'toolbarToolStripMenuItem
        '
        Me.toolbarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.notateXpressToolStripMenuItem, Me.imagXpressToolStripMenuItem, Me.smallToolbarToolStripMenuItem, Me.largeToolbarToolStripMenuItem, Me.buttonToolToolStripMenuItem, Me.enableNoteToolToolStripMenuItem})
        Me.toolbarToolStripMenuItem.Name = "toolbarToolStripMenuItem"
        Me.toolbarToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.toolbarToolStripMenuItem.Text = "&Toolbar"
        '
        'notateXpressToolStripMenuItem
        '
        Me.notateXpressToolStripMenuItem.Name = "notateXpressToolStripMenuItem"
        Me.notateXpressToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.notateXpressToolStripMenuItem.Text = "NotateXpress"
        '
        'imagXpressToolStripMenuItem
        '
        Me.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem"
        Me.imagXpressToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.imagXpressToolStripMenuItem.Text = "ImagXpress"
        '
        'smallToolbarToolStripMenuItem
        '
        Me.smallToolbarToolStripMenuItem.Name = "smallToolbarToolStripMenuItem"
        Me.smallToolbarToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.smallToolbarToolStripMenuItem.Text = "Small Toolbar"
        '
        'largeToolbarToolStripMenuItem
        '
        Me.largeToolbarToolStripMenuItem.Name = "largeToolbarToolStripMenuItem"
        Me.largeToolbarToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.largeToolbarToolStripMenuItem.Text = "Large Toolbar"
        '
        'buttonToolToolStripMenuItem
        '
        Me.buttonToolToolStripMenuItem.Name = "buttonToolToolStripMenuItem"
        Me.buttonToolToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.buttonToolToolStripMenuItem.Text = "Show Button Tool"
        '
        'enableNoteToolToolStripMenuItem
        '
        Me.enableNoteToolToolStripMenuItem.Name = "enableNoteToolToolStripMenuItem"
        Me.enableNoteToolToolStripMenuItem.Size = New System.Drawing.Size(169, 22)
        Me.enableNoteToolToolStripMenuItem.Text = "Enable Note Tool"
        '
        'ToolStripMenuItem39
        '
        Me.ToolStripMenuItem39.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem40, Me.ToolStripMenuItem41})
        Me.ToolStripMenuItem39.Name = "ToolStripMenuItem39"
        Me.ToolStripMenuItem39.Size = New System.Drawing.Size(44, 20)
        Me.ToolStripMenuItem39.Text = "&Help"
        '
        'ToolStripMenuItem40
        '
        Me.ToolStripMenuItem40.Name = "ToolStripMenuItem40"
        Me.ToolStripMenuItem40.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem40.Text = "About NotateXpress"
        '
        'ToolStripMenuItem41
        '
        Me.ToolStripMenuItem41.Name = "ToolStripMenuItem41"
        Me.ToolStripMenuItem41.Size = New System.Drawing.Size(180, 22)
        Me.ToolStripMenuItem41.Text = "About ImagXpress"
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 732)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "FormMain"
        Me.Text = "Extended VB.Net Annotations"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents NotateXpress1 As Accusoft.NotateXpressSdk.NotateXpress
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Processor1 As Accusoft.ImagXpressSdk.Processor
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Private WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Private WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem9 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem10 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem11 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem12 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem14 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem15 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem16 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents ToolStripMenuItem17 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem18 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem19 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem20 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem21 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem22 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem23 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem24 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents brandToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem25 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents displayToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents lineScalingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents fontScalingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents editModeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents interactiveModeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents useMLEForTextEntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents rotate90ClockwiseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents rotate90CounterClockwiseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents layersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents createToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents deleteCurrentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents getLayerNameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents setLayerNameToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents hideLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem26 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem27 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem28 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents showLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem29 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem30 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem31 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents switchToLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem32 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem33 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem34 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents getOfLayersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents createAnnotationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem35 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem36 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem37 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem38 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents imageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents redactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents selectedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents backgroundColorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents penColorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents penWidthToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents fontToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents allToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents backgroundColorToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents penColorToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents penWidthToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents fontToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents contextMenuToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents enabledToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents addCustomTextToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolbarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents notateXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents imagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents smallToolbarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents largeToolbarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents buttonToolToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents enableNoteToolToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem39 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem40 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem41 As System.Windows.Forms.ToolStripMenuItem

End Class

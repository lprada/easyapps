﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ImagXpress1 = New Accusoft.ImagXpressSdk.ImagXpress(Me.components)
        Me.NotateXpress1 = New Accusoft.NotateXpressSdk.NotateXpress(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.openImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.noAnnotationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.wangKodakAnnotationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.notateXpressNXPAnnotationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.saveImageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.openAnnotationFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.notateXpressNXPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.notateXpressXMLNXXMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.adobePDFXFDFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.saveAnnotationFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem5 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripMenuItem6 = New System.Windows.Forms.ToolStripMenuItem
        Me.exitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripMenuItem
        Me.rectangleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.stampToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.textToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.rulerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutNotateXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutImagXpressToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.Processor1 = New Accusoft.ImagXpressSdk.Processor(Me.components)
        Me.ImageXView1 = New Accusoft.ImagXpressSdk.ImageXView(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit
        Me.NotateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal
        Me.NotateXpress1.MultiLineEdit = False
        Me.NotateXpress1.RecalibrateXdpi = -1
        Me.NotateXpress1.RecalibrateYdpi = -1
        Me.NotateXpress1.ToolTipTimeEdit = 0
        Me.NotateXpress1.ToolTipTimeInteractive = 0
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 710)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1008, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolStripMenuItem7, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.openImageToolStripMenuItem, Me.saveImageToolStripMenuItem, Me.toolStripSeparator1, Me.openAnnotationFileToolStripMenuItem, Me.saveAnnotationFileToolStripMenuItem, Me.exitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'openImageToolStripMenuItem
        '
        Me.openImageToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.noAnnotationsToolStripMenuItem, Me.wangKodakAnnotationsToolStripMenuItem, Me.notateXpressNXPAnnotationsToolStripMenuItem})
        Me.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem"
        Me.openImageToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.openImageToolStripMenuItem.Text = "&Open Image"
        '
        'noAnnotationsToolStripMenuItem
        '
        Me.noAnnotationsToolStripMenuItem.Name = "noAnnotationsToolStripMenuItem"
        Me.noAnnotationsToolStripMenuItem.Size = New System.Drawing.Size(277, 22)
        Me.noAnnotationsToolStripMenuItem.Text = "No Annotations"
        '
        'wangKodakAnnotationsToolStripMenuItem
        '
        Me.wangKodakAnnotationsToolStripMenuItem.Name = "wangKodakAnnotationsToolStripMenuItem"
        Me.wangKodakAnnotationsToolStripMenuItem.Size = New System.Drawing.Size(277, 22)
        Me.wangKodakAnnotationsToolStripMenuItem.Text = "Embedded Wang / Kodak Annotations"
        '
        'notateXpressNXPAnnotationsToolStripMenuItem
        '
        Me.notateXpressNXPAnnotationsToolStripMenuItem.Name = "notateXpressNXPAnnotationsToolStripMenuItem"
        Me.notateXpressNXPAnnotationsToolStripMenuItem.Size = New System.Drawing.Size(277, 22)
        Me.notateXpressNXPAnnotationsToolStripMenuItem.Text = "Embedded NotateXpress Annotations"
        '
        'saveImageToolStripMenuItem
        '
        Me.saveImageToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripMenuItem1, Me.toolStripMenuItem2, Me.toolStripMenuItem3})
        Me.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem"
        Me.saveImageToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.saveImageToolStripMenuItem.Text = "&Save Image"
        '
        'toolStripMenuItem1
        '
        Me.toolStripMenuItem1.Name = "toolStripMenuItem1"
        Me.toolStripMenuItem1.Size = New System.Drawing.Size(217, 22)
        Me.toolStripMenuItem1.Text = "No Annotations"
        '
        'toolStripMenuItem2
        '
        Me.toolStripMenuItem2.Name = "toolStripMenuItem2"
        Me.toolStripMenuItem2.Size = New System.Drawing.Size(217, 22)
        Me.toolStripMenuItem2.Text = "Wang / Kodak Annotations"
        '
        'toolStripMenuItem3
        '
        Me.toolStripMenuItem3.Name = "toolStripMenuItem3"
        Me.toolStripMenuItem3.Size = New System.Drawing.Size(217, 22)
        Me.toolStripMenuItem3.Text = "NotateXpress Annotations"
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(184, 6)
        '
        'openAnnotationFileToolStripMenuItem
        '
        Me.openAnnotationFileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.notateXpressNXPToolStripMenuItem, Me.notateXpressXMLNXXMLToolStripMenuItem, Me.adobePDFXFDFToolStripMenuItem})
        Me.openAnnotationFileToolStripMenuItem.Name = "openAnnotationFileToolStripMenuItem"
        Me.openAnnotationFileToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.openAnnotationFileToolStripMenuItem.Text = "Open Annotation File"
        '
        'notateXpressNXPToolStripMenuItem
        '
        Me.notateXpressNXPToolStripMenuItem.Name = "notateXpressNXPToolStripMenuItem"
        Me.notateXpressNXPToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.notateXpressNXPToolStripMenuItem.Text = "NotateXpress (NXP)"
        '
        'notateXpressXMLNXXMLToolStripMenuItem
        '
        Me.notateXpressXMLNXXMLToolStripMenuItem.Name = "notateXpressXMLNXXMLToolStripMenuItem"
        Me.notateXpressXMLNXXMLToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.notateXpressXMLNXXMLToolStripMenuItem.Text = "NotateXpress XML (NXXML)"
        '
        'adobePDFXFDFToolStripMenuItem
        '
        Me.adobePDFXFDFToolStripMenuItem.Name = "adobePDFXFDFToolStripMenuItem"
        Me.adobePDFXFDFToolStripMenuItem.Size = New System.Drawing.Size(222, 22)
        Me.adobePDFXFDFToolStripMenuItem.Text = "Adobe PDF (XFDF)"
        '
        'saveAnnotationFileToolStripMenuItem
        '
        Me.saveAnnotationFileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolStripMenuItem4, Me.toolStripMenuItem5, Me.toolStripMenuItem6})
        Me.saveAnnotationFileToolStripMenuItem.Name = "saveAnnotationFileToolStripMenuItem"
        Me.saveAnnotationFileToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.saveAnnotationFileToolStripMenuItem.Text = "Save Annotation File"
        '
        'toolStripMenuItem4
        '
        Me.toolStripMenuItem4.Name = "toolStripMenuItem4"
        Me.toolStripMenuItem4.Size = New System.Drawing.Size(222, 22)
        Me.toolStripMenuItem4.Text = "NotateXpress (NXP)"
        '
        'toolStripMenuItem5
        '
        Me.toolStripMenuItem5.Name = "toolStripMenuItem5"
        Me.toolStripMenuItem5.Size = New System.Drawing.Size(222, 22)
        Me.toolStripMenuItem5.Text = "NotateXpress XML (NXXML)"
        '
        'toolStripMenuItem6
        '
        Me.toolStripMenuItem6.Name = "toolStripMenuItem6"
        Me.toolStripMenuItem6.Size = New System.Drawing.Size(222, 22)
        Me.toolStripMenuItem6.Text = "Adobe PDF (XFDF)"
        '
        'exitToolStripMenuItem
        '
        Me.exitToolStripMenuItem.Name = "exitToolStripMenuItem"
        Me.exitToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.exitToolStripMenuItem.Text = "E&xit"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.rectangleToolStripMenuItem, Me.stampToolStripMenuItem, Me.textToolStripMenuItem, Me.rulerToolStripMenuItem})
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(121, 20)
        Me.ToolStripMenuItem7.Text = "Create Annotations"
        '
        'rectangleToolStripMenuItem
        '
        Me.rectangleToolStripMenuItem.Name = "rectangleToolStripMenuItem"
        Me.rectangleToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.rectangleToolStripMenuItem.Text = "Rectangle"
        '
        'stampToolStripMenuItem
        '
        Me.stampToolStripMenuItem.Name = "stampToolStripMenuItem"
        Me.stampToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.stampToolStripMenuItem.Text = "Stamp"
        '
        'textToolStripMenuItem
        '
        Me.textToolStripMenuItem.Name = "textToolStripMenuItem"
        Me.textToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.textToolStripMenuItem.Text = "Text"
        '
        'rulerToolStripMenuItem
        '
        Me.rulerToolStripMenuItem.Name = "rulerToolStripMenuItem"
        Me.rulerToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.rulerToolStripMenuItem.Text = "Ruler"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutNotateXpressToolStripMenuItem, Me.AboutImagXpressToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutNotateXpressToolStripMenuItem
        '
        Me.AboutNotateXpressToolStripMenuItem.Name = "AboutNotateXpressToolStripMenuItem"
        Me.AboutNotateXpressToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AboutNotateXpressToolStripMenuItem.Text = "About NotateXpress"
        '
        'AboutImagXpressToolStripMenuItem
        '
        Me.AboutImagXpressToolStripMenuItem.Name = "AboutImagXpressToolStripMenuItem"
        Me.AboutImagXpressToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AboutImagXpressToolStripMenuItem.Text = "About ImagXpress"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ImageXView1
        '
        Me.ImageXView1.Location = New System.Drawing.Point(57, 65)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(901, 619)
        Me.ImageXView1.TabIndex = 2
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 732)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FormMain"
        Me.Text = "Basic VB.Net Annotations"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImagXpress1 As Accusoft.ImagXpressSdk.ImagXpress
    Friend WithEvents NotateXpress1 As Accusoft.NotateXpressSdk.NotateXpress
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Processor1 As Accusoft.ImagXpressSdk.Processor
    Friend WithEvents ImageXView1 As Accusoft.ImagXpressSdk.ImageXView
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutNotateXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutImagXpressToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents openImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents noAnnotationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents wangKodakAnnotationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents notateXpressNXPAnnotationsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents saveImageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Private WithEvents openAnnotationFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents notateXpressNXPToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents notateXpressXMLNXXMLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents adobePDFXFDFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents saveAnnotationFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem5 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents toolStripMenuItem6 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents exitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents rectangleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents stampToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents textToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents rulerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class

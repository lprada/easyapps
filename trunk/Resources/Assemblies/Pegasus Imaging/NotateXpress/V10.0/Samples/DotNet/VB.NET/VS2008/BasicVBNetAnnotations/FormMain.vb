﻿'
' Copyright (C) 2009 Pegasus Imaging Corp.  
'

' Accusoft Pegasus 
Imports Accusoft.ImagXpressSdk
Imports Accusoft.NotateXpressSdk

Public Class FormMain

    Private OpenImageFilterIndex As Integer = 1

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' all initializations of Accusoft code is done here 

        ' The Viewer part of ImagXpress 
        ImageXView1.Anchor = AnchorStyles.Bottom Or AnchorStyles.Right Or AnchorStyles.Top Or AnchorStyles.Left
        ImageXView1.Anchor = AnchorStyles.Bottom Or AnchorStyles.Right Or AnchorStyles.Top Or AnchorStyles.Left
        ImageXView1.AlphaBlend = True
        ImageXView1.AutoImageDispose = True

        ' The annotations 
        NotateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "The date is: " & DateTime.Now.ToString()
        NotateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = New Font("Arial", 14, FontStyle.Italic)
        NotateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = New Font("Arial", 14, FontStyle.Regular)

        ResetSystem()
    End Sub
    Private Sub ResetSystem()
        Dim lo As Accusoft.NotateXpressSdk.LoadOptions
        lo = New Accusoft.NotateXpressSdk.LoadOptions()

        lo.UnicodeMode = True
        ' Set up NX for Unicode 
        lo.AnnType = AnnotationType.NotateXpressXml
        NotateXpress1.Layers.SetLoadOptions(lo)

        NotateXpress1.Layers.Clear()

        NotateXpress1.ClientWindow = ImageXView1.Handle
        ImageXView1.AutoResize = AutoResizeType.BestFit


        ' Path to Accusoft shared images 
        ' Load default image, from default location 

        Dim strCurrentDir As [String] = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\..\Common\Images")
        If System.IO.Directory.Exists(strCurrentDir) Then
            System.IO.Directory.SetCurrentDirectory(strCurrentDir)
        End If

        ' Make sure we do not leak images. 
        If ImageXView1.Image IsNot Nothing Then
            ImageXView1.Image.Dispose()
        End If

        If (System.IO.File.Exists("Benefits.tif")) Then
            ImageXView1.Image = ImageX.FromFile(ImagXpress1, "Benefits.tif")
        End If



        ' make sure we have a layer to draw upon. 
        If NotateXpress1.Layers.Count = 0 Then
            ' we could have a built in layer from the image. 
            If (ImageXView1.Image IsNot Nothing) Then ' make sure we loaded an image
                NotateXpress1.Layers.Add(New Layer())
            End If
        End If

        ' turn on the built in toolbar 
        NotateXpress1.ToolbarDefaults.ToolbarActivated = True

        ' make sure we have a layer to draw upon. 
        If NotateXpress1.Layers.Count > 0 Then
            NotateXpress1.Layers(0).Toolbar.Visible = True
        End If

        NotateXpress1.FontScaling = FontScaling.ResolutionY
        NotateXpress1.LineScaling = LineScaling.ResolutionY
        NotateXpress1.MultiLineEdit = True
        ' The more sophisticated editor. 
        StatusStrip1.Text = "Welcome to the VB.Net Basic Annotation Demonstration"
    End Sub
    Private Sub openImage(ByVal annType As String)
        Dim lo As Accusoft.NotateXpressSdk.LoadOptions
        lo = New Accusoft.NotateXpressSdk.LoadOptions()



        If annType = "Wang" Then
            lo.PreserveWangLayers = False
            ' not a popular option. 
            lo.UnicodeMode = True
            ' Unicode, can be changed for non-unicode wang files 
            lo.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressLoad = True

            NotateXpress1.Layers.SetLoadOptions(lo)
            NotateXpress1.FontScaling = FontScaling.Normal
            NotateXpress1.LineScaling = LineScaling.Normal
        ElseIf annType = "NXP" Then
            lo.PreserveWangLayers = False
            ' not a popular option. 
            lo.UnicodeMode = True
            ' Unicode, can be changed for non-unicode wang files 
            lo.AnnType = AnnotationType.NotateXpress
            ' internal fast format 
            NotateXpress1.ImagXpressLoad = True

            NotateXpress1.Layers.SetLoadOptions(lo)
        Else
            ' if (annType == "none") 
            lo.UnicodeMode = True
            ' Set up NX for Unicode 
            lo.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressLoad = False
            NotateXpress1.Layers.SetLoadOptions(lo)
            NotateXpress1.FontScaling = FontScaling.ResolutionY
            NotateXpress1.LineScaling = LineScaling.ResolutionY
        End If

        ' A reduced list of supported file formats 
        Dim strDefaultImageFilter As System.String = "All Files (*.*)|*.*|Windows Bitmap (*.BMP)|*.bmp" & "|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx" & "|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2" & "|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif" & "|Zsoft PaintBrush (*.PCX)|*.pcx" & "|Portable Network Graphics (*.PNG)|*.png" & "|Tagged Image Format (*.TIFF)|*.tif;*.tiff"

        OpenFileDialog1.FileName = ""
        OpenFileDialog1.Title = "Please select an image"
        OpenFileDialog1.Filter = strDefaultImageFilter
        OpenFileDialog1.Multiselect = False
        OpenFileDialog1.FilterIndex = OpenImageFilterIndex

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            ' Blank out existing annotations. 
            NotateXpress1.Layers.Clear()

            If ImageXView1.Image IsNot Nothing Then
                ImageXView1.Image.Dispose()
            End If
            ImageXView1.Image = ImageX.FromFile(ImagXpress1, OpenFileDialog1.FileName)

            ' make sure we have a layer to draw upon. 
            If NotateXpress1.Layers.Count = 0 Then
                ' we could have a built in layer from the image. 
                NotateXpress1.Layers.Add(New Layer())
            End If

            ' default to layer #1, and turn on the toolbar. 
            NotateXpress1.Layers(0).Active = True
            NotateXpress1.Layers(0).Toolbar.Visible = True


        End If
        OpenImageFilterIndex = OpenFileDialog1.FilterIndex
    End Sub


    Private Sub noAnnotationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles noAnnotationsToolStripMenuItem.Click
        openImage("none")
    End Sub

    Private Sub wangKodakAnnotationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles wangKodakAnnotationsToolStripMenuItem.Click

        openImage("Wang")
    End Sub

    Private Sub notateXpressNXPAnnotationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notateXpressNXPAnnotationsToolStripMenuItem.Click

        openImage("NXP")
    End Sub

    Private Sub exitToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles exitToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub saveImage(ByVal annType As String)
        Dim so As Accusoft.NotateXpressSdk.SaveOptions
        so = New Accusoft.NotateXpressSdk.SaveOptions()


        If annType = "Wang" Then
            so.PreserveWangLayers = False
            ' not a popular option. 
            so.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressSave = True

            NotateXpress1.Layers.SetSaveOptions(so)
        ElseIf annType = "NXP" Then
            so.PreserveWangLayers = False
            ' not a popular option. 
            so.AnnType = AnnotationType.NotateXpress
            ' internal fast format 
            NotateXpress1.ImagXpressSave = True

            NotateXpress1.Layers.SetSaveOptions(so)
        Else
            ' if (annType == "none") 
            so.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressSave = False
            NotateXpress1.Layers.SetSaveOptions(so)
        End If


        ' A reduced list of supported file formats 
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.Title = "Please save the new image"

        Dim strDefaultImageFilter As System.String = "Windows Bitmap (*.BMP)|*.bmp" & "|JPEG (*.JPG)|*.jpg" & "|Portable Network Graphics (*.PNG)|*.png" & "|Tagged Image Format G4 (*.TIFF)|*.tif;*.tiff"

        Dim soOpts As New Accusoft.ImagXpressSdk.SaveOptions()

        SaveFileDialog1.Filter = strDefaultImageFilter
        SaveFileDialog1.FilterIndex = 4
        ' default to tiff 
        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            If SaveFileDialog1.FilterIndex = 1 Then
                ' bmp 
                soOpts.Format = ImageXFormat.Bmp
                soOpts.Bmp.Compression = Compression.Rle
            ElseIf SaveFileDialog1.FilterIndex = 2 Then
                ' jpg 
                soOpts.Format = ImageXFormat.Jpeg
            ElseIf SaveFileDialog1.FilterIndex = 3 Then
                ' png 
                soOpts.Format = ImageXFormat.Png
            ElseIf SaveFileDialog1.FilterIndex = 4 Then
                ' tiff G4 
                soOpts.Format = ImageXFormat.Tiff
                soOpts.Tiff.Compression = Compression.Group4
                soOpts.Tiff.MultiPage = False
            End If

            Processor1 = New Processor(ImagXpress1, ImageXView1.Image)
            Processor1.Image.Save(SaveFileDialog1.FileName, soOpts)

        End If
    End Sub

    Private Sub toolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles toolStripMenuItem1.Click
        saveImage("none")
    End Sub

    Private Sub toolStripMenuItem2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles toolStripMenuItem2.Click

        saveImage("Wang")
    End Sub

    Private Sub toolStripMenuItem3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles toolStripMenuItem3.Click
        saveImage("NXP")
    End Sub

    Private Sub notateXpress1_Stamping(ByVal sender As Object, ByVal e As EventArgs) Handles NotateXpress1.Stamping
        ' show the stamp tool being updated to the second 
        NotateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = ("Have a nice day " & Environment.NewLine) + DateTime.Now.ToString()
    End Sub

    Private Sub notateXpress1_AnnotationAdded(ByVal sender As Object, ByVal e As AnnotationAddedEventArgs) Handles NotateXpress1.AnnotationAdded
        ' reset the toolbar to pointer after adding an annotation. 
        NotateXpress1.Layers(0).Toolbar.Selected = AnnotationTool.PointerTool
    End Sub

    ' open an external annotation file 
    Private Sub openAnnotations(ByVal annType As String)
        Dim lo As Accusoft.NotateXpressSdk.LoadOptions
        lo = New Accusoft.NotateXpressSdk.LoadOptions()
        Dim strDefaultImageFilter As System.String

        lo.PreserveWangLayers = False
        ' not a popular option. 
        lo.UnicodeMode = True
        If annType = "XFDF" Then
            strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf"
            lo.AnnType = AnnotationType.Xfdf
            ' XFDF is used by PDF 
            OpenFileDialog1.FilterIndex = 2
        ElseIf annType = "NXP" Then
            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp"
            lo.AnnType = AnnotationType.NotateXpress
            ' internal fast format 

            OpenFileDialog1.FilterIndex = 2
        Else
            ' if (annType == "NXXML") 
            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml"
            lo.AnnType = AnnotationType.NotateXpressXml
            ' Standard XML 
            OpenFileDialog1.FilterIndex = 2
        End If

        OpenFileDialog1.FileName = ""
        OpenFileDialog1.Title = "Please select an annotation file"
        OpenFileDialog1.Filter = strDefaultImageFilter
        OpenFileDialog1.Multiselect = False

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            ' Blank out existing annotations. 
            NotateXpress1.Layers.Clear()

            NotateXpress1.Layers.FromFile(OpenFileDialog1.FileName, lo)

            ' make sure we have a layer to draw upon. 
            If NotateXpress1.Layers.Count = 0 Then
                ' we could have a built in layer from the image. 
                NotateXpress1.Layers.Add(New Layer())
            End If

            ' default to layer #1, and turn on the toolbar. 
            NotateXpress1.Layers(0).Active = True
            NotateXpress1.Layers(0).Toolbar.Visible = True

        End If
    End Sub

    Private Sub notateXpressNXPToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notateXpressNXPToolStripMenuItem.Click
        openAnnotations("NXP")
    End Sub

    Private Sub notateXpressXMLNXXMLToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notateXpressXMLNXXMLToolStripMenuItem.Click

        openAnnotations("NXXML")
    End Sub

    Private Sub adobePDFXFDFToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles adobePDFXFDFToolStripMenuItem.Click
        openAnnotations("XFDF")
    End Sub




    ' save to an external annotation file 
    Private Sub saveAnnotations(ByVal annType As String)
        Dim so As Accusoft.NotateXpressSdk.SaveOptions
        so = New Accusoft.NotateXpressSdk.SaveOptions()
        Dim strDefaultImageFilter As System.String



        so.PreserveWangLayers = False
        ' not a popular option. 
        If annType = "XFDF" Then
            so.AnnType = AnnotationType.Xfdf
            ' XFDF is used by PDF 
            strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf"
        ElseIf annType = "NXP" Then
            so.AnnType = AnnotationType.NotateXpress
            ' internal fast format 

            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp"
        Else
            ' if (annType == "NXXML") 
            so.AnnType = AnnotationType.NotateXpressXml
            ' Standard XML 

            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml"
        End If

        SaveFileDialog1.FilterIndex = 2
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.Title = "Please save the annotation file"
        SaveFileDialog1.Filter = strDefaultImageFilter

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            NotateXpress1.Layers.SaveToFile(SaveFileDialog1.FileName, so)
        End If
    End Sub


    Private Sub toolStripMenuItem4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles toolStripMenuItem4.Click
        saveAnnotations("NXP")
    End Sub

    Private Sub toolStripMenuItem5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles toolStripMenuItem5.Click
        saveAnnotations("NXXML")
    End Sub

    Private Sub toolStripMenuItem6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles toolStripMenuItem6.Click
        saveAnnotations("XFDF")
    End Sub


    ' Create annotations 
    Private Sub rectangleToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rectangleToolStripMenuItem.Click
        Dim myRect As RectangleTool

        myRect = New RectangleTool()
        myRect.BoundingRectangle = New Rectangle(100, 30, 500, 245)
        myRect.BackStyle = BackStyle.Translucent
        myRect.FillColor = Color.Yellow
        myRect.ToolTipText = "Thanks for hovering"

        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myRect)
    End Sub

    Private Sub stampToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles stampToolStripMenuItem.Click
        Dim myStamp As StampTool

        myStamp = New StampTool()
        ' A starting point, autosize will adjust to fit 
        myStamp.BoundingRectangle = New Rectangle(700, 30, 10, 10)
        myStamp.BackStyle = BackStyle.Translucent
        myStamp.BackColor = Color.Yellow
        myStamp.PenWidth = 3
        myStamp.PenColor = Color.Black
        myStamp.Text = ("Complete" & Environment.NewLine) + DateTime.Now.ToString() & " "
        myStamp.TextFont = New Font("Segoe UI", 16, FontStyle.Bold)
        myStamp.TextColor = Color.DarkBlue
        myStamp.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents


        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myStamp)
    End Sub


    Private Sub textToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles textToolStripMenuItem.Click
        Dim myText As TextTool

        myText = New TextTool()
        ' A starting point, autosize will adjust to fit 
        myText.BoundingRectangle = New Rectangle(450, 1100, 10, 10)
        myText.BackStyle = BackStyle.Opaque
        myText.BackColor = Color.White
        myText.TextColor = Color.Blue
        myText.TextFont = New Font("Arial Unicode MS", 16, FontStyle.Regular)
        myText.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents
        myText.TextJustification = TextJustification.Center
        myText.PenWidth = 2
        myText.PenColor = Color.DarkBlue

        myText.Text = (("The current date and time is: " & Environment.NewLine) + DateTime.Now.ToString() + Environment.NewLine & "Unicode is fully supported: ") + Environment.NewLine & "שָׁלוֹם"


        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myText)
    End Sub


    Private Sub rulerToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rulerToolStripMenuItem.Click
        Dim myRuler As RulerTool

        myRuler = New RulerTool()
        myRuler.BoundingRectangle = New Rectangle(200, 550, 900, 0)
        myRuler.ShowAbbreviation = False
        myRuler.MeasurementUnit = MeasurementUnit.Millimeters
        myRuler.PenWidth = 3


        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myRuler)
    End Sub

    Private Sub notateXpress1_ToolbarSelect(ByVal sender As Object, ByVal e As Accusoft.NotateXpressSdk.ToolbarEventArgs) Handles NotateXpress1.ToolbarSelect
        If e.Tool = AnnotationTool.ImageTool Then
            MessageBox.Show("To learn how to set up the image tool, please see the help file or the full sample")
        End If
    End Sub

    Private Sub aboutNotateXpressToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AboutNotateXpressToolStripMenuItem.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub aboutImagXpressToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AboutImagXpressToolStripMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

End Class

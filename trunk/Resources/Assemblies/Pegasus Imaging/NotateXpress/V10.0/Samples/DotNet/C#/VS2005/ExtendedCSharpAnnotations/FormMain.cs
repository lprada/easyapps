﻿//
// Copyright (C) 2009 Pegasus Imaging Corp.  
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
// for a simple input box.
using Microsoft.VisualBasic;

// Accusoft Pegasus 
using Accusoft.ImagXpressSdk;
using Accusoft.NotateXpressSdk;

namespace ExtendedCSharpAnnotations
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private int CustomMenuCounter = 20000;
        private int OpenImageFilterIndex = 1;

        private void FormMain_Load(object sender, EventArgs e)
        {
            ImageX imageToolImage;
            // all initializations of Accusoft code is done here

            // The Viewer part of ImagXpress
            imageXView1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
            imageXView1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
            imageXView1.AlphaBlend = true;
            imageXView1.AutoImageDispose = true;

            // Path to Accusoft shared images
            // Load default image, from default location
            String strCurrentDir = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images");
            if (System.IO.Directory.Exists(strCurrentDir))
                System.IO.Directory.SetCurrentDirectory(strCurrentDir);

            // set up toolbar defaults
            // These are set globally for all toolbars, they can also be set on a layer basis.
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "The date is: " + DateTime.Now.ToString();
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = new Font("Arial", 14, FontStyle.Italic);
            notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = new Font("Arial", 14, FontStyle.Regular);

            notateXpress1.ToolbarDefaults.ImageToolbarDefaults.BackStyle = BackStyle.Transparent;
            notateXpress1.ToolbarDefaults.ImageToolbarDefaults.TransparentColor = Color.White;
            notateXpress1.ToolbarDefaults.ImageToolbarDefaults.PenWidth = 0;
            notateXpress1.ToolbarDefaults.ImageToolbarDefaults.ToolTipText = "Image Annotation";
            notateXpress1.ToolbarDefaults.ImageToolbarDefaults.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;

            if (System.IO.File.Exists(@"Signature48.png"))
            {
                imageToolImage = ImageX.FromFile(imagXpress1, @"Signature48.png");
                IntPtr dibHandle = imageToolImage.ToHdib(true);
                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle;
            
                //resource cleanup
                if (imageToolImage != null)
                {
                    imageToolImage.Dispose();
                    imageToolImage = null;
                }
            }


 


            ResetSystem();

        }

        private void ResetSystem()
        {

            Accusoft.NotateXpressSdk.LoadOptions lo;
            lo = new Accusoft.NotateXpressSdk.LoadOptions();

            lo.UnicodeMode = true;  // Set up NX for Unicode
            lo.AnnType = AnnotationType.NotateXpressXml;
            notateXpress1.Layers.SetLoadOptions(lo);

            notateXpress1.Layers.Clear();

            notateXpress1.ClientWindow = imageXView1.Handle;
            imageXView1.AutoResize = AutoResizeType.BestFit;



            // Make sure we do not leak images.
            if (imageXView1.Image != null)
                imageXView1.Image.Dispose();

            if (System.IO.File.Exists(@"Benefits.tif"))
                imageXView1.Image = ImageX.FromFile(imagXpress1, @"Benefits.tif");


            // make sure we have a layer to draw upon.
            if (notateXpress1.Layers.Count == 0)  // we could have a built in layer from the image.
                if (imageXView1.Image != null) // make sure we loaded an image
                   notateXpress1.Layers.Add(new Layer());

            // turn on the built in toolbar
            notateXpress1.ToolbarDefaults.ToolbarActivated = true;
            if (notateXpress1.Layers.Count > 0)  // make sure we have a layer on an image
               notateXpress1.Layers[0].Toolbar.Visible = true;

            // pre start the ImagXpress toolbar
            imageXView1.Toolbar.Activated = true;
            imageXView1.Toolbar.Visible = false;

            notateXpress1.FontScaling = FontScaling.ResolutionY;
            notateXpress1.LineScaling = LineScaling.ResolutionY;
            notateXpress1.MultiLineEdit = true; // The more sophisticated editor.

            toolStripStatusLabel1.Text = "Welcome to the Extended C# Annotation Demonstration";

            // set menu check boxes
            if (notateXpress1.LineScaling == LineScaling.ResolutionY)
                lineScalingToolStripMenuItem.Checked = true;
            else
                lineScalingToolStripMenuItem.Checked = false;

            if (notateXpress1.FontScaling == FontScaling.ResolutionY)
                fontScalingToolStripMenuItem.Checked = true;
            else
                fontScalingToolStripMenuItem.Checked = false;


            useMLEForTextEntryToolStripMenuItem.Checked = notateXpress1.MultiLineEdit;
            enabledToolStripMenuItem.Checked = notateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool);
            imagXpressToolStripMenuItem.Checked = imageXView1.Toolbar.Visible;
            if (notateXpress1.Layers.Count > 0)  // make sure we have a layer on an image
            {
                notateXpressToolStripMenuItem.Checked = notateXpress1.Layers[0].Toolbar.Visible;
                buttonToolToolStripMenuItem.Checked = notateXpress1.Layers[0].Toolbar.GetToolVisible(AnnotationTool.ButtonTool);
                enableNoteToolToolStripMenuItem.Checked = notateXpress1.Layers[0].Toolbar.GetToolEnabled(AnnotationTool.NoteTool);
            }

        }


        private void openImage(string annType)
        {
            Accusoft.NotateXpressSdk.LoadOptions lo;
            lo = new Accusoft.NotateXpressSdk.LoadOptions();



            if (annType == "Wang")
            {
                lo.PreserveWangLayers = false;  // not a popular option.
                lo.UnicodeMode = true;  // Unicode, can be changed for non-unicode wang files
                lo.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak

                notateXpress1.ImagXpressLoad = true;
                notateXpress1.Layers.SetLoadOptions(lo);
                notateXpress1.FontScaling = FontScaling.Normal;
                notateXpress1.LineScaling = LineScaling.Normal;

            }
            else if (annType == "NXP")
            {
                lo.PreserveWangLayers = false;  // not a popular option.
                lo.UnicodeMode = true;  // Unicode, can be changed for non-unicode wang files
                lo.AnnType = AnnotationType.NotateXpress; // internal fast format

                notateXpress1.ImagXpressLoad = true;
                notateXpress1.Layers.SetLoadOptions(lo);

            }
            else // if (annType == "none")
            {
                lo.UnicodeMode = true;  // Set up NX for Unicode
                lo.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak
                notateXpress1.ImagXpressLoad = false;
                notateXpress1.Layers.SetLoadOptions(lo);
                notateXpress1.FontScaling = FontScaling.ResolutionY;
                notateXpress1.LineScaling = LineScaling.ResolutionY;
            }

            // A reduced list of supported file formats
            System.String strDefaultImageFilter = "All Files (*.*)|*.*|Windows Bitmap (*.BMP)|*.bmp" +
                                                  "|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx" +
                                                  "|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2" +
                                                  "|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif" +
                                                  "|Zsoft PaintBrush (*.PCX)|*.pcx" +
                                                  "|Portable Network Graphics (*.PNG)|*.png" +
                                                  "|Tagged Image Format (*.TIFF)|*.tif;*.tiff";

            openFileDialog1.FileName = "";
            openFileDialog1.Title = "Please select an image";
            openFileDialog1.Filter = strDefaultImageFilter;
            openFileDialog1.Multiselect = false;
            openFileDialog1.FilterIndex = OpenImageFilterIndex;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Blank out existing annotations.
                notateXpress1.Layers.Clear();

                if (imageXView1.Image != null)
                    imageXView1.Image.Dispose();
                imageXView1.Image = ImageX.FromFile(imagXpress1, openFileDialog1.FileName);

                // make sure we have a layer to draw upon.
                if (notateXpress1.Layers.Count == 0)  // we could have a built in layer from the image.
                    notateXpress1.Layers.Add(new Layer());

                // default to layer #1, and turn on the toolbar.
                notateXpress1.Layers[0].Active = true;
                notateXpress1.Layers[0].Toolbar.Visible = true;
            }

            OpenImageFilterIndex = openFileDialog1.FilterIndex;

        }


        private void noAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openImage("none");
        }

        private void wangKodakAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openImage("Wang");

        }

        private void notateXpressNXPAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openImage("NXP");

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveImage(string annType)
        {
            Accusoft.NotateXpressSdk.SaveOptions so;
            so = new Accusoft.NotateXpressSdk.SaveOptions();


            if (annType == "Wang")
            {
                so.PreserveWangLayers = false;  // not a popular option.
                so.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak

                notateXpress1.ImagXpressSave = true;
                notateXpress1.Layers.SetSaveOptions(so);

            }
            else if (annType == "NXP")
            {
                so.PreserveWangLayers = false;  // not a popular option.
                so.AnnType = AnnotationType.NotateXpress; // internal fast format

                notateXpress1.ImagXpressSave = true;
                notateXpress1.Layers.SetSaveOptions(so);

            }
            else // if (annType == "none")
            {
                so.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak
                notateXpress1.ImagXpressSave = false;
                notateXpress1.Layers.SetSaveOptions(so);
            }


            // A reduced list of supported file formats
            saveFileDialog1.FileName = "";
            saveFileDialog1.Title = "Please save the new image";

            System.String strDefaultImageFilter = "Windows Bitmap (*.BMP)|*.bmp" +
                                                  "|JPEG (*.JPG)|*.jpg" +
                                                  "|Portable Network Graphics (*.PNG)|*.png" +
                                                  "|Tagged Image Format G4 (*.TIFF)|*.tif;*.tiff";

            Accusoft.ImagXpressSdk.SaveOptions soOpts = new Accusoft.ImagXpressSdk.SaveOptions();

            saveFileDialog1.Filter = strDefaultImageFilter;
            saveFileDialog1.FilterIndex = 4;  // default to tiff

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FilterIndex == 1) // bmp
                {
                    soOpts.Format = ImageXFormat.Bmp;
                    soOpts.Bmp.Compression = Compression.Rle;
                }
                else if (saveFileDialog1.FilterIndex == 2) // jpg
                {
                    soOpts.Format = ImageXFormat.Jpeg;
                }
                else if (saveFileDialog1.FilterIndex == 3) // png
                {
                    soOpts.Format = ImageXFormat.Png;
                }
                else if (saveFileDialog1.FilterIndex == 4) // tiff G4
                {
                    soOpts.Format = ImageXFormat.Tiff;
                    soOpts.Tiff.Compression = Compression.Group4;
                    soOpts.Tiff.MultiPage = false;
                }

                processor1 = new Processor(imagXpress1, imageXView1.Image);
                processor1.Image.Save(saveFileDialog1.FileName, soOpts);
            }

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            saveImage("none");
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            saveImage("Wang");

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            saveImage("NXP");
        }


        // Note: Brand is memory intensive, it will need make copies of the image.
        private void brandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //brand annotations on current image at 24-bit
                notateXpress1.Layers.Brand(24);

                //delete annotations because now they've been branded in and we don't
                //need a duplicate of selectable annotations
                notateXpress1.Layers.Clear();

                // make sure we have a layer to draw upon.
                notateXpress1.Layers.Add(new Layer());

                // turn on the built in toolbar
                notateXpress1.ToolbarDefaults.ToolbarActivated = true;
                notateXpress1.Layers[0].Toolbar.Visible = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured in the Demo. " + ex.Message);
            }
        }

        private void notateXpress1_Stamping(object sender, EventArgs e)
        {
            // show the stamp tool being updated to the second
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "Have a nice day " + Environment.NewLine + DateTime.Now.ToString();
        }

        private void notateXpress1_AnnotationAdded(object sender, AnnotationAddedEventArgs e)
        {
            // reset the toolbar to pointer after adding an annotation.
            e.Layer.Toolbar.Selected = AnnotationTool.PointerTool;
        }

        // open an external annotation file
        private void openAnnotations(string annType)
        {
            Accusoft.NotateXpressSdk.LoadOptions lo;
            lo = new Accusoft.NotateXpressSdk.LoadOptions();
            System.String strDefaultImageFilter;

            lo.PreserveWangLayers = false;  // not a popular option.
            lo.UnicodeMode = true;
            if (annType == "XFDF")
            {
                strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf";
                lo.AnnType = AnnotationType.Xfdf; // XFDF is used by PDF
                openFileDialog1.FilterIndex = 2;
            }
            else if (annType == "NXP")
            {
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp";
                lo.AnnType = AnnotationType.NotateXpress; // internal fast format
                openFileDialog1.FilterIndex = 2;

            }
            else // if (annType == "NXXML")
            {
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml";
                lo.AnnType = AnnotationType.NotateXpressXml; // Standard XML
                openFileDialog1.FilterIndex = 2;
            }

            openFileDialog1.FileName = "";
            openFileDialog1.Title = "Please select an annotation file";
            openFileDialog1.Filter = strDefaultImageFilter;
            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Blank out existing annotations.
                notateXpress1.Layers.Clear();

                notateXpress1.Layers.FromFile(openFileDialog1.FileName, lo);

                // make sure we have a layer to draw upon.
                if (notateXpress1.Layers.Count == 0)  // we could have a built in layer from the image.
                    notateXpress1.Layers.Add(new Layer());

                // default to layer #1, and turn on the toolbar.
                notateXpress1.Layers[0].Active = true;
                notateXpress1.Layers[0].Toolbar.Visible = true;
            }

        }

        private void notateXpressNXPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openAnnotations("NXP");
        }

        private void notateXpressXMLNXXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openAnnotations("NXXML");

        }

        private void adobePDFXFDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openAnnotations("XFDF");
        }




        // save to an external annotation file
        private void saveAnnotations(string annType)
        {
            Accusoft.NotateXpressSdk.SaveOptions so;
            so = new Accusoft.NotateXpressSdk.SaveOptions();
            System.String strDefaultImageFilter;



            so.PreserveWangLayers = false;  // not a popular option.

            if (annType == "XFDF")
            {
                so.AnnType = AnnotationType.Xfdf; // XFDF is used by PDF
                strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf";
            }
            else if (annType == "NXP")
            {
                so.AnnType = AnnotationType.NotateXpress; // internal fast format
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp";

            }
            else // if (annType == "NXXML")
            {
                so.AnnType = AnnotationType.NotateXpressXml; // Standard XML
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml";

            }

            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.FileName = "";
            saveFileDialog1.Title = "Please save the annotation file";
            saveFileDialog1.Filter = strDefaultImageFilter;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                notateXpress1.Layers.SaveToFile(saveFileDialog1.FileName, so);
            }
        }


        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            saveAnnotations("NXP");
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            saveAnnotations("NXXML");
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            saveAnnotations("XFDF");
        }


        // Create annotations
        private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RectangleTool myRect;
            Layer layer;

            layer = notateXpress1.Layers.Selected();

            myRect = new RectangleTool();
            myRect.BoundingRectangle = new Rectangle(100, 30, 500, 245);
            myRect.BackStyle = BackStyle.Translucent;
            myRect.FillColor = Color.Yellow;
            myRect.ToolTipText = "Thanks for hovering";

            layer.Elements.Add(myRect);
        }

        private void stampToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StampTool myStamp;
            Layer layer;

            layer = notateXpress1.Layers.Selected();

            myStamp = new StampTool();
            // A starting point, autosize will adjust to fit
            myStamp.BoundingRectangle = new Rectangle(700, 30, 10, 10);
            myStamp.BackStyle = BackStyle.Translucent;
            myStamp.BackColor = Color.Yellow;
            myStamp.PenWidth = 3;
            myStamp.PenColor = Color.Black;
            myStamp.Text = "Complete" + Environment.NewLine + DateTime.Now.ToString() + " ";
            myStamp.TextFont = new Font("Segoe UI", 16, FontStyle.Bold);
            myStamp.TextColor = Color.DarkBlue;
            myStamp.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;


            layer.Elements.Add(myStamp);

        }


        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextTool myText;
            Layer layer;

            layer = notateXpress1.Layers.Selected();

            myText = new TextTool();
            // A starting point, autosize will adjust to fit
            myText.BoundingRectangle = new Rectangle(450, 1100, 10, 10);
            myText.BackStyle = BackStyle.Opaque;
            myText.BackColor = Color.White;
            myText.TextColor = Color.Blue;
            myText.TextFont = new Font("Arial Unicode MS", 16, FontStyle.Regular);
            myText.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;
            myText.TextJustification = TextJustification.Center;
            myText.PenWidth = 2;
            myText.PenColor = Color.DarkBlue;

            myText.Text = "The current date and time is: " + Environment.NewLine + DateTime.Now.ToString() +
                Environment.NewLine + "Unicode is fully supported: " + Environment.NewLine + "שָׁלוֹם";

            layer.Elements.Add(myText);
        }


        private void rulerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RulerTool myRuler;
            Layer layer;

            layer = notateXpress1.Layers.Selected();

            myRuler = new RulerTool();
            myRuler.BoundingRectangle = new Rectangle(200, 550, 900, 0);
            myRuler.ShowAbbreviation = false;
            myRuler.MeasurementUnit = MeasurementUnit.Millimeters;
            myRuler.PenWidth = 3;


            layer.Elements.Add(myRuler);
        }

        private void notateXpress1_ToolbarSelect(object sender, Accusoft.NotateXpressSdk.ToolbarEventArgs e)
        {
            //if (e.Tool == AnnotationTool.ImageTool)
            //{
            //    MessageBox.Show("To learn how to set up the image tool, please see the help file or the full sample");
            //}
        }

        private void aboutNotateXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.AboutBox();
        }

        private void aboutImagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImageTool myImage;
            ImageX imageToolImage;
            Layer layer;

            if (System.IO.File.Exists(@"Signature48.png"))
            {
                layer = notateXpress1.Layers.Selected();

                myImage = new ImageTool();
                myImage.BoundingRectangle = new Rectangle(350, 1760, 10, 10);
                myImage.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;
                myImage.TransparentColor = Color.White;
                myImage.BackStyle = BackStyle.Transparent;

                imageToolImage = ImageX.FromFile(imagXpress1, @"Signature48.png");
                IntPtr dibHandle = imageToolImage.ToHdib(true);

                notateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle;
                myImage.DibHandle = dibHandle;

                //resource cleanup
                if (imageToolImage != null)
                {
                    imageToolImage.Dispose();
                    imageToolImage = null;
                }


                layer.Elements.Add(myImage);
            }
        }

        private void redactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RectangleTool myRedaction;
            Layer layer;

            layer = notateXpress1.Layers.Selected();

            myRedaction = new RectangleTool();
            myRedaction.BoundingRectangle = new Rectangle(1200, 625, 400, 100);
            myRedaction.BackStyle = BackStyle.Opaque;
            myRedaction.FillColor = Color.Black;
            myRedaction.PenWidth = 0;
            myRedaction.ToolTipText = "Redacted";
            myRedaction.Moveable = false;
            myRedaction.Sizeable = false;
            myRedaction.Locked = true;

            layer.Elements.Add(myRedaction);

        }



        #region Layers
        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create Layer
            Layer layer;

            layer = new Layer();
            layer.Toolbar.Visible = true;
            notateXpress1.Layers.Add(layer);
        }


        private void deleteCurrentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Delete the current layer
            Layer layer;

            layer = notateXpress1.Layers.Selected();
            notateXpress1.Layers.Remove(layer);

            // make the 1st layer, the selected one
            if (notateXpress1.Layers.Count > 0)
                notateXpress1.Layers[0].Select();
        }

        private void getLayerNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // show the current layer name
            Layer layer;

            layer = notateXpress1.Layers.Selected();
            MessageBox.Show("Layer Name is : " + layer.Name);

        }

        private void setLayerNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // enter a layer name, displaying the old name
            Layer layer;
            String layerName;

            try
            {
                layer = notateXpress1.Layers.Selected();
                layerName = layer.Name;
                layerName = Microsoft.VisualBasic.Interaction.InputBox("Specify Name of Layer", "NotateXpress Sample", layerName, 100, 100);
                layer.Name = layerName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            // Hide layer #1
            try
            {
                notateXpress1.Layers[0].Visible = false;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            // Hide layer #2
            try
            {
                notateXpress1.Layers[1].Visible = false;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }

        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            // Hide layer #3
            try
            {
                notateXpress1.Layers[2].Visible = false;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }

        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            // Show layer #1
            try
            {
                notateXpress1.Layers[0].Visible = true;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }

        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            // Show layer #2
            try
            {
                notateXpress1.Layers[1].Visible = true;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }


        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            // Show layer #3
            try
            {
                notateXpress1.Layers[2].Visible = true;
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }


        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            // change to layer #1
            try
            {
                notateXpress1.Layers[0].Select();
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }
        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            // change to layer #2
            try
            {
                notateXpress1.Layers[1].Select();
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }

        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            // change to layer #3
            try
            {
                notateXpress1.Layers[2].Select();
            }
            catch (System.ArgumentOutOfRangeException)
            {
                MessageBox.Show("Error: Layer does not exist.");
            }


        }

        private void getOfLayersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Layer Count: " + notateXpress1.Layers.Count.ToString());
        }

        #endregion  // Layers

        #region Display
        private void lineScalingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (notateXpress1.LineScaling == LineScaling.Normal)
            {
                notateXpress1.LineScaling = LineScaling.ResolutionY;
                lineScalingToolStripMenuItem.Checked = true;
            }
            else
            {
                notateXpress1.LineScaling = LineScaling.Normal;
                lineScalingToolStripMenuItem.Checked = false;
            }
        }


        private void fontScalingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (notateXpress1.FontScaling == FontScaling.Normal)
            {
                notateXpress1.FontScaling = FontScaling.ResolutionY;
                fontScalingToolStripMenuItem.Checked = true;
            }
            else
            {
                notateXpress1.FontScaling = FontScaling.Normal;
                fontScalingToolStripMenuItem.Checked = false;
            }

        }
        private void editModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Edit mode allows users to change annotations using the built in controls.
            notateXpress1.InteractMode = AnnotationMode.Edit;
        }


        private void interactiveModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // InteractMode mode is used in a viewing situation.  No automatic editing is allowed.
            notateXpress1.InteractMode = AnnotationMode.Interactive;
        }

        private void useMLEForTextEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {

            notateXpress1.MultiLineEdit = !notateXpress1.MultiLineEdit;
            useMLEForTextEntryToolStripMenuItem.Checked = notateXpress1.MultiLineEdit;
        }


        private void rotate90ClockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //resource cleanup
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }
            }

            processor1.Image = imageXView1.Image;
            processor1.Rotate(270);
            processor1.Image = null;

        }

        private void rotate90CounterClockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //resource cleanup
            if (processor1 != null)
            {
                if (processor1.Image != null)
                {
                    processor1.Image.Dispose();
                    processor1.Image = null;
                }
            }

            processor1.Image = imageXView1.Image;
            processor1.Rotate(90);
            processor1.Image = null;


        }
        #endregion  // Display

        #region SelectedChange

        private void selectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // edit only the single selected annotation on this layer
        }

        private void selectedToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            // start by turning off all choices
            foreach (ToolStripMenuItem menuItem in selectedToolStripMenuItem.DropDownItems)
            {
                menuItem.Enabled = false;
            }

        }

        private void selectedToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            Layer currentLayer;
            Element firstElement;
            System.Reflection.PropertyInfo propertyExists;


            // Select what menu items can be enabled for the currently selected item

            // Note:
            // A layer must exist and be selected
            // An annotation must exist and be selected
            // Only one annotation must be selected

            currentLayer = notateXpress1.Layers.Selected();
            if (currentLayer == null)
            {
                return;
            }

            firstElement = currentLayer.Elements.FirstSelected();
            if (firstElement == null)
            {
                return;
            }

            if (currentLayer.Elements.NextSelected() != null)
            {
                return;
            }


            // Start enabling valid menu items, by checking if the class supports the property/method

            propertyExists = firstElement.GetType().GetProperty("BackColor");
            if (propertyExists == null) // try fill color, they are both very close in functionality
                propertyExists = firstElement.GetType().GetProperty("FillColor");
            backgroundColorToolStripMenuItem.Enabled = (propertyExists != null);

            penColorToolStripMenuItem.Enabled = (firstElement.GetType().GetProperty("PenColor") != null);

            penWidthToolStripMenuItem.Enabled = (firstElement.GetType().GetProperty("PenWidth") != null);

            fontToolStripMenuItem.Enabled = (firstElement.GetType().GetProperty("TextFont") != null);

        }


        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set the back color / fill color
            Layer currentLayer;
            Element firstElement;
            ColorDialog colorChooser;
            Boolean isBackColor;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    firstElement = currentLayer.Elements.FirstSelected();
                    if (firstElement != null)
                    {
                        // we go through reflection, since we don't know what type the element is. (rect, image, etc)
                        isBackColor = (firstElement.GetType().GetProperty("BackColor") != null);
                        colorChooser = new ColorDialog();
                        if (isBackColor)
                        {
                            colorChooser.Color = (Color)firstElement.GetType().GetProperty("BackColor").GetValue(firstElement, null);
                        }
                        else
                        {
                            colorChooser.Color = (Color)firstElement.GetType().GetProperty("FillColor").GetValue(firstElement, null);
                        }

                        if (colorChooser.ShowDialog() == DialogResult.OK)
                        {


                            if (isBackColor) // should be back color, if not fill color.  Unless safety check failed.
                            {
                                firstElement.GetType().GetProperty("BackColor").SetValue(firstElement, colorChooser.Color, null);
                            }
                            else // fill color
                            {
                                firstElement.GetType().GetProperty("FillColor").SetValue(firstElement, colorChooser.Color, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }


        private void penColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set the pen color
            Layer currentLayer;
            Element firstElement;
            ColorDialog colorChooser;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    firstElement = currentLayer.Elements.FirstSelected();
                    if (firstElement != null)
                    {
                        colorChooser = new ColorDialog();
                        colorChooser.Color = (Color)firstElement.GetType().GetProperty("PenColor").GetValue(firstElement, null);
                        if (colorChooser.ShowDialog() == DialogResult.OK)
                        {
                            // we jump through reflection, since we don't know what type the element is. (rect, image, etc)
                            firstElement.GetType().GetProperty("PenColor").SetValue(firstElement, colorChooser.Color, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void penWidthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set the pen width (line width)
            Layer currentLayer;
            Element firstElement;
            string penWidth;
            int penWidthVal;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    firstElement = currentLayer.Elements.FirstSelected();
                    if (firstElement != null)
                    {
                        // we go through reflection, since we don't know what type the element is. (rect, image, etc)
                        penWidth = firstElement.GetType().GetProperty("PenWidth").GetValue(firstElement, null).ToString();
                        penWidth = Microsoft.VisualBasic.Interaction.InputBox("Specify the pen width", "NotateXpress Sample", penWidth, 100, 100);
                        int.TryParse(penWidth, out penWidthVal);
                        firstElement.GetType().GetProperty("PenWidth").SetValue(firstElement, penWidthVal, null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set the font
            Layer currentLayer;
            Element firstElement;
            FontDialog fontChooser;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    firstElement = currentLayer.Elements.FirstSelected();
                    if (firstElement != null)
                    {
                        fontChooser = new FontDialog();
                        fontChooser.Font = (Font)firstElement.GetType().GetProperty("TextFont").GetValue(firstElement, null);
                        if (fontChooser.ShowDialog() == DialogResult.OK)
                        {
                            // we jump through reflection, since we don't know what type the element is. (rect, image, etc)
                            firstElement.GetType().GetProperty("TextFont").SetValue(firstElement, fontChooser.Font, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        #endregion  // SelectedChange

        #region AllChange


        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // change all annotations on this layer
        }

        private void allToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            Layer currentLayer;
            System.Reflection.PropertyInfo propertyExists;


            // Select what menu items can be enabled for the currently selected item


            // Note:
            // A layer must exist and be selected
            // Annotations must exist on the current layer

            currentLayer = notateXpress1.Layers.Selected();
            if (currentLayer == null)
            {
                return;
            }

            if (currentLayer.Elements.Count <= 0)
            {
                return;
            }


            // Start enabling valid menu items, by checking if the class supports the property/method
            foreach (Element element in currentLayer.Elements)
            {

                if (!backgroundColorToolStripMenuItem1.Enabled)
                {
                    propertyExists = element.GetType().GetProperty("BackColor");
                    if (propertyExists == null) // try fill color, they are both very close in functionality
                        propertyExists = element.GetType().GetProperty("FillColor");
                    backgroundColorToolStripMenuItem1.Enabled = (propertyExists != null);
                }

                if (!penColorToolStripMenuItem1.Enabled)
                    penColorToolStripMenuItem1.Enabled = (element.GetType().GetProperty("PenColor") != null);


                if (!penWidthToolStripMenuItem1.Enabled)
                    penWidthToolStripMenuItem1.Enabled = (element.GetType().GetProperty("PenWidth") != null);

                if (!fontToolStripMenuItem1.Enabled)
                    fontToolStripMenuItem1.Enabled = (element.GetType().GetProperty("TextFont") != null);

            }

        }

        private void allToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            // start by turning off all choices
            foreach (ToolStripMenuItem menuItem in allToolStripMenuItem.DropDownItems)
            {
                menuItem.Enabled = false;
            }

        }

        private void backgroundColorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Set the back (or fill) color for all elements on this layer
            Layer currentLayer;
            ColorDialog colorChooser;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    // we go through reflection, since we don't know what type the element is. (rect, image, etc)
                    colorChooser = new ColorDialog();
                    if (colorChooser.ShowDialog() == DialogResult.OK)
                    {

                        foreach (Element element in currentLayer.Elements)
                        {

                            if (element.GetType().GetProperty("BackColor") != null)
                            {
                                element.GetType().GetProperty("BackColor").SetValue(element, colorChooser.Color, null);
                            }
                            if (element.GetType().GetProperty("FillColor") != null)
                            {
                                element.GetType().GetProperty("FillColor").SetValue(element, colorChooser.Color, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void penColorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Set the pen color for all elements on this layer
            Layer currentLayer;
            ColorDialog colorChooser;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    // we go through reflection, since we don't know what type the element is. (rect, image, etc)
                    colorChooser = new ColorDialog();
                    if (colorChooser.ShowDialog() == DialogResult.OK)
                    {

                        foreach (Element element in currentLayer.Elements)
                        {

                            if (element.GetType().GetProperty("PenColor") != null)
                            {
                                element.GetType().GetProperty("PenColor").SetValue(element, colorChooser.Color, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }

        private void penWidthToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Set the pen width for all elements on this layer
            Layer currentLayer;
            string penWidth;
            int penWidthVal;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    // we go through reflection, since we don't know what type the element is. (rect, image, etc)
                    penWidth = "1";
                    penWidth = Microsoft.VisualBasic.Interaction.InputBox("Specify the pen width", "NotateXpress Sample", penWidth, 100, 100);
                    int.TryParse(penWidth, out penWidthVal);


                    foreach (Element element in currentLayer.Elements)
                    {

                        if (element.GetType().GetProperty("PenWidth") != null)
                        {
                            element.GetType().GetProperty("PenWidth").SetValue(element, penWidthVal, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void fontToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // Set the font for all elements on this layer
            Layer currentLayer;
            FontDialog fontChooser;

            try
            {
                currentLayer = notateXpress1.Layers.Selected();
                if (currentLayer != null)
                {
                    // we go through reflection, since we don't know what type the element is. (rect, image, etc)
                    fontChooser = new FontDialog();
                    if (fontChooser.ShowDialog() == DialogResult.OK)
                    {

                        foreach (Element element in currentLayer.Elements)
                        {

                            if (element.GetType().GetProperty("TextFont") != null)
                            {
                                element.GetType().GetProperty("TextFont").SetValue(element, fontChooser.Font, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        #endregion  // All Change

        #region ContextMenu

        private void enabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Boolean isEnabled;

            isEnabled = notateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool);
            isEnabled = !isEnabled;
            notateXpress1.MenuSetEnabled(MenuType.Context, AnnotationTool.NoTool, isEnabled);

            enabledToolStripMenuItem.Checked = isEnabled;
        }

        private void addCustomTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Enter a custom menu name, displaying the old name if one exists

            // To see when the new items are fired, please see the MenuSelect event.

            // If you want to create your own custom menus outside of NotateXpress, Turn off the context menu and 
            // NotateXpress will fire a ContextMenu event, when you should launch your own menuing system.
            string customName;

            try
            {
                // 20000 is the starting number for custom menu items.
                customName = "";
                customName = Microsoft.VisualBasic.Interaction.InputBox("Please enter a custom menu item", "NotateXpress Sample", customName, 100, 100);
                notateXpress1.MenuAddItem(MenuType.Context, AnnotationTool.NoTool, CustomMenuCounter++, 0, customName, 0, 0);



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        #endregion // Context Menu
        #region Toolbars
        private void notateXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // reminder:  NotateXpress Toolbars are configured per layer
            Layer currentLayer;

            currentLayer = notateXpress1.Layers.Selected();
            currentLayer.Toolbar.Visible = !currentLayer.Toolbar.Visible;
        }


        private void imagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageXView1.Toolbar.Visible = !imageXView1.Toolbar.Visible;
        }

        private void toolbarToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            Layer currentLayer;

            currentLayer = notateXpress1.Layers.Selected();

            notateXpressToolStripMenuItem.Checked = currentLayer.Toolbar.Visible;
            imagXpressToolStripMenuItem.Checked = imageXView1.Toolbar.Visible;

        }

        private void smallToolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.ToolbarDefaults.ToolbarLargeIcons = false;
        }

        private void largeToolbarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.ToolbarDefaults.ToolbarLargeIcons = true;
        }



        private void buttonToolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Layer currentLayer;
            Boolean isVisible;

            currentLayer = notateXpress1.Layers.Selected();
            isVisible = currentLayer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool);
            isVisible = !isVisible;
            currentLayer.Toolbar.SetToolVisible(AnnotationTool.ButtonTool, isVisible);
            
            buttonToolToolStripMenuItem.Checked = isVisible;
 
        }

        private void enableNoteToolToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Layer currentLayer;
            Boolean isEnabled;

            currentLayer = notateXpress1.Layers.Selected();
            isEnabled = currentLayer.Toolbar.GetToolEnabled(AnnotationTool.NoteTool);
            isEnabled = !isEnabled;
            currentLayer.Toolbar.SetToolEnabled(AnnotationTool.NoteTool, isEnabled);

            enableNoteToolToolStripMenuItem.Checked = isEnabled;

        }
        #endregion  // Toolbars
    }
}

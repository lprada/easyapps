﻿namespace BasicCSharpAnnotations
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            // Critical to dispose objects


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.notateXpress1 = new Accusoft.NotateXpressSdk.NotateXpress(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wangKodakAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressNXPAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openAnnotationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressNXPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressXMLNXXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adobePDFXFDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAnnotationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stampToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rulerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutNotateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutImagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.processor1 = new Accusoft.ImagXpressSdk.Processor(this.components);
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.Location = new System.Drawing.Point(75, 61);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(869, 618);
            this.imageXView1.TabIndex = 0;
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            this.notateXpress1.Stamping += new Accusoft.NotateXpressSdk.NotateXpress.StampingEventHandler(this.notateXpress1_Stamping);
            this.notateXpress1.ToolbarSelect += new Accusoft.NotateXpressSdk.NotateXpress.ToolbarEventHandler(this.notateXpress1_ToolbarSelect);
            this.notateXpress1.AnnotationAdded += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 710);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.createAnnotationsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.saveImageToolStripMenuItem,
            this.toolStripSeparator1,
            this.openAnnotationFileToolStripMenuItem,
            this.saveAnnotationFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noAnnotationsToolStripMenuItem,
            this.wangKodakAnnotationsToolStripMenuItem,
            this.notateXpressNXPAnnotationsToolStripMenuItem});
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.openImageToolStripMenuItem.Text = "&Open Image";
            // 
            // noAnnotationsToolStripMenuItem
            // 
            this.noAnnotationsToolStripMenuItem.Name = "noAnnotationsToolStripMenuItem";
            this.noAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.noAnnotationsToolStripMenuItem.Text = "No Annotations";
            this.noAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.noAnnotationsToolStripMenuItem_Click);
            // 
            // wangKodakAnnotationsToolStripMenuItem
            // 
            this.wangKodakAnnotationsToolStripMenuItem.Name = "wangKodakAnnotationsToolStripMenuItem";
            this.wangKodakAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.wangKodakAnnotationsToolStripMenuItem.Text = "Embedded Wang / Kodak Annotations";
            this.wangKodakAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.wangKodakAnnotationsToolStripMenuItem_Click);
            // 
            // notateXpressNXPAnnotationsToolStripMenuItem
            // 
            this.notateXpressNXPAnnotationsToolStripMenuItem.Name = "notateXpressNXPAnnotationsToolStripMenuItem";
            this.notateXpressNXPAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.notateXpressNXPAnnotationsToolStripMenuItem.Text = "Embedded NotateXpress Annotations";
            this.notateXpressNXPAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.notateXpressNXPAnnotationsToolStripMenuItem_Click);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveImageToolStripMenuItem.Text = "&Save Image";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.toolStripMenuItem1.Text = "No Annotations";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(217, 22);
            this.toolStripMenuItem2.Text = "Wang / Kodak Annotations";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(217, 22);
            this.toolStripMenuItem3.Text = "NotateXpress Annotations";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // openAnnotationFileToolStripMenuItem
            // 
            this.openAnnotationFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notateXpressNXPToolStripMenuItem,
            this.notateXpressXMLNXXMLToolStripMenuItem,
            this.adobePDFXFDFToolStripMenuItem});
            this.openAnnotationFileToolStripMenuItem.Name = "openAnnotationFileToolStripMenuItem";
            this.openAnnotationFileToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.openAnnotationFileToolStripMenuItem.Text = "Open Annotation File";
            // 
            // notateXpressNXPToolStripMenuItem
            // 
            this.notateXpressNXPToolStripMenuItem.Name = "notateXpressNXPToolStripMenuItem";
            this.notateXpressNXPToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.notateXpressNXPToolStripMenuItem.Text = "NotateXpress (NXP)";
            this.notateXpressNXPToolStripMenuItem.Click += new System.EventHandler(this.notateXpressNXPToolStripMenuItem_Click);
            // 
            // notateXpressXMLNXXMLToolStripMenuItem
            // 
            this.notateXpressXMLNXXMLToolStripMenuItem.Name = "notateXpressXMLNXXMLToolStripMenuItem";
            this.notateXpressXMLNXXMLToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.notateXpressXMLNXXMLToolStripMenuItem.Text = "NotateXpress XML (NXXML)";
            this.notateXpressXMLNXXMLToolStripMenuItem.Click += new System.EventHandler(this.notateXpressXMLNXXMLToolStripMenuItem_Click);
            // 
            // adobePDFXFDFToolStripMenuItem
            // 
            this.adobePDFXFDFToolStripMenuItem.Name = "adobePDFXFDFToolStripMenuItem";
            this.adobePDFXFDFToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.adobePDFXFDFToolStripMenuItem.Text = "Adobe PDF (XFDF)";
            this.adobePDFXFDFToolStripMenuItem.Click += new System.EventHandler(this.adobePDFXFDFToolStripMenuItem_Click);
            // 
            // saveAnnotationFileToolStripMenuItem
            // 
            this.saveAnnotationFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.saveAnnotationFileToolStripMenuItem.Name = "saveAnnotationFileToolStripMenuItem";
            this.saveAnnotationFileToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveAnnotationFileToolStripMenuItem.Text = "Save Annotation File";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItem4.Text = "NotateXpress (NXP)";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItem5.Text = "NotateXpress XML (NXXML)";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItem6.Text = "Adobe PDF (XFDF)";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // createAnnotationsToolStripMenuItem
            // 
            this.createAnnotationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rectangleToolStripMenuItem,
            this.stampToolStripMenuItem,
            this.textToolStripMenuItem,
            this.rulerToolStripMenuItem});
            this.createAnnotationsToolStripMenuItem.Name = "createAnnotationsToolStripMenuItem";
            this.createAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.createAnnotationsToolStripMenuItem.Text = "Create Annotations";
            // 
            // rectangleToolStripMenuItem
            // 
            this.rectangleToolStripMenuItem.Name = "rectangleToolStripMenuItem";
            this.rectangleToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.rectangleToolStripMenuItem.Text = "Rectangle";
            this.rectangleToolStripMenuItem.Click += new System.EventHandler(this.rectangleToolStripMenuItem_Click);
            // 
            // stampToolStripMenuItem
            // 
            this.stampToolStripMenuItem.Name = "stampToolStripMenuItem";
            this.stampToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stampToolStripMenuItem.Text = "Stamp";
            this.stampToolStripMenuItem.Click += new System.EventHandler(this.stampToolStripMenuItem_Click);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.textToolStripMenuItem.Text = "Text";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.textToolStripMenuItem_Click);
            // 
            // rulerToolStripMenuItem
            // 
            this.rulerToolStripMenuItem.Name = "rulerToolStripMenuItem";
            this.rulerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.rulerToolStripMenuItem.Text = "Ruler";
            this.rulerToolStripMenuItem.Click += new System.EventHandler(this.rulerToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutNotateXpressToolStripMenuItem,
            this.aboutImagXpressToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutNotateXpressToolStripMenuItem
            // 
            this.aboutNotateXpressToolStripMenuItem.Name = "aboutNotateXpressToolStripMenuItem";
            this.aboutNotateXpressToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutNotateXpressToolStripMenuItem.Text = "About NotateXpress";
            this.aboutNotateXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutNotateXpressToolStripMenuItem_Click);
            // 
            // aboutImagXpressToolStripMenuItem
            // 
            this.aboutImagXpressToolStripMenuItem.Name = "aboutImagXpressToolStripMenuItem";
            this.aboutImagXpressToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutImagXpressToolStripMenuItem.Text = "About ImagXpress";
            this.aboutImagXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutImagXpressToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 732);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.imageXView1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.Text = "Basic C# Annotations";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private Accusoft.NotateXpressSdk.NotateXpress notateXpress1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem openAnnotationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAnnotationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressNXPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressXMLNXXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adobePDFXFDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wangKodakAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressNXPAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Accusoft.ImagXpressSdk.Processor processor1;
        private System.Windows.Forms.ToolStripMenuItem createAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rectangleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stampToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rulerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutNotateXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutImagXpressToolStripMenuItem;
    }
}


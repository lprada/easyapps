﻿//
// Copyright (C) 2009 Pegasus Imaging Corp.  
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

// Accusoft Pegasus 
using Accusoft.ImagXpressSdk;
using Accusoft.NotateXpressSdk;

namespace BasicCSharpAnnotations
{
    public partial class FormMain : Form
    {
        private int OpenImageFilterIndex = 1;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // all initializations of Accusoft code is done here

            // The Viewer part of ImagXpress
            imageXView1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
            imageXView1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left;
            imageXView1.AlphaBlend = true;
            imageXView1.AutoImageDispose = true;

            // The annotations
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "The date is: " + DateTime.Now.ToString();
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = new Font("Arial", 14, FontStyle.Italic);
            notateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = new Font("Arial", 14, FontStyle.Regular);

            ResetSystem();

        }

        private void ResetSystem()
        {
            Accusoft.NotateXpressSdk.LoadOptions lo;
            lo = new Accusoft.NotateXpressSdk.LoadOptions();

            lo.UnicodeMode = true;  // Set up NX for Unicode
            lo.AnnType = AnnotationType.NotateXpressXml;
            notateXpress1.Layers.SetLoadOptions(lo);

            notateXpress1.Layers.Clear();

            notateXpress1.ClientWindow = imageXView1.Handle;
            imageXView1.AutoResize = AutoResizeType.BestFit;


            // Path to Accusoft shared images
            // Load default image, from default location

            String strCurrentDir = System.IO.Path.Combine(Application.StartupPath, @"..\..\..\..\..\..\..\..\..\..\Common\Images");
            if (System.IO.Directory.Exists(strCurrentDir))
                System.IO.Directory.SetCurrentDirectory(strCurrentDir);

            // Make sure we do not leak images.
            if (imageXView1.Image != null)
                imageXView1.Image.Dispose();

            if (System.IO.File.Exists(@"Benefits.tif"))
                imageXView1.Image = ImageX.FromFile(imagXpress1, @"Benefits.tif");

            // make sure we have a layer to draw upon.
            if (notateXpress1.Layers.Count == 0)  // we could have a built in layer from the image.
                if (imageXView1.Image != null) // make sure we loaded an image
                   notateXpress1.Layers.Add(new Layer());

            // turn on the built in toolbar
            notateXpress1.ToolbarDefaults.ToolbarActivated = true;
            
            if (notateXpress1.Layers.Count > 0)  // make sure we have a layer on an image
               notateXpress1.Layers[0].Toolbar.Visible = true;

            notateXpress1.FontScaling = FontScaling.ResolutionY;
            notateXpress1.LineScaling = LineScaling.ResolutionY;
            notateXpress1.MultiLineEdit = true; // The more sophisticated editor.

            toolStripStatusLabel1.Text = "Welcome to the BasicCSharpAnnotation Demonstration";
        }


        private void openImage(string annType)
        {
            Accusoft.NotateXpressSdk.LoadOptions lo;
            lo = new Accusoft.NotateXpressSdk.LoadOptions();



            if (annType == "Wang")
            {
                lo.PreserveWangLayers = false;  // not a popular option.
                lo.UnicodeMode = true;  // Unicode, can be changed for non-unicode wang files
                lo.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak

                notateXpress1.ImagXpressLoad = true;
                notateXpress1.Layers.SetLoadOptions(lo);
                notateXpress1.FontScaling = FontScaling.Normal;
                notateXpress1.LineScaling = LineScaling.Normal;

            }
            else if (annType == "NXP")
            {
                lo.PreserveWangLayers = false;  // not a popular option.
                lo.UnicodeMode = true;  // Unicode, can be changed for non-unicode wang files
                lo.AnnType = AnnotationType.NotateXpress; // internal fast format

                notateXpress1.ImagXpressLoad = true;
                notateXpress1.Layers.SetLoadOptions(lo);

            }
            else // if (annType == "none")
            {
                lo.UnicodeMode = true;  // Set up NX for Unicode
                lo.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak
                notateXpress1.ImagXpressLoad = false;
                notateXpress1.Layers.SetLoadOptions(lo);
                notateXpress1.FontScaling = FontScaling.ResolutionY;
                notateXpress1.LineScaling = LineScaling.ResolutionY;
            }

            // A reduced list of supported file formats
            System.String strDefaultImageFilter = "All Files (*.*)|*.*|Windows Bitmap (*.BMP)|*.bmp" +
                                                  "|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx" +
                                                  "|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2" +
                                                  "|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif" +
                                                  "|Zsoft PaintBrush (*.PCX)|*.pcx" +
                                                  "|Portable Network Graphics (*.PNG)|*.png" +
                                                  "|Tagged Image Format (*.TIFF)|*.tif;*.tiff";

            openFileDialog1.FileName = "";
            openFileDialog1.Title = "Please select an image";
            openFileDialog1.Filter = strDefaultImageFilter;
            openFileDialog1.Multiselect = false;
            openFileDialog1.FilterIndex = OpenImageFilterIndex;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Blank out existing annotations.
                notateXpress1.Layers.Clear();

                if (imageXView1.Image != null)
                    imageXView1.Image.Dispose();
                imageXView1.Image = ImageX.FromFile(imagXpress1, openFileDialog1.FileName);

                // make sure we have a layer to draw upon.
                if (notateXpress1.Layers.Count == 0)  // we could have a built in layer from the image.
                    notateXpress1.Layers.Add(new Layer());

                // default to layer #1, and turn on the toolbar.
                notateXpress1.Layers[0].Active = true;
                notateXpress1.Layers[0].Toolbar.Visible = true;
            }

            OpenImageFilterIndex = openFileDialog1.FilterIndex;

        }


        private void noAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openImage("none");
        }

        private void wangKodakAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openImage("Wang");

        }

        private void notateXpressNXPAnnotationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openImage("NXP");

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveImage(string annType)
        {
            Accusoft.NotateXpressSdk.SaveOptions so;
            so = new Accusoft.NotateXpressSdk.SaveOptions();


            if (annType == "Wang")
            {
                so.PreserveWangLayers = false;  // not a popular option.
                so.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak

                notateXpress1.ImagXpressSave = true;
                notateXpress1.Layers.SetSaveOptions(so);

            }
            else if (annType == "NXP")
            {
                so.PreserveWangLayers = false;  // not a popular option.
                so.AnnType = AnnotationType.NotateXpress; // internal fast format

                notateXpress1.ImagXpressSave = true;
                notateXpress1.Layers.SetSaveOptions(so);

            }
            else // if (annType == "none")
            {
                so.AnnType = AnnotationType.ImagingForWindows; // aka Wang, aka Kodak
                notateXpress1.ImagXpressSave = false;
                notateXpress1.Layers.SetSaveOptions(so);
            }


            // A reduced list of supported file formats
            saveFileDialog1.FileName = "";
            saveFileDialog1.Title = "Please save the new image";

            System.String strDefaultImageFilter = "Windows Bitmap (*.BMP)|*.bmp" +
                                                  "|JPEG (*.JPG)|*.jpg" +
                                                  "|Portable Network Graphics (*.PNG)|*.png" +
                                                  "|Tagged Image Format G4 (*.TIFF)|*.tif;*.tiff";

            Accusoft.ImagXpressSdk.SaveOptions soOpts = new Accusoft.ImagXpressSdk.SaveOptions();

            saveFileDialog1.Filter = strDefaultImageFilter;
            saveFileDialog1.FilterIndex = 4;  // default to tiff

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FilterIndex == 1) // bmp
                {
                    soOpts.Format = ImageXFormat.Bmp;
                    soOpts.Bmp.Compression = Compression.Rle;
                }
                else if (saveFileDialog1.FilterIndex == 2) // jpg
                {
                    soOpts.Format = ImageXFormat.Jpeg;
                }
                else if (saveFileDialog1.FilterIndex == 3) // png
                {
                    soOpts.Format = ImageXFormat.Png;
                }
                else if (saveFileDialog1.FilterIndex == 4) // tiff G4
                {
                    soOpts.Format = ImageXFormat.Tiff;
                    soOpts.Tiff.Compression = Compression.Group4;
                    soOpts.Tiff.MultiPage = false;
                }

                processor1 = new Processor(imagXpress1, imageXView1.Image);
                processor1.Image.Save(saveFileDialog1.FileName, soOpts);
            }

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            saveImage("none");
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            saveImage("Wang");

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            saveImage("NXP");
        }

        private void notateXpress1_Stamping(object sender, EventArgs e)
        {
            // show the stamp tool being updated to the second
            notateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "Have a nice day " + Environment.NewLine + DateTime.Now.ToString();
        }

        private void notateXpress1_AnnotationAdded(object sender, AnnotationAddedEventArgs e)
        {
            // reset the toolbar to pointer after adding an annotation.
            notateXpress1.Layers[0].Toolbar.Selected = AnnotationTool.PointerTool;
        }

        // open an external annotation file
        private void openAnnotations(string annType)
        {
            Accusoft.NotateXpressSdk.LoadOptions lo;
            lo = new Accusoft.NotateXpressSdk.LoadOptions();
            System.String strDefaultImageFilter;

            lo.PreserveWangLayers = false;  // not a popular option.
            lo.UnicodeMode = true;
            if (annType == "XFDF")
            {
                strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf";
                lo.AnnType = AnnotationType.Xfdf; // XFDF is used by PDF
                openFileDialog1.FilterIndex = 2;
            }
            else if (annType == "NXP")
            {
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp";
                lo.AnnType = AnnotationType.NotateXpress; // internal fast format
                openFileDialog1.FilterIndex = 2;

            }
            else // if (annType == "NXXML")
            {
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml";
                lo.AnnType = AnnotationType.NotateXpressXml; // Standard XML
                openFileDialog1.FilterIndex = 2;
            }

            openFileDialog1.FileName = "";
            openFileDialog1.Title = "Please select an annotation file";
            openFileDialog1.Filter = strDefaultImageFilter;
            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Blank out existing annotations.
                notateXpress1.Layers.Clear();

                notateXpress1.Layers.FromFile(openFileDialog1.FileName, lo);

                // make sure we have a layer to draw upon.
                if (notateXpress1.Layers.Count == 0)  // we could have a built in layer from the image.
                    notateXpress1.Layers.Add(new Layer());

                // default to layer #1, and turn on the toolbar.
                notateXpress1.Layers[0].Active = true;
                notateXpress1.Layers[0].Toolbar.Visible = true;
            }

        }

        private void notateXpressNXPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openAnnotations("NXP");
        }

        private void notateXpressXMLNXXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openAnnotations("NXXML");

        }

        private void adobePDFXFDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openAnnotations("XFDF");
        }




        // save to an external annotation file
        private void saveAnnotations(string annType)
        {
            Accusoft.NotateXpressSdk.SaveOptions so;
            so = new Accusoft.NotateXpressSdk.SaveOptions();
            System.String strDefaultImageFilter;



            so.PreserveWangLayers = false;  // not a popular option.

            if (annType == "XFDF")
            {
                so.AnnType = AnnotationType.Xfdf; // XFDF is used by PDF
                strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf";
            }
            else if (annType == "NXP")
            {
                so.AnnType = AnnotationType.NotateXpress; // internal fast format
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp";

            }
            else // if (annType == "NXXML")
            {
                so.AnnType = AnnotationType.NotateXpressXml; // Standard XML
                strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml";

            }

            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.FileName = "";
            saveFileDialog1.Title = "Please save the annotation file";
            saveFileDialog1.Filter = strDefaultImageFilter;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                notateXpress1.Layers.SaveToFile(saveFileDialog1.FileName, so);
            }
        }


        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            saveAnnotations("NXP");
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            saveAnnotations("NXXML");
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            saveAnnotations("XFDF");
        }


        // Create annotations
        private void rectangleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RectangleTool myRect;

            myRect = new RectangleTool();
            myRect.BoundingRectangle = new Rectangle(100, 30, 500, 245);
            myRect.BackStyle = BackStyle.Translucent;
            myRect.FillColor = Color.Yellow;
            myRect.ToolTipText = "Thanks for hovering";

            // we can use Layers[0], as we only keep one layer.
            notateXpress1.Layers[0].Elements.Add(myRect);
        }

        private void stampToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StampTool myStamp;

            myStamp = new StampTool();
            // A starting point, autosize will adjust to fit
            myStamp.BoundingRectangle = new Rectangle(700, 30, 10, 10);
            myStamp.BackStyle = BackStyle.Translucent;
            myStamp.BackColor = Color.Yellow;
            myStamp.PenWidth = 3;
            myStamp.PenColor = Color.Black;
            myStamp.Text = "Complete" + Environment.NewLine + DateTime.Now.ToString() + " ";
            myStamp.TextFont = new Font("Segoe UI", 16, FontStyle.Bold);
            myStamp.TextColor = Color.DarkBlue;
            myStamp.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;


            // we can  use Layers[0], as we only keep one layer.
            notateXpress1.Layers[0].Elements.Add(myStamp);

        }


        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextTool myText;

            myText = new TextTool();
            // A starting point, autosize will adjust to fit
            myText.BoundingRectangle = new Rectangle(450, 1100, 10, 10);
            myText.BackStyle = BackStyle.Opaque;
            myText.BackColor = Color.White;
            myText.TextColor = Color.Blue;
            myText.TextFont = new Font("Arial Unicode MS", 16, FontStyle.Regular);
            myText.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents;
            myText.TextJustification = TextJustification.Center;
            myText.PenWidth = 2;
            myText.PenColor = Color.DarkBlue;

            myText.Text = "The current date and time is: " + Environment.NewLine + DateTime.Now.ToString() +
                Environment.NewLine + "Unicode is fully supported: " + Environment.NewLine + "שָׁלוֹם";


            // we can  use Layers[0], as we only keep one layer.
            notateXpress1.Layers[0].Elements.Add(myText);
        }


        private void rulerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RulerTool myRuler;

            myRuler = new RulerTool();
            myRuler.BoundingRectangle = new Rectangle(200, 550, 900, 0);
            myRuler.ShowAbbreviation = false;
            myRuler.MeasurementUnit = MeasurementUnit.Millimeters;
            myRuler.PenWidth = 3;


            // we can  use Layers[0], as we only keep one layer.
            notateXpress1.Layers[0].Elements.Add(myRuler);
        }

        private void notateXpress1_ToolbarSelect(object sender, Accusoft.NotateXpressSdk.ToolbarEventArgs e)
        {
            if (e.Tool == AnnotationTool.ImageTool)
            {
                MessageBox.Show("To learn how to set up the image tool, please see the help file or the full sample");
            }
        }

        private void aboutNotateXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notateXpress1.AboutBox();
        }

        private void aboutImagXpressToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

    }
}

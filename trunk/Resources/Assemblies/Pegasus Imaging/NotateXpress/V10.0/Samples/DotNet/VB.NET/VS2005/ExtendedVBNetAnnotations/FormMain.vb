﻿'
' Copyright (C) 2009 Pegasus Imaging Corp.  
'

' Accusoft Pegasus 
Imports Accusoft.ImagXpressSdk
Imports Accusoft.NotateXpressSdk

Public Class FormMain
    Private CustomMenuCounter As Integer = 20000
    Private OpenImageFilterIndex As Integer = 1

    Private Sub FormMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim imageToolImage As ImageX
        ' all initializations of Accusoft code is done here 

        ' The Viewer part of ImagXpress 
        ImageXView1.Anchor = AnchorStyles.Bottom Or AnchorStyles.Right Or AnchorStyles.Top Or AnchorStyles.Left
        ImageXView1.Anchor = AnchorStyles.Bottom Or AnchorStyles.Right Or AnchorStyles.Top Or AnchorStyles.Left
        ImageXView1.AlphaBlend = True
        ImageXView1.AutoImageDispose = True

        ' Path to Accusoft shared images 
        ' Load default image, from default location 
        Dim strCurrentDir As [String] = System.IO.Path.Combine(Application.StartupPath, "..\..\..\..\..\..\..\..\..\..\Common\Images")
        If System.IO.Directory.Exists(strCurrentDir) Then
            System.IO.Directory.SetCurrentDirectory(strCurrentDir)
        End If

        ' set up toolbar defaults 
        ' These are set globally for all toolbars, they can also be set on a layer basis. 
        NotateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "The date is: " & DateTime.Now.ToString()
        NotateXpress1.ToolbarDefaults.StampToolbarDefaults.TextFont = New Font("Arial", 14, FontStyle.Italic)
        NotateXpress1.ToolbarDefaults.TextToolbarDefaults.TextFont = New Font("Arial", 14, FontStyle.Regular)

        NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.BackStyle = BackStyle.Transparent
        NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.TransparentColor = Color.White
        NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.PenWidth = 0
        NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.ToolTipText = "Image Annotation"
        NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents

        If (System.IO.File.Exists("Signature48.png")) Then
            imageToolImage = ImageX.FromFile(ImagXpress1, "Signature48.png")
            Dim dibHandle As IntPtr = imageToolImage.ToHdib(True)

            NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle

            'resource cleanup 
            If imageToolImage IsNot Nothing Then
                imageToolImage.Dispose()
                imageToolImage = Nothing
            End If

        End If


        ResetSystem()

    End Sub
    Private Sub ResetSystem()
        Dim lo As Accusoft.NotateXpressSdk.LoadOptions
        lo = New Accusoft.NotateXpressSdk.LoadOptions()

        lo.UnicodeMode = True
        ' Set up NX for Unicode 
        lo.AnnType = AnnotationType.NotateXpressXml
        NotateXpress1.Layers.SetLoadOptions(lo)

        NotateXpress1.Layers.Clear()

        NotateXpress1.ClientWindow = ImageXView1.Handle
        ImageXView1.AutoResize = AutoResizeType.BestFit



        ' Make sure we do not leak images. 
        If ImageXView1.Image IsNot Nothing Then
            ImageXView1.Image.Dispose()
        End If
        If (System.IO.File.Exists("Benefits.tif")) Then
            ImageXView1.Image = ImageX.FromFile(ImagXpress1, "Benefits.tif")
        End If


        ' make sure we have a layer to draw upon. 
        If NotateXpress1.Layers.Count = 0 Then
            ' we could have a built in layer from the image. 
            If (ImageXView1.Image IsNot Nothing) Then ' make sure we loaded an image
                NotateXpress1.Layers.Add(New Layer())
            End If
        End If

        ' turn on the built in toolbar 
        NotateXpress1.ToolbarDefaults.ToolbarActivated = True

        ' make sure we have a layer to draw upon. 
        If NotateXpress1.Layers.Count > 0 Then
            NotateXpress1.Layers(0).Toolbar.Visible = True
        End If

        ' pre start the ImagXpress toolbar 
        ImageXView1.Toolbar.Activated = True
        ImageXView1.Toolbar.Visible = False

        NotateXpress1.FontScaling = FontScaling.ResolutionY
        NotateXpress1.LineScaling = LineScaling.ResolutionY
        NotateXpress1.MultiLineEdit = True
        ' The more sophisticated editor. 
        StatusStrip1.Text = "Welcome to the Extended VB Net Annotation Demonstration"

        ' set menu check boxes 
        If NotateXpress1.LineScaling = LineScaling.ResolutionY Then
            lineScalingToolStripMenuItem.Checked = True
        Else
            lineScalingToolStripMenuItem.Checked = False
        End If

        If NotateXpress1.FontScaling = FontScaling.ResolutionY Then
            fontScalingToolStripMenuItem.Checked = True
        Else
            fontScalingToolStripMenuItem.Checked = False
        End If


        useMLEForTextEntryToolStripMenuItem.Checked = NotateXpress1.MultiLineEdit
        enabledToolStripMenuItem.Checked = NotateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool)
        imagXpressToolStripMenuItem.Checked = ImageXView1.Toolbar.Visible
        ' make sure we have a layer to draw upon. 
        If NotateXpress1.Layers.Count > 0 Then
            notateXpressToolStripMenuItem.Checked = NotateXpress1.Layers(0).Toolbar.Visible
            buttonToolToolStripMenuItem.Checked = NotateXpress1.Layers(0).Toolbar.GetToolVisible(AnnotationTool.ButtonTool)
            enableNoteToolToolStripMenuItem.Checked = NotateXpress1.Layers(0).Toolbar.GetToolEnabled(AnnotationTool.NoteTool)
        End If


    End Sub
    Private Sub openImage(ByVal annType As String)
        Dim lo As Accusoft.NotateXpressSdk.LoadOptions
        lo = New Accusoft.NotateXpressSdk.LoadOptions()



        If annType = "Wang" Then
            lo.PreserveWangLayers = False
            ' not a popular option. 
            lo.UnicodeMode = True
            ' Unicode, can be changed for non-unicode wang files 
            lo.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressLoad = True

            NotateXpress1.Layers.SetLoadOptions(lo)
            NotateXpress1.FontScaling = FontScaling.Normal
            NotateXpress1.LineScaling = LineScaling.Normal
        ElseIf annType = "NXP" Then
            lo.PreserveWangLayers = False
            ' not a popular option. 
            lo.UnicodeMode = True
            ' Unicode, can be changed for non-unicode wang files 
            lo.AnnType = AnnotationType.NotateXpress
            ' internal fast format 
            NotateXpress1.ImagXpressLoad = True

            NotateXpress1.Layers.SetLoadOptions(lo)
        Else
            ' if (annType == "none") 
            lo.UnicodeMode = True
            ' Set up NX for Unicode 
            lo.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressLoad = False
            NotateXpress1.Layers.SetLoadOptions(lo)
            NotateXpress1.FontScaling = FontScaling.ResolutionY
            NotateXpress1.LineScaling = LineScaling.ResolutionY
        End If

        ' A reduced list of supported file formats 
        Dim strDefaultImageFilter As System.String = "All Files (*.*)|*.*|Windows Bitmap (*.BMP)|*.bmp" & "|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx" & "|CompuServe GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2" & "|JFIF Compliant JPEG (*.JPG & *.JIF)|*.jpg;*.jif" & "|Zsoft PaintBrush (*.PCX)|*.pcx" & "|Portable Network Graphics (*.PNG)|*.png" & "|Tagged Image Format (*.TIFF)|*.tif;*.tiff"

        OpenFileDialog1.FileName = ""
        OpenFileDialog1.Title = "Please select an image"
        OpenFileDialog1.Filter = strDefaultImageFilter
        OpenFileDialog1.Multiselect = False
        OpenFileDialog1.FilterIndex = OpenImageFilterIndex

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            ' Blank out existing annotations. 
            NotateXpress1.Layers.Clear()

            If ImageXView1.Image IsNot Nothing Then
                ImageXView1.Image.Dispose()
            End If
            ImageXView1.Image = ImageX.FromFile(ImagXpress1, OpenFileDialog1.FileName)

            ' make sure we have a layer to draw upon. 
            If NotateXpress1.Layers.Count = 0 Then
                ' we could have a built in layer from the image. 
                NotateXpress1.Layers.Add(New Layer())
            End If

            ' default to layer #1, and turn on the toolbar. 
            NotateXpress1.Layers(0).Active = True
            NotateXpress1.Layers(0).Toolbar.Visible = True

        End If
        OpenImageFilterIndex = OpenFileDialog1.FilterIndex
    End Sub


    Private Sub noAnnotationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem10.Click
        openImage("none")
    End Sub

    Private Sub wangKodakAnnotationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem11.Click

        openImage("Wang")
    End Sub

    Private Sub notateXpressNXPAnnotationsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem12.Click

        openImage("NXP")
    End Sub

    Private Sub exitToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem25.Click
        Close()
    End Sub

    Private Sub saveImage(ByVal annType As String)
        Dim so As Accusoft.NotateXpressSdk.SaveOptions
        so = New Accusoft.NotateXpressSdk.SaveOptions()


        If annType = "Wang" Then
            so.PreserveWangLayers = False
            ' not a popular option. 
            so.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressSave = True

            NotateXpress1.Layers.SetSaveOptions(so)
        ElseIf annType = "NXP" Then
            so.PreserveWangLayers = False
            ' not a popular option. 
            so.AnnType = AnnotationType.NotateXpress
            ' internal fast format 
            NotateXpress1.ImagXpressSave = True

            NotateXpress1.Layers.SetSaveOptions(so)
        Else
            ' if (annType == "none") 
            so.AnnType = AnnotationType.ImagingForWindows
            ' aka Wang, aka Kodak 
            NotateXpress1.ImagXpressSave = False
            NotateXpress1.Layers.SetSaveOptions(so)
        End If


        ' A reduced list of supported file formats 
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.Title = "Please save the new image"

        Dim strDefaultImageFilter As System.String = "Windows Bitmap (*.BMP)|*.bmp" & "|JPEG (*.JPG)|*.jpg" & "|Portable Network Graphics (*.PNG)|*.png" & "|Tagged Image Format G4 (*.TIFF)|*.tif;*.tiff"

        Dim soOpts As New Accusoft.ImagXpressSdk.SaveOptions()

        SaveFileDialog1.Filter = strDefaultImageFilter
        SaveFileDialog1.FilterIndex = 4
        ' default to tiff 
        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            If SaveFileDialog1.FilterIndex = 1 Then
                ' bmp 
                soOpts.Format = ImageXFormat.Bmp
                soOpts.Bmp.Compression = Compression.Rle
            ElseIf SaveFileDialog1.FilterIndex = 2 Then
                ' jpg 
                soOpts.Format = ImageXFormat.Jpeg
            ElseIf SaveFileDialog1.FilterIndex = 3 Then
                ' png 
                soOpts.Format = ImageXFormat.Png
            ElseIf SaveFileDialog1.FilterIndex = 4 Then
                ' tiff G4 
                soOpts.Format = ImageXFormat.Tiff
                soOpts.Tiff.Compression = Compression.Group4
                soOpts.Tiff.MultiPage = False
            End If

            Processor1 = New Processor(ImagXpress1, ImageXView1.Image)
            Processor1.Image.Save(SaveFileDialog1.FileName, soOpts)

        End If
    End Sub

    Private Sub toolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem14.Click
        saveImage("none")
    End Sub

    Private Sub toolStripMenuItem2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem15.Click

        saveImage("Wang")
    End Sub

    Private Sub toolStripMenuItem3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem16.Click
        saveImage("NXP")
    End Sub

    ' Note: Brand is memory intensive, it will need to make two copies of the image. 
    Private Sub brandToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles brandToolStripMenuItem.Click
        Try
            'brand annotations on current image at 24-bit 
            NotateXpress1.Layers.Brand(24)

            'delete annotations because now they've been branded in and we don't 
            'need a duplicate of selectable annotations 
            NotateXpress1.Layers.Clear()

            ' make sure we have a layer to draw upon. 
            NotateXpress1.Layers.Add(New Layer())

            ' turn on the built in toolbar 
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True

            NotateXpress1.Layers(0).Toolbar.Visible = True
        Catch ex As Exception
            MessageBox.Show("An error occured in the Demo. " & ex.Message)
        End Try
    End Sub


    Private Sub notateXpress1_Stamping(ByVal sender As Object, ByVal e As EventArgs) Handles NotateXpress1.Stamping
        ' show the stamp tool being updated to the second 
        NotateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = ("Have a nice day " & Environment.NewLine) + DateTime.Now.ToString()
    End Sub

    Private Sub notateXpress1_AnnotationAdded(ByVal sender As Object, ByVal e As AnnotationAddedEventArgs) Handles NotateXpress1.AnnotationAdded
        ' reset the toolbar to pointer after adding an annotation. 
        NotateXpress1.Layers(0).Toolbar.Selected = AnnotationTool.PointerTool
    End Sub

    ' open an external annotation file 
    Private Sub openAnnotations(ByVal annType As String)
        Dim lo As Accusoft.NotateXpressSdk.LoadOptions
        lo = New Accusoft.NotateXpressSdk.LoadOptions()
        Dim strDefaultImageFilter As System.String

        lo.PreserveWangLayers = False
        ' not a popular option. 
        lo.UnicodeMode = True
        If annType = "XFDF" Then
            strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf"
            lo.AnnType = AnnotationType.Xfdf
            ' XFDF is used by PDF 
            OpenFileDialog1.FilterIndex = 2
        ElseIf annType = "NXP" Then
            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp"
            lo.AnnType = AnnotationType.NotateXpress
            ' internal fast format 

            OpenFileDialog1.FilterIndex = 2
        Else
            ' if (annType == "NXXML") 
            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml"
            lo.AnnType = AnnotationType.NotateXpressXml
            ' Standard XML 
            OpenFileDialog1.FilterIndex = 2
        End If

        OpenFileDialog1.FileName = ""
        OpenFileDialog1.Title = "Please select an annotation file"
        OpenFileDialog1.Filter = strDefaultImageFilter
        OpenFileDialog1.Multiselect = False

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            ' Blank out existing annotations. 
            NotateXpress1.Layers.Clear()

            NotateXpress1.Layers.FromFile(OpenFileDialog1.FileName, lo)

            ' make sure we have a layer to draw upon. 
            If NotateXpress1.Layers.Count = 0 Then
                ' we could have a built in layer from the image. 
                NotateXpress1.Layers.Add(New Layer())
            End If

            ' default to layer #1, and turn on the toolbar. 
            NotateXpress1.Layers(0).Active = True
            NotateXpress1.Layers(0).Toolbar.Visible = True

        End If
    End Sub

    Private Sub notateXpressNXPToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem18.Click
        openAnnotations("NXP")
    End Sub

    Private Sub notateXpressXMLNXXMLToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem19.Click

        openAnnotations("NXXML")
    End Sub

    Private Sub adobePDFXFDFToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem20.Click
        openAnnotations("XFDF")
    End Sub




    ' save to an external annotation file 
    Private Sub saveAnnotations(ByVal annType As String)
        Dim so As Accusoft.NotateXpressSdk.SaveOptions
        so = New Accusoft.NotateXpressSdk.SaveOptions()
        Dim strDefaultImageFilter As System.String



        so.PreserveWangLayers = False
        ' not a popular option. 
        If annType = "XFDF" Then
            so.AnnType = AnnotationType.Xfdf
            ' XFDF is used by PDF 
            strDefaultImageFilter = "All Files (*.*)|*.*|PDF - XFDF (*.XFDF)|*.xfdf"
        ElseIf annType = "NXP" Then
            so.AnnType = AnnotationType.NotateXpress
            ' internal fast format 

            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress Original (*.NXP)|*.nxp"
        Else
            ' if (annType == "NXXML") 
            so.AnnType = AnnotationType.NotateXpressXml
            ' Standard XML 

            strDefaultImageFilter = "All Files (*.*)|*.*|NotateXpress XML (*.NXXML)|*.nxxml"
        End If

        SaveFileDialog1.FilterIndex = 2
        SaveFileDialog1.FileName = ""
        SaveFileDialog1.Title = "Please save the annotation file"
        SaveFileDialog1.Filter = strDefaultImageFilter

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            NotateXpress1.Layers.SaveToFile(SaveFileDialog1.FileName, so)
        End If
    End Sub


    Private Sub toolStripMenuItem4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem22.Click
        saveAnnotations("NXP")
    End Sub

    Private Sub toolStripMenuItem5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem23.Click
        saveAnnotations("NXXML")
    End Sub

    Private Sub toolStripMenuItem6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem24.Click
        saveAnnotations("XFDF")
    End Sub


    ' Create annotations 
    Private Sub rectangleToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem35.Click
        Dim myRect As RectangleTool

        myRect = New RectangleTool()
        myRect.BoundingRectangle = New Rectangle(100, 30, 500, 245)
        myRect.BackStyle = BackStyle.Translucent
        myRect.FillColor = Color.Yellow
        myRect.ToolTipText = "Thanks for hovering"

        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myRect)
    End Sub

    Private Sub stampToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem36.Click
        Dim myStamp As StampTool

        myStamp = New StampTool()
        ' A starting point, autosize will adjust to fit 
        myStamp.BoundingRectangle = New Rectangle(700, 30, 10, 10)
        myStamp.BackStyle = BackStyle.Translucent
        myStamp.BackColor = Color.Yellow
        myStamp.PenWidth = 3
        myStamp.PenColor = Color.Black
        myStamp.Text = ("Complete" & Environment.NewLine) + DateTime.Now.ToString() & " "
        myStamp.TextFont = New Font("Segoe UI", 16, FontStyle.Bold)
        myStamp.TextColor = Color.DarkBlue
        myStamp.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents


        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myStamp)
    End Sub


    Private Sub textToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem37.Click
        Dim myText As TextTool

        myText = New TextTool()
        ' A starting point, autosize will adjust to fit 
        myText.BoundingRectangle = New Rectangle(450, 1100, 10, 10)
        myText.BackStyle = BackStyle.Opaque
        myText.BackColor = Color.White
        myText.TextColor = Color.Blue
        myText.TextFont = New Font("Arial Unicode MS", 16, FontStyle.Regular)
        myText.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents
        myText.TextJustification = TextJustification.Center
        myText.PenWidth = 2
        myText.PenColor = Color.DarkBlue

        myText.Text = (("The current date and time is: " & Environment.NewLine) + DateTime.Now.ToString() + Environment.NewLine & "Unicode is fully supported: ") + Environment.NewLine & "שָׁלוֹם"


        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myText)
    End Sub


    Private Sub rulerToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem38.Click
        Dim myRuler As RulerTool

        myRuler = New RulerTool()
        myRuler.BoundingRectangle = New Rectangle(200, 550, 900, 0)
        myRuler.ShowAbbreviation = False
        myRuler.MeasurementUnit = MeasurementUnit.Millimeters
        myRuler.PenWidth = 3


        ' we can use Layers[0], as we only keep one layer. 
        NotateXpress1.Layers(0).Elements.Add(myRuler)
    End Sub

    Private Sub aboutNotateXpressToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem40.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub aboutImagXpressToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem41.Click
        ImagXpress1.AboutBox()
    End Sub
    Private Sub imageToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imageToolStripMenuItem.Click
        Dim myImage As ImageTool
        Dim imageToolImage As ImageX
        Dim layer As Layer

        If (System.IO.File.Exists("Signature48.png")) Then

            layer = NotateXpress1.Layers.Selected()

            myImage = New ImageTool()
            myImage.BoundingRectangle = New Rectangle(350, 1760, 10, 10)
            myImage.AutoSize = Accusoft.NotateXpressSdk.AutoSize.ToContents
            myImage.TransparentColor = Color.White
            myImage.BackStyle = BackStyle.Transparent

            imageToolImage = ImageX.FromFile(ImagXpress1, "Signature48.png")
            Dim dibHandle As IntPtr = imageToolImage.ToHdib(True)

            NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dibHandle
            myImage.DibHandle = dibHandle

            'resource cleanup 
            If imageToolImage IsNot Nothing Then
                imageToolImage.Dispose()
                imageToolImage = Nothing
            End If

            layer.Elements.Add(myImage)
        End If

    End Sub

    Private Sub redactionToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles redactionToolStripMenuItem.Click
        Dim myRedaction As RectangleTool
        Dim layer As Layer

        layer = NotateXpress1.Layers.Selected()

        myRedaction = New RectangleTool()
        myRedaction.BoundingRectangle = New Rectangle(1200, 625, 400, 100)
        myRedaction.BackStyle = BackStyle.Opaque
        myRedaction.FillColor = Color.Black
        myRedaction.PenWidth = 0
        myRedaction.ToolTipText = "Redacted"
        myRedaction.Moveable = False
        myRedaction.Sizeable = False
        myRedaction.Locked = True


        layer.Elements.Add(myRedaction)
    End Sub

#Region "Layers"
    Private Sub createToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles createToolStripMenuItem.Click
        ' Create Layer 
        Dim layer As Layer

        layer = New Layer()
        layer.Toolbar.Visible = True
        NotateXpress1.Layers.Add(layer)
    End Sub


    Private Sub deleteCurrentToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles deleteCurrentToolStripMenuItem.Click
        ' Delete the current layer 
        Dim layer As Layer

        layer = NotateXpress1.Layers.Selected()
        NotateXpress1.Layers.Remove(layer)

        ' make the 1st layer, the selected one 
        If NotateXpress1.Layers.Count > 0 Then
            NotateXpress1.Layers(0).[Select]()
        End If
    End Sub

    Private Sub getLayerNameToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles getLayerNameToolStripMenuItem.Click
        ' show the current layer name 
        Dim layer As Layer

        layer = NotateXpress1.Layers.Selected()

        MessageBox.Show("Layer Name is : " & layer.Name)
    End Sub

    Private Sub setLayerNameToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles setLayerNameToolStripMenuItem.Click
        ' enter a layer name, displaying the old name 
        Dim layer As Layer
        Dim layerName As [String]

        Try
            layer = NotateXpress1.Layers.Selected()
            layerName = layer.Name
            layerName = Microsoft.VisualBasic.Interaction.InputBox("Specify Name of Layer", "NotateXpress Sample", layerName, 100, 100)
            layer.Name = layerName
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub toolStripMenuItem8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem26.Click
        ' Hide layer #1 
        Try
            NotateXpress1.Layers(0).Visible = False
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem9_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem27.Click
        ' Hide layer #2 
        Try
            NotateXpress1.Layers(1).Visible = False
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem10_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem28.Click
        ' Hide layer #3 
        Try
            NotateXpress1.Layers(2).Visible = False
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem11_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem29.Click
        ' Show layer #1 
        Try
            NotateXpress1.Layers(0).Visible = True
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem12_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem30.Click
        ' Show layer #2 
        Try
            NotateXpress1.Layers(1).Visible = True
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem13_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem31.Click
        ' Show layer #3 
        Try
            NotateXpress1.Layers(2).Visible = True
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem14_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem32.Click
        ' change to layer #1 
        Try
            NotateXpress1.Layers(0).[Select]()
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem15_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem33.Click
        ' change to layer #2 
        Try
            NotateXpress1.Layers(1).[Select]()
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub toolStripMenuItem16_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolStripMenuItem34.Click
        ' change to layer #3 
        Try
            NotateXpress1.Layers(2).[Select]()
        Catch ex As System.ArgumentOutOfRangeException
            MessageBox.Show("Error: Layer does not exist.")
        End Try
    End Sub

    Private Sub getOfLayersToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles getOfLayersToolStripMenuItem.Click
        MessageBox.Show("Layer Count: " & NotateXpress1.Layers.Count.ToString())
    End Sub

#End Region

#Region "Display"
    Private Sub lineScalingToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lineScalingToolStripMenuItem.Click
        If NotateXpress1.LineScaling = LineScaling.Normal Then
            NotateXpress1.LineScaling = LineScaling.ResolutionY
            lineScalingToolStripMenuItem.Checked = True
        Else
            NotateXpress1.LineScaling = LineScaling.Normal
            lineScalingToolStripMenuItem.Checked = False
        End If
    End Sub


    Private Sub fontScalingToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles fontScalingToolStripMenuItem.Click
        If NotateXpress1.FontScaling = FontScaling.Normal Then
            NotateXpress1.FontScaling = FontScaling.ResolutionY
            fontScalingToolStripMenuItem.Checked = True
        Else
            NotateXpress1.FontScaling = FontScaling.Normal
            fontScalingToolStripMenuItem.Checked = False

        End If
    End Sub
    Private Sub editModeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles editModeToolStripMenuItem.Click
        ' Edit mode allows users to change annotations using the built in controls. 
        NotateXpress1.InteractMode = AnnotationMode.Edit
    End Sub


    Private Sub interactiveModeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles interactiveModeToolStripMenuItem.Click
        ' InteractMode mode is used in a viewing situation. No automatic editing is allowed. 
        NotateXpress1.InteractMode = AnnotationMode.Interactive
    End Sub

    Private Sub useMLEForTextEntryToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles useMLEForTextEntryToolStripMenuItem.Click

        NotateXpress1.MultiLineEdit = Not NotateXpress1.MultiLineEdit
        useMLEForTextEntryToolStripMenuItem.Checked = NotateXpress1.MultiLineEdit
    End Sub


    Private Sub rotate90ClockwiseToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rotate90ClockwiseToolStripMenuItem.Click
        'resource cleanup 
        If Processor1 IsNot Nothing Then
            If Processor1.Image IsNot Nothing Then
                Processor1.Image.Dispose()
                Processor1.Image = Nothing
            End If
        End If

        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(270)

        Processor1.Image = Nothing
    End Sub

    Private Sub rotate90CounterClockwiseToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rotate90CounterClockwiseToolStripMenuItem.Click
        'resource cleanup 
        If Processor1 IsNot Nothing Then
            If Processor1.Image IsNot Nothing Then
                Processor1.Image.Dispose()
                Processor1.Image = Nothing
            End If
        End If

        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(90)


        Processor1.Image = Nothing
    End Sub
#End Region

#Region "SelectedChange"

    Private Sub selectedToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles selectedToolStripMenuItem.Click
        ' edit only the single selected annotation on this layer 
    End Sub

    Private Sub selectedToolStripMenuItem_DropDownOpening(ByVal sender As Object, ByVal e As EventArgs) Handles selectedToolStripMenuItem.DropDownOpening
        ' start by turning off all choices 
        For Each menuItem As ToolStripMenuItem In selectedToolStripMenuItem.DropDownItems
            menuItem.Enabled = False

        Next
    End Sub

    Private Sub selectedToolStripMenuItem_DropDownOpened(ByVal sender As Object, ByVal e As EventArgs) Handles selectedToolStripMenuItem.DropDownOpened
        Dim currentLayer As Layer
        Dim firstElement As Element
        Dim propertyExists As System.Reflection.PropertyInfo


        ' Select what menu items can be enabled for the currently selected item 

        ' Note: 
        ' A layer must exist and be selected 
        ' An annotation must exist and be selected 
        ' Only one annotation must be selected 

        currentLayer = NotateXpress1.Layers.Selected()
        If currentLayer Is Nothing Then
            Exit Sub
        End If

        firstElement = currentLayer.Elements.FirstSelected()
        If firstElement Is Nothing Then
            Exit Sub
        End If

        If currentLayer.Elements.NextSelected() IsNot Nothing Then
            Exit Sub
        End If


        ' Start enabling valid menu items, by checking if the class supports the property/method 

        propertyExists = firstElement.[GetType]().GetProperty("BackColor")
        If propertyExists Is Nothing Then
            ' try fill color, they are both very close in functionality 
            propertyExists = firstElement.[GetType]().GetProperty("FillColor")
        End If
        backgroundColorToolStripMenuItem.Enabled = (propertyExists IsNot Nothing)

        penColorToolStripMenuItem.Enabled = (firstElement.[GetType]().GetProperty("PenColor") IsNot Nothing)

        penWidthToolStripMenuItem.Enabled = (firstElement.[GetType]().GetProperty("PenWidth") IsNot Nothing)


        fontToolStripMenuItem.Enabled = (firstElement.[GetType]().GetProperty("TextFont") IsNot Nothing)
    End Sub


    Private Sub backgroundColorToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles backgroundColorToolStripMenuItem.Click
        ' Set the back color / fill color 
        Dim currentLayer As Layer
        Dim firstElement As Element
        Dim colorChooser As ColorDialog
        Dim isBackColor As [Boolean]

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                firstElement = currentLayer.Elements.FirstSelected()
                If firstElement IsNot Nothing Then
                    ' we go through reflection, since we don't know what type the element is. (rect, image, etc) 
                    isBackColor = (firstElement.[GetType]().GetProperty("BackColor") IsNot Nothing)
                    colorChooser = New ColorDialog()
                    If isBackColor Then
                        colorChooser.Color = DirectCast(firstElement.[GetType]().GetProperty("BackColor").GetValue(firstElement, Nothing), Color)
                    Else
                        colorChooser.Color = DirectCast(firstElement.[GetType]().GetProperty("FillColor").GetValue(firstElement, Nothing), Color)
                    End If

                    If colorChooser.ShowDialog() = DialogResult.OK Then


                        If isBackColor Then
                            ' should be back color, if not fill color. Unless safety check failed. 
                            firstElement.[GetType]().GetProperty("BackColor").SetValue(firstElement, colorChooser.Color, Nothing)
                        Else
                            ' fill color 
                            firstElement.[GetType]().GetProperty("FillColor").SetValue(firstElement, colorChooser.Color, Nothing)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)


        End Try
    End Sub


    Private Sub penColorToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles penColorToolStripMenuItem.Click
        ' Set the pen color 
        Dim currentLayer As Layer
        Dim firstElement As Element
        Dim colorChooser As ColorDialog

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                firstElement = currentLayer.Elements.FirstSelected()
                If firstElement IsNot Nothing Then
                    colorChooser = New ColorDialog()
                    colorChooser.Color = DirectCast(firstElement.[GetType]().GetProperty("PenColor").GetValue(firstElement, Nothing), Color)
                    If colorChooser.ShowDialog() = DialogResult.OK Then
                        ' we jump through reflection, since we don't know what type the element is. (rect, image, etc) 
                        firstElement.[GetType]().GetProperty("PenColor").SetValue(firstElement, colorChooser.Color, Nothing)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub penWidthToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles penWidthToolStripMenuItem.Click
        ' Set the pen width (line width) 
        Dim currentLayer As Layer
        Dim firstElement As Element
        Dim penWidth As String
        Dim penWidthVal As Integer

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                firstElement = currentLayer.Elements.FirstSelected()
                If firstElement IsNot Nothing Then
                    ' we go through reflection, since we don't know what type the element is. (rect, image, etc) 
                    penWidth = firstElement.[GetType]().GetProperty("PenWidth").GetValue(firstElement, Nothing).ToString()
                    penWidth = Microsoft.VisualBasic.Interaction.InputBox("Specify the pen width", "NotateXpress Sample", penWidth, 100, 100)
                    Integer.TryParse(penWidth, penWidthVal)
                    firstElement.[GetType]().GetProperty("PenWidth").SetValue(firstElement, penWidthVal, Nothing)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub fontToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles fontToolStripMenuItem.Click
        ' Set the font 
        Dim currentLayer As Layer
        Dim firstElement As Element
        Dim fontChooser As FontDialog

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                firstElement = currentLayer.Elements.FirstSelected()
                If firstElement IsNot Nothing Then
                    fontChooser = New FontDialog()
                    fontChooser.Font = DirectCast(firstElement.[GetType]().GetProperty("TextFont").GetValue(firstElement, Nothing), Font)
                    If fontChooser.ShowDialog() = DialogResult.OK Then
                        ' we jump through reflection, since we don't know what type the element is. (rect, image, etc) 
                        firstElement.[GetType]().GetProperty("TextFont").SetValue(firstElement, fontChooser.Font, Nothing)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

#End Region
#Region "AllChange"


    Private Sub allToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles allToolStripMenuItem.Click
        ' change all annotations on this layer 
    End Sub

    Private Sub allToolStripMenuItem_DropDownOpened(ByVal sender As Object, ByVal e As EventArgs) Handles allToolStripMenuItem.DropDownOpened
        Dim currentLayer As Layer
        Dim propertyExists As System.Reflection.PropertyInfo


        ' Select what menu items can be enabled for the currently selected item 


        ' Note: 
        ' A layer must exist and be selected 
        ' Annotations must exist on the current layer 

        currentLayer = NotateXpress1.Layers.Selected()
        If currentLayer Is Nothing Then
            Exit Sub
        End If

        If currentLayer.Elements.Count <= 0 Then
            Exit Sub
        End If


        ' Start enabling valid menu items, by checking if the class supports the property/method 
        For Each element As Element In currentLayer.Elements

            If Not backgroundColorToolStripMenuItem1.Enabled Then
                propertyExists = element.[GetType]().GetProperty("BackColor")
                If propertyExists Is Nothing Then
                    ' try fill color, they are both very close in functionality 
                    propertyExists = element.[GetType]().GetProperty("FillColor")
                End If
                backgroundColorToolStripMenuItem1.Enabled = (propertyExists IsNot Nothing)
            End If

            If Not penColorToolStripMenuItem1.Enabled Then
                penColorToolStripMenuItem1.Enabled = (element.[GetType]().GetProperty("PenColor") IsNot Nothing)
            End If


            If Not penWidthToolStripMenuItem1.Enabled Then
                penWidthToolStripMenuItem1.Enabled = (element.[GetType]().GetProperty("PenWidth") IsNot Nothing)
            End If

            If Not fontToolStripMenuItem1.Enabled Then
                fontToolStripMenuItem1.Enabled = (element.[GetType]().GetProperty("TextFont") IsNot Nothing)

            End If

        Next
    End Sub

    Private Sub allToolStripMenuItem_DropDownOpening(ByVal sender As Object, ByVal e As EventArgs) Handles allToolStripMenuItem.DropDownOpening
        ' start by turning off all choices 
        For Each menuItem As ToolStripMenuItem In allToolStripMenuItem.DropDownItems
            menuItem.Enabled = False

        Next
    End Sub

    Private Sub backgroundColorToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles backgroundColorToolStripMenuItem1.Click
        ' Set the back (or fill) color for all elements on this layer 
        Dim currentLayer As Layer
        Dim colorChooser As ColorDialog

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                ' we go through reflection, since we don't know what type the element is. (rect, image, etc) 
                colorChooser = New ColorDialog()
                If colorChooser.ShowDialog() = DialogResult.OK Then

                    For Each element As Element In currentLayer.Elements

                        If element.[GetType]().GetProperty("BackColor") IsNot Nothing Then
                            element.[GetType]().GetProperty("BackColor").SetValue(element, colorChooser.Color, Nothing)
                        End If
                        If element.[GetType]().GetProperty("FillColor") IsNot Nothing Then
                            element.[GetType]().GetProperty("FillColor").SetValue(element, colorChooser.Color, Nothing)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub penColorToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles penColorToolStripMenuItem1.Click
        ' Set the pen color for all elements on this layer 
        Dim currentLayer As Layer
        Dim colorChooser As ColorDialog

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                ' we go through reflection, since we don't know what type the element is. (rect, image, etc) 
                colorChooser = New ColorDialog()
                If colorChooser.ShowDialog() = DialogResult.OK Then

                    For Each element As Element In currentLayer.Elements

                        If element.[GetType]().GetProperty("PenColor") IsNot Nothing Then
                            element.[GetType]().GetProperty("PenColor").SetValue(element, colorChooser.Color, Nothing)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)



        End Try
    End Sub

    Private Sub penWidthToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles penWidthToolStripMenuItem1.Click
        ' Set the pen width for all elements on this layer 
        Dim currentLayer As Layer
        Dim penWidth As String
        Dim penWidthVal As Integer

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                ' we go through reflection, since we don't know what type the element is. (rect, image, etc) 
                penWidth = "1"
                penWidth = Microsoft.VisualBasic.Interaction.InputBox("Specify the pen width", "NotateXpress Sample", penWidth, 100, 100)
                Integer.TryParse(penWidth, penWidthVal)


                For Each element As Element In currentLayer.Elements

                    If element.[GetType]().GetProperty("PenWidth") IsNot Nothing Then
                        element.[GetType]().GetProperty("PenWidth").SetValue(element, penWidthVal, Nothing)
                    End If
                Next
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub fontToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles fontToolStripMenuItem1.Click
        ' Set the font for all elements on this layer 
        Dim currentLayer As Layer
        Dim fontChooser As FontDialog

        Try
            currentLayer = NotateXpress1.Layers.Selected()
            If currentLayer IsNot Nothing Then
                ' we go through reflection, since we don't know what type the element is. (rect, image, etc) 
                fontChooser = New FontDialog()
                If fontChooser.ShowDialog() = DialogResult.OK Then

                    For Each element As Element In currentLayer.Elements

                        If element.[GetType]().GetProperty("TextFont") IsNot Nothing Then
                            element.[GetType]().GetProperty("TextFont").SetValue(element, fontChooser.Font, Nothing)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub
#End Region

#Region "ContextMenu"

    Private Sub enabledToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles enabledToolStripMenuItem.Click
        Dim isEnabled As [Boolean]

        isEnabled = NotateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool)
        isEnabled = Not isEnabled
        NotateXpress1.MenuSetEnabled(MenuType.Context, AnnotationTool.NoTool, isEnabled)

        enabledToolStripMenuItem.Checked = isEnabled
    End Sub

    Private Sub addCustomTextToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addCustomTextToolStripMenuItem.Click
        ' Enter a custom menu name, displaying the old name if one exists 

        ' To see when the new items are fired, please see the MenuSelect event. 

        ' If you want to create your own custom menus outside of NotateXpress, Turn off the context menu and 
        ' NotateXpress will fire a ContextMenu event, when you should launch your own menuing system. 
        Dim customName As String

        Try
            ' 20000 is the starting number for custom menu items. 
            customName = ""
            customName = Microsoft.VisualBasic.Interaction.InputBox("Please enter a custom menu item", "NotateXpress Sample", customName, 100, 100)



            NotateXpress1.MenuAddItem(MenuType.Context, AnnotationTool.NoTool, System.Math.Max(System.Threading.Interlocked.Increment(CustomMenuCounter), CustomMenuCounter - 1), 0, customName, 0, _
            0)
        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

#End Region
#Region "Toolbars"
    Private Sub notateXpressToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notateXpressToolStripMenuItem.Click
        ' reminder: NotateXpress Toolbars are configured per layer 
        Dim currentLayer As Layer

        currentLayer = NotateXpress1.Layers.Selected()
        currentLayer.Toolbar.Visible = Not currentLayer.Toolbar.Visible
    End Sub


    Private Sub imagXpressToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imagXpressToolStripMenuItem.Click
        ImageXView1.Toolbar.Visible = Not ImageXView1.Toolbar.Visible
    End Sub

    Private Sub toolbarToolStripMenuItem_DropDownOpening(ByVal sender As Object, ByVal e As EventArgs) Handles toolbarToolStripMenuItem.DropDownOpening
        Dim currentLayer As Layer

        currentLayer = NotateXpress1.Layers.Selected()

        notateXpressToolStripMenuItem.Checked = currentLayer.Toolbar.Visible

        imagXpressToolStripMenuItem.Checked = ImageXView1.Toolbar.Visible
    End Sub

    Private Sub smallToolbarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles smallToolbarToolStripMenuItem.Click
        NotateXpress1.ToolbarDefaults.ToolbarLargeIcons = False
    End Sub

    Private Sub largeToolbarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles largeToolbarToolStripMenuItem.Click
        NotateXpress1.ToolbarDefaults.ToolbarLargeIcons = True
    End Sub



    Private Sub buttonToolToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonToolToolStripMenuItem.Click
        Dim currentLayer As Layer
        Dim isVisible As [Boolean]

        currentLayer = NotateXpress1.Layers.Selected()
        isVisible = currentLayer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool)
        isVisible = Not isVisible
        currentLayer.Toolbar.SetToolVisible(AnnotationTool.ButtonTool, isVisible)


        buttonToolToolStripMenuItem.Checked = isVisible
    End Sub

    Private Sub enableNoteToolToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles enableNoteToolToolStripMenuItem.Click

        Dim currentLayer As Layer
        Dim isEnabled As [Boolean]

        currentLayer = NotateXpress1.Layers.Selected()
        isEnabled = currentLayer.Toolbar.GetToolEnabled(AnnotationTool.NoteTool)
        isEnabled = Not isEnabled
        currentLayer.Toolbar.SetToolEnabled(AnnotationTool.NoteTool, isEnabled)


        enableNoteToolToolStripMenuItem.Checked = isEnabled
    End Sub
#End Region

  
End Class

﻿namespace ExtendedCSharpAnnotations
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            // Critical to dispose objects


            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new Accusoft.ImagXpressSdk.ImagXpress(this.components);
            this.imageXView1 = new Accusoft.ImagXpressSdk.ImageXView(this.components);
            this.notateXpress1 = new Accusoft.NotateXpressSdk.NotateXpress(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wangKodakAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressNXPAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openAnnotationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressNXPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressXMLNXXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adobePDFXFDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAnnotationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.brandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineScalingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontScalingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.editModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interactiveModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.useMLEForTextEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.rotate90ClockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotate90CounterClockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.layersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCurrentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getLayerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setLayerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.showLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.switchToLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.getOfLayersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createAnnotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectangleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stampToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rulerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutNotateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutImagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.processor1 = new Accusoft.ImagXpressSdk.Processor(this.components);
            this.selectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penWidthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundColorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.penColorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.penWidthToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fontToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enabledToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addCustomTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notateXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagXpressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallToolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.largeToolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableNoteToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.Location = new System.Drawing.Point(75, 61);
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(869, 618);
            this.imageXView1.TabIndex = 0;
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = Accusoft.NotateXpressSdk.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = Accusoft.NotateXpressSdk.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = Accusoft.NotateXpressSdk.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            this.notateXpress1.Stamping += new Accusoft.NotateXpressSdk.NotateXpress.StampingEventHandler(this.notateXpress1_Stamping);
            this.notateXpress1.ToolbarSelect += new Accusoft.NotateXpressSdk.NotateXpress.ToolbarEventHandler(this.notateXpress1_ToolbarSelect);
            this.notateXpress1.AnnotationAdded += new Accusoft.NotateXpressSdk.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 710);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.displayToolStripMenuItem,
            this.layersToolStripMenuItem,
            this.createAnnotationsToolStripMenuItem,
            this.selectedToolStripMenuItem,
            this.allToolStripMenuItem,
            this.contextMenuToolStripMenuItem,
            this.toolbarToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.saveImageToolStripMenuItem,
            this.toolStripSeparator1,
            this.openAnnotationFileToolStripMenuItem,
            this.saveAnnotationFileToolStripMenuItem,
            this.brandToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noAnnotationsToolStripMenuItem,
            this.wangKodakAnnotationsToolStripMenuItem,
            this.notateXpressNXPAnnotationsToolStripMenuItem});
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.openImageToolStripMenuItem.Text = "&Open Image";
            // 
            // noAnnotationsToolStripMenuItem
            // 
            this.noAnnotationsToolStripMenuItem.Name = "noAnnotationsToolStripMenuItem";
            this.noAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.noAnnotationsToolStripMenuItem.Text = "No Annotations";
            this.noAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.noAnnotationsToolStripMenuItem_Click);
            // 
            // wangKodakAnnotationsToolStripMenuItem
            // 
            this.wangKodakAnnotationsToolStripMenuItem.Name = "wangKodakAnnotationsToolStripMenuItem";
            this.wangKodakAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.wangKodakAnnotationsToolStripMenuItem.Text = "Embedded Wang / Kodak Annotations";
            this.wangKodakAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.wangKodakAnnotationsToolStripMenuItem_Click);
            // 
            // notateXpressNXPAnnotationsToolStripMenuItem
            // 
            this.notateXpressNXPAnnotationsToolStripMenuItem.Name = "notateXpressNXPAnnotationsToolStripMenuItem";
            this.notateXpressNXPAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.notateXpressNXPAnnotationsToolStripMenuItem.Text = "Embedded NotateXpress Annotations";
            this.notateXpressNXPAnnotationsToolStripMenuItem.Click += new System.EventHandler(this.notateXpressNXPAnnotationsToolStripMenuItem_Click);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3});
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.saveImageToolStripMenuItem.Text = "&Save Image";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(217, 22);
            this.toolStripMenuItem1.Text = "No Annotations";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(217, 22);
            this.toolStripMenuItem2.Text = "Wang / Kodak Annotations";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(217, 22);
            this.toolStripMenuItem3.Text = "NotateXpress Annotations";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(203, 6);
            // 
            // openAnnotationFileToolStripMenuItem
            // 
            this.openAnnotationFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notateXpressNXPToolStripMenuItem,
            this.notateXpressXMLNXXMLToolStripMenuItem,
            this.adobePDFXFDFToolStripMenuItem});
            this.openAnnotationFileToolStripMenuItem.Name = "openAnnotationFileToolStripMenuItem";
            this.openAnnotationFileToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.openAnnotationFileToolStripMenuItem.Text = "Open Annotation File";
            // 
            // notateXpressNXPToolStripMenuItem
            // 
            this.notateXpressNXPToolStripMenuItem.Name = "notateXpressNXPToolStripMenuItem";
            this.notateXpressNXPToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.notateXpressNXPToolStripMenuItem.Text = "NotateXpress (NXP)";
            this.notateXpressNXPToolStripMenuItem.Click += new System.EventHandler(this.notateXpressNXPToolStripMenuItem_Click);
            // 
            // notateXpressXMLNXXMLToolStripMenuItem
            // 
            this.notateXpressXMLNXXMLToolStripMenuItem.Name = "notateXpressXMLNXXMLToolStripMenuItem";
            this.notateXpressXMLNXXMLToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.notateXpressXMLNXXMLToolStripMenuItem.Text = "NotateXpress XML (NXXML)";
            this.notateXpressXMLNXXMLToolStripMenuItem.Click += new System.EventHandler(this.notateXpressXMLNXXMLToolStripMenuItem_Click);
            // 
            // adobePDFXFDFToolStripMenuItem
            // 
            this.adobePDFXFDFToolStripMenuItem.Name = "adobePDFXFDFToolStripMenuItem";
            this.adobePDFXFDFToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.adobePDFXFDFToolStripMenuItem.Text = "Adobe PDF (XFDF)";
            this.adobePDFXFDFToolStripMenuItem.Click += new System.EventHandler(this.adobePDFXFDFToolStripMenuItem_Click);
            // 
            // saveAnnotationFileToolStripMenuItem
            // 
            this.saveAnnotationFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.saveAnnotationFileToolStripMenuItem.Name = "saveAnnotationFileToolStripMenuItem";
            this.saveAnnotationFileToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.saveAnnotationFileToolStripMenuItem.Text = "Save Annotation File";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItem4.Text = "NotateXpress (NXP)";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItem5.Text = "NotateXpress XML (NXXML)";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItem6.Text = "Adobe PDF (XFDF)";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // brandToolStripMenuItem
            // 
            this.brandToolStripMenuItem.Name = "brandToolStripMenuItem";
            this.brandToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.brandToolStripMenuItem.Text = "&Brand (Make Permanent)";
            this.brandToolStripMenuItem.Click += new System.EventHandler(this.brandToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lineScalingToolStripMenuItem,
            this.fontScalingToolStripMenuItem,
            this.toolStripSeparator2,
            this.editModeToolStripMenuItem,
            this.interactiveModeToolStripMenuItem,
            this.toolStripSeparator3,
            this.useMLEForTextEntryToolStripMenuItem,
            this.toolStripSeparator4,
            this.rotate90ClockwiseToolStripMenuItem,
            this.rotate90CounterClockwiseToolStripMenuItem});
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.displayToolStripMenuItem.Text = "&Display";
            // 
            // lineScalingToolStripMenuItem
            // 
            this.lineScalingToolStripMenuItem.Name = "lineScalingToolStripMenuItem";
            this.lineScalingToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.lineScalingToolStripMenuItem.Text = "Line Scaling";
            this.lineScalingToolStripMenuItem.Click += new System.EventHandler(this.lineScalingToolStripMenuItem_Click);
            // 
            // fontScalingToolStripMenuItem
            // 
            this.fontScalingToolStripMenuItem.Name = "fontScalingToolStripMenuItem";
            this.fontScalingToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.fontScalingToolStripMenuItem.Text = "Font Scaling";
            this.fontScalingToolStripMenuItem.Click += new System.EventHandler(this.fontScalingToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(218, 6);
            // 
            // editModeToolStripMenuItem
            // 
            this.editModeToolStripMenuItem.Name = "editModeToolStripMenuItem";
            this.editModeToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.editModeToolStripMenuItem.Text = "Edit Mode";
            this.editModeToolStripMenuItem.Click += new System.EventHandler(this.editModeToolStripMenuItem_Click);
            // 
            // interactiveModeToolStripMenuItem
            // 
            this.interactiveModeToolStripMenuItem.Name = "interactiveModeToolStripMenuItem";
            this.interactiveModeToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.interactiveModeToolStripMenuItem.Text = "Interactive Mode";
            this.interactiveModeToolStripMenuItem.Click += new System.EventHandler(this.interactiveModeToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(218, 6);
            // 
            // useMLEForTextEntryToolStripMenuItem
            // 
            this.useMLEForTextEntryToolStripMenuItem.Name = "useMLEForTextEntryToolStripMenuItem";
            this.useMLEForTextEntryToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.useMLEForTextEntryToolStripMenuItem.Text = "Use MLE for Text Entry";
            this.useMLEForTextEntryToolStripMenuItem.Click += new System.EventHandler(this.useMLEForTextEntryToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(218, 6);
            // 
            // rotate90ClockwiseToolStripMenuItem
            // 
            this.rotate90ClockwiseToolStripMenuItem.Name = "rotate90ClockwiseToolStripMenuItem";
            this.rotate90ClockwiseToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.rotate90ClockwiseToolStripMenuItem.Text = "Rotate 90 clockwise";
            this.rotate90ClockwiseToolStripMenuItem.Click += new System.EventHandler(this.rotate90ClockwiseToolStripMenuItem_Click);
            // 
            // rotate90CounterClockwiseToolStripMenuItem
            // 
            this.rotate90CounterClockwiseToolStripMenuItem.Name = "rotate90CounterClockwiseToolStripMenuItem";
            this.rotate90CounterClockwiseToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.rotate90CounterClockwiseToolStripMenuItem.Text = "Rotate 90 counter clockwise";
            this.rotate90CounterClockwiseToolStripMenuItem.Click += new System.EventHandler(this.rotate90CounterClockwiseToolStripMenuItem_Click);
            // 
            // layersToolStripMenuItem
            // 
            this.layersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.deleteCurrentToolStripMenuItem,
            this.getLayerNameToolStripMenuItem,
            this.setLayerNameToolStripMenuItem,
            this.hideLayerToolStripMenuItem,
            this.showLayerToolStripMenuItem,
            this.switchToLayerToolStripMenuItem,
            this.getOfLayersToolStripMenuItem});
            this.layersToolStripMenuItem.Name = "layersToolStripMenuItem";
            this.layersToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.layersToolStripMenuItem.Text = "&Layers";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.createToolStripMenuItem.Text = "Create";
            this.createToolStripMenuItem.Click += new System.EventHandler(this.createToolStripMenuItem_Click);
            // 
            // deleteCurrentToolStripMenuItem
            // 
            this.deleteCurrentToolStripMenuItem.Name = "deleteCurrentToolStripMenuItem";
            this.deleteCurrentToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.deleteCurrentToolStripMenuItem.Text = "Delete Current";
            this.deleteCurrentToolStripMenuItem.Click += new System.EventHandler(this.deleteCurrentToolStripMenuItem_Click);
            // 
            // getLayerNameToolStripMenuItem
            // 
            this.getLayerNameToolStripMenuItem.Name = "getLayerNameToolStripMenuItem";
            this.getLayerNameToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.getLayerNameToolStripMenuItem.Text = "Get Layer Name";
            this.getLayerNameToolStripMenuItem.Click += new System.EventHandler(this.getLayerNameToolStripMenuItem_Click);
            // 
            // setLayerNameToolStripMenuItem
            // 
            this.setLayerNameToolStripMenuItem.Name = "setLayerNameToolStripMenuItem";
            this.setLayerNameToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.setLayerNameToolStripMenuItem.Text = "Set Layer Name";
            this.setLayerNameToolStripMenuItem.Click += new System.EventHandler(this.setLayerNameToolStripMenuItem_Click);
            // 
            // hideLayerToolStripMenuItem
            // 
            this.hideLayerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.hideLayerToolStripMenuItem.Name = "hideLayerToolStripMenuItem";
            this.hideLayerToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.hideLayerToolStripMenuItem.Text = "Hide Layer";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(108, 22);
            this.toolStripMenuItem8.Text = "Hide 1";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(108, 22);
            this.toolStripMenuItem9.Text = "Hide 2";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(108, 22);
            this.toolStripMenuItem10.Text = "Hide 3";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // showLayerToolStripMenuItem
            // 
            this.showLayerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem13});
            this.showLayerToolStripMenuItem.Name = "showLayerToolStripMenuItem";
            this.showLayerToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.showLayerToolStripMenuItem.Text = "Show Layer";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItem11.Text = "Show 1";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItem12.Text = "Show 2";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItem13.Text = "Show 3";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // switchToLayerToolStripMenuItem
            // 
            this.switchToLayerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem14,
            this.toolStripMenuItem15,
            this.toolStripMenuItem16});
            this.switchToLayerToolStripMenuItem.Name = "switchToLayerToolStripMenuItem";
            this.switchToLayerToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.switchToLayerToolStripMenuItem.Text = "Switch to Layer";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(118, 22);
            this.toolStripMenuItem14.Text = "Switch 1";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(118, 22);
            this.toolStripMenuItem15.Text = "Switch 2";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(118, 22);
            this.toolStripMenuItem16.Text = "Switch 3";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
            // 
            // getOfLayersToolStripMenuItem
            // 
            this.getOfLayersToolStripMenuItem.Name = "getOfLayersToolStripMenuItem";
            this.getOfLayersToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.getOfLayersToolStripMenuItem.Text = "Get # of Layers";
            this.getOfLayersToolStripMenuItem.Click += new System.EventHandler(this.getOfLayersToolStripMenuItem_Click);
            // 
            // createAnnotationsToolStripMenuItem
            // 
            this.createAnnotationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rectangleToolStripMenuItem,
            this.stampToolStripMenuItem,
            this.textToolStripMenuItem,
            this.rulerToolStripMenuItem,
            this.imageToolStripMenuItem,
            this.redactionToolStripMenuItem});
            this.createAnnotationsToolStripMenuItem.Name = "createAnnotationsToolStripMenuItem";
            this.createAnnotationsToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.createAnnotationsToolStripMenuItem.Text = "&Create Annotations";
            // 
            // rectangleToolStripMenuItem
            // 
            this.rectangleToolStripMenuItem.Name = "rectangleToolStripMenuItem";
            this.rectangleToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.rectangleToolStripMenuItem.Text = "Rectangle";
            this.rectangleToolStripMenuItem.Click += new System.EventHandler(this.rectangleToolStripMenuItem_Click);
            // 
            // stampToolStripMenuItem
            // 
            this.stampToolStripMenuItem.Name = "stampToolStripMenuItem";
            this.stampToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stampToolStripMenuItem.Text = "Stamp";
            this.stampToolStripMenuItem.Click += new System.EventHandler(this.stampToolStripMenuItem_Click);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.textToolStripMenuItem.Text = "Text";
            this.textToolStripMenuItem.Click += new System.EventHandler(this.textToolStripMenuItem_Click);
            // 
            // rulerToolStripMenuItem
            // 
            this.rulerToolStripMenuItem.Name = "rulerToolStripMenuItem";
            this.rulerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.rulerToolStripMenuItem.Text = "Ruler";
            this.rulerToolStripMenuItem.Click += new System.EventHandler(this.rulerToolStripMenuItem_Click);
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.imageToolStripMenuItem.Text = "Image";
            this.imageToolStripMenuItem.Click += new System.EventHandler(this.imageToolStripMenuItem_Click);
            // 
            // redactionToolStripMenuItem
            // 
            this.redactionToolStripMenuItem.Name = "redactionToolStripMenuItem";
            this.redactionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.redactionToolStripMenuItem.Text = "Redaction";
            this.redactionToolStripMenuItem.Click += new System.EventHandler(this.redactionToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutNotateXpressToolStripMenuItem,
            this.aboutImagXpressToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutNotateXpressToolStripMenuItem
            // 
            this.aboutNotateXpressToolStripMenuItem.Name = "aboutNotateXpressToolStripMenuItem";
            this.aboutNotateXpressToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutNotateXpressToolStripMenuItem.Text = "About NotateXpress";
            this.aboutNotateXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutNotateXpressToolStripMenuItem_Click);
            // 
            // aboutImagXpressToolStripMenuItem
            // 
            this.aboutImagXpressToolStripMenuItem.Name = "aboutImagXpressToolStripMenuItem";
            this.aboutImagXpressToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutImagXpressToolStripMenuItem.Text = "About ImagXpress";
            this.aboutImagXpressToolStripMenuItem.Click += new System.EventHandler(this.aboutImagXpressToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // selectedToolStripMenuItem
            // 
            this.selectedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backgroundColorToolStripMenuItem,
            this.penColorToolStripMenuItem,
            this.penWidthToolStripMenuItem,
            this.fontToolStripMenuItem});
            this.selectedToolStripMenuItem.Name = "selectedToolStripMenuItem";
            this.selectedToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.selectedToolStripMenuItem.Text = "&Selected";
            this.selectedToolStripMenuItem.DropDownOpening += new System.EventHandler(this.selectedToolStripMenuItem_DropDownOpening);
            this.selectedToolStripMenuItem.DropDownOpened += new System.EventHandler(this.selectedToolStripMenuItem_DropDownOpened);
            this.selectedToolStripMenuItem.Click += new System.EventHandler(this.selectedToolStripMenuItem_Click);
            // 
            // backgroundColorToolStripMenuItem
            // 
            this.backgroundColorToolStripMenuItem.Name = "backgroundColorToolStripMenuItem";
            this.backgroundColorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.backgroundColorToolStripMenuItem.Text = "Background Color";
            this.backgroundColorToolStripMenuItem.Click += new System.EventHandler(this.backgroundColorToolStripMenuItem_Click);
            // 
            // penColorToolStripMenuItem
            // 
            this.penColorToolStripMenuItem.Name = "penColorToolStripMenuItem";
            this.penColorToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.penColorToolStripMenuItem.Text = "Pen Color";
            this.penColorToolStripMenuItem.Click += new System.EventHandler(this.penColorToolStripMenuItem_Click);
            // 
            // penWidthToolStripMenuItem
            // 
            this.penWidthToolStripMenuItem.Name = "penWidthToolStripMenuItem";
            this.penWidthToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.penWidthToolStripMenuItem.Text = "Pen Width";
            this.penWidthToolStripMenuItem.Click += new System.EventHandler(this.penWidthToolStripMenuItem_Click);
            // 
            // fontToolStripMenuItem
            // 
            this.fontToolStripMenuItem.Name = "fontToolStripMenuItem";
            this.fontToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.fontToolStripMenuItem.Text = "Font";
            this.fontToolStripMenuItem.Click += new System.EventHandler(this.fontToolStripMenuItem_Click);
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backgroundColorToolStripMenuItem1,
            this.penColorToolStripMenuItem1,
            this.penWidthToolStripMenuItem1,
            this.fontToolStripMenuItem1});
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.allToolStripMenuItem.Text = "Change &All";
            this.allToolStripMenuItem.DropDownOpening += new System.EventHandler(this.allToolStripMenuItem_DropDownOpening);
            this.allToolStripMenuItem.DropDownOpened += new System.EventHandler(this.allToolStripMenuItem_DropDownOpened);
            this.allToolStripMenuItem.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
            // 
            // backgroundColorToolStripMenuItem1
            // 
            this.backgroundColorToolStripMenuItem1.Name = "backgroundColorToolStripMenuItem1";
            this.backgroundColorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.backgroundColorToolStripMenuItem1.Text = "Background Color";
            this.backgroundColorToolStripMenuItem1.Click += new System.EventHandler(this.backgroundColorToolStripMenuItem1_Click);
            // 
            // penColorToolStripMenuItem1
            // 
            this.penColorToolStripMenuItem1.Name = "penColorToolStripMenuItem1";
            this.penColorToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.penColorToolStripMenuItem1.Text = "Pen Color";
            this.penColorToolStripMenuItem1.Click += new System.EventHandler(this.penColorToolStripMenuItem1_Click);
            // 
            // penWidthToolStripMenuItem1
            // 
            this.penWidthToolStripMenuItem1.Name = "penWidthToolStripMenuItem1";
            this.penWidthToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.penWidthToolStripMenuItem1.Text = "Pen Width";
            this.penWidthToolStripMenuItem1.Click += new System.EventHandler(this.penWidthToolStripMenuItem1_Click);
            // 
            // fontToolStripMenuItem1
            // 
            this.fontToolStripMenuItem1.Name = "fontToolStripMenuItem1";
            this.fontToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.fontToolStripMenuItem1.Text = "Font";
            this.fontToolStripMenuItem1.Click += new System.EventHandler(this.fontToolStripMenuItem1_Click);
            // 
            // contextMenuToolStripMenuItem
            // 
            this.contextMenuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enabledToolStripMenuItem,
            this.addCustomTextToolStripMenuItem});
            this.contextMenuToolStripMenuItem.Name = "contextMenuToolStripMenuItem";
            this.contextMenuToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.contextMenuToolStripMenuItem.Text = "Context &Menu";
            // 
            // enabledToolStripMenuItem
            // 
            this.enabledToolStripMenuItem.Name = "enabledToolStripMenuItem";
            this.enabledToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.enabledToolStripMenuItem.Text = "Enabled";
            this.enabledToolStripMenuItem.Click += new System.EventHandler(this.enabledToolStripMenuItem_Click);
            // 
            // addCustomTextToolStripMenuItem
            // 
            this.addCustomTextToolStripMenuItem.Name = "addCustomTextToolStripMenuItem";
            this.addCustomTextToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.addCustomTextToolStripMenuItem.Text = "Add Custom Text";
            this.addCustomTextToolStripMenuItem.Click += new System.EventHandler(this.addCustomTextToolStripMenuItem_Click);
            // 
            // toolbarToolStripMenuItem
            // 
            this.toolbarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notateXpressToolStripMenuItem,
            this.imagXpressToolStripMenuItem,
            this.smallToolbarToolStripMenuItem,
            this.largeToolbarToolStripMenuItem,
            this.buttonToolToolStripMenuItem,
            this.enableNoteToolToolStripMenuItem});
            this.toolbarToolStripMenuItem.Name = "toolbarToolStripMenuItem";
            this.toolbarToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.toolbarToolStripMenuItem.Text = "&Toolbar";
            this.toolbarToolStripMenuItem.DropDownOpening += new System.EventHandler(this.toolbarToolStripMenuItem_DropDownOpening);
            // 
            // notateXpressToolStripMenuItem
            // 
            this.notateXpressToolStripMenuItem.Name = "notateXpressToolStripMenuItem";
            this.notateXpressToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.notateXpressToolStripMenuItem.Text = "NotateXpress";
            this.notateXpressToolStripMenuItem.Click += new System.EventHandler(this.notateXpressToolStripMenuItem_Click);
            // 
            // imagXpressToolStripMenuItem
            // 
            this.imagXpressToolStripMenuItem.Name = "imagXpressToolStripMenuItem";
            this.imagXpressToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.imagXpressToolStripMenuItem.Text = "ImagXpress";
            this.imagXpressToolStripMenuItem.Click += new System.EventHandler(this.imagXpressToolStripMenuItem_Click);
            // 
            // smallToolbarToolStripMenuItem
            // 
            this.smallToolbarToolStripMenuItem.Name = "smallToolbarToolStripMenuItem";
            this.smallToolbarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.smallToolbarToolStripMenuItem.Text = "Small Toolbar";
            this.smallToolbarToolStripMenuItem.Click += new System.EventHandler(this.smallToolbarToolStripMenuItem_Click);
            // 
            // largeToolbarToolStripMenuItem
            // 
            this.largeToolbarToolStripMenuItem.Name = "largeToolbarToolStripMenuItem";
            this.largeToolbarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.largeToolbarToolStripMenuItem.Text = "Large Toolbar";
            this.largeToolbarToolStripMenuItem.Click += new System.EventHandler(this.largeToolbarToolStripMenuItem_Click);
            // 
            // buttonToolToolStripMenuItem
            // 
            this.buttonToolToolStripMenuItem.Name = "buttonToolToolStripMenuItem";
            this.buttonToolToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.buttonToolToolStripMenuItem.Text = "Show Button Tool";
            this.buttonToolToolStripMenuItem.Click += new System.EventHandler(this.buttonToolToolStripMenuItem_Click);
            // 
            // enableNoteToolToolStripMenuItem
            // 
            this.enableNoteToolToolStripMenuItem.Name = "enableNoteToolToolStripMenuItem";
            this.enableNoteToolToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.enableNoteToolToolStripMenuItem.Text = "Enable Note Tool";
            this.enableNoteToolToolStripMenuItem.Click += new System.EventHandler(this.enableNoteToolToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 732);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.imageXView1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.Text = "Extended C# Annotations";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Accusoft.ImagXpressSdk.ImagXpress imagXpress1;
        private Accusoft.ImagXpressSdk.ImageXView imageXView1;
        private Accusoft.NotateXpressSdk.NotateXpress notateXpress1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem openAnnotationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAnnotationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressNXPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressXMLNXXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adobePDFXFDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wangKodakAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressNXPAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Accusoft.ImagXpressSdk.Processor processor1;
        private System.Windows.Forms.ToolStripMenuItem createAnnotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rectangleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stampToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rulerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutNotateXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutImagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem layersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCurrentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getLayerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setLayerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem showLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem switchToLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem getOfLayersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineScalingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontScalingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem editModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interactiveModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem useMLEForTextEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem rotate90ClockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotate90CounterClockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penWidthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundColorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem penColorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem penWidthToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fontToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contextMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enabledToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addCustomTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolbarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notateXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagXpressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallToolbarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem largeToolbarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buttonToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableNoteToolToolStripMenuItem;
    }
}


'****************************************************************'
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms

Public Class InputDlg
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal strTitle As String)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        Me.Text = strTitle

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

  

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents InputCancel As System.Windows.Forms.Button
    Friend WithEvents IDOK As System.Windows.Forms.Button
    Friend WithEvents textBox1 As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.InputCancel = New System.Windows.Forms.Button
		Me.IDOK = New System.Windows.Forms.Button
		Me.textBox1 = New System.Windows.Forms.TextBox
		Me.SuspendLayout()
		'
		'InputCancel
		'
		Me.InputCancel.Location = New System.Drawing.Point(175, 54)
		Me.InputCancel.Name = "InputCancel"
		Me.InputCancel.Size = New System.Drawing.Size(163, 28)
		Me.InputCancel.TabIndex = 5
		Me.InputCancel.Text = "Cancel"
		'
		'IDOK
		'
		Me.IDOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.IDOK.Location = New System.Drawing.Point(12, 54)
		Me.IDOK.Name = "IDOK"
		Me.IDOK.Size = New System.Drawing.Size(144, 28)
		Me.IDOK.TabIndex = 4
		Me.IDOK.Text = "OK"
		'
		'textBox1
		'
		Me.textBox1.Location = New System.Drawing.Point(12, 17)
		Me.textBox1.Name = "textBox1"
		Me.textBox1.Size = New System.Drawing.Size(326, 22)
		Me.textBox1.TabIndex = 3
		'
		'InputDlg
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(6, 15)
		Me.ClientSize = New System.Drawing.Size(351, 86)
		Me.Controls.Add(Me.InputCancel)
		Me.Controls.Add(Me.IDOK)
		Me.Controls.Add(Me.textBox1)
		Me.Name = "InputDlg"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

    Public Property InputText() As String
        Get
            Return textBox1.Text
        End Get
        Set(ByVal Value As String)
            textBox1.Text = value
        End Set
    End Property

    Private Sub InputDlg_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Me.CenterToParent()
    End Sub

    Private Sub IDOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IDOK.Click
        DialogResult = DialogResult.OK
    End Sub

    Private Sub InputCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InputCancel.Click
        DialogResult = DialogResult.Cancel
    End Sub

    Private Sub InputDlg_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Return Then
            DialogResult = DialogResult.OK
        End If
    End Sub

    Private Sub textBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles textBox1.KeyDown
        If e.KeyCode = Keys.Return Then
            DialogResult = DialogResult.OK
        End If

    End Sub

End Class

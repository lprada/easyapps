/***************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.NotateXpress9;


namespace RulersDemo
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.OpenFileDialog OpenFileDialog;
		private System.Windows.Forms.ListBox ListBox1;
		private System.Windows.Forms.Label Label1;
		private System.Windows.Forms.MenuItem MenuItem7;
		private System.Windows.Forms.MenuItem MenuItem1;
		private System.Windows.Forms.SaveFileDialog SaveFileDialog;
		private System.Windows.Forms.MainMenu MainMenu1;
		internal System.Windows.Forms.GroupBox GroupBox1;
		private System.Windows.Forms.CheckBox ShowAbbreviations;
		internal System.Windows.Forms.CheckBox ShowMeasurement;
		private System.Windows.Forms.CheckBox ShowGauge;
		private System.Windows.Forms.TrackBar RulerPrecision;
		private System.Windows.Forms.Button RulerLengths;
		private System.Windows.Forms.Button ClearList;
		private System.Windows.Forms.MenuItem LoadImageFile;
		private System.Windows.Forms.MenuItem FileExit;
		private System.Windows.Forms.MenuItem LoadAnnotationFile;
		private System.Windows.Forms.MenuItem SaveImageFile;
		private System.Windows.Forms.MenuItem SaveAnnotationFile;
		private System.Windows.Forms.MenuItem CreateRuler;
		internal System.Windows.Forms.RadioButton Centimeters;
		internal System.Windows.Forms.RadioButton Feet;
		internal System.Windows.Forms.RadioButton Inches;
		internal System.Windows.Forms.RadioButton MicroMeters;
		internal System.Windows.Forms.RadioButton Twips;
		internal System.Windows.Forms.RadioButton Kilometers;
		internal System.Windows.Forms.RadioButton Meters;
		internal System.Windows.Forms.RadioButton Millimeters;
		internal System.Windows.Forms.RadioButton Miles;
		internal System.Windows.Forms.RadioButton Pixels;
		private System.Windows.Forms.MenuItem DeleteRulers;
		private PegasusImaging.WinForms.NotateXpress9.SaveOptions m_saveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();
		private System.String strCurrentDir;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        private PegasusImaging.WinForms.NotateXpress9.NotateXpress notateXpress1;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.Label lblerror;
		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private System.Windows.Forms.ListBox lstInfo;

		PegasusImaging.WinForms.ImagXpress9.ImageX imagX1;
		PegasusImaging.WinForms.NotateXpress9.Layer layer;
		PegasusImaging.WinForms.ImagXpress9.LoadOptions loIXLoadOptions;
		PegasusImaging.WinForms.ImagXpress9.SaveOptions soIXSaveOptions;
		PegasusImaging.WinForms.NotateXpress9.SaveOptions soNXSaveOptions;
		PegasusImaging.WinForms.NotateXpress9.LoadOptions soNXLoadOptions;

		string strImageFileName;
		string strNXPFileName;
		private System.String strSaveFileName;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuToolbarShow;
		private IContainer components;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                // Don't forget to dispose NXP
                //                
                if (notateXpress1 != null)
                {
                    notateXpress1.ClientDisconnect();

                    notateXpress1.Dispose();
                    notateXpress1 = null;
                }
                if (imagX1 != null)
                {

                    imagX1.Dispose();
                    imagX1 = null;
                } 
			    // Don't forget to dispose IX
			    //
				if (imageXView1 != null)
				{
					// following line is causing exception on shutdown
					imageXView1.Dispose();
					imageXView1 = null;
				}                
				if (imagXpress1 != null)
			    {
					
					imagXpress1.Dispose();
			        imagXpress1 = null;
                }                    
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.ListBox1 = new System.Windows.Forms.ListBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.DeleteRulers = new System.Windows.Forms.MenuItem();
            this.MenuItem7 = new System.Windows.Forms.MenuItem();
            this.CreateRuler = new System.Windows.Forms.MenuItem();
            this.FileExit = new System.Windows.Forms.MenuItem();
            this.LoadAnnotationFile = new System.Windows.Forms.MenuItem();
            this.LoadImageFile = new System.Windows.Forms.MenuItem();
            this.MenuItem1 = new System.Windows.Forms.MenuItem();
            this.SaveImageFile = new System.Windows.Forms.MenuItem();
            this.SaveAnnotationFile = new System.Windows.Forms.MenuItem();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.RulerPrecision = new System.Windows.Forms.TrackBar();
            this.ShowMeasurement = new System.Windows.Forms.CheckBox();
            this.ShowGauge = new System.Windows.Forms.CheckBox();
            this.Twips = new System.Windows.Forms.RadioButton();
            this.Centimeters = new System.Windows.Forms.RadioButton();
            this.Kilometers = new System.Windows.Forms.RadioButton();
            this.Meters = new System.Windows.Forms.RadioButton();
            this.Millimeters = new System.Windows.Forms.RadioButton();
            this.MainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.mnuToolbar = new System.Windows.Forms.MenuItem();
            this.mnuToolbarShow = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.Miles = new System.Windows.Forms.RadioButton();
            this.Feet = new System.Windows.Forms.RadioButton();
            this.Inches = new System.Windows.Forms.RadioButton();
            this.ShowAbbreviations = new System.Windows.Forms.CheckBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Pixels = new System.Windows.Forms.RadioButton();
            this.MicroMeters = new System.Windows.Forms.RadioButton();
            this.RulerLengths = new System.Windows.Forms.Button();
            this.ClearList = new System.Windows.Forms.Button();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.notateXpress1 = new PegasusImaging.WinForms.NotateXpress9.NotateXpress(this.components);
            this.lblLastError = new System.Windows.Forms.Label();
            this.lblerror = new System.Windows.Forms.Label();
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.lstInfo = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.RulerPrecision)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ListBox1
            // 
            this.ListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBox1.Location = new System.Drawing.Point(688, 584);
            this.ListBox1.Name = "ListBox1";
            this.ListBox1.Size = new System.Drawing.Size(192, 43);
            this.ListBox1.TabIndex = 17;
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.Location = new System.Drawing.Point(520, 605);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(160, 24);
            this.Label1.TabIndex = 18;
            this.Label1.Text = "Set Ruler\'s Unit of Precision";
            // 
            // DeleteRulers
            // 
            this.DeleteRulers.Index = 1;
            this.DeleteRulers.Text = "Delete All Rulers";
            this.DeleteRulers.Click += new System.EventHandler(this.DeleteRulers_Click);
            // 
            // MenuItem7
            // 
            this.MenuItem7.Index = 1;
            this.MenuItem7.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.CreateRuler,
            this.DeleteRulers});
            this.MenuItem7.Text = "Rulers";
            // 
            // CreateRuler
            // 
            this.CreateRuler.Index = 0;
            this.CreateRuler.Text = "Create Ruler";
            this.CreateRuler.Click += new System.EventHandler(this.CreateRuler_Click);
            // 
            // FileExit
            // 
            this.FileExit.Index = 4;
            this.FileExit.Text = "Exit";
            this.FileExit.Click += new System.EventHandler(this.FileExit_Click);
            // 
            // LoadAnnotationFile
            // 
            this.LoadAnnotationFile.Index = 3;
            this.LoadAnnotationFile.Text = "Load Annotation File";
            this.LoadAnnotationFile.Click += new System.EventHandler(this.LoadAnnotationFile_Click);
            // 
            // LoadImageFile
            // 
            this.LoadImageFile.Index = 0;
            this.LoadImageFile.Text = "Load Image File";
            this.LoadImageFile.Click += new System.EventHandler(this.LoadImageFile_Click);
            // 
            // MenuItem1
            // 
            this.MenuItem1.Index = 0;
            this.MenuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.LoadImageFile,
            this.SaveImageFile,
            this.SaveAnnotationFile,
            this.LoadAnnotationFile,
            this.FileExit});
            this.MenuItem1.Text = "File";
            // 
            // SaveImageFile
            // 
            this.SaveImageFile.Index = 1;
            this.SaveImageFile.Text = "Save Image File";
            this.SaveImageFile.Click += new System.EventHandler(this.SaveImageFile_Click);
            // 
            // SaveAnnotationFile
            // 
            this.SaveAnnotationFile.Index = 2;
            this.SaveAnnotationFile.Text = "Save Annotation File";
            this.SaveAnnotationFile.Click += new System.EventHandler(this.SaveAnnotationFile_Click);
            // 
            // RulerPrecision
            // 
            this.RulerPrecision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RulerPrecision.Location = new System.Drawing.Point(520, 637);
            this.RulerPrecision.Name = "RulerPrecision";
            this.RulerPrecision.Size = new System.Drawing.Size(151, 45);
            this.RulerPrecision.TabIndex = 19;
            this.RulerPrecision.ValueChanged += new System.EventHandler(this.RulerPrecision_ValueChanged);
            // 
            // ShowMeasurement
            // 
            this.ShowMeasurement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowMeasurement.Checked = true;
            this.ShowMeasurement.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowMeasurement.Location = new System.Drawing.Point(520, 453);
            this.ShowMeasurement.Name = "ShowMeasurement";
            this.ShowMeasurement.Size = new System.Drawing.Size(151, 32);
            this.ShowMeasurement.TabIndex = 12;
            this.ShowMeasurement.Text = "Show/Hide Unit of Measurement";
            this.ShowMeasurement.CheckedChanged += new System.EventHandler(this.ShowMeasurement_CheckedChanged);
            // 
            // ShowGauge
            // 
            this.ShowGauge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowGauge.Checked = true;
            this.ShowGauge.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowGauge.Location = new System.Drawing.Point(520, 549);
            this.ShowGauge.Name = "ShowGauge";
            this.ShowGauge.Size = new System.Drawing.Size(151, 32);
            this.ShowGauge.TabIndex = 14;
            this.ShowGauge.Text = "Show/Hide Ruler\'s Gauge";
            this.ShowGauge.CheckedChanged += new System.EventHandler(this.ShowGauge_CheckedChanged);
            // 
            // Twips
            // 
            this.Twips.Location = new System.Drawing.Point(16, 216);
            this.Twips.Name = "Twips";
            this.Twips.Size = new System.Drawing.Size(112, 16);
            this.Twips.TabIndex = 8;
            this.Twips.Text = "Twips";
            this.Twips.CheckedChanged += new System.EventHandler(this.Twips_CheckedChanged);
            // 
            // Centimeters
            // 
            this.Centimeters.Location = new System.Drawing.Point(16, 24);
            this.Centimeters.Name = "Centimeters";
            this.Centimeters.Size = new System.Drawing.Size(112, 16);
            this.Centimeters.TabIndex = 0;
            this.Centimeters.Text = "Centimeters";
            this.Centimeters.CheckedChanged += new System.EventHandler(this.Centimeters_CheckedChanged);
            // 
            // Kilometers
            // 
            this.Kilometers.Location = new System.Drawing.Point(16, 120);
            this.Kilometers.Name = "Kilometers";
            this.Kilometers.Size = new System.Drawing.Size(112, 16);
            this.Kilometers.TabIndex = 4;
            this.Kilometers.Text = "Kilometers";
            this.Kilometers.CheckedChanged += new System.EventHandler(this.Kilometers_CheckedChanged);
            // 
            // Meters
            // 
            this.Meters.Location = new System.Drawing.Point(16, 144);
            this.Meters.Name = "Meters";
            this.Meters.Size = new System.Drawing.Size(112, 16);
            this.Meters.TabIndex = 5;
            this.Meters.Text = "Meters";
            this.Meters.CheckedChanged += new System.EventHandler(this.Meters_CheckedChanged);
            // 
            // Millimeters
            // 
            this.Millimeters.Location = new System.Drawing.Point(16, 168);
            this.Millimeters.Name = "Millimeters";
            this.Millimeters.Size = new System.Drawing.Size(112, 16);
            this.Millimeters.TabIndex = 6;
            this.Millimeters.Text = "Millimeters";
            this.Millimeters.CheckedChanged += new System.EventHandler(this.Millimeters_CheckedChanged);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.MenuItem1,
            this.MenuItem7,
            this.mnuToolbar,
            this.mnuAbout});
            // 
            // mnuToolbar
            // 
            this.mnuToolbar.Index = 2;
            this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuToolbarShow});
            this.mnuToolbar.Text = "Toolbar";
            // 
            // mnuToolbarShow
            // 
            this.mnuToolbarShow.Index = 0;
            this.mnuToolbarShow.Text = "Hide";
            this.mnuToolbarShow.Click += new System.EventHandler(this.mnuToolbarShow_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 3;
            this.mnuAbout.Text = "&About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // Miles
            // 
            this.Miles.Location = new System.Drawing.Point(16, 240);
            this.Miles.Name = "Miles";
            this.Miles.Size = new System.Drawing.Size(112, 16);
            this.Miles.TabIndex = 9;
            this.Miles.Text = "Miles";
            this.Miles.CheckedChanged += new System.EventHandler(this.Miles_CheckedChanged);
            // 
            // Feet
            // 
            this.Feet.Location = new System.Drawing.Point(16, 48);
            this.Feet.Name = "Feet";
            this.Feet.Size = new System.Drawing.Size(112, 16);
            this.Feet.TabIndex = 1;
            this.Feet.Text = "Feet";
            this.Feet.CheckedChanged += new System.EventHandler(this.Feet_CheckedChanged);
            // 
            // Inches
            // 
            this.Inches.Location = new System.Drawing.Point(16, 72);
            this.Inches.Name = "Inches";
            this.Inches.Size = new System.Drawing.Size(112, 16);
            this.Inches.TabIndex = 2;
            this.Inches.Text = "Inches";
            this.Inches.CheckedChanged += new System.EventHandler(this.Inches_CheckedChanged);
            // 
            // ShowAbbreviations
            // 
            this.ShowAbbreviations.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowAbbreviations.Location = new System.Drawing.Point(520, 501);
            this.ShowAbbreviations.Name = "ShowAbbreviations";
            this.ShowAbbreviations.Size = new System.Drawing.Size(151, 32);
            this.ShowAbbreviations.TabIndex = 13;
            this.ShowAbbreviations.Text = "Show Abbreviations";
            this.ShowAbbreviations.CheckedChanged += new System.EventHandler(this.ShowAbbreviations_CheckedChanged);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox1.Controls.Add(this.Miles);
            this.GroupBox1.Controls.Add(this.Twips);
            this.GroupBox1.Controls.Add(this.Pixels);
            this.GroupBox1.Controls.Add(this.Millimeters);
            this.GroupBox1.Controls.Add(this.Meters);
            this.GroupBox1.Controls.Add(this.Kilometers);
            this.GroupBox1.Controls.Add(this.MicroMeters);
            this.GroupBox1.Controls.Add(this.Inches);
            this.GroupBox1.Controls.Add(this.Feet);
            this.GroupBox1.Controls.Add(this.Centimeters);
            this.GroupBox1.Location = new System.Drawing.Point(511, 72);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(144, 357);
            this.GroupBox1.TabIndex = 11;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Unit of Measurement";
            // 
            // Pixels
            // 
            this.Pixels.Location = new System.Drawing.Point(16, 192);
            this.Pixels.Name = "Pixels";
            this.Pixels.Size = new System.Drawing.Size(112, 16);
            this.Pixels.TabIndex = 7;
            this.Pixels.Text = "Pixels";
            this.Pixels.CheckedChanged += new System.EventHandler(this.Pixels_CheckedChanged);
            // 
            // MicroMeters
            // 
            this.MicroMeters.Location = new System.Drawing.Point(16, 96);
            this.MicroMeters.Name = "MicroMeters";
            this.MicroMeters.Size = new System.Drawing.Size(112, 16);
            this.MicroMeters.TabIndex = 3;
            this.MicroMeters.Text = "MicroMeters";
            this.MicroMeters.CheckedChanged += new System.EventHandler(this.MicroMeters_CheckedChanged);
            // 
            // RulerLengths
            // 
            this.RulerLengths.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RulerLengths.Location = new System.Drawing.Point(695, 525);
            this.RulerLengths.Name = "RulerLengths";
            this.RulerLengths.Size = new System.Drawing.Size(73, 32);
            this.RulerLengths.TabIndex = 15;
            this.RulerLengths.Text = "Get Ruler Lengths";
            this.RulerLengths.Click += new System.EventHandler(this.RulerLengths_Click);
            // 
            // ClearList
            // 
            this.ClearList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearList.Location = new System.Drawing.Point(800, 525);
            this.ClearList.Name = "ClearList";
            this.ClearList.Size = new System.Drawing.Size(71, 32);
            this.ClearList.TabIndex = 16;
            this.ClearList.Text = "Clear List";
            this.ClearList.Click += new System.EventHandler(this.ClearList_Click);
            // 
       
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = PegasusImaging.WinForms.NotateXpress9.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(680, 309);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(184, 32);
            this.lblLastError.TabIndex = 22;
            this.lblLastError.Text = "Last Error:";
            // 
            // lblerror
            // 
            this.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblerror.Location = new System.Drawing.Point(688, 357);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(192, 128);
            this.lblerror.TabIndex = 21;
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.Location = new System.Drawing.Point(16, 72);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(488, 600);
            this.imageXView1.TabIndex = 25;
            // 
            // lstInfo
            // 
            this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstInfo.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Adding Ruler annotations and setting the various attributes."});
            this.lstInfo.Location = new System.Drawing.Point(16, 8);
            this.lstInfo.Name = "lstInfo";
            this.lstInfo.Size = new System.Drawing.Size(855, 30);
            this.lstInfo.TabIndex = 26;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(888, 697);
            this.Controls.Add(this.lstInfo);
            this.Controls.Add(this.imageXView1);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.RulerPrecision);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.ListBox1);
            this.Controls.Add(this.ClearList);
            this.Controls.Add(this.RulerLengths);
            this.Controls.Add(this.ShowAbbreviations);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.ShowGauge);
            this.Controls.Add(this.ShowMeasurement);
            this.Menu = this.MainMenu1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "C# Rulers Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RulerPrecision)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		
		#region Pegasus Imaging Sample Application Standard Functions

		System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		string strDefaultImageFilter = ("All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" +
			"" + (";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" + ("2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" + ("t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" + ("e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" + (")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" + ("cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" + ("s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" + "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"))))))));

    
		private string GetFileName(string FullName) 
		{
			return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length 
				- (FullName.LastIndexOf("\\") + 1)));
		}
    
		private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ ((ErrorException.Source + "") 
				+ "\n"))));
		}
    
		private static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
		}
    
		private string PegasusOpenFile(string sTitle, string sFileFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFileFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}
    
		private string PegasusSaveFile(string sTitle, string sFilter) 
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}

		#endregion


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		
		private void Form1_Load(object sender, System.EventArgs e)
		{	
			try 
			{
				//**The UnlockRuntime function must be called to distribute the runtime**
				//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

				// Create a new load options object so we can recieve events from the images we load
				loIXLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
				soIXSaveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
				soIXSaveOptions.Tiff.Compression = PegasusImaging.WinForms.ImagXpress9.Compression.Group4;
				soNXSaveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();
				soNXLoadOptions = new PegasusImaging.WinForms.NotateXpress9.LoadOptions();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}

			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString (cultNumber);
			string strImagePath = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\benefits.tif");
			strCurrentDir = strCommonImagesDirectory;
			try
			{
				imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImagePath);
				imageXView1.AutoScroll = true;
				notateXpress1.ClientWindow = imageXView1.Handle;
				notateXpress1.ToolbarDefaults.ToolbarActivated = true;
				// Create a new layer and its toolbar
				createNewLayer(System.IntPtr.Zero);
			}
			catch (Exception ex)
			{
				MessageBox.Show("FormMain_Load Error msg:   " + ex.Message + "\n" + 
					"Error type:  " + ex.GetType().ToString());
			}
		}

		private PegasusImaging.WinForms.NotateXpress9.Layer createNewLayer(System.IntPtr DibHandle)
		{
			// ---------------
			// create new layer
			// ---------------
			PegasusImaging.WinForms.NotateXpress9.Layer layer = new Layer();

			// ---------------
			// Add the new layer to the collection and make it the selected layer
			// ---------------

			notateXpress1.Layers.Add(layer);
			layer.Select();

			layer.Description = "Layer created " + System.DateTime.Now.ToShortTimeString ();
			layer.Active = true; // probably default value
			layer.Visible = true; // probably default value
			if(notateXpress1.InteractMode == AnnotationMode.Edit)
			{
				layer.Toolbar.Visible = true;
				mnuToolbarShow.Text = "Hide";
			}
			else
			{
				layer.Toolbar.Visible = false;
				mnuToolbarShow.Text = "Show";
			}
			// ---------------
			// set the button tool text and pen width for this demo
			// ---------------
			layer.Toolbar.ButtonToolbarDefaults.PenWidth = 4;
			layer.Toolbar.ButtonToolbarDefaults.Text = "Click me";

			// ---------------
			// set the image tool defaults for this demo
			// ---------------
			layer.Toolbar.ImageToolbarDefaults.BackStyle = BackStyle.Transparent;
			if(DibHandle != IntPtr.Zero)
				layer.Toolbar.ImageToolbarDefaults.DibHandle = DibHandle;

			// ---------------
			// set the stamp tool text and font for this demo
			// ---------------
			layer.Toolbar.StampToolbarDefaults.Text = "NotateXpress 8";
			System.Drawing.Font fontCurrent = layer.Toolbar.StampToolbarDefaults.TextFont;
			System.Drawing.Font fontNew = new System.Drawing.Font (fontCurrent.Name, 16, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
			layer.Toolbar.StampToolbarDefaults.TextFont = fontNew;

			return layer;
		}

		private void ShowAbbreviations_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool)
						((RulerTool)layer.Elements[iCounter]).ShowAbbreviation = ShowAbbreviations.Checked;
				}
			}
		}

		private void ShowMeasurement_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool)
						((RulerTool)layer.Elements[iCounter]).ShowLength = ShowMeasurement.Checked;
				}
			}
		}

		private void ShowGauge_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool)
						((RulerTool)layer.Elements[iCounter]).ShowGauge = ShowGauge.Checked;
				}
			}
		}

		private void RulerPrecision_ValueChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool)
						((RulerTool)layer.Elements[iCounter]).Precision = RulerPrecision.Value;
				}
			}
		}

		private void RulerLengths_Click(object sender, System.EventArgs e)
		{
			int iCounter;
			int iNumRulers;

			iNumRulers = 0;

			ListBox1.Items.Clear();

			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool)
					{
						iNumRulers = iNumRulers + 1;
						ListBox1.Items.Add("Ruler#" + iNumRulers + "     " + ((RulerTool)layer.Elements[iCounter]).RulerLength);
					}
				}
			}
		}

		private void ClearList_Click(object sender, System.EventArgs e)
		{
			ListBox1.Items.Clear();
		}

		private void LoadFile() 
		{
			try 
			{
				PegasusImaging.WinForms.ImagXpress9.ImageX oldImage = imageXView1.Image;
				imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFileName, loIXLoadOptions);
				imageXView1.Image = imagX1;
				if (oldImage != null)
				{
					oldImage.Dispose();
					oldImage = null;
				}
				lblerror.Text = "";
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
		}

		private void LoadImageFile_Click(object sender, System.EventArgs e)
		{
			strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter);
			LoadFile();
			notateXpress1.Layers.Clear();
			layer = new PegasusImaging.WinForms.NotateXpress9.Layer();
			layer.Active = true;
			notateXpress1.Layers.Add(layer);
			mnuToolbarShow.Text = "Hide";
			notateXpress1.ToolbarDefaults.ToolbarActivated = true;
        }

		private void SaveImageFile_Click(object sender, System.EventArgs e)
		{
			try 
			{
				strSaveFileName = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif");
				imagX1.Save(strSaveFileName, soIXSaveOptions);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}
		}

		private void SaveAnnotationFile_Click(object sender, System.EventArgs e)
		{
			strSaveFileName = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp");
			try 
			{
				notateXpress1.Layers.Comments = "This file was saved today";
				notateXpress1.Layers.Subject = "testing 123";
				notateXpress1.Layers.Title = "One awesome annotation file";
				notateXpress1.Layers.SaveToFile(strSaveFileName, soNXSaveOptions);
			}
			catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}
		}

		private void LoadAnnotationFile_Click(object sender, System.EventArgs e)
		{
			strNXPFileName = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp");
			try 
			{
				notateXpress1.Layers.FromFile(strNXPFileName, soNXLoadOptions);
				mnuToolbarShow.Text = "Hide";
				notateXpress1.ToolbarDefaults.ToolbarActivated = true;
			}
			catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}
		}

		private void FileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void CreateRuler_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				RulerTool rulerTool = new RulerTool();
				rulerTool.PenWidth = 4;
				Rectangle rect = new Rectangle(25, 50, 100, 100);
                rulerTool.BoundingRectangle = rect;
				//set the size of the gauge
				rulerTool.GaugeLength = 20;
				//set the unit of measurement being used for the ruler
				rulerTool.MeasurementUnit = MeasurementUnit.Inches;
				Inches.Checked = true;
				//set the precision being used for the ruler
				rulerTool.Precision = 1;
				//set the color for the units of measurement
				rulerTool.TextColor = System.Drawing.Color.Blue;
				//append the ruler to image
				layer.Elements.Add(rulerTool);
				//set the annotation to selected
				rulerTool.Selected = true;
			}
		}
		
		private void DeleteRulers_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				notateXpress1.Layers.Remove(layer);
				createNewLayer(System.IntPtr.Zero);			
			}
		}

		/// <summary>
		/// The following are all of the units of measurement radio buttons
		/// </summary>
		
		private void Centimeters_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Centimeters.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Centimeters;
				}
			}
		}

		private void Feet_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Feet.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Feet;
				}
			}
		}

		private void Inches_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Inches.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Inches;
				}
			}
		}

		private void MicroMeters_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && MicroMeters.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.MicroMeters;
				}
			}
		}

		private void Kilometers_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Kilometers.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Kilometers;
				}
			}
		}

		private void Meters_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Meters.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Meters;
				}
			}
		}

		private void Millimeters_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Millimeters.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Millimeters;
				}
			}
		}

		private void Pixels_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Pixels.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Pixels;
				}
			}
		}

		private void Twips_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Twips.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Twips;
				}
			}
		}

		private void Miles_CheckedChanged(object sender, System.EventArgs e)
		{
			int iCounter;
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(iCounter=0; iCounter < layer.Elements.Count; iCounter++)
				{
					if(layer.Elements[iCounter] is RulerTool && Miles.Checked)
						((RulerTool)layer.Elements[iCounter]).MeasurementUnit = MeasurementUnit.Miles;
				}
			}
		}

		private void mnuToolbarShow_Click(object sender, System.EventArgs e)
		{
			if ((notateXpress1.ToolbarDefaults.ToolbarActivated == true)) 
			{
				mnuToolbarShow.Text = "Show";
				notateXpress1.ToolbarDefaults.ToolbarActivated = false;
			}
			else 
			{
				mnuToolbarShow.Text = "Hide";
				notateXpress1.ToolbarDefaults.ToolbarActivated = true;
			}
		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			notateXpress1.AboutBox();
		}
					
	}
}

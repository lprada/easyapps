'****************************************************************'
'* Copyright 2008- Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress9
Imports PegasusImaging.WinForms.NotateXpress9
Imports System.Windows.Forms
Imports System
Imports System.Drawing


Public Class MainForm
    Inherits System.Windows.Forms.Form

    'global variables declared here
    Private strImageFileName As String
    Private strNXPFileName As String
    Private strCurrentDir As String
    Private strSaveFileName As String
    Private strCommonImagesDirectory As String

    Private imagX1 As PegasusImaging.WinForms.ImagXpress9.ImageX
    Private layer As PegasusImaging.WinForms.NotateXpress9.Layer
    Private loIXLoadOptions As PegasusImaging.WinForms.ImagXpress9.LoadOptions
    Private soIXSaveOptions As PegasusImaging.WinForms.ImagXpress9.SaveOptions
    Private soNXSaveOptions As PegasusImaging.WinForms.NotateXpress9.SaveOptions
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents NotateXpress1 As PegasusImaging.WinForms.NotateXpress9.NotateXpress
    Friend WithEvents NoteMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents BlockItem As System.Windows.Forms.MenuItem
    Friend WithEvents NotateXpressMenuItem As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpressMenuItem As System.Windows.Forms.MenuItem
    Private soNXLoadOptions As PegasusImaging.WinForms.NotateXpress9.LoadOptions

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            ' Don't forget to dispose NXP
            '                
            If (Not (NotateXpress1) Is Nothing) Then
                NotateXpress1.ClientDisconnect()

                NotateXpress1.Dispose()
                NotateXpress1 = Nothing
            End If
            ' Don't forget to dispose IX
            '
            If (Not (ImageXView1) Is Nothing) Then
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If (Not (ImagXpress1) Is Nothing) Then
                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If (Not (imagX1) Is Nothing) Then
                imagX1.Dispose()
                imagX1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSave As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWang As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSaveA As System.Windows.Forms.MenuItem
    Friend WithEvents mnuLoadAnno As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents mnuCreate As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTxt As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRedac As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRuler As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRects As System.Windows.Forms.MenuItem
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuOpen = New System.Windows.Forms.MenuItem
        Me.mnuSave = New System.Windows.Forms.MenuItem
        Me.mnuWang = New System.Windows.Forms.MenuItem
        Me.mnuSaveA = New System.Windows.Forms.MenuItem
        Me.mnuLoadAnno = New System.Windows.Forms.MenuItem
        Me.mnuExit = New System.Windows.Forms.MenuItem
        Me.mnuCreate = New System.Windows.Forms.MenuItem
        Me.mnuTxt = New System.Windows.Forms.MenuItem
        Me.mnuRedac = New System.Windows.Forms.MenuItem
        Me.mnuRuler = New System.Windows.Forms.MenuItem
        Me.mnuRects = New System.Windows.Forms.MenuItem
        Me.NoteMenuItem = New System.Windows.Forms.MenuItem
        Me.BlockItem = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem
        Me.mnuAbout = New System.Windows.Forms.MenuItem
        Me.ImagXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.NotateXpressMenuItem = New System.Windows.Forms.MenuItem
        Me.lblerror = New System.Windows.Forms.Label
        Me.lstInfo = New System.Windows.Forms.ListBox
        Me.lblLastError = New System.Windows.Forms.Label
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.NotateXpress1 = New PegasusImaging.WinForms.NotateXpress9.NotateXpress(Me.components)
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuCreate, Me.MenuItem1, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.mnuSave, Me.mnuWang, Me.mnuSaveA, Me.mnuLoadAnno, Me.mnuExit})
        Me.mnuFile.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Text = "&Open Image"
        '
        'mnuSave
        '
        Me.mnuSave.Index = 1
        Me.mnuSave.Text = "&Save Image"
        '
        'mnuWang
        '
        Me.mnuWang.Checked = True
        Me.mnuWang.Index = 2
        Me.mnuWang.Text = "&Wang Compatible"
        '
        'mnuSaveA
        '
        Me.mnuSaveA.Index = 3
        Me.mnuSaveA.Text = "&Save Annotation"
        '
        'mnuLoadAnno
        '
        Me.mnuLoadAnno.Index = 4
        Me.mnuLoadAnno.Text = "&Load Annotation"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 5
        Me.mnuExit.Text = "&Exit"
        '
        'mnuCreate
        '
        Me.mnuCreate.Index = 1
        Me.mnuCreate.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuTxt, Me.mnuRedac, Me.mnuRuler, Me.mnuRects, Me.NoteMenuItem, Me.BlockItem})
        Me.mnuCreate.Text = "&Create Annotations"
        '
        'mnuTxt
        '
        Me.mnuTxt.Index = 0
        Me.mnuTxt.Text = "Add Text"
        '
        'mnuRedac
        '
        Me.mnuRedac.Index = 1
        Me.mnuRedac.Text = "Add Redaction"
        '
        'mnuRuler
        '
        Me.mnuRuler.Index = 2
        Me.mnuRuler.Text = "Add Ruler"
        '
        'mnuRects
        '
        Me.mnuRects.Index = 3
        Me.mnuRects.Text = "Add Rectangles"
        '
        'NoteMenuItem
        '
        Me.NoteMenuItem.Index = 4
        Me.NoteMenuItem.Text = "Add Note"
        '
        'BlockItem
        '
        Me.BlockItem.Index = 5
        Me.BlockItem.Text = "Add Blockhighlight"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 2
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.MenuItem1.Text = "&Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 3
        Me.mnuAbout.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ImagXpressMenuItem, Me.NotateXpressMenuItem})
        Me.mnuAbout.Text = "&About"
        '
        'ImagXpressMenuItem
        '
        Me.ImagXpressMenuItem.Index = 0
        Me.ImagXpressMenuItem.Text = "Imag&Xpress"
        '
        'NotateXpressMenuItem
        '
        Me.NotateXpressMenuItem.Index = 1
        Me.NotateXpressMenuItem.Text = "&NotateXpress"
        '
        'lblerror
        '
        Me.lblerror.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblerror.BackColor = System.Drawing.SystemColors.Control
        Me.lblerror.Location = New System.Drawing.Point(584, 395)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(192, 109)
        Me.lblerror.TabIndex = 1
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Creating various annotations programatically."})
        Me.lstInfo.Location = New System.Drawing.Point(12, 24)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(780, 69)
        Me.lstInfo.TabIndex = 2
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLastError.Location = New System.Drawing.Point(584, 352)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(184, 32)
        Me.lblLastError.TabIndex = 3
        Me.lblLastError.Text = "Last Error:"
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(12, 111)
        Me.ImageXView1.MouseWheelCapture = False
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(530, 356)
        Me.ImageXView1.TabIndex = 6
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit
        Me.NotateXpress1.LineScaling = PegasusImaging.WinForms.NotateXpress9.LineScaling.Normal
        Me.NotateXpress1.MultiLineEdit = False
        Me.NotateXpress1.RecalibrateXdpi = -1
        Me.NotateXpress1.RecalibrateYdpi = -1
        Me.NotateXpress1.ToolTipTimeEdit = 0
        Me.NotateXpress1.ToolTipTimeInteractive = 0
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(808, 535)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.lblLastError)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.lblerror)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Simple Annotations"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"

    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile(ByVal sTitle As String, ByVal sFileFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFileFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusSaveFile(ByVal sTitle As String, ByVal sFilter As String) As String
        Dim dlg As SaveFileDialog = New SaveFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub mnuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click

        Try
            'Open an image file with ImagXpress

            strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter)
            LoadFile()

            NotateXpress1.Layers.Clear()

            layer = New PegasusImaging.WinForms.NotateXpress9.Layer()
            layer.Active = True
            NotateXpress1.Layers.Add(layer)

            mnuToolbarShow.Text = "Hide"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadFile()
        Try
            'Open an image file with ImagXpress
            Dim oldImage As PegasusImaging.WinForms.ImagXpress9.ImageX = ImageXView1.Image
            imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFileName)
            ImageXView1.Image = imagX1
            lblerror.Text = ""
            If Not oldImage Is Nothing Then
                oldImage.Dispose()
            End If
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub simpleannotations_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Try
                '**The UnlockRuntime function must be called to distribute the runtime**
                'imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

                'Create a new load options object so we can recieve events from the images we load
                loIXLoadOptions = New PegasusImaging.WinForms.ImagXpress9.LoadOptions()

                soIXSaveOptions = New PegasusImaging.WinForms.ImagXpress9.SaveOptions()
                soIXSaveOptions.Tiff.Compression = PegasusImaging.WinForms.ImagXpress9.Compression.Group4

                soNXSaveOptions = New PegasusImaging.WinForms.NotateXpress9.SaveOptions()
                soNXLoadOptions = New PegasusImaging.WinForms.NotateXpress9.LoadOptions()

                Application.EnableVisualStyles()
            Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
                PegasusError(ex, lblerror)
            End Try

            strCurrentDir = System.IO.Directory.GetCurrentDirectory()
            strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\benefits.tif")
            strCommonImagesDirectory = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
            strCurrentDir = strCommonImagesDirectory

            imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFileName)
            ImageXView1.Image = imagX1
            'Connect ImagXpress to NotateXpress
            NotateXpress1.ClientWindow = ImageXView1.Handle
            layer = New PegasusImaging.WinForms.NotateXpress9.Layer()
            layer.Active = True
            NotateXpress1.Layers.Add(layer)
            'disable the toolbar 
            NotateXpress1.ToolbarDefaults.ToolbarActivated = False
            NotateXpress1.MenuSetEnabled(PegasusImaging.WinForms.NotateXpress9.MenuType.Context, PegasusImaging.WinForms.NotateXpress9.AnnotationTool.NoTool, True)

            NotateXpress1.ToolbarDefaults.StampToolbarDefaults.Text = "NotateXpress9"

            Dim dib As IntPtr
            Try
                Using img As ImageX = ImageX.FromFile(ImagXpress1, strCommonImagesDirectory & "\shack8.tif")
                    dib = img.ToHdib(True)
                    NotateXpress1.ToolbarDefaults.ImageToolbarDefaults.DibHandle = dib
                End Using
            Finally
                System.Runtime.InteropServices.Marshal.FreeHGlobal(dib)
            End Try

            NotateXpress1.ToolbarDefaults.ToolbarActivated = True

        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
        Try
            'Toggle the showing of the NotateXpress toolbar
            If NotateXpress1.ToolbarDefaults.ToolbarActivated = True Then
                mnuToolbarShow.Text = "Show"
                NotateXpress1.ToolbarDefaults.ToolbarActivated = False
            Else
                mnuToolbarShow.Text = "Hide"
                NotateXpress1.ToolbarDefaults.ToolbarActivated = True
            End If
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuTxt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTxt.Click
        Try
            '**** Text Annotation
            Dim rect As System.Drawing.Rectangle
            rect.X = 30
            rect.Y = 30
            rect.Width = 280
            rect.Height = 300
            Dim text As New PegasusImaging.WinForms.NotateXpress9.TextTool()

            text.TextJustification = PegasusImaging.WinForms.NotateXpress9.TextJustification.Center
            text.Text = "Rob Young"
            text.BackColor = System.Drawing.Color.Red
            text.BoundingRectangle = rect
            text.PenWidth = 2
            text.BackStyle = PegasusImaging.WinForms.NotateXpress9.BackStyle.Translucent
            text.PenWidth = 2
            text.ToolTipText = "Text"
            'Add the annotation to the image
            layer.Elements.Add(text)
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuRects_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRects.Click
        Try
            'Rectangle Annotation
            Dim counter As Integer
            Dim rect As System.Drawing.Rectangle
            rect.X = 30
            rect.Y = 30
            rect.Width = 60
            rect.Height = 60
            Dim rand As New System.Random()
            For counter = 1 To 10
                Dim rectangle As New PegasusImaging.WinForms.NotateXpress9.RectangleTool()
                rectangle.BoundingRectangle = rect
                rectangle.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                rectangle.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                rectangle.ToolTipText = "Rectangle" + counter.ToString()

                layer.Elements.Add(rectangle)
                rect.X = rect.X + 5
                rect.Y = rect.Y + 5
            Next
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuRedac_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRedac.Click
        Try
            'Add a redaction annotation
            Dim rect As New System.Drawing.Rectangle(100, 100, 150, 150)
            Dim text As New PegasusImaging.WinForms.NotateXpress9.TextTool()
            text.Text = "Approved by Pegasus Imaging Corp." & Space(2) & Now
            text.TextJustification = PegasusImaging.WinForms.NotateXpress9.TextJustification.Center
            text.TextColor = System.Drawing.Color.Red
            text.PenWidth = 1
            text.PenColor = System.Drawing.Color.White
            text.BackStyle = PegasusImaging.WinForms.NotateXpress9.BackStyle.Opaque
            text.BackColor = System.Drawing.Color.White
            text.BoundingRectangle = rect
            text.ToolTipText = "Redaction"
            layer.Elements.Add(text)
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuRuler_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRuler.Click
        Try
            'Add a ruler annotation
            Dim rect As New System.Drawing.Rectangle(25, 50, 200, 100)
            Dim ruler As New PegasusImaging.WinForms.NotateXpress9.RulerTool()
            ruler.BoundingRectangle = rect
            ruler.GaugeLength = 20
            ruler.MeasurementUnit = PegasusImaging.WinForms.NotateXpress9.MeasurementUnit.Pixels
            ruler.Precision = 1
            ruler.BackColor = System.Drawing.Color.Blue
            ruler.ToolTipText = "Ruler"
            layer.Elements.Add(ruler)
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSave.Click
        Try
            'save the image file with Imagxpress
            strSaveFileName = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif")
            imagX1.Save(strSaveFileName, soIXSaveOptions)
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub mnuSaveA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveA.Click
        strSaveFileName = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            'save the annotation to NXP
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strSaveFileName, soNXSaveOptions)
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub mnuWang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWang.Click
        Try
            'set the Wang annotation save options
            If soNXSaveOptions.PreserveWangLayers Then
                soNXSaveOptions.PreserveWangLayers = False
                mnuWang.Checked = False
            Else
                soNXSaveOptions.PreserveWangLayers = True
                mnuWang.Checked = True
            End If
        Catch ex As Exception

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuLoadAnno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLoadAnno.Click
        strNXPFileName = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            'set the LoadOptions for NotateXpress
            NotateXpress1.Layers.FromFile(strNXPFileName, soNXLoadOptions)
            mnuToolbarShow.Text = "Hide"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub NoteMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NoteMenuItem.Click
        Try
            'Add a note annotation
            Dim note As NoteTool = New PegasusImaging.WinForms.NotateXpress9.NoteTool()

            note.BackColor = Color.Red
            note.BoundingRectangle = New Rectangle(50, 50, 200, 200)
            note.Text = "Note Tool"
            note.TextColor = Color.PowderBlue
            note.ToolTipText = "Note Tool"
            layer.Elements.Add(note)


        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException

            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BlockItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BlockItem.Click
        Try
            'Add a blockhighlight annotation
            Dim highlight As PegasusImaging.WinForms.NotateXpress9.BlockHighlightTool = New BlockHighlightTool()

            highlight.BoundingRectangle = New Rectangle(100, 100, 300, 300)
            highlight.FillColor = Color.Blue
            highlight.ToolTipText = "Highlight Block"
            layer.Elements.Add(highlight)

        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub NotateXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotateXpressMenuItem.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub ImagXpressMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImagXpressMenuItem.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub ImageXView1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImageXView1.Load

    End Sub
End Class
